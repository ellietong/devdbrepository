SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vtblPerson]
AS
SELECT DISTINCT p.PersonID, 
				p.FirmID, 
				p.FirmAddressID,
				p.PlaintiffCounselAddressID, 
				p.LName, 
				p.FName, 
				p.Email, 
				p.Status, 
				p.Password,
				p.PositionID, 
				pe.InvoiceContact, 
				pe.ComplaintContact, 
                pe.PrimaryContact, 
				p.LastPasswordChangedDate, 
				p.LastLoginDate, 
				p.IsLockedOut, 
				COALESCE (pe.NoFilters, 0) | COALESCE (fac.NoFilters, 0) AS NoFilters,
				pe.TenderContactCC,
				pe.TenderContactBCC, 
				CASE WHEN pr.RoleID = 16 THEN 1 ELSE 0 END AS InsurerRole,
				CASE WHEN p.PersonID IN (161, 3398) THEN 1 ELSE 0 END AS NCCContact,
				p.CarrierGroupID, 
				pe.TenderContactTo
FROM [Ligado-ExtranetSecurity].dbo.vPerson AS p 
	INNER JOIN [Ligado-ExtranetSecurity].dbo.tblPermissions AS pe ON p.PersonID = pe.PersonID 
	INNER JOIN [Ligado-ExtranetSecurity].dbo.tblFirmCase AS fc ON fc.FirmID = p.FirmID 
	LEFT JOIN [Ligado-ExtranetSecurity].dbo.tblFirmAddressCase AS fac ON p.FirmAddressID = fac.FirmAddressID 
			AND pe.CaseID = fac.CaseID
	INNER JOIN [Ligado-ExtranetSecurity].dbo.tblPermissionsRole AS pr ON pr.FirmID = p.FirmID 
			AND pr.CaseID = pe.CaseID
WHERE (pe.CaseID = dbo.fn_GetCaseID())

GO
GRANT VIEW DEFINITION ON  [dbo].[vtblPerson] TO [base_user]
GRANT SELECT ON  [dbo].[vtblPerson] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vtblPerson] TO [power_user]
GRANT SELECT ON  [dbo].[vtblPerson] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 342
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pe"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "fc"
            Begin Extent = 
               Top = 126
               Left = 250
               Bottom = 245
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pr"
            Begin Extent = 
               Top = 6
               Left = 380
               Bottom = 125
               Right = 556
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "fac"
            Begin Extent = 
               Top = 6
               Left = 594
               Bottom = 125
               Right = 777
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 31
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1', 'SCHEMA', N'dbo', 'VIEW', N'vtblPerson', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vtblPerson', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vtblPerson', NULL, NULL
GO
