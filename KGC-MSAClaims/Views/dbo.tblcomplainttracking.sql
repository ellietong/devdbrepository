SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [dbo].[tblcomplainttracking] as
select * from [kga-claimsadmin].dbo.tblComplaintTracking
GO
GRANT VIEW DEFINITION ON  [dbo].[tblcomplainttracking] TO [base_user]
GRANT SELECT ON  [dbo].[tblcomplainttracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblcomplainttracking] TO [power_user]
GRANT SELECT ON  [dbo].[tblcomplainttracking] TO [power_user]
GO
