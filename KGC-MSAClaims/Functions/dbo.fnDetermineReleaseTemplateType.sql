SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Function [dbo].[fnDetermineReleaseTemplateType] (@ClaimantPersonalInfoID bigint)
Returns Int

as

Begin
--Declare @ClaimantPersonalInfoID bigint
--Set @ClaimantPersonalInfoID = 43369

Declare @DeceasedTest bit
Declare @MarriedTest bit
Declare @SpouseNotAsExecutor bit
Declare @ReleaseTypeID int
Declare @ReleaseFormat int
Declare @ReleaseTemplateTypeID int

Set @ReleaseTypeID = (Select Case When ReleaseTypeID like '2' then 1 else 0 end from tblClaimantPersonalInfo cpi left join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID left join tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and IsPrimaryForClaim=1)

Set @DeceasedTest = (Select Case When DeceasedDate is not null then 1 Else 0 End From tblClaimantPersonalInfo where ClaimantPersonalInfoID = @ClaimantPersonalInfoID)

Set @MarriedTest = (Select Case When a.ClaimantRelatedPartyID is not null then 1 else 0 End From tblClaimantPersonalInfo cpi 
	Left Join (Select ClaimantPersonalInfoID, ClaimantRelatedPartyID From tblClaimantRelatedParty Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID and RelationshipID = 9) a 
		on a.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID)
	
Set @SpouseNotAsExecutor = (Select Case When a.ClaimantRelatedPartyID is not null then 1 else 0 End From tblClaimantPersonalInfo cpi 
	Left Join (Select ClaimantPersonalInfoID, ClaimantRelatedPartyID From tblClaimantRelatedParty Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID and RelationshipID = 9 and LegalCapacityID in (19, 0)) a 
		on a.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID)
	
IF @DeceasedTest = 1 AND @MarriedTest = 1 AND @SpouseNotAsExecutor = 0 BEGIN
			--Insert Full Spouse Release
Set @ReleaseFormat = 1
END
IF @DeceasedTest = 1 AND @MarriedTest = 1 AND @SpouseNotAsExecutor = 1 BEGIN
			--Insert Full Executor Spouse
Set @ReleaseFormat = 2
END
IF @DeceasedTest = 1 AND @MarriedTest = 0 BEGIN
			--Insert Full Executor Third
Set @ReleaseFormat = 3
END
IF @DeceasedTest = 0 AND @ReleaseTypeID = 1 AND @MarriedTest = 1 BEGIN
		--Insert Full HW
Set @ReleaseFormat = 4
END
IF @DeceasedTest = 0 AND @ReleaseTypeID = 1 AND @MarriedTest = 0 BEGIN
		--Insert Full Single
Set @ReleaseFormat = 5
END
IF @DeceasedTest = 0 AND @ReleaseTypeID = 0 AND @MarriedTest = 1 BEGIN
		--Insert Limited HW
Set @ReleaseFormat = 6
END
IF @DeceasedTest = 0 AND @ReleaseTypeID = 0 AND @MarriedTest = 0 BEGIN
		--Insert Limited Single
Set @ReleaseFormat = 7
END

Set @ReleaseTemplateTypeID = (Select @ReleaseFormat As ReleaseFormat)

Return @ReleaseTemplateTypeID

End
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDetermineReleaseTemplateType] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnDetermineReleaseTemplateType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnDetermineReleaseTemplateType] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[fnDetermineReleaseTemplateType] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[fnDetermineReleaseTemplateType] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnDetermineReleaseTemplateType] TO [power_user]
GO
