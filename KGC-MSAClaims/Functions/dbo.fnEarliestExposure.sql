SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE FUNCTION [dbo].[fnEarliestExposure]
(@ClaimantPersonalInfoID int)

RETURNS date
AS
BEGIN

--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 75049

DECLARE @EarliestExposure Date

/**SECTION OF CODE TO USE IF YOU DO WANT TO CONSIDER PRODUCTS IN THE EXPOSURE CALCULATION. IUNA AND NSI FOR EXAMPLE**/

IF (SELECT COUNT(ClaimantProductID)	From tblclaimantemployment ce 
inner join tblclaimantproduct cp on cp.claimantemploymentid = ce.claimantemploymentID 
inner join tblProduct p on p.ProductID = cp.ProductID 
where ClaimantPersonalInfoID = @ClaimantPersonalInfoID and p.ProductManufacturerID=1) = 0
BEGIN 

SET @EarliestExposure = (select Min(EmpStartDate) as EarliestExposureDate
from tblClaimantEmployment
where ClaimantPersonalInfoID=@ClaimantPersonalInfoID)

END
ELSE 
BEGIN

SET @EarliestExposure = (select MIN(EarliestExposureDate) as EarliestExposureDate
From
(SELECT case when coalesce(min(cp.StartDate), '1/1/1900') < ce.EmpStartDate then coalesce(Min(cp.startdate),ce.empstartdate) else MIN(cp.startdate) end as EarliestExposureDate
From tblclaimantemployment ce 
inner join tblclaimantproduct cp on cp.claimantemploymentid = ce.claimantemploymentID
inner join tblProduct p on p.ProductID = cp.ProductID 
where ClaimantPersonalInfoID = @ClaimantPersonalInfoID and p.ProductManufacturerID = 1
group by ce.EmpStartDate) tblEmploymentExposure)

end


--/**SECTION OF CODE TO USE IF YOU DO NOT WANT TO CONSIDER PRODUCTS IN THE EXPOSURE CALCULATION. DANA FOR EXAMPLE**/

--SET @EarliestExposure = (select Min(EmpStartDate) as EarliestExposureDate
--from tblClaimantEmployment
--where ClaimantPersonalInfoID=@ClaimantPersonalInfoID)

RETURN @EarliestExposure

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnEarliestExposure] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnEarliestExposure] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnEarliestExposure] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnEarliestExposure] TO [power_user]
GO
