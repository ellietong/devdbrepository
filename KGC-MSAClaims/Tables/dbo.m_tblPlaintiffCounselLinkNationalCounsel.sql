CREATE TABLE [dbo].[m_tblPlaintiffCounselLinkNationalCounsel]
(
[PlaintiffCounselAddressID] [int] NULL,
[NationalPlaintiffCounselAddressID] [int] NULL,
[LinkingID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblPlaintiffCounselLinkNationalCounsel] ADD CONSTRAINT [PK_m_tblPlaintiffCounselLinkNationalCounsel] PRIMARY KEY CLUSTERED  ([LinkingID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblPlaintiffCounselLinkNationalCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPlaintiffCounselLinkNationalCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPlaintiffCounselLinkNationalCounsel] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblPlaintiffCounselLinkNationalCounsel] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblPlaintiffCounselLinkNationalCounsel] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPlaintiffCounselLinkNationalCounsel] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblPlaintiffCounselLinkNationalCounsel] TO [power_user]
GO
