CREATE TABLE [dbo].[CurrentTenderAssign]
(
[NCC_Reference] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POLICY NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CARRIER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CurrentTenderAssign] TO [base_user]
GRANT SELECT ON  [dbo].[CurrentTenderAssign] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CurrentTenderAssign] TO [power_user]
GRANT SELECT ON  [dbo].[CurrentTenderAssign] TO [power_user]
GO
