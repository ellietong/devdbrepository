CREATE TABLE [dbo].[tblClaimantEmployment]
(
[ClaimantEmploymentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[OccupationID] [int] NULL,
[Jobsite] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmpStartDate] [datetime] NULL,
[EmpEndDate] [datetime] NULL,
[EarliestExposureDate] [datetime] NULL,
[LatestExposureDate] [datetime] NULL,
[IsPrimary] [bit] NULL,
[JobSiteCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobSiteStateID] [int] NULL,
[IsSecondaryExposure] [bit] NULL,
[SPUsername] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EarliestExposureDate_TenderDOFE] [datetime] NULL,
[EarliestExposureDate_MSA] [datetime] NULL,
[EarliestExposureDate_NCC] [datetime] NULL,
[EarliestExposureDate_KIPSDOFU] [datetime] NULL,
[Occupation] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [bigint] NULL,
[Old_OccupationID] [int] NULL,
[EmpSupportingDocID] [int] NULL,
[EmpSupportingPageNums] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_DelEmploymentStart]
   ON [dbo].[tblClaimantEmployment]
   AFTER DELETE
AS 
BEGIN
	--This trigger checks to see if KCIC should be alerted about a change in start date

	DECLARE	@return_value int
	DECLARE @CPIID bigint
	DECLARE @NewEmpID bigint
	DECLARE @CurrentEarliestExposureDate datetime
	DECLARE @NewEarliestExposureDate datetime
	
	DECLARE @NewEmploymentDate datetime
	DECLARE @OldEmploymentDate datetime
	DECLARE @OldEmpID bigint
	
	--DECLARE @Deleted Table (claimantemploymentid int, claimantpersonalinfoid int, empstartdate datetime)
	--insert into @Deleted 
	--select 999999, 42120, '1/1/1985'
	
	SET @OldEmpID = (Select ClaimantEmploymentID from deleted)
	SET @CPIID = (SELECT ClaimantPersonalInfoID FROM deleted)
	SET @OldEmploymentDate = (Select EmpStartDate from deleted)
		
	SET @CurrentEarliestExposureDate = (select coalesce(min(cp.StartDate), min(case when coalesce(ce.claimantemploymentid, d.claimantemploymentID) = @OldEmpID then d.empstartdate else ce.EmpStartDate end))
										from tblClaimantEmployment ce 
										full outer join deleted d on d.claimantemploymentid = ce.claimantemploymentId
										left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
										where coalesce(ce.ClaimantPersonalInfoID, d.claimantpersonalinfoid) = @CPIID)
	SET @NewEarliestExposureDate = (select coalesce(min(cp.StartDate), min(case when ce.claimantemploymentid = @OldEmpID then null else ce.EmpStartDate end))
										from tblClaimantEmployment ce 
										left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
										where ClaimantPersonalInfoID = @CPIID)
	
	select @CurrentEarliestExposureDate, @NewEarliestExposureDate
	
	IF (select dbo.fnGetTenderBlock(1, @CurrentEarliestExposureDate)) <> (select dbo.fnGetTenderBlock(1, @NewEarliestExposureDate)) OR  (select dbo.fnGetTenderBlock(2, @CurrentEarliestExposureDate)) <> (select dbo.fnGetTenderBlock(2, @NewEarliestExposureDate))
	BEGIN
		--Send an email alert about this claimant
		EXEC @return_value = [dbo].[qryInternalDoFEAlert]
			 @ClaimantPersonalInfoID = @CPIID,
			 @Action = 'DEL',
			 @ClaimantEmploymentID = @NewEmpID
	END

END
GO
DISABLE TRIGGER [dbo].[TRIG_DelEmploymentStart] ON [dbo].[tblClaimantEmployment]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_InsEmploymentStart]
   ON [dbo].[tblClaimantEmployment]
   AFTER INSERT
AS 
BEGIN
	--This trigger checks to see if KCIC should be alerted about a change in start date

	DECLARE	@return_value int
	DECLARE @CPIID bigint
	DECLARE @NewEmpID bigint
	SET @CPIID = (SELECT ClaimantPersonalInfoID FROM inserted)
	SET @NewEmpID = (SELECT ClaimantEmploymentID FROM inserted)
	DECLARE @NewEmploymentDate datetime
	SET @NewEmploymentDate = (select empstartdate from inserted)
	DECLARE @CurrentEarliestExposureDate datetime
	DECLARE @NewEarliestExposureDate datetime
	
	SET @CurrentEarliestExposureDate = (select coalesce(min(cp.StartDate), min(case when ce.claimantemploymentid = @NewEmpID then null else ce.EmpStartDate end))
										from tblClaimantEmployment ce 
										left join inserted i on i.claimantemploymentid = ce.claimantemploymentId
										left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
										where ce.ClaimantPersonalInfoID = @CPIID)
	SET @NewEarliestExposureDate = (select coalesce(min(cp.StartDate), min(case when coalesce(ce.claimantemploymentid, i.empstartdate) = @NewEmpID then @NewEmploymentDate else ce.EmpStartDate end))
										from tblClaimantEmployment ce 
										left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
										left join inserted i on i.claimantemploymentid = ce.claimantemploymentId
										where ce.ClaimantPersonalInfoID = @CPIID)
	
	
	SELECT dbo.fnEarliestExposure(@CPIID)
	IF (select dbo.fnGetTenderBlock(1, @CurrentEarliestExposureDate)) <> (select dbo.fnGetTenderBlock(1, @NewEarliestExposureDate)) OR  (select dbo.fnGetTenderBlock(2, @CurrentEarliestExposureDate)) <> (select dbo.fnGetTenderBlock(2, @NewEarliestExposureDate))
	BEGIN
		--Send an email alert about this claimant
		EXEC @return_value = [dbo].[qryInternalDoFEAlert]
			 @ClaimantPersonalInfoID = @CPIID,
			 @Action = 'INS',
			 @ClaimantEmploymentID = @NewEmpID
	END

END
GO
DISABLE TRIGGER [dbo].[TRIG_InsEmploymentStart] ON [dbo].[tblClaimantEmployment]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_UpdEmploymentStart]
   ON [dbo].[tblClaimantEmployment]
   AFTER UPDATE
AS 
BEGIN
	--This trigger checks to see if KCIC should be alerted about a change in start date

	DECLARE	@return_value int
	DECLARE @CPIID bigint
	DECLARE @NewEmpID bigint
	DECLARE @CurrentEarliestExposureDate datetime
	DECLARE @NewEarliestExposureDate datetime
	
	DECLARE @NewEmploymentDate datetime
	DECLARE @OldEmploymentDate datetime
	DECLARE @OldEmpID bigint
	
	SET @OldEmpID = (Select ClaimantEmploymentID from deleted)
	SET @CPIID = (SELECT ClaimantPersonalInfoID FROM inserted)
	SET @NewEmpID = (SELECT ClaimantEmploymentID FROM inserted)
	SET @NewEmploymentDate = (Select EmpStartDate from inserted)
	SET @OldEmploymentDate = (Select EmpStartDate from deleted)
		
	SET @CurrentEarliestExposureDate = (select coalesce(min(cp.StartDate), min(case when ce.claimantemploymentid = @OldEmpID then d.empstartdate else ce.EmpStartDate end))
										from tblClaimantEmployment ce 
										left join deleted d on d.claimantemploymentid = ce.claimantemploymentId
										left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
										where ce.ClaimantPersonalInfoID = @CPIID)
	SET @NewEarliestExposureDate = (select coalesce(min(cp.StartDate), min(case when ce.claimantemploymentid = @OldEmpID then @NewEmploymentDate else ce.EmpStartDate end))
										from tblClaimantEmployment ce 
										left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
										where ClaimantPersonalInfoID = @CPIID)

	IF (select dbo.fnGetTenderBlock(1, @CurrentEarliestExposureDate)) <> (select dbo.fnGetTenderBlock(1, @NewEarliestExposureDate)) OR  (select dbo.fnGetTenderBlock(2, @CurrentEarliestExposureDate)) <> (select dbo.fnGetTenderBlock(2, @NewEarliestExposureDate))
	BEGIN
		--Send an email alert about this claimant

		EXEC @return_value = [dbo].[qryInternalDoFEAlert]
			 @ClaimantPersonalInfoID = @CPIID,
			 @Action = 'UPD',
			 @ClaimantEmploymentID = @NewEmpID
			 
	END

END

GO
DISABLE TRIGGER [dbo].[TRIG_UpdEmploymentStart] ON [dbo].[tblClaimantEmployment]
GO
ALTER TABLE [dbo].[tblClaimantEmployment] ADD CONSTRAINT [PK__mBtblCla__F25662D42665ABE1] PRIMARY KEY CLUSTERED  ([ClaimantEmploymentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimantEmployment_ClaimantpersonalInfoID] ON [dbo].[tblClaimantEmployment] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantEmployment] WITH NOCHECK ADD CONSTRAINT [FK_tblClaimantEmployment_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantPersonalInfoID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantPersonalInfoID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantPersonalInfoID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=1125
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpEndDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpStartDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=4785
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'LatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'LatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'LatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
