CREATE TABLE [dbo].[tblTenderBlock]
(
[TenderBlockID] [int] NOT NULL IDENTITY(1, 1),
[TenderBlockName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemplateURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WithdrawalURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RetenderURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [bit] NULL,
[TenderBlockName_Reporting] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderBlock] ADD CONSTRAINT [PK_tblTenderBlock] PRIMARY KEY CLUSTERED  ([TenderBlockID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBlock] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderBlock] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBlock] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderBlock] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderBlock] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderBlock] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderBlock] TO [power_user]
GO
