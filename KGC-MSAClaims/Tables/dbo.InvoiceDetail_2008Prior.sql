CREATE TABLE [dbo].[InvoiceDetail_2008Prior]
(
[Law_Firm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claimant_Last_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claimant_First_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCC_Reference] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice_Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantCost] [money] NULL,
[TotalPayment] [money] NULL,
[MSAPayment] [money] NULL,
[MSANetCost] [money] NULL,
[Invoice_End_Date] [datetime] NULL,
[Style255] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceDetail_2008Prior] TO [base_user]
GRANT SELECT ON  [dbo].[InvoiceDetail_2008Prior] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[InvoiceDetail_2008Prior] TO [power_user]
GRANT SELECT ON  [dbo].[InvoiceDetail_2008Prior] TO [power_user]
GO
