CREATE TABLE [dbo].[tblClaimantSettlement]
(
[ClaimantSettlementID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NOT NULL,
[SettlementDate] [datetime] NULL,
[PostingDate] [datetime] NULL,
[SettlementAmount] [money] NULL,
[SettlementAmount_MSAPaid] [money] NULL,
[SettlementDueDate] [datetime] NULL,
[SettlementNotificationDate] [datetime] NULL,
[SettlementsFundsRequestDate] [datetime] NULL,
[PayToCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaidDate] [datetime] NULL,
[SettlementGroupID] [bigint] NULL,
[SettlementNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavResolutionKey] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementAmount_GleasonFull] [money] NULL,
[SettlementAmount_GleasonReceivable] [money] NULL,
[SettlementAmount_NCCIndividual] [money] NULL,
[SettlementAmount_NCCGroup] [money] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CashAmount] [money] NULL,
[AssignmentAmount] [money] NULL,
[DemandAmount] [money] NULL,
[DemandDate] [datetime] NULL,
[OfferAmount] [money] NULL,
[OfferDate] [datetime] NULL,
[SettlementFundsRequested] [money] NULL,
[SettlementFundsRequestDesc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementFundsRequestDate] [datetime] NULL,
[PaymentApprovedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_UpdateSettlementAmount_MSAPaid] ON [dbo].[tblClaimantSettlement]
AFTER INSERT, UPDATE
AS 
SET NOCOUNT ON
IF ( UPDATE(SettlementAmount))
BEGIN	
	UPDATE tblClaimantSettlement
		SET SettlementAmount_MSAPaid = cs.SettlementAmount
		FROM tblClaimantSettlement cs
			JOIN Inserted i ON cs.ClaimantSettlementID = i.ClaimantSettlementID
END
GO
ALTER TABLE [dbo].[tblClaimantSettlement] ADD CONSTRAINT [PK_mBtblClaimantSettlement] PRIMARY KEY CLUSTERED  ([ClaimantSettlementID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSettlement] WITH NOCHECK ADD CONSTRAINT [FK_tblClaimantSettlement_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSettlement] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantSettlement] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantSettlement] TO [power_user]
GO
