CREATE TABLE [dbo].[tblMultiDocType]
(
[MultiDocTypeID] [int] NOT NULL,
[DocumentID] [int] NOT NULL,
[DocumentTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMultiDocType] TO [base_user]
GRANT SELECT ON  [dbo].[tblMultiDocType] TO [base_user]
GRANT SELECT ON  [dbo].[tblMultiDocType] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMultiDocType] TO [power_user]
GRANT SELECT ON  [dbo].[tblMultiDocType] TO [power_user]
GO
