CREATE TABLE [dbo].[tblOccurrence]
(
[OccurrenceID] [bigint] NOT NULL IDENTITY(1, 1),
[AggregateApplies] [bit] NOT NULL CONSTRAINT [DF_tblOccurrence_AggregateApplies] DEFAULT ((0)),
[ExpectedPayDate] [datetime] NULL,
[InClaim] [bit] NOT NULL CONSTRAINT [DF_tblOccurrence_InClaim] DEFAULT ((0)),
[OccurrenceName] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceAddress] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceStateID] [int] NULL,
[OccurrenceGroupID] [int] NULL,
[CorporateHistoryID] [int] NULL,
[OccurrenceTypeID] [int] NULL,
[ClaimID] [bigint] NULL,
[Segment] [int] NULL,
[TriggerStart1] [datetime] NULL,
[TriggerStart1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriggerEnd1] [datetime] NULL,
[TriggerEnd1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriggerStart2] [datetime] NULL,
[TriggerEnd2] [datetime] NULL,
[TriggerStart3] [datetime] NULL,
[TriggerEnd3] [datetime] NULL,
[TriggerStart4] [datetime] NULL,
[TriggerEnd4] [datetime] NULL,
[Cost1] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost1] DEFAULT ((0)),
[Cost1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost2] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost2] DEFAULT ((0)),
[Cost2Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost3] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost3] DEFAULT ((0)),
[Cost3Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost4] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost4] DEFAULT ((0)),
[Cost5] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost5] DEFAULT ((0)),
[Cost6] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost6] DEFAULT ((0)),
[Cost7] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost7] DEFAULT ((0)),
[Cost8] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost8] DEFAULT ((0)),
[Cost9] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost9] DEFAULT ((0)),
[Cost10] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost10] DEFAULT ((0)),
[Cost11] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost11] DEFAULT ((0)),
[Cost12] [money] NULL CONSTRAINT [DF_tblOccurrence_Cost12] DEFAULT ((0)),
[OccurrenceNote] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccurrence] ADD CONSTRAINT [PK_tblOccurrence] PRIMARY KEY CLUSTERED  ([OccurrenceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrence] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrence] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrence] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[tblOccurrence] TO [Occurrence_User]
GRANT DELETE ON  [dbo].[tblOccurrence] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[tblOccurrence] TO [Occurrence_User]
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrence] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccurrence] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccurrence] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccurrence] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence', 'COLUMN', N'OccurrenceID'
GO
