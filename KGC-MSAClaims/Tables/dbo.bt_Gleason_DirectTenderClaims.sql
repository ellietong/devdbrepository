CREATE TABLE [dbo].[bt_Gleason_DirectTenderClaims]
(
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[bt_Gleason_DirectTenderClaims] TO [base_user]
GRANT CONTROL ON  [dbo].[bt_Gleason_DirectTenderClaims] TO [power_user]
GRANT SELECT ON  [dbo].[bt_Gleason_DirectTenderClaims] TO [power_user]
GRANT INSERT ON  [dbo].[bt_Gleason_DirectTenderClaims] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_Gleason_DirectTenderClaims] TO [power_user]
GO
