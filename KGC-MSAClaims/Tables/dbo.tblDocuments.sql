CREATE TABLE [dbo].[tblDocuments]
(
[DocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[DetailDocumentTypeID] [int] NULL,
[LawsuitID] [bigint] NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[Comments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTitle] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPDocId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NULL,
[SettlementGroupID] [bigint] NULL,
[SettlementDate] [datetime] NULL,
[SettlementAmount] [money] NULL,
[InvoiceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatePayable] [datetime] NULL,
[FeeAmount] [money] NULL,
[DateOfFiling] [datetime] NULL,
[DateOfDeath] [datetime] NULL,
[ModifiedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateModified] [datetime] NULL,
[Accepted] [bit] NULL,
[AcceptedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAccepted] [datetime] NULL,
[DisbursementAmount] [money] NULL,
[Adjustment] [money] NULL,
[AdjustmentNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusID] [int] NULL,
[ApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateApproved] [datetime] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[FirmID] [int] NULL,
[DocumentSourceID] [int] NULL,
[VendorFirmID] [int] NULL CONSTRAINT [DF_tblDocuments_VendorFirmID] DEFAULT ((0)),
[ClaimLawsuitStatusID] [int] NULL,
[DismissalDate] [date] NULL,
[DismissalProcessDate] [date] NULL,
[MatterID] [int] NULL,
[HistoricalDocumentID] [bigint] NULL,
[RS_BegNo] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_EndNo] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_Privileged] [bit] NULL,
[RS_NCCNumber] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_Claimant] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_MultiClaimants] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_CaseName] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_Category] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_FileName] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_Source] [int] NULL,
[rs_HistTenderLtr] [bit] NULL,
[ComplaintID] [bigint] NULL,
[AmazonKey] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DismissalForQC] [bit] NULL,
[DismissalQCIssue] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_tblDocuments_Upd_Type]
   ON [dbo].[tblDocuments]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--only run this trigger if document type is changed (and it was created by KCIC)
	IF((SELECT DetailDocumentTypeID FROM inserted) <> (SELECT DetailDocumentTypeID FROM deleted)) AND (SELECT RIGHT(ModifiedBy, 11) FROM inserted) = 'kcicllc.com'
	BEGIN
		
		--if it is changed TO complaint, then insert TenderHistory record(s)
		IF((SELECT DetailDocumentTypeID FROM inserted) = 8)
		BEGIN
			DECLARE @ComplaintDocumentID bigint = (SELECT DocumentID FROM inserted)
			DECLARE @Emailaddress nvarchar(MAX) = (SELECT ModifiedBy FROM inserted)
			EXEC WSS_TENDER_qryCreateTenders @ComplaintDocumentID, @emailaddress
		END
		
		--if it is changed FROM complaint to something else, delete tender history, CTH, and tender ack records.
		ELSE IF((SELECT DetailDocumentTypeID FROM deleted) = 8)  AND (SELECT RIGHT(ModifiedBy, 11) FROM inserted) = 'kcicllc.com'
		BEGIN
			--Delete ClaimTenderHistory records
			DELETE FROM tblClaimTenderHistory
			WHERE TenderHistoryID IN (SELECT TenderHistoryID FROM tblTenderHistory WHERE ComplaintDocumentID = (SELECT DocumentID FROM deleted))
			
			--Delete tender acks
			DELETE FROM tblTenderAcknowledgement
			WHERE TenderHistoryID IN (SELECT TenderHistoryID FROM tblTenderHistory WHERE ComplaintDocumentID = (SELECT DocumentID FROM deleted))
			
			--tender tender history record
			DELETE FROM tblTenderHistory 
			WHERE ComplaintDocumentID = (SELECT DocumentID FROM deleted)
		
			
			
		END
	
	
	END

END
GO
ALTER TABLE [dbo].[tblDocuments] ADD CONSTRAINT [PK_mBtblDocuments] PRIMARY KEY CLUSTERED  ([DocumentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[tblDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DetailDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DetailDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DetailDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'SettlementGroupID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'SettlementGroupID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'SettlementGroupID'
GO
