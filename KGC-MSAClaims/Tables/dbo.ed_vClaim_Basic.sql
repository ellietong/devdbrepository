CREATE TABLE [dbo].[ed_vClaim_Basic]
(
[PolicyID] [int] NULL,
[ClaimID] [int] NULL,
[NCCReferenceID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateLawsuit] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndemnityTendered] [money] NULL,
[DefenseTendered] [money] NULL,
[TotalTendered] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ed_vClaim_Basic] TO [base_user]
GRANT ALTER ON  [dbo].[ed_vClaim_Basic] TO [power_user]
GRANT SELECT ON  [dbo].[ed_vClaim_Basic] TO [power_user]
GRANT INSERT ON  [dbo].[ed_vClaim_Basic] TO [power_user]
GRANT DELETE ON  [dbo].[ed_vClaim_Basic] TO [power_user]
GRANT UPDATE ON  [dbo].[ed_vClaim_Basic] TO [power_user]
GO
