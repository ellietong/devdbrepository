CREATE TABLE [dbo].[tblPolicyLimitType]
(
[LimitTypeID] [int] NOT NULL IDENTITY(1, 1),
[LimitType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyLimitType] ADD CONSTRAINT [PK_tblPolicyLimitType] PRIMARY KEY CLUSTERED  ([LimitTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyLimitType] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyLimitType] TO [base_user]
GRANT ALTER ON  [dbo].[tblPolicyLimitType] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblPolicyLimitType] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblPolicyLimitType] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblPolicyLimitType] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblPolicyLimitType] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyLimitType] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicyLimitType] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicyLimitType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicyLimitType] TO [power_user]
GO
