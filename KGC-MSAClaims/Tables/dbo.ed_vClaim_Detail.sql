CREATE TABLE [dbo].[ed_vClaim_Detail]
(
[ClaimID] [int] NULL,
[NCCReferenceID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gender] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaritalStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateAddress] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialSecurityNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [datetime] NULL,
[DeceasedDate] [datetime] NULL,
[CaseCaption] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffFirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffLastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateLawsuit] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocketNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalPlaintiffCounsel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NationalPlaintiffCounsel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounsel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimStatusGroup] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimStatusDetail] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusDate] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceDate] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EarliestExposureDate] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICD9] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighLevelDisease] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiseaseDiagnosisDate] [datetime] NULL,
[DoctorFirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoctorLastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease3] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease4] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease5] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease6] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsMDL] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaidDate] [datetime] NULL,
[CheckAmount] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrialDate] [datetime] NULL,
[IndemnityTendered] [money] NULL,
[DefenseTendered] [money] NULL,
[TotalTendered] [money] NULL,
[FilePathExt] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ed_vClaim_Detail] TO [base_user]
GRANT ALTER ON  [dbo].[ed_vClaim_Detail] TO [power_user]
GRANT SELECT ON  [dbo].[ed_vClaim_Detail] TO [power_user]
GRANT INSERT ON  [dbo].[ed_vClaim_Detail] TO [power_user]
GRANT DELETE ON  [dbo].[ed_vClaim_Detail] TO [power_user]
GRANT UPDATE ON  [dbo].[ed_vClaim_Detail] TO [power_user]
GO
