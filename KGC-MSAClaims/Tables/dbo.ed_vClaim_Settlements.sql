CREATE TABLE [dbo].[ed_vClaim_Settlements]
(
[ClaimID] [int] NULL,
[Carrier] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndemnityTendered] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ed_vClaim_Settlements] TO [base_user]
GRANT ALTER ON  [dbo].[ed_vClaim_Settlements] TO [power_user]
GRANT SELECT ON  [dbo].[ed_vClaim_Settlements] TO [power_user]
GRANT INSERT ON  [dbo].[ed_vClaim_Settlements] TO [power_user]
GRANT DELETE ON  [dbo].[ed_vClaim_Settlements] TO [power_user]
GRANT UPDATE ON  [dbo].[ed_vClaim_Settlements] TO [power_user]
GO
