CREATE TABLE [dbo].[bt_DefenseCounselLookup_FINAL]
(
[LawFirm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselFinal] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[bt_DefenseCounselLookup_FINAL] TO [base_user]
GRANT SELECT ON  [dbo].[bt_DefenseCounselLookup_FINAL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[bt_DefenseCounselLookup_FINAL] TO [power_user]
GRANT SELECT ON  [dbo].[bt_DefenseCounselLookup_FINAL] TO [power_user]
GRANT INSERT ON  [dbo].[bt_DefenseCounselLookup_FINAL] TO [power_user]
GRANT DELETE ON  [dbo].[bt_DefenseCounselLookup_FINAL] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_DefenseCounselLookup_FINAL] TO [power_user]
GO
