CREATE TABLE [dbo].[ed_Pre2008_PolicyLookup]
(
[PolicyNumber_GleasonPre2008] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyDateID_KCIC] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ed_Pre2008_PolicyLookup] TO [base_user]
GRANT SELECT ON  [dbo].[ed_Pre2008_PolicyLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ed_Pre2008_PolicyLookup] TO [power_user]
GRANT SELECT ON  [dbo].[ed_Pre2008_PolicyLookup] TO [power_user]
GRANT INSERT ON  [dbo].[ed_Pre2008_PolicyLookup] TO [power_user]
GRANT DELETE ON  [dbo].[ed_Pre2008_PolicyLookup] TO [power_user]
GRANT UPDATE ON  [dbo].[ed_Pre2008_PolicyLookup] TO [power_user]
GO
