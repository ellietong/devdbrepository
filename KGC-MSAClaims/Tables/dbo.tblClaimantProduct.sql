CREATE TABLE [dbo].[tblClaimantProduct]
(
[ClaimantProductID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantEmploymentID] [bigint] NULL,
[ProductID] [int] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product_NCC] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupportingDocID] [int] NULL,
[SupportingPageNums] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_DelProductStart]
   ON [dbo].[tblClaimantProduct]
   AFTER DELETE
AS 
BEGIN
	--This trigger checks to see if KCIC should be alerted about a change in start date

	DECLARE	@return_value int
	DECLARE @CPIID bigint
	DECLARE @NewProdID bigint
	SET @CPIID = (SELECT ClaimantPersonalInfoID FROM deleted d INNER JOIN tblClaimantEmployment ce ON d.ClaimantEmploymentID = ce.ClaimantEmploymentID)
	SET @NewProdID = (SELECT ClaimantProductID FROM deleted)
	
	SELECT dbo.fnEarliestExposure(@CPIID)
	IF(SELECT StartDate FROM deleted) IS NOT NULL AND (SELECT Startdate FROM deleted) < (SELECT MIN(StartDate) FROM tblClaimantProduct cp INNER JOIN tblClaimantEmployment ce ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID WHERE ClaimantPersonalInfoID = @CPIID)
	BEGIN
		--Send an email alert about this claimant
		EXEC @return_value = [dbo].[qryInternalDoFEAlert]
			 @ClaimantPersonalInfoID = @CPIID,
			 @Action = 'DEL',
			 @ClaimantProductID = @NewProdID
	END

END
GO
DISABLE TRIGGER [dbo].[TRIG_DelProductStart] ON [dbo].[tblClaimantProduct]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_InsProductStart]
   ON [dbo].[tblClaimantProduct]
   AFTER INSERT
AS 
BEGIN
	--This trigger checks to see if KCIC should be alerted about a change in start date

	DECLARE	@return_value int
	DECLARE @CPIID bigint
	DECLARE @NewProdID bigint
	SET @CPIID = (SELECT ClaimantPersonalInfoID FROM inserted i INNER JOIN tblClaimantEmployment ce ON i.ClaimantEmploymentID = ce.ClaimantEmploymentID)
	SET @NewProdID = (SELECT ClaimantProductID FROM inserted)
	SELECT dbo.fnEarliestExposure(@CPIID)
	
	IF(SELECT StartDate FROM inserted) IS NOT NULL AND (SELECT Startdate FROM inserted) < (SELECT COALESCE(MIN(StartDate), '1/1/2100') FROM tblClaimantProduct cp INNER JOIN  tblClaimantEmployment ce ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID WHERE ClaimantPersonalInfoID = @CPIID AND ClaimantProductID <> @NewProdID)
	BEGIN
		--Send an email alert about this claimant
		EXEC @return_value = [dbo].[qryInternalDoFEAlert]
			 @ClaimantPersonalInfoID = @CPIID,
			 @Action = 'INS',
			 @ClaimantProductID = @NewProdID
	END

END
GO
DISABLE TRIGGER [dbo].[TRIG_InsProductStart] ON [dbo].[tblClaimantProduct]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_UpdProductStart]
   ON [dbo].[tblClaimantProduct]
   AFTER UPDATE
AS 
BEGIN
	--This trigger checks to see if KCIC should be alerted about a change in start date

	DECLARE	@return_value int
	DECLARE @CPIID bigint
	DECLARE @NewEmpID bigint
	SET @CPIID = (SELECT ClaimantPersonalInfoID FROM inserted i INNER JOIN tblClaimantEmployment ce ON i.ClaimantEmploymentID = ce.ClaimantEmploymentID)
	SET @NewEmpID = (SELECT ClaimantEmploymentID FROM inserted)
	
	SELECT dbo.fnEarliestExposure(@CPIID)
	IF(SELECT StartDate FROM inserted) IS NOT NULL AND (SELECT Startdate FROM inserted) < (SELECT StartDate FROM deleted)
	BEGIN
		--Send an email alert about this claimant
		EXEC @return_value = [dbo].[qryInternalDoFEAlert]
			 @ClaimantPersonalInfoID = @CPIID,
			 @Action = 'UPD',
			 @ClaimantProductID = @NewEmpID
			 
	END
	SELECT * FROM inserted

END
GO
DISABLE TRIGGER [dbo].[TRIG_UpdProductStart] ON [dbo].[tblClaimantProduct]
GO
ALTER TABLE [dbo].[tblClaimantProduct] ADD CONSTRAINT [PK__mBtblCla__3B9049C42E06CDA9] PRIMARY KEY CLUSTERED  ([ClaimantProductID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantProduct] ADD CONSTRAINT [FK_tblClaimantProduct_tblClaimantProduct] FOREIGN KEY ([ClaimantEmploymentID]) REFERENCES [dbo].[tblClaimantEmployment] ([ClaimantEmploymentID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantProduct] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantProduct] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantProduct] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'ClaimantProductID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'ClaimantProductID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'ClaimantProductID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'EndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'EndDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'EndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'StartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'StartDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'StartDate'
GO
