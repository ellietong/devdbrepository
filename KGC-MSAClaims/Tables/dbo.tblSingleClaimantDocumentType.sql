CREATE TABLE [dbo].[tblSingleClaimantDocumentType]
(
[ClaimantDocumentTypeID] [int] NULL,
[SingleClaimantDocumentTypeID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSingleClaimantDocumentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSingleClaimantDocumentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSingleClaimantDocumentType] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSingleClaimantDocumentType] TO [power_user]
GRANT SELECT ON  [dbo].[tblSingleClaimantDocumentType] TO [power_user]
GO
