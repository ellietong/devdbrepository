CREATE TABLE [dbo].[bt_Pre2008DefenseImport]
(
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceStartDate] [datetime] NULL,
[InvoiceEndDate] [datetime] NULL,
[TotalCosts] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[bt_Pre2008DefenseImport] TO [base_user]
GRANT SELECT ON  [dbo].[bt_Pre2008DefenseImport] TO [base_user]
GRANT SELECT ON  [dbo].[bt_Pre2008DefenseImport] TO [power_user]
GRANT INSERT ON  [dbo].[bt_Pre2008DefenseImport] TO [power_user]
GRANT DELETE ON  [dbo].[bt_Pre2008DefenseImport] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_Pre2008DefenseImport] TO [power_user]
GO
