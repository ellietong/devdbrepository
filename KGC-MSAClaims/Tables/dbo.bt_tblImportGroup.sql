CREATE TABLE [dbo].[bt_tblImportGroup]
(
[ImportGroupID] [int] NOT NULL IDENTITY(1, 1),
[ImportGroup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[bt_tblImportGroup] TO [base_user]
GRANT SELECT ON  [dbo].[bt_tblImportGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[bt_tblImportGroup] TO [power_user]
GRANT SELECT ON  [dbo].[bt_tblImportGroup] TO [power_user]
GRANT INSERT ON  [dbo].[bt_tblImportGroup] TO [power_user]
GRANT DELETE ON  [dbo].[bt_tblImportGroup] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_tblImportGroup] TO [power_user]
GO
