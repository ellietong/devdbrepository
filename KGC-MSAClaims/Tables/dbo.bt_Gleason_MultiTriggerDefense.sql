CREATE TABLE [dbo].[bt_Gleason_MultiTriggerDefense]
(
[BillingCycle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fees] [money] NULL,
[Expenses] [money] NULL,
[TotalCosts] [money] NULL,
[InvoiceStartDate] [datetime] NULL,
[InvoiceEndDate] [datetime] NULL,
[BatchNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentDocNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullDOFE] [datetime] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[bt_Gleason_MultiTriggerDefense] TO [base_user]
GRANT ALTER ON  [dbo].[bt_Gleason_MultiTriggerDefense] TO [power_user]
GRANT SELECT ON  [dbo].[bt_Gleason_MultiTriggerDefense] TO [power_user]
GRANT INSERT ON  [dbo].[bt_Gleason_MultiTriggerDefense] TO [power_user]
GRANT DELETE ON  [dbo].[bt_Gleason_MultiTriggerDefense] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_Gleason_MultiTriggerDefense] TO [power_user]
GO
