CREATE TABLE [dbo].[m_tblPre1985Claims_Q1_2014_CNAReport]
(
[ClaimID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblPre1985Claims_Q1_2014_CNAReport] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPre1985Claims_Q1_2014_CNAReport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPre1985Claims_Q1_2014_CNAReport] TO [fowlerh]
GRANT SELECT ON  [dbo].[m_tblPre1985Claims_Q1_2014_CNAReport] TO [fowlerh]
GRANT INSERT ON  [dbo].[m_tblPre1985Claims_Q1_2014_CNAReport] TO [fowlerh]
GRANT DELETE ON  [dbo].[m_tblPre1985Claims_Q1_2014_CNAReport] TO [fowlerh]
GRANT UPDATE ON  [dbo].[m_tblPre1985Claims_Q1_2014_CNAReport] TO [fowlerh]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPre1985Claims_Q1_2014_CNAReport] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblPre1985Claims_Q1_2014_CNAReport] TO [power_user]
GO
