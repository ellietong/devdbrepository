CREATE TABLE [dbo].[tblClaimantRelatedParty]
(
[ClaimantRelatedPartyID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RelationshipID] [int] NULL,
[LegalCapacity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LegalCapacityID] [int] NULL,
[RPAddress1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPAddress2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPCity] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[RPZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPMailZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPExtension] [int] NULL,
[RPSocialSecurityNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPTIN] [int] NULL,
[SPUserName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantRelatedParty] ADD CONSTRAINT [PK__tblClaimantRelat__48EFCE0F] PRIMARY KEY CLUSTERED  ([ClaimantRelatedPartyID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantRelatedParty] ADD CONSTRAINT [FK_tblClaimantRelatedParty_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantRelatedParty] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantRelatedParty] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantRelatedParty] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantRelatedParty] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantRelatedParty] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantRelatedParty] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantRelatedParty] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantRelatedParty] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantRelatedParty] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantRelatedParty] TO [power_user]
GO
