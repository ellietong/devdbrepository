CREATE TABLE [dbo].[tblDocumentSource_OLD]
(
[DocumentSourceID] [int] NOT NULL IDENTITY(1, 1),
[DocumentSource] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDocumentSource_OLD] ADD CONSTRAINT [PK_tblDocumentSource] PRIMARY KEY CLUSTERED  ([DocumentSourceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDocumentSource_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocumentSource_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDocumentSource_OLD] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDocumentSource_OLD] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDocumentSource_OLD] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDocumentSource_OLD] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDocumentSource_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblDocumentSource_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblDocumentSource_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDocumentSource_OLD] TO [power_user]
GO
