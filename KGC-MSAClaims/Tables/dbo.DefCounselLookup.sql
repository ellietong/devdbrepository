CREATE TABLE [dbo].[DefCounselLookup]
(
[Law_Firm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[DefCounselLookup] TO [base_user]
GRANT SELECT ON  [dbo].[DefCounselLookup] TO [base_user]
GRANT SELECT ON  [dbo].[DefCounselLookup] TO [Claims_User]
GRANT INSERT ON  [dbo].[DefCounselLookup] TO [Claims_User]
GRANT UPDATE ON  [dbo].[DefCounselLookup] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[DefCounselLookup] TO [power_user]
GRANT SELECT ON  [dbo].[DefCounselLookup] TO [power_user]
GO
