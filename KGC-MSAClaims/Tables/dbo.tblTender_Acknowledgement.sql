CREATE TABLE [dbo].[tblTender_Acknowledgement]
(
[TenderAcknowledgementID] [bigint] NOT NULL IDENTITY(1, 1),
[TenderHistoryID] [bigint] NULL,
[PersonID] [int] NULL,
[AcknowledgementDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTender_Acknowledgement] ADD CONSTRAINT [PK_tblTender_Acknowledgement] PRIMARY KEY CLUSTERED  ([TenderAcknowledgementID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTender_Acknowledgement] TO [base_user]
GRANT SELECT ON  [dbo].[tblTender_Acknowledgement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTender_Acknowledgement] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblTender_Acknowledgement] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblTender_Acknowledgement] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblTender_Acknowledgement] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblTender_Acknowledgement] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTender_Acknowledgement] TO [power_user]
GRANT SELECT ON  [dbo].[tblTender_Acknowledgement] TO [power_user]
GRANT INSERT ON  [dbo].[tblTender_Acknowledgement] TO [power_user]
GRANT DELETE ON  [dbo].[tblTender_Acknowledgement] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTender_Acknowledgement] TO [power_user]
GO
