CREATE TABLE [dbo].[ed_vPolicy_Basic]
(
[PolicyID] [int] NULL,
[CarrierGroupName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyNum] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PStartDate] [datetime] NULL,
[AttachmentPoint] [money] NULL,
[CarrierNameShort] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndemnityTendered] [money] NULL,
[DefenseTendered] [money] NULL,
[TotalTendered] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ed_vPolicy_Basic] TO [base_user]
GRANT ALTER ON  [dbo].[ed_vPolicy_Basic] TO [power_user]
GRANT SELECT ON  [dbo].[ed_vPolicy_Basic] TO [power_user]
GRANT INSERT ON  [dbo].[ed_vPolicy_Basic] TO [power_user]
GRANT DELETE ON  [dbo].[ed_vPolicy_Basic] TO [power_user]
GRANT UPDATE ON  [dbo].[ed_vPolicy_Basic] TO [power_user]
GO
