CREATE TABLE [dbo].[tblDefenseInvoice_Pre2008]
(
[DefenseInvoiceID] [bigint] NOT NULL IDENTITY(1, 1),
[StatusID] [int] NULL,
[FirmID] [bigint] NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeAmount] [money] NULL,
[DisbursementAmount] [money] NULL,
[Adjustment] [money] NULL,
[InvoiceTotal] [money] NULL,
[SubmitDate] [datetime] NULL,
[DatePayable] [datetime] NULL,
[CheckNumber] [bigint] NULL,
[FundsRequestDate] [datetime] NULL,
[DatePaid] [datetime] NULL,
[DocumentID] [bigint] NULL,
[AdjustmentNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceLink] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckLink] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[VendorFirmID] [int] NULL,
[Firm] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodStart] [datetime] NULL,
[PeriodEnd] [datetime] NULL,
[InvoiceTotal_Receivable] [money] NULL,
[Pre2008] [bit] NULL,
[PaymentReference] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseInvoice_Pre2008] ADD CONSTRAINT [PK_tblDefenseInvoice_Pre2008] PRIMARY KEY CLUSTERED  ([DefenseInvoiceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice_Pre2008] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoice_Pre2008] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoice_Pre2008] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice_Pre2008] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoice_Pre2008] TO [power_user]
GO
