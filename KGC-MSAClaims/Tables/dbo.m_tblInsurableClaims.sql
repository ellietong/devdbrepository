CREATE TABLE [dbo].[m_tblInsurableClaims]
(
[ClaimID] [int] NULL,
[InsurableFlag] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblInsurableClaims] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblInsurableClaims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblInsurableClaims] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblInsurableClaims] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblInsurableClaims] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblInsurableClaims] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblInsurableClaims] TO [power_user]
GO
