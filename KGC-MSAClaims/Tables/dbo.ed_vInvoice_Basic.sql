CREATE TABLE [dbo].[ed_vInvoice_Basic]
(
[PolicyID] [int] NULL,
[InvoiceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSANetCostAmount] [money] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[LawFirm] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePathExt] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [int] NULL,
[InvoiceID] [int] NULL,
[CarrierNameShort] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyNum] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ed_vInvoice_Basic] TO [base_user]
GRANT ALTER ON  [dbo].[ed_vInvoice_Basic] TO [power_user]
GRANT SELECT ON  [dbo].[ed_vInvoice_Basic] TO [power_user]
GRANT INSERT ON  [dbo].[ed_vInvoice_Basic] TO [power_user]
GRANT DELETE ON  [dbo].[ed_vInvoice_Basic] TO [power_user]
GRANT UPDATE ON  [dbo].[ed_vInvoice_Basic] TO [power_user]
GO
