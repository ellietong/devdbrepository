CREATE TABLE [dbo].[InvoiceDetails_2009Future]
(
[Invoice_Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Law Firm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice_Start_Date (Billing Period)] [datetime] NULL,
[Invoice_End_Date (Billing Period)] [datetime] NULL,
[NCC_Reference] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Total_Costs] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceDetails_2009Future] TO [base_user]
GRANT SELECT ON  [dbo].[InvoiceDetails_2009Future] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[InvoiceDetails_2009Future] TO [power_user]
GRANT SELECT ON  [dbo].[InvoiceDetails_2009Future] TO [power_user]
GO
