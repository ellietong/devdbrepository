CREATE TABLE [dbo].[bt_Gleason_TenderByClaim]
(
[TenderType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenderDate] [datetime] NULL,
[2008DefenseTenderAmount] [money] NULL,
[NetSettlementTenderAmount] [money] NULL,
[GrossSettlementAmount] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[bt_Gleason_TenderByClaim] TO [base_user]
GRANT ALTER ON  [dbo].[bt_Gleason_TenderByClaim] TO [power_user]
GRANT SELECT ON  [dbo].[bt_Gleason_TenderByClaim] TO [power_user]
GRANT INSERT ON  [dbo].[bt_Gleason_TenderByClaim] TO [power_user]
GRANT DELETE ON  [dbo].[bt_Gleason_TenderByClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_Gleason_TenderByClaim] TO [power_user]
GO
