CREATE TABLE [dbo].[bt_Gleason_RS_Invoice2499908_Allocation]
(
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fees] [money] NULL,
[Expenses] [money] NULL,
[TotalCosts] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[bt_Gleason_RS_Invoice2499908_Allocation] TO [base_user]
GRANT ALTER ON  [dbo].[bt_Gleason_RS_Invoice2499908_Allocation] TO [power_user]
GRANT SELECT ON  [dbo].[bt_Gleason_RS_Invoice2499908_Allocation] TO [power_user]
GRANT INSERT ON  [dbo].[bt_Gleason_RS_Invoice2499908_Allocation] TO [power_user]
GRANT DELETE ON  [dbo].[bt_Gleason_RS_Invoice2499908_Allocation] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_Gleason_RS_Invoice2499908_Allocation] TO [power_user]
GO
