CREATE TABLE [dbo].[Midland_Submission_ClaimsData_20130114]
(
[NCC Reference ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Middle Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Birth Date] [datetime] NULL,
[Deceased Date] [datetime] NULL,
[Lead Plaintiff First Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Lead Plaintiff Last Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Docket Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claim Status Group] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claim Status Detail] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status Date] [datetime] NULL,
[File Date] [datetime] NULL,
[Service Date] [datetime] NULL,
[DOFE] [datetime] NULL,
[Product Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claim Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease 1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease 2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease 3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease 4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease 5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease 6] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSA Indemnity Paid Date] [money] NULL,
[Indemnity] [money] NULL,
[MSA Indemnity] [money] NULL,
[MSA Defense] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Midland_Submission_ClaimsData_20130114] TO [base_user]
GRANT SELECT ON  [dbo].[Midland_Submission_ClaimsData_20130114] TO [base_user]
GRANT SELECT ON  [dbo].[Midland_Submission_ClaimsData_20130114] TO [Claims_User]
GRANT INSERT ON  [dbo].[Midland_Submission_ClaimsData_20130114] TO [Claims_User]
GRANT UPDATE ON  [dbo].[Midland_Submission_ClaimsData_20130114] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[Midland_Submission_ClaimsData_20130114] TO [power_user]
GRANT SELECT ON  [dbo].[Midland_Submission_ClaimsData_20130114] TO [power_user]
GO
