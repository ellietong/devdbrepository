CREATE TABLE [dbo].[ImportDocs_State]
(
[BEGNO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantPersonalInfoID] [float] NULL,
[Links] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ImportDocs_State] TO [base_user]
GRANT SELECT ON  [dbo].[ImportDocs_State] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ImportDocs_State] TO [power_user]
GRANT SELECT ON  [dbo].[ImportDocs_State] TO [power_user]
GO
