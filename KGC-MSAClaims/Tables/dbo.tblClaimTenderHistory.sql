CREATE TABLE [dbo].[tblClaimTenderHistory]
(
[ClaimTenderHistoryID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID] [bigint] NOT NULL,
[TenderHistoryID] [bigint] NOT NULL,
[TenderStatusID] [int] NOT NULL,
[PreviousClaimTenderHistoryID] [bigint] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimTenderHistory] ADD CONSTRAINT [PK_tblClaimTenderHistory] PRIMARY KEY CLUSTERED  ([ClaimTenderHistoryID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimTenderHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimTenderHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimTenderHistory] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimTenderHistory] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimTenderHistory] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimTenderHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimTenderHistory] TO [power_user]
GO
