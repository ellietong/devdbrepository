CREATE TABLE [dbo].[tblTenderHistory]
(
[TenderHistoryID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID_old] [bigint] NULL,
[TenderBlockID] [int] NULL,
[TenderStatusID_old] [int] NULL,
[TenderTypeID] [int] NULL,
[EffectiveDate] [datetime] NULL,
[DocumentID] [bigint] NULL,
[AdditionalText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComplaintDocumentID] [bigint] NULL,
[DateApproved] [datetime] NULL,
[ApprovedBy] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NULL,
[Retendered] [bit] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenderTemplateID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_InsertAckRecords]
   ON [dbo].[tblTenderHistory]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO tblTenderAcknowledgement (TenderHistoryID, TenderContactID, Hash, Acknowledged)
	
	SELECT DISTINCT inserted.TenderHistoryID, tc.TenderContactID, CONVERT(char(32), HASHBYTES('MD5', CONVERT(nvarchar, inserted.TenderHistoryID) + COALESCE(Email, '')), 2), 0 FROM inserted 
	INNER JOIN tblTenderBlock tb ON inserted.TenderBlockID =tb.TenderBlockID
	INNER JOIN tblTenderBlockPolicy tbp  ON tb.TenderBlockID = tbp.TenderBlockID
	INNER JOIN tblPolicy p ON tbp.PolicyID = p.POlicyID
	INNER JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
	WHERE COALESCE(tc.[To] , 0) = 1 AND COALESCE(CurrentContact, 0) = 1
    -- Insert statements for trigger here

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_TenderStatusChanged]
   ON  [dbo].[tblTenderHistory]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Temporarily disable trigger
	--DISABLE TRIGGER dbo.TRIG_TenderStatusChanged ON dbo.tblTenderHistory;
	
	--only insert withdrawal & re-tender records if the tender status has changed to "Withdrawn"
	IF ((SELECT TenderStatusID FROM deleted) <> (SELECT TenderStatusID FROM inserted)) AND (SELECT TenderStatusID FROM inserted) = 3
	BEGIN
		DECLARE @CPIID bigint = (SELECT TOP 1 ClaimantPersonalInfOID FROM tblClaim WHERE ClaimID IN (SELECT ClaimID FROM inserted))
		DECLARE @DocID bigint = (SELECT TOP 1 ComplaintDocumentID FROM inserted)
		DECLARE @MaxServiceClaims TABLE (ClaimID bigint, ServiceDate datetime)
		INSERT INTO @MaxServiceClaims
		SELECT cl.ClaimID, MAX(COALESCE(ServiceDate, FileDate)) as mx FROM tblLawsuit l 
				LEFT JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
				INNER JOIN tblClaim c ON c.ClaimID = cl.ClaimID
				WHERE COALESCE(IsPrimaryForClaim, 0) = 1 AND COALESCE(IsCurrentClaim, 0) = 1
				AND c.ClaimantPersonalInfoID = @CPIID AND COALESCE(ServiceDate, FileDate) IS NOT NULL
				GROUP BY cl.ClaimID
				ORDER BY ClaimID
				
		DECLARE @MaterialID bigint = (SELECT TOP 1 MaterialID FROM inserted i 
										LEFT JOIN tblClaim c ON i.ClaimID = c.ClaimID
										LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID 
										LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
										INNER JOIN @MaxServiceClaims msc ON msc.ClaimID = c.ClaimID
										ORDER BY ServiceDate DESC)

		--If re-tending a historic claim without a documentID or with the dummy record, reset to newest complaint
		IF(@DocID IS NULL  OR @DocID = 9999999)
		BEGIN
			SET @DocID = (SELECT TOP 1 mcd.DocumentID FROM tblMultiClaimantDocument mcd 
				INNER JOIN tblClaim c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
				INNER JOIN tblDocuments d ON mcd.DocumentID = d.DocumentID
				INNER JOIN @MaxServiceClaims msc ON msc.ClaimID = c.ClaimID
				WHERE DetailDocumentTypeID = 8
				ORDER BY DateCreated DESC)
		END
		
		--If there aren't any complaints, set DocID back to 9999999
		IF(@DocID IS NULL)
		BEGIN
			SET @DocID = 9999999
		END
					
		DECLARE @EarliestExposure date
		SET @EarliestExposure = (SELECT dbo.fnEarliestExposure(@CPIID))
		
		--Get potential list of tender blocks for given exposure date and material
		DECLARE @TenderBlockID int
		DECLARE @TenderBlocks TABLE (TenderBlockID int)
		INSERT INTO @TenderBlocks
		EXEC WSS_qryGetTenderBlocks @EarliestExposure, @MaterialID
		
		--If the tender being withdrawn is in the 79 lane (tender block 19 or 20), set the other record to withdrawn as well,
		-- for that specific claimID
		DECLARE @ClaimID int = (SELECT TOP 1 ClaimID FROM deleted) 
		SET @TenderBlockID = (SELECT TOP 1 TenderBlockID FROM inserted)
		IF(SELECT TenderBlockID FROM inserted) IN (19, 20)
		BEGIN
			UPDATE tblTenderHistory
			SET TenderStatusID = 3
			WHERE ClaimID = @ClaimID
			AND TenderStatusID <> 3 
			AND TenderBlockID <> @TenderBlockID
			AND TenderTypeID <> 2
			AND TenderHistoryID NOT IN (SELECT TenderHistoryID FROM inserted)
			AND TenderBlockID IN (19, 20)
		END
		
		
		
		--INSERT Withdrawal Record for existing tender history record
		INSERT INTO tblTenderHistory (ClaimID, TenderBlockID, TenderStatusID, ComplaintDocumentID, TenderTypeID, DateCreated)  
		SELECT i.ClaimID, i.TenderBlockID, 1, ComplaintDocumentID, 2, GETDATE()
		FROM inserted i
		INNER JOIN tblClaim	 c ON i.ClaimID = c.ClaimID
		
		
		--Insert second withdrawal for 79 block
		IF(SELECT TenderBlockID FROM inserted) IN (19, 20)
		BEGIN
			INSERT INTO tblTenderHistory (ClaimID, TenderBlockID, TenderStatusID, ComplaintDocumentID, TenderTypeID, DateCreated)  
			SELECT i.ClaimID, TenderBlockID, 1, ComplaintDocumentID, 2, GETDATE()
			FROM tblTenderHistory i
			INNER JOIN tblClaim	 c ON i.ClaimID = c.ClaimID
			WHERE i.ClaimID = @ClaimID
			AND TenderStatusID = 3 
			AND TenderBlockID <> @TenderBlockID
			AND TenderTypeID <> 2
			AND TenderBlockID IN (19, 20)
			AND TenderHistoryID NOT IN (SELECT TenderHistoryID FROM inserted)
		END
		
		--INSERT Re-Tender Record for new tender block
		--Loop through tender blocks and insert tender history records for each (79 should be the only block with 2 records)
		DECLARE curs_TenderBlocks Cursor FOR 
		SELECT DISTINCT TenderBlockID FROM @TenderBlocks
		
		OPEN curs_TenderBlocks
			FETCH NEXT FROM curs_TenderBlocks INTO @TenderBlockID
			WHILE @@Fetch_Status = 0
			BEGIN
				INSERT INTO tblTenderHistory (ClaimID, TenderBlockID, TenderStatusID, ComplaintDocumentID, TenderTypeID, DateCreated)  
				SELECT msc.ClaimID, @TenderBlockID, 1, @DocID, 3, GETDATE()
				FROM @MaxServiceClaims msc
				INNER JOIN tblClaim	 c ON msc.ClaimID = c.ClaimID
				WHERE ClaimantPersonalInfoID = @CPIID
					
				FETCH NEXT FROM curs_TenderBlocks INTO @TenderBlockID
			END

		CLOSE curs_TenderBlocks 
		DEALLOCATE curs_TenderBlocks

		
		
	END;
    
	--ENABLE TRIGGER dbo.TRIG_TenderStatusChanged ON dbo.tblTenderHistory;
END
GO
DISABLE TRIGGER [dbo].[TRIG_TenderStatusChanged] ON [dbo].[tblTenderHistory]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[TRIG_UpdateAckRecords]
   ON [dbo].[tblTenderHistory]
   AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	DELETE FROM tblTenderAcknowledgement
	WHERE TenderHistoryID = (SELECT TenderHistoryID FROM inserted)
		AND AcknowledgementDate IS NULL
	
	INSERT INTO tblTenderAcknowledgement (TenderHistoryID, TenderContactID, Hash, Acknowledged)
	
	SELECT DISTINCT inserted.TenderHistoryID, 
					tc.TenderContactID, 
					CONVERT(char(32), 
					HASHBYTES('MD5', CONVERT(nvarchar, inserted.TenderHistoryID) + COALESCE(Email, '')), 2),
					0 
	FROM inserted 
	INNER JOIN tblTenderBlock tb ON inserted.TenderBlockID =tb.TenderBlockID
	INNER JOIN tblTenderBlockPolicy tbp  ON tb.TenderBlockID = tbp.TenderBlockID
	INNER JOIN tblPolicy p ON tbp.PolicyID = p.POlicyID
	INNER JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
	WHERE COALESCE(tc.[To] , 0) = 1 AND COALESCE(CurrentContact, 0) = 1


END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_UpdateTenderBlock]
   ON  [dbo].[tblTenderHistory]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
    IF(SELECT TenderBlockID FROM inserted) <> (SELECT TEnderBlockID FROM deleted)
    BEGIN
		--Delete old tender ack records (only if they haven't been ack'd)
		DELETE FROM tblTenderAcknowledgement
		WHERE TenderHistoryID = (SELECT TenderHistoryID FROM deleted)
		AND AcknowledgementDate IS NULL
    
		--Re-insert for new TenderBlockID
			INSERT INTO tblTenderAcknowledgement (TenderHistoryID, TenderContactID, Hash, Acknowledged)
	
			SELECT DISTINCT inserted.TenderHistoryID, tc.TenderContactID, CONVERT(char(32), HASHBYTES('MD5', CONVERT(nvarchar, inserted.TenderHistoryID) + COALESCE(Email, '')), 2), 0 FROM inserted 
			INNER JOIN tblTenderBlock tb ON inserted.TenderBlockID =tb.TenderBlockID
			INNER JOIN tblTenderBlockPolicy tbp  ON tb.TenderBlockID = tbp.TenderBlockID
			INNER JOIN tblPolicy p ON tbp.PolicyID = p.POlicyID
			INNER JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
			WHERE COALESCE(tc.[To] , 0) = 1
    
		--For tender block requiring different letters for each carrier:  If switching to that tender block, insert the opposite record
		--than the one that was selected, so the "tender block" is complete with records both insurers
		IF(SELECT TenderBlockID FROM inserted) = 19 AND (SELECT TenderBlockID FROM deleted) NOT IN (19, 20)
		BEGIN
			INSERT INTO tblTenderHistory (ClaimID, TenderBlockID, TenderStatusID, TenderTypeID, EffectiveDate, 
			DocumentID, AdditionalText, ComplaintDocumentID, DateApproved, ApprovedBy, DateCreated, Retendered, SPUsername)
			SELECT ClaimID, 20, TenderStatusID, TenderTypeID, EffectiveDate, 
			DocumentID, AdditionalText, ComplaintDocumentID, DateApproved, ApprovedBy, DateCreated, Retendered, SPUsername 
			FROM inserted
		END
		IF(SELECT TenderBlockID FROM inserted) = 20 AND (SELECT TenderBlockID FROM deleted) NOT IN (19, 20)
		BEGIN
			INSERT INTO tblTenderHistory (ClaimID, TenderBlockID, TenderStatusID, TenderTypeID, EffectiveDate, 
			DocumentID, AdditionalText, ComplaintDocumentID, DateApproved, ApprovedBy, DateCreated, Retendered, SPUsername)
			SELECT ClaimID, 19, TenderStatusID, TenderTypeID, EffectiveDate, 
			DocumentID, AdditionalText, ComplaintDocumentID, DateApproved, ApprovedBy, DateCreated, Retendered, SPUsername 
			FROM inserted
		END
    END

END
GO
DISABLE TRIGGER [dbo].[TRIG_UpdateTenderBlock] ON [dbo].[tblTenderHistory]
GO
ALTER TABLE [dbo].[tblTenderHistory] ADD CONSTRAINT [PK_tblTenderHistory] PRIMARY KEY CLUSTERED  ([TenderHistoryID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblTenderHistory_TenderBlockID_TenderStatusID] ON [dbo].[tblTenderHistory] ([TenderBlockID], [TenderStatusID_old]) INCLUDE ([ClaimID_old]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblTenderHistory_ClaimIDTenderBlockID] ON [dbo].[tblTenderHistory] ([TenderStatusID_old], [TenderTypeID]) INCLUDE ([ClaimID_old], [TenderBlockID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderHistory] WITH NOCHECK ADD CONSTRAINT [FK_tblTenderHistory_tblDocuments] FOREIGN KEY ([ComplaintDocumentID]) REFERENCES [dbo].[tblDocuments] ([DocumentID]) ON DELETE CASCADE NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[tblTenderHistory] NOCHECK CONSTRAINT [FK_tblTenderHistory_tblDocuments]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderHistory] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderHistory] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderHistory] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderHistory] TO [power_user]
GO
