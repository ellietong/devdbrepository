CREATE TABLE [dbo].[ed_vInvoice_Single]
(
[InvoiceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSANetCostAmount] [money] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[LawFirm] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePathExt] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceID] [int] NULL,
[DocumentFilter] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ed_vInvoice_Single] TO [base_user]
GRANT SELECT ON  [dbo].[ed_vInvoice_Single] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ed_vInvoice_Single] TO [power_user]
GRANT SELECT ON  [dbo].[ed_vInvoice_Single] TO [power_user]
GRANT INSERT ON  [dbo].[ed_vInvoice_Single] TO [power_user]
GRANT DELETE ON  [dbo].[ed_vInvoice_Single] TO [power_user]
GRANT UPDATE ON  [dbo].[ed_vInvoice_Single] TO [power_user]
GO
