CREATE TABLE [dbo].[MSAWithdrawal]
(
[NCCReferenceID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[MSAWithdrawal] TO [base_user]
GRANT SELECT ON  [dbo].[MSAWithdrawal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[MSAWithdrawal] TO [power_user]
GRANT SELECT ON  [dbo].[MSAWithdrawal] TO [power_user]
GRANT INSERT ON  [dbo].[MSAWithdrawal] TO [power_user]
GRANT UPDATE ON  [dbo].[MSAWithdrawal] TO [power_user]
GO
