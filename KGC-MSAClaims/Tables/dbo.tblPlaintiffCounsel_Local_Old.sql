CREATE TABLE [dbo].[tblPlaintiffCounsel_Local_Old]
(
[PlaintiffCounselID] [int] NOT NULL IDENTITY(1, 1),
[PlaintiffCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPlaintiffCounsel_Local_Old] ADD CONSTRAINT [PK_tblPlaintiffCounsel] PRIMARY KEY CLUSTERED  ([PlaintiffCounselID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [power_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [power_user]
GRANT INSERT ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [power_user]
GRANT DELETE ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPlaintiffCounsel_Local_Old] TO [power_user]
GO
