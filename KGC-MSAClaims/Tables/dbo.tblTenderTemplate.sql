CREATE TABLE [dbo].[tblTenderTemplate]
(
[TenderTemplateID] [int] NOT NULL IDENTITY(1, 1),
[TemplateDisplayName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenderTypeID] [int] NOT NULL,
[TenderBlockID] [int] NOT NULL,
[MaterialID] [int] NOT NULL,
[TemplateURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncludeExhibit] [bit] NULL,
[IncludeComplaint] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderTemplate] ADD CONSTRAINT [PK_tblTenderTemplate] PRIMARY KEY CLUSTERED  ([TenderTemplateID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderTemplate] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderTemplate] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderTemplate] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblTenderTemplate] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblTenderTemplate] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderTemplate] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderTemplate] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderTemplate] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderTemplate] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderTemplate] TO [power_user]
GO
