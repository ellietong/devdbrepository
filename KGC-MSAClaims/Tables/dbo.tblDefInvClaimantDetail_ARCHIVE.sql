CREATE TABLE [dbo].[tblDefInvClaimantDetail_ARCHIVE]
(
[InvoiceDetailID] [bigint] NULL,
[DefenseInvoiceID] [bigint] NULL,
[ClaimID] [int] NULL,
[FirmID] [int] NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[Fees] [money] NULL,
[Costs] [money] NULL,
[Adjustments] [money] NULL,
[Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefInvClaimantDetail_ARCHIVE] TO [power_user]
GO
