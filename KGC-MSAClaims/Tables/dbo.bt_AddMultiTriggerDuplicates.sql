CREATE TABLE [dbo].[bt_AddMultiTriggerDuplicates]
(
[ImportNote] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportGroupID] [int] NULL,
[ImportGroup_SubID] [int] NULL,
[PolicyDateID] [int] NULL,
[BillingCycle] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCReferenceID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceStartDate] [datetime] NULL,
[InvoiceEndDate] [datetime] NULL,
[Fees] [money] NULL,
[Expenses] [money] NULL,
[TotalCosts] [money] NULL,
[BatchNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentDocNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportGroupID_2] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[bt_AddMultiTriggerDuplicates] TO [base_user]
GRANT SELECT ON  [dbo].[bt_AddMultiTriggerDuplicates] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[bt_AddMultiTriggerDuplicates] TO [power_user]
GRANT SELECT ON  [dbo].[bt_AddMultiTriggerDuplicates] TO [power_user]
GRANT INSERT ON  [dbo].[bt_AddMultiTriggerDuplicates] TO [power_user]
GRANT DELETE ON  [dbo].[bt_AddMultiTriggerDuplicates] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_AddMultiTriggerDuplicates] TO [power_user]
GO
