CREATE TABLE [dbo].[bt_tblImport_Indemnity]
(
[ImportID_Indemnity] [float] NULL,
[PolicyDateID] [float] NULL,
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settlement] [money] NULL,
[Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[bt_tblImport_Indemnity] TO [base_user]
GRANT SELECT ON  [dbo].[bt_tblImport_Indemnity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[bt_tblImport_Indemnity] TO [power_user]
GRANT SELECT ON  [dbo].[bt_tblImport_Indemnity] TO [power_user]
GRANT INSERT ON  [dbo].[bt_tblImport_Indemnity] TO [power_user]
GRANT DELETE ON  [dbo].[bt_tblImport_Indemnity] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_tblImport_Indemnity] TO [power_user]
GO
