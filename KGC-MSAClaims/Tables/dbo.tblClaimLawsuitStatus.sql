CREATE TABLE [dbo].[tblClaimLawsuitStatus]
(
[ClaimLawsuitStatusID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitStatusGroupID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatus] ADD CONSTRAINT [PK_mBtblClaimLawsuitStatus] PRIMARY KEY CLUSTERED  ([ClaimLawsuitStatusID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatus] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatus] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatus] TO [power_user]
GO
