CREATE TABLE [dbo].[tblClaim]
(
[ClaimID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[IsCurrentClaim] [bit] NULL,
[ClaimSequenceID] [int] NULL,
[FullyDocumented] [bit] NULL,
[CauseOfInjuryID] [int] NULL,
[TortTypeID] [int] NOT NULL CONSTRAINT [DF_tblClaim_TortTypeID] DEFAULT ((1)),
[MatterID] [int] NULL,
[CLM_KEY] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILODoctorID] [int] NULL,
[ILOProfusion] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILODate] [datetime] NULL,
[PFTDoctorID] [int] NULL,
[PFTDate] [datetime] NULL,
[PFTTypeID] [int] NULL,
[PFTFVC_Actual] [float] NULL,
[PFTFVC_Predicted] [float] NULL,
[PFTFEV1_Actual] [float] NULL,
[PFTFEV1_Predicted] [float] NULL,
[PFTFEV1AndFVC_Actual] [float] NULL,
[PFTFEV1AndFVC_Predicted] [float] NULL,
[PFTTLC_Actual] [float] NULL,
[PFTTLC_Predicted] [float] NULL,
[PFTDLCO_Actual] [float] NULL,
[PFTDLCO_Predicted] [float] NULL,
[PFTTV] [float] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCReferenceID] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease_MSA] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jobsite_MSA] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product_MSA] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HistoricalNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HasLossofConsortiumClaim] [bit] NULL,
[HasEstateClaim] [bit] NULL,
[InsurableFlag] [bit] NOT NULL CONSTRAINT [DF__tblClaim__Insura__126049E2] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_NCCReference]
   ON  [dbo].[tblClaim]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	/** DO NOT CREATE NCC FOR NON TOXIC TORT **/
	if (select TortTypeID from inserted) <> 2 
	begin 	
	UPDATE tblClaim
	SET NCCReferenceID = (select 'NCC' + right(convert(nvarchar(4), YEAR(getdate())),2) +   LEFT('000000', 6- len(convert(nvarchar(6),(coalesce(max(convert(int, right(NCCReferenceID, 6))),0)+1)))) + convert(nvarchar(6),(coalesce(max(convert(int, right(NCCReferenceID, 6))),0)+1))
							from tblClaim
							where right(LEFT(nccreferenceid,5),2) = right(convert(nvarchar(4), YEAR(getdate())),2))
	WHERE ClaimID IN (SELECT ClaimID FROM inserted)
	end


END
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [PK_mBtblClaim] PRIMARY KEY CLUSTERED  ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaim_ClaimantPersonalInfoID] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID]) INCLUDE ([ClaimID], [IsCurrentClaim], [MatterID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaim_MatterID] ON [dbo].[tblClaim] ([MatterID]) INCLUDE ([ClaimantPersonalInfoID], [ClaimID], [ClaimSequenceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [FK_tblClaim_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaim] TO [base_user]
GRANT DELETE ON  [dbo].[tblClaim] TO [ClaimDeletePermission]
GRANT SELECT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaim] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaim] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaim] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaim] TO [power_user]
GO
