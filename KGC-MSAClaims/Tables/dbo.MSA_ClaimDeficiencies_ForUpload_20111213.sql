CREATE TABLE [dbo].[MSA_ClaimDeficiencies_ForUpload_20111213]
(
[ClaimantPersonalInfoID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCReferenceID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTypeID] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTitle] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 5] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[MSA_ClaimDeficiencies_ForUpload_20111213] TO [base_user]
GRANT SELECT ON  [dbo].[MSA_ClaimDeficiencies_ForUpload_20111213] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[MSA_ClaimDeficiencies_ForUpload_20111213] TO [power_user]
GRANT SELECT ON  [dbo].[MSA_ClaimDeficiencies_ForUpload_20111213] TO [power_user]
GO
