CREATE TABLE [dbo].[tblDefenseCounselAddress_ALT]
(
[DefenseCounsel] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [int] NOT NULL,
[DefenseCounselID] [int] NULL,
[DefenseCounselAddress1] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddress2] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselCity] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselZip] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselMailZip] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselPhone] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselFax] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullNameState] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoFilters] [bit] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselAddress_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselAddress_ALT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselAddress_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselAddress_ALT] TO [power_user]
GO
