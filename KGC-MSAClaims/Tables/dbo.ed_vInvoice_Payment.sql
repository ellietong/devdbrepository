CREATE TABLE [dbo].[ed_vInvoice_Payment]
(
[InvoiceID] [int] NULL,
[DocumentNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountApplied] [money] NULL,
[FilePathExt] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentDate] [datetime] NULL,
[DocumentAmount] [money] NULL,
[PaymentReference] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ed_vInvoice_Payment] TO [base_user]
GRANT SELECT ON  [dbo].[ed_vInvoice_Payment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ed_vInvoice_Payment] TO [power_user]
GRANT SELECT ON  [dbo].[ed_vInvoice_Payment] TO [power_user]
GRANT INSERT ON  [dbo].[ed_vInvoice_Payment] TO [power_user]
GRANT DELETE ON  [dbo].[ed_vInvoice_Payment] TO [power_user]
GRANT UPDATE ON  [dbo].[ed_vInvoice_Payment] TO [power_user]
GO
