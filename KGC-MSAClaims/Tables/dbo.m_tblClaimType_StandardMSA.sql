CREATE TABLE [dbo].[m_tblClaimType_StandardMSA]
(
[ClaimTypeID] [int] NOT NULL IDENTITY(1, 1),
[MaterialID] [int] NULL,
[CauseAgent] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblClaimType_StandardMSA] ADD CONSTRAINT [PK_m_tblClaimType_StandardMSA] PRIMARY KEY CLUSTERED  ([ClaimTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimType_StandardMSA] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblClaimType_StandardMSA] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimType_StandardMSA] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblClaimType_StandardMSA] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblClaimType_StandardMSA] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblClaimType_StandardMSA] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblClaimType_StandardMSA] TO [power_user]
GO
