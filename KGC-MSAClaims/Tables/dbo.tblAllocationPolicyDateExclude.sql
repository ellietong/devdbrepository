CREATE TABLE [dbo].[tblAllocationPolicyDateExclude]
(
[PolicyDateExcludeID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[PolicyDateID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationPolicyDateExclude] ADD CONSTRAINT [PK_tblAllocationPolicyDateExclude] PRIMARY KEY NONCLUSTERED  ([PolicyDateExcludeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationPolicyDateExclude] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationPolicyDateExclude] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationPolicyDateExclude] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationPolicyDateExclude] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationPolicyDateExclude] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationPolicyDateExclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationPolicyDateExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationPolicyDateExclude] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationPolicyDateExclude] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationPolicyDateExclude] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationPolicyDateExclude] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationPolicyDateExclude] TO [power_user]
GO
