CREATE TABLE [dbo].[InvoiceSummary_2008Prior]
(
[NCC Reference] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pre Allocation] [money] NULL,
[Post Allocation] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[InvoiceSummary_2008Prior] TO [base_user]
GRANT SELECT ON  [dbo].[InvoiceSummary_2008Prior] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[InvoiceSummary_2008Prior] TO [power_user]
GRANT SELECT ON  [dbo].[InvoiceSummary_2008Prior] TO [power_user]
GO
