CREATE TABLE [dbo].[tblCheckApproval]
(
[CheckApprovalID] [bigint] NOT NULL IDENTITY(1, 1),
[CheckID] [bigint] NULL,
[IsApproved] [bit] NULL,
[ApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedDate] [datetime] NULL,
[IsCheckCleared] [bit] NULL,
[CheckClearedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCheckApproval] ADD CONSTRAINT [PK_tblCheckApproval] PRIMARY KEY CLUSTERED  ([CheckApprovalID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCheckApproval] TO [base_user]
GRANT SELECT ON  [dbo].[tblCheckApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckApproval] TO [CheckApproval_User]
GRANT SELECT ON  [dbo].[tblCheckApproval] TO [CheckApproval_User]
GRANT INSERT ON  [dbo].[tblCheckApproval] TO [CheckApproval_User]
GRANT UPDATE ON  [dbo].[tblCheckApproval] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckApproval] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[tblCheckApproval] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckApproval] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblCheckApproval] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckApproval] TO [power_user]
GRANT SELECT ON  [dbo].[tblCheckApproval] TO [power_user]
GO
