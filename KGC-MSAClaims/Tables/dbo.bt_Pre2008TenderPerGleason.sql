CREATE TABLE [dbo].[bt_Pre2008TenderPerGleason]
(
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyNum] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[bt_Pre2008TenderPerGleason] TO [base_user]
GRANT SELECT ON  [dbo].[bt_Pre2008TenderPerGleason] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[bt_Pre2008TenderPerGleason] TO [power_user]
GRANT SELECT ON  [dbo].[bt_Pre2008TenderPerGleason] TO [power_user]
GRANT INSERT ON  [dbo].[bt_Pre2008TenderPerGleason] TO [power_user]
GRANT DELETE ON  [dbo].[bt_Pre2008TenderPerGleason] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_Pre2008TenderPerGleason] TO [power_user]
GO
