CREATE TABLE [dbo].[tblTender_Response]
(
[TenderResponseID] [bigint] NOT NULL IDENTITY(1, 1),
[TenderHistoryID] [int] NULL,
[PersonID] [int] NULL,
[DocumentID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTender_Response] ADD CONSTRAINT [PK_tblTender_Response] PRIMARY KEY CLUSTERED  ([TenderResponseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTender_Response] TO [base_user]
GRANT SELECT ON  [dbo].[tblTender_Response] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTender_Response] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblTender_Response] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblTender_Response] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblTender_Response] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblTender_Response] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTender_Response] TO [power_user]
GRANT SELECT ON  [dbo].[tblTender_Response] TO [power_user]
GRANT INSERT ON  [dbo].[tblTender_Response] TO [power_user]
GRANT DELETE ON  [dbo].[tblTender_Response] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTender_Response] TO [power_user]
GO
