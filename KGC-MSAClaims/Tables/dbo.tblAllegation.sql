CREATE TABLE [dbo].[tblAllegation]
(
[AllegationID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[DefendantCompanyID] [int] NULL,
[AllegationTypeID] [int] NULL,
[DamageID] [int] NULL,
[MaterialID] [int] NULL,
[MaterialID_NCC] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegation] ADD CONSTRAINT [PK__tblAllegation1__414EAC47] PRIMARY KEY CLUSTERED  ([AllegationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblAllegation_AllegationTypeID_ClaimLawsuitID] ON [dbo].[tblAllegation] ([AllegationTypeID]) INCLUDE ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblAllegation_ClaimLawsuitID_AllegationTypeID] ON [dbo].[tblAllegation] ([ClaimLawsuitID], [AllegationTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegation] WITH NOCHECK ADD CONSTRAINT [FK_tblAllegation_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllegation] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllegation] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllegation] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllegation] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllegation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllegation] TO [power_user]
GO
