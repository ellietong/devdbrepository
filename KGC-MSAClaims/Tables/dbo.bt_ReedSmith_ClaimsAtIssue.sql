CREATE TABLE [dbo].[bt_ReedSmith_ClaimsAtIssue]
(
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[bt_ReedSmith_ClaimsAtIssue] TO [base_user]
GRANT ALTER ON  [dbo].[bt_ReedSmith_ClaimsAtIssue] TO [power_user]
GRANT SELECT ON  [dbo].[bt_ReedSmith_ClaimsAtIssue] TO [power_user]
GRANT INSERT ON  [dbo].[bt_ReedSmith_ClaimsAtIssue] TO [power_user]
GRANT DELETE ON  [dbo].[bt_ReedSmith_ClaimsAtIssue] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_ReedSmith_ClaimsAtIssue] TO [power_user]
GO
