CREATE TABLE [dbo].[ImportDocs_Staging]
(
[BEGNO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantPersonalInfoID] [float] NULL,
[Link] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ImportDocs_Staging] TO [base_user]
GRANT SELECT ON  [dbo].[ImportDocs_Staging] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ImportDocs_Staging] TO [power_user]
GRANT SELECT ON  [dbo].[ImportDocs_Staging] TO [power_user]
GO
