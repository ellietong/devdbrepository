CREATE TABLE [dbo].[mtblNCCSource20111101]
(
[#Update] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CoverageInformation] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientMatter] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientName] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[No#plt] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Style] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCReference] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Msa_id] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Spouse] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Children] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CoWorker] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmokingHistory] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Education] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MilitaryUnion] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Worksite] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearsatWorksite] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSACounsel] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounsel] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrialDate] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fees] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Expenses] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Total] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Msgid] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Closdbx#] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Applicat] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deflead] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Docid] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Docdate] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Doctitle] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Dco] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Defexpts] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Issues] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Otherdef] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Privlege] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rule11] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Summary] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reviewer] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblNCCSource20111101] TO [base_user]
GRANT SELECT ON  [dbo].[mtblNCCSource20111101] TO [base_user]
GRANT SELECT ON  [dbo].[mtblNCCSource20111101] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[mtblNCCSource20111101] TO [power_user]
GRANT SELECT ON  [dbo].[mtblNCCSource20111101] TO [power_user]
GO
