CREATE TABLE [dbo].[tblDefenseInvoice]
(
[DefenseInvoiceID] [bigint] NOT NULL IDENTITY(1, 1),
[StatusID] [int] NULL,
[FirmID] [bigint] NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeAmount] [money] NULL CONSTRAINT [DF_mBtblDefenseInvoice_FeeAmount] DEFAULT ((0)),
[DisbursementAmount] [money] NULL CONSTRAINT [DF_mBtblDefenseInvoice_DisbursementAmount] DEFAULT ((0)),
[Adjustment] [money] NULL CONSTRAINT [DF_mBtblDefenseInvoice_Adjustment] DEFAULT ((0)),
[InvoiceTotal] [money] NULL,
[SubmitDate] [datetime] NULL,
[DatePayable] [datetime] NULL,
[CheckNumber] [bigint] NULL,
[FundsRequestDate] [datetime] NULL,
[DatePaid] [datetime] NULL,
[DocumentID] [bigint] NULL,
[AdjustmentNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceLink] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckLink] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[VendorFirmID] [int] NULL,
[Firm] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodStart] [datetime] NULL,
[PeriodEnd] [datetime] NULL,
[InvoiceTotal_Receivable] [money] NULL,
[Pre2008] [bit] NULL,
[PaymentReference] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [int] NULL,
[DatePaymentApproved] [datetime] NULL,
[OriginalSourceID] [int] NULL,
[BatchNumber] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateApproved] [datetime] NULL,
[QCDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseInvoice] ADD CONSTRAINT [PK_mBtblDefenseInvoiceTracking] PRIMARY KEY CLUSTERED  ([DefenseInvoiceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblDefenseInvoice] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblDefenseInvoice] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblDefenseInvoice] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblDefenseInvoice] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblDefenseInvoice] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseInvoice] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseInvoice] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoice] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseInvoice] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseInvoice] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'Adjustment'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'Adjustment'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'Adjustment'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'Adjustment'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'Adjustment'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'Adjustment'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'CheckNumber'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'CheckNumber'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'CheckNumber'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'CheckNumber'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'CheckNumber'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'CheckNumber'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DatePayable'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DatePayable'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DatePayable'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DefenseInvoiceID'
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DefenseInvoiceID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DefenseInvoiceID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DefenseInvoiceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DefenseInvoiceID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DefenseInvoiceID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DisbursementAmount'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DisbursementAmount'
GO
DECLARE @xp int
SELECT @xp=1065
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DisbursementAmount'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DisbursementAmount'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DisbursementAmount'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'DisbursementAmount'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FeeAmount'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FeeAmount'
GO
DECLARE @xp int
SELECT @xp=1245
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FeeAmount'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FeeAmount'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FeeAmount'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FeeAmount'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FirmID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FirmID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'InvoiceNumber'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'InvoiceNumber'
GO
DECLARE @xp int
SELECT @xp=1980
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'InvoiceNumber'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'InvoiceTotal'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'InvoiceTotal'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'InvoiceTotal'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'Period'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'Period'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'Period'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'StatusID'
GO
DECLARE @xp int
SELECT @xp=3
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'StatusID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'StatusID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'StatusID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'StatusID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'SubmitDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'SubmitDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice', 'COLUMN', N'SubmitDate'
GO
