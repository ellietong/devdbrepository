CREATE TABLE [dbo].[tblDefInvClaimantDetail_Pre2008]
(
[InvoiceDetailID] [int] NOT NULL IDENTITY(1, 1),
[DefenseInvoiceID] [int] NULL,
[ClaimID] [int] NULL,
[FirmID] [int] NULL,
[InvoiceNumber] [nchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [date] NULL,
[Fees] [money] NULL,
[Costs] [money] NULL,
[Adjustments] [money] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefInvClaimantDetail_Pre2008] ADD CONSTRAINT [PK_tblDefInvClaimantDetail_Pre2008] PRIMARY KEY CLUSTERED  ([InvoiceDetailID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail_Pre2008] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail_Pre2008] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail_Pre2008] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail_Pre2008] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail_Pre2008] TO [power_user]
GO
