CREATE TABLE [dbo].[MMOAuditDocLoadSheet2]
(
[ClaimantPersonalInfoID] [float] NULL,
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTypeID] [float] NULL,
[DocumentTitle] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F6] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[MMOAuditDocLoadSheet2] TO [base_user]
GRANT SELECT ON  [dbo].[MMOAuditDocLoadSheet2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[MMOAuditDocLoadSheet2] TO [power_user]
GRANT SELECT ON  [dbo].[MMOAuditDocLoadSheet2] TO [power_user]
GO
