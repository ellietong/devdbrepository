CREATE TABLE [dbo].[ed_PolicyLookup]
(
[PolicyID_ED] [float] NULL,
[PolicyDateID_KCIC] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ed_PolicyLookup] TO [base_user]
GRANT SELECT ON  [dbo].[ed_PolicyLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ed_PolicyLookup] TO [power_user]
GRANT SELECT ON  [dbo].[ed_PolicyLookup] TO [power_user]
GRANT INSERT ON  [dbo].[ed_PolicyLookup] TO [power_user]
GRANT DELETE ON  [dbo].[ed_PolicyLookup] TO [power_user]
GRANT UPDATE ON  [dbo].[ed_PolicyLookup] TO [power_user]
GO
