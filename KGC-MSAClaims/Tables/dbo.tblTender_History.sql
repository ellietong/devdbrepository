CREATE TABLE [dbo].[tblTender_History]
(
[TenderHistoryID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID] [bigint] NULL,
[DocumentID] [bigint] NULL,
[DOFE] [datetime] NULL,
[TenderTypeID] [int] NULL,
[SentDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTender_History] ADD CONSTRAINT [PK_tblTender_History] PRIMARY KEY CLUSTERED  ([TenderHistoryID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTender_History] WITH NOCHECK ADD CONSTRAINT [FK_tblTender_History_tblDocuments] FOREIGN KEY ([DocumentID]) REFERENCES [dbo].[tblDocuments] ([DocumentID]) ON DELETE CASCADE NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[tblTender_History] NOCHECK CONSTRAINT [FK_tblTender_History_tblDocuments]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTender_History] TO [base_user]
GRANT SELECT ON  [dbo].[tblTender_History] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTender_History] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblTender_History] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblTender_History] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblTender_History] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblTender_History] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTender_History] TO [power_user]
GRANT SELECT ON  [dbo].[tblTender_History] TO [power_user]
GRANT INSERT ON  [dbo].[tblTender_History] TO [power_user]
GRANT DELETE ON  [dbo].[tblTender_History] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTender_History] TO [power_user]
GO
