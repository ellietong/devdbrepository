CREATE TABLE [dbo].[bt_Gleason_MultiTriggerIndemnity]
(
[BillingCycle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostingDate] [datetime] NULL,
[PaidDate] [datetime] NULL,
[NetSettlementAmount] [money] NULL,
[DOFEWhenBilled] [datetime] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[bt_Gleason_MultiTriggerIndemnity] TO [base_user]
GRANT ALTER ON  [dbo].[bt_Gleason_MultiTriggerIndemnity] TO [power_user]
GRANT SELECT ON  [dbo].[bt_Gleason_MultiTriggerIndemnity] TO [power_user]
GRANT INSERT ON  [dbo].[bt_Gleason_MultiTriggerIndemnity] TO [power_user]
GRANT DELETE ON  [dbo].[bt_Gleason_MultiTriggerIndemnity] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_Gleason_MultiTriggerIndemnity] TO [power_user]
GO
