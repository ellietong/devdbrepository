CREATE TABLE [dbo].[tblProvision]
(
[ProvisionID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionLabelID] [int] NULL,
[PolicyID] [int] NULL,
[ProvisionValueID] [int] NULL,
[ProvisionNotes] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvision] ADD CONSTRAINT [PK_dropTemptblProvision] PRIMARY KEY CLUSTERED  ([ProvisionID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProvision] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvision] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblProvision] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvision] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvision] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvision] TO [power_user]
GRANT ALTER ON  [dbo].[tblProvision] TO [Provision_User]
GRANT SELECT ON  [dbo].[tblProvision] TO [Provision_User]
GRANT INSERT ON  [dbo].[tblProvision] TO [Provision_User]
GRANT DELETE ON  [dbo].[tblProvision] TO [Provision_User]
GRANT UPDATE ON  [dbo].[tblProvision] TO [Provision_User]
GO
