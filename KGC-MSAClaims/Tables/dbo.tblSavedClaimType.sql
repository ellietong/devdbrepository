CREATE TABLE [dbo].[tblSavedClaimType]
(
[SavedAllegationID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimTypeID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedClaimType] ADD CONSTRAINT [PK_tblSavedAllegation] PRIMARY KEY CLUSTERED  ([SavedAllegationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedClaimType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedClaimType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedClaimType] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedClaimType] TO [power_user]
GO
