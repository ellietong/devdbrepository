CREATE TABLE [dbo].[tblTortType]
(
[TortTypeID] [int] NOT NULL IDENTITY(1, 1),
[TortType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTortType] ADD CONSTRAINT [PK_tblTortType] PRIMARY KEY CLUSTERED  ([TortTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTortType] TO [base_user]
GRANT SELECT ON  [dbo].[tblTortType] TO [base_user]
GRANT SELECT ON  [dbo].[tblTortType] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblTortType] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblTortType] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblTortType] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTortType] TO [power_user]
GRANT SELECT ON  [dbo].[tblTortType] TO [power_user]
GO
