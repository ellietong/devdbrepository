CREATE TABLE [dbo].[tblGender]
(
[GenderID] [int] NOT NULL IDENTITY(1, 1),
[Gender] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblGender] ADD CONSTRAINT [PK_mBtblGender] PRIMARY KEY CLUSTERED  ([GenderID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblGender] TO [base_user]
GRANT SELECT ON  [dbo].[tblGender] TO [base_user]
GRANT SELECT ON  [dbo].[tblGender] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblGender] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblGender] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblGender] TO [power_user]
GRANT SELECT ON  [dbo].[tblGender] TO [power_user]
GRANT INSERT ON  [dbo].[tblGender] TO [power_user]
GRANT UPDATE ON  [dbo].[tblGender] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblGender', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblGender', 'COLUMN', N'Gender'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblGender', 'COLUMN', N'Gender'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblGender', 'COLUMN', N'Gender'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblGender', 'COLUMN', N'GenderID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblGender', 'COLUMN', N'GenderID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblGender', 'COLUMN', N'GenderID'
GO
