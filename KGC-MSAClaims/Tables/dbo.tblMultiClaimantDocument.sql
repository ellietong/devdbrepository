CREATE TABLE [dbo].[tblMultiClaimantDocument]
(
[MultiClaimantDocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[DocumentID] [bigint] NOT NULL,
[ClaimantPersonalInfoID] [bigint] NOT NULL,
[HistoricalDocumentLink] [bigint] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[ClaimLawsuitID] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_DeleteMultiDoc]
   ON  [dbo].[tblMultiClaimantDocument]
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE tblTenderHistory FROM tblTenderHistory th
    INNER JOIN tblClaimTenderHistory cth on th.TenderHistoryID = cth.TenderHistoryID
    INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
    INNER JOIN deleted d ON d.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	WHERE ComplaintDocumentID  = d.DocumentID

	
	
END
GO
DISABLE TRIGGER [dbo].[TRIG_DeleteMultiDoc] ON [dbo].[tblMultiClaimantDocument]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[TRIG_InsertTenderHistory]
   ON  [dbo].[tblMultiClaimantDocument]
   AFTER INSERT
AS 
BEGIN
SET NOCOUNT ON

IF( (SELECT DetailDocumentTypeID FROM inserted INNER JOIN tblDocuments d ON inserted.DocumentID = d.DocumentID) = 8 AND
	(SELECT RIGHT(CreatedBy, 8) FROM inserted INNER JOIN tblDocuments d ON inserted.DocumentID = d.DocumentID) = 'kcic.com')
BEGIN
	DECLARE @DocumentID bigint = (SELECT DocumentID FROM inserted)
	DECLARE @DateCreated datetime = (SELECT DateCreated FROM tblDocuments WHERE DocumentID = @DocumentID)
	DECLARE @ClaimantPersonalInfoID int = (SELECT ClaimantPersonalInfoID FROM inserted)
	DECLARE @ClaimID int = (SELECT ClaimID FROM tblClaim WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND IsCurrentClaim = 1)
	DECLARE @ExistingClaimID int = (SELECT ClaimID FROM tblTender_History WHERE ClaimID = @ClaimID)

	IF COALESCE(@ExistingClaimID, 0) = 0
	BEGIN
		DECLARE @DOFE datetime = dbo.fnEarliestExposure(@ClaimantPersonalInfoID)
		DECLARE @ContainsAsbestos int = (SELECT ContainsAsbestos
										 FROM tblClaimLawsuit		  cl
											 INNER JOIN tblAllegation a  ON cl.ClaimLawsuitID = a.ClaimLawsuitID
											 INNER JOIN tblMaterial   m  ON a.MaterialID = m.MaterialID
										 WHERE ClaimID = @ClaimID)

		INSERT INTO tblTender_History (ClaimID, DocumentID, DOFE, TenderTypeID)
			SELECT @ClaimID, @DocumentID, @DOFE, 1 as TenderTypeID

		DECLARE @TenderHistoryID bigint = (SELECT IDENT_CURRENT('tblTender_History'))
		DECLARE @TenderCarrierGroup int
		DECLARE @TenderCarrierGroups TABLE (CarrierGroupID int)
		DECLARE @TenderCarrierGroupPeople TABLE (PersonID int)
		
		IF @ContainsAsbestos = 0
		BEGIN
			INSERT INTO @TenderCarrierGroups
				SELECT DISTINCT CarrierGroupID
				FROM tblPolicy p
					INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
					INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID
				WHERE PEndDate >= COALESCE(@DOFE, '1/1/1900')
					AND COALESCE(ExhaustedDate, '1/1/2100') >= @DateCreated
		END
		ELSE
		BEGIN
			INSERT INTO @TenderCarrierGroups
				SELECT DISTINCT CarrierGroupID
				FROM tblPolicy p
					INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
					INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID
				WHERE PEndDate >= COALESCE(@DOFE, '1/1/1900')
					AND PStartDate < '1/1/1985'
					AND COALESCE(ExhaustedDate, '1/1/2100') >= @DateCreated
		END
	
		DECLARE curs_TenderCarrierGroups CURSOR FOR SELECT DISTINCT CarrierGroupID FROM @TenderCarrierGroups
		OPEN curs_TenderCarrierGroups
		FETCH NEXT FROM curs_TenderCarrierGroups INTO @TenderCarrierGroup

		IF COALESCE(@TenderCarrierGroup, 0) = 0
		BEGIN
			INSERT INTO @TenderCarrierGroupPeople
				SELECT PersonID
				FROM vTenderContacts
				WHERE CarrierGroupID = 999
				AND TenderContactTo = 1

			INSERT INTO tblTender_Acknowledgement (TenderHistoryID, PersonID)
				SELECT @TenderHistoryID, PersonID
				FROM @TenderCarrierGroupPeople
		END

		WHILE @@Fetch_Status = 0
		BEGIN
			INSERT INTO @TenderCarrierGroupPeople
				SELECT PersonID
				FROM vTenderContacts
				WHERE CarrierGroupID = @TenderCarrierGroup
				AND TenderContactTo = 1

			INSERT INTO tblTender_Acknowledgement (TenderHistoryID, PersonID)
				SELECT @TenderHistoryID, PersonID
				FROM @TenderCarrierGroupPeople
				
			DELETE FROM @TenderCarrierGroupPeople
			
			FETCH NEXT FROM curs_TenderCarrierGroups INTO @TenderCarrierGroup
		END

		DELETE FROM @TenderCarrierGroups
		
		CLOSE curs_TenderCarrierGroups 
		DEALLOCATE curs_TenderCarrierGroups
	END
END
END
GO
ALTER TABLE [dbo].[tblMultiClaimantDocument] ADD CONSTRAINT [PK_tblMultiClaimantDocument] PRIMARY KEY CLUSTERED  ([MultiClaimantDocumentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblMultiClaimantDocument_ClaimantPersonalInfoID] ON [dbo].[tblMultiClaimantDocument] ([ClaimantPersonalInfoID]) INCLUDE ([DocumentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblMultiClaimantDocument_DocumentID] ON [dbo].[tblMultiClaimantDocument] ([DocumentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMultiClaimantDocument] WITH NOCHECK ADD CONSTRAINT [FK_tblMultiClaimantDocument_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID])
GO
ALTER TABLE [dbo].[tblMultiClaimantDocument] WITH NOCHECK ADD CONSTRAINT [FK_tblMultiClaimantDocument_tblDocuments] FOREIGN KEY ([DocumentID]) REFERENCES [dbo].[tblDocuments] ([DocumentID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMultiClaimantDocument] TO [base_user]
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [base_user]
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GRANT INSERT ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GRANT DELETE ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GO
