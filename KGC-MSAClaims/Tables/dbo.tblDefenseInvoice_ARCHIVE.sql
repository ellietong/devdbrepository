CREATE TABLE [dbo].[tblDefenseInvoice_ARCHIVE]
(
[DefenseInvoiceID] [bigint] NULL,
[StatusID] [int] NULL,
[FirmID] [int] NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeAmount] [money] NULL,
[DisbursementAmount] [money] NULL,
[Adjustment] [money] NULL,
[InvoiceTotal] [money] NULL,
[SubmitDate] [datetime] NULL,
[DatePayable] [datetime] NULL,
[CheckNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FundsRequestDate] [datetime] NULL,
[DatePaid] [datetime] NULL,
[DocumentID] [bigint] NULL,
[AdjustmentNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceLink] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckLink] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[ClaimantPersonalInfoID] [int] NULL,
[VendorFirmID] [int] NULL,
[Firm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodStart] [datetime] NULL,
[PeriodEnd] [datetime] NULL,
[InvoiceTOtal_Receivable] [money] NULL,
[Pre2008] [bit] NULL,
[PaymentReference] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [int] NULL,
[DatePaymentApproved] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseInvoice_ARCHIVE] TO [power_user]
GO
