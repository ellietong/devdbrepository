CREATE TABLE [dbo].[tblAdditionalNamedInsured]
(
[AdditionalNamedInsID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NULL,
[NamedInsuredID] [int] NULL,
[AdditionalNamedInsTypeID] [int] NULL,
[Bates] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EffectiveDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAdditionalNamedInsured] ADD CONSTRAINT [PK_tblAdditionalNamedInsured] PRIMARY KEY NONCLUSTERED  ([AdditionalNamedInsID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAdditionalNamedInsured] TO [base_user]
GRANT SELECT ON  [dbo].[tblAdditionalNamedInsured] TO [base_user]
GRANT ALTER ON  [dbo].[tblAdditionalNamedInsured] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblAdditionalNamedInsured] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblAdditionalNamedInsured] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblAdditionalNamedInsured] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblAdditionalNamedInsured] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAdditionalNamedInsured] TO [power_user]
GRANT SELECT ON  [dbo].[tblAdditionalNamedInsured] TO [power_user]
GRANT INSERT ON  [dbo].[tblAdditionalNamedInsured] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAdditionalNamedInsured] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblAdditionalNamedInsured', 'COLUMN', N'Bates'
GO
EXEC sp_addextendedproperty N'MS_Format', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblAdditionalNamedInsured', 'COLUMN', N'Bates'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblAdditionalNamedInsured', 'COLUMN', N'Bates'
GO
