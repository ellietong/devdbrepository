CREATE TABLE [dbo].[ImportDocs_Pulled]
(
[BEGNO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantPersonalInfoID] [float] NULL,
[Link] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ImportDocs_Pulled] TO [base_user]
GRANT SELECT ON  [dbo].[ImportDocs_Pulled] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ImportDocs_Pulled] TO [power_user]
GRANT SELECT ON  [dbo].[ImportDocs_Pulled] TO [power_user]
GO
