CREATE TABLE [dbo].[tblSavedTortType]
(
[SavedMatterID] [bigint] NOT NULL IDENTITY(1, 1),
[TortTypeID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedTortType] ADD CONSTRAINT [PK_tblSavedMatter] PRIMARY KEY CLUSTERED  ([SavedMatterID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedTortType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedTortType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedTortType] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedTortType] TO [power_user]
GO
