CREATE TABLE [dbo].[tblAllocationResultsColumn]
(
[ColumnID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[InsuranceProgramID] [int] NULL,
[ColumnStartDate] [datetime] NULL,
[ColumnEndDate] [datetime] NULL,
[ColumnDays] [int] NULL,
[ColumnPerOcc] [money] NULL,
[ColumnCumAllocation] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsColumn] ADD CONSTRAINT [PK_tblAllocationResultsColumn] PRIMARY KEY NONCLUSTERED  ([ColumnID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsColumn] ADD CONSTRAINT [FK_tblAllocationResultsColumn_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblAllocationResultsColumn] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationResultsColumn] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationResultsColumn] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationResultsColumn] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsColumn] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsColumn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsColumn] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsColumn] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationResultsColumn] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationResultsColumn] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationResultsColumn] TO [power_user]
GO
