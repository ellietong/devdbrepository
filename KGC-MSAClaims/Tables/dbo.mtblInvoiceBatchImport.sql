CREATE TABLE [dbo].[mtblInvoiceBatchImport]
(
[LawFirm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Costs] [money] NULL,
[PeriodStart] [date] NULL,
[PeriodEnd] [date] NULL,
[MatterName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportDate] [date] NULL CONSTRAINT [DF_mtblInvoiceBatchImport_ImportDate] DEFAULT (getdate()),
[PaidDate] [date] NULL,
[PaymentStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblInvoiceBatchImport] TO [base_user]
GRANT SELECT ON  [dbo].[mtblInvoiceBatchImport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[mtblInvoiceBatchImport] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblInvoiceBatchImport] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblInvoiceBatchImport] TO [Claims_User]
GRANT DELETE ON  [dbo].[mtblInvoiceBatchImport] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblInvoiceBatchImport] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[mtblInvoiceBatchImport] TO [power_user]
GRANT SELECT ON  [dbo].[mtblInvoiceBatchImport] TO [power_user]
GO
