CREATE TABLE [dbo].[tblGenericLanguage]
(
[GenericLanguageID] [int] NOT NULL IDENTITY(1, 1),
[GenericLanguage] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblGenericLanguage] ADD CONSTRAINT [PK_tblLanguage] PRIMARY KEY CLUSTERED  ([GenericLanguageID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblGenericLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblGenericLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblGenericLanguage] TO [Language_User]
GRANT INSERT ON  [dbo].[tblGenericLanguage] TO [Language_User]
GRANT DELETE ON  [dbo].[tblGenericLanguage] TO [Language_User]
GRANT UPDATE ON  [dbo].[tblGenericLanguage] TO [Language_User]
GRANT VIEW DEFINITION ON  [dbo].[tblGenericLanguage] TO [power_user]
GRANT SELECT ON  [dbo].[tblGenericLanguage] TO [power_user]
GRANT INSERT ON  [dbo].[tblGenericLanguage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblGenericLanguage] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderByOn', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2147483647
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_ColumnHidden', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', 'COLUMN', N'GenericLanguage'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', 'COLUMN', N'GenericLanguage'
GO
DECLARE @xp int
SELECT @xp=5355
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', 'COLUMN', N'GenericLanguage'
GO
EXEC sp_addextendedproperty N'MS_ColumnHidden', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', 'COLUMN', N'GenericLanguageID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', 'COLUMN', N'GenericLanguageID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblGenericLanguage', 'COLUMN', N'GenericLanguageID'
GO
