CREATE TABLE [dbo].[bt_tblImport_MultiTriggerBillings]
(
[SubID] [int] NOT NULL IDENTITY(1, 1),
[UniqueID] [int] NULL,
[GleasonSource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingCycle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCCReferenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceStartDate] [datetime] NULL,
[InvoiceEndDate] [datetime] NULL,
[Fees] [money] NULL,
[Expenses] [money] NULL,
[TotalCosts] [money] NULL,
[BatchNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentDocNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyDateID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[bt_tblImport_MultiTriggerBillings] TO [base_user]
GRANT SELECT ON  [dbo].[bt_tblImport_MultiTriggerBillings] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[bt_tblImport_MultiTriggerBillings] TO [power_user]
GRANT SELECT ON  [dbo].[bt_tblImport_MultiTriggerBillings] TO [power_user]
GRANT INSERT ON  [dbo].[bt_tblImport_MultiTriggerBillings] TO [power_user]
GRANT DELETE ON  [dbo].[bt_tblImport_MultiTriggerBillings] TO [power_user]
GRANT UPDATE ON  [dbo].[bt_tblImport_MultiTriggerBillings] TO [power_user]
GO
