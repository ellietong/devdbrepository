CREATE TABLE [dbo].[USysRibbons]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[RibbonName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RibbonXML] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[USysRibbons] ADD CONSTRAINT [PK_USysRibbons] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[USysRibbons] TO [base_user]
GRANT SELECT ON  [dbo].[USysRibbons] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[USysRibbons] TO [power_user]
GRANT SELECT ON  [dbo].[USysRibbons] TO [power_user]
GO
