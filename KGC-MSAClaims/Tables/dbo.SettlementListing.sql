CREATE TABLE [dbo].[SettlementListing]
(
[Paid Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Posting Date] [datetime] NULL,
[NCC_Reference] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settlement Amount] [money] NULL,
[Amount of Reimbursement] [money] NULL,
[Net Out of Pocket Settlement Amount] [money] NULL,
[Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[SettlementListing] TO [base_user]
GRANT SELECT ON  [dbo].[SettlementListing] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[SettlementListing] TO [power_user]
GRANT SELECT ON  [dbo].[SettlementListing] TO [power_user]
GO
