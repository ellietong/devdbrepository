CREATE TABLE [dbo].[InsurerResponses]
(
[BEGNO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENDNO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRODBEG] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRODEND] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CATEGORY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[InsurerResponses] TO [base_user]
GRANT SELECT ON  [dbo].[InsurerResponses] TO [base_user]
GRANT SELECT ON  [dbo].[InsurerResponses] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[InsurerResponses] TO [power_user]
GRANT SELECT ON  [dbo].[InsurerResponses] TO [power_user]
GO
