CREATE TABLE [dbo].[ztblByMonthColumns]
(
[MonthColumnID] [int] NOT NULL IDENTITY(1, 1),
[ColumnID] [int] NULL,
[ColumnStartDate] [datetime] NULL,
[ColumnEndDate] [datetime] NULL,
[ColumnRemaining] [money] NULL,
[TriggeredDays] [int] NULL,
[TotalMonths] [int] NULL,
[MinRemaining] [money] NULL,
[InsuranceProgramID] [int] NULL,
[OccurrenceID] [bigint] NULL,
[AllocationID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblByMonthColumns] ADD CONSTRAINT [PK_ztblByMonthColumns] PRIMARY KEY CLUSTERED  ([MonthColumnID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblByMonthColumns] TO [Allocation_User]
GRANT SELECT ON  [dbo].[ztblByMonthColumns] TO [Allocation_User]
GRANT INSERT ON  [dbo].[ztblByMonthColumns] TO [Allocation_User]
GRANT DELETE ON  [dbo].[ztblByMonthColumns] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[ztblByMonthColumns] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[ztblByMonthColumns] TO [base_user]
GRANT SELECT ON  [dbo].[ztblByMonthColumns] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblByMonthColumns] TO [power_user]
GRANT SELECT ON  [dbo].[ztblByMonthColumns] TO [power_user]
GRANT INSERT ON  [dbo].[ztblByMonthColumns] TO [power_user]
GRANT DELETE ON  [dbo].[ztblByMonthColumns] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblByMonthColumns] TO [power_user]
GO
