CREATE TABLE [dbo].[tblCheckFundingSource]
(
[FundingSourceID] [int] NOT NULL IDENTITY(1, 1),
[FundingSource] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCheckFundingSource] ADD CONSTRAINT [PK_tblFundingSource] PRIMARY KEY CLUSTERED  ([FundingSourceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingSource] TO [base_user]
GRANT SELECT ON  [dbo].[tblCheckFundingSource] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingSource] TO [CheckApproval_User]
GRANT SELECT ON  [dbo].[tblCheckFundingSource] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingSource] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[tblCheckFundingSource] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingSource] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblCheckFundingSource] TO [CheckWriter_User]
GRANT INSERT ON  [dbo].[tblCheckFundingSource] TO [CheckWriter_User]
GRANT UPDATE ON  [dbo].[tblCheckFundingSource] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingSource] TO [power_user]
GRANT SELECT ON  [dbo].[tblCheckFundingSource] TO [power_user]
GO
