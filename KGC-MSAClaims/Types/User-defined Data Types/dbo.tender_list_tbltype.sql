CREATE TYPE [dbo].[tender_list_tbltype] AS TABLE
(
[ClaimID] [bigint] NOT NULL,
[TenderBlockID] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([ClaimID])
)
GO
