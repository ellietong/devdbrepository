SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantLawsuitStatusGroupList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimLawsuitStatusGroupID, ClaimLawsuitStatusGroup FROM tblClaimLawsuitStatusGroup WHERE ClaimLawsuitStatusGroupID NOT IN (2)
	UNION
	SELECT NULL, ''
	ORDER BY ClaimLawsuitStatusGroup
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantLawsuitStatusGroupList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantLawsuitStatusGroupList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimantLawsuitStatusGroupList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantLawsuitStatusGroupList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantLawsuitStatusGroupList] TO [power_user]
GO
