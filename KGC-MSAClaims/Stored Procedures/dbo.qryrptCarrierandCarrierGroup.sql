SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


create  PROCEDURE [dbo].[qryrptCarrierandCarrierGroup]
(@carrierID int  = null, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL, @startDate int = NULL, @endDate int = NULL
)
AS
SET NOCOUNT ON

DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)
DECLARE @maxEndDate int

-- Find max ID values
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxEndDate = MAX(annualperiod) FROM vPolicy
IF @carrierGroupID = 0 SET @carrierGroupID = NULL

SELECT CarrierID, CarrierGroupName, CarrierName, CarrierSolvency, PolicyHolder, CarrierGroupID
FROM vPolicy
WHERE CarrierID >= COALESCE(@carrierID, 0) AND CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
	(CarrierGroupID >= COALESCE(@carrierGroupID, 0) AND CarrierGroupID <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR CarrierGroupID = @carrierGroupID) AND
	CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%')
AND AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
GROUP BY CarrierID, CarrierGroupName, CarrierName, CarrierSolvency, PolicyHolder, CarrierGroupID
ORDER BY CarrierGroupName, CarrierName

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCarrierandCarrierGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCarrierandCarrierGroup] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptCarrierandCarrierGroup] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCarrierandCarrierGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCarrierandCarrierGroup] TO [power_user]
GO
