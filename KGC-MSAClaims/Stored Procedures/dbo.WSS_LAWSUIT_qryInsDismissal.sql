SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryInsDismissal]
(@ClaimLawsuitID bigint, @DismissalTypeID int, @DismissalDate date, @DocumentID bigint, @Emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



	--If settled or dismissed, add dismissal but don't change the status - settlement is still primary status.
        IF ( SELECT claimlawsuitstatusid
             FROM   tblClaimLawsuitStatusTracking
             WHERE  ClaimLawsuitID = @ClaimLawsuitID
                    AND IsPrimaryStatus = 1
           ) IN ( 23, 24, 25, 15, 16, 17,18,19,26,27,30,34 ) 
	BEGIN

		insert into tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, StatusNotes, ProcessedDate)
		select @ClaimLawsuitID, @DismissalTypeID, @DismissalDate, 0, 'Updated by upload of documentID ' + convert(varchar(500),@DocumentID) + ' by ' + @EmailAddress, convert(date,GETDATE())

	END
	ELSE
	BEGIN
	
		--Claim not settled, dismissal will change status and become primary.
		update tblClaimLawsuitStatusTracking
		set  IsPrimaryStatus = 0
		where ClaimLawsuitID = @ClaimLawsuitID
		
		
		insert into tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, StatusNotes, ProcessedDate)
		select @ClaimLawsuitID, @DismissalTypeID, @DismissalDate, 1, 'Updated by upload of documentID ' + convert(varchar(500),@DocumentID) + ' by ' + @EmailAddress, convert(date,GETDATE())
	END
	
	--Insert record into tblMultiClaimantDocument to link document to individual claimant
	INSERT INTO tblMultiClaimantDocument (DocumentID, ClaimantPersonalInfoID, ClaimLawsuitID)
	SELECT @DocumentID, c.ClaimantPersonalInfoID, @ClaimLawsuitID
	FROM tblClaimLawsuit cl
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryInsDismissal] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryInsDismissal] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_LAWSUIT_qryInsDismissal] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryInsDismissal] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryInsDismissal] TO [power_user]
GO
