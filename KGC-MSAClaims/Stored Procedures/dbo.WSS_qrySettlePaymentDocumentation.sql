SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_qrySettlePaymentDocumentation] (@ClaimID int)
as

--DECLARE @ClaimID int
--SET @ClaimID = 1

Select pd.ExecutedReleaseDate, PD.ReleaseTypeID, pd.MedicareFullyDocumented, pd.MedicareReported,
	pd.SettlePaymentComments, coalesce(sd.ClaimantIDed, 0) as ClaimantIDed, coalesce(sd.CompServ, 0) as CompServ, 
		coalesce(sd.DODCert,0) as DODCert, coalesce(sd.DxMedicals, 0) as DxMedicals, 
		coalesce(sd.EmployExpoHist, 0) as EmployExpoHist, coalesce(sd.EstatePapers, 0) as EstatePapers,
			coalesce(sd.ProductID,0) as ProductID, coalesce(pd.SetPymtID, 0) as SetPymtID, 
			coalesce(sd.SettlementConfirmation,0) as SettlementConfirmation , 
	cpi.DeceasedDate, COALESCE(ReleaseType, '') as ReleaseType, ReleaseRqstDate, ReleaseToPC, COALESCE(Release, 0) AS Release, 
	COALESCE(MedicareReq, 0) AS MedicareReq, COALESCE(SettlementAuth, 0) AS SettlementAuth
From tblSettlePaymentDocumentation pd
LEFT JOIN tblSettlementDocumentation sd ON sd.setpymtID=PD.SetPymtID
LEFT JOIN tblReleaseType rt ON pd.ReleaseTypeID = rt.ReleaseTypeID
LEFT JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
LEFT JOIN tblclaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
Where pd.ClaimLawsuitID = (SELECT DISTINCT ClaimLawsuitID FROM tblClaimLawsuit WHERE ClaimID = @ClaimID AND COALESCE(IsPrimaryForClaim, 0) = 1)

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySettlePaymentDocumentation] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qrySettlePaymentDocumentation] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySettlePaymentDocumentation] TO [power_user]
GO
