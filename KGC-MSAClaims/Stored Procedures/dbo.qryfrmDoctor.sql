SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmDoctor]
as

Select DoctorID, case when DoctorMiddleName is NULL then DoctorLastName + ', ' + coalesce(DoctorFirstName,'null') else DoctorLastName + ', ' + coalesce(DoctorFirstName,'null')  + ' ' + DoctorMiddleName end as DoctorName, DirtyDoctorFlag
From tblDoctor
Order By DoctorName

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDoctor] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDoctor] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmDoctor] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDoctor] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDoctor] TO [power_user]
GO
