SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[qryrptCarrierNoticePolicyAttachment]
(@carrierid varchar(200) = NULL, @insprog int = null)
as
set nocount on

--TEST VARS
--DECLARE @carrierid varchar(200) = NULL
--DECLARE @insprog int = 0
--END TEST VARS

DECLARE @maxinsprog int
Declare @MAXcarrierid int
SELECT @MAXcarrierid = MAX(carrierid) from tblCarrier
SELECT @maxInsProg = MAX(insuranceprogramid) from tblPolicy

SELECT carriername, PolicyNum, min(PStartDate) 
AS MinStartDate, max(PEndDate) as MaxEndDate 
FROM tblpolicy p 
INNER JOIN tblpolicydates pd ON pd.PolicyID = p.PolicyID 

INNER JOIN tblcarrier c ON c.carrierID = p.CarrierID 

Where c.CarrierID >= COALESCE(@carrierID, 0) AND c.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID)
AND coalesce(p.InsuranceProgramID, 0) >= coalesce(@insprog, 0) and coalesce(p.InsuranceProgramID, 0)  <= coalesce(nullif(@insprog, 0), @maxinsprog)

GROUP BY carriername, 
PolicyNum 
ORDER BY carriername


Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCarrierNoticePolicyAttachment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCarrierNoticePolicyAttachment] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptCarrierNoticePolicyAttachment] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCarrierNoticePolicyAttachment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCarrierNoticePolicyAttachment] TO [power_user]
GO
