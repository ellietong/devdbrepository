SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmState]
As

SET NOCOUNT ON

SELECT StateID, State, FullNameState, StateDiscount
FROM tblState
ORDER BY State

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmState] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmState] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmState] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmState] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmState] TO [power_user]
GO
