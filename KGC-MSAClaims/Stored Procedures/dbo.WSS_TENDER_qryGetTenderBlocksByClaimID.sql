SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryGetTenderBlocksByClaimID]
(@ClaimID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @StartDate date
	DECLARE @MaterialID int
	
	SELECT DISTINCT 
	@MaterialID  = MaterialID, @StartDate = MIN(EmpStartDate)
	FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	LEFT JOIN tblClaimantEmployment ce ON c.ClaimantPersonalInfoID = ce.ClaimantPersonalInfoID
	WHERE c.ClaimID = @ClaimID
	GROUP BY c.ClaimID, MaterialID


    IF(@StartDate IS NULL)
	BEGIN
		--hardcoded for 1983 case -- no specific date but year = 1983
		SELECT @ClaimID, 3
	END
	ELSE
	BEGIN
		SELECT @ClaimID, TenderBlockID FROM tblTenderRules tr
				WHERE @StartDate BETWEEN tr.StartDate AND tr.EndDate 	
				AND  AllowAsbestos = (SELECT ContainsAsbestos FROM tblMaterial WHERE MaterialID = @MaterialID)
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryGetTenderBlocksByClaimID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryGetTenderBlocksByClaimID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryGetTenderBlocksByClaimID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryGetTenderBlocksByClaimID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryGetTenderBlocksByClaimID] TO [power_user]
GO
