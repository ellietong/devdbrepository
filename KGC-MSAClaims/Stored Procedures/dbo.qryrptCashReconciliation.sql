SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptCashReconciliation] (@CurrentStatement MONEY,
													@LastStatement MONEY,
													@CurrentCashActivity MONEY,
													@MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime)

As
Set NoCount On

--Declare @MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime, @CurrentStatement MONEY, @LastStatement MONEY, @CurrentCashActivity MONEY
--Set @MatterID = 1
--Set @BalanceStartDate = '6/1/2011'
--Set @BalanceEndDate = '6/30/2011'
--SET @CurrentStatement = 980596.81
--SET @LastStatement = 779522.08
--SET @CurrentCashActivity = 682196.81

--Declare @StartingBalance money
--Set @StartingBalance = (
--Coalesce((Select SUM(CashActivityAmount) From tblMiscCashActivity mca 
--Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID 
--where CashActivityDate >= '1/1/2010' and CashActivityDate < @BalanceStartDate and MatterID = @MatterID),0)
--+
--Coalesce((Select SUM(SourceAmount)*-1 From tblCheck c
--Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
--Where SourceDate >= '1/1/2010' and SourceDate < @BalanceStartDate and MatterID = @MatterID),0)
--)

Declare @Matter nvarchar(250)
Set @Matter = (Select Matter from tblMatter where MatterID = @MatterID)

Declare @Detail table (Matter nvarchar(250), FundingType nvarchar(250), FundingSource nvarchar(250), SourceDate datetime, SourceNumber nvarchar(250), 
Indemnity money, Defense money, MiscFees money, CashReceipts money, Comment nvarchar(Max), CheckStatus DATETIME, SectionHeader NVARCHAR(250),
AmountReconciled MONEY, AmountOutstanding MONEY)
Insert Into @Detail

--Defense
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, 0, SourceAmount*-1 as SourceAmount, 0, 0, Firm, CheckClearedDate, 'Reconciliation Detail', Null, Null
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentDefense pd on pd.CheckID = c.CheckID
	Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
	Inner Join tblDefenseInvoiceFirm dif on dif.FirmID = di.FirmID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (CheckClearedDate >= @BalanceStartDate and CheckClearedDate <= @BalanceEndDate) AND c.MatterID = @MatterID AND c.FundingTypeID = 1
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, Firm, CheckClearedDate

Union ALL

--Settlement
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1 as SourceAmount, 0, 0, 0, PlaintiffCounsel, CheckClearedDate, 'Reconciliation Detail', Null, Null
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
	Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (CheckClearedDate >= @BalanceStartDate and CheckClearedDate <= @BalanceEndDate) AND c.MatterID = @MatterID AND c.FundingTypeID = 2
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, PlaintiffCounsel, CheckClearedDate

Union ALL

Select Matter, CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, CashActivityAmount, 0, CashActivityNotes, Null, 'Reconciliation Detail', Null, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @BalanceStartDate and CashActivityDate <= @BalanceEndDate) AND mca.MatterID = @MatterID AND CashActivityAmount < 0

Union ALL

Select Matter, CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, 0, CashActivityAmount, CashActivityNotes, Null, 'Reconciliation Detail', Null, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @BalanceStartDate and CashActivityDate <= @BalanceEndDate) AND mca.MatterID = @MatterID AND CashActivityAmount > 0

union all

--Defense
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, 0, SourceAmount*-1 as SourceAmount, 0, 0, Firm, CheckClearedDate, 'Outstanding Detail', Null, Null
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentDefense pd on pd.CheckID = c.CheckID
	Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
	Inner Join tblDefenseInvoiceFirm dif on dif.FirmID = di.FirmID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (CheckClearedDate is null or CheckClearedDate = '') AND c.MatterID = @MatterID AND c.FundingTypeID = 1
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, Firm, CheckClearedDate

Union ALL

--Settlement
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1 as SourceAmount, 0, 0, 0, PlaintiffCounsel, CheckClearedDate, 'Outstanding Detail', Null, Null
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
	Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (CheckClearedDate is null or CheckClearedDate = '') AND c.MatterID = @MatterID AND c.FundingTypeID = 2
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, PlaintiffCounsel, CheckClearedDate

UPDATE @Detail
SET AmountReconciled = (SELECT SUM(Indemnity + Defense + MiscFees + CashReceipts) from @Detail where SectionHeader = 'Reconciliation Detail'), 
AmountOutstanding = (SELECT SUM(Indemnity + Defense + MiscFees + CashReceipts) from @Detail where SectionHeader = 'Outstanding Detail')

Select *, @BalanceStartDate as BalanceStartDate, @BalanceEndDate as BalanceEndDate, @CurrentStatement AS CurrentStatement, @LastStatement AS LastStatement, @CurrentCashActivity AS CurrentCashActivity
from @Detail Order By SourceDate

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashReconciliation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashReconciliation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashReconciliation] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qryrptCashReconciliation] TO [Billing_User]
GRANT ALTER ON  [dbo].[qryrptCashReconciliation] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashReconciliation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashReconciliation] TO [power_user]
GO
