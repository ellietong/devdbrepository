SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptToxicTortReports_SpendByYear](@DataDate DATETIME) 
AS
SET NOCOUNT ON

--DECLARE @DataDate DATETIME
--Set @DataDate = '6/18/2014'

SELECT COALESCE(a.yr,b.yr) AS YR, a.InsurableSettlement, a.UninsurableSettlement, a.TotalSettlement, b.InsurableDefense, b.UninsurableDefense, b.TotalDefense, a.InsurableSettlement+b.InsurableDefense AS TotalInsurable, a.UninsurableSettlement+b.UninsurableDefense AS TotalUninsurable, a.TotalSettlement+b.TotalDefense AS OverallTotal,
	@DataDate AS DD

FROM

(SELECT Year(PostingDate) AS yr,
		COALESCE( SUM( CASE WHEN InsurableFlag = 1 THEN SettlementAmount END),0) AS InsurableSettlement,
		COALESCE( SUM( CASE WHEN InsurableFlag = 0 THEN SettlementAmount END),0) AS UninsurableSettlement,
		COALESCE( SUM( SettlementAmount ), 0) AS TotalSettlement
FROM
		tblClaimantSettlement CS
			LEFT JOIN tblClaimLawsuit CL ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
			LEFT JOIN tblClaim C ON C.ClaimID = CL.ClaimID
			LEFT JOIN tblClaimantPersonalInfo CPI ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
			LEFT JOIN tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID
			LEFT JOIN tblMaterial M ON M.MaterialID = A.MaterialID
			LEFT JOIN m_tblClaimType_StandardMSA CTS ON CTS.MaterialID = M.MaterialID
			LEFT JOIN dbo.m_tblInsurableClaims IC ON IC.ClaimID = C.ClaimID

WHERE PostingDate >= '1/1/2009' AND PostingDate <= @DataDate AND M.MaterialID != 10

GROUP BY Year(PostingDate)
) AS a

FULL OUTER JOIN
		
(SELECT 	Year(PeriodEnd) AS yr,	
		
		COALESCE( SUM( CASE WHEN InsurableFlag = 1 THEN Costs + Fees END),0) AS InsurableDefense,
		COALESCE( SUM( CASE WHEN InsurableFlag = 0 THEN Costs + Fees END),0) AS UninsurableDefense,
		COALESCE( SUM( Costs + Fees ), 0) AS TotalDefense

FROM

		tblDefenseInvoice DI
			LEFT JOIN tblDefInvClaimantDetail DICD ON DICD.DefenseInvoiceID = DI.DefenseInvoiceID
			LEFT JOIN tblClaim C ON C.ClaimID = DICD.ClaimID
			LEFT JOIN tblClaimantPersonalInfo CPI ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
			LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
			LEFT JOIN tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID
			LEFT JOIN tblMaterial M ON M.MaterialID = A.MaterialID
			LEFT JOIN m_tblClaimType_StandardMSA CTS ON CTS.MaterialID = M.MaterialID
			LEFT JOIN dbo.m_tblInsurableClaims IC ON IC.ClaimID = C.ClaimID

WHERE PeriodEnd >= '1/1/2009' AND PeriodEnd <= @DataDate AND COALESCE(NCCReferenceID, 'Unknown') NOT LIKE '%LIT%'
			AND COALESCE(M.MaterialID, 100) != 10

GROUP BY Year(PeriodEnd)

) AS b

ON a.yr = b.yr
	

GROUP BY COALESCE(a.yr,b.yr), a.InsurableSettlement, a.UninsurableSettlement, a.TotalSettlement,
a.InsurableSettlement, a.UninsurableSettlement, a.TotalSettlement, b.InsurableDefense, b.UninsurableDefense, b.TotalDefense, a.InsurableSettlement+b.InsurableDefense,
a.UninsurableSettlement+b.UninsurableDefense, a.TotalSettlement+b.TotalDefense
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptToxicTortReports_SpendByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptToxicTortReports_SpendByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptToxicTortReports_SpendByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptToxicTortReports_SpendByYear] TO [power_user]
GO
