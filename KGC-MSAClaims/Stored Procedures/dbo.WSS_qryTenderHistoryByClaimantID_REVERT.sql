SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryTenderHistoryByClaimantID_REVERT]
(@ClaimantPersonalInfoID bigint, @emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT TenderHistoryID, TenderBlockName, th.TenderBlockID, TenderStatus, EffectiveDate, d.DocumentURL, th.TenderStatusID, th.DocumentID, DocumentURL,
	th.DateApproved, th.ApprovedBy, ServiceDate, TenderType
	FROM tblTenderHistory th 
	INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
	INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
	INNER JOIN tblTenderStatus ts ON th.TenderStatusID = ts.TenderStatusID
	LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblTenderType tt ON th.TenderTypeID = tt.TenderTypeID
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	ORDER BY ServiceDate DESC
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderHistoryByClaimantID_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderHistoryByClaimantID_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryTenderHistoryByClaimantID_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderHistoryByClaimantID_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderHistoryByClaimantID_REVERT] TO [power_user]
GO
