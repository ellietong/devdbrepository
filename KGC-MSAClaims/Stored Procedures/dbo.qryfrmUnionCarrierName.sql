SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmUnionCarrierName]
AS
SET NOCOUNT ON

SELECT CarrierID, CarrierName
FROM tblCarrier
where carrierid in (select carrierid from tblpolicy)
UNION 

SELECT  0, '<<All Carriers>>' AS CarrierName 
FROM tblCarrier

ORDER BY CarrierName

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionCarrierName] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionCarrierName] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmUnionCarrierName] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionCarrierName] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionCarrierName] TO [power_user]
GO
