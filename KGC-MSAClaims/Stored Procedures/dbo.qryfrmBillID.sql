SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmBillID]
As
SELECT b.BillID, bg.BillGroupName + ' ' + CONVERT (varchar, b.BillDate, 107) AS BillName
FROM dbo.tblBill b INNER JOIN dbo.tblBillGroup bg ON b.BillGroupID = bg.BillGroupID
Group By b.BillID, bg.BillGroupName, b.BillDate
Order by b.BillID
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmBillID] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmBillID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmBillID] TO [power_user]
GO
