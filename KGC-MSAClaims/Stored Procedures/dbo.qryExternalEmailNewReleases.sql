SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryExternalEmailNewReleases]
AS
BEGIN

/*Query run daily to send an alert email for new Releases. - DRE*/

--Declare and set email inputs.
DECLARE @Subject NVARCHAR (100)
DECLARE @LogoHeader NVARCHAR (2000)
SET @Subject = 'MSA Claims: New Releases Uploaded ' + CONVERT(varchar, GETDATE(), 107)
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"><img src="http://www.kcic.com/i/banner_home.jpg" alt="logo"/>'

--Declare and set desired email time coverage.
DECLARE @Today4PM DATETIME
DECLARE @LastWeek4PM DATETIME
SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')
SET @LastWeek4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')

--Declare flag used to stop the email from sending if no releases have been uploaded. (added by CY, 8/30/2013)
DECLARE @SendFlag int
SET @SendFlag = 1

--Create a temp table for Releases which have been uploaded in the past week.
DECLARE @NewReleasesTable TABLE
(
DocumentID INT,
DocumentTitle NVARCHAR (MAX),
LastName NVARCHAR (50),
FirstName NVARCHAR (50),
NCCReferenceID NVARCHAR (15),
ClaimantPersonalInfoID BIGINT,
SettlementDate DATE,
SettlementAmount MONEY,
LawsuitState NVARCHAR (4),
NationalPlaintiffCounsel NVARCHAR (1000),
DefenseCounsel NVARCHAR (255),
UploadDate DATE,
ClaimLawsuitStatus NVARCHAR (500),
ClaimLawsuitID BIGINT,
ExposureDate NVARCHAR (25),
ClaimType NVARCHAR (150),
ChargeAcct NVARCHAR (10),
Username NVARCHAR (300)
)

--Populate the temp table with Releases from the past week.
INSERT INTO @NewReleasesTable
	SELECT
		d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/MSA/cp/Pages/DocumentProfile.aspx?ItemId='
		+ COALESCE(CONVERT(nvarchar(50), d.DocumentID), '')
		+ '">' + COALESCE(d.DocumentTitle, '') + '</a>'
		AS DocumentTitle,
		cpi.LastName,
		cpi.FirstName,
		NCCReferenceID,
		cpi.ClaimantPersonalInfoID,
		cs.PostingDate AS SettlementDate,
		REPLACE(CONVERT(nvarchar(20), CAST(cs.SettlementAmount AS money), 1), '.00', ''), --cs.SettlementAmount,
		s.[state],
		plaintiffcounsel,
		DefenseCounsel, --,pca.PlaintiffCounsel,
		COALESCE(MAX(d.DateCreated), '1/1/1900') AS UploadDate,
		cls.ClaimLawsuitStatus,
		cl.ClaimLawsuitID,
		CONVERT(nvarchar(50), dbo.fnEarliestExposure(cpi.ClaimantPersonalInfoID), 101) AS ExposureDate,
		Material AS ClaimType,
		CASE
		--updated accounting code for costs outside coverage from 231060 to 900070 per Gleason on 3/11/2014
			WHEN dbo.fnEarliestExposure(cpi.ClaimantPersonalInfoID) > '1/1/1985' AND a.MaterialID = 1 THEN 900070
			WHEN dbo.fnEarliestExposure(cpi.ClaimantPersonalInfoID) > '1/1/1986' THEN 900070 ELSE 231030
		END AS ChargeAcct,
		COALESCE(CreatedBy, '') AS Username
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatus cls ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
	LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
	LEFT JOIN tblState s ON s.StateID = l.StateID
	LEFT JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.NationalPlaintiffCounselAddressID
	LEFT JOIN tblClaimantSettlement cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	INNER JOIN tblMaterial m ON a.MaterialID = m.MaterialID
	WHERE d.DateCreated BETWEEN @LastWeek4PM AND @Today4PM
	AND d.DetailDocumentTypeID IN (31, 32, 454, 456, 466, 468, 469, 472, 488, 496, 498, 500, 511, 512, 515, 518, 519, 520, 522, 524)
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	AND IsPrimaryStatus = 1
	AND CreatedBy NOT LIKE '%Historical%'
	GROUP BY	cl.ClaimLawsuitID,
				LastName,
				d.documentID,
				d.DocumentTitle,
				FirstName,
				cpi.ClaimantPersonalInfoID,
				ClaimLawsuitStatus,
				cl.LawsuitID,
				CreatedBy,
				NCCReferenceID,
				cs.PostingDate,
				cs.SettlementAmount,
				s.[state],
				DefenseCounsel,
				PlaintiffCounsel,
				Material,
				a.MaterialID
SELECT
	*
FROM @NewReleasesTable

--Create a temp table to store one or many associated claimants for each doc.					
DECLARE @ClaimantPerDoc TABLE
(
DocumentID INT,
ClaimantList NVARCHAR (MAX)
)

--Declare variables for iterating over the docs and filling the document temp table.
DECLARE @DocumentID INT,
@LastDocID INT,
@Claimant NVARCHAR (MAX),
@ClaimantID BIGINT,
@ClaimantList NVARCHAR (MAX) = '',
@NCCRefID NVARCHAR (15)

--Declare and run a cursor over the temp Releases table, populating the temp document table.		
DECLARE Cur_Releases CURSOR Local Fast_Forward
FOR SELECT
	DocumentID,
	COALESCE(LTRIM(RTRIM(LastName)), '') + ',&nbsp;' + COALESCE(LTRIM(RTRIM(FirstName)), ''),
	ClaimantPersonalInfoID,
	LTRIM(RTRIM(NCCReferenceID))
FROM @NewReleasesTable

OPEN Cur_Releases
FETCH NEXT FROM Cur_Releases INTO @DocumentID, @Claimant, @ClaimantID, @NCCRefID
WHILE @@FETCH_STATUS = 0
BEGIN
SET @ClaimantList = COALESCE(@ClaimantList, '')
+ '<ul style="margin:0; padding:0;"> <a href="https://www.kcicextranet.com/clients/MSA/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
+ CONVERT(nvarchar(50), @ClaimantID) + '">'
+ COALESCE(@Claimant, '') + COALESCE('&nbsp;(' + @NCCRefID + ')', '') + '</a></ul>'
SET @LastDocID = @DocumentID

FETCH NEXT FROM Cur_Releases INTO @DocumentID, @Claimant, @ClaimantID, @NCCRefID

IF @DocumentID < > @LastDocID OR @@FETCH_STATUS < > 0
BEGIN
INSERT INTO @ClaimantPerDoc
	SELECT
		@LastDocID,
		@ClaimantList

SET @ClaimantList = ''
END
END
CLOSE Cur_Releases
DEALLOCATE Cur_Releases
SELECT
	*
FROM @ClaimantPerDoc
--Declare and populate the body of the email
DECLARE @BodyMessage NVARCHAR (MAX)
--Set initial HTML markup for the email body. NOTE: Gmail does not render CSS style placed in <style> tags (must be inline).
SET @BodyMessage = '<HTML>
								<BODY>'
+ @LogoHeader
DECLARE @LastLawsuitID NVARCHAR (250) = ''
--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
IF (SELECT
	COUNT(DocumentTitle)
FROM @NewReleasesTable) > 0
BEGIN
SET @BodyMessage = @BodyMessage
+ '<H4>The following Releases have been processed in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage
+ N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">'
SET @BodyMessage = @BodyMessage
+ N'<tr>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Document Title</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Linked Claimant(s)<br/>(NCC Reference ID)</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Settlement Date</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Settlement Amount</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">State</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">National Plaintiff Counsel</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Defense Counsel</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Claim Type</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Exposure Date</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Charge Acct</th>
						</tr>'
DECLARE @DocTitle NVARCHAR (249),
@clClaimantList NVARCHAR (MAX),
@UploadDate NVARCHAR (300),
@SettlementDate NVARCHAR (300),
@SettlementAmount NVARCHAR (300),
@LawsuitState NVARCHAR (4),
@Username Nvarchar (300),
@NationalPlaintiffCounsel NVARCHAR (300),
@DefenseCounsel NVARCHAR (255),
@ClaimType NVARCHAR (150),
@ExposureDate NVARCHAR (25),
@ChargeAcct NVARCHAR (10)
DECLARE Cur_Docs CURSOR
FOR SELECT
	DocumentTitle,
	cl.ClaimantList,
	CAST(MAX(UploadDate) AS varchar(MAX)),
	(SELECT
		COALESCE(CONVERT(nvarchar(25), SettlementDate, 101), ' '))
	, --COALESCE(CAST(MAX(SettlementDate) AS VARCHAR(MAX)),'Not Entered'),
	CASE
		WHEN SettlementAmount IS NULL THEN '' ELSE '$' + COALESCE(REPLACE(CONVERT(nvarchar(20), CAST(SettlementAmount AS money), 1), '.00', ''), '')
	END, --COALESCE(CAST(MAX(SettlementAmount) AS VARCHAR(MAX)),'-'),
	LawsuitState,
	Username,
	NationalPlaintiffCounsel,
	DefenseCounsel,
	ClaimType,
	ExposureDate,
	ChargeAcct
FROM @NewReleasesTable ndt
INNER JOIN @ClaimantPerDoc cl ON ndt.DocumentID = cl.DocumentID
GROUP BY	cl.DocumentID,
			ndt.DocumentTitle,
			cl.ClaimantList,
			SettlementDate,
			SettlementAmount,
			LawsuitState,
			ndt.Username,
			NationalPlaintiffCounsel,
			DefenseCounsel,
			ClaimType,
			ExposureDate,
			ChargeAcct
ORDER BY cl.DocumentID

OPEN Cur_Docs
FETCH NEXT FROM Cur_Docs INTO @DocTitle, @clClaimantList,
@UploadDate, @SettlementDate, @SettlementAmount, @LawsuitState, @Username, @NationalPlaintiffCounsel, @DefenseCounsel, @ClaimType, @ExposureDate, @ChargeAcct
WHILE @@FETCH_STATUS = 0
BEGIN
SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @DocTitle + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @clClaimantList + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @SettlementDate + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @SettlementAmount + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @LawsuitState + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ COALESCE(@NationalPlaintiffCounsel, 'Unknown') + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ COALESCE(@DefenseCounsel, 'Unknown') + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @ClaimType + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ COALESCE(@ExposureDate, ' ') + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @ChargeAcct + ' </td>
							'

SET @BodyMessage = @BodyMessage + '</td>
														</tr>'
FETCH NEXT FROM Cur_Docs INTO @DocTitle,
@clClaimantList, @UploadDate,
@SettlementDate, @SettlementAmount, @LawsuitState, @UserName, @NationalPlaintiffCounsel, @DefenseCounsel, @ClaimType, @ExposureDate, @ChargeAcct
END
CLOSE Cur_Docs
DEALLOCATE Cur_Docs
SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'
END
ELSE
BEGIN
SET @SendFlag = 0
SET @BodyMessage = @BodyMessage
+ '<H4>No Releases have been uploaded in the last 24 hours. </H4>'
END

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--KCIC address (disclaimer applied by mail server)
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
												733 10th Street, NW, Suite 3001<br>
												Washington, DC 20001<br>
												(t) 202-772-2300<br>
												(f) 202-772-2333
											</BODY>
										</HTML>'
--QC the message	
PRINT @BodyMessage

--Execute stored procedure to send the email to that firm, if the SendFlag is still flagged
IF @SendFlag = 1
BEGIN
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'Christine.Krolicki@msasafety.com',
								@copy_recipients = 'Stephanie.Sciullo@msasafety.com; William.Berner@msasafety.com; JSchramm@reedsmith.com',
								@blind_copy_recipients='potterm@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'
END
END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewReleases] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewReleases] TO [base_user]
GRANT ALTER ON  [dbo].[qryExternalEmailNewReleases] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewReleases] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewReleases] TO [power_user]
GO
