SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_qryProduct]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SELECT ProductID, COALESCE(ProductManufacturer, 'UNSPECIFIED') + ' - ' + COALESCE(ProductType, 'UNSPECIFIED') + ' - ' + COALESCE(ProductName, 'UNSPECIFIED') AS Product
	FROM
	tblProduct p
	LEFT Join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
	LEFT Join tblProductManufacturer pm on pm.ProductManufacturerID = p.ProductManufacturerID
	LEFT Join tblProductName pn on pn.ProductNameID = p.ProductNameID
	LEFT Join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
	WHERE P.ProductManufacturerID = 1
	UNION
	SELECT NULL, '' As Product
	ORDER BY COALESCE(ProductManufacturer, 'UNSPECIFIED') + ' - ' + COALESCE(ProductType, 'UNSPECIFIED') + ' - ' + COALESCE(ProductName, 'UNSPECIFIED')
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryProduct] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryProduct] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryProduct] TO [power_user]
GO
