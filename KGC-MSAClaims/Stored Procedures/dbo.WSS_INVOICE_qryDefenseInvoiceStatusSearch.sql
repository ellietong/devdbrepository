SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_INVOICE_qryDefenseInvoiceStatusSearch]
AS 
    SET NOCOUNT ON

    SELECT  statusid ,
            status
    FROM    tblDefenseInvoiceStatus
    WHERE   StatusID <> 5
    UNION
	--SELECT null as StatusID, '<< All Status >>' As status
	--UNION
    SELECT  NULL ,
            ''
    ORDER BY Status
    RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryDefenseInvoiceStatusSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryDefenseInvoiceStatusSearch] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_INVOICE_qryDefenseInvoiceStatusSearch] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryDefenseInvoiceStatusSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryDefenseInvoiceStatusSearch] TO [power_user]
GO
