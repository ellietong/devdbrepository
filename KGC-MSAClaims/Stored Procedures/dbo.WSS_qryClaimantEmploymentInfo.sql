SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryClaimantEmploymentInfo]
@ClaimantPersonalInfoID bigint=null
AS
Select ce.ClaimantEmploymentID, 
	ce.ClaimantPersonalInfoID, 
	o.Occupation, 
	Jobsite, 
	Comments, 
	LEFT(Comments, 100) + '...' AS Summary,
	EmpStartDate, 
	EmpEndDate, 
	EarliestExposureDate, 
	LatestExposureDate, 
	IsPrimary, 
	ProductManufacturer, 
	pt.ProductType,
	ProductBrand,
	pn.ProductName
From tblClaimantEmployment ce
left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
left join tblProduct p on p.ProductID = cp.ProductID
left join tblProductName pn on pn.ProductNameID=p.ProductNameID
left join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
left join tblProductManufacturer pm on pm.ProductManufacturerID=p.ProductManufacturerID
left join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
left join tblOccupation o ON ce.OccupationID = o.OccupationID
WHERE ce.ClaimantPersonalInfoID=@ClaimantPersonalInfoID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantEmploymentInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantEmploymentInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimantEmploymentInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantEmploymentInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantEmploymentInfo] TO [power_user]
GO
