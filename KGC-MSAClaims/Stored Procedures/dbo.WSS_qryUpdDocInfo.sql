SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdDocInfo]
(@DocumentID int, @DocumentTypeID int, @DocumentSourceID int, @Accept bit, @EmailAddress nvarchar(250), @FirmID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
IF @Accept =1 AND RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'
BEGIN
	UPDATE tblDocuments
	SET DetailDocumentTypeID = @DocumentTypeID,
		DocumentSourceID = @DocumentSourceID,
		FirmID = @FirmID,
		DateModified = GETDATE(),
		ModifiedBy = @EmailAddress,
		--DismissalProcessDate = CASE WHEN @DocumentTypeID = 16 THEN GETDATE() ELSE NULL END,
		--DismissalClaimLawsuitStatusID = CASE WHEN @DocumentTypeID = 16 THEN @DismissalTypeID ELSE NULL END,
		--DismissalDate = @DismissalDate,
		Accepted = CASE WHEN COALESCE(Accepted, 0) = 1 THEN Accepted WHEN @Accept = 0 AND COALESCE(Accepted, 0) = 0 THEN NULL ELSE 1 END,
		AcceptedBy = Case WHEN AcceptedBy IS NOT NULL THEN AcceptedBy WHEN @Accept = 1 THEN @EmailAddress ELSE NULL END,
		DateAccepted = CASE WHEN DateAccepted IS NOT NULL THEN DateAccepted WHEN @Accept = 1 THEN GETDATE() ELSE NULL END,
		--Add QCBy and Date to the query
		QCBy = Case WHEN QCBy IS NOT NULL THEN QCBy WHEN @Accept = 1 THEN @EmailAddress ELSE NULL END,
		QCDate = CASE WHEN QCDate IS NOT NULL THEN QCDate WHEN @Accept = 1 THEN GETDATE() ELSE NULL End
		
	WHERE DocumentID = @DocumentID
	UPDATE tblMultiClaimantDocument
	SET 
		QCBy = @EmailAddress, 
		QCDate = GETDATE()
	WHERE DocumentID = @DocumentID
END
ELSE
UPDATE tblDocuments
	SET DetailDocumentTypeID = @DocumentTypeID,
		DocumentSourceID = @DocumentSourceID,
		FirmID = @FirmID,
		DateModified = GETDATE(),
		ModifiedBy = @EmailAddress,
		--DismissalProcessDate = CASE WHEN @DocumentTypeID = 16 THEN GETDATE() ELSE NULL END,
		--DismissalClaimLawsuitStatusID = CASE WHEN @DocumentTypeID = 16 THEN @DismissalTypeID ELSE NULL END,
		--DismissalDate = @DismissalDate,
		Accepted = CASE WHEN COALESCE(Accepted, 0) = 1 THEN Accepted WHEN @Accept = 0 AND COALESCE(Accepted, 0) = 0 THEN NULL ELSE 1 END,
		AcceptedBy = Case WHEN AcceptedBy IS NOT NULL THEN AcceptedBy WHEN @Accept = 1 THEN @EmailAddress ELSE NULL END,
		DateAccepted = CASE WHEN DateAccepted IS NOT NULL THEN DateAccepted WHEN @Accept = 1 THEN GETDATE() ELSE NULL END
		
	WHERE DocumentID = @DocumentID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdDocInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdDocInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdDocInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdDocInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdDocInfo] TO [power_user]
GO
