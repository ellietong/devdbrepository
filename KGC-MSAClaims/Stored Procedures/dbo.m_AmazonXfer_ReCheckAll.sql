SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_AmazonXfer_ReCheckAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT DocumentID, DocumentURL, REPLACE(REPLACE(DocumentTitle, '\', ''), '/', '') AS DocumentTitle,
			d.DetailDocumentTypeID,REPLACE(REPLACE(ClaimantDocumentType, '\', '_'), '/', '_') AS ClaimantDocumentType,
			 AmazonKey
    FROM tblDocuments d
    INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
   where AmazonKey not like '%' + Convert(nvarchar(250),DocumentID) + '%'
AND DocumentURL <> '#N/A'
     ORDER BY DocumentID ASC
  END
  --643 documents then failure
--select * from tblDocuments where AmazonKey not like '%' + Convert(nvarchar(250),DocumentID) + '%'
--AND DocumentURL <> '#N/A'
GO
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonXfer_ReCheckAll] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_AmazonXfer_ReCheckAll] TO [base_user]
GRANT ALTER ON  [dbo].[m_AmazonXfer_ReCheckAll] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonXfer_ReCheckAll] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_AmazonXfer_ReCheckAll] TO [power_user]
GO
