SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryCreateTenders]
(@ComplaintDocumentID bigint, @emailaddress nvarchar(500))
AS
BEGIN
	SET NOCOUNT ON;

	--NOTE: ALL code commented out due to transitioning to new tender process (CY, 7/22/2013)

 --   --Test vars
 --   --DECLARE @emailaddress nvarchar(500) = 'crookc@kcicllc.com'
 --   DECLARE @ClaimantPersonalInfoIDs bigint_list_tbltype
 --   --DECLARE @ComplaintDocumentID bigint = 526069
 --   DECLARE @CreatedBy nvarchar(250)
 --   SET @CreatedBy = (SELECT RIGHT(createdby, 11) AS CreatedBy FROM tblDocuments WHERE DocumentID = @ComplaintDocumentID)
    
    
 -- IF (@CreatedBy = 'kcicllc.com')
 --   --Get all the CPIs for the Complaint
 --   INSERT INTO @ClaimantPersonalInfoIDs
 --   SELECT DISTINCT ClaimantPersonalInfoID FROM tblMultiClaimantDocument WHERE DocumentID = @ComplaintDocumentID
		
	----BEGIN TRAN
	----Get the ID of the to-be-created TenderHistory record
	----DECLARE @THID bigint = (SELECT IDENT_CURRENT('tblTenderHistory')+IDENT_INCR('tblTenderHistory'))
	
	----Declare place to store tender blocks
	--DECLARE @TenderBlockID int
	--DECLARE @TenderBlocks TABLE (ClaimID bigint, TenderBlockID int)
	
	----Get Unique tender blocks and insert into @TenderBlocks:
	--DECLARE @ClaimID bigint
	
	----Declare cursor to get primary Claims of all associated claimants, so we can check unique number of tender blocks
	--DECLARE curs_TenderBlocks Cursor FOR 
	--SELECT DISTINCT 
	--c.ClaimID
	--FROM tblClaim c 
	--INNER JOIN @ClaimantPersonalInfoIDs cpis ON c.ClaimantPersonalInfoID = cpis.n
	--WHERE COALESCE(IsCurrentClaim, 0) = 1
	--GROUP BY c.ClaimID
	
	----Loop through and populate TenderBlocks temp table with all tender blocks by claimID
	--OPEN curs_TenderBlocks
	--	FETCH NEXT FROM curs_TenderBlocks INTO @ClaimID
	--	WHILE @@Fetch_Status = 0
	--	BEGIN
	--		INSERT INTO @TenderBlocks
	--		EXEC WSS_TENDER_qryGetTenderBlocksByClaimID @ClaimID
				
	--		FETCH NEXT FROM curs_TenderBlocks INTO @ClaimID
	--	END

	--CLOSE curs_TenderBlocks 
	--DEALLOCATE curs_TenderBlocks
	
	
	----Loop through unique TenderBlockIDs in @TenderBlocks
	----Declare cursor to get unqiue set of TenderBlockIDs for which to create TenderHistory records
	--DECLARE curs_TenderBlocks Cursor FOR 
	--SELECT DISTINCT 
	--TenderBlockID 
	--FROM @TenderBlocks
	
	--OPEN curs_TenderBlocks
	--	FETCH NEXT FROM curs_TenderBlocks INTO @TenderBlockID
	--	WHILE @@Fetch_Status = 0
	--	BEGIN
			
	--		--For each tender block, insert a tenderHistory record using the complaint documentID supplied (save this TenderHistoryID!)
	--		INSERT INTO tblTenderHistory (TenderBlockID, TenderTypeID, ComplaintDocumentID, DateCreated, SPUsername)
	--		SELECT @TenderBlockID, 1, @ComplaintDocumentID, GETDATE(), @emailaddress
			
	--		--Get the ID of the most recently inserted TenderHistory record
	--		DECLARE @THID bigint = (SELECT IDENT_CURRENT('tblTenderHistory'))
			
	--		--Then Insert tblClaimTenderHistory records for all other ClaimIDs with that tenderBlock
	--		INSERT INTO tblClaimTenderHistory (TenderHistoryID, ClaimID, TenderStatusID, SPUsername)
	--		SELECT @THID, ClaimID, 1, @emailaddress
	--		FROM @TenderBlocks
	--		WHERE TenderBlockID = @TenderBlockID			

	--		--If there is only 1 template for the newly inserted retender record, default the TemplateID to that value
	--		DECLARE @TenderTemplate TABLE(TenderTemplateID int, TemplateDisplayName nvarchar(MAX))
	--		INSERT INTO @TenderTemplate
	--		EXEC [WSS_TENDER_qryTenderTemplateList] @THID
	--		IF(SELECT COUNT(COALESCE(TenderTemplateID, 0)) FROM @TenderTemplate) = 2
	--		BEGIN
	--			UPDATE tblTenderHistory
	--			SET TenderTemplateID = (SELECT TenderTemplateID FROM @TenderTemplate WHERE TenderTemplateID IS NOT NULL)
	--			WHERE TenderHistoryID = @THID
	--		END	
				
	--		FETCH NEXT FROM curs_TenderBlocks INTO @TenderBlockID
	--	END

	--CLOSE curs_TenderBlocks 
	--DEALLOCATE curs_TenderBlocks
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryCreateTenders] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryCreateTenders] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryCreateTenders] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryCreateTenders] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryCreateTenders] TO [power_user]
GO
