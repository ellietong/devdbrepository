SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdTenderBlock_REVERT]
(@TenderHistoryID bigint, @emailaddress nvarchar(250), @TenderBlockID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblTenderHistory
	SET SPUsername = @emailaddress,
		TenderBlockID = @TenderBlockID
	WHERE TenderHistoryID = @TenderHistoryID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdTenderBlock_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdTenderBlock_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdTenderBlock_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdTenderBlock_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdTenderBlock_REVERT] TO [power_user]
GO
