SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailUnacknowledgedTenders]
AS
BEGIN

/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

SET @Subject = 'MSA: Unacknowledged Tenders Over 5 Days Old'
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.

DECLARE @NewTendersTable TABLE (email nvarchar (250), Hash nvarchar (250), TenderBlockName nvarchar (250), ClaimantName nvarchar (50),
EffectiveDate date, TenderContactID int, NCCReferenceID nvarchar (100))
INSERT INTO @NewTendersTable
	SELECT
		Email,
		Hash AS Other,
		TenderBlockName,
		LastName + ' ' + FirstName AS 'Claimant Name',
		EffectiveDate,
		tc.TenderContactID,
		NCCReferenceID
	FROM tblTenderAcknowledgement ta
	INNER JOIN tblTenderHistory th ON ta.TenderHistoryID = th.TenderHistoryID
	INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
	INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
	INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
	INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
	INNER JOIN tblTenderContact tc ON ta.TenderContactID = tc.TenderContactID
	INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	WHERE AcknowledgementDate IS NULL
	AND DateApproved < GETDATE() - 5
	AND ApprovedBy NOT LIKE 'Historical Tender'
	AND th.TenderBlockID NOT IN (2, 8, 9, 10, 11, 16)
	AND TenderTypeID <> 2
	GROUP BY	NCCReferenceID,
				Hash,
				LastName + ' ' + FirstName,
				Email,
				TenderBlockName,
				tc.TenderContactID,
				EffectiveDate




--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

DECLARE @TenderContactAddressID nvarchar (10)

DECLARE TenderContact_Curs CURSOR FOR SELECT
	TenderContactID
--'https://www.kcicextranet.com/clients/msa/cp/Pages/Documents.aspx'
FROM @NewTendersTable ntt
WHERE TenderContactID <> 48 --stockman
GROUP BY TenderContactID


OPEN TenderContact_curs
FETCH NEXT FROM TenderContact_curs INTO @TenderContactAddressID
WHILE @@FETCH_STATUS = 0
BEGIN

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD    {font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000}
TH    {border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	padding:0;}	      
--->
</STYLE>'



--Set table column headings and populate the cells with the Tenders from @NewTendersTable
IF (SELECT
	COUNT(NCCReferenceID)
FROM @NewTendersTable
WHERE TenderContactID = @TenderContactAddressID) > 0
begin
SET @BodyMessage = @BodyMessage + '<H4>The following Tender Letters were uploaded more than 5 days ago and have not yet been acknowledged: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
N'<tr><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Name</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">NCC #</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Tender Block</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Effective Date</th>' +
CAST((SELECT
	'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000"><a href="https://www.kcicextranet.com/_layouts/tenderack.aspx?Hash=' + hash + '">' + ClaimantName + '</a></td>',
	'',
	'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000">' + NCCReferenceID + '</td>',
	'',
	'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000">' + TenderBlockName + '</td>',
	'',
	'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000">' + COALESCE(CONVERT(nvarchar, EffectiveDate, 107), '') + '</td>',
	''
FROM @NewTendersTable ntt
WHERE TenderContactID = @TenderContactAddressID
GROUP BY	NCCReferenceID,
			Hash,
			TenderBlockName,
			EffectiveDate,
			ClaimantName

FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'
end
ELSE
begin
SET @BodyMessage = @BodyMessage + '<H4>There are currently no outstanding Tender Letters awaiting acknowledgement. </H4>'
end



--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

----Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


DECLARE @RecipientList nvarchar (500)
SET @RecipientList = ''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar (500)
DECLARE contact_curs CURSOR FOR SELECT
	email
FROM tblTenderContact
WHERE TenderContactID = @TenderContactAddressID
--AND complaintContact = 1
AND TenderContactID <> 48
AND currentContact = 1

OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS = 0
BEGIN

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--set @BodyMessage = @BodyMessage + '<br><br>Recipients: ' + @RecipientList

----Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail --The Claims sends from msa@kcic.com
@profile_name = 'Claims',
@recipients = @RecipientList,
@blind_copy_recipients = 'msa@kcic.com',
--@recipients ='amanp@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

--Move to the next firm to create a new email
FETCH NEXT FROM TenderContact_curs INTO @TenderContactAddressID
END
CLOSE TenderContact_curs
DEALLOCATE TenderContact_curs

END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailUnacknowledgedTenders] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailUnacknowledgedTenders] TO [base_user]
GRANT ALTER ON  [dbo].[qryExternalEmailUnacknowledgedTenders] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailUnacknowledgedTenders] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailUnacknowledgedTenders] TO [power_user]
GO
