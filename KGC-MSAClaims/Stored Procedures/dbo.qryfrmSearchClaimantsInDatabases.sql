SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryfrmSearchClaimantsInDatabases] (@strSearchWhere  nvarchar(500))
AS





--DECLARE @strSearchWhere nvarchar(4000)
--SET @strSearchWhere = ' Firstname = ''Blair'' and  convert(nvarchar, SocialSecurityNumber) like ''%'' + convert(nvarchar, 207) + ''%'' AND '


DECLARE @strReturnSearch nvarchar(4000)


set @strReturnSearch = 
'SELECT    cpi.ClaimantPersonalInfoID, cpi.LastName, cpi.FirstName, cpi.SocialSecurityNumber, cpi.BirthDate, l.CaseCaption, l.DocketNumber, s.FullNameState,
	 ''[KGC-Dana-Claims]'' as DatabaseName


FROM [WEBSQL].[KGC-Dana-Claims].dbo.tblClaimantPersonalInfo cpi
inner join [WEBSQL].[KGC-Dana-Claims].dbo.tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
inner join [WEBSQL].[KGC-Dana-Claims].dbo.tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
inner join [WEBSQL].[KGC-Dana-Claims].dbo.tblLawsuit l on l.LawsuitID = cl.LawsuitID
inner join [WEBSQL].[KGC-Dana-Claims].dbo.tblState s on s.StateID=l.StateID

WHERE '+ @strSearchWhere + '
 c.IsCurrentClaim = 1 and cl.IsPrimaryForClaim = 1

Union

SELECT     cpi.ClaimantPersonalInfoID, cpi.LastName, cpi.FirstName, cpi.SocialSecurityNumber, cpi.BirthDate, l.CaseCaption, l.DocketNumber, s.FullNameState,
 ''[KGC-IUNA-Nosroc]'' as DatabaseName

FROM [WEBSQL].[KGC-IUNA-Nosroc].dbo.tblClaimantPersonalInfo cpi
inner join [WEBSQL].[KGC-IUNA-Nosroc].dbo.tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
inner join [WEBSQL].[KGC-IUNA-Nosroc].dbo.tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
inner join [WEBSQL].[KGC-IUNA-Nosroc].dbo.tblLawsuit l on l.LawsuitID = cl.LawsuitID
inner join [WEBSQL].[KGC-IUNA-Nosroc].dbo.tblState s on s.StateID=l.StateID

WHERE '+ @strSearchWhere + '
 c.IsCurrentClaim = 1 and cl.IsPrimaryForClaim = 1

Union

SELECT     cpi.ClaimantPersonalInfoID, cpi.LastName, cpi.FirstName, cpi.SocialSecurityNumber, cpi.BirthDate, l.CaseCaption, l.DocketNumber, s.State as FullNameState,
 ''[KGC-MSAClaims]'' as DatabaseName

FROM [WEBSQL].[KGC-MSAClaims].dbo.tblClaimantPersonalInfo cpi
inner join [WEBSQL].[KGC-MSAClaims].dbo.tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
inner join [WEBSQL].[KGC-MSAClaims].dbo.tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
inner join [WEBSQL].[KGC-MSAClaims].dbo.tblLawsuit l on l.LawsuitID = cl.LawsuitID
inner join [WEBSQL].[KGC-MSAClaims].dbo.tblState s on s.StateID=l.StateID

WHERE '+ @strSearchWhere + '
 c.IsCurrentClaim = 1 and cl.IsPrimaryForClaim = 1
 
 UNION 

SELECT     cpi.ClaimantPersonalInfoID, cpi.LastName, cpi.FirstName, cpi.SocialSecurityNumber, cpi.BirthDate, l.CaseCaption, l.DocketNumber, s.State as FullNameState,
 ''[KGC-MarleyWylain]'' as DatabaseName

FROM [WEBSQL].[KGC-MarleyWylain].dbo.tblClaimantPersonalInfo cpi
inner join [WEBSQL].[KGC-MarleyWylain].dbo.tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
inner join [WEBSQL].[KGC-MarleyWylain].dbo.tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
inner join [WEBSQL].[KGC-MarleyWylain].dbo.tblLawsuit l on l.LawsuitID = cl.LawsuitID
inner join [WEBSQL].[KGC-MarleyWylain].dbo.tblState s on s.StateID=l.StateID

WHERE '+ @strSearchWhere + '
 c.IsCurrentClaim = 1 and cl.IsPrimaryForClaim = 1

ORDER BY 2, 3, 4'


--print @strReturnSearch

execute sp_executesql @strReturnSearch



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSearchClaimantsInDatabases] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSearchClaimantsInDatabases] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmSearchClaimantsInDatabases] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSearchClaimantsInDatabases] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSearchClaimantsInDatabases] TO [power_user]
GO
