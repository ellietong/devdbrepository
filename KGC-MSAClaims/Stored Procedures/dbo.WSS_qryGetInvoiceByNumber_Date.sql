SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetInvoiceByNumber_Date] 
(@InvoiceDate date, @InvoiceNumber nvarchar(10), @FirmID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dicd.InvoiceDetailID FROM tblDefInvClaimantDetail dicd
	INNER JOIN tblDocuments d ON dicd.DocumentID = d.DocumentID
	WHERE InvoiceDate = @InvoiceDate 
		AND RTRIM(dicd.InvoiceNumber) = @InvoiceNumber 
		AND dicd.FirmID = @FirmID
		AND COALESCE(d.StatusID, 0) <> 4
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetInvoiceByNumber_Date] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetInvoiceByNumber_Date] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetInvoiceByNumber_Date] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetInvoiceByNumber_Date] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetInvoiceByNumber_Date] TO [power_user]
GO
