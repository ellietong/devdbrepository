SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryHistoricalDocuments]
	-- Add the parameters for the stored procedure here
@ClaimantPersonalInfoID bigint = null
AS
BEGIN
--DECLARE @claimantpersonalinfoid bigint
--SET @ClaimantPersonalInfoID = 9551
    -- Insert statements for procedure here
	
	SELECT     dbo.tblClaimantDocuments.ClaimantDocumentID, dbo.tblClaimantDocuments.ClaimantPersonalInfoID, 
                      dbo.tblClaimantDocuments.SPDocId, dbo.tblClaimantDocuments.DocumentTitle, 
                      'https://extranet.kcicllc.com' + dbo.tblClaimantDocuments.DocumentURL AS DocumentURL, 
                      dbo.tblClaimantDocumentType.ClaimantDocumentType
FROM         dbo.tblClaimantDocuments INNER JOIN
                      dbo.tblClaimantDocumentType ON 
                      dbo.tblClaimantDocuments.ClaimantDocumentTypeID = dbo.tblClaimantDocumentType.ClaimantDocumentTypeID WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND DocumentTitle IS NOT NULL

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryHistoricalDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryHistoricalDocuments] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryHistoricalDocuments] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryHistoricalDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryHistoricalDocuments] TO [power_user]
GO
