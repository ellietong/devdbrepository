SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryResponseTypeList]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimantDocumentTypeID, ClaimantDocumentType from tblClaimantDocumentType WHERE DocumentGroupID = 5 AND ClaimantDocumentTypeID NOT IN (42, 43)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryResponseTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryResponseTypeList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryResponseTypeList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryResponseTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryResponseTypeList] TO [power_user]
GO
