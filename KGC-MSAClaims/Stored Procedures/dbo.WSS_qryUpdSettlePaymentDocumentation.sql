SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_qryUpdSettlePaymentDocumentation] 
(@SetPymtID int, 
 @SettlePaymentComments nvarchar(max),
 @ReleaseTypeID int, 
 @ExecutedReleaseDate Date, 
 @MedicareFullyDocumented bit, 
 @MedicareReported bit, 
 @Emailaddress nvarchar(250))
as

--DECLARE @ClaimID int
--SET @ClaimID = 1

update tblSettlePaymentDocumentation
set SettlePaymentComments = @SettlePaymentComments, ReleaseTypeID = nullif(@ReleaseTypeID, ''), ExecutedReleaseDate = nullif(@ExecutedReleaseDate,''), 
	MedicareFullyDocumented = @MedicareFullyDocumented, MedicareReported = @MedicareReported, SPUsername = @Emailaddress
where SetPymtID = @SetPymtID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdSettlePaymentDocumentation] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdSettlePaymentDocumentation] TO [power_user]
GO
