SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDelProduct](@ClaimantProductID int, @emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Update the record first to record who is deleting it
    UPDATE tblClaimantProduct
    SET SPUsername = @emailaddress
    WHERE ClaimantProductID = @ClaimantProductID
    
	DELETE FROM tblClaimantProduct WHERE ClaimantProductID = @ClaimantProductID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelProduct] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryDelProduct] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelProduct] TO [power_user]
GO
