SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[zQCqryYearsAllocated]
(@allocationid int)
As

SET NOCOUNT ON

SELECT a.AllocationID, ar.PStartSplit, ar.PEndSplit, p.InsuranceProgramID
FROM (tblInsuranceProgram ip INNER JOIN tblPolicy p ON ip.InsuranceProgramID = p.InsuranceProgramID) 
	INNER JOIN (tblAllocation a INNER JOIN tblAllocationResults ar ON a.AllocationID = ar.AllocationID) ON p.PolicyID = ar.PolicyID
WHERE a.AllocationID LIKE COALESCE(@allocationid, 0)
GROUP BY a.AllocationID, ar.PStartSplit, ar.PEndSplit, p.InsuranceProgramID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[zQCqryYearsAllocated] TO [base_user]
GRANT EXECUTE ON  [dbo].[zQCqryYearsAllocated] TO [base_user]
GRANT ALTER ON  [dbo].[zQCqryYearsAllocated] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryYearsAllocated] TO [power_user]
GRANT EXECUTE ON  [dbo].[zQCqryYearsAllocated] TO [power_user]
GO
