SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptOutstandingSettlementsByPeriod] (@SettlementStartDate datetime, @SettlementEndDate datetime, @MatterID int)

As

SET NOCOUNT ON

--DECLARE @SettlementStartDate datetime
--SET @SettlementStartDate = '1/1/1900'
--DECLARE @SettlementEndDate datetime
--SET @SettlementEndDate = '1/1/2010'
--declare @matterid int
--set @matterid = null

DECLARE @MaxMatterID int
SELECT @MaxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @Counts TABLE(Matter nvarchar(250), SettlementDateAsOf datetime, Count30 int, Count60 int, Count90 int, Count120 int, CountLong int, Amount30 money, Amount60 money, Amount90 money, Amount120 money, AmountLong money)

DECLARE @Count30 int
Declare @Amount30 int
DECLARE @Count60 int
DECLARE @Amount60 int
DECLARE @Count90 int
DECLARE @Amount90 int
DECLARE @Count120 int
DECLARE @Amount120 int
DECLARE @CountLong int
DECLARE @AmountLong int

SELECT @Count30 = COUNT(m.ClaimID), @amount30 = SUM(settlementamount) FROM tblClaim m
INNER JOIN tblClaimLawsuit cl       ON m.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
WHERE m.ClaimID IN(
      SELECT c.ClaimID
      FROM tblClaim c   
            INNER JOIN tblClaimLawsuit cl       ON c.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
      WHERE GETDATE() - cs.SettlementDueDate <= 30
            AND SettlementDate >= @SettlementStartDate and  SettlementDate <= @SettlementEndDate
      GROUP BY c.ClaimID
      HAVING SUM(CASE WHEN paiddate IS NULL THEN COALESCE(SettlementAmount, 0) ELSE 0 END) > 0) 

SELECT @Count60 = COUNT(m.ClaimID),  @amount60 =SUM(settlementamount) FROM tblClaim m
INNER JOIN tblClaimLawsuit cl       ON m.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
WHERE m.ClaimID IN(
      SELECT c.ClaimID
      FROM tblClaim c
            INNER JOIN tblClaimLawsuit cl       ON c.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
      WHERE GETDATE() - cs.SettlementDueDate > 30 AND GETDATE() - cs.SettlementDueDate <= 60
               AND SettlementDate >= @SettlementStartDate and  SettlementDate <= @SettlementEndDate
      GROUP BY c.ClaimID
      HAVING SUM(CASE WHEN paiddate IS NULL THEN COALESCE(SettlementAmount, 0) ELSE 0 END) > 0)

SELECT @Count90 = COUNT(m.ClaimID), @amount90 = SUM(settlementamount) FROM tblClaim m
INNER JOIN tblClaimLawsuit cl       ON m.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
WHERE m.ClaimID IN(
      SELECT c.ClaimID
      FROM tblClaim c
            INNER JOIN tblClaimLawsuit cl       ON c.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
      WHERE GETDATE() - cs.SettlementDueDate > 60 AND GETDATE() - cs.SettlementDueDate <= 90
              AND SettlementDate >= @SettlementStartDate and  SettlementDate <= @SettlementEndDate
      GROUP BY c.ClaimID
      HAVING SUM(CASE WHEN paiddate IS NULL THEN COALESCE(SettlementAmount, 0) ELSE 0 END) > 0)

SELECT @Count120 = COUNT(m.ClaimID),  @amount120 =SUM(settlementamount) FROM tblClaim m
INNER JOIN tblClaimLawsuit cl       ON m.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
WHERE m.ClaimID IN(
      SELECT c.ClaimID
      FROM tblClaim c
            INNER JOIN tblClaimLawsuit cl       ON c.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
      WHERE GETDATE() - cs.SettlementDueDate > 90 AND GETDATE() - cs.SettlementDueDate <= 120
               AND SettlementDate >= @SettlementStartDate and  SettlementDate <= @SettlementEndDate
      GROUP BY c.ClaimID
      HAVING SUM(CASE WHEN paiddate IS NULL THEN COALESCE(SettlementAmount, 0) ELSE 0 END) > 0)

SELECT @CountLong = COUNT(m.ClaimID),  @AmountLong =SUM(settlementamount) FROM tblClaim m
INNER JOIN tblClaimLawsuit cl       ON m.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
WHERE m.ClaimID IN(
      SELECT c.ClaimID
      FROM tblClaim c
            INNER JOIN tblClaimLawsuit cl       ON c.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
      WHERE GETDATE() - cs.SettlementDueDate > 120
                AND SettlementDate >= @SettlementStartDate and  SettlementDate <= @SettlementEndDate
      GROUP BY c.ClaimID
      HAVING SUM(CASE WHEN paiddate IS NULL THEN COALESCE(SettlementAmount, 0) ELSE 0 END) > 0)

INSERT INTO @Counts
      SELECT Matter, @SettlementStartDate, COALESCE(@Count30,0), COALESCE(@Count60,0), COALESCE(@Count90,0), COALESCE(@Count120,0), COALESCE(@CountLong,0), coalesce(@Amount30, 0), coalesce(@Amount60, 0), coalesce(@Amount90,0),
      coalesce(@Amount120,0),coalesce(@AmountLong,0)
      FROM tblMatter
      WHERE MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)

SELECT * FROM @Counts
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlementsByPeriod] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlementsByPeriod] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptOutstandingSettlementsByPeriod] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlementsByPeriod] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlementsByPeriod] TO [power_user]
GO
