SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>

-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdMedicalScores]
	(
	@ILODoctorID int, @PFTDoctorID int, 
	@ILOProfusion nvarchar(100), @ILODate datetime,
    @PFTDate datetime, @PFTTypeID int, 
    @PFTFVC_Actual float, @PFTFVC_Predicted float,
    @PFTFEV1_Actual float, @PFTFEV1_Predicted float,
    --@PFTFEV1FVC_Actual float, @PFTFEV1FVC_Predicted float,
    @PFTTLC_Actual float, @PFTTLC_Predicted float,
    @PFTDLCO_Actual float, @PFTDLCO_Predicted float,
    @PFTTV float, @Username nvarchar(250),
    @ClaimID bigint
	)
AS
BEGIN

	UPDATE tblClaim
	SET ILODoctorID = @ILODoctorID,
      ILOProfusion = @ILOProfusion,
      ILODate = @ILODate,
      PFTDoctorID = @PFTDoctorID,
      PFTDate = @PFTDate,
      PFTTypeID = @PFTTypeID,
      PFTFVC_Actual = @PFTFVC_Actual,
      PFTFVC_Predicted = @PFTFVC_Predicted,
      PFTFEV1_Actual = @PFTFEV1_Actual,
      PFTFEV1_Predicted = @PFTFEV1_Predicted,
      [PFTFEV1andFVC_Actual] = CASE WHEN COALESCE(@PFTFVC_Actual, 0) > 0 THEN @PFTFEV1_Actual/@PFTFVC_Actual ELSE NULL END,
      [PFTFEV1andFVC_Predicted] = CASE WHEN COALESCE(@PFTFVC_Predicted, 0) > 0 THEN @PFTFEV1_Predicted/@PFTFVC_Predicted ELSE NULL END,
      PFTTLC_Actual = @PFTTLC_Actual,
      PFTTLC_Predicted = @PFTTLC_Predicted,
      PFTDLCO_Actual = @PFTDLCO_Actual,
      PFTDLCO_Predicted = @PFTDLCO_Predicted,
      PFTTV = @PFTTV,
      SPUsername = @Username
      WHERE ClaimID = @ClaimID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdMedicalScores] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdMedicalScores] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdMedicalScores] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdMedicalScores] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdMedicalScores] TO [power_user]
GO
