SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_HistoricalUpload]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT d.DocumentID, DocumentURL, DocumentTitle, d.DetailDocumentTypeID AS ClaimantDocumentTypeID, ClaimantDocumentType,
	DefenseCounsel, DefenseCounselID
	FROM tblDocuments d
	INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
	INNER JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
	WHERE CreatedBy = 'princet@kcicllc.com'
	--WHERE DocumentURL <> DocumentLink
	--INNER JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
--	SELECT CASE WHEN Folder = '\TIF' THEN REPLACE(DocumentLink, '\TIF\', '\TIF\MIGRATE\0\' + CONVERT(Nvarchar(5), CONVERT(int, DocumentName)/1024) + '\') ELSE DocumentURL END FROM tblClaimantDocuments
--WHERE DocumentType = 'CHECK'

--SELECT * FROM tblDefenseCounsel WHERe DefenseCounselID = 68
END

--SELECT CASE WHEN Folder = '\TIF' THEN REPLACE(DocumentLink, '\TIF\', '\TIF\MIGRATE\0\' + CONVERT(Nvarchar(5), CONVERT(int, DocumentName)/1024) + '\') WHEN (Folder='\NEWIMAGES' AND RIGHT(DocumentURL, 3) = 'bin') THEN Folder + '\' + DocumentName + '.tif' ELSE DocumentURL END
--FROM tblClaimantDocuments

----WHERE DocumentURL <> DocumentLink AND RIGHT(DocumentURL, 3) = 'bin'
--BEGIN TRAN
--UPDATE tblClaimantDocuments
--SET DocumentURL = DocumentLink
--WHERE DocumentURL <> DocumentLink AND RIGHT(DocumentURL, 3) = 'bin'
--COMMIT TRAN
----SELECT * FROM tblClaimantDocuments WHERE DocumentURL <> DocumentLink AND RIGHT(DocumentURL, 3) = 'bin'


--SELECT * FROm tblDocuments
GO
GRANT VIEW DEFINITION ON  [dbo].[m_HistoricalUpload] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_HistoricalUpload] TO [base_user]
GRANT ALTER ON  [dbo].[m_HistoricalUpload] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_HistoricalUpload] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_HistoricalUpload] TO [power_user]
GO
