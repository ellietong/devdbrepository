SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*SET QUOTED_IDENTIFIER ON
GO*/
CREATE PROC [dbo].[WSS_PaidClaims]
AS
SELECT     C.ClaimID, S.State, J.Jurisdiction, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      HLDDefense.HighLevelDisease AS Disease, CS.SettlementAmount, CONVERT(date, CS.SettlementDate) 
                      AS SettlementDate, CONVERT(date, MIN(CS.PaidDate)) AS MinPaidDate,  pca.PlaintiffCounsel, CS.CheckNumber, CS.SettlementGroupID, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS SSN, DocketNumber
FROM         dbo.tblClaimantPersonalInfo AS CPI LEFT OUTER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID LEFT OUTER JOIN
					  (SELECT * FROM tblClaimDisease WHERE PrimaryDisease = 1) AS CD ON CD.ClaimID = C.ClaimID LEFT OUTER JOIN
                      dbo.tblDisease AS DDefense ON CD.DiseaseID = DDefense.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS HLDDefense ON HLDDefense.HighLevelDiseaseID = DDefense.HighLevelDiseaseID LEFT OUTER JOIN
                      dbo.tblClaimSequence AS ClaimS ON ClaimS.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblClaimLawsuitStatus AS CLS ON CL.ClaimLawsuitStatusID = CLS.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS CS ON CL.ClaimLawsuitID = CS.ClaimLawsuitID LEFT JOIN
                      dbo.tblPlaintiffCounselAddress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
WHERE     (CL.ClaimLawsuitStatusID IN (23, 25, 28)) AND (CL.IsPrimaryForClaim = 1) AND (CS.PaidDate IS NOT NULL) OR
                      (CL.ClaimLawsuitStatusID IN (23, 25, 28)) AND (CL.IsPrimaryForClaim = 1) AND (CS.CheckNumber IS NOT NULL)
GROUP BY C.ClaimID, HLDDefense.HighLevelDisease, S.State, J.Jurisdiction, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END, CS.SettlementDate, CLS.ClaimLawsuitStatus, CS.SettlementAmount, CS.PaidDate, CS.PaymentDueDate, 
                       pca.PlaintiffCounsel, CL.ClaimLawsuitStatusID, CS.CheckNumber, CS.SettlementGroupID, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE SocialSecurityNumber END, DocketNumber


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_PaidClaims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_PaidClaims] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_PaidClaims] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_PaidClaims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_PaidClaims] TO [power_user]
GO
