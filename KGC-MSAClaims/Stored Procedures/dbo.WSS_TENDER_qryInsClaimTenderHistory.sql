SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryInsClaimTenderHistory]
(@TenderHistoryID bigint, @ClaimID bigint, @TenderStatusID int, @emailaddress nvarchar(500))
AS
BEGIN
	SET NOCOUNT ON;

    --Test vars
    --DECLARE @TenderHistoryID bigint = 219004
    --DECLARE @ClaimID bigint = 34
    --DECLARE @TenderStatusID int = 1
    --DECLARE @emailaddress nvarchar(MAX) = 'crookc@kcicllc.com'
    
    DECLARE @WithdrawalCTH bigint
		SELECT TOP 1 @WithdrawalCTH = ClaimTenderHistoryID FROM tblClaimTenderHistory cth
		INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID
		WHERE	cth.TenderHistoryID <> @TenderHistoryID 
				AND ClaimID = @ClaimID 
				AND TenderTypeID IN (1, 3)
				AND TenderStatusID IN (2, 5)
		ORDER BY DateCreated DESC
    
    --DECLARE @val int = (SELECT COUNT(ClaimTenderHistoryID) FROM tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID= th.TenderHistoryID WHERE ClaimID = @ClaimID AND TenderTypeID = 2 AND TenderHistoryID = @TenderHistoryID)
    --This probably needs to do more -- like if the new tender history record is a retender
    IF (SELECT TenderTypeID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID) = 3 AND --and no pending withdrawal exists for the old ClaimTenderHistory record
    (SELECT COUNT(ClaimTenderHistoryID) FROM tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID= th.TenderHistoryID WHERE ClaimID = @ClaimID AND TenderTypeID = 2 AND cth.TenderHistoryID = @TenderHistoryID) = 0
    BEGIN
		
		
		--Update the status to "Withdrawn" for 
		UPDATE tblClaimTenderHistory
		SET TenderStatusID = 3
		WHERE ClaimTenderHistoryID = @WithdrawalCTH
				
		
		--Insert withdrawal TH record record
		INSERT INTO tblTenderHistory (TenderBlockID, TenderTypeID, DateCreated, SPUsername)
		SELECT TenderBlockID, 2, GETDATE(), @Emailaddress
		FROM tblTenderHistory 
		WHERE TenderHistoryID = (SELECT TenderHistoryID FROM tblClaimTenderHistory WHERE ClaimTenderHistoryID = @WithdrawalCTH)
		
		DECLARE @WithdrawalTH bigint = (SELECT IDENT_CURRENT('tblTenderHistory'))
		
		--Insert claimtenderhistory for new withdrawal TH
		INSERT INTO tblClaimTenderHistory (ClaimID, TenderHistoryID, TenderStatusID, PreviousClaimTenderHistoryID)
		SELECT @ClaimID, @WithdrawalTH, 1, @WithdrawalCTH
		
    END
    
    
	INSERT INTO tblClaimTenderHistory (ClaimID, TenderHistoryID, TenderStatusID, SPUsername, PreviousClaimTenderHistoryID)
	SELECT	@ClaimID,
			@TenderHistoryID, 
			@TenderStatusID,
			@emailaddress,
			@WithdrawalCTH

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryInsClaimTenderHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryInsClaimTenderHistory] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryInsClaimTenderHistory] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryInsClaimTenderHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryInsClaimTenderHistory] TO [power_user]
GO
