SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryOutstandingTenders_REVERT]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
DECLARE @HoldForTender TABLE (TenderHistoryID int)
INSERT INTO @HoldForTender
SELECT TenderHistoryID
FROM tblTenderHistory TH	
INNER JOIN tblTenderBlock TB ON TH.TenderBlockID = TB.TenderBlockID
INNER JOIN tblTenderType TT ON TH.TenderTypeID = TT.TenderTypeID
WHERE TH.TenderBlockID = 18 AND TH.TenderTypeID = 3
    
SELECT DISTINCT COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') + ' (' + COALESCE(NCCReferenceID, 'Error: No NCC #') + ')' AS 'ClaimantName', CPI.ClaimantPersonalInfoID, TenderBlockName, TT.TenderType, CONVERT(date, ServiceDate) AS 'ServiceDate', CONVERT (date,DateCreated) AS 'TenderCreationDate'
FROM tblTenderHistory TH	
INNER JOIN tblTenderBlock TB ON TH.TenderBlockID = TB.TenderBlockID
INNER JOIN tblTenderType TT ON TH.TenderTypeID = TT.TenderTypeID
INNER JOIN tblClaim C ON TH.ClaimID = C.ClaimID
LEFT JOIN tblClaimLawsuit CL ON C.ClaimID=CL.ClaimID
LEFT JOIN tblLawsuit L ON CL.LawsuitID=L.LawsuitID
INNER JOIN tblClaimantPersonalInfo CPI ON C.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
WHERE DateApproved IS NULL AND NOT TenderHistoryID IN (SELECT * FROM @HoldForTender)
ORDER BY CONVERT(date, ServiceDate) ASC, [ClaimantName] ASC, TenderType DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOutstandingTenders_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOutstandingTenders_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryOutstandingTenders_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOutstandingTenders_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOutstandingTenders_REVERT] TO [power_user]
GO
