SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrpt_BillingReport_ClaimsBilled]
(@ExpectedPayDate date)

AS
SET NOCOUNT ON


--declare @ExpectedPayDate date
--set  @ExpectedPayDate = '3/31/2014'

DECLARE @IndemnityBilledInQuarter Table (claimid int, QuarterIndemnity money)
insert into @IndemnityBilledInQuarter
select claimid, sum(Cost3) as Indemnity
from tbloccurrence
where ExpectedPayDate = @ExpectedPayDate
group by claimid

DECLARE @DefenseBilledInQuarter Table (claimid int, QuarterDefense money)
insert into @DefenseBilledInQuarter
select claimid, sum(cost1) as Defense
from tbloccurrence
where ExpectedPayDate = @ExpectedPayDate
group by claimid

DECLARE @CumulativeDefenseBilled Table (claimid int, CumDefense money)
insert into @CumulativeDefenseBilled
select o.claimid, sum(cost1) as Defense
from tbloccurrence o
inner join @DefenseBilledInQuarter d on d.claimid = o.claimid
where ExpectedPayDate <= @ExpectedPayDate
group by o.claimid

Declare @AllClaimsBilled table (Claimid int)
insert into @AllClaimsBilled
select claimid from @DefenseBilledInQuarter
union
select claimid from @IndemnityBilledInQuarter


select FullNameState, CauseAgent as Material, NCCReferenceID as NCCID, LastName, FirstName, 
CONVERT(DATETIME, dbo.fnEarliestExposure(cpi.ClaimantPersonalInfoID)) as DOFE, cls.ClaimLawsuitStatus as Status, 
ibq.QuarterIndemnity as Indemnity, dbq.QuarterDefense as Defense, cdb.CumDefense as CumulativeDefense,
datepart(q,@ExpectedPayDate) as Quarter, DATEPART(yyyy,@ExpectedPayDate) as Year, StatusDate
from @AllClaimsBilled acb
inner join tblclaim c on c.claimid = acb.claimid
inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on l.LawsuitID=cl.LawsuitID
inner join (select * From tblClaimLawsuitStatusTracking where IsPrimaryStatus =1) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on clst.ClaimLawsuitStatusID=cls.ClaimLawsuitStatusID
inner join tblState s on l.StateID=s.StateID
inner join tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
inner join tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
inner join tblMaterial m on m.MaterialID = a.MaterialID
left join @CumulativeDefenseBilled cdb on cdb.claimid = acb.claimid
left join @DefenseBilledInQuarter dbq on dbq.claimid = acb.claimid
left join @IndemnityBilledInQuarter ibq on ibq.claimid = acb.Claimid
WHERE (M.MaterialID IN (1, 2) AND COALESCE(dbo.fnEarliestExposure(CPI.ClaimantPersonalInfoID), '1/1/1900') < '4/1/1985')
OR (M.MaterialID IN (3, 4, 5, 6, 7, 8, 9) AND COALESCE(dbo.fnEarliestExposure(CPI.ClaimantPersonalInfoID), '1/1/1900') < '4/1/1986') 
order by LastName, FirstName


--SELECT * FROM m_tblClaimType_StandardMSA
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrpt_BillingReport_ClaimsBilled] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrpt_BillingReport_ClaimsBilled] TO [base_user]
GRANT ALTER ON  [dbo].[qryrpt_BillingReport_ClaimsBilled] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrpt_BillingReport_ClaimsBilled] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrpt_BillingReport_ClaimsBilled] TO [power_user]
GO
