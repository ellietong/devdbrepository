SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] 
(@prefixText nvarchar(MAX), @Emailaddress nvarchar(100), @count int = 500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)


DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)

IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1 OR @FirmAddressID IN (SELECT DefenseCounselAddressID FROM tblDefenseCounselAddress WHERE DefenseCounselID IN(188)))
BEGIN
    SET ROWCOUNT @count
    -- Insert statements for procedure here
	SELECT DISTINCT cl.ClaimLawsuitID AS ClaimLawsuitID, NCCReferenceID + '  (' + (CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
								  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + '' END +')') AS NCCReference
	FROM tblClaimLawsuit cl
	INNER JOIN tblClaim c on cl.ClaimID = c.ClaimID
	LEFT JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantSuffix cls WITH (READCOMMITTED)  
	 on cpi.SuffixID = cls.SuffixID 
	WHERE COALESCE(IsCurrentClaim, 0) = 1 AND cl.ClaimLawsuitID IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1 AND ClaimLawsuitStatusID = 22)
	AND NCCReferenceID LIKE  COALESCE('%' + @PrefixText, '') + '%'--COALESCE('%' + 'ncc', '') + '%'
	ORDER BY NCCReference
END
ELSE	
BEGIN
    SET ROWCOUNT @count
	SELECT DISTINCT cl.ClaimLawsuitID AS ClaimLawsuitID, NCCReferenceID + '  (' + (CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
								  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + '' END +')') AS NCCReference
	FROM tblClaimLawsuit cl
	INNER JOIN tblClaim c on cl.ClaimID = c.ClaimID
	LEFT JOIN tblLawsuit l ON cl.LawsuitID  = l.LawsuitID
	INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID --AND c.MatterID = dj.matterID
	INNER JOIN tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantSuffix cls WITH (READCOMMITTED)  
	 on cpi.SuffixID = cls.SuffixID 
	WHERE COALESCE(IsCurrentClaim, 0) = 1 AND
	--EXISTS (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1 AND COALESCE(IsCurrentClaim, 0) = 1 AND ClaimLawsuitStatusID <> 8)
	NCCReferenceID LIKE  COALESCE('%' + @PrefixText, '') + '%'
	  --('%' + @prefixText + '%')
	AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
			AND cl.ClaimLawsuitID IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 1) = 1 AND ClaimLawsuitStatusID = 22)
	ORDER BY NCCReference
END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] TO [power_user]
GO
