SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmUpdateDismissal]
(@ClaimLawsuitID int, @ClaimLawsuitStatusTrackingID int, @ClaimLawsuitStatusID int, @DismissalDate Date, @QCBy nvarchar(255), @DocumentID int)

As
Set NoCount On

/**If Settled Do Not set dismissed as primary status**/
IF (select claimlawsuitstatusid from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusTrackingID = @ClaimLawsuitStatusTrackingID) in (23,24,25)
BEGIN

insert into tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, StatusNotes, ProcessedDate)
select @ClaimLawsuitID, @ClaimLawsuitStatusID, @DismissalDate, 0, 'Updated by review of documentID ' + convert(varchar(500),@DocumentID) + ' by ' + @QCBy, convert(date,GETDATE())

END
ELSE
BEGIN
update tblClaimLawsuitStatusTracking
set  IsPrimaryStatus = 0
where ClaimLawsuitStatusTrackingID = @ClaimLawsuitStatusTrackingID

insert into tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, StatusNotes, ProcessedDate)
select @ClaimLawsuitID, @ClaimLawsuitStatusID, @DismissalDate, 1, 'Updated by review of documentID ' + convert(varchar(500),@DocumentID) + ' by ' + @QCBy, convert(date,GETDATE())
END


RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateDismissal] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateDismissal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateDismissal] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateDismissal] TO [Claims_User]
GRANT ALTER ON  [dbo].[qryfrmUpdateDismissal] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateDismissal] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateDismissal] TO [power_user]
GO
