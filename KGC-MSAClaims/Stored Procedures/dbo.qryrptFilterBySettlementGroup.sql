SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptFilterBySettlementGroup]
(@SettlementGroupID bigint)
As
Set NoCount On

--declare @SettlementGroupID bigint
--set @SettlementGroupID =6749


Update tblSettlePaymentDocumentation 
Set ReleaseTemplateTypeID = Case When ReleaseTemplateTypeID is null then dbo.fnDetermineReleaseTemplateType(cpi.ClaimantPersonalInfoID) else ReleaseTemplateTypeID end
--Select cpi.claimantpersonalinfoID, Case When ReleaseTemplateTypeID is null then dbo.fnDetermineReleaseTemplateType(cpi.ClaimantPersonalInfoID) else ReleaseTemplateTypeID end
From tblClaimantPersonalInfo cpi left join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID left join tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID
Where spd.ClaimLawsuitID in (Select ClaimLawsuitID From tblClaimantSettlement Where SettlementGroupID = @SettlementGroupID)


Select LastName,
FirstName,
cpi.ClaimantPersonalInfoID, 
DocketNumber, 
LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4)as SSN, 
CASE WHEN DeceasedDate IS NOT NULL and (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
		 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END AS SupportingDocumentation,
sd.ReleaseTemplateTypeID, ReleaseTemplateType, cl.ClaimLawsuitID, HighLevelDisease, SettlementGroup   
from tblClaimantPersonalInfo cpi
left join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
left join (Select * From tblClaimDisease Where PrimaryDisease = 1) cd on cd.ClaimID = c.ClaimID
left join tblDisease d on d.DiseaseID = cd.DiseaseID
left join tblHighLevelDisease hd on hd.HighLevelDiseaseID = d.HighLevelDiseaseID
left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
left join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblSettlementGroup sg on sg.SettlementGroupID = cs.SettlementGroupID
left join tblSettlePaymentDocumentation sd on sd.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblReleaseTemplateType rtt on rtt.ReleaseTemplateTypeID = sd.ReleaseTemplateTypeID
where cs.SettlementGroupID=@SettlementGroupID
GROUP BY LastName,cpi.ClaimantPersonalInfoID, FirstName, HighLevelDisease, SettlementGroup, sd.ReleaseTemplateTypeID, ReleaseTemplateType, cl.ClaimLawsuitID, DocketNumber, LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4),SettlementConfirmation, ClaimantIDed,DxMedicals,DODCert,EstatePapers,ProductID,EmployExpoHist,DeceasedDate
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptFilterBySettlementGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptFilterBySettlementGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFilterBySettlementGroup] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptFilterBySettlementGroup] TO [Claims_User]
GRANT ALTER ON  [dbo].[qryrptFilterBySettlementGroup] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFilterBySettlementGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptFilterBySettlementGroup] TO [power_user]
GO
