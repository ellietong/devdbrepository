SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DisableAllTriggers]
AS
DECLARE @string VARCHAR(8000)
DECLARE @tableName NVARCHAR(500)
DECLARE cur CURSOR
FOR SELECT name AS tbname FROM sysobjects WHERE id IN(SELECT parent_obj FROM sysobjects WHERE xtype='tr')
OPEN cur
FETCH next FROM cur INTO @tableName
WHILE @@fetch_status = 0
BEGIN
SET @string ='Alter table '+ @tableName + ' Disable trigger all'
EXEC (@string)
FETCH next FROM cur INTO @tableName
END
CLOSE cur
DEALLOCATE cur
GO
GRANT VIEW DEFINITION ON  [dbo].[DisableAllTriggers] TO [base_user]
GRANT EXECUTE ON  [dbo].[DisableAllTriggers] TO [base_user]
GRANT ALTER ON  [dbo].[DisableAllTriggers] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[DisableAllTriggers] TO [power_user]
GRANT EXECUTE ON  [dbo].[DisableAllTriggers] TO [power_user]
GO
