SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[WSS_qryTenderTemplateList_REVERT]
(@TenderHistoryID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--DECLARE @TenderHistoryID bigint = 171720
    -- Insert statements for procedure here
	SELECT TenderTemplateID, TemplateDisplayName
	FROM 
	tblTenderHistory th
	INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	INNER JOIN tblTenderTemplate tt ON th.TenderBlockID = tt.TenderBlockID AND th.TenderTypeID = tt.TenderTypeID AND a.MaterialID = tt.MaterialID
	WHERE th.TenderHistoryID = @TenderHistoryID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderTemplateList_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderTemplateList_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryTenderTemplateList_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderTemplateList_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderTemplateList_REVERT] TO [power_user]
GO
