SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SEARCH_qryDocumentSearch] (@ClaimantName			nvarchar(max), 
													  @DocumentTitle		nvarchar(max), 
													  @DocumentTypeID		int,
													  @FirmID				int,
													  @CreatedBy			nvarchar(250), 
													  @DateCreatedStart		datetime, 
													  @DateCreatedEnd		datetime, 
													  @IncludeHistorical	bit,
													  @ClaimantIDs			integer_list_tbltype READONLY,
													  @emailaddress			nvarchar(250))
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantName      nvarchar(max)
--DECLARE @DocumentTitle     nvarchar(max)
--DECLARE @DocumentTypeID    int
--DECLARE @FirmID            int
--DECLARE @CreatedBy         nvarchar(250)
--DECLARE @DateCreatedStart  datetime
--DECLARE @DateCreatedEnd    datetime
--DECLARE @IncludeHistorical bit
--DECLARE @ClaimantIDs       TABLE (n int) 
--DECLARE @emailaddress      nvarchar(250)
--SET @ClaimantName      = NULL
--SET @DocumentTitle     = NULL
--SET @DocumentTypeID    = 8
--SET @FirmID            = NULL
--SET @CreatedBy         = NULL
--SET @DateCreatedStart  = NULL
--SET @DateCreatedEnd    = NULL
--SET @IncludeHistorical = 0
----SET @ClaimantIDs     = NULL
--SET @emailaddress      = 'skim@cbslawllp.com'

DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)	

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

IF object_id('tempdb..#AccessibleClaimants') IS NOT NULL DROP TABLE #AccessibleClaimants
CREATE TABLE #AccessibleClaimants (DocumentID int, ClaimantName nvarchar(max))

IF object_id('tempdb..#DocsToConsider') IS NOT NULL DROP TABLE #DocsToConsider
CREATE TABLE #DocsToConsider (DocumentID int, DocumentTitle nvarchar(4000), DocumentURL nvarchar(4000), ClaimantDocumentType nvarchar(200), FirmID int, 
							  Firm nvarchar(250), CreatedBy nvarchar(250), DateCreated datetime, AcceptedBy nvarchar(250), DateAccepted datetime, AmazonKey nvarchar(max), IsHistorical bit)

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
IF object_id('tempdb..#ClaimantIDs') IS NOT NULL DROP TABLE #ClaimantIDs 
CREATE TABLE #ClaimantIDs (n int)
INSERT INTO #ClaimantIDs
	SELECT n FROM @ClaimantIDs
	
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @SelectStringAccessibleClaimants nvarchar(max)
DECLARE @GroupByAccessibleClaimants nvarchar(max)
DECLARE @SelectStringDocsToConsider nvarchar(max)
DECLARE @JoinString nvarchar(max)
DECLARE @WhereStringClaimants nvarchar(max)
DECLARE @WhereStringClaimantName nvarchar(max)
DECLARE @WhereStringDocuments nvarchar(max)
DECLARE @QueryString nvarchar(max)

SET @SelectStringAccessibleClaimants = 'INSERT INTO #AccessibleClaimants
											SELECT mcd.DocumentID, 
												''<a target="_blank" href="/clients/msa/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + ''">'' + (COALESCE(LastName, '''') + '', '' + COALESCE(FirstName, '''') + '' ('' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + '')'') + ''</a>''
											FROM tblMultiClaimantDocument mcd
												INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID '

SET @GroupByAccessibleClaimants =	   'GROUP BY mcd.DocumentID, 
											''<a target="_blank" href="/clients/msa/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + ''">'' + (COALESCE(LastName, '''') + '', '' + COALESCE(FirstName, '''') + '' ('' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + '')'') + ''</a>'',
											LastName, FirstName	
										ORDER BY LastName, FirstName '

SET @SelectStringDocsToConsider =	   'INSERT INTO #DocsToConsider
											SELECT DISTINCT d.DocumentID,
												DocumentTitle,
												DocumentURL,
												ClaimantDocumentType,
												d.FirmID,
												Firm,
												CreatedBy,
												DateCreated,
												AcceptedBy,
												DateAccepted,
												AmazonKey,
												CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical
											FROM tblDocuments d 
												INNER JOIN tblSingleClaimantDocumentType sc ON sc.ClaimantDocumentTypeID = d.DetailDocumentTypeID			
												INNER JOIN tblClaimantDocumentType cdt		ON sc.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
												LEFT  JOIN vtblFirm f						ON f.FirmID = d.FirmID
												LEFT JOIN tblClaimDocuments cd on cd.DocumentID = d.DocumentID'

SET @JoinString = ' '
SET @WhereStringClaimantName = ' '
SET @WhereStringClaimants = ' WHERE 1=1 '
SET @WhereStringDocuments = ' WHERE 1=1 '

IF @ClaimantName	  IS NOT NULL           BEGIN SET @WhereStringClaimantName = @WhereStringClaimantName + ' AND ClaimantNames like ''%'' + @ClaimantNameParam + ''%'' ' END
IF (SELECT COUNT(n) FROM #ClaimantIDs) <> 0 BEGIN SET @WhereStringClaimants = @WhereStringClaimants + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)' END

IF @DocumentTitle	  IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND DocumentTitle LIKE ''%'' + @DocumentTitleParam + ''%''' END
IF @DocumentTypeID	  IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND d.DetailDocumentTypeID = ' + CONVERT(nvarchar(20), @DocumentTypeID) END
IF @FirmID            IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND d.FirmID = ' + CONVERT(nvarchar(20),@FirmID) END
IF @CreatedBy		  IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND d.CreatedBy LIKE ''%'' + @CreatedByParam + ''%''' END
IF @DateCreatedStart IS NOT NULL OR @DateCreatedEnd IS NOT NULL 
        BEGIN
            SET @WhereStringDocuments = @WhereStringDocuments
                + ' AND COALESCE(CONVERT(date,d.DateCreated), ''1/1/1900'') >= '''
                + CONVERT(NVARCHAR, COALESCE(@DateCreatedStart, '1/1/1900'))
                + ''' AND COALESCE(CONVERT(date,d.DateCreated), CONVERT(date, GETDATE())) <= '''
                + CONVERT(NVARCHAR, COALESCE(@DateCreatedEnd,
                                             CONVERT(DATE, GETDATE()))) + ''''
        END
IF @IncludeHistorical IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END <= ' + CONVERT(nvarchar(20),@IncludeHistorical) END

IF @InsurerRole = 1
BEGIN
SET @WhereStringDocuments = @WhereStringDocuments
                + ' AND cdt.ClaimantDocumentTypeID NOT IN (42,43,45,52) 
				    AND COALESCE(cd.ClaimID,0) NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3)) 
					GROUP BY d.DocumentID,
							DocumentTitle,
							DocumentURL,
							ClaimantDocumentType,
							d.FirmID,
							Firm,
							CreatedBy,
							DateCreated,
							AcceptedBy,
							DateAccepted,
							AmazonKey,
							CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END '
END 

IF((RIGHT(@emailaddress, 11) <> 'kcicllc.com' OR RIGHT(@emailaddress, 8) <> 'kcic.com') AND ((SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters 
													 FROM vtblPerson vp
															INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
													 WHERE Email = @emailaddress) <> 1))
BEGIN
	SET @WhereStringClaimants = @WhereStringClaimants + ' AND (l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @JoinString = ' INNER JOIN tblClaim c                  ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						INNER JOIN tblClaimLawsuit cl          ON c.ClaimID = cl.ClaimID
						INNER JOIN tblLawsuit l                ON cl.LawsuitID = l.LawsuitID
						INNER JOIN tblDefenseJurisdiction dj   ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID '
	SET @QueryString = @SelectStringAccessibleClaimants + 
					   @JoinString + 
					   @WhereStringClaimants + 
					   @GroupByAccessibleClaimants +
					   @SelectStringDocsToConsider +
					   @WhereStringDocuments +
					   ' AND d.DocumentID IN(SELECT DocumentID FROM #AccessibleClaimants) AND d.DetailDocumentTypeID NOT IN (1, 40) GROUP BY d.DocumentID,
							DocumentTitle,
							DocumentURL,
							ClaimantDocumentType,
							d.FirmID,
							Firm,
							CreatedBy,
							DateCreated,
							AcceptedBy,
							DateAccepted,
							AmazonKey,
							CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END '			   
END
ELSE
BEGIN
	SET @QueryString = @SelectStringDocsToConsider +
					   @WhereStringDocuments +
					   @SelectStringAccessibleClaimants +  
					   @WhereStringClaimants + 
					   ' AND DocumentID IN(SELECT DocumentID FROM #DocsToConsider) ' +
					   @GroupByAccessibleClaimants			   	
END



--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
SET @QueryString = @QueryString + ' 
	SELECT DISTINCT DocumentID, 
		AmazonKey,
		''/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId='' + CONVERT(nvarchar(100), d.DocumentID) + ''&source='' + ''/clients/msa/cp/Pages/Search.aspx'' AS DocumentURL,
		DocumentTitle,
		ClaimantDocumentType AS DocumentType,
		Firm,
		COALESCE(ClaimantNames, '''') AS Claimants,
		DateCreated,
		CreatedBy  + ''</br>'' + ''('' + CONVERT(nvarchar,CONVERT(DATE, DateCreated))  + '')'' AS Created,
		AcceptedBy + ''</br>'' + ''('' + CONVERT(nvarchar,CONVERT(DATE, DateAccepted)) + '')'' AS Accepted,		
		IsHistorical
	FROM #DocsToConsider d
		CROSS APPLY (SELECT ClaimantName + ''</br> ''
					 FROM #AccessibleClaimants ac
					 WHERE ac.DocumentID = d.DocumentID
					 FOR XML PATH('''') ) Q ( ClaimantNames )
	WHERE DocumentID IN(SELECT DocumentID FROM #AccessibleClaimants) ' +
		@WhereStringClaimantName + '
	ORDER BY DateCreated DESC
	OPTION (RECOMPILE)'

--Finally, the query created above is executed.
PRINT @QueryString

EXEC sp_executesql @QueryString, N'@ClaimantNameParam nvarchar(max), @DocumentTitleParam nvarchar(max), @CreatedByParam nvarchar(250)', @ClaimantNameParam = @ClaimantName, @DocumentTitleParam = @DocumentTitle, @CreatedByParam = @CreatedBy


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [power_user]
GO
