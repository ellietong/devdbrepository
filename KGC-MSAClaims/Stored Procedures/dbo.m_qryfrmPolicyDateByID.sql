SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[m_qryfrmPolicyDateByID]
AS
SET NOCOUNT ON
SELECT PolicyDateID, PolicyNum + ' ' + CarrierName + ' ' + CONVERT(varchar, PStartDate, 101) + ' ' + InsuranceProgram AS PolicyInfo
FROM tblInsuranceProgram ip INNER JOIN ( (tblPolicy p INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID) 
     INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID) ON ip.InsuranceProgramID = p.InsuranceProgramID
GROUP BY PolicyDateID, PolicyNum + ' ' + CarrierName + ' ' + CONVERT(varchar, PStartDate, 101) + ' ' + InsuranceProgram, p.PolicyNum, c.CarrierName
ORDER BY p.PolicyNum, c.CarrierName, Min(pd.PStartDate)

RETURN


CREATE TABLE [dbo].[tblAllocationPolicyDateExclude](
[PolicyDateExcludeID] [int] IDENTITY(1,1) NOT NULL,
[AllocationID] [int] NULL,
[PolicyDateID] [int] NULL,
 CONSTRAINT [PK_tblAllocationPolicyDateExclude] PRIMARY KEY NONCLUSTERED 
(
[PolicyDateExcludeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_qryfrmPolicyDateByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qryfrmPolicyDateByID] TO [base_user]
GRANT ALTER ON  [dbo].[m_qryfrmPolicyDateByID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_qryfrmPolicyDateByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_qryfrmPolicyDateByID] TO [power_user]
GO
