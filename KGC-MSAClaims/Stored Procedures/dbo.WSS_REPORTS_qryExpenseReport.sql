SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_REPORTS_qryExpenseReport] ( @StartDate  datetime=null,	@EndDate  datetime=null, @TortTypeID int=null, @ClaimTypeID int=null) 

AS

SET NOCOUNT ON

--DECLARE @StartDate  datetime='1/1/2005',	@EndDate  datetime='1/1/2014', @TortTypeID int=null, @ClaimTypeID int=null

------Find Max Matter ID
DECLARE @MaxTortTypeID int
SELECT
	@MaxTortTypeID = MAX(TortTypeID)
FROM tblTortType

DECLARE @SettlementSums Table (IndemnityTotal Money, PostingYear INT)
INSERT INTO @SettlementSums
SELECT
	CONVERT(Money,SUM(COALESCE(cs.SettlementAmount, 0))) AS Indemnity,
	YEAR(cs.PostingDate)

FROM tblClaim c 
INNER JOIN tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
INNER JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblAllegation a on a.ClaimLawsuitID = cs.ClaimLawsuitID

WHERE 
c.TortTypeID >= COALESCE(@TortTypeID, 0) AND c.TortTypeID <= COALESCE(NULLIF(@TortTypeID, 0), @MaxTortTypeID) 
AND a.AllegationTypeID >= COALESCE(@ClaimTypeID, 0) AND a.AllegationTypeID <= COALESCE(NULLIF(@ClaimTypeID, 0), 4)
AND
(cs.PostingDate >= COALESCE(@StartDate, '1/1/1900')
AND cs.PostingDate <= COALESCE(@EndDate, GETDATE()))

GROUP BY YEAR(cs.PostingDate)

DECLARE @DefenseTotals Table (DefenseTotal Money, PeriodStart INT)
INSERT INTO @DefenseTotals
SELECT
	CONVERT(Money,SUM(COALESCE(dicd.Fees, 0) + COALESCE(dicd.Costs, 0) - COALESCE(dicd.Adjustments, 0))) AS Defense,
	YEAR(di.PeriodStart) AS ExpenseYear

FROM tblDefenseInvoice di
INNER JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID
INNER JOIN tblDefInvClaimantDetail dicd ON di.DefenseInvoiceID = dicd.DefenseInvoiceID
LEFT JOIN tblDefenseCounsel dc ON di.FirmID = dc.DefenseCounselID
LEFT JOIN tblVendor ex ON ex.VendorID = di.VendorfirmID
INNER JOIN tblClaim c on c.ClaimID = dicd.ClaimID
INNER JOIN tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
INNER JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblAllegation a on a.ClaimLawsuitID = cs.ClaimLawsuitID

WHERE
c.TortTypeID >= COALESCE(@TortTypeID, 0) AND c.TortTypeID <= COALESCE(NULLIF(@TortTypeID, 0), @MaxTortTypeID) 
AND a.AllegationTypeID >= COALESCE(@ClaimTypeID, 0) AND a.AllegationTypeID <= COALESCE(NULLIF(@ClaimTypeID, 0), 4)
AND (di.PeriodStart >= COALESCE(@StartDate, '1/1/1900')
AND di.PeriodStart <= COALESCE(@EndDate, GETDATE()))


GROUP BY YEAR(di.PeriodStart)



SELECT 
s.IndemnityTotal as Indemnity, 
d.DefenseTotal as Defense,
s.IndemnityTotal + d.DefenseTotal as Total,
COALESCE(s.PostingYear, d.PeriodStart) as ExpenseYear
From @SettlementSums s
INNER JOIN @DefenseTotals d on d.PeriodStart = s.PostingYear
ORDER BY ExpenseYear

GO
GRANT ALTER ON  [dbo].[WSS_REPORTS_qryExpenseReport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryExpenseReport] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryExpenseReport] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_REPORTS_qryExpenseReport] TO [power_user]
GRANT CONTROL ON  [dbo].[WSS_REPORTS_qryExpenseReport] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryExpenseReport] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryExpenseReport] TO [power_user]
GO
