SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qrysrptToxicTortReports_SpendByYearByType2](@DataDate DATETIME) 
AS
SET NOCOUNT ON

--DECLARE @DataDate DATETIME
--Set @DataDate = '6/18/2014'

Select COALESCE(a.yr, b.yr) AS YR, a.AsbestosSettlement, b.AsbestosDefense, a.AsbestosSettlement+b.AsbestosDefense AS AsbestosTotal, 
	a.CoalSettlement, b.CoalDefense, a.CoalSettlement+b.CoalDefense AS CoalTotal, 
	a.SilicaSettlement, b.SilicaDefense, a.SilicaSettlement+b.SilicaDefense AS SilicaTotal,
	a.OtherSettlement, b.OtherDefense, a.OtherSettlement+b.OtherDefense AS OtherTotal,  
	a.GeneralSettlement, b.GeneralDefense, a.GeneralSettlement+b.GeneralDefense AS GeneralTotal, 
	@DataDate AS DD

From

(
		Select Year(PostingDate) AS yr,
			COALESCE(SUM(CASE WHEN ClaimType LIKE '%Asbestos%' THEN SettlementAmount_MSAPaid END),0) AS AsbestosSettlement,
			COALESCE(SUM(CASE WHEN ClaimType LIKE '%Coal%' THEN SettlementAmount_MSAPaid END),0) AS CoalSettlement,
			COALESCE(SUM(CASE WHEN ClaimType LIKE '%Silica%' THEN SettlementAmount_MSAPaid END),0) AS SilicaSettlement,
			COALESCE(SUM(CASE WHEN ClaimType LIKE '%Other%' AND NCCReferenceID != 'GENERALEXPS' THEN SettlementAmount_MSAPaid END),0) AS OtherSettlement,
			COALESCE(SUM(CASE WHEN NCCReferenceID = 'GENERALEXPS' THEN SettlementAmount_MSAPaid END),0) AS GeneralSettlement

		FROM
			
			 tblClaimantSettlement CS
			LEFT JOIN tblClaimLawsuit CL ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
			LEFT JOIN tblClaim C ON C.ClaimID = CL.ClaimID
			LEFT JOIN tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID
			LEFT JOIN tblMaterial M ON M.MaterialID = A.MaterialID
			LEFT JOIN m_tblClaimType_StandardMSA CTS ON CTS.MaterialID = M.MaterialID

		  WHERE M.MaterialID != 10 AND PostingDate BETWEEN '1/1/2009' AND @DataDate

		  GROUP BY Year(PostingDate)
) AS a

FULL OUTER JOIN

(
		Select Year(PeriodEnd) AS yr,
			COALESCE(SUM(CASE WHEN ClaimType LIKE '%Asbestos%' THEN Costs + Fees END),0) AS AsbestosDefense,
			COALESCE(SUM(CASE WHEN ClaimType LIKE '%Coal%' THEN Costs + Fees END),0) AS CoalDefense,
			COALESCE(SUM(CASE WHEN ClaimType LIKE '%Silica%' THEN Costs + Fees END),0) AS SilicaDefense,
			COALESCE(SUM(CASE WHEN ClaimType LIKE '%Other%' AND NCCReferenceID != 'GENERALEXPS' THEN Costs + Fees END),0) AS OtherDefense,
			COALESCE(SUM(CASE WHEN NCCReferenceID = 'GENERALEXPS' THEN Costs + Fees END),0) AS GeneralDefense

			FROM

			tblDefenseInvoice DI
			LEFT JOIN tblDefInvClaimantDetail DICD ON DICD.DefenseInvoiceID = DI.DefenseInvoiceID
			LEFT JOIN tblClaim C ON C.ClaimID = DICD.ClaimID
			LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
			LEFT JOIN tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID
			LEFT JOIN tblMaterial M ON M.MaterialID = A.MaterialID
			LEFT JOIN m_tblClaimType_StandardMSA CTS ON CTS.MaterialID = M.MaterialID

			WHERE COALESCE(NCCReferenceID, 'Unknown') NOT LIKE '%LIT%'
			AND COALESCE(M.MaterialID, 100) != 10 AND PeriodEnd BETWEEN '1/1/2009' AND @DataDate

			GROUP BY Year(PeriodEnd)

) AS b

ON a.yr = b.yr

Group by COALESCE(a.yr, b.yr), a.AsbestosSettlement, b.AsbestosDefense, a.AsbestosSettlement+b.AsbestosDefense, 
	a.CoalSettlement, b.CoalDefense, a.CoalSettlement+b.CoalDefense, 
	a.SilicaSettlement, b.SilicaDefense, a.SilicaSettlement+b.SilicaDefense,
	a.OtherSettlement, b.OtherDefense, a.OtherSettlement+b.OtherDefense,  
	a.GeneralSettlement, b.GeneralDefense, a.GeneralSettlement+b.GeneralDefense


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptToxicTortReports_SpendByYearByType2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptToxicTortReports_SpendByYearByType2] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptToxicTortReports_SpendByYearByType2] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptToxicTortReports_SpendByYearByType2] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptToxicTortReports_SpendByYearByType2] TO [power_user]
GO
