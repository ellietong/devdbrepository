SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryRetenderClaims]
(@RetenderList tender_list_tbltype READONLY,  @emailaddress nvarchar(500))
AS
BEGIN
	SET NOCOUNT ON;

    --Test vars
    --DECLARE @emailaddress nvarchar(500) = 'crookc@kcicllc.com'
    --DECLARE @RetenderList tender_list_tbltype
    --INSERT INTO @RetenderList
    --SELECT 6789, 11
    --UNION
    --SELECT 6788, 19


    --SELECT * FROm tblTenderHistory WHERE ClaimID_old = 6789
    --SELECT * FROM tblClaimTenderHistory WHERE ClaimID = 6789
  --BEGIN TRAN
  
  --Get relevant tender history records for each claimant
	DECLARE @ClaimID bigint

	--Create tblTenderHistory & tblClaimTenderHistory records for actual withdrawals
	DECLARE @TenderBlockID int
	DECLARE tenderhistory_curs CURSOR FOR
	SELECT DISTINCT TenderBlockID
	FROM @RetenderList
	
	
	OPEN tenderhistory_curs
	FETCH NEXT FROM tenderhistory_curs INTO @TenderBlockID
	WHILE @@FETCH_STATUS=0
	BEGIN 
	
		--INSERT tblTenderHistory record		
		INSERT INTO tblTenderHistory (TenderBlockID, TenderTypeID, DateCreated, SPUsername)
		SELECT @TenderBlockID, 3, GETDATE(), @Emailaddress
		
		DECLARE @RetenderTHID bigint = (SELECT IDENT_CURRENT('tblTenderHistory'))

		--INSERT tblClaimTenderHistory records for new retender record
		INSERT INTO tblClaimTenderHistory (ClaimID, TenderHistoryID, TenderStatusID, SPUsername, PreviousClaimTenderHistoryID)
		SELECT rt.ClaimID, @RetenderTHID, 1, @emailaddress, pcth.ClaimTenderHistoryID
		FROM @RetenderList rt
		LEFT JOIN (SELECT TOP 1 cth.* FROM tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID WHERE TenderStatusID = 3 ORDER BY DateCreated DESC) pcth ON rt.ClaimID = pcth.ClaimID
		WHERE TenderBlockID = @TenderBlockID 
		
		--If there is only 1 template for the newly inserted retender record, default the TemplateID to that value
		DECLARE @TenderTemplate TABLE(TenderTemplateID int, TemplateDisplayName nvarchar(MAX))
		INSERT INTO @TenderTemplate
		EXEC [WSS_TENDER_qryTenderTemplateList] @RetenderTHID
		IF(SELECT COUNT(TenderTemplateID) FROM @TenderTemplate) = 2
		BEGIN
			UPDATE tblTenderHistory
			SET TenderTemplateID = (SELECT TenderTemplateID FROM @TenderTemplate WHERE TenderTemplateID IS NOT NULL)
			WHERE TenderHistoryID = @RetenderTHID
		END
		

	FETCH NEXT FROM tenderhistory_curs INTO @TenderBlockID
	END
	CLOSE tenderhistory_curs
	DEALLOCATE tenderhistory_curs
	
	
  
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryRetenderClaims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryRetenderClaims] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryRetenderClaims] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryRetenderClaims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryRetenderClaims] TO [power_user]
GO
