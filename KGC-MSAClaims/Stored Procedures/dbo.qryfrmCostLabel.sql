SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmCostLabel]
(@costID int = NULL)
AS
SET NOCOUNT ON

IF @costID IS NULL
	SELECT CostID, CostDescription
	FROM tblCostLabel
ELSE
	SELECT CostID, CostDescription
	FROM tblCostLabel
	WHERE CostID = @costID
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCostLabel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCostLabel] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmCostLabel] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCostLabel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCostLabel] TO [power_user]
GO
