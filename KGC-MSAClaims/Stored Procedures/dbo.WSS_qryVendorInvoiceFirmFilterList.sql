SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryVendorInvoiceFirmFilterList]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT ExpertFirmID, ExpertFirm FROM m_tblExpertFirm
	--UNION
	SELECT null as VendorFirmID, '<< All Expert Firms >>' As VendorFirm
	UNION
	SELECT VendorFirmID, VendorFirm FROM m_tblVendorFirm
	--ORDER BY ExpertFirm
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryVendorInvoiceFirmFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryVendorInvoiceFirmFilterList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryVendorInvoiceFirmFilterList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryVendorInvoiceFirmFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryVendorInvoiceFirmFilterList] TO [power_user]
GO
