SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_GetDocByDocID]
	@DocId int
AS
--DECLARE @SPDocId nvarchar(50)
--SET @SPDocId = 'c43ef74a-898e-40bb-a64f-a09ae1b9ed25'
BEGIN
	SELECT
	d.CreatedBy,
	COALESCE(d.Accepted, 0) AS Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	d.FirmID,
	cdt.DocumentGroupID,
	DocumentURL,
	DetailDocumentTypeID,
	StatusID,
	VendorFirmID,
	AmazonKey
FROM tblDocuments d
INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
WHERE DocumentID = @DocId
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_GetDocByDocID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_GetDocByDocID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_GetDocByDocID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_GetDocByDocID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_GetDocByDocID] TO [power_user]
GO
