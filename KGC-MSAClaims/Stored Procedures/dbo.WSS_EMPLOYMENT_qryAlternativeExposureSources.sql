SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryAlternativeExposureSources]
(@ClaimantEmploymentID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimantEmploymentID, 
		   StartDate, 
		   EndDate, 
		   cp.ProductID, 
		   ProductType, 
		   ClaimantProductID, 
		   Notes,
		   ProductManufacturer,
		   COALESCE(ProductManufacturer, 'UNSPECIFIED') + ' - ' + COALESCE(ProductName, 'UNSPECIFIED') + ' - ' + COALESCE(ProductType, 'UNSPECIFIED') AS FullProductName, 
		   ProductType,
		   ProductName,
		   d.DocumentTitle AS SupportingDocTitle, 
		   COALESCE(SupportingDocID,-1) AS SupportingDocID, 
		   SupportingPageNums
	FROM tblClaimantProduct cp
	INNER Join tblProduct p on p.ProductID = cp.ProductID
	LEFT Join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
	LEFT Join tblProductManufacturer pm on pm.ProductManufacturerID = p.ProductManufacturerID
	LEFT Join tblProductName pn on pn.ProductNameID = p.ProductNameID
	LEFT Join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
	LEFT JOIN tblDocuments d ON d.DocumentID = SupportingDocID
	WHERE ClaimantEmploymentID = @ClaimantEmploymentID
		AND p.ProductManufacturerID <> 1
	ORDER BY StartDate
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryAlternativeExposureSources] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryAlternativeExposureSources] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_EMPLOYMENT_qryAlternativeExposureSources] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryAlternativeExposureSources] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryAlternativeExposureSources] TO [power_user]
GO
