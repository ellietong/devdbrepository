SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryTenderCarrierList_REVERT] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	SELECT distinct c.CarrierID as TenderCarrierID, CarrierName as TenderCarrier
	FROM tblPolicy p
	inner join tblCarrier c on c.CarrierID = p.CarrierID
	UNION 
	SELECT NULL, ''
	ORDER BY TenderCarrier, TenderCarrierID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderCarrierList_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderCarrierList_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryTenderCarrierList_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderCarrierList_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderCarrierList_REVERT] TO [power_user]
GO
