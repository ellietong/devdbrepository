SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[qryrptFilterBySettlementGroup_FullyDocumented]
(@SettlementGroupID bigint)
As
Set NoCount On

--declare @SettlementGroupID bigint
--set @SettlementGroupID =6749



Select LastName,
FirstName,
cpi.ClaimantPersonalInfoID, 
DocketNumber, 
LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4)as SSN, 
CASE WHEN DeceasedDate IS NOT NULL and (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
		 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END AS SupportingDocumentation,
sd.ReleaseTemplateTypeID, ReleaseTemplateType, cl.ClaimLawsuitID, HighLevelDisease  
from tblClaimantPersonalInfo cpi
left join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
left join (Select * From tblClaimDisease Where PrimaryDisease = 1) cd on cd.ClaimID = c.ClaimID
left join tblDisease d on d.DiseaseID = cd.DiseaseID
left join tblHighLevelDisease hd on hd.HighLevelDiseaseID = d.HighLevelDiseaseID
left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
left join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblSettlementGroup sg on sg.SettlementGroupID = cs.SettlementGroupID
left join tblSettlePaymentDocumentation sd on sd.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblReleaseTemplateType rtt on rtt.ReleaseTemplateTypeID = sd.ReleaseTemplateTypeID
where cs.SettlementGroupID=@SettlementGroupID 
and (CASE WHEN DeceasedDate IS NOT NULL and (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
		 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END) = 'Complete'
GROUP BY LastName,cpi.ClaimantPersonalInfoID, FirstName, HighLevelDisease, sd.ReleaseTemplateTypeID, ReleaseTemplateType, cl.ClaimLawsuitID, DocketNumber, LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4),SettlementConfirmation, ClaimantIDed,DxMedicals,DODCert,EstatePapers,ProductID,EmployExpoHist,DeceasedDate
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptFilterBySettlementGroup_FullyDocumented] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptFilterBySettlementGroup_FullyDocumented] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFilterBySettlementGroup_FullyDocumented] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptFilterBySettlementGroup_FullyDocumented] TO [Claims_User]
GRANT ALTER ON  [dbo].[qryrptFilterBySettlementGroup_FullyDocumented] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFilterBySettlementGroup_FullyDocumented] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptFilterBySettlementGroup_FullyDocumented] TO [power_user]
GO
