SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryUpdClaimantRelatedParty]
(@ClaimantRelatedPartyID bigint, @FirstName nvarchar(500), @MiddleName nvarchar(500), @LastName nvarchar(500), @RelationshipID int, @LegalCapacityID int, @FullSSN nvarchar(15),
@RPAddress1 nvarchar(250), @RPAddress2 nvarchar(250), @RPCity nvarchar(250), @StateID int, @RPZip nvarchar(5), @RPMailZip nvarchar(5),
@RPPhone nvarchar(20), @RPExtension nvarchar(10))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE tblClaimantRelatedParty
    SET FirstName = UPPER(@FirstName),
		MiddleName = UPPER(@MiddleName),
		LastName = UPPER(@LastName),
		RelationshipID = @RelationshipID, 
		LegalCapacityID = @LegalCapacityID,
		RPSocialSecurityNumber = CASE WHEN @FullSSN = '0' OR LEN(@FullSSN) = 0 THEN NULL ELSE REPLACE(REPLACE(@FullSSN, '-', ''), ' ', '')  END ,
		RPAddress1 = UPPER(@RPAddress1),
		RPAddress2 = UPPER(@RPAddress2),
		RPCity = UPPER(@RPCity),
		StateID = @StateID,
		RPZip = @RPZip,
		RPMailZip = @RPMailZip,
		RPPhone = @RPPhone,
		RPExtension = @RPExtension
	WHERE ClaimantRelatedPartyID = @ClaimantRelatedPartyID
	
END

--CPI.ClaimantPersonalInfoID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
--                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
--                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, Gender, cpi.GenderID, cpi.MaritalStatusID,
--                      MaritalStatus, coalesce(nullif(coalesce(Address1 + CHAR(13) + CHAR(10), '') + coalesce(Address2 + CHAR(13) + CHAR(10),'') + 
--						coalesce(City + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,Zip),'') + Coalesce('-'+convert(varchar,mailzip),''),''),'') as CompleteAddress, BirthDate, DeceasedDate, City, cpi.StateID, Zip, MailZip, Address1, Address2
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantRelatedParty] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdClaimantRelatedParty] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantRelatedParty] TO [power_user]
GO
