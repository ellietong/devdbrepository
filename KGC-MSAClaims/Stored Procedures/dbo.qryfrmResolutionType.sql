SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmResolutionType] as

Select ResolutionTypeID, ResolutionType
From tblResolutionType
Order By ResolutionType

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmResolutionType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmResolutionType] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmResolutionType] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmResolutionType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmResolutionType] TO [power_user]
GO
