SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryAcknowledgeTender]
(@TenderHistoryID bigint, @emailaddress nvarchar(500))
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE tblTender_Acknowledgement
	SET AcknowledgementDate = GETDATE()
	FROM tblTender_Acknowledgement ta
	INNER JOIN vtblPerson p ON ta.PersonID = p.PersonID
	WHERE AcknowledgementDate IS NULL
			AND p.Email LIKE @emailaddress
			AND TenderHistoryID = @TenderHistoryID

	
	--Return the AmazonKey
	SELECT AmazonKey, ack.AcknowledgementDate, @emailaddress AS Email
	FROM tblTender_History th 
	INNER JOIN tblDocuments d ON th.DocumentID = d.DocumentID
	LEFT JOIN (SELECT ta.* FROM tblTender_Acknowledgement ta
				INNER JOIN vTenderContacts tc ON ta.PersonID = tc.PersonID
				INNER JOIN vtblPerson p ON tc.PersonID = p.PersonID
				WHERE p.Email = @emailaddress) ack ON th.TenderHistoryID = ack.TenderHistoryID
	WHERE th.TenderHistoryID = @TenderHistoryID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryAcknowledgeTender] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryAcknowledgeTender] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryAcknowledgeTender] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryAcknowledgeTender] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryAcknowledgeTender] TO [power_user]
GO
