SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE PROC [dbo].[qryfrmSiteName]
AS
 
SET CONCAT_NULL_YIELDS_NULL OFF
 
SELECT SiteID, SiteName +' ('+State+', '+country+')'
FROM  tblEnviroSitesDetail A
LEFT JOIN tblstate B on a.stateid = b.stateid
LEFT JOIN tblCountry C on c.countryId = a.countryid
ORDER BY sitename
 
 
RETURN
 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteName] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteName] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmSiteName] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteName] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteName] TO [power_user]
GO
