SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantProductInfo] (@ClaimantPersonalInfoID int)
as

Select ClaimantProductID, 
	ClaimantPersonalInfoID, 
	ProductName, 
	Brand, 
	ProductType, 
	Manufacturer, 
	StartDate, 
	EndDate
From tblClaimantProduct
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
Order By ProductType, ProductName

Return

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantProductInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantProductInfo] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmClaimantProductInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantProductInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantProductInfo] TO [power_user]
GO
