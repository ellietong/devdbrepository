SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryAddMultiClaimantDocument]
(@ClaimantPersonalInfoID bigint, @DocumentID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblMultiClaimantDocument
	(ClaimantPersonalInfoID, DocumentID)
	VALUES(@ClaimantPersonalInfoID, @DocumentID)
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAddMultiClaimantDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAddMultiClaimantDocument] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryAddMultiClaimantDocument] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAddMultiClaimantDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAddMultiClaimantDocument] TO [power_user]
GO
