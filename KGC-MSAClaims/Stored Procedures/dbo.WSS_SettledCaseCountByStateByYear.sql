SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[WSS_SettledCaseCountByStateByYear] (@login nvarchar(100) = null)

AS
Set NoCount ON

Declare @AllResponsibleDefenseCounsel table (DefenseCounselAddressID int, 
				DefenseCounsel nvarchar(100), 
				ResponsibleDefenseCounselAddressID int,
				StateID int, 
				ResponsibleDefenseCounsel nvarchar(100),
				IsActiveCounsel bit)
Insert Into @AllResponsibleDefenseCounsel
Select dcs.DefenseCounselAddressID,
	d.DefenseCounsel as DefenseCounsel,
	dcs.responsibledefensecounselAddressID,
	StateID as StateID,
	r.DefenseCOunsel as ResponsibleDefenseCOunsel,
	IsActiveCounsel
From tblDefenseCounselState dcs 
	Inner join tblDefenseCounselAddress da on da.DefenseCounselAddressID = dcs.defensecounselAddressID
	Inner Join tblDefenseCounsel d on d.defensecounselID = da.defensecounselID
	INNER JOIN tblDefenseCounselAddress dar on dar.DefenseCounselAddressID = dcs.responsibledefensecounseladdressid
	Inner Join tblDefenseCounsel r on r.defensecounselID = dar.defensecounselID
Where r.DefenseCounselID = (Select DefenseCounselID
From tblDefenseCounsel dc
Inner Join vtblPerson p ON p.FirmID = dc.DefenseCounselID
Where (email = (Select Case When right(p.email,12) Not Like '@kcicllc.com' and right(p.email,8) Not Like 'kcic.com' Then p.email Else 'melaniemorrison@phsm.net' END AS 'LoginFiltered'
FROM vtblPerson p
WHERE p.email = @Login)))
Group By d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCOunseladdressID, dcs.responsibledefensecounseladdressID, IsActiveCounsel, StateID

Select  State, year(filedate) AS 'Year', count(DocketNumber) AS 'Count'
From tblClaimantPersonalInfo CPI
	Left Join tblClaim C on CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
	Left Join tblClaimLawsuit CL on C.ClaimID = CL.ClaimID
	Left Join tblLawsuit L on CL.LawsuitID = L.LawsuitID
	Left Join tblState S on S.StateID = L.StateID
	Left Join tblJurisdiction J on J.JurisdictionID = L.JurisdictionID
	LEFT JOIN dbo.tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = CL.ClaimLawsuitID
	INNER Join @AllResponsibleDefenseCounsel r on r.DefenseCounselAddressID = l.DefenseCounselAddressID and r.StateID = l.StateID
Where	
	IsPrimaryForClaim = 1 
	and IsActiveCounsel = 1
	and ClaimLawsuitStatusID IN (23,25,28)
	AND IsPrimaryStatus = 1
	AND year(filedate) IS NOT NULL
Group By year(filedate), state
ORDER BY state, year(filedate)

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SettledCaseCountByStateByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SettledCaseCountByStateByYear] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SettledCaseCountByStateByYear] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SettledCaseCountByStateByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SettledCaseCountByStateByYear] TO [power_user]
GO
