SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[WSS_qryMaritalStatus]
as

Select MaritalStatusID, MaritalStatus
From tblMaritalStatus
UNION
SELECT NULL, ''
Order By MaritalStatus

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryMaritalStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryMaritalStatus] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryMaritalStatus] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryMaritalStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryMaritalStatus] TO [power_user]
GO
