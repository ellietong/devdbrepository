SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptQuarterlyClaimsReport_NewSettlements] (@Quarter int, @Year varchar(max)) 
AS
SET NOCOUNT ON





--get last quarter start date
DECLARE @PreviousQuarterStart varchar(25)
SET @PreviousQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '1/1/' + @Year
		 WHEN @Quarter = 2 THEN '4/1/' + @Year
		 WHEN @Quarter = 3 THEN '7/1/' + @Year
		 WHEN @Quarter = 4 THEN '10/1/' + @Year
		 ELSE '1/1/2050' END

--get last quarter end date
DECLARE @PreviousQuarterEnd varchar(25)
SET @PreviousQuarterEnd = 
	CASE WHEN @Quarter = 1 THEN '3/31/' + @Year
		 WHEN @Quarter = 2 THEN '6/30/' + @Year
		 WHEN @Quarter = 3 THEN '9/30/' + @Year
		 WHEN @Quarter = 4 THEN '12/31/' + @Year
		 ELSE '12/31/2050' END
		 
		 
	SELECT FullNameState,
		   CauseAgent AS Material,
		   NCCReferenceID, 
		   LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,'') as Claimant,
		   CaseCaption,
		   DocketNumber,
		   ClaimLawsuitStatus,
		   Convert(varchar, FileDate, 101) as FileDate,
		   Convert(varchar, StatusDate, 101) as StatusDate,
		   Convert(varchar, ActiveTrialDate, 101) as ActiveTrialDate,
		   Convert(varchar, ProcessedDate, 101) as ProcessedDate,
		   SettlementAmount
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		  LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN tblState s on s.StateID = l.StateID
		  LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		  LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	WHERE clst.IsPrimaryStatus = 1
		  AND COALESCE(ProcessedDate, StatusDate,'1/1/1900') <= @PreviousQuarterEnd AND COALESCE(ProcessedDate, StatusDate,'1/1/1900')  >= @PreviousQuarterStart
		  AND m.MaterialID != 10
		  AND cls.ClaimLawsuitStatusGroupID = 1
		  AND c.ClaimID NOT IN (SELECT ClaimID 
								FROM tblClaimTenderHistory CTH
								INNER JOIN tblTenderHistory TH ON TH.TenderHistoryID = CTH.TenderHistoryID
								WHERE TenderStatusID in (2, 5) and tenderblockid = 15 and TenderTypeID <> 2)
		AND C.ClaimID NOT IN (SELECT ClaimID
					FROM tblTender_History TH
					INNER JOIN tblTender_Acknowledgement TA ON TA.TenderHistoryID = TH.TenderHistoryID
					WHERE PersonID = 3279 AND TenderTypeID <> 2)
		  AND TortTypeID = 1
		  AND c.ClaimID != 26234
	GROUP BY FullNameState,
		   CauseAgent,
		   NCCReferenceID,
		   LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,''),
		   CaseCaption,
		   DocketNumber,
		   ClaimLawsuitStatus,
		   Convert(varchar, FileDate, 101),
		   Convert(varchar, StatusDate, 101),
		   Convert(varchar, ActiveTrialDate, 101),
		   Convert(varchar, ProcessedDate, 101),
		   SettlementAmount
	ORDER BY FullNameState, CauseAgent, ProcessedDate, LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,'')


RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_NewSettlements] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_NewSettlements] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptQuarterlyClaimsReport_NewSettlements] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_NewSettlements] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_NewSettlements] TO [power_user]
GO
