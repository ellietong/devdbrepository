SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_SettlementReport]
AS

SELECT     C.ClaimID, DHLD.HighLevelDisease AS Disease, S.state, J.Jurisdiction, ClaimS.ClaimSequence, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE RIGHT(SocialSecurityNumber, 4) 
                      END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CONVERT(date, CPI.DeceasedDate) AS DeceasedDate, CASE WHEN Month(BirthDate) 
                      < Month(SettlementDate) THEN DateDiff(yy, BirthDate, SettlementDate) WHEN Month(BirthDate) > Month(SettlementDate) THEN DateDiff(yy, BirthDate, 
                      SettlementDate) - 1 WHEN Month(BirthDate) = Month(SettlementDate) THEN CASE WHEN Day(BirthDate) > Day(SettlementDate) THEN DateDiff(yy, 
                      BirthDate, SettlementDate) - 1 ELSE DateDiff(yy, BirthDate, SettlementDate) END END AS SettlementAge, CASE WHEN Month(BirthDate) 
                      < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) THEN DateDiff(yy, BirthDate, 
                      DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) > Day(DeceasedDate) THEN DateDiff(yy, 
                      BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      CASE WHEN DeceasedDate < SettlementDate THEN 'Yes' ELSE 'No' END AS DeceasedAtSettlement, CE.Occupation, CE.Jobsite, CONVERT(date, 
                      CE.EarliestExposureDate) AS EarliestExposureDate, CONVERT(date, CE.LatestExposureDate) AS LatestExposureDate, 
                      CASE WHEN CL.ClaimLawsuitStatusID = 25 THEN 'SBND' WHEN CL.ClaimLawsuitStatusID = 28 THEN 'Zero Dollar' ELSE ClaimLawsuitStatus END AS ClaimLawsuitStatus,
                       CONVERT(date, CS.SettlementDate) AS SettlementDate, CS.SettlementAmount, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, 
                      L.FileDate) AS FileDate, L.DocketNumber,  pca.PlaintiffCounsel, dc.DefenseCounsel,
                                    --Added by BH 6/25/09 to assist in summary statistics in reporting
                                    Case when birthdate is null then 0 else 1 end as NonNullAgeCount,
                                    CASE WHEN DeceasedDate < SettlementDate THEN 1 ELSE 0 END AS DeadAtSettledCount,
                                     CASE when BirthDate IS NULL then 0
                                     WHEN Month(BirthDate) 
                      < Month(SettlementDate) THEN coalesce(DateDiff(yy, BirthDate, SettlementDate),0) WHEN Month(BirthDate) > Month(SettlementDate) THEN coalesce(DateDiff(yy, BirthDate, 
                      SettlementDate) - 1,0) WHEN Month(BirthDate) = Month(SettlementDate) THEN CASE WHEN Day(BirthDate) > Day(SettlementDate) THEN coalesce(DateDiff(yy, 
                      BirthDate, SettlementDate) - 1,0) ELSE coalesce(DateDiff(yy, BirthDate, SettlementDate),0) 
                      END END AS SettlementAgeWithZeros,
                      coalesce(settlementamount, 0) as SettlementAmountNoNulls,
                      Case when SettlementAmount IS not null then 1 else 0 end as NonNullSettlementCount
FROM         dbo.tblClaimantPersonalInfo AS CPI LEFT OUTER JOIN
                      dbo.tblClaimantEmployment AS CE ON CPI.ClaimantPersonalInfoID = CE.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID LEFT OUTER JOIN
                      (SELECT * FROM tblClaimDisease CD WHERE PrimaryDisease = 1) AS CD ON CD.ClaimID = C.ClaimID LEFT OUTER JOIN
                      dbo.tblDisease AS DDisease ON CD.DiseaseID = DDisease.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS DHLD ON DHLD.HighLevelDiseaseID = DDisease.HighLevelDiseaseID LEFT OUTER JOIN
                      dbo.tblClaimSequence AS ClaimS ON ClaimS.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblClaimLawsuitStatus AS CLS ON CL.ClaimLawsuitStatusID = CLS.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS CS ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
					LEFT JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = L.defensecounseladdressid
					left join dbo.tblDefenseCounsel  DC ON dca.defensecounselid = DC.DefenseCounselID
					LEFT JOIN dbo.tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
WHERE     (CE.IsPrimary = 1) AND (CL.ClaimLawsuitStatusID IN (23, 25, 28)) AND (CL.IsPrimaryForClaim = 1) AND (CS.SettlementDate >= '2/1/2001')
GROUP BY C.ClaimID, DHLD.HighLevelDisease, S.state, J.Jurisdiction, ClaimS.ClaimSequence, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE RIGHT(SocialSecurityNumber, 4) END, CPI.BirthDate, 
                      CPI.DeceasedDate, CASE WHEN Month(BirthDate) < Month(SettlementDate) THEN DateDiff(yy, BirthDate, SettlementDate) WHEN Month(BirthDate) 
                      > Month(SettlementDate) THEN DateDiff(yy, BirthDate, SettlementDate) - 1 WHEN Month(BirthDate) = Month(SettlementDate) 
                      THEN CASE WHEN Day(BirthDate) > Day(SettlementDate) THEN DateDiff(yy, BirthDate, SettlementDate) - 1 ELSE DateDiff(yy, BirthDate, 
                      SettlementDate) END END, CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) 
                      WHEN Month(BirthDate) > Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) 
                      THEN CASE WHEN Day(BirthDate) > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) 
                      END END, CASE WHEN DeceasedDate < SettlementDate THEN 'Yes' ELSE 'No' END, CE.Occupation, CE.Jobsite, CE.EarliestExposureDate, 
                      CE.LatestExposureDate, 
                      CASE WHEN CL.ClaimLawsuitStatusID = 25 THEN 'SBND' WHEN CL.ClaimLawsuitStatusID = 28 THEN 'Zero Dollar' ELSE ClaimLawsuitStatus END, 
                      CS.SettlementDate, CS.SettlementAmount, L.ServiceDate, L.FileDate, L.DocketNumber,  pca.PlaintiffCounsel, dc.DefenseCounsel
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SettlementReport] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SettlementReport] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SettlementReport] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SettlementReport] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SettlementReport] TO [power_user]
GO
