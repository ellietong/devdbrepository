SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_INVOICES_NewInvoiceSearch]
(@FirmID int, @StatusID bigint, @InvoiceNumber nvarchar(50), @PaidDateStart date, @PaidDateEnd date, @InvoicePeriodStart date, @InvoicePeriodEnd date,@emailaddress nvarchar(250))
AS

------VARIABLES FOR TESTING----
--DECLARE @FirmID int
--DECLARE @VendorFirmID int
--DECLARE @StatusID bigint
--DECLARE @PaidDateStart date
--DECLARE @PaidDateEnd date
--DECLARE @Period nvarchar(50)
--DECLARE @emailaddress nvarchar(250)
--DECLARE @InvoiceNumber nvarchar(250)
--SET @FirmID		   = NULL
--SET @VendorFirmID  = 0
--SET @StatusID	   = NULL
--SET @PaidDateStart = NULL
--SET @PaidDateEnd   = NULL
--SET @Period		   = NULL
--SET @emailaddress  = 'hagep@kcicllc.com'
--SET @InvoiceNumber=null

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE 1=1'
IF @FirmID				IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.FirmID = ' + CONVERT(nvarchar(20),@FirmID) END
--IF @VendorFirmID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.VendorFirmID = ' + CONVERT(nvarchar(20),@VendorFirmID) END
IF @StatusID			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND dis.StatusID = ' + CONVERT(nvarchar(20),@StatusID) END
IF @InvoiceNumber		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.InvoiceNumber LIKE ''%'' + @InvoiceNumberParam + ''%''' END
IF @PaidDateStart		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND CONVERT(date, di.DatePaid) >= ''' + CONVERT(nvarchar(20),@PaidDateStart) + '''' END
IF @PaidDateEnd			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND CONVERT(date, di.DatePaid) <= ''' + CONVERT(nvarchar(20),@PaidDateEnd) + '''' END
IF @InvoicePeriodStart	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(PeriodStart, CONVERT(date, ''1/1/1900'')) >= ''' + CONVERT(nvarchar(20),@InvoicePeriodStart) + ''''  END
IF @InvoicePeriodEnd	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(PeriodEnd, CONVERT(date, ''1/1/1900'')) <= ''' + CONVERT(nvarchar(20),@InvoicePeriodEnd) + '''' END

--These IF statements determine additional WHERE statements by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.

DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

DECLARE @JoinString nvarchar(max) -- Colfax (363) and LeaderBerkon ( can see everything 
IF (@DefenseCounselID IN (374, 363)) OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com')
	SET @JoinString = ' '
--ELSE IF (@DefenseCounselID IN (63))
--BEGIN
--	SET @JoinString = ' '
--	SET @WhereString = @WhereString + ' AND di.FirmID IN (340)'
--END
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND di.FirmID = ' + CONVERT(nvarchar(20),@DefenseCounselID)
END

DECLARE @QueryString nvarchar(max)
SET @QueryString = '
	SELECT 
		di.ApprovedBy, 
		di.DateApproved, 
		dc.DefenseCounsel as Firm,
		di.InvoiceNumber, 
		di.Period,	
		di.DatePayable, 
		--dcd.Fees as feeamount, 
		--dcd.costs as disbursementamount, 
		--dcd.adjustments as Adjustment,
		--di.InvoiceNotes as AdjustmentNotes, 
		dis.Status, '''' AS DefenseInvoiceFirm, 
		di.DatePaid as PaidDate,
		--c.SourceDate as PaidDate, 
		--c.SourceNumber as CheckNumber, 
		di.FirmID, 
		COALESCE(ex.VendorName, '''') AS VendorFirm, 
		--c.SourceDate, 
		COALESCE (SUM(dcd.Fees), 0) + COALESCE (SUM(dcd.Costs), 0) AS InvoiceTotal
		
	FROM tblDefenseInvoice di	
		LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID 
		Left JOIN tblDefInvClaimantDetail dcd ON di.DefenseInvoiceID = dcd.DefenseInvoiceID
		LEFT JOIN tblDefenseCounsel dc		  ON di.FirmID = dc.DefenseCounselID
		LEFT JOIN tblVendor ex on ex.VendorID = di.VendorfirmID
		LEFT JOIN tblPaymentDefense pd		  ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		--LEFT JOIN tblCheck c				  ON pd.CheckID = c.CheckID

		 ' +
	@WhereString + '
	GROUP BY
		di.ApprovedBy, 
		di.DateApproved, 
		dc.DefenseCounsel ,
		di.InvoiceNumber, 
		di.Period,	
		di.DatePayable, 
		--dcd.Fees , 
		--dcd.costs , 
		--dcd.adjustments ,
		di.InvoiceNotes , 
		dis.Status,  
		di.DatePaid,
		--c.SourceDate , 
		--c.SourceNumber , 
		di.FirmID, 
		COALESCE(ex.VendorName, '''') 
		--c.SourceDate 
	ORDER BY Period DESC 
	OPTION (RECOMPILE)'
	
Print @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@InvoiceNumberParam nvarchar(250)', @InvoiceNumberParam = @InvoiceNumber




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_NewInvoiceSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_NewInvoiceSearch] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_INVOICES_NewInvoiceSearch] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_NewInvoiceSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_NewInvoiceSearch] TO [power_user]
GO
