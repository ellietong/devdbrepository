SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_qryAMHomeTenders]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select d.DocumentID, d.DocumentTitle, d.DocumentURL from tblTenderHistory th
	inner join tblDocuments d on th.DocumentID = d.DocumentID
	where th.TenderBlockID in (22,23)
	and th.DateApproved is not null
	group by d.documentid, d.DocumentTitle, d.DocumentURL 
	order by d.DocumentID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[m_qryAMHomeTenders] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qryAMHomeTenders] TO [base_user]
GRANT ALTER ON  [dbo].[m_qryAMHomeTenders] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_qryAMHomeTenders] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_qryAMHomeTenders] TO [power_user]
GO
