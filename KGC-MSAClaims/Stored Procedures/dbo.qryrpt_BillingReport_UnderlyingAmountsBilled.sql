SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE procedure [dbo].[qryrpt_BillingReport_UnderlyingAmountsBilled] 
(@ExhastionTypeID INT = NULL, @CarrierGroupID INT = NULL, @CurrentPeriodStart Date = NULL)
AS

SET NOCOUNT ON

--DECLARE @ExhastionTypeID INT
--DECLARE @CarrierGroupID INT
--DECLARE @CurrentPeriodStart Date

--SET @ExhastionTypeID = 6
--SET @CarrierGroupID = 3
--SET @CurrentPeriodStart = '12/31/2013'

--Return information about Policy

DECLARE @CarrierInfo TABLE (CarrierGroupName nvarchar(200), CarrierName nvarchar(200), PolicyID int, PolicyNum nvarchar(200), AnnualPeriod int, 
StartDate datetime, EndDate datetime, AttachmentPoint MONEY, ProductsLimit MONEY, LayerLimit MONEY)
INSERT INTO @CarrierInfo
SELECT cg.CarrierGroupName, c.CarrierName, p.PolicyID, PolicyNum, AnnualPeriod, PStartDate, PEndDate, AttachmentPoint, PerOccLimit, LayerPerOccLimit
from tblPolicy p
inner join tblPolicyLimits pl on pl.PolicyID = p.PolicyID
inner join tblPolicyDates pd on pd.PolicyID = p.PolicyID
inner join tblCarrier c on c.CarrierID = p.CarrierID
inner join tblCarrierGroup cg on cg.CarrierGroupID = c.CarrierGroupID
where c.CarrierGroupID = @CarrierGroupID and limittypeid = 4

--select *
--from @CarrierInfo

--Find allPolicyInformation
Declare @Policies TABLE (CarrierGroupName nvarchar(200), CarrierName nvarchar(200), PolicyID int, PolicyDateID INT, PolicyNum nvarchar(200), AnnualPeriod int, 
StartDate datetime, EndDate datetime, AttachmentPoint MONEY, ProductsLimit MONEY, LayerLimit MONEY)
INSERT INTO @Policies
SELECT cg.CarrierGroupName, c.CarrierName, p.PolicyID, PolicyDateID, p.PolicyNum, AnnualPeriod, PStartDate, PEndDate, AttachmentPoint, PerOccLimit, LayerPerOccLimit
from tblPolicy p
inner join tblPolicyLimits pl on pl.PolicyID = p.PolicyID
inner join tblPolicyDates pd on pd.PolicyID = p.PolicyID
inner join tblCarrier c on c.CarrierID = p.CarrierID
inner join tblCarrierGroup cg on cg.CarrierGroupID = c.CarrierGroupID
where limittypeid = 4

DECLARE @Exhaustion TABLE (ExhaustionTypeID INT, PolicyDateID INT, IndemnityAllocation Money, DefenseAllocation MONEY)

INSERT INTO @Exhaustion (PolicyDateID, IndemnityAllocation, DefenseAllocation)
SELECT E.PolicyDateID, sum(IndemnityAllocation), sum(DefenseAllocation)
FROM tblExhaustion E
INNER JOIN tblExhaustionDate ED ON ED.ExhaustionDateID = E.ExhaustionDateID
INNER JOIN tblPolicyDates PD ON PD.PolicyDateID = E.PolicyDateID
WHERE ExhaustionTypeID = @ExhastionTypeID and ExhaustionDate <= @CurrentPeriodStart
GROUP BY E.PolicyDateID

--Find the underlying Policies of the CarrierGroupID policies
declare @Underlying table (CarrierGroupName nvarchar(200), CarrierName nvarchar(200), PolicyID int, PolicyNum nvarchar (200), AnnualPeriod int, 
StartDate datetime, EndDate datetime, AttachmentPoint money, ProductsLimit money, LayerLimit money, LeadPolicy nvarchar(200), Exhaustion MONEY, 
RemainingLimit money, LeadCarrierName nvarchar(200), LeadAttachment money, Leadperocclimit money, Leadlayerlimit money, LeadPolicyID int,
LeadPolicyStartDate datetime, LeadPolicyEndDate datetime)
insert into @Underlying 
SELECT po.CarrierGroupName, po.CarrierName, po.PolicyID, po.PolicyNum, po.AnnualPeriod, po.StartDate, po.EndDate, po.AttachmentPoint, po.ProductsLimit, 
po.LayerLimit, ci.PolicyNum, 
coalesce(IndemnityAllocation + DefenseAllocation,0), 
CASE WHEN Po.PolicyDateID IN (333, 338, 421, 344, 422) THEN 0 ELSE
	(po.ProductsLimit - isnull(IndemnityAllocation + DefenseAllocation,0)) END as RemainingLimits,
	ci.CarrierName, ci.AttachmentPoint, ci.ProductsLimit, ci.LayerLimit, ci.policyid, ci.StartDate, ci.EndDate
from @Policies po
	--full outer join @CarrierInfo ci on ci.PolicyID = po.PolicyID
	left join @CarrierInfo ci on ci.AnnualPeriod = po.AnnualPeriod
	LEFT JOIN @Exhaustion E ON E.PolicyDateID = PO.PolicyDateID
where po.annualperiod in (select AnnualPeriod from @CarrierInfo) and po.AttachmentPoint < ci.attachmentpoint
group by po.CarrierGroupName, po.CarrierName, po.PolicyNum, po.policyid, po.AnnualPeriod, po.StartDate, po.EndDate, po.AttachmentPoint, po.ProductsLimit, 
po.LayerLimit, ci.PolicyNum, ci.CarrierName, ci.AttachmentPoint, ci.ProductsLimit, ci.LayerLimit, ci.PolicyID, ci.StartDate, ci.EndDate,
coalesce(IndemnityAllocation + DefenseAllocation,0), CASE WHEN Po.PolicyDateID IN (333, 338, 421, 344, 422) THEN 0 ELSE
	(po.ProductsLimit - isnull(IndemnityAllocation + DefenseAllocation,0)) END
 
select CarrierGroupName, CarrierName, PolicyID , PolicyNum , AnnualPeriod , StartDate , EndDate , AttachmentPoint , ProductsLimit , LayerLimit , 
LeadPolicy , SUM(Exhaustion) AS Exhaustion , SUM(RemainingLimit) AS RemainingLimit , LeadCarrierName , LeadAttachment , Leadperocclimit, Leadlayerlimit, leadpolicyid, leadpolicystartdate, leadpolicyenddate
from @Underlying 
GROUP BY CarrierGroupName, CarrierName, PolicyID , PolicyNum , AnnualPeriod , StartDate , EndDate , AttachmentPoint , ProductsLimit , LayerLimit , 
LeadPolicy, LeadCarrierName , LeadAttachment , Leadperocclimit, Leadlayerlimit, leadpolicyid, leadpolicystartdate, leadpolicyenddate 
order by annualperiod, AttachmentPoint

--select * from @Underlying u
--union
--select * from @CarrierInfo ci


--order by u.AnnualPeriod, u.AttachmentPoint 


--select * from @Underlying u
--	full outer join @CarrierInfo ci on ci.AnnualPeriod = u.AnnualPeriod
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrpt_BillingReport_UnderlyingAmountsBilled] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrpt_BillingReport_UnderlyingAmountsBilled] TO [base_user]
GRANT ALTER ON  [dbo].[qryrpt_BillingReport_UnderlyingAmountsBilled] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrpt_BillingReport_UnderlyingAmountsBilled] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrpt_BillingReport_UnderlyingAmountsBilled] TO [power_user]
GO
