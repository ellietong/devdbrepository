SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmLanguage]
(@provisionLabelID int)
As

SET NOCOUNT ON

DECLARE @maxProvisionLabelID int

SELECT @maxProvisionLabelID  = MAX(ProvisionLabelID) FROM tblProvisionLabel

 
IF @provisionLabelID IS NULL
	SELECT pr.ProvisionLabelID, pr.LanguageID, PolicyLanguage
	FROM tblProvision pr INNER JOIN tblLanguage l ON pr.LanguageID = l.LanguageID
	GROUP BY pr.ProvisionLabelID, pr.LanguageID, PolicyLanguage
	ORDER BY pr.ProvisionLabelID
ELSE
	SELECT pr.ProvisionLabelID, pr.LanguageID, PolicyLanguage
	FROM tblProvision pr INNER JOIN tblLanguage l ON pr.LanguageID = l.LanguageID
	WHERE pr.ProvisionLabelID >= COALESCE(@provisionLabelID, 0) AND pr.ProvisionLabelID <= COALESCE(NULLIF(@provisionLabelID, 0), @maxProvisionLabelID)
	GROUP BY pr.ProvisionLabelID, pr.LanguageID, PolicyLanguage
	ORDER BY pr.ProvisionLabelID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLanguage] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLanguage] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmLanguage] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLanguage] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLanguage] TO [power_user]
GO
