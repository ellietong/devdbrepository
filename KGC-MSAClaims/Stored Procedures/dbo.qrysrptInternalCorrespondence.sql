SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[qrysrptInternalCorrespondence]
(@CorrespondenceID as int)
AS
SET NOCOUNT ON

SELECT c.CorrespondenceID, rt.RecipientType, COALESCE (ic.FirstName, '') + ' ' + 
	COALESCE (ic.LastName, '') + '   ' + ' ' + COALESCE (ic.[Address Line 1], '') 
	+ '   ' + COALESCE (ic.[Address Line 2], '') + '  ' + COALESCE (ic.City, '') 
	+ ', ' + COALESCE (ic.State, '') AS INContactMerge
FROM     tblCorrespondenceParties cp FULL OUTER JOIN
         tblRecipientType rt ON cp.mRecipientTypeID = rt.RecipientTypeID FULL OUTER JOIN
         tblCorrespondence c LEFT OUTER JOIN
         tblCorrespondenceMode cm ON c.CorrespondenceModeID = cm.CorrespondenceModeID LEFT OUTER JOIN
         tblCorrespondenceType ct ON c.CorrespondenceTypeID = ct.CorrespondenceTypeID ON cp.CorrespondenceID = c.CorrespondenceID FULL OUTER JOIN
         tblInternalContact ic ON cp.InternalContactID = ic.InternalContactID
WHERE @CorrespondenceID = c.CorrespondenceID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptInternalCorrespondence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptInternalCorrespondence] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptInternalCorrespondence] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptInternalCorrespondence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptInternalCorrespondence] TO [power_user]
GO
