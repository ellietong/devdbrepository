SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptQuarterlyClaimsReport_NewDefense_CNAOnly]
 (@Quarter int, @Year varchar(max), @NextYear varchar(max)) 
AS
SET NOCOUNT ON
/*=================================================
Date: 02/10/2015
Author: Removed join to m_tblInsurableClaims and
changed some left joins to inner join as they have
conditions in the where claus.  Added Noluck to all
the joining tables.

execute  [dbo].[qryrptQuarterlyClaimsReport_NewDefense_CNAOnly]
@Quarter = 3
,@Year = '2014'
,@NextYear = '2015'
===================================================*/



--Declare @Quarter int
--Set @Quarter = 4
--Declare @Year nvarchar(25)
--Set @Year = '2014'
--Declare @NextYear nvarchar(25)
--Set @NextYear = '2015'



--get last quarter start date
DECLARE @PreviousQuarterStart varchar(25)
SET @PreviousQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '1/1/' + @Year
		 WHEN @Quarter = 2 THEN '4/1/' + @Year
		 WHEN @Quarter = 3 THEN '7/1/' + @Year
		 WHEN @Quarter = 4 THEN '10/1/' + @Year
		 ELSE '1/1/2050' END

--get first day of next quarter
DECLARE @NextQuarterStart varchar(25)
SET @NextQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '4/1/' + @Year
		 WHEN @Quarter = 2 THEN '7/1/' + @Year
		 WHEN @Quarter = 3 THEN '10/1/' + @Year
		 WHEN @Quarter = 4 THEN '1/1/' + @NextYear
		 ELSE '12/31/2050' END

	SELECT CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END AS Material, 
		   SUM(paymentamount) AS Defense,
		   CASE WHEN COALESCE(dbo.fnEarliestExposure(C.ClaimantPersonalInfoID), '1/1/1900') <= '4/1/1985' THEN 'Yes' ELSE 'No' END AS Pre1985Designate,
		   FullNameState,
		   CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'GENERALEXPS' ELSE NCCReferenceID END AS NCCReferenceID,
		   LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,'') as Claimant,
		   CaseCaption,
		   DocketNumber,
		   ClaimLawsuitStatus,
		   Convert(varchar, FileDate, 101) as FileDate,
		   Convert(varchar, StatusDate, 101) as StatusDate,
		   Convert(varchar, ActiveTrialDate, 101) as ActiveTrialDate,
		   Convert(varchar, ProcessedDate, 101) as ProcessedDate,
		   Convert(varchar, SourceDate, 101) as SourceDate
	FROM dbo.tblDefenseInvoice(nolock) di
		LEFT JOIN dbo.tblDefInvClaimantDetail(nolock)  dicd on dicd.DefenseInvoiceID = di.DefenseInvoiceID
		INNER JOIN dbo.tblClaim(nolock)  c on c.ClaimID = dicd.ClaimID
		LEFT JOIN dbo.tblClaimLawsuit(nolock)  cl ON cl.claimID = c.claimID
		left join dbo.tblClaimantSettlement(nolock)  cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN dbo.tblAllegation(nolock)  a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN dbo.tblMaterial(nolock)  m on m.MaterialID = a.MaterialID
		LEFT JOIN dbo.tblClaimantPersonalInfo(nolock) cpi on cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN dbo.tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		inner JOIN dbo.tblClaimLawsuitStatusTracking(nolock) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN dbo.tblClaimLawsuitStatus(nolock)  cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		LEFT JOIN dbo.tblJurisdiction(nolock)  j on j.JurisdictionID = l.JurisdictionID
		LEFT JOIN dbo.tblState(nolock) s on s.StateID = l.StateID
		--LEFT JOIN m_tblInsurableClaims(nolock) i on i.ClaimID =c.ClaimID 
		INNER JOIN dbo.tblPaymentDefense(nolock) d on d.DefenseInvoiceID =di.DefenseInvoiceID 
		INNER JOIN dbo.tblcheck(nolock) ch on ch.CheckID =d.CheckID 
	WHERE COALESCE(sourcedate,'1/1/1900') < @NextQuarterStart AND COALESCE(sourcedate,'1/1/1900')  >= @PreviousQuarterStart
		AND c.InsurableFlag =1
		AND TortTypeID = 1
		AND IsPrimaryStatus = 1
	GROUP BY CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END,
			CASE WHEN COALESCE(dbo.fnEarliestExposure(C.ClaimantPersonalInfoID), '1/1/1900') <= '4/1/1985' THEN 'Yes' ELSE 'No' END,
	       FullNameState,
		   CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'GENERALEXPS' ELSE NCCReferenceID END,
		   LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,''),
		   CaseCaption,
		   DocketNumber,
		   ClaimLawsuitStatus,
		   Convert(varchar, FileDate, 101),
		   Convert(varchar, StatusDate, 101),
		   Convert(varchar, ActiveTrialDate, 101),
		   Convert(varchar, ProcessedDate, 101),
		   Convert(varchar, SourceDate, 101)


	UNION ALL

	SELECT CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END AS Material, 
		   SUM(paymentamount) AS Defense,
		   CASE WHEN COALESCE(dbo.fnEarliestExposure(C.ClaimantPersonalInfoID), '1/1/1900') <= '4/1/1985' THEN 'Yes' ELSE 'No' END AS Pre1985Designate,
		   FullNameState,
		   CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'GENERALEXPS' ELSE NCCReferenceID END AS NCCReferenceID,
		   LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,'') as Claimant,
		   CaseCaption,
		   DocketNumber,
		   ClaimLawsuitStatus,
		   Convert(varchar, FileDate, 101) as FileDate,
		   Convert(varchar, StatusDate, 101) as StatusDate,
		   Convert(varchar, ActiveTrialDate, 101) as ActiveTrialDate,
		   Convert(varchar, ProcessedDate, 101) as ProcessedDate,
		   Convert(varchar, SourceDate, 101) as SourceDate
	FROM dbo.tblDefenseInvoice(nolock) di
		LEFT JOIN dbo.tblDefInvClaimantDetail(nolock) dicd on dicd.DefenseInvoiceID = di.DefenseInvoiceID
		INNER JOIN dbo.tblClaim(nolock) c on c.ClaimID = dicd.ClaimID
		LEFT JOIN dbo.tblClaimLawsuit(nolock) cl ON cl.claimID = c.claimID
		left join dbo.tblClaimantSettlement(nolock) cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN dbo.tblAllegation(nolock) a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN dbo.tblMaterial(nolock) m on m.MaterialID = a.MaterialID
		LEFT JOIN dbo.tblClaimantPersonalInfo(nolock) cpi on cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN dbo.tblLawsuit(nolock) l ON cl.lawsuitid = l.lawsuitid
		INNER JOIN dbo.tblClaimLawsuitStatusTracking(nolock) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN dbo.tblClaimLawsuitStatus(nolock) cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		LEFT JOIN dbo.tblJurisdiction(nolock) j on j.JurisdictionID = l.JurisdictionID
		LEFT JOIN dbo.tblState(nolock) s on s.StateID = l.StateID
		--LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
		INNER JOIN dbo.tblPaymentDefense(nolock) d on d.DefenseInvoiceID =di.DefenseInvoiceID 
		INNER JOIN dbo.tblcheck(nolock) ch on ch.CheckID =d.CheckID
			WHERE 
 QCDate between '10/1/2014' and '1/1/2015'
and c.InsurableFlag = 1
and isPrimaryStatus = 1
and di.QCDate > ch.SourceDate
	GROUP BY CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END,
			CASE WHEN COALESCE(dbo.fnEarliestExposure(C.ClaimantPersonalInfoID), '1/1/1900') <= '4/1/1985' THEN 'Yes' ELSE 'No' END,
	       FullNameState,
		   CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'GENERALEXPS' ELSE NCCReferenceID END,
		   LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,''),
		   CaseCaption,
		   DocketNumber,
		   ClaimLawsuitStatus,
		   Convert(varchar, FileDate, 101),
		   Convert(varchar, StatusDate, 101),
		   Convert(varchar, ActiveTrialDate, 101),
		   Convert(varchar, ProcessedDate, 101),
		   Convert(varchar, SourceDate, 101)
		   --,di.QCDate
		   --,ch.SourceDate
--HAVING di.QCDate > ch.SourceDate

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_NewDefense_CNAOnly] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_NewDefense_CNAOnly] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptQuarterlyClaimsReport_NewDefense_CNAOnly] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_NewDefense_CNAOnly] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_NewDefense_CNAOnly] TO [power_user]
GO
