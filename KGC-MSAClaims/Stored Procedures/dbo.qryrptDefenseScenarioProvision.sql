SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptDefenseScenarioProvision] (@ScenarioID int)
As
SET NOCOUNT ON

--DECLARE @ScenarioID int
--SET @ScenarioID = 1

DECLARE @maxScenarioID int
SELECT @maxScenarioID = MAX (ScenarioID) from tblScenario
-------------------------------------------------------------------------------
--Defense Scenario & Applicable Provisions
-------------------------------------------------------------------------------
SELECT ScenarioProvisionID, sp.ScenarioID, Scenario, sp.ProvisionLabelID, Provision, DateCreated, ScenarioNotes
FROM tblScenarioProvisions sp
INNER JOIN tblScenario s ON sp.ScenarioID = s.ScenarioID
INNER JOIN tblProvisionLabel pl ON sp.ProvisionLabelID = pl.ProvisionLabelID
WHERE s.ScenarioID >= COALESCE(@ScenarioID, 0) AND s.ScenarioID <= COALESCE(NULLIF(@ScenarioID, 0), @maxScenarioID) 

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseScenarioProvision] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseScenarioProvision] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptDefenseScenarioProvision] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseScenarioProvision] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseScenarioProvision] TO [power_user]
GO
