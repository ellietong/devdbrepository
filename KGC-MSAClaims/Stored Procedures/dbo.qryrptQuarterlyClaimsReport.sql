SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptQuarterlyClaimsReport] (@Quarter int, @Year varchar(max)) 
AS
SET NOCOUNT ON

SELECT @Quarter as [Quarter], @Year as [Year]

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptQuarterlyClaimsReport] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport] TO [power_user]
GO
