SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmCopyClaimant] (@OldCPIID bigint, @CurrentDB nvarchar (1000))
AS 

SET NOCOUNT ON
-------------------------
--For Testing Only
--set temporary variables 
-------------------------

--Declare @OldCPIID bigint
--DECLARE @CurrentDB nvarchar (1000)

--SET @OldCPIID = 251962
--SET @CurrentDB = '[KGC-Dana-claim]'
--DECLARE @NEWCPIID bigint
--SET @NEWCPIID =42288
-------------------------
--Declare temporary variables
-------------------------
DECLARE @NewDB nvarchar (1000)
DECLARE @string nvarchar (max)
DECLARE @NEWCPIID bigint
declare @params nvarchar (max)
DECLARE @NewClaimID bigint
DECLARE @NewLawsuitID bigint

Set @NewDB = '['+DB_NAME()+']'



-------------------------
/*Before starting - need to verify if claimants are new or already in DB.
If not, insert into claimant table and determine the new personal info ID.*/
-------------------------
Set @string = 'Insert into '+ @NewDB + '.dbo.tblClaimantPersonalInfo (FirstName, MiddleName, LastName, SuffixID, MaritalStatusID, GenderID, BirthDate, DeceasedDate, 
					SocialSecurityNumber,HealthInsuranceClaimNumber, CMSQueryDate, Address1, Address2,City, StateID, Zip, Phone, Extension, IsSmoker, SmokerStart, 
					SmokerEnd, PacksPerDay)
select FirstName, MiddleName, LastName, SuffixID,
MaritalStatusID, GenderID, BirthDate, DeceasedDate, SocialSecurityNumber,
HealthInsuranceClaimNumber, CMSQueryDate, Address1, Address2,
City, StateID, Zip, Phone, Extension, IsSmoker, SmokerStart, SmokerEnd, PacksPerDay
from [WEBSQL].' + @CurrentDB + '.DBO.tblClaimantPersonalInfo cp 
where ClaimantPersonalInfoID = '+convert(nvarchar,@OldCPIID)
exec sp_executesql @string


--now retrieve the CPI of the new claimant for use in other related tables.
select @string = N'select @newcpiid = max (claimantpersonalinfoID) from ' + @NewDB + '.dbo.tblClaimantPersonalInfo'
Select @params = N'@NewCPIID bigint OUTPUT'
exec sp_executesql @string,@Params,@newCPIID output 

---------------------------
----New CPI ID is now stored in @NewCPIID.  Use this to insert the related table information.
---------------------------

Set @string = 'Insert into '+ @NewDB + '.dbo.tblClaimantRelatedParty (ClaimantPersonalInfoID,FirstName, MiddleName, LastName,RPSocialSecurityNumber,RPTIN, RelationshipID, LegalCapacityID,RPAddress1, RPAddress2, RPCity, StateID, RPZip, RPPhone, RPExtension)
Select '+ convert(nvarchar,@NewCPIID) +', FirstName, MiddleName, LastName,p.RPSocialSecurityNumber, p.RPTIN, p.RelationshipID, p.LegalCapacityID,p.RPAddress1, p.RPAddress2,
p.RPCity, p.StateID, p.RPZip, p.RPPhone, p.RPExtension
from [WEBSQL].' + @CurrentDB + '.DBO.tblClaimantRelatedParty p 
where ClaimantPersonalInfoID = ' + convert(nvarchar,@OldCPIID)
exec sp_executesql @string

---------------------------
----New CPI ID is now stored in @NewCPIID.  Use this to insert the Claimant Employment.
---------------------------

Set @string = 'Insert into '+ @NewDB + '.dbo.tblClaimantEmployment (ClaimantPersonalInfoID, Jobsite, JobsiteCity, JobsiteStateID, Comments, EmpStartDate, EmpEndDate,	EarliestExposureDate, LatestExposureDate, IsPrimary, IsSecondaryExposure)
Select '+ convert(nvarchar,@NewCPIID) +', Jobsite, JobsiteCity, JobsiteStateID, Comments, EmpStartDate, EmpEndDate,	EarliestExposureDate, LatestExposureDate, IsPrimary, IsSecondaryExposure
from [WEBSQL].' + @CurrentDB + '.DBO.tblClaimantEmployment p 
where ClaimantPersonalInfoID = ' + convert(nvarchar,@OldCPIID)
exec sp_executesql @string


---------------------------
----Need to create this claim. Must enter data in as Claim, Claim Lawsuit and then Lawsuit.
----Default Input of Current Claim to Yes, Sequence to Initial Claim and Fully Documented to No.
---------------------------
IF @NewDB = '[KGC-IUNA-Nosroc]' 
--If IUNA/Nosroc, default MatterID to IUNA
	Begin

	Set @string = 'Insert into '+ @NewDB + '.dbo.tblclaim (ClaimantPersonalInfoID, IsCurrentClaim,ClaimSequenceID,FullyDocumented, CauseOfInjuryID, MatterID)
	Select ' + convert (nvarchar,@NewCPIID) + ',1,1,0,CauseOfInjuryID,1
	from [WEBSQL].' + @CurrentDB + '.DBO.tblClaim Where ClaimantpersonalInfoID = '+convert (nvarchar,@OldCPIID) + ' and IsCurrentClaim = 1'
	exec sp_executesql @string

	End
Else 
	Begin

	Set @string = 'Insert into '+ @NewDB + '.dbo.tblclaim (ClaimantPersonalInfoID, IsCurrentClaim,ClaimSequenceID,FullyDocumented, CauseOfInjuryID)
	Select ' + convert (nvarchar,@NewCPIID) + ',1,1,0,CauseOfInjuryID
	from [WEBSQL].' + @CurrentDB + '.DBO.tblClaim Where ClaimantpersonalInfoID = '+convert (nvarchar,@OldCPIID) + ' and IsCurrentClaim = 1'
	exec sp_executesql @string

	End


--now retrieve the claimID of the new claim for use in other related tables.
select @string = N'select @NewClaimID = ClaimID from ' + @NewDB + '.dbo.tblclaim where claimantpersonalInfoID = '+ convert (nvarchar,@NewCPIID)
Select @params = N'@NewClaimID bigint OUTPUT'
exec sp_executesql @string,@Params,@NewClaimID output 



--Now enter data for Disease
Set @string = 'Insert into '+ @NewDB + '.dbo.tblClaimDisease (ClaimID, DiseaseID, PrimaryDisease, DiseaseDiagnosisDate, DoctorID)
Select ' +convert(nvarchar,@NewClaimID)+', c.DiseaseID, c.PrimaryDisease,  c.DiseaseDiagnosisDate, c.DoctorID
from [WEBSQL].' + @CurrentDB + '.DBO.tblClaimDisease c where ClaimID = (Select c.claimID from [WEBSQL].' + @CurrentDB + '.DBO.tblClaim c where c.ClaimantPersonalInfoID = ' + convert (nvarchar,@OldCPIID)+')'
exec sp_executesql @string


--Now create a new Lawsuit record
Set @string = 'Insert into '+ @NewDB + '.dbo.tblLawsuit (FileDate, ActiveTrialDate, CaseCaption, DocketNumber, JurisdictionID, CourtTypeID, ServiceDate, ReceivedDate, LeadPlaintiffFirstName,LeadPlaintiffLastName, StateID, PlaintiffCounselAddressID, IsThirdPartyComplaint)
select FileDate, ActiveTrialDate, CaseCaption, DocketNumber, JurisdictionID, CourtTypeID, ServiceDate, ReceivedDate, LeadPlaintiffFirstName,
LeadPlaintiffLastName,  StateID,PlaintiffCounselAddressID, coalesce(IsThirdPartyComplaint,0)
from [WEBSQL].' + @CurrentDB + '.DBO.tblLawsuit L
where l.lawsuitid = (select cl.LawsuitID from [WEBSQL].' + @CurrentDB + '.DBO.tblClaimLawsuit cl join [WEBSQL].' + @CurrentDB + '.DBO.tblClaim c on c.ClaimID = cl.ClaimID
					 where iscurrentclaim = 1 and isprimaryforclaim=1 and c.ClaimantPersonalInfoID = '+ convert (nvarchar,@OldCPIID)+')'
exec sp_executesql @string


--now retrieve the LawsuitID of the new lawsuit for use in tblClaimLawsuit.
select @string = N'select @NewLawsuitID = max(LawsuitID) from ' + @NewDB + '.dbo.tblLawsuit '
Select @params = N'@NewLawsuitID bigint OUTPUT'
exec sp_executesql @string,@Params,@NewLawsuitID output 


--Now create a new ClaimLawsuit record
Set @string = 'Insert into '+ @NewDB + '.dbo.tblClaimLawsuit (ClaimID, LawsuitID, IndividualDocketNumber, IsPrimaryForClaim, IsMDL)
select '+ convert(nvarchar,@NewClaimID) + ', '+  convert(nvarchar,@NewLawsuitID) + ', IndividualDocketNumber, IsPrimaryForClaim, IsMDL
from [WEBSQL].' + @CurrentDB + '.DBO.tblClaimLawsuit L
where l.claimlawsuitid = (select ClaimLawsuitID from [WEBSQL].' + @CurrentDB + '.DBO.tblClaimLawsuit cl join [WEBSQL].' + @CurrentDB + '.DBO.tblClaim c on c.ClaimID = cl.ClaimID
					 where iscurrentclaim = 1 and isprimaryforclaim=1 and c.ClaimantPersonalInfoID = '+ convert (nvarchar,@OldCPIID)+')'
exec sp_executesql @string





Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyClaimant] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyClaimant] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmCopyClaimant] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyClaimant] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyClaimant] TO [power_user]
GO
