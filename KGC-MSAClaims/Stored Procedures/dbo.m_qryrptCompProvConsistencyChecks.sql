SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create PROCEDURE [dbo].[m_qryrptCompProvConsistencyChecks]
(@limitTypeID int = NULL, @carrierid int = NULL)
AS

SET NOCOUNT ON

--DECLARE @limitTypeID int
--DECLARE @carrierID int
--SET @limitTypeID = 1
--SET @carrierid = 16

DECLARE @maxLimitTypeID int
DECLARE @maxCarrierID int

SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy

DECLARE @Provision1 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision1 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 1 AND p.CarrierID = @carrierID
DECLARE @Provision2 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision2 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 2 AND p.CarrierID = @carrierID
DECLARE @Provision3 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision3 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 3 AND p.CarrierID = @carrierID
DECLARE @Provision4 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision4 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 4 AND p.CarrierID = @carrierID
DECLARE @Provision5 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision5 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 5 AND p.CarrierID = @carrierID
DECLARE @Provision6 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision6 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 6 AND p.CarrierID = @carrierID
DECLARE @Provision7 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision7 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 7 AND p.CarrierID = @carrierID
DECLARE @Provision8 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision8 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 8 AND p.CarrierID = @carrierID
DECLARE @Provision9 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision9 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 9 AND p.CarrierID = @carrierID
DECLARE @Provision10 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision10 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 10 AND p.CarrierID = @carrierID
DECLARE @Provision11 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision11 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 11 AND p.CarrierID = @carrierID
DECLARE @Provision12 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision12 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 12 AND p.CarrierID = @carrierID
DECLARE @Provision13 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision13 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 13 AND p.CarrierID = @carrierID
DECLARE @Provision14 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision14 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 14 AND p.CarrierID = @carrierID
DECLARE @Provision15 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision15 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 15 AND p.CarrierID = @carrierID
DECLARE @Provision16 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision16 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 16 AND p.CarrierID = @carrierID
DECLARE @Provision17 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision17 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 17 AND p.CarrierID = @carrierID
DECLARE @Provision18 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision18 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 18 AND p.CarrierID = @carrierID
DECLARE @Provision19 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision19 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 19 AND p.CarrierID = @carrierID
DECLARE @Provision20 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision20 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 20 AND p.CarrierID = @carrierID
DECLARE @Provision21 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision21 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 21 AND p.CarrierID = @carrierID
DECLARE @Provision22 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision22 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 22 AND p.CarrierID = @carrierID
DECLARE @Provision23 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision23 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 23 AND p.CarrierID = @carrierID
DECLARE @Provision24 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision24 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 24 AND p.CarrierID = @carrierID
DECLARE @Provision25 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision25 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 25 AND p.CarrierID = @carrierID
DECLARE @Provision26 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision26 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 26 AND p.CarrierID = @carrierID
DECLARE @Provision27 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision27 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 27 AND p.CarrierID = @carrierID
DECLARE @Provision28 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision28 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 28 AND p.CarrierID = @carrierID
DECLARE @Provision29 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision29 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 29 AND p.CarrierID = @carrierID
DECLARE @Provision30 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision30 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 30 AND p.CarrierID = @carrierID
DECLARE @Provision31 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision31 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 31 AND p.CarrierID = @carrierID
DECLARE @Provision32 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision32 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 32 AND p.CarrierID = @carrierID
DECLARE @Provision33 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision33 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 33 AND p.CarrierID = @carrierID
DECLARE @Provision34 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision34 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 34 AND p.CarrierID = @carrierID
DECLARE @Provision35 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision35 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 35 AND p.CarrierID = @carrierID
DECLARE @Provision36 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision36 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 36 AND p.CarrierID = @carrierID
DECLARE @Provision37 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision37 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 37 AND p.CarrierID = @carrierID
DECLARE @Provision38 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision38 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 38 AND p.CarrierID = @carrierID
DECLARE @Provision39 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision39 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 39 AND p.CarrierID = @carrierID
DECLARE @Provision40 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision40 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 40 AND p.CarrierID = @carrierID
DECLARE @Provision41 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision41 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 41 AND p.CarrierID = @carrierID
DECLARE @Provision42 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision42 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 42 AND p.CarrierID = @carrierID
DECLARE @Provision43 TABLE(PolicyID int, ProvisionValueID int)
INSERT INTO @Provision43 SELECT p.PolicyID, ProvisionValueID FROM tblPolicy p INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID WHERE pr.ProvisionLabelID = 43 AND p.CarrierID = @carrierID

SELECT CarrierNickname AS 'Carrier', MIN(ActualPStartDate) AS 'Start Date', MAX(ActualPEndDate) AS 'End Date', 
	AttachmentPoint AS 'Attach', p.PolicyID AS 'ID', PolicyNum AS 'Policy Number', 
	p1.ProvisionValueID AS 'Def Treat for Alloc',
	p2.ProvisionValueID AS 'Agg Prod',
	p3.ProvisionValueID AS 'Agg Non-Prod BI',
	p4.ProvisionValueID AS 'Agg Non-Prod PD',
	p5.ProvisionValueID AS 'Annual Agg',
	p6.ProvisionValueID AS 'Annual Per Occ',
	p7.ProvisionValueID AS 'Anti-Assign',
	p8.ProvisionValueID AS 'Asbestos Excl',
	p9.ProvisionValueID AS 'BI Excl',
	p10.ProvisionValueID AS 'Choice of Law',
	p11.ProvisionValueID AS 'Claims Made',
	p12.ProvisionValueID AS 'Consent to Settle',
	p13.ProvisionValueID AS 'CSL Agg',
	p14.ProvisionValueID AS 'CSL Per Occ',
	p15.ProvisionValueID AS 'Coal Excl',
	p16.ProvisionValueID AS 'Def Oblig',
	p17.ProvisionValueID AS 'Def Treat Spec',
	p18.ProvisionValueID AS 'Def of Costs',
	p19.ProvisionValueID AS 'Def of UNL/Loss',
	p20.ProvisionValueID AS 'Design Def Excl',
	p21.ProvisionValueID AS 'Drop Down',
	p22.ProvisionValueID AS 'Evidence',
	p23.ProvisionValueID AS 'FF Language',
	p24.ProvisionValueID AS 'Insured A&C',
	p25.ProvisionValueID AS 'Insurer A&C',
	p26.ProvisionValueID AS 'Insuring Agr',
	p27.ProvisionValueID AS 'Maint of Und',
	p28.ProvisionValueID AS 'Mix Dust Excl',
	p29.ProvisionValueID AS 'Multi-Year',
	p30.ProvisionValueID AS 'Named Insured',
	p31.ProvisionValueID AS 'No Action',
	p32.ProvisionValueID AS 'Occ Disease',
	p33.ProvisionValueID AS 'Notice',
	p34.ProvisionValueID AS 'Occ Deemer',
	p35.ProvisionValueID AS 'Occ/Exp or Int',
	p36.ProvisionValueID AS 'Other Ins',
	p37.ProvisionValueID AS 'Poll Excl',
	p38.ProvisionValueID AS 'Prof Liab Excl',
	p39.ProvisionValueID AS 'Prior Ins/ Non-Cum',
	p40.ProvisionValueID AS 'Qualcomm',
	p41.ProvisionValueID AS 'Service of Suit',
	p42.ProvisionValueID AS 'Silica Excl',
	p43.ProvisionValueID AS 'Vol Pmnt Excl'
FROM tblPolicy p 
	INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID 
	INNER JOIN tblPolicyDates d ON p.PolicyID = d .PolicyID
	INNER JOIN tblPolicyLimits pl ON p.PolicyID = pl.PolicyID 
	LEFT JOIN @Provision1 p1 ON p.PolicyID = p1.PolicyID
	LEFT JOIN @Provision2 p2 ON p.PolicyID = p2.PolicyID
	LEFT JOIN @Provision3 p3 ON p.PolicyID = p3.PolicyID
	LEFT JOIN @Provision4 p4 ON p.PolicyID = p4.PolicyID
	LEFT JOIN @Provision5 p5 ON p.PolicyID = p5.PolicyID
	LEFT JOIN @Provision6 p6 ON p.PolicyID = p6.PolicyID
	LEFT JOIN @Provision7 p7 ON p.PolicyID = p7.PolicyID
	LEFT JOIN @Provision8 p8 ON p.PolicyID = p8.PolicyID
	LEFT JOIN @Provision9 p9 ON p.PolicyID = p9.PolicyID
	LEFT JOIN @Provision10 p10 ON p.PolicyID = p10.PolicyID
	LEFT JOIN @Provision11 p11 ON p.PolicyID = p11.PolicyID
	LEFT JOIN @Provision12 p12 ON p.PolicyID = p12.PolicyID
	LEFT JOIN @Provision13 p13 ON p.PolicyID = p13.PolicyID
	LEFT JOIN @Provision14 p14 ON p.PolicyID = p14.PolicyID
	LEFT JOIN @Provision15 p15 ON p.PolicyID = p15.PolicyID
	LEFT JOIN @Provision16 p16 ON p.PolicyID = p16.PolicyID
	LEFT JOIN @Provision17 p17 ON p.PolicyID = p17.PolicyID
	LEFT JOIN @Provision18 p18 ON p.PolicyID = p18.PolicyID
	LEFT JOIN @Provision19 p19 ON p.PolicyID = p19.PolicyID
	LEFT JOIN @Provision20 p20 ON p.PolicyID = p20.PolicyID
	LEFT JOIN @Provision21 p21 ON p.PolicyID = p21.PolicyID
	LEFT JOIN @Provision22 p22 ON p.PolicyID = p22.PolicyID
	LEFT JOIN @Provision23 p23 ON p.PolicyID = p23.PolicyID
	LEFT JOIN @Provision24 p24 ON p.PolicyID = p24.PolicyID
	LEFT JOIN @Provision25 p25 ON p.PolicyID = p25.PolicyID
	LEFT JOIN @Provision26 p26 ON p.PolicyID = p26.PolicyID
	LEFT JOIN @Provision27 p27 ON p.PolicyID = p27.PolicyID
	LEFT JOIN @Provision28 p28 ON p.PolicyID = p28.PolicyID
	LEFT JOIN @Provision29 p29 ON p.PolicyID = p29.PolicyID
	LEFT JOIN @Provision30 p30 ON p.PolicyID = p30.PolicyID
	LEFT JOIN @Provision31 p31 ON p.PolicyID = p31.PolicyID
	LEFT JOIN @Provision32 p32 ON p.PolicyID = p32.PolicyID
	LEFT JOIN @Provision33 p33 ON p.PolicyID = p33.PolicyID
	LEFT JOIN @Provision34 p34 ON p.PolicyID = p34.PolicyID
	LEFT JOIN @Provision35 p35 ON p.PolicyID = p35.PolicyID
	LEFT JOIN @Provision36 p36 ON p.PolicyID = p36.PolicyID
	LEFT JOIN @Provision37 p37 ON p.PolicyID = p37.PolicyID
	LEFT JOIN @Provision38 p38 ON p.PolicyID = p38.PolicyID
	LEFT JOIN @Provision39 p39 ON p.PolicyID = p39.PolicyID
	LEFT JOIN @Provision40 p40 ON p.PolicyID = p40.PolicyID
	LEFT JOIN @Provision41 p41 ON p.PolicyID = p41.PolicyID
	LEFT JOIN @Provision42 p42 ON p.PolicyID = p42.PolicyID
	LEFT JOIN @Provision43 p43 ON p.PolicyID = p43.PolicyID
WHERE LimitTypeID >= COALESCE(@limitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)
	AND p.CarrierID >= COALESCE(@carrierID, 0) AND p.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID)
	AND p.policyid NOT IN (SELECT policyid from tblProvision where ProvisionValueID in (250, 267)) 
GROUP BY CarrierNickname, AttachmentPoint, p.PolicyID, PolicyNum,
	p1.ProvisionValueID,
	p2.ProvisionValueID,
	p3.ProvisionValueID,
	p4.ProvisionValueID,
	p5.ProvisionValueID,
	p6.ProvisionValueID,
	p7.ProvisionValueID,
	p8.ProvisionValueID,
	p9.ProvisionValueID,
	p10.ProvisionValueID,
	p11.ProvisionValueID,
	p12.ProvisionValueID,
	p13.ProvisionValueID,
	p14.ProvisionValueID,
	p15.ProvisionValueID,
	p16.ProvisionValueID,
	p17.ProvisionValueID,
	p18.ProvisionValueID,
	p19.ProvisionValueID,
	p20.ProvisionValueID,
	p21.ProvisionValueID,
	p22.ProvisionValueID,
	p23.ProvisionValueID,
	p24.ProvisionValueID,
	p25.ProvisionValueID,
	p26.ProvisionValueID,
	p27.ProvisionValueID,
	p28.ProvisionValueID,
	p29.ProvisionValueID,
	p30.ProvisionValueID,
	p31.ProvisionValueID,
	p32.ProvisionValueID,
	p33.ProvisionValueID,
	p34.ProvisionValueID,
	p35.ProvisionValueID,
	p36.ProvisionValueID,
	p37.ProvisionValueID,
	p38.ProvisionValueID,
	p39.ProvisionValueID,
	p40.ProvisionValueID,
	p41.ProvisionValueID,
	p42.ProvisionValueID,
	p43.ProvisionValueID
ORDER BY CarrierNickname, MIN(ActualPStartDate), MAX(ActualPEndDate), AttachmentPoint, p.PolicyID, PolicyNum

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[m_qryrptCompProvConsistencyChecks] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qryrptCompProvConsistencyChecks] TO [base_user]
GRANT ALTER ON  [dbo].[m_qryrptCompProvConsistencyChecks] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_qryrptCompProvConsistencyChecks] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_qryrptCompProvConsistencyChecks] TO [power_user]
GO
