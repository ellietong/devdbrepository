SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[qrysrptInsGroupCorrespondence] 
(@CorrespondenceID int = null)
as


--DECLARE @CorrespondenceID int
--SET @CorrespondenceID = 4


select  case when c.carriergroupid is null then c.carriername else cg.carriergroupname end as insurergroup
from tblcarrier c
left join tblcarriergroup cg on c.carriergroupid = cg.carriergroupid
inner join tblPolicy p on p.CarrierID=c.CarrierID
inner join tblPolicyNoticed pn on pn.PolicyID=p.PolicyID
WHERE pn.CorrespondenceID = @CorrespondenceID

UNION

select  case when c.carriergroupid is null then c.carriername else cg.carriergroupname end as insurergroup
from tblcarrier c
left join tblcarriergroup cg on c.carriergroupid = cg.carriergroupid
inner join tblCarrierContact cc on cc.CarrierID = c.CarrierID
inner join tblCorrCarrierContact ccc on ccc.CarrierContactID=cc.CarrierContactID 
WHERE ccc.CorrespondenceID = @CorrespondenceID


GROUP BY case when c.carriergroupid is null then c.carriername else cg.carriergroupname end

--, (select case when c.carriergroupid is null then c.carriername else cg.carriergroupname end) 

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptInsGroupCorrespondence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptInsGroupCorrespondence] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptInsGroupCorrespondence] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptInsGroupCorrespondence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptInsGroupCorrespondence] TO [power_user]
GO
