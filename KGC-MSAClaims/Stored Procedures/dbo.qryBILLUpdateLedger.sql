SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryBILLUpdateLedger]
(@BillDate datetime, @ExhaustionDateID int, @ExhaustionTypeID int, @BillGroupID int)

As

Set NoCount On

--DECLARE @ExhaustionDateID int
--DECLARE @ExhaustionTypeID int
--DECLARE @BillGroupID int
--DECLARE @BillDate datetime
--SET @ExhaustionDateID = 1701
--SET @ExhaustionTypeID = 7
--SET @BillGroupID = 7
--SET @BillDate= '3/24/2010'

DECLARE @Exhaustion table (ExhaustionTypeID               int, 
						   ExhaustionID                   bigint, 
						   ExhaustionDateID               int, 
						   OccurrenceID                   bigint, 
						   PolicyDateID                   bigint,
						   IndemnityAllocation            money, 
						   DefenseAllocation              money, 
						   DefenseOutsideLimitsAllocation money, 
						   SIRIndemnityAllocation         money,
						   SIRDefenseAllocation           money, 
						   LedgerID                       bigint)
INSERT INTO @Exhaustion
	SELECT ExhaustionTypeID, ExhaustionID, e.ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation,
		DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation, LedgerID
	FROM tblExhaustion e
		INNER JOIN tblExhaustionDate ed on e.ExhaustionDateID = ed.ExhaustionDateID
	WHERE ExhaustionTypeID = @ExhaustionTypeID 
		AND ed.ExhaustionDateID = @ExhaustionDateID

--SELECT * FROM @Exhaustion

DECLARE @Ledger table (BillGroupID      int, 
					   CarrierID        int, 
					   SubscriberID     int, 
					   LedgerTypeID     int, 
					   LedgerDate       datetime, 
					   CheckNumber      char(10), 
					   Debit            money, 
					   Credit           money, 
					   ExhaustionDateID int, 
					   BillID           int, 
					   LedgerNote       nvarchar(255))
INSERT INTO @Ledger
	SELECT @BillGroupID, P.CarrierID, NULL as SubscriberID, 1 AS LedgerTypeID, @BillDate, NULL AS CheckNumber, 
		COALESCE(SUM(IndemnityAllocation) + SUM(DefenseAllocation) + SUM(DefenseOutsideLimitsAllocation),0) AS Debit, 0 AS Credit,
		ExhaustionDateID, NULL AS BillID, NULL AS LedgerNote
	FROM @Exhaustion E
		INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
		INNER JOIN tblPolicy p ON pd.PolicyID = p.PolicyID
	WHERE p.CarrierID IN(SELECT CarrierID FROM tblBillGroupCarriers WHERE billgroupid=@BillGroupID) 
		AND e.PolicyDateID NOT IN(SELECT PolicyDateID FROM tblBillGroupPolicyExclude bgpe INNER JOIN tblPolicyDates pd ON pd.PolicyID = bgpe.PolicyID WHERE BillGroupID = @BillGroupID)
		AND p.CarrierID <> 20
	GROUP BY p.CarrierID, ExhaustionDateID
	UNION
	SELECT @BillGroupID, P.CarrierID, SubscriberID, 1 AS LedgerTypeID, @BillDate, NULL AS CheckNumber, 
		COALESCE(SUM(IndemnityAllocation) + SUM(DefenseAllocation) + SUM(DefenseOutsideLimitsAllocation),0) AS Debit, 0 AS Credit,
		ExhaustionDateID, NUlL AS BillID, NULL AS LedgerNote
	FROM @Exhaustion E
		INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
		INNER JOIN tblPolicy p ON pd.PolicyID = p.PolicyID
		INNER JOIN tblSubscriptionSectionCoverage ssc ON p.PolicyID = ssc.PolicyID
		INNER JOIN tblSubscriptionCoverage sc ON ssc.SubscriptionSectionCoverageID = sc.SubscriptionSectionCoverageID
	WHERE sc.SubscriberID IN(SELECT SubscriberID FROM tblBillGroupSubscribers WHERE BillGroupID = @BillGroupID)
		AND e.PolicyDateID NOT IN(SELECT PolicyDateID FROM tblBillGroupPolicyExclude bgpe INNER JOIN tblPolicyDates pd ON pd.PolicyID = bgpe.PolicyID WHERE BillGroupID = @BillGroupID)
		AND p.CarrierID = 20
	GROUP BY p.CarrierID, SubscriberID, ExhaustionDateID

--SELECT * FROM @Ledger

DECLARE @Final table (BillGroupID      int, 
					  CarrierID        int, 
					  SubscriberID     int, 
					  LedgerTypeID     int, 
					  LedgerDate       datetime, 
					  CheckNumber      char(10), 
					  Debit            money, 
					  Credit           money, 
					  ExhaustionDateID int, 
					  BillID           int, 
					  LedgerNote       nvarchar(255))
INSERT INTO @Final
	SELECT BillGroupID, CarrierID, SubscriberID, LedgerTypeID, LedgerDate, CheckNumber, Sum(Debit) as Debit, Credit,
		ExhaustionDateID, BillID, LedgerNote
	FROM @Ledger
	GROUP BY BillGroupID, CarrierID, SubscriberID, LedgerTypeID, LedgerDate, CheckNumber, Credit,
		ExhaustionDateID, BillID, LedgerNote

--SELECT * FROM @Final

INSERT INTO tblLedger (BillGroupID, CarrierID, SubscriberID, LedgerTypeID, LedgerDate, CheckNumber, Debit, Credit, ExhaustionDateID, BillID, LedgerNote)
	SELECT BillGroupID, CarrierID, SubscriberID, LedgerTypeID, LedgerDate, CheckNumber, Debit, Credit,
			ExhaustionDateID, BillID, LedgerNote
	FROM @Final

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryBILLUpdateLedger] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryBILLUpdateLedger] TO [base_user]
GRANT ALTER ON  [dbo].[qryBILLUpdateLedger] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryBILLUpdateLedger] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryBILLUpdateLedger] TO [power_user]
GO
