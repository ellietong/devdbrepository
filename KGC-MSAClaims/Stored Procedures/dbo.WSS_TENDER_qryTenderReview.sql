SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryTenderReview] (@TenderHistoryID bigint)--, @TenderTemplateID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SELECT
	--SELECT * FROM tblTenderHistory ORDER BY TenderHistoryID DESC
	--SELECT * FROM tblTenderHistory ORDER BY TenderHistoryID DESC
	--DECLARE @TenderHistoryID bigint = 173678
	--DECLARE @TenderTemplateID int = 635
	--SELECT * FROM tblTenderTemplate
	DECLARE @ClaimID bigint
	SET @ClaimID = (SELECT TOP 1 ClaimID 
					FROM tblClaimTenderHistory 
					WHERE TenderHistoryID = @TenderHistoryID)
	
	--SELECT * FROM tblClaimTenderHistory cth
	--INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID
	--WHERE ClaimID IN (44001, 44641)
	--AND TenderTypeID = 2
	
	DECLARE @WithdrawnTenderBlockName nvarchar(MAX)
	--SET @WithdrawnTenderBlockName = (SELECT  TenderBlockName_Reporting
	--								 FROM tblTenderBlock tb
	--									INNER JOIN tblTenderHistory th ON tb.TenderBlockID = th.TenderBlockID
	--									INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
	--								 WHERE TenderTypeID = 2 and cth.TenderStatusID=5
	--								AND ClaimID IN (SELECT ClaimID FROM tblClaimTenderHistory cth 
	--												INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID
	--												WHERE th.TenderHistoryID =  @TenderHistoryID))
	--IF ((Select tenderhistoryid FROM tblTenderHistory) =173678)
	--	SET @WithdrawnTenderBlockName = NULL
	
	--SELECT @WithdrawnTenderBlockName
	--Check Claim Count -- if 1 then return usual fields, else return less fields + exhibit 
	DECLARE @ClaimCount bigint = (SELECT COUNT(ClaimID) FROM tblClaimTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
	
	DECLARE @NewLine nvarchar(5) =  '<br/>'
	
	DECLARE @Lname nvarchar(250), @Fname nvarchar(250), @Mname nvarchar(250), @Title nvarchar(250),  @Email nvarchar(250), @CCList nvarchar(max)

	SET @CCList = ''

	DECLARE Cur_ccContacts CURSOR Local Fast_Forward for
	SELECT DISTINCT LName, FName, MName, Email
	FROM tblTenderContact tc
	INNER JOIN tblPolicy p ON tc.CarrierID = p.CarrierID
	INNER JOIN tblTenderBlockPolicy tbp ON p.PolicyID = tbp.PolicyID
	INNER JOIN tblTenderBlock tb ON tbp.TenderBlockID = tb.TenderBlockID
	INNER JOIN tblTenderHistory th ON tb.TenderBlockID = th.TenderBlockID
	WHERE TenderHistoryID = @TenderHistoryID AND CC=1
	--Union in "alwaysCC" contacts
	UNION
	SELECT LName, FName, MName, Email
	FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
	UNION
	--Add in defense counsel tender contact
	SELECT p.LName, p.FName, null, p.Email
	FROM tblTenderHistory th
	INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
	INNER JOIN tblClaim c ON c.ClaimID = cth.ClaimID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN vtblPerson p ON CASE WHEN l.DefenseCounselAddressID = 127 THEN 189 ELSE l.DefenseCounselAddressID END = p.FirmAddressID--INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
	WHERE (p.TendercontactCC = 1) AND c.ClaimID IN (SELECT ClaimID FROM tblClaimTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
	UNION--NCC depending on claim type
	SELECT p.LName, p.FName, null, p.Email
	FROM tblTenderHistory th
	INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
	INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
	INNER JOIN tblClaimLawsuit cl On c.ClaimID = cl.ClaimID
	INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
	INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
	WHERE th.TenderHistoryID = @TenderHistoryID
	ORDER BY LName, FName
	
	
	DECLARE @LDCList nvarchar(MAX)
	SET @LDCList = ''
	DECLARE @LDCName nvarchar(MAX)
	IF(@ClaimCount >= 1)
	BEGIN
		DECLARE Cur_LDCContact CURSOR Local Fast_Forward for
		SELECT DISTINCT COALESCE(p.FName, '') + ' ' + COALESCE(p.LName, '') AS LDCName
		FROM tblTenderHistory th
		INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
		INNER JOIN tblClaim c ON c.ClaimID = cth.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE (p.PrimaryContact = 1) AND c.ClaimID IN (SELECT ClaimID FROM tblClaimTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
		ORDER BY COALESCE(p.FName, '') + ' ' + COALESCE(p.LName, '')
		
		OPEN Cur_LDCContact
		FETCH NEXT FROM Cur_LDCContact INTO @LDCName--@LName, @FName, @MName, @Title, @Email
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF(@LDCList = '')
			BEGIN
				SET @LDCList = @LDCName + @NewLine
			END
			ELSE
			BEGIN
				SET @LDCList = @LDCList + @LDCName + @NewLine
			END
		
			FETCH NEXT FROM Cur_LDCContact INTO @LDCName--@LName, @FName, @MName, @Title, @Email
		END
		CLOSE Cur_LDCContact
		DEALLOCATE Cur_LDCContact
	END
	
	-------------------------------------------------------------
	-----------LDC Email List--------- at bottom of LDC address (in a table)
	DECLARE @LDCEmails nvarchar(MAX)
	SET @LDCEmails = ''
	DECLARE @LDCEmail nvarchar(100) = ''
	DECLARE @Name nvarchar(500)
	
	
	IF(@ClaimCount >= 1)
	BEGIN
		DECLARE Cur_LDCEmails CURSOR Local Fast_Forward for
		SELECT  p.Email, p.LName + ', ' + p.FName
		FROM tblTenderHistory th
		INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
		INNER JOIN tblClaim c ON c.ClaimID = cth.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE (p.PrimaryContact = 1) AND c.ClaimID IN (SELECT ClaimID FROM tblClaimTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
		GROUP BY p.Email, p.LName + ', ' + p.FName
		ORDER BY p.LName + ', ' + p.FName
		
		OPEN Cur_LDCEmails
		FETCH NEXT FROM Cur_LDCEmails INTO @LDCEmail, @Name
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF(@LDCEmails = '')
			BEGIN
				SET @LDCEmails = '<table style="margin: 0; padding: 0;" cellspacing="0" cellpadding="0"><tr><td>Email:&nbsp;</td><td>' + @LDCEmail + '</td></tr>'
			END
			ELSE
			BEGIN
				SET @LDCEmails = @LDCEmails + '<tr><td></td><td>' + @LDCEmail + '</td></tr>'
			END
		
			FETCH NEXT FROM Cur_LDCEmails INTO @LDCEmail, @Name
			IF(@@FETCH_STATUS < 0 AND @LDCEmails <> '')
			BEGIN
				SET @LDCEmails = @LDCEmails + '</table>'
			END
		END
		CLOSE Cur_LDCEmails
		DEALLOCATE Cur_LDCEmails
	END
	
	--Assemble table for CC contacts listing at bottom of page
	IF(@ClaimCount >= 1)
	BEGIN
		OPEN Cur_ccContacts
		FETCH NEXT FROM Cur_ccContacts INTO @LName, @FName, @MName,  @Email
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF(@CCList = '')
			BEGIN

				SET @CCList = '<table><tr><td>cc:</td>' + '<td>' + @FName + COALESCE(' ' + @MName + ' ', ' ') + @Lname  + ' (by email only - ' + @Email + ')</td></tr>' 

			END
			ELSE
			BEGIN

				SET @CCList = @CCList + '<td></td><td>' + @FName + COALESCE(' ' + @MName + ' ', ' ') + @Lname + ' (by email only - ' + @Email + ')</td></tr>' 
				
			END

			FETCH NEXT FROM Cur_ccContacts INTO @LName, @FName, @MName,  @Email
			IF(@@FETCH_STATUS < 0 AND @CCList <> '')
			BEGIN
				SET @CCList = @CCList + '</table>'
			END
		END
		CLOSE Cur_ccContacts
		DEALLOCATE Cur_ccContacts

	END
	
	--Create exhibit table
	DECLARE @TenderTypeID int = (SELECT TenderTypeID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
	DECLARE @ClaimantName nvarchar(MAX)
	DECLARE @NCCRef nvarchar(15)
	DECLARE @Style nvarchar(Max)
	DECLARE @LDC nvarchar(MAX)
	DECLARE @ExhibitTableSimple nvarchar(MAX) = '' 
	DECLARE @ExhibitTableDetailed nvarchar(MAX) = '' 
	
	-- Only populate the fields if current TH record has a template that is flagged to use it
	IF(SELECT COALESCE(IncludeExhibit, 0) FROM tblTenderHistory th INNER JOIN tblTenderTemplate tt ON th.TenderTemplateID = tt.TenderTemplateID WHERE TenderHistoryID = @TenderHistoryID) = 1
	BEGIN
		SET @ExhibitTableSimple = '<style type="text/css"> #tblExhibit td,  #tblExhibit table {
		border: 1px solid black;
		} #tblExhibit th{
		border: 1px solid black;
		background-color: lightgrey;
		font-color: black;
		}   </style><div style="page-break-before: always; text-align: center"><h3>Exhibit A</h3><table id="tblExhibit" style="margin-left: auto; margin-right: auto;text-align: left; border-collapse:collapse; " cellpadding="4">'
		SET @ExhibitTableDetailed = @ExhibitTableSimple
		
		--Simple version (Just Name & NCC)
		SET @ExhibitTableSimple = @ExhibitTableSimple + '<thead><th>Claimant Name</th><th>NCC #</th></thead>'
		
		DECLARE Cur_Exhibit CURSOR Local Fast_Forward for
		SELECT DISTINCT COALESCE(cpi.LastName, '') + COALESCE(', '  + FirstName, '') + COALESCE(' ' + MiddleName, '') AS ClaimantFullName,
				NCCReferenceID
		FROM tblTenderHistory th
			INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
			INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
			INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalinfOID = cpi.ClaimantPersonalInfoID
		WHERE th.TenderHistoryID = @TenderHistoryID
		OPEN Cur_Exhibit
		FETCH NEXT FROM Cur_Exhibit INTO @ClaimantName, @NCCRef
		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			SET @ExhibitTableSimple = @ExhibitTableSimple + '<tr><td>' + @ClaimantName + '</td><td>' + @NCCRef + '</td></tr>'
			
			FETCH NEXT FROM Cur_Exhibit INTO @ClaimantName, @NCCRef
		END
		CLOSE Cur_Exhibit
		DEALLOCATE Cur_Exhibit



		--Detailed Exhibit (Name, NCC, Style & LDC)
		SET @ExhibitTableDetailed = @ExhibitTableDetailed + '<thead><th>Claimant Name</th><th>NCC #</th><th>Style</th><th>Local Defense Counsel</th></thead>'

		DECLARE @PrimaryContacts TABLE (PersonID int)
		INSERT INTO @PrimaryContacts
		SELECT PersoNID FROM (SELECT vp.PersonID, ROW_NUMBER() OVER (PARTITION BY FirmAddressID ORDER BY FirmAddressID, PersonID DESC) AS [Row] FROM vtblPerson vp 
INNER JOIN tblDefenseCounselAddress dca1 ON vp.FirmAddressID = dca1.DefenseCounselAddressID 
WHERE PrimaryContact = 1) p WHERE Row = 1
		
		DECLARE Cur_Exhibit CURSOR Local Fast_Forward for
		SELECT  DISTINCT COALESCE(cpi.LastName, '') + COALESCE(', '  + FirstName, '') + COALESCE(' ' + MiddleName, '') AS ClaimantFullName,
				NCCReferenceID, COALESCE(CaseCaption, '') + ' ' + COALESCE(DocketNumber, '') + ' (' + COALESCE(Jurisdiction, '') + ', ' + COALESCE(s.[State], '') + ')' AS Style,
				+ COALESCE(p.FName+ ' ' + p.LName + '</br>', '') 
				+ COALESCE(dca.DefenseCounsel, '')  
				+ '</br>' + COALESCE(dca.DefenseCounselAddress1, '') + 
				+ COALESCE('</br>' + dca.DefenseCounselAddress2, '') + --coalesce entire line since the second address may not exist
				+ '</br>' + COALESCE(dca.DefenseCounselCity, '') + ', ' + COALESCE(dca.State, '') + ' ' + COALESCE(dca.DefenseCounselZip, '')
				
		FROM tblTenderHistory th 
			INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
			INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
			INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalinfOID = cpi.ClaimantPersonalInfoID
			INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
			INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
			INNER JOIN tblState s ON l.StateID = s.StateID
			INNER JOIN tblJurisdiction j ON l.JurisdictionID = j.JurisdictionID
			--INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID --AND c.MatterID = dj.matterID
			INNER JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
			LEFT JOIN (SELECT vp.* FROM vtblPerson vp INNER JOIN @PrimaryContacts pc ON vp.PersonID = pc.PersonID) p ON dca.DefenseCounselAddressID = p.FirmAddressID
		WHERE th.TenderHistoryID = @TenderHistoryID
		OPEN Cur_Exhibit
		FETCH NEXT FROM Cur_Exhibit INTO @ClaimantName, @NCCRef, @Style, @LDC
		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			SET @ExhibitTableDetailed = @ExhibitTableDetailed + '<tr><td style="white-space: nowrap;">' + @ClaimantName + '</td><td>' + @NCCRef + '</td><td>' + @Style + '</td><td style="white-space: nowrap;">' + @LDC + '</td></tr>'
			
			FETCH NEXT FROM Cur_Exhibit INTO @ClaimantName, @NCCRef, @Style, @LDC
		END
		CLOSE Cur_Exhibit
		DEALLOCATE Cur_Exhibit
		
		--Close the table and div tags!!
		SET @ExhibitTableSimple = @ExhibitTableSimple + '</table></div>'	
		SET @ExhibitTableDetailed = @ExhibitTableDetailed + '</table></div>'
	END
	
	--Final select
    SELECT DISTINCT th.TenderHistoryID, DATENAME(mm,GETDATE()) + ' ' + CONVERT(nvarchar, DAY(GETDATE())) + ', ' + CONVERT(nvarchar, YEAR(GETDATE())) AS [Date],
    AdditionalText, ttemp.TemplateURL, TenderBlockName, EffectiveDate, TenderType,
    CASE WHEN l.ReceivedDate IS NULL THEN CaseCaption ELSE CaseCaption + ', Case No. ' + DocketNumber + ' (' + CourtType + ', ' + Jurisdiction + ', ' + s.State + ')' END AS CaseCaption,
    CASE WHEN l.StateID = 3 THEN 'Huddleston Bolen LLP' ELSE dca.DefenseCounsel END AS DefenseCounsel,
    COALESCE(cpi.FirstName, '') + COALESCE(' '  + MiddleName, '') + COALESCE(' ' + LastName, '') AS ClaimantFullName,
    NCCReferenceID, CaseCaption, dca.DefenseCounselAddress1, dca.DefenseCounselAddress2, dca.DefenseCounselCity, 
    dca.DefenseCounselZip, dca.DefenseCounselMailZip, dca.DefenseCounselPhone, dca.State AS DefenseCounselState,
    Jurisdiction, s.State, CourtType, DocketNumber, CONVERT(nvarchar, ServiceDate, 101) AS ServiceDate,
    CASE WHEN l.StateID = 3 THEN @NewLine + '611 Third Avenue' + @Newline + 'Huntington, WV 25701' + @NewLine + 'Phone: (304) 529-6181' + 
    '<table style="margin: 0; padding: 0;" cellspacing="0" cellpadding="0"><tr><td>Email:&nbsp;</td><td>jmahaney@huddlestonbolen.com</td></tr></table>'
     ELSE
    @NewLine + COALESCE(dca.DefenseCounselAddress1, '') +  
    COALESCE(@NewLine + dca.DefenseCounselAddress2, '') + @NewLine + 
    COALESCE(dca.DefenseCounselCity + ', ', '')  + COALESCE(dca.State, '') + ' ' + COALESCE(dca.DefenseCounselZip, '') + COALESCE(('-' + dca.DefenseCounselMailZip), '') + @NewLine + 
    COALESCE('Phone: ' + '(' + Substring(dca.DefenseCounselPhone,1,3) + ') ' 
           + Substring(dca.DefenseCounselPhone,4,3) + '-' 
           + Substring(dca.DefenseCounselPhone,7,4), '') +
    --COALESCE(@NewLine + 'Fax: ' + '(' + Substring(dca.DefenseCounselFax,1,3) + ') ' 
    --       + Substring(dca.DefenseCounselFax,4,3) + '-' 
    --       + Substring(dca.DefenseCounselFax,7,4), '') + 
    COALESCE(@NewLine + @LDCEmails, '')
           
      END AS DefenseCounselAddress, 
     
     CASE WHEN COALESCE(ttemp.IncludeComplaint, 0) = 0 THEN NULL WHEN d.AmazonKey IS NOT NULL THEN d.AmazonKey ELSE d.DocumentURL END AS ComplaintPath, TenderType,
     CASE WHEN cl.LawsuitID IN (SELECT lawsuitid FROM tblClaimLawsuit group by lawsuitid having COUNT(claimID)>1) THEN 'claim brought by ' + CASE WHEN GenderID = 2 THEN 'Ms. '+LastName WHEN GenderID = 3 THEN 'Mr. '+ LastName ELSE FirstName + ' ' + LastName END + ' as part of the lawsuit captioned ' ELSE
     'lawsuit captioned ' END AS LawsuitText,
     CASE WHEN (ServiceDate IS NULL OR ServiceDate = '1/1/1900') AND FileDate IS NOT NULL THEN 'which was filed on or about ' + DATENAME(mm,FileDate) + ' ' + CONVERT(nvarchar, DAY(FileDate)) + ', ' + CONVERT(nvarchar, YEAR(FileDate))
		ELSE 'which was served on MSA on or about ' + DATENAME(mm,ServiceDate) + ' ' + CONVERT(nvarchar, DAY(ServiceDate)) + ', ' + CONVERT(nvarchar, YEAR(ServiceDate)) END AS DateText,
		  CASE WHEN dbo.fnEarliestExposure(claim.ClaimantPersonalInfoID) IS NULL THEN 'No exposure dates are alleged in the complaint; accordingly, the defense is being tendered to North River and Northbrook on the basis that the claims giving rise to the complaint potentially fall within the coverage afforded by the aforementioned policies.' ELSE '' END AS EarliestExposureText,
		  @CCList AS CCRecipients, CASE WHEN l.StateID = 3 THEN 'J. H. Mahaney' + @NewLine ELSE @LDCList END AS LDCList, CASE WHEN ServiceDate IS NULL THEN 'This complaint has been filed, but not yet formally served on MSA.' ELSE '' END AS FormallyServed,
		  CASE WHEN a.MaterialID IN (1,2,3,4,5,6,7,8,9) THEN COALESCE(p.FName, '') + ' ' + COALESCE(p.LName, '') + COALESCE(', ' + p.Title, '') ELSE '' END AS NCCName, 
	
	CASE WHEN a.MaterialID IN (1,2,3,4,5,6,7,8,9) THEN @NewLine + COALESCE(ncca.DefenseCounsel, '') + @NewLine + COALESCE(ncca.DefenseCounselAddress1, '') +  
    COALESCE(@NewLine + ncca.DefenseCounselAddress2, '') + @NewLine + 
    COALESCE(ncca.DefenseCounselCity + ', ', '')  + COALESCE(ncca.State, '') + ' ' + COALESCE(ncca.DefenseCounselZip, '') + COALESCE(('-' + ncca.DefenseCounselMailZip), '') + @NewLine + 
    COALESCE('Phone: ' + '(' + Substring(ncca.DefenseCounselPhone,1,3) + ') ' 
           + Substring(ncca.DefenseCounselPhone,4,3) + '-' 
           + Substring(ncca.DefenseCounselPhone,7,4), '') +
    --COALESCE(@NewLine + 'Fax: ' + '(' + Substring(dca.DefenseCounselFax,1,3) + ') ' 
    --       + Substring(dca.DefenseCounselFax,4,3) + '-' 
    --       + Substring(dca.DefenseCounselFax,7,4), '') + 
    COALESCE(@NewLine + 'Email: ' + p.Email, '') ELSE '' END AS NCCAddress, 
    CASE WHEN a.MaterialID IN (1,2,3,4,5,6,7,8,9) THEN 'His contact information is as follows: ' ELSE '' END as NCCAdj,
    --CASE WHEN nm.PersonID = 161 THEN 'Her ' ELSE 'His ' END + 'contact information is as follows: ' AS NCCAdj,
    CASE WHEN nm.PersonID = 3398 THEN 'is MSA''s National Coordinating Counsel.' ELSE '' END AS NCCClaimType,
    @ExhibitTableSimple AS ExhibitTableSimple, @ExhibitTableDetailed AS ExhibitTableDetailed, dtender.DocumentURL AS TenderDocURL, dtender.AmazonKey AS TenderAmazonKey, th.ApprovedBy, th.DateApproved
    --@WithdrawnTenderBlockName AS WithdrawnTenderBlock
	--CASE WHEN a.MaterialID NOT IN (3,4,7,8) THEN COALESCE(p.FName, '') + ' ' + COALESCE(p.LName, '') + COALESCE(', ' + p.Title, '') ELSE '' END AS NCCName, 
	
	--CASE WHEN a.MaterialID NOT IN (3,4,7,8) THEN @NewLine + COALESCE(ncca.DefenseCounsel, '') + @NewLine + COALESCE(ncca.DefenseCounselAddress1, '') +  
 --   COALESCE(@NewLine + ncca.DefenseCounselAddress2, '') + @NewLine + 
 --   COALESCE(ncca.DefenseCounselCity + ', ', '')  + COALESCE(ncca.State, '') + ' ' + COALESCE(ncca.DefenseCounselZip, '') + COALESCE(('-' + ncca.DefenseCounselMailZip), '') + @NewLine + 
 --   COALESCE('Phone: ' + '(' + Substring(ncca.DefenseCounselPhone,1,3) + ') ' 
 --          + Substring(ncca.DefenseCounselPhone,4,3) + '-' 
 --          + Substring(ncca.DefenseCounselPhone,7,4), '') +
 --   --COALESCE(@NewLine + 'Fax: ' + '(' + Substring(dca.DefenseCounselFax,1,3) + ') ' 
 --   --       + Substring(dca.DefenseCounselFax,4,3) + '-' 
 --   --       + Substring(dca.DefenseCounselFax,7,4), '') + 
 --   COALESCE(@NewLine + 'Email: ' + p.Email, '') ELSE '' END AS NCCAddress, 
 --   CASE WHEN a.MaterialID NOT IN (3,4,7,8) THEN 'His contact information is as follows: ' ELSE '' END as NCCAdj,
 --   --CASE WHEN nm.PersonID = 161 THEN 'Her ' ELSE 'His ' END + 'contact information is as follows: ' AS NCCAdj,
 --   CASE WHEN nm.PersonID = 3398 THEN 'is MSA''s National Coordinating Counsel.' ELSE '' END AS NCCClaimType,
 --   @ExhibitTableSimple AS ExhibitTableSimple, @ExhibitTableDetailed AS ExhibitTableDetailed, dtender.DocumentURL AS TenderDocURL, th.ApprovedBy, th.DateApproved
		  
	FROM tblTenderHistory th
	INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
	INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
	INNER JOIN tblClaimLawsuit cl ON cth.ClaimID = cl.ClaimID
	INNER JOIN tblClaim claim ON cth.ClaimID = claim.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON claim.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
	LEFT JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN vtblPerson p ON nm.PersonID = p.PersonID
	LEFT JOIN tblDefenseCounselAddress ncca ON p.FirmAddressID = ncca.DefenseCounselAddressID
	LEFT JOIN tblJurisdiction j ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblState s ON l.StateID = s.StateID
	LEFT JOIN tblCourtType ct ON l.CourtTypeID = ct.CourtTypeID
	LEFT JOIN tblDocuments d ON th.ComplaintDocumentID = d.DocumentID
	LEFT JOIN tblDocuments dtender ON th.DocumentID = dtender.DocumentID
	LEFT JOIN tblTenderType tt ON th.TenderTypeID = tt.TenderTypeID
	LEFT JOIN tblTenderTemplate ttemp ON th.TenderTemplateID = ttemp.TenderTemplateID
	--INNER JOIN tblTenderTemplate ttemp ON th.TenderBlockID = ttemp.TenderBlockID AND th.TenderTypeID = ttemp.TenderTypeID AND a.MaterialID = ttemp.MaterialID
	WHERE th.TenderHistoryID = @TenderHistoryID

	


END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTenderReview] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTenderReview] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryTenderReview] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTenderReview] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTenderReview] TO [power_user]
GO
