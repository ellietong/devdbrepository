SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryGetFirmByEmail]
	-- Add the parameters for the stored procedure here
	@emailaddress nvarchar(1000)
AS
--DECLARE @emailaddress nvarchar(100)
--SET @emailaddress = 'crookc@kcicllc.com'
BEGIN
	--DECLARE @emaildomain nvarchar(1000)
	--SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress), LEN(@emailaddress)-CHARINDEX('@', @emailaddress)+1)
	IF RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'
	BEGIN
		SELECT DISTINCT DefenseCounselID AS FirmID, DefenseCounsel as Firm FROM tblDefenseCounselAddress
	END
	ELSE
	BEGIN
		SELECT DISTINCT FirmID, DefenseCounsel as Firm FROM vtblPerson INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
		WHERE @emailaddress = vtblPerson.Email
	END
		
	 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetFirmByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetFirmByEmail] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetFirmByEmail] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetFirmByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetFirmByEmail] TO [power_user]
GO
