SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmPolicyPStartDate]
(@policyID int = NULL)
AS

DECLARE @maxPolicyID int

SELECT @maxPolicyID  = MAX(PolicyID) FROM tblPolicy

SELECT MIN(PStartDate) AS MinPStartDate
FROM tblPolicyDates 
WHERE PolicyID >= COALESCE(@policyID, 0) AND PolicyID <= COALESCE(NULLIF(@policyID, 0), @maxPolicyID) 

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyPStartDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyPStartDate] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmPolicyPStartDate] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyPStartDate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyPStartDate] TO [power_user]
GO
