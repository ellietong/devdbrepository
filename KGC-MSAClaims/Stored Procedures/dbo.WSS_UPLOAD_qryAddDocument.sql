SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryAddDocument]
(
	@DetailDocumentTypeID int,					--aka ClaimantDocumentTypeID
	@ClaimantPersonalInfoID bigint = null,
	@CreatedBy nvarchar(250),
	@DateCreated datetime,
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	--@SPDocId nvarchar(50),	
	@Comments nvarchar(4000) = null,			--comments are optional	
	@FirmID int = null,
	@DocumentSourceID int,
	@MatterID int = null,
	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,
    @Adjustment money = null,
    @AdjustmentNotes nvarchar(4000) = null,
    @StatusID int = null,
    @VendorFirmID int = null,
    @PrivateComments nvarchar(4000) = null,
    @Approved bit = null,
    @ApprovedBy nvarchar(250) = null,
    @DateApproved datetime = null
)

AS
DECLARE @MaxDocID int
SET @MaxDocID = (SELECT IDENT_CURRENT('tblDocuments')+IDENT_INCR('tblDocuments'))

IF(RIGHT(@CreatedBy, 11) = 'kcicllc.com') OR RIGHT(@CreatedBy, 8) = 'kcic.com' OR (@DetailDocumentTypeID IN (40, 41))
BEGIN
INSERT INTO tblDocuments (
	DetailDocumentTypeID,	
	ClaimantPersonalInfoID,
	Comments,
	DocumentURL,
	DocumentTitle,
	--SPDocId,
	CreatedBy,
	DateCreated,
	InvoiceNumber,
	Period,
	DatePayable,
	FeeAmount,
	DisbursementAmount,
    Adjustment,
    AdjustmentNotes,
    StatusID,	
	ModifiedBy,
	DateModified,
	FirmID,
	DocumentSourceID,
	VendorFirmID,
	ApprovedBy,
	DateApproved
)
VALUES (	
	@DetailDocumentTypeID,
	@ClaimantPersonalInfoID,
	@Comments,
	REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
	@DocumentTitle,
	--@SPDocId,
	@CreatedBy,
	@DateCreated,
	@InvoiceNumber,
	@Period,
	@DatePayable,
	@FeeAmount,
	@DisbursementAmount,
    @Adjustment,
    @AdjustmentNotes,
    @StatusID,	
	@CreatedBy,
	@DateCreated,	
	@FirmID,
	@DocumentSourceID,
	@VendorFirmID,
	@ApprovedBy,
	@DateApproved
)

SELECT @MaxDocID
END
ELSE
BEGIN
INSERT INTO tblDocuments (
	DetailDocumentTypeID,	
	ClaimantPersonalInfoID,
	Comments,
	DocumentURL,
	DocumentTitle,
	--SPDocId,
	CreatedBy,
	DateCreated,
	InvoiceNumber,
	Period,
	DatePayable,
	FeeAmount,
	DisbursementAmount,
    Adjustment,
    AdjustmentNotes,
    StatusID,	
	ModifiedBy,
	DateModified,
	FirmID,
	DocumentSourceID,
	VendorFirmID,
	ApprovedBy,
	DateApproved,
	Accepted,
    AcceptedBy,
    DateAccepted
)
VALUES (	
	@DetailDocumentTypeID,
	@ClaimantPersonalInfoID,
	@Comments,
	REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
	@DocumentTitle,
	--@SPDocId,
	@CreatedBy,
	@DateCreated,
	@InvoiceNumber,
	@Period,
	@DatePayable,
	@FeeAmount,
	@DisbursementAmount,
    @Adjustment,
    @AdjustmentNotes,
    @StatusID,	
	@CreatedBy,
	@DateCreated,	
	@FirmID,
	@DocumentSourceID,
	@VendorFirmID,
	@ApprovedBy,
	@DateApproved,
	1,
	'msa@kcic.com',
	GETDATE()
)

SELECT @MaxDocID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [power_user]
GO
