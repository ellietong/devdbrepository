SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantExposureInfo]
(@ClaimantPersonalInfoID bigint, @Emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--DECLARE @ClaimantPersonalInfoID bigint = 23576
	--DECLARE @EmailAddress nvarchar(250) = 'owenss@kcic.com'

	DECLARE @InsurerRole bit
	SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)					
						
	DECLARE @Products nvarchar(MAX)
	SET @Products = ''
	DECLARE @product nvarchar(100) = ''
	
	IF(@InsurerRole = 1)
	BEGIN
		DECLARE Cur_ClaimantProducts CURSOR Local Fast_Forward for
		SELECT DISTINCT COALESCE(ProductManufacturer, 'UNSPECIFIED') + ' - ' + COALESCE(ProductType, 'UNSPECIFIED') + ' - ' + COALESCE(ProductName, 'UNSPECIFIED') AS Product
		FROM
		tblProduct p
		LEFT Join tblProductName pn on pn.ProductNameID = p.ProductNameID
		LEFT Join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
		LEFT JOIN tblProductManufacturer pm on p.ProductManufacturerID = pm.ProductManufacturerID
		INNER JOIN tblClaimantProduct cp ON p.ProductID = cp.ProductID
		INNER JOIN tblClaimantEmployment ce ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
		INNER JOIN tblClaim c ON ce.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalinfoID
		AND p.ProductManufacturerID = 1
		AND c.ClaimID NOT IN (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
		AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
	END
	ELSE
	BEGIN
		DECLARE Cur_ClaimantProducts CURSOR Local Fast_Forward for
		SELECT DISTINCT COALESCE(ProductManufacturer, 'UNSPECIFIED') + ' - ' + COALESCE(ProductType, 'UNSPECIFIED') + ' - ' + COALESCE(ProductName, 'UNSPECIFIED') AS Product
		FROM
		tblProduct p
		LEFT Join tblProductName pn on pn.ProductNameID = p.ProductNameID
		LEFT Join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
		LEFT JOIN tblProductManufacturer pm on p.ProductManufacturerID = pm.ProductManufacturerID
		INNER JOIN tblClaimantProduct cp ON p.ProductID = cp.ProductID
		INNER JOIN tblClaimantEmployment ce ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
		INNER JOIN tblClaim c ON ce.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalinfoID
			AND p.ProductManufacturerID = 1

	END
	
	OPEN Cur_ClaimantProducts
	FETCH NEXT FROM Cur_ClaimantProducts INTO @product
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF(@Products = '')
		BEGIN
			SET @Products = '<table style="margin: 0; padding: 0;" cellspacing="0" cellpadding="0"><tr><td>' + @product + '</td></tr>'
		END
		ELSE
		BEGIN
			SET @Products = @Products + '<tr><td>' + @product + '</td></tr>'
		END
	
		FETCH NEXT FROM Cur_ClaimantProducts INTO @product
		IF(@@FETCH_STATUS < 0 AND @Products <> '')
		BEGIN
			SET @Products = @Products + '</table>'
		END
	END
	CLOSE Cur_ClaimantProducts
	DEALLOCATE Cur_ClaimantProducts
	
	SELECT COALESCE(CONVERT(nvarchar, dbo.fnEarliestExposure(@ClaimantPersonalInfoID), 101), 'No exposure data available.') AS EarliestExposure, CASE WHEN @Products = '' THEN 'No product data available.' ELSE @Products END AS Products
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantExposureInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantExposureInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimantExposureInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantExposureInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantExposureInfo] TO [power_user]
GO
