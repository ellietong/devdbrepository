SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryDelClaimTenderHistory]
(@ClaimTenderHistoryID bigint, @Emailaddress nvarchar(250))
AS
BEGIN
	SET NOCOUNT ON;

    --Test vars
    --DECLARE @TenderHistoryID bigint = 218951
    
    DECLARE @PrevClaimTenderHistoryID bigint = (SELECT PreviousClaimTenderHistoryID FROM tblClaimTenderHistory WHERE ClaimTenderHistoryID = @ClaimTenderHistoryID)
    
    --Check if the previous claim tender history ID exists, and if it does, 
    --reset the status back to Tendered for Historicals and Approved for non-historicals
    IF(@PrevClaimTenderHistoryID IS NOT NULL)
    BEGIN
		UPDATE tblClaimTenderHistory 
		SET TenderStatusID = CASE WHEN ApprovedBy LIKE '%Historical%' THEN 2 ELSE 5 END
		FROM tblClaimTenderHistory cth
		INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID
		WHERE ClaimTenderHistoryID = @PrevClaimTenderHistoryID
    END
    
    UPDATE tblClaimTenderHistory
    SET SPUsername = @Emailaddress
    WHERE ClaimTenderHistoryID = @ClaimTenderHistoryID
    
	DELETE FROM tblClaimTenderHistory
	WHERE ClaimTenderHistoryID = @ClaimTenderHistoryID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryDelClaimTenderHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryDelClaimTenderHistory] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryDelClaimTenderHistory] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryDelClaimTenderHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryDelClaimTenderHistory] TO [power_user]
GO
