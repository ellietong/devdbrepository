SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[REVERT_qryExternalEmailNewInsurerResponse_RS] 
AS
BEGIN


/*Query run daily to send an alert email to Reed Smith that an insurer response has been uploaded - CPC*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)

SET @Subject = 	
'MSA: Insurer Response Alert ' + CONVERT(varchar, getdate(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for insurer responses which have been uploaded in the past day.
DECLARE @NewResponseTable TABLE (ClaimantName nvarchar(250), ClaimantPersonalInfoID bigint, TenderBlockName nvarchar(250), CarrierName nvarchar(250), DocumentURL nvarchar(max), DocumentType nvarchar(250))
INSERT INTO @NewResponseTable
--Insurer response query
SELECT LastName + ', ' + FirstName + ' (' + NCCReferenceID + ')' AS ClaimantName, c.ClaimantPersonalInfoID, 
TenderBlockName, CarrierName, DocumentURL, ClaimantDocumentType 
FROM tblDocuments d
INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
INNER JOIN tblTenderResponse tr ON d.DocumentID = tr.DocumentID
INNER JOIN tblTenderHistory th ON tr.TenderHistoryID = th.TenderHistoryID
INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
INNER JOIN tblTenderContact tc ON tr.TenderContactID = tc.TenderContactID
INNER JOIN tblCarrier car ON tc.CarrierID = car.CarrierID
INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE DetailDocumentTypeID IN (45, 46,47) --Tender RESPONSE, DENIAL, RESERVATION OF RIGHTS
AND d.DateCreated BETWEEN GETDATE()-1 AND GETDATE()
AND RIGHT(CreatedBy, 11) = 'kcicllc.com'
ORDER BY LastName + ', ' + FirstName + ' (' + NCCReferenceID + ')'

--Declare and populate the body of the email for Reed Smith
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'



--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

IF (Select COUNT(DocumentURL) from @NewResponseTable) >0 
begin
set @BodyMessage = @BodyMessage + '<H4>The following insurer responses have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table>' + 
	
	
N'<tr><th>Claimant Name (NCC #)</th><th>Response Type</th><th>Tender Block</th><th>Insurer</th>' +
	
	
CAST ( (
SELECT td = '<a href="https://www.kcicextranet.com/clients/msa/cp/pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, ClaimantPersonalInfoID) + '&ClaimantTab=1&ClaimTab=4">' + ClaimantName + '</a>', '',
 td = '<a href="https://www.kcicextranet.com' + DocumentURL +' ">' + DocumentType + '</a>', '',
  td = TenderBlockName, '', td = CarrierName, ''
FROM @NewResponseTable R


for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
N'</table>' +  N'<br/>'	

end
ELSE
begin
Set @BodyMessage = @BodyMessage + '<H4>No responses have been uploaded in the last 24 hours. </H4>'
end


--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')


--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

  
DECLARE @RecipientList nvarchar(500)
SET @RecipientList=''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar(500)
DECLARE contact_curs CURSOR FOR
SELECT email 
FROM vtblPerson p
WHERE firmid in (31, 68) and complaintcontact = 1
	
AND complaintContact = 1
	

OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS=0
BEGIN 

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--PRINT @BodyMessage
--QC Recipient List
--Set @Bodymessage = @BodyMessage + '<br><br>Recipients: ' + @RecipientList
--PRINT @RecipientList

if (select count(ClaimantName) from @NewResponseTable) <>0
begin

EXEC msdb.dbo.sp_send_dbmail
--The DanaProfile sends from Dana@kcicllc.com
@profile_name='MSAProfile',
@recipients = @RecipientList, -- 'Hubbardb@kcicllc.com',--
@copy_recipients = 'quinnk@gotofirm.com; lechevallierg@gotofirm.com; hudsonj@gotofirm.com; msa@kcicllc.com',
--@blind_copy_recipients = 'msa@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

end

END


GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailNewInsurerResponse_RS] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailNewInsurerResponse_RS] TO [base_user]
GRANT ALTER ON  [dbo].[REVERT_qryExternalEmailNewInsurerResponse_RS] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailNewInsurerResponse_RS] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailNewInsurerResponse_RS] TO [power_user]
GO
