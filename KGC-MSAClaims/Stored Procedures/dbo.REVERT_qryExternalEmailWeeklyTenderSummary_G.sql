SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		owenss
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_qryExternalEmailWeeklyTenderSummary_G] 

AS
BEGIN
/*Query run daily to send an alert email to send a summary of the new tenders that have been created in the past week. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)

SET @Subject =    'MSA: Claims Tendered ' + CONVERT(nvarchar, GETDATE()-7, 107) + ' through ' + CONVERT(nvarchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Tenders that have been created in the past week.
DECLARE @TenderTable TABLE (ClaimantName nvarchar(250), TenderBlockName nvarchar(250), TenderType nvarchar(250), DateApproved date, NccRef# nvarchar (100), EffectiveDate date)
INSERT INTO @TenderTable
--Reed Smith Outstanding tenders (per tender type)
SELECT LastName + ', ' + FirstName , TenderBlockName, TenderType, DateApproved, NCCReferenceID, EffectiveDate
FROM tblTenderHistory th
INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
INNER JOIN tblTenderType tt ON th.TenderTypeID = tt.TenderTypeID
INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE DateApproved between GETDATE()-7 and GETDATE()
ORDER BY DateCreated ASC

--Declare and populate the body of the email for each tender type
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH    {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
UL {margin:0; 
	padding:0;}	      
--->
</STYLE>'





--Set table column headings and populate the cells with the Tenders from @TendersTable
IF (Select COUNT(ClaimantName) from @TenderTable) > 0 
begin
SET @BodyMessage = @BodyMessage + '<H4>The following claims were tendered by MSA in the previous week: </H4>'
SET @BodyMessage = @BodyMessage + N'<table>' + 
		N'<tr><th>Claimant Name</th><th>NCC #</th><th>Tender Type</th><th>Tender Block</th><th>Effective Date</th>' + 
		CAST((
		SELECT td = ClaimantName, '', td = NccRef#, '', td = TenderType, '',td = TenderBlockName, '', td = EffectiveDate 
		FROM @TenderTable
	for XML path ('tr'), TYPE) AS NVARCHAR(MAX) ) +
		N'</table>' +  N'<br/>'		
end
ELSE
begin
Set @BodyMessage = @BodyMessage + '<H4>There were no claims tendered by MSA in the previous week. </H4>'
end



--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')




----Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

  
DECLARE @RecipientList nvarchar(500)
SET @RecipientList=''

----Use the complaintContact field to determine the contact list.
----Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar(500)
DECLARE contact_curs CURSOR FOR

SELECT email 
FROM vtblPerson p
WHERE firmid = 66
      AND complaintContact = 1
      
OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS=0
BEGIN 

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--set @BodyMessage = @BodyMessage + '<br><br>Recipients: ' + @RecipientList

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The marleywylainProfile sends from marleywylain@kcicllc.com
@profile_name='MSAProfile',
@recipients = @RecipientList,--'hubbardb@kcicllc.com',--
@copy_recipients = 'msa@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

END

GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailWeeklyTenderSummary_G] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailWeeklyTenderSummary_G] TO [base_user]
GRANT ALTER ON  [dbo].[REVERT_qryExternalEmailWeeklyTenderSummary_G] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailWeeklyTenderSummary_G] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailWeeklyTenderSummary_G] TO [power_user]
GO
