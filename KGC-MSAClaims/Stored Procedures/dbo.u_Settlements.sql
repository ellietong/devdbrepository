SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[u_Settlements]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

select DISTINCT d.DocumentID, DocumentURL 
from tblClaim c
inner join tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
inner join tblClaimLawsuit cl on cl.ClaimID =c.ClaimID 
inner join tblLawsuit l on l.LawsuitID =cl.LawsuitID 
inner join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID =l.DefenseCounselAddressID 
inner join tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
inner join tblDocuments d on d.DocumentID =mcd.DocumentID 
inner join tblSingleClaimantDocumentType scdt on scdt.ClaimantDocumentTypeID =d.DetailDocumentTypeID 
left join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = scdt.SingleClaimantDocumentTypeID
left join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID =cl.ClaimLawsuitID 
left join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID =clst.ClaimLawsuitStatusID 
left join tblClaimLawsuitStatusGroup clsg on clsg.ClaimLawsuitStatusGroupID =cls.ClaimLawsuitStatusGroupID 
where scdt.SingleClaimantDocumentTypeID in (6,4,8,33,12,15,55,13,14,16,36,18,19,24,50,42,31,34,2,44,59) and NCCReferenceID  in ('NCC00043918',
'NCC00020214',
'NCC10000045',
'NCC00045264',
'NCC00020231',
'NCC10000485',
'NCC00031612',
'NCC00044217',
'NCC09000521',
'NCC09000595',
'NCC00021783',
'NCC00020372',
'NCC11000077',
'NCC09000153',
'NCC09000527',
'NCC00043785',
'NCC00020414',
'NCC00020418',
'NCC00019861',
'NCC11000151',
'NCC00044404',
'NCC00044415',
'NCC09000152',
'NCC09000443',
'NCC11000354',
'NCC00044574',
'NCC00016059',
'NCC00043519',
'NCC11000059',
'NCC11000031',
'NCC00002509',
'NCC10000192',
'NCC00002644',
'NCC00043412',
'NCC10000518',
'NCC11000131',
'NCC00016057',
'NCC00044810',
'NCC00044846',
'NCC11000071',
'NCC00020609',
'NCC00044920',
'NCC09000716',
'NCC00021385',
'NCC10000514',
'NCC00044956',
'NCC00031649',
'NCC00044993',
'NCC00020680',
'NCC00011592',
'NCC09000707',
'NCC09000031',
'NCC10000503',
'NCC09000496',
'NCC09000452',
'NCC00045276', 'NCC00044560',
'NCC00044839',
'NCC10000483') and IsPrimaryForClaim=1 and IsPrimaryStatus =1 and IsCurrentClaim =1

END
GO
GRANT VIEW DEFINITION ON  [dbo].[u_Settlements] TO [base_user]
GRANT EXECUTE ON  [dbo].[u_Settlements] TO [base_user]
GRANT ALTER ON  [dbo].[u_Settlements] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[u_Settlements] TO [power_user]
GRANT EXECUTE ON  [dbo].[u_Settlements] TO [power_user]
GO
