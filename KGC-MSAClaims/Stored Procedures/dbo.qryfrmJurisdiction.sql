SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmJurisdiction]
as
Select JurisdictionID, Jurisdiction
From tblJurisdiction
Order By Jurisdiction
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmJurisdiction] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmJurisdiction] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmJurisdiction] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmJurisdiction] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmJurisdiction] TO [power_user]
GO
