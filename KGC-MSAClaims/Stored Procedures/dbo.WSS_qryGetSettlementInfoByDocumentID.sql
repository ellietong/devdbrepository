SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetSettlementInfoByDocumentID]
(@DocumentID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT CONVERT(nvarchar(MAX), sd.SettleDocID) As SettleDocID, CONVERT(nvarchar(MAX), sd.DocCategoryID) as DocCategoryID, CONVERT(nvarchar(MAX), PageNums) As PageNums, CONVERT(nvarchar(MAX), SetPymtID) AS SetPymtID
    FROM tblDocuments d
    INNER JOIN tblSettleDocs sd ON d.DocumentID = sd.DocumentID
    INNER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
    WHERE d.DocumentID = @DocumentID
  
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetSettlementInfoByDocumentID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetSettlementInfoByDocumentID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetSettlementInfoByDocumentID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetSettlementInfoByDocumentID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetSettlementInfoByDocumentID] TO [power_user]
GO
