SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_Reports_qryTotalNewFilings]
	-- Add the parameters for the stored procedure here
	(@MonthID int, @YearID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--DECLARE @MonthID int
--DECLARE @YearID int
--SET @MonthID = 5
--SET @YearID = 2011
--Declare @MatterID int
--Set @MatterID = null



DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

select COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
where month(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))=@Month 
and year(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))=@Year 


END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Reports_qryTotalNewFilings] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Reports_qryTotalNewFilings] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_Reports_qryTotalNewFilings] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_Reports_qryTotalNewFilings] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_Reports_qryTotalNewFilings] TO [power_user]
GO
