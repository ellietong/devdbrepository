SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetClaimIDByClaimant_Matter](@ClaimantPersonalInfoID bigint, @MatterID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--SELECT TOP 1 ClaimID FROM [kga-DemoDB].dbo.tblClaim c 
	--WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND c.MatterID = @MatterID
	SELECT TOP 1 ClaimID FROM tblClaim c 
	LEFT JOIN m_tblClaimantPersonalInfo_Lookup cpil ON c.ClaimantPersonalInfoID = cpil.ClaimantPersonalInfoID
	WHERE (c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND c.MatterID = @MatterID) OR (cpil.INJ_PRTY_ID = @ClaimantPersonalInfoID AND c.MatterID = @MatterID)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetClaimIDByClaimant_Matter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetClaimIDByClaimant_Matter] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetClaimIDByClaimant_Matter] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetClaimIDByClaimant_Matter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetClaimIDByClaimant_Matter] TO [power_user]
GO
