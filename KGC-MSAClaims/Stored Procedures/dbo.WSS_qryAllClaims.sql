SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryAllClaims]
@ClaimantPersonalInfoID bigint=null, @emailaddress nvarchar(250)
AS
BEGIN

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)

DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)


IF (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1)
BEGIN
SELECT   DISTINCT  C.ClaimID, S.State, J.Jurisdiction, cs.ClaimSequence, Disease, Material, ClaimLawsuitStatusGroup, --hddis.HighLevelDisease AS Disease, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      cls.ClaimLawsuitStatus, L.ServiceDate AS ServiceDate, L.FileDate AS FileDate, L.DocketNumber, L.CaseCaption, 
                      L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  pca.PlaintiffCounsel, 
                      DefenseCounsel AS PrimaryDefenseCounsel,  CPI.ClaimantPersonalInfoID, ReleaseSignedDate, rt.ReleaseType, 
                      CLst.StatusNotes, SettlementGroupID,  ExecutedReleaseDate as ReleaseReceivedDate, SettlementDueDate, PaidDate, CheckNumber, SettlementAmount, ClaimDiseaseID,
                      CASE WHEN IsMDL = 0 THEN 'No' WHEN IsMDL = 1 THEN 'Yes' ELSE 'Unknown' END AS IsMDL, CASE WHEN IsThirdPartyComplaint = 0 THEN 'No' WHEN IsThirdPartyComplaint = 1 THEN 'Yes' ELSE 'Unknown' END AS IsThirdPartyComplaint, SettlementDate,
                      CASE WHEN cls.ClaimLawsuitStatusID = 22 THEN NULL ELSE StatusDate END AS ResolutionDate, ClaimLawsuitStatus AS ResolutionStatus, NCCReferenceID, COALESCE(IsCurrentClaim, 0)
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID Inner Join
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID LEFT JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID LEFT JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID LEFT JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
                      left join (SELECT * from tblClaimDisease where PrimaryDisease = 1) as CD on CD.ClaimID = c.ClaimID
					  left join tblDisease ddis on ddis.DiseaseID = cd.DiseaseID
					  left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID left outer join
					  (Select * From dbo.tblClaimLawsuitStatusTracking where COALESCE(IsPrimaryStatus, 0) = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatusGroup clsg ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
					  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
                      left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
				      LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
                      LEFT JOIN tblMaterial mat ON a.MaterialID = mat.MaterialID
WHERE CPI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID --AND cl.IsPrimaryForClaim = 1
	--and coalesce(cls.ClaimLawsuitStatusGroupID,0) <> 2
ORDER BY COALESCE(IsCurrentClaim, 0) DESC, L.ServiceDate DESC, L.FileDate DESC
END
ELSE

IF (@InsurerRole = 1)
BEGIN
SELECT   DISTINCT  C.ClaimID, Disease, S.State, J.Jurisdiction, cs.ClaimSequence, Material, ClaimLawsuitStatusGroup,
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      cls.ClaimLawsuitStatus, L.ServiceDate AS ServiceDate, L.FileDate AS FileDate, L.DocketNumber, L.CaseCaption, 
                      L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  pca.PlaintiffCounsel,
                      DefenseCounsel AS PrimaryDefenseCounsel,  CPI.ClaimantPersonalInfoID, ReleaseSignedDate, rt.ReleaseType, 
                      CLst.StatusNotes, SettlementGroupID,  ExecutedReleaseDate as ReleaseReceivedDate, SettlementDueDate, PaidDate, CheckNumber, SettlementAmount, ClaimDiseaseID,
                      CASE WHEN IsMDL = 0 THEN 'No' WHEN IsMDL = 1 THEN 'Yes' ELSE 'Unknown' END AS IsMDL, CASE WHEN IsThirdPartyComplaint = 0 THEN 'No' WHEN IsThirdPartyComplaint = 1 THEN 'Yes' ELSE 'Unknown' END AS IsThirdPartyComplaint, SettlementDate,
                      CASE WHEN cls.ClaimLawsuitStatusID = 22 THEN NULL ELSE StatusDate END AS ResolutionDate, ClaimLawsuitStatus AS ResolutionStatus, NCCReferenceID, COALESCE(IsCurrentClaim, 0)
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID Inner Join
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID LEFT JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID LEFT JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID LEFT JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
                      left join (SELECT * from tblClaimDisease where PrimaryDisease = 1) as CD on CD.ClaimID = c.ClaimID
					  left join tblDisease ddis on ddis.DiseaseID = cd.DiseaseID
					  left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID left outer join
					  (Select * From dbo.tblClaimLawsuitStatusTracking where COALESCE(IsPrimaryStatus, 0) = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatusGroup clsg ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
					  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
                      left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
				      LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
                      LEFT JOIN tblMaterial mat ON a.MaterialID = mat.MaterialID                      
WHERE CPI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND c.ClaimID NOT IN (select cth.ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
and coalesce(cls.ClaimLawsuitStatusGroupID,0) <> 2
AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
ORDER BY COALESCE(IsCurrentClaim, 0) DESC, L.ServiceDate DESC, L.FileDate DESC
END

ELSE

BEGIN
SELECT DISTINCT     C.ClaimID, Disease, S.State, J.Jurisdiction, Material, ClaimLawsuitStatusGroup, --cs.ClaimSequence, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      cls.ClaimLawsuitStatus, -- L.CaseCaption, 
                      L.ServiceDate AS ServiceDate, L.FileDate AS FileDate,
                     -- L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName, 
                       pca.PlaintiffCounsel, 
                      DefenseCounsel AS PrimaryDefenseCounsel,  CPI.ClaimantPersonalInfoID, ReleaseSignedDate, rt.ReleaseType, 
                      CLst.StatusNotes, SettlementGroupID,  ExecutedReleaseDate as ReleaseReceivedDate, SettlementDueDate, PaidDate, CheckNumber, SettlementAmount, ClaimDiseaseID,
                      CASE WHEN IsMDL = 0 THEN 'No' WHEN IsMDL = 1 THEN 'Yes' ELSE 'Unknown' END AS IsMDL, CASE WHEN IsThirdPartyComplaint = 0 THEN 'No' WHEN IsThirdPartyComplaint = 1 THEN 'Yes' ELSE 'Unknown' END AS IsThirdPartyComplaint, SettlementDate,
                      CASE WHEN cls.ClaimLawsuitStatusID = 22 THEN NULL ELSE StatusDate END AS ResolutionDate, ClaimLawsuitStatus AS ResolutionStatus, NCCReferenceID, COALESCE(IsCurrentClaim, 0)
--CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, L.DocketNumber,
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID Inner Join
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID LEFT JOIN
                      dbo.tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID LEFT JOIn
                      dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID
                      left JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
				      LEFT OUTER JOIN (SELECT * from tblClaimDisease where PrimaryDisease = 1) as CD on CD.ClaimID = c.ClaimID
					  left join tblDisease ddis on ddis.DiseaseID = cd.DiseaseID
					  left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID left outer join
					  (Select * From dbo.tblClaimLawsuitStatusTracking where COALESCE(IsPrimaryStatus, 0) = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatusGroup clsg ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
					  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
                      left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
				      LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
                      LEFT JOIN tblMaterial mat ON a.MaterialID = mat.MaterialID                      
WHERE CPI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID) --AND cl.IsPrimaryForClaim = 1
--and coalesce(cls.ClaimLawsuitStatusGroupID,0) <> 2
ORDER BY COALESCE(IsCurrentClaim, 0) DESC, L.ServiceDate DESC, L.FileDate DESC
END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAllClaims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAllClaims] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryAllClaims] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAllClaims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAllClaims] TO [power_user]
GO
