SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_MSAProduction2_TenderHistory]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--documents linked to tblTenderHistory 755 documents		
select d.DocumentID, d.DocumentTitle, d.DocumentURL, TenderBlockName--, th.DateApproved		
from tblTenderHistory th		
inner join tblDocuments d on th.DocumentID = d.DocumentID		
inner join tblClaimTenderHistory cth on th.TenderHistoryID = cth.TenderHistoryID		
inner join tblTenderBlock tb on tb.TenderBlockID = th.TenderBlockID		
where th.DateApproved <= '8/10/2012'		
group by d.DocumentID, d.DocumentTitle, d.DocumentURL, TenderBlockName--, th.DateApproved		
order by d.DocumentID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_MSAProduction2_TenderHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_MSAProduction2_TenderHistory] TO [base_user]
GRANT ALTER ON  [dbo].[m_MSAProduction2_TenderHistory] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_MSAProduction2_TenderHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_MSAProduction2_TenderHistory] TO [power_user]
GO
