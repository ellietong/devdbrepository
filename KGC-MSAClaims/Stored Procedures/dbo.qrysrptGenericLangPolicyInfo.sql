SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[qrysrptGenericLangPolicyInfo]
(@GenericLanguageID int = null)
as
set nocount on 



	SELECT     p.PolicyID, c.CarrierName, p.PolicyNum, 
		convert(nvarchar,MIN(PStartDate), 103) + ' - ' + CONVERT(nvarchar, MAX(PEndDate), 103) AS PolicyDates
	FROM   tblPolicy p
			inner join tblCarrier c on c.CarrierID = p.CarrierID 
			INNER JOIN tblExactLanguage el on el.PolicyID = p.policyid
	        INNER JOIN tblGenericLanguage gl on gl.GenericLanguageID=el.GenericLanguageID               
	        Inner Join tblPolicyDates PD ON p.PolicyID = pd.PolicyID 
	WHERE   el.GenericLanguageID = @GenericLanguageID
	GROUP BY p.PolicyID, c.CarrierName, p.PolicyNum
	ORDER BY carriername, MIN(pstartdate)






GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptGenericLangPolicyInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptGenericLangPolicyInfo] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptGenericLangPolicyInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptGenericLangPolicyInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptGenericLangPolicyInfo] TO [power_user]
GO
