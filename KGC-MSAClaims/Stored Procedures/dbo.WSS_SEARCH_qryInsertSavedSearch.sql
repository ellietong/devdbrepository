SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryInsertSavedSearch]
(@Title nvarchar(max), @EmailAddress nvarchar(max))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO tblSavedSearch
	SELECT @Title, @EmailAddress
	
	SELECT MAX(SavedSearchID) FROM tblSavedSearch
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryInsertSavedSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryInsertSavedSearch] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SEARCH_qryInsertSavedSearch] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryInsertSavedSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryInsertSavedSearch] TO [power_user]
GO
