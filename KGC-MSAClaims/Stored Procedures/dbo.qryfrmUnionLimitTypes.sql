SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmUnionLimitTypes] 
AS
SET NOCOUNT ON

SELECT *
FROM tblPolicyLimitType
ORDER BY LimitTypeID 

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionLimitTypes] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionLimitTypes] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmUnionLimitTypes] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionLimitTypes] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionLimitTypes] TO [power_user]
GO
