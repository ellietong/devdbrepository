SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDelTenderHistory_REVERT]
(@TenderHistoryID bigint, @Emailaddress nvarchar(250))
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE tblTenderHistory
	SET SPUsername = @Emailaddress
	WHERE TenderHistoryID = @TenderHistoryID
	
	DELETE FROM tblTenderHistory
	WHERE TenderHistoryID = @TenderHistoryID
	
	DELETE FROM tblTenderAcknowledgement
	WHERE TenderHistoryID = @TenderHistoryID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelTenderHistory_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelTenderHistory_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryDelTenderHistory_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelTenderHistory_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelTenderHistory_REVERT] TO [power_user]
GO
