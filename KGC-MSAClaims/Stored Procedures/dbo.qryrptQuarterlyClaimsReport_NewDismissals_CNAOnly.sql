SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptQuarterlyClaimsReport_NewDismissals_CNAOnly] (@Quarter int, @Year varchar(max), @NextYear varchar(max)) 
AS
SET NOCOUNT ON


--Declare @Quarter int
--Set @Quarter = 2
--Declare @Year nvarchar(25)
--Set @Year = '2014'

--get last quarter start date
DECLARE @PreviousQuarterStart varchar(25)
SET @PreviousQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '1/1/' + @Year
		 WHEN @Quarter = 2 THEN '4/1/' + @Year
		 WHEN @Quarter = 3 THEN '7/1/' + @Year
		 WHEN @Quarter = 4 THEN '10/1/' + @Year
		 ELSE '1/1/2050' END

--get first day of next quarter
DECLARE @NextQuarterStart varchar(25)
SET @NextQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '4/1/' + @Year
		 WHEN @Quarter = 2 THEN '7/1/' + @Year
		 WHEN @Quarter = 3 THEN '10/1/' + @Year
		 WHEN @Quarter = 4 THEN '1/1/' + @NextYear
		 ELSE '12/31/2050' END


	SELECT CASE WHEN COALESCE(dbo.fnEarliestExposure(C.ClaimantPersonalInfoID), '1/1/1900') <= '4/1/1985' THEN 'Yes' ELSE 'No' END AS Pre1985Designate,
		   FullNameState,
		   CauseAgent AS Material,
		   NCCReferenceID, 
		   LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,'') as Claimant,
		   CaseCaption,
		   DocketNumber,
		   ClaimLawsuitStatus,
		   Convert(varchar, FileDate, 101) as FileDate,
		   Convert(varchar, StatusDate, 101) as StatusDate,
		   Convert(varchar, ActiveTrialDate, 101) as ActiveTrialDate,
		   Convert(varchar, ProcessedDate, 101) as ProcessedDate,
		   SettlementAmount
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN dbo.tblClaim(nolock) c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN dbo.tblClaimLawsuit(nolock) cl ON cl.claimID = c.claimID
		  LEFT JOIN dbo.tblLawsuit(nolock) l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN dbo.tblClaimLawsuitStatusTracking(nolock) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN dbo.tblClaimLawsuitStatus(nolock) cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN dbo.tblJurisdiction(nolock) j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN dbo.tblState(nolock) s on s.StateID = l.StateID
		  LEFT JOIN dbo.tblAllegation(nolock) a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN dbo.tblMaterial(nolock) m on m.MaterialID = a.MaterialID
		  LEFT JOIN dbo.tblClaimantSettlement(nolock) cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		 -- LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
	WHERE clst.IsPrimaryStatus = 1
		  AND cls.ClaimLawsuitStatusGroupID = 4
		  AND COALESCE(ProcessedDate, StatusDate,'1/1/1900') < @NextQuarterStart AND COALESCE(ProcessedDate, StatusDate,'1/1/1900')  >= @PreviousQuarterStart
		  AND m.MaterialID != 10
  		  AND c.InsurableFlag =1
		  AND TortTypeID = 1
		  AND c.ClaimID != 26234
		  --------AND NCCReferenceID NOT IN (SELECT NCCReferenceID 
				--------	FROM tblClaim C
				--------	LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
				--------	LEFT JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID = CL.ClaimLawsuitID
				--------	LEFT JOIN tblClaimLawsuitStatus CLS ON CLS.ClaimLawsuitStatusID = CLST.ClaimLawsuitStatusID
				--------	WHERE IsPrimaryStatus <> 1
				--------	AND ClaimLawsuitStatusGroupID = 4)
	GROUP BY CASE WHEN COALESCE(dbo.fnEarliestExposure(C.ClaimantPersonalInfoID), '1/1/1900') <= '4/1/1985' THEN 'Yes' ELSE 'No' END,
	       FullNameState,
		   CauseAgent,
		   NCCReferenceID, 
		   LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,''),
		   CaseCaption,
		   DocketNumber,
		   ClaimLawsuitStatus,
		   Convert(varchar, FileDate, 101),
		   Convert(varchar, StatusDate, 101),
		   Convert(varchar, ActiveTrialDate, 101),
		   Convert(varchar, ProcessedDate, 101),
		   SettlementAmount
	ORDER BY FullNameState, CauseAgent, ProcessedDate, LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,'')


RETURN


--USE [KGC-MSAClaims]
--GO
/****** Object:  StoredProcedure [dbo].[qryrptQuarterlyClaimsReport_NewFilings_CNAOnly]    Script Date: 1/30/2015 2:33:40 PM ******/
SET ANSI_NULLS ON
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_NewDismissals_CNAOnly] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_NewDismissals_CNAOnly] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptQuarterlyClaimsReport_NewDismissals_CNAOnly] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_NewDismissals_CNAOnly] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_NewDismissals_CNAOnly] TO [power_user]
GO
