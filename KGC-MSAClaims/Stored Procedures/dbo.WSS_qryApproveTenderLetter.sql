SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryApproveTenderLetter]
(@TenderHistoryID bigint, @Emailaddress nvarchar(500))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblTenderHistory
	SET ApprovedBy = @Emailaddress,
		DateApproved = GETDATE(),
		TenderStatusID = 5
	WHERE TenderHistoryID = @TenderHistoryID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryApproveTenderLetter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryApproveTenderLetter] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryApproveTenderLetter] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryApproveTenderLetter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryApproveTenderLetter] TO [power_user]
GO
