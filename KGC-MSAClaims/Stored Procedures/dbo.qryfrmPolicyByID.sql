SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmPolicyByID] (@AllocationID int = null)
AS
SET NOCOUNT ON


--Declare @allocationid int
--set @AllocationID = 5

If @AllocationID is null 

SELECT p.PolicyID, PolicyNum + ' ' + CarrierName + ' ' + CONVERT(varchar, PStartDate, 101) + ' ' + InsuranceProgram AS PolicyInfo
FROM tblInsuranceProgram ip INNER JOIN ( (tblPolicy p INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID) 
     INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID) ON ip.InsuranceProgramID = p.InsuranceProgramID

GROUP BY p.PolicyID, PolicyNum + ' ' + CarrierName + ' ' + CONVERT(varchar, PStartDate, 101) + ' ' + InsuranceProgram, p.PolicyNum, c.CarrierName
ORDER BY p.PolicyNum, c.CarrierName, Min(pd.PStartDate)

ELSE

SELECT p.PolicyID, PolicyNum + ' ' + CarrierName + ' ' + CONVERT(varchar, PStartDate, 101) + ' ' + InsuranceProgram AS PolicyInfo
FROM tblInsuranceProgram ip INNER JOIN ( (tblPolicy p INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID) 
     INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID) ON ip.InsuranceProgramID = p.InsuranceProgramID

WHERE p.PolicyID not in 
(select PolicyID from tblProvision where ProvisionValueID in 
(select ProvisionValueID from tblAllocationProvisionToExclude 
	where AllocationID = @AllocationID))

GROUP BY p.PolicyID, PolicyNum + ' ' + CarrierName + ' ' + CONVERT(varchar, PStartDate, 101) + ' ' + InsuranceProgram, p.PolicyNum, c.CarrierName
ORDER BY p.PolicyNum, c.CarrierName, Min(pd.PStartDate)

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyByID] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmPolicyByID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyByID] TO [power_user]
GO
