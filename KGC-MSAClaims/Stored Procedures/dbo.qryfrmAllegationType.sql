SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmAllegationType]

as

Select AllegationTypeID, AllegationType
From tblAllegationType
Order By AllegationType

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllegationType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllegationType] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmAllegationType] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllegationType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllegationType] TO [power_user]
GO
