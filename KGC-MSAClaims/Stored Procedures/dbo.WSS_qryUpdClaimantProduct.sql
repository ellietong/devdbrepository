SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_qryUpdClaimantProduct]
(@ClaimantProductID int, @ProductID nvarchar(150), @StartDate datetime, @EndDate datetime, @Username nvarchar(250), @Notes nvarchar(max), @SupportingDocID int, @SupportingPageNums nvarchar(50))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE tblClaimantProduct
    SET ProductID = @ProductID,
		StartDate = @StartDate,
		EndDate = @EndDate,
		Notes = @Notes, 
		SPUsername = @Username,
		SupportingDocID = @SupportingDocID,
		SupportingPageNums = @SupportingPageNums
	WHERE ClaimantProductID = @ClaimantProductID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantProduct] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdClaimantProduct] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantProduct] TO [power_user]
GO
