SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmCarrierContactSelect]
As

SET NOCOUNT ON

select CarrierContactID, CarrierNickname +  ' - ' + ContactClaimType +' - '+ coalesce(contacttype,'')+ '  -  ' + coalesce(Lastname + ', ','') + 
		coalesce(firstname + '   ', '') + coalesce(addressline1 + '   ', '') + COALESCE( addressline2+ '  ','')   + coalesce(city + ', ', '') + 
		coalesce(state, '') as contactmerge

from tblCarrierContact cc
	inner join tblCarrier car on car.CarrierID = cc.CarrierID
	inner join tblcontact c on c.contactid = cc.contactid
	inner join tblState st on st.StateID=c.StateID
	inner join tblContactType ct on ct.ContactTypeID = c.ContactTypeID
	inner join tblContactClaimType clt on clt.ContactClaimTypeID=c.ContactClaimTypeID

Order by CarrierNickname, ContactClaimType, ContactType, LastName


RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierContactSelect] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierContactSelect] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmCarrierContactSelect] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierContactSelect] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierContactSelect] TO [power_user]
GO
