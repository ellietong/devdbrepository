SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_qryInvoiceStatusFilter]
AS
SET NOCOUNT ON

SELECT statusid, status
FROM tblDefenseInvoiceStatus
WHERE StatusID NOT IN (5, 1, 3, 4)
UNION
SELECT NULL, '<< All Statuses >>'
ORDER BY Status
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInvoiceStatusFilter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInvoiceStatusFilter] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryInvoiceStatusFilter] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInvoiceStatusFilter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInvoiceStatusFilter] TO [power_user]
GO
