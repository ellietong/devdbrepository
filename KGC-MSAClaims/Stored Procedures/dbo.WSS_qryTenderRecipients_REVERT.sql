SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryTenderRecipients_REVERT]
(@TenderHistoryID bigint)
AS

--SELECT * FROM tblTenderHistory ORDER BY TenderHistoryID DESC
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--DECLARE @TenderHistoryID int
	--SET @TenderHistoryID = 172940
    -- Insert statements for procedure here
    DECLARE @DocumentURL nvarchar(MAX)
    DECLARE @CPILastName nvarchar(100)
    
    SET @DocumentURL = (SELECT d.DocumentURL	
		FROM tblTenderHistory th
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		WHERE TenderHistoryID = @TenderHistoryID)
	SET @CPILastName = (SELECT cpi.LastName
		FROM tblTenderHistory th
		LEFT JOIN tblClaim c ON th.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE TenderHistoryID = @TenderHistoryID)
    
    DECLARE @Recpients TABLE (Email nvarchar(MAX), [To] bit, [CC] bit, BCC bit, LastName nvarchar(200), DocumentURL nvarchar(MAX), ContactName nvarchar(MAX), [Hash] char(32))

	
	IF(@TenderHistoryID IN (172032))
	BEGIN
		INSERT INTO @Recpients
		SELECT tc.Email, tc.[To], tc.CC, tc.BCC, cpi.LastName, d.DocumentURL, LName + ', ' + FName AS ContactName,  COALESCE(Hash, '')
		FROM tblTenderHistory th
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
		INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
		LEFT JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
		LEFT JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID  AND tc.TenderContactID = ta.TenderContactID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		LEFT JOIN tblClaim c ON th.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE tc.CurrentContact = 1 
			AND (th.TenderHistoryID = @TenderHistoryID) --People for that specific tender block, or always CC
		UNION
		--Add in people who should always be BCC'd (not related to carriers)
		SELECT Email, 0, 0, BCC, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContactForMassRetender WHERE CarrierID IS NULL AND BCC = 1 AND TenderContactID <> 22 AND CurrentContact = 1
		UNION
		--Add in people who should always be CC'd (not related to carriers)
		SELECT Email, 0, CC, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContactForMassRetender WHERE CarrierID IS NULL AND CC = 1 AND CurrentContact = 1
	END
	ELSE IF(@TenderHistoryID IN (172033))
	BEGIN
		INSERT INTO @Recpients
		SELECT tc.Email, tc.[To], tc.CC, tc.BCC, cpi.LastName, d.DocumentURL, LName + ', ' + FName AS ContactName,  COALESCE(Hash, '')
		FROM tblTenderHistory th
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
		INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
		LEFT JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
		LEFT JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID  AND tc.TenderContactID = ta.TenderContactID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		LEFT JOIN tblClaim c ON th.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE tc.CurrentContact = 1 
			AND (th.TenderHistoryID = @TenderHistoryID) --People for that specific tender block, or always CC
		UNION
		--Add in people who should always be BCC'd (not related to carriers)
		SELECT Email, 0, 0, BCC, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContactForMassRetender WHERE CarrierID IS NULL AND BCC = 1 AND CurrentContact = 1
		UNION
		--Add in people who should always be CC'd (not related to carriers)
		SELECT Email, 0, CC, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContactForMassRetender WHERE CarrierID IS NULL AND CC = 1 AND CurrentContact = 1
	END
	ELSE IF (@TenderHistoryID IN (171964))
	BEGIN
		INSERT INTO @Recpients
		SELECT tc.Email, tc.[To], tc.CC, tc.BCC, cpi.LastName, d.DocumentURL, LName + ', ' + FName AS ContactName, COALESCE(Hash, '')
		FROM tblTenderHistory th
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
		INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
		LEFT JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
		LEFT JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID  AND tc.TenderContactID = ta.TenderContactID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		LEFT JOIN tblClaim c ON th.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE tc.CurrentContact = 1 
			AND (th.TenderHistoryID = @TenderHistoryID) --People for that specific tender block, or always CC
		UNION
		--Union on NCC contact depending on material type
		SELECT email, 0, 1, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE th.TenderHistoryID = @TenderHistoryID
		UNION
		--Add in people who should always be BCC'd (not related to carriers)
		SELECT Email, 0, 0, AlwaysBCC, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysBCC = 1
		UNION
		--Add in people who should always be CC'd (not related to carriers)
		SELECT Email, 0, AlwaysCC, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact CCs
		SELECT p.Email, 0, 1, 0, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactCC = 1 AND c.ClaimID = (SELECT ClaimID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
		UNION
		--Add in defense counsel tender contact BCCs
		SELECT p.Email, 0, 0, 1, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactBCC = 1 AND c.ClaimID = (SELECT ClaimID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
		UNION
		--Add in Karen Maston
		SELECT Email, 0, CC, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContactForMassRetender WHERE TenderContactID = 4
	END
	ELSE IF (@TenderHistoryID IN (172254)) --Letter 1
	BEGIN
		INSERT INTO @Recpients
		SELECT tc.Email, tc.[To], tc.CC, tc.BCC, cpi.LastName, d.DocumentURL, LName + ', ' + FName AS ContactName, COALESCE(Hash, '')
		FROM tblTenderHistory th
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
		INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
		LEFT JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
		LEFT JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID  AND tc.TenderContactID = ta.TenderContactID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		LEFT JOIN tblClaim c ON th.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE tc.CurrentContact = 1 
			AND (th.TenderHistoryID = @TenderHistoryID) --People for that specific tender block, or always CC
		UNION
		--Union on NCC contact depending on material type
		SELECT email, 0, 1, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE th.TenderHistoryID = @TenderHistoryID
		UNION
		--Add in people who should always be BCC'd (not related to carriers)
		SELECT Email, 0, 0, AlwaysBCC, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysBCC = 1
		UNION
		--Add in people who should always be CC'd (not related to carriers)
		SELECT Email, 0, AlwaysCC, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact CCs
		SELECT DISTINCT p.Email, 0, 1, 0, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactCC = 1 AND c.NCCReferenceID IN ('NCC00040640', 
			'NCC00050751', 
			'NCC10000013', 
			'NCC10000046', 
			'NCC11000002', 
			'NCC11000008', 
			'NCC11000011', 
			'NCC11000018', 
			'NCC11000019', 
			'NCC11000020', 
			'NCC11000024', 
			'NCC11000027', 
			'NCC11000036', 
			'NCC11000042', 
			'NCC11000056', 
			'NCC11000058', 
			'NCC11000072', 
			'NCC11000078', 
			'NCC11000082', 
			'NCC11000084', 
			'NCC11000085', 
			'NCC11000089', 
			'NCC11000091', 
			'NCC11000092', 
			'NCC11000096', 
			'NCC11000098', 
			'NCC11000099', 
			'NCC11000100', 
			'NCC11000103', 
			'NCC11000106', 
			'NCC11000107', 
			'NCC11000125', 
			'NCC11000130', 
			'NCC11000137', 
			'NCC11000138', 
			'NCC11000139', 
			'NCC11000146', 
			'NCC11000147', 
			'NCC11000153', 
			'NCC11000154', 
			'NCC11000156', 
			'NCC11000157', 
			'NCC11000158', 
			'NCC11000164', 
			'NCC11000165', 
			'NCC11000169', 
			'NCC11000249', 
			'NCC11000250', 
			'NCC11000255', 
			'NCC11000256', 
			'NCC11000259', 
			'NCC11000273', 
			'NCC11000275', 
			'NCC11000299', 
			'NCC11000300', 
			'NCC11000301', 
			'NCC11000302', 
			'NCC11000303', 
			'NCC11000309', 
			'NCC11000310', 
			'NCC11000315', 
			'NCC11000325', 
			'NCC11000326', 
			'NCC11000328', 
			'NCC11000329', 
			'NCC11000330', 
			'NCC11000331', 
			'NCC11000333', 
			'NCC11000335', 
			'NCC11000336', 
			'NCC11000337', 
			'NCC11000338', 
			'NCC11000339', 
			'NCC11000341', 
			'NCC11000342', 
			'NCC11000343', 
			'NCC11000344', 
			'NCC11000345', 
			'NCC11000346', 
			'NCC11000347', 
			'NCC11000350', 
			'NCC11000351')
		UNION
		--Add in defense counsel tender contact BCCs
		SELECT DISTINCT p.Email, 0, 0, 1, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactBCC = 1 AND c.NCCReferenceID IN ('NCC00040640', 
			'NCC00050751', 
			'NCC10000013', 
			'NCC10000046', 
			'NCC11000002', 
			'NCC11000008', 
			'NCC11000011', 
			'NCC11000018', 
			'NCC11000019', 
			'NCC11000020', 
			'NCC11000024', 
			'NCC11000027', 
			'NCC11000036', 
			'NCC11000042', 
			'NCC11000056', 
			'NCC11000058', 
			'NCC11000072', 
			'NCC11000078', 
			'NCC11000082', 
			'NCC11000084', 
			'NCC11000085', 
			'NCC11000089', 
			'NCC11000091', 
			'NCC11000092', 
			'NCC11000096', 
			'NCC11000098', 
			'NCC11000099', 
			'NCC11000100', 
			'NCC11000103', 
			'NCC11000106', 
			'NCC11000107', 
			'NCC11000125', 
			'NCC11000130', 
			'NCC11000137', 
			'NCC11000138', 
			'NCC11000139', 
			'NCC11000146', 
			'NCC11000147', 
			'NCC11000153', 
			'NCC11000154', 
			'NCC11000156', 
			'NCC11000157', 
			'NCC11000158', 
			'NCC11000164', 
			'NCC11000165', 
			'NCC11000169', 
			'NCC11000249', 
			'NCC11000250', 
			'NCC11000255', 
			'NCC11000256', 
			'NCC11000259', 
			'NCC11000273', 
			'NCC11000275', 
			'NCC11000299', 
			'NCC11000300', 
			'NCC11000301', 
			'NCC11000302', 
			'NCC11000303', 
			'NCC11000309', 
			'NCC11000310', 
			'NCC11000315', 
			'NCC11000325', 
			'NCC11000326', 
			'NCC11000328', 
			'NCC11000329', 
			'NCC11000330', 
			'NCC11000331', 
			'NCC11000333', 
			'NCC11000335', 
			'NCC11000336', 
			'NCC11000337', 
			'NCC11000338', 
			'NCC11000339', 
			'NCC11000341', 
			'NCC11000342', 
			'NCC11000343', 
			'NCC11000344', 
			'NCC11000345', 
			'NCC11000346', 
			'NCC11000347', 
			'NCC11000350', 
			'NCC11000351')
	END
	ELSE IF (@TenderHistoryID IN (172255)) --Letter2
	BEGIN
		INSERT INTO @Recpients
		SELECT tc.Email, tc.[To], tc.CC, tc.BCC, cpi.LastName, d.DocumentURL, LName + ', ' + FName AS ContactName, COALESCE(Hash, '')
		FROM tblTenderHistory th
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
		INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
		LEFT JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
		LEFT JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID  AND tc.TenderContactID = ta.TenderContactID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		LEFT JOIN tblClaim c ON th.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE tc.CurrentContact = 1 
			AND (th.TenderHistoryID = @TenderHistoryID) --People for that specific tender block, or always CC
		UNION
		--Union on NCC contact depending on material type
		SELECT email, 0, 1, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE th.TenderHistoryID = @TenderHistoryID
		UNION
		--Add in people who should always be BCC'd (not related to carriers)
		SELECT Email, 0, 0, AlwaysBCC, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysBCC = 1
		UNION
		--Add in people who should always be CC'd (not related to carriers)
		SELECT Email, 0, AlwaysCC, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact CCs
		SELECT DISTINCT p.Email, 0, 1, 0, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactCC = 1 AND c.NCCReferenceID IN (
		'NCC00044135', 
		'NCC00045210', 
		'NCC11000004', 
		'NCC11000005', 
		'NCC11000006', 
		'NCC11000012', 
		'NCC11000013', 
		'NCC11000017', 
		'NCC11000026', 
		'NCC11000032', 
		'NCC11000034', 
		'NCC11000039', 
		'NCC11000041', 
		'NCC11000050', 
		'NCC11000053', 
		'NCC11000057', 
		'NCC11000066', 
		'NCC11000067', 
		'NCC11000070', 
		'NCC11000081', 
		'NCC11000083', 
		'NCC11000086', 
		'NCC11000094', 
		'NCC11000102', 
		'NCC11000108', 
		'NCC11000109', 
		'NCC11000111', 
		'NCC11000113', 
		'NCC11000119', 
		'NCC11000123', 
		'NCC11000124', 
		'NCC11000126', 
		'NCC11000135', 
		'NCC11000141', 
		'NCC11000142', 
		'NCC11000143', 
		'NCC11000148', 
		'NCC11000149', 
		'NCC11000155', 
		'NCC11000160', 
		'NCC11000161', 
		'NCC11000166', 
		'NCC11000251', 
		'NCC11000252', 
		'NCC11000260', 
		'NCC11000261', 
		'NCC11000262', 
		'NCC11000276', 
		'NCC11000304', 
		'NCC11000306', 
		'NCC11000327')
		UNION
		--Add in defense counsel tender contact BCCs
		SELECT DISTINCT p.Email, 0, 0, 1, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactBCC = 1 AND c.NCCReferenceID IN (
		'NCC00044135', 
		'NCC00045210', 
		'NCC11000004', 
		'NCC11000005', 
		'NCC11000006', 
		'NCC11000012', 
		'NCC11000013', 
		'NCC11000017', 
		'NCC11000026', 
		'NCC11000032', 
		'NCC11000034', 
		'NCC11000039', 
		'NCC11000041', 
		'NCC11000050', 
		'NCC11000053', 
		'NCC11000057', 
		'NCC11000066', 
		'NCC11000067', 
		'NCC11000070', 
		'NCC11000081', 
		'NCC11000083', 
		'NCC11000086', 
		'NCC11000094', 
		'NCC11000102', 
		'NCC11000108', 
		'NCC11000109', 
		'NCC11000111', 
		'NCC11000113', 
		'NCC11000119', 
		'NCC11000123', 
		'NCC11000124', 
		'NCC11000126', 
		'NCC11000135', 
		'NCC11000141', 
		'NCC11000142', 
		'NCC11000143', 
		'NCC11000148', 
		'NCC11000149', 
		'NCC11000155', 
		'NCC11000160', 
		'NCC11000161', 
		'NCC11000166', 
		'NCC11000251', 
		'NCC11000252', 
		'NCC11000260', 
		'NCC11000261', 
		'NCC11000262', 
		'NCC11000276', 
		'NCC11000304', 
		'NCC11000306', 
		'NCC11000327')

	END
	ELSE IF (@TenderHistoryID IN (172727)) --Mass Withdrawal Letter 05-09-2012
	BEGIN
		INSERT INTO @Recpients
		SELECT tc.Email, tc.[To], tc.CC, tc.BCC, cpi.LastName, d.DocumentURL, LName + ', ' + FName AS ContactName, COALESCE(Hash, '')
		FROM tblTenderHistory th
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
		INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
		LEFT JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
		LEFT JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID  AND tc.TenderContactID = ta.TenderContactID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		LEFT JOIN tblClaim c ON th.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE tc.CurrentContact = 1 
			AND (th.TenderHistoryID = @TenderHistoryID) --People for that specific tender block, or always CC
		UNION
		--Union on NCC contact depending on material type
		SELECT email, 0, 1, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE th.TenderHistoryID = @TenderHistoryID
		UNION
		--Add in people who should always be BCC'd (not related to carriers)
		SELECT Email, 0, 0, AlwaysBCC, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysBCC = 1
		UNION
		--Add in people who should always be CC'd (not related to carriers)
		SELECT Email, 0, AlwaysCC, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact CCs
		SELECT DISTINCT p.Email, 0, 1, 0, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactCC = 1 AND c.NCCReferenceID IN ('NCC10000040',
		'NCC10000041',
		'NCC10000206',
		'NCC11000003',
		'NCC11000021')
		UNION
		--Add in defense counsel tender contact BCCs
		SELECT DISTINCT p.Email, 0, 0, 1, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactBCC = 1 AND c.NCCReferenceID IN ('NCC10000040',
		'NCC10000041',
		'NCC10000206',
		'NCC11000003',
		'NCC11000021')
	END
	ELSE IF (@TenderHistoryID IN (172728)) --Mass Re-tender Letter 05-09-2012
	BEGIN
		INSERT INTO @Recpients
		SELECT tc.Email, tc.[To], tc.CC, tc.BCC, cpi.LastName, d.DocumentURL, LName + ', ' + FName AS ContactName, COALESCE(Hash, '')
		FROM tblTenderHistory th
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
		INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
		LEFT JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
		LEFT JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID  AND tc.TenderContactID = ta.TenderContactID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		LEFT JOIN tblClaim c ON th.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE tc.CurrentContact = 1 
			AND (th.TenderHistoryID = @TenderHistoryID) --People for that specific tender block, or always CC
		UNION
		--Union on NCC contact depending on material type
		SELECT email, 0, 1, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE p.PersonID = 3398	
		UNION
		--Add in people who should always be BCC'd (not related to carriers)
		SELECT Email, 0, 0, AlwaysBCC, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysBCC = 1
		UNION
		--Add in people who should always be CC'd (not related to carriers)
		SELECT Email, 0, AlwaysCC, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact CCs
		SELECT DISTINCT p.Email, 0, 1, 0, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactCC = 1 AND c.NCCReferenceID IN ('NCC10000040',
		'NCC10000041',
		'NCC10000206',
		'NCC11000003',
		'NCC11000021',
		'NCC00044250',
		'NCC00044298')
		UNION
		--Add in defense counsel tender contact BCCs
		SELECT DISTINCT p.Email, 0, 0, 1, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactBCC = 1 AND c.NCCReferenceID IN ('NCC10000040',
		'NCC10000041',
		'NCC10000206',
		'NCC11000003',
		'NCC11000021',
		'NCC00044250',
		'NCC00044298')

	END
	ELSE IF (@TenderHistoryID IN (172778)) --Mass Re-tender Letter 05-16-2012
	BEGIN
		INSERT INTO @Recpients
		SELECT tc.Email, tc.[To], tc.CC, tc.BCC, cpi.LastName, d.DocumentURL, LName + ', ' + FName AS ContactName, COALESCE(Hash, '')
		FROM tblTenderHistory th
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
		INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
		LEFT JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
		LEFT JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID  AND tc.TenderContactID = ta.TenderContactID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		LEFT JOIN tblClaim c ON th.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE tc.CurrentContact = 1 
			AND (th.TenderHistoryID = @TenderHistoryID) --People for that specific tender block, or always CC
		UNION
		--Union on NCC contact depending on material type
		SELECT email, 0, 1, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE p.PersonID = 3398	
		UNION
		--Add in people who should always be BCC'd (not related to carriers)
		SELECT Email, 0, 0, AlwaysBCC, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysBCC = 1
		UNION
		--Add in people who should always be CC'd (not related to carriers)
		SELECT Email, 0, AlwaysCC, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact CCs
		SELECT DISTINCT p.Email, 0, 1, 0, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactCC = 1 AND c.NCCReferenceID IN ('NCC11000092',
		'NCC11000130',
		'NCC11000165',
		'NCC11000273',
		'NCC11000342'
		)
		UNION
		--Add in defense counsel tender contact BCCs
		SELECT DISTINCT p.Email, 0, 0, 1, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE p.TendercontactBCC = 1 AND c.NCCReferenceID IN ('NCC11000092',
		'NCC11000130',
		'NCC11000165',
		'NCC11000273',
		'NCC11000342'
		)

	END
	ELSE
	BEGIN
		INSERT INTO @Recpients
		SELECT tc.Email, tc.[To], tc.CC, tc.BCC, cpi.LastName, d.DocumentURL, LName + ', ' + FName AS ContactName, COALESCE(Hash, '')
		FROM tblTenderHistory th
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
		INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
		LEFT JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
		LEFT JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID  AND tc.TenderContactID = ta.TenderContactID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		LEFT JOIN tblClaim c ON th.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE tc.CurrentContact = 1 
			AND (th.TenderHistoryID = @TenderHistoryID) --People for that specific tender block, or always CC
		UNION
		--Union on NCC contact depending on material type
		SELECT email, 0, 1, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE th.TenderHistoryID = @TenderHistoryID
		UNION
		--Add in people who should always be BCC'd (not related to carriers)
		SELECT Email, 0, 0, AlwaysBCC, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysBCC = 1
		UNION
		--Add in people who should always be CC'd (not related to carriers)
		SELECT Email, 0, AlwaysCC, 0, @CPILastName, @DocumentURL, LName + ', ' + FName AS ContactName, ''
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact CCs
		SELECT p.Email, 0, 1, 0, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON CASE WHEN l.DefenseCounselAddressID = 127 THEN 189 ELSE l.DefenseCounselAddressID END = p.FirmAddressID
		WHERE p.TendercontactCC = 1 AND c.ClaimID = (SELECT ClaimID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
		UNION
		--Add in defense counsel tender contact BCCs
		SELECT p.Email, 0, 0, 1, @CPILastName, @DocumentURL, p.LName + ', ' + p.FName AS ContactName, ''
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON CASE WHEN l.DefenseCounselAddressID = 127 THEN 189 ELSE l.DefenseCounselAddressID END = p.FirmAddressID
		WHERE p.TendercontactBCC = 1 AND c.ClaimID = (SELECT ClaimID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
	END
	
	SELECT DISTINCT * FROM @Recpients
	ORDER BY [TO]DESC, CC DESC, BCC DESC, ContactName

	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderRecipients_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderRecipients_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryTenderRecipients_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderRecipients_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderRecipients_REVERT] TO [power_user]
GO
