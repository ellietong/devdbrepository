SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmAllocationBillID]
As
select 0 as AllocationID, '' as MinExpectedPayDate
Union 
Select a.AllocationID as AllocationID,
 convert(nvarchar, case month(min(ExpectedPayDate))
		when 1 then 'January'
		when 2 then 'February'
		when 3 then 'March'
		when 4 then 'April'
		when 5 then 'May'
		when 6 then 'June'
		when 7 then 'July'
		when 8 then 'August'
		when 9 then 'September'
		when 10 then 'October'
		when 11 then 'November'
		when 12 then 'December'
		else 'Not a valid month'end) + ' ' + convert(nvarchar,Year(min(ExpectedPayDate))) as MinExpectedPayDate
from tblAllocation a 
	inner join tblAllocationResults ar on a.allocationID = ar.allocationID
	inner join tblOccurrence o on o.occurrenceid = ar.occurrenceid
Group By a.AllocationID
Order By a.AllocationID
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationBillID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationBillID] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmAllocationBillID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationBillID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationBillID] TO [power_user]
GO
