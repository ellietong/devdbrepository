SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimLawsuitInfo] (@ClaimID int)
as

Select *	
From tblClaimLawsuit
Where ClaimID = @ClaimID
Order By IsPrimaryForClaim

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitInfo] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmClaimLawsuitInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitInfo] TO [power_user]
GO
