SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_qryUpdSettleDocs] (@SettleDocID int, 
										 @DocumentID int, 
										 @DocCategoryID int, 
										 @PageNums nvarchar(100), 
										 @DocumentURL nvarchar(500) = NULL,
										 @AmazonKey nvarchar(max) = NULL,
										 @IsHistorical bit = NULL,
										 @Emailaddress nvarchar(250))
as

--DECLARE @SettlePaymentID int
--SET @SettlePaymentID = 23
--DECLARE @EmailAddress nvarchar(100)
--set @EmailAddress = 'owenss@kcicllc.com' 

UPDATE tblSettleDocs
SET DocumentID = @DocumentID, 
	DocCategoryID = @DocCategoryID, 
	PageNums=@PageNums,
	SPUsername = @Emailaddress
WHERE SettleDocID = @SettleDocID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdSettleDocs] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdSettleDocs] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdSettleDocs] TO [power_user]
GO
