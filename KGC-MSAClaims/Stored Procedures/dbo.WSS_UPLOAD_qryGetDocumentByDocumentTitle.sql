SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryGetDocumentByDocumentTitle]
	@DocTitle nvarchar(4000)
AS
BEGIN
	SELECT COUNT(*) FROM tblDocuments WHERE DocumentTitle = @DocTitle
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetDocumentByDocumentTitle] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetDocumentByDocumentTitle] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_UPLOAD_qryGetDocumentByDocumentTitle] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetDocumentByDocumentTitle] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetDocumentByDocumentTitle] TO [power_user]
GO
