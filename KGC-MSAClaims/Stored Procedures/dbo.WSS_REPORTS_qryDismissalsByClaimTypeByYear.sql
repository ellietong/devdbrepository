SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryDismissalsByClaimTypeByYear] 
	-- Add the parameters for the stored procedure here
	(@YearID int, @MonthID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @YearID int
--Set @YearId=2010


DECLARE @year int
SET @year = @YearID

DECLARE @month int
set @month = @monthid

    -- Insert statements for procedure here
select	Material as ClaimType, 
		COUNT(distinct(cl.ClaimLawsuitID)) as Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
inner join tblAllegation a on a.ClaimLawsuitID=cl.ClaimLawsuitID
inner join tblMaterial m on m.MaterialID=a.MaterialID
where	YEAR(ProcessedDate)=@year 
		and MONTH(ProcessedDate) <=@month
		and ClaimLawsuitStatusGroupID =4
		and  IsPrimaryStatus=1
group by Material
order by Material

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByClaimTypeByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByClaimTypeByYear] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_REPORTS_qryDismissalsByClaimTypeByYear] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByClaimTypeByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByClaimTypeByYear] TO [power_user]
GO
