SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryTortType]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
SELECT DISTINCT TortTypeID, UPPER(TortType)AS TortType
FROM tblTortType

UNION
		Select NULL,''
		from tblTortType
	
	ORDER BY UPPER(TortType)

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTortType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTortType] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryTortType] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTortType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTortType] TO [power_user]
GO
