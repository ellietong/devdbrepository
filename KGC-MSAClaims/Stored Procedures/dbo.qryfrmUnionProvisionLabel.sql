SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmUnionProvisionLabel]
AS

SET NOCOUNT ON

SELECT ProvisionLabelID, Provision
FROM tblProvisionLabel

UNION 

SELECT 0 AS ProvisionLabelID, '<<All Provisions>>' AS Provision
FROM tblProvisionLabel
ORDER BY Provision

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionProvisionLabel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionProvisionLabel] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmUnionProvisionLabel] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionProvisionLabel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionProvisionLabel] TO [power_user]
GO
