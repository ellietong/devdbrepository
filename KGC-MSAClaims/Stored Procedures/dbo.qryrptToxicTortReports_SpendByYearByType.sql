SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptToxicTortReports_SpendByYearByType](@DataDate DATETIME) 
AS
SET NOCOUNT ON

--DECLARE @DataDate DATETIME
--Set @DataDate = '6/30/2014'

SELECT COALESCE(a.yr, b.yr) AS YR, a.AsbestosSettlement, b.AsbestosDefense, a.AsbestosSettlement+b.AsbestosDefense AS AsbestosTotal, 
	a.CoalSettlement, b.CoalDefense, a.CoalSettlement+b.CoalDefense AS CoalTotal, 
	a.SilicaSettlement, b.SilicaDefense, a.SilicaSettlement+b.SilicaDefense AS SilicaTotal,
	a.OtherSettlement, b.OtherDefense, a.OtherSettlement+b.OtherDefense AS OtherTotal,  
	a.GeneralSettlement, b.GeneralDefense, a.GeneralSettlement+b.GeneralDefense AS GeneralTotal, 
	A.AsbestosSettlement + A.CoalSettlement + A.SilicaSettlement + A.OtherSettlement + A.GeneralSettlement AS SettlementTotal,
	B.AsbestosDefense + B.CoalDefense + B.SilicaDefense + B.OtherDefense + B.GeneralDefense AS DefenseTotal,
	A.AsbestosSettlement + A.CoalSettlement + A.SilicaSettlement + A.OtherSettlement + A.GeneralSettlement +
		B.AsbestosDefense + B.CoalDefense + B.SilicaDefense + B.OtherDefense + B.GeneralDefense AS Total,
	@DataDate AS DD

FROM

(SELECT Year(PostingDate) AS yr,
		COALESCE( SUM( CASE WHEN ClaimType LIKE '%Asbestos%' THEN SettlementAmount END),0) AS AsbestosSettlement,
		COALESCE( SUM( CASE WHEN ClaimType LIKE '%Coal%' THEN SettlementAmount END),0) AS CoalSettlement,
		COALESCE( SUM( CASE WHEN ClaimType LIKE '%Silica%' THEN SettlementAmount END),0) AS SilicaSettlement,
		COALESCE( SUM( CASE WHEN ClaimType LIKE '%Other%' THEN SettlementAmount END),0) AS OtherSettlement,
		0 AS GeneralSettlement
FROM
		dbo.tblClaimantSettlement CS
			LEFT JOIN dbo.tblClaimLawsuit CL ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
			LEFT JOIN dbo.tblClaim C ON C.ClaimID = CL.ClaimID
			LEFT JOIN dbo.tblClaimantPersonalInfo CPI ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
			LEFT JOIN dbo.tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID
			LEFT JOIN dbo.tblMaterial M ON M.MaterialID = A.MaterialID
			LEFT JOIN dbo.m_tblClaimType_StandardMSA CTS ON CTS.MaterialID = M.MaterialID

WHERE PostingDate >= '1/1/2009' AND PostingDate <= @DataDate AND M.MaterialID != 10

GROUP BY Year(PostingDate)
) AS a

FULL OUTER JOIN
		
(SELECT 	Year(PeriodEnd) AS yr,	
		
		COALESCE( SUM( CASE WHEN ClaimType LIKE '%Asbestos%' THEN Costs + Fees END),0) AS AsbestosDefense,
		COALESCE( SUM( CASE WHEN ClaimType LIKE '%Coal%' THEN Costs + Fees END),0) AS CoalDefense,
		COALESCE( SUM( CASE WHEN ClaimType LIKE '%Silica%' THEN Costs + Fees END),0) AS SilicaDefense,
		COALESCE(SUM(CASE WHEN coalesce(ClaimType, 'Other') LIKE '%Other%' AND coalesce(NCCReferenceID,'') != 'GENERALEXPS' THEN Costs + Fees END),0) AS OtherDefense,
		COALESCE( SUM( CASE WHEN NCCReferenceID LIKE '%GEN%' THEN Costs + Fees END),0) AS GeneralDefense
FROM

		dbo.tblDefenseInvoice DI
			LEFT JOIN dbo.tblDefInvClaimantDetail DICD ON DICD.DefenseInvoiceID = DI.DefenseInvoiceID
			LEFT JOIN dbo.tblClaim C ON C.ClaimID = DICD.ClaimID
			LEFT JOIN dbo.tblClaimantPersonalInfo CPI ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
			LEFT JOIN dbo.tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
			LEFT JOIN dbo.tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID
			LEFT JOIN dbo.tblMaterial M ON M.MaterialID = A.MaterialID
			LEFT JOIN dbo.m_tblClaimType_StandardMSA CTS ON CTS.MaterialID = M.MaterialID

WHERE PeriodEnd >= '1/1/2009' AND PeriodEnd <= @DataDate
			AND COALESCE(NCCReferenceID, 'Unknown') NOT LIKE '%LIT%'
			AND COALESCE(M.MaterialID, 100) != 10

GROUP BY Year(PeriodEnd)

) AS b

ON a.yr = b.yr
	

GROUP BY COALESCE(a.yr, b.yr), a.AsbestosSettlement, b.AsbestosDefense, a.AsbestosSettlement+b.AsbestosDefense, 
	a.CoalSettlement, b.CoalDefense, a.CoalSettlement+b.CoalDefense, 
	a.SilicaSettlement, b.SilicaDefense, a.SilicaSettlement+b.SilicaDefense,
	a.OtherSettlement, b.OtherDefense, a.OtherSettlement+b.OtherDefense,  
	a.GeneralSettlement, b.GeneralDefense, a.GeneralSettlement+b.GeneralDefense, 
	A.AsbestosSettlement + A.CoalSettlement + A.SilicaSettlement + A.OtherSettlement + A.GeneralSettlement,
	B.AsbestosDefense + B.CoalDefense + B.SilicaDefense + B.OtherDefense + B.GeneralDefense,
	A.AsbestosSettlement + A.CoalSettlement + A.SilicaSettlement + A.OtherSettlement + A.GeneralSettlement +
		B.AsbestosDefense + B.CoalDefense + B.SilicaDefense + B.OtherDefense + B.GeneralDefense
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptToxicTortReports_SpendByYearByType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptToxicTortReports_SpendByYearByType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptToxicTortReports_SpendByYearByType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptToxicTortReports_SpendByYearByType] TO [power_user]
GO
