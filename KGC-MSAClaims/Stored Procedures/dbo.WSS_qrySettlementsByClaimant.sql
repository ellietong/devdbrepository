SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qrySettlementsByClaimant]
(@ClaimantPersonalInfoID bigint=null, @Emailaddress nvarchar(250))
AS
BEGIN
--DECLARE @ClaimantpersonalinfoID int
--SET @ClaimantpersonalinfoID = 22240

					
DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)
IF(@InsurerRole = 1)
BEGIN
	SELECT     C.ClaimID, ClaimantSettlementID, Material AS ClaimType, SettlementAmount, SettlementDate
	FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
						  dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
						  dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
						  dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID LEFT JOIN
						  tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
						  INNER JOIN tblMaterial m ON a.MaterialID = m.MaterialID
						  LEFT JOIN (Select * From dbo.tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
						  dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID INNER JOIN
						  dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
						  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
						  dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
						  left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
	WHERE CPI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND cl.IsPrimaryForClaim = 1
	AND c.ClaimID NOT IN (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
	AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
	--AND SettlementDate IS NOT NULL and SettlementAmount IS NOT NULL

END
ELSE
BEGIN

	SELECT     C.ClaimID, ClaimantSettlementID, Material AS ClaimType, SettlementAmount, SettlementDate
	FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
						  dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
						  dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
						  dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID LEFT JOIN
						  tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
						  INNER JOIN tblMaterial m ON a.MaterialID = m.MaterialID
						  LEFT JOIN (Select * From dbo.tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
						  dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID INNER JOIN
						  dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
						  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
						  dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
						  left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
	WHERE CPI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND cl.IsPrimaryForClaim = 1
	--AND SettlementDate IS NOT NULL and SettlementAmount IS NOT NULL
END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySettlementsByClaimant] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySettlementsByClaimant] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qrySettlementsByClaimant] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySettlementsByClaimant] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySettlementsByClaimant] TO [power_user]
GO
