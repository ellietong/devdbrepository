SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmOccurrence]
As

SET NOCOUNT ON

SELECT *
FROM tblOccurrence 
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrence] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmOccurrence] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrence] TO [power_user]
GO
