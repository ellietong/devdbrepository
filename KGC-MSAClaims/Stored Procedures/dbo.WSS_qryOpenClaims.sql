SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_qryOpenClaims]
AS
SELECT     C.ClaimID, DHLD.HighLevelDisease AS Disease, S.State, J.Jurisdiction, cs.ClaimSequence, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS SSN, 
                      CONVERT(VARCHAR(10), CPI.BirthDate, 101) AS BirthDate, CPI.DeceasedDate, CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, 
                      DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) 
                      = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, 
                      BirthDate, DeceasedDate) END END AS DeathAge, cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) 
                      AS FileDate, L.DocketNumber, L.CaseCaption, L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName, 
                      pca.PlaintiffCounsel, r_1.DefenseCounsel AS PrimaryDefenseCounsel
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID 
                      left join tblplaintiffcounselAddress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid INNER JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                          (SELECT     dcs.DefenseCounselAddressID, d.DefenseCounsel, dcs.ResponsibleDefenseCounselAddressID, dcs.StateID, 
                                                   r.DefenseCounsel AS ResponsibleDefenseCounsel, dcs.IsActiveCounsel
                            FROM          dbo.tblDefenseCounselState AS dcs INNER JOIN
												   dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid inner join
												   dbo.tblDefenseCounselAddress dcar on dcar.defensecounseladdressid = dcs.responsibledefensecounseladdressid inner join
                                                   dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID INNER JOIN
                                                   dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
                            GROUP BY d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounseladdressID, dcs.ResponsibleDefenseCounseladdressID, dcs.IsActiveCounsel, dcs.StateID) 
                      AS r_1 ON r_1.DefenseCounseladdressID = L.DefenseCounseladdressID AND r_1.StateID = L.StateID LEFT OUTER JOIN
                      (select * from dbo.tblClaimDisease where PrimaryDisease = 1) as cd on cd.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblDisease AS DDisease ON Cd.DiseaseID = DDisease.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS DHLD ON DHLD.HighLevelDiseaseID = DDisease.HighLevelDiseaseID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatus AS cls ON CL.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
WHERE     (CL.IsPrimaryForClaim = 1) AND cl.ClaimLawsuitStatusID = 22
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOpenClaims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOpenClaims] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryOpenClaims] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOpenClaims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOpenClaims] TO [power_user]
GO
