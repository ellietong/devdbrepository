SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAllocationAllRelatedTables]
(@AllocationID int)
As
SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 25

DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation


SELECT AllocationID, AllocationDescription, SaveResults, TriggerStartID, TriggerStartDate, TriggerEndID, TriggerEndDate, 
UnCoveredYearstoPH, VerticallyUnCollapse, Cost1, Cost2, Cost3, Cost4, Cost5, Cost6, Cost7, 
Cost8, Cost9, Cost10, Cost11, Cost12, ConsiderPreviousExhaustion, ConsiderExhaustionCarterWallace, LessThanAttachmentPoint, LessThanLayer, 
OccurrencesInClaim, Cost1Discount, Cost2Discount, Cost3Discount, Cost4Discount, Cost5Discount, 
Cost6Discount, Cost7Discount, Cost8Discount, Cost9Discount, Cost10Discount, Cost11Discount, 
Cost12Discount, AllocationNotes, ConsiderPreviousAllocation, PreviousAllocation, 
AllocationSchemeID, a.AllocationScheme, a.SIRID, SIRTreatment, a.SIRtoPay, ProratedSIRAgg, a.OccurrenceDefId, od.OccurrenceDef, 
tl.TriggerDescription AS TriggerStart, tblTriggerLabel_1.TriggerDescription AS TriggerEnd, 
LimitTypeID, a.LimitType, ExhaustionTypeID, ExhaustionType, ExhaustionDateID, ExhaustionDate, UnevenQSID, UnevenQSType,
DefenseScenarioID, Scenario
FROM vAllocation a 
	LEFT JOIN tblOccurrenceDef od ON a.OccurrenceDefId = od.OccurrenceDefID
	LEFT JOIN tblTriggerLabel tl ON a.TriggerStartID = tl.TriggerID
	LEFT JOIN tblTriggerLabel AS tblTriggerLabel_1 ON a.TriggerEndID = tblTriggerLabel_1.TriggerID
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) 
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationAllRelatedTables] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationAllRelatedTables] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptAllocationAllRelatedTables] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationAllRelatedTables] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationAllRelatedTables] TO [power_user]
GO
