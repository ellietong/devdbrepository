SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantLawsuitStatusList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimLawsuitStatusID, ClaimLawsuitStatus 
	FROM tblClaimLawsuitStatus cls
	WHERE cls.ClaimLawsuitStatusGroupID IN (1,2,3,4)
	UNION
	SELECT NULL, ''
	ORDER BY ClaimLawsuitStatus
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantLawsuitStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantLawsuitStatusList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimantLawsuitStatusList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantLawsuitStatusList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantLawsuitStatusList] TO [power_user]
GO
