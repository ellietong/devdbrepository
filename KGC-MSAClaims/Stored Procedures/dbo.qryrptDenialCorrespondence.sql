SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qryrptDenialCorrespondence]
(@CorrespondenceDate datetime = NULL, @Site nvarchar(100) = null, @carrierid int = NULL)
as
set nocount on

--DECLARE @CorrespondenceDate datetime
--DECLARE @Site int
--Declare @Carrierid int
--Set @Carrierid = 0
--SET @CorrespondenceDate='1/1/1900'
--SET @Site = 0

DECLARE @MaxSiteID Int
SELECT @MaxSiteID = MAX(siteid) from tblEnviroSitesDetail

DECLARE @MAXCarrierID Int
SELECT @MAXCarrierID = MAX(carrierID) from tblCarrier


select cor.correspondenceid, correspondencedate, ct.correspondencetypeid, ct.correspondencetype,
	cor.correspondencemodeid, cm.correspondencemode, notes, attachments, confirmreceipt, con.Company, CarrierName
from tblcorrespondence cor left join tblcorrespondencemode cm on cor.correspondencemodeid = cm.correspondencemodeid
	left join tblcorrespondencetype ct on cor.correspondencetypeid = ct.correspondencetypeid
	left join tblCorrCarrierContact ca on ca.CorrespondenceID=cor.CorrespondenceID
	left join tblCarrierContact a on a.CarrierContactID = ca.CarrierContactID
	left join tblCarrier c on c.CarrierID = a.CarrierID
	left join tblContact con on con.ContactID=a.ContactID
where cor.correspondencetypeid = 3
and RecipientTypeID in (5, null)

AND cor.CorrespondenceDate >= @correspondencedate and cor.correspondencedate <= COALESCE(NULLIF(@CorrespondenceDate, '1/1/1900'), getdate())

and cor.CorrespondenceID in 
(select CorrespondenceID from tblSiteNoticed 
where SiteID >= coalesce(@site, 0) and SiteID <= coalesce(nullif(@site, 0), @maxsiteid))
AND a.CarrierID >= coalesce(@carrierid, 0) and a.CarrierID  <= coalesce(nullif(@carrierid, 0), @Maxcarrierid)

order by cor.correspondencedate


return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDenialCorrespondence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDenialCorrespondence] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptDenialCorrespondence] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDenialCorrespondence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDenialCorrespondence] TO [power_user]
GO
