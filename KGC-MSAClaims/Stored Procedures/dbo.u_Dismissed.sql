SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[u_Dismissed]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
select nccreferenceid,lastname, firstname,ClaimLawsuitStatusGroup,CaseCaption, cdt.ClaimantDocumentType, scdt.SingleClaimantDocumentTypeID,d.DocumentID, DocumentTitle, DocumentURL 
from tblClaim c
inner join tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
inner join tblClaimLawsuit cl on cl.ClaimID =c.ClaimID 
inner join tblLawsuit l on l.LawsuitID =cl.LawsuitID 
inner join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID =l.DefenseCounselAddressID 
inner join tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
inner join tblDocuments d on d.DocumentID =mcd.DocumentID 
inner join tblSingleClaimantDocumentType scdt on scdt.ClaimantDocumentTypeID =d.DetailDocumentTypeID 
left join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = scdt.SingleClaimantDocumentTypeID
left join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID =cl.ClaimLawsuitID 
left join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID =clst.ClaimLawsuitStatusID 
left join tblClaimLawsuitStatusGroup clsg on clsg.ClaimLawsuitStatusGroupID =cls.ClaimLawsuitStatusGroupID 
where scdt.SingleClaimantDocumentTypeID in (16,36,4,8,33,58) and NCCReferenceID  in ('NCC11000145',
'NCC00043583',
'NCC00017762',
'NCC00044110',
'NCC00002544',
'NCC00043775',
'NCC09000477',
'NCC10000434',
'NCC10000538',
'NCC10000422',
'NCC11000048',
'NCC10000432',
'NCC11000033',
'NCC09000455',
'NCC10000504',
'NCC00007586',
'NCC11000047',
'NCC11000144',
'NCC00031885',
'NCC12000022',
'NCC09000046',
'NCC10000241',
'NCC11000110',
'NCC00045311',
'NCC10000426',
'NCC00043428',
'NCC00044356',
'NCC11000028',
'NCC10000517',
'NCC00044369',
'NCC00043788',
'NCC11000049',
'NCC10000523',
'NCC09000468',
'NCC11000044',
'NCC10000423',
'NCC00044442',
'NCC00044462',
'NCC11000055',
'NCC10000283',
'NCC11000043',
'NCC00008714',
'NCC10000236',
'NCC11000136',
'NCC00044678',
'NCC10000546',
'NCC11000093',
'NCC00004415',
'NCC09000417',
'NCC10000499',
'NCC00002500',
'NCC00002549',
'NCC11000046',
'NCC10000027',
'NCC11000171',
'NCC09000720',
'NCC00007621',
'NCC00007612',
'NCC00044895',
'NCC00045550',
'NCC10000497',
'NCC11000016',
'NCC09000718',
'NCC10000218',
'NCC00045019',
'NCC10000240',
'NCC11000167',
'NCC10000281',
'NCC11000090',
'NCC00008740',
'NCC10000488',
'NCC00023568',
'NCC10000433',
'NCC11000038','NCC10000282',
'NCC11000348',
'NCC12000164',
'NCC11000370',
'NCC10000057',
'NCC12000217',
'NCC10000544',
'NCC12000262',
'NCC11000248',
'NCC11000035',
'NCC12000252',
'NCC12000253',
'NCC11000040',
'NCC11000246',
'NCC11000159' ) and IsPrimaryForClaim=1 and IsPrimaryStatus =1 and IsCurrentClaim =1
order by LastName, FirstName 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[u_Dismissed] TO [base_user]
GRANT EXECUTE ON  [dbo].[u_Dismissed] TO [base_user]
GRANT ALTER ON  [dbo].[u_Dismissed] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[u_Dismissed] TO [power_user]
GRANT EXECUTE ON  [dbo].[u_Dismissed] TO [power_user]
GO
