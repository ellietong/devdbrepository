SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByMonth] 
	-- Add the parameters for the stored procedure here
	(@StartDate datetime, @EndDate datetime, @EmailAddress nvarchar(250), @TortTypeID int)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @MaxTortTypeID int
SET @MaxTortTypeID = (SELECT MAX(TortTypeID) FROM tblTortType)

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)

--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1)
BEGIN

	select DATENAME("mm", coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) + ' ' + CONVERT(nvarchar, YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))) AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS MonthCount, MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) As MonthID, 
	YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) AS [Year]
	from tblClaim c 
	inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
	left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	left join tblState s on l.StateID=s.StateID
	where	coalesce(ServiceDate,FileDate,ReceivedDate,processeddate, Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
	and c.TortTypeID >= coalesce(@TortTypeID, 0) and c.TortTypeID <= coalesce(@torttypeid, @maxtorttypeid) 
	group by DATENAME("mm", coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) + ' ' +  CONVERT(nvarchar, YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))) , MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)),YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))
	order by YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)), MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))

END
ELSE IF(@InsurerRole = 1) --Insurers
BEGIN

select DATENAME("mm", coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) + ' ' + CONVERT(nvarchar, YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))) AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS MonthCount, MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) As MonthID, 
	YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) AS [Year]
	from tblClaim c 
	inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
	left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	left join tblState s on l.StateID=s.StateID
	where	coalesce(ServiceDate,FileDate,ReceivedDate,processeddate, Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate 
			AND c.ClaimID NOT IN (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
			and c.TortTypeID >= coalesce(@TortTypeID, 0) and c.TortTypeID <= coalesce(@torttypeid, @maxtorttypeid)
			AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
	group by DATENAME("mm", coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) + ' ' +  CONVERT(nvarchar, YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))) , MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)),YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))
	order by YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)), MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))

END
ELSE
BEGIN

select DATENAME("mm", coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) + ' ' + CONVERT(nvarchar, YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))) AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS MonthCount, MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) As MonthID, 
	YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) AS [Year]
	from tblClaim c 
	inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
	left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	left join tblState s on l.StateID=s.StateID
	INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
	where	coalesce(ServiceDate,FileDate,ReceivedDate,processeddate, Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate 
			AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
			and c.TortTypeID >= coalesce(@TortTypeID, 0) and c.TortTypeID <= coalesce(@torttypeid, @maxtorttypeid)
	group by DATENAME("mm", coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) + ' ' +  CONVERT(nvarchar, YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))) , MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)),YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))
	order by YEAR(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)), MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))

END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [power_user]
GO
