SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmClaimantDocumentsEntry] (@ClaimantPersonalInfoID int)
as

Select ClaimantDocumentID, 
	ClaimantPersonalInfoID,
	DocumentNumber,
	ClaimantDocumentTypeID,
	DocumentLink
From tblClaimantDocuments
Where ClaimantPersonalInfoID = @CLaimantPersonalInfoID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [power_user]
GO
