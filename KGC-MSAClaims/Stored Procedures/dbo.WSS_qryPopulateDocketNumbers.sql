SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




Create PROCEDURE [dbo].[WSS_qryPopulateDocketNumbers] (@ClaimantName nvarchar(max), @Matter nvarchar(10), @Birthdate nvarchar(20), @SSN nvarchar(20))
AS

--DECLARE @ClaimantName nvarchar(max)
--SET @ClaimantName = 'dee'
--DECLARE @Birthdate nvarchar(20)
--SET @Birthdate = null
--DECLARE @SSN nvarchar(20)
--SET @SSN = null


DECLARE @tblDocket TABLE (docketNums nvarchar(4000), ClaimantPersonalInfoID bigint)
	DECLARE @CPI bigint
	DECLARE @DocketNumStr nvarchar(4000)
	DECLARE @addDocket nvarchar(4000)
BEGIN

	
	--Declare Cursor
	DECLARE Cur_CPI CURSOR Local Fast_Forward for
	SELECT DISTINCT 
                      CPI.ClaimantPersonalInfoID
	FROM         dbo.tblClaimantPersonalInfo AS CPI

	--Open CPI Cursor
	OPEN Cur_CPI

	--Fetch Data
	FETCH NEXT FROM Cur_CPI INTO @CPI

	WHILE @@Fetch_Status = 0
	BEGIN

				SET @DocketNumStr = ''
				DECLARE Cur_Docket CURSOR Local Fast_Forward for
				SELECT L.DocketNumber FROM tblLawsuit l 
				INNER JOIN tblClaimLawsuit cl ON cl.LawsuitID = l.LawsuitID
				INNER JOIN tblClaim c ON C.ClaimID = cl.ClaimID WHERE ClaimantPersonalInfoID = @CPI ORDER BY L.DocketNumber ASC

				--Open Docket Cursor
				OPEN Cur_Docket

				--Fetch Data
				FETCH NEXT FROM Cur_Docket INTO @addDocket

				WHILE @@Fetch_Status = 0
				BEGIN
					--assemble the string
					if LEN(@DocketNumStr) = 0 SET @DocketNumStr = @addDocket
					else SET @DocketNumStr =  @DocketNumStr  + ', ' +  @addDocket
					
				
		
					FETCH NEXT FROM Cur_Docket INTO @addDocket
					
				END
				
				CLOSE Cur_Docket 
				DEALLOCATE Cur_Docket
				--insert the string into a temp table with CPI
				INSERT INTO tblClaimantDocketNumbers (DocketNumbers, ClaimantPersonalInfoID) VALUES (@DocketNumStr, @CPI)
				
	FETCH NEXT FROM Cur_CPI INTO @CPI
	
END

CLOSE Cur_CPI 
DEALLOCATE Cur_CPI 
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryPopulateDocketNumbers] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryPopulateDocketNumbers] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryPopulateDocketNumbers] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryPopulateDocketNumbers] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryPopulateDocketNumbers] TO [power_user]
GO
