SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryNationalPlaintiffCounselList]
	-- Add the parameters for the stored procedure here
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT tca.PlaintiffCounselID, PlaintiffCounsel
	From tblPlaintiffCounselAddress tca
	INNER JOIN tblLawsuit l on tca.PlaintiffCounselAddressID=l.NationalPlaintiffCounselAddressID
	WHERE NationalPlaintiffCounselAddressID=l.PlaintiffCounselAddressID AND  PlaintiffCounsel IS NOT NULL AND PlaintiffCounsel <> ''
	UNION
	SELECT NULL, NULL
	ORDER BY PlaintiffCounsel
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryNationalPlaintiffCounselList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryNationalPlaintiffCounselList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryNationalPlaintiffCounselList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryNationalPlaintiffCounselList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryNationalPlaintiffCounselList] TO [power_user]
GO
