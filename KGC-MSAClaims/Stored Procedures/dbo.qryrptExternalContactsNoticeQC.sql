SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[qryrptExternalContactsNoticeQC]
(@carrierid int = null)
as
set nocount on

--DECLARE @CarrierID int
--SET @CarrierID=0



Declare @MAXcarrierid int
SELECT @MAXcarrierid = MAX(carrierid) from tblCarrier


SELECT  carriername, c.*, ct.ContactType, FullNameState as 'State', CurrentContact, DateAdded 
FROM dbo.tblCarrierContact cc 
inner join tblCarrier car on car.CarrierID = cc.CarrierID
INNER JOIN dbo.tblContact c ON cc.ContactID = c.ContactID 
LEFT JOIN dbo.tblContactType ct ON ct.ContactTypeID = c.ContactTypeID
left join tblState s on s.StateID = c.StateID

Where car.carrierid >= COALESCE(@carrierID, 0) AND car.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID)
and InternalContact<>1
group by carriername, c.AddressLine1, c.AddressLine2, c.AddressLine3, c.BadAddress, c.CareOf, c.City, c.Company, c.ContactClaimTypeID, c.ContactID,
	c.ContactTypeID, c.Email, c.Fax, c.FirstName, c.LastName, c.MiddleName, c.Phone, c.Position, c.StateID, c.Title, c.Zip, 
	ct.ContactType, FullNameState, CurrentContact, DateAdded, c.InternalContact
order by ContactID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExternalContactsNoticeQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExternalContactsNoticeQC] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptExternalContactsNoticeQC] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExternalContactsNoticeQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExternalContactsNoticeQC] TO [power_user]
GO
