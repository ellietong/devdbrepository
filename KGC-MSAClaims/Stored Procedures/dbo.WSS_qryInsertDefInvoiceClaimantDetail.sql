SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInsertDefInvoiceClaimantDetail]
(@FirmID int, @InvoiceNumber nchar(10), @InvoiceDate date, @ClaimantPersonalInfoID int, @Fees money, @Costs money, 
@Adjustments money, @Notes nvarchar(mAX), @PACEClaimID int,  @DocumentID int, @MatterID int)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO tblDefInvClaimantDetail ([FirmID]
      ,[InvoiceNumber]
      ,[InvoiceDate]
      ,[ClaimantPersonalInfoID]
      ,[Fees]
      ,[Costs]
      ,[Adjustments]
      ,[Notes]
      ,[PACEClaimID]
      ,[DocumentID]
      ,[MatterID]) VALUES (@FirmID, @InvoiceNumber, @InvoiceDate, @ClaimantPersonalInfoID, @Fees, @Costs, @Adjustments, @Notes, @PACEClaimID, @DocumentID, @MatterID)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsertDefInvoiceClaimantDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsertDefInvoiceClaimantDetail] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryInsertDefInvoiceClaimantDetail] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsertDefInvoiceClaimantDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsertDefInvoiceClaimantDetail] TO [power_user]
GO
