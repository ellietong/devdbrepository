SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrpt_BillingReport_TotalBilling]
(@ExhastionTypeID INT = NULL, @CarrierGroupID INT = NULL,  @CurrentPeriodStart Date = NULL)

AS
SET NOCOUNT ON

--DECLARE @ExhastionTypeID INT
--DECLARE @CarrierGroupID INT
--DECLARE @CurrentPeriodStart Date


--SET @ExhastionTypeID = 1
--SET @CarrierGroupID = 1
--SET @CurrentPeriodStart = '12/31/2013'



DECLARE @Policy TABLE (PolicyID INT, PolicyDateID INT, CarrierGroupName NVARCHAR(101), CarrierName NVARCHAR(101),
PolicyNum NVARCHAR(101), StartDate DATETIME, EndDate DATETIME, AttachmentPoint MONEY, PerOccLimit MONEY, LayerPerOccLimit MONEY, IndemnityAllocationPriorPeriod Money, DefenseAllocationPriorPeriod MONEY, 
IndemnityAllocationCurrentPeriod Money, DefenseAllocationCurrentPeriod MONEY, RemainingLimits Money, CurrentPeriodStart NVARCHAR(101))

DECLARE @ExhaustionPriorPeriod TABLE (ExhaustionTypeID INT, PolicyDateID INT, IndemnityAllocationPriorPeriod Money, DefenseAllocationPriorPeriod MONEY)

INSERT INTO @ExhaustionPriorPeriod (PolicyDateID, IndemnityAllocationPriorPeriod, DefenseAllocationPriorPeriod)
SELECT  E.PolicyDateID, sum(IndemnityAllocation), sum(DefenseAllocation)
FROM dbo.tblExhaustion E
INNER JOIN tblExhaustionDate ED ON ED.ExhaustionDateID = E.ExhaustionDateID
INNER JOIN tblPolicyDates PD ON PD.PolicyDateID = E.PolicyDateID
WHERE ExhaustionTypeID = @ExhastionTypeID and ExhaustionDate < @CurrentPeriodStart
GROUP BY E.PolicyDateID

DECLARE @ExhaustionCurrentPeriod TABLE (ExhaustionTypeID INT, PolicyDateID INT, IndemnityAllocationCurrentPeriod Money, DefenseAllocationCurrentPeriod MONEY)

INSERT INTO @ExhaustionCurrentPeriod ( PolicyDateID, IndemnityAllocationCurrentPeriod, DefenseAllocationCurrentPeriod)
SELECT  E.PolicyDateID, sum(IndemnityAllocation), sum(DefenseAllocation)
FROM dbo.tblExhaustion E
INNER JOIN tblExhaustionDate ED ON ED.ExhaustionDateID = E.ExhaustionDateID
INNER JOIN tblPolicyDates PD ON PD.PolicyDateID = E.PolicyDateID
WHERE ExhaustionTypeID = @ExhastionTypeID and ExhaustionDate >= @CurrentPeriodStart
GROUP BY  E.PolicyDateID


DECLARE @Quarter TABLE (CurrentPeriodStart NVARCHAR(101), Quarter NVARCHAR(101), Year NVARCHAR(101))
Insert into @Quarter (CurrentPeriodStart, Quarter, Year)
select @CurrentPeriodStart as CurrentPeriodStart, datepart(q,@CurrentPeriodStart) as Quarter, datepart(yy,@CurrentPeriodStart) as Year

INSERT INTO @ExhaustionCurrentPeriod (PolicyDateID, IndemnityAllocationCurrentPeriod, DefenseAllocationCurrentPeriod)
SELECT  E.PolicyDateID, sum(IndemnityAllocation), sum(DefenseAllocation)
FROM dbo.tblExhaustion E
INNER JOIN tblExhaustionDate ED ON ED.ExhaustionDateID = E.ExhaustionDateID
INNER JOIN tblPolicyDates PD ON PD.PolicyDateID = E.PolicyDateID
WHERE ExhaustionTypeID = @ExhastionTypeID and ExhaustionDate >= @CurrentPeriodStart
GROUP BY E.PolicyDateID

INSERT INTO @Policy (PolicyID, PolicyDateID, CarrierGroupName, CarrierName, PolicyNum, StartDate, EndDate, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
IndemnityAllocationPriorPeriod, DefenseAllocationPriorPeriod, IndemnityAllocationCurrentPeriod, DefenseAllocationCurrentPeriod, 
RemainingLimits, CurrentPeriodStart)

SELECT P.PolicyID, PD.PolicyDateID, CASE WHEN C.CarrierGroupID = 1 THEN 'Chartis Inc.' ELSE COALESCE(CarrierGroupName, CarrierName) END AS CarrierGroupName,
CarrierName, PolicyNum, ActualPStartDate, ActualPEndDate, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
coalesce(IndemnityAllocationPriorPeriod,0), 
coalesce(DefenseAllocationPriorPeriod,0), 
coalesce(IndemnityAllocationCurrentPeriod,0), 
coalesce(DefenseAllocationCurrentPeriod,0), 
(PerOccLimit - isnull(IndemnityAllocationPriorPeriod,0) - isnull(DefenseAllocationPriorPeriod,0) - isnull(IndemnityAllocationCurrentPeriod,0) - isnull(DefenseAllocationCurrentPeriod,0)) as RemainingLimits, @CurrentPeriodStart
FROM tblPolicy P
INNER JOIN tblPolicyDates PD ON PD.PolicyID = P.PolicyID
INNER JOIN tblPolicyLimits PL ON PL.PolicyID = P.PolicyID
INNER JOIN tblCarrier C ON C.CarrierID = P.CarrierID
LEFT JOIN tblCarrierGroup CG ON CG.CarrierGroupID = C.CarrierGroupID
LEFT JOIN @ExhaustionPriorPeriod EP ON EP.PolicyDateID = PD.PolicyDateID
LEFT JOIN @ExhaustionCurrentPeriod EC ON EC.PolicyDateID = PD.PolicyDateID
WHERE LimitTypeID = 4
AND C.CarrierGroupID = @CarrierGroupID
--and ((IndemnityAllocationPriorPeriod > 0 or DefenseAllocationPriorPeriod > 0) or (IndemnityAllocationCurrentPeriod > 0 or DefenseAllocationCurrentPeriod > 0))
GROUP BY P.PolicyID, PD.PolicyDateID, CASE WHEN C.CarrierGroupID = 1 THEN 'Chartis Inc.' ELSE COALESCE(CarrierGroupName, CarrierName) END,
CarrierName, PolicyNum, ActualPStartDate, ActualPEndDate, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
IndemnityAllocationPriorPeriod, DefenseAllocationPriorPeriod, IndemnityAllocationCurrentPeriod, DefenseAllocationCurrentPeriod

DECLARE @PolicywQuarter TABLE (PolicyID INT, PolicyDateID INT, CarrierGroupName NVARCHAR(101), CarrierName NVARCHAR(101),
PolicyNum NVARCHAR(101), StartDate DATETIME, EndDate DATETIME, AttachmentPoint MONEY, PerOccLimit MONEY, LayerPerOccLimit MONEY,
IndemnityAllocationPriorPeriod Money, DefenseAllocationPriorPeriod MONEY, 
IndemnityAllocationCurrentPeriod Money, DefenseAllocationCurrentPeriod MONEY, RemainingLimits Money, Quarter NVARCHAR(101), Year NVARCHAR(101))
INSERT INTO @PolicywQuarter (PolicyID, PolicyDateID, CarrierGroupName, CarrierName,
PolicyNum, StartDate, EndDate, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
IndemnityAllocationPriorPeriod, DefenseAllocationPriorPeriod,
IndemnityAllocationCurrentPeriod, DefenseAllocationCurrentPeriod, RemainingLimits, Quarter, Year)
select PolicyID, PolicyDateID, CarrierGroupName, CarrierName, PolicyNum, StartDate, EndDate, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
IndemnityAllocationPriorPeriod, DefenseAllocationPriorPeriod, IndemnityAllocationCurrentPeriod, DefenseAllocationCurrentPeriod, RemainingLimits, Quarter, Year
from @Policy p
inner join @Quarter q on q.CurrentPeriodStart = p.CurrentPeriodStart
group by PolicyID, PolicyDateID, CarrierGroupName, CarrierName, PolicyNum, StartDate, EndDate, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
IndemnityAllocationPriorPeriod, DefenseAllocationPriorPeriod, IndemnityAllocationCurrentPeriod, DefenseAllocationCurrentPeriod, RemainingLimits, Quarter, Year


select PolicyID
,PolicyDateID
,CarrierGroupName
,CarrierName
,PolicyNum
,StartDate
,EndDate
,AttachmentPoint
,PerOccLimit
,LayerPerOccLimit
,SUM(IndemnityAllocationPriorPeriod) AS IndemnityAllocationPriorPeriod
,SUM(DefenseAllocationPriorPeriod) AS DefenseAllocationPriorPeriod
,SUM(IndemnityAllocationPriorPeriod+DefenseAllocationPriorPeriod) as TotalAllocationPriorPeriod
,SUM(IndemnityAllocationCurrentPeriod) AS IndemnityAllocationCurrentPeriod
,SUM(DefenseAllocationCurrentPeriod) AS DefenseAllocationCurrentPeriod,
SUM(IndemnityAllocationCurrentPeriod+DefenseAllocationCurrentPeriod) as TotalAllocationCurrentPeriod,
SUM(IndemnityAllocationPriorPeriod+IndemnityAllocationCurrentPeriod) as TotalIndemnity,
SUM(DefenseAllocationPriorPeriod+DefenseAllocationCurrentPeriod) as TotalDefense,
SUM(IndemnityAllocationPriorPeriod+IndemnityAllocationCurrentPeriod + DefenseAllocationPriorPeriod+DefenseAllocationCurrentPeriod) as TotalBilling
,SUM(RemainingLimits) AS RemainingLimits
,Quarter
,Year
from @PolicywQuarter
GROUP BY PolicyID
,PolicyDateID
,CarrierGroupName
,CarrierName
,PolicyNum
,StartDate
,EndDate
,AttachmentPoint
,PerOccLimit
,LayerPerOccLimit
,Quarter
,Year
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrpt_BillingReport_TotalBilling] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrpt_BillingReport_TotalBilling] TO [base_user]
GRANT ALTER ON  [dbo].[qryrpt_BillingReport_TotalBilling] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrpt_BillingReport_TotalBilling] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrpt_BillingReport_TotalBilling] TO [power_user]
GO
