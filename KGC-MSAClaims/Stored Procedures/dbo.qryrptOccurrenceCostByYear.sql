SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  Procedure [dbo].[qryrptOccurrenceCostByYear] 
(@CorporateProfileID INT = NULL,@InClaim int, @selectedOccTypes nvarchar(1000) = NULL, @selectedOccGroups nvarchar(1000) = NULL) 
As
SET NOCOUNT ON

--DECLARE @CorporateProfileID int
--SET @CorporateProfileID = 0
--DECLARE @InClaim bit
--SET @InClaim = 1

DECLARE @maxCorporateProfileID int
SELECT @maxCorporateProfileID = MAX(CorporateHistoryID) FROM vOccurrence
--Declare backup strings of the passed in lists to be used in the ultimate where clauses below
DECLARE @selectedProvisionsList nvarchar(1000)
SET @selectedProvisionsList = @selectedOccTypes
DECLARE @selectedProvisionValuesList nvarchar(1000)
SET @selectedProvisionValuesList = @selectedOccGroups

--Declare the tables to be used for the separated provision and provision value IDs
DECLARE @selectedOccTypesTable TABLE(OccTypeID int)
DECLARE @selectedOccGroupsTable TABLE(OccGroupID int)

--Prepare flags if there is only a zero in the table (and thus there should be no filter)
DECLARE @flagOccTypeFilter int
DECLARE @flagOccGroupFilter int
SET @flagOccTypeFilter = 0
SET @flagOccGroupFilter = 0
--add params for occType and occGroup
--add parsing of params into temp tables
-- add where for vars in temp tables
--Parse out occtypes	
WHILE LEN(@selectedOccTypes)>0 BEGIN
	IF CHARINDEX(',',@selectedOccTypes) = 0 BEGIN
		INSERT INTO @selectedOccTypesTable
			SELECT CONVERT(int, @selectedOcctypes)
		IF CONVERT(int,@selectedOccTypes) <> 0
			SET @flagOccTypeFilter = 1
		SET @selectedOcctypes = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedOccTypesTable
			SELECT CONVERT(int, LEFT(@selectedOccTypes, CHARINDEX(',',@selectedOccTypes)-1))
		SET @selectedOccTypes = RIGHT(@selectedOccTypes,LEN(@selectedOccTypes)-CHARINDEX(',',@selectedOccTypes))
		SET @flagOccTypeFilter = 1
	END
END
----Parse out occgroups	
WHILE LEN(@selectedOccGroups)>0 BEGIN
	IF CHARINDEX(',',@selectedOccGroups) = 0 BEGIN
		INSERT INTO @selectedOccGroupsTable
			SELECT CONVERT(int, @selectedOccGroups)
		IF CONVERT(int,@selectedOccGroups) <> 0
			SET @flagOccGroupFilter = 1
		SET @selectedOccGroups = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedoccgroupsTable
			SELECT CONVERT(int, LEFT(@selectedoccgroups, CHARINDEX(',',@selectedOccGroups)-1))
		SET @selectedOccGroups = RIGHT(@selectedOccGroups,LEN(@selectedOccGroups)-CHARINDEX(',',@selectedOccGroups))
		SET @flagOccGroupFilter = 1
	END
END


IF @InClaim = -1
	
	--if type and group = 1
	IF @flagOccGroupFilter = 1 and @flagOccTypeFilter = 1
	BEGIN
		SELECT InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1) AS Start1Year, SUM(Cost1) AS SumOfCost1, SUM(Cost2) AS SumOfCost2, SUM(Cost3) AS SumOfCost3, 
			SUM(Cost4) AS SumOfCost4, SUM(Cost5) AS SumOfCost5, SUM(Cost6) AS SumOfCost6, SUM(Cost7) AS SumOfCost7, SUM(Cost8) AS SumOfCost8, SUM(Cost9) AS SumOfCost9, 
			SUM(Cost10) AS SumOfCost10, SUM(Cost11) AS SumOfCost11, SUM(Cost12) AS SumOfCost12
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
		AND OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
		AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
		GROUP BY InClaim, CorporateHistoryID, CorporateHistoryprofile, Year(TriggerStart1)
	END
	--if type = 1
	ELSE IF @flagOccTypeFilter = 1
	BEGIN
		SELECT InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1) AS Start1Year, SUM(Cost1) AS SumOfCost1, SUM(Cost2) AS SumOfCost2, SUM(Cost3) AS SumOfCost3, 
			SUM(Cost4) AS SumOfCost4, SUM(Cost5) AS SumOfCost5, SUM(Cost6) AS SumOfCost6, SUM(Cost7) AS SumOfCost7, SUM(Cost8) AS SumOfCost8, SUM(Cost9) AS SumOfCost9, 
			SUM(Cost10) AS SumOfCost10, SUM(Cost11) AS SumOfCost11, SUM(Cost12) AS SumOfCost12
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
		AND OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
		GROUP BY InClaim, CorporateHistoryID, CorporateHistoryprofile, Year(TriggerStart1)
	END
	--if group = 1
	ELSE IF @flagOccGroupFilter = 1
	BEGIN
		SELECT InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1) AS Start1Year, SUM(Cost1) AS SumOfCost1, SUM(Cost2) AS SumOfCost2, SUM(Cost3) AS SumOfCost3, 
			SUM(Cost4) AS SumOfCost4, SUM(Cost5) AS SumOfCost5, SUM(Cost6) AS SumOfCost6, SUM(Cost7) AS SumOfCost7, SUM(Cost8) AS SumOfCost8, SUM(Cost9) AS SumOfCost9, 
			SUM(Cost10) AS SumOfCost10, SUM(Cost11) AS SumOfCost11, SUM(Cost12) AS SumOfCost12
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
		AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
		GROUP BY InClaim, CorporateHistoryID, CorporateHistoryprofile, Year(TriggerStart1)
	END
	ELSE
	BEGIN
		SELECT InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1) AS Start1Year, SUM(Cost1) AS SumOfCost1, SUM(Cost2) AS SumOfCost2, SUM(Cost3) AS SumOfCost3, 
			SUM(Cost4) AS SumOfCost4, SUM(Cost5) AS SumOfCost5, SUM(Cost6) AS SumOfCost6, SUM(Cost7) AS SumOfCost7, SUM(Cost8) AS SumOfCost8, SUM(Cost9) AS SumOfCost9, 
			SUM(Cost10) AS SumOfCost10, SUM(Cost11) AS SumOfCost11, SUM(Cost12) AS SumOfCost12
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
		GROUP BY InClaim, CorporateHistoryID, CorporateHistoryprofile, Year(TriggerStart1)
	END
ELSE
	BEGIN--if type and group = 1
	IF @flagOccGroupFilter = 1 AND @flagOccTypeFilter = 1
	BEGIN
		SELECT InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1) AS Start1Year, SUM(Cost1) AS SumOfCost1, SUM(Cost2) AS SumOfCost2, SUM(Cost3) AS SumOfCost3, 
			SUM(Cost4) AS SumOfCost4, SUM(Cost5) AS SumOfCost5, SUM(Cost6) AS SumOfCost6, SUM(Cost7) AS SumOfCost7, SUM(Cost8) AS SumOfCost8, SUM(Cost9) AS SumOfCost9, 
			SUM(Cost10) AS SumOfCost10, SUM(Cost11) AS SumOfCost11, SUM(Cost12) AS SumOfCost12
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), @InClaim)
			AND OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
			AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
		GROUP BY InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1)
	END
	--else if group = 1
	ELSE IF @flagOccGroupFilter = 1
	BEGIN
		SELECT InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1) AS Start1Year, SUM(Cost1) AS SumOfCost1, SUM(Cost2) AS SumOfCost2, SUM(Cost3) AS SumOfCost3, 
			SUM(Cost4) AS SumOfCost4, SUM(Cost5) AS SumOfCost5, SUM(Cost6) AS SumOfCost6, SUM(Cost7) AS SumOfCost7, SUM(Cost8) AS SumOfCost8, SUM(Cost9) AS SumOfCost9, 
			SUM(Cost10) AS SumOfCost10, SUM(Cost11) AS SumOfCost11, SUM(Cost12) AS SumOfCost12
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), @InClaim)
			AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
		GROUP BY InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1)
	END
	--else if type = 1
	ELSE IF @flagOccTypeFilter = 1
	BEGIN
		SELECT InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1) AS Start1Year, SUM(Cost1) AS SumOfCost1, SUM(Cost2) AS SumOfCost2, SUM(Cost3) AS SumOfCost3, 
			SUM(Cost4) AS SumOfCost4, SUM(Cost5) AS SumOfCost5, SUM(Cost6) AS SumOfCost6, SUM(Cost7) AS SumOfCost7, SUM(Cost8) AS SumOfCost8, SUM(Cost9) AS SumOfCost9, 
			SUM(Cost10) AS SumOfCost10, SUM(Cost11) AS SumOfCost11, SUM(Cost12) AS SumOfCost12
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), @InClaim)
			AND OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
		GROUP BY InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1)
	END
	--else
	ELSE
	BEGIN
		SELECT InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1) AS Start1Year, SUM(Cost1) AS SumOfCost1, SUM(Cost2) AS SumOfCost2, SUM(Cost3) AS SumOfCost3, 
			SUM(Cost4) AS SumOfCost4, SUM(Cost5) AS SumOfCost5, SUM(Cost6) AS SumOfCost6, SUM(Cost7) AS SumOfCost7, SUM(Cost8) AS SumOfCost8, SUM(Cost9) AS SumOfCost9, 
			SUM(Cost10) AS SumOfCost10, SUM(Cost11) AS SumOfCost11, SUM(Cost12) AS SumOfCost12
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), @InClaim)
		GROUP BY InClaim, CorporateHistoryID, CorporateHistoryProfile, Year(TriggerStart1)
	END
END
RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrenceCostByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOccurrenceCostByYear] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptOccurrenceCostByYear] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrenceCostByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOccurrenceCostByYear] TO [power_user]
GO
