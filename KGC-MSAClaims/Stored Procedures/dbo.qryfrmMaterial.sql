SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmMaterial]
as
Select MaterialID, Material
From tblMaterial
Order By Material
Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMaterial] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmMaterial] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmMaterial] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMaterial] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmMaterial] TO [power_user]
GO
