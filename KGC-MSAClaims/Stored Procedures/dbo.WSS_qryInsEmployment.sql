SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInsEmployment]
(@ClaimantPersonalInfoID bigint, @Jobsite nvarchar(250), @City nvarchar(100), @StateID int, @OccupationID int, @StartDate datetime, @EndDate datetime, @Emailaddress nvarchar(100), @Comments nvarchar(250), @SupportingDocID int, @SupportingPageNums nvarchar(50), @chkIsSecondaryExposure bit = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblClaimantEmployment (ClaimantPersonalInfoID, Jobsite, OccupationID, JobsiteCity, JobsiteStateID, EmpStartDate, EmpEndDate, SPUsername, Comments, EmpSupportingDocID, EmpSupportingPageNums, IsSecondaryExposure)
	VALUES (@ClaimantPersonalInfoID, UPPER(@Jobsite), @OccupationID, UPPER(@City), @StateID, @StartDate, @EndDate, @Emailaddress, @Comments, @SupportingDocID, @SupportingPageNums, @chkIsSecondaryExposure)
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsEmployment] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsEmployment] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryInsEmployment] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsEmployment] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsEmployment] TO [power_user]
GO
