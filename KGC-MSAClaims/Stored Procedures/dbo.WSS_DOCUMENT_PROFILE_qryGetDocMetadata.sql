SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata]
( @DocID int, @emailaddress nvarchar(250))
AS
--DECLARE @DocID int
--SET @DocID = 71385
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'rbaker@huddlestonbolen.com'


--GET LIST OF CLAIMANTS FOR DOCUMENT

DECLARE @FirmID int
SET @FirmID = (SELECT FirmID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @Claimants nvarchar(max)
SET @Claimants= '' 

DECLARE @SettleDocs nvarchar(max)
SET @SettleDocs= '' 

DECLARE @addClaimant nvarchar(max)
DECLARE @addClaimantID int


DECLARE @DocCat nvarchar(max)
DECLARE @PageNum nvarchar(max)

DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)	

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)

IF (RIGHT(@emailaddress, 11) = 'kcicllc.com' OR RIGHT(@emailaddress, 8) = 'kcic.com' OR @NoFilters = 1)
BEGIN
	DECLARE Cur_Claimants CURSOR Local Fast_Forward for
	SELECT COALESCE(mcd.Claimantpersonalinfoid, ''), COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') + ' (' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '') + ')'
	FROM tblMultiClaimantDocument mcd
	LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	WHERE mcd.DocumentID = @DocID
	GROUP BY mcd.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID) + ')' , LastName, FirstName

END
ELSE IF (@InsurerRole = 1)
BEGIN
	DECLARE Cur_Claimants CURSOR Local Fast_Forward for
	SELECT COALESCE(mcd.Claimantpersonalinfoid, ''), COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') + ' (' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '') + ')'
	FROM tblMultiClaimantDocument mcd
	LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	WHERE mcd.DocumentID = @DocID
	AND c.ClaimID NOT IN (select ClaimID from tblTenderHistory where TenderStatusID_old in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
	GROUP BY mcd.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID) + ')' , LastName, FirstName

END
ELSE
BEGIN
	DECLARE Cur_Claimants CURSOR Local Fast_Forward for
	SELECT mcd.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID) + ')' 
	FROM tblMultiClaimantDocument mcd
	LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.claimantpersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
	INNER JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID
	WHERE mcd.DocumentID = @DocID AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
	GROUP BY mcd.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID) + ')' , LastName, FirstName

END

--------------------------------------------------------------------

--Open Docket Cursor
OPEN Cur_Claimants

--Fetch Data
FETCH NEXT FROM Cur_Claimants INTO @addClaimantID, @addClaimant

WHILE @@Fetch_Status = 0
BEGIN
--assemble the string
--if LEN(@Claimants) = 0 SET @Claimants = '' + @addClaimant + ''
--else SET @Claimants = @Claimants + '
--' + '' + @addClaimant + ''
  if LEN(@Claimants) = 0 SET @Claimants = '<a target="_blank" href="/clients/msa/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + COALESCE(CONVERT(nvarchar(10), @addClaimantID), '') + '">' + COALESCE(@addClaimant, '') + '</a>'
      else SET @Claimants =  @Claimants  + '<br />' +  '<a target="_blank" href="/clients/msa/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + COALESCE(CONVERT(nvarchar(10), @addClaimantID), '') + '">' + COALESCE(@addClaimant, '') + '</a>'
      PRINT @Claimants


FETCH NEXT FROM Cur_Claimants INTO @addClaimantID, @addClaimant

END

CLOSE Cur_Claimants 
DEALLOCATE Cur_Claimants


-----------------------------------------------------------------

DECLARE Cur_SettleDoc CURSOR Local Fast_Forward for
SELECT DocCategory, PageNums
--ClaimLawsuitStatus, DismissalDate, DismissalProcessDate
FROM tblSettleDocs sd 
LEFT JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
WHERE DocumentID = @DocID

--Open Docket Cursor
OPEN Cur_SettleDoc

--Fetch Data
FETCH NEXT FROM Cur_SettleDoc INTO @DocCat, @PageNum

WHILE @@Fetch_Status = 0
BEGIN
      --assemble the string
      if LEN(@SettleDocs) = 0 SET @SettleDocs = COALESCE(@DocCat, '') + '&nbsp;&nbsp;pg(s): ' + COALESCE(@PageNum, '')
      else SET @SettleDocs =  @SettleDocs  + '<br />' +  COALESCE(@DocCat, '') + '&nbsp;&nbsp;pg(s): ' + COALESCE(@PageNum, '')
      PRINT @SettleDocs


      FETCH NEXT FROM Cur_SettleDoc INTO @DocCat, @PageNum
      
END


CLOSE Cur_SettleDoc 
DEALLOCATE Cur_SettleDoc

------------------------------------------------------------------------------------------------------------------
IF (RIGHT(@emailaddress, 11) = 'kcicllc.com' OR RIGHT(@emailaddress, 8) = 'kcic.com' OR @NoFilters = 1)
BEGIN

SELECT DISTINCT
d.DocumentID,
dt.ClaimantDocumentType, 
dg.DocumentGroup, 
d.Comments,
d.DocumentURL,
d.DocumentTitle,
d.SPDocId,
d.CreatedBy,
d.DateCreated,
d.ModifiedBy,
d.DateModified,
d.Accepted,
d.AcceptedBy,
d.DateAccepted,
d.ApprovedBy,
d.DateApproved,
f.Firm,
COALESCE(@Claimants, '') AS Claimants,
CASE WHEN LEN(@SettleDocs)=0 THEN 'N/A' ELSE COALESCE(@SettleDocs, 'N/A') END AS SettleDocs,
d.DocumentID,
d.DismissalDate,
mcd.QCDate AS DismissalProcessDate,
ClaimLawsuitStatus AS DismissalType,
DocumentSource,
d.DocumentSourceID,
NULL as AutoAccept,

--Invoices Only
d.InvoiceNumber,
d.Period,
d.DatePayable,
d.FeeAmount,
d.DisbursementAmount,
d.Adjustment,
d.AdjustmentNotes,
dis.Status, 
'' As VendorFirm, 
di.SubmitDate,
'' as PrivateComments,
Coalesce(IsFullyPaid,0) as IsFullyPaid


FROM tblDocuments d
LEFT OUTER JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
LEFT OUTER JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
LEFT OUTER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
LEFT OUTER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID 
LEFT OUTER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
LEFT OUTER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
LEFT OUTER JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
LEFT OUTER JOIN tblJurisdiction j ON l.jurisdictionid = j.jurisdictionid
LEFT OUTER JOIN tblState s ON l.stateid = s.stateid
--INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
LEFT OUTER JOIN tblDefenseCounselAddress dca ON l.defensecounseladdressid = dca.defensecounseladdressid 
LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
LEFT OUTER JOIN tblPaymentDefense pd on di.DefenseInvoiceID=pd.DefenseInvoiceID
--LEFT OUTER JOIN m_tblVendorFirm ef ON d.VendorFirmID = ef.VendorFirmID 
LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
LEFT OUTER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
LEFT OUTER JOIN tblDocumentSource ds ON d.DocumentSourceID = ds.DocumentSourceID
LEFT OUTER JOIN tblClaimlawsuitstatus cls ON d.claimlawsuitstatusID = cls.claimlawsuitstatusID

WHERE --AND (dj.DefenseCounselAddressID = @FirmID OR l.DefenseCounselAddressID = @FirmID)
d.DocumentID = @DociD --AND c.IsCurrentClaim = 1
GROUP BY cast(d.DocumentID as varchar(50)), dt.ClaimantDocumentType, dg.DocumentGroup, d.Comments, d.DocumentURL, d.DocumentTitle, d.SPDocId,
d.CreatedBy, d.DateCreated, d.ModifiedBy, d.DateModified, d.Accepted, d.AcceptedBy, d.DateAccepted, d.ApprovedBy, d.DateApproved,
--settlements and claimants, values through different joins for lawsuits
l.docketnumber, l.CaseCaption, s.[State],
--lawsuits only
j.jurisdiction, l.leadplaintifflastname+', '+l.leadplaintifffirstname, l.filedate, l.servicedate, pca.PlaintiffCounsel, f.Firm, COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, ''), DocketNumbers,
d.DocumentID, DismissalDate, mcd.QCDate, ClaimLawsuitStatus, DocumentSource, d.DocumentSourceID,d.InvoiceNumber,
d.Period,
d.DatePayable,
d.FeeAmount,
d.DisbursementAmount,
d.Adjustment,
d.AdjustmentNotes,
dis.[Status], 
di.SubmitDate,
Coalesce(IsFullyPaid,0)
END
ELSE IF (@InsurerRole = 1)
BEGIN
		
	SELECT DISTINCT
	d.DocumentID,
	dt.ClaimantDocumentType, 
	dg.DocumentGroup, 
	d.Comments,
	d.DocumentURL,
	d.DocumentTitle,
	d.SPDocId,
	d.CreatedBy,
	d.DateCreated,
	d.ModifiedBy,
	d.DateModified,
	d.Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	d.ApprovedBy,
	d.DateApproved,
	f.Firm,
	COALESCE(@Claimants, '') AS Claimants,
	CASE WHEN LEN(@SettleDocs)=0 THEN 'N/A' ELSE COALESCE(@SettleDocs, 'N/A') END AS SettleDocs,
	d.DocumentID,
	d.DismissalDate,
	mcd.QCDate AS DismissalProcessDate,
	ClaimLawsuitStatus AS DismissalType,
	DocumentSource,
	d.DocumentSourceID,
	NULL as AutoAccept,

	--Invoices Only
	d.InvoiceNumber,
	d.Period,
	d.DatePayable,
	d.FeeAmount,
	d.DisbursementAmount,
	d.Adjustment,
	d.AdjustmentNotes,
	dis.Status, 
	'' As VendorFirm, 
	di.SubmitDate,
	'' as PrivateComments,
	Coalesce(IsFullyPaid,0) as IsFullyPaid


	FROM tblDocuments d
	LEFT OUTER JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
	LEFT OUTER JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
	LEFT OUTER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	LEFT OUTER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID 
	LEFT OUTER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT OUTER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT OUTER JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
	LEFT OUTER JOIN tblJurisdiction j ON l.jurisdictionid = j.jurisdictionid
	LEFT OUTER JOIN tblState s ON l.stateid = s.stateid
	--INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
	LEFT OUTER JOIN tblDefenseCounselAddress dca ON l.defensecounseladdressid = dca.defensecounseladdressid 
	LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
	LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
	LEFT OUTER JOIN tblPaymentDefense pd on di.DefenseInvoiceID=pd.DefenseInvoiceID
	--LEFT OUTER JOIN m_tblVendorFirm ef ON d.VendorFirmID = ef.VendorFirmID 
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
	LEFT OUTER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	LEFT OUTER JOIN tblDocumentSource ds ON d.DocumentSourceID = ds.DocumentSourceID
	LEFT OUTER JOIN tblClaimlawsuitstatus cls ON d.claimlawsuitstatusID = cls.claimlawsuitstatusID

	WHERE --AND (dj.DefenseCounselAddressID = @FirmID OR l.DefenseCounselAddressID = @FirmID)
	d.DocumentID = @DociD --AND c.IsCurrentClaim = 1
	AND c.ClaimID NOT IN (select ClaimID_old from tblTenderHistory where TenderStatusID_old in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
	AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
	GROUP BY cast(d.DocumentID as varchar(50)), dt.ClaimantDocumentType, dg.DocumentGroup, d.Comments, d.DocumentURL, d.DocumentTitle, d.SPDocId,
	d.CreatedBy, d.DateCreated, d.ModifiedBy, d.DateModified, d.Accepted, d.AcceptedBy, d.DateAccepted, d.ApprovedBy, d.DateApproved,
	--settlements and claimants, values through different joins for lawsuits
	l.docketnumber, l.CaseCaption, s.[State],
	--lawsuits only
	j.jurisdiction, l.leadplaintifflastname+', '+l.leadplaintifffirstname, l.filedate, l.servicedate, pca.PlaintiffCounsel, f.Firm, COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, ''), DocketNumbers,
	d.DocumentID, DismissalDate, mcd.QCDate, ClaimLawsuitStatus, DocumentSource, d.DocumentSourceID,d.InvoiceNumber,
	d.Period,
	d.DatePayable,
	d.FeeAmount,
	d.DisbursementAmount,
	d.Adjustment,
	d.AdjustmentNotes,
	dis.[Status], 
	di.SubmitDate,
	Coalesce(IsFullyPaid,0)

END
ELSE
BEGIN


SELECT DISTINCT
d.DocumentID,
dt.ClaimantDocumentType, 
dg.DocumentGroup, 
d.Comments,
d.DocumentURL,
d.DocumentTitle,
d.SPDocId,
d.CreatedBy,
d.DateCreated,
d.ModifiedBy,
d.DateModified,
d.Accepted,
d.AcceptedBy,
d.DateAccepted,
d.ApprovedBy,
d.DateApproved,
f.Firm,
COALESCE(@Claimants, '') AS Claimants,
CASE WHEN LEN(@SettleDocs)=0 THEN 'N/A' ELSE COALESCE(@SettleDocs, 'N/A') END AS SettleDocs,
d.DocumentID,
d.DismissalDate,
mcd.QCDate AS DismissalProcessDate,
ClaimLawsuitStatus AS DismissalType,
DocumentSource,
d.DocumentSourceID,
NULL as AutoAccept,

--Invoices Only
d.InvoiceNumber,
d.Period,
d.DatePayable,
d.FeeAmount,
d.DisbursementAmount,
d.Adjustment,
d.AdjustmentNotes,
dis.Status, 
'' AS VendorFirm, 
di.SubmitDate,
'' as PrivateComments,
Coalesce(IsFullyPaid,0) as IsFullyPaid

FROM tblDocuments d
LEFT OUTER JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
LEFT OUTER JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
LEFT OUTER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
LEFT OUTER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
LEFT OUTER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
LEFT OUTER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
LEFT OUTER JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
LEFT OUTER JOIN tblJurisdiction j ON l.jurisdictionid = j.jurisdictionid
LEFT OUTER JOIN tblState s ON l.stateid = s.stateid
LEFT OUTER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
LEFT OUTER JOIN tblDefenseCounselAddress dca ON l.defensecounseladdressid = dca.defensecounseladdressid 
LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
LEFT OUTER JOIN tblPaymentDefense pd on di.DefenseInvoiceID=pd.DefenseInvoiceID
--LEFT OUTER JOIN m_tblVendorFirm ef ON d.VendorFirmID = ef.VendorFirmID 
LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
LEFT OUTER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
LEFT OUTER JOIN tblDocumentSource ds ON d.DocumentSourceID = ds.DocumentSourceID
LEFT OUTER JOIN tblClaimlawsuitstatus cls ON d.claimlawsuitstatusID = cls.claimlawsuitstatusID
WHERE  (l.DefenseCounselAddressID = @FirmAddressID OR d.FirmID = @FirmID)
AND d.DocumentID = @DociD --AND c.IsCurrentClaim = 1
GROUP BY cast(d.DocumentID as varchar(50)), dt.ClaimantDocumentType, dg.DocumentGroup, d.Comments, d.DocumentURL, d.DocumentTitle, d.SPDocId,
d.CreatedBy, d.DateCreated, d.ModifiedBy, d.DateModified, d.Accepted, d.AcceptedBy, d.DateAccepted, d.ApprovedBy, d.DateApproved,
--settlements and claimants, values through different joins for lawsuits
l.docketnumber, l.CaseCaption, s.[State],
--lawsuits only
j.jurisdiction, l.leadplaintifflastname+', '+l.leadplaintifffirstname, l.filedate, l.servicedate, pca.PlaintiffCounsel, f.Firm, COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, ''), DocketNumbers,
d.DocumentID, DismissalDate, mcd.QCDate, ClaimLawsuitStatus, DocumentSource, d.DocumentSourceID,d.InvoiceNumber,
d.Period,
d.DatePayable,
d.FeeAmount,
d.DisbursementAmount,
d.Adjustment,
d.AdjustmentNotes,
dis.Status, 
di.SubmitDate,
Coalesce(IsFullyPaid,0)

END
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata] TO [power_user]
GO
