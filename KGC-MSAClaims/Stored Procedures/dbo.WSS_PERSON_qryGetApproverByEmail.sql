SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_PERSON_qryGetApproverByEmail]
    (
      @ApproverTypeID int
    )
AS -- Declare the temporary fields used to track firm and email domain
    DECLARE @PersonID INT


BEGIN
	SELECT DISTINCT CONVERT(nvarchar(10),PersonID) AS PersonID
	FROM vtblPerson 
	WHERE FirmAddressID = 35
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [power_user]
GO
