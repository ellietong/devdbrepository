SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptQuarterlyClaimsReport_State] (@Quarter int, @Year varchar(max)) 
AS
SET NOCOUNT ON

--Declare @Quarter int
--Set @Quarter = 2
--Declare @Year nvarchar(25)
--Set @Year = '2013'

--get last quarter start date
DECLARE @PreviousQuarterStart varchar(25)
SET @PreviousQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '1/1/' + @Year
		 WHEN @Quarter = 2 THEN '4/1/' + @Year
		 WHEN @Quarter = 3 THEN '7/1/' + @Year
		 WHEN @Quarter = 4 THEN '10/1/' + @Year
		 ELSE '1/1/2050' END

--get last quarter end date
DECLARE @PreviousQuarterEnd varchar(25)
SET @PreviousQuarterEnd = 
	CASE WHEN @Quarter = 1 THEN '3/31/' + @Year
		 WHEN @Quarter = 2 THEN '6/30/' + @Year
		 WHEN @Quarter = 3 THEN '9/30/' + @Year
		 WHEN @Quarter = 4 THEN '12/31/' + @Year
		 ELSE '12/31/2050' END

DECLARE @Temp Table (FullNameState nvarchar(max), Costs money, SettlementAmount money, OpenCount int, SettleCount int, DismissalCount int,
PendingCount int)
INSERT INTO @Temp
	SELECT COALESCE(FullNameState,'Unknown'), 
		   SUM(Costs),0,0,0,0,0
	FROM tblDefenseInvoice di
		LEFT JOIN tblDefInvClaimantDetail dicd on dicd.DefenseInvoiceID = di.DefenseInvoiceID
		LEFT JOIN tblClaim c on c.ClaimID = dicd.ClaimID
		LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		LEFT JOIN tblState s on s.StateID = l.StateID
	WHERE COALESCE(SubmitDate,'1/1/1900') <= @PreviousQuarterEnd AND COALESCE(SubmitDate,'1/1/1900')  >= @PreviousQuarterStart
		AND c.ClaimID NOT IN (SELECT ClaimID 
								FROM tblClaimTenderHistory CTH
								INNER JOIN tblTenderHistory TH ON TH.TenderHistoryID = CTH.TenderHistoryID
								WHERE TenderStatusID in (2, 5) and tenderblockid = 15 and TenderTypeID <> 2)
		AND C.ClaimID NOT IN (SELECT ClaimID
					FROM tblTender_History TH
					INNER JOIN tblTender_Acknowledgement TA ON TA.TenderHistoryID = TH.TenderHistoryID
					WHERE PersonID = 3279 AND TenderTypeID <> 2)
		AND TortTypeID = 1
	GROUP BY FullNameState
	
UNION ALL
	  
	SELECT COALESCE(FullNameState, 'Unknown'), 0,
		   SUM(COALESCE(SettlementAmount,0)) as SettlementAmount,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 3 THEN 1 ELSE 0 END) as OpenCount,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 1 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END) as SettleCount,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 4 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END) as DismissalCount, 0
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		  LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN tblState s on s.StateID = l.StateID
		  LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		  LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	WHERE cls.ClaimLawsuitStatusGroupID in (1,3,4)
		  AND COALESCE(ProcessedDate,'1/1/1900') <= @PreviousQuarterEnd AND COALESCE(ProcessedDate,'1/1/1900') >= @PreviousQuarterStart
	      AND m.MaterialID != 10
		  	AND c.ClaimID NOT IN (SELECT ClaimID 
									FROM tblClaimTenderHistory CTH
									INNER JOIN tblTenderHistory TH ON TH.TenderHistoryID = CTH.TenderHistoryID
									WHERE TenderStatusID in (2, 5) and tenderblockid = 15 and TenderTypeID <> 2)
		AND C.ClaimID NOT IN (SELECT ClaimID
					FROM tblTender_History TH
					INNER JOIN tblTender_Acknowledgement TA ON TA.TenderHistoryID = TH.TenderHistoryID
					WHERE PersonID = 3279 AND TenderTypeID <> 2)	
		  AND (CASE WHEN ClaimLawsuitStatusGroupID = 3 THEN 1 ELSE 0 END>0 
			  or CASE WHEN ClaimLawsuitStatusGroupID = 1 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END>0 
			  or CASE WHEN ClaimLawsuitStatusGroupID = 4 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END>0)
		  AND TortTypeID = 1
		  AND c.ClaimID != 26234
		  ----------AND NCCReferenceID NOT IN (SELECT NCCReferenceID 
				----------	FROM tblClaim C
				----------	LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
				----------	LEFT JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID = CL.ClaimLawsuitID
				----------	LEFT JOIN tblClaimLawsuitStatus CLS ON CLS.ClaimLawsuitStatusID = CLST.ClaimLawsuitStatusID
				----------	WHERE IsPrimaryStatus <> 1
				----------	AND ClaimLawsuitStatusGroupID = 4)
		  AND CLST.ClaimLawsuitStatusID <> 38
		  AND CL.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID = 38 AND IsPrimaryStatus = 1)
		  AND CL.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID = 29)
		  AND DocketNumber NOT LIKE '%330307728B%'
		  AND CLST.ClaimLawsuitStatusID != 41  ----> Exclude Verdict claims
	GROUP BY FullNameState
	
UNION ALL

	SELECT FullNameState, 0, 0, 0, 0, 0, 
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 3 THEN 1 ELSE 0 END) as PendingCount
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		  LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN tblState s on s.StateID = l.StateID
		  LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		  LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	WHERE cls.ClaimLawsuitStatusGroupID = 3
		  AND IsPrimaryStatus = 1
		  AND COALESCE(ProcessedDate,'1/1/1900') <= @PreviousQuarterEnd
	      AND m.MaterialID != 10		
		  AND c.ClaimID NOT IN (SELECT ClaimID 
								FROM tblClaimTenderHistory CTH
								INNER JOIN tblTenderHistory TH ON TH.TenderHistoryID = CTH.TenderHistoryID
								WHERE TenderStatusID in (2, 5) and TenderBlockID = 15 and TenderTypeID <> 2)
		AND C.ClaimID NOT IN (SELECT ClaimID
					FROM tblTender_History TH
					INNER JOIN tblTender_Acknowledgement TA ON TA.TenderHistoryID = TH.TenderHistoryID
					WHERE PersonID = 3279 AND TenderTypeID <> 2)
		  AND TortTypeID = 1
		  AND CLST.ClaimLawsuitStatusID != 41  ----> Exclude Verdict claims
	GROUP BY FullNameState

UNION ALL

	SELECT FullNameState, 0, 0, 0, 0, 0, 
		  SUM(CASE WHEN ClaimLawsuitStatusGroupID <> 3 THEN 1 ELSE 0 END) as PendingCount
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		  LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN tblState s on s.StateID = l.StateID
		  LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		  LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	WHERE cls.ClaimLawsuitStatusGroupID <> 3
		  AND IsPrimaryStatus = 1
		  AND ProcessedDate > @PreviousQuarterEnd
	      AND m.MaterialID != 10		
		  AND c.ClaimID NOT IN (SELECT ClaimID 
								FROM tblClaimTenderHistory CTH
								INNER JOIN tblTenderHistory TH ON TH.TenderHistoryID = CTH.TenderHistoryID
								WHERE TenderStatusID in (2, 5) and TenderBlockID = 15 and TenderTypeID <> 2)
		AND C.ClaimID NOT IN (SELECT ClaimID
					FROM tblTender_History TH
					INNER JOIN tblTender_Acknowledgement TA ON TA.TenderHistoryID = TH.TenderHistoryID
					WHERE PersonID = 3279 AND TenderTypeID <> 2)
		  AND TortTypeID = 1
		  AND CLST.ClaimLawsuitStatusID != 41  ----> Exclude Verdict claims
	GROUP BY FullNameState
	
	
Select CASE WHEN FullNameState = 'Unknown' THEN 'General Expenses' ELSE FullNameState END AS FullNameState,
	   SUM(Costs) as Defense,
	   SUM(SettlementAmount) as Indemnity,
	   SUM(OpenCount) as OpenCount,
	   SUM(SettleCount) as SettledCount,
	   SUM(DismissalCount) as DismissedCount,
	   SUM(PendingCOunt) as PendingCount
from @Temp
group by FullNameState
ORDER BY CASE WHEN FullNameState = 'Unknown' THEN 1 ELSE 0 END, FullNameState

RETURN


--select * from tblClaimLawsuitStatusGroup

select * from tbltendertype
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_State] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_State] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptQuarterlyClaimsReport_State] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_State] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_State] TO [power_user]
GO
