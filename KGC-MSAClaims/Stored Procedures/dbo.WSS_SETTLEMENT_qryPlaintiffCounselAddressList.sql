SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList]
AS
SET NOCOUNT ON

SELECT PlaintiffCounselAddressID, 
	PlaintiffCounsel + ' (' + CASE WHEN PlaintiffCounselCity IS NULL THEN 'UNKNOWN' ELSE PlaintiffCounselCity END + ')' AS PlaintiffCounsel 
FROM tblPlaintiffCounselAddress pca 
WHERE PlaintiffCounsel IS NOT NULL 
	AND PlaintiffCounsel <> ''
UNION
SELECT NULL, NULL
ORDER BY PlaintiffCounsel + ' (' + CASE WHEN PlaintiffCounselCity IS Null THEN 'UNKNOWN' ELSE PlaintiffCounselCity END + ')', PlaintiffCounselAddressID



GO
