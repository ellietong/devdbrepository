SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimLawsuits]
(@ClaimID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT cl.LawsuitID, CaseCaption + ' (' + COALESCE(DocketNumber, 'N/A') + ')' AS CaseCaption
	FROM tblClaimLawsuit cl
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	WHERE ClaimID = @ClaimID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimLawsuits] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimLawsuits] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimLawsuits] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimLawsuits] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimLawsuits] TO [power_user]
GO
