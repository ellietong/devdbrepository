SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmAllocationByID]
AS
SET NOCOUNT ON
SELECT AllocationID, CONVERT(varchar, allocationid) + ' ' + AllocationDescription AS AllocationNumDescription, AllocationDescription
FROM tblAllocation
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationByID] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmAllocationByID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationByID] TO [power_user]
GO
