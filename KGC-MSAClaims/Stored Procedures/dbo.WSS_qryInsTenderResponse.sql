SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInsTenderResponse]
(@TenderHistoryID bigint, @TenderContactID int, @DocumentID bigint)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	INSERT INTO tblTenderResponse (TenderHistoryID, TenderContactID, DocumentID)
	SELECT @TenderHistoryID, @TenderContactID, @DocumentID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsTenderResponse] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsTenderResponse] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryInsTenderResponse] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsTenderResponse] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsTenderResponse] TO [power_user]
GO
