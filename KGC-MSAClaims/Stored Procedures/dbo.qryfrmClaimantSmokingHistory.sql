SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantSmokingHistory] (@ClaimantPersonalInfoID int)
as

Select *
From tblClaimantSmokingHistory
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By SmokingStart, SmokingEnd

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSmokingHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSmokingHistory] TO [fowlerh]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSmokingHistory] TO [fowlerh]
GRANT ALTER ON  [dbo].[qryfrmClaimantSmokingHistory] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSmokingHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSmokingHistory] TO [power_user]
GO
