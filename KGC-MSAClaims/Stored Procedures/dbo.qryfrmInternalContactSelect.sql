SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmInternalContactSelect]
As

SET NOCOUNT ON

SELECT cc.InternalContactID, ic.InternalCompany + ' - ' + clt.ContactClaimType + ' - ' + COALESCE (ct.ContactType, '') + ' - ' + 
	COALESCE (c.LastName + ', ', '') + COALESCE (c.FirstName + ' ', '') + COALESCE (c.AddressLine1 + ' ', '') + 
	COALESCE (c.AddressLine2 + ' ', '') + COALESCE (c.City + ', ', '') + COALESCE (st.State, '') AS contactmerge 

FROM dbo.tblInternalContact cc 
INNER JOIN dbo.tblInternalCompany ic ON ic.InternalCompanyID = cc.InternalCompanyID 
INNER JOIN dbo.tblContact c ON c.ContactID = cc.ContactID 
INNER JOIN dbo.tblState st ON st.StateID = c.StateID 
INNER JOIN dbo.tblContactType ct ON ct.ContactTypeID = c.ContactTypeID 
INNER JOIN dbo.tblContactClaimType clt ON clt.ContactClaimTypeID = c.ContactClaimTypeID 

ORDER BY ic.InternalCompany, clt.ContactClaimType, ct.ContactType, c.LastName


RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInternalContactSelect] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmInternalContactSelect] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmInternalContactSelect] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInternalContactSelect] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmInternalContactSelect] TO [power_user]
GO
