SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByStateByMonth] 
	-- Add the parameters for the stored procedure here
	(@MonthID int, @YearID int)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @month int
--Set @month= 11
--Declare @year int
--Set @Year=2010

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

select FullNameState As State, COUNT(distinct(cl.ClaimLawsuitID)) As Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s on l.StateID=s.StateID
where	month(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))=@Month 
		and year(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))=@Year 
group by FullNameState
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByStateByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByStateByMonth] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_REPORTS_qryNewFilingsByStateByMonth] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByStateByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByStateByMonth] TO [power_user]
GO
