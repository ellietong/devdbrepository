SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryClaimantRelatedPartyInfo]
@ClaimantPersonalInfoID bigint=null, @emailaddress nvarchar(250)
AS
BEGIN
--DECLARE @Claimantpersonalinfoid bigint
--SET @ClaimantPersonalInfoID = 30661
--DECLARE @emailaddress nvarchar(150)
--SET @emailaddress = 'marleywylainncc'

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)	
					

IF RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1
BEGIN
Select ClaimantRelatedPartyID, 
	ClaimantPersonalInfoID,  
	COALESCE(LastName, '') + ', ' + CASE WHEN FirstName IS NULL 
    THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
    LastName, FirstName, MiddleName,
	Relationship, 
	LC.LegalCapacity,
	Case When crp.RPSocialSecurityNumber IS NULL Then '' ELSE 'XXX-XX-' + RIGHT(crp.RPSocialSecurityNumber, 4) END AS SSN,
	COALESCE(CONVERT(nvarchar(15), crp.RPSocialSecurityNumber), '') AS FullSSN, crp.RelationshipID, crp.LegalCapacityID,
	coalesce(nullif(coalesce(RPAddress1 + CHAR(13) + CHAR(10), '') + coalesce(RPAddress2 + CHAR(13) + CHAR(10),'') + 
						coalesce(RPCity + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,RPZip),'') + Coalesce('-'+convert(varchar,RPmailzip),''),''),'') as CompleteAddress,
						  RPCity, s.StateID, RPZip, RPMailZip, RPAddress1, RPAddress2, RPPhone, RPExtension
From tblClaimantRelatedParty crp 
LEFT JOIN tblRelationship r on r.RelationshipID=crp.RelationshipID
LEFT JOIN tblLegalCapacity LC ON LC.LegalCapacityID = crp.LegalCapacityID
LEFT JOIN tblState s ON crp.StateID = s.StateID
WHERE ClaimantPersonalInfoID=@ClaimantPersonalInfoID
END
ELSE IF (@InsurerRole = 1)
BEGIN
	Select ClaimantRelatedPartyID, 
		crp.ClaimantPersonalInfoID,  
		COALESCE(LastName, '') + ', ' + CASE WHEN FirstName IS NULL 
		THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
		LastName, FirstName, MiddleName,
		Relationship, 
		LC.LegalCapacity,
		Case When crp.RPSocialSecurityNumber IS NULL Then '' ELSE 'XXX-XX-' + RIGHT(crp.RPSocialSecurityNumber, 4) END AS SSN,
		COALESCE(CONVERT(nvarchar(15), crp.RPSocialSecurityNumber), '') AS FullSSN, crp.RelationshipID, crp.LegalCapacityID,
		coalesce(nullif(coalesce(RPAddress1 + CHAR(13) + CHAR(10), '') + coalesce(RPAddress2 + CHAR(13) + CHAR(10),'') + 
							coalesce(RPCity + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,RPZip),'') + Coalesce('-'+convert(varchar,RPmailzip),''),''),'') as CompleteAddress,
							  RPCity, s.StateID, RPZip, RPMailZip, RPAddress1, RPAddress2, RPPhone, RPExtension
	From tblClaimantRelatedParty crp 
	LEFT JOIN tblRelationship r on r.RelationshipID=crp.RelationshipID
	LEFT JOIN tblLegalCapacity LC ON LC.LegalCapacityID = crp.LegalCapacityID
	LEFT JOIN tblState s ON crp.StateID = s.StateID
	INNER JOIN tblClaim c ON crp.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	WHERE crp.ClaimantPersonalInfoID=@ClaimantPersonalInfoID
	AND c.ClaimID NOT IN (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
	AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
END
ELSE
BEGIN
Select DISTINCT ClaimantRelatedPartyID, 
	crp.ClaimantPersonalInfoID,  
	COALESCE(LastName, '') + ', ' + CASE WHEN FirstName IS NULL 
    THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
    LastName, FirstName, MiddleName,
	Relationship, 
	LC.LegalCapacity,
	Case When crp.RPSocialSecurityNumber IS NULL Then '' ELSE 'XXX-XX-' + RIGHT(crp.RPSocialSecurityNumber, 4) END AS SSN,
	COALESCE(CONVERT(nvarchar(15), crp.RPSocialSecurityNumber), '') AS FullSSN, crp.RelationshipID, crp.LegalCapacityID,
	coalesce(nullif(coalesce(RPAddress1 + CHAR(13) + CHAR(10), '') + coalesce(RPAddress2 + CHAR(13) + CHAR(10),'') + 
						coalesce(RPCity + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,RPZip),'') + Coalesce('-'+convert(varchar,RPmailzip),''),''),'') as CompleteAddress,
						  RPCity, s.StateID, RPZip, RPMailZip, RPAddress1, RPAddress2, RPPhone, RPExtension
From tblClaimantRelatedParty crp 
LEFT JOIN tblRelationship r on r.RelationshipID=crp.RelationshipID
LEFT JOIN tblLegalCapacity LC ON LC.LegalCapacityID = crp.LegalCapacityID
LEFT JOIN tblClaim c ON crp.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
                      LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
                      LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
INNER JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID
INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
LEFT JOIN tblState s ON crp.StateID = s.StateID
WHERE crp.ClaimantPersonalInfoID=@ClaimantPersonalInfoID AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)

END



END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantRelatedPartyInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantRelatedPartyInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimantRelatedPartyInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantRelatedPartyInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantRelatedPartyInfo] TO [power_user]
GO
