SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdOccupationInfo]
(@ClaimantEmploymentID int, @OccupationID int, @EmpStartDate datetime, @EmpEndDate datetime, @Username nvarchar(250), @Jobsite nvarchar(4000), @JobsiteCity nvarchar(250), @JobsiteStateID int, @Comments nvarchar (1000), @SupportingDocID int, @SupportingPageNums nvarchar(50), @chkIsSecondaryExposure bit = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE tblClaimantEmployment 
    SET OccupationID = @OccupationID,
		EmpStartDate = @EmpStartDate,
		EmpEndDate = @EmpEndDate,
		SPUsername = @Username,
		Jobsite = UPPER(@Jobsite),
		JobsiteCity = UPPER(@JobsiteCity),
		JobsiteStateID = @JobsiteStateID,
		Comments = @Comments,
		EmpSupportingDocID = @SupportingDocID,
		EmpSupportingPageNums = @SupportingPageNums,
		IsSecondaryExposure = @chkIsSecondaryExposure
	WHERE ClaimantEmploymentID = @ClaimantEmploymentID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdOccupationInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdOccupationInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdOccupationInfo] TO [power_user]
GO
