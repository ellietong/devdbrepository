SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryLawsuitClaimants]
(@LawsuitID bigint, @emailaddress nvarchar)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @InsurerRole bit
	SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)

	IF(@InsurerRole = 1)
	BEGIN
		SELECT CPI.ClaimantPersonalInfoID AS ClaimantID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
						  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName
		FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo CPI ON c.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		WHERE cl.LawsuitID = @LawsuitID
			AND c.ClaimID NOT IN (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
			AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
		ORDER BY LastName, FirstName
	END
	ELSE
	BEGIN
		SELECT CPI.ClaimantPersonalInfoID AS ClaimantID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
						  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName
		FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo CPI ON c.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		WHERE cl.LawsuitID = @LawsuitID
		ORDER BY LastName, FirstName
	
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryLawsuitClaimants] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryLawsuitClaimants] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryLawsuitClaimants] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryLawsuitClaimants] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryLawsuitClaimants] TO [power_user]
GO
