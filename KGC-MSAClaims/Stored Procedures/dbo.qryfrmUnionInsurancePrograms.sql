SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmUnionInsurancePrograms]
AS
SET NOCOUNT ON

SELECT InsuranceProgramID, InsuranceProgram
FROM tblInsuranceProgram

UNION 

SELECT 0, '<<All Insurance Programs>>' AS InsuranceProgram
FROM tblInsuranceProgram
ORDER BY InsuranceProgram

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionInsurancePrograms] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionInsurancePrograms] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmUnionInsurancePrograms] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionInsurancePrograms] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionInsurancePrograms] TO [power_user]
GO
