SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmSuffix]
as

Select SuffixID, Suffix
From tblClaimantSuffix
Order By Suffix

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSuffix] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSuffix] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmSuffix] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSuffix] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSuffix] TO [power_user]
GO
