SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetLastNameByID](@ClaimantPersonalInfoID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--SELECT TOP 1 LastName FROM [kga-demodb].dbo.tblClaimantPersonalInfo WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	--SELECT TOP 1 LastName--, cpil.ClaimantPersonalInfoID, cpil.Inj_prty_ID
	--FROM tblClaimantPersonalInfo cpi
	
	--WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID-- OR cpil.Inj_Prty_ID = @ClaimantPersonalInfoID
	SELECT TOP 1 LastName, cpil.ClaimantPersonalInfoID, cpil.Inj_prty_ID
	FROM tblClaimantPersonalInfo cpi
	LEFT JOIN m_tblClaimantPersonalInfo_Lookup cpil ON cpi.ClaimantPersonalInfoID = cpil.ClaimantPersonalInfoID
	WHERE cpil.ClaimantPersonalInfoID = @ClaimantPersonalInfoID OR cpil.Inj_Prty_ID = @ClaimantPersonalInfoID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetLastNameByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetLastNameByID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetLastNameByID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetLastNameByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetLastNameByID] TO [power_user]
GO
