SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch]
	-- Add the parameters for the stored procedure here
    @emailaddress NVARCHAR(1000)
AS --DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'dpicone@hptylaw.com'

    DECLARE @firmID INT
    SET @firmID = ( SELECT  FirmID
                    FROM    vtblPerson
                    WHERE   Email = @emailaddress
                  )
    BEGIN
        DECLARE @emaildomain NVARCHAR(1000)
        SET @emaildomain = SUBSTRING(@emailaddress,
                                     CHARINDEX('@', @emailaddress),
                                     LEN(@emailaddress) - CHARINDEX('@',
                                                              @emailaddress)
                                     + 1)
        IF @emaildomain = '@kcicllc.com' OR @emailDomain='@kcic.com'
            OR @firmID IN ( 374,269 ) 
            BEGIN
                SELECT DISTINCT
                        DefenseCounselID ,
                        DefenseCounsel
                FROM    tblDefenseCounsel
               -- WHERE   AcuityKey is not null--DefenseCounselID NOT IN ( 188, 265 )
                UNION
                SELECT  NULL ,
                        ''
                ORDER BY DefenseCounsel
            END
        --Custom Marley Code    
        --IF ( SELECT FirmAddressID
        --     FROM   vtblPerson
        --     WHERE  Email = @emailaddress
        --   ) = 138 
        --    BEGIN
        --        SELECT DISTINCT
        --                DefenseCounselID ,
        --                DefenseCounsel + ' (' + COALESCE(DefenseCounselCity,
        --                                                 'Unknown') + ')' AS DefenseCounsel
        --        FROM    tblDefenseCounselAddress
        --        WHERE   DefenseCounselID NOT IN ( 188, 265 )
        --        UNION
        --        SELECT  NULL ,
        --                ''
        --        ORDER BY DefenseCounsel + ' (' + COALESCE(DefenseCounselCity,
        --                                                  'Unknown') + ')'
	
        --    END	
        ELSE 
            BEGIN
                SELECT  dca.DefenseCounselID ,
                        dca.DefenseCounsel
                FROM    dbo.tblDefenseCounselAddress dca
                WHERE   dca.DefenseCounselID = @FirmID
                UNION
                SELECT  NULL ,
                        ''
                ORDER BY dca.DefenseCounsel
            END
		
	 
    END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch] TO [power_user]
GO
