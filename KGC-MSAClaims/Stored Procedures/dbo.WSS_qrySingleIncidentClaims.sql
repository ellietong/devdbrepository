SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qrySingleIncidentClaims]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
SELECT DISTINCT COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') AS 'ClaimantName', CPI.ClaimantPersonalInfoID, CONVERT(date, ServiceDate) AS 'ServiceDate', ce.Jobsite AS 'PlaceOfIncident', s.State, j.Jurisdiction
FROM tblClaim C 
LEFT JOIN tblClaimLawsuit CL ON C.ClaimID=CL.ClaimID
inner JOIN tblLawsuit L ON CL.LawsuitID=L.LawsuitID
INNER JOIN tblClaimantPersonalInfo CPI ON C.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
left join tblClaimantEmployment ce on ce.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
left join tblDefenseJurisdiction dj on dj.JurisdictionID = L.JurisdictionID and L.StateID = dj.StateID
left join tblJurisdiction j on j.JurisdictionID = dj.JurisdictionID
left join tblState s on s.StateID=dj.StateID
inner join tblTortType t on t.TortTypeID = C.TortTypeID
WHERE C.TortTypeID = 2
ORDER BY CONVERT(date, ServiceDate) ASC, [ClaimantName] ASC

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySingleIncidentClaims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySingleIncidentClaims] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qrySingleIncidentClaims] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySingleIncidentClaims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySingleIncidentClaims] TO [power_user]
GO
