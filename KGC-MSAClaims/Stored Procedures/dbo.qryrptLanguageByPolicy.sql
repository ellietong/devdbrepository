SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptLanguageByPolicy]
(@policyID int = null)
As

SET NOCOUNT ON

--declare @policyid int
--set @policyid=1

--DECLARE @maxPolicyID int
--SELECT @maxPolicyID = MAX(PolicyID) FROM tblPolicy

--SELECT p.PolicyID, p.LanguageID, p.Batesstart, p.batesend, Provision, p.ExactLanguage
--FROM vPolicyLanguage p
--WHERE p.PolicyID >= COALESCE(@policyID, 0) AND p.PolicyID <= COALESCE(NULLIF(@policyID, 0), @maxPolicyID) 
--ORDER BY Provision

--RETURN

--DECLARE @SRPTTable TABLE(PolicyID INT, ProvisionLabelID INT, Provision varchar(75), Provisionvalue varchar (300), LanguageID INT, 
--	BatesStart varchar(50), BatesEnd varchar(50), ExactLanguage varchar(4000), ProvisionNotes varchar(500), Location varchar(50), 
--	langtype varchar (100),EndorsementNum varchar(50),  LocationID INT)
--	INSERT INTO @SRPTTable
SELECT EL.PolicyID, 
	PL.ProvisionLabelID, 
	PL.Provision, 
	PVV.Provisionvalue,
	EL.ExactLanguageID, 
	EL.BatesStart, 
	EL.BatesEnd, 
	EL.ExactLanguage, 
	EL.LanguageNotes, 
	LOC.Location, 
	DetailedLocation,
	EL.EndorsementNum,
	coalesce(EL.EffectiveDate, min(pstartdate)) as effectivedate,
	f.FormNumber,
	LOC.LocationID
FROM 
	dbo.tblProvisionLanguage PRVL 
	INNER JOIN tblExactLanguage el on el.ExactLanguageID = PRVL.ExactLanguageID
	LEFT JOIN dbo.tblProvisionLabel PL ON PRVL.ProvisionLabelID = PL.ProvisionLabelID
	INNER JOIN tblprovision psn on psn.policyid = EL.policyid
	LEFT JOIN tblDetailedLocation dl ON dl.DetailedLocationID = el.DetailedLocationID
	INNER JOIN tblprovisionvalue pvv on pvv.provisionlabelid = pl.provisionlabelid and pvv.provisionvalueid = psn.provisionvalueid
	LEFT JOIN dbo.tblLocation LOC ON EL.LocationID = LOC.LocationID
	inner join tblPolicyDates pd on pd.PolicyID=el.PolicyID
	left join tblForms f on f.FormID = el.formid
WHERE EL.policyid = @policyID
group by EL.PolicyID, 
	PL.ProvisionLabelID, 
	PL.Provision, 
	PVV.Provisionvalue,
	EL.ExactLanguageID, 
	EL.BatesStart, 
	EL.BatesEnd, 
	EL.ExactLanguage, 
	EL.LanguageNotes, 
	LOC.Location, 
	DetailedLocation,
	EL.EndorsementNum,
	EL.EffectiveDate,
	f.FormNumber,
	LOC.LocationID

--SELECT PolicyID,  ProvisionNotes,location,batesstart,batesend, langtype,endorsementnum,Provisionlabelid,provisionvalue,
--CASE 

--	WHEN  ExactLanguage <> 'No Policy Language Found.' 
--		AND ExactLanguage <> 'No Relevant Policy Language Found.' 
--		AND ExactLanguage <> 'Policy Language Not Analyzed.'
--		THEN ExactLanguage --+ ' [' + location + ', effective ' + expr1 + ', at ABC-' + right(BatesStart,6) + ']' + '.'
--	WHEN ExactLanguage = 'No Policy Language Found.' or ExactLanguage = 'No Relevant Policy Language Found.'
--		or ExactLanguage = 'Policy Language Not Analyzed.'
--		THEN ExactLanguage 
--	WHEN rtrim(ltrim(ExactLanguage)) is null THEN 'Policy Language Not Analyzed.'

--END AS Language
--From @SRPTTable


--Group by PolicyID, ProvisionNotes,location,batesstart,batesend,langtype,endorsementnum, provisionlabelid,provisionvalue,
--CASE 

--	WHEN  ExactLanguage <> 'No Policy Language Found.' 
--		AND ExactLanguage <> 'No Relevant Policy Language Found.' 
--		AND ExactLanguage <> 'Policy Language Not Analyzed.'
--		THEN ExactLanguage-- + ' [' + location + ', effective ' + expr1 + ', at ABC-' + right(BatesStart,6) + ']' + '.'
--	WHEN ExactLanguage = 'No Policy Language Found.' or ExactLanguage = 'No Relevant Policy Language Found.'
--		or ExactLanguage = 'Policy Language Not Analyzed.'
--		THEN ExactLanguage 
--	WHEN rtrim(ltrim(ExactLanguage)) is null THEN 'Policy Language Not Analyzed.'
----'No Policy Language Found.'
--END

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptLanguageByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptLanguageByPolicy] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptLanguageByPolicy] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptLanguageByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptLanguageByPolicy] TO [power_user]
GO
