SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryrptNoticeCorrespondenceDefense]
(@CorrespondenceID int)

AS
SET NOCOUNT ON
SELECT cnd.correspondenceID, cnd.noticedefenseid, nd.NoticeDefense
FROM   dbo.tblCorrespondenceNoticeDefense cnd INNER JOIN dbo.tblNoticeDefense nd on
cnd.NoticeDefenseID = nd.noticedefenseid	
WHERE @CorrespondenceID = cnd.CorrespondenceID
ORDER BY cnd.correspondencenoticedefense

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNoticeCorrespondenceDefense] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNoticeCorrespondenceDefense] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptNoticeCorrespondenceDefense] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNoticeCorrespondenceDefense] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNoticeCorrespondenceDefense] TO [power_user]
GO
