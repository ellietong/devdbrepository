SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmLanguagebyProvision]
--(@provisionLabelID int = null, @carrierID int = null)
As

SET NOCOUNT ON

--DECLARE @maxProvisionLabelID int

--SELECT @maxProvisionLabelID  = MAX(ProvisionLabelID) FROM tblProvisionLabel

 
Select
CarrierName, policynum, Provision, ExactLanguage, c.CarrierID, pl.ProvisionLabelID, p.PolicyID, el.ExactLanguageID, year(minpstartdate) as PolYear 

from tblPolicy p
inner join tblCarrier c on c.CarrierID = p.CarrierID
inner join tblexactlanguage el on el.policyid = p.PolicyID
inner join tblProvisionLanguage pl on pl.ExactLanguageID=el.exactlanguageid
inner join tblProvisionLabel prl on prl.ProvisionLabelID = pl.ProvisionLabelID
order by provision, carriername, policyid
	--WHERE pr.ProvisionLabelID >= COALESCE(@provisionLabelID, 0) AND pr.ProvisionLabelID <= COALESCE(NULLIF(@provisionLabelID, 0), @maxProvisionLabelID)

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLanguagebyProvision] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLanguagebyProvision] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmLanguagebyProvision] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLanguagebyProvision] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLanguagebyProvision] TO [power_user]
GO
