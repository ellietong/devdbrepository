SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryAcknowledgeTenderedClaim]
(@Hash varbinary(50))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--DECLARE @Hash varbinary(50) = 0x2A011F5888F7E4FE8577114402BD89C3
	DECLARE @retVal int
	IF(SELECT Acknowledged FROM tblTenderAcknowledgement WHERE [Hash] = @Hash) = 1
	BEGIN
		SET @retVal = -1
	
	END
	ELSE IF(SELECT Acknowledged FROM tblTenderAcknowledgement WHERE [Hash] = @Hash)=0
	BEGIN
		UPDATE tblTenderAcknowledgement
		SET Acknowledged = 1,
			AcknowledgementDate = GETDATE()
		WHERE Hash = @Hash
		
		SET @retVal = 0
	END
	ELSE
	BEGIN
		SET @retVal = -2
	
	END
	SELECT @retVal	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAcknowledgeTenderedClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAcknowledgeTenderedClaim] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryAcknowledgeTenderedClaim] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAcknowledgeTenderedClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAcknowledgeTenderedClaim] TO [power_user]
GO
