SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_TENDER_qryTenderHistoryByClaimID]
(@ClaimID bigint, @emailaddress nvarchar(250))
AS

SET NOCOUNT ON;

--Declare @ClaimID bigint
--Set @ClaimID=46511
--Declare @emailaddress nvarchar(250)
--Set @emailaddress= 'William.Berner@msasafety.com'


DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE cth.ClaimID =' + CONVERT(nvarchar(20), @ClaimID)


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR @DefenseCounselAddressID IN (245, 246, 250, 254))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ''
	SET @WhereString = @WhereString + ' AND th.ApprovedBy <> ''Historical Tender'''
END	

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT th.TenderHistoryID, 
		TenderBlockName, 
		th.TenderBlockID, 
		TenderStatus, 
		EffectiveDate, 
		d.DocumentURL, 
		cth.TenderStatusID, 
		th.DocumentID, 
		DocumentURL,
		th.DateApproved, 
		th.ApprovedBy, 
		ServiceDate, 
		TenderType, 
		th.DateCreated,
		AmazonKey
	FROM tblTenderHistory th 
		INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
		INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderStatus ts ON cth.TenderStatusID = ts.TenderStatusID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblTenderType tt ON th.TenderTypeID = tt.TenderTypeID' + 
	@WhereString + '
	ORDER BY th.DateCreated DESC, TenderType
	OPTION (RECOMPILE)'
	
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTenderHistoryByClaimID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTenderHistoryByClaimID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryTenderHistoryByClaimID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTenderHistoryByClaimID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTenderHistoryByClaimID] TO [power_user]
GO
