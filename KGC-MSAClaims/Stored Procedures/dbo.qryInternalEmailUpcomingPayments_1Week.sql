SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryInternalEmailUpcomingPayments_1Week]
AS
BEGIN


--/*Query run weekly to send a payment reminder for upcoming due dates. - ONB*/

--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

SET @Subject =
'MSA: Upcoming Settlement Due Dates for ' + CONVERT(varchar, GETDATE() + 7, 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--Create a temp table for Payments which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @UpcomingPaymentsTable TABLE (ClaimantPersonalInfoID bigint, Claimant nvarchar (500), SettlementAmount nvarchar (100), SettlementDueDate nvarchar (100))
INSERT INTO @UpcomingPaymentsTable
	SELECT
		cpi.ClaimantPersonalInfoID,
		'<a href="https://admin.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), cpi.ClaimantPersonalInfoID) + '&ClaimantTab=3&ResolutionTab=1' + '">' + LastName + ', ' + FirstName + COALESCE(' (' + CONVERT(nvarchar(50), NCCReferenceID) + ')', '') + '</a>' AS Claimant,
		'$' + CONVERT(nvarchar(12), SettlementAmount, 1),
		CONVERT(nvarchar(10), MONTH(SettlementDueDate)) + '/' + CONVERT(nvarchar(10), DAY(SettlementDueDate)) + '/' + CONVERT(nvarchar(10), YEAR(SettlementDueDate))
	FROM tblClaimantSettlement cs
	INNER JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblClaim c ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	WHERE SettlementDueDate IS NOT NULL
	AND PaidDate IS NULL
	AND CONVERT(int, SettlementDueDate - CONVERT(date, GETDATE())) = 7

--Declare and populate the body of the email for NCC, Gleason, & ReedSmith
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {
border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;
}

TH {
border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);
}

TD {	
font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
}


--->
</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'


--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @BodyMessage + '<H4>The following payment is due in one week: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +

N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">ClaimantID</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Amount</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Due Date</th>' +

CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		ClaimantPersonalInfoID)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		Claimant)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		SettlementAmount)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		SettlementDueDate)
	AS td,
	''
FROM @UpcomingPaymentsTable
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +

N'</table>' + N'<br/>'


--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


DECLARE @RecipientList nvarchar (500)
SET @RecipientList = 'blairo@kcic.com'

IF ((SELECT
	COUNT(ClaimantPersonalInfoID)
FROM @UpcomingPaymentsTable) > 0)
BEGIN

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail --The Claims sends from msa@kcic.com
@profile_name = 'Claims',
@recipients = 'msa@kcic.com',
--@blind_copy_recipients = '',
--@blind_copy_recipients = 'msa@kcic.com',
--@recipients = 'amanp@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

END


END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailUpcomingPayments_1Week] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailUpcomingPayments_1Week] TO [base_user]
GRANT ALTER ON  [dbo].[qryInternalEmailUpcomingPayments_1Week] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailUpcomingPayments_1Week] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailUpcomingPayments_1Week] TO [power_user]
GO
