SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qrySuffixList]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT SuffixID, UPPER(Suffix) AS Suffix
FROM tblClaimantSuffix
UNION 
SELECT NULL, ''
ORDER BY Suffix

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySuffixList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySuffixList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qrySuffixList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySuffixList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySuffixList] TO [power_user]
GO
