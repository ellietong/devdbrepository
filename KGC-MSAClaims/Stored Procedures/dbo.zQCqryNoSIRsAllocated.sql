SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[zQCqryNoSIRsAllocated]
(@allocationid int)
As

SET NOCOUNT ON

SELECT a.AllocationID, SUM(ar.SIRAllocated) AS SumOfSIRAllocated, SUM(ar.Allocation) AS SumOfAllocation, p.CarrierID
FROM tblPolicy p INNER JOIN (tblAllocation a INNER JOIN tblAllocationResults ar ON a.AllocationID = ar.AllocationID) ON p.PolicyID = ar.PolicyID
WHERE a.AllocationID LIKE COALESCE(@allocationID, 0)
GROUP BY a.AllocationID, p.CarrierID
HAVING SUM(ar.SIRAllocated) > 0

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[zQCqryNoSIRsAllocated] TO [base_user]
GRANT EXECUTE ON  [dbo].[zQCqryNoSIRsAllocated] TO [base_user]
GRANT ALTER ON  [dbo].[zQCqryNoSIRsAllocated] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryNoSIRsAllocated] TO [power_user]
GRANT EXECUTE ON  [dbo].[zQCqryNoSIRsAllocated] TO [power_user]
GO
