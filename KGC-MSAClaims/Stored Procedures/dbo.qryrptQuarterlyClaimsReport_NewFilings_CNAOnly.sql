SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptQuarterlyClaimsReport_NewFilings_CNAOnly] (@Quarter int, @Year varchar(max), @NextYear varchar(max)) 
AS
SET NOCOUNT ON


--Declare @Quarter int
--Set @Quarter = 4
--Declare @Year nvarchar(25)
--Set @Year = '2014'
--Declare @NextYear nvarchar(25)
--Set @NextYear = '2015'


--get last quarter start date
DECLARE @PreviousQuarterStart varchar(25)
SET @PreviousQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '1/1/' + @Year
		 WHEN @Quarter = 2 THEN '4/1/' + @Year
		 WHEN @Quarter = 3 THEN '7/1/' + @Year
		 WHEN @Quarter = 4 THEN '10/1/' + @Year
		 ELSE '1/1/2050' END

--get first day of next quarter
DECLARE @NextQuarterStart varchar(25)
SET @NextQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '4/1/' + @Year
		 WHEN @Quarter = 2 THEN '7/1/' + @Year
		 WHEN @Quarter = 3 THEN '10/1/' + @Year
		 WHEN @Quarter = 4 THEN '1/1/' + @NextYear
		 ELSE '12/31/2050' END
		 
		 
	SELECT CASE WHEN COALESCE(dbo.fnEarliestExposure(C.ClaimantPersonalInfoID), '1/1/1900') <= '4/1/1985' THEN 'Yes' ELSE 'No' END AS Pre1985Designate,
		   COALESCE(FullNameState, 'Unknown') AS FullNameState,
		   CauseAgent AS Material,
		   NCCReferenceID, 
		   LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,'') as Claimant,
		   CaseCaption,
		   DocketNumber,
		   ClaimLawsuitStatus,
		   Convert(varchar, FileDate, 101) as FileDate,
		   Convert(varchar, ServiceDate, 101) as ServiceDate,
		   Convert(varchar, ActiveTrialDate, 101) as ActiveTrialDate,
		   Convert(varchar, ProcessedDate, 101) as ProcessedDate, StatusNotes
	FROM dbo.tblClaimantPersonalInfo cpi 
		  LEFT JOIN dbo.tblClaim(NOLOCK) c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN dbo.tblClaimLawsuit(NOLOCK) cl ON cl.claimID = c.claimID
		  LEFT JOIN dbo.tblLawsuit(NOLOCK) l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking(NOLOCK) clst
		  on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  AND ClaimLawsuitStatusID=22
		  LEFT JOIN dbo.tblClaimLawsuitStatus(NOLOCK) cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN dbo.tblJurisdiction(NOLOCK) j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN dbo.tblState(NOLOCK) s on s.StateID = l.StateID
		  LEFT JOIN dbo.tblAllegation(NOLOCK) a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN dbo.tblMaterial(NOLOCK) m on m.MaterialID = a.MaterialID
		--  LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
	WHERE CASE WHEN (
				CASE WHEN cls.ClaimLawsuitStatusGroupID in (1,4) AND COALESCE(ProcessedDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END = 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID in (1,4) AND COALESCE(ProcessedDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1 OR
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 3 AND COALESCE(ServiceDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END= 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 3 AND COALESCE(ServiceDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1)
		  THEN 1 ELSE 0 END = 1
		  AND m.MaterialID != 10
  		  AND C.InsurableFlag =1
		  AND cls.ClaimLawsuitStatusGroupID in (1,3,4) 
		  AND TortTypeID = 1
		  AND c.ClaimID != 26234
		  AND CL.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID = 38 AND IsPrimaryStatus = 1)
		  --AND CL.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID = 29)
		  --AND DocketNumber NOT LIKE '%330307728B%'
	GROUP BY CASE WHEN COALESCE(dbo.fnEarliestExposure(C.ClaimantPersonalInfoID), '1/1/1900') <= '4/1/1985' THEN 'Yes' ELSE 'No' END, 
		   FullNameState,
		   CauseAgent,
		   NCCReferenceID, 
		   LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,''),
		   CaseCaption,
		   DocketNumber,
		   ClaimLawsuitStatus,
		   Convert(varchar, FileDate, 101),
		   Convert(varchar, ServiceDate, 101),
		   Convert(varchar, ActiveTrialDate, 101),
		   Convert(varchar, ProcessedDate, 101), StatusNotes
	ORDER BY FullNameState, CauseAgent, ProcessedDate, LastName + ', ' + COALESCE(FirstName, '') + ' ' + COALESCE(MiddleName,'')

RETURN


--USE [KGC-MSAClaims]
--GO
/****** Object:  StoredProcedure [dbo].[qryrptQuarterlyClaimsReport_NewSettlements_CNAOnly]    Script Date: 1/30/2015 2:35:29 PM ******/
SET ANSI_NULLS ON
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_NewFilings_CNAOnly] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_NewFilings_CNAOnly] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptQuarterlyClaimsReport_NewFilings_CNAOnly] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_NewFilings_CNAOnly] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_NewFilings_CNAOnly] TO [power_user]
GO
