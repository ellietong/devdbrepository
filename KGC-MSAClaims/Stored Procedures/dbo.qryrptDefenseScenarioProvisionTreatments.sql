SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptDefenseScenarioProvisionTreatments] (@ScenarioID int)
As
SET NOCOUNT ON

--DECLARE @ScenarioID int
--SET @ScenarioID = null

DECLARE @maxScenarioID int
SELECT @maxScenarioID = MAX (ScenarioID) from tblScenario
-------------------------------------------------------------------------------
--Defense Scenario, Combo, & Treatment
-------------------------------------------------------------------------------
SELECT Scenario, ct.ScenarioID, c.Combination, Provision, ProvisionValue, Treatment
FROM tblCombinationTreatment ct 
LEFT JOIN tblScenario s ON s.ScenarioID = ct.ScenarioID
LEFT JOIN tblCombination c ON ct.Combination = c.Combination
LEFT JOIN tblProvisionLabel pl ON c.ProvisionLabelID = pl.ProvisionLabelID
LEFT JOIN tblProvisionValue pv ON c.ProvisionValueID = pv.ProvisionValueID
LEFT JOIN tblTreatment t ON ct.TreatmentID = t.TreatmentID
WHERE s.ScenarioID >= COALESCE(@ScenarioID, 0) AND s.ScenarioID <= COALESCE(NULLIF(@ScenarioID, 0), @maxScenarioID)
GROUP BY Scenario, ct.ScenarioID, c.Combination, Provision, ProvisionValue, Treatment
ORDER BY Scenario, ct.ScenarioID, c.Combination, Provision, ProvisionValue, Treatment

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseScenarioProvisionTreatments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseScenarioProvisionTreatments] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptDefenseScenarioProvisionTreatments] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseScenarioProvisionTreatments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseScenarioProvisionTreatments] TO [power_user]
GO
