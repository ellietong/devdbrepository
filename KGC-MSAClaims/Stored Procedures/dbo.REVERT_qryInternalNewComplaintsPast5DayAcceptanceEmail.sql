SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_qryInternalNewComplaintsPast5DayAcceptanceEmail]
AS
BEGIN

/*Query run daily to send an internal email listing any Complaints which are over 5
days old and have not yet been accepted. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(max)

--1-30-12 - CAS modified to query data QC'ed between yesterday and current day at 4 PM so that query goes off those time stamps and not the current system date
--which could cause some notifications to be lost.
Declare @Yesterday4PM datetime
SET @Yesterday4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-5))+' 16:00:00.000')

SET @Subject =    'Marley-Wylain: Unaccepted Complaints Over 5 Days Old'
--Set body font, insert KCIC logo/banner at the top of the email, and set table styles. 
--These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>The following firms have unaccepted complaints are over 5 days old: </H4>
<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

--Create temp table to hold all complaints over 5 days old which have not been accepted
--DocTitle and firmAbb are found by parsing the documentURL
DECLARE @NewComplaintsTable TABLE (docTitle nvarchar(200), FirmAddressID int, lName nvarchar(50), fname nvarchar(200))
INSERT INTO @NewComplaintsTable
SELECT DocumentTitle,DefenseCounselAddressID, LastName, FirstName      
FROM tblDocuments d
INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
left join tblclaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblclaimlawsuit cl on cl.claimid = c.claimid	
left join tbllawsuit l on l.lawsuitid =cl.lawsuitid
WHERE mcd.QCDate < @Yesterday4PM
      AND detailDocumentTypeID in (8,4,33)
      AND Accepted IS NULL

--Declare variables to be used in the firm cursor to list documents by firm
DECLARE @FirmAddressID nvarchar(10), @firmURL nvarchar(100), @firmname nvarchar(100), @docCount int
SET @firmURL = 'https://admin.kcicextranet.com/clients/marleywylain/cp/Pages/Documents.aspx'
DECLARE firm_Curs CURSOR FOR

SELECT nct.FirmAddressID, dca.DefenseCounsel + ' (' + dca.DefenseCounselCity + ')', COUNT(DISTINCT doctitle) 
FROM tblDefenseCounselAddress dca
INNER JOIN @NewComplaintsTable nct ON nct.FirmAddressID = dca.DefenseCounselAddressID
GROUP BY nct.FirmAddressID,  dca.DefenseCounsel + ' (' + dca.DefenseCounselCity + ')'

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmname, @docCount

WHILE @@FETCH_STATUS = 0

BEGIN
      --Header for each firm with link to the document library
      SET @BodyMessage = @BodyMessage + '<br>'+ CONVERT(nvarchar(5), (SELECT @docCount))+ ' Unaccepted Complaint(s) for <A HREF = ' + (select @firmURL)+'> ' + (select @firmName) +'</A>:'
      --List documents in a table               
      SET @BodyMessage = @bodymessage + 
            N'<table>' + 
            --Set column headings and populate cells from the cursor
            N'<tr><th>Document Title</th><th>Claimant Last Name</th><th>Claimant First Name</th>' +
            CAST ( (
                  SELECT  td = docTitle, '', td = lname, '', td = fname, ''
                  FROM @NewComplaintsTable C
                  Where  C.FirmAddressID = (select @FirmAddressID)
                  order by lname
                  for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
            N'</table>' +  N'<br/>' 

--Move to next firm                 
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmname, @doccount
END
CLOSE firm_curs
DEALLOCATE firm_curs

SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person respoDemo Clientble for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'
--Check if the email needs to be sent
IF (SELECT COUNT(DISTINCT docTitle)
FROM @NewComplaintsTable)<>0 

BEGIN
      --Send email
      EXEC msdb.dbo.sp_send_dbmail
      @profile_name='Admin',
      @recipients = 'mwc@kcicllc.com',
      @copy_recipients = '',--'gearyj@kcicllc.com; selfL@kcicllc.com; shockleym@kcicllc.com; johnn@kcicllc.com; montgomeryi@kcicllc.com; dayc@kcicllc.com',
      @subject= @subject,
      @body= @BodyMessage,
      @body_format = 'html'
END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalNewComplaintsPast5DayAcceptanceEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalNewComplaintsPast5DayAcceptanceEmail] TO [base_user]
GRANT ALTER ON  [dbo].[REVERT_qryInternalNewComplaintsPast5DayAcceptanceEmail] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalNewComplaintsPast5DayAcceptanceEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalNewComplaintsPast5DayAcceptanceEmail] TO [power_user]
GO
