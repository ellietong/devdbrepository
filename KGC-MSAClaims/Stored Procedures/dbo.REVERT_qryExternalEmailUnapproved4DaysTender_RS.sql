SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_qryExternalEmailUnapproved4DaysTender_RS] 
AS
BEGIN


/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)

SET @Subject =    'MSA: Claims to Tender Over 4 Days Old'
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
DECLARE @TenderTable TABLE (ClaimantName nvarchar(250), ClaimantPersonalInfoID bigint, TenderBlockName nvarchar(250), TenderType nvarchar(250), DateCreated datetime)
INSERT INTO @TenderTable
--Reed Smith Outstanding tenders (per tender type)
SELECT LastName + ', ' + FirstName + ' (' + NCCReferenceID + ')' AS ClaimantName, c.ClaimantPersonalInfoID, 
TenderBlockName, TenderType, DateCreated
FROM tblTenderHistory th
INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
INNER JOIN tblTenderType tt ON th.TenderTypeID = tt.TenderTypeID
INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE DateCreated < GETDATE()-4
AND DateApproved IS NULL
AND th.TenderBlockID <> 18
ORDER BY DateCreated ASC


--Declare and populate the body of the email for each tender type
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH    {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
UL {margin:0; 
	padding:0;}	      
--->
</STYLE><H4>The following claims require tendering: </H4>'




--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
IF (Select COUNT(ClaimantName) from @TenderTable) > 0 
begin
--set @BodyMessage = @BodyMessage + '<H2>' + @TenderType + '</H2>'
SET @BodyMessage = @BodyMessage + N'<table>' + 
		N'<tr><th>Claimant Name (NCC #)</th><th>Tender Block</th><th>Date Created</th>' +
		CAST ( (
			SELECT td = '<a href="https://www.kcicextranet.com/clients/msa/cp/pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, ClaimantPersonalInfoID) + '&ClaimantTab=1&ClaimTab=4'  + '">' + ClaimantName + '</a>', '', td = TenderBlockName, '', td = CONVERT(nvarchar, COALESCE(DateCreated, '1/1/1900'), 101), ''
			FROM @TenderTable 
			--WHERE TenderType = (select @TenderType)
			
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
		N'</table>' +  N'<br/>'		
end
ELSE
begin
Set @BodyMessage = @BodyMessage + '<H4>There are currently no outstanding claims that require tendering. </H4>'
end



--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')




----Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

  
DECLARE @RecipientList nvarchar(500)
SET @RecipientList=''

----Use the complaintContact field to determine the contact list.
----Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar(500)
DECLARE contact_curs CURSOR FOR

SELECT email 
FROM vtblPerson p
WHERE FirmID = 31 and complaintcontact = 1
      
OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS=0
BEGIN 

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--set @BodyMessage = @BodyMessage + '<br><br>Recipients: ' + @RecipientList

if(select count(TenderBlockName) from @TenderTable)  <> 0 
begin 
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The marleywylainProfile sends from marleywylain@kcicllc.com
@profile_name='MSAProfile',
@recipients = @RecipientList,-- 'hubbardb@kcicllc.com', --
@copy_recipients = 'msa@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
end

END



GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailUnapproved4DaysTender_RS] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailUnapproved4DaysTender_RS] TO [base_user]
GRANT ALTER ON  [dbo].[REVERT_qryExternalEmailUnapproved4DaysTender_RS] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailUnapproved4DaysTender_RS] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailUnapproved4DaysTender_RS] TO [power_user]
GO
