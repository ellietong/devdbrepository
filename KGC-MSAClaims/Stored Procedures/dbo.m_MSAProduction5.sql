SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_MSAProduction5]
	
AS
BEGIN
	select d.DocumentID,d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL			
from tblMultiClaimantDocument mcd			
inner join tblDocuments d on mcd.DocumentID = d.DocumentID			
inner join tblSingleClaimantDocumentType sc on sc.ClaimantDocumentTypeID = d.DetailDocumentTypeID			
inner join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = sc.SingleClaimantDocumentTypeID			
inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID			
inner join tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID			
where cdt.ClaimantDocumentTypeID in (8,13,14,15,22,31,59) and  
NCCReferenceID in ('NCC00044802',
'NCC00043999',
'NCC00044010',
'NCC00044772',
'NCC09000186',
'NCC00043790',
'NCC00044996',
'NCC00043785',
'NCC00045868',
'NCC00045066',
'NCC09000011',
'NCC00040725',
'NCC00027698',
'NCC00043951',
'NCC00044166',
'NCC00045322',
'NCC09000018',
'NCC09000001',
'NCC00027369',
'NCC00044372',
'NCC00032695',
'NCC00033045',
'NCC00040829',
'NCC00044752',
'NCC11000079',
'NCC10000288',
'NCC10000017',
'NCC09000151',
'NCC00044648',
'NCC00044357'
)	
AND d.DocumentID = 385335
group by d.DocumentID,  d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL				
order by d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[m_MSAProduction5] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_MSAProduction5] TO [base_user]
GRANT ALTER ON  [dbo].[m_MSAProduction5] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_MSAProduction5] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_MSAProduction5] TO [power_user]
GO
