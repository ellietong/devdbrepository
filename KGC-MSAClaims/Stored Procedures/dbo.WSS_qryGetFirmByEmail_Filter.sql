SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryGetFirmByEmail_Filter]
	-- Add the parameters for the stored procedure here
	@emailaddress nvarchar(1000)
AS
--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'daaron@smsm.com'

DECLARE @firmID int
SET @firmID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)


DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
								
BEGIN
	IF RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1
	BEGIN
		SELECT DISTINCT DefenseCounselID AS FirmID, DefenseCounsel as Firm 
		FROM tblDefenseCounsel dc
		INNER JOIN vtblPerson p ON dc.DefenseCounselID = p.FirmID
		WHERE COALESCE(p.InsurerRole, 0) = 0
		UNION 
		SELECT NULL, '<< All Firms >>'
		ORDER BY DefenseCounsel
	END
	ELSE
	BEGIN
		Select dca.DefenseCounselID As FirmID, dca.DefenseCounsel as Firm
		From  dbo.tblDefenseCounselAddress dca
		Where dca.DefenseCounselID = @FirmID
		UNION 
		SELECT NULL, '<< All Firms >>'
		ORDER BY dca.DefenseCounsel
	END
		
	 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetFirmByEmail_Filter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetFirmByEmail_Filter] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetFirmByEmail_Filter] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetFirmByEmail_Filter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetFirmByEmail_Filter] TO [power_user]
GO
