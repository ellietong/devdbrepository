SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryTenderRecipients]
(@TenderHistoryID bigint)
AS

--SELECT * FROM tblTenderHistory ORDER BY TenderHistoryID DESC
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--DECLARE @TenderHistoryID int
	--SET @TenderHistoryID = 172994
    -- Insert statements for procedure here
    DECLARE @AmazonKey nvarchar(MAX)
    DECLARE @CPILastName nvarchar(100)
    
    SET @AmazonKey = (SELECT d.AmazonKey	
		FROM tblTenderHistory th
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		WHERE TenderHistoryID = @TenderHistoryID)
		
	--Get claimant name list (up to 3 names then  "et al." added to end		
	DECLARE @CPINames TABLE(Names nvarchar(max))
	INSERT INTO @CPINames
	SELECT LEFT(dl.list, LEN(dl.List)-1) + Case WHEN COUNT(c.ClaimantPersonalInfoID) > 3 THEN ' et al.' ELSE '' END
		FROM tblTenderHistory th
		INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
		LEFT JOIN tblClaim c ON cth.ClaimID = c.ClaimID
		LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		
				CROSS APPLY
				(
					SELECT TOP 3
					CONVERT(nvarchar, COALESCE(LastName + ', ', '') + COALESCE(LEFT(FirstName, 1)+'.', '')) + ', ' AS [text()] 
					FROM 
						tblClaimantPersonalInfo cpi1
					INNER JOIN tblClaim claim ON cpi1.ClaimantPersonalInfoID = claim.ClaimantPersonalInfoID
					INNER JOIN tblClaimTenderHistory cth1 ON claim.ClaimID = cth1.ClaimID
					WHERE cth1.TenderHistoryID = th.TenderHistoryID    
					ORDER BY 
						LastName 
					FOR XML PATH('') 
				) dl (list)
			WHERE th.TenderHistoryID = @TenderHistoryID
			GROUP BY LEFT(dl.list, LEN(dl.List)-1) 
	SET @CPILastName = (SELECT TOP 1 Names FROM @CPINames)

	SELECT tc.Email, tc.[To], tc.CC, tc.BCC, @CPILastName AS LastName, AmazonKey, LName + ', ' + FName AS ContactName, COALESCE(Hash,'') AS Hash
	FROM tblTenderHistory th
	INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
	INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
	INNER JOIN tblTenderBlockPolicy tbp ON tb.TenderBlockID = tbp.TenderBlockID
	INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
	LEFT JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
	LEFT JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID  AND tc.TenderContactID = ta.TenderContactID
	LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
	LEFT JOIN tblClaim c ON cth.ClaimID = c.ClaimID
	LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	WHERE tc.CurrentContact = 1 
		AND (th.TenderHistoryID = @TenderHistoryID) --People for that specific tender block, or always CC
	UNION
	--Add in people who should always be BCC'd (not related to carriers)
	SELECT Email, 0, 0, AlwaysBCC, @CPILastName, @AmazonKey, LName + ', ' + FName AS ContactName, ''
	FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysBCC = 1
	UNION
	--Add in people who should always be CC'd (not related to carriers)
	SELECT Email, 0, AlwaysCC, 0, @CPILastName, @AmazonKey, LName + ', ' + FName AS ContactName, ''
	FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
	--Union on NCC contact depending on material type
	--UNION
	--SELECT email, 0, 1, 0, @CPILastName, @AmazonKey, LName + ', ' + FName AS ContactName, ''
	--FROM tblTenderHistory th
	--INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
	--INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
	--INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	--INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	--INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
	--INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
	--WHERE th.TenderHistoryID = @TenderHistoryID
	
	--UNION
	--Add in defense counsel tender contact CCs
	--SELECT p.Email, 0, 1, 0, @CPILastName, @AmazonKey, p.LName + ', ' + p.FName AS ContactName, ''
	--FROM tblTenderHistory th
	--INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
	--INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	--INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	--INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
	--WHERE p.TendercontactCC = 1 AND c.ClaimID = (SELECT ClaimID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
	--UNION
	----Add in defense counsel tender contact BCCs
	--SELECT p.Email, 0, 0, 1, @CPILastName, @AmazonKey, p.LName + ', ' + p.FName AS ContactName, ''
	--FROM tblTenderHistory th
	--INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
	--INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	--INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	--INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
	--WHERE p.TendercontactBCC = 1 AND c.ClaimID = (SELECT ClaimID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTenderRecipients] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTenderRecipients] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryTenderRecipients] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTenderRecipients] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTenderRecipients] TO [power_user]
GO
