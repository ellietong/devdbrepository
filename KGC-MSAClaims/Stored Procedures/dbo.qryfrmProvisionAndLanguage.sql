SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmProvisionAndLanguage]
(@policyID int = Null)
AS

SELECT *
FROM tblProvision
WHERE PolicyID = @policyID
ORDER BY ProvisionLabelID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionAndLanguage] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionAndLanguage] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmProvisionAndLanguage] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionAndLanguage] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionAndLanguage] TO [power_user]
GO
