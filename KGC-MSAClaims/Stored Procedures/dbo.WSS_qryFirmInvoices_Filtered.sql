SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROCEDURE [dbo].[WSS_qryFirmInvoices_Filtered](@emailaddress nvarchar (1000)=null, @FilterStatusID int, @FilterDocumentTypeID int, @FilterFirmID int, @FilterVendorFirmID int)
AS
--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'vicki.stringham@danacollc.com'
--DECLARE @FilterStatusID int
--DECLARE @FilterDocumentTypeID int
--DECLARE @FilterFirmID int
 
--SET @FilterStatusID = null
--SET @FilterDocumentTypeID = null
--SET @FilterFirmID = 38
 
DECLARE @firmid int
SET @firmid = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)
 
DECLARE @MaxStatusID int
SET @MaxStatusID = (SELECT MAX(StatusID) FROM tblDefenseInvoiceStatus)
 
DECLARE @MaxDocType int
SET @MaxDocType = (SELECT MAX(ClaimantDocumentTypeID) FROM tblClaimantDocumentType)
 
DECLARE @MaxFirmID int
SET @MaxFirmID = (SELECT MAX(DefenseCounselID) FROM tblDefenseCounsel)
 
DECLARE @MaxVendorFirmID int
SET @MaxVendorFirmID = (SELECT MAX(VendorFirmID) FROM m_tblVendorFirm)
 
IF ((@firmid in(188)) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com')) OR (Right(@EmailAddress, 8) = 'kcic.com')
BEGIN
	SELECT CAST(d.DocumentID as varchar(50)) as DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		'' AS DefenseInvoiceFirm, 				
		di.DatePaid, 
		di.CheckNumber,
		d.FirmID,
		COALESCE(VendorFirm, '') AS VendorFirm,
		SourceDate,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) + COALESCE(d.Adjustment, 0)) AS InvoiceTotal
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		--LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.VendorFirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
		LEFT OUTER JOIN m_tblVendorFirm ex ON d.VendorFirmID = ex.VendorFirmID
		LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
	WHERE dt.DocumentGroupID = 2
	AND ClaimantDocumentTypeID >= COALESCE(@FilterDocumentTypeID, 0) AND ClaimantDocumentTypeID <= COALESCE(@FilterDocumentTypeID, @maxDocType)
		AND d.FirmID >= COALESCE(@FilterFirmID, 0) AND d.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
		AND COALESCE(d.StatusID, 0) >= COALESCE(@FilterStatusID, 0) AND d.StatusID <= COALESCE(@FilterStatusID, @MaxStatusID)
		AND COALESCE(d.VendorFirmID, 0) >= COALESCE(@FilterVendorFirmID, 0) AND COALESCE(d.VendorFirmID, 0) <= COALESCE(@FilterVendorFirmID, @maxVendorFirmID)
		and ((coalesce(d.statusid,0)!=5) or (coalesce(di.StatusID,0)!=5))
	ORDER BY Accepted ASC, DateCreated DESC
END 
ELSE BEGIN
	SELECT 
		cast(d.DocumentID as varchar(50)) as DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		'' AS DefenseInvoiceFirm, 				
		di.DatePaid, 
		di.CheckNumber,
		d.FirmID,
		COALESCE(VendorFirm, '') AS VendorFirm,
		SourceDate,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) + COALESCE(d.Adjustment, 0)) AS InvoiceTotal
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		--LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.VendorFirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
		LEFT OUTER JOIN m_tblVendorFirm ex ON d.VendorFirmID = ex.VendorFirmID
		LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
	WHERE dg.DocumentGroupID = 2
		AND f.FirmID = @firmid
		AND ClaimantDocumentTypeID >= COALESCE(@FilterDocumentTypeID, 0) AND ClaimantDocumentTypeID <= COALESCE(@FilterDocumentTypeID, @maxDocType)
		AND d.FirmID >= COALESCE(@FilterFirmID, 0) AND d.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
		AND COALESCE(d.StatusID, 0) >= COALESCE(@FilterStatusID, 0) AND d.StatusID <= COALESCE(@FilterStatusID, @MaxStatusID)
		AND COALESCE(d.VendorFirmID, 0) >= COALESCE(@FilterVendorFirmID, 0) AND COALESCE(d.VendorFirmID, 0) <= COALESCE(@FilterVendorFirmID, @maxVendorFirmID)
		and ((coalesce(d.statusid,0)!=5) or (coalesce(di.StatusID,0)!=5))
	ORDER BY Accepted ASC, DateCreated DESC
END
 


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryFirmInvoices_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryFirmInvoices_Filtered] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryFirmInvoices_Filtered] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryFirmInvoices_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryFirmInvoices_Filtered] TO [power_user]
GO
