SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryWithdrawTenders]
(@ClaimIDs bigint_list_tbltype READONLY,  @emailaddress nvarchar(500))
AS
BEGIN
	SET NOCOUNT ON;

    --Test vars
    --DECLARE @emailaddress nvarchar(500) = 'crookc@kcicllc.com'
    --DECLARE @ClaimIDs bigint_list_tbltype
    --INSERT INTO @ClaimIDs
    --SELECT 6789

    --SELECT * FROm tblTenderHistory WHERE ClaimID_old = 6789
    --SELECT * FROM tblClaimTenderHistory WHERE ClaimID = 6789
  --BEGIN TRAN
  
  --Get relevant tender history records for each claimant
	DECLARE @ClaimID bigint
	DECLARE @WithdrawnTenderHistories TABLE (TenderHistoryID bigint, dat date)

	
	DECLARE tenderhistory_curs CURSOR FOR
	SELECT n FROM @ClaimIDs
	
	
	
	OPEN tenderhistory_curs
	FETCH NEXT FROM tenderhistory_curs INTO @ClaimID
	WHILE @@FETCH_STATUS=0
	BEGIN 

		INSERT INTO @WithdrawnTenderHistories
		SELECT DISTINCT TOP 1 th.TenderHistoryID, DateApproved
		FROM tblTenderHistory th
		INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		WHERE cth.ClaimID = @ClaimID AND cth.TenderStatusID IN (2, 5)
		ORDER BY DateApproved DESC
 

	FETCH NEXT FROM tenderhistory_curs INTO @ClaimID
	END
	CLOSE tenderhistory_curs
	DEALLOCATE tenderhistory_curs


	--Create tblTenderHistory & tblClaimTenderHistory records for actual withdrawals
	DECLARE @TenderBlockID int
	DECLARE withdrawal_curs CURSOR FOR
	SELECT DISTINCT th.TenderBlockID
	FROM tblTenderHistory th 
	INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
	INNER JOIN @WithdrawnTenderHistories wth ON th.TenderHistoryID = wth.TenderHistoryID
	
	OPEN withdrawal_curs
	FETCH NEXT FROM withdrawal_curs INTO @TenderBlockID
	WHILE @@FETCH_STATUS=0
	BEGIN 
		--INSERT tblTenderHistory record		
		INSERT INTO tblTenderHistory (TenderBlockID, TenderTypeID, DateCreated, SPUsername)
		SELECT @TenderBlockID, 2, GETDATE(), @Emailaddress
		
		DECLARE @WithdrawalTHID bigint = (SELECT IDENT_CURRENT('tblTenderHistory'))
		--SELECT @WithdrawalTHID
		
		--INSERT tblClaimTenderHistory records for new withdrawal
		INSERT INTO tblClaimTenderHistory (ClaimID, TenderHistoryID, TenderStatusID, SPUsername, PreviousClaimTenderHistoryID)
		SELECT cth.ClaimID, @WithdrawalTHID, 1, @emailaddress, pcth.ClaimTenderHistoryID
		FROM @WithdrawnTenderHistories wth 
		INNER JOIN tblTenderHistory th ON wth.TenderHistoryID = th.TenderHistoryID
		INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
		LEFT JOIN (	SELECT TOP 1 cth1.* FROM tblClaimTenderHistory cth1 INNER JOIN tblTenderHistory th1 ON cth1.TenderHistoryID = th1.TenderHistoryID 
					WHERE TenderStatusID IN (2,5) AND TenderTypeID IN (1,3) ORDER BY DateCreated DESC ) pcth ON cth.ClaimID = pcth.ClaimID AND wth.TenderHistoryID = pcth.TenderHistoryID
		WHERE th.TenderBlockID = @TenderBlockID 
		
		--If there is only 1 template for the newly inserted retender record, default the TemplateID to that value
		DECLARE @TenderTemplate TABLE(TenderTemplateID int, TemplateDisplayName nvarchar(MAX))
		INSERT INTO @TenderTemplate
		EXEC [WSS_TENDER_qryTenderTemplateList] @WithdrawalTHID
		IF(SELECT COUNT(COALESCE(TenderTemplateID, 0)) FROM @TenderTemplate) = 2
		BEGIN
			UPDATE tblTenderHistory
			SET TenderTemplateID = (SELECT TenderTemplateID FROM @TenderTemplate WHERE TenderTemplateID IS NOT NULL)
			WHERE TenderHistoryID = @WithdrawalTHID
		END	
				

	FETCH NEXT FROM withdrawal_curs INTO @TenderBlockID
	END
	CLOSE withdrawal_curs
	DEALLOCATE withdrawal_curs
	
	

	--Update the existing tender records to "withdrawn"  
	UPDATE tblClaimTenderHistory
	SET TenderStatusID = 3
	WHERE ClaimID IN (SELECT n FROM @ClaimIDs) AND TenderHistoryID IN (SELECT TenderHistoryID FROM @WithdrawnTenderHistories)
	AND TenderStatusID IN (2,5)

	--SELECT * FROM tblClaimTenderHistory WHERE ClaimID IN (SELECT n FROM @ClaimIDs)
	--ROLLBACK TRAN

  
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryWithdrawTenders] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryWithdrawTenders] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryWithdrawTenders] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryWithdrawTenders] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryWithdrawTenders] TO [power_user]
GO
