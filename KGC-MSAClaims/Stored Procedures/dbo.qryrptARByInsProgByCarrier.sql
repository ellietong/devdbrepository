SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  Procedure [dbo].[qryrptARByInsProgByCarrier]
(@allocationID int = null)
As

SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 274

DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation

DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
CarrierID int, CarrierName nvarchar(250), InsuranceProgramID int, InsuranceProgram nvarchar(50),
PolicyHolder bit)
INSERT INTO @Policy

SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, 
InsuranceProgramID, InsuranceProgram, PolicyHolder
FROM vPolicy
GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, 
InsuranceProgramID, InsuranceProgram, PolicyHolder

SELECT pinfo.InsuranceProgramID, pinfo.CarrierID, pinfo.CarrierGroupID, pinfo.PolicyHolder, MAX(ar.DateTime) AS [DateTime], pinfo.CarrierGroupName, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' ('+ pinfo.CarrierGroupName + ')' END as 'CarrierInfo', pinfo.CarrierName, pinfo.InsuranceProgram, 
	SUM(ar.Allocation) AS Allocation, SUM(ar.PV) AS SumOfPV, SUM(ar.SIRAllocated) AS SIRAllocated, SUM(ar.AllocationIndemnity) as AllocationIndemnity, SUM(ar.AllocationDefense) as AllocationDefense,
	SUM(ar.AllocationDefenseOutsideLimits) AS AllocationDefenseOutsideLimits, a.AllocationID, a.AllocationDescription
FROM @Policy pinfo INNER JOIN vAllocationResults ar ON pinfo.PolicydateID = ar.PolicydateID
	LEFT JOIN vAllocation a ON a.AllocationID = ar.AllocationID 
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID)
GROUP BY pinfo.InsuranceProgramID, pinfo.CarrierID, pinfo.CarrierGroupID, pinfo.PolicyHolder, pinfo.CarrierGroupName, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' ('+ pinfo.CarrierGroupName + ')' END, pinfo.CarrierName, pinfo.InsuranceProgram, a.AllocationID, a.AllocationDescription
ORDER BY pinfo.CarrierName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByInsProgByCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByInsProgByCarrier] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptARByInsProgByCarrier] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByInsProgByCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByInsProgByCarrier] TO [power_user]
GO
