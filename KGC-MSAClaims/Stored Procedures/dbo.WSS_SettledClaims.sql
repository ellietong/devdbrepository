SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_SettledClaims]
AS
SELECT     C.ClaimID, S.State, J.Jurisdiction, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      HLDDefense.HighLevelDisease AS Disease, 
                      CASE WHEN CL.ClaimLawsuitStatusID = 25 THEN 'SBND' WHEN CL.ClaimLawsuitStatusID = 28 THEN 'Zero Dollar' ELSE ClaimLawsuitStatus END AS ClaimLawsuitStatus,
                       CS.SettlementAmount, CONVERT(date, CS.SettlementDate) AS SettlementDate, CASE WHEN MIN(PaidDate) IS NOT NULL 
                      THEN 'Yes' ELSE 'No' END AS Paid, CONVERT(date, MIN(CS.PaidDate)) AS MinPaidDate, CONVERT(date, CS.PaymentDueDate) AS settlementduedate, 
                       pca.PlaintiffCounsel,
                      Case when PaidDate IS null then SettlementAmount else 0 end as UnpaidSettlement
FROM         dbo.tblClaimantPersonalInfo AS CPI LEFT OUTER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID LEFT OUTER JOIN
                      (SELECT * FROM tblClaimDisease WHERE PrimaryDisease=1) AS CD ON CD.ClaimID = C.ClaimID LEFT OUTER JOIN 
                      dbo.tblDisease AS DDefense ON CD.DiseaseID = DDefense.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS HLDDefense ON HLDDefense.HighLevelDiseaseID = DDefense.HighLevelDiseaseID LEFT OUTER JOIN
                      dbo.tblClaimSequence AS ClaimS ON ClaimS.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblClaimLawsuitStatus AS CLS ON CL.ClaimLawsuitStatusID = CLS.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS CS ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
                      LEFT JOIN dbo.tblplaintiffCounselAddress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
WHERE     (CL.ClaimLawsuitStatusID IN (23, 25, 28)) AND (CL.IsPrimaryForClaim = 1) AND (CS.SettlementDate >= '1/1/2001') AND 
                      (HLDDefense.HighLevelDiseaseID IN (1, 2, 3, 4, 5, 6, 7))
GROUP BY C.ClaimID, HLDDefense.HighLevelDisease,  S.State, J.Jurisdiction, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END, CS.SettlementDate, CLS.ClaimLawsuitStatus, CS.SettlementAmount, CS.PaidDate, CS.PaymentDueDate, 
                       pca.PlaintiffCounsel, CL.ClaimLawsuitStatusID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SettledClaims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SettledClaims] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SettledClaims] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SettledClaims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SettledClaims] TO [power_user]
GO
