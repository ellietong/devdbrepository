SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[REVERT_qryInternalInvoiceEmailUpdate]
AS
Begin
/*Query run daily to send an internal email listing any invoices which have been approved or rejected
and are ready to be QC'd. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(2000)

SET @Subject =    'Marley-Wylain: New Defense Invoices Approved/Rejected as of ' + CONVERT(varchar, GETDATE(), 107) 
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>Below are any newly approved/rejected invoices for review.</H4>'

--Declare firm counter
DECLARE @FirmCount int
SET @FirmCount = 0

--Create temp table for invoices with new status changes
DECLARE @NewInvoicesTable TABLE (spdocid nvarchar(100), FirmAddressID int)
INSERT INTO @NewInvoicesTable
SELECT SPDocID, l.DefenseCounselAddressID
FROM tblDocuments d
	  INNER JOIN tblDefenseInvoiceStatus s ON COALESCE(d.StatusID, 3) = s.statusid
      LEFT JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
      LEFT JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalinfoID
      LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
      LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
      LEFT JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID 
WHERE DateApproved BETWEEN GETDATE()-1 AND GETDATE() 
      AND detailDocumentTypeID IN (40, 41)
GROUP BY l.DefenseCounselAddressID, SPDocId

--Gather the new invoice information into a cursor object
DECLARE @invoicecount int
DECLARE @FirmAddressID int
DECLARE @firmName nvarchar(100)
DECLARE @firmInvoiceURL nvarchar(100)
set @firmInvoiceURL =  'https://admin.kcicextranet.com/clients/marleywylain/cp/Pages/Invoices.aspx'
DECLARE Invoice_Curs CURSOR FOR
SELECT count(spdocid), dca.DefenseCounselAddressID, DefenseCounsel + ' (' + DefenseCounselCity + ')'
FROM @NewInvoicesTable i
      INNER JOIN tblDefenseCounselAddress dca ON i.FirmAddressID = dca.DefenseCounselAddressID
GROUP BY dca.DefenseCounselAddressID, DefenseCounsel + ' (' + DefenseCounselCity + ')'

--Open the invoice cursor
OPEN Invoice_Curs

--Fetch the first record from the cursor
FETCH NEXT FROM Invoice_Curs INTO @Invoicecount, @FirmAddressID, @firmName

WHILE @@FETCH_STATUS = 0 BEGIN
      SET @FirmCount = @FirmCount + 1
      SET @BodyMessage = @BodyMessage  + CONVERT(nvarchar(10), (SELECT @invoicecount)) + ' New Invoices Were Approved or Rejected in the <A HREF=' +
            (select @firmInvoiceURL) + '>' + @firmName + '</a> Invoice Library.<br><br>'
      FETCH NEXT FROM Invoice_Curs INTO @Invoicecount, @FirmAddressID, @firmName
END

CLOSE Invoice_Curs
DEALLOCATE Invoice_Curs

SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person respoDemo Clientble for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'


--Only send email if there are new status updates.
IF (@FirmCount > 0)     
      EXEC msdb.dbo.sp_send_dbmail
      @profile_name='Admin',
      @recipients = 'mwc@kcicllc.com',
      @subject= @subject,
      @body= @BodyMessage,
      @body_format = 'html'

End
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalInvoiceEmailUpdate] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalInvoiceEmailUpdate] TO [base_user]
GRANT ALTER ON  [dbo].[REVERT_qryInternalInvoiceEmailUpdate] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalInvoiceEmailUpdate] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalInvoiceEmailUpdate] TO [power_user]
GO
