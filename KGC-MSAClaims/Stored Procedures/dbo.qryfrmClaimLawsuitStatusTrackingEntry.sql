SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Proc [dbo].[qryfrmClaimLawsuitStatusTrackingEntry] 
as

Select *
From tblClaimLawsuitStatusTracking
Order By StatusDate desc


Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitStatusTrackingEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitStatusTrackingEntry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitStatusTrackingEntry] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitStatusTrackingEntry] TO [Claims_User]
GRANT ALTER ON  [dbo].[qryfrmClaimLawsuitStatusTrackingEntry] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitStatusTrackingEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitStatusTrackingEntry] TO [power_user]
GO
