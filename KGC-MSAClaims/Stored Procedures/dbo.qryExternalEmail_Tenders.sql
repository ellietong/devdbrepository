SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmail_Tenders]
AS
BEGIN

--/*Query run daily to an update to all insurer tender contacts regarding new tenders.*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @LogoHeader nvarchar (2000)
DECLARE @Signature nvarchar (2000)

SET @Subject = 'Claims served on MSA on or about ' + CONVERT(nvarchar, GETDATE(), 101)

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
SET @Signature = '<img src="http://www.kcic.com/i/BillBernerSig.png" alt="logo" height="42" width="100"/>'

--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewTendersTable TABLE (CarrierGroupID int,
								ClaimantName nvarchar (50),
								DOFE date,
								ClaimID bigint,
								NCCReferenceID nvarchar (12),
								CaseCaption nvarchar (MAX),
								ServiceDate datetime,
								CauseAgent nvarchar (200),
								DefenseCounsel nvarchar (500),
								ClaimantPersonalInfoID bigint,
								TenderHistoryID bigint)
INSERT INTO @NewTendersTable
	SELECT tc.CarrierGroupID,
		LastName + ', ' + FirstName AS 'Claimant Name',
		DOFE,
		th.ClaimID,
		c.NCCReferenceID,
		CaseCaption,
		ServiceDate,
		CauseAgent,
		DefenseCounsel,
		c.ClaimantPersonalInfoID,
		Th.TenderHistoryID
	FROM tblTender_Acknowledgement ta
		INNER JOIN tblTender_History th ON ta.TenderHistoryID = th.TenderHistoryID
		INNER JOIN vTenderContacts tc ON ta.PersonID = tc.PersonID
		INNER JOIN [Ligado-ExtranetSecurity].dbo.vPerson p ON tc.PersonID = p.PersonID
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblMaterial m ON a.MaterialID = m.MaterialID
		INNER JOIN tblDocuments d ON th.DocumentID = d.DocumentID
	WHERE AcknowledgementDate IS NULL
		AND DateCreated >= (GETDATE() - 1)
		--NOTE: Below added by CY on 5/13/2014 - TEMPORARY exclusion of MSA contacts while issue is investigated
		AND tc.CarrierGroupID <> 999
		--NOTE: Below added by CY on 6/17/2014 per ticket #1287800 from M. Potter
		AND TortTypeID = 1
	GROUP BY tc.CarrierGroupID,
		LastName + ', ' + FirstName,
		DOFE,
		th.ClaimID,
		c.NCCReferenceID,
		CaseCaption,
		ServiceDate,
		CauseAgent,
		DefenseCounsel,
		c.ClaimantPersonalInfoID,
		th.TenderHistoryID

--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.
DECLARE @CarrierGroupID nvarchar (10)
DECLARE CarrierGroup_Curs CURSOR FOR 
	SELECT CarrierGroupID
	FROM @NewTendersTable ntt
	GROUP BY CarrierGroupID

OPEN CarrierGroup_Curs
FETCH NEXT FROM CarrierGroup_Curs INTO @CarrierGroupID

WHILE @@FETCH_STATUS = 0
BEGIN
	--Declare and populate the body of the email for individual firms
	DECLARE @BodyMessage nvarchar (MAX)

	--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
	--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
	SET @BodyMessage = @LogoHeader +
	'<STYLE TYPE="text/css">
			<!--
			TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
			TD    {font-family: Goudy Old Style; font-size: 12pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000}
			TH    {border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
			UL {margin:0; 
				padding:0;}	      
			--->
			</STYLE>'

	DECLARE @CarrierGroupName nvarchar (250)
	SET @CarrierGroupName = (SELECT CarrierGroupName
							 FROM tblCarrierGroup
							 WHERE CarrierGroupID = @CarrierGroupID)

	DECLARE @CarrierGroupCarrierList nvarchar (MAX)
	SET @CarrierGroupCarrierList = (SELECT CAST((SELECT CarrierName + ', '
												 FROM tblCarrier c
												     INNER JOIN tblPolicy p ON c.CarrierID = p.CarrierID
												 WHERE CarrierGroupID = @CarrierGroupID
												 GROUP BY CarrierName
												 FOR xml PATH ('')) AS text))
	SET @CarrierGroupCarrierList = LEFT(@CarrierGroupCarrierList, LEN(@CarrierGroupCarrierList) - 1)
	SET @CarrierGroupCarrierList = COALESCE(@CarrierGroupCarrierList, 'MSA')
	SET @CarrierGroupCarrierList = @CarrierGroupCarrierList + ' ("' + @CarrierGroupName + '"), '

	DECLARE @CarrierGroupTenderContact nvarchar (250)
	SET @CarrierGroupTenderContact = (SELECT MAX(FName + ' ' + LName)
									  FROM vTenderContacts tc
									      INNER JOIN vtblPerson p ON tc.PersonID = p.PersonID
									  WHERE tc.CarrierGroupID = @CarrierGroupID AND tc.TenderContactTo = 1)

	--Establish opening text of email
	SET @BodyMessage = @BodyMessage + '<br>' +
	'Dear ' + COALESCE(@CarrierGroupTenderContact, @CarrierGroupName) + ',<br><br>

			Mine Safety Appliances Company ("MSA") hereby submits to ' + @CarrierGroupCarrierList +
	'as well as all other triggered insurers, the claim(s) in the table below. Each 
			claimant below allegedly was exposed to and inhaled, ingested or otherwise absorbed the 
			causative agent listed below on account of certain products that allegedly were manufactured, 
			sold, and/or distributed by MSA. Each claim below also falls within the coverage period 
			afforded by ' + @CarrierGroupName + '''s policy(ies):<br><br>'

	--NOTE: This section populates the tender table, and is still being difficult with the revised structure. It has been temporarily commented out to commit the query to SQL. 
	--Set table column headings and populate the cells with the Tenders from @NewTendersTable
	IF (SELECT
		COUNT([ClaimantName])
	FROM @NewTendersTable
	WHERE CarrierGroupID = @CarrierGroupID) > 0
	BEGIN
	--SET @BodyMessage = @BodyMessage + '<H4>The following Tender Letters were uploaded more than 5 days ago and have not yet been acknowledged: </H4>'
	SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
	N'<tr>	<th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Name</th>
							<th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">NCC Number</th>
							<th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Case Caption</th>
							<th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Service Date</th>
							<th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Exposure Date Alleged in Complaint</th>
							<th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Causative Agent</th>
							<th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Local Defense Counsel</th>' +
	CAST((SELECT
		'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000"><a href="https://www.kcicextranet.com/clients/msa/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, ClaimantPersonalInfoID) + '">' + ClaimantName + '</a></td>',
		'',
		'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000">' + NCCReferenceID + '</td>',
		'',
		'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000">' + CaseCaption + '</td>',
		'',
		'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000">' + COALESCE(CONVERT(nvarchar, ServiceDate, 101), 'N/A') + '</td>',
		'',
		'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000">' + COALESCE(CONVERT(nvarchar, DOFE, 101), 'N/A') + '</td>',
		'',
		'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000">' + COALESCE(CauseAgent, 'Unknown') + '</td>',
		'',
		'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000">' + DefenseCounsel + '</td>',
		''
	FROM @NewTendersTable ntt
	WHERE ntt.CarrierGroupID = @CarrierGroupID
	GROUP BY	ClaimantName,
				DOFE,
				NCCReferenceID,
				CaseCaption,
				ServiceDate,
				CauseAgent,
				DefenseCounsel,
				ClaimantPersonalInfoID

	FOR xml PATH ('tr'), TYPE)
	AS nvarchar(MAX)) +
	N'</table>' + N'<br/>'
	END
	ELSE
	BEGIN
	SET @BodyMessage = @BodyMessage + '<H4>There are currently no outstanding Tender Letters awaiting acknowledgement. </H4>'
	END

	--Undo the HTML stripping from the xml path function above so embedded tags work
	SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

	--Add the rest of the body text of the email
	SET @BodyMessage = @BodyMessage + '<br>
			MSA seeks full indemnity and defense costs from its triggered, on-the-risk insurers collectively 
			for each claim as it is entitled to do under <i>J.H. France Refractories Co. v. Allstate Insurance Co.</i>, 
			626 A.2d 502 (Pa. 1993). As you know, should MSA''s carriers not arrange to fund for its 
			complete defense among themselves, under <i>J.H. France</i>, MSA is entitled to choose any (or any 
			combination) of its insurers to bear full financial responsibility for the defense of each claim.  
			MSA likewise is entitled to recoup indemnity payments from any of its triggered, on-the-risk 
			insurers. MSA specifically reserves all of these rights.<br>'

IF (@CarrierGroupID IN (38,89))
BEGIN

	SET @BodyMessage = @BodyMessage + '<br>
			<b>Please inform me immediately whether MSA''s insurers collectively, any group of them, 
			are willing to fund MSA''s defense of each claim.</b>  
			To the extent that ' + @CarrierGroupName + ' believes that one or more of its policies 
			covers (subject to exhaustion of the underlying policies) the enclosed claim(s), please inform 
			MSA of this fact immediately. Please click <a href="https://www.kcicextranet.com/clients/msa/cp/Pages/Tenders.aspx">here</a> to 
			acknowledge receipt of this letter and view the complaint(s).<br>'
END
ELSE IF (@CarrierGroupID in (86,3))
BEGIN 

	SET @BodyMessage = @BodyMessage + '<br>
			<b>Please inform me immediately whether MSA''s insurers collectively, any group of them, or 
			' + @CarrierGroupName + ' specifically, are willing to fund MSA''s defense of each claim.</b>  MSA welcomes 
			and appreciates ' + @CarrierGroupName + '''s on-going involvement in this matter and assumes that ' + @CarrierGroupName + 
			' consents to MSA incurring costs to defend the enclosed claims(s).  To the extent that ' + @CarrierGroupName + 
			' does not agree to fund MSA''s defense costs, MSA will assume that ' + @CarrierGroupName + ' has denied 
			coverage and, accordingly, will defend and resolve each claim in such a manner as to protect its 
			own interests. To the extent that ' + @CarrierGroupName + ' believes that one or more of its policies 
			covers (subject to exhaustion of the underlying policies) the enclosed claim(s), please inform 
			MSA of this fact immediately. Please click <a href="https://www.kcicextranet.com/clients/msa/cp/Pages/Tenders.aspx">here</a> to acknowledge receipt of this letter and view 
			the complaint(s).<br>'
END
ELSE 
BEGIN

	--M. Potter updated the paragraph below
	SET @BodyMessage = @BodyMessage + '<br>
			<b>Please inform me immediately whether MSA''s insurers collectively, any group of them, or 
			' + @CarrierGroupName + ' specifically, are willing to fund MSA''s defense of each claim.</b>  MSA welcomes 
			and appreciates ' + @CarrierGroupName + '''s on-going involvement in this matter and assumes that ' + @CarrierGroupName + 
			' consents to MSA incurring costs to defend the enclosed claims(s).  To the extent that ' + @CarrierGroupName + 
			' does not agree to fund MSA''s defense costs, MSA will assume that ' + @CarrierGroupName + ' has denied 
			coverage and, accordingly, will defend and resolve each claim in such a manner as to protect its 
			own interests. To the extent that ' + @CarrierGroupName + ' believes that one or more of its policies 
			covers (subject to exhaustion of the underlying policies) the enclosed claim(s), please inform 
			MSA of this fact immediately. Please click <a href="https://www.kcicextranet.com/clients/msa/cp/Pages/Tenders.aspx">here</a> to acknowledge receipt of this letter and view 
			the complaint(s).<br>'
END



	SET @BodyMessage = @BodyMessage + '<br>
			To protect its interests in the interim, MSA has chosen the local defense counsel listed above to 
			represent it.  MSA''s coordinating counsel for all asbestos, silica, and coal mine dust-related
			claims is John Hooper of Reed Smith LLP.<br>'
	SET @BodyMessage = @BodyMessage + '<br>
			You may obtain information and documents related to MSA''s claims on the claims-handling
			website to which you have been provided access.<br>'
	SET @BodyMessage = @BodyMessage + '<br>
			In addition, MSA will provide its insurers with quarterly status reports summarizing case 
			filings, settlements, dismissals for all its claims. Indemnity and defense costs associated with
			the claims above will also be reported to its insurers on a quarterly basis. MSA will subtract
			from the costs included in these reports those amounts paid by any carrier for the claimants listed
			above.<br>'
	SET @BodyMessage = @BodyMessage + '<br>
			Please direct inquiries regarding claimant information and documents to Michelle Potter at KCIC 
			LLC (202-772-2392 potterm@kcic.com).<br>'
	--NOTE: We'll need a link here too ^
	SET @BodyMessage = @BodyMessage + '<br>
			If you have any questions or would like to discuss this matter further, please do not hesitate to 
			contact me.  In the interim, please note that MSA reserves all rights concerning each claim.<br>'
	SET @BodyMessage = @BodyMessage + '<br>
			Sincerely, <br>' +
	@Signature + '<br>' +
	'William Berner<br><br>'

	--Generate the TO recipient list
	DECLARE @RecipientList nvarchar (max)
	SET @RecipientList = (SELECT DISTINCT
		'',
		COALESCE(email, '') + ';'
	FROM vTenderContacts tc
	INNER JOIN vtblPerson p ON tc.PersonID = p.PersonID
	WHERE tc.CarrierGroupID = @CarrierGroupID
	AND tc.TenderContactTo = 1
	FOR xml PATH (''))

	--Generate the CC recipient list (CC for specific carrier group OR contacts who should be CC'd on ALL emails)
	DECLARE @CCList nvarchar (max)
	SET @CCList = (SELECT DISTINCT
		'',
		COALESCE(email, '') + ';'
	FROM vTenderContacts tc
	INNER JOIN vtblPerson p ON tc.PersonID = p.PersonID
	WHERE (tc.CarrierGroupID = @CarrierGroupID
	AND tc.TenderContactCC = 1)
	OR tc.TenderContactAlwaysCC = 1
	FOR xml PATH (''))
	----Add in Local Defense Counsels (removed on 9/27/2013 by C. Young, per ticket #791646 from C. Monahan)
	--SET @CCList = @CCList + COALESCE((SELECT DISTINCT
	--	'',
	--	COALESCE(email, '') + ';'
	--FROM @NewTendersTable ntt
	--INNER JOIN tblClaimLawsuit cl ON ntt.ClaimID = cl.ClaimID
	--INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	--INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
	--WHERE ntt.CarrierGroupID = @CarrierGroupID
	--AND p.TenderContactCC = 1
	--FOR xml PATH (''))
	--, '')

	--Generate the "plain text" Cc list, which is concatenated into the message body
	DECLARE @CCListText nvarchar (MAX)
	SET @CCListText = (SELECT DISTINCT
		FName + ' ' + LName + ', ' + Firm,
		'' AS br
	FROM vTenderContacts tc
	INNER JOIN vtblPerson p ON tc.PersonID = p.PersonID
	INNER JOIN vtblFirm f ON p.FirmID = f.FirmID
	WHERE (tc.CarrierGroupID = @CarrierGroupID
	AND tc.TenderContactCC = 1)
	OR tc.TenderContactAlwaysCC = 1
	FOR xml PATH (''))
	--Add in Local Defense Counsels
	SET @CCListText = @CCListText + COALESCE((SELECT DISTINCT
		FName + ' ' + LName + ', ' + Firm,
		'' AS br
	FROM @NewTendersTable ntt
	INNER JOIN tblClaimLawsuit cl ON ntt.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
	INNER JOIN vtblFirm f ON p.FirmID = f.FirmID
	WHERE ntt.CarrierGroupID = @CarrierGroupID
	AND p.TenderContactCC = 1
	FOR xml PATH (''))
	, '')

	--Generate the BCC recipient list (BCC for specific carrier group OR contacts who should be BCC'd on ALL emails)
	DECLARE @BCCList nvarchar (max)
	SET @BCCList = (SELECT DISTINCT
		'',
		email + ';'
	FROM vTenderContacts tc
	INNER JOIN vtblPerson p ON tc.PersonID = p.PersonID
	WHERE (tc.CarrierGroupID = @CarrierGroupID
	AND tc.TenderContactBCC = 1)
	OR tc.TenderContactAlwaysBCC = 1
	FOR xml PATH (''))
	--Add in Local Defense Counsel paralegals
	SET @BCCList = @BCCList + COALESCE((SELECT DISTINCT
		'',
		COALESCE(email, '') + ';'
	FROM @NewTendersTable ntt
	INNER JOIN tblClaimLawsuit cl ON ntt.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
	WHERE ntt.CarrierGroupID = @CarrierGroupID
	AND p.TenderContactBCC = 1
	FOR xml PATH (''))
	, '')
	SET @BCCList = @BCCList + 'potterm@kcic.com; monahanc@kcic.com; msa@kcic.com'

	SET @BodyMessage = @BodyMessage + 'cc: ' + COALESCE(@CCListText, '') + '<br>'

	--SET @BodyMessage = @BodyMessage + 'QC: Recipients : ' + COALESCE(@RecipientList, '') + '<br>'
	--SET @BodyMessage = @BodyMessage + 'QC: CC  Listing: ' + COALESCE(@CCList, '') + '<br>'
	--SET @BodyMessage = @BodyMessage + 'QC: BCC Listing: ' + COALESCE(@BCCList, '') + '<br>'

	--KCIC address and disclaimer
	SET @BodyMessage = @BodyMessage + '<br><br>
			KCIC, LLC<br>
			733 10th Street, NW, Suite 3001<br>
			Washington, DC 20001<br>
			(t) 202-772-2300<br>
			(f) 202-772-2333'

	--Execute stored procedure to send the email to that firm
	EXEC msdb.dbo.sp_send_dbmail --The Claims sends from msa@kcic.com
	@profile_name = 'Claims',
	@recipients = @RecipientList,
	@copy_recipients = @CCList,
	@blind_copy_recipients = @BCCList,
	--@recipients = 'owenss@kcic.com',--'potterm@kcic.com',
	@subject = @subject,
	@body = @BodyMessage,
	@body_format = 'html'

	 --Update Tender_History table with the email sent date for any tenders that went out
	UPDATE tblTender_History
	SET SentDate = GETDATE()
	WHERE TenderHistoryID IN (SELECT
		TenderHistoryID
	FROM @NewTendersTable)

	--Move to the next firm to create a new email
	FETCH NEXT FROM CarrierGroup_Curs INTO @CarrierGroupID
END

CLOSE CarrierGroup_Curs
DEALLOCATE CarrierGroup_Curs

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmail_Tenders] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmail_Tenders] TO [base_user]
GRANT ALTER ON  [dbo].[qryExternalEmail_Tenders] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmail_Tenders] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmail_Tenders] TO [power_user]
GO
