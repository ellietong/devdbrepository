SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryOccupationInfo]
(@ClaimantPersonalInfoID int, @emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--DECLARE @emailaddress nvarchar(MAX)
--DECLARE @ClaimantpersonalinfoID int
--SET @emailaddress = 'reaton@aultmantyner.com'--'PLChandler@ewhlaw.com'
--SET @Claimantpersonalinfoid = 21263

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)

IF RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1
BEGIN
    -- Insert statements for procedure here
	SELECT DISTINCT ce.ClaimantEmploymentID, ce.OccupationID, o.Occupation, Jobsite, EmpStartDate, EmpEndDate, cpi.FirstName + ' ' + cpi.LastName AS Claimant, 
			JobsiteCity, JobsiteStateID, s.State AS JobsiteState, IsPrimary, ce.Comments, LEFT(ce.Comments, 100) + CASE WHEN LEN(ce.Comments) > 100 THEN '...' ELSE '' END AS Summary, d.DocumentTitle AS SupportingDocTitle, COALESCE(EmpSupportingDocID,-1) AS SupportingDocID, EmpSupportingPageNums AS SupportingPageNums, 
			CASE WHEN IsSecondaryExposure = 1 THEN 'YES' ELSE 'NO' END AS IsSecondaryExposure,
			COALESCE(IsSecondaryExposure, 0) AS IsSecondaryExposureID
	FROM tblClaimantEmployment ce 
	INNER JOIN tblClaimantPersonalInfo cpi ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT Join tblOccupation o on o.OccupationID = ce.OccupationID
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblState s ON ce.JobsiteStateID = s.StateID
	LEFT JOIN tblDocuments d ON d.DocumentID = EmpSupportingDocID
	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	ORDER BY EmpStartDate
END
IF (@InsurerRole = 1)
BEGIN
	SELECT DISTINCT ce.ClaimantEmploymentID, ce.OccupationID, o.Occupation, Jobsite, EmpStartDate, EmpEndDate, cpi.FirstName + ' ' + cpi.LastName AS Claimant,
			JobsiteCity, JobsiteStateID, s.State AS JobsiteState, IsPrimary, ce.Comments, LEFT(ce.Comments, 100) + CASE WHEN LEN(ce.Comments) > 100 THEN '...' ELSE '' END AS Summary, d.DocumentTitle AS SupportingDocTitle, COALESCE(EmpSupportingDocID,-1) AS SupportingDocID, EmpSupportingPageNums AS SupportingPageNums, 
			CASE WHEN IsSecondaryExposure = 1 THEN 'YES' ELSE 'NO' END AS IsSecondaryExposure,
			COALESCE(IsSecondaryExposure, 0) AS IsSecondaryExposureID
	FROM tblClaimantEmployment ce 
	INNER JOIN tblClaimantPersonalInfo cpi ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT Join tblOccupation o on o.OccupationID = ce.OccupationID
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblState s ON ce.JobsiteStateID = s.StateID
	LEFT JOIN tblDocuments d ON d.DocumentID = EmpSupportingDocID
	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND c.ClaimID NOT IN (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where cth.TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
	AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
	ORDER BY EmpStartDate
END
ELSE
BEGIN
	SELECT DISTINCT ce.ClaimantEmploymentID, ce.OccupationID, o.Occupation, Jobsite, EmpStartDate, EmpEndDate, cpi.FirstName + ' ' + cpi.LastName AS Claimant,
	JobsiteCity, JobsiteStateID, s.State AS JobsiteState, IsPrimary, ce.Comments, LEFT(ce.Comments, 100) + CASE WHEN LEN(ce.Comments) > 100 THEN '...' ELSE '' END AS Summary,  d.DocumentTitle AS SupportingDocTitle, COALESCE(EmpSupportingDocID,-1) AS SupportingDocID, EmpSupportingPageNums AS SupportingPageNums,
	CASE WHEN IsSecondaryExposure = 1 THEN 'YES' ELSE 'NO' END AS IsSecondaryExposure,
	COALESCE(IsSecondaryExposure, 0) AS IsSecondaryExposureID	
	FROM tblClaimantEmployment ce 
	INNER JOIN tblClaimantPersonalInfo cpi ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT Join tblOccupation o on o.OccupationID = ce.OccupationID
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblState s ON ce.JobsiteStateID = s.StateID
	LEFT JOIN tblDocuments d ON d.DocumentID = EmpSupportingDocID
	INNER JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID
	INNER JOIN tblDefenseJurisdiction dj ON  l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID 
	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND dj.DefenseCounselAddressID IN (SELECT DefenseCounselAddressID FROM vtblPerson p inner join tblDefenseCOunselAddress dca on p.FirmID=dca.DefenseCounselID WHERE Email = @emailaddress) 
	ORDER BY EmpStartDate
END
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOccupationInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryOccupationInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOccupationInfo] TO [power_user]
GO
