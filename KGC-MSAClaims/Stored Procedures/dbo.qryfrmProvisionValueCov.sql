SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmProvisionValueCov]

AS
SET NOCOUNT ON
SELECT ProvisionValueID, ProvisionLabelID, ProvisionValue
FROM tblProvisionValue
WHERE ProvisionLabelID = 4
RETURN 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionValueCov] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionValueCov] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmProvisionValueCov] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionValueCov] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionValueCov] TO [power_user]
GO
