SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[qrysrptLawFirmFollowUp]
(@CorrespondenceID int)
as
SET NOCOUNT ON

--DECLARE @CorrespondenceID int
--SET @CorrespondenceID=1

DECLARE @Client table(correspondenceid int, followup nvarchar(4000), taskcategoryid int, taskcategory nvarchar(100), 
	completed bit, datecompleted date)
DECLARE @kcic table(correspondenceid int, followup nvarchar(4000), taskcategoryid int, taskcategory nvarchar(100), 
	completed bit, datecompleted date)
DECLARE @LawFirm table (correspondenceid int, followup nvarchar(4000), taskcategoryid int, taskcategory nvarchar(100), 
	 completed bit, datecompleted date)
DECLARE @final table(correspondenceid int, followup nvarchar(4000), taskcategory nvarchar(100))
	
INSERT INTO @Client
select Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory, completed, datecompleted
from tblCorrespondence cor
INNER JOIN tblFollowUp f on cor.CorrespondenceID = f.CorrespondenceID
INNER JOIN tblTaskCategoryLookup tc on f.TaskCategoryID = tc.TaskCategoryID
inner join tblInternalContact ic on f.InternalContactID = ic.InternalContactID
inner join tblInternalCompany ico on ic.InternalCompanyID = ico.InternalCompanyID
where ico.InternalCompanyID = 3 and Completed = 0

INSERT INTO @kcic
select Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory,  completed, datecompleted
from tblCorrespondence cor
INNER JOIN tblFollowUp f on cor.CorrespondenceID = f.CorrespondenceID
INNER JOIN tblTaskCategoryLookup tc on f.TaskCategoryID = tc.TaskCategoryID
inner join tblInternalContact ic on f.InternalContactID = ic.InternalContactID
inner join tblInternalCompany ico on ic.InternalCompanyID = ico.InternalCompanyID
where ico.InternalCompanyID = 1 and Completed = 0

INSERT INTO @LawFirm
select Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory,  completed, datecompleted
from tblCorrespondence cor
INNER JOIN tblFollowUp f on cor.CorrespondenceID = f.CorrespondenceID
INNER JOIN tblTaskCategoryLookup tc on f.TaskCategoryID = tc.TaskCategoryID
inner join tblInternalContact ic on f.InternalContactID = ic.InternalContactID
inner join tblInternalCompany ico on ic.InternalCompanyID = ico.InternalCompanyID
where ico.InternalCompanyID = 2 and Completed = 0
order by cor.CorrespondenceID


Insert into @final

select Cor.correspondenceid,
Case	when coalesce(c.followup,k.followup) is not null and l.followup IS null Then 'N/A' 
		Else l.followup end as Followup, l.taskcategory
from tblCorrespondence cor
Left JOIN @LawFirm L on cor.CorrespondenceID=L.correspondenceid
Left JOIN @Client c on cor.CorrespondenceID=c.correspondenceid
Left JOIN @kcic k on cor.CorrespondenceID=k.correspondenceid
where cor.CorrespondenceID=@CorrespondenceID 
Group BY Cor.correspondenceid,l.taskcategory,c.followup, k.followup,l.followup

Select correspondenceid, followup , taskcategory
from @Final
Group By correspondenceid, followup , taskcategory


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptLawFirmFollowUp] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptLawFirmFollowUp] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptLawFirmFollowUp] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptLawFirmFollowUp] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptLawFirmFollowUp] TO [power_user]
GO
