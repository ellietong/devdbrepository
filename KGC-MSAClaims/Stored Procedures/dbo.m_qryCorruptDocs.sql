SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_qryCorruptDocs]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DocumentID AS DocumentID, DocumentURL, DocumentTitle, d.DetailDocumentTypeID,-- ClaimantDocumentType,
	'' AS DefenseCounsel, '' as DefenseCounselID
	FROM tblDocuments d
	--INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID 
	WHERE DocumentID in (
	485450,
509264,
519815,
521374,
373081,
514548,
385537,
478603,
482117,
514322,
374015,
518160,
373998,
484780,
508570,
385702,
496138,
498893,
516360,
503276,
494840,
486644,
505130,
493976,
496474,
508276,
487858,
514478,
483958,
492851,
384373,
384373,
384459,
384460,
384461,
384462,
384464,
384465,
384466,
384467,
384469,
384470,
384471,
384472,
384285,
384286,
384287,
384288,
384289,
384285,
384286,
384287,
384288,
384289,
384285,
384286,
384287,
384288,
384289,
384285,
384286,
384287,
384288,
384289,
384285,
384286,
384287,
384288,
384289,
384285,
384286,
384287,
384288,
384289,
384305,
384305,
384299,
384300,
384301,
384302,
384303,
384299,
384300,
384301,
384302,
384303,
384299,
384300,
384301,
384302,
384303,
384299,
384300,
384301,
384302,
384303,
384299,
384300,
384301,
384302,
384303,
384299,
384300,
384301,
384302,
384303,
384321,
384328,
384329,
384330,
384328,
384329,
384330,
384328,
384329,
384330,
384328,
384329,
384330,
387164,
388394,
527946,
385304,
385305,
385306,
385307,
385308,
385309,
385310,
385311,
385312,
385313,
385314,
385315,
385316,
385317,
385318,
385319,
385320,
385321,
385322,
385323,
385324,
385325,
480373,
495726,
498874,
507512,
507513,
513872,
519480,
520528,
385304,
385305,
385306,
385307,
385308,
385309,
385310,
385311,
385312,
385313,
385314,
385315,
385316,
385317,
385318,
385319,
385320,
385321,
385322,
385323,
385324,
385325,
480373,
495726,
498874,
519480,
480730,
486866,
478276,
478278,
499069,
522868,
496179,
502674,
503228,
518368,
492743,
515399,
385410,
479103,
479918,
484010,
484537,
495123,
508548,
517023,
520231,
479955,
483709,
487006,
494519,
500334,
502389,
510213,
510235,
519340,
385381,
479006,
479007,
491358,
482221,
499012,
499414,
491348,
491349,
497455,
502491,
503308,
505380,
519568,
483764,
504857,
505608,
507832,
523517,
498739,
499072,
478460,
486673,
486674,
486675,
486676,
500199,
509522,
515648,
515649,
515650,
515651,
516733,
519366,
524230,
487643,
487922,
496491,
499677,
479479,
519842,
384082,
384404,
388642,
386306,
386309,
498955
	)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_qryCorruptDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qryCorruptDocs] TO [base_user]
GRANT ALTER ON  [dbo].[m_qryCorruptDocs] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_qryCorruptDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_qryCorruptDocs] TO [power_user]
GO
