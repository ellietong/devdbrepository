SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetProductManufacturerList]
(@PrefixText nvarchar(200), @Emailaddress nvarchar(250) = NULL )
AS
BEGIN
	SELECT ProductManufacturerID, ProductManufacturer
	FROM tblProductManufacturer
	WHERE ProductManufacturerID <> 1 
		AND ProductManufacturer LIKE '%' + @PrefixText + '%'
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetProductManufacturerList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetProductManufacturerList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SERVICES_qryGetProductManufacturerList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetProductManufacturerList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetProductManufacturerList] TO [power_user]
GO
