SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryGetClaimsByTenderHistoryID]
(@TenderHistoryID bigint, @Emailaddress nvarchar(500) = null)
AS
BEGIN
	SET NOCOUNT ON;

    --Test vars
    --DECLARE @TenderHistoryID bigint = 218951
    
	SELECT		cth.ClaimID,
				c.ClaimantPersonalInfoID,
				c.NCCReferenceID,
				cth.ClaimTenderHistoryID,
				CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
				THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, 
				DocketNumber, 
				Material,
				cth.TenderStatusID,
				TenderStatus
	FROM tblTenderHistory th
	INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
	INNER JOIN tblClaim c ON cth.ClaimID = c.ClaimID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	INNER JOIN tblMaterial m ON a.MaterialID = m.MaterialID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblTenderStatus ts ON cth.TenderStatusID = ts.TenderStatusID
	WHERE th.TenderHistoryID = @TenderHistoryID AND COALESCE(IsPrimaryForClaim,0) = 1
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryGetClaimsByTenderHistoryID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryGetClaimsByTenderHistoryID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryGetClaimsByTenderHistoryID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryGetClaimsByTenderHistoryID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryGetClaimsByTenderHistoryID] TO [power_user]
GO
