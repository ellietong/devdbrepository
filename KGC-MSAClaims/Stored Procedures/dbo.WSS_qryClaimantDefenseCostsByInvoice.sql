SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_qryClaimantDefenseCostsByInvoice] 


	
 ----Add the parameters for the stored procedure here
	
(@ClaimantPersonalInfoID int, @DefenseCounselID nvarchar(250)) 
AS
BEGIN
	
	
--DECLARE @ClaimantPersonalInfoID bigint
--DECLARE @DefenseCounselID int

--SET @ClaimantPersonalInfoID = 2245
--SET @DefenseCounselID = 233

-- SET NOCOUNT ON added to prevent extra result sets from
	
-- interfering with SELECT statements.
	
SET NOCOUNT ON;
	

----Approved Expert and General Invoices with a ClaimantID link
Select claim.ClaimantPersonalInfoID, Di.FirmID as DefenseCounselID, Di.InvoiceNumber as InvoiceNumber, 
	SUM(COALESCE(Costs, 0)) as DefenseTotal, 
	Case when IsFullyPaid = 1 then 'Paid' Else Status end as Status, SourceDate, case when DatePaid = '11/1/2011' then 'Historical' else convert(nvarchar(50), DatePaid, 101) end as DatePaid
From tblClaim claim
LEFT JOIN tblDefInvClaimantDetail DICD ON claim.ClaimId = DICD.ClaimID
INNER Join tblDefenseInvoice di on DICD.DefenseInvoiceID =  DI.DefenseInvoiceID
LEFT Join tblDefenseInvoiceStatus ds on di.StatusID=ds.StatusID
Left Join tblPaymentDefense pd on di.DefenseInvoiceID=pd.DefenseInvoiceID
LEFT JOIN tblCheck c ON pd.CheckID = c.CheckID
Where claim.ClaimantPersonalInfoID = @ClaimantPersonalInfoID  AND di.FirmID = @DefenseCounselID
Group by claim.ClaimantPersonalInfoID, Di.FirmID,  Di.InvoiceNumber, 
Case when IsFullyPaid = 1 then 'Paid'
Else Status end, SourceDate, DatePaid
HAVING SUM(COALESCE(DI.InvoiceTotal, 0)) <> 0
ORDER BY SourceDate DESC
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantDefenseCostsByInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantDefenseCostsByInvoice] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimantDefenseCostsByInvoice] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantDefenseCostsByInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantDefenseCostsByInvoice] TO [power_user]
GO
