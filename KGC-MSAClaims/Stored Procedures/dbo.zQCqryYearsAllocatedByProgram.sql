SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[zQCqryYearsAllocatedByProgram]
(@allocationid int )
As

SET NOCOUNT ON

SELECT a.AllocationDescription, ar.AllocationID, MIN(ar.PStartSplit) AS MinOfPStartSplit, MAX(ar.PEndSplit) AS MaxOfPEndSplit, p.InsuranceProgramID, ip.InsuranceProgram
FROM (tblInsuranceProgram ip INNER JOIN tblPolicy p ON ip.InsuranceProgramID = p.InsuranceProgramID) 
	INNER JOIN (tblAllocation a INNER JOIN tblAllocationResults ar ON a.AllocationID = ar.AllocationID) ON p.PolicyID = ar.PolicyID
WHERE ar.AllocationID LIKE COALESCE(@allocationid, 0)
GROUP BY a.AllocationDescription, ar.AllocationID, p.InsuranceProgramID, ip.InsuranceProgram
ORDER BY ar.AllocationID, MIN(ar.PStartSplit)

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[zQCqryYearsAllocatedByProgram] TO [base_user]
GRANT EXECUTE ON  [dbo].[zQCqryYearsAllocatedByProgram] TO [base_user]
GRANT ALTER ON  [dbo].[zQCqryYearsAllocatedByProgram] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryYearsAllocatedByProgram] TO [power_user]
GRANT EXECUTE ON  [dbo].[zQCqryYearsAllocatedByProgram] TO [power_user]
GO
