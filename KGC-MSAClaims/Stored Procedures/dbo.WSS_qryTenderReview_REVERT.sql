SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryTenderReview_REVERT] (@TenderHistoryID bigint, @TenderTemplateID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--SELECT * FROM tblTenderHistory ORDER BY TenderHistoryID DESC
	--SELECT * FROM tblTenderHistory ORDER BY TenderHistoryID DESC
	--Withdrawal
	--DECLARE @TenderHistoryID bigint = 172940 --(retender)
	--DECLARE @TenderTemplateID int = 45 --(retender)
	

--select * from tblTenderHistory where ClaimID=45599
	--Retender
	--DECLARE @TenderHistoryID bigint = 172893 
	--DECLARE @TenderTemplateID int = 41
	
	DECLARE @ClaimID bigint
	SET @ClaimID = (SELECT ClaimID 
					FROM tblTenderHistory 
					WHERE TenderHistoryID = @TenderHistoryID)
	DECLARE @WithdrawnTenderBlockName nvarchar(MAX)
	SET @WithdrawnTenderBlockName = (SELECT TOP 1 TenderBlockName_Reporting 
									 FROM tblTenderBlock tb
										INNER JOIN tblTenderHistory th ON tb.TenderBlockID = th.TenderBlockID
									 WHERE tb.TenderBlockID in (SELECT TenderBlockID FROM tblTenderHistory WHERE TenderTypeID = 2 AND ClaimID = @ClaimID)
									 ORDER BY DateCreated DESC)
	
	DECLARE @NewLine nvarchar(5) =  '<br/>'
	
	DECLARE @Lname nvarchar(250), @Fname nvarchar(250), @Mname nvarchar(250), @Title nvarchar(250),  @Email nvarchar(250), @CCList nvarchar(max)

	SET @CCList = ''
	IF (SELECT @TenderHistoryID) IN (172032) --withdrawal
	BEGIN
		DECLARE Cur_ccContacts CURSOR Local Fast_Forward for
		SELECT DISTINCT LName, FName, MName, Email
		FROM tblTenderContactForMassRetender
		WHERE CC = 1 AND CurrentContact = 1 AND TenderContactID <> 22
	END
	ELSE IF (SELECT @TenderHistoryID) IN (172033) --retender
	BEGIN
		DECLARE Cur_ccContacts CURSOR Local Fast_Forward for
		SELECT DISTINCT LName, FName, MName, Email
		FROM tblTenderContactForMassRetender
		WHERE CC = 1 AND CurrentContact = 1
	END
	ELSE IF (SELECT @TenderHistoryID) IN (171964)
	BEGIN
		DECLARE Cur_ccContacts CURSOR Local Fast_Forward for
		SELECT DISTINCT LName, FName, MName, Email
		FROM tblTenderContact tc
		INNER JOIN tblPolicy p ON tc.CarrierID = p.CarrierID
		INNER JOIN tblTenderBlockPolicy tbp ON p.PolicyID = tbp.PolicyID
		INNER JOIN tblTenderBlock tb ON tbp.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderHistory th ON tb.TenderBlockID = th.TenderBlockID
		WHERE TenderHistoryID = @TenderHistoryID AND CC=1
		--Union in "alwaysCC" contacts
		UNION
		SELECT LName, FName, MName, Email
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact
		SELECT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE (p.TendercontactCC = 1) AND c.ClaimID = (SELECT ClaimID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
		UNION--NCC depending on claim type
		SELECT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl On c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE TenderHistoryID = @TenderHistoryID
		UNION
		SELECT DISTINCT LName, FName, MName, Email
		FROM tblTenderContactForMassRetender
		WHERE TenderContactID = 4	
		ORDER BY LName, FName
	END
	ELSE IF (SELECT @TenderHistoryID) IN (172254) --Mass withdrawal Letter1
	BEGIN
		DECLARE Cur_ccContacts CURSOR Local Fast_Forward for
		SELECT DISTINCT LName, FName, MName, Email
		FROM tblTenderContact tc
		INNER JOIN tblPolicy p ON tc.CarrierID = p.CarrierID
		INNER JOIN tblTenderBlockPolicy tbp ON p.PolicyID = tbp.PolicyID
		INNER JOIN tblTenderBlock tb ON tbp.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderHistory th ON tb.TenderBlockID = th.TenderBlockID
		WHERE TenderHistoryID = @TenderHistoryID AND CC=1
		--Union in "alwaysCC" contacts
		UNION
		SELECT LName, FName, MName, Email
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact
		SELECT DISTINCT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE (p.TendercontactCC = 1) AND c.NCCReferenceID IN ('NCC00040640', 
			'NCC00050751', 
			'NCC10000013', 
			'NCC10000046', 
			'NCC11000002', 
			'NCC11000008', 
			'NCC11000011', 
			'NCC11000018', 
			'NCC11000019', 
			'NCC11000020', 
			'NCC11000024', 
			'NCC11000027', 
			'NCC11000036', 
			'NCC11000042', 
			'NCC11000056', 
			'NCC11000058', 
			'NCC11000072', 
			'NCC11000078', 
			'NCC11000082', 
			'NCC11000084', 
			'NCC11000085', 
			'NCC11000089', 
			'NCC11000091', 
			'NCC11000092', 
			'NCC11000096', 
			'NCC11000098', 
			'NCC11000099', 
			'NCC11000100', 
			'NCC11000103', 
			'NCC11000106', 
			'NCC11000107', 
			'NCC11000125', 
			'NCC11000130', 
			'NCC11000137', 
			'NCC11000138', 
			'NCC11000139', 
			'NCC11000146', 
			'NCC11000147', 
			'NCC11000153', 
			'NCC11000154', 
			'NCC11000156', 
			'NCC11000157', 
			'NCC11000158', 
			'NCC11000164', 
			'NCC11000165', 
			'NCC11000169', 
			'NCC11000249', 
			'NCC11000250', 
			'NCC11000255', 
			'NCC11000256', 
			'NCC11000259', 
			'NCC11000273', 
			'NCC11000275', 
			'NCC11000299', 
			'NCC11000300', 
			'NCC11000301', 
			'NCC11000302', 
			'NCC11000303', 
			'NCC11000309', 
			'NCC11000310', 
			'NCC11000315', 
			'NCC11000325', 
			'NCC11000326', 
			'NCC11000328', 
			'NCC11000329', 
			'NCC11000330', 
			'NCC11000331', 
			'NCC11000333', 
			'NCC11000335', 
			'NCC11000336', 
			'NCC11000337', 
			'NCC11000338', 
			'NCC11000339', 
			'NCC11000341', 
			'NCC11000342', 
			'NCC11000343', 
			'NCC11000344', 
			'NCC11000345', 
			'NCC11000346', 
			'NCC11000347', 
			'NCC11000350', 
			'NCC11000351')
		UNION--NCC depending on claim type
		SELECT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl On c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE TenderHistoryID = @TenderHistoryID		
		ORDER BY LName, FName
	END
	ELSE IF (SELECT @TenderHistoryID) IN (172255) --Mass withdrawal letter 2
	BEGIN
		DECLARE Cur_ccContacts CURSOR Local Fast_Forward for
		SELECT DISTINCT LName, FName, MName, Email
		FROM tblTenderContact tc
		INNER JOIN tblPolicy p ON tc.CarrierID = p.CarrierID
		INNER JOIN tblTenderBlockPolicy tbp ON p.PolicyID = tbp.PolicyID
		INNER JOIN tblTenderBlock tb ON tbp.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderHistory th ON tb.TenderBlockID = th.TenderBlockID
		WHERE TenderHistoryID = @TenderHistoryID AND CC=1
		--Union in "alwaysCC" contacts
		UNION
		SELECT LName, FName, MName, Email
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact
		SELECT DISTINCT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE (p.TendercontactCC = 1) AND c.NCCReferenceID IN (
		'NCC00044135', 
		'NCC00045210', 
		'NCC11000004', 
		'NCC11000005', 
		'NCC11000006', 
		'NCC11000012', 
		'NCC11000013', 
		'NCC11000017', 
		'NCC11000026', 
		'NCC11000032', 
		'NCC11000034', 
		'NCC11000039', 
		'NCC11000041', 
		'NCC11000050', 
		'NCC11000053', 
		'NCC11000057', 
		'NCC11000066', 
		'NCC11000067', 
		'NCC11000070', 
		'NCC11000081', 
		'NCC11000083', 
		'NCC11000086', 
		'NCC11000094', 
		'NCC11000102', 
		'NCC11000108', 
		'NCC11000109', 
		'NCC11000111', 
		'NCC11000113', 
		'NCC11000119', 
		'NCC11000123', 
		'NCC11000124', 
		'NCC11000126', 
		'NCC11000135', 
		'NCC11000141', 
		'NCC11000142', 
		'NCC11000143', 
		'NCC11000148', 
		'NCC11000149', 
		'NCC11000155', 
		'NCC11000160', 
		'NCC11000161', 
		'NCC11000166', 
		'NCC11000251', 
		'NCC11000252', 
		'NCC11000260', 
		'NCC11000261', 
		'NCC11000262', 
		'NCC11000276', 
		'NCC11000304', 
		'NCC11000306', 
		'NCC11000327')
		UNION--NCC depending on claim type
		SELECT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl On c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE TenderHistoryID = @TenderHistoryID	
		ORDER BY LName, FName
	END
	ELSE IF (SELECT @TenderHistoryID) IN (172727) --Mass withdrawal Letter 05-09-2012
	BEGIN
		DECLARE Cur_ccContacts CURSOR Local Fast_Forward for
		SELECT DISTINCT LName, FName, MName, Email
		FROM tblTenderContact tc
		INNER JOIN tblPolicy p ON tc.CarrierID = p.CarrierID
		INNER JOIN tblTenderBlockPolicy tbp ON p.PolicyID = tbp.PolicyID
		INNER JOIN tblTenderBlock tb ON tbp.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderHistory th ON tb.TenderBlockID = th.TenderBlockID
		WHERE TenderHistoryID = @TenderHistoryID AND CC=1
		--Union in "alwaysCC" contacts
		UNION
		SELECT LName, FName, MName, Email
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact
		SELECT DISTINCT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE (p.TendercontactCC = 1) AND c.NCCReferenceID IN ('NCC10000040',
		'NCC10000041',
		'NCC10000206',
		'NCC11000003',
		'NCC11000021')
		UNION--NCC depending on claim type
		SELECT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl On c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE TenderHistoryID = @TenderHistoryID		
		ORDER BY LName, FName
	END
	ELSE IF (SELECT @TenderHistoryID) IN (172728) --Mass re-temder letter 05-09-2012
	BEGIN
		DECLARE Cur_ccContacts CURSOR Local Fast_Forward for
		SELECT DISTINCT LName, FName, MName, Email
		FROM tblTenderContact tc
		INNER JOIN tblPolicy p ON tc.CarrierID = p.CarrierID
		INNER JOIN tblTenderBlockPolicy tbp ON p.PolicyID = tbp.PolicyID
		INNER JOIN tblTenderBlock tb ON tbp.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderHistory th ON tb.TenderBlockID = th.TenderBlockID
		WHERE TenderHistoryID = @TenderHistoryID AND CC=1
		--Union in "alwaysCC" contacts
		UNION
		SELECT LName, FName, MName, Email
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact
		SELECT DISTINCT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE (p.TendercontactCC = 1) AND c.NCCReferenceID IN ('NCC10000040',
		'NCC10000041',
		'NCC10000206',
		'NCC11000003',
		'NCC11000021',
		'NCC00044250',
		'NCC00044298')
		UNION--NCC depending on claim type
		SELECT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl On c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE p.PersonID = 3398	
		ORDER BY LName, FName
	END
		ELSE IF (SELECT @TenderHistoryID) IN (172778) --Mass re-temder letter 05-16-2012
	BEGIN
		DECLARE Cur_ccContacts CURSOR Local Fast_Forward for
		SELECT DISTINCT LName, FName, MName, Email
		FROM tblTenderContact tc
		INNER JOIN tblPolicy p ON tc.CarrierID = p.CarrierID
		INNER JOIN tblTenderBlockPolicy tbp ON p.PolicyID = tbp.PolicyID
		INNER JOIN tblTenderBlock tb ON tbp.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderHistory th ON tb.TenderBlockID = th.TenderBlockID
		WHERE TenderHistoryID = @TenderHistoryID AND CC=1
		--Union in "alwaysCC" contacts
		UNION
		SELECT LName, FName, MName, Email
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact
		SELECT DISTINCT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
		WHERE (p.TendercontactCC = 1) AND c.NCCReferenceID IN ('NCC11000092',
		'NCC11000130',
		'NCC11000165',
		'NCC11000273',
		'NCC11000342')
		UNION--NCC depending on claim type
		SELECT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl On c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE p.PersonID = 3398	
		ORDER BY LName, FName
	END
	ELSE
	BEGIN
		DECLARE Cur_ccContacts CURSOR Local Fast_Forward for
		SELECT DISTINCT LName, FName, MName, Email
		FROM tblTenderContact tc
		INNER JOIN tblPolicy p ON tc.CarrierID = p.CarrierID
		INNER JOIN tblTenderBlockPolicy tbp ON p.PolicyID = tbp.PolicyID
		INNER JOIN tblTenderBlock tb ON tbp.TenderBlockID = tb.TenderBlockID
		INNER JOIN tblTenderHistory th ON tb.TenderBlockID = th.TenderBlockID
		WHERE TenderHistoryID = @TenderHistoryID AND CC=1
		--Union in "alwaysCC" contacts
		UNION
		SELECT LName, FName, MName, Email
		FROM tblTenderContact WHERE CarrierID IS NULL AND AlwaysCC = 1
		UNION
		--Add in defense counsel tender contact
		SELECT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN vtblPerson p ON CASE WHEN l.DefenseCounselAddressID = 127 THEN 189 ELSE l.DefenseCounselAddressID END = p.FirmAddressID
		WHERE (p.TendercontactCC = 1) AND c.ClaimID = (SELECT ClaimID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
		UNION--NCC depending on claim type
		SELECT p.LName, p.FName, null, p.Email
		FROM tblTenderHistory th
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimLawsuit cl On c.ClaimID = cl.ClaimID
		INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
		INNER JOIN vtblPerson p ON nm.PersonID = p.PersonID
		WHERE TenderHistoryID = @TenderHistoryID
		ORDER BY LName, FName	
	END
	
	DECLARE @LDCList nvarchar(MAX)
	SET @LDCList = ''
	DECLARE @LDCName nvarchar(MAX)
	
	DECLARE Cur_LDCContact CURSOR Local Fast_Forward for
	SELECT DISTINCT COALESCE(p.FName, '') + ' ' + COALESCE(p.LName, '') AS LDCName
	FROM tblTenderHistory th
	INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
	WHERE (p.PrimaryContact = 1) AND c.ClaimID = (SELECT ClaimID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
	ORDER BY COALESCE(p.FName, '') + ' ' + COALESCE(p.LName, '')
	
	OPEN Cur_LDCContact
	FETCH NEXT FROM Cur_LDCContact INTO @LDCName--@LName, @FName, @MName, @Title, @Email
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF(@LDCList = '')
		BEGIN
			SET @LDCList = @LDCName + @NewLine
		END
		ELSE
		BEGIN
			SET @LDCList = @LDCList + @LDCName + @NewLine
		END
	
		FETCH NEXT FROM Cur_LDCContact INTO @LDCName--@LName, @FName, @MName, @Title, @Email
	END
	CLOSE Cur_LDCContact
	DEALLOCATE Cur_LDCContact
	
	
	-------------------------------------------------------------
	-----------LDC Email List--------- at bottom of LDC address (in a table)
	DECLARE @LDCEmails nvarchar(MAX)
	SET @LDCEmails = ''
	DECLARE @LDCEmail nvarchar(100) = ''
	DECLARE @Name nvarchar(500)
	DECLARE Cur_LDCEmails CURSOR Local Fast_Forward for
	SELECT  p.Email, p.LName + ', ' + p.FName
	FROM tblTenderHistory th
	INNER JOIN tblClaim c ON c.ClaimID = th.ClaimID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN vtblPerson p ON l.DefenseCounselAddressID = p.FirmAddressID
	WHERE (p.PrimaryContact = 1) AND c.ClaimID = (SELECT ClaimID FROM tblTenderHistory WHERE TenderHistoryID = @TenderHistoryID)
	GROUP BY p.Email, p.LName + ', ' + p.FName
	ORDER BY p.LName + ', ' + p.FName
	
	OPEN Cur_LDCEmails
	FETCH NEXT FROM Cur_LDCEmails INTO @LDCEmail, @Name
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF(@LDCEmails = '')
		BEGIN
			SET @LDCEmails = '<table style="margin: 0; padding: 0;" cellspacing="0" cellpadding="0"><tr><td>Email:&nbsp;</td><td>' + @LDCEmail + '</td></tr>'
		END
		ELSE
		BEGIN
			SET @LDCEmails = @LDCEmails + '<tr><td></td><td>' + @LDCEmail + '</td></tr>'
		END
	
		FETCH NEXT FROM Cur_LDCEmails INTO @LDCEmail, @Name
		IF(@@FETCH_STATUS < 0 AND @LDCEmails <> '')
		BEGIN
			SET @LDCEmails = @LDCEmails + '</table>'
		END
	END
	CLOSE Cur_LDCEmails
	DEALLOCATE Cur_LDCEmails
	
	
	--Assemble table for CC contacts listing at bottom of page
	
	OPEN Cur_ccContacts
	FETCH NEXT FROM Cur_ccContacts INTO @LName, @FName, @MName,  @Email
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF(@CCList = '')
		BEGIN

			SET @CCList = '<table><tr><td>cc:</td>' + '<td>' + @FName + COALESCE(' ' + @MName + ' ', ' ') + @Lname  + ' (by email only - ' + @Email + ')</td></tr>' 

		END
		ELSE
		BEGIN

			SET @CCList = @CCList + '<td></td><td>' + @FName + COALESCE(' ' + @MName + ' ', ' ') + @Lname + ' (by email only - ' + @Email + ')</td></tr>' 
			
		END

		FETCH NEXT FROM Cur_ccContacts INTO @LName, @FName, @MName,  @Email
		IF(@@FETCH_STATUS < 0 AND @CCList <> '')
		BEGIN
			SET @CCList = @CCList + '</table>'
		END
	END
	CLOSE Cur_ccContacts
	DEALLOCATE Cur_ccContacts

		
   SELECT DISTINCT th.TenderHistoryID, DATENAME(mm,GETDATE()) + ' ' + CONVERT(nvarchar, DAY(GETDATE())) + ', ' + CONVERT(nvarchar, YEAR(GETDATE())) AS [Date],
    AdditionalText, ttemp.TemplateURL, TenderBlockName, EffectiveDate, TenderType,
    CASE WHEN l.ReceivedDate IS NULL THEN CaseCaption ELSE CaseCaption + ', Case No. ' + DocketNumber + ' (' + CourtType + ', ' + Jurisdiction + ', ' + s.State + ')' END AS CaseCaption,
    CASE WHEN l.StateID = 3 THEN 'Huddleston Bolen LLP' ELSE dca.DefenseCounsel END AS DefenseCounsel,
    COALESCE(cpi.FirstName, '') + COALESCE(' '  + MiddleName, '') + COALESCE(' ' + LastName, '') AS ClaimantFullName,
    NCCReferenceID, CaseCaption, dca.DefenseCounselAddress1, dca.DefenseCounselAddress2, dca.DefenseCounselCity, 
    dca.DefenseCounselZip, dca.DefenseCounselMailZip, dca.DefenseCounselPhone, dca.State AS DefenseCounselState,
    Jurisdiction, s.State, CourtType, DocketNumber, CONVERT(nvarchar, ServiceDate, 101) AS ServiceDate,
    CASE WHEN l.StateID = 3 THEN @NewLine + '611 Third Avenue' + @Newline + 'Huntington, WV 25701' + @NewLine + 'Phone: (304) 529-6181' + 
    '<table style="margin: 0; padding: 0;" cellspacing="0" cellpadding="0"><tr><td>Email:&nbsp;</td><td>jmahaney@huddlestonbolen.com</td></tr></table>'
     ELSE
    @NewLine + COALESCE(dca.DefenseCounselAddress1, '') +  
    COALESCE(@NewLine + dca.DefenseCounselAddress2, '') + @NewLine + 
    COALESCE(dca.DefenseCounselCity + ', ', '')  + COALESCE(dca.State, '') + ' ' + COALESCE(dca.DefenseCounselZip, '') + COALESCE(('-' + dca.DefenseCounselMailZip), '') + @NewLine + 
    COALESCE('Phone: ' + '(' + Substring(dca.DefenseCounselPhone,1,3) + ') ' 
           + Substring(dca.DefenseCounselPhone,4,3) + '-' 
           + Substring(dca.DefenseCounselPhone,7,4), '') +
    --COALESCE(@NewLine + 'Fax: ' + '(' + Substring(dca.DefenseCounselFax,1,3) + ') ' 
    --       + Substring(dca.DefenseCounselFax,4,3) + '-' 
    --       + Substring(dca.DefenseCounselFax,7,4), '') + 
    COALESCE(@NewLine + @LDCEmails, '')
           
      END AS DefenseCounselAddress, 
     DocumentURL as ComplaintPath, TenderType,
     --CASE WHEN th.TenderTypeID = 2 THEN NULL ELSE DocumentURL END AS ComplaintPath, TenderType,
     CASE WHEN cl.LawsuitID IN (SELECT lawsuitid FROM tblClaimLawsuit group by lawsuitid having COUNT(claimID)>1) THEN 'claim brought by ' + CASE WHEN GenderID = 2 THEN 'Ms.'+LastName WHEN GenderID = 3 THEN 'Mr.'+ LastName ELSE FirstName + ' ' + LastName END + ' as part of the lawsuit captioned ' ELSE
     'lawsuit captioned ' END AS LawsuitText,
     CASE WHEN (ServiceDate IS NULL OR ServiceDate = '1/1/1900') AND FileDate IS NOT NULL THEN 'which was filed on or about ' + DATENAME(mm,FileDate) + ' ' + CONVERT(nvarchar, DAY(FileDate)) + ', ' + CONVERT(nvarchar, YEAR(FileDate))
		ELSE 'which was served on MSA on or about ' + DATENAME(mm,ServiceDate) + ' ' + CONVERT(nvarchar, DAY(ServiceDate)) + ', ' + CONVERT(nvarchar, YEAR(ServiceDate)) END AS DateText,
		  CASE WHEN dbo.fnEarliestExposure(claim.ClaimantPersonalInfoID) IS NULL THEN 'No exposure dates are alleged in the complaint; accordingly, the defense is being tendered to North River and Northbrook on the basis that the claims giving rise to the complaint potentially fall within the coverage afforded by the aforementioned policies.' ELSE '' END AS EarliestExposureText,
		  @CCList AS CCRecipients, CASE WHEN l.StateID = 3 THEN 'J. H. Mahaney' + @NewLine ELSE @LDCList END AS LDCList, CASE WHEN ServiceDate IS NULL THEN 'This complaint has been filed, but not yet formally served on MSA.' ELSE '' END AS FormallyServed,
		  
	CASE WHEN a.MaterialID NOT IN (3,4,7,8) THEN COALESCE(p.FName, '') + ' ' + COALESCE(p.LName, '') + COALESCE(', ' + p.Title, '') ELSE '' END AS NCCName, 
	
	CASE WHEN a.MaterialID NOT IN (3,4,7,8) THEN @NewLine + COALESCE(ncca.DefenseCounsel, '') + @NewLine + COALESCE(ncca.DefenseCounselAddress1, '') +  
    COALESCE(@NewLine + ncca.DefenseCounselAddress2, '') + @NewLine + 
    COALESCE(ncca.DefenseCounselCity + ', ', '')  + COALESCE(ncca.State, '') + ' ' + COALESCE(ncca.DefenseCounselZip, '') + COALESCE(('-' + ncca.DefenseCounselMailZip), '') + @NewLine + 
    COALESCE('Phone: ' + '(' + Substring(ncca.DefenseCounselPhone,1,3) + ') ' 
           + Substring(ncca.DefenseCounselPhone,4,3) + '-' 
           + Substring(ncca.DefenseCounselPhone,7,4), '') +
    --COALESCE(@NewLine + 'Fax: ' + '(' + Substring(dca.DefenseCounselFax,1,3) + ') ' 
    --       + Substring(dca.DefenseCounselFax,4,3) + '-' 
    --       + Substring(dca.DefenseCounselFax,7,4), '') + 
    COALESCE(@NewLine + 'Email: ' + p.Email, '') ELSE '' END AS NCCAddress, 
    CASE WHEN a.MaterialID NOT IN (3,4,7,8) THEN 'His contact information is as follows: ' ELSE '' END as NCCAdj,
    --CASE WHEN nm.PersonID = 161 THEN 'Her ' ELSE 'His ' END + 'contact information is as follows: ' AS NCCAdj,
    CASE WHEN nm.PersonID = 3398 THEN 'is MSA''s National Coordinating Counsel for asbestos and silica claims.' ELSE '' END AS NCCClaimType,
    @WithdrawnTenderBlockName AS WithdrawnTenderBlock
		  
	FROM tblTenderHistory th
	INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
	INNER JOIN tblClaimLawsuit cl ON th.ClaimID = cl.ClaimID
	INNER JOIN tblClaim claim ON th.ClaimID = claim.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON claim.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	INNER JOIN tblNCCMaterial nm ON a.MaterialID = nm.MaterialID
	LEFT JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN vtblPerson p ON nm.PersonID = p.PersonID
	LEFT JOIN tblDefenseCounselAddress ncca ON p.FirmAddressID = ncca.DefenseCounselAddressID
	LEFT JOIN tblJurisdiction j ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblState s ON l.StateID = s.StateID
	LEFT JOIN tblCourtType ct ON l.CourtTypeID = ct.CourtTypeID
	LEFT JOIN tblDocuments d ON th.ComplaintDocumentID = d.DocumentID
	LEFT JOIN tblTenderType tt ON th.TenderTypeID = tt.TenderTypeID
	INNER JOIN tblTenderTemplate ttemp ON th.TenderBlockID = ttemp.TenderBlockID AND th.TenderTypeID = ttemp.TenderTypeID AND a.MaterialID = ttemp.MaterialID
	WHERE th.TenderHistoryID = @TenderHistoryID
	AND ttemp.TenderTemplateID = @TenderTemplateID
	


END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderReview_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderReview_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryTenderReview_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderReview_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderReview_REVERT] TO [power_user]
GO
