SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[qryrptBILLByPaydateByPolicyDetail] 
(@LimitTypeID int = null, @bill_id  int = 0, @BillDate1 datetime = 0, @BillDate2 datetime = 0, @ExpectedPayDate1 datetime = 0, @ExpectedPayDate2 datetime = 0)
AS

SET NOCOUNT ON
-- declare @bill_id int
-- declare @BillDate1 datetime
-- declare @BillDate2 datetime
-- declare @ExpectedPayDate1 datetime
-- declare @ExpectedPayDate2 datetime
-- set @bill_id = 1
-- set @BillDate1 = '1/1/2005'
-- set @BillDate2 = '1/1/2008'
-- set @ExpectedPayDate1 = '1/1/2005'
-- set @ExpectedPayDate2 = '1/1/2008'

DECLARE @maxLimitTypeID int
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy

SELECT pinfo.PolicyID, b.BillDate, b.PaymentDueDate as RemitDate, pinfo.CarrierName, pinfo.PolicyNum, pinfo.PStartDate, pinfo.PEndDate, pinfo.AggregateLimit,
	(CONVERT(int, pinfo.PEndDate) - CONVERT(int,PStartDate)) AS NumberOfDays,
	SUM(e.IndemnityAllocation + e.DefenseAllocation + e.DefenseOutsideLimitsAllocation) AS TotalAllocation,
	b.BillID, o.occurrencetype, o.OccurrenceNote, o.OccurrenceID, TriggerStart2, TriggerEnd2,
	CONVERT(int, TriggerEnd2) - CONVERT(int, TriggerStart2) AS TotalTriggerDays,
	SUM(e.IndemnityAllocation + e.DefenseAllocation + e.DefenseOutsideLimitsAllocation)/CASE WHEN o.OccurrenceTypeID BETWEEN 1 AND 4 THEN [Cost1] ELSE [Cost3] END AS '%TriggeredDays', 
	o.ExpectedPayDate, CASE WHEN o.OccurrenceTypeID BETWEEN 1 AND 4 THEN 'Defense' ELSE 'Indemnity' END AS DefenseOrIndem, 
	CASE WHEN o.OccurrenceTypeID BETWEEN 1 AND 4 THEN [Cost1] ELSE [Cost3] END AS Cost
FROM vExhaustion e INNER JOIN vBilling b ON e.LedgerID = b.LedgerID 
	INNER JOIN vPolicy pinfo ON e.PolicyDateID = pinfo.PolicyDateID 
	INNER JOIN vOccurrence o ON e.OccurrenceID = o.OccurrenceID 
WHERE b.BillID = @bill_id AND (BillDate BETWEEN @BillDate1 AND @BillDate2) AND
	(ExpectedPayDate BETWEEN @ExpectedPayDate1 AND @ExpectedPayDate2)
	AND LimitTypeID >= COALESCE(@LimitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@LimitTypeID, 0), @maxLimitTypeID )
GROUP BY b.BillDate, pinfo.PolicyID, pinfo.CarrierName, pinfo.PolicyNum, pinfo.PStartDate,
	pinfo.PEndDate, pinfo.AggregateLimit, b.Debit, b.Credit, b.BillID,
	o.OccurrenceType, o.OccurrenceNote, o.OccurrenceID, TriggerStart2, TriggerEnd2, o.ExpectedPayDate,
	o.OccurrenceTypeID, Cost1, Cost3, b.PaymentDueDate
ORDER BY BillDate

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBILLByPaydateByPolicyDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBILLByPaydateByPolicyDetail] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptBILLByPaydateByPolicyDetail] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBILLByPaydateByPolicyDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBILLByPaydateByPolicyDetail] TO [power_user]
GO
