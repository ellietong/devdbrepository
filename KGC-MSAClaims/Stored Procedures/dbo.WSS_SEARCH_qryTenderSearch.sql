SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SEARCH_qryTenderSearch]
(@ClaimLawsuitStatusGroupID int,  @StateID int, @PlaintiffCounselID bigint, 
 @emailaddress nvarchar(250), @ClaimTypeID int, @TenderBlockID int,
@TenderCarrierID int, @FileStart date, @FileEnd date, @SettleStart date, @SettleEnd date, @ExposureStart date, @ExposureEnd date,
@NCCs nvarchar_list_tbltype READONLY)
AS
BEGIN



--DECLARE @ClaimLawsuitStatusgroupID int
--SET @ClaimLawsuitStatusgroupID = null
--DECLARE @StateID int
--SET @StateID = null
--DECLARE @PlaintiffCounselID bigint
--SET @PlaintiffCounselID = null
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'hubbardb@kcicllc.com' 
--DECLARE @ClaimTypeID int
--SET @ClaimTypeID = null
--DECLARE @TenderBlockID int
--SET @TenderBlockID = null
--DECLARE @TenderCarrierID int
--SET @TenderCarrierID = null
--DECLARE @FileStart date
--SET @FileStart = null
--DECLARE @FileEnd date
--SET @FileEnd = NULL
--DECLARE @SettleStart date
--SET @SettleStart = '1/1/2011'
--DECLARE @SettleEnd date
--SET @SettleEnd = '4/1/2012'
--DECLARE @ExposureStart date
--SET @ExposureStart = null
--DECLARE @ExposureEnd date
--SET @ExposureEnd = null


DECLARE @NCCCount int = (SELECT COUNT(*) FROM @NCCs)

DECLARE @MaxTenderBlockID int
SET @MaxTenderBlockID = (SELECT MAX(TenderBlockID) from tblTenderBlock) 
DECLARE @MaxTenderCarrierID int
SET @MaxTenderCarrierID = (SELECT MAX(CarrierID) from tblPolicy)



DECLARE @MaxStateID int
SET @MaxStateID = (SELECT MAX(StateID) FROM tblState)
DECLARE @MaxPlaintiffCounselID int
SET @MaxPlaintiffCounselID = (SELECT MAX(PlaintiffCounselID) FROM tblPlaintiffCounselAddress)
DECLARE @MaxClaimLawsuitStatusGROUPID int
SET @MaxClaimLawsuitStatusGROUPID = (SELECT MAX(ClaimLawsuitStatusGROUPID) FROM tblClaimLawsuitStatusGroup)
DECLARE @MaxClaimTypeID int
SET @MaxClaimTypeID = (SELECT MAX(MaterialID) FROM tblMaterial)


DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)


DECLARE @ColumnListComma nvarchar(MAX)
DECLARE @ColumnListPlus nvarchar(MAX)



SET @ColumnListComma = (
SELECT LEFT(DiseaseList, LEN(DiseaseList)-1) FROM (
SELECT  DISTINCT
            '[' + Disease + ']' + ', ' AS [text()] 
        FROM 
            tblDisease
        FOR XML PATH('')  ) dl (DiseaseList))
        
SET @ColumnListPlus = (
SELECT LEFT(DiseaseList, LEN(DiseaseList)-1) FROM (
SELECT DISTINCT
            'CASE WHEN COALESCE([' + Disease + '], 0) = 1 THEN ''' + REPLACE(Disease, '''', '"') + ', '' ELSE '''' END + ' AS [text()] 
        FROM 
            tblDisease
        FOR XML PATH('')  ) dl (DiseaseList))
        
        
DECLARE @Command nvarchar(MAX)

SET @Command = 'SELECT ClaimID, ' + @ColumnListPlus  +

' AS Diseases FROM

(SELECT DISTINCT cd.ClaimID, Disease, COUNT(cd.ClaimDiseaseID) AS DiseaseCount FROM tblDisease d
LEFT JOIN (SELECT * FROM tblClaimDisease) cd ON cd.DiseaseID = d.DiseaseID
GROUP BY cd.ClaimID, Disease
HAVING COUNT(cd.ClaimDiseaseID) > 0
)
    AS dlist
PIVOT

(

    Sum(DiseaseCount)

FOR

Disease

    IN ( ' + @ColumnListComma + ')

) AS piv
WHERE ClaimID IS NOT NULL'

IF object_id('tempdb..#ClaimDisease') IS NOT NULL
BEGIN
   DROP TABLE #ClaimDisease
END

CREATE TABLE  #ClaimDisease (ClaimID bigint, Diseases nvarchar(MAX))
INSERT INTO #ClaimDisease 
EXEC(@Command)

--If no filters are selected, run modified query that does not include filters to increase speed per irrational Gleason needs to run unfiltered report on a regular basis.


DECLARE @WhereString nvarchar(max)
SET @WhereString = ''


 IF @ClaimLawsuitStatusgroupID is not null begin set @WhereString = @WhereString + ' AND cls.ClaimLawsuitStatusGroupID >= ' + convert(nvarchar,COALESCE(@ClaimLawsuitStatusGroupID, 0))+' AND cls.ClaimLawsuitStatusGroupID <= ' + convert(nvarchar,COALESCE(@ClaimLawsuitStatusGroupID,@MAXClaimLawsuitStatusGroupID)) END
IF @StateID is not null begin set @WhereString = @WhereString + ' AND l.StateID >= ' + convert(nvarchar,COALESCE(@StateID, 0))+' AND l.StateID <= ' + convert(nvarchar,COALESCE(@StateID, @MAXStateID))END              
IF @PlaintiffCounselID is not null begin set @WhereString = @WhereString + ' AND coalesce(PCA.PlaintiffCounselID,0) >= ' + convert(nvarchar,COALESCE(@PlaintiffCounselID, 0))+' AND coalesce(PCA.PlaintiffCounselID,0) <= ' + convert(nvarchar,COALESCE(@PlaintiffCounselID,@MaxPlaintiffCounselID))  END               
IF @ClaimTypeID is not null begin set @WhereString = @WhereString + ' AND a.MaterialID >= ' + convert(nvarchar,COALESCE(@ClaimTypeID, 0))+' AND a.MaterialID <= ' + convert(nvarchar,COALESCE(@ClaimTypeID,@MaxClaimTypeID))  END                 
IF @TenderBlockID is not null begin set @WhereString = @WhereString + ' AND coalesce(th.tenderblockid,0) >= ' + convert(nvarchar,COALESCE(@tenderblockID, 0))+' AND coalesce(th.tenderblockID,0) <= ' + convert(nvarchar,COALESCE(@tenderblockID,@MAXtenderblockID)) END              
IF @TenderCarrierID is not null begin set @WhereString = @WhereString + ' AND coalesce(p.carrierid,0) >= ' + convert(nvarchar,COALESCE(@tendercarrierID, 0))+' AND coalesce(p.carrierid,0) <= ' + convert(nvarchar,COALESCE(@tendercarrierID,@MaxTenderCarrierID)) END                
IF @FileEnd is not null or @FileStart is not null begin set @WhereString = @WhereString + ' AND Coalesce(servicedate, filedate, receiveddate, ''1/1/1900'')  >= ''' + convert(nvarchar,coalesce(@FileStart, '1/1/1900'))+'''  AND Coalesce(servicedate, filedate, receiveddate, convert(date, getdate())) <= ''' + convert(nvarchar,coalesce(@FileEnd, convert(date, getdate()))) +'''' END                  
IF @SettleStart is not null or @SettleEnd is not null begin set @WhereString = @WhereString + ' AND Coalesce(SettlementDate, clst2.StatusDate, clst3.StatusDate,''1/1/1900'') >= ''' + convert(nvarchar,coalesce(@SettleStart, '1/1/1900'))+''' and coalesce(SettlementDate, clst2.StatusDate, clst3.StatusDate, convert(date, getdate())) <= ''' + convert(nvarchar,coalesce(@SettleEnd,convert(date, getdate())))+'''' END                  
IF @ExposureEnd is not null or @ExposureStart is not null begin set @WhereString = @WhereString + ' AND COALESCE(ProdStartDate, EmpStartDate, ''1/1/1900'') >= ''' + convert(nvarchar,COALESCE(@ExposureStart, '1/1/1900'))+''' AND   coalesce(ProdStartDate, EmpStartDate, CONVERT(Date, GETDATE())) <= ''' + convert(nvarchar,COALESCE(@ExposureEnd, CONVERT(date, getdate())))+'''' END             



                       
                  
                        
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
'SELECT   distinct   C.ClaimID, NCCReferenceID, cpi.ClaimantPersonalInfoID, --DHLD.HighLevelDisease AS Disease, 
                        S.State, J.Jurisdiction,  CASE WHEN Suffix IS NULL THEN cpi.LastName + '', '' ELSE cpi.LastName + '' '' + Suffix + '', '' END + CASE WHEN FirstName IS NULL 
							  THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
                                      BirthDate, CPI.DeceasedDate, cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) 
                                      AS FileDate, L.DocketNumber, L.CaseCaption, L.LeadPlaintiffLastName + coalesce('', '' + L.LeadPlaintiffFirstName,'''') AS LeadPlaintiffName, 
                                      pca.PlaintiffCounsel, DefenseCounsel AS PrimaryDefenseCounsel,  
                                      cl.ActiveTrialDate, a.MaterialID as ClaimTypeID, Material as ClaimType,
                                      case when cls.ClaimLawsuitStatusID in (22,29) then null else   clst.StatusDate end AS ResolutionDate,
                                      TenderBlockName_Reporting as TenderBlockName, SettlementAmount, LEFT(cd1.Diseases, LEN(cd1.Diseases)-1) AS Diseases,
                                      coalesce(ProdStartDate, EmpStartDate) as EarliestExposure, CASE WHEN COALESCE(SettlementAuth, 0) = 0 THEN ''NO'' ELSE ''YES'' END AS RequestForSettlement
      FROM         dbo.tblClaimantPersonalInfo AS CPI 
                        INNER JOIN dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID 
                        INNER JOIN dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID 
                        INNER JOIN dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID 
                        LEFT JOIN tblClaimantSuffix cs ON cpi.SuffixID = cs.SuffixID
                        left join tblplaintiffcounselAddress pca on pca.plaintiffcounseladdressid = L.NationalPlaintiffCounselAddressID 
                        left JOIN dbo.tblState AS S ON S.StateID = L.StateID 
                        left JOIN dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID 
                        LEFT JOIN dbo.tblDefenseCounselAddress DCA ON l.DefenseCOunselAddressID = dca.DefenseCounselAddressID
                        INNER JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE IsPrimaryStatus = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID 
                        INNER JOIN dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
                        LEFT OUTER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
                        Left join tblMaterial m on m.MaterialID = a.MaterialID
                        left join (select thist.*, cth.TenderStatusID, cth.ClaimID from tblTenderHistory thist INNER JOIN tblClaimTenderHistory cth ON thist.TenderHistoryID = cth.TenderHistoryID where tenderstatusid in (2, 5) and tendertypeid in (1,3)) th on th.ClaimID = c.ClaimID
                        left join tblTenderBlock  tb on tb.TenderBlockID = th.TenderBlockID
                        left join tblTenderBlockPolicy tbp on tbp.TenderBlockID = tb.TenderBlockID
                        left join tblPolicy p on p.PolicyID = tbp.PolicyID
                        left JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE IsPrimaryStatus = 1 and ClaimLawsuitStatusID = 23) clst2 ON cl.ClaimLawsuitID = clst2.ClaimLawsuitID
                        left JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE IsPrimaryStatus = 1 and ClaimLawsuitStatusID in (15,16,17,18,19,26,27,30,34)) clst3 ON cl.ClaimLawsuitID = clst3.ClaimLawsuitID
                        left join tblClaimantSettlement csett on csett.ClaimLawsuitID = clst2.ClaimLawsuitID
                        left JOIN #ClaimDisease cd1 ON c.ClaimID = cd1.ClaimID
                        left join (Select claimantpersonalinfoid, MIN(empstartdate) as EmpStartDate, MIN(cp.startdate) as ProdStartDate from  tblClaimantEmployment ce 
                                    left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID group by ClaimantPersonalInfoID) ce on ce.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID




                        left join tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID=cl.ClaimLawsuitID
                        ' 
       SET @QueryString = @QueryString+ 'left join tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
                        
      WHERE       coalesce(cls.ClaimLawsuitStatusGroupID,0) <> 2 AND C.TortTypeID = 1 ' +  @WhereString
      
      IF(@NCCCount > 0)
      BEGIN
			
		IF object_id('tempdb..#NCCs') IS NOT NULL
		BEGIN
		   DROP TABLE #NCCs
		END
		CREATE TABLE #NCCs (id int NOT NULL IDENTITY(1,1) PRIMARY KEY, NCCReferenceID nvarchar(250))
		INSERT INTO #NCCs
			SELECT value FROM @NCCs
		SET @QueryString = @QueryString + ' AND NCCReferenceID IN (SELECT NCCReferenceID FROM #NCCs) '
	  END


                  
                  
exec (@QueryString)


end


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryTenderSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryTenderSearch] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SEARCH_qryTenderSearch] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryTenderSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryTenderSearch] TO [power_user]
GO
