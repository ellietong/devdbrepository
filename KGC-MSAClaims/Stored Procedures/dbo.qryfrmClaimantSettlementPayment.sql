SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmClaimantSettlementPayment] (@ClaimantSettlementID int)
As
Set NoCount On

Select ClaimantSettlementID, SourceNumber, SourceDate, PaymentAmount, CheckPDF, IsFullyPaid
From tblPaymentSettlement ps
Inner Join tblCheck c on c.CheckID = ps.CheckID
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Where ClaimantSettlementID = @ClaimantSettlementID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementPayment] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmClaimantSettlementPayment] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementPayment] TO [power_user]
GO
