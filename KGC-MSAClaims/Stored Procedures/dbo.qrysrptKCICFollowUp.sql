SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create procedure [dbo].[qrysrptKCICFollowUp]
(@CorrespondenceID int)
as
SET NOCOUNT ON

--DECLARE @CorrespondenceID int
--SET @CorrespondenceID=1

DECLARE @Client table(correspondenceid int, followup nvarchar(4000), taskcategoryid int, taskcategory nvarchar(100), 
	completed bit, datecompleted date)
DECLARE @Lawfirm table (correspondenceid int, followup nvarchar(4000), taskcategoryid int, taskcategory nvarchar(100), 
	 completed bit, datecompleted date)
DECLARE @kcic table(correspondenceid int, followup nvarchar(4000), taskcategoryid int, taskcategory nvarchar(100), 
	completed bit, datecompleted date)
DECLARE @final table(correspondenceid int, followup nvarchar(4000), taskcategory nvarchar(100))
	
INSERT INTO @Client
select Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory,  completed, datecompleted
from tblCorrespondence cor
INNER JOIN tblFollowUp f on cor.CorrespondenceID = f.CorrespondenceID
INNER JOIN tblTaskCategoryLookup tc on f.TaskCategoryID = tc.TaskCategoryID
inner join tblInternalContact ic on f.InternalContactID = ic.InternalContactID
inner join tblInternalCompany ico on ic.InternalCompanyID = ico.InternalCompanyID
where ico.InternalCompanyID = 3 and Completed = 0

INSERT INTO @Lawfirm
select Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory, completed, datecompleted
from tblCorrespondence cor
INNER JOIN tblFollowUp f on cor.CorrespondenceID = f.CorrespondenceID
INNER JOIN tblTaskCategoryLookup tc on f.TaskCategoryID = tc.TaskCategoryID
inner join tblInternalContact ic on f.InternalContactID = ic.InternalContactID
inner join tblInternalCompany ico on ic.InternalCompanyID = ico.InternalCompanyID
where ico.InternalCompanyID = 2 and Completed = 0

INSERT INTO @kcic
select Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory,completed, datecompleted
from tblCorrespondence cor
INNER JOIN tblFollowUp f on cor.CorrespondenceID = f.CorrespondenceID
INNER JOIN tblTaskCategoryLookup tc on f.TaskCategoryID = tc.TaskCategoryID
inner join tblInternalContact ic on f.InternalContactID = ic.InternalContactID
inner join tblInternalCompany ico on ic.InternalCompanyID = ico.InternalCompanyID
where ico.InternalCompanyID = 1 and Completed = 0

Insert into @Final

select Cor.correspondenceid,
Case	when coalesce(p.followup,g.followup) is not null and k.followup IS null Then 'N/A' 
		Else k.followup end as Followup, k.taskcategory
from tblCorrespondence cor
Left JOIN @Lawfirm g on cor.CorrespondenceID=g.correspondenceid
Left JOIN @Client p on cor.CorrespondenceID=p.correspondenceid
Left JOIN @kcic k on cor.CorrespondenceID=k.correspondenceid
where cor.CorrespondenceID=@CorrespondenceID
Group by Cor.correspondenceid,k.taskcategory,p.followup, k.followup,g.followup

Select correspondenceid, followup , taskcategory
from @Final
Group By correspondenceid, followup , taskcategory

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptKCICFollowUp] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptKCICFollowUp] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptKCICFollowUp] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptKCICFollowUp] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptKCICFollowUp] TO [power_user]
GO
