SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_qrySettleDocs] (@SettlePaymentID int, @emailaddress nvarchar(100))
as

--DECLARE @SettlePaymentID int
--SET @SettlePaymentID = 23
--DECLARE @EmailAddress nvarchar(100)
--set @EmailAddress = 'hubbardb@kcicllc.com' 

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)

IF RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1
BEGIN
	Select DISTINCT 
	  DocumentTitle, 
	  PageNums, 
	  DocCategory, 
	  SettleDocID, 
	  sd.DocumentID, 
	  dc.DocCategoryID, 
	  sd.SetPymtID,
	  AmazonKey,
	  '/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=/clients/msa/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), cpi.ClaimantPersonalInfoID) AS DocumentURL,
	  CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical	 	  
	From tblSettleDocs sd 
	LEFT OUTER JOIN tblDocuments d ON sd.DocumentID = d.DocumentID
	LEFT OUTER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
	LEFT OUTER JOIN tblDocumentGroup dg ON cdt.DocumentGroupID = dg.DocumentGroupID
	LEFT OUTER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
	INNER JOIN (SELECT ClaimantPersonalInfoID, SetPymtID  
				FROM tblSettlePaymentDocumentation spd
				INNER JOIN  tblClaimLawsuit cl on spd.ClaimLawsuitID = cl.ClaimLawsuitID
				INNER JOIN  tblClaim c on cl.ClaimID = c.ClaimID) cpi ON sd.SetPymtID = cpi.SetPymtID		
	WHERE dg.DocumentGroupID NOT IN (2) --invoices
	AND sd.SetPymtID = @SettlePaymentID
	

END
ELSE
BEGIN


	Select DISTINCT 
	  DocumentTitle, 
	  PageNums, 
	  DocCategory, 
	  SettleDocID, 
	  sd.DocumentID, 
	  dc.DocCategoryID, 
	  sd.SetPymtID,
	  AmazonKey,
	  '/clients/iuna_nosroc/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=/clients/iuna_nosroc/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), cpi.ClaimantPersonalInfoID) AS DocumentURL,
	  CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical	 	  
	From tblSettleDocs sd 
	LEFT OUTER JOIN tblDocuments d ON sd.DocumentID = d.DocumentID
	LEFT OUTER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
	LEFT OUTER JOIN tblDocumentGroup dg ON cdt.DocumentGroupID = dg.DocumentGroupID
	LEFT OUTER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
	INNER JOIN (SELECT ClaimantPersonalInfoID, SetPymtID  
				FROM tblSettlePaymentDocumentation spd
				INNER JOIN  tblClaimLawsuit cl on spd.ClaimLawsuitID = cl.ClaimLawsuitID
				INNER JOIN  tblClaim c on cl.ClaimID = c.ClaimID) cpi ON sd.SetPymtID = cpi.SetPymtID		
	WHERE dg.DocumentGroupID NOT IN (2) AND cdt.ClaimantDocumentTypeID NOT IN (1, 40)
	AND sd.SetPymtID = @SettlePaymentID
	


END
Return


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySettleDocs] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qrySettleDocs] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySettleDocs] TO [power_user]
GO
