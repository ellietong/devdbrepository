SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmLimitType]
AS
SET NOCOUNT ON
SELECT limittypeid, limittype
FROM tblPolicyLimitType
ORDER BY limittype
RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLimitType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLimitType] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmLimitType] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLimitType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLimitType] TO [power_user]
GO
