SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[mqryImportApprovedInvoiceBatch] (@ImportDate date)
AS
BEGIN

--declare @ImportDate date
--set @ImportDate = '11/28/2011'


insert into tblDefenseInvoice (StatusID, FirmID, DefenseCounselAddressID, InvoiceNumber, Period, InvoiceTotal, 
				SubmitDate, PeriodStart, PeriodEnd, InvoiceTotal_Receivable, DatePaid)
select 6, dca.DefenseCounselID, dca.DefenseCounselAddressID, invoicenumber, CONVERT(varchar, periodstart) + ' - ' + convert(varchar, periodend),
		sum(costs), importdate, periodstart, periodend, sum(costs), PaidDate
from mtblinvoicebatchimport inv
left join DefCounselLookup def on def.Law_Firm = Inv.lawfirm
left join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = def.DefenseCounselAddressID
where inv.importdate = @ImportDate
group by dca.DefenseCounselID, dca.DefenseCounselAddressID, invoicenumber, CONVERT(varchar, periodstart) + ' - ' + convert(varchar, periodend),
		importdate, periodstart, periodend, PaidDate


insert into tblDefInvClaimantDetail (DefenseInvoiceID, ClaimID, FirmID, InvoiceNumber, InvoiceDate, Costs)
select di.DefenseInvoiceID, c.ClaimID, di.FirmID, di.InvoiceNumber, di.SubmitDate, sum(costs)
from mtblinvoicebatchimport inv
left join tblClaim c on c.NCCReferenceID = inv.nccreferenceid
left join DefCounselLookup def on def.Law_Firm = Inv.lawfirm
left join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = def.DefenseCounselAddressID
left join tblDefenseInvoice di on di.InvoiceNumber = inv.invoicenumber and di.FirmID = dca.DefenseCounselID
where inv.importdate = @Importdate
group by di.DefenseInvoiceID, c.ClaimID, di.FirmID, di.InvoiceNumber, di.SubmitDate

select * from tblDefenseInvoice where SubmitDate = @ImportDate

select * from tblDefInvClaimantDetail where InvoiceDate = @ImportDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[mqryImportApprovedInvoiceBatch] TO [base_user]
GRANT EXECUTE ON  [dbo].[mqryImportApprovedInvoiceBatch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[mqryImportApprovedInvoiceBatch] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[mqryImportApprovedInvoiceBatch] TO [Claims_User]
GRANT ALTER ON  [dbo].[mqryImportApprovedInvoiceBatch] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[mqryImportApprovedInvoiceBatch] TO [power_user]
GRANT EXECUTE ON  [dbo].[mqryImportApprovedInvoiceBatch] TO [power_user]
GO
