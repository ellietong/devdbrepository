SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryUpdClaimantSettlementInfo]
	-- Add the parameters for the stored procedure here
(@ClaimantPersonalInfoID int, @ClaimLawsuitID int, @LastName nvarchar(250), @FirstName nvarchar(250),
@MiddleName nvarchar(250), @SuffixID int, @SettlementAmount money, @SettlementDate date, @PaymentDueDate date,
@NotificationDate date, @TaxID nvarchar(250) = null, @EmailAddress nvarchar(250), @PlaintiffCounselAddressID int, @NationalPlaintiffCounselAddressID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @LawsuitID int
SET @LawsuitID = (SELECT LawsuitID FROM tblClaimLawsuit WHERE ClaimLawsuitID = @ClaimLawsuitID)

UPDATE tblClaimantPersonalInfo
SET FirstName = UPPER(@FirstName),
	MiddleName = UPPER(@MiddleName),
	LastName = UPPER(@LastName),
	SuffixID = @SuffixID,
	SPUsername = @EmailAddress
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID

IF @ClaimLawsuitID IN (Select ClaimLawsuitID FROM tblClaimantSettlement)
BEGIN
UPDATE tblClaimantSettlement
SET SettlementAmount = @SettlementAmount,
	SettlementDate = @SettlementDate,
	SettlementDueDate = @PaymentDueDate,
	SettlementNotificationDate = @NotificationDate,
	SPUsername = @EmailAddress
WHERE ClaimLawsuitID = @ClaimLawsuitID
END
ELSE
BEGIN
INSERT INTO tblClaimantSettlement (ClaimLawsuitID, SettlementAmount, SettlementDueDate, SettlementDate, SPUsername)
VALUES (@ClaimLawsuitID, @SettlementAmount, @PaymentDueDate, @SettlementDate, @EmailAddress)
END

UPDATE tblLawsuit	
SET NationalPlaintiffCounselAddressID = @NationalPlaintiffCounselAddressID,
	PlaintiffCounselAddressID = @PlaintiffCounselAddressID,
	SPUsername = @EmailAddress
WHERE LawsuitID = @LawsuitID	

UPDATE tblClaimLawsuitStatusTracking
SET IsPrimaryStatus = 0
WHERE ClaimLawsuitID = @ClaimLawsuitID

INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, ProcessedDate)
VALUES(@ClaimLawsuitID, 43, @SettlementDate, 1, GETDATE())
 	

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantSettlementInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantSettlementInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdClaimantSettlementInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantSettlementInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantSettlementInfo] TO [power_user]
GO
