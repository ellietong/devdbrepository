SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[WSS_qryDefenseInvoiceSummaryByFirm_Filtered] (@emailaddress nvarchar(250), @FilterFirmID int, @StatusID int)
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	 --SET NOCOUNT ON added to prevent extra result sets from
	 --interfering with SELECT statements.
	SET NOCOUNT ON;

--Declare @DefenseCounselAddressID int
--Set @DefenseCounselAddressID = null

--Declare @emailaddress nvarchar(250)
--Set @emailaddress = 'crookc@kcicllc.com'

--DECLARE @FilterFirmID int
--SET @FilterFirmID = null
--DECLARE @StatusID int
--SET @StatusID = NULL


DECLARE @MaxFirmID int
SET @MaxFirmID = (SELECT MAX(DefenseCounselID) FROM tblDefenseCounsel)
DECLARE @MaxStatusID int
SET @MaxStatusID = (SELECT MAX([StatusID]) FROM tblDefenseInvoiceStatus)


--Declare @UnpaidInvoice table (InvoiceID int, IsFullyPaid bit)
--Insert into @UnpaidInvoice

--Select	i.DefenseInvoiceID as InvoiceID, 
--		IsFullyPaid 
--From	tblDefenseInvoice i
--	Left Join tblPaymentDefense pd on i.DefenseInvoiceID = pd.DefenseInvoiceID
--Group By i.DefenseInvoiceID, IsFullyPaid
		
DECLARE @DefenseCounselID int
SET @DefenseCounselID =  (SELECT DefenseCounselID FROM tblDefenseCounselAddress WHERE DefenseCounselAddressID = @FilterFirmID)

		
DECLARE @firmid int
SET @firmid = (SELECT Case when RIGHT(@emailaddress, 15)='smithmurphy.com' then '115' else FirmID end FROM vtblperson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
				INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
				WHERE Email = @emailaddress)
					
IF  RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @firmid = 68 --@NoFilters = 1 OR 
BEGIN
	SELECT d.StatusID, FirmID, CASE WHEN InvoiceNumber = '' THEN 'Unknown' ELSE InvoiceNumber END AS InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, SubmitDate, DatePayable, CheckNumber, FundsRequestDate, 
	DatePaid, DocumentID, AdjustmentNotes, DefenseCounselAddressID, PeriodStart, PeriodEnd,
	CASE WHEN COALESCE(DisbursementAmount, 0)+COALESCE(Adjustment, 0)+COALESCE(FeeAmount, 0) = 0 THEN InvoiceTotal ELSE COALESCE(DisbursementAmount, 0)+COALESCE(Adjustment, 0)+COALESCE(FeeAmount, 0) END AS InvoiceTotal, dis.Status  
	FROM tblDefenseInvoice d
	LEFT JOIN tblDefenseInvoiceStatus dis ON COALESCE(d.StatusID, 3) = dis.StatusID
	WHERE FirmID <= COALESCE(@FilterFirmID, @MaxFirmID) AND FirmID >= COALESCE(@FilterFirmID, 0)
	AND COALESCE(d.StatusID, 0) <= COALESCE(@StatusID, @MaxStatusID) AND COALESCE(d.StatusID, 0) >= COALESCE(@StatusID, 0)
	
	UNION
	
	SELECT d.StatusID, FirmID, CASE WHEN InvoiceNumber = '' THEN 'Unknown' ELSE InvoiceNumber END AS InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, SubmitDate, DatePayable, CheckNumber, FundsRequestDate, 
	DatePaid, DocumentID, AdjustmentNotes, DefenseCounselAddressID, PeriodStart, PeriodEnd,
	CASE WHEN COALESCE(DisbursementAmount, 0)+COALESCE(Adjustment, 0)+COALESCE(FeeAmount, 0) = 0 THEN InvoiceTotal ELSE COALESCE(DisbursementAmount, 0)+COALESCE(Adjustment, 0)+COALESCE(FeeAmount, 0) END AS InvoiceTotal, dis.Status  
	FROM tblDefenseInvoice_Pre2008 d
	LEFT JOIN tblDefenseInvoiceStatus dis ON COALESCE(d.StatusID, 3) = dis.StatusID
	WHERE FirmID <= COALESCE(@FilterFirmID, @MaxFirmID) AND FirmID >= COALESCE(@FilterFirmID, 0)
	AND COALESCE(d.StatusID, 0) <= COALESCE(@StatusID, @MaxStatusID) AND COALESCE(d.StatusID, 0) >= COALESCE(@StatusID, 0)
	--AND d.DateCreated > DATEADD(yy, -1, GETDATE())
	ORDER BY PeriodStart DESC, PeriodEnd DESC, SubmitDate DESC
END
ELSE
BEGIN
	SELECT d.StatusID, FirmID, CASE WHEN InvoiceNumber = '' THEN 'Unknown' ELSE InvoiceNumber END AS InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, SubmitDate, DatePayable, CheckNumber, FundsRequestDate, 
	DatePaid, DocumentID, AdjustmentNotes, DefenseCounselAddressID, PeriodStart, PeriodEnd,
	CASE WHEN COALESCE(DisbursementAmount, 0)+COALESCE(Adjustment, 0)+COALESCE(FeeAmount, 0) = 0 THEN InvoiceTotal ELSE COALESCE(DisbursementAmount, 0)+COALESCE(Adjustment, 0)+COALESCE(FeeAmount, 0) END AS InvoiceTotal, dis.Status    
	FROM tblDefenseInvoice d
	LEFT JOIN tblDefenseInvoiceStatus dis ON COALESCE(d.StatusID, 3) = dis.StatusID
	WHERE FirmID <= COALESCE(@DefenseCounselID, @MaxFirmID) AND FirmID >= COALESCE(@DefenseCounselID, 0)
	AND COALESCE(d.StatusID, 0) <= COALESCE(@StatusID, @MaxStatusID) AND COALESCE(d.StatusID, 0) >= COALESCE(@StatusID, 0)
	--AND d.DateCreated > DATEADD(yy, -1, GETDATE())
	UNION
	SELECT d.StatusID, FirmID, CASE WHEN InvoiceNumber = '' THEN 'Unknown' ELSE InvoiceNumber END AS InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, SubmitDate, DatePayable, CheckNumber, FundsRequestDate, 
	DatePaid, DocumentID, AdjustmentNotes, DefenseCounselAddressID, PeriodStart, PeriodEnd,
	CASE WHEN COALESCE(DisbursementAmount, 0)+COALESCE(Adjustment, 0)+COALESCE(FeeAmount, 0) = 0 THEN InvoiceTotal ELSE COALESCE(DisbursementAmount, 0)+COALESCE(Adjustment, 0)+COALESCE(FeeAmount, 0) END AS InvoiceTotal, dis.Status    
	FROM tblDefenseInvoice_Pre2008 d
	LEFT JOIN tblDefenseInvoiceStatus dis ON COALESCE(d.StatusID, 3) = dis.StatusID
	WHERE FirmID <= COALESCE(@DefenseCounselID, @MaxFirmID) AND FirmID >= COALESCE(@DefenseCounselID, 0)
	AND COALESCE(d.StatusID, 0) <= COALESCE(@StatusID, @MaxStatusID) AND COALESCE(d.StatusID, 0) >= COALESCE(@StatusID, 0)
	ORDER BY PeriodStart DESC, PeriodEnd DESC, SubmitDate DESC
END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDefenseInvoiceSummaryByFirm_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDefenseInvoiceSummaryByFirm_Filtered] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryDefenseInvoiceSummaryByFirm_Filtered] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDefenseInvoiceSummaryByFirm_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDefenseInvoiceSummaryByFirm_Filtered] TO [power_user]
GO
