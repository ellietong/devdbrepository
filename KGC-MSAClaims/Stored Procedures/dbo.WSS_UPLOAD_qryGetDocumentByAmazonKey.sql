SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryGetDocumentByAmazonKey]
	@Key nvarchar(4000)
AS
BEGIN
	--SELECT COUNT(*) FROM tblDocuments WHERE AmazonKey LIKE @Key
	SELECT COUNT(*) FROM tblDocuments WHERE AmazonKey LIKE '%' + (SELECT LEFT(@Key, (CHARINDEX('_', @Key)-1))) + '%'
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetDocumentByAmazonKey] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetDocumentByAmazonKey] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_UPLOAD_qryGetDocumentByAmazonKey] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetDocumentByAmazonKey] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetDocumentByAmazonKey] TO [power_user]
GO
