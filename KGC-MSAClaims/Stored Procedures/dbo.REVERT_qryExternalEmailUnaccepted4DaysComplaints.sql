SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] 
AS
BEGIN

/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)


Declare @4DaysOld datetime
SET @4DaysOld = convert(datetime,convert(nvarchar,convert(date, GETDATE()-4))+' 16:00:00.000')



SET @Subject =    'MSA: Unaccepted Complaints/Documents Over 4 Days Old'
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (DocID int, docTitle nvarchar(500), FirmAddressID int, lName nvarchar(50), 
	fName nvarchar(50), ClaimantPersonalInfoID bigint, ServiceDate date, NCCID nvarchar(15))
INSERT INTO @NewComplaintsTable
SELECT d.DocumentID, '<a href="https://www.kcicextranet.com/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId='+COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle, 
	DefenseCounselAddressID, cpi.LastName, cpi.FirstName, cpi.ClaimantPersonalInfoID, max(ServiceDate), NCCReferenceID
	
FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	left join tblclaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblclaimlawsuit cl on cl.claimid = c.claimid
	left join tbllawsuit l on l.lawsuitid =cl.lawsuitid
WHERE mcd.QCDate < @4DaysOld
	AND detailDocumentTypeID IN (8,4,33)
	AND RIGHT(CreatedBy, 11) = 'kcicllc.com'
	and coalesce(Accepted,0) <> 1 
	AND COALESCE(IsPrimaryForClaim, 0) = 1
	AND COALESCE(IsCurrentClaim, 0) = 1
	AND TortTypeID = 1
GROUP BY d.documentID, d.DocumentTitle,
	 DefenseCounselAddressID, LastName, FirstName, cpi.ClaimantPersonalInfoID, NCCReferenceID

 
 DECLARE @NewDocumentsTable TABLE (DocID int, docTitle nvarchar(500), FirmAddressID int, lName nvarchar(50), 
	fName nvarchar(50), DocType nvarchar(50), ClaimantPersonalInfoID bigint, nccid nvarchar(15))
INSERT INTO @NewDocumentsTable
SELECT d.DocumentID, '<a href="https://www.kcicextranet.com/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId='+ COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle, 
	DefenseCounselAddressID, cpi.LastName, cpi.FirstName, ClaimantDocumentType, cpi.claimantpersonalinfoid, NCCReferenceID
FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	left join tblClaimantDocumentType ct on ct.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	left join tblclaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblclaimlawsuit cl on cl.claimid = c.claimid
	left join tbllawsuit l on l.lawsuitid =cl.lawsuitid
WHERE datecreated < GETDATE()-4
	AND detailDocumentTypeID IN (7,11,23,25,6)
	and RIGHT(CreatedBy, 11) = 'kcicllc.com'
	and coalesce(Accepted,0) <> 1 
	AND COALESCE(IsPrimaryForClaim, 0) = 1
	AND COALESCE(IsCurrentClaim, 0) = 1
GROUP BY d.DocumentID,  d.DocumentTitle,
	 DefenseCounselAddressID, LastName, FirstName, ClaimantDocumentType, cpi.claimantpersonalinfoid, NCCReferenceID
      
DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar(max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar(max), @ClaimantID bigint, @ClaimantList nvarchar(max)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for
SELECT DocID, COALESCE(Lname, '') + ', ' +  COALESCE(Fname, '') + coalesce(' ('+NCCID+')', ''), claimantpersonalInfoID FROM @NewComplaintsTable
UNION
SELECT DocID, COALESCE(Lname, '') + ', ' +  COALESCE(Fname, '')+ coalesce(' ('+NCCID+')', ''), claimantpersonalInfoID FROM @NewDocumentsTable
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
WHILE @@FETCH_STATUS = 0
BEGIN

IF(LEN(@ClaimantList)=0 OR @ClaimantList IS NULL)
BEGIN
	SET @ClaimantList = '<ul> <a href="https://www.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+'">'  + @Claimant + '</a></ul>'
END
ELSE
BEGIN
	SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="https://www.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+'">' + COALESCE(@Claimant, '') + '</a></ul>'

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
PRINT @@FETCH_STATUS
IF @DocID <> @LastDocID OR @@FETCH_STATUS <> 0
BEGIN
	INSERT INTO @ClaimantPerDoc
	SELECT @LastDocID, @ClaimantList
	SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints
      
      
--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

DECLARE @FirmAddressID nvarchar(10), @firmURL nvarchar(100)

DECLARE firm_Curs CURSOR FOR

SELECT nct.FirmAddressID, 
'https://www.kcicextranet.com/clients/msa/cp/Pages/Documents.aspx'
FROM @NewComplaintsTable nct
GROUP BY nct.FirmAddressID 
Union
Select doc.FirmAddressID, 
'https://www.kcicextranet.com/clients/msa/cp/Pages/Documents.aspx'
from @NewDocumentsTable doc
group by doc.FirmAddressID


OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmURL
WHILE @@FETCH_STATUS = 0
BEGIN

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH    {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
UL {margin:0; 
	padding:0;}	      
--->
</STYLE>'



--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
IF (Select COUNT(docTitle) from @NewComplaintsTable where FirmAddressID = @FirmAddressID) >0 
begin
set @BodyMessage = @BodyMessage + '<H4>The following complaints were uploaded more than 4 days ago and have not yet been accepted: </H4>'
SET @BodyMessage = @BodyMessage + N'<table>' + 
		N'<tr><th>Document Title</th><th>Linked Claimants (NCC#)</th><th>Date Served</th>' +
		CAST ( (
			SELECT td = docTitle, '', td = cl.ClaimantList, '', td = max(ServiceDate), ''

			FROM @NewComplaintsTable C
			INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
			WHERE c.FirmAddressID = (select @FirmAddressID)
			GROUP BY docTitle, cl.ClaimantList
			
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
		N'</table>' +  N'<br/>'		
end
ELSE
begin
Set @BodyMessage = @BodyMessage + '<H4>There are currently no outstanding complaints. </H4>'
end


IF (Select COUNT(docTitle) from @NewDocumentsTable where FirmAddressID = @FirmAddressID) >0 
begin		
Set @BodyMessage = @BodyMessage + '<H4>The following documents were uploaded more than 4 days ago and have not yet been accepted: </H4>'
		
SET @BodyMessage = @BodyMessage + N'<table>' + 
		N'<tr><th>Document Title</th><th>Linked Claimants (NCC#)</th><th>Document Type</th>' +
		CAST ( (
			SELECT  td = docTitle, '', td = cl.ClaimantList, '', td = DocType, ''

			FROM @NewDocumentsTable d
			INNER JOIN @ClaimantPerDoc cl ON d.DocID = cl.DocID
			WHERE FirmAddressID = (select @FirmAddressID)
			GROUP BY docTitle, cl.ClaimantList, DocType
			
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
		N'</table>' +  N'<br/>'				
end
ELSE
begin
Set @BodyMessage = @BodyMessage + '<H4>There are currently no other documents awaiting acceptance.</H4>'
end

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

  
DECLARE @RecipientList nvarchar(500)
SET @RecipientList=''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar(500)
DECLARE contact_curs CURSOR FOR

SELECT email 
FROM vtblPerson p
WHERE FirmAddressID = @FirmAddressID
      AND complaintContact = 1
      
OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS=0
BEGIN 

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs


--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The marleywylainProfile sends from marleywylain@kcicllc.com
@profile_name='MSAProfile',
@recipients = @RecipientList, --'Hubbardb@kcicllc.com',--
@copy_recipients = 'msa@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

--Move to the next firm to create a new email
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmurl
END
CLOSE firm_curs
DEALLOCATE firm_curs

END


GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] TO [base_user]
GRANT ALTER ON  [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GO
