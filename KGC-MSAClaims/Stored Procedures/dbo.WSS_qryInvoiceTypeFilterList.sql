SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[WSS_qryInvoiceTypeFilterList] (@clientFlag bit=0)

AS
SET NOCOUNT ON
--DECLARE @invoiceFlag bit
--SET @invoiceFlag = 0
--DECLARE @settlementFlag bit
--SET @settlementFlag = 0
IF @clientFlag = 1
	BEGIN
		Select 
			ClaimantDocumentTypeID, 
			ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID =2
		UNION
		SELECT null, '<< All Invoice Types >>', null
		ORDER BY ClaimantDocumentType
	END
ELSE
BEGIN
		Select 
			ClaimantDocumentTypeID, 
			ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID = 2 AND ClaimantDocumentTypeID NOT IN (41, 42)
		UNION
		SELECT null, '<< All Invoice Types >>', null
		ORDER BY ClaimantDocumentType
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInvoiceTypeFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInvoiceTypeFilterList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryInvoiceTypeFilterList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInvoiceTypeFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInvoiceTypeFilterList] TO [power_user]
GO
