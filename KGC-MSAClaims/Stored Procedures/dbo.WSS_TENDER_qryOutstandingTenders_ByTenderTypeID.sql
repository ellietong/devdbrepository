SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryOutstandingTenders_ByTenderTypeID]
(@TenderTypeID int)
	
AS
BEGIN
 
	SET NOCOUNT ON;
	
--Declare @TenderTypeID int = 3	

	DECLARE @ClaimantLastNames TABLE (TenderHistoryID bigint, Names nvarchar(MAX))
	
	
    --This needs to be updated to concatenate the first X number of last names associated with the tender into a field
	SELECT  LEFT(dl.list, LEN(dl.List)-1) + Case WHEN COUNT(c.ClaimantPersonalInfoID) > 3 THEN ' et al.' ELSE '' END AS Names,
			th.TenderHistoryID,
			TenderBlockName, 
			TT.TenderType, 
			CONVERT (date,DateCreated) AS 'TenderCreationDate',
			th.TenderHistoryID,
			th.TenderTypeID,
			TenderStatus
	FROM tblTenderHistory TH	
		INNER JOIN tblClaimTenderHistory cth ON TH.TenderHistoryID = cth.TenderHistoryID
		INNER JOIN tblTenderBlock TB ON TH.TenderBlockID = TB.TenderBlockID
		INNER JOIN tblTenderType TT ON TH.TenderTypeID = TT.TenderTypeID
		INNER JOIN tblClaim C ON cth.ClaimID = C.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit CL ON C.ClaimID=CL.ClaimID
		LEFT JOIN tblLawsuit L ON CL.LawsuitID=L.LawsuitID
		INNER JOIN tblTenderStatus ts ON cth.TenderStatusID = ts.TenderStatusID
		INNER JOIN tblState s ON L.StateID = s.StateID
		CROSS APPLY
				(
					SELECT TOP 3
					CONVERT(nvarchar, COALESCE(LastName + ', ', '') + COALESCE(LEFT(FirstName, 1)+'.', '')) + ', ' AS [text()] 
					FROM 
						tblClaimantPersonalInfo cpi1
					INNER JOIN tblClaim claim ON cpi1.ClaimantPersonalInfoID = claim.ClaimantPersonalInfoID
					INNER JOIN tblClaimTenderHistory cth1 ON claim.ClaimID = cth1.ClaimID
					WHERE cth1.TenderHistoryID = th.TenderHistoryID    
					ORDER BY 
						LastName 
					FOR XML PATH('') 
				) dl (list)
	WHERE DateApproved IS NULL
	AND th.TenderTypeID = @TenderTypeID
	GROUP BY dl.list,
	th.TenderHistoryID,
			TenderBlockName, 
			TT.TenderType, 
			CONVERT (date,DateCreated),
			th.TenderHistoryID,
			th.TenderTypeID,
			TenderStatus
	ORDER BY TenderCreationDate DESC
	OPTION (RECOMPILE)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryOutstandingTenders_ByTenderTypeID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryOutstandingTenders_ByTenderTypeID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryOutstandingTenders_ByTenderTypeID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryOutstandingTenders_ByTenderTypeID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryOutstandingTenders_ByTenderTypeID] TO [power_user]
GO
