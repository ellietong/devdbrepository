SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_updDocument]
(
	@DetailDocumentTypeID int,					--aka ClaimantDocumentTypeID
	@ClaimantPersonalInfoID bigint = null,	
	@Comments nvarchar(4000) = null,			--comments are optional		
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@MatterID int=null,					--this is the guid assigned by SharePoint
	@DocID int,
	@FirmID int,
	
	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,

	@ModifiedBy nvarchar(250),
	@DateModified datetime,
	@Approved bit = null,
	@ApprovedBy nvarchar(50) = null,
	@DateApproved	datetime = null,
	@DocumentSourceID int,
	@PrivateComments nvarchar(4000),
	@VendorFirmID int=null
)

AS
Update tblDocuments 
SET
	DetailDocumentTypeID	= @DetailDocumentTypeID,	
	ClaimantPersonalInfoID	= @ClaimantPersonalInfoID,
	Comments				= @Comments,	
	DocumentTitle			= @DocumentTitle,
	InvoiceNumber			= @InvoiceNumber,
	Period					= @Period,
	DatePayable				= @DatePayable,
	FeeAmount				= @FeeAmount,
	DisbursementAmount		= @DisbursementAmount,
	ModifiedBy				= @ModifiedBy,
	DateModified			= @DateModified,
	DocumentSourceID		= @DocumentSourceID,
	--PrivateComments         = @PrivateComments,
	MatterID				= @MatterID,
	FirmID					= @FirmID,
	VendorFirmID			= @VendorFirmID
	
WHERE DocumentID = @DocID

Select DocumentID From tblDocuments Where DocumentID = @DocID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_updDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_updDocument] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_updDocument] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_updDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_updDocument] TO [power_user]
GO
