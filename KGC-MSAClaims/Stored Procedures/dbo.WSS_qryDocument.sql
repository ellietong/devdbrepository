SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryDocument]
(
	@DocumentID int					--aka ClaimantDocumentTypeID
)
AS
SELECT
	DetailDocumentTypeID,
	d.ClaimantPersonalInfoID,
	Comments,
	DocumentURL,
	DocumentTitle,
	SPDocId,
	CreatedBy,
	d.DateCreated,
	d.InvoiceNumber,
	d.Period,
	d.DatePayable,
	ModifiedBy,
	DateModified,
	d.FeeAmount,
	d.DisbursementAmount,
    d.Adjustment,
    d.AdjustmentNotes,
    d.StatusID,
    d.FirmID, 
    --PrivateComments,
    DocumentSourceID,
    d.VendorFirmID,
    cdt.DocumentGroupID,
    IsFullyPaid,
    d.DocumentID,
    DismissalDate,
    QCDate AS DismissalProcessDate,
    d.ClaimLawsuitStatusID,
    NULL as Temphash,
    Accepted AS AutoAccept,
    DetailDocumentTypeID AS ClaimantDocumentTypeID,
    AmazonKey
FROM tblDocuments d
INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
	LEFT OUTER JOIN tblPaymentDefense pd on di.DefenseInvoiceID=pd.DefenseInvoiceID
	--LEFT OUTER JOIN m_tblVendorFirm vf ON d.VendorFirmID = vf.VendorFirmID 
	LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	LEFT OUTER JOIN tblClaimantDocketNumbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
WHERE d.DocumentID = @DocumentID AND COALESCE(IsFullyPaid, 0) <> 1


RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDocument] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryDocument] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDocument] TO [power_user]
GO
