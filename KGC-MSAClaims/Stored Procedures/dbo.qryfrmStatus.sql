SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create proc [dbo].[qryfrmStatus]
AS
SELECT StatusID, Status 
FROM tblStatus
ORDER BY Status 

return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmStatus] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmStatus] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmStatus] TO [power_user]
GO
