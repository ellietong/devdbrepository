SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_SEARCH_qryDocumentTypes](@emailaddress nvarchar(250))
AS

SET NOCOUNT ON

DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)

IF (@InsurerRole = 1)
BEGIN

SELECT cdt.ClaimantDocumentTypeID, cdt.ClaimantDocumentType
FROM tblSingleClaimantDocumentType sc
inner join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = sc.SingleClaimantDocumentTypeID
WHERE DocumentGroupID IN (1, 3, 4, 5, 6)
	AND cdt.ClaimantDocumentTypeID NOT IN (1,42,43,45,52)-- AND ClaimantDocumentTypeID <> 16 AND ClaimantDocumentTypeID <> 34
GROUP BY cdt.ClaimantDocumentTypeID, cdt.ClaimantDocumentType
UNION 
SELECT NULL, '<< All Document Types >>'
ORDER BY ClaimantDocumentType ASC

END
ELSE
SELECT cdt.ClaimantDocumentTypeID, cdt.ClaimantDocumentType
FROM tblSingleClaimantDocumentType sc
inner join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = sc.SingleClaimantDocumentTypeID
WHERE DocumentGroupID IN (1, 3, 4, 5, 6)
	AND cdt.ClaimantDocumentTypeID <> 1-- AND ClaimantDocumentTypeID <> 16 AND ClaimantDocumentTypeID <> 34
GROUP BY cdt.ClaimantDocumentTypeID, cdt.ClaimantDocumentType
UNION 
SELECT NULL, '<< All Document Types >>'
ORDER BY ClaimantDocumentType ASC

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentTypes] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentTypes] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SEARCH_qryDocumentTypes] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentTypes] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentTypes] TO [power_user]
GO
