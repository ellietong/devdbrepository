SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmBrokerRef]
(@BrokerID int = NULL)
AS
SET NOCOUNT ON

IF @BrokerID IS NULL

SELECT BrokerRefID, BrokerRefNum 
FROM tblBrokerRef 
ORDER BY BrokerRefNum

ELSE

SELECT br.BrokerRefID, BrokerRefNum 
FROM tblBrokerRef br 
INNER JOIN tblbrokerRefLink bl ON br.BrokerRefID = bl.brokerRefID
WHERE BrokerID = @BrokerID
ORDER BY BrokerRefNum

RETURN 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBrokerRef] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmBrokerRef] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmBrokerRef] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBrokerRef] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmBrokerRef] TO [power_user]
GO
