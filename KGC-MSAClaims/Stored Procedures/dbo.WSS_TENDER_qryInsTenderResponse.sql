SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryInsTenderResponse]
(@TenderHistoryID bigint, @TenderContactID int, @PersonID int = NULL, @DocumentID bigint)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF(@TenderContactID IS NOT NULL)
	BEGIN
		INSERT INTO tblTenderResponse (TenderHistoryID, TenderContactID, DocumentID)
		SELECT @TenderHistoryID, @TenderContactID, @DocumentID
	END
	IF(@PersonID IS NOT NULL)
	BEGIN
		INSERT INTO tblTender_Response (TenderHistoryID, PersonID, DocumentID)
		SELECT @TenderHistoryID, @PersonID, @DocumentID
	END

	----NOTE: Pre-Tender process update code follows (CY, 7/22/2013)
	--INSERT INTO tblTenderResponse (TenderHistoryID, TenderContactID, DocumentID)
	--SELECT @TenderHistoryID, @TenderContactID, @DocumentID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryInsTenderResponse] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryInsTenderResponse] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryInsTenderResponse] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryInsTenderResponse] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryInsTenderResponse] TO [power_user]
GO
