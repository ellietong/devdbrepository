SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptSubscriptionCoverageByCompany]
(@SubscriberID int = NULL, @Solvency varchar(50) = NULL, @InsuranceProgramID int = NULL, @limitTypeID int = Null)
AS 
SET NOCOUNT ON

--DECLARE @InsuranceProgramID int 
--DECLARE @SubscriberID int 
--DECLARE @Solvency varchar(50) 
--DECLARE @limitTypeID int
--SET @InsuranceProgramID = 0
--SET @SubscriberID = 0
--SET @Solvency = null
--SET @limitTypeID = 0

DECLARE @maxInsuranceProgramID int
DECLARE @maxSubscriberID int
DECLARE @maxLimitTypeID int

SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxInsuranceProgramID  = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxSubscriberID  = MAX(SubscriberID) FROM vSubscription

SELECT p.InsuranceProgram, s.SubscriptionCompany, s.SubscriptionCompanySolvency, 
	(s.SubscriptionActualPercentOfPolicy * p.PerOccLimit) AS SubscriberPerOccActual, 
	(s.SubscriptionAllocationPercentOfPolicy * p.PerOccLimit) AS SubscriberPerOccAllocation,
	p.CarrierID, p.InsuranceProgramID, 
	s.SubscriberID, SubscriptionSort
FROM vPolicy p
	INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID
WHERE p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) AND
	p.limitTypeID >= COALESCE(@limitTypeID, 0) AND p.limitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	s.SubscriberID >= COALESCE(@SubscriberID, 0) AND s.SubscriberID <= COALESCE(NULLIF(@SubscriberID, 0), @maxSubscriberID) AND
	s.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%')
	and DoNotReport <>1
GROUP BY p.InsuranceProgram, s.SubscriptionCompany, s.SubscriptionCompanySolvency,  
	(s.SubscriptionActualPercentOfPolicy * p.PerOccLimit), (s.SubscriptionAllocationPercentOfPolicy * p.PerOccLimit), 
	p.CarrierID, p.InsuranceProgramID, s.SubscriberID, SubscriptionSort
ORDER BY SubscriptionSort

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByCompany] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByCompany] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptSubscriptionCoverageByCompany] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByCompany] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByCompany] TO [power_user]
GO
