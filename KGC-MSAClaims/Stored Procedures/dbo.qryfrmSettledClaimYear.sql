SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmSettledClaimYear]
as

Select Year(SettlementDate) as SettlementYear
From tblClaimantSettlement
Where Year(SettlementDate) is not null
Group By Year(SettlementDate)
Order By Year(SettlementDate)

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettledClaimYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettledClaimYear] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmSettledClaimYear] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettledClaimYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettledClaimYear] TO [power_user]
GO
