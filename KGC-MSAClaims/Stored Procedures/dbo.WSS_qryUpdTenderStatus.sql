SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdTenderStatus] (@TenderHistoryID bigint, @TenderStatusID int, @Emailaddress nvarchar(250))
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblTenderHistory
	SET TenderStatusID = @TenderStatusID,
		SPUsername = @Emailaddress
	WHERE TenderHistoryID = @TenderHistoryID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdTenderStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdTenderStatus] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdTenderStatus] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdTenderStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdTenderStatus] TO [power_user]
GO
