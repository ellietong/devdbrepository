SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qryfrmAllocationScenarioCopy]
(@basescenario int)
as

set nocount on

declare @newAllocationID int

insert into tblAllocation (AllocationDescription, SaveResults, LimitTypeID, AllocationSchemeID, TriggerStartID, TriggerStartDate, TriggerEndID, TriggerEndDate, OccurrenceDefId, UnCoveredYearstoPH, 
VerticallyUnCollapse, Cost1, Cost2, Cost3, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, SIRID, SIRToPay, ConsiderPreviousExhaustion, ConsiderExhaustionCarterWallace, ExhaustionTypeID, 
ExhaustionDateID, LessThanAttachmentPoint, LessThanLayer, OccurrencesInClaim, Cost1Discount, Cost2Discount, Cost3Discount, Cost4Discount, Cost5Discount, Cost6Discount, Cost7Discount, Cost8Discount, Cost9Discount, 
Cost10Discount, Cost11Discount, Cost12Discount, AllocationNotes, ConsiderPreviousAllocation, PreviousAllocation, UnevenQSID, ProratedSIRAgg, DefenseScenarioID)

select left(allocationdescription, 243) + ' Copy of ' + convert(varchar, @basescenario), SaveResults, LimitTypeID, AllocationSchemeID, TriggerStartID, TriggerStartDate, TriggerEndID, TriggerEndDate, OccurrenceDefId, 
UnCoveredYearstoPH, VerticallyUnCollapse, Cost1, Cost2, Cost3, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, SIRID, SIRToPay, ConsiderPreviousExhaustion, ConsiderExhaustionCarterWallace, 
ExhaustionTypeID, ExhaustionDateID, LessThanAttachmentPoint, LessThanLayer, OccurrencesInClaim, Cost1Discount, Cost2Discount, Cost3Discount, Cost4Discount, Cost5Discount, Cost6Discount, Cost7Discount, Cost8Discount, 
Cost9Discount, Cost10Discount, Cost11Discount, Cost12Discount, AllocationNotes, ConsiderPreviousAllocation, PreviousAllocation, UnevenQSID, ProratedSIRAgg, DefenseScenarioID
from tblAllocation
where AllocationID = @basescenario

set @newAllocationID = (select max(AllocationID) from tblallocation)

-- MAIN ALLOCATION SCENARIO
insert into tblAllocationOccurrenceExclude (AllocationID, OccurrenceID)
select @newAllocationID, OccurrenceID
from tblAllocationOccurrenceExclude
where AllocationID = @basescenario
order by OccurrenceID

-- OCCURRENCE-RELATED TABLES
insert into tblAllocationOccurrenceGroupInclude (AllocationID, OccurrenceGroupID)
select @newAllocationID, OccurrenceGroupID
from tblAllocationOccurrenceGroupInclude
where AllocationID = @basescenario
order by OccurrenceGroupID

insert into tblAllocationOccurrenceTypeInclude (AllocationID, OccurrenceTypeID)
select @newAllocationID, OccurrenceTypeID
from tblAllocationOccurrenceTypeInclude
where AllocationID = @basescenario
order by OccurrenceTypeID

insert into tblAllocationSortOrder (AllocationID, SortOrderID)
select @newAllocationID, SortOrderID
from tblAllocationSortOrder
where AllocationID = @basescenario
order by SortOrderAllocationID

-- POLICY- AND PROVISION-RELATED TABLES
insert into tblAllocationPolicyExclude (AllocationID, PolicyID)
select @newAllocationID, PolicyID
from tblAllocationPolicyExclude
where AllocationID = @basescenario
order by PolicyID

insert into tblAllocationProvisionToExclude (AllocationID, ProvisionLabelID, ProvisionValueID)
select @newAllocationID, ProvisionLabelID, ProvisionValueID
from tblAllocationProvisionToExclude
where AllocationID = @basescenario
order by ProvisionLabelID, ProvisionValueID

return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationScenarioCopy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationScenarioCopy] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmAllocationScenarioCopy] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationScenarioCopy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationScenarioCopy] TO [power_user]
GO
