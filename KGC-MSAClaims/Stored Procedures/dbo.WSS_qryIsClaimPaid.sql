SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryIsClaimPaid]
(@ClaimID int)
AS
BEGIN
	--Swicthed from IsPaid to MedicareReported on 10/12/2012 per M. Potter's request in ticket #3085.  
	--Did not change the query name because it would have required a code update.
	SET NOCOUNT ON;

	SELECT MedicareReported FROM tblSettlePaymentDocumentation spd 
	LEFT JOIN tblClaimLawsuit cl ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	WHERE c.ClaimID = @ClaimID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryIsClaimPaid] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryIsClaimPaid] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryIsClaimPaid] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryIsClaimPaid] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryIsClaimPaid] TO [power_user]
GO
