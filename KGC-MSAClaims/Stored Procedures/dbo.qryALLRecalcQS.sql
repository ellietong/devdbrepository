SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryALLRecalcQS] (@AllocationID nvarchar(5), @DefenseType bit)

as

Set NoCount On

--Declare @AllocationID nvarchar(5)
--Declare @DefenseType bit
--Set @AllocationID = 1
--Set @DefenseType = 0


DECLARE @strsql nvarchar(4000)


If @DefenseType = 0 Begin -- Indemnity types

SET @strsql = ' Update ztblPolicyAllocation 
Set OccurrenceRemaining = 0
Where (OccurrenceRemaining < 0.25) and AllocationID = ' + @AllocationID + '


Declare @tblUpdatedLayerPerOcc table (PStartDate datetime, PEndDate datetime, AttachmentPoint money, InsuranceProgramID int, AllocationID int, UpdatedLayerPerOcc float)
Insert Into @tblUpdatedLayerPerOcc
Select PStartDate, 
	PEndDate, 
	AttachmentPoint, 
	InsuranceProgramID, 
	AllocationID, 
	sum(OccurrenceRemaining) as UpdatedLayerPerOcc
From ztblPolicyAllocation
Group By PStartDate, PEndDate, AttachmentPoint, AllocationID, InsuranceProgramID


Update ztblPolicyAllocation
Set LayerPerOccLimit = UpdatedLayerPerOcc
From ztblPolicyAllocation z Inner Join @tblUpdatedLayerPerOcc u on z.PStartDate = u.PStartDate 
										and z.PEndDate = u.PEndDate
										and z.AttachmentPoint = u.AttachmentPoint
										and z.InsuranceProgramID = u.InsuranceProgramID
										and z.AllocationID = u.AllocationID
Where z.AllocationID = ' + @AllocationID + '


Update ztblPolicyAllocation 
Set LayerPerOccLimit = PerOccLimit
Where (OccurrenceRemaining = 0) and AllocationID = ' + @AllocationID +'
						
UPDATE ztblPolicyAllocation
SET LayerPercentUneven1 = OccurrenceRemaining/LayerPerOccLimit
Where AllocationID = ' + @AllocationID 
						

--print @strSQL
EXEC sp_executesql @strSQL

End


If @DefenseType = 1  -- Defense
 Begin
SET @strsql = ' Update ztblPolicyAllocation 
Set OccurrenceRemaining = 0
Where (OccurrenceRemaining < 0.25) and AllocationID = ' + @AllocationID + '



Declare @tblUpdatedLayerPerOcc table (PStartDate datetime, PEndDate datetime, AttachmentPoint money, InsuranceProgramID int, 
AllocationID int, UpdatedLayerPerOcc float)
Insert Into @tblUpdatedLayerPerOcc
Select PStartDate, 
	PEndDate, 
	AttachmentPoint, 
	InsuranceProgramID, 
	AllocationID, 
	sum(OccurrenceRemaining) as UpdatedLayerPerOcc
From ztblPolicyAllocation
Where Provision1 <> 4
Group By PStartDate, PEndDate, AttachmentPoint, AllocationID, InsuranceProgramID


Update ztblPolicyAllocation
Set LayerPerOccLimit = UpdatedLayerPerOcc
From ztblPolicyAllocation z Inner Join @tblUpdatedLayerPerOcc u on z.PStartDate = u.PStartDate 
										and z.PEndDate = u.PEndDate
										and z.AttachmentPoint = u.AttachmentPoint
										and z.InsuranceProgramID = u.InsuranceProgramID
										and z.AllocationID = u.AllocationID
Where z.AllocationID = ' + @AllocationID + '


Update ztblPolicyAllocation 
Set LayerPerOccLimit = (Case When OccurrenceRemaining = 0 then PerOccLimit else OccurrenceRemaining end)
Where (Provision1 = 4 or OccurrenceRemaining = 0) and AllocationID = ' + @AllocationID


+'
						
UPDATE ztblPolicyAllocation
SET LayerPercentUneven1 = OccurrenceRemaining/LayerPerOccLimit
Where AllocationID = ' + @AllocationID 
						
--print @strSQL
EXEC sp_executesql @strSQL

End


Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLRecalcQS] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLRecalcQS] TO [base_user]
GRANT ALTER ON  [dbo].[qryALLRecalcQS] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLRecalcQS] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLRecalcQS] TO [power_user]
GO
