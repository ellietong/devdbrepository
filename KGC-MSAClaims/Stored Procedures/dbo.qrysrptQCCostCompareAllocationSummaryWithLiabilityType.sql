SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qrysrptQCCostCompareAllocationSummaryWithLiabilityType]
(@allocationid int)

As
Set NoCount On

--Declare @allocationid int
--Set @allocationid = 25

DECLARE @OccsConsidered TABLE (OccurrenceGroupID int, OccurrenceGroup nvarchar(1000), OccurrenceTypeID int, OccurrenceType nvarchar(1000))
INSERT INTO @OccsConsidered

SELECT o.OccurrenceGroupID, OccurrenceGroup, o.OccurrenceTypeID, OccurrenceType
FROM tblOccurrence o
LEFT JOIN tblOccurrenceGroup og ON o.OccurrenceGroupID = og.OccurrenceGroupID
LEFT JOIN tblOccurrenceType ot ON o.OccurrenceTypeID = ot.OccurrenceTypeID
WHERE o.OccurrenceGroupID IN (SELECT OccurrenceGroupID FROM tblAllocationOccurrenceGroupInclude WHERE AllocationID = @allocationid GROUP BY OccurrenceGroupID)
AND o.OccurrenceTypeID IN (SELECT OccurrenceTypeID FROM tblAllocationOccurrenceTypeInclude WHERE AllocationID = @allocationid GROUP BY OccurrenceTypeID)
GROUP BY o.OccurrenceGroupID, OccurrenceGroup, o.OccurrenceTypeID, OccurrenceType

DECLARE @OccsConsideredAndCosts TABLE (OccurrenceGroupID int, OccurrenceGroup nvarchar(1000), OccurrenceTypeID int, OccurrenceType nvarchar(1000), Cost1Disc float, Cost2Disc float, 
Cost3Disc float, Cost4Disc float, Cost5Disc float, Cost6Disc float, Cost7Disc float, Cost8Disc float, Cost9Disc float, Cost10Disc float, Cost11Disc float, Cost12Disc float)
INSERT INTO @OccsConsideredAndCosts

SELECT oc.OccurrenceGroupID, OccurrenceGroup, oc.OccurrenceTypeID, OccurrenceType, COALESCE(Cost1,0)*Cost1Discount, COALESCE(Cost2,0)*Cost2Discount, COALESCE(Cost3,0)*Cost3Discount,
COALESCE(Cost4,0)*Cost4Discount, COALESCE(Cost5,0)*Cost5Discount, COALESCE(Cost6,0)*Cost6Discount, COALESCE(Cost7,0)*Cost7Discount, COALESCE(Cost8,0)*Cost8Discount,
COALESCE(Cost9,0)*Cost9Discount, COALESCE(Cost10,0)*Cost10Discount, COALESCE(Cost11,0)*Cost11Discount, COALESCE(Cost12,0)*Cost12Discount
FROM @OccsConsidered oc, tblAllocation
WHERE AllocationID = @allocationid 


Declare @costconsidered Table (occurrencetypeid int, OccurrenceGroupID int, TotalCostsConsidered money, TotalDefenseConsidered money, TotalIndemnityConsidered money)
Insert into @costconsidered

Select o.Occurrencetypeid, o.OccurrenceGroupID, SUM((cost1*Cost1Disc) + (cost2*Cost2Disc) + (cost3*Cost3Disc) + (cost4*Cost4Disc) + (cost5*Cost5Disc) + (cost6*Cost6Disc) +
(cost7*Cost7Disc) + (cost8*Cost8Disc) + (cost9*Cost9Disc) + (cost10*Cost10Disc) + (cost11*Cost11Disc) + (cost12*Cost12Disc)) AS TotalCostsConsidered,
SUM((cost1*Cost1Disc) + (cost2*Cost2Disc)) AS TotalDefenseConsidered, 
SUM((cost3*Cost3Disc) + (cost4*Cost4Disc) + (cost5*Cost5Disc) + (cost6*Cost6Disc) + (cost7*Cost7Disc) + (cost8*Cost8Disc) + (cost9*Cost9Disc) + (cost10*Cost10Disc) + 
(cost11*Cost11Disc) + (cost12*Cost12Disc)) AS TotalIndemnityConsidered
From tblOccurrence o
INNER JOIN @OccsConsideredAndCosts oc ON o.OccurrenceGroupID = oc.OccurrenceGroupID AND o.OccurrenceTypeID = oc.OccurrenceTypeID
Group By o.Occurrencetypeid, o.OccurrenceGroupID


Declare @allocationtocoverage Table (OccurrenceTypeID int, OccurrenceGroupID int, TotalAllocatedToCoverage money, 
TotalDefenseToCoverage money, TotalIndemnityToCoverage money)
Insert into @allocationtocoverage

Select OccurrenceTypeid, OccurrenceGroupID, SUM(Allocation+SIRAllocated) AS TotalAllocatedToCoverage,
SUM(SIRAllocatedDefense+AllocationDefense+AllocationDefenseOutsideLimits) AS TotalDefenseToCoverage,
SUM(SIRAllocatedIndemnity+AllocationIndemnity) AS TotalIndemnityToCoverage
From tblOccurrence o 
Inner Join tblAllocationResults ar on o.OccurrenceID = ar.OccurrenceID
where ar.AllocationID = @allocationid 
Group by OccurrenceTypeid, OccurrenceGroupID


Declare @unallocatedvertical Table (OccurrenceTypeID int, OccurrenceGroupID int, TotalUncollapsedVertical money)
Insert into @unallocatedvertical

Select OccurrenceTypeid, OccurrenceGroupID, SUM(uncollapsedVertical) 
From tblOccurrence o 
Left Join tblAllocationResultsVertical arv on o.OccurrenceID=arv.OccurrenceID
where arv.AllocationID = @AllocationID 
Group by OccurrenceTypeid, OccurrenceGroupID


Declare @unallocatedhorizontal Table (OccurrenceTypeID int, OccurrenceGroupID int, TotalUncollapsedHorizontal money)
Insert into @unallocatedhorizontal

Select OccurrenceTypeid, OccurrenceGroupID, SUM(uncollapsedPost+uncollapsedPrior) 
From tblOccurrence o 
Left Join tblAllocationResultsHorizontal arh on o.OccurrenceID=arh.OccurrenceID
where arh.AllocationID = @allocationid 
Group by OccurrenceTypeid, OccurrenceGroupID


Select OccurrenceType, OccurrenceGroup, totalcostsconsidered as TotalCostsConsidered, 
Case When TotalUncollapsedHorizontal IS NULL Then COALESCE(TotalAllocatedToCoverage,0)+COALESCE(TotalUncollapsedVertical,0)
When TotalUncollapsedVertical IS NULL Then COALESCE(TotalAllocatedToCoverage,0)+COALESCE(TotalUncollapsedHorizontal,0)
When (TotalUncollapsedHorizontal IS NULL AND TotalUncollapsedVertical IS NULL) Then COALESCE(TotalAllocatedToCoverage,0) Else
COALESCE(TotalAllocatedToCoverage,0)+COALESCE(TotalUncollapsedVertical,0)+COALESCE(TotalUncollapsedHorizontal,0) END AS TotalCostsAllocated
From @OccsConsidered oc
Left Join @costconsidered cc on oc.OccurrenceTypeID=cc.occurrencetypeid AND oc.OccurrenceGroupID = cc.OccurrenceGroupID
Left Join @allocationtocoverage ac on oc.OccurrenceTypeID=ac.OccurrenceTypeID AND oc.OccurrenceGroupID = ac.OccurrenceGroupID
Left Join @unallocatedvertical uv on oc.OccurrenceTypeID=uv.OccurrenceTypeID AND oc.OccurrenceGroupID = uv.OccurrenceGroupID
Left Join @unallocatedhorizontal uh on oc.OccurrenceTypeID=uh.OccurrenceTypeID AND oc.OccurrenceGroupID = uh.OccurrenceGroupID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptQCCostCompareAllocationSummaryWithLiabilityType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptQCCostCompareAllocationSummaryWithLiabilityType] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptQCCostCompareAllocationSummaryWithLiabilityType] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptQCCostCompareAllocationSummaryWithLiabilityType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptQCCostCompareAllocationSummaryWithLiabilityType] TO [power_user]
GO
