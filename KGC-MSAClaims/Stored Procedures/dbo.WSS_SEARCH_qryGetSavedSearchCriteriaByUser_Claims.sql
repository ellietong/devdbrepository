SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims]
	-- Add the parameters for the stored procedure here
	@emailaddress nvarchar(500), @SavedSearchID int
AS
BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT Jurisdiction AS ParamValue, sj.JurisdictionID AS ID, 'JParam' AS ParamType 
FROM tblSavedJurisdiction sj
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = sj.SavedSearchID
	INNER JOIN tblJurisdiction j on sj.JurisdictionID = j.JurisdictionID
WHERE SPUserName = @emailaddress AND sj.SavedSearchID = @SavedSearchID
UNION
SELECT Material AS Param, ct.ClaimTypeID, 'CTParam'
FROM tblSavedClaimType ct
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = ct.SavedSearchID
	INNER JOIN tblMaterial mt ON ct.ClaimTypeID = mt.MaterialID
WHERE SPUserName = @emailaddress AND ct.SavedSearchID = @SavedSearchID
UNION
SELECT ClaimLawsuitStatusGroup AS Param2, ClaimLawsuitStatusGroupID AS LawsuitStatusID, 'LSParam' 
FROM tblSavedLawsuitStatus sls
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = sls.SavedSearchID
	INNER JOIN tblClaimLawsuitStatusGroup clsg ON sls.LawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID
WHERE SPUserName = @emailaddress AND sls.SavedSearchID = @SavedSearchID
UNION
SELECT ActiveTrialDateRange, '', 'ATDRParam'
FROM tblSavedTrialDateRange atdr
	INNER JOIN tblSavedSearch ss ON atdr.SavedSearchID = ss.SavedSearchID
WHERE SPUserName = @emailaddress AND atdr.SavedSearchID = @SavedSearchID
UNION
SELECT ProcessedDateRange, '', 'PDRParam'
FROM tblSavedProcessedDateRange pdr
	INNER JOIN tblSavedSearch ss ON pdr.SavedSearchID = ss.SavedSearchID
WHERE SPUserName = @emailaddress AND pdr.SavedSearchID = @SavedSearchID
UNION
SELECT StatusDateRange, '', 'SDRParam'
FROM tblSavedStatusDateRange sdr
	INNER JOIN tblSavedSearch ss ON sdr.SavedSearchID = ss.SavedSearchID
WHERE SPUserName = @emailaddress AND sdr.SavedSearchID = @SavedSearchID
UNION
SELECT State, s.StateID, 'StParam' 
FROM tblSavedState s
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = s.SavedSearchID
	INNER JOIN tblState st ON s.StateID = st.StateID
WHERE SPUserName = @emailaddress AND s.SavedSearchID = @SavedSearchID
UNION
SELECT PlaintiffCounsel, pc.PlaintiffCounselAddressID, 'PCParam' 
FROM tblSavedPlaintiffCounsel pc
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = pc.SavedSearchID
	INNER JOIN tblPlaintiffCounselAddress pca ON pc.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE SPUserName = @emailaddress AND pc.SavedSearchID = @SavedSearchID
UNION
SELECT TortType, stt.TortTypeID, 'TTParam' 
FROM tblSavedTortType stt
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = stt.SavedSearchID
	INNER JOIN tblTortType tt ON stt.TortTypeID = tt.TortTypeID
WHERE SPUserName = @emailaddress AND stt.SavedSearchID = @SavedSearchID



END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims] TO [power_user]
GO
