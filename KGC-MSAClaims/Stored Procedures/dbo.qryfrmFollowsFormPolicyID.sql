SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmFollowsFormPolicyID]
(@policyDateID int = NULL)

AS
SET NOCOUNT ON

SELECT *
FROM tblFollowsForm
WHERE PolicyDateID = @policyDateID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmFollowsFormPolicyID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmFollowsFormPolicyID] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmFollowsFormPolicyID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmFollowsFormPolicyID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmFollowsFormPolicyID] TO [power_user]
GO
