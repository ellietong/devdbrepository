SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryDismissalsByPlaintiffsCounselByMonth] 
	-- Add the parameters for the stored procedure here
	(@MonthID int, @YearID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @Month int
--Set @Month=11
--Declare @Year int
--Set @Year=2010

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

    -- Insert statements for procedure here
select coalesce(pca.PlaintiffCounsel, 'Unknown') as PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) as Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
left join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID

where	YEAR(ProcessedDate)=@year 
		and MONTH(ProcessedDate)=@Month
		and ClaimLawsuitStatusGroupID =4 
		and  IsPrimaryStatus=1
group by pca.PlaintiffCounsel
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, pca.PlaintiffCounsel


END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByPlaintiffsCounselByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByPlaintiffsCounselByMonth] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_REPORTS_qryDismissalsByPlaintiffsCounselByMonth] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByPlaintiffsCounselByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByPlaintiffsCounselByMonth] TO [power_user]
GO
