SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryUnlockAltExposure]
(@ClaimantPersonalInfoID bigint,
 @UnlockAltExposure bit, 
 @Username nvarchar(250)
 )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE tblClaimantPersonalInfo
    SET UnlockAltExposure = CASE WHEN UnlockAltExposure = 1 THEN 0 ELSE 1 END
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryUnlockAltExposure] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryUnlockAltExposure] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_EMPLOYMENT_qryUnlockAltExposure] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryUnlockAltExposure] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryUnlockAltExposure] TO [power_user]
GO
