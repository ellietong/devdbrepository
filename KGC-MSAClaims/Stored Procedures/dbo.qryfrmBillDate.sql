SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmBillDate]
as
Select convert(varchar,BillDate,110), BillID 
From tblBill
Group by BillDate, BillID
Order By BillDate
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmBillDate] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmBillDate] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillDate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmBillDate] TO [power_user]
GO
