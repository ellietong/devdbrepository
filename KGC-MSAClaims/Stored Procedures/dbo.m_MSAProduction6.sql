SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_MSAProduction6]
	
AS
BEGIN
	select d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL			
from tblMultiClaimantDocument mcd			
inner join tblDocuments d on mcd.DocumentID = d.DocumentID			
inner join tblSingleClaimantDocumentType sc on sc.ClaimantDocumentTypeID = d.DetailDocumentTypeID			
inner join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = sc.SingleClaimantDocumentTypeID			
inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID			
inner join tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID			
where cdt.ClaimantDocumentTypeID in (8,13,14,15,22,31,59) and  
NCCReferenceID in ('NCC00020134',
'NCC00020431',
'NCC00020526',
'NCC00020694',
'NCC00020662',
'NCC00020524',
'NCC00044654',
'NCC00021634',
'NCC00020122',
'NCC00021390',
'NCC00021386',
'NCC00021389',
'NCC00021387',
'NCC00044011',
'NCC00044173',
'NCC00044039',
'NCC00044833',
'NCC09000158',
'NCC00020206',
'NCC00044081'
)	
group by d.DocumentID,  d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL				
order by  d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[m_MSAProduction6] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_MSAProduction6] TO [base_user]
GRANT ALTER ON  [dbo].[m_MSAProduction6] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_MSAProduction6] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_MSAProduction6] TO [power_user]
GO
