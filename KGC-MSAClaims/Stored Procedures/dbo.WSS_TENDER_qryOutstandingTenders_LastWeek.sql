SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryOutstandingTenders_LastWeek]
(@Emailaddress nvarchar(250))
AS
BEGIN
	--/*Test variable*/
	--DECLARE @Emailaddress nvarchar(250) = 'youngc@kcicllc.com'
	SET NOCOUNT ON;

	DECLARE @QueryString nvarchar(MAX) ='
	SELECT	th.TenderHistoryID, 
			DOFE, 
			th.DocumentID,
			ServiceDate,
			cd.DateCreated,
			DocumentTitle,
			DocumentURL,
			LastName + '', '' + FirstName + COALESCE('' '' + MiddleName, '''') AS ClaimantName,
			c.ClaimantPersonalInfoID,
			AcknowledgementDate,
			p.Email,
			CarrierGroupName
	FROM tblTender_History th
		LEFT JOIN tblDocuments cd ON th.DocumentID = cd.DocumentID
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN tblTender_Acknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID
		--LEFT JOIN tblTenderTemplate tt ON th.TenderBlockID = tt.TenderBlockID AND th.TenderTypeID = tt.TenderTypeID
		LEFT JOIN vtblPerson p ON ta.PersonID = p.PersonID
		INNER JOIN vTenderContacts tc ON ta.PersonID = tc.PersonID
		INNER JOIN tblCarrierGroup cg ON tc.CarrierGroupID = cg.CarrierGroupID
		WHERE IsCurrentClaim = 1 AND cl.IsPrimaryForClaim = 1 
			AND (CONVERT(date, DateCreated) > CONVERT(date, DATEADD(dd, -7, GETDATE()))
					OR
				ta.AcknowledgementDate IS NULL)
			AND th.DocumentID IS NOT NULL'
	--Get current user's CarrierGroupID (if relevant)
	DECLARE @CarrierGroupID int
	SET @CarrierGroupID = (	SELECT tc.CarrierGroupID 
							FROM vTenderContacts tc
								INNER JOIN vtblPerson p ON tc.PersonID = p.PersonID
							WHERE p.Email = @Emailaddress )
				
	IF(RIGHT(@emailaddress, 11) <> 'kcicllc.com' OR RIGHT(@emailaddress, 8) <> 'kcic.com' AND @CarrierGroupID IS NOT NULL)
	BEGIN
		SET @QueryString = @QueryString + ' AND tc.CarrierGroupID = ' + CONVERT(nvarchar, @CarrierGroupID) + ''
	END
	SET @QueryString = @QueryString + ' ORDER BY AcknowledgementDate ASC'
		--TODO: If this is an insurer, filter to only ones with acknowledgements outstanding
	PRINT @QueryString
	EXEC sp_executesql @QueryString
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryOutstandingTenders_LastWeek] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryOutstandingTenders_LastWeek] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryOutstandingTenders_LastWeek] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryOutstandingTenders_LastWeek] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryOutstandingTenders_LastWeek] TO [power_user]
GO
