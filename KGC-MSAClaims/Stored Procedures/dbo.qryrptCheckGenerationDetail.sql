SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCheckGenerationDetail]

( @FirmID int, @CheckNo int)

As

SET NOCOUNT ON

DECLARE @maxFirmID int
Declare @MaxCheckNo int
SELECT @maxFirmID = MAX(firmID) from tblDefenseInvoiceFirm
SELECT @MaxCheckNo = MAX(CheckNo) from tblChecks


SELECT C.FirmID,Firm, PayTo, Period, SubmitDate, InvoiceTotal, FirmAddress, InvoiceNumber, FundsRequestDate,
C.CheckNo, C.AmountDesc, C.checkAmount, C.CheckDate,c.CommentLine
FROM	tblChecks C 
Join tblDefenseInvoice DI on C.CheckNo = DI.CheckNumber 
JOIN	tblDefenseInvoiceFirm DIF ON DI.FirmID = DIF.FirmID
JOIN	tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID

Where 
DI.FirmID >= COALESCE(@FirmID,0) AND DI.FirmID <= COALESCE(NULLIF(@FirmID,0),@MaxFirmID)
AND c.CheckNo>= COALESCE(@checkno,0) AND C.CheckNo <= COALESCE(NULLIF(@CheckNo,0),@MaxCheckNo)
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGenerationDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckGenerationDetail] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptCheckGenerationDetail] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGenerationDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckGenerationDetail] TO [power_user]
GO
