SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetClaimantsByDocumentID]
(@DocumentID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  CONVERT(nvarchar(20), CPI.ClaimantPersonalInfoID) AS ClaimantID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName,
                      CONVERT(nvarchar(20), MultiClaimantDocumentID) AS MultiClaimantDocumentID 
   FROM tblClaimantPersonalInfo cpi 
   INNER JOIN tblMultiClaimantDocument mcd ON cpi.ClaimantPersonalInfoID=mcd.ClaimantPersonalInfoID
   WHERE DocumentID = @documentID                   
   UNION
   SELECT  CONVERT(nvarchar(20), CPI.ClaimantPersonalInfoID) AS ClaimantID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName,
                      '' AS MultiClaimantDocumentID
   FROM tblClaimantPersonalInfo cpi
   INNER JOIN tblDocuments d ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
   WHERE DocumentID = @documentID    
   
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetClaimantsByDocumentID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetClaimantsByDocumentID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetClaimantsByDocumentID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetClaimantsByDocumentID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetClaimantsByDocumentID] TO [power_user]
GO
