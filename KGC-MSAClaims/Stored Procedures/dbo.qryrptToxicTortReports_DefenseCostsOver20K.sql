USE [KGC-MSAClaims]
GO
/****** Object:  StoredProcedure [dbo].[qryrptToxicTortReports_DefenseCostsOver20K]    Script Date: 2/26/2015 4:37:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[qryrptToxicTortReports_DefenseCostsOver20K](@DataDate DATETIME) 
AS
/*
Author: Chris Schilling
Date: 02/26/2015
Purpose: Fixed Ambiguous column name for InsurerFlag
Example: exec dbo.[qryrptToxicTortReports_DefenseCostsOver20K]
		 @DataDate = '2/27/2015'
*/
SET NOCOUNT ON
BEGIN
BEGIN TRY

--DECLARE @DataDate DATETIME
--SET @DataDate = '2/27/2015'

;WITH a as
(SELECT CASE WHEN c.InsurableFlag = 1 THEN 'Insurable' ELSE 'Uninsurable' END AS InsurableFlag,
			NCCReferenceID, 
			FirstName, 
			LastName,

			COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2009' AND @DataDate
				THEN Costs + Fees END),0) AS TotalDefense,
			COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2014' AND @DataDate
				THEN Costs + Fees END),0) AS Defense14,
			ClaimType


	FROM dbo.tblDefenseInvoice DI
			LEFT JOIN dbo.tblDefInvClaimantDetail DICD ON DICD.DefenseInvoiceID = DI.DefenseInvoiceID
			LEFT JOIN dbo.tblClaim C ON C.ClaimID = DICD.ClaimID
			LEFT JOIN dbo.tblClaimantPersonalInfo CPI ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
			LEFT JOIN dbo.tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
			LEFT JOIN dbo.tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID = CL.ClaimLawsuitID
			LEFT JOIN dbo.tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID
			LEFT JOIN dbo.tblMaterial M ON M.MaterialID = A.MaterialID
			LEFT JOIN dbo.m_tblClaimType_StandardMSA CTS ON CTS.MaterialID = M.MaterialID
		--	LEFT JOIN dbo.m_tblInsurableClaims IC ON IC.ClaimID = DICD.ClaimID

	WHERE COALESCE(ProcessedDate, '1/1/1900') <= @DataDate
		AND COALESCE(IsPrimaryStatus, 100) = 1
		AND COALESCE(ClaimLawsuitStatusID, 100) = 22
		AND COALESCE(NCCReferenceID, 'Unknown') NOT LIKE '%LIT%'
		AND COALESCE(NCCReferenceID, 'Unknown') NOT LIKE '%GENERAL%'
		AND COALESCE(M.MaterialID, 100) != 10

	GROUP BY CASE WHEN c.InsurableFlag = 1 THEN 'Insurable' ELSE 'Uninsurable' END, NCCReferenceID, FirstName, LastName, ClaimType

	HAVING COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2009' AND @DataDate tHEN Costs + Fees END),0) > 20000
)
,b AS (
SELECT NCCReferenceID, 
		MIN(ServiceDate) AS ServiceDate, 
		FullNameState

	FROM dbo.tblClaim C
		LEFT JOIN dbo.tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
		LEFT JOIN dbo.tblLawsuit L ON L.LawsuitID = CL.LawsuitID
		LEFT JOIN dbo.tblState S ON S. StateID = L.StateID

	WHERE COALESCE(FullNameState, '!') != '!'

	GROUP BY NCCReferenceID, FullNameState
				
)
SELECT InsurableFlag,
	COALESCE(a.NCCReferenceID, b.NCCReferenceID) AS NCC, 
	a.LastName + ', ' + a.FirstName AS ClaimantName, 
	a.TotalDefense, 
	a.Defense14, 
	b.FullNameState, 
	b.ServiceDate, 
	a.ClaimType, 
	@DataDate AS DD

FROM
 a
INNER JOIN 
 b

ON a.NCCReferenceID = b.NCCReferenceID

GROUP BY InsurableFlag,

	COALESCE(a.NCCReferenceID, b.NCCReferenceID), 
	a.LastName + ', ' + a.FirstName , 
	a.TotalDefense, 
	a.Defense14, 
	b.FullNameState, 
	b.ServiceDate, 
	a.ClaimType

ORDER BY InsurableFlag, FullNameState, COALESCE(a.NCCReferenceID, b.NCCReferenceID), a.LastName + ', ' + a.FirstName

END TRY
BEGIN CATCH
	SELECT ERROR_NUMBER()
		 ,ERROR_LINE()
		 ,ERROR_MESSAGE()
		 ,ERROR_SEVERITY()
		 ,ERROR_STATE()	
END CATCH
END
go