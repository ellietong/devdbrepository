SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryClaimantInfo](@ClaimantPersonalInfoID int = NULL, @emailaddress nvarchar(1000) = null)
AS 
SET NOCOUNT ON 


--DECLARE @ClaimantpersonalinfoID bigint
--SET @ClaimantPersonalInfoID = 9494
--DECLARE @Emailaddress nvarchar(250)
--SET @emailaddress = 'crookc@kcicllc.com'

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmaddressID FROM vtblPerson WHERE Email = @EmailAddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)

DECLARE @MaxCPI bigint
SET @MaxCPI = (SELECT MAX(ClaimantPersonalInfoID) FROM tblClaimantPersonalInfo)		
	
--If KCIC or no filters
IF RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'  OR @NoFilters = 1
BEGIN
	SELECT CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + cpi.FirstName AS Claimant,
		cpi.ClaimantPersonalInfoID, FirstName, CASE WHEN Suffix IS NULL THEN cpi.LastName ELSE cpi.LastName + ' ' +Suffix END AS LastName
	FROM dbo.tblLawsuit l
		INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblClaimantSuffix cs ON cpi.SuffixID = cs.SuffixID
	WHERE c.ClaimantPersonalInfoID  BETWEEN COALESCE(@ClaimantPersonalInfoID, 0) AND COALESCE(@ClaimantPersonalInfoID, @MaxCPI)
	GROUP BY CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' +Suffix + ', ' END + cpi.FirstName,
		cpi.ClaimantPersonalInfoID, FirstName, CASE WHEN Suffix IS NULL THEN cpi.LastName ELSE cpi.LastName + ' ' + Suffix END
	ORDER BY Claimant
END
--Insurer
ELSE IF (@InsurerRole = 1)
BEGIN
	SELECT CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + cpi.FirstName AS Claimant,
		cpi.ClaimantPersonalInfoID, FirstName, CASE WHEN Suffix IS NULL THEN cpi.LastName ELSE cpi.LastName + ' ' + Suffix END AS LastName
	FROM dbo.tblLawsuit l
		INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblClaimantSuffix cs ON cpi.SuffixID = cs.SuffixID
	WHERE c.ClaimantPersonalInfoID  BETWEEN COALESCE(@ClaimantPersonalInfoID, 0) AND COALESCE(@ClaimantPersonalInfoID, @MaxCPI)
	AND c.ClaimID not in (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
	AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
	GROUP BY CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + cpi.FirstName,
		cpi.ClaimantPersonalInfoID, FirstName, CASE WHEN Suffix IS NULL THEN cpi.LastName ELSE cpi.LastName + ' ' + Suffix END
	ORDER BY Claimant


END
--LDC
ELSE
BEGIN
	SELECT CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + cpi.FirstName AS Claimant,
		cpi.ClaimantPersonalInfoID, FirstName, CASE WHEN Suffix IS NULL THEN cpi.LastName ELSE cpi.LastName + ' ' + Suffix END AS LastName
	FROM dbo.tblLawsuit l
		INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
		LEFT JOIN tblClaimantSuffix cs ON cpi.SuffixID = cs.SuffixID
	WHERE c.ClaimantPersonalInfoID  BETWEEN COALESCE(@ClaimantPersonalInfoID, 0) AND COALESCE(@ClaimantPersonalInfoID, @MaxCPI) AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
	GROUP BY CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + cpi.FirstName,
		cpi.ClaimantPersonalInfoID, FirstName, CASE WHEN Suffix IS NULL THEN cpi.LastName ELSE cpi.LastName + ' ' + Suffix END
	ORDER BY Claimant
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimantInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantInfo] TO [power_user]
GO
