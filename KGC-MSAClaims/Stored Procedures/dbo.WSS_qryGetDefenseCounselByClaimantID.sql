SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetDefenseCounselByClaimantID]
(@ClaimantPersonalInfoID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DefenseCounselID, dca.DefenseCounselAddressID
	FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblDefenseJurisdiction dj ON l.DefenseCounselAddressID = dj.DefenseCounselAddressID AND l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.MatterID
	INNER JOIN tblDefenseCounselAddress dca ON dj.DefenseCounselAddressID = dca.DefenseCounselAddressID
	WHERE COALESCE(c.IsCurrentClaim, 0) = 1 AND COALESCE(IsPrimaryForClaim, 0) = 1 AND c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetDefenseCounselByClaimantID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetDefenseCounselByClaimantID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetDefenseCounselByClaimantID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetDefenseCounselByClaimantID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetDefenseCounselByClaimantID] TO [power_user]
GO
