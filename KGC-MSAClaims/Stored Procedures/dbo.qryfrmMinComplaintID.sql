SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



Create PROC [dbo].[qryfrmMinComplaintID] (@DateRec Date)
as

Select MIN(ComplaintID) as MinComplaintID
from tblComplaintTracking
where DateReceived >= @DateRec 
Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMinComplaintID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmMinComplaintID] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmMinComplaintID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMinComplaintID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmMinComplaintID] TO [power_user]
GO
