SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryUpdTenderDocID]
(@TenderHistoryID bigint, @DocumentID bigint, @EffectiveDate datetime)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE tblTenderHistory
    SET DocumentID = @DocumentID,
		EffectiveDate = @EffectiveDate
    WHERE TenderHistoryID = @TenderHistoryID
    
    --set status to "Awaiting Approval"
    UPDATE tblClaimTenderHistory
    SET TenderStatusID = 4 
    WHERE TenderHistoryID = @TenderHistoryID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryUpdTenderDocID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryUpdTenderDocID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryUpdTenderDocID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryUpdTenderDocID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryUpdTenderDocID] TO [power_user]
GO
