SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptQuarterlyClaimsReport_State_CNAOnly] (@Quarter int, @Year varchar(max), @NextYear varchar(max)) 
AS
SET NOCOUNT ON

--Declare @Quarter int
--Set @Quarter = 4
--Declare @Year nvarchar(25)
--Set @Year = '2014'
--Declare @NextYear nvarchar(25)
--Set @NextYear = '2015'

--get last quarter start date
DECLARE @PreviousQuarterStart varchar(25)
SET @PreviousQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '1/1/' + @Year
		 WHEN @Quarter = 2 THEN '4/1/' + @Year
		 WHEN @Quarter = 3 THEN '7/1/' + @Year
		 WHEN @Quarter = 4 THEN '10/1/' + @Year
		 ELSE '1/1/2050' END

--get first day of next quarter
DECLARE @NextQuarterStart varchar(25)
SET @NextQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '4/1/' + @Year
		 WHEN @Quarter = 2 THEN '7/1/' + @Year
		 WHEN @Quarter = 3 THEN '10/1/' + @Year
		 WHEN @Quarter = 4 THEN '1/1/' + @NextYear
		 ELSE '12/31/2050' END

If Object_ID('tempdb..#temp') is not null drop table #temp
Create Table #Temp (FullNameState nvarchar(max), Costs money, SettlementAmount money, OpenCount int, SettleCount int, DismissalCount int,
PendingCount int)

	INSERT INTO #Temp
		SELECT CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(FullNameState, 'Unknown') END AS FullNameState, 
		   SUM(paymentamount),0,0,0,0,0
	FROM dbo.tblDefenseInvoice(NOLOCK) di
		LEFT JOIN dbo.tblDefInvClaimantDetail(NOLOCK) dicd on dicd.DefenseInvoiceID = di.DefenseInvoiceID
		LEFT JOIN dbo.tblClaim(NOLOCK) c on c.ClaimID = dicd.ClaimID
		LEFT JOIN dbo.tblClaimLawsuit(NOLOCK) cl ON cl.claimID = c.claimID
		LEFT JOIN dbo.tblLawsuit(NOLOCK) l ON cl.lawsuitid = l.lawsuitid
		LEFT JOIN dbo.tblState(NOLOCK) s on s.StateID = l.StateID
	--	LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
		INNER JOIN dbo.tblPaymentDefense(NOLOCK) d on d.DefenseInvoiceID =di.DefenseInvoiceID 
		INNER JOIN dbo.tblcheck(NOLOCK) ch on ch.CheckID =d.CheckID 
	WHERE c.InsurableFlag = 1
	AND ((COALESCE(sourcedate,'1/1/1900') < @NextQuarterStart 
	      AND COALESCE(sourcedate,'1/1/1900')  >= @PreviousQuarterStart		
		  AND TortTypeID = 1)
	--This part is only for 4th Q 2014, remove for future runs
	OR	(di.QCDate > ch.SourceDate
	    and QCDate between '10/1/2014' and '1/1/2015'
	    ))
	GROUP BY CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(FullNameState, 'Unknown') END 

	--UNION ALL
	--This part is only for 4th Q 2014, remove for future runs

	--SELECT CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(FullNameState,'Unknown') END AS FullNameState, 
	--	   sum(PaymentAmount),0,0,0,0,0
	--FROM dbo.tblDefenseInvoice(NOLOCK) di
	--	LEFT JOIN dbo.tblDefInvClaimantDetail(NOLOCK) dicd on dicd.DefenseInvoiceID = di.DefenseInvoiceID
	--	LEFT JOIN dbo.tblClaim(NOLOCK) c on c.ClaimID = dicd.ClaimID
	--	LEFT JOIN dbo.tblClaimLawsuit(NOLOCK) cl ON cl.claimID = c.claimID
	--	LEFT JOIN dbo.tblLawsuit(NOLOCK) l ON cl.lawsuitid = l.lawsuitid
	--	LEFT JOIN dbo.tblState(NOLOCK) s on s.StateID = l.StateID
	--	--LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
	--	INNER JOIN dbo.tblPaymentDefense(NOLOCK) d on d.DefenseInvoiceID =di.DefenseInvoiceID 
	--	INNER JOIN dbo.tblcheck(NOLOCK) ch on ch.CheckID =d.CheckID
	--WHERE di.QCDate > ch.SourceDate
	--and QCDate between '10/1/2014' and '1/1/2015'
	--and c.InsurableFlag = 1
	--	GROUP BY CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(FullNameState,'Unknown') END 
	
	UNION ALL

	SELECT COALESCE(FullNameState, 'Unknown'),0, sum(PaymentAmount), 
		   0,0,0,0
	FROM dbo.tblClaimantPersonalInfo(NOLOCK) cpi 
		 LEFT JOIN  dbo.tblClaim(NOLOCK) c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN dbo.tblClaimLawsuit(NOLOCK) cl ON cl.claimID = c.claimID
		  LEFT JOIN dbo.tblLawsuit(NOLOCK) l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN dbo.tblClaimLawsuitStatusTracking(NOLOCK) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN dbo.tblClaimLawsuitStatus(NOLOCK) cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN dbo.tblJurisdiction(NOLOCK) j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN dbo.tblState(NOLOCK) s on s.StateID = l.StateID
		  LEFT JOIN dbo.tblAllegation(NOLOCK) a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN dbo.tblMaterial(NOLOCK) m on m.MaterialID = a.MaterialID
		  LEFT JOIN dbo.tblClaimantSettlement(NOLOCK) cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		 -- LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
		  INNER JOIN dbo.tblPaymentSettlement(NOLOCK) ps on ps.ClaimantSettlementID =cs.ClaimantSettlementID  
		  INNER JOIN dbo.tblcheck(NOLOCK) ch on ch.CheckID =ps.CheckID 
	WHERE COALESCE(sourcedate,'1/1/1900') < @NextQuarterStart AND COALESCE(sourcedate,'1/1/1900')  >= @PreviousQuarterStart
		AND c.InsurableFlag =1
		AND TortTypeID = 1 AND m.MaterialID != 10	 AND c.ClaimID != 26234
				  AND cls.ClaimLawsuitStatusGroupID = 1
		  		  --AND CLST.ClaimLawsuitStatusID != 43
				  and isPrimaryStatus = 1
	GROUP BY fullnamestate

	UNION ALL
	  
	SELECT COALESCE(FullNameState, 'Unknown'), 0,0,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 3 THEN 1 ELSE 0 END) as OpenCount,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 1 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END) as SettleCount,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 4 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END) as DismissalCount, 0
	FROM dbo.tblClaimantPersonalInfo(NOLOCK) cpi 
		  LEFT JOIN dbo.tblClaim(NOLOCK) c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN dbo.tblClaimLawsuit(NOLOCK) cl ON cl.claimID = c.claimID
		  LEFT JOIN dbo.tblLawsuit(NOLOCK) l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN dbo.tblClaimLawsuitStatusTracking(NOLOCK) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN dbo.tblClaimLawsuitStatus(NOLOCK) cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN dbo.tblJurisdiction(NOLOCK) j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN dbo.tblState(NOLOCK) s on s.StateID = l.StateID
		  LEFT JOIN dbo.tblAllegation(NOLOCK) a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN dbo.tblMaterial(NOLOCK) m on m.MaterialID = a.MaterialID
		  LEFT JOIN dbo.tblClaimantSettlement(NOLOCK) cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		--  LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID
		  LEFT JOIN dbo.tblPaymentSettlement(NOLOCK) ps on ps.ClaimantSettlementID =cs.ClaimantSettlementID  
		  LEFT JOIN dbo.tblcheck(NOLOCK) ch on ch.CheckID =ps.CheckID 
	WHERE cls.ClaimLawsuitStatusGroupID in (1,3,4)
		  AND CASE WHEN (
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 4 AND COALESCE(ProcessedDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END = 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 4 AND COALESCE(ProcessedDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1 OR
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 1 AND COALESCE(SourceDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END = 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 1 AND COALESCE(SourceDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1 OR
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 3 AND COALESCE(ServiceDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END= 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 3 AND COALESCE(ServiceDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1)
		  THEN 1 ELSE 0 END = 1
	      AND m.MaterialID != 10
		  AND c.InsurableFlag =1
		  AND isPrimaryStatus = 1
		  AND (CASE WHEN ClaimLawsuitStatusGroupID = 3 THEN 1 ELSE 0 END>0 
			  or CASE WHEN ClaimLawsuitStatusGroupID = 1 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END>0 
			  or CASE WHEN ClaimLawsuitStatusGroupID = 4 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END>0)
		  AND TortTypeID = 1
		  AND c.ClaimID != 26234
		  ----------AND NCCReferenceID NOT IN (SELECT NCCReferenceID 
				----------	FROM tblClaim C
				----------	LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
				----------	LEFT JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID = CL.ClaimLawsuitID
				----------	LEFT JOIN tblClaimLawsuitStatus CLS ON CLS.ClaimLawsuitStatusID = CLST.ClaimLawsuitStatusID
				----------	WHERE IsPrimaryStatus <> 1
				----------	AND ClaimLawsuitStatusGroupID = 4)
		  AND CLST.ClaimLawsuitStatusID <> 38
		  AND CL.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID = 38 AND IsPrimaryStatus = 1)
		  AND CL.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID = 29)
		  AND COALESCE(DocketNumber, '') NOT LIKE '%330307728B%'
		  AND CLST.ClaimLawsuitStatusID != 41  ----> Exclude Verdict claims
		  --AND CLST.ClaimLawsuitStatusID != 43  ----> Exclude Settlement in Principle - Awaiting Release claims
	GROUP BY FullNameState
	
	UNION ALL

	SELECT FullNameState, 0, 0, 0, 0, 0, 
		   count(distinct(cl.claimlawsuitid)) as PendingCount
	FROM dbo.tblClaimantPersonalInfo(NOLOCK) cpi 
		  LEFT JOIN dbo.tblClaim(NOLOCK) c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN dbo.tblClaimLawsuit(NOLOCK) cl ON cl.claimID = c.claimID
		  LEFT JOIN dbo.tblLawsuit(NOLOCK) l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN dbo.tblClaimLawsuitStatusTracking(NOLOCK) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN dbo.tblClaimLawsuitStatus(NOLOCK) cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN dbo.tblJurisdiction(NOLOCK) j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN dbo.tblState(NOLOCK) s on s.StateID = l.StateID
		  LEFT JOIN dbo.tblAllegation(NOLOCK) a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN dbo.tblMaterial(NOLOCK) m on m.MaterialID = a.MaterialID
		  LEFT JOIN dbo.tblClaimantSettlement(NOLOCK) cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		--  LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
	WHERE ((coalesce(filedate, servicedate, processeddate, statusdate, '1/1/1900') <= @NextQuarterStart)
	and (cls.ClaimLawsuitStatusGroupID in (3) and clst.ClaimLawsuitStatusID !=29 and IsPrimaryStatus = 1
		and c.TortTypeID =1 
	and c.InsurableFlag =1 AND m.MaterialID != 10))
	OR (cls.ClaimLawsuitStatusGroupID not in (3,2,5) and IsPrimaryStatus = 1 and Convert(Date,coalesce(processeddate, statusdate)) > @NextQuarterStart 
	and c.TortTypeID =1 
	and InsurableFlag =1 AND m.MaterialID != 10 ) 
	GROUP BY FullNameState 
	
	SELECT CASE WHEN FullNameState = 'Unknown' THEN 'General Expenses' ELSE FullNameState END AS FullNameState,
	   SUM(Costs) as Defense,
	   SUM(SettlementAmount) as Indemnity,
	   SUM(OpenCount) as OpenCount,
	   SUM(SettleCount) as SettledCount,
	   SUM(DismissalCount) as DismissedCount,
	   SUM(PendingCOunt) as PendingCount,
	   CASE WHEN FullNameState = 'Unknown' THEN 1
			WHEN FullNameState = 'General Expenses' THEN 2
		      ELSE 0 END AS SortOrder
	FROM #Temp
	GROUP by CASE WHEN FullNameState = 'Unknown' THEN 'General Expenses' ELSE FullNameState END,
		  CASE WHEN FullNameState = 'Unknown' THEN 1
				WHEN FullNameState = 'General Expenses' THEN 2
			      ELSE 0 END
	ORDER BY SortOrder, FullNameState

	RETURN


--select * from tblClaimLawsuitStatusGroup

--select * from tbltendertype

GO
