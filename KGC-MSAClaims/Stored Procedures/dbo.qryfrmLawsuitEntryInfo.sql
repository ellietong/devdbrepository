SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmLawsuitEntryInfo] 
as

Select *
From tblLawsuit
Order By FileDate desc, DocketNumber, CaseCaption


Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawsuitEntryInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawsuitEntryInfo] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmLawsuitEntryInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawsuitEntryInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawsuitEntryInfo] TO [power_user]
GO
