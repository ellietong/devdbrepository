SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_updDocumentByNCC]
(
	@DetailDocumentTypeID int,					--aka ClaimantDocumentTypeID
	@ClaimantPersonalInfoID bigint = null,	
	@Comments nvarchar(4000) = null,			--comments are optional		
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@MatterID int=null,					--this is the guid assigned by SharePoint
	@FirmID int,
	
	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,
    @Adjustment money = null,
    @AdjustmentNotes nvarchar(4000) = null,
    @StatusID int = null,

	@ModifiedBy nvarchar(250),
	@DateModified datetime,
	--@Approved bit = null,
	@ApprovedBy nvarchar(50) = null,
	@DateApproved	datetime = null,
	@DocID int=null,
	@DocumentSourceID int,
	@PrivateComments nvarchar(4000)=null,
	@VendorFirmID int=null
)

AS
BEGIN
Update tblDocuments 
SET
	DetailDocumentTypeID	= @DetailDocumentTypeID,	
	ClaimantPersonalInfoID	= @ClaimantPersonalInfoID,
	Comments				= @Comments,	
	DocumentTitle			= @DocumentTitle,	
	InvoiceNumber			= @InvoiceNumber,
	Period					= @Period,
	DatePayable				= CASE WHEN @StatusID IN (1, 2) AND VendorFirmID IS NOT NULL AND @VendorFirmID IS NOT NULL THEN GETDATE()+1 ELSE @DatePayable END,
	FeeAmount				= @FeeAmount,
	DisbursementAmount		= @DisbursementAmount,
    Adjustment				= @Adjustment,
    AdjustmentNotes			= @AdjustmentNotes,
    StatusID				= @StatusID,
	ModifiedBy				= @ModifiedBy,
	DateModified			= @DateModified,
	ApprovedBy				= @ApprovedBy,
	DateApproved			= @DateApproved,
	DocumentSourceID		= @DocumentSourceID,
	--PrivateComments			= @PrivateComments,
	MatterID				= @MatterID,
	FirmID					= @FirmID,
	VendorFirmID			= @VendorFirmID
	
WHERE DocumentID = @DocID


Select @DocID
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_updDocumentByNCC] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_updDocumentByNCC] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_updDocumentByNCC] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_updDocumentByNCC] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_updDocumentByNCC] TO [power_user]
GO
