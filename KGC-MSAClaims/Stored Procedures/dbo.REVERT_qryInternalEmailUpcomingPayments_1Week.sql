SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[REVERT_qryInternalEmailUpcomingPayments_1Week] 
AS
BEGIN


--/*Query run weekly to send a payment reminder for upcoming due dates. - ONB*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)

SET @Subject = 	
'MSA: Upcoming Settlement Due Dates for ' + CONVERT(varchar, getdate()+7, 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--Create a temp table for Payments which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @UpcomingPaymentsTable TABLE (ClaimantPersonalInfoID bigint, Claimant nvarchar(500), SettlementAmount nvarchar(100), SettlementDueDate nvarchar(100))
INSERT INTO @UpcomingPaymentsTable
	Select cpi.ClaimantPersonalInfoID, '<a href="https://admin.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), cpi.ClaimantPersonalInfoID) + '&ClaimantTab=3&ResolutionTab=1'+'">'  + LastName + ', ' + FirstName + coalesce(' ('+ CONVERT(nvarchar(50), NCCReferenceID) + ')', '') + '</a>' as Claimant, '$' + CONVERT(nvarchar(12),SettlementAmount,1), CONVERT(nvarchar(10),MONTH(SettlementDueDate))+'/'+CONVERT(nvarchar(10),DAY(SettlementDueDate))+'/'+CONVERT(nvarchar(10),YEAR(SettlementDueDate))
	from tblClaimantSettlement cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	where SettlementDueDate is not null
		  and PaidDate is null
		  and CONVERT(int,SettlementDueDate - CONVERT(date,GETDATE())) = 7

--Declare and populate the body of the email for NCC, Gleason, & ReedSmith
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @BodyMessage + '<H4>The following payment is due in one week: </H4>'
SET @BodyMessage = @BodyMessage + N'<table>' + 
	
N'<tr><th>ClaimantID</th><th>Claimant</th><th>Amount</th><th>Due Date</th>' +
	
CAST ( (
SELECT td = ClaimantPersonalInfoID, '', td = Claimant, '', td = SettlementAmount, '', td = SettlementDueDate, ''
FROM @UpcomingPaymentsTable
for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
N'</table>' +  N'<br/>'	


--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br>KCIC, LLC<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

DECLARE @RecipientList nvarchar(500)
SET @RecipientList='blairo@kcicllc.com'

IF((Select COUNT(ClaimantPersonalInfoID) from @UpcomingPaymentsTable) > 0)
BEGIN

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The MSAProfile sends from msa@kcicllc.com
@profile_name='Admin',
@recipients = 'msa@kcicllc.com',
@copy_recipients = '',
--@blind_copy_recipients = 'msa@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

END


END

GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalEmailUpcomingPayments_1Week] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalEmailUpcomingPayments_1Week] TO [base_user]
GRANT ALTER ON  [dbo].[REVERT_qryInternalEmailUpcomingPayments_1Week] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalEmailUpcomingPayments_1Week] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalEmailUpcomingPayments_1Week] TO [power_user]
GO
