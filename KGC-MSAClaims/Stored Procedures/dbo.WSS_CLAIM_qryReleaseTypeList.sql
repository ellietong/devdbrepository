SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIM_qryReleaseTypeList]
AS

SET NOCOUNT ON

SELECT ReleaseTypeID, 
	COALESCE(ReleaseType, '') AS ReleaseType
FROM tblReleaseType
WHERE ReleaseTypeID IN (2, 4, 5, 10)
UNION
SELECT NULL AS ReleaseTypeID, 
	'-- Release Type --' AS ReleaseType
ORDER BY ReleaseType

GO
