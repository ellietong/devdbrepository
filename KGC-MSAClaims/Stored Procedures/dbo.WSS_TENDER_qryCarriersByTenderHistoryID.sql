SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_TENDER_qryCarriersByTenderHistoryID] (@TenderHistoryID int, 
																 @emailaddress    nvarchar(250))
AS
BEGIN
SET NOCOUNT ON

--DECLARE @TenderHistoryID int = 9
--DECLARE @emailaddress nvarchar(250) = 'crookc@kcicllc.com'--'tpotocki@allstate.com'

DECLARE @NewLine nvarchar(5) = '<br/>'--CHAR(13) + CHAR(10)	

--Get CarrierGroupID of current user
DECLARE @CarrierGroupID int = (	SELECT tc.CarrierGroupID 
								FROM vtblPerson p
									INNER JOIN vTenderContacts tc ON p.PersonID = tc.PersonID
								WHERE Email = @Emailaddress)

SELECT DISTINCT tc.*, 
	CarrierGroupName, 
	COALESCE(FName, '') + COALESCE(' ' + LName, '') AS FullName, 
	COALESCE(FName, '') + COALESCE(' ' + LName, '') AS ContactName,
	COALESCE('<a href="mailto:' + Email + '">' + Email + '</a>', '') AS ContactInfo, 
	ta.AcknowledgementDate,
	ta.TenderHistoryID,
	tc.PersonID			
FROM vTenderContacts tc
	INNER JOIN tblCarrierGroup           cg  ON tc.CarrierGroupID = cg.CarrierGroupID
	INNER JOIN vtblPerson                p   ON tc.PersonID = p.PersonID
	LEFT  JOIN tblDefenseCounsel         dc  ON p.FirmID = dc.DefenseCounselID
	LEFT  JOIN tblTender_Acknowledgement ta  ON tc.PersonID = ta.PersonID
	LEFT  JOIN tblDefenseCounselAddress  dca ON p.FirmAddressID = dca.DefenseCounselAddressID
WHERE ta.TenderHistoryID = @TenderHistoryID
	AND COALESCE(@CarrierGroupID, 0) <= tc.CarrierGroupID
	AND COALESCE(@CarrierGroupID, 1000) >= tc.CarrierGroupID

SELECT c.CarrierName,
	p.CarrierGroupID,
	PolicyNum, 
	p.ActualPStartDate, 
	p.ActualPEndDate
FROM tblCarrier c
	INNER JOIN vPolicy                   p  ON c.CarrierID = p.CarrierID
	INNER JOIN vTenderContacts           tc ON c.CarrierGroupID = tc.CarrierGroupID
	INNER JOIN tblTender_Acknowledgement ta ON tc.PersonID = ta.PersonID
	INNER JOIN tblTender_History         th ON ta.TenderHistoryID = th.TenderHistoryID
	INNER JOIN tblDocuments              d  ON th.DocumentID = d.DocumentID
WHERE ta.TenderHistoryID = @TenderHistoryID
	AND PEndDate >= COALESCE(DOFE, '1/1/1900')
	AND DateCreated <= COALESCE(ExhaustedDate, '1/1/2100')
GROUP BY c.CarrierName,
	p.CarrierGroupID,
	PolicyNum,
	p.ActualPStartDate, 
	p.ActualPEndDate
ORDER BY ActualPStartDate
				
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryCarriersByTenderHistoryID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryCarriersByTenderHistoryID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryCarriersByTenderHistoryID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryCarriersByTenderHistoryID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryCarriersByTenderHistoryID] TO [power_user]
GO
