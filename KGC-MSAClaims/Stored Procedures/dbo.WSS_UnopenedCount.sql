SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_UnopenedCount] (@login nvarchar(100) = null)
AS

Declare @AllResponsibleDefenseCounsel table (DefenseCounselAddressID int, 
				DefenseCounsel nvarchar(100), 
				ResponsibleDefenseCounselAddressID int,
				StateID int, 
				ResponsibleDefenseCounsel nvarchar(100),
				IsActiveCounsel bit)
Insert Into @AllResponsibleDefenseCounsel
Select dcs.DefenseCounselAddressID,
	d.DefenseCounsel as DefenseCounsel,
	dcs.responsibledefensecounselAddressID,
	StateID as StateID,
	r.DefenseCOunsel as ResponsibleDefenseCOunsel,
	IsActiveCounsel
From tblDefenseCounselState dcs 
	Inner join tblDefenseCounselAddress da on da.DefenseCounselAddressID = dcs.defensecounselAddressID
	Inner Join tblDefenseCounsel d on d.defensecounselID = da.defensecounselID
	INNER JOIN tblDefenseCounselAddress dar on dar.DefenseCounselAddressID = dcs.responsibledefensecounseladdressid
	Inner Join tblDefenseCounsel r on r.defensecounselID = dar.defensecounselID
Where r.DefenseCounselID = (Select DefenseCounselID
From tblDefenseCounsel dc
Inner Join vtblPerson p ON p.FirmID = dc.DefenseCounselID
Where (email = @login))
Group By d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCOunseladdressID, dcs.responsibledefensecounseladdressID, IsActiveCounsel, StateID

SELECT State, count(documentid) AS Count
FROM tblDocuments d
	Left Join tblClaimantPersonalInfo CPI ON d.claimantpersonalinfoid = cpi.claimantpersonalinfoid
	Left Join tblClaim C on CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
	Left Join tblClaimLawsuit CL on C.ClaimID = CL.ClaimID
	Left Join tblLawsuit L on CL.LawsuitID = L.LawsuitID
	Left Join tblState S on S.StateID = L.StateID
	INNER Join @AllResponsibleDefenseCounsel r on r.DefenseCounselAddressID = l.DefenseCounselAddressID and r.StateID = l.StateID
WHERE Accepted IS NULL AND detaildocumenttypeid = 13 AND IsPrimaryForClaim = 1 and IsActiveCounsel = 1
GROUP BY state

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UnopenedCount] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UnopenedCount] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_UnopenedCount] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UnopenedCount] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UnopenedCount] TO [power_user]
GO
