SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_DefenseInvoiceLookup]
AS
SET NOCOUNT ON

SELECT statusid, status
FROM tblDefenseInvoiceStatus

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DefenseInvoiceLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DefenseInvoiceLookup] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_DefenseInvoiceLookup] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DefenseInvoiceLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DefenseInvoiceLookup] TO [power_user]
GO
