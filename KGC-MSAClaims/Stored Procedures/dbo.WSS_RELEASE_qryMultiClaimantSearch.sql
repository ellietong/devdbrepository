SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_RELEASE_qryMultiClaimantSearch] (@ClaimantName                      nvarchar(max), 
													@SSN                               nvarchar(20), 
													@StateID                           int, 
													@PlaintiffCounselAddressID         int,
													@emailaddress                      nvarchar(250),
													@DocketNumber                      nvarchar(250),
													--@ClaimLawsuitStatusGroupID         int,
													@NationalPlaintiffCounselAddressID int,
													@NCCReferenceID                    nvarchar(11),
													@MaterialID                        int,
													@ClaimantIDs                       integer_list_tbltype READONLY)
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantName                      nvarchar(max)
--DECLARE @SSN                               nvarchar(20)
--DECLARE @StateID                           int
--DECLARE @PlaintiffCounselAddressID         int
--DECLARE @emailaddress                      nvarchar(250)
--DECLARE @DocketNumber                      nvarchar(250)
----DECLARE @ClaimLawsuitStatusGroupID         int
--DECLARE @NationalPlaintiffCounselAddressID int
--DECLARE @NCCReferenceID                    int
--DECLARE @MaterialID                        int
--DECLARE @ClaimantIDs                       TABLE (n int)
--SET @ClaimantName                      = NULL
--SET @SSN                               = NULL
--SET @StateID                           = NULL
--SET @PlaintiffCounselAddressID         = NULL
--SET @emailaddress                      = 'youngc@kcicllc.com'
--SET @DocketNumber                      = NULL
----SET @ClaimLawsuitStatusGroupID         = NULL
--SET @NationalPlaintiffCounselAddressID = NULL
--SET @NCCReferenceID                    = NULL
--SET @MaterialID                        = NULL

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
IF object_id('tempdb..#ClaimantIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimantIDs
END
CREATE TABLE #ClaimantIDs (n int)
INSERT INTO #ClaimantIDs
	SELECT n FROM @ClaimantIDs

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE 1=1 
						AND clst.ClaimLawsuitStatusID IN (43)
						AND COALESCE(IsPrimaryForClaim, 0) = 1 
						AND COALESCE(IsCurrentClaim, 0) = 1 '
IF @ClaimantName			          IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND ((CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' end) like ''%'' + @ClaimantNameParam + ''%'')' END
IF @SSN						          IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(socialsecurityNumber, '''') LIKE ''%'' + @SSNParam + ''%''' END
IF @StateID					          IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.StateID = ' + CONVERT(nvarchar(20), @StateID) END
IF @PlaintiffCounselAddressID         IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(l.PlaintiffCounselAddressID,0) = ' + CONVERT(nvarchar(20),@PlaintiffCounselAddressID) END
IF @DocketNumber			          IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND DocketNumber LIKE ''%'' + @DocketNumberParam + ''%''' END
--IF @ClaimLawsuitStatusGroupID         IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND cls.ClaimLawsuitStatusGroupID = ' + CONVERT(nvarchar(20),@ClaimLawsuitStatusGroupID) END
IF @NationalPlaintiffCounselAddressID IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(l.NationalPlaintiffCounselAddressID,0) = ' + CONVERT(nvarchar(20),@NationalPlaintiffCounselAddressID) END
IF @NCCReferenceID                    IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(c.NCCReferenceID, '''') LIKE ''%'' + @NCCReferenceIDParam + ''%''' END
IF @MaterialID                        IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND a.MaterialID = ' + CONVERT(nvarchar(20),@MaterialID) END
IF (SELECT COUNT(n) FROM #ClaimantIDs) <> 0       BEGIN SET @WhereString = @WhereString + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)' END

--These IF statements determine additional WHERE statements by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @DefenseCounselAddressID int
DECLARE @NoFilters               bit
DECLARE @InsurerRole             bit
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)
SET @NoFilters               = (SELECT (dca.NoFilters | vp.NoFilters) FROM vtblPerson vp INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID WHERE Email = @emailaddress)
SET @InsurerRole             = (SELECT DISTINCT InsurerRole FROM vtblPerson WHERE Email = @emailaddress)

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	IF (@InsurerRole = 1)
	BEGIN
		SET @JoinString = ' LEFT JOIN (select th.*, cth.ClaimID, cth.ClaimTenderHistoryID, cth.PreviousClaimTenderHistoryID, cth.TenderStatusID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2) th on th.ClaimID = c.ClaimID '
		SET @WhereString = @WhereString + ' AND c.ClaimID NOT IN (SELECT cth.ClaimID FROM tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID WHERE TenderStatusID IN(2, 5) and TenderBlockID = 15 and TenderTypeID <> 2)
											AND COALESCE(TenderBlockID,0) <> 15
											AND COALESCE(cls.ClaimLawsuitStatusGroupID,0) <> 2 
											AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3)) '
	END
	ELSE
	BEGIN
		SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
							INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID = dca.DefenseCounselAddressID '
		SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	END
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT cpi.ClaimantPersonalInfoID, 
		cl.ClaimLawsuitID,
		CASE WHEN Suffix IS NULL THEN cpi.LastName + '', '' ELSE cpi.LastName + '' '' + Suffix + '', '' END + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' END AS ClaimantName, 
		NCCReferenceID,
		CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
		DocketNumber,
		s.[State],
		COALESCE(pca.PlaintiffCounsel, ''Unknown'') + '' ('' + COALESCE(pca.PlaintiffCounselCity, ''Unknown'') + '')'' As PlaintiffCounsel,
		COALESCE(npca.PlaintiffCounsel, ''Unknown'') + '' ('' + COALESCE(npca.PlaintiffCounselCity, ''Unknown'') + '')'' As NationalPlaintiffCounsel,
		Material,
		COALESCE(ClaimLawsuitStatus, '''') AS ClaimLawsuitStatus,		
		spd.SetPymtID
		
	FROM tblClaimantPersonalInfo cpi 
		INNER JOIN tblClaim c                                                                                ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT  JOIN tblClaimLawsuit CL                                                                        ON c.ClaimID = cl.ClaimID
		LEFT  JOIN tblLawsuit L                                                                              ON cl.LawsuitID = L.LawsuitID
		LEFT  JOIN tblMatter M                                                                               ON c.MatterID = M.MatterID
		LEFT  JOIN tblPlaintiffCounselAddress PCA                                                            ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		LEFT  JOIN tblPlaintiffCounselAddress NPCA                                                           ON l.NationalPlaintiffCounselAddressID = npca.PlaintiffCounselAddressID
		LEFT  JOIN tblState s                                                                                ON l.StateID = s.StateID
		LEFT  JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT  JOIN tblClaimLawsuitStatus cls                                                                 ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		INNER JOIN tblAllegation a                                                                           ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblMaterial mat                                                                           ON a.MaterialID = mat.MaterialID
		LEFT  JOIN tblClaimantSuffix cs                                                                      ON cpi.SuffixID = cs.SuffixID
		LEFT JOIN tblSettlePaymentDocumentation	spd															 ON cl.ClaimLawsuitID = spd.ClaimLawsuitID ' +
		@JoinString +
	@WhereString + ' 
	ORDER BY ClaimantName'

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@ClaimantNameParam nvarchar(max), @SSNParam nvarchar(max), @DocketNumberParam nvarchar(max), @NCCReferenceIDParam nvarchar(max)', @ClaimantNameParam = @ClaimantName, @SSNParam = @SSN, @DocketNumberParam = @DocketNumber, @NCCReferenceIDParam = @NCCReferenceID

GO
