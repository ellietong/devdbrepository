SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptClaimantTenderQC] (@ClaimID bigint)
AS

--DECLARE @ClaimID bigint = 22182

SELECT ClaimID,
	cth.TenderHistoryID,
	cth.TenderStatusID,
	TenderStatus,
	th.TenderTypeID,
	TenderType,
	th.TenderTemplateID,
	TemplateDisplayName,
	DateCreated,
	DateApproved
FROM tblClaimTenderHistory cth
	INNER JOIN tblTenderHistory  th    ON cth.TenderHistoryID = th.TenderHistoryID
	LEFT  JOIN tblTenderStatus   ts    ON cth.TenderStatusID  = ts.TenderStatusID
	LEFT  JOIN tblTenderType     ttype ON th.TenderTypeID     = ttype.TenderTypeID
	LEFT  JOIN tblTenderTemplate ttemp ON th.TenderTemplateID = ttemp.TenderTemplateID
WHERE ClaimID = @ClaimID
ORDER BY ClaimID, cth.TenderHistoryID
	
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantTenderQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantTenderQC] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptClaimantTenderQC] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantTenderQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantTenderQC] TO [power_user]
GO
