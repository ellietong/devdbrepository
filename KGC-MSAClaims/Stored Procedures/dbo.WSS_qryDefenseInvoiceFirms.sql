SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[WSS_qryDefenseInvoiceFirms] (@emailaddress nvarchar(250))
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	 --SET NOCOUNT ON added to prevent extra result sets from
	 --interfering with SELECT statements.
	 
	 --DECLARE @EmailAddress nvarchar(250)
	 --SET @EmailAddress = 'crookc@kcicllc.com'
	 
	SET NOCOUNT ON;
	
	DECLARE @DefenseCounselID int
	SET @DefenseCounselID =  (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)
	
	DECLARE @firmid int
	SET @firmid = (SELECT DISTINCT FirmAddressID FROM vtblperson WHERE Email = @emailaddress)
	
	DECLARE @NoFilters bit
	SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
IF RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @DefenseCounselID = 68 --@NoFilters = 1 OR 
	BEGIN

		SELECT DIStINCT COALESCE(DefenseCounselID, 0) AS DefenseCounselID,
		UPPER(DefenseCounsel) AS DefenseCounsel 
		FROM tblDefenseInvoice d
		LEFT JOIN tblDefenseCounselAddress dca ON d.FirmID = dca.DefenseCounselID
		--AND d.VendorFirmID <> 0
		ORDER BY DefenseCounsel
	END
	ELSE
	BEGIN
		SELECT DIStINCT COALESCE(DefenseCounselID, 0) AS DefenseCounselID,
		UPPER(DefenseCounsel) AS DefenseCounsel 
		FROM tblDefenseInvoice d
		LEFT JOIN tblDefenseCounselAddress dca ON d.FirmID = dca.DefenseCounselID
		WHERE d.FirmID = (SELECT DefenseCounselID FROM tblDefenseCounselAddress WHERE DefenseCounselAddressID = @FirmID)
		ORDER BY DefenseCounsel
	END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDefenseInvoiceFirms] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDefenseInvoiceFirms] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryDefenseInvoiceFirms] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDefenseInvoiceFirms] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDefenseInvoiceFirms] TO [power_user]
GO
