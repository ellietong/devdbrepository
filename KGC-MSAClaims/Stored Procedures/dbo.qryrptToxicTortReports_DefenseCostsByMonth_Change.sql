SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptToxicTortReports_DefenseCostsByMonth_Change](@DataDate DATETIME, @StartYear INT, @EndYear INT) 
AS
SET NOCOUNT ON

--DECLARE @DataDate DATETIME
--Set @DataDate = '6/18/2014'
--DECLARE @StartYear INT
--Set @StartYear = 2011
--DECLARE @EndYear INT
--Set @EndYear = 2012


SELECT COALESCE(a.Firm,b.Firm) AS Firm,
@StartYear AS StartYear, @EndYear AS EndYear, @DataDate AS DD,
SUM(a.Jan-b.Jan) AS JanSum, SUM(a.Feb-b.Feb) AS FebSum, SUM(a.Mar-b.Mar) AS MarSum, SUM(a.Apr-b.Apr) AS AprSum, SUM(a.May-b.May) AS MaySum, 
SUM(a.June-b.June) AS JuneSum,SUM(a.July-b.July) AS JulySum, SUM(a.Aug-b.Aug) AS AugSum, SUM(a.Sept-b.Sept) AS SeptSum, SUM(a.Oct-b.Oct) AS OctSum,
SUM(a.Nov-b.Nov) AS NovSum, SUM(a.Dece-b.Dece) AS DeceSum, SUM(a.Total-b.Total) AS TotalSum,
COALESCE(a.Flag, b.Flag) AS Flag

FROM
(
SELECT
	COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown') AS Firm,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 1 THEN Costs + Fees END),0) AS Jan,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 2 THEN Costs + Fees END),0) AS Feb,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 3 THEN Costs + Fees END),0) AS Mar,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 4 THEN Costs + Fees END),0) AS Apr,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 5 THEN Costs + Fees END),0) AS May,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 6 THEN Costs + Fees END),0) AS June,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 7 THEN Costs + Fees END),0) AS July,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 8 THEN Costs + Fees END),0) AS Aug,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 9 THEN Costs + Fees END),0) AS Sept,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 10 THEN Costs + Fees END),0) AS Oct,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 11 THEN Costs + Fees END),0) AS Nov,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear AND Month(PeriodEnd) = 12 THEN Costs + Fees END),0) AS Dece,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @EndYear THEN Costs + Fees END),0) AS Total,
		CASE WHEN COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown') = 'Unknown' THEN 2 ELSE 1 END AS Flag

FROM dbo.tblDefenseInvoice DI
	LEFT JOIN dbo.tblDefInvClaimantDetail DICD ON DICD.DefenseInvoiceID = DI.DefenseInvoiceID
	LEFT JOIN dbo.m_tblDefenseCounselLookup_ToxicTortReporting DCL ON DCL.DefenseCounselAddressID = DI.DefenseCounselAddressID
	LEFT JOIN dbo.tblClaim C ON C.ClaimID = DICD.ClaimID
	LEFT JOIN dbo.tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
	LEFT JOIN dbo.tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID
	LEFT JOIN dbo.tblMaterial M ON M.MaterialID = A.MaterialID
	LEFT JOIN dbo.m_tblClaimType_StandardMSA CTS ON CTS.MaterialID = M.MaterialID

WHERE COALESCE(NCCReferenceID, 'Unknown') NOT LIKE '%LIT%'
AND COALESCE(M.MaterialID, 100) != 10


GROUP BY COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown'), 
		CASE WHEN COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown') = 'Unknown' THEN 2 ELSE 1 END
)  AS a 

FULL OUTER JOIN

(
SELECT
	COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown') AS Firm,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 1 THEN Costs + Fees END),0) AS Jan,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 2 THEN Costs + Fees END),0) AS Feb,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 3 THEN Costs + Fees END),0) AS Mar,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 4 THEN Costs + Fees END),0) AS Apr,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 5 THEN Costs + Fees END),0) AS May,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 6 THEN Costs + Fees END),0) AS June,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 7 THEN Costs + Fees END),0) AS July,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 8 THEN Costs + Fees END),0) AS Aug,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 9 THEN Costs + Fees END),0) AS Sept,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 10 THEN Costs + Fees END),0) AS Oct,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 11 THEN Costs + Fees END),0) AS Nov,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear AND Month(PeriodEnd) = 12 THEN Costs + Fees END),0) AS Dece,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @StartYear THEN Costs + Fees END),0) AS Total,
		CASE WHEN COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown') = 'Unknown' THEN 2 ELSE 1 END AS Flag

FROM dbo.tblDefenseInvoice DI
	LEFT JOIN dbo.tblDefInvClaimantDetail DICD ON DICD.DefenseInvoiceID = DI.DefenseInvoiceID
	LEFT JOIN dbo.m_tblDefenseCounselLookup_ToxicTortReporting DCL ON DCL.DefenseCounselAddressID = DI.DefenseCounselAddressID
	LEFT JOIN dbo.tblClaim C ON C.ClaimID = DICD.ClaimID
	LEFT JOIN dbo.tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
	LEFT JOIN dbo.tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID
	LEFT JOIN dbo.tblMaterial M ON M.MaterialID = A.MaterialID
	LEFT JOIN dbo.m_tblClaimType_StandardMSA CTS ON CTS.MaterialID = M.MaterialID

WHERE COALESCE(NCCReferenceID, 'Unknown') NOT LIKE '%LIT%'
AND COALESCE(M.MaterialID, 100) != 10


GROUP BY COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown'), 
		CASE WHEN COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown') = 'Unknown' THEN 2 ELSE 1 END
)  AS b 

ON a.Firm = b.Firm

GROUP BY COALESCE(a.Flag,b.Flag), COALESCE(a.Firm,b.Firm)

ORDER BY COALESCE(a.Flag,b.Flag), COALESCE(a.Firm,b.Firm)

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptToxicTortReports_DefenseCostsByMonth_Change] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptToxicTortReports_DefenseCostsByMonth_Change] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptToxicTortReports_DefenseCostsByMonth_Change] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptToxicTortReports_DefenseCostsByMonth_Change] TO [power_user]
GO
