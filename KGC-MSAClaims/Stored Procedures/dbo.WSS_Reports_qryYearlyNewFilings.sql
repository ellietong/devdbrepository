SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_Reports_qryYearlyNewFilings] 
	-- Add the parameters for the stored procedure here
	(@YearID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @yearID int
--Set @YearId=2010

--DECLARE @PreviousMonthYTD int
--set @PreviousMonthYTD = case when @yearID = YEAR(getdate()) then (MONTH(GETDATE())-1) else 12 end



select COUNT(distinct(cl.ClaimLawsuitID))
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22)clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s on l.StateID=s.StateID
where year(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))=@YearID 
--and MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate)) <=@PreviousMonthYTD
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Reports_qryYearlyNewFilings] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Reports_qryYearlyNewFilings] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_Reports_qryYearlyNewFilings] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_Reports_qryYearlyNewFilings] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_Reports_qryYearlyNewFilings] TO [power_user]
GO
