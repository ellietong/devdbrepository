SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInsProduct]
(@ProductID bigint, @ClaimantEmploymentID bigint, @StartDate datetime, @EndDate datetime, @Emailaddress nvarchar(100), @Notes nvarchar(max),@SupportingDocID int, @SupportingPageNums nvarchar(50))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblClaimantProduct (ProductID, ClaimantEmploymentID, StartDate, EndDate, SPUsername, Notes, SupportingDocID, SupportingPageNums)
	VALUES (@ProductID, @ClaimantEmploymentID, @StartDate, @EndDate, @Emailaddress, @Notes, @SupportingDocID, @SupportingPageNums)
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsProduct] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryInsProduct] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsProduct] TO [power_user]
GO
