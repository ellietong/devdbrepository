SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantDocumentType]

as

Select ClaimantDocumentTypeID, ClaimantDocumentType
From tblClaimantDocumentType
Order By ClaimantDocumentType

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantDocumentType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantDocumentType] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmClaimantDocumentType] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantDocumentType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantDocumentType] TO [power_user]
GO
