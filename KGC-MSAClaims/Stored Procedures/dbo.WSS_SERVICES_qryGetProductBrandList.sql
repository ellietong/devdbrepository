SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetProductBrandList]
(@PrefixText nvarchar(200), @Emailaddress nvarchar(250) )
AS
BEGIN
	SELECT ProductBrandID, ProductBrand
	FROM tblProductBrand
	WHERE ProductBrand LIKE '%' + @PrefixText + '%'
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetProductBrandList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetProductBrandList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SERVICES_qryGetProductBrandList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetProductBrandList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetProductBrandList] TO [power_user]
GO
