SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryAllClaimsDetails]
@ClaimID bigint=null, @emailaddress nvarchar(250)
AS
BEGIN

--DECLARE @ClaimID int
--SET @ClaimID = 50692
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'snyderd@kcicllc.com'

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress)
--(SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
--					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
--					WHERE Email = @emailaddress)

DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)					
					
IF (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1)
BEGIN
SELECT     l.stateid, l.DefenseCounselAddressID, c.ClaimID, Disease + ' (ICD9: ' + COALESCE(CONVERT(nvarchar(10),ICD9),'N/A') + '), ' + HighLevelDisease AS Disease, S.State, J.Jurisdiction, cs.ClaimSequence, --hddis.HighLevelDisease AS Disease,  
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, L.DocketNumber, L.CaseCaption, 
                      L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  pca.PlaintiffCounsel AS PrimaryLocalPlaintiffCounsel, npca.PlaintiffCounsel AS PrimaryNationalPlaintiffCounsel, L.PlaintiffCounselAddressID, L.NationalPlaintiffCounselAddressID,
                      DefenseCounsel AS PrimaryDefenseCounsel, CPI.ClaimantPersonalInfoID, ReleaseSignedDate, rt.ReleaseType, 
                      CLst.StatusNotes, ExecutedReleaseDate as ReleaseReceivedDate, SettlementDueDate, PaidDate, CheckNumber, JOBSITE, ClaimDiseaseID, cl.LawsuitID, ActiveTrialDate,
                      StatusDate, SettlementDate, cd.DiseaseID,doc.doctorID,
                       doc.DoctorLastName + ', ' + COALESCE(doc.DoctorFirstName, '') + COALESCE(' ' + doc.DoctorMiddleName, '') AS Doctor, SettlementAmount, SettlementGroupID,
                       FullyDocumented, CauseOfInjuryID,CASE WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + ', ' + COALESCE(ilodoc.DoctorFirstName, '') + COALESCE(' ' + ilodoc.DoctorMiddleName, '') END AS ILODoctor,
                       CASE WHEN PFTDoctorID IS NULL THEN NULL ELSE pftdoc.DoctorLastName + ', ' + COALESCE(pftdoc.DoctorFirstName, '') + COALESCE(' ' + pftdoc.DoctorMiddleName, '')END AS PFTDoctor, ILODoctorID, ILOProfusion,
                       ILODate, PFTDoctorID, PFTDate, PFTTypeID, PFTFVC_Actual, PFTFVC_Predicted, 
                       CASE WHEN PFTFVC_Predicted <> 0 THEN (PFTFVC_Actual/PFTFVC_Predicted) ELSE 0 END AS PFTFVC_Pct,
                       
                         PFTFEV1_Actual, PFTFEV1_Predicted, CASE WHEN PFTFEV1_Predicted <> 0 THEN (PFTFEV1_Actual/PFTFEV1_Predicted) ELSE 0 END AS PFTFEV1_Pct,
                        [PFTFEV1andFVC_Actual] AS PFTFEV1FVC_Actual, [PFTFEV1andFVC_Predicted] AS PFTFEV1FVC_Predicted,
                         CASE WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN ([PFTFEV1andFVC_Actual]/[PFTFEV1andFVC_Predicted]) ELSE 0 END AS PFTFEV1FVC_Pct,
                       
                       PFTTLC_Actual, PFTTLC_Predicted, CASE WHEN PFTTLC_Predicted <> 0 THEN (PFTTLC_Actual/PFTTLC_Predicted) ELSE 0 END AS PFTTLC_Pct, 
                        PFTDLCO_Actual, PFTDLCO_Predicted, CASE WHEN PFTDLCO_Predicted <> 0 THEN (PFTDLCO_Actual/PFTDLCO_Predicted) ELSE 0 END AS PFTDLCO_Pct, 
                        PFTTV, IsThirdPartyComplaint, IsMDL, Material, CASE WHEN IsPrimaryForClaim = 1 THEN 'YES' ELSE 'NO' END AS PrimaryLawsuit, DiseaseDiagnosisDate, --AllegationType + ' - ' + Material AS Allegation
                        Disease_MSA, Jobsite_MSA, Product_MSA, cd.DoctorID,
						DemandAmount, DemandDate, OfferAmount, OfferDate, SettlementsFundsRequestDate AS SettlementFundsRequestDate, SettlementFundsRequested, SettlementFundsRequestDesc, HistoricalNotes,
			   CASE WHEN HasLossofConsortiumClaim = 1 THEN 'YES' ELSE 'NO' END AS HasLossofConsortiumClaim,
			   COALESCE(HasLossofConsortiumClaim, 0) AS HasLossofConsortiumClaimID,
			   CASE WHEN HasEstateClaim = 1 THEN 'YES' ELSE 'NO' END AS HasEstateClaim,
			   COALESCE(HasEstateClaim, 0) AS HasEstateClaimID
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
                      
                      
                      dbo.tblState AS S ON S.StateID = L.StateID LEFt JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID LEFT JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID 
                      LEFT JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID   
                      left join (SELECT * from tblClaimDisease where PrimaryDisease = 1) as CD on CD.ClaimID = c.ClaimID
					  left join tblDisease ddis on ddis.DiseaseID = cd.DiseaseID
					
					  left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID --OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
					  LEFT JOIN (SELECT * FROM tblClaimantEmployment WHERE IsPrimary = 1) CE ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID   
                      Left Join (Select * From dbo.tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                      LEFT JOIN dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
					  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
                      left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
                      left join tblplaintiffcounseladdress npca on npca.plaintiffcounseladdressid = L.NationalPlaintiffCounselAddressID
                      LEFT JOIN tblDoctor doc ON cd.DoctorID = doc.DoctorID 
                      LEFT JOIN tblDoctor ILODoc ON ILODoctorID=ILODoc.DoctorID
                      LEFT JOIN tblDoctor PFTDoc ON PFTDoctorID=PFTDoc.DoctorID
                      LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
                      LEFT JOIN tblAllegationType at ON a.AllegationTypeID = at.AllegationTypeID
                      LEFT JOIN tblMaterial mat ON a.MaterialID = mat.MaterialID
                      
            
WHERE C.ClaimID = @ClaimID --AND cl.IsPrimaryForClaim = 1
--ORDER BY PrimaryLawsuit DESC, State

END
ELSE IF (@InsurerRole = 1)
BEGIN
	
SELECT  DISTINCT   l.stateid, l.DefenseCounselAddressID, c.ClaimID, Disease + ' (ICD9: ' + COALESCE(CONVERT(nvarchar(10),ICD9),'N/A') + '), ' + HighLevelDisease AS Disease, S.State, J.Jurisdiction,
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, L.DocketNumber, L.CaseCaption, 
                      L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  pca.PlaintiffCounsel AS PrimaryLocalPlaintiffCounsel, npca.PlaintiffCounsel AS PrimaryNationalPlaintiffCounsel, L.PlaintiffCounselAddressID, L.NationalPlaintiffCounselAddressID, 
                      DefenseCOunsel AS PrimaryDefenseCounsel, CPI.ClaimantPersonalInfoID, ReleaseSignedDate, rt.ReleaseType, 
                      CLst.StatusNotes, ExecutedReleaseDate as ReleaseReceivedDate, SettlementDueDate, PaidDate, CheckNumber, JOBSITE, ClaimDiseaseID, cl.LawsuitID, ActiveTrialDate,
                      StatusDate, SettlementDate, cd.DiseaseID, cd.DoctorID,doc.DoctorID,
                       doc.DoctorLastName + ', ' + COALESCE(doc.DoctorFirstName, '') + COALESCE(' ' + doc.DoctorMiddleName, '') AS Doctor, SettlementAmount, SettlementGroupID,
                       FullyDocumented, CauseOfInjuryID,CASE WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + ', ' + COALESCE(ilodoc.DoctorFirstName, '') + COALESCE(' ' + ilodoc.DoctorMiddleName, '') END AS ILODoctor,
                       CASE WHEN PFTDoctorID IS NULL THEN NULL ELSE pftdoc.DoctorLastName + ', ' + COALESCE(pftdoc.DoctorFirstName, '') + COALESCE(' ' + pftdoc.DoctorMiddleName, '')END AS PFTDoctor, ILODoctorID, ILOProfusion,
                       ILODate, PFTDoctorID, PFTDate, PFTTypeID, PFTFVC_Actual, PFTFVC_Predicted, 
                       CASE WHEN PFTFVC_Predicted <> 0 THEN (PFTFVC_Actual/PFTFVC_Predicted) ELSE 0 END AS PFTFVC_Pct,
                       
                         PFTFEV1_Actual, PFTFEV1_Predicted, CASE WHEN PFTFEV1_Predicted <> 0 THEN (PFTFEV1_Actual/PFTFEV1_Predicted) ELSE 0 END AS PFTFEV1_Pct,
                        [PFTFEV1andFVC_Actual] AS PFTFEV1FVC_Actual, [PFTFEV1andFVC_Predicted] AS PFTFEV1FVC_Predicted,
                         CASE WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN ([PFTFEV1andFVC_Actual]/[PFTFEV1andFVC_Predicted]) ELSE 0 END AS PFTFEV1FVC_Pct,
                       
                       PFTTLC_Actual, PFTTLC_Predicted, CASE WHEN PFTTLC_Predicted <> 0 THEN (PFTTLC_Actual/PFTTLC_Predicted) ELSE 0 END AS PFTTLC_Pct, 
                        PFTDLCO_Actual, PFTDLCO_Predicted, CASE WHEN PFTDLCO_Predicted <> 0 THEN (PFTDLCO_Actual/PFTDLCO_Predicted) ELSE 0 END AS PFTDLCO_Pct, 
                        PFTTV, IsThirdPartyComplaint, IsMDL, Material, CASE WHEN IsPrimaryForClaim = 1 THEN 'YES' ELSE 'NO' END AS PrimaryLawsuit, DiseaseDiagnosisDate, --AllegationType + ' - ' + Material AS Allegation
                        Disease_MSA, Jobsite_MSA, Product_MSA, 
						DemandAmount, DemandDate, OfferAmount, OfferDate, SettlementsFundsRequestDate AS SettlementFundsRequestDate, SettlementFundsRequested, SettlementFundsRequestDesc, HistoricalNotes,
			   CASE WHEN HasLossofConsortiumClaim = 1 THEN 'YES' ELSE 'NO' END AS HasLossofConsortiumClaim,
			   COALESCE(HasLossofConsortiumClaim, 0) AS HasLossofConsortiumClaimID,
			   CASE WHEN HasEstateClaim = 1 THEN 'YES' ELSE 'NO' END AS HasEstateClaim,
			   COALESCE(HasEstateClaim, 0) AS HasEstateClaimID
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID LEFT JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblDefenseCounselAddress DCA ON l.DefenseCOunselAddressID = dca.DefenseCounselAddressID
                      --INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
                      left join (SELECT * from tblClaimDisease where PrimaryDisease = 1) as CD on CD.ClaimID = c.ClaimID
					  left join tblDisease ddis on ddis.DiseaseID = cd.DiseaseID
					  left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID --OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
					  LEFT JOIN (SELECT * FROM tblClaimantEmployment WHERE IsPrimary = 1) CE ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID   
                      Left Join (Select * From dbo.tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                      LEFT JOIN dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
					  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
                      left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
                      left join tblplaintiffcounseladdress npca on npca.plaintiffcounseladdressid = L.NationalPlaintiffCounselAddressID
                      LEFT JOIN tblDoctor doc ON cd.DoctorID = doc.DoctorID 
                      LEFT JOIN tblDoctor ILODoc ON ILODoctorID=ILODoc.DoctorID
                      LEFT JOIN tblDoctor PFTDoc ON PFTDoctorID=PFTDoc.DoctorID
                      LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
                      INNER JOIN tblAllegationType at ON a.AllegationTypeID = at.AllegationTypeID
                      INNER JOIN tblMaterial mat ON a.MaterialID = mat.MaterialID
                      
            
WHERE C.ClaimID = @ClaimID 
	--AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID) 
	AND cl.IsPrimaryForClaim = 1
	AND c.ClaimID NOT IN (select ClaimID from tblTenderHistory th INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
	AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
ORDER BY PrimaryLawsuit DESC, State

END
ELSE
BEGIN

SELECT  DISTINCT   l.stateid, l.DefenseCounselAddressID, c.ClaimID, Disease + ' (ICD9: ' + COALESCE(CONVERT(nvarchar(10),ICD9),'N/A') + '), ' + HighLevelDisease AS Disease, S.State, J.Jurisdiction,
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, L.DocketNumber, L.CaseCaption, 
                      L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  pca.PlaintiffCounsel AS PrimaryLocalPlaintiffCounsel, npca.PlaintiffCounsel AS PrimaryNationalPlaintiffCounsel, L.PlaintiffCounselAddressID, L.NationalPlaintiffCounselAddressID,
                      DefenseCOunsel AS PrimaryDefenseCounsel, CPI.ClaimantPersonalInfoID, ReleaseSignedDate, rt.ReleaseType, 
                      CLst.StatusNotes, ExecutedReleaseDate as ReleaseReceivedDate, SettlementDueDate, PaidDate, CheckNumber, JOBSITE, ClaimDiseaseID, cl.LawsuitID, ActiveTrialDate,
                      StatusDate, SettlementDate, cd.DiseaseID, cd.DoctorID,
                       doc.DoctorLastName + ', ' + COALESCE(doc.DoctorFirstName, '') + COALESCE(' ' + doc.DoctorMiddleName, '') AS Doctor, SettlementAmount, SettlementGroupID,
                       FullyDocumented, CauseOfInjuryID,CASE WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + ', ' + COALESCE(ilodoc.DoctorFirstName, '') + COALESCE(' ' + ilodoc.DoctorMiddleName, '') END AS ILODoctor,
                       CASE WHEN PFTDoctorID IS NULL THEN NULL ELSE pftdoc.DoctorLastName + ', ' + COALESCE(pftdoc.DoctorFirstName, '') + COALESCE(' ' + pftdoc.DoctorMiddleName, '')END AS PFTDoctor, ILODoctorID, ILOProfusion,
                       ILODate, PFTDoctorID, PFTDate, PFTTypeID, PFTFVC_Actual, PFTFVC_Predicted, 
                       CASE WHEN PFTFVC_Predicted <> 0 THEN (PFTFVC_Actual/PFTFVC_Predicted) ELSE 0 END AS PFTFVC_Pct,
                       
                         PFTFEV1_Actual, PFTFEV1_Predicted, CASE WHEN PFTFEV1_Predicted <> 0 THEN (PFTFEV1_Actual/PFTFEV1_Predicted) ELSE 0 END AS PFTFEV1_Pct,
                        [PFTFEV1andFVC_Actual] AS PFTFEV1FVC_Actual, [PFTFEV1andFVC_Predicted] AS PFTFEV1FVC_Predicted,
                         CASE WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN ([PFTFEV1andFVC_Actual]/[PFTFEV1andFVC_Predicted]) ELSE 0 END AS PFTFEV1FVC_Pct,
                       
                       PFTTLC_Actual, PFTTLC_Predicted, CASE WHEN PFTTLC_Predicted <> 0 THEN (PFTTLC_Actual/PFTTLC_Predicted) ELSE 0 END AS PFTTLC_Pct, 
                        PFTDLCO_Actual, PFTDLCO_Predicted, CASE WHEN PFTDLCO_Predicted <> 0 THEN (PFTDLCO_Actual/PFTDLCO_Predicted) ELSE 0 END AS PFTDLCO_Pct, 
                        PFTTV, IsThirdPartyComplaint, IsMDL, Material, CASE WHEN IsPrimaryForClaim = 1 THEN 'YES' ELSE 'NO' END AS PrimaryLawsuit, DiseaseDiagnosisDate, --AllegationType + ' - ' + Material AS Allegation
                        Disease_MSA, Jobsite_MSA, Product_MSA,
						DemandAmount, DemandDate, OfferAmount, OfferDate, SettlementsFundsRequestDate AS SettlementFundsRequestDate, SettlementFundsRequested, SettlementFundsRequestDesc, HistoricalNotes,
			   CASE WHEN HasLossofConsortiumClaim = 1 THEN 'YES' ELSE 'NO' END AS HasLossofConsortiumClaim,
			   COALESCE(HasLossofConsortiumClaim, 0) AS HasLossofConsortiumClaimID,
			   CASE WHEN HasEstateClaim = 1 THEN 'YES' ELSE 'NO' END AS HasEstateClaim,
			   COALESCE(HasEstateClaim, 0) AS HasEstateClaimID
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID LEFT JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblDefenseCounselAddress DCA ON l.DefenseCOunselAddressID = dca.DefenseCounselAddressID
                      INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
                      left join (SELECT * from tblClaimDisease where PrimaryDisease = 1) as CD on CD.ClaimID = c.ClaimID
					  left join tblDisease ddis on ddis.DiseaseID = cd.DiseaseID
					  left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID --OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
					  LEFT JOIN (SELECT * FROM tblClaimantEmployment WHERE IsPrimary = 1) CE ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID   
                      Left Join (Select * From dbo.tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                      LEFT JOIN dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
					  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
                      left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
                      left join tblplaintiffcounseladdress npca on npca.plaintiffcounseladdressid = L.NationalPlaintiffCounselAddressID
                      LEFT JOIN tblDoctor doc ON cd.DoctorID = doc.DoctorID 
                      LEFT JOIN tblDoctor ILODoc ON ILODoctorID=ILODoc.DoctorID
                      LEFT JOIN tblDoctor PFTDoc ON PFTDoctorID=PFTDoc.DoctorID
                      LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
                      INNER JOIN tblAllegationType at ON a.AllegationTypeID = at.AllegationTypeID
                      INNER JOIN tblMaterial mat ON a.MaterialID = mat.MaterialID
                      
            
WHERE C.ClaimID = @ClaimID AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID) AND cl.IsPrimaryForClaim = 1
ORDER BY PrimaryLawsuit DESC, State
END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAllClaimsDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAllClaimsDetails] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryAllClaimsDetails] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAllClaimsDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAllClaimsDetails] TO [power_user]
GO
