SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmDocumentType]
AS
Set Nocount On
SELECT DocumentTypeID, DocumentType 
FROM tblDocumentType 
ORDER BY DocumentType
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDocumentType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDocumentType] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmDocumentType] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDocumentType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDocumentType] TO [power_user]
GO
