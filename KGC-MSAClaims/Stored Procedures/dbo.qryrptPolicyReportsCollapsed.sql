SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


CREATE  Procedure [dbo].[qryrptPolicyReportsCollapsed]
(@insuranceProgramID int = NULL,@NamedInsuredID int = null, @limitTypeID int = NULL, @carrierID int = NULL, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL,
@lowLayer int = NULL, @highLayer int = NULL, @lowAttach nvarchar(50) = NULL, @highAttach nvarchar(50) = NULL, @startDate int = NULL, @endDate int = NULL)
As

SET NOCOUNT ON

/*
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.Tables WHERE Table_Name = 'tblPolicyLimits')
BEGIN
-- No multiple limit scenario 
SELECT a.PolicyID, a.PolicyNum, f.CarrierID, f.CarrierName, f.CarrierSolvency, g.CarrierGroupID, g.CarrierGroupName, 
	d.NamedInsured, e.InsuranceProgram, 
	COUNT(a.PolicyID) AS NumOccurrences, MIN(actualPStartDate) AS MinOfPStartDate, MAX(actualPEndDate) AS MaxOfPEndDate, 
	MIN(AttachmentPoint) AS AttachmentPoint, MIN(AnnualPeriod) AS MinOfAnnualPeriod, SUM(c.SIR) AS SIR, SUM(c.PerOccLimit) AS PerOccLimit, 
	SUM(c.LayerPerOccLimit) AS LayerPerOccLimit, SUM(c.AggregateLimit) AS AggregateLimit, MIN(c.Layer) AS Layer, 
	SUM(c.PerOccLimit) AS TotalLimitsPerPolicy
FROM (((((tblPolicy a INNER JOIN tblPolicyDates b ON a.PolicyID = b.PolicyID)
	INNER JOIN tblPolicyLimits c ON a.PolicyID = c.PolicyID)
	INNER JOIN tblNamedInsured d ON a.NamedInsuredID = d.NamedInsuredID)
	INNER JOIN tblInsuranceProgram e ON a.InsuranceProgramID = e.InsuranceProgramID)
	INNER JOIN tblCarrier f ON a.CarrierID = f.CarrierID)
	LEFT JOIN tblCarrierGroup g ON f.CarrierGroupID = g.CarrierGroupID
WHERE a.InsuranceProgramID = COALESCE(@insuranceProgramID, 0)
GROUP BY a.PolicyID, a.PolicyNum, f.CarrierID, f.CarrierName, f.CarrierSolvency, g.CarrierGroupID, g.CarrierGroupName, 
	d.NamedInsured, e.InsuranceProgram
ORDER BY e.InsuranceProgram

END
*/

--DECLARE @insuranceProgramID int 
--DECLARE @limitTypeID int
--DECLARE @carrierID int 
--DECLARE @carrierGroupID int 
--DECLARE @carrierSolvency varchar(50) 
--DECLARE @namedInsuredID int 
--DECLARE @lowLayer int 
--DECLARE @highLayer int 
--DECLARE @lowAttach nvarchar(50) 
--DECLARE @highAttach nvarchar(50) 
--DECLARE @provisionLabelID int 
--DECLARE @provisionValueID int 
--DECLARE @startDate int
--DECLARE @endDate int

--SET @insuranceProgramID = NULL
--SET @limitTypeID = NULL
--SET @carrierID = NULL
--SET @carrierGroupID = NULL
--SET @carrierSolvency = null
--SET @namedInsuredID = NULL
--SET @lowLayer = NULL
--SET @highLayer = NULL
--SET @lowAttach = null
--SET @highAttach = null
--SET @provisionLabelID = NULL
--SET @provisionValueID = NULL
--SET @startDate = NULL
--SET @endDate = 1983
DECLARE @maxInsuranceProgram int
DECLARE @maxNamedInsured Int
DECLARE @maxLimitTypeID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)
DECLARE @maxLayer int
DECLARE @maxAttach money
DECLARE @maxEndDate int

-- Find max ID values
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxNamedInsured = MAX(NamedInsuredID) FROM vPolicy
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxLayer = MAX(Layer) FROM vPolicy
SELECT @maxAttach = MAX(AttachmentPoint) FROM vPolicy
SELECT @maxEndDate = MAX(annualperiod) FROM vPolicy

IF @carrierGroupID = 0 SET @carrierGroupID = NULL

SELECT PolicyID, PolicyNum, CarrierID, CarrierName, CarrierSolvency, CarrierGroupID, CarrierGroupName, NamedInsured, InsuranceProgram, NamedInsuredID,
	COUNT(PolicyID) AS NumOccurrences, MIN(ActualPStartDate) AS MinOfPStartDate, MAX(ActualPEndDate) AS MaxOfPEndDate, MIN(AttachmentPoint) AS AttachmentPoint, 
	MIN(AnnualPeriod) AS MinOfAnnualPeriod, SUM(SIR) AS SIR, SUM(PerOccLimit) AS PerOccLimit, SUM(LayerPerOccLimit) AS LayerPerOccLimit, SUM(AggregateLimit) AS AggregateLimit, 
	MIN(Layer) AS Layer, SUM(PerOccLimit) AS TotalLimitsPerPolicy
FROM vPolicy pinfo
WHERE InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND
 	NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsured ) AND
	LimitTypeID >= COALESCE(@limitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	CarrierID >= COALESCE(@carrierID, 0) AND CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
	(coalesce(CarrierGroupID, 0) >= COALESCE(@carrierGroupID, 0) AND coalesce(CarrierGroupID, 0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(CarrierGroupID, 0) = @carrierGroupID) AND
	CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND Layer BETWEEN COALESCE(@lowLayer, 0) AND COALESCE(@highLayer, @maxLayer) AND
	AttachmentPoint BETWEEN COALESCE(CONVERT(money, @lowAttach), 0) AND COALESCE(CONVERT(money, @highAttach), @maxAttach)
	and DoNotReport <>1
	AND annualperioD >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
GROUP BY PolicyID, PolicyNum, CarrierID, CarrierName, CarrierSolvency, CarrierGroupID, CarrierGroupName, NamedInsured, InsuranceProgram, NamedInsuredID 
ORDER BY InsuranceProgram

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsCollapsed] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsCollapsed] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptPolicyReportsCollapsed] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsCollapsed] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsCollapsed] TO [power_user]
GO
