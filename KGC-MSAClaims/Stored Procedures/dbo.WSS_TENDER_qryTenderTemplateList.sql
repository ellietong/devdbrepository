SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryTenderTemplateList]
(@TenderHistoryID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--DECLARE @TenderHistoryID bigint = 171720
    -- Insert statements for procedure here
	SELECT tt.TenderTemplateID, TemplateDisplayName
	FROM 
	tblTenderHistory th
	INNER JOIN tblTenderTemplate tt ON th.TenderBlockID = tt.TenderBlockID AND th.TenderTypeID = tt.TenderTypeID
	WHERE th.TenderHistoryID = @TenderHistoryID
	UNION
	SELECT NULL, '<< Select a Template >>'
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTenderTemplateList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTenderTemplateList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryTenderTemplateList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTenderTemplateList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTenderTemplateList] TO [power_user]
GO
