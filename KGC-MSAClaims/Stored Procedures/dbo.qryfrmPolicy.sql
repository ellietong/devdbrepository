SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmPolicy]
(@filter varchar(5) = NULL)
AS
SET NOCOUNT ON

SELECT *
FROM tblPolicy
WHERE CarrierID LIKE COALESCE(@filter, '%')
ORDER BY PolicyID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicy] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmPolicy] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicy] TO [power_user]
GO
