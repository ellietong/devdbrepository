SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDelDocument] (@DocumentID int, 
											@Emailaddress nvarchar(250))
AS

SET NOCOUNT ON
	
--Select the DocumentURL so C# code knows the path of the file which is to be deleted
SELECT DocumentURL 
FROM tblDocuments 
WHERE DocumentID = @DocumentID

--Update the SPUsername field in tblMultiClaimantDocument prior to delete for audit log purposes
UPDATE tblMultiClaimantDocument
	SET SPUsername = @emailaddress
	WHERE DocumentID = @DocumentID

--Update the ModifiedBy field in tblDocuments prior to delete for audit log purposes
UPDATE tblDocuments
	SET ModifiedBy = @emailaddress
	WHERE DocumentID = @DocumentID

--This is used on ViewDoc to delete **ALL** tblDocument and tblMultiClaimantDocument records for a given DocumentID
DELETE FROM tblMultiClaimantDocument WHERE DocumentID = @DocumentID
DELETE FROM tblDocuments WHERE DocumentID = @DocumentID

--This is used to delete the document-related records from the settlement and invoice tables
DELETE FROM tblSettleDocs WHERE DocumentID = @DocumentID
DELETE FROM tblDefenseInvoice WHERE DocumentID = @DocumentID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelDocument] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryDelDocument] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelDocument] TO [power_user]
GO
