SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


CREATE  Procedure [dbo].[qryrptPolicyCoverageByCarrierGroupByAnnualPeriod]
(@insuranceProgramID int = NULL, @namedInsuredID INT = null, @limitTypeID int = NULL, @carrierID int = NULL, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL,
@startDate int = NULL, @endDate int = NULL)
As

SET NOCOUNT ON

--DECLARE @insuranceProgramID int 
--DECLARE @limitTypeID int
--DECLARE @carrierID int 
--DECLARE @carrierGroupID int 
--DECLARE @carrierSolvency varchar(50) 
--DECLARE @namedInsuredID int 
--
--SET @insuranceProgramID = 0
--SET @limitTypeID = 0
--SET @carrierID = 0
--SET @carrierGroupID = 0
--SET @carrierSolvency = null
--SET @namedInsuredID = 0

DECLARE @maxNamedInsured int
DECLARE @maxInsuranceProgram int
DECLARE @maxLimitTypeID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)
DECLARE @maxEndDate int

-- Find max ID values
SELECT @maxNamedInsured = MAX(NamedInsuredID) FROM vPolicy
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxEndDate = MAX(annualperiod) FROM vPolicy

IF @carrierGroupID = 0 SET @carrierGroupID = NULL

SELECT InsuranceProgram, CarrierSolvency, LimitTypeID, 
	CASE WHEN CarrierGroupName IS NULL THEN CarrierName ELSE CarrierName + ' ('+ CarrierGroupName + ')' END AS 'CarrierGroupInfo',  SUM(PerOccLimit) AS SumOfPerOccLimit, 
	SUM(SIR) AS SumOfSIR, AnnualPeriod, MIN(AttachmentPoint) AS MinOfAttachmentPoint, MAX(AttachmentPoint) AS MaxOfAttachmentPoint, CarrierGroupID, CarrierID, 
	InsuranceProgramID, NamedInsured
FROM vPolicy pinfo
WHERE InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND
	NamedInsuredID >= COALESCE(@namedInsuredID, 0) AND NamedInsuredID <= COALESCE(NULLIF(@namedInsuredID, 0), @maxNamedInsured) AND
	LimitTypeID >= COALESCE(@limitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	CarrierID >= COALESCE(@carrierID, 0) AND CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
	(coalesce(CarrierGroupID, 0) >= COALESCE(@carrierGroupID, 0) AND coalesce(CarrierGroupID, 0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(CarrierGroupID, 0) = @carrierGroupID) AND
	CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%')
	AND DoNotReport<>1
	AND AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
GROUP BY InsuranceProgram, CarrierSolvency, LimitTypeID, CASE WHEN CarrierGroupName IS NULL THEN CarrierName ELSE CarrierName + ' ('+ CarrierGroupName + ')' END,  
	AnnualPeriod, CarrierGroupID, CarrierID, InsuranceProgramID, NamedInsured

RETURN

/*
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.Tables WHERE Table_Name = 'tblPolicyLimits')
-- Non multiple limits scenario
BEGIN

SELECT tblInsuranceProgram.InsuranceProgram, 
	CASE 
	WHEN carriergroupname is null then [carriername] 
	ELSE [carriername] + ' ('+ [carriergroupname] + ')' 
	END as 'CarrierGroupInfo',  
Sum(tblPolicy.PerOccLimit) AS SumOfPerOccLimit, Sum(tblPolicy.SIR) AS SumOfSIR, tblPolicyDates.AnnualPeriod, Min(tblPolicy.AttachmentPoint) AS MinOfAttachmentPoint, 
Max(tblPolicy.AttachmentPoint) AS MaxOfAttachmentPoint, tblCarrierGroup.CarrierGroupID, tblCarrier.CarrierID, tblInsuranceProgram.InsuranceProgramID
FROM (tblInsuranceProgram INNER JOIN (tblCarrierGroup RIGHT JOIN (tblCarrier INNER JOIN tblPolicy ON tblCarrier.CarrierID = tblPolicy.CarrierID) 
ON tblCarrierGroup.CarrierGroupID = tblCarrier.CarrierGroupID) ON tblInsuranceProgram.InsuranceProgramID = tblPolicy.InsuranceProgramID) LEFT JOIN tblPolicyDates 
ON tblPolicy.PolicyID = tblPolicyDates.PolicyID
GROUP BY tblInsuranceProgram.InsuranceProgram, 
	CASE 
	WHEN carriergroupname is null then [carriername] 
	ELSE [carriername] + ' ('+ [carriergroupname] + ')' 
	END,  
tblPolicyDates.AnnualPeriod, tblCarrierGroup.CarrierGroupID, tblCarrier.CarrierID, tblInsuranceProgram.InsuranceProgramID

END
*/


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyCoverageByCarrierGroupByAnnualPeriod] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyCoverageByCarrierGroupByAnnualPeriod] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptPolicyCoverageByCarrierGroupByAnnualPeriod] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyCoverageByCarrierGroupByAnnualPeriod] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyCoverageByCarrierGroupByAnnualPeriod] TO [power_user]
GO
