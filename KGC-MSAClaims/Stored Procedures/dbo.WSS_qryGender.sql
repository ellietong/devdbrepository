SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[WSS_qryGender]
as

Select GenderID, Gender
From tblGender
UNION
SELECT NULL, ''
Order By Gender

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGender] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGender] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGender] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGender] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGender] TO [power_user]
GO
