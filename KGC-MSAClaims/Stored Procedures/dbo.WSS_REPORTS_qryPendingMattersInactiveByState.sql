SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryPendingMattersInactiveByState] 
	-- Add the parameters for the stored procedure here
(@EndDate datetime, @EmailAddress nvarchar(250), @TortTypeID int)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


DECLARE @MaxTortTypeID int
set @MaxTortTypeID = (SELECT MAX(TortTypeID) FROM tblTortType)

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)

--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1)
BEGIN
    
select  FullNameState AS State, COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
where coalesce(filedate, servicedate, processeddate, statusdate, '1/1/1900') <= @EndDate
and c.TortTypeID >= coalesce(@torttypeid, 0) and c.TortTypeID <= coalesce(@TortTypeID, @MaxTortTypeID)
and clst.ClaimLawsuitStatusID = 29 and IsPrimaryStatus = 1

group by FullNameState
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END

ELSE IF(@InsurerRole = 1) --Insurers
BEGIN

select  FullNameState AS State, COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
where coalesce(filedate, servicedate, processeddate, statusdate, '1/1/1900') <= @EndDate
and clst.ClaimLawsuitStatusID = 29 and IsPrimaryStatus = 1
and c.TortTypeID >= coalesce(@torttypeid, 0) and c.TortTypeID <= coalesce(@torttypeid, @maxtorttypeid)
AND c.ClaimID NOT IN (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
group by FullNameState
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END
ELSE 
BEGIN

select  FullNameState AS State, COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
where coalesce(filedate, servicedate, processeddate, statusdate, '1/1/1900') <= @EndDate
and clst.ClaimLawsuitStatusID = 29 and IsPrimaryStatus = 1
and (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
and c.TortTypeID >= coalesce(@torttypeid, 0) and c.TortTypeID <= coalesce(@torttypeid, @maxtorttypeid)
group by FullNameState
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryPendingMattersInactiveByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryPendingMattersInactiveByState] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_REPORTS_qryPendingMattersInactiveByState] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryPendingMattersInactiveByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryPendingMattersInactiveByState] TO [power_user]
GO
