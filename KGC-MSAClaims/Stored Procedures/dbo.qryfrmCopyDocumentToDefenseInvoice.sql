SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryfrmCopyDocumentToDefenseInvoice]
(@DocumentID int, @Firm int, @InvoiceNumber nvarchar(255), @Period nvarchar(255))
AS

SET NOCOUNT ON

--Copy Document Record to mBtblDefenseInvoice
INSERT INTO tblDefenseInvoice (StatusID, FirmID, InvoiceNumber, Period, 
	FeeAmount, DisbursementAmount, Adjustment, InvoiceTotal, SubmitDate, DatePayable, CheckNumber,
	FundsRequestDate, DatePaid, DocumentID, AdjustmentNotes, InvoiceLink, CheckLink)
SELECT StatusID, 
	@Firm AS FirmID,
	@InvoiceNumber AS InvoiceNumber, 
	@Period AS Period, 
	FeeAmount, 
	DisbursementAmount, 
	Adjustment, 
	(FeeAmount + DisbursementAmount - Adjustment) AS InvoiceTotal,
	DateCreated AS SubmitDate, 
	(DateCreated + 60) AS DatePayable,
	NULL AS CheckNumber,
	NULL AS FundsRequestDate,
	NULL AS DatePaid,
	DocumentID,
	AdjustmentNotes,
	DocumentURL AS InvoiceLink,
	'' AS CheckLink
FROM tblDocuments 
WHERE DocumentID = @DocumentID

--Update Document Record in mBtblDocuments
UPDATE tblDocuments
SET QCDate = GETDATE()
WHERE DocumentID = @DocumentID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [power_user]
GO
