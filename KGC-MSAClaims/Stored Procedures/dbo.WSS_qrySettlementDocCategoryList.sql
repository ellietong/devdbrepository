SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qrySettlementDocCategoryList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT DocCategoryID, DocCategory FROM tblDocumentCategory
    WHERE DocCategoryID NOT IN (1, 7, 12)
    UNION
    SELECT NULL, '-- Select a Category --'
    ORDER BY DocCategory
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySettlementDocCategoryList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySettlementDocCategoryList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qrySettlementDocCategoryList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySettlementDocCategoryList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySettlementDocCategoryList] TO [power_user]
GO
