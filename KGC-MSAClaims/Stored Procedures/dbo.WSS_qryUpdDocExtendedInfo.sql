SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryUpdDocExtendedInfo]
(@DocumentID int, @DocumentTypeID int, @DocumentSourceID int, @FirmID int, @Accept bit, @DismissalTypeID int,  @DismissalDate datetime, @EmailAddress nvarchar(250),
@ReleaseTypeID int, @ExecutedReleaseRcvdDate datetime, @CheckNumber nvarchar(250), @CheckDate datetime)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblDocuments
	SET DetailDocumentTypeID = @DocumentTypeID,
		DocumentSourceID = @DocumentSourceID,
		FirmID = @FirmID,
		DateModified = GETDATE(),
		ModifiedBy = @EmailAddress,
		DismissalProcessDate = CASE WHEN @DocumentTypeID = 16 AND DismissalProcessDate IS NULL THEN GETDATE() WHEN @DocumentTypeID <> 16 AND DismissalProcessDate IS NOT NULL THEN NULL ELSE DismissalProcessDate END,
		ClaimLawsuitStatusID = CASE WHEN @DocumentTypeID = 16 THEN @DismissalTypeID ELSE NULL END,
		DismissalDate = CASE WHEN @DocumentTypeID = 16 THEN @DismissalDate ELSE DismissalDate END,
		Accepted = CASE WHEN COALESCE(Accepted, 0) = 1 THEN Accepted WHEN @Accept = 0 AND COALESCE(Accepted, 0) = 0 THEN NULL ELSE 1 END,
		AcceptedBy = Case WHEN AcceptedBy IS NOT NULL THEN AcceptedBy WHEN @Accept = 1 THEN @EmailAddress ELSE NULL END,
		DateAccepted = CASE WHEN DateAccepted IS NOT NULL THEN DateAccepted WHEN @Accept = 1 THEN GETDATE() ELSE NULL END
		
	WHERE DocumentID = @DocumentID

IF @Accept = 1 AND RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'
BEGIN
	UPDATE tblMultiClaimantDocument
		SET QCBy = @EmailAddress,
			QCDate = GETDATE()
			WHERE DocumentID = @DocumentID 	
END				
	
DECLARE @ClaimLawsuitID BIGINT

/**Declare cursor to attach documentation to each claimant the document is attached to**/
DECLARE Cur_ClaimantLawsuits CURSOR Local Fast_Forward for
(select cl.ClaimlawsuitID 
 from tblClaimantPersonalInfo cpi 
 inner join tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID 
 inner join tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
 INNER JOIN tblMultiClaimantDocument mcd ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
 where c.IsCurrentClaim = 1 and cl.IsPrimaryForClaim = 1 and mcd.DocumentID = @DocumentID)

OPEN Cur_ClaimantLawsuits
FETCH NEXT FROM Cur_ClaimantLawsuits INTO @ClaimLawsuitID
WHILE @@FETCH_STATUS = 0
BEGIN

DECLARE @NewSettlePaymentDocumentationID BIGINT
DECLARE @NewClaimantSettlementID BIGINT

--if there is no record for this claim in tblSettlePaymentDocumentation then create one
if (select COUNT(setpymtid) from tblSettlePaymentDocumentation where ClaimLawsuitID = @ClaimLawsuitID) = 0 
begin
Insert into tblSettlePaymentDocumentation (ClaimLawsuitID)
select @ClaimLawsuitID
set @NewSettlePaymentDocumentationID = (select MAX(SetPymtID) from tblSettlePaymentDocumentation)
end
ELSE
begin
set @NewSettlePaymentDocumentationID = (Select setpymtID from tblSettlePaymentDocumentation where ClaimLawsuitID = @ClaimLawsuitID)
end

SELECT * FROm tblClaimantSettlement
--if there is no settlement record for this ClaimLawsuitID in tblClaimantSettlement then create one
if (select COUNT(ClaimantSettlementID) from tblClaimantSettlement where ClaimLawsuitID = @ClaimLawsuitID) = 0 
begin
Insert into tblClaimantSettlement (ClaimLawsuitID)
select @ClaimLawsuitID

set @NewClaimantSettlementID = (select MAX(ClaimantSettlementID) from tblClaimantSettlement)
end
ELSE
begin
set @NewClaimantSettlementID = (Select ClaimantSettlementID from tblClaimantSettlement where ClaimLawsuitID = @ClaimLawsuitID)
end


--Update check
UPDATE tblClaimantSettlement 
SET	CheckNumber = COALESCE(@CheckNumber, CheckNumber),
	PaidDate = COALESCE(@CheckDate, PaidDate),
	SPUsername = @EmailAddress
WHERE ClaimantSettlementID = @NewClaimantSettlementID



--Update release information
UPDATE tblSettlePaymentDocumentation 
SET	ExecutedReleaseDate = COALESCE(@ExecutedReleaseRcvdDate, ExecutedReleaseDate),
	ReleaseTypeID = COALESCE(@ReleaseTypeID, ReleaseTypeID),
	SPUsername = @EmailAddress
WHERE SetPymtID = @NewSettlePaymentDocumentationID


FETCH NEXT FROM Cur_ClaimantLawsuits INTO @ClaimLawsuitID

END
CLOSE Cur_ClaimantLawsuits
DEALLOCATE Cur_ClaimantLawsuits


END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdDocExtendedInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdDocExtendedInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdDocExtendedInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdDocExtendedInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdDocExtendedInfo] TO [power_user]
GO
