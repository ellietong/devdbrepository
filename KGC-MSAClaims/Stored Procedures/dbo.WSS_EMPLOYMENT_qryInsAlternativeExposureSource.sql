SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryInsAlternativeExposureSource]
(@ClaimantEmploymentID bigint, 
 @StartDate datetime, 
 @EndDate datetime, 
 @Emailaddress nvarchar(100), 
 @Notes nvarchar(max), 
 @ProductManufacturerID int,
 @ProductTypeID int,
 @ProductNameID int,
 @ProductManufacturer nvarchar(2000),
 @ProductType nvarchar(2000),
 @ProductName nvarchar(2000),
 @SupportingDocID int, 
 @SupportingPageNums nvarchar(50))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

----Test Variables
--DECLARE @ClaimantEmploymentID bigint, 
--		@StartDate datetime, 
--		@EndDate datetime, 
--		@Emailaddress nvarchar(100), 
--		@Notes nvarchar(max), 
--		@ProductManufacturerID int,
--		@ProductTypeID int,
--		@ProductNameID int,
--		@ProductManufacturer nvarchar(2000),
--		@ProductType nvarchar(2000),
--		@ProductName nvarchar(2000) 

IF (SELECT COALESCE(@ProductManufacturerID, 0)) <= 0
	BEGIN
		IF (SELECT COUNT(ProductManufacturerID)
			FROM tblProductManufacturer
			WHERE ProductManufacturer LIKE '%'+ @ProductManufacturer +'%'
			) = 0
			BEGIN
				INSERT INTO tblProductManufacturer (ProductManufacturer)
				VALUES (@ProductManufacturer)
				SET @ProductManufacturerID = (SELECT MAX(ProductManufacturerID) FROM tblProductManufacturer)
			END
		ELSE 
			BEGIN
				SET @ProductManufacturerID = (SELECT TOP 1 ProductManufacturerID
										      FROM tblProductManufacturer
											  WHERE ProductManufacturer LIKE '%'+ @ProductManufacturer +'%')
			END
	END

IF @ProductManufacturerID = 1
BEGIN
	RETURN
END
ELSE
	BEGIN

	IF (SELECT COALESCE(@ProductTypeID, 0)) <= 0
		BEGIN
			IF (SELECT COUNT(ProductTypeID)
				FROM tblProductType
				WHERE ProductType LIKE '%'+ @ProductType +'%'
				) = 0
				BEGIN
					INSERT INTO tblProductType (ProductType)
					VALUES (@ProductType)
					SET @ProductTypeID = (SELECT MAX(ProductTypeID) FROM tblProductType)
				END
			ELSE 
				BEGIN
					SET @ProductTypeID = (SELECT TOP 1 ProductTypeID
												  FROM tblProductType
												  WHERE ProductType LIKE '%'+ @ProductType +'%')
				END
		END

	IF (SELECT COALESCE(@ProductNameID, 0)) <= 0
		BEGIN
			IF (SELECT COUNT(ProductNameID)
				FROM tblProductName
				WHERE ProductName LIKE '%'+ @ProductName +'%'
				) = 0
				BEGIN
					INSERT INTO tblProductName (ProductName)
					VALUES (@ProductName)
					SET @ProductNameID = (SELECT MAX(ProductNameID) FROM tblProductName)
				END
			ELSE 
				BEGIN
					SET @ProductNameID = (SELECT TOP 1 ProductNameID
												  FROM tblProductName
												  WHERE ProductName LIKE '%'+ @ProductName +'%')
				END
		END

	DECLARE @ProductID bigint

	IF (SELECT COUNT(ProductID) 
		FROM tblProduct 
		WHERE ProductManufacturerID = @ProductManufacturerID
			AND ProductTypeID = @ProductTypeID
			AND ProductNameID = @ProductNameID) > 0
		BEGIN
			SET @ProductID = (SELECT TOP 1 ProductID
							  FROM tblProduct 
							  WHERE ProductManufacturerID = @ProductManufacturerID
								  AND ProductTypeID = @ProductTypeID
								  AND ProductNameID = @ProductNameID)
		END
	ELSE
		BEGIN
			INSERT INTO tblProduct (ProductManufacturerID, ProductTypeID, ProductNameID)
			VALUES (@ProductManufacturerID, @ProductTypeID, @ProductNameID)
			SET @ProductID = (SELECT MAX(ProductID) FROM tblProduct)
		END
	
		INSERT INTO tblClaimantProduct (ProductID, ClaimantEmploymentID, StartDate, EndDate, SPUsername, Notes, SupportingDocID, SupportingPageNums)
		VALUES (@ProductID, @ClaimantEmploymentID, @StartDate, @EndDate, @Emailaddress, @Notes, @SupportingDocID, @SupportingPageNums)
	END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsAlternativeExposureSource] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsAlternativeExposureSource] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_EMPLOYMENT_qryInsAlternativeExposureSource] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsAlternativeExposureSource] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsAlternativeExposureSource] TO [power_user]
GO
