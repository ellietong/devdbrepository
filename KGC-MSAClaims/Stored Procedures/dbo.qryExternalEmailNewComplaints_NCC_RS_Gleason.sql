SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailNewComplaints_NCC_RS_Gleason]
AS
BEGIN

/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @LogoHeader nvarchar (2000)

SET @Subject =
'MSA: New Complaints Uploaded ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	

--1-28-12 - CAS modified to query data QC'ed between yesterday and current day at 4 PM so that query goes off those time stamps and not the current system date
--which could cause some notifications to be lost.
Declare @Today4PM datetime
Declare @Yesterday4PM datetime
SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')
SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (DocID int, docTitle nvarchar (500), FirmAddressID int, lName nvarchar (50),
fName nvarchar (50), ClaimantPersonalInfoID bigint, ServiceDate date, NCCReferenceID nvarchar (11), claimtype nvarchar (50),
PlaintiffCounsel nvarchar (1000), NationalPlaintiffCounsel nvarchar (1000))
INSERT INTO @NewComplaintsTable
	SELECT DISTINCT
		d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		l.DefenseCounselAddressID,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		MAX(ServiceDate),
		NCCReferenceID,
		Material AS claimtype,
		pca.PlaintiffCounsel,
		npca.PlaintiffCounsel AS NationalPlaintiffCounsel
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
	LEFT JOIN tblAllegation a ON a.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblMaterial m ON m.MaterialID = a.MaterialID
	LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	LEFT JOIN tblPlaintiffCounselAddress npca ON l.NationalPlaintiffCounselAddressID = npca.PlaintiffCounselAddressID
	WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
	AND detailDocumentTypeID IN (8, 4, 33)
	AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	AND FirmID IS NOT NULL
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	GROUP BY	d.documentID,
				d.DocumentTitle,
				DefenseCounselAddressID,
				LastName,
				FirstName,
				cpi.ClaimantPersonalInfoID,
				NCCReferenceID,
				Material,
				pca.PlaintiffCounsel,
				npca.PlaintiffCounsel

DECLARE @NewDocumentsTable TABLE (DocID int, docTitle nvarchar (500), FirmAddressID int, lName nvarchar (50),
fName nvarchar (50), DocType nvarchar (50), ClaimantPersonalInfoID bigint, NCCReferenceID nvarchar (11), createdby nvarchar (50), claimtype nvarchar (50),
PlaintiffCounsel nvarchar (1000), NationalPlaintiffCounsel nvarchar (1000))
INSERT INTO @NewDocumentsTable
	SELECT
		d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		l.DefenseCOunselAddressID,
		cpi.LastName,
		cpi.FirstName,
		ClaimantDocumentType,
		cpi.claimantpersonalinfoid,
		NCCReferenceID,
		CreatedBy,
		Material AS claimtype,
		pca.PlaintiffCounsel,
		npca.PlaintiffCounsel AS NationalPlaintiffCounsel
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantDocumentType ct ON ct.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
	LEFT JOIN tblAllegation a ON a.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblMaterial m ON m.MaterialID = a.MaterialID
	LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	LEFT JOIN tblPlaintiffCounselAddress npca ON l.NationalPlaintiffCounselAddressID = npca.PlaintiffCounselAddressID
	WHERE datecreated BETWEEN GETDATE() - 1 AND GETDATE()
	AND detailDocumentTypeID IN (7, 11, 23, 25, 6, 28, 2, 5, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 24, 30, 31, 35, 36, 37, 38, 39, 44)
	AND FirmID IS NOT NULL
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	GROUP BY	d.DocumentID,
				d.DocumentTitle,
				DefenseCOunselAddressID,
				LastName,
				FirstName,
				ClaimantDocumentType,
				cpi.claimantpersonalinfoid,
				NCCReferenceID,
				CreatedBy,
				Material,
				pca.PlaintiffCounsel,
				npca.PlaintiffCounsel

DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar (max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar (max), @ClaimantID bigint, @ClaimantList nvarchar (max), @NCCRef nvarchar (11), @ClaimType nvarchar (50)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for SELECT
	DocID,
	COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
	ClaimantPersonalInfoID,
	NCCReferenceID,
	ClaimType
FROM @NewComplaintsTable UNION SELECT
	DocID,
	COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
	ClaimantPersonalInfoID,
	NCCReferenceID,
	ClaimType
FROM @NewDocumentsTable

OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID, @NCCRef, @ClaimType

WHILE @@FETCH_STATUS = 0
BEGIN
IF (LEN (@ClaimantList) = 0 OR @ClaimantList IS NULL)
BEGIN
SET @ClaimantList = '<ul> <a href="https://www.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + @Claimant + COALESCE(' (' + CONVERT(nvarchar(50), @NCCRef) + ')', '') + ' - ' + @ClaimType + '</a></ul>'
END
ELSE
BEGIN
SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="https://www.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + COALESCE(@Claimant, '') + COALESCE(' (' + CONVERT(nvarchar(50), @NCCRef) + ')', '') + ' - ' + @ClaimType + '</a></ul>'
END

SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID, @NCCRef, @ClaimType

--PRINT @@FETCH_STATUS

IF @DocID < > @LastDocID OR @@FETCH_STATUS < > 0
BEGIN
INSERT INTO @ClaimantPerDoc
	SELECT
		@LastDocID,
		@ClaimantList
SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints

--Declare and populate the body of the email for NCC, Gleason, & ReedSmith
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
IF (SELECT
	COUNT(docTitle)
FROM @NewComplaintsTable) > 0
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>The following complaints have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
N'<tr><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants (NCC #) - Claim Type </th><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Served</th><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Plaintiff Counsel</th><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">National Plaintiff Counsel</th>' +
CAST((SELECT
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + docTitle + '</td>',
	'',
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + cl.ClaimantList + '</td>',
	'',
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + CAST(MAX(ServiceDate) AS varchar(MAX)) + '</td>',
	'',
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + PlaintiffCounsel + '</td>',
	'',
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + NationalPlaintiffCounsel + '</td>',
	''
FROM @NewComplaintsTable C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
GROUP BY	docTitle,
			cl.ClaimantList,
			PlaintiffCounsel,
			NationalPlaintiffCounsel
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'
END
ELSE
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>No Complaints have been uploaded in the last 24 hours. </H4>'
END

IF (SELECT
	COUNT(docTitle)
FROM @NewDocumentsTable) > 0
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>The following documents have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants (NCC #) - Claim Type</th><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Created By</th><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Plaintiff Counsel</th><th style ="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">National Plaintiff Counsel</th>' +
CAST((SELECT
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + docTitle + '</td>',
	'',
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + cl.ClaimantList + '</td>',
	'',
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + DocType + '</td>',
	'',
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + createdby + '</td>',
	'',
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + PlaintiffCounsel + '</td>',
	'',
	'<td style="font-family: Goudy Old Style;  font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + NationalPlaintiffCounsel + '</td>',
	''
FROM @NewDocumentsTable d
INNER JOIN @ClaimantPerDoc cl ON d.DocID = cl.DocID
GROUP BY	docTitle,
			cl.ClaimantList,
			DocType,
			createdby,
			PlaintiffCounsel,
			NationalPlaintiffCounsel
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'
END
ELSE
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>No other new documents have been uploaded in the last 24 hours. </H4>'
END

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href="https://www.kcicextranet.com/clients/msa/cp/pages/Documents.aspx">here</A> to access the document(s).</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
	733 10th Street, NW, Suite 3001<br>
	Washington, DC 20001<br>
	(t) 202-772-2300<br>
	(f) 202-772-2333'

DECLARE @RecipientList nvarchar (500)
SET @RecipientList = ''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar (500)
DECLARE contact_curs CURSOR FOR SELECT
	email
FROM vtblPerson
WHERE FirmID IN (68, 31, 66)
AND ComplaintContact = 1

OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact

WHILE @@FETCH_STATUS = 0
BEGIN
SET @RecipientList = @RecipientList + @Contact + '; '
FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail --The Claims sends from msa@kcic.com
@profile_name = 'Claims',
@recipients = @RecipientList,
@blind_copy_recipients = 'msa@kcic.com; potterm@kcic.com',
--@recipients = 'youngc@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints_NCC_RS_Gleason] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints_NCC_RS_Gleason] TO [base_user]
GRANT ALTER ON  [dbo].[qryExternalEmailNewComplaints_NCC_RS_Gleason] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints_NCC_RS_Gleason] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints_NCC_RS_Gleason] TO [power_user]
GO
