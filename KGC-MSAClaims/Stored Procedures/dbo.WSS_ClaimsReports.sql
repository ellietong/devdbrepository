SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_ClaimsReports]
(@ClaimLawsuitStatusGroupID int,  @StateID int, @PlaintiffCounselID bigint, @JurisdictionID int, 
 @emailaddress nvarchar(250), @ClaimTypeID int, @TortTypeID int)
AS
BEGIN

----VARIABLES FOR TESTING----
--DECLARE @ClaimLawsuitStatusGroupID int
--DECLARE @StateID int
--DECLARE @PlaintiffCounselID bigint
--DECLARE @JurisdictionID int
--DECLARE @emailaddress nvarchar(250)
--DECLARE @ClaimTypeID int
--DECLARE @TortTypeID int
--SET @ClaimLawsuitStatusGroupID = null
--SET @StateID = 30
--SET @PlaintiffCounselID = NULL
--SET @JurisdictionID = NULL
----SET @emailaddress = 'youngc@kcicllc.com'
--SET @emailaddress = 'lucy.lorence@hacdlaw.com' 
--SET @ClaimTypeID = NULL
--SET @TortTypeID = 1

--Set up ranges for use in WHERE statements below.
DECLARE @MaxStateID int
DECLARE @MaxPlaintiffCounselID int
DECLARE @MaxJurisdictionID int
DECLARE @MaxClaimLawsuitStatusGROUPID int
DECLARE @MaxClaimTypeID int
DECLARE @MaxTortTypeID int
SET @MaxStateID = (SELECT MAX(StateID) FROM tblState)
SET @MaxPlaintiffCounselID = (SELECT MAX(PlaintiffCounselID) FROM tblPlaintiffCounselAddress)
SET @MaxJurisdictionID = (SELECT MAX(JurisdictionID) FROM tblJurisdiction)
SET @MaxClaimLawsuitStatusGROUPID = (SELECT MAX(ClaimLawsuitStatusGroupID) FROM tblClaimLawsuitStatusGroup)
SET @MaxClaimTypeID = (SELECT MAX(MaterialID) FROM tblMaterial)
SET @MaxTortTypeID = (SELECT MAX(TortTypeID) from tblTortType)

--Prepare permissions-related fields for use in queries below.
DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID WHERE Email = @emailaddress)
DECLARE @InsurerRole bit
SET @InsurerRole = (SELECT DISTINCT InsurerRole FROM vtblPerson WHERE Email = @emailaddress)

--Prepare a comma-delineated list of diseases by ClaimID for use in reporting.
DECLARE @ColumnListComma nvarchar(MAX)
DECLARE @ColumnListPlus nvarchar(MAX)
DECLARE @Command nvarchar(MAX)
SET @ColumnListComma = (
	SELECT LEFT(DiseaseList, LEN(DiseaseList)-1) FROM (
		SELECT  DISTINCT
					'[' + Disease + ']' + ', ' AS [text()] 
				FROM 
					tblDisease
				FOR XML PATH('')  ) dl (DiseaseList))   
SET @ColumnListPlus = (
	SELECT LEFT(DiseaseList, LEN(DiseaseList)-1) FROM (
		SELECT DISTINCT
					'CASE WHEN COALESCE([' + Disease + '], 0) = 1 THEN ''' + REPLACE(Disease, '''', '"') + ', '' ELSE '''' END + ' AS [text()] 
				FROM 
					tblDisease
				FOR XML PATH('')  ) dl (DiseaseList))        
SET @Command = 'SELECT ClaimID, ' + @ColumnListPlus  +
' AS Diseases FROM
(SELECT DISTINCT cd.ClaimID, Disease, COUNT(cd.ClaimID) AS DiseaseCount FROM tblDisease d
LEFT JOIN (SELECT DISTINCT ClaimID, DiseaseID FROM tblClaimDisease) cd ON cd.DiseaseID = d.DiseaseID
GROUP BY cd.ClaimID, Disease
HAVING COUNT(cd.ClaimID) > 0
)
    AS dlist
PIVOT
(
    Sum(DiseaseCount)
FOR
Disease
    IN ( ' + @ColumnListComma + ')
) AS piv
WHERE ClaimID IS NOT NULL'

--This temp table pulls the disease information from the pivot table query created above, putting them in a form usable by the queries below.
IF object_id('tempdb..#ClaimDisease') IS NOT NULL
BEGIN
   DROP TABLE #ClaimDisease
END
CREATE TABLE #ClaimDisease (ClaimID bigint, Diseases nvarchar(MAX))
INSERT INTO #ClaimDisease 
	EXEC(@Command)



--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE COALESCE(cls.ClaimLawsuitStatusGroupID,0) <> 2'
IF @ClaimLawsuitStatusGroupID IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND cls.ClaimLawsuitStatusGroupID >= ' + CONVERT(nvarchar(20),COALESCE(@ClaimLawsuitStatusGroupID, 0)) + ' AND cls.ClaimLawsuitStatusGroupID <= ' + CONVERT(nvarchar(20),COALESCE(@ClaimLawsuitStatusGroupID, @MaxClaimLawsuitStatusGroupID)) END
IF @StateID					  IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.StateID >= ' + CONVERT(nvarchar(20),COALESCE(@StateID, 0)) + ' AND l.StateID <= ' + CONVERT(nvarchar(20),COALESCE(@StateID, @MaxStateID)) END
IF @PlaintiffCounselID		  IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(pca.PlaintiffCounselID, 0) >= ' + CONVERT(nvarchar(20),COALESCE(@PlaintiffCounselID, 0)) + ' AND COALESCE(pca.PlaintiffCounselID, 0) <= ' + CONVERT(nvarchar(20),COALESCE(@PlaintiffCounselID, @MaxPlaintiffCounselID)) END
IF @JurisdictionID			  IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.JurisdictionID >= ' + CONVERT(nvarchar(20),COALESCE(@JurisdictionID, 0)) + ' AND l.JurisdictionID <= ' + CONVERT(nvarchar(20),COALESCE(@JurisdictionID, @MaxJurisdictionID)) END
IF @ClaimTypeID				  IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND a.MaterialID >= ' + CONVERT(nvarchar(20),COALESCE(@ClaimTypeID, 0)) + ' AND a.MaterialID <= ' + CONVERT(nvarchar(20),COALESCE(@ClaimTypeID, @MaxClaimTypeID)) END
IF @TortTypeID				  IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND c.TortTypeID >= ' + CONVERT(nvarchar(20),COALESCE(@TortTypeID, 0)) + ' AND c.TorttypeID <= ' + CONVERT(nvarchar(20),COALESCE(@tortTypeID, @MaxTortTypeID)) END

--These IF statements determine additional WHERE statements by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
IF (@InsurerRole = 1)
	SET @WhereString = @WhereString + ' AND c.ClaimID NOT IN (SELECT cth.ClaimID FROM tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID WHERE TenderStatusID IN(2, 5) and TenderBlockID = 15 and TenderTypeID <> 2)
										AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3)) 
										AND c.TortTypeID = 1 '
IF (@InsurerRole <> 1 AND @NoFilters <> 1 AND RIGHT(@emailaddress, 11) <> 'kcicllc.com' OR RIGHT(@emailaddress, 8) <> 'kcic.com')
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),@FirmAddressID) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),@FirmAddressID) + ')'


--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT c.ClaimID, NCCReferenceID, cpi.ClaimantPersonalInfoID, s.State, j.Jurisdiction,
		CASE WHEN Suffix IS NULL THEN cpi.LastName + '', '' ELSE cpi.LastName + '' '' + Suffix + '', '' END + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL 
		THEN '''' ELSE MiddleName + '' '' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
		BirthDate, cpi.DeceasedDate, cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, 
		l.DocketNumber, l.CaseCaption, l.LeadPlaintiffLastName + COALESCE('', '' + l.LeadPlaintiffFirstName,'''') AS LeadPlaintiffName, 
		pca.PlaintiffCounsel, DefenseCounsel AS PrimaryDefenseCounsel,  cl.ActiveTrialDate, a.MaterialID as ClaimTypeID, Material AS ClaimType,
		case when claimlawsuitstatusgroupid = 3 then null else StatusDate end AS ResolutionDate, 
		CASE WHEN LEN(cd1.Diseases) >= 2 THEN LEFT(COALESCE(cd1.Diseases,''  ''), LEN(COALESCE(cd1.Diseases, ''  ''))-1)ELSE '''' END AS Diseases
	 FROM tblClaimantPersonalInfo cpi
		LEFT JOIN tblClaimantSuffix cs ON cpi.SuffixID = cs.SuffixID 
		INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID 
		INNER JOIN tblClaimLawsuit cl ON C.ClaimID = cl.ClaimID 
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID 
		LEFT JOIN  tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID 
		LEFT JOIN  tblState s ON s.StateID = l.StateID 
		LEFT JOIN  tblJurisdiction j ON j.JurisdictionID = l.JurisdictionID
		LEFT JOIN  tblDefenseCounselAddress dca ON l.DefenseCOunselAddressID = dca.DefenseCounselAddressID
		LEFT JOIN  tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
		LEFT JOIN  (SELECT * FROM dbo.tblClaimDisease WHERE PrimaryDisease = 1) cd ON cd.ClaimID = c.ClaimID 
		INNER JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE IsPrimaryStatus = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID 
		INNER JOIN tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN  tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		LEFT JOIN  tblMaterial m on m.MaterialID = a.MaterialID
		LEFT JOIN  (SELECT * FROM tblClaimantEmployment WHERE COALESCE(IsPrimary, 0) = 1) ce ON cpi.ClaimantPersonalInfoID = ce.ClaimantPersonalInfoID
		LEFT JOIN  #ClaimDisease cd1 ON c.ClaimID = cd1.ClaimID ' +
	 @WhereString + '
	 OPTION (RECOMPILE)'

--Finally, the query created above is executed.
Print (@QueryString)
EXEC (@QueryString)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_ClaimsReports] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_ClaimsReports] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_ClaimsReports] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_ClaimsReports] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_ClaimsReports] TO [power_user]
GO
