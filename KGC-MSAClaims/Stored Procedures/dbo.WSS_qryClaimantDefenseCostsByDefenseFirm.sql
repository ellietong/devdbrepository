SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryClaimantDefenseCostsByDefenseFirm] 
	-- Add the parameters for the stored procedure here
	(@ClaimantPersonalInfoID int, @EmailAddress nvarchar(250)) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 9494

--DECLARE @Emailaddress nvarchar(250)
--SET @EmailAddress = 'test'

--SELECT * FROm vtblPerson
--SELECT * FROM tblDefInvClaimantDetail WHERE ClaimID = (SELECT ClaimID FROM tblClaim WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID)
--SELECT * FROM tblDefInvClaimantDetail_Pre2008 WHERE ClaimID =  (SELECT ClaimID FROM tblClaim WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID)


	
DECLARE @FirmID int
SET @FirmID = (SELECT FirmID FROM vtblPerson WHERE Email = @EmailAddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)
					
Declare @InvoiceTotal table (ClaimantPersonalInfoID int,  DefenseCounselID int, DefenseTotal money)
Insert into @InvoiceTotal

SELECT c.ClaimantPersonalInfoID, dc.DefenseCounselID, Costs
FROM tblDefInvClaimantDetail dicd
INNER JOIN tblDefenseCounsel dc ON dicd.FirmID = dc.DefenseCounselID
LEFT JOIN tblClaim c ON dicd.ClaimID = c.ClaimID
WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
UNION ALL
SELECT c.ClaimantPersonalInfoID, dc.DefenseCounselID, Costs
FROM tblDefInvClaimantDetail_Pre2008 dicd
LEFT JOIN tblDefenseCounsel dc ON dicd.FirmID = dc.DefenseCounselID
LEFT JOIN tblClaim c ON dicd.ClaimID = c.ClaimID
WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
AND FirmID IS NULL

----Approved Expert and General Invoices with a ClaimantID link
--Select CPI.ClaimantPersonalInfoID, Di.DefenseCounselAddressID, SUM(COALESCE(Costs, 0) + COALESCE(Fees, 0) + COALESCE(Adjustments, 0)) as DefenseTotal
--From tblDefenseInvoice DI
--LEFT JOIN tblDefInvClaimantDetail dicd ON di.InvoiceNumber = dicd.InvoiceNumber
--LEFT Join tblClaimantPersonalInfo CPI on CPI.ClaimantPersonalInfoID=DIcd.ClaimantPersonalInfoID
--Where DICD.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and DI.StatusID in (1,2)
--Group by CPI.ClaimantPersonalInfoID, di.VendorFirmID, Di.DefenseCounselAddressID

IF @NoFilters = 1 OR RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' 
BEGIN


	Select	ClaimantPersonalInfoID, Coalesce(DefenseCounsel,'Pre-2008') as DefenseCounsel, Coalesce(i.DefenseCounselID, 0) as DefenseCounselID,  Sum(DefenseTotal) as DefenseTotal 
	From @InvoiceTotal i 
	Left Join tblDefenseCounsel d on i.DefenseCounselID = d.DefenseCounselID
	--Left Join m_tblVendorFirm e on i.VendorFirmID = e.VendorFirmID
	Group By ClaimantPersonalInfoID, DefenseCounsel, i.DefenseCounselID
	HAVING Sum(DefenseTotal) <> 0

END
IF(@InsurerRole = 1)
BEGIN
	Select	i.ClaimantPersonalInfoID, Coalesce(DefenseCounsel,'Pre-2008') as DefenseCounsel, Coalesce(i.DefenseCounselID, 0) as DefenseCounselID,  Sum(DefenseTotal) as DefenseTotal 
	From @InvoiceTotal i 
	Left Join tblDefenseCounsel d on i.DefenseCounselID = d.DefenseCounselID
	INNER JOIN tblClaim c ON i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	--Left Join m_tblVendorFirm e on i.VendorFirmID = e.VendorFirmID
	WHERE c.ClaimID NOT IN (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
		AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
	Group By i.ClaimantPersonalInfoID, DefenseCounsel, i.DefenseCounselID
	HAVING Sum(DefenseTotal) <> 0

END
ELSE
BEGIN

	Select	ClaimantPersonalInfoID, Coalesce(DefenseCounsel,'Pre-2008') as DefenseCounsel, Coalesce(i.DefenseCounselID, 0) as DefenseCounselID,  Sum(DefenseTotal) as DefenseTotal 
	From @InvoiceTotal i 
	Left Join tblDefenseCounsel d on i.DefenseCounselID = d.DefenseCounselID
	--Left Join m_tblVendorFirm e on i.VendorFirmID = e.VendorFirmID
	WHERE i.DefenseCounselID = @FirmID OR i.DefenseCounselID IS NULL
	Group By ClaimantPersonalInfoID, DefenseCounsel, i.DefenseCounselID
	HAVING Sum(DefenseTotal) <> 0

END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantDefenseCostsByDefenseFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantDefenseCostsByDefenseFirm] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GO
