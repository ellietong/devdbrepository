SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_RELEASE_qryUpdReleaseInfo]
(
	@ClaimantPersonalInfoID bigint, 
	@ClaimLawsuitID int, 
	@ReleaseDate datetime, 
	@EmailAddress nvarchar(250), 
	@ReleaseTypeID int,
	@DocumentID bigint

)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @ClaimLawsuitID IN (Select ClaimLawsuitID FROM tblClaimantSettlement)
	BEGIN
		UPDATE tblClaimantSettlement
		SET SettlementDate = @ReleaseDate,
			SPUsername = @EmailAddress
		WHERE ClaimLawsuitID = @ClaimLawsuitID
	END
	ELSE
	BEGIN
		INSERT INTO tblClaimantSettlement (ClaimLawsuitID, SettlementDate, SPUsername)
		VALUES (@ClaimLawsuitID, @ReleaseDate, @EmailAddress)
	END

	IF @ClaimLawsuitID IN (Select ClaimLawsuitID FROM tblSettlePaymentDocumentation)
	BEGIN
		UPDATE tblSettlePaymentDocumentation	
		SET ExecutedReleaseDate = @ReleaseDate,
			ReleaseTypeID = @ReleaseTypeID,
			SPUsername = @EmailAddress
		WHERE ClaimLawsuitID = @ClaimLawsuitID	
	END
	ELSE
	BEGIN
		INSERT INTO tblSettlePaymentDocumentation (ClaimLawsuitID, ReleaseTypeID, ExecutedReleaseDate, SPUsername)
	VALUES (@ClaimLawsuitID, @ReleaseTypeID, @ReleaseDate, @EmailAddress)
	END

	UPDATE tblClaimLawsuitStatusTracking
	SET IsPrimaryStatus = 0
	WHERE ClaimLawsuitID = @ClaimLawsuitID

	INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, ProcessedDate, StatusDate, StatusNotes, IsPrimaryStatus)
	VALUES(@ClaimLawsuitID, 23, GETDATE(), @ReleaseDate, 'Updated by upload of documentID ' + convert(varchar(500),@DocumentID) + ' by ' + @EmailAddress, 1)
 	
	--Insert record into tblMultiClaimantDocument to link document to individual claimant
	INSERT INTO tblMultiClaimantDocument (DocumentID, ClaimantPersonalInfoID, ClaimLawsuitID)
	SELECT @DocumentID, @ClaimantPersonalInfoID, @ClaimLawsuitID



END

GO
