SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qrysfrmOriginalNoticeLetter]
(@CorrespondenceID bigint)
AS 
SET NOCOUNT ON

SELECT  CorrespondenceID, NoticeResponseID
FROM tblResponse 
WHERE CorrespondenceID = @CorrespondenceID
ORDER BY CorrespondenceID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmOriginalNoticeLetter] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmOriginalNoticeLetter] TO [base_user]
GRANT ALTER ON  [dbo].[qrysfrmOriginalNoticeLetter] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmOriginalNoticeLetter] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmOriginalNoticeLetter] TO [power_user]
GO
