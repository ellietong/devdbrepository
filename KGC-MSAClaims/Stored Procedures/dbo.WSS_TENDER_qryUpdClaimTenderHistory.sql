SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryUpdClaimTenderHistory]
(@ClaimTenderHistoryID bigint, @TenderStatusID int, @emailaddress nvarchar(500))
AS
BEGIN
	SET NOCOUNT ON;

    --Test vars
    --DECLARE @TenderHistoryID bigint = 218951
    
	UPDATE tblClaimTenderHistory 
	SET TenderStatusID	=	@TenderStatusID,
		SPUsername		=	@emailaddress
	WHERE ClaimTenderHistoryID = @ClaimTenderHistoryID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryUpdClaimTenderHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryUpdClaimTenderHistory] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryUpdClaimTenderHistory] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryUpdClaimTenderHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryUpdClaimTenderHistory] TO [power_user]
GO
