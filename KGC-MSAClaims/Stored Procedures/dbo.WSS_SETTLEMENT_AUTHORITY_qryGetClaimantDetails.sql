SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] 
	-- Add the parameters for the stored procedure here
(@ClaimantPersonalInfoID int, 
 @ClaimID int = NULL, 
 @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--DECLARE @ClaimantPersonalInfoID int = 42751
--DECLARE @ClaimID int = 47820
--DECLARE @EmailAddress nvarchar(250) = 'owenss@kcic.com'

DECLARE @EmploymentInfo nvarchar(max) = ''
DECLARE @ProductInfo nvarchar(max) = ''
DECLARE @DiagnosisInfo nvarchar(max) = ''
DECLARE @SmokingInfo nvarchar(max) = ''
DECLARE @TDStyle nvarchar(max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'
DECLARE @TDStyleHidden nvarchar(max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; display:none;'

IF (SELECT Count(ClaimantEmploymentID) FROM tblclaimantemployment WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID) > 0
BEGIN

DECLARE @ClaimantEmploymentInfo TABLE (Jobsite nvarchar(1000), JobSiteCity nvarchar(500), State nvarchar(100), Occupation nvarchar(1000), EmpStartDate nvarchar(100), EmpEndDate nvarchar(100), Notes nvarchar(max))
INSERT INTO @ClaimantEmploymentInfo
SELECT DISTINCT Jobsite, JobSiteCity, S.State, O.Occupation, CONVERT(nvarchar(25), EmpStartDate, 101), CONVERT(nvarchar(25), EmpEndDate, 101), Comments
		FROM tblClaimantEmployment ce 
		INNER JOIN tblClaimantPersonalInfo cpi ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT Join tblOccupation o on o.OccupationID = ce.OccupationID
		LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN tblState s ON ce.JobsiteStateID = s.StateID
		WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		ORDER BY CONVERT(nvarchar(25), EmpStartDate, 101), CONVERT(nvarchar(25), EmpEndDate, 101)

SET @EmploymentInfo = @EmploymentInfo + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	

N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jobsite</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">City</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">State</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Occupation</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Employment Start Date</th>
<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Employment End Date</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Notes</th>' +
	
CAST ( (
SELECT DISTINCT	@TDStyle AS 'td/@style', (Jobsite) as td, '',
		@TDStyle AS 'td/@style', (COALESCE(JobSiteCity, ' ')) as td, '',
		@TDStyle AS 'td/@style', (COALESCE(State, ' ')) as td, '',
		@TDStyle AS 'td/@style', (COALESCE(Occupation, ' ')) as td, '',
		@TDStyle AS 'td/@style', EmpStartDate as td, '',
		@TDStyle AS 'td/@style', EmpEndDate as td, '',
		@TDStyleHidden AS 'td/@style', CAST((COALESCE(CONVERT(nvarchar(25), EmpStartDate, 101), ' ')) AS DATE) as td, '',
		@TDStyleHidden AS 'td/@style', CAST((COALESCE(CONVERT(nvarchar(25), EmpEndDate, 101), ' ')) AS DATE) as td, '',
		@TDStyle AS 'td/@style', (COALESCE(Notes, ' ')) as td, ''
	FROM @ClaimantEmploymentInfo
	ORDER BY CAST((COALESCE(CONVERT(nvarchar(25), EmpStartDate, 101), ' ')) AS DATE), CAST((COALESCE(CONVERT(nvarchar(25), EmpEndDate, 101), ' ')) AS DATE)

for xml path( 'tr' ), type) AS NVARCHAR(MAX) )

+ 
	
N'</table>' +  N'<br/>'	
END
ELSE
BEGIN
	SET @EmploymentInfo = @EmploymentInfo + 'No employment information available for this claimant.' 
END

	DECLARE @ClaimantEmploymentIDs TABLE (ceIDs int)
	INSERT INTO @ClaimantEmploymentIDs
	SELECT DISTINCT ClaimantEmploymentID
	FROM tblClaimantEmployment
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID

IF (SELECT Count(ClaimantProductID) FROM tblClaimantProduct cp INNER JOIN @ClaimantEmploymentIDs ceid ON cp.ClaimantEmploymentID = ceid.ceIDs) > 0
BEGIN

DECLARE @ClaimantProductInfo TABLE (ProductManufacturer nvarchar(1000), ProductType nvarchar(1000), ProductName nvarchar(1000), StartDate nvarchar(100), EndDate nvarchar(100), Notes nvarchar(max))
INSERT INTO @ClaimantProductInfo
SELECT DISTINCT ProductManufacturer, ProductType, ProductName, CONVERT(nvarchar(25), StartDate, 101), CONVERT(nvarchar(25), EndDate, 101), Notes
	FROM tblClaimantProduct cp
	INNER Join tblProduct p on p.ProductID = cp.ProductID
	LEFT Join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
	LEFT Join tblProductManufacturer pm on pm.ProductManufacturerID = p.ProductManufacturerID
	LEFT Join tblProductName pn on pn.ProductNameID = p.ProductNameID
	LEFT Join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
	INNER JOIN (
				SELECT DISTINCT ceIDs
				FROM @ClaimantEmploymentIDs
				) ceid ON cp.ClaimantEmploymentID = ceid.ceIDs
	ORDER BY CONVERT(nvarchar(25), StartDate, 101), CONVERT(nvarchar(25), EndDate, 101)

SET @ProductInfo = @ProductInfo + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	

N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Manufacturer - Type - Name</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Exposure Start Date</th>
<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Exposure End Date</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Notes</th>' +
	
--(select * from (	
--select 
CAST ( (
SELECT DISTINCT	@TDStyle AS 'td/@style', (  COALESCE(ProductManufacturer, 'UNSPECIFIED') + ' - ' + COALESCE(ProductType, 'UNSPECIFIED') + ' - ' + COALESCE(ProductName, 'UNSPECIFIED')) as td, '',
		@TDStyle AS 'td/@style', StartDate as td, '',
		@TDStyle AS 'td/@style', EndDate as td, '',
		@TDStyleHidden AS 'td/@style', CAST((COALESCE(CONVERT(nvarchar(25), StartDate, 101), ' ')) AS DATE) as td, '',
		@TDStyleHidden AS 'td/@style', CAST((COALESCE(CONVERT(nvarchar(25), EndDate, 101), ' ')) AS DATE) as td, '',
		@TDStyle AS 'td/@style', ( COALESCE(Notes, ' ')) as td, ''
		FROM @ClaimantProductInfo
		ORDER BY CAST((COALESCE(CONVERT(nvarchar(25), StartDate, 101), ' ')) AS DATE), CAST((COALESCE(CONVERT(nvarchar(25), EndDate, 101), ' ')) AS DATE)

	for xml path( 'tr' ), type) AS NVARCHAR(MAX) )  
	--) emp
	--)
	+ 
	
	N'</table>' +  N'<br/>'	

END
ELSE
BEGIN
	SET @ProductInfo = @ProductInfo + 'No product information available for this claimant.'
END



IF (SELECT Count(ClaimDiseaseID) FROM tblClaimDisease WHERE ClaimID = @ClaimID) > 0
	BEGIN

SET @DiagnosisInfo = @DiagnosisInfo + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	

N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disease</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Diagnosis Date</th>
<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Doctor</th>' +
	
	
CAST ( (
SELECT DISTINCT	@TDStyle AS 'td/@style', (SELECT COALESCE(Disease, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(25), DiseaseDiagnosisDate, 101), ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE((doc.DoctorLastName + ', ' + COALESCE(doc.DoctorFirstName, '') + COALESCE(' ' + doc.DoctorMiddleName, '')), ' ')) as td, ''
		FROM tblClaimDisease CD
		INNER JOIN tblClaim C ON C.ClaimID = CD.ClaimID
		LEFT JOIN (select * from tblDisease union select * from tbldisease_alt) ddis on ddis.DiseaseID = CD.DiseaseID
		LEFT JOIN tblDoctor DOC on CD.DoctorID = DOC.DoctorID
		WHERE cd.ClaimID = @ClaimID	
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	
			
	END
ELSE
	BEGIN
		SET @DiagnosisInfo = @DiagnosisInfo + 'No diagnosis information available for this claimant.'
	END

IF (SELECT Count(ClaimantSmokingHistoryID) FROM tblClaimantSmokingHistory WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID) > 0
	BEGIN

SET @SmokingInfo = @SmokingInfo + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	

N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Packs Per Day</th>
<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Start Date</th>
<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">End Date</th>
<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Packs Per Year</th>' +
	
	
CAST ( (
SELECT DISTINCT	@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(decimal(10,1), (PacksPerDay)), 0)) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(25), SmokingStart, 101), ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(25), SmokingEnd, 101), ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(decimal(10,2), ((DATEDIFF(DAY, SmokingStart, SmokingEnd))/365.00)*PacksPerDay), 0)) as td, ''
		FROM tblClaimantSmokingHistory
		WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	
			
	END
ELSE
	BEGIN
		SET @SmokingInfo = @SmokingInfo + 'There is no smoking history data for this claimant.'
	END

--SELECT @EmploymentInfo,	@ProductInfo, @DiagnosisInfo
--PRINT @EmploymentInfo
--PRINT @ProductInfo
--PRINT @DiagnosisInfo


SELECT DISTINCT CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' 
			ELSE cpi.LastName + ' ' + Suffix + ', ' END + 
			CASE WHEN FirstName IS NULL THEN '' 
				ELSE FirstName + ' ' END + 
				CASE WHEN MiddleName IS NULL THEN '' 
					 ELSE MiddleName + ' ' END AS ClaimantName,
--CASE WHEN Suffix IS NULL THEN STUFF(LOWER(LastName), 1, 1, UPPER(Left(LastName, 1))) + ', ' 
--			ELSE STUFF(LOWER(LastName), 1, 1, UPPER(Left(LastName, 1))) + ' ' + Suffix + ', ' END + 
--			CASE WHEN FirstName IS NULL THEN '' 
--				ELSE STUFF(LOWER(FirstName), 1, 1, UPPER(Left(FirstName, 1))) + ' ' END + 
--				CASE WHEN MiddleName IS NULL THEN '' 
--					 ELSE CASE WHEN LEN(MiddleName) > 1 THEN STUFF(LOWER(MiddleName), 1, 1, UPPER(Left(MiddleName, 1))) ELSE MiddleName END + ' ' END AS ClaimantName, 
	Jurisdiction,
	s.State,
	PlaintiffCounsel, 
	DefenseCounsel,
	NCCReferenceID,
	Material AS ClaimType,
	CONVERT(nvarchar(25), ActiveTrialDate, 101) AS ActiveTrialDate,
	DemandAmount,
	DemandDate,
	OfferAmount,
	OfferDate,
	SettlementFundsRequested,
	SettlementsFundsRequestDate,
	SettlementFundsRequestDesc AS RequestReason, 
	'Request for $' + REPLACE(convert(nvarchar(20), cast(SettlementFundsRequested as money), 1), '.00', '') + ' made ' + CONVERT(nvarchar(20), DATENAME(MONTH, SettlementsFundsRequestDate)) + ' ' + CONVERT(nvarchar(20), DAY(SettlementsFundsRequestDate)) + ', ' +  CONVERT(nvarchar(20), YEAR(SettlementsFundsRequestDate)) + '. ' AS AuthorityRequest,
	'Demand for $' + REPLACE(convert(nvarchar(20), cast(DemandAmount as money), 1), '.00', '') + ' made ' + CONVERT(nvarchar(20), DATENAME(MONTH, DemandDate)) + ' ' + CONVERT(nvarchar(20), DAY(DemandDate)) + ', ' +  CONVERT(nvarchar(20), YEAR(DemandDate)) + '. ' AS SettlementDemand,
	--CASE WHEN OfferAmount IS NULL THEN 'MSA has not made an offer.' 
	CASE WHEN OfferAmount IS NULL THEN '' 
		 ELSE 	'Offer of $' + REPLACE(convert(nvarchar(20), cast(OfferAmount as money), 1), '.00', '') + ' made ' + CONVERT(nvarchar(20), DATENAME(MONTH, OfferDate)) + ' ' + CONVERT(nvarchar(20), DAY(OfferDate)) + ', ' +  CONVERT(nvarchar(20), YEAR(OfferDate)) + '. ' END AS MSAOffer,
	@EmploymentInfo AS EmploymentTable,
	@ProductInfo AS ProductTable,
	@DiagnosisInfo AS DiagnosisTable, 
	@SmokingInfo AS SmokingTable,
	CASE WHEN HasLossofConsortiumClaim = 1 THEN 'Yes.' WHEN HasEstateClaim = 1 THEN 'Yes.' WHEN (SELECT DISTINCT MAX(CAST(IsSecondaryExposure AS tinyint)) FROM tblClaimantEmployment WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID) = 1 THEN 'Yes.' ELSE 'No.' END AS AdditionalClaim

FROM tblClaimantPersonalInfo cpi
	LEFT JOIN tblClaimantSuffix cs ON cpi.SuffixID = cs.SuffixID 
	INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID 
	INNER JOIN tblClaimLawsuit cl ON C.ClaimID = cl.ClaimID 
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblClaimantSettlement cst on cl.ClaimLawsuitID = cst.ClaimLawsuitID 
	LEFT JOIN  tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID 
	LEFT JOIN  tblState s ON s.StateID = l.StateID 
	LEFT JOIN  tblJurisdiction j ON j.JurisdictionID = l.JurisdictionID
	LEFT JOIN  tblDefenseCounselAddress dca ON l.DefenseCOunselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN  tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
	LEFT JOIN  (SELECT * FROM dbo.tblClaimDisease WHERE PrimaryDisease = 1) cd ON cd.ClaimID = c.ClaimID   
	INNER JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE IsPrimaryStatus = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID 
	INNER JOIN tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	LEFT JOIN  tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	LEFT JOIN  tblMaterial m on m.MaterialID = a.MaterialID
	LEFT JOIN  (SELECT * FROM tblClaimantEmployment WHERE COALESCE(IsPrimary, 0) = 1) ce ON cpi.ClaimantPersonalInfoID = ce.ClaimantPersonalInfoID
	WHERE c.ClaimID = @ClaimID


END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] TO [power_user]
GO
