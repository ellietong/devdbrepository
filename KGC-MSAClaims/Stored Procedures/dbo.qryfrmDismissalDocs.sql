SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmDismissalDocs]


As
Set NoCount On

SELECT d.DocumentID, comments, DetailDocumentTypeID, DocumentURL, DocumentTitle,
 d.QCDate, d.QCBy,  d.CreatedBy, AmazonKey--clst.ClaimLawsuitStatusID, DismissalDate,

FROM tblDocuments d
	inner join tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
	inner join tblClaim c on c.ClaimantPersonalInfoID=mcd.ClaimantPersonalInfoID
	inner join tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
	inner join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID

WHERE   (clst.ClaimLawsuitStatusID is not null or clst.ProcessedDate is not null) 
		AND (d.QCDate IS NULL) AND (d.DetailDocumentTypeID = 16) and CreatedBy not like '%Historical%'
		
Group BY
d.DocumentID, comments, DetailDocumentTypeID, DocumentURL, DocumentTitle,
 d.QCDate, d.QCBy,  d.CreatedBy, AmazonKey
 

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalDocs] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalDocs] TO [Claims_User]
GRANT ALTER ON  [dbo].[qryfrmDismissalDocs] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalDocs] TO [power_user]
GO
