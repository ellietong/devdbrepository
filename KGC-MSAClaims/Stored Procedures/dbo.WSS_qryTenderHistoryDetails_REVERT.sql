SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryTenderHistoryDetails_REVERT] (@TenderHistoryID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @NewLine nvarchar(5) = '<br/>'--CHAR(13) + CHAR(10)
    SELECT th.TenderHistoryID, PolicyNum, ActualPStartDate, ActualPEndDate, (PerOccLimit/LayerPerOccLimit) AS Responsibility,
    LName, FName, MName, Address1, Address2, City, tc.StateID, Zip, MailZip, Acknowledged, AcknowledgementDate,
    COALESCE(FName, '') + COALESCE(' '  + MName, ' ') + COALESCE(' ' + LName, '') AS FullName, 
    COALESCE(Address1 + @NewLine, '') +  
    COALESCE(Address2 + @NewLine, '') + 
    COALESCE(City + ', ', '') +
    COALESCE(s.State, '') + ' ' + COALESCE(Zip, '') + Coalesce('-'+MailZip, '') + @NewLine + 
    COALESCE('<a href="mailto:' + Email + '">' + Email + '</a>', '') AS ContactInfo, 
     Email, CarrierName, AdditionalText, TenderBlockName, EffectiveDate, tc.TenderContactID, TenderType
	FROM tblTenderHistory th
	INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
	INNER JOIN tblTenderBlockPolicy tbp ON th.TenderBlockID = tbp.TenderBlockID
	INNER JOIN tblPolicy p ON tbp.PolicyID = p.PolicyID
	INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
	INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID
	INNER JOIN tblTenderContact tc ON p.CarrierID = tc.CarrierID
	INNER JOIN tblPolicyLimits pl ON tbp.PolicyID = pl.PolicyID
	INNER JOIN tblTenderAcknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID AND ta.TenderContactID = tc.TenderContactID
	LEFT JOIN tblState s ON tc.StateID = s.StateID
	LEFT JOIN tblTenderType tt ON th.TenderTypeID = tt.TenderTypeID
	WHERE th.TenderHistoryID = @TenderHistoryID
		AND COALESCE(tc.[To], 0) = 1
	ORDER BY TenderHistoryID ASC
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderHistoryDetails_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderHistoryDetails_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryTenderHistoryDetails_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderHistoryDetails_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderHistoryDetails_REVERT] TO [power_user]
GO
