SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_qryIsDocumentLocked]
(@DocumentID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--DECLARE @DocumentID int
	--SET @DocumentID = 385290
	
	SELECT COUNT(DISTINCT ClaimantSettlementID) 
	FROM tblClaimantSettlement cs 
	LEFT JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblMultiClaimantDocument mcd ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	WHERE mcd.DocumentID = @DocumentID AND PaidDate IS NOT NULL
	GROUP BY c.ClaimantPersonalInfoID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [power_user]
GO
