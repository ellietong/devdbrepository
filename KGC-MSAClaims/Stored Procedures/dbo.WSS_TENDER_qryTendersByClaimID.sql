SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_TENDER_qryTendersByClaimID]
(@ClaimID bigint, @Emailaddress nvarchar(250))-- = 'crookc@kcicllc.com')
	
AS
BEGIN

	SET NOCOUNT ON;
	/* Test Vars */
	--DECLARE @ClaimID int = 11303
	--DECLARE @emailaddress nvarchar(250) = 'tpotocki@allstate.com'
	
	DECLARE @CarrierGroupID int = (	SELECT tc.CarrierGroupID 
									FROM vtblPerson p
									INNER JOIN vTenderContacts tc ON p.PersonID = tc.PersonID
									WHERE Email = @Emailaddress)
	
	
	
	SELECT	DISTINCT th.TenderHistoryID, 
			COALESCE(CONVERT(nvarchar, DOFE, 101), 'N/A') AS DOFE, 
			th.DocumentID,
			ServiceDate,
			th.SentDate as TenderSentDate,
			DocumentTitle,
			DocumentURL,
			tt.TenderType
			--,
			--AcknowledgementDate
	FROM tblTender_History th
		LEFT JOIN tblDocuments cd ON th.DocumentID = cd.DocumentID
		INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		--LEFT JOIN tblTender_Acknowledgement ta ON th.TenderHistoryID = ta.TenderHistoryID
		--LEFT JOIN tblTenderTemplate tt ON th.TenderBlockID = tt.TenderBlockID AND th.TenderTypeID = tt.TenderTypeID
		--INNER JOIN vTenderSentDate tsd ON th.TenderHistoryID = tsd.TenderHistoryID
		LEFT JOIN tblTenderType tt on tt.TenderTypeID = th.TenderTypeID
	WHERE IsCurrentClaim = 1 AND cl.IsPrimaryForClaim = 1 
		AND th.ClaimID = @ClaimID
	

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTendersByClaimID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTendersByClaimID] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryTendersByClaimID] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTendersByClaimID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTendersByClaimID] TO [power_user]
GO
