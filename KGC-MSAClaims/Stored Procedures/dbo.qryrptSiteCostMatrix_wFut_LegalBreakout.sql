SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptSiteCostMatrix_wFut_LegalBreakout] (@siteID int)
AS 
SET NOCOUNT ON


/*The purpose of this script is to determine the past cost totals for 
Environmental Invoices by site.
The detailed invoice data is then pulled for each of the top invoiced sites.
*/

--DECLARE @SiteID int
--SET @SiteID=0


DECLARE @maxSiteID int
SET @maxSiteID= (Select MAX(siteid) from tblEnviroSitesDetail)




------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, PULL INCLUDED SITE PAST COSTS by site
------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @PastCosts TABLE (siteid int, PrevBilledLegal money, PrevBilledOther money, 
	AdtlAmt_Legal money, AdtlAmt_Other money)
INSERT INTO @PastCosts
SELECT sc.siteid, 
	SUM(CASE WHEN (sc.Billed = 1 AND sc.CostTypeID = 1) THEN sc.ReimbursableAmount ELSE 0 END) AS PrevBilledLegal, 
	SUM(CASE WHEN (sc.Billed = 1 AND sc.CostTypeID <> 1) THEN sc.ReimbursableAmount ELSE 0 END) AS PrevBilledOther, 
	SUM(CASE WHEN ((sc.Billed <>1 OR sc.Billed IS NULL) AND sc.CostTypeID = 1) THEN sc.ReimbursableAmount ELSE 0 END) AS AdtlAmt_Legal,
	SUM(CASE WHEN ((sc.Billed <>1 OR sc.Billed IS NULL) AND sc.CostTypeID <> 1) THEN sc.ReimbursableAmount ELSE 0 END) AS AdtlAmt_Other
FROM tblSiteCosts sc
GROUP BY sc.siteid
ORDER BY sc.siteid

------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, PULL INCLUDED SITE FUTURE COSTS by site
------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @FutureCosts table (SiteID int, LiabPercent decimal(18,4), FutureCosts money)
INSERT INTO @FutureCosts
SELECT fc.siteid, CASE WHEN fc.LiabPercent IS NULL THEN 1
	ELSE fc.LiabPercent END AS LiabPercent,
	(fc.TotalCosts * fc.LiabPercent) AS FutureCosts
FROM tblFutureCosts fc
	LEFT JOIN tblEnviroSitesDetail e ON fc.siteid = e.siteid
GROUP BY fc.siteid, CASE WHEN fc.LiabPercent IS NULL THEN 1
	ELSE fc.LiabPercent END, TotalCosts, LiabPercent

------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, PULL INCLUDED SITE INFO by site
------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @Site table (SiteID int, SiteName nvarchar (150),  state nvarchar(10), country nvarchar(50), excludedsite bit)
INSERT INTO @Site
SELECT SiteID, SiteName, state, country, coalesce(excludedsite, 0)
FROM tblEnvirositesDetail d
	LEFT JOIN tblstate t on t.stateid = d.stateid
	LEFT JOIN tblcountry c on c.countryid = d.countryid
GROUP BY SiteID, SiteName, state, country, ExcludedSite

------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, FLAG PREVIOUSLY BILLED SITES
------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @Billed table (SiteId int, Flag bit)
INSERT INTO @Billed
SELECT SiteID, 1
FROM tblSiteCosts
WHERE Billed = 1
GROUP BY SiteID
ORDER BY SiteID

------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, FLAG SITES WITH FUTURE COSTS
------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @Future table (SiteID int, Flag bit)
INSERT INTO @Future
SELECT siteID, 1
FROM tblFutureCosts
GROUP BY SiteID
ORDER BY SiteID

------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, PULL DATA TOGETHER
------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT s.SiteID, s.SiteName, s.state, s.country,
	CASE WHEN p.PrevBilledLegal IS NULL THEN 0 ELSE p.PrevBilledLegal END AS 'PrevBilledLegal',
	CASE WHEN p.PrevBilledOther IS NULL THEN 0 ELSE p.PrevBilledOther END AS 'PrevBilledOther',
	CASE WHEN p.AdtlAmt_Legal IS NULL THEN 0 ELSE p.AdtlAmt_Legal END AS 'AdtlAmt_Legal', 
	CASE WHEN p.AdtlAmt_Other IS NULL THEN 0 ELSE p.AdtlAmt_Other END AS 'AdtlAmt_Other', 
	CASE WHEN p.PrevBilledLegal IS NULL THEN 0 ELSE p.PrevBilledLegal END  +
	CASE WHEN p.PrevBilledOther IS NULL THEN 0 ELSE p.PrevBilledOther END  +
	CASE WHEN p.AdtlAmt_Legal IS NULL THEN 0 ELSE p.AdtlAmt_Legal END  +
	CASE WHEN p.AdtlAmt_Other IS NULL THEN 0 ELSE p.AdtlAmt_Other END AS PastCosts,
	CASE WHEN fc.futurecosts IS NULL THEN 0 ELSE fc.futurecosts END AS 'FutureCosts',
	CASE WHEN p.PrevBilledLegal IS NULL THEN 0 ELSE p.PrevBilledLegal END +
	CASE WHEN p.PrevBilledOther IS NULL THEN 0 ELSE p.PrevBilledOther END +
	CASE WHEN p.AdtlAmt_Legal IS NULL THEN 0 ELSE p.AdtlAmt_Legal END +
	CASE WHEN p.AdtlAmt_Other IS NULL THEN 0 ELSE p.AdtlAmt_Other END + 
	CASE when fc.futurecosts IS NULL THEN 0 ELSE fc.futurecosts END as Total, 
	CASE WHEN b.Flag = 1 then 1 else 0 END AS 'Billed',
	CASE WHEN f.Flag = 1 then 1 else 0 END AS 'Future',
	fc.LiabPercent, excludedsite 
FROM @site S
	LEFT JOIN @PastCosts P on p.siteid = S.siteid
	LEFT JOIN @billed B on B.siteid = S.siteid
	LEFT JOIN @Future F on F.siteid = S.siteid
	LEFT JOIN @FutureCosts fc on fc.siteid = s.siteid
WHERE CASE WHEN p.PrevBilledLegal is null then 0 else p.PrevBilledLegal end +
	CASE WHEN p.PrevBilledOther is null then 0 else p.PrevBilledOther end +
	CASE WHEN p.AdtlAmt_Legal is null then 0 else p.AdtlAmt_Legal end +
	CASE WHEN p.AdtlAmt_Other is null then 0 else p.AdtlAmt_Other end + 
	CASE WHEN fc.futurecosts is null then 0 else fc.futurecosts END >=0
	and s.SiteID >= COALESCE(@siteid, 0) 
AND S.SiteID <= COALESCE(NULLIF(@siteid, 0), @maxsiteid)

ORDER BY s.sitename 
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSiteCostMatrix_wFut_LegalBreakout] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSiteCostMatrix_wFut_LegalBreakout] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptSiteCostMatrix_wFut_LegalBreakout] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSiteCostMatrix_wFut_LegalBreakout] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSiteCostMatrix_wFut_LegalBreakout] TO [power_user]
GO
