SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[REVERT_qryInternalEmailNewPayments] 
AS
BEGIN


/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)

SET @Subject = 	
'MSA: New Payments Uploaded ' + CONVERT(varchar, getdate(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Payments which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewPaymentsTable TABLE (DocID int, docTitle nvarchar(500), DocumentType nvarchar(250), FirmAddressID int, lName nvarchar(50), 
	
fName nvarchar(50), ClaimantPersonalInfoID bigint, NCCReferenceID nvarchar(11), CheckNumber nvarchar(250), PaidDate date)
INSERT INTO @NewPaymentsTable
SELECT DISTINCT d.DocumentID, '<a href="https://admin.kcicextranet.com/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId='+COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle, 
ClaimantDocumentType as DocumentType,l.DefenseCOunselAddressID, cpi.LastName, cpi.FirstName, cpi.ClaimantPersonalInfoID,NCCReferenceID, 
COALESCE(cs.CheckNumber, ''), COALESCE(cs.PaidDate, NULL)
FROM tblDocuments d
INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
left join tblclaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblclaimlawsuit cl on cl.claimid = c.claimid	
left join tbllawsuit l on l.lawsuitid =cl.lawsuitid
inner join tblClaimantSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID
inner join tblClaimantDocumentType cdt on d.DetailDocumentTypeID=cdt.ClaimantDocumentTypeID
WHERE datecreated BETWEEN GETDATE()-1 AND GETDATE() 
AND detailDocumentTypeID IN (29, 31, 34, 44, 50)
AND FirmID IS NOT NULL
AND IsPrimaryForClaim = 1
AND IsCurrentClaim = 1	
GROUP BY d.documentID, d.DocumentTitle,ClaimantDocumentType,
DefenseCOunselAddressID, LastName, FirstName, cpi.ClaimantPersonalInfoID, NCCReferenceID,cs.CheckNumber, cs.PaidDate
	

DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar(max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar(max), @ClaimantID bigint, @ClaimantList nvarchar(max), @NCCRef nvarchar(11)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for
SELECT DocID, COALESCE(Lname, '') + ', ' +  COALESCE(Fname, ''), ClaimantPersonalInfoID, NCCReferenceID FROM @NewPaymentsTable
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID, @NCCRef
WHILE @@FETCH_STATUS = 0
BEGIN

IF(LEN(@ClaimantList)=0 OR @ClaimantList IS NULL)
BEGIN
	
SET @ClaimantList = '<ul> <a href="https://admin.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+ '&ClaimantTab=3&ResolutionTab=1'+'">'  + @Claimant + coalesce(' ('+ CONVERT(nvarchar(50), @NCCRef) + ')', '') +  '</a></ul>'
END
ELSE
BEGIN
	
SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="https://admin.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+ '&ClaimantTab=3&ResolutionTab=1'+'">' + COALESCE(@Claimant, '') + coalesce(' ('+ CONVERT(nvarchar(50), @NCCRef) + ')', '') + '</a></ul>'

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID, @NCCRef
PRINT @@FETCH_STATUS
IF @DocID <> @LastDocID OR @@FETCH_STATUS <> 0
BEGIN
	
INSERT INTO @ClaimantPerDoc
	
SELECT @LastDocID, @ClaimantList
	
SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints
	


--Declare and populate the body of the email for NCC, Gleason, & ReedSmith
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'



--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

IF (Select COUNT(docTitle) from @NewPaymentsTable) >0 
begin
set @BodyMessage = @BodyMessage + '<H4>The following payments have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table>' + 
	
	
N'<tr><th>Document Title</th><th>Document Type</th><th>Linked Claimants (NCC #) </th><th>Check Number</th><th>Paid Date</th>' +
	
	
CAST ( (
SELECT td = docTitle, '', td=DocumentType, '', td = cl.ClaimantList, '', td = CheckNumber, '', td = PaidDate, ''
FROM @NewPaymentsTable C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
GROUP BY docTitle, DocumentType, cl.ClaimantList, CheckNumber, PaidDate 
ORDER BY DocumentType

for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
N'</table>' +  N'<br/>'	

end
ELSE
begin
Set @BodyMessage = @BodyMessage + '<H4>No payments have been uploaded in the last 24 hours. </H4>'
end


--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href="https://admin.kcicextranet.com/clients/msa/cp/pages/Documents.aspx">here</A> to access the document(s).</b><br><br>'


--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

DECLARE @RecipientList nvarchar(500)
SET @RecipientList=''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar(500)
DECLARE contact_curs CURSOR FOR
select email
from vtblPerson
where FirmID in (68, 31, 201, 66)
and ComplaintContact = 1
	

OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS=0
BEGIN 

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--set @BodyMessage = @BodyMessage +  '<br><br> Recipients: ' + @RecipientList


--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The MSAProfile sends from msa@kcicllc.com
@profile_name='Admin',
@recipients = 'msa@kcicllc.com',
@copy_recipients = 'potterm@kcicllc.com; monahanc@kcicllc.com',
--@blind_copy_recipients = 'msa@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'



END

GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalEmailNewPayments] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalEmailNewPayments] TO [base_user]
GRANT ALTER ON  [dbo].[REVERT_qryInternalEmailNewPayments] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalEmailNewPayments] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalEmailNewPayments] TO [power_user]
GO
