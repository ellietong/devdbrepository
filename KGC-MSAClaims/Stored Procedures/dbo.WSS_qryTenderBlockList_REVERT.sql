SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryTenderBlockList_REVERT]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT TenderBlockID, TenderBlockName + CASE WHEN Active = 1 THEN ' (Active)' ELSE ' (Inactive)' END AS TenderBlockName , Active
	FROM tblTenderBlock 
	ORDER BY Active DESC, TenderBlockName
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderBlockList_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderBlockList_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryTenderBlockList_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryTenderBlockList_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryTenderBlockList_REVERT] TO [power_user]
GO
