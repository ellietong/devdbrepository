SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailNewDismissals_RS]
AS
BEGIN

/*Query run weekly to send an alert email for new dismissals to Reed Smith. - DRE*/

--Declare and set email inputs.
DECLARE @Subject NVARCHAR (100)
DECLARE @LogoHeader NVARCHAR (2000)
SET @Subject = 'MSA Claims: New Dismissals Uploaded ' + CONVERT(varchar, GETDATE(), 107)
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"><img src="http://www.kcic.com/i/banner_home.jpg" alt="logo"/>'

--Declare and set desired email time coverage.
DECLARE @Today4PM DATETIME
DECLARE @LastWeek4PM DATETIME
SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')
SET @LastWeek4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 7)) + ' 16:00:00.000')

--Create a temp table for Dismissals which have been uploaded in the past week.
DECLARE @NewDismissalsTable TABLE
(
DocumentID INT,
DocumentTitle NVARCHAR (MAX),
LastName NVARCHAR (50),
FirstName NVARCHAR (50),
NCCReferenceID NVARCHAR (15),
ClaimantPersonalInfoID BIGINT,
UploadDate DATE,
ClaimLawsuitStatus NVARCHAR (500),
Username nvarchar (300)
)

--Populate the temp table with Dismissals from the past week.
INSERT INTO @NewDismissalsTable
	SELECT
		d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/MSA/cp/Pages/DocumentProfile.aspx?ItemId='
		+ COALESCE(CONVERT(nvarchar(50), d.DocumentID), '')
		+ '">' + COALESCE(d.DocumentTitle, '') + '</a>'
		AS DocumentTitle,
		cpi.LastName,
		cpi.FirstName,
		NCCReferenceID,
		cpi.ClaimantPersonalInfoID,
		COALESCE(MAX(d.DateCreated), '1/1/1900') AS UploadDate,
		cls.ClaimLawsuitStatus,
		COALESCE(CreatedBy, '') AS Username
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatus cls ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
	WHERE d.DateCreated BETWEEN @LastWeek4PM AND @Today4PM
	AND d.DetailDocumentTypeID IN (16)
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	AND IsPrimaryStatus = 1
	GROUP BY	LastName,
				d.documentID,
				d.DocumentTitle,
				FirstName,
				cpi.ClaimantPersonalInfoID,
				ClaimLawsuitStatus,
				cl.LawsuitID,
				CreatedBy,
				NCCReferenceID
	ORDER BY DocumentID, ClaimLawsuitStatus

SELECT
	*
FROM @NewDismissalsTable
--Create a temp table to store one or many associated claimants for each doc.					
DECLARE @ClaimantPerDoc TABLE
(
DocumentID INT,
ClaimantList NVARCHAR (MAX),
ClaimLawsuitStatus NVARCHAR (500)
)

--Declare variables for iterating over the docs and filling the document temp table.
DECLARE @DocumentID INT,
@LastDocID INT,
@Claimant NVARCHAR (MAX),
@ClaimantID BIGINT,
@ClaimantList NVARCHAR (MAX) = '',
@NCCRefID NVARCHAR (15),
@ClaimLawsuitStatus NVARCHAR (500),
@LastClaimLawsuitStatus NVARCHAR (500)

--Declare and run a cursor over the temp dismissals table, populating the temp document table.		
DECLARE Cur_Dismissals CURSOR Local Fast_Forward
FOR SELECT
	DocumentID,
	COALESCE(LTRIM(RTRIM(LastName)), '') + ',&nbsp;' + COALESCE(LTRIM(RTRIM(FirstName)), ''),
	ClaimantPersonalInfoID,
	LTRIM(RTRIM(NCCReferenceID)),
	ClaimLawsuitStatus
FROM @NewDismissalsTable


OPEN Cur_Dismissals
FETCH NEXT FROM Cur_Dismissals INTO @DocumentID, @Claimant, @ClaimantID, @NCCRefID, @ClaimLawsuitStatus
WHILE @@FETCH_STATUS = 0
BEGIN
SET @ClaimantList = COALESCE(@ClaimantList, '')
+ '<ul style="margin:0; padding:0;"> <a href="https://www.kcicextranet.com/clients/MSA/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
+ CONVERT(nvarchar(50), @ClaimantID) + '">'
+ COALESCE(@Claimant, '') + COALESCE('&nbsp;(' + @NCCRefID + ')', '') + '</a></ul>'
SET @LastDocID = @DocumentID
SET @LastClaimLawsuitStatus = @ClaimLawsuitStatus

FETCH NEXT FROM Cur_Dismissals INTO @DocumentID, @Claimant, @ClaimantID, @NCCRefID, @ClaimLawsuitStatus

IF @DocumentID < > @LastDocID OR ((@LastClaimLawsuitStatus < > @ClaimLawsuitStatus) AND (@DocumentID = @LastDocID)) OR @@FETCH_STATUS < > 0
BEGIN
INSERT INTO @ClaimantPerDoc
	SELECT
		@LastDocID,
		@ClaimantList,
		@LastClaimLawsuitStatus

SET @ClaimantList = ''
END
END
CLOSE Cur_Dismissals
DEALLOCATE Cur_Dismissals
SELECT
	*
FROM @ClaimantPerDoc
--Declare and populate the body of the email
DECLARE @BodyMessage NVARCHAR (MAX)
--Set initial HTML markup for the email body. NOTE: Gmail does not render CSS style placed in <style> tags (must be inline).
SET @BodyMessage = '<HTML>
								<BODY>'
+ @LogoHeader
DECLARE @LastLawsuitID NVARCHAR (250) = ''
--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
IF (SELECT
	COUNT(DocumentTitle)
FROM @NewDismissalsTable) > 0
BEGIN
SET @BodyMessage = @BodyMessage
+ '<H4>The following dismissals have been processed in the last week (7 days): </H4>'
SET @BodyMessage = @BodyMessage
+ N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">'
SET @BodyMessage = @BodyMessage
+ N'<tr>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Document Title</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Associated Claimant(s)<br/>(NCC Reference ID)</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Status</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Upload Date</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Uploaded By</th>
						</tr>'


DECLARE @DocTitle NVARCHAR (250),
@clClaimantList NVARCHAR (MAX),
@UploadDate NVARCHAR (300),
@clClaimLawsuitStatus NVARCHAR (500),
@Username Nvarchar (300)
DECLARE Cur_Docs CURSOR
FOR SELECT
	DocumentTitle,
	cl.ClaimantList,
	CAST(MAX(UploadDate) AS varchar(MAX)),
	ndt.ClaimLawsuitStatus,
	Username
FROM @NewDismissalsTable ndt
INNER JOIN @ClaimantPerDoc cl ON ndt.DocumentID = cl.DocumentID AND ndt.ClaimLawsuitStatus = cl.ClaimLawsuitStatus
GROUP BY	cl.DocumentID,
			ndt.DocumentTitle,
			cl.ClaimantList,
			ndt.ClaimLawsuitStatus,
			ndt.Username
ORDER BY cl.DocumentID

OPEN Cur_Docs
FETCH NEXT FROM Cur_Docs INTO @DocTitle, @clClaimantList,
@UploadDate, @clClaimLawsuitStatus, @Username
WHILE @@FETCH_STATUS = 0
BEGIN
SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @DocTitle + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @clClaimantList + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @clClaimLawsuitStatus + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @UploadDate + ' </td>
							'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @Username + ' </td>
							'
SET @BodyMessage = @BodyMessage + '</td>
														</tr>'
FETCH NEXT FROM Cur_Docs INTO @DocTitle,
@clClaimantList, @UploadDate,
@clClaimLawsuitStatus, @UserName
END
CLOSE Cur_Docs
DEALLOCATE Cur_Docs
SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'
END
ELSE
BEGIN
SET @BodyMessage = @BodyMessage
+ '<H4>No Dismissals have been uploaded in the last week (7 days). </H4>'
END

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--KCIC address (disclaimer applied by mail server)
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
												733 10th Street, NW, Suite 3001<br>
												Washington, DC 20001<br>
												(t) 202-772-2300<br>
												(f) 202-772-2333
											</BODY>
										</HTML>'
--QC the message	
PRINT @BodyMessage

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'JSchramm@reedsmith.com; akelly@reedsmith.com',
								@blind_copy_recipients = 'msa@kcic.com; potterm@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'
END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewDismissals_RS] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewDismissals_RS] TO [base_user]
GRANT ALTER ON  [dbo].[qryExternalEmailNewDismissals_RS] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewDismissals_RS] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewDismissals_RS] TO [power_user]
GO
