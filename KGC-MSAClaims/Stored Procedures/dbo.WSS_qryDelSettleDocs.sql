SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_qryDelSettleDocs] (@SettleDocID int, @emailaddress nvarchar(250))
as

--DECLARE @SettlePaymentID int
--SET @SettlePaymentID = 23
--DECLARE @EmailAddress nvarchar(100)
--set @EmailAddress = 'hubbardb@kcicllc.com' 

UPDATE tblSettleDocs
SET SPUsername = @emailaddress
WHERE SettleDocID = @SettleDocID

Delete from tblSettleDocs
where SettleDocID = @SettleDocID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelSettleDocs] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryDelSettleDocs] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelSettleDocs] TO [power_user]
GO
