SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qrySettlementDetails]
@ClaimID bigint=null
AS
BEGIN
--DECLARE @ClaimID int
--SET @ClaimID = 1
SELECT     C.ClaimID,   
                       rt.ReleaseType,  PaidDate, CheckNumber,  SettlementDate, SettlementAmount, ClaimantSettlementID, PostingDate
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID=CL.ClaimLawsuitID left outer join 
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID

                      
            
WHERE C.ClaimID = @ClaimID AND cl.IsPrimaryForClaim = 1 
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySettlementDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySettlementDetails] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qrySettlementDetails] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qrySettlementDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qrySettlementDetails] TO [power_user]
GO
