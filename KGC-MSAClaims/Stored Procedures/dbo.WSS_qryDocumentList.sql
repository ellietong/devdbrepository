SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDocumentList]
(@ClaimantPersonalInfoID bigint = null, @emailaddress nvarchar(250))
AS
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'enseyd@kcicllc.com'
--DECLARE @ClaimantPersonalInfoID bigint
--SET @ClaimantPersonalInfoID = 3409 
BEGIN

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)					
	
DECLARE @locked TABLE (DocumentID bigint, Locked bit)

INSERT INTO @locked

SELECT mcd.DocumentID, CASE WHEN (COUNT(c.ClaimID)+ COUNT(ps.PaymentSettlementID)) > 0 THEN 1 ELSE 0 END  FROM 
	tblClaim c
	
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	LEFT JOIN tblPaymentSettlement ps ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
	INNER JOIN tblLawsuit l ON cl.LawsuitID	 = l.LawsuitID
	INNER JOIN tblMultiClaimantDocument mcd ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	WHERE COALESCE(c.IsCurrentClaim,0) = 1 AND
			COALESCE(IsPrimaryForClaim, 0) = 1 AND
			ClaimLawsuitStatusID IN (15,16,17)
			AND c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	GROUP BY mcd.DocumentID	
	
					
IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR RIGHT(@emailaddress, 8) = 'kcic.com' OR @NoFilters = 1
	BEGIN
		SELECT  DISTINCT   COALESCE(d.DocumentID,NULL) AS DocumentID, d.SPDocId, COALESCE(d.DocumentTitle,'') AS DocumentTitle,
		CASE WHEN d.FirmID IS NULL THEN DocumentURL ELSE '/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/clients/msa/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), @ClaimantPersonalInfoID) END AS DocumentURL, dg.DocumentGroup, cdt.ClaimantDocumentType, 
							  f.Firm, d.AcceptedBy, d.DateAccepted, d.DateCreated, mcd.ClaimantPersonalInfoID, DocumentURL as DirectDocumentURL, CreatedBy, COALESCE(Locked, 0) AS Locked,
		CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical, AmazonKey
		FROM tblMultiClaimantDocument mcd INNER JOIN         
			dbo.tblDocuments AS d ON mcd.DocumentID = d.documentID INNER JOIN
							  dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID INNER JOIN
							  dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID INNER JOIN
							  dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID LEFT JOIN
							  dbo.vtblFirm AS f ON f.FirmID = d.FirmID
							  LEFT JOIN @locked lock ON mcd.DocumentID = lock.DocumentID          
		WHERE mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND (dg.DocumentGroupID NOT IN (2,6) AND cdt.ClaimantDocumentTypeID NOT IN (40) OR cdt.ClaimantDocumentTypeID = 139)	
		UNION SELECT -1, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL				
		ORDER BY DocumentTitle
	END
ELSE IF (@InsurerRole = 1)
	BEGIN
		SELECT  DISTINCT   COALESCE(d.DocumentID,NULL) AS DocumentID, d.SPDocId, COALESCE(d.DocumentTitle,'') AS DocumentTitle,
		CASE WHEN d.FirmID IS NULL THEN DocumentURL ELSE '/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/clients/msa/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), @ClaimantPersonalInfoID) END AS DocumentURL, dg.DocumentGroup, cdt.ClaimantDocumentType, 
							  f.Firm, d.AcceptedBy, d.DateAccepted, d.DateCreated, mcd.ClaimantPersonalInfoID, DocumentURL as DirectDocumentURL, CreatedBy, COALESCE(Locked, 0) AS Locked,
		CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical, AmazonKey
		FROM tblMultiClaimantDocument mcd INNER JOIN         
			dbo.tblDocuments AS d ON mcd.DocumentID = d.documentID INNER JOIN
							  dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID INNER JOIN
							  dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID INNER JOIN
							  dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID LEFT JOIN
							  dbo.vtblFirm AS f ON f.FirmID = d.FirmID  
							  INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID        
							  LEFT JOIN @locked lock ON mcd.DocumentID = lock.DocumentID
		WHERE mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND (dg.DocumentGroupID NOT IN (2,6) AND cdt.ClaimantDocumentTypeID NOT IN (40) OR cdt.ClaimantDocumentTypeID = 139)
		AND c.ClaimID NOT IN (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)								
		AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
		UNION SELECT -1, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
		ORDER BY DocumentTitle
	END
ELSE
	BEGIN
		SELECT  DISTINCT   COALESCE(d.DocumentID,NULL) AS DocumentID, d.SPDocId, COALESCE(d.DocumentTitle,'') AS DocumentTitle,
		CASE WHEN d.FirmID IS NULL THEN DocumentURL ELSE '/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/clients/msa/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), @ClaimantPersonalInfoID) END AS DocumentURL, dg.DocumentGroup, cdt.ClaimantDocumentType, 
							   f.Firm, d.AcceptedBy, d.DateAccepted, d.DateCreated, mcd.ClaimantPersonalInfoID, DocumentURL as DirectDocumentURL, CreatedBy, COALESCE(Locked, 0) AS Locked,
		CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical, AmazonKey
		FROM tblMultiClaimantDocument mcd INNER JOIN         
			dbo.tblDocuments AS d ON mcd.DocumentID = d.documentID INNER JOIN
							  dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID INNER JOIN
							  dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID INNER JOIN
							  dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID INNER JOIN
							  dbo.tblClaim AS c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT JOIN
							  dbo.vtblFirm AS f ON f.FirmID = d.FirmID
							  LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
							  LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
							  INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
							  LEFT JOIN @locked lock ON mcd.DocumentID = lock.DocumentID
		WHERE mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND (dg.DocumentGroupID NOT IN (2,6) AND cdt.ClaimantDocumentTypeID NOT IN (1, 40) OR cdt.ClaimantDocumentTypeID = 139)
		AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
		UNION
		SELECT  DISTINCT   COALESCE(d.DocumentID,NULL) AS DocumentID, d.SPDocId, COALESCE(d.DocumentTitle,'') AS DocumentTitle, 
		CASE WHEN d.FirmID IS NULL THEN DocumentURL ELSE '/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/clients/msa/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), @ClaimantPersonalInfoID) END AS DocumentURL, dg.DocumentGroup, cdt.ClaimantDocumentType, 
							   f.Firm, d.AcceptedBy, d.DateAccepted, d.DateCreated, mcd.ClaimantPersonalInfoID, DocumentURL as DirectDocumentURL, CreatedBy, COALESCE(Locked, 0) AS Locked,
		CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical, AmazonKey
		FROM tblMultiClaimantDocument mcd INNER JOIN         
			dbo.tblDocuments AS d ON mcd.DocumentID = d.documentID INNER JOIN
							  dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID INNER JOIN
							  dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID INNER JOIN
							  dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID INNER JOIN
							  dbo.tblClaim AS c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT JOIN
							  dbo.vtblFirm AS f ON f.FirmID = d.FirmID
							  LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
							  LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
							  INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
							  LEFT JOIN @locked lock ON mcd.DocumentID = lock.DocumentID
		WHERE mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND (dg.DocumentGroupID NOT IN (2,6) AND cdt.ClaimantDocumentTypeID NOT IN (1, 40) OR cdt.ClaimantDocumentTypeID = 139)
		and d.FirmID is null
		UNION SELECT -1, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL				
		ORDER BY DocumentTitle

	END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDocumentList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDocumentList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryDocumentList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDocumentList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDocumentList] TO [power_user]
GO
