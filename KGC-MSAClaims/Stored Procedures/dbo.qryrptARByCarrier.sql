SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByCarrier]
(@allocationID int = NULL, @CarrierID int = NULL)
As

SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 56
--DECLARE @CarrierID int
--SET @CarrierID = NULL

DECLARE @maxAllocationID int
DECLARE @maxCarrierID int

SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy



DECLARE @Allocation TABLE (AllocationID int, TotalSIR money, TotalAllocation money, PVTotalAllocation money)
INSERT INTO @Allocation

SELECT AllocationID, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
WHERE AllocationID = @allocationID
GROUP BY AllocationID


DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
CarrierID int, CarrierName nvarchar(250))
INSERT INTO @Policy

SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName
FROM vPolicy
GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName

SELECT ar.DateToPresentValue, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' (' + pinfo.CarrierGroupName + ')' END AS 'CarrierGroupInfo',
	pinfo.CarrierGroupID, pinfo.CarrierID, MAX(ar.DateTime) AS [DateTime], pinfo.CarrierGroupName, pinfo.CarrierName, CONVERT(money, SUM(ar.PV)) AS PV, 
	SUM(ar.Allocation) AS Allocation, SUM(ar.SIRAllocated) AS SIRAllocated, a.AllocationID, a.AllocationDescription, YieldCurveDate as PVRateDate, TotalSIR, TotalAllocation, PVTotalAllocation
FROM vAllocationResults ar
	LEFT JOIN @Policy pinfo ON pinfo.PolicyDateID = ar.PolicyDateID
	LEFT JOIN vAllocation a ON a.AllocationID = ar.AllocationID
	LEFT JOIN tblYieldCurve yc on yc.YieldCurveID = ar.YieldCurveID 
	LEFT JOIN @Allocation at ON ar.AllocationID = at.AllocationID
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationid) 
	AND pinfo.CarrierID >= COALESCE(@CarrierID, 0) AND pinfo.CarrierID <= COALESCE(nullif(@CarrierID,0), @maxCarrierID)
GROUP BY ar.DateToPresentValue, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' (' + pinfo.CarrierGroupName + ')' END, 
	pinfo.CarrierGroupID, pinfo.CarrierID, pinfo.CarrierGroupName, pinfo.CarrierName, a.AllocationID, a.AllocationDescription, YieldCurveDate, TotalSIR, TotalAllocation, PVTotalAllocation
ORDER BY pinfo.CarrierName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByCarrier] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptARByCarrier] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByCarrier] TO [power_user]
GO
