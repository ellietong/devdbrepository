SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptOutstandingSettlementsMissingOnly] (@MatterID int, @SettlementDate datetime)

As

SET NOCOUNT ON

--DECLARE @SettlementDate datetime
--SET @SettlementDate = '1/1/2004'
--DECLARE @MatterID int
--SET @MatterID = 1

DECLARE @MaxMatterID int
SELECT @MaxMatterID = MAX(MatterID) FROM tblMatter


DECLARE @SettlementGroupCount TABLE (SettlementGroupID int, ClaimCount nvarchar(50))
INSERT INTO @SettlementGroupCount	
	SELECT SettlementGroupID, COUNT(SettlementDate) as ClaimCount
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupPaidCount TABLE (SettlementGroupID int, PaidCount nvarchar(50))
INSERT INTO @SettlementGroupPaidCount	
	SELECT SettlementGroupID, COUNT(PaidDate) as PaidCount
	FROM tblClaimantSettlement
	WHERE SettlementGroupID IN	(SELECT SettlementGroupID
								 FROM tblClaimantSettlement
								 WHERE PaidDate IS NOT NULL 
								 GROUP BY SettlementGroupID)
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupMaxDate TABLE (SettlementGroupID int, MaxSettlementDate datetime)
INSERT INTO @SettlementGroupMaxDate
	SELECT SettlementGroupID, MAX(SettlementDate) as MaxSettlementDate
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY MaxSettlementDate


SELECT pl.PlaintiffCounsel , cs.SettlementDate, COALESCE(SettlementDueDate, 1/1/1900) as DatePayable, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, CheckNumber, FullyDocumented, c.MatterID, Matter, cl.LawsuitID, paiddate, case when PaidDate IS not null then SettlementAmount else 0 end as paymentamount,
	@SettlementDate AS SettlementDateAsOf,  DocketNumber,  sgc.ClaimCount, MaxSettlementDate,
	CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
		 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END AS SupportingDocumentation,
	CASE WHEN ReleaseRcvd = 1
		 THEN coalesce( crr.ReleaseType,'Executed' )
		 ELSE 'Release Missing'  END AS ReleaseExecution
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m						 ON c.MatterID = m.MatterID		
	INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs			 ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblLawsuit l						 ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pl	 ON l.PlaintiffCounselAddressID = pl.PlaintiffCounselAddressID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	--INNER JOIN tblClaimLawsuitStatus cls		 ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID	
	LEFT  JOIN tblSettlementGroup s				 ON cs.SettlementGroupID = s.SettlementGroupID
	--LEFT  JOIN tblClaimantDocketNumbers cd		 ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	--LEFT  JOIN @SettlementGroupPaidUnpaid sgp	 ON s.SettlementGroupID = sgp.SettlementGroupID
	LEFT  JOIN @SettlementGroupCount sgc		 ON s.SettlementGroupID= sgc.SettlementGroupID
	LEFT  JOIN @SettlementGroupPaidCount sgpc	 ON s.SettlementGroupID = sgpc.SettlementGroupID
	LEFT  JOIN @SettlementGroupMaxDate sgm		 ON s.SettlementGroupID = sgm.SettlementGroupID
	LEFT JOIN  tblReleasetype crr				 ON crr.ReleaseTypeID =spd.ReleaseTypeID
	LEFT JOIN tblClaimSequence csq				 ON c.ClaimSequenceID = csq.ClaimSequenceID
WHERE MaxSettlementDate >= @SettlementDate
	AND IsPrimaryStatus = 1
	--AND COALESCE(IsCurrentClaim,0) = 1
	AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	AND csq.ClaimSequenceID not in (4,2,3,11)
	AND clst.ClaimLawsuitStatusID in (23,24,25,28)
	AND cl.IsPrimaryForClaim=1
	AND COALESCE(PaidCount,0) <> COALESCE(ClaimCount,0)

GROUP BY cs.SettlementDate, SettlementDueDate, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, CheckNumber, FullyDocumented, c.MatterID, Matter, cl.LawsuitID,paiddate,pl.PlaintiffCounsel ,
	 DocketNumber, sgpc.PaidCount, sgc.ClaimCount, MaxSettlementDate,ReleaseRcvd, case when PaidDate IS not null then SettlementAmount else 0 end ,

CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
			 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END,
	CASE WHEN ReleaseRcvd = 1
		 THEN coalesce( crr.ReleaseType,'Executed' ) 
		 ELSE 'Release Missing'  END
		 
		 having 	(CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
		 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END ) = 'Documentation Needed'
		 
ORDER BY MaxSettlementDate, SettlementGroup, SettlementDate, LastName
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlementsMissingOnly] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlementsMissingOnly] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptOutstandingSettlementsMissingOnly] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlementsMissingOnly] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlementsMissingOnly] TO [power_user]
GO
