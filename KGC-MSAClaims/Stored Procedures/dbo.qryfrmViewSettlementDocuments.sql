SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmViewSettlementDocuments] (@ClaimLawsuitID int)
as

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 17075

Select sd.ClaimantDocumentID, 
UPPER(DocCategory) + ': PACE Document - ' + ClaimantDocumentType + coalesce(' (' + pagenums + ')', '') AS DocumentTypeWithDate, 
	CASE WHEN LEFT(DocumentURL,2) NOT LIKE '/c' THEN DocumentURL ELSE 'https://admin.kcicllc.com' + DocumentURL END AS DocumentLink
From tblSettleDocs sd
INNER JOIN tblSettlePaymentDocumentation pd ON sd.SetPymtID = pd.SetPymtID
LEFT JOIN tblDocumentCategory cd ON sd.DocCategoryID = cd.DocCategoryID
LEFT JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblClaimantDocuments d ON sd.DocumentID = d.ClaimantDocumentID AND d.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
LEFT JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
Where pd.ClaimLawsuitID = @ClaimLawsuitID

UNION

SELECT sd.DocumentID,
case when DateCreated is NULL then DocCategory else UPPER(DocCategory) + ': ' + DocumentTitle + ' - ' + convert(nvarchar,month(DateCreated))+'/'+convert(nvarchar,day(DateCreated))+
	'/'+convert(nvarchar,Year(DateCreated)) end  + coalesce(' (' + pagenums + ')', '')  AS DocumentTypeWithDate,
	CASE WHEN SUBSTRING(DocumentURL,8,8) = 'extranet' THEN 'https://admin' + SUBSTRING(DocumentURL,17,LEN(DocumentURL)-15) 
	WHEN SUBSTRING(DocumentURL,9,8) = 'extranet' THEN 'https://admin' + SUBSTRING(DocumentURL,17,LEN(DocumentURL)-15) 
	ELSE DocumentURL END AS DocumentLink
FROM tblSettleDocs sd
INNER JOIN tblSettlePaymentDocumentation pd ON sd.SetPymtID = pd.SetPymtID
LEFT JOIN tblDocumentCategory cd ON sd.DocCategoryID = cd.DocCategoryID
LEFT JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblDocuments d ON sd.DocumentID = d.DocumentID AND d.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
WHERE pd.ClaimLawsuitID = @ClaimLawsuitID
ORDER BY DocumentTypeWithDate

Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewSettlementDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewSettlementDocuments] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmViewSettlementDocuments] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewSettlementDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewSettlementDocuments] TO [power_user]
GO
