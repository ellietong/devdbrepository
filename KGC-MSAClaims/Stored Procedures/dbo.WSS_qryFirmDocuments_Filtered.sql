SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryFirmDocuments_Filtered](@emailaddress nvarchar (1000)=null, @FilterDocumentTypeID int, @FilterFirmID int)
AS


--DECLARE @Emailaddress nvarchar(250)
--SET @emailaddress = 'MDCochran@mdwcg.com'
--DECLARE @FilterDocumentTypeID int
--DECLARE @FilterFirmID int
--SET @FilterDocumentTypeID = null
--SET @FilterFirmID = null


--Declare and set the temporary field used to check the user's firm based on their login email address
DECLARE @firmid int
SET @firmid = (SELECT DISTINCT FirmID FROM  vtblPerson WHERE Email = @emailaddress)

DECLARE @maxDocType int
SET @maxDocType = (SELECT MAX(ClaimantDocumentTypeID) FROM tblClaimantDocumentType)

DECLARE @MaxFirmID int
SET @MaxFirmID = (SELECT MAX(DefensecounselID) FROM tblDefenseCounsel)

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM  vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)

IF (@NoFilters = 1 OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'))
BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL As DirectDocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, dca.DefenseCounsel + ' (' + COALESCE(DefenseCounselCity, 'Unknown') + ')' AS Firm, Accepted, '/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/clients/msa/cp/Pages/Documents.aspx' AS DocumentURL--,
		--mcd.ClaimantPersonalInfoID
	FROM dbo.tblDocuments AS d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		LEFT JOIN tblClaim c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		--INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
		INNER JOIN tblDefenseCOunselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40) 
	AND d.DetailDocumentTypeID >= COALESCE(@FilterDocumentTypeID, 0) AND d.DetailDocumentTypeID <= COALESCE(@FilterDocumentTypeID, @maxDocType)
	AND dca.DefenseCounselID >= COALESCE(@FilterFirmID, 0) AND dca.DefenseCounselID <= COALESCE(@FilterFirmID, @maxFirmID)
	AND CreatedBy <> 'Historical'
	ORDER BY Accepted ASC, DateCreated DESC

END 
ELSE 
BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL AS DirectDocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, dca.DefenseCounsel + ' (' + COALESCE(DefenseCounselCity, 'Unknown') + ')' AS Firm, Accepted, '/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/clients/msa/cp/Pages/Documents.aspx' AS DocumentURL--,
		--mcd.ClaimantPersonalInfoID
	FROM dbo.tblDocuments AS d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		LEFT JOIN tblClaim c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
		INNER JOIN tblDefenseCOunselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
		
	WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40)
		--AND f.DefenseCounselID = d.FirmID
		AND d.DetailDocumentTypeID >= COALESCE(@FilterDocumentTypeID, 0) AND d.DetailDocumentTypeID <= COALESCE(@FilterDocumentTypeID, @maxDocType)
		AND dca.DefenseCounselID >= COALESCE(@FilterFirmID, 0) AND dca.DefenseCounselID <= COALESCE(@FilterFirmID, @maxFirmID)
		AND l.DefenseCounselAddressID = @FirmAddressID
		AND CreatedBy <> 'Historical'
	ORDER BY Accepted ASC, DateCreated DESC
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryFirmDocuments_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryFirmDocuments_Filtered] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryFirmDocuments_Filtered] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryFirmDocuments_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryFirmDocuments_Filtered] TO [power_user]
GO
