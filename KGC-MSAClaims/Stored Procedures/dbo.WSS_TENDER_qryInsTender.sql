SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryInsTender]
(	@ClaimIDList bigint_list_tbltype READONLY, 
	@TenderTypeID int, 
	@TenderStatusID int, 
	@TenderBlockID int, 
	@EffectiveDate datetime,
	@DocumentID bigint,
	@EmailAddress nvarchar(500)
	)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	INSERT INTO tblTenderHistory (TenderBlockID, TenderTypeID, ComplaintDocumentID, EffectiveDate, DateCreated, SPUsername)
	SELECT @TenderBlockID, @TenderTypeID, @DocumentID, @EffectiveDate, GETDATE(), @EmailAddress
	
	DECLARE @TenderHistoryID bigint
	SELECT @TenderHistoryID = MAX(TenderHistoryID) FROM tblTenderHistory
	
	INSERT INTO tblClaimTenderHistory (ClaimID, TenderHistoryID, TenderStatusID)
	SELECT n, @TenderHistoryID, @TenderStatusID
	FROM @ClaimIDList
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryInsTender] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryInsTender] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryInsTender] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryInsTender] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryInsTender] TO [power_user]
GO
