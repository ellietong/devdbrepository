SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptQuarterlyClaimsReport_YTD] (@QuarterBegin int, @QuarterEnd int, @Year varchar(max)) 
AS
SET NOCOUNT ON

SELECT @QuarterBegin as [QuarterBegin], @QuarterEnd as [QuarterEnd], @Year as [Year]

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_YTD] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_YTD] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptQuarterlyClaimsReport_YTD] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_YTD] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_YTD] TO [power_user]
GO
