SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysrptClaimantEmployment]
(@ClaimantPersonalInfoID bigint)

As
Set NoCount On

Select ClaimantEmploymentID, o.Occupation, Jobsite, Comments, EmpStartDate, EmpEndDate, dbo.fnEarliestExposure(ClaimantPersonalInfoID) AS EarliestExposureDate, dbo.fnLatestExposure(ClaimantPersonalInfoID) AS LatestExposureDate, IsPrimary, JobSiteCity, IsSecondaryExposure, s.State
From tblClaimantEmployment CE
Inner Join tblOccupation o on o.OccupationID = CE.OccupationID
left join tblState s on s.StateID = CE.JobSiteStateID
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By EmpStartDate

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantEmployment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantEmployment] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptClaimantEmployment] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantEmployment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantEmployment] TO [power_user]
GO
