SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_AmazonXfer_since20130123]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT DocumentID, DocumentURL, REPLACE(REPLACE(DocumentTitle, '\', ''), '/', '') AS DocumentTitle,
			d.DetailDocumentTypeID,REPLACE(REPLACE(ClaimantDocumentType, '\', '_'), '/', '_') AS ClaimantDocumentType,
			 AmazonKey
    FROM tblDocuments d
    INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
    WHERE --AmazonKey IS NULL AND DocumentURL IS NOT NULL
     --AND DocumentID NOT IN (17486, 19524, 21145)
     --AND 
     LEFT(DocumentURL, 1) = '/'
     AND DateCreated >= '1/1/2013'
     ORDER BY DocumentID ASC
  END
  
GO
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonXfer_since20130123] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_AmazonXfer_since20130123] TO [base_user]
GRANT ALTER ON  [dbo].[m_AmazonXfer_since20130123] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonXfer_since20130123] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_AmazonXfer_since20130123] TO [power_user]
GO
