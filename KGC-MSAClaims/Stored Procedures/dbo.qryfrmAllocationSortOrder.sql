SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmAllocationSortOrder]
AS
SET NOCOUNT ON
SELECT SortOrderID, SortOrderType, SortOrder
FROM tblAllocationSortOrder
ORDER BY SortOrderType
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationSortOrder] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationSortOrder] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmAllocationSortOrder] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationSortOrder] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationSortOrder] TO [power_user]
GO
