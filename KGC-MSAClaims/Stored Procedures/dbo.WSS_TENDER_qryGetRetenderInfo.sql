SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryGetRetenderInfo]
(@ClaimID bigint)
AS
BEGIN

	--DECLARE @ClaimID bigint = 13605
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	

    
    DECLARE @TenderBlocks TABLE (ClaimID bigint, TenderBlockID int)
    INSERT INTO @TenderBlocks
    EXEC WSS_TENDER_qryGetTenderBlocksByClaimID @ClaimID
	
	--Here we actually return the selected info to display in the gridview
	SELECT	c.ClaimID, 
			NCCReferenceID, 
			tb.TenderBlockID, 
			COALESCE(LastName + ', ', '') + FirstName + COALESCE(' ' + MiddleName, '') AS ClaimantName,
			c.ClaimantPersonalInfoID
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN @TenderBlocks tb ON tb.TenderBlockID = tb.TenderBlockID --only join to itself
	WHERE	c.ClaimID = @ClaimID
			
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryGetRetenderInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryGetRetenderInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryGetRetenderInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryGetRetenderInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryGetRetenderInfo] TO [power_user]
GO
