SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptAccountsReceivableActivity] (@BillGroupID int = null,
														  @BillDate1 datetime = null, 
														  @BillDate2 datetime = null)
AS
SET NOCOUNT ON

--DECLARE @BillGroupID int
--DECLARE @BillDate1 datetime
--DECLARE @BillDate2 datetime
--SET @BillGroupID = 0
--SET @BillDate1 = '3/26/10'
--SET @BillDate2 = '3/29/10'

DECLARE @MaxBillGroupID int
SELECT @MaxBillGroupID = MAX(BillGroupID) FROM tblBillGroup

-- As of DATE Accounts Receivable
DECLARE @Ledger1 TABLE (LedgerID int, 
						CarrierID int, 
						SubscriberID int, 
						Debit money, 
						Credit money,
						BillDate datetime)
INSERT INTO @Ledger1
	SELECT LedgerID, CarrierID, SubscriberID, Debit, Credit, BillDate
	FROM tblLedger l 
		INNER JOIN tblBill b ON l.BillID = b.BillID
	WHERE CAST(BillDate AS date) <= CAST(@BillDate1 AS date)
		AND l.BillGroupID >= COALESCE(@BillGroupID, 0) AND l.BillGroupID <= COALESCE(NULLIF(@BillGroupID, 0), @maxBillGroupID)

DECLARE @PastAR TABLE (CarrierID int, 
				   SubscriberID int, 
				   AccountReceivable money)
INSERT INTO @PastAR
	SELECT CarrierID, SubscriberID, 
		CASE WHEN SUM(COALESCE(Debit,0)-COALESCE(Credit,0)) > -.01 AND SUM(COALESCE(Debit,0)-COALESCE(Credit,0)) < .01 
			 THEN 0 
			 ELSE SUM(COALESCE(Debit,0)-COALESCE(Credit,0)) 
			 END AS AccountReceivable
	FROM @Ledger1
	GROUP BY CarrierID, SubscriberID

--New Activity Accounts Receivable
Declare @Ledger2 Table (LedgerID int, 
						CarrierID int, 
						SubscriberID int, 
						Debit money, 
						Credit money,
						BillDate datetime)
INSERT INTO @Ledger2
	SELECT LedgerID, CarrierID, SubscriberID, Debit, Credit, BillDate
	FROM tblLedger l
		INNER JOIN tblBill b ON l.BillID = b.BillID
	WHERE CAST(BillDate AS date) > CAST(COALESCE(@BillDate1,'1/1/1900') AS date) AND CAST(BillDate AS date) <= CAST(COALESCE(@BillDate2,'12/31/2099') AS date)
		AND l.BillGroupID >= COALESCE(@BillGroupID, 0) AND l.BillGroupID <= COALESCE(NULLIF(@BillGroupID, 0), @maxBillGroupID)

DECLARE @CurrentAR TABLE (CarrierID int, 
				   SubscriberID int, 
				   NewBilledAmounts money,
				   NewPayments money)
INSERT INTO @CurrentAR
	SELECT CarrierID, SubscriberID, 
		CASE WHEN SUM(COALESCE(Debit,0)) > -.01 AND SUM(COALESCE(Debit,0)) < .01 
			 THEN 0 
			 ELSE SUM(COALESCE(Debit,0))
			 END AS NewBilledAmounts,
		CASE WHEN SUM(COALESCE(Credit,0)) > -.01 AND SUM(COALESCE(Credit,0)) < .01 
			 THEN 0 
			 ELSE SUM(COALESCE(Credit,0))
			 END AS NewPayments
	FROM @Ledger2
	GROUP BY CarrierID, SubscriberID

--Report Data
DECLARE @Final TABLE (OrderID int, 
					  CarrierGrouping nvarchar(25), 
					  CarrierName nvarchar(2000), 
					  AccountReceivable money, 
					  NewBilledAmounts money, 
					  NewPayments money)
INSERT INTO @Final
	SELECT 1 AS OrderID, 'Domestic', COALESCE(CarrierGroupName, CarrierName), AccountReceivable, NewBilledAmounts, NewPayments
	FROM tblCarrier c
		LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID
		LEFT JOIN @PastAR par ON c.CarrierID = par.CarrierID
		LEFT JOIN @CurrentAR car ON c.CarrierID = car.CarrierID
	WHERE (c.CarrierID <> 20 OR par.CarrierID <> 20 OR car.CarrierID <> 20) AND
		(AccountReceivable IS NOT NULL OR NewBilledAmounts IS NOT NULL OR NewPayments IS NOT NULL)
	UNION
	SELECT 2 AS OrderID, 'London Market', SubscriptionCompany, AccountReceivable, NewBilledAmounts, NewPayments
	FROM tblSubscriberCompany subc
		LEFT JOIN @PastAR par ON subc.SubscriberID = par.SubscriberID
		LEFT JOIN @CurrentAR car ON subc.SubscriberID = car.SubscriberID
	WHERE (par.CarrierID = 20 OR car.CarrierID = 20) AND
		(AccountReceivable IS NOT NULL OR NewBilledAmounts IS NOT NULL OR NewPayments IS NOT NULL)
	--Union
	--Select 3 as OrderID, 'Self Insured', CarrierName, AccountReceivable as DatedAccountsReceivable, NewBillings as NewBilledAmounts,
	--	NewPayments as NewPayments
	--From tblCarrier C
	--Left Join @AR A on A.CarrierID=C.CarrierID
	--Left Join @Ledger2 L2 on L2.CarrierID=C.CarrierID
	--Left Join @Payment2 P2 on P2.CarrierID=C.CarrierID
	--Where (A.CarrierID = 20002 or L2.CarrierID = 20002 or P2.CarrierID = 20002) and (AccountReceivable is not null or NewBillings is not null or newpayments is not null)

SELECT @BillDate1 AS BillDate1, @BillDate2 AS BillDate2, OrderID, CarrierGrouping, CarrierName, 
	SUM(COALESCE(AccountReceivable,0)) AS DatedAccountsReceivable,
	SUM(COALESCE(NewBilledAmounts,0)) AS NewBilledAmounts,
	SUM(COALESCE(NewPayments,0)) AS NewPayments
FROM @Final
GROUP BY OrderID, CarrierName, CarrierGrouping
ORDER BY OrderID, CarrierName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAccountsReceivableActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAccountsReceivableActivity] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptAccountsReceivableActivity] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAccountsReceivableActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAccountsReceivableActivity] TO [power_user]
GO
