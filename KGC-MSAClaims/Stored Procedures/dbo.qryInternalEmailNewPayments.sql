SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryInternalEmailNewPayments]
AS
BEGIN


/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

SET @Subject =
'MSA: New Payments Uploaded ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Payments which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewPaymentsTable TABLE (DocID int, docTitle nvarchar (500), DocumentType nvarchar (250), FirmAddressID int, lName nvarchar (50),

fName nvarchar (50), ClaimantPersonalInfoID bigint, NCCReferenceID nvarchar (11), CheckNumber nvarchar (250), PaidDate date)
INSERT INTO @NewPaymentsTable
	SELECT DISTINCT
		d.DocumentID,
		'<a href="https://admin.kcicextranet.com/clients/msa/cp/Pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		ClaimantDocumentType AS DocumentType,
		l.DefenseCOunselAddressID,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		NCCReferenceID,
		COALESCE(cs.CheckNumber, ''),
		COALESCE(cs.PaidDate, NULL)
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
	INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
	WHERE datecreated BETWEEN GETDATE() - 10 AND GETDATE()
	AND detailDocumentTypeID IN (29, 31, 34, 44, 50)
	AND FirmID IS NOT NULL
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	GROUP BY	d.documentID,
				d.DocumentTitle,
				ClaimantDocumentType,
				DefenseCOunselAddressID,
				LastName,
				FirstName,
				cpi.ClaimantPersonalInfoID,
				NCCReferenceID,
				cs.CheckNumber,
				cs.PaidDate


DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar (max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar (max), @ClaimantID bigint, @ClaimantList nvarchar (max), @NCCRef nvarchar (11)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for SELECT
	DocID,
	COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
	ClaimantPersonalInfoID,
	NCCReferenceID
FROM @NewPaymentsTable
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID, @NCCRef
WHILE @@FETCH_STATUS = 0
BEGIN

IF (LEN (@ClaimantList) = 0 OR @ClaimantList IS NULL)
BEGIN

SET @ClaimantList = '<ul> <a href="https://admin.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '&ClaimantTab=3&ResolutionTab=1' + '">' + @Claimant + COALESCE(' (' + CONVERT(nvarchar(50), @NCCRef) + ')', '') + '</a></ul>'
END
ELSE
BEGIN

SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="https://admin.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '&ClaimantTab=3&ResolutionTab=1' + '">' + COALESCE(@Claimant, '') + COALESCE(' (' + CONVERT(nvarchar(50), @NCCRef) + ')', '') + '</a></ul>'

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID, @NCCRef
PRINT @@FETCH_STATUS
IF @DocID < > @LastDocID OR @@FETCH_STATUS < > 0
BEGIN

INSERT INTO @ClaimantPerDoc

	SELECT
		@LastDocID,
		@ClaimantList

SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints



--Declare and populate the body of the email for NCC, Gleason, & ReedSmith
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

IF (SELECT
	COUNT(docTitle)
FROM @NewPaymentsTable) > 0
begin
SET @BodyMessage = @BodyMessage + '<H4>The following payments have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +


N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants (NCC #) </th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Check Number</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Paid Date</th>' +


CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		docTitle)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		DocumentType)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		cl.ClaimantList)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CheckNumber)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		PaidDate)
	AS td,
	''
FROM @NewPaymentsTable C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
GROUP BY	docTitle,
			DocumentType,
			cl.ClaimantList,
			CheckNumber,
			PaidDate
ORDER BY DocumentType

FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +

N'</table>' + N'<br/>'

end
ELSE
begin
SET @BodyMessage = @BodyMessage + '<H4>No payments have been uploaded in the last 24 hours. </H4>'
end


--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href="https://admin.kcicextranet.com/clients/msa/cp/pages/Documents.aspx">here</A> to access the document(s).</b><br><br>'


--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


DECLARE @RecipientList nvarchar (500)
SET @RecipientList = ''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar (500)
DECLARE contact_curs CURSOR FOR SELECT
	email
FROM vtblPerson
WHERE FirmID IN (68, 31, 201, 66)
AND ComplaintContact = 1


OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS = 0
BEGIN

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--set @BodyMessage = @BodyMessage +  '<br><br> Recipients: ' + @RecipientList


--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail --The Claims sends from msa@kcic.com
@profile_name = 'Claims',
@recipients = 'msa@kcic.com',
@blind_copy_recipients = 'potterm@kcic.com; monahanc@kcic.com',
--@recipients = 'amanp@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'



END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailNewPayments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailNewPayments] TO [base_user]
GRANT ALTER ON  [dbo].[qryInternalEmailNewPayments] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailNewPayments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailNewPayments] TO [power_user]
GO
