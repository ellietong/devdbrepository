SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmBilledLedgerItems]
(@BillID int = NULL)
As

SET NOCOUNT ON

SELECT *
FROM tblLedger
WHERE BillID LIKE COALESCE(@BillID, 0) 

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBilledLedgerItems] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmBilledLedgerItems] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmBilledLedgerItems] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBilledLedgerItems] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmBilledLedgerItems] TO [power_user]
GO
