SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptARQCVerticallyUncollapsedCostByColumn]
(@allocationID int, @insuranceProgramID int = null)
AS

SET NOCOUNT ON

--DECLARE @AllocationID INT
--SET @AllocationID = 6
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID  = 1

/* The following procedue can be used to verify dollars that are unallocated due
to column exhaustion / vertically uncollapsing.
5/31/2007 - Created by CLC */

DECLARE @maxInsuranceProgram int
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM vPolicy

SELECT c.AllocationID, AllocationDescription, InsuranceProgram, c.ColumnID, 
ColumnStartDate, ColumnEndDate, 
	SUM(COALESCE(uncollapsedVertical, 0)) AS uncollapsedVertical
FROM tblAllocationResultsColumn c 
	INNER JOIN vAllocation a ON c.AllocationID = a.AllocationID
	INNER JOIN tblInsuranceProgram ip ON c.insuranceprogramid = ip.insuranceprogramid
	LEFT JOIN tblAllocationResultsVertical av ON c.AllocationID = av.AllocationID AND c.ColumnID = av.ColumnID 
WHERE c.AllocationID = @allocationID and 
c.InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND c.InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram)
GROUP BY c.AllocationID, AllocationDescription, InsuranceProgram, c.ColumnID, 
ColumnStartDate, ColumnEndDate
ORDER BY InsuranceProgram, ColumnStartDate

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARQCVerticallyUncollapsedCostByColumn] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARQCVerticallyUncollapsedCostByColumn] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptARQCVerticallyUncollapsedCostByColumn] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARQCVerticallyUncollapsedCostByColumn] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARQCVerticallyUncollapsedCostByColumn] TO [power_user]
GO
