SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDocList] (@ClaimantPersonalInfoID int)
AS
BEGIN

--DECLARE @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID=1615
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT TOP 1000 d.DocumentID as DocumentID, COALESCE(ClaimantDocumentType +': ','') + DOCUMENTTITLE as DocumentTitle
    FROM tblDocuments D
    LEFT JOIN tblClaimantDocumentType CDT ON CDT.ClaimantDocumentTypeID = D.DetailDocumentTypeID
    inner join tblMultiClaimantDocument mcd on mcd.DocumentID=d.DocumentID
    where mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND DetailDocumentTypeID NOT IN (40)
						--and FirmID is not null
						--UNION
						--SELECT TOP 1000 d.DocumentID as DocumentID, COALESCE(upper(ClaimantDocumentType) +': ','') + DOCUMENTTITLE as DocumentTitle
						--FROM tblDocuments D
						--left join dbo.tblmultidoctype mdt on mdt.documentid = D.DocumentID 
						--left JOIN dbo.tblhistdocumenttype hdt on hdt.claimantdocumenttypeid = mdt.documenttypeid 
						--inner join tblMultiClaimantDocument mcd on mcd.DocumentID=d.DocumentID
						--where mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID --AND DetailDocumentTypeID NOT IN (40)
						--and FirmID is null    
    UNION
    SELECT NULL as DocumentID, '-- Select a Document --'  as DocumentTitle
    ORDER BY 2
    
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDocList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDocList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryDocList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDocList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDocList] TO [power_user]
GO
