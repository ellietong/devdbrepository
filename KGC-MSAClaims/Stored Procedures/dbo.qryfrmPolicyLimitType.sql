SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmPolicyLimitType]
AS
SET NOCOUNT ON
SELECT *
FROM tblPolicyLimitType
ORDER BY LimitType
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLimitType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLimitType] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmPolicyLimitType] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLimitType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLimitType] TO [power_user]
GO
