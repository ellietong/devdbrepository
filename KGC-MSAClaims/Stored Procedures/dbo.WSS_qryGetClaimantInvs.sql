SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetClaimantInvs]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dicd.ClaimantPersonalInfoID AS ClaimantID, dicd.*, 
	cpi.LastName + ', ' + COALESCE(cpi.FirstName , '') + COALESCE(' ' + MiddleName, '') AS ClaimantName, '' as LegalBillStatus,
	fees + costs+adjustments as InvoiceTotal, VendorFirmID, Matter
	FROM tblDefInvClaimantDetail dicd
	LEFT JOIN tblDocuments d ON dicd.DocumentID = d.DocumentID
	--LEFT JOIN [kga-demodb].dbo.tblClaimantPersonalInfo cpi ON dicd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantPersonalInfo cpi ON dicd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblMatter m ON dicd.MatterID = m.MatterID
	WHERE d.DateCreated > DATEADD(yy, -1, GETDATE())
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetClaimantInvs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetClaimantInvs] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetClaimantInvs] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetClaimantInvs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetClaimantInvs] TO [power_user]
GO
