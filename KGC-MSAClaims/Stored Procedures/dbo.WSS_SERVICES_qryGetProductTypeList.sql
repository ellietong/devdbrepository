SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetProductTypeList]
(@PrefixText nvarchar(200), @Emailaddress nvarchar(250) = NULL )
AS
BEGIN
	SELECT ProductTypeID, ProductType
	FROM tblProductType
	WHERE ProductType LIKE '%' + @PrefixText + '%'
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetProductTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetProductTypeList] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_SERVICES_qryGetProductTypeList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetProductTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetProductTypeList] TO [power_user]
GO
