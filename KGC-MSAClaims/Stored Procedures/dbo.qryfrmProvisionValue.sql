SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmProvisionValue]
(@provisionLabelID int = NULL)
AS
SET NOCOUNT ON

IF @provisionLabelID IS NULL

SELECT ProvisionValueID, ProvisionLabelID, ProvisionValue 
FROM tblProvisionValue
ORDER BY ProvisionValue

ELSE

SELECT ProvisionValueID, ProvisionLabelID, ProvisionValue
FROM tblProvisionValue
WHERE ProvisionLabelID = @provisionLabelID
ORDER BY ProvisionValue

RETURN 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionValue] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionValue] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmProvisionValue] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionValue] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionValue] TO [power_user]
GO
