SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptPreviousClaimantsCheck] (@ClaimantPersonalInfoID bigint)

AS
SET NOCOUNT ON


SELECT ClaimantPersonalInfoID, FirstName, MiddleName, LastName, SocialSecurityNumber, BirthDate,
CASE WHEN FirstName LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1 
WHEN FirstName LIKE (SELECT MiddleName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1
ELSE 0 END AS FlagFirstName,
CASE WHEN MiddleName LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1 
WHEN MiddleName LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1
ELSE 0 END AS FlagMiddleName,
CASE WHEN LastName LIKE (SELECT LastName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END AS FlagLastName,
CASE WHEN SocialSecurityNumber LIKE (SELECT SocialSecurityNumber FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END AS FlagSocialSecurityNumber,
CASE WHEN BirthDate LIKE (SELECT BirthDate FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END AS FlagBirthDate
FROM tblClaimantPersonalInfo cpi
WHERE ClaimantPersonalInfoID <> @claimantPersonalinfoID AND
(((((LEFT(FirstName,3) LIKE (SELECT LEFT(FirstName,3) FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) OR
LEFT(MiddleName,3) LIKE (SELECT LEFT(FirstName,3) FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)) OR
(LEFT (FirstName,3) LIKE (SELECT LEFT(MiddleName,3) FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID))))
AND LastName LIKE (SELECT LastName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)) OR
(SocialSecurityNumber LIKE (SELECT SocialSecurityNumber FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)) OR
(BirthDate = (SELECT BirthDate FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)))

ORDER BY CASE WHEN LastName LIKE (SELECT LastName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END,
CASE WHEN FirstName LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END,
CASE WHEN SocialSecurityNumber LIKE (SELECT SocialSecurityNumber FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END,
CASE WHEN BirthDate LIKE (SELECT BirthDate FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPreviousClaimantsCheck] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPreviousClaimantsCheck] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptPreviousClaimantsCheck] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPreviousClaimantsCheck] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPreviousClaimantsCheck] TO [power_user]
GO
