SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantSmokingHistory]
(@ClaimantPersonalInfoID bigint, @emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimantSmokingHistoryID, ClaimantPersonalInfoID, SmokingStart, SmokingEnd, COALESCE(PacksPerDay, 0) AS PacksPerDay
	FROM tblClaimantSmokingHistory
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantSmokingHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantSmokingHistory] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimantSmokingHistory] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantSmokingHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantSmokingHistory] TO [power_user]
GO
