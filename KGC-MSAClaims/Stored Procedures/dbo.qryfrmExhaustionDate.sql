SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmExhaustionDate]
AS

SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT ExhaustionDateID, CONVERT(nvarchar, ExhaustionDate, 101) + ' ' + Description AS ExhaustionDateName
FROM tblExhaustionDate

SET CONCAT_NULL_YIELDS_NULL ON
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustionDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustionDate] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmExhaustionDate] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustionDate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustionDate] TO [power_user]
GO
