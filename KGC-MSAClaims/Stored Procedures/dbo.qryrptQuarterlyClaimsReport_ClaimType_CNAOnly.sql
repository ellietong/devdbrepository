SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptQuarterlyClaimsReport_ClaimType_CNAOnly] (@Quarter int, @Year varchar(max), @NextYear varchar(max)) 
AS
SET NOCOUNT ON

--Declare @Quarter int
--Set @Quarter = 4
--Declare @Year nvarchar(25)
--Set @Year = '2014'
--Declare @NextYear nvarchar(25)
--Set @NextYear = '2015'

--get last quarter start date
DECLARE @PreviousQuarterStart varchar(25)
SET @PreviousQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '1/1/' + @Year
		 WHEN @Quarter = 2 THEN '4/1/' + @Year
		 WHEN @Quarter = 3 THEN '7/1/' + @Year
		 WHEN @Quarter = 4 THEN '10/1/' + @Year
		 ELSE '1/1/2050' END

--get first day of next quarter
DECLARE @NextQuarterStart varchar(25)
SET @NextQuarterStart = 
	CASE WHEN @Quarter = 1 THEN '4/1/' + @Year
		 WHEN @Quarter = 2 THEN '7/1/' + @Year
		 WHEN @Quarter = 3 THEN '10/1/' + @Year
		 WHEN @Quarter = 4 THEN '1/1/' + @NextYear
		 ELSE '12/31/2050' END
		 
If Object_ID('tempdb..#temp') is not null drop table #temp
Create Table #Temp (Material nvarchar(max), Costs money, SettlementAmount money, OpenCount int, SettleCount int, DismissalCount int, PendingCount int)			 

INSERT INTO #Temp
	SELECT CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END AS Material, 
		   SUM(paymentamount),0,0,0,0,0
	FROM tblDefenseInvoice di
		LEFT JOIN tblDefInvClaimantDetail dicd on dicd.DefenseInvoiceID = di.DefenseInvoiceID
		LEFT JOIN tblClaim c on c.ClaimID = dicd.ClaimID
		LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		--LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
		INNER JOIN tblPaymentDefense d on d.DefenseInvoiceID =di.DefenseInvoiceID 
		INNER JOIN tblcheck ch on ch.CheckID =d.CheckID 
	WHERE c.InsurableFlag = 1
		AND ((COALESCE(sourcedate,'1/1/1900') < @NextQuarterStart AND COALESCE(sourcedate,'1/1/1900') >= @PreviousQuarterStart AND TortTypeID = 1) 
			OR (di.QCDate > ch.SourceDate and QCDate between '10/1/2014' and '1/1/2015'))
	GROUP BY CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END 

--		UNION ALL

----This part is only for 4th Q 2014, remove for future runs

--	SELECT CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END AS Material, 
--		   sum(PaymentAmount),0,0,0,0,0
--	FROM tblDefenseInvoice di
--		LEFT JOIN tblDefInvClaimantDetail dicd on dicd.DefenseInvoiceID = di.DefenseInvoiceID
--		LEFT JOIN tblClaim c on c.ClaimID = dicd.ClaimID
--		LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
--		LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
--		LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
--		--LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
--		INNER JOIN tblPaymentDefense d on d.DefenseInvoiceID =di.DefenseInvoiceID 
--		INNER JOIN tblcheck ch on ch.CheckID =d.CheckID 
--	WHERE di.QCDate > ch.SourceDate
--and QCDate between '10/1/2014' and '1/1/2015'
--and c.InsurableFlag = 1
--	GROUP BY CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END 

UNION ALL

	SELECT causeagent,0, sum(PaymentAmount), 
		   0,0,0,0
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		  LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN tblState s on s.StateID = l.StateID
		  LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		  LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		  --LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
		  INNER JOIN tblPaymentSettlement ps on ps.ClaimantSettlementID =cs.ClaimantSettlementID  
		  INNER JOIN tblcheck ch on ch.CheckID =ps.CheckID
	WHERE COALESCE(sourcedate,'1/1/1900') < @NextQuarterStart AND COALESCE(sourcedate,'1/1/1900')  >= @PreviousQuarterStart
		AND c.InsurableFlag =1
		AND TortTypeID = 1 AND m.MaterialID != 10	 AND c.ClaimID != 26234
		  AND cls.ClaimLawsuitStatusGroupID = 1
		  and isPrimaryStatus = 1
		  		  --AND CLST.ClaimLawsuitStatusID != 43
	GROUP BY causeagent

UNION ALL
	  
	SELECT CauseAgent, 0,0,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 3 THEN 1 ELSE 0 END) as OpenCount,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 1 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END) as SettleCount,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 4 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END) as DismissalCount, 0
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		  LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN tblState s on s.StateID = l.StateID
		  LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		  LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		  --LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID
		  LEFT JOIN tblPaymentSettlement ps on ps.ClaimantSettlementID =cs.ClaimantSettlementID  
		  LEFT JOIN tblcheck ch on ch.CheckID =ps.CheckID  
	WHERE cls.ClaimLawsuitStatusGroupID in (1,3,4)
		  AND CASE WHEN (
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 4 AND COALESCE(ProcessedDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END = 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 4 AND COALESCE(ProcessedDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1 OR
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 1 AND COALESCE(SourceDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END = 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 1 AND COALESCE(SourceDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1 OR
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 3 AND COALESCE(ServiceDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END= 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 3 AND COALESCE(ServiceDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1)
		  THEN 1 ELSE 0 END = 1
	      AND m.MaterialID != 10		
		  AND c.InsurableFlag =1
		  and IsPrimaryStatus = 1
		  AND (CASE WHEN ClaimLawsuitStatusGroupID = 3 THEN 1 ELSE 0 END>0 
			  or CASE WHEN ClaimLawsuitStatusGroupID = 1 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END>0 
			  or CASE WHEN ClaimLawsuitStatusGroupID = 4 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END>0)
		  AND TortTypeID = 1
		  AND c.ClaimID != 26234
		  ----------AND NCCReferenceID NOT IN (SELECT NCCReferenceID 
				----------	FROM tblClaim C
				----------	LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
				----------	LEFT JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID = CL.ClaimLawsuitID
				----------	LEFT JOIN tblClaimLawsuitStatus CLS ON CLS.ClaimLawsuitStatusID = CLST.ClaimLawsuitStatusID
				----------	WHERE IsPrimaryStatus <> 1
				----------	AND ClaimLawsuitStatusGroupID = 4)
		  AND CLST.ClaimLawsuitStatusID <> 38
		  AND CL.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE (ClaimLawsuitStatusID = 38 AND IsPrimaryStatus = 1) OR (ClaimLawsuitStatusID = 29))
		  --AND CL.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID = 29)
		  AND COALESCE(DocketNumber, '') NOT LIKE '%330307728B%'
		  AND CLST.ClaimLawsuitStatusID != 41  ----> Exclude Verdict claims
		  --AND CLST.ClaimLawsuitStatusID != 43  ----> Exclude Settlement in Principle - Awaiting Release claims
	GROUP BY CauseAgent

UNION ALL
	SELECT CauseAgent AS Material, 0, 0, 0, 0, 0, 
		   count(distinct(cl.claimlawsuitid)) as PendingCount
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		  LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN tblState s on s.StateID = l.StateID
		  LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		  LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		  --LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
		  where ((coalesce(filedate, servicedate, processeddate, statusdate, '1/1/1900') <= @NextQuarterStart)
	and (cls.ClaimLawsuitStatusGroupID in (3) and clst.ClaimLawsuitStatusID !=29 and IsPrimaryStatus = 1 	and c.TortTypeID =1 
	and c.InsurableFlag =1 AND m.MaterialID != 10 ))
	OR (cls.ClaimLawsuitStatusGroupID not in (3,2,5) and IsPrimaryStatus = 1 and Convert(Date,coalesce(processeddate, statusdate)) > @NextQuarterStart 
	and c.TortTypeID =1 
	and c.InsurableFlag =1 AND m.MaterialID != 10 ) 
	group by causeagent
	
If Object_ID('tempdb..#temp_pre1985') is not null drop table #temp_pre1985
Create Table #Temp_Pre1985 (Material_Pre1985 nvarchar(max), Costs_Pre1985 money, SettlementAmount_Pre1985 money, 
OpenCount_Pre1985 int, SettleCount_Pre1985 int, DismissalCount_Pre1985 int, PendingCount_Pre1985 int)	

INSERT INTO #Temp_Pre1985
	SELECT CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END AS Material, 
		   SUM(paymentamount),0,0,0,0,0
	FROM tblDefenseInvoice di
		LEFT JOIN tblDefInvClaimantDetail dicd on dicd.DefenseInvoiceID = di.DefenseInvoiceID
		LEFT JOIN tblClaim c on c.ClaimID = dicd.ClaimID
		LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		--LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
		INNER JOIN tblPaymentDefense d on d.DefenseInvoiceID =di.DefenseInvoiceID 
		INNER JOIN tblcheck ch on ch.CheckID =d.CheckID 
	WHERE COALESCE(sourcedate,'1/1/1900') < @NextQuarterStart AND COALESCE(sourcedate,'1/1/1900')  >= @PreviousQuarterStart
		AND c.InsurableFlag =1
		AND TortTypeID = 1
		AND C.ClaimID IN (SELECT ClaimID FROM m_tblPre1985Claims_QuarterlyCNAReport)
	GROUP BY CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END


		UNION ALL

--This part is only for 4th Q 2014, remove for future runs

	SELECT CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END AS Material, 
		   sum(PaymentAmount),0,0,0,0,0
	FROM tblDefenseInvoice di
		LEFT JOIN tblDefInvClaimantDetail dicd on dicd.DefenseInvoiceID = di.DefenseInvoiceID
		LEFT JOIN tblClaim c on c.ClaimID = dicd.ClaimID
		LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		--LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
		INNER JOIN tblPaymentDefense d on d.DefenseInvoiceID =di.DefenseInvoiceID 
		INNER JOIN tblcheck ch on ch.CheckID =d.CheckID 
		INNER JOIN m_tblPre1985Claims_QuarterlyCNAReport ON m_tblPre1985Claims_QuarterlyCNAReport.ClaimID = c.ClaimID
	WHERE di.QCDate > ch.SourceDate
		and QCDate between '10/1/2014' and '1/1/2015'
		and c.InsurableFlag = 1
--AND C.ClaimID IN (SELECT ClaimID FROM m_tblPre1985Claims_QuarterlyCNAReport)
	GROUP BY CASE WHEN NCCReferenceID LIKE '%GENERALEXP%' THEN 'General Expenses' ELSE COALESCE(CauseAgent,'Unknown') END 


UNION ALL

	SELECT causeagent,0, sum(PaymentAmount), 
		   0,0,0,0
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		  LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN tblState s on s.StateID = l.StateID
		  LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		  LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		  --LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
		  INNER JOIN tblPaymentSettlement ps on ps.ClaimantSettlementID =cs.ClaimantSettlementID  
		  INNER JOIN tblcheck ch on ch.CheckID =ps.CheckID
	WHERE COALESCE(sourcedate,'1/1/1900') < @NextQuarterStart AND COALESCE(sourcedate,'1/1/1900')  >= @PreviousQuarterStart
		AND c.InsurableFlag =1
		AND TortTypeID = 1
		AND m.MaterialID != 10	 AND c.ClaimID != 26234
		  AND cls.ClaimLawsuitStatusGroupID = 1
		  		  --AND CLST.ClaimLawsuitStatusID != 43
				  and isPrimaryStatus = 1
	GROUP BY causeagent	

UNION ALL
	  
	SELECT CauseAgent, 0, 0,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 3 THEN 1 ELSE 0 END) as OpenCount_Pre1985,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 1 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END) as SettleCount_Pre1985,
		   SUM(CASE WHEN ClaimLawsuitStatusGroupID = 4 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END) as DismissalCount_Pre1985, 0
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		  LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN tblState s on s.StateID = l.StateID
		  LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		  LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		  --LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID
		  LEFT JOIN tblPaymentSettlement ps on ps.ClaimantSettlementID =cs.ClaimantSettlementID  
		  LEFT JOIN tblcheck ch on ch.CheckID =ps.CheckID 
	WHERE cls.ClaimLawsuitStatusGroupID in (1,3,4)
		  AND CASE WHEN (
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 4 AND COALESCE(ProcessedDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END = 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 4 AND COALESCE(ProcessedDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1 OR
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 1 AND COALESCE(SourceDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END = 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 1 AND COALESCE(SourceDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1 OR
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 3 AND COALESCE(ServiceDate,'1/1/1900') < @NextQuarterStart THEN 1 ELSE 0 END= 1 AND
				CASE WHEN cls.ClaimLawsuitStatusGroupID = 3 AND COALESCE(ServiceDate,'1/1/1900') >= @PreviousQuarterStart THEN 1 ELSE 0 END= 1)
		  THEN 1 ELSE 0 END = 1
	      AND m.MaterialID != 10		
		AND c.InsurableFlag =1
		and isPrimaryStatus = 1
		  AND (CASE WHEN ClaimLawsuitStatusGroupID = 3 THEN 1 ELSE 0 END>0 
			  or CASE WHEN ClaimLawsuitStatusGroupID = 1 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END>0 
			  or CASE WHEN ClaimLawsuitStatusGroupID = 4 and IsPrimaryStatus = 1 THEN 1 ELSE 0 END>0)
		  AND TortTypeID = 1
		AND C.ClaimID IN (SELECT ClaimID FROM m_tblPre1985Claims_QuarterlyCNAReport)
		  AND c.ClaimID != 26234
		  ----------AND NCCReferenceID NOT IN (SELECT NCCReferenceID 
				----------	FROM tblClaim C
				----------	LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
				----------	LEFT JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID = CL.ClaimLawsuitID
				----------	LEFT JOIN tblClaimLawsuitStatus CLS ON CLS.ClaimLawsuitStatusID = CLST.ClaimLawsuitStatusID
				----------	WHERE IsPrimaryStatus <> 1
				----------	AND ClaimLawsuitStatusGroupID = 4)
		  AND CLST.ClaimLawsuitStatusID <> 38
		  AND CL.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE (ClaimLawsuitStatusID = 38 AND IsPrimaryStatus = 1) OR (ClaimLawsuitStatusID = 29))
		  --AND CL.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID = 29)
		  AND COALESCE(DocketNumber, '') NOT LIKE '%330307728B%'
		  AND CLST.ClaimLawsuitStatusID != 41  ----> Exclude Verdict claims
		  --AND CLST.ClaimLawsuitStatusID != 43  ----> Exclude Settlement in Principle - Awaiting Release claims
	GROUP BY CauseAgent

UNION ALL

	SELECT CauseAgent AS Material, 0, 0, 0, 0, 0, 
		   count(distinct(cl.claimlawsuitid)) as PendingCount_Pre1985
	FROM tblClaimantPersonalInfo cpi 
		  LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.claimID = c.claimID
		  LEFT JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
		  LEFT JOIN tblState s on s.StateID = l.StateID
		  LEFT JOIN tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		  LEFT JOIN tblMaterial m on m.MaterialID = a.MaterialID
		  LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		  --LEFT JOIN m_tblInsurableClaims i on i.ClaimID =c.ClaimID 
		  INNER JOIN m_tblPre1985Claims_QuarterlyCNAReport ON m_tblPre1985Claims_QuarterlyCNAReport.ClaimID = c.ClaimID
	WHERE ((coalesce(filedate, servicedate, processeddate, statusdate, '1/1/1900') <= @NextQuarterStart)
			AND (cls.ClaimLawsuitStatusGroupID in (3) and clst.ClaimLawsuitStatusID !=29 and IsPrimaryStatus = 1 
			AND m.MaterialID != 10		
			AND c.InsurableFlag  =1
			AND TortTypeID = 1)
			--AND C.ClaimID IN (SELECT ClaimID FROM m_tblPre1985Claims_QuarterlyCNAReport)))
		OR (cls.ClaimLawsuitStatusGroupID not in (3,2,5) and IsPrimaryStatus = 1 and Convert(Date,coalesce(processeddate, statusdate)) > @NextQuarterStart 
	        AND m.MaterialID != 10		
			AND c.InsurableFlag  =1
		    AND TortTypeID = 1))
			--AND C.ClaimID IN (SELECT ClaimID FROM m_tblPre1985Claims_QuarterlyCNAReport)) 
	GROUP BY CauseAgent

If Object_ID('tempdb..#CNAQuarterlyReport') is not null drop table #CNAQuarterlyReport
Create Table #CNAQuarterlyReport (Material nvarchar(max), Defense money, Indemnity money, OpenCount int, SettledCount int, DismissedCount int, PendingCount int,
Material_Pre1985 nvarchar(max), Defense_Pre1985 money, Indemnity_Pre1985 money, 
OpenCount_Pre1985 int, SettledCount_Pre1985 int, DismissedCount_Pre1985 int, PendingCount_Pre1985 int)

INSERT INTO #CNAQuarterlyReport
Select Material,
	   SUM(Costs) as Defense,
	   SUM(SettlementAmount) as Indemnity,
	   SUM(OpenCount) as OpenCount,
	   SUM(SettleCount) as SettledCount,
	   SUM(DismissalCount) as DismissedCount,
	   SUM(PendingCount) as PendingCount,
'',0,0,0,0,0,0
FROM #Temp
group by Material
UNION
SELECT Material_Pre1985,0,0,0,0,0,0,
Material_Pre1985,
	   SUM(Costs_Pre1985) as Defense_Pre1985,
	   SUM(SettlementAmount_Pre1985) as Indemnity_Pre1985,
	   SUM(OpenCount_Pre1985) as OpenCount_Pre1985,
	   SUM(SettleCount_Pre1985) as SettledCount_Pre1985,
	   SUM(DismissalCount_Pre1985) as DismissedCount_Pre1985,
	   SUM(PendingCount_Pre1985) as PendingCount_Pre1985
FROM #Temp_Pre1985
group by Material_Pre1985

SELECT Material, SUM(Defense) AS Defense, SUM(Indemnity) AS Indemnity, 
SUM(OpenCount) AS OpenCount, SUM(SettledCount) AS SettledCount, SUM(DismissedCount) AS DismissedCount, SUM(PendingCount) AS PendingCount,
SUM(Defense_Pre1985) AS Defense_Pre1985, SUM(Indemnity_Pre1985) AS Indemnity_Pre1985, 
SUM(OpenCount_Pre1985) AS OpenCount_Pre1985, SUM(SettledCount_Pre1985) AS SettledCount_Pre1985, SUM(DismissedCount_Pre1985) AS DismissedCount_Pre1985, SUM(PendingCount_Pre1985) AS PendingCount_Pre1985,
	   CASE WHEN Material = 'General Expenses' THEN 1 ELSE 0 END AS SortOrder
FROM #CNAQuarterlyReport
GROUP BY Material, CASE WHEN Material = 'General Expenses' THEN 1 ELSE 0 END
ORDER BY SortOrder

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_ClaimType_CNAOnly] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_ClaimType_CNAOnly] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptQuarterlyClaimsReport_ClaimType_CNAOnly] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuarterlyClaimsReport_ClaimType_CNAOnly] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptQuarterlyClaimsReport_ClaimType_CNAOnly] TO [power_user]
GO
