SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmOriginalNoticeLetterSelect]
As

SET NOCOUNT ON

SELECT c.CorrespondenceID AS ID, c.CorrespondenceDate AS Date, ct.Company, 
	CONVERT(nvarchar(20), c.CorrespondenceDate, 101) + COALESCE (' - ' + car.CarrierNickname, ' - No Contact Company Coded') + 
	COALESCE (' - ' + ct.FirstName, '') + COALESCE (' ' + ct.LastName, '') AS NoticeLetter 

FROM dbo.tblCorrespondence c 
LEFT OUTER JOIN dbo.tblCorrCarrierContact ca ON ca.CorrespondenceID = c.CorrespondenceID 
INNER JOIN dbo.tblCarrierContact cat ON cat.CarrierContactID = ca.CarrierContactID 
INNER JOIN dbo.tblCarrier car ON car.CarrierID = cat.CarrierID 
INNER JOIN dbo.tblContact ct ON ct.ContactID = cat.ContactID 

WHERE (c.correspondencetypeid = 1)

ORDER BY c.CorrespondenceDate, car.CarrierNickname




RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOriginalNoticeLetterSelect] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmOriginalNoticeLetterSelect] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmOriginalNoticeLetterSelect] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOriginalNoticeLetterSelect] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmOriginalNoticeLetterSelect] TO [power_user]
GO
