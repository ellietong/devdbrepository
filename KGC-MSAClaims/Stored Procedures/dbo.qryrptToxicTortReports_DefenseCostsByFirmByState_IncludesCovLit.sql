SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptToxicTortReports_DefenseCostsByFirmByState_IncludesCovLit] (@DataDate DATETIME) 
AS
SET NOCOUNT ON

--DECLARE @DataDate DATETIME
--SET @DataDate = '6/18/2014'

SELECT COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown') AS DefenseCounsel,
			CASE WHEN NCCReferenceID LIKE '%GEN%' THEN 'General Expenses'
				 WHEN NCCReferenceID LIKE '%LIT%' THEN 'Coverage Litigation'
				 ELSE COALESCE(S.FullNameState, 'Unknown') END AS FullNameState,
		    COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2009' AND '12/31/2009' THEN Costs + Fees END),0) AS Cost2009,
			COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2010' AND '12/31/2010' THEN Costs + Fees END),0) AS Cost2010,
			COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2011' AND '12/31/2011' THEN Costs + Fees END),0) AS Cost2011,
			COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2012' AND '12/31/2012' THEN Costs + Fees END),0) AS Cost2012,
			COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2013' AND '12/31/2013' THEN Costs + Fees END),0) AS Cost2013,
			COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2014' AND @DataDate THEN Costs + Fees END),0) AS Cost2014,
			COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2009' AND @DataDate THEN Costs + Fees END),0) AS Total,
			@DataDate AS DD,
			CASE WHEN (CASE WHEN NCCReferenceID LIKE '%GEN%' THEN 'General Expenses'
						WHEN NCCReferenceID LIKE '%LIT%' THEN 'Coverage Litigation'
						ELSE COALESCE(S.FullNameState, 'Unknown') END) = 'Unknown' THEN 2
				 WHEN (CASE WHEN NCCReferenceID LIKE '%GEN%' THEN 'General Expenses'
						WHEN NCCReferenceID LIKE '%LIT%' THEN 'Coverage Litigation'
						ELSE COALESCE(S.FullNameState, 'Unknown') END) = 'General Expenses' THEN 3
				 WHEN (CASE WHEN NCCReferenceID LIKE '%GEN%' THEN 'General Expenses'
						WHEN NCCReferenceID LIKE '%LIT%' THEN 'Coverage Litigation'
						ELSE COALESCE(S.FullNameState, 'Unknown') END) = 'Coverage Litigation' THEN 4
				 ELSE 1 END AS Flag
FROM

		dbo.tblDefenseInvoice DI
			LEFT JOIN dbo.tblDefInvClaimantDetail DICD ON DICD.DefenseInvoiceID = DI.DefenseInvoiceID
			LEFT JOIN dbo.tblClaim C ON C.ClaimID = DICD.ClaimID
			LEFT JOIN dbo.tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
			LEFT JOIN dbo.tblLawsuit L ON L.LawsuitID = CL.LawsuitID
			LEFT JOIN dbo.tblState S ON S.StateID = L.StateID
			LEFT JOIN dbo.m_tblDefenseCounselLookup_ToxicTortReporting DCL ON DCL.DefenseCounselAddressID = DI.DefenseCounselAddressID
			LEFT JOIN dbo.tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID

WHERE COALESCE(MaterialID, 100) != 10
AND PeriodEnd >= '1/1/2009'

GROUP BY COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown'),
			CASE WHEN NCCReferenceID LIKE '%GEN%' THEN 'General Expenses'
				 WHEN NCCReferenceID LIKE '%LIT%' THEN 'Coverage Litigation'
				 ELSE COALESCE(S.FullNameState, 'Unknown') END,
		 	CASE WHEN (CASE WHEN NCCReferenceID LIKE '%GEN%' THEN 'General Expenses'
						WHEN NCCReferenceID LIKE '%LIT%' THEN 'Coverage Litigation'
						ELSE COALESCE(S.FullNameState, 'Unknown') END) = 'Unknown' THEN 2
				 WHEN (CASE WHEN NCCReferenceID LIKE '%GEN%' THEN 'General Expenses'
						WHEN NCCReferenceID LIKE '%LIT%' THEN 'Coverage Litigation'
						ELSE COALESCE(S.FullNameState, 'Unknown') END) = 'General Expenses' THEN 3
				 WHEN (CASE WHEN NCCReferenceID LIKE '%GEN%' THEN 'General Expenses'
						WHEN NCCReferenceID LIKE '%LIT%' THEN 'Coverage Litigation'
						ELSE COALESCE(S.FullNameState, 'Unknown') END) = 'Coverage Litigation' THEN 4
				 ELSE 1 END

HAVING COALESCE(SUM(CASE WHEN PeriodEnd BETWEEN '1/1/2009' AND @DataDate THEN Costs + Fees END),0) <> 0

ORDER BY DefenseCounsel, Flag, FullNameState
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptToxicTortReports_DefenseCostsByFirmByState_IncludesCovLit] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptToxicTortReports_DefenseCostsByFirmByState_IncludesCovLit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptToxicTortReports_DefenseCostsByFirmByState_IncludesCovLit] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptToxicTortReports_DefenseCostsByFirmByState_IncludesCovLit] TO [power_user]
GO
