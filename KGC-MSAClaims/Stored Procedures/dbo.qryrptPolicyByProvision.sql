SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  Procedure [dbo].[qryrptPolicyByProvision]
(@insuranceProgramID int = NULL, @limitTypeID int = NULL, @carrierID int = NULL, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL, @namedInsuredID int = NULL, 
@lowLayer int = NULL, @highLayer int = NULL, @lowAttach nvarchar(50) = NULL, @highAttach nvarchar(50) = NULL, @selectedProvisions nvarchar(1000) = NULL, @selectedProvisionValues nvarchar(1000) = NULL, @startDate int = NULL, @endDate int = NULL)
As

SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

--DECLARE @insuranceProgramID int 
--DECLARE @limitTypeID int
--DECLARE @carrierID int 
--DECLARE @carrierGroupID int 
--DECLARE @carrierSolvency varchar(50) 
--DECLARE @namedInsuredID int 
--DECLARE @lowLayer int 
--DECLARE @highLayer int 
--DECLARE @lowAttach nvarchar(50) 
--DECLARE @highAttach nvarchar(50) 
--DECLARE @startdate int
--DECLARE @enddate int

--SET @insuranceProgramID = 0
--SET @limitTypeID = 1
--SET @carrierID = 0
--SET @carrierGroupID = 0
--SET @carrierSolvency = '<<All>>'
--SET @namedInsuredID = 0
--SET @lowLayer = null
--SET @highLayer = null
--SET @lowAttach = null
--SET @highAttach = null
--SET @startdate = ''
--SET @enddate = ''


-- Variables hold maximum values
DECLARE @lowAttachNum money
DECLARE @highAttachNum money
DECLARE @maxInsuranceProgram int
DECLARE @maxLimitTypeID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)
DECLARE @maxLayer int
DECLARE @maxAttach int
DECLARE @maxNamedInsuredID int
DECLARE @maxProvisionValueID int
DECLARE @maxProvisionLabelID int
DECLARE @minProvisionValueID int
DECLARE @maxEndDate int

-- Find max ID values
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxLayer = MAX(Layer) FROM vPolicy
SELECT @maxAttach = MAX(AttachmentPoint) FROM vPolicy
SELECT @maxNamedInsuredID = MAX(NamedInsuredID) FROM vPolicy
SELECT @maxProvisionLabelID = MAX(ProvisionLabelID) FROM vProvisions
SELECT @maxEndDate = MAX(annualperiod) FROM vPolicy
IF @carrierGroupID = 0 SET @carrierGroupID = NULL

-- Eliminate Null Values
IF @highAttach IS NULL
	SET @highAttachNum = @maxAttach
ELSE 
	SET @highAttachNum = CONVERT(money, @highAttach)
IF @lowAttach IS NULL
	SET @lowAttachNum = 0
ELSE 
	SET @lowAttachNum =  CONVERT(money, @lowAttach)

----Declare the lists of selected IDs; note: these will be passed in, and thus should be commented out in actual stored proc
--DECLARE @selectedProvisions nvarchar(1000)
--SET @selectedProvisions = '20,21'
--DECLARE @selectedProvisionValues nvarchar(1000)
--SET @selectedProvisionValues = '96'

--Declare backup strings of the passed in lists to be used in the ultimate where clauses below
DECLARE @selectedProvisionsList nvarchar(1000)
SET @selectedProvisionsList = @selectedProvisions
DECLARE @selectedProvisionValuesList nvarchar(1000)
SET @selectedProvisionValuesList = @selectedProvisionValues

--Declare the tables to be used for the separated provision and provision value IDs
DECLARE @selectedProvisionsTable TABLE(provisionLabelID int)
DECLARE @selectedProvisionValuesTable TABLE(provisionValueID int)

--Prepare flags if there is only a zero in the table (and thus there should be no filter)
DECLARE @flagProvisionFilter int
DECLARE @flagProvisionValueFilter int
SET @flagProvisionFilter = 0
SET @flagProvisionValueFilter = 0

--Parse out provision and provision value ID strings	
WHILE LEN(@selectedProvisions)>0 BEGIN
	IF CHARINDEX(',',@selectedProvisions) = 0 BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, @selectedProvisions)
		IF CONVERT(int,@selectedProvisions) <> 0
			SET @flagProvisionFilter = 1
		SET @selectedProvisions = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, LEFT(@selectedProvisions, CHARINDEX(',',@selectedProvisions)-1))
		SET @selectedProvisions = RIGHT(@selectedProvisions,LEN(@selectedProvisions)-CHARINDEX(',',@selectedProvisions))
		SET @flagProvisionFilter = 1
	END
END
WHILE LEN(@selectedProvisionValues)>0 BEGIN
	IF CHARINDEX(',',@selectedProvisionValues) = 0 BEGIN
		INSERT INTO @selectedProvisionValuesTable
			SELECT CONVERT(int, @selectedProvisionValues)
		IF CONVERT(int,@selectedProvisionValues) <> 0
			SET @flagProvisionValueFilter = 1
		SET @selectedProvisionValues = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedProvisionValuesTable
			SELECT CONVERT(int, LEFT(@selectedProvisionValues, CHARINDEX(',',@selectedProvisionValues)-1))
		SET @selectedProvisionValues = RIGHT(@selectedProvisionValues,LEN(@selectedProvisionValues)-CHARINDEX(',',@selectedProvisionValues))
		SET @flagProvisionValueFilter = 1
	END
END


---------------------------------------------------------------------------------------
--QUERY DATA
IF @flagProvisionFilter = 1 AND @flagProvisionValueFilter = 1

SELECT pr.Provision, pr.ProvisionValue, pinfo.InsuranceProgram, pinfo.InsuranceProgramID, pinfo.NamedInsuredID, pinfo.NamedInsured, pinfo.MinPStartDate, pinfo.PolicyID, pinfo.PolicyNum, pinfo.PolicyNotes, 
	pinfo.AnnualPeriod AS SplitAnnualPeriod, pinfo.ActualPStartDate AS SplitStart, pinfo.ActualPEndDate AS SplitEnd, pinfo.CarrierID, pinfo.CarrierName, pinfo.CarrierSolvency, pinfo.CarrierGroupID, pinfo.CarrierGroupName,
	pinfo.LimitTypeID, pinfo.Layer, pinfo.AttachmentPoint, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit 
FROM vPolicy pinfo INNER JOIN vProvisions pr ON pinfo.PolicyID = pr.PolicyID
WHERE pinfo.InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND 
	pinfo.LimitTypeID >= COALESCE(@limitTypeID, 0) AND pinfo.LimitTypeID  <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	pinfo.CarrierID >= COALESCE(@carrierID, 0) AND pinfo.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
	(coalesce(pinfo.CarrierGroupID, 0) >= COALESCE(@carrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID, 0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID, 0) = @carrierGroupID) AND
	pinfo.NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND pinfo.NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsuredID) AND
	pinfo.CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND pinfo.Layer BETWEEN COALESCE(@lowLayer,0) AND COALESCE(NullIF(@highLayer,0),@maxLayer) AND
	pinfo.AttachmentPoint BETWEEN @lowAttachNum AND @highAttachNum AND
	pr.ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) AND 
	(pr.ProvisionValueID IN(SELECT * FROM @selectedProvisionValuesTable) OR (pr.ProvisionValueID IN(SELECT ProvisionValueID FROM tblProvisionValue WHERE ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) AND ProvisionLabelID NOT IN(SELECT ProvisionLabelID FROM tblProvisionValue WHERE ProvisionValueID IN(@selectedProvisionValuesList)))))
	and DoNotReport <> 1
	AND AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)

ELSE IF @flagProvisionFilter = 1

SELECT pr.Provision, pr.ProvisionValue, pinfo.InsuranceProgram, pinfo.InsuranceProgramID, pinfo.NamedInsuredID, pinfo.NamedInsured, pinfo.MinPStartDate, pinfo.PolicyID, pinfo.PolicyNum, pinfo.PolicyNotes, 
	pinfo.AnnualPeriod AS SplitAnnualPeriod, pinfo.ActualPStartDate AS SplitStart, pinfo.ActualPEndDate AS SplitEnd, pinfo.CarrierID, pinfo.CarrierName, pinfo.CarrierSolvency, pinfo.CarrierGroupID, pinfo.CarrierGroupName,
	pinfo.LimitTypeID, pinfo.Layer, pinfo.AttachmentPoint, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit 
FROM vPolicy pinfo INNER JOIN vProvisions pr ON pinfo.PolicyID = pr.PolicyID
WHERE pinfo.InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND 
	pinfo.LimitTypeID >= COALESCE(@limitTypeID, 0) AND pinfo.LimitTypeID  <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	pinfo.CarrierID >= COALESCE(@carrierID, 0) AND pinfo.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
	(coalesce(pinfo.CarrierGroupID, 0) >= COALESCE(@carrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID, 0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID, 0) = @carrierGroupID) AND
	pinfo.NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND pinfo.NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsuredID) AND
	pinfo.CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND pinfo.Layer BETWEEN COALESCE(@lowLayer,0) AND COALESCE(NullIF(@highLayer,0),@maxLayer) AND
	pinfo.AttachmentPoint BETWEEN @lowAttachNum AND @highAttachNum AND
	pr.ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) AND 
	DoNotReport <> 1
	AND AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)

ELSE

SELECT pr.Provision, pr.ProvisionValue, pinfo.InsuranceProgram, pinfo.InsuranceProgramID, pinfo.NamedInsuredID, pinfo.NamedInsured, pinfo.MinPStartDate, pinfo.PolicyID, pinfo.PolicyNum, pinfo.PolicyNotes, 
	pinfo.AnnualPeriod AS SplitAnnualPeriod, pinfo.ActualPStartDate AS SplitStart, pinfo.ActualPEndDate AS SplitEnd, pinfo.CarrierID, pinfo.CarrierName, pinfo.CarrierSolvency, pinfo.CarrierGroupID, pinfo.CarrierGroupName,
	pinfo.LimitTypeID, pinfo.Layer, pinfo.AttachmentPoint, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit 
FROM vPolicy pinfo INNER JOIN vProvisions pr ON pinfo.PolicyID = pr.PolicyID
WHERE pinfo.InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND 
	pinfo.LimitTypeID >= COALESCE(@limitTypeID, 0) AND pinfo.LimitTypeID  <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	pinfo.CarrierID >= COALESCE(@carrierID, 0) AND pinfo.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
	(coalesce(pinfo.CarrierGroupID, 0) >= COALESCE(@carrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID, 0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID, 0) = @carrierGroupID) AND
	pinfo.NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND pinfo.NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsuredID) AND
	pinfo.CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND pinfo.Layer BETWEEN COALESCE(@lowLayer,0) AND COALESCE(NullIF(@highLayer,0),@maxLayer) AND
	pinfo.AttachmentPoint BETWEEN @lowAttachNum AND @highAttachNum AND
	DoNotReport <> 1
	AND AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)


SET CONCAT_NULL_YIELDS_NULL ON
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyByProvision] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyByProvision] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptPolicyByProvision] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyByProvision] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyByProvision] TO [power_user]
GO
