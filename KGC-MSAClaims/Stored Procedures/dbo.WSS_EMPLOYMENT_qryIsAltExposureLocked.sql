SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryIsAltExposureLocked]
@ClaimantPersonalInfoID bigint=null, @emailaddress nvarchar(250)
AS
BEGIN


--DECLARE @ClaimantpersonalinfoID bigint
--SET @ClaimantPersonalInfoID = 6
--DECLARE @Emailaddress nvarchar(250)
--SET @emailaddress = 'crookc@kcicllc.com'

DECLARE @LastClaimantPersonalInfOID bigint = 0
DECLARE @navString nvarchar(MAX) = ''
DECLARE @CPIID bigint



DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)	

IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR RIGHT(@emailaddress, 8) = 'kcic.com' OR @NoFilters = 1
BEGIN

				SELECT DISTINCT COUNT(UnLockAltExposure)
				FROM         dbo.tblClaimantPersonalInfo AS CPI 
				WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
					AND UnlockAltExposure = 1

END
IF (@InsurerRole = 1)
BEGIN
	SELECT DISTINCT COUNT(UnLockAltExposure)
	FROM         dbo.tblClaimantPersonalInfo AS CPI 
        LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND UnlockAltExposure = 1
		AND c.ClaimID NOT IN (select ClaimID 
							  from tblTenderHistory th 
								INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID 
							  where TenderStatusID in (2, 5) 
								and tenderblockid = 15 
								AND TenderTypeID <> 2)
		AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
END
ELSE
BEGIN
					SELECT DISTINCT COUNT(UnLockAltExposure)                     
					FROM         dbo.tblClaimantPersonalInfo AS CPI 
						LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
						LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
						INNER JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID
						INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
					Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
					AND UnlockAltExposure = 1
					AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)

END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryIsAltExposureLocked] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryIsAltExposureLocked] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_EMPLOYMENT_qryIsAltExposureLocked] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryIsAltExposureLocked] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryIsAltExposureLocked] TO [power_user]
GO
