SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryTenderDetails]
(@TenderHistoryID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	th.TenderHistoryID, 
			th.AdditionalText, 
			th.EffectiveDate, 
			th.DateApproved, 
			th.ApprovedBy, 
			th.TenderTypeID, 
			TenderType, 
			TenderBlockName, 
			th.TenderBlockID,
			th.TenderTemplateID,
			TemplateDisplayName,
			th.DocumentID
	FROM tblTenderHistory th
		INNER JOIN tblClaimTenderHistory cth ON th.TenderHistoryID = cth.TenderHistoryID
		INNER JOIN tblTenderBlock tb ON th.TenderBlockID = tb.TenderBlockID
		LEFT JOIN tblTenderTemplate tt ON th.TenderTemplateID = tt.TenderTemplateID
		LEFT JOIN tblDocuments cd ON th.ComplaintDocumentID = cd.DocumentID
		LEFT JOIN tblDocuments d ON th.DocumentID = d.DocumentID
		LEFT JOIN tblTenderType ttype ON th.TenderTypeID = ttype.TenderTypeID
		--LEFT JOIN tblTenderTemplate tt ON th.TenderBlockID = tt.TenderBlockID AND th.TenderTypeID = tt.TenderTypeID
		WHERE th.TenderHistoryID = @TenderHistoryID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTenderDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTenderDetails] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryTenderDetails] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTenderDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTenderDetails] TO [power_user]
GO
