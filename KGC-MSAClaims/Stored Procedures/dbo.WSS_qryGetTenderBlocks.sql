SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetTenderBlocks]
(@StartDate date, @MaterialID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF(@StartDate IS NULL)
	BEGIN
		--no specific start date 
		SELECT 3
	END
	ELSE
	BEGIN
		SELECT TenderBlockID FROM tblTenderRules tr
				WHERE @StartDate BETWEEN tr.StartDate AND tr.EndDate 	
				AND  AllowAsbestos = (SELECT ContainsAsbestos FROM tblMaterial WHERE MaterialID = @MaterialID)
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetTenderBlocks] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetTenderBlocks] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryGetTenderBlocks] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetTenderBlocks] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetTenderBlocks] TO [power_user]
GO
