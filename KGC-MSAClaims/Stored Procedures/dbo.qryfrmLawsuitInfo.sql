SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryfrmLawsuitInfo] (@LawsuitID int)
as

Select LawsuitID,
	CaseCaption, 
	DocketNumber, 
	FileDate, 
	JurisdictionID, 
	StateID, 
	CourtTypeID, 
	ServiceDate, 
	ReceivedDate, 
	DefenseCounselAddressID,
	PlaintiffCounselAddressID,
	LeadPlaintiffFirstName, 
	LeadPlaintiffLastName,
	--ActiveTrialDate,
	NationalPlaintiffCounselAddressID as PrimaryNationalPlaintiffCounselID
From tblLawsuit
Where LawsuitID = @LawsuitID
Order By FileDate desc, DocketNumber, CaseCaption

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawsuitInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawsuitInfo] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmLawsuitInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawsuitInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawsuitInfo] TO [power_user]
GO
