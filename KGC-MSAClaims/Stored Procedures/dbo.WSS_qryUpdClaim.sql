SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdClaim]
	(
		@ClaimDiseaseID bigint, 
		@DiseaseID int, 
		@ActiveTrialDate datetime, 
		@LawsuitID bigint,
		@Username nvarchar(250),
		@ClaimID bigint, 
		@DiseaseDiagnosisDate datetime, 
		@DoctorID int,
		@PlaintiffCounselAddressID int,
		@NationalPlaintiffCounselAddressID int,
		@DemandAmount money, 
		@DemandDate datetime, 
		@OfferAmount money, 
		@OfferDate datetime, 
		@SettlementFundsRequestDate datetime, 
		@SettlementFundsRequested money, 
		@SettlementFundsRequestDesc nvarchar(max),
		@HasLCC bit = 0,
		@HasSSEC bit = 0,
		@HistoricalNotes nvarchar(MAX)
	)
AS
BEGIN
	UPDATE tblClaimDisease
	SET DiseaseID = @DiseaseID,
	    SPUsername = @Username,
	    DiseaseDiagnosisDate = @DiseaseDiagnosisDate,
	    DoctorID = CASE WHEN @DoctorID = 0 THEN NULL ELSE @DoctorID END
	WHERE ClaimDiseaseID = @ClaimDiseaseID
	
	UPDATE tblLawsuit
	SET SPUsername = @Username,
	    PlaintiffCounselAddressID = @PlaintiffCounselAddressID,
	    NationalPlaintiffCounselAddressID = @NationalPlaintiffCounselAddressID
	WHERE LawsuitID = @LawsuitID
	
	UPDATE tblClaimLawsuit
	SET ActiveTrialDate = @ActiveTrialDate,
		SPUsername = @Username
	WHERE LawsuitID = @LawsuitID AND ClaimID = @ClaimID
	
	UPDATE tblClaim
	SET SPUsername = @Username,
		HasLossofConsortiumClaim = @HasLCC,
		HasEstateClaim = @HasSSEC,
		HistoricalNotes = @HistoricalNotes
      WHERE ClaimID = @ClaimID

DECLARE @ClaimLawsuitID int 
SET @ClaimLawsuitID = (SELECT TOP 1 ClaimLawsuitID FROM tblClaimLawsuit WHERE ClaimID = @ClaimID AND LawsuitID = @LawsuitID)





IF (SELECT COUNT(ClaimantSettlementID) FROM tblClaimantSettlement WHERE ClaimLawsuitID =  @ClaimLawsuitID) > 0
BEGIN
	UPDATE tblClaimantSettlement
	SET DemandAmount = @DemandAmount, 
		DemandDate = @DemandDate, 
		OfferAmount = @OfferAmount, 
		OfferDate = @OfferDate, 
		SettlementsFundsRequestDate = @SettlementFundsRequestDate, 
		SettlementFundsRequested = @SettlementFundsRequested, 
		SettlementFundsRequestDesc = @SettlementFundsRequestDesc,
		SPUsername = @Username
	WHERE ClaimLawsuitID = @ClaimLawsuitID
END
ELSE
BEGIN
INSERT INTO tblClaimantSettlement (ClaimLawsuitID, DemandAmount, DemandDate, OfferAmount, OfferDate, SettlementsFundsRequestDate, SettlementFundsRequested, SettlementFundsRequestDesc, SPUsername)
VALUES (@ClaimLawsuitID, @DemandAmount,	@DemandDate, @OfferAmount, @OfferDate, @SettlementFundsRequestDate, @SettlementFundsRequested, @SettlementFundsRequestDesc, @Username)
END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaim] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdClaim] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaim] TO [power_user]
GO
