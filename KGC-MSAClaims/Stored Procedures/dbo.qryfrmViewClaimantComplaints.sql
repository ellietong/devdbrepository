SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmViewClaimantComplaints] (@ClaimantPersonalInfoID int)
AS

--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 55

SELECT
	d.DocumentID AS ClaimantDocumentID,
	mcd.ClaimantPersonalInfoID,
	CASE
		WHEN DateCreated IS NULL THEN ClaimantDocumentType + ': ' + DocumentTitle ELSE ClaimantDocumentType + ': ' + DocumentTitle + ': ' + CONVERT(nvarchar, MONTH(DateCreated)) + '/' + CONVERT(nvarchar, DAY(DateCreated)) + '/' + CONVERT(nvarchar, YEAR(DateCreated))
	END AS DocumentTypeWithDate,
	'https://admin.kcicllc.com' + DocumentURL AS DocumentLink
FROM tblMultiClaimantDocument mcd
INNER JOIN tblDocuments d ON mcd.DocumentID = d.DocumentID
INNER JOIN tblClaimantDocumentType ct ON d.DetailDocumentTypeID = ct.ClaimantDocumentTypeID
WHERE mcd.ClaimantPersonalInfoID = @CLaimantPersonalInfoID
AND d.DetailDocumentTypeID IN (8, 4, 33)
AND mcd.QCBy IS NULL

RETURN



--GO
--/****** Object:  StoredProcedure [dbo].[qryfrmViewSettlementDocuments]    Script Date: 9/6/2013 8:20:45 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

--ALTER PROC [dbo].[qryfrmViewSettlementDocuments] (@ClaimLawsuitID int)
--as

----DECLARE @ClaimLawsuitID int
----SET @ClaimLawsuitID = 17075

--SELECT
--	ClaimantDocumentID,
--	UPPER(DocCategory) + ': PACE Document - ' + ClaimantDocumentType + COALESCE(' (' + pagenums + ')', '') AS DocumentTypeWithDate,
--	CASE
--		WHEN LEFT(DocumentURL, 2) NOT LIKE '/c' THEN DocumentURL ELSE 'https://admin.kcicllc.com' + DocumentURL
--	END AS DocumentLink
--FROM tblSettleDocs sd
--INNER JOIN tblSettlePaymentDocumentation pd ON sd.SetPymtID = pd.SetPymtID
--LEFT JOIN tblDocumentCategory cd ON sd.DocCategoryID = cd.DocCategoryID
--LEFT JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
--LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
--INNER JOIN tblClaimantDocuments d ON sd.DocumentID = d.ClaimantDocumentID AND d.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
--LEFT JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
--WHERE pd.ClaimLawsuitID = @ClaimLawsuitID UNION SELECT
--	sd.DocumentID,
--	CASE
--		WHEN DateCreated IS NULL THEN DocCategory ELSE UPPER(DocCategory) + ': ' + DocumentTitle + ' - ' + CONVERT(nvarchar, MONTH(DateCreated)) + '/' + CONVERT(nvarchar, DAY(DateCreated)) +
--	'/' + CONVERT(nvarchar, YEAR(DateCreated))
--	END + COALESCE(' (' + pagenums + ')', '') AS DocumentTypeWithDate,
--	CASE
--		WHEN SUBSTRING(DocumentURL, 8, 8) = 'extranet' THEN 'https://admin' + SUBSTRING(DocumentURL, 17, LEN(DocumentURL) - 15)
--		WHEN SUBSTRING(DocumentURL, 9, 8) = 'extranet' THEN 'https://admin' + SUBSTRING(DocumentURL, 17, LEN(DocumentURL) - 15) ELSE DocumentURL
--	END AS DocumentLink
--FROM tblSettleDocs sd
--INNER JOIN tblSettlePaymentDocumentation pd ON sd.SetPymtID = pd.SetPymtID
--LEFT JOIN tblDocumentCategory cd ON sd.DocCategoryID = cd.DocCategoryID
--LEFT JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
--LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
--INNER JOIN tblDocuments d ON sd.DocumentID = d.DocumentID AND d.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
--WHERE pd.ClaimLawsuitID = @ClaimLawsuitID
--ORDER BY DocumentTypeWithDate

--Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewClaimantComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewClaimantComplaints] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmViewClaimantComplaints] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewClaimantComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewClaimantComplaints] TO [power_user]
GO
