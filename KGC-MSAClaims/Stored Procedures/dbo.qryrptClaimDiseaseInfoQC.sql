SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[qryrptClaimDiseaseInfoQC] (@ClaimID int)
As
Set NoCount On

Select cd.ClaimID, CD.DiseaseDiagnosisDate, CD.PrimaryDisease, d.Disease, 
CASE WHEN do.DoctorMiddleName is NULL THEN do.DoctorLastName + ', ' + do.DoctorFirstName ELSE do.DoctorLastName + ', ' + do.DoctorFirstName+ ' ' + do.DoctorMiddleName END as DoctorName
From tblClaimDisease CD
Left Join tblDisease D on D.DiseaseID = CD.DiseaseID
Left Join tblDoctor Do on Do.DoctorID = CD.DoctorID
Where ClaimID = @ClaimID
Order By PrimaryDisease Desc


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimDiseaseInfoQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimDiseaseInfoQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimDiseaseInfoQC] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptClaimDiseaseInfoQC] TO [Claims_User]
GRANT ALTER ON  [dbo].[qryrptClaimDiseaseInfoQC] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimDiseaseInfoQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimDiseaseInfoQC] TO [power_user]
GO
