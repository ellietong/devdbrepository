SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  Procedure [dbo].[qryrptARByAnnualPeriodByOcc]
(@allocationID int = null)
As

SET NOCOUNT On
-----------Not Updated for Discounting or In-Claim--------------
--DECLARE @AllocationID int
--SET @AllocationID = 83

DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation

/* Select applicable occurrence IDs*/
DECLARE @tmpOccConsideredOccurrencesConsideredinAllocation Table (occurrenceid bigint, defenseconsidered money, costsconsidered money, allocationid int)

/* Insert costs cosidered into table*/
INSERT INTO @tmpOccConsideredOccurrencesConsideredinAllocation (occurrenceid, defenseconsidered, costsconsidered, allocationid)

(SELECT OccurrenceID, SUM(Cost1+Cost2) AS 'DefenseConsidered', SUM(Cost3+Cost4+Cost5+Cost6+Cost7+Cost8+Cost9+Cost10+Cost11+Cost12) AS 'CostsConsidered', @allocationid AS 'AllocationID'
FROM vOccurrence 
WHERE OccurrenceID NOT IN
	(SELECT aoe.OccurrenceID 
	FROM vAllocation a INNER JOIN tblAllocationOccurrenceExclude aoe ON a.AllocationID = aoe.AllocationID 
	WHERE a.AllocationID = @allocationid) AND 
(OccurrenceTypeID IN  --Update to NOT IN for cases where occurrence types are excluded
	(SELECT aote.OccurrenceTypeID
	FROM vAllocation a INNER JOIN tblAllocationOccurrenceTypeInclude aote ON a.AllocationID = aote.AllocationID 
	WHERE a.AllocationID = @allocationid))
GROUP BY OccurrenceID)

/*Query temp table*/

DECLARE @Policy TABLE (PolicyID int)
INSERT INTO @Policy

SELECT PolicyID
FROM vPolicy
GROUP BY PolicyID

SELECT costsconsidered, o.OccurrenceName, ar.AllocationID, ar.AnnualPeriodSplit, SUM(ar.SIRAllocated) AS SumOfSIRAllocated, SUM(ar.PV) AS SumOfPV, SUM(ar.Allocation) AS SumOfAllocation, 
	SUM(SIRAllocated+Allocation) AS TotalAllocated, MAX(ar.DateTime) AS MaxOfDateTime, a.AllocationDescription, ar.TriggerStartDateUsed, ar.TriggerEndDateUsed, o.OccurrenceID, 
	SUM(ar.AllocationIndemnity) AS SumOfAllocationIndemnity, SUM(ar.AllocationDefense) AS SumOfAllocationDefense, Sum(ar.AllocationDefenseOutsideLimits) AS SumOfAllocationDefenseOutsideLimits
FROM (@Policy p INNER JOIN (vOccurrence o INNER JOIN (vAllocation a INNER JOIN vAllocationResults ar ON a.AllocationID = ar.AllocationID) ON o.OccurrenceID = ar.OccurrenceID) 
	ON p.PolicyID = ar.PolicyID) INNER JOIN @tmpOccConsideredOccurrencesConsideredinAllocation tmp ON (a.AllocationID = tmp.AllocationID) AND (o.OccurrenceID = tmp.OccurrenceID)
WHERE ar.AllocationID >= COALESCE(@allocationID, 0) AND ar.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) 
GROUP BY costsconsidered, o.OccurrenceName, ar.AllocationID, ar.AnnualPeriodSplit, a.AllocationDescription, ar.TriggerStartDateUsed, ar.TriggerEndDateUsed, o.OccurrenceID
ORDER BY ar.AllocationID, ar.AnnualPeriodSplit

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAnnualPeriodByOcc] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByAnnualPeriodByOcc] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptARByAnnualPeriodByOcc] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAnnualPeriodByOcc] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByAnnualPeriodByOcc] TO [power_user]
GO
