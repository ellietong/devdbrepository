SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryClaimantPersonalInfo]
@ClaimantPersonalInfoID bigint=null, @emailaddress nvarchar(250)
AS
BEGIN



--DECLARE @ClaimantpersonalinfoID bigint
--SET @ClaimantPersonalInfoID = 41750
--DECLARE @Emailaddress nvarchar(250)
--SET @emailaddress = 'ckirby@huddlestonbolen.com'

--DECLARE @CPI_INJ TABLE (ClaimantPersonalInfoID bigint, NavIDs nvarchar(MAX))

DECLARE @LastClaimantPersonalInfOID bigint = 0
DECLARE @navString nvarchar(MAX) = ''
DECLARE @CPIID bigint



DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
DECLARE @InsurerRole bit
SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)	

IF RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1
BEGIN

				SELECT CPI.ClaimantPersonalInfoID, CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END
                      + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, Gender, COALESCE(cpi.GenderID, 4) AS GenderID, COALESCE(cpi.MaritalStatusID, 1) AS MaritalStatusID,
                      MaritalStatus, coalesce(nullif(coalesce(Address1 + CHAR(13) + CHAR(10), '') + coalesce(Address2 + CHAR(13) + CHAR(10),'') + 
						coalesce(City + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,Zip),'') + Coalesce('-'+convert(varchar,mailzip),''),''),'') as CompleteAddress, BirthDate, DeceasedDate, City,
					  s.StateID, Zip, MailZip, Address1, Address2, IsSmoker, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS FullSSN, HealthInsuranceClaimNumber, MedicareBeneficiaryStatus,
					  '' As NavInjPartyID, SmokerStart, SmokerEnd, PacksPerDay                 
					  
					FROM         dbo.tblClaimantPersonalInfo AS CPI 
					 LEFT JOIN tblGender as G on G.GenderID = CPI.GenderID
                     LEFT JOIN tblMaritalStatus as MS on MS.MaritalStatusID=CPI.MaritalStatusID
                     LEFT JOIN tblState s ON cpi.StateID = s.StateID
                     lEFT JOIN tblMedicareBeneficiaryStatus mbs ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID
                     LEFT JOIN tblClaimantSuffix cs ON cpi.SuffixID = cs.SuffixID
                     --LEFT JOIN @CPI_INJ inj ON cpi.ClaimantPersonalinfoID = inj.ClaimantPersonalInfoID
				Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID

END
IF (@InsurerRole = 1)
BEGIN
	SELECT DISTINCT
                      CPI.ClaimantPersonalInfoID, CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, Gender, COALESCE(cpi.GenderID, 4) AS GenderID, COALESCE(cpi.MaritalStatusID, 1) AS MaritalStatusID,
                      MaritalStatus, coalesce(nullif(coalesce(Address1 + CHAR(13) + CHAR(10), '') + coalesce(Address2 + CHAR(13) + CHAR(10),'') + 
						coalesce(City + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,Zip),'') + Coalesce('-'+convert(varchar,mailzip),''),''),'') as CompleteAddress, BirthDate, DeceasedDate, City,
					  s.StateID, Zip, MailZip, Address1, Address2, IsSmoker, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS FullSSN, HealthInsuranceClaimNumber, MedicareBeneficiaryStatus,
                     '' As NavInjPartyID
					FROM         dbo.tblClaimantPersonalInfo AS CPI 
					 LEFT JOIN tblGender as G on G.GenderID = CPI.GenderID
                     LEFT JOIN tblMaritalStatus as MS on MS.MaritalStatusID=CPI.MaritalStatusID
                     LEFT JOIN tblState s ON cpi.StateID = s.StateID
                     LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
					LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
					LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
					INNER JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID
					INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
					lEFT JOIN tblMedicareBeneficiaryStatus mbs ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID
					LEFT JOIN tblClaimantSuffix cs ON cpi.SuffixID = cs.SuffixID
					--LEFT JOIN @CPI_INJ inj ON cpi.ClaimantPersonalinfoID = inj.ClaimantPersonalInfoID
				Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
				AND c.ClaimID not in (select ClaimID from tblClaimTenderHistory cth INNER JOIN tblTenderHistory th ON cth.TenderHistoryID = th.TenderHistoryID where TenderStatusID in (2, 5) and tenderblockid = 15 AND TenderTypeID <> 2)
				AND c.ClaimID NOT IN (select th.ClaimID from tblTender_History th INNER JOIN tblTender_Acknowledgement ta on ta.TenderHistoryID = th.TenderHistoryID where ta.PersonID = 3279 and th.TenderTypeID in (1,3))
END
ELSE
BEGIN

				SELECT DISTINCT CPI.ClaimantPersonalInfoID,
                      CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, Gender, COALESCE(cpi.GenderID, 4) AS GenderID, COALESCE(cpi.MaritalStatusID, 1) AS MaritalStatusID,
                      MaritalStatus, coalesce(nullif(coalesce(Address1 + CHAR(13) + CHAR(10), '') + coalesce(Address2 + CHAR(13) + CHAR(10),'') + 
						coalesce(City + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,Zip),'') + Coalesce('-'+convert(varchar,mailzip),''),''),'') as CompleteAddress, BirthDate, DeceasedDate, City,
					  s.StateID, Zip, MailZip, Address1, Address2, IsSmoker, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS FullSSN, HealthInsuranceClaimNumber, MedicareBeneficiaryStatus,
                     '' As NavInjPartyID
                        
					FROM         dbo.tblClaimantPersonalInfo AS CPI 
					 LEFT JOIN tblGender as G on G.GenderID = CPI.GenderID
                     LEFT JOIN tblMaritalStatus as MS on MS.MaritalStatusID=CPI.MaritalStatusID
                     LEFT JOIN tblState s ON cpi.StateID = s.StateID
                     LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
					LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
					LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
					INNER JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID
					INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
					lEFT JOIN tblMedicareBeneficiaryStatus mbs ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID
					LEFT JOIN tblClaimantSuffix cs ON cpi.SuffixID = cs.SuffixID
					--LEFT JOIN @CPI_INJ inj ON cpi.ClaimantPersonalinfoID = inj.ClaimantPersonalInfoID
				Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)

END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantPersonalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantPersonalInfo] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryClaimantPersonalInfo] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantPersonalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantPersonalInfo] TO [power_user]
GO
