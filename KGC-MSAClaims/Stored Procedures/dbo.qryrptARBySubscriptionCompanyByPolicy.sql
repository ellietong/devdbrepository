SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[qryrptARBySubscriptionCompanyByPolicy] 
(@SubscriberID int = NULL, @Solvency varchar(50) = NULL, @InsuranceProgramID int = NULL, @AllocationID int = NULL, @LimitTypeID int = NULL)
AS
SET NOCOUNT ON

--DECLARE @InsuranceProgramID int 
--DECLARE @SubscriberID int 
--DECLARE @Solvency varchar(50) 
--DECLARE @AllocationID int
--DECLARE @LimitTypeID int
--SET @InsuranceProgramID = 0
--SET @SubscriberID = 0
--SET @Solvency = null
--SET @AllocationID = 83
--SET @LimitTypeID = 0

DECLARE @maxInsuranceProgramID int
DECLARE @maxSubscriberID int
DECLARE @maxAllocationID int
DECLARE @maxLimitTypeID int
SELECT @maxInsuranceProgramID = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxSubscriberID = MAX(SubscriberID) FROM vSubscription
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy

SELECT s.SubscriberID, s.SubscriptionCompany, p.PolicyHolder, MAX(ar.DateTime) AS [DateTime], p.CarrierGroupName, p.CarrierName, p.InsuranceProgram, 
	s.SubscriptionActualPercentOfPolicy,
	s.SubscriptionAllocationPercentOfPolicy, 
	SUM(ar.PV * s.SubscriptionActualPercentOfPolicy) AS PVAmtActual, 
	SUM(ar.Allocation * s.SubscriptionActualPercentOfPolicy) AS AllocationAmtActual, 
	SUM(ar.SIRAllocated * s.SubscriptionActualPercentOfPolicy) AS SIRAmtActual,
	SUM(ar.PV * s.SubscriptionAllocationPercentOfPolicy) AS PVAmtAllocation, 
	SUM(ar.Allocation * s.SubscriptionAllocationPercentOfPolicy) AS AllocationAmtAllocation, 
	SUM(ar.SIRAllocated * s.SubscriptionAllocationPercentOfPolicy) AS SIRAmtAllocation, 
	a.AllocationID, a.AllocationDescription, 
	p.PolicyNum, p.AttachmentPoint, p.PerOccLimit, p.LayerPerOccLimit, p.SIR, p.AggregateLimit, ar.PStartSplit, ar.PEndSplit, ar.AnnualPeriodSplit, p.PolicyID, p.Layer, 
	s.SubscriptionCompanySolvency, p.InsuranceProgramID
FROM vPolicy p 
	INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID
	INNER JOIN vAllocationResults ar ON p.PolicydateID = ar.PolicydateID
	LEFT JOIN vAllocation a ON a.AllocationID = ar.AllocationID
WHERE p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) AND
	s.SubscriberID >= COALESCE(@SubscriberID, 0) AND s.SubscriberID <= COALESCE(NULLIF(@SubscriberID, 0), @maxSubscriberID) AND
	ar.AllocationID >= COALESCE(@AllocationID, 0) AND ar.AllocationID <= COALESCE(NULLIF(@AllocationID, 0), @maxAllocationID) AND 
	s.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%') AND
	p.LimitTypeID >= COALESCE(@LimitTypeID, 0) AND p.LimitTypeID <= COALESCE(NULLIF(@LimitTypeID, 0), @maxLimitTypeID)
GROUP BY s.SubscriberID, s.SubscriptionCompany, s.SubscriptionActualPercentOfPolicy, s.SubscriptionAllocationPercentOfPolicy, 
	p.PolicyHolder,  p.CarrierGroupName, p.CarrierName, p.InsuranceProgram, a.AllocationID, a.AllocationDescription, p.PolicyNum, 
	p.AttachmentPoint, p.PerOccLimit, p.LayerPerOccLimit, p.SIR, p.AggregateLimit, ar.PStartSplit, ar.PEndSplit, ar.AnnualPeriodSplit, 
	p.PolicyID, p.Layer, s.SubscriptionCompanySolvency, p.InsuranceProgramID
ORDER BY p.CarrierName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriptionCompanyByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriptionCompanyByPolicy] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptARBySubscriptionCompanyByPolicy] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriptionCompanyByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriptionCompanyByPolicy] TO [power_user]
GO
