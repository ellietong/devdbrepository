SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantSettlementInfoEntry]
as
 
Select *
From tblClaimantSettlement cs
Order By SettlementDate desc
 
Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [power_user]
GO
