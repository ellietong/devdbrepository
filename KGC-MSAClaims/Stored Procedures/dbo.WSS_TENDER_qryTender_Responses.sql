SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_TENDER_qryTender_Responses]
(@PersonID int, @TenderHistoryID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *, cdt.ClaimantDocumentType AS DocumentType 
	FROM tblTender_Response tr
	INNER JOIN tblDocuments d ON tr.DocumentID = d.DocumentID
	INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
	WHERE PersonID = @PersonID AND TenderHistoryID = @TenderHistoryID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTender_Responses] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTender_Responses] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_TENDER_qryTender_Responses] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_TENDER_qryTender_Responses] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_TENDER_qryTender_Responses] TO [power_user]
GO
