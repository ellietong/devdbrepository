SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdTenderDocID_REVERT]
(@TenderHistoryID bigint, @DocumentID bigint, @EffectiveDate datetime)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE tblTenderHistory
    SET DocumentID = @DocumentID,
		TenderStatusID = 4, --set to "Awaiting Approval",
		EffectiveDate = @EffectiveDate
    WHERE TenderHistoryID = @TenderHistoryID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdTenderDocID_REVERT] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdTenderDocID_REVERT] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryUpdTenderDocID_REVERT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdTenderDocID_REVERT] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdTenderDocID_REVERT] TO [power_user]
GO
