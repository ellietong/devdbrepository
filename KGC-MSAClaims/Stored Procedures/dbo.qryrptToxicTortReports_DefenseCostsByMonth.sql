SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptToxicTortReports_DefenseCostsByMonth](@DataDate DATETIME, @Year INT) 
AS
SET NOCOUNT ON

--DECLARE @DataDate DATETIME
--Set @DataDate = '6/18/2014'
--DECLARE @Year INT
--Set @Year = 2013


SELECT COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown') AS DefenseCounsel, COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 1 THEN Costs + Fees END),0) AS January,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 2 THEN Costs + Fees END),0) AS February,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 3 THEN Costs + Fees END),0) AS March,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 4 THEN Costs + Fees END),0) AS April,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 5 THEN Costs + Fees END),0) AS May,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 6 THEN Costs + Fees END),0) AS June,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 7 THEN Costs + Fees END),0) AS July,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 8 THEN Costs + Fees END),0) AS August,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 9 THEN Costs + Fees END),0) AS September,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 10 THEN Costs + Fees END),0) AS October,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 11 THEN Costs + Fees END),0) AS November,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year AND Month(PeriodEnd) = 12 THEN Costs + Fees END),0) AS December,
	COALESCE(SUM(CASE WHEN Year(PeriodEnd) = @Year THEN Costs + Fees END),0) AS Total,
	CASE WHEN COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown') = 'Unknown' THEN 2 ELSE 1 END AS Flag,
	@Year AS YR, @DataDate AS DD

FROM dbo.tblDefenseInvoice DI
	LEFT JOIN dbo.tblDefInvClaimantDetail DICD ON DICD.DefenseInvoiceID = DI.DefenseInvoiceID
	LEFT JOIN dbo.m_tblDefenseCounselLookup_ToxicTortReporting DCL ON DCL.DefenseCounselAddressID = DI.DefenseCounselAddressID
	LEFT JOIN dbo.tblClaim C ON C.ClaimID = DICD.ClaimID
	LEFT JOIN dbo.tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
	LEFT JOIN dbo.tblAllegation A ON A.ClaimLawsuitID = CL.ClaimLawsuitID
	LEFT JOIN dbo.tblMaterial M ON M.MaterialID = A.MaterialID
	LEFT JOIN dbo.m_tblClaimType_StandardMSA CTS ON CTS.MaterialID = M.MaterialID

WHERE COALESCE(NCCReferenceID, 'Unknown') NOT LIKE '%LIT%'
AND COALESCE(M.MaterialID, 100) != 10

GROUP BY COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown'),
	CASE WHEN COALESCE(DefenseCounsel_ToxicTortReport, 'Unknown') = 'Unknown' THEN 2 ELSE 1 END

ORDER BY Flag, DefenseCounsel
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptToxicTortReports_DefenseCostsByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptToxicTortReports_DefenseCostsByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptToxicTortReports_DefenseCostsByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptToxicTortReports_DefenseCostsByMonth] TO [power_user]
GO
