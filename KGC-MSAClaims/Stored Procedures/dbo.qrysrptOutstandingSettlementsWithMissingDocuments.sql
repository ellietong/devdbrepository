SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] (@ClaimantPersonalInfoID int, @LawsuitID int)

As

SET NOCOUNT ON

--DECLARE @ClaimantPersonalInfoID int
--DECLARE @LawsuitID int
--SET @ClaimantPersonalInfoID = 1
--SET @LawsuitID = 10533

DECLARE @ClaimantDocs TABLE(ClaimantIDed int, DxMedicals int, DODCert int, EstatePapers int, ProductID int, EmployExpoHist int, settlementconfirmation int)
DECLARE @MissingDocuments TABLE(MissingDocName nvarchar(100))

INSERT INTO @ClaimantDocs
	SELECT ClaimantIDed, DxMedicals, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE DODCert END AS DODCert, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE EstatePapers END AS EstatePapers, 
		ProductID, EmployExpoHist, settlementconfirmation
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID		
		INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND LawsuitID = @LawsuitID
		
INSERT INTO @MissingDocuments
	SELECT 'Fully Identified - SSN, DOB, DOD' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ClaimantIDed IS NULL Or ClaimantIDed = 0
INSERT INTO @MissingDocuments
	SELECT 'Diagnosing Medicals' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DxMedicals IS NULL Or DxMedicals = 0
INSERT INTO @MissingDocuments
	SELECT 'Death Certificate' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DODCert IS NULL Or DODCert = 0
INSERT INTO @MissingDocuments
	SELECT 'Estate Paperwork' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EstatePapers IS NULL Or EstatePapers = 0
INSERT INTO @MissingDocuments
	SELECT 'Product ID' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ProductID IS NULL Or ProductID = 0
INSERT INTO @MissingDocuments
	SELECT 'Employment/Exposure History' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EmployExpoHist IS NULL Or EmployExpoHist = 0
	INSERT INTO @MissingDocuments
	SELECT 'Settlement Confirmation Letter' AS MissingDocName
	FROM @ClaimantDocs
	WHERE settlementconfirmation IS NULL Or settlementconfirmation = 0

SELECT * FROM @MissingDocuments
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] TO [power_user]
GO
