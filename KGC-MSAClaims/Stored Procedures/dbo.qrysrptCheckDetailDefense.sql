SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create procedure [dbo].[qrysrptCheckDetailDefense] (@CheckID int)

AS
Set NoCount On


Select DI.InvoiceNumber, Period, InvoiceTotal, PaymentAmount
from tblPaymentDefense pd
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Where CheckID = @CheckID


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailDefense] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailDefense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailDefense] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailDefense] TO [CheckQC_User]
GRANT ALTER ON  [dbo].[qrysrptCheckDetailDefense] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailDefense] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailDefense] TO [power_user]
GO
