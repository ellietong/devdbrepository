SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmOccurrenceDef]
AS
SET NOCOUNT ON
SELECT OccurrenceDefID, OccurrenceDef
FROM tblOccurrenceDef
ORDER BY OccurrenceDef
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceDef] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceDef] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmOccurrenceDef] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceDef] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceDef] TO [power_user]
GO
