SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCashActivity] (@MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime)

As
Set NoCount On

--Declare @MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime
--Set @MatterID = 1
--Set @BalanceStartDate = '10/2/2010'
--Set @BalanceEndDate = '4/11/2012'

Declare @StartingBalance money
Set @StartingBalance = (
Coalesce((Select SUM(CashActivityAmount) From tblMiscCashActivity mca 
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID 
where CashActivityDate >= '1/1/2010' and CashActivityDate < @BalanceStartDate and MatterID = @MatterID),0)
+
Coalesce((Select SUM(SourceAmount)*-1 From tblCheck c
Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
Where SourceDate >= '1/1/2010' and SourceDate < @BalanceStartDate and MatterID = @MatterID),0)
)

Declare @Matter nvarchar(250)
Set @Matter = (Select Matter from tblMatter where MatterID = @MatterID)

Declare @Detail table (Matter nvarchar(250), FundingType nvarchar(250), FundingSource nvarchar(250), SourceDate datetime, SourceNumber nvarchar(250), Indemnity money, Defense money, MiscFees money, CashReceipts money, Comment nvarchar(Max), CheckStatus bit)
Insert Into @Detail

--Defense
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, 0, SourceAmount*-1 as SourceAmount, 0, 0, coalesce(defensecounsel, vendorfirm), IsCheckCleared
From tblCheck c
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Inner Join tblPaymentDefense pd on pd.CheckID = c.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
--Inner Join tblDefenseInvoiceFirm dif on dif.FirmID = di.FirmID
Left join tblDefenseCounselAddress da on da.DefenseCounselAddressID=di.DefenseCounselAddressID
left join m_tblVendorFirm v on v.VendorFirmID=di.VendorFirmID
Inner Join tblMatter m on c.MatterID = m.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @BalanceStartDate and SourceDate <= @BalanceEndDate) AND c.MatterID = @MatterID AND c.FundingTypeID = 1
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, DefenseCounsel, vendorfirm, IsCheckCleared

Union ALL

--Settlement
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1 as SourceAmount, 0, 0, 0, PlaintiffCounsel, IsCheckCleared
From tblCheck c
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
Inner Join tblMatter m on c.MatterID = m.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @BalanceStartDate and SourceDate <= @BalanceEndDate) AND c.MatterID = @MatterID AND c.FundingTypeID = 2
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, PlaintiffCounsel, IsCheckCleared

Union ALL

Select Matter, CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, CashActivityAmount, 0, CashActivityNotes, Null
From tblMiscCashActivity mca
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @BalanceStartDate and CashActivityDate <= @BalanceEndDate) AND mca.MatterID = @MatterID AND CashActivityAmount < 0

Union ALL

Select Matter, CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, 0, CashActivityAmount, CashActivityNotes, Null
From tblMiscCashActivity mca
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @BalanceStartDate and CashActivityDate <= @BalanceEndDate) AND mca.MatterID = @MatterID AND CashActivityAmount > 0

Union ALL

Select @Matter, 'Starting Balance', 'N/A', @BalanceStartDate-1 , 'N/A', 0, 0, 0, Coalesce(@StartingBalance,0), 'Starting Balance', Null

Select *, @BalanceStartDate as BalanceStartDate, @BalanceEndDate as BalanceEndDate from @Detail Order By SourceDate
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckWriter_User]
GRANT ALTER ON  [dbo].[qryrptCashActivity] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [power_user]
GO
