GRANT VIEW DEFINITION ON SCHEMA:: [dbo] TO [base_user]
GRANT SELECT ON SCHEMA:: [dbo] TO [base_user]
GRANT VIEW DEFINITION ON SCHEMA:: [dbo] TO [Claims_User]
GRANT SELECT ON SCHEMA:: [dbo] TO [Claims_User]
GRANT INSERT ON SCHEMA:: [dbo] TO [Claims_User]
GRANT DELETE ON SCHEMA:: [dbo] TO [Claims_User]
GRANT UPDATE ON SCHEMA:: [dbo] TO [Claims_User]
GO
