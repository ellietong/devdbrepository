CREATE TABLE [dbo].[tblSubscriberCompany_ALT]
(
[SubscriberID] [int] NOT NULL IDENTITY(1, 1),
[SubscriptionCompany] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubscriptionCompanySolvency] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubscriptionCompanySolvencyStatus] [int] NULL,
[SubscriptionCompanyNote] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubscriberGroup] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SolvencySource] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateUpdated] [smalldatetime] NULL,
[AMBestNo] [int] NULL,
[WellingtonSignatory] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriberCompany_ALT] ADD CONSTRAINT [PK_tblSubscriberCompany] PRIMARY KEY CLUSTERED  ([SubscriberID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriberCompany_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriberCompany_ALT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriberCompany_ALT] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSubscriberCompany_ALT] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSubscriberCompany_ALT] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSubscriberCompany_ALT] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSubscriberCompany_ALT] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriberCompany_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriberCompany_ALT] TO [power_user]
GRANT INSERT ON  [dbo].[tblSubscriberCompany_ALT] TO [power_user]
GRANT DELETE ON  [dbo].[tblSubscriberCompany_ALT] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSubscriberCompany_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriberCompany_ALT] TO [Subscription_User]
GRANT INSERT ON  [dbo].[tblSubscriberCompany_ALT] TO [Subscription_User]
GRANT DELETE ON  [dbo].[tblSubscriberCompany_ALT] TO [Subscription_User]
GRANT UPDATE ON  [dbo].[tblSubscriberCompany_ALT] TO [Subscription_User]
GO
