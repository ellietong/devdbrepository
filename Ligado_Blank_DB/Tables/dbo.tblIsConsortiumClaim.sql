CREATE TABLE [dbo].[tblIsConsortiumClaim]
(
[IsConsortiumClaimID] [int] NOT NULL IDENTITY(1, 1),
[IsConsortiumClaim] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblIsConsortiumClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblIsConsortiumClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblIsConsortiumClaim] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblIsConsortiumClaim] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblIsConsortiumClaim] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblIsConsortiumClaim] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblIsConsortiumClaim] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblIsConsortiumClaim] TO [power_user]
GRANT SELECT ON  [dbo].[tblIsConsortiumClaim] TO [power_user]
GRANT INSERT ON  [dbo].[tblIsConsortiumClaim] TO [power_user]
GRANT DELETE ON  [dbo].[tblIsConsortiumClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[tblIsConsortiumClaim] TO [power_user]
GO
