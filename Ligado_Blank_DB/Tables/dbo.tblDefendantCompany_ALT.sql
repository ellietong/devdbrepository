CREATE TABLE [dbo].[tblDefendantCompany_ALT]
(
[DefendantCompanyID] [int] NOT NULL IDENTITY(1, 1),
[DefendantCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefendantCompany_ALT] ADD CONSTRAINT [PK_mBtblDefendantCompany] PRIMARY KEY CLUSTERED  ([DefendantCompanyID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompany_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefendantCompany_ALT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompany_ALT] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDefendantCompany_ALT] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblDefendantCompany_ALT] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblDefendantCompany_ALT] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblDefendantCompany_ALT] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompany_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefendantCompany_ALT] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefendantCompany_ALT] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefendantCompany_ALT] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefendantCompany_ALT] TO [power_user]
GO
