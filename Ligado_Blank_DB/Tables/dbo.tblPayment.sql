CREATE TABLE [dbo].[tblPayment]
(
[PaymentID] [bigint] NOT NULL IDENTITY(1, 1),
[LedgerID] [int] NULL,
[Payment] [money] NULL,
[PaymentDate] [datetime] NULL,
[PaymentNote] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPayment] ADD CONSTRAINT [PK_tblPayment] PRIMARY KEY CLUSTERED  ([PaymentID]) ON [PRIMARY]
GO
