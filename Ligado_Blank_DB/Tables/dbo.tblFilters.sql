CREATE TABLE [dbo].[tblFilters]
(
[FilterID] [int] NOT NULL IDENTITY(1, 1),
[Filter] [char] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFilters] ADD CONSTRAINT [PK_TblFilters] PRIMARY KEY CLUSTERED  ([FilterID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblFilters] TO [base_user]
GRANT SELECT ON  [dbo].[tblFilters] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblFilters] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblFilters] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblFilters] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblFilters] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblFilters] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblFilters] TO [power_user]
GRANT SELECT ON  [dbo].[tblFilters] TO [power_user]
GRANT INSERT ON  [dbo].[tblFilters] TO [power_user]
GRANT DELETE ON  [dbo].[tblFilters] TO [power_user]
GRANT UPDATE ON  [dbo].[tblFilters] TO [power_user]
GRANT SELECT ON  [dbo].[tblFilters] TO [Reports_User]
GRANT INSERT ON  [dbo].[tblFilters] TO [Reports_User]
GRANT DELETE ON  [dbo].[tblFilters] TO [Reports_User]
GRANT UPDATE ON  [dbo].[tblFilters] TO [Reports_User]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', 'COLUMN', N'Filter'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', 'COLUMN', N'Filter'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', 'COLUMN', N'Filter'
GO
DECLARE @xp smallint
SELECT @xp=3150
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', 'COLUMN', N'Filter'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', 'COLUMN', N'Filter'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', 'COLUMN', N'FilterID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', 'COLUMN', N'FilterID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', 'COLUMN', N'FilterID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', 'COLUMN', N'FilterID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblFilters', 'COLUMN', N'FilterID'
GO
