CREATE TABLE [dbo].[tblScenarioProvisions]
(
[ScenarioProvisionID] [int] NOT NULL IDENTITY(1, 1),
[ScenarioID] [int] NOT NULL,
[ProvisionLabelID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblScenarioProvisions] ADD CONSTRAINT [PK_tblScenarioProvision] PRIMARY KEY CLUSTERED  ([ScenarioProvisionID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblScenarioProvisions] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblScenarioProvisions] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblScenarioProvisions] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblScenarioProvisions] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblScenarioProvisions] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblScenarioProvisions] TO [base_user]
GRANT SELECT ON  [dbo].[tblScenarioProvisions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblScenarioProvisions] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblScenarioProvisions] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblScenarioProvisions] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblScenarioProvisions] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblScenarioProvisions] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblScenarioProvisions] TO [power_user]
GRANT SELECT ON  [dbo].[tblScenarioProvisions] TO [power_user]
GRANT INSERT ON  [dbo].[tblScenarioProvisions] TO [power_user]
GRANT DELETE ON  [dbo].[tblScenarioProvisions] TO [power_user]
GRANT UPDATE ON  [dbo].[tblScenarioProvisions] TO [power_user]
GO
