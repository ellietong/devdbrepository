CREATE TABLE [dbo].[NavLegacyIDs]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[cls] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NavLegacyIDs] ADD CONSTRAINT [PK__NavLegac__3213E83F74100195] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[NavLegacyIDs] TO [base_user]
GRANT SELECT ON  [dbo].[NavLegacyIDs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[NavLegacyIDs] TO [Ligado_User]
GRANT SELECT ON  [dbo].[NavLegacyIDs] TO [Ligado_User]
GRANT INSERT ON  [dbo].[NavLegacyIDs] TO [Ligado_User]
GRANT DELETE ON  [dbo].[NavLegacyIDs] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[NavLegacyIDs] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[NavLegacyIDs] TO [power_user]
GRANT SELECT ON  [dbo].[NavLegacyIDs] TO [power_user]
GRANT INSERT ON  [dbo].[NavLegacyIDs] TO [power_user]
GRANT DELETE ON  [dbo].[NavLegacyIDs] TO [power_user]
GRANT UPDATE ON  [dbo].[NavLegacyIDs] TO [power_user]
GO
