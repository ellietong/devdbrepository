CREATE TABLE [dbo].[tblZipQueueDocument]
(
[ZipQueueDocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[ZipQueueID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Key] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblZipQueueDocument] ADD CONSTRAINT [PK_tblZipQueueDocument] PRIMARY KEY CLUSTERED  ([ZipQueueDocumentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblZipQueueDocument] TO [base_user]
GRANT SELECT ON  [dbo].[tblZipQueueDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblZipQueueDocument] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblZipQueueDocument] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblZipQueueDocument] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblZipQueueDocument] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblZipQueueDocument] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblZipQueueDocument] TO [power_user]
GRANT SELECT ON  [dbo].[tblZipQueueDocument] TO [power_user]
GRANT INSERT ON  [dbo].[tblZipQueueDocument] TO [power_user]
GRANT DELETE ON  [dbo].[tblZipQueueDocument] TO [power_user]
GRANT UPDATE ON  [dbo].[tblZipQueueDocument] TO [power_user]
GO
