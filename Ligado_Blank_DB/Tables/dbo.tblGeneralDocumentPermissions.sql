CREATE TABLE [dbo].[tblGeneralDocumentPermissions]
(
[GeneralDocumentPermissionsID] [bigint] NOT NULL IDENTITY(1, 1),
[GeneralDocumentGroupID] [int] NULL,
[PersonID] [int] NULL,
[GoogleObjectID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CanAdd] [bit] NOT NULL CONSTRAINT [DF_tblGeneralDocumentPermissions_CanAdd] DEFAULT ((0)),
[CanEdit] [bit] NOT NULL CONSTRAINT [DF_tblGeneralDocumentPermissions_CanEdit] DEFAULT ((0)),
[CanDelete] [bit] NOT NULL CONSTRAINT [DF_tblGeneralDocumentPermissions_CanDelete] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblGeneralDocumentPermissions] ADD CONSTRAINT [PK_tblGeneralDocumentPermissions] PRIMARY KEY CLUSTERED  ([GeneralDocumentPermissionsID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblGeneralDocumentPermissions_tblGeneralDocumentGroup] ON [dbo].[tblGeneralDocumentPermissions] ([GeneralDocumentGroupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblGeneralDocumentPermissions] ADD CONSTRAINT [FK_tblGeneralDocumentPermissions_tblGeneralDocumentGroup] FOREIGN KEY ([GeneralDocumentGroupID]) REFERENCES [dbo].[tblGeneralDocumentGroup] ([GeneralDocumentGroupID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblGeneralDocumentPermissions] TO [base_user]
GRANT SELECT ON  [dbo].[tblGeneralDocumentPermissions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblGeneralDocumentPermissions] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblGeneralDocumentPermissions] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblGeneralDocumentPermissions] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblGeneralDocumentPermissions] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblGeneralDocumentPermissions] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblGeneralDocumentPermissions] TO [power_user]
GRANT SELECT ON  [dbo].[tblGeneralDocumentPermissions] TO [power_user]
GRANT INSERT ON  [dbo].[tblGeneralDocumentPermissions] TO [power_user]
GRANT DELETE ON  [dbo].[tblGeneralDocumentPermissions] TO [power_user]
GRANT UPDATE ON  [dbo].[tblGeneralDocumentPermissions] TO [power_user]
GO
