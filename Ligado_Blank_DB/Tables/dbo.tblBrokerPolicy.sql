CREATE TABLE [dbo].[tblBrokerPolicy]
(
[BrokerPolicyID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NOT NULL,
[BrokerID] [int] NOT NULL,
[BrokerTypeID] [int] NOT NULL,
[BrokerRefID] [int] NULL,
[PolicyBrokerNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBrokerPolicy] ADD CONSTRAINT [PK_tblBrokerPolicy] PRIMARY KEY CLUSTERED  ([BrokerPolicyID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblBrokerPolicy_tblPolicy] ON [dbo].[tblBrokerPolicy] ([PolicyID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBrokerPolicy] ADD CONSTRAINT [FK_tblBrokerPolicy_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy] ([PolicyID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerPolicy] TO [base_user]
GRANT SELECT ON  [dbo].[tblBrokerPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerPolicy] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBrokerPolicy] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBrokerPolicy] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBrokerPolicy] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBrokerPolicy] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerPolicy] TO [power_user]
GRANT SELECT ON  [dbo].[tblBrokerPolicy] TO [power_user]
GRANT INSERT ON  [dbo].[tblBrokerPolicy] TO [power_user]
GRANT DELETE ON  [dbo].[tblBrokerPolicy] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBrokerPolicy] TO [power_user]
GRANT SELECT ON  [dbo].[tblBrokerPolicy] TO [Subscription_User]
GRANT INSERT ON  [dbo].[tblBrokerPolicy] TO [Subscription_User]
GRANT DELETE ON  [dbo].[tblBrokerPolicy] TO [Subscription_User]
GRANT UPDATE ON  [dbo].[tblBrokerPolicy] TO [Subscription_User]
GO
