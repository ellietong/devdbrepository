CREATE TABLE [dbo].[tblDocuments]
(
[DocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantDocumentTypeID] [int] NULL,
[DocumentSourceID] [int] NULL,
[HistoricalDocumentID] [bigint] NULL,
[ComplaintID] [bigint] NULL,
[MatterID] [int] NULL,
[DocumentTitle] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmazonKey] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NOT NULL,
[CreatedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateModified] [datetime] NOT NULL,
[ModifiedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Accepted] [bit] NULL,
[DateAccepted] [datetime] NULL,
[AcceptedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileSizeBytes] [bigint] NULL,
[UploadCompleted] [bit] NULL,
[UploadCompletedOn] [datetime] NULL,
[DismissalForQC] [bit] NULL,
[DismissalQCIssue] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDocuments] ADD CONSTRAINT [PK_tblDocuments_Marley] PRIMARY KEY CLUSTERED  ([DocumentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDocuments] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblDocuments] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblDocuments] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[tblDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblDocuments] TO [power_user]
GRANT DELETE ON  [dbo].[tblDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [power_user]
GO
CREATE FULLTEXT INDEX ON [dbo].[tblDocuments] KEY INDEX [PK_tblDocuments_Marley] ON [DocumentTitle]
GO
ALTER FULLTEXT INDEX ON [dbo].[tblDocuments] ADD ([DocumentTitle] LANGUAGE 1033)
GO
