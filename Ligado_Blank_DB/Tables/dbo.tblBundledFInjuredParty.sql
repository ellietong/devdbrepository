CREATE TABLE [dbo].[tblBundledFInjuredParty]
(
[Injured_Party_Id] [float] NULL,
[First_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Middle_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SS_Nbr] [float] NULL,
[Date of Birth] [datetime] NULL,
[Date of Death] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBundledFInjuredParty] TO [base_user]
GRANT SELECT ON  [dbo].[tblBundledFInjuredParty] TO [base_user]
GRANT SELECT ON  [dbo].[tblBundledFInjuredParty] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblBundledFInjuredParty] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblBundledFInjuredParty] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBundledFInjuredParty] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBundledFInjuredParty] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBundledFInjuredParty] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBundledFInjuredParty] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBundledFInjuredParty] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBundledFInjuredParty] TO [power_user]
GRANT SELECT ON  [dbo].[tblBundledFInjuredParty] TO [power_user]
GRANT INSERT ON  [dbo].[tblBundledFInjuredParty] TO [power_user]
GRANT DELETE ON  [dbo].[tblBundledFInjuredParty] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBundledFInjuredParty] TO [power_user]
GO
