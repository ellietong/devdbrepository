CREATE TABLE [dbo].[m_tblLegalBillInvoices]
(
[InvoiceID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[NavLegacyID] [bigint] NULL,
[MatterID] [int] NOT NULL,
[DefenseCounselAddressID] [int] NULL,
[LegalBillID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavFirmID] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Firm] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LBMatter] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LBInvoiceNumber] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalInvoiceNumber] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [datetime] NULL,
[SubmittedDate] [datetime] NULL,
[DanaApprovalDate] [datetime] NULL,
[LBApprovalDate] [datetime] NULL,
[SubmittedForPayment] [datetime] NULL,
[FundsRequestDate] [datetime] NULL,
[StatusID] [int] NULL,
[OriginalTotal] [money] NULL,
[OriginalFee] [money] NULL,
[OriginalDisbursement] [money] NULL,
[AdjustedTotal] [money] NULL,
[AdjustedFee] [money] NULL,
[AdjustedDisbursement] [money] NULL,
[AppealTotal] [money] NULL,
[AppealFee] [money] NULL,
[AppealCost] [money] NULL,
[InvoiceNotes] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppealFlag] [bit] NULL,
[NavPaidInvoice] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblLegalBillInvoices] ADD CONSTRAINT [PK_m_tblLegalBillInvoices] PRIMARY KEY CLUSTERED  ([InvoiceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_m_tbleLegalBillInvoices_CPID] ON [dbo].[m_tblLegalBillInvoices] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblLegalBillInvoices_DefenseCounselAddressID_MatterID_StatusID] ON [dbo].[m_tblLegalBillInvoices] ([DefenseCounselAddressID], [MatterID], [StatusID]) INCLUDE ([AdjustedTotal], [AppealFlag], [AppealTotal], [DanaApprovalDate], [InvoiceID], [OriginalInvoiceNumber], [OriginalTotal], [Period], [SubmittedDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_m_tblLegalBillInvoices_DefenseCounselAddressID_StatusID] ON [dbo].[m_tblLegalBillInvoices] ([DefenseCounselAddressID], [StatusID]) INCLUDE ([AdjustedTotal], [AppealFlag], [AppealTotal], [DanaApprovalDate], [InvoiceID], [MatterID], [NavPaidInvoice], [OriginalInvoiceNumber], [OriginalTotal], [Period], [SubmittedDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_LegalBillInvoices_MatterID_StatusID] ON [dbo].[m_tblLegalBillInvoices] ([MatterID], [StatusID]) INCLUDE ([AdjustedTotal], [AppealFlag], [AppealTotal], [DanaApprovalDate], [DefenseCounselAddressID], [InvoiceID], [OriginalInvoiceNumber], [OriginalTotal], [Period], [SubmittedDate]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblLegalBillInvoices] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblLegalBillInvoices] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblLegalBillInvoices] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblLegalBillInvoices] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblLegalBillInvoices] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblLegalBillInvoices] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblLegalBillInvoices] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblLegalBillInvoices] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblLegalBillInvoices] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblLegalBillInvoices] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblLegalBillInvoices] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblLegalBillInvoices] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblLegalBillInvoices] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblLegalBillInvoices] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblLegalBillInvoices] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblLegalBillInvoices] TO [power_user]
GO
