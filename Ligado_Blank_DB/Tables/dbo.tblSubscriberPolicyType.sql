CREATE TABLE [dbo].[tblSubscriberPolicyType]
(
[PolicyTypeID] [int] NOT NULL IDENTITY(1, 1),
[PolicyType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriberPolicyType] ADD CONSTRAINT [PK_tblSubscriberPolicyType] PRIMARY KEY CLUSTERED  ([PolicyTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriberPolicyType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriberPolicyType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriberPolicyType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSubscriberPolicyType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSubscriberPolicyType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSubscriberPolicyType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSubscriberPolicyType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriberPolicyType] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriberPolicyType] TO [power_user]
GRANT INSERT ON  [dbo].[tblSubscriberPolicyType] TO [power_user]
GRANT DELETE ON  [dbo].[tblSubscriberPolicyType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSubscriberPolicyType] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriberPolicyType] TO [Subscription_User]
GRANT INSERT ON  [dbo].[tblSubscriberPolicyType] TO [Subscription_User]
GRANT DELETE ON  [dbo].[tblSubscriberPolicyType] TO [Subscription_User]
GRANT UPDATE ON  [dbo].[tblSubscriberPolicyType] TO [Subscription_User]
GO
