CREATE TABLE [dbo].[tblClaimLawsuitStatus_OLD]
(
[ClaimLawsuitStatusID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitStatusGroupID] [int] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatus_OLD] ADD CONSTRAINT [PK_mBtblClaimLawsuitStatus] PRIMARY KEY CLUSTERED  ([ClaimLawsuitStatusID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatus_OLD] TO [power_user]
GO
