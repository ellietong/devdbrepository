CREATE TABLE [dbo].[tblAcceptanceType]
(
[AcceptanceTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClaimantDocumentTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAcceptanceType] ADD CONSTRAINT [PK_tblAcceptanceType] PRIMARY KEY CLUSTERED  ([AcceptanceTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAcceptanceType] TO [base_user]
GRANT SELECT ON  [dbo].[tblAcceptanceType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAcceptanceType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblAcceptanceType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblAcceptanceType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblAcceptanceType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblAcceptanceType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAcceptanceType] TO [power_user]
GRANT SELECT ON  [dbo].[tblAcceptanceType] TO [power_user]
GRANT INSERT ON  [dbo].[tblAcceptanceType] TO [power_user]
GRANT DELETE ON  [dbo].[tblAcceptanceType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAcceptanceType] TO [power_user]
GO
