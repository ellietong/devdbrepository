SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE FUNCTION [dbo].[fnEarliestExposure]
(@ClaimantPersonalInfoID int)

RETURNS datetime
AS
BEGIN

--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 6

DECLARE @EarliestExposure Date

/**SECTION OF CODE TO USE IF YOU DO WANT TO CONSIDER PRODUCTS IN THE EXPOSURE CALCULATION. IUNA AND NSI FOR EXAMPLE*

IF (SELECT COUNT(ClaimantProductID)	From tblclaimantemployment ce inner join tblclaimantproduct cp on cp.claimantemploymentid = ce.claimantemploymentID where ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID) = 0
BEGIN 

SET @EarliestExposure = (select Min(EmpStartDate) as EarliestExposureDate
from tblClaimantEmployment
where ClaimantPersonalInfoID=@ClaimantPersonalInfoID)

END
ELSE 
BEGIN

SET @EarliestExposure = (select MIN(EarliestExposureDate) as EarliestExposureDate
From
(SELECT case when coalesce(min(cp.StartDate), '1/1/1900') < ce.EmpStartDate then ce.EmpStartDate else MIN(cp.startdate) end as EarliestExposureDate
From tblclaimantemployment ce 
inner join tblclaimantproduct cp on cp.claimantemploymentid = ce.claimantemploymentID 
where ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
group by ce.EmpStartDate) tblEmploymentExposure)

end
*/

/**SECTION OF CODE TO USE IF YOU DO NOT WANT TO CONSIDER PRODUCTS IN THE EXPOSURE CALCULATION. DANA FOR EXAMPLE**/

SET @EarliestExposure = (select Min(EmpStartDate) as EarliestExposureDate
from tblClaimantEmployment
where ClaimantPersonalInfoID=@ClaimantPersonalInfoID)

Return convert(nvarchar(10),@EarliestExposure,101)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnEarliestExposure] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnEarliestExposure] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnEarliestExposure] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnEarliestExposure] TO [power_user]
GO
