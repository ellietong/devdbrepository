SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmAllocation]
AS
SET NOCOUNT ON
SELECT *
FROM tblAllocation
ORDER BY AllocationID
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocation] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmAllocation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocation] TO [power_user]
GO
