SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryExternalEmailNewComplaints_DanaVP] 
AS
BEGIN
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)

Declare @Today4PM datetime
Declare @Yesterday4PM datetime
SET @Yesterday4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-1))+' 16:00:00.000')
SET @Today4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 16:00:00.000')


SET @Subject = 	
'Dana Companies, LLC: New Complaints Uploaded ' + CONVERT(varchar, getdate(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (DocID int, docTitle nvarchar(500), FirmID int, lName nvarchar(50), 
	
fName nvarchar(50), ClaimantPersonalInfoID bigint, ServiceDate date)
INSERT INTO @NewComplaintsTable
SELECT d.DocumentID, '<a href="https://www.kcicextranet.com/clients/dana/cp/Pages/DocumentProfile.aspx?ItemId='+COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle, 
	
FirmID, cpi.LastName, cpi.FirstName, cpi.ClaimantPersonalInfoID, max(ServiceDate)
	

FROM tblDocuments d
	
INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	
INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	
left join tblclaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	
left join tblclaimlawsuit cl on cl.claimid = c.claimid
	
left join tbllawsuit l on l.lawsuitid =cl.lawsuitid
	
left join tblAllegation a on a.ClaimLawsuitID=cl.ClaimLawsuitID
WHERE mcd.QCDate BETWEEN @Yesterday4PM AND @Today4PM 
	
AND detailDocumentTypeID IN (8,4,33)
	
AND RIGHT(CreatedBy, 11) = 'kcicllc.com'
	
AND FirmID IS NOT NULL
	
AND (c.MatterID <> 4 OR a.AllegationTypeID in (2,3))
	

GROUP BY d.documentID, d.DocumentTitle,
	
FirmID, LastName, FirstName, cpi.ClaimantPersonalInfoID
	


DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar(max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar(max), @ClaimantID bigint, @ClaimantList nvarchar(max)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for
SELECT DocID, COALESCE(Lname, '') + ', ' +  COALESCE(Fname, ''), claimantpersonalInfoID FROM @NewComplaintsTable
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
WHILE @@FETCH_STATUS = 0
BEGIN

IF(LEN(@ClaimantList)=0 OR @ClaimantList IS NULL)
BEGIN
	
SET @ClaimantList = '<ul> <a href="https://www.kcicextranet.com/clients/dana/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+'">'  + @Claimant + coalesce(' ('+ CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
END
ELSE
BEGIN
	
SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="https://www.kcicextranet.com/clients/dana/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+'">' + COALESCE(@Claimant, '') + coalesce(' ('+ CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
PRINT @@FETCH_STATUS
IF @DocID <> @LastDocID OR @@FETCH_STATUS <> 0
BEGIN
	
INSERT INTO @ClaimantPerDoc
	
SELECT @LastDocID, @ClaimantList
	
SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints
	


--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'



--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

DECLARE @TDStyle nvarchar(max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

IF (Select COUNT(docTitle) from @NewComplaintsTable) >0 
begin
set @BodyMessage = @BodyMessage + '<H4>The following Warner Electric, Wichita Clutch or Premises complaints have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	
	
N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants (ClaimantID)</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Served*</th>' +
	
	
CAST ( (
SELECT	@TDStyle AS 'td/@style', (SELECT docTitle) as td, '',
		@TDStyle AS 'td/@style', (SELECT cl.ClaimantList) as td, '',
		@TDStyle AS 'td/@style', (SELECT max(ServiceDate)) as td, ''
FROM @NewComplaintsTable C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
GROUP BY docTitle, cl.ClaimantList
for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	
N'</table>' +  N'<br/>'	
	

end
ELSE
begin
Set @BodyMessage = @BodyMessage + '<H4>No Complaints have been uploaded in the last 24 hours. </H4>'
end



--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/dana/cp/Pages/Documents.aspx">here</A> to access the document(s).</b><br><br>'

--Service Date Assumption 

IF (Select COUNT(docTitle) from @NewComplaintsTable) >0 
begin
SET @BodyMessage = @BodyMessage + '<br>*Note: Date served is the latest service date for claimants'' lawsuits.<br><br>'
end

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


IF (Select COUNT(docTitle) from @NewComplaintsTable) >0 
begin
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The DanaProfile sends from Dana@kcicllc.com
@profile_name='DanaProfile',
@recipients = 'charlotte.murd@danacollc.com',
@copy_recipients = 'dana@kcicllc.com',
--@recipients = 'amanp@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
end

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints_DanaVP] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints_DanaVP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints_DanaVP] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints_DanaVP] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints_DanaVP] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints_DanaVP] TO [power_user]
GO
