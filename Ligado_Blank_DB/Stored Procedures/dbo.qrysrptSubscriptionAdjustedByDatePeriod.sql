SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qrysrptSubscriptionAdjustedByDatePeriod] (@PolicyDateID int)
AS
SET NOCOUNT ON

-- Declare the temporary fields used to track loop progress and percentages
DECLARE @CountSubRecords int
DECLARE @CounterSubRecords int
DECLARE @CountDates int
DECLARE @CounterDates int
DECLARE @CurrentSubRecord int
DECLARE @CurrentPolicy int
DECLARE @AdjustmentActual float
DECLARE @AdjustedSubscriptionActual float
DECLARE @AdjustmentAllocation float
DECLARE @AdjustedSubscriptionAllocation float

-- Declare standing variables for the policy dates
DECLARE @SubStartDate date
DECLARE @SubEndDate date
--DECLARE @PolicyDateID int
--SET @PolicyDateID = 2
DECLARE @ActualPStartDate date
DECLARE @ActualPEndDate date
DECLARE @PStartDate date
DECLARE @PEndDate date
DECLARE @CompareStart date
DECLARE @CompareEnd date

-- Declare standing variables for calculating adjusted percent
DECLARE @PercentSubscriber float
DECLARE @PercentAgency float
DECLARE @PercentGroup float

-- Declare the four temporary tables for policy dates and adjustments: 
--		one to serve as the final summary of dates and associate adjustments, by policy,
--		one to summarize all policy IDs that have subscription and their subscription dates,
--		one to track one policy and all of its policy and allocation dates,
--		one to track one policy and one set of dates
DECLARE @zSummaryOfPoliciesWithAdjustments TABLE (PolicyID int,
												  SubscriptionCoverageID int,
												  SubStartDate date,
												  SubEndDate date,
												  PolicyDateID int,
												  ActualPStartDate date, 
												  ActualPEndDate date,
												  AdjustmentActual float,
												  AdjustedSubscriptionActual float,
												  PStartDate date,
												  PEndDate date,
												  AdjustmentAllocation float,
												  AdjustedSubscriptionAllocation float)
DECLARE @zAllPoliciesWithSubscription TABLE (PolicyID int,
											 SubscriptionCoverageID int,
											 SubStartDate date,
											 SubEndDate date)
DECLARE @zOnePolicyAllDates TABLE (PolicyDateID int,
								   ActualPStartDate date, 
								   ActualPEndDate date,
								   PStartDate date, 
								   PEndDate date)
DECLARE @zOnePolicyOneDate TABLE (PolicyDateID int,
							      ActualPStartDate date, 
							      ActualPEndDate date,
								  PStartDate date, 
								  PEndDate date)

-- Populate the temporary table for all policy IDs
INSERT INTO @zAllPoliciesWithSubscription
	SELECT PolicyID, SubscriptionCoverageID,
		CAST(StartDate AS date) AS SubStartDate, CAST(EndDate AS date) AS SubEndDate
	FROM vSubscription
	WHERE PolicyID IN(SELECT DISTINCT PolicyID FROM tblPolicyDates WHERE PolicyDateID = @PolicyDateID)
	ORDER BY SubscriptionCoverageID

-- Prepare the temporary fields for the loop
SET @CountSubRecords = (SELECT COUNT(SubscriptionCoverageID) FROM @zAllPoliciesWithSubscription)
SET @CounterSubRecords = 0

-- Loop through the temporary table of policy records, one records at a time
WHILE @CounterSubRecords < @CountSubRecords
BEGIN
	-- Prepare the temporary fields to track the current record for the outer loop
	SET @CurrentSubRecord = (SELECT MIN(SubscriptionCoverageID) FROM @zAllPoliciesWithSubscription)
	SET @CurrentPolicy = (SELECT PolicyID FROM @zAllPoliciesWithSubscription WHERE SubscriptionCoverageID = @CurrentSubRecord)
	SET @SubStartDate = (SELECT SubStartDate FROM @zAllPoliciesWithSubscription WHERE SubscriptionCoverageID = @CurrentSubRecord)
	SET @SubEndDate = (SELECT SubEndDate FROM @zAllPoliciesWithSubscription WHERE SubscriptionCoverageID = @CurrentSubRecord)
	
	-- Populate the temporary table for all policy date IDs associated with the policy
	INSERT INTO @zOnePolicyAllDates
		SELECT PolicyDateID, 
			CAST(ActualPStartDate AS date) AS ActualPStartDate, CAST(ActualPEndDate AS date) AS ActualPEndDate,
			CAST(PStartDate AS date) AS PStartDate, CAST(PEndDate AS date) AS PEndDate
		FROM tblPolicyDates
		WHERE PolicyID = @CurrentPolicy AND PolicyDateID = @PolicyDateID
		ORDER BY PolicyDateID

	-- Prepare the temporary fields for the loop
	SET @AdjustmentActual = 0.000
	SET @AdjustmentAllocation = 0.000
	SET @CountDates = (SELECT COUNT(PolicyDateID) FROM @zOnePolicyAllDates)
	SET @CounterDates = 0

	-- Loop through the temporary table of policy date records, one records at a time
	WHILE @CounterDates < @CountDates
	BEGIN
		-- Grab a single set of policy dates from the temporary table
		INSERT INTO @zOnePolicyOneDate
			SELECT TOP 1 * 
			FROM @zOnePolicyAllDates
			
		-- Parse out the information from the record
		--SET @PolicyDateID = (SELECT PolicyDateID FROM @zOnePolicyOneDate)
		SET @ActualPStartDate = (SELECT CAST(ActualPStartDate AS date) FROM @zOnePolicyOneDate)
		SET @ActualPEndDate = (SELECT CAST(ActualPEndDate AS date) FROM @zOnePolicyOneDate)
		SET @PStartDate = (SELECT CAST(PStartDate AS date) FROM @zOnePolicyOneDate)
		SET @PEndDate = (SELECT CAST(PEndDate AS date) FROM @zOnePolicyOneDate)

		-- Check if the actual policy dates fall fully with the subscription dates
		IF ((COALESCE(@SubStartDate, @ActualPStartDate) <= @ActualPStartDate) AND (COALESCE(@SubEndDate, @ActualPEndDate) >= @ActualPEndDate)) BEGIN
			-- If so, set the percentage for this date record to 100%
			SET @AdjustmentActual = 1.000
		END
		-- Check if the subscription dates are totally outside the actual policy dates
		ELSE IF ((COALESCE(@SubStartDate, @ActualPStartDate) > @ActualPEndDate) OR (COALESCE(@SubEndDate, @ActualPEndDate) < @ActualPStartDate)) BEGIN
			-- If so, set the percentage for this date record to 0% (this should be redundant to the default value)
			SET @AdjustmentActual = 0.000
		END
		-- Otherwise, proportion the percentage based on the number of subscription days falling within actual policy dates
		ELSE BEGIN
			IF COALESCE(@SubStartDate, @ActualPStartDate) > @ActualPStartDate BEGIN SET @CompareStart = @SubStartDate	 END
				ELSE														  BEGIN SET @CompareStart = @ActualPStartDate END
			IF COALESCE(@SubEndDate, @ActualPEndDate) < @ActualPEndDate BEGIN SET @CompareEnd   = @SubEndDate       END
				ELSE													BEGIN SET @CompareEnd   = @ActualPEndDate   END
			SET @AdjustmentActual = ((CAST(DATEDIFF(day, @CompareStart, @CompareEnd) AS float)) / 
									 (CAST(DATEDIFF(day, @ActualPStartDate, @ActualPEndDate) AS float)))
		END
		
		-- Check if the policy allocation dates fall fully with the subscription dates
		IF ((COALESCE(@SubStartDate, @PStartDate) <= @PStartDate) AND (COALESCE(@SubEndDate, @PEndDate) >= @PEndDate)) BEGIN
			-- If so, set the percentage for this date record to 100%
			SET @AdjustmentAllocation = 1.000
		END
		-- Check if the subscription dates are totally outside the policy allocation dates
		ELSE IF ((COALESCE(@SubStartDate, @PStartDate) > @PEndDate) OR (COALESCE(@SubEndDate, @PEndDate) < @PStartDate)) BEGIN
			-- If so, set the percentage for this date record to 0% (this should be redundant to the default value)
			SET @AdjustmentAllocation = 0.000
		END
		-- Otherwise, proportion the percentage based on the number of subscription days falling within policy allocation dates
		ELSE BEGIN
			IF COALESCE(@SubStartDate, @PStartDate) > @PStartDate BEGIN SET @CompareStart = @SubStartDate END
				ELSE											  BEGIN SET @CompareStart = @PStartDate   END
			IF COALESCE(@SubEndDate, @PEndDate)   < @PEndDate BEGIN SET @CompareEnd   = @SubEndDate   END
				ELSE										  BEGIN SET @CompareEnd   = @PEndDate   END
			SET @AdjustmentAllocation = ((CAST(DATEDIFF(day, @CompareStart, @CompareEnd) AS float)) / 
										 (CAST(DATEDIFF(day, @PStartDate, @PEndDate) AS float)))
		END
		
		-- Update the temporary values used to calculate adjusted percent
		SET @PercentSubscriber = (SELECT SubscriptionCompanyPercent FROM vSubscription WHERE SubscriptionCoverageID = @CurrentSubRecord)
		SET @PercentAgency = (SELECT AgencyPercentOfSection FROM vSubscription WHERE SubscriptionCoverageID = @CurrentSubRecord)
		SET @PercentGroup = (SELECT GroupWithinAgencyPercent FROM vSubscription WHERE SubscriptionCoverageID = @CurrentSubRecord)
		
		-- Populate the summary table with the calculated results
		INSERT INTO @zSummaryOfPoliciesWithAdjustments
			SELECT @CurrentPolicy, @CurrentSubRecord, @SubStartDate, @SubEndDate,
				@PolicyDateID, @ActualPStartDate, @ActualPEndDate, @AdjustmentActual,
				(@PercentSubscriber * @PercentAgency * @PercentGroup * @AdjustmentActual) AS AdjustedSubscriptionActual,
				@PStartDate, @PEndDate, @AdjustmentAllocation,
				(@PercentSubscriber * @PercentAgency * @PercentGroup * @AdjustmentAllocation) AS AdjustedSubscriptionAllocation

		
		-- Clear the reviewed record from the temporary table being used to track all date records
		DELETE FROM @zOnePolicyAllDates WHERE PolicyDateID = @PolicyDateID
		
		-- Clear the reviewed record from the temporary table used to track a single date record
		DELETE FROM @zOnePolicyOneDate
		
		-- Increment the counter and reset the temporary percentage field
		SET @CounterDates = @CounterDates + 1
		SET @AdjustmentActual = 0.000
		SET @AdjustmentAllocation = 0.000
	END
	
	-- Clear the reviewed record(s) from the temporary table used to track policies
	DELETE FROM @zAllPoliciesWithSubscription WHERE SubscriptionCoverageID = @CurrentSubRecord
	
	-- Increment the counter and reset the temporary percentage field
	SET @CounterSubRecords = @CounterSubRecords + 1
	SET @AdjustmentActual = 0.000
	SET @AdjustmentAllocation = 0.000
END

SELECT s1.PolicyDateID, s1.ActualPStartDate, s1.ActualPEndDate, s1.AdjustmentActual, s1.AdjustedSubscriptionActual,
	s1.PStartDate, s1.PEndDate, s1.AdjustmentAllocation, s1.AdjustedSubscriptionAllocation,
	s2.* 
FROM @zSummaryOfPoliciesWithAdjustments s1 
	INNER JOIN vSubscription s2 ON s1.SubscriptionCoverageID = s2.SubscriptionCoverageID

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSubscriptionAdjustedByDatePeriod] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptSubscriptionAdjustedByDatePeriod] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSubscriptionAdjustedByDatePeriod] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysrptSubscriptionAdjustedByDatePeriod] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSubscriptionAdjustedByDatePeriod] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptSubscriptionAdjustedByDatePeriod] TO [power_user]
GRANT ALTER ON  [dbo].[qrysrptSubscriptionAdjustedByDatePeriod] TO [Subscription_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSubscriptionAdjustedByDatePeriod] TO [Subscription_User]
GRANT EXECUTE ON  [dbo].[qrysrptSubscriptionAdjustedByDatePeriod] TO [Subscription_User]
GO
