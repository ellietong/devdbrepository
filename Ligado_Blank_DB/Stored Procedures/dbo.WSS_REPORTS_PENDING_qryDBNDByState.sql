SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qryDBNDByState] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null,@EndDate datetime=null, @emailaddress nvarchar(200) = null)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

    -- Insert statements for procedure here
select TOP 10 FullNameState As State, COUNT(distinct(cl.ClaimLawsuitID)) as Count
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s ON l.StateID=s.StateID
where	ClaimLawsuitStatusID in (17,34, 37) 
		and  IsPrimaryStatus=1
group by FullNameState
order by COUNT(distinct(cl.ClaimLawsuitID))  desc, FullNameState

END





GO
