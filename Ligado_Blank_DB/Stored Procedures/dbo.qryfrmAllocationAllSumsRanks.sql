SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
create procedure [dbo].[qryfrmAllocationAllSumsRanks]
as

set nocount on

select allocationid, policyid, rank_indem, rank_def
from tblallocationallsumsprefs
order by allocationid, policyid

return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationAllSumsRanks] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationAllSumsRanks] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationAllSumsRanks] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationAllSumsRanks] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationAllSumsRanks] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationAllSumsRanks] TO [power_user]
GO
