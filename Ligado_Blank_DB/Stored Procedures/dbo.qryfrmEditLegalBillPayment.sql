SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create procedure [dbo].[qryfrmEditLegalBillPayment] (@CheckID int)
AS
SET NOCOUNT ON

select * from tblPaymentLegalBill
where CheckID = @CheckID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditLegalBillPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmEditLegalBillPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditLegalBillPayment] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditLegalBillPayment] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditLegalBillPayment] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditLegalBillPayment] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditLegalBillPayment] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditLegalBillPayment] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditLegalBillPayment] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditLegalBillPayment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditLegalBillPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmEditLegalBillPayment] TO [power_user]
GO
