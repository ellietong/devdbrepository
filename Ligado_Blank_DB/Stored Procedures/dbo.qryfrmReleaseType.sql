SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Proc [dbo].[qryfrmReleaseType]
as

Select ReleaseTypeID, ReleaseType
From tblReleaseType
Order By ReleaseTYpe

Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmReleaseType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmReleaseType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmReleaseType] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmReleaseType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmReleaseType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmReleaseType] TO [power_user]
GO
