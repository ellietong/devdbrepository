SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Procedure [dbo].[qryfrmMiscCashActivity] (@MiscCashActivityID int)

As
Set NoCount On

Select * from tblMiscCashActivityApproval Where MiscCashActivityID = @MiscCashActivityID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMiscCashActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmMiscCashActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMiscCashActivity] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmMiscCashActivity] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMiscCashActivity] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmMiscCashActivity] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMiscCashActivity] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmMiscCashActivity] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMiscCashActivity] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmMiscCashActivity] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMiscCashActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmMiscCashActivity] TO [power_user]
GO
