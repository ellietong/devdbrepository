SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qryDBNDByDisease] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null,@EndDate datetime=null, @emailaddress nvarchar(200) = null)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

    -- Insert statements for procedure here
select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end as HighLevelDisease, 
COUNT(distinct(cl.ClaimLawsuitID)) as Count
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join (select * from tblClaimDisease where PrimaryDisease =1) cd ON c.ClaimID=cd.ClaimID
left join tblDisease d ON d.DiseaseID=cd.DiseaseID
left join tblHighLevelDisease hld ON hld.HighLevelDiseaseID=d.HighLevelDiseaseID
where	
		 ClaimLawsuitStatusID in (17,34, 37) 
		and  IsPrimaryStatus=1
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end 
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END





GO
