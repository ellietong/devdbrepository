SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryInsertSettlementConfirmationContact]
(@PersonID bigint, @SettlementGroupID bigint)
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO tblSettlementConfirmationContact (PersonID, SettlementGroupID)
    SELECT @PersonID, @SettlementGroupID
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryInsertSettlementConfirmationContact] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryInsertSettlementConfirmationContact] TO [Ligado_User]
GO
