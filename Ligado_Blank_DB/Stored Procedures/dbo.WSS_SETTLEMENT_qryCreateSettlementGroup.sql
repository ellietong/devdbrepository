SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryCreateSettlementGroup]
	-- Add the parameters for the stored procedure here
(@SettlementGroupName nvarchar(500), @PaymentTermsID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO tblClaimSettlementGroup (SettlementGroup, PaymentTermsID)
	SELECT @SettlementGroupName, @PaymentTermsID
	
	SELECT MAX(SettlementGroupID) FROM tblClaimSettlementGroup
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryCreateSettlementGroup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryCreateSettlementGroup] TO [Ligado_User]
GO
