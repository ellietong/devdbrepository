SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmClaimantDocumentsEntry] (@ClaimantPersonalInfoID int)
as

Select cd.ClaimantDocumentID, 
	ClaimantPersonalInfoID,
	DocumentNumber,
	ClaimantDocumentTypeID,
	DocumentLink
From tblClaimantDocuments cd
	INNER JOIN tblHistoricalMultiClaimantDocuments hmcd ON cd.ClaimantDocumentID = hmcd.ClaimantDocumentID
Where ClaimantPersonalInfoID = @CLaimantPersonalInfoID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantDocumentsEntry] TO [power_user]
GO
