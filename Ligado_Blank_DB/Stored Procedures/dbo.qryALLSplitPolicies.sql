SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryALLSplitPolicies]
(@allocationID int = null, @strWhere varchar(4000) = null, @considerPrevExhaustion bit, @exhaustionTypeID int = NULL, @limitTypeID int = null)
AS
SET NOCOUNT ON

-- 2008-05-01 - DTO - modified to correctly track aggregate limits (previously aggregate limits would not be calculated correctly for the very first occurrence
-- 2007-07-16 - DTO - modified to correctly track prior exhaustion. Prior exhaustion now does not change 'AggregateLimit' and updates 'Occurrence
-- 	Remaining' (as before) and 'AggregateAllocated' and 'AggregateRemaining' (modified). AggregateAllocated is updated by prior exhaustion
--	and AggregateRemaining is now updated properly, -1 if no aggregate and no prior exhaustion, 0 if an exhausted aggregate, and >0 if
--	a partially exhausted aggregate.

DECLARE @strSQL nvarchar(4000)
DELETE 
FROM ztblPolicyAllocation
WHERE AllocationID = @allocationID
SET @strSQL = 
    'DECLARE @previousExhaustion table (PolicyDateID int, AllocationTotal money)
    INSERT INTO @previousExhaustion 
    SELECT PolicyDateID, 
    SUM(IndemnityAllocation) + SUM(DefenseAllocation) AS AllocationTotal 
    FROM tblExhaustion a INNER JOIN tblExhaustionDate b ON a.ExhaustionDateID = b.ExhaustionDateID
    WHERE ExhaustionTypeID = ' + COALESCE(CONVERT(varchar(10), @exhaustionTypeID), 'NULL') + ' 
    GROUP BY PolicyDateID 
	
    INSERT INTO ztblPolicyAllocation (AllocationID, PolicyID, PolicyDateID, Provision1, PStartDate, PEndDate,
	   AnnualPeriod, InsuranceProgramID, AttachmentPoint, SIR, PerOccLimit, LayerPerOccLimit, AggregateLimit,
	   OccurrenceAllocation, OccurrenceRemaining, CumAllocation, CumAllocationIndemnity, CumAllocationDefense,
	   CumAllocationDefenseOutsideLimits, CumSIR, CumSIRIndemnity, CumSIRDefense, AggregateAllocated, 
	   AggregateRemaining, SIRAllocated, SIRProRation, SIRremaining, Exhausted, ColumnAllocation, ColumnSIR, UpperLimit, ColumnDefense, LayerPercent)
	
    SELECT ' + convert(varchar(10), @allocationID) + ', a.PolicyID, c.PolicyDateID, 
	   b.ProvisionValueID as Provision1, c.PStartDate, c.PEndDate, c.AnnualPeriod, a.InsuranceProgramID, 
	   d.AttachmentPoint, d.SIR, d.PerOccLimit, d.LayerPerOccLimit, d.AggregateLimit, 
	   0 AS OccurrenceAllocation, 
	   CASE WHEN AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * 
			 COALESCE(AllocationTotal, 0) <= PerOccLimit 
		  THEN CASE WHEN ' + convert(varchar(1), @considerPrevExhaustion) + ' * 
				    COALESCE(AllocationTotal, 0) > 0
				THEN CASE WHEN AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * 
				    COALESCE(AllocationTotal, 0) < 0 
					   THEN 0 
					   ELSE AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * 
						  COALESCE(AllocationTotal, 0) 
				    END
				ELSE CASE WHEN AggregateLimit > 0
					   THEN AggregateLimit
					   ELSE PerOccLimit
				    END
			 END
		  ELSE PerOccLimit 
	   END AS OccurrenceRemaining, 0 AS CumAllocation, 0 AS CumAllocationIndemnity, 
	   0 AS CumAllocationDefense, 0 AS CumAllocationDefenseOutsideLimits, 0 AS CumSIR, 0 AS CumSIRIndemnity, 
	   0 AS CumSIRDefense, ' + convert(varchar(1), @considerPrevExhaustion) + ' * 
	   COALESCE(AllocationTotal, 0) AS AggregateAllocated, 
	   CASE WHEN ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) > 0
		  THEN CASE WHEN AggregateLimit - COALESCE(AllocationTotal, 0) <= 0 
				THEN 0 
				ELSE AggregateLimit - COALESCE(AllocationTotal, 0)
			 END
		  ELSE CASE WHEN AggregateLimit > 0 
				THEN AggregateLimit 
				ELSE -1 
			 END
	   END AS AggregateRemaining, 0 AS SIRAllocated, 0 AS SIRProration, SIR AS SIRRemaining, 0 AS Exhausted, 
	   0 AS ColumnAllocation, 0 AS ColumnSIR, LayerPerOccLimit + AttachmentPoint + SIR as UpperLimit, 0 as ColumnDefense, PerOccLimit/LayerPerOccLimit as LayerPercent
    FROM tblPolicy a INNER JOIN tblProvision b ON a.PolicyID = b.PolicyID 
	   INNER JOIN tblPolicyDates c ON a.PolicyID = c.PolicyID
	   INNER JOIN tblPolicyLimits d ON a.PolicyID = d.PolicyID
	   LEFT JOIN @previousExhaustion e ON e.PolicyDateID = c.PolicyDateID
    WHERE b.ProvisionLabelID = 1 AND (d.AttachmentPoint IS NOT NULL) AND (d.PerOccLimit IS NOT NULL) AND 
	   (d.PerOccLimit <> 0) AND (d.LayerPerOccLimit IS NOT NULL) AND (d.LayerPerOccLimit >= PerOccLimit) AND 
	   (d.LayerPerOccLimit > 0) AND (d.SIR IS NOT NULL) AND (d.AggregateLimit IS NOT NULL) AND 
	   (c.PStartDate IS NOT NULL) AND LimitTypeID = ' + convert(varchar(10), @limitTypeID)

IF @strWhere IS NOT NULL AND @strWhere <> ''
Begin	
	SET @strSQL = @strSQL + ' AND ' + @strWhere
	EXEC sp_executesql @strSQL
	--print @strSQL
End
Else
Begin
	--print @strSQL
	EXEC sp_executesql @strSQL
End

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLSplitPolicies] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLSplitPolicies] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLSplitPolicies] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryALLSplitPolicies] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLSplitPolicies] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLSplitPolicies] TO [power_user]
GO
