SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency]
(@SubscriberID int = NULL, @Solvency varchar(50) = NULL, @InsuranceProgramID int = NULL, @limitTypeID int = NULL)
AS 
SET NOCOUNT ON

--DECLARE @InsuranceProgramID int 
--DECLARE @SubscriberID int 
--DECLARE @Solvency varchar(50) 
--DECLARE @limitTypeID int
--SET @InsuranceProgramID = 0
--SET @SubscriberID = 0
--SET @Solvency = null
--SET @limitTypeID = 0

DECLARE @maxInsuranceProgramID int
DECLARE @maxSubscriberID int
DECLARE @maxLimitTypeID int
SELECT @maxInsuranceProgramID = MAX(InsuranceProgramID) FROM tblInsuranceProgram
SELECT @maxSubscriberID  = MAX(SubscriberID) FROM vSubscription
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy

DECLARE @qrySubscriptionCoverageByPolicyByCompany TABLE (
	[minOfPStartDate] smalldatetime, 
	[maxOfPEndDate] smalldatetime,
	[minOfAnnualPeriod] int,
	[insuranceprogram] varchar (256),
	[subscriptionCompany] varchar(256),
	[subscriptionCompanySolvency] varchar(256),
	[PercentageActual] decimal,	
	[PercentageAllocation] decimal,		
	[SubscriberPerOccActual] money,
	[SubscriberPerOccAllocation] money,
	[CarrierID] int, 
	[InsuranceProgramID] int, 
	[AttachmentPoint] money, 
	[PerOccLimit] money,
	[LayerPerOccLimit] money, 
	[SIR] money, 
	[AggregateLimit] money, 
	[Layer] int, 
	[PolicyNum] varchar (256), 
	[PolicyID] int, 
	[SubscriberID] int,
	[SubscriptionSort] varchar(256)
)

INSERT INTO @qrySubscriptionCoverageByPolicyByCompany
	SELECT MIN(p.PStartDate) AS MinOfPStartDate, MAX(p.PEndDate) AS MaxOfPEndDate, MIN(p.AnnualPeriod) AS MinOfAnnualPeriod, 
		p.InsuranceProgram, s.SubscriptionCompany, s.SubscriptionCompanySolvency, 
		s.SubscriptionActualPercentOfPolicy, s.SubscriptionAllocationPercentOfPolicy,
		(s.SubscriptionActualPercentOfPolicy * p.PerOccLimit) AS SubscriberPerOccActual, 
		(s.SubscriptionAllocationPercentOfPolicy * p.PerOccLimit) AS SubscriberPerOccAllocation,
		p.CarrierID, p.InsuranceProgramID, p.AttachmentPoint, p.PerOccLimit, 
		p.LayerPerOccLimit, p.SIR, p.AggregateLimit, p.Layer, p.PolicyNum, p.PolicyID, s.SubscriberID, SubscriptionSort
	FROM vPolicy p 
		INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID 
	WHERE p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
		AND s.SubscriberID >= COALESCE(@SubscriberID, 0) AND s.SubscriberID <= COALESCE(NULLIF(@SubscriberID, 0), @maxSubscriberID) 
		AND s.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%')
		AND p.limitTypeID >= COALESCE(@limitTypeID, 0) AND p.limitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)
		AND DoNotReport <> 1
	GROUP BY p.InsuranceProgram, s.SubscriptionCompany, s.SubscriptionCompanySolvency, 
		s.SubscriptionActualPercentOfPolicy, s.SubscriptionAllocationPercentOfPolicy,
		(s.SubscriptionActualPercentOfPolicy * p.PerOccLimit), 
		(s.SubscriptionAllocationPercentOfPolicy * p.PerOccLimit),
		p.CarrierID, p.InsuranceProgramID, p.AttachmentPoint, p.PerOccLimit, 
		p.LayerPerOccLimit, p.SIR, p.AggregateLimit, p.Layer, p.PolicyNum, p.PolicyID, s.SubscriberID, SubscriptionSort

SELECT SubscriptionCompany, InsuranceProgramID, InsuranceProgram, CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END AS 'SubscriberSolvencyGroup',
	SUM(SubscriberPerOccActual) AS SumOfSubscriberPerOccActual, SUM(SubscriberPerOccAllocation) AS SumOfSubscriberPerOccAllocation, SubscriptionSort
FROM @qrySubscriptionCoverageByPolicyByCompany
GROUP BY SubscriptionCompany, InsuranceProgramID, InsuranceProgram, CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END, SubscriptionSort
ORDER BY SubscriptionSort

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency] TO [power_user]
GO
