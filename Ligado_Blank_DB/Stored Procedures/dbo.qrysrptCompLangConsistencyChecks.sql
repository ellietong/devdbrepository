SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qrysrptCompLangConsistencyChecks]
(@policyID int = NULL, @provisionlabelid int = NULL)
AS

SET NOCOUNT ON



SELECT Location, DetailedLocation, BatesStart, BatesEnd, EndorsementNum,
	FormNumber, coalesce(EffectiveDate, min(pstartdate)) as effectiveDate, Sort, ExactLanguage, LanguageNotes
FROM tblProvisionLanguage pl
	left JOIN tblExactLanguage el on el.ExactLanguageID = pl.ExactLanguageID
	left JOIN tblLocation l ON el.LocationID = l.LocationID
	LEFT JOIN tblDetailedLocation dl ON dl.DetailedLocationID = el.DetailedLocationID
	left join tblPolicyDates pd on pd.PolicyID = el.PolicyID 
	left join tblForms f on f.FormID = el.formid
WHERE el.PolicyID = @policyID AND ProvisionLabelID = @provisionlabelid
GROUP BY Location, DetailedLocation, BatesStart, BatesEnd, EndorsementNum,
	FormNumber, effectiveDate, Sort, ExactLanguage, LanguageNotes
ORDER BY Sort

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCompLangConsistencyChecks] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCompLangConsistencyChecks] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCompLangConsistencyChecks] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysrptCompLangConsistencyChecks] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCompLangConsistencyChecks] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCompLangConsistencyChecks] TO [power_user]
GO
