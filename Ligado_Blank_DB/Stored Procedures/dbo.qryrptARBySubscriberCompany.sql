SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryrptARBySubscriberCompany] 
(@SubscriberID int = NULL, @Solvency varchar(50) = NULL, @InsuranceProgramID int = NULL, @AllocationID int = NULL, @SchemeID int = NULL)
AS
SET NOCOUNT ON

--DECLARE @InsuranceProgramID int 
--DECLARE @SubscriberID int 
--DECLARE @Solvency varchar(50) 
--DECLARE @AllocationID int
--DECLARE @SchemeID int
--SET @InsuranceProgramID = 0
--SET @SubscriberID = 0
--SET @Solvency = null
--SET @AllocationID = 83
--SET @SchemeID = 28

DECLARE @maxSubscriberID int
DECLARE @maxAllocationID int
DECLARE @maxInsuranceProgramID int
SELECT @maxSubscriberID = MAX(SubscriberID) FROM vSubscription
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxInsuranceProgramID = MAX(InsuranceProgramID) FROM vPolicy

DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
					   CarrierID int, CarrierName nvarchar(250), InsuranceProgramID int)
INSERT INTO @Policy
	SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, InsuranceProgramID
	FROM vPolicy
	GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, InsuranceProgramID

IF @SchemeID <> 0 BEGIN
	SELECT s.SubscriberID, s.SubscriptionCompany, s.SubscriptionSection,
		MAX(ar.DateTime) AS [DateTime], p.CarrierGroupName, p.CarrierName, 
		SUM(ar.PV * s.SubscriptionActualPercentOfPolicy) AS PVAmtActual, 
		SUM(ar.Allocation * s.SubscriptionActualPercentOfPolicy) AS AllocationAmtActual, 
		SUM(ar.SIRAllocated * s.SubscriptionActualPercentOfPolicy) AS SIRAmtActual,
		SUM(ar.PV * s.SubscriptionAllocationPercentOfPolicy) AS PVAmtAllocation, 
		SUM(ar.Allocation * s.SubscriptionAllocationPercentOfPolicy) AS AllocationAmtAllocation, 
		SUM(ar.SIRAllocated * s.SubscriptionAllocationPercentOfPolicy) AS SIRAmtAllocation, 
		a.AllocationID, a.AllocationDescription, s.SubscriptionCompanySolvency, p.CarrierID, p.CarrierGroupID
	FROM @Policy p 
		INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID
		INNER JOIN vAllocationResults ar ON p.PolicydateID = ar.PolicydateID
		INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID
	WHERE s.SubscriberID >= COALESCE(@SubscriberID, 0) AND s.SubscriberID <= COALESCE(NULLIF(@SubscriberID, 0), @maxSubscriberID) 
		AND ar.AllocationID >= COALESCE(@AllocationID, 0) AND ar.AllocationID <= COALESCE(NULLIF(@AllocationID, 0), @maxAllocationID) 
		AND s.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%')  
		AND p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
		AND s.SubscriberID IN(SELECT SubscriberID FROM vSchemeSubscribers WHERE SchemeID = @SchemeID)
	GROUP BY s.SubscriptionSection,s.SubscriberID, s.SubscriptionCompany, p.CarrierGroupName, p.CarrierName, a.AllocationID, a.AllocationDescription, 
		s.SubscriptionCompanySolvency, p.CarrierID, p.CarrierGroupID
	ORDER BY p.CarrierName
END
ELSE BEGIN
	SELECT s.SubscriberID, s.SubscriptionCompany, s.SubscriptionSection,
		MAX(ar.DateTime) AS [DateTime], p.CarrierGroupName, p.CarrierName, 
		SUM(ar.PV * s.SubscriptionActualPercentOfPolicy) AS PVAmtActual, 
		SUM(ar.Allocation * s.SubscriptionActualPercentOfPolicy) AS AllocationAmtActual, 
		SUM(ar.SIRAllocated * s.SubscriptionActualPercentOfPolicy) AS SIRAmtActual,
		SUM(ar.PV * s.SubscriptionAllocationPercentOfPolicy) AS PVAmtAllocation, 
		SUM(ar.Allocation * s.SubscriptionAllocationPercentOfPolicy) AS AllocationAmtAllocation, 
		SUM(ar.SIRAllocated * s.SubscriptionAllocationPercentOfPolicy) AS SIRAmtAllocation, 
		a.AllocationID, a.AllocationDescription, s.SubscriptionCompanySolvency, p.CarrierID, p.CarrierGroupID
	FROM @Policy p 
		INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID
		INNER JOIN vAllocationResults ar ON p.PolicydateID = ar.PolicydateID
		INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID
	WHERE s.SubscriberID >= COALESCE(@SubscriberID, 0) AND s.SubscriberID <= COALESCE(NULLIF(@SubscriberID, 0), @maxSubscriberID) 
		AND ar.AllocationID >= COALESCE(@AllocationID, 0) AND ar.AllocationID <= COALESCE(NULLIF(@AllocationID, 0), @maxAllocationID) 
		AND s.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%')  
		AND p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
	GROUP BY s.SubscriptionSection,s.SubscriberID, s.SubscriptionCompany, p.CarrierGroupName, p.CarrierName, a.AllocationID, a.AllocationDescription, 
		s.SubscriptionCompanySolvency, p.CarrierID, p.CarrierGroupID
	ORDER BY p.CarrierName
END

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompany] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompany] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompany] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompany] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompany] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompany] TO [power_user]
GO
