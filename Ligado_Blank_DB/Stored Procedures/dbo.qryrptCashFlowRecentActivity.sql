SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptCashFlowRecentActivity]
(@StartDate date)
As

SET NOCOUNT ON

--Declare @StartDate date
--Set @StartDate = '1/1/2008'


SELECT	CashFlowID, TransactionType, [Date], Indemnity, DefenseCOunsel, NonBillable_DefenseCOunsel, MiscFees,
	Shortfall, Bundled, InvestmentINterest, UnpostedInsurer, UnpostedSelf, UnpostedCashReceipts,
	Indemnity+DefenseCOunsel+NonBillable_DefenseCOunsel+MiscFees+Shortfall+Bundled+InvestmentINterest+UnpostedInsurer+UnpostedSelf+UnpostedCashReceipts as TransferTotal,
	Total, Comment, CheckNumber, CheckStatus, Year([Date]) as Year, @StartDate as MinDate
FROM	tblCashFlow CF
Left Join tblTransactionType TT on CF.TransactionTypeID=TT.TransactionTypeID
Where Year([Date])>=YEAR(@startdate)

Order By [Date]

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity] TO [power_user]
GO
