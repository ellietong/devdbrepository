SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryWriteZipKey](@ZipQueueID uniqueidentifier, @ZipKey nvarchar(250))
AS
BEGIN
	--DECLARE @ZipQueueID uniqueidentifier = 'CC4729FE-FDB9-463B-B95F-070FD9C9F543'
	UPDATE tblZipQueue
	SET ZipKey = @ZipKey
	WHERE ZipQueueID = @ZipQueueID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryWriteZipKey] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_qryWriteZipKey] TO [Ligado_User]
GO
