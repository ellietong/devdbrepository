SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmUpdateDismissal] (@ClaimLawsuitID               int, 
										  @ClaimLawsuitStatusTrackingID int, 
										  @ClaimLawsuitStatusID         int, 
										  @DismissalDate                date, 
										  @QCBy                         nvarchar(255), 
										  @DocumentID                   int)
AS
SET NOCOUNT ON

/**If Settled Do Not set dismissed as primary status**/
IF (SELECT ClaimLawsuitStatusID 
	FROM tblClaimLawsuitStatusTracking 
	WHERE ClaimLawsuitStatusTrackingID = @ClaimLawsuitStatusTrackingID) IN(23,24,25)
BEGIN
	INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID,
											   ClaimLawsuitStatusID,
											   ProcessedDate,
											   IsPrimaryStatus,
											   StatusNotes,
											   StatusDate)
	SELECT @ClaimLawsuitID, 
		@ClaimLawsuitStatusID, 
		CONVERT(date,GETDATE()), 
		0, 
		'Updated by review of documentID ' + CONVERT(varchar(500),@DocumentID) + ' by ' + @QCBy, 
		@DismissalDate
END
ELSE
BEGIN
	UPDATE tblClaimLawsuitStatusTracking
		SET IsPrimaryStatus = 0
		WHERE ClaimLawsuitStatusTrackingID = @ClaimLawsuitStatusTrackingID

	INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, 
											   ClaimLawsuitStatusID, 
											   ProcessedDate, 
											   IsPrimaryStatus, 
											   StatusNotes, 
											   StatusDate)
	SELECT @ClaimLawsuitID, 
		@ClaimLawsuitStatusID, 
		CONVERT(date,GETDATE()), 
		1, 
		'Updated by review of documentID ' + convert(varchar(500),@DocumentID) + ' by ' + @QCBy, 
		@DismissalDate
END

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateDismissal] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateDismissal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateDismissal] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateDismissal] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateDismissal] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateDismissal] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateDismissal] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateDismissal] TO [power_user]
GO
