SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantProductInfo] (@ClaimantPersonalInfoID int)
as

Select ClaimantProductID, 
	ClaimantPersonalInfoID, 
	NavProductName, 
	NavBrand, 
	NavProductType, 
	NavManufacturer, 
	StartDate, 
	EndDate
From tblClaimantProduct cp
	INNER JOIN tblClaimantEmployment ce ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
Order By NavProductType, NavProductName

Return

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantProductInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantProductInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantProductInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantProductInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantProductInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantProductInfo] TO [power_user]
GO
