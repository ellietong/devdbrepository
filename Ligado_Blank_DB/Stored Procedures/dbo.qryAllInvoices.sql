SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryAllInvoices]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT * FROM tblDocuments WHERE DetailDocumentTypeID IN (40, 41, 42)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryAllInvoices] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryAllInvoices] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryAllInvoices] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryAllInvoices] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryAllInvoices] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryAllInvoices] TO [power_user]
GO
