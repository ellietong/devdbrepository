SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] (@ClaimID int)
AS

--DECLARE @ClaimID int
--SET @ClaimID = 1

SELECT cs.ClaimLawsuitID, 
	COALESCE(csd.ClaimantIDed, 0) AS ClaimantIDed, 
	COALESCE(csd.DODCert, 0) AS DODCert, 
	COALESCE(csd.DxMedicals, 0) AS DxMedicals,  
	COALESCE(csd.EmployExpoHist,0) AS EmployExpoHist,  
	COALESCE(csd.EstatePapers,0) AS EstatePapers, 
	COALESCE(csd.productid,0) AS ProductID, 
	COALESCE(csd.SettlementConfirmation, 0) AS SettlementConfirmation, 
	cs.ReleaseSignedDate, 
	cs.ReleaseRqstDate,
	cs.ReleaseSentDate, 
	COALESCE(rt.ReleaseType, '') as ReleaseType, 
	cpi.MedicareBeneficiaryStatusID, 
	MedicareBeneficiaryStatus,
	COALESCE(csd.MMSEA, 0) AS MMSEA, 
	ReleaseSentDate, 
	COALESCE(csd.MedicarePlaintiff, 0) AS MedicarePlaintiff, 
	COALESCE(csd.MedicarePlaintiffCounsel, 0) AS MedicarePlaintiffCounsel, 
	DeceasedDate, 
	SettlePaymentComments,
	COALESCE(MedicareReported, 0) AS MedicareReported, 
	cs.ReleaseTypeID, 
	ReleaseReceivedDate,
	NULL as SettlementAuth
FROM tblClaimSettlement cs
	LEFT JOIN tblClaimSettlementDocumentation csd ON cs.ClaimLawsuitID= csd.ClaimLawsuitID
	LEFT JOIN tblReleaseType rt					ON cs.ReleaseTypeID = rt.ReleaseTypeID
	LEFT JOIN tblClaimLawsuit cl				ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaim c						ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblMedicareBeneficiaryStatus mbs	ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID
WHERE cs.ClaimLawsuitID IN (SELECT DISTINCT ClaimLawsuitID 
							FROM tblClaimLawsuit 
							WHERE ClaimID = @ClaimID)
						
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] TO [power_user]
GO
