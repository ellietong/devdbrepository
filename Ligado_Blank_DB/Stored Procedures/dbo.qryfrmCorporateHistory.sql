SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmCorporateHistory]
As

SET NOCOUNT ON
SELECT CorporateHistoryID, CorporateHistoryProfile
FROM tblCorporateHistoryProfile

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorporateHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorporateHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorporateHistory] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCorporateHistory] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorporateHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorporateHistory] TO [power_user]
GO
