SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmNamedInsured]
AS
SET NOCOUNT ON
SELECT NamedInsuredID, NamedInsured
FROM tblNamedInsured
ORDER BY NamedInsured
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmNamedInsured] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmNamedInsured] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmNamedInsured] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmNamedInsured] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmNamedInsured] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmNamedInsured] TO [power_user]
GO
