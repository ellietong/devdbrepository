SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryfrmLawsuitEntryInfo] 
as

Select LawsuitID, 
	CaseCaption, 
	DocketNumber, 
	FileDate, 
	JurisdictionID, 
	StateID, 
	CourtTypeID, 
	ServiceDate, 
	ReceivedDate, 
	DefenseCounselAddressID,
	PlaintiffCounselAddressID,
	LeadPlaintiffFirstName, 
	LeadPlaintiffLastName,
	NationalPlaintiffCounselID,
	IsThirdPartyComplaint
From tblLawsuit
Order By FileDate desc, DocketNumber, CaseCaption

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawsuitEntryInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawsuitEntryInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawsuitEntryInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmLawsuitEntryInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawsuitEntryInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawsuitEntryInfo] TO [power_user]
GO
