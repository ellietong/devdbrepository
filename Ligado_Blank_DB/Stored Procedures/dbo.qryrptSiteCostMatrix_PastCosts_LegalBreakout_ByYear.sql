SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*The purpose of this script is to determine the past cost totals for 
Environmental Invoices by site by year.
Created: 4/16/07 - CK
Updated: 5/11/07 - CK
*/
-------------------------------------------------------
--	1	Calculate the total past cost invoice amount for each site.
CREATE  PROC  [dbo].[qryrptSiteCostMatrix_PastCosts_LegalBreakout_ByYear] (@SiteID int)
AS 
Set Nocount on

--DECLARE @SiteID int
--SET @SiteID =0

DECLARE @MaxSiteID int
SET @MaxSiteID = (Select MAX(siteid) from tblEnviroSitesDetail)

declare @Amount table (SiteID int, SiteName nvarchar (150), InvYear int, PrevBilledLegal money, PrevBilledOther money,
AdtlAmt_Legal money, AdtlAmt_Other money, vendorid int)

insert into @Amount

select s.siteid, d.sitename, CASE WHEN year(s.InvoiceDate) is null then year(s.paiddate) else year(s.invoicedate) end,
CASE WHEN (s.Billed = 1 AND s.CostTypeID = 1) THEN Sum(ReimbursableAmount) ELSE 0 END AS PrevBilledLegal, 
CASE WHEN (s.Billed = 1 AND s.CostTypeID <> 1) THEN Sum(ReimbursableAmount) ELSE 0 END AS PrevBilledOther, 
CASE WHEN (s.Billed <>1 AND s.CostTypeID = 1) THEN sum(ReimbursableAmount) ELSE 0 END as AdtlAmt_Legal,
CASE WHEN (s.Billed <>1 AND s.CostTypeID <> 1) THEN sum(ReimbursableAmount) ELSE 0 END as AdtlAmt_Other, 
Vendorid

from tblSiteCosts s
left join tblEnviroSitesDetail d on s.siteid = d.siteid
LEFT JOIN tblRecordSource rs ON s.recordsourceid = rs.recordsourceid
LEFT JOIN tblCostType ct ON s.CostTypeID = ct.CostTypeID

WHERE coalesce(rs.ExcludeRecordSource,0) <> 1 AND coalesce(ct.ExcludeCostType,0) <> 1 AND coalesce(ExcludedSite,0) <> 1 

group by s.siteid, d.sitename, CASE WHEN year(s.InvoiceDate) is null then year(s.paiddate) else year(s.invoicedate) end, 
billed, s.CostTypeID, vendorid


declare @PastSite table (SiteID int, vendorid INT, CostYear INT, 
PrevBill_Legal money, PrevBill_Other money, Adtl_Legal money, Adtl_Other money)

insert into @PastSite

SELECT SiteID, vendorid, InvYear, SUM(PrevBilledLegal) as PrevBill_Legal,
	SUM(PrevBilledOther) as PrevBill_Other,
	Sum(AdtlAmt_Legal) as Adtl_Legal, Sum(AdtlAmt_Other) as Adtl_Other
	
FROM @Amount
-- where  siteid = 306
GROUP BY SiteID, vendorid, InvYear

HAVING SUM(PrevBilledLegal) <>0 or  SUM(PrevBilledOther) <>0 or 
	Sum(AdtlAmt_Legal) <>0 or  Sum(AdtlAmt_Other) <>0
	
	
--sites
declare @Site table (SiteID int, SiteName nvarchar (150),  state nvarchar(10), country nvarchar(50))
insert into @Site

SELECT SiteID, SiteName, state, country

FROM tblEnvirositesDetail d
left join tblstate t on t.stateid = d.stateid
left join tblcountry c on c.countryid = d.countryid

GROUP BY SiteID, SiteName, state, country


--flag included sites
declare @Include table (SiteID int, Flag bit, Reason nvarchar (500))
INSERT INTO @Include

select SiteID, ExcludedSite, ExcludedSiteReason
from tblEnviroSitesDetail 
WHERE ExcludedSite = 1
group by SiteID, ExcludedSite, ExcludedSiteReason


--Pull data together
select s.SiteID, VendorName, s.SiteName, state, country, CostYear, 
Sum(CASE WHEN PrevBill_Legal is null then 0 else PrevBill_Legal end) as PrevBill_Legal,
sum(CASE WHEN PrevBill_Other is null then 0 else PrevBill_Other end) as PrevBill_Other,
sum(CASE WHEN Adtl_Legal is null then 0 else Adtl_Legal end )as Adtl_Legal, 
sum(CASE WHEN Adtl_Other is null then 0 else Adtl_Other end )as Adtl_Other, 
sum(CASE WHEN PrevBill_Legal is null then 0 else PrevBill_Legal end) +
sum(CASE WHEN PrevBill_Other is null then 0 else PrevBill_Other end) + sum(CASE WHEN Adtl_Legal is null then 0 else Adtl_Legal end )+
sum(CASE WHEN Adtl_Other is null then 0 else Adtl_Other end )as Total,
CASE WHEN i.Flag = 1 then 1 else 0 END as Excluded, i.Reason

from @site S
LEFT JOIN @PastSite P on p.siteid = S.siteid
LEFT JOIN @Include I on S.siteid = I.siteid
LEFT JOIN tblVendor V on v.vendorid = p.vendorid

where  
i.flag is null and
s.SiteID >= coalesce(@SiteID,0) 
AND s.SiteID<= coalesce(nullif(@SiteID, 0), @maxsiteid)
group by s.SiteID, s.SiteName, i.Flag, i.reason, VendorName, state, country, CostYear
order by s.sitename desc


RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSiteCostMatrix_PastCosts_LegalBreakout_ByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSiteCostMatrix_PastCosts_LegalBreakout_ByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSiteCostMatrix_PastCosts_LegalBreakout_ByYear] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSiteCostMatrix_PastCosts_LegalBreakout_ByYear] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSiteCostMatrix_PastCosts_LegalBreakout_ByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSiteCostMatrix_PastCosts_LegalBreakout_ByYear] TO [power_user]
GO
