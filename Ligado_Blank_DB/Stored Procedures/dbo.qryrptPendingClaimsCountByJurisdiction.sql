SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptPendingClaimsCountByJurisdiction] (@StateID int, @StartDate date, @EndDate date)
AS

Set NoCount ON

--DECLARE @StateID int
--set @StateID = 0
--DECLARE @StartDate DATETIME
--SET @StartDate = '7/1/2010'
--DECLARE @ENDDATE DATETIME
--SET @ENDDATE = '9/30/2010'


----Declare @ResponsibleDefenseCounselID int

Declare @AllResponsibleDefenseCounsel table (DefenseCounselAddressID int, 
				DefenseCounsel nvarchar(100), 
				ResponsibleDefenseCounselAddressID int,
				StateID int, 
				ResponsibleDefenseCounsel nvarchar(100),
				IsActiveCounsel bit)
Insert Into @AllResponsibleDefenseCounsel
Select dcs.DefenseCounselAddressID,
	d.DefenseCounsel+' - '+d.DefenseCounselCity as DefenseCounsel,
	dcs.ResponsibleDefenseCounselAddressID,
	StateID as StateID,
	r.DefenseCounsel+' - '+r.DefenseCounselCity  as ResponsibleDefenseCOunsel,
	IsActiveCounsel
From tblDefenseCounselState dcs 
	Inner Join tblDefenseCounselAddress d on d.DefenseCounselAddressID = dcs.DefenseCounselAddressID
	Inner JOin tblDefenseCounselAddress r on r.DefenseCounselAddressID = dcs.ResponsibleDefenseCounselAddressID
Group By d.DefenseCounsel+' - '+d.DefenseCounselCity, r.DefenseCounsel+' - '+r.DefenseCounselCity, dcs.DefenseCounselAddressID, dcs.ResponsibleDefenseCounselAddressID, IsActiveCounsel, StateID


-- Find Max ID Values
--Declare @MaxResponsibleDefenseCounselID int
Declare @MaxStateID int

--Define Max State ID
Select @MaxStateID = MAX(stateid) from tblState


Declare @DiseaseCount Table (State nvarchar(150), Jurisdiction nvarchar(250), PrimaryLocalPlaintiffCounsel nvarchar(1000),
	LastYearOpen datetime, MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int)
Insert into @DiseaseCount

Select s.FullNameState, Jurisdiction, pca.PlaintiffCounsel+' - '+pca.PlaintiffCounselCity as PrimaryLocalPlaintiffCounsel,
convert(datetime,('12/31/'+convert(nvarchar,(Year(StatusDate)-1)))) as LastYearOpen,
--Meso
Case	when Meso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when LC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when OC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when NM.HighLevelDiseaseID = 4 Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when Unk.HighLevelDiseaseID = 5 Then 1
		When Unk.HighLevelDiseaseID is Null Then 1
Else 0 End AS UnknownCount

From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
--Meso
Left Join tblDisease Meso on CD.DiseaseID=Meso.DiseaseID

--Lung Cancer
Left Join tblDisease LC on CD.DiseaseID=LC.DiseaseID

--Other Cancer
Left Join tblDisease OC on CD.DiseaseID=OC.DiseaseID

--Non-Malig
Left Join tblDisease NM on CD.DiseaseID=NM.DiseaseID

--Unknown
Left Join tblDisease Unk on CD.DiseaseID=Unk.DiseaseID



Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
--Left Join tblDefenseCounselState DCS on L.DefenseCounselAddressID=DCS.DefenseCounselAddressID AND L.StateID=DCS.StateID
Left Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
Left Join tblState S on L.StateID=S.StateID
INNER Join @AllResponsibleDefenseCounsel r on r.DefenseCounselAddressID = l.DefenseCounselAddressID and r.StateID = l.StateID

Where  s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID)
	and isprimaryforclaim=1 
	AND	(FileDate <= @EndDate or ServiceDate <= @EndDate)
	 and  (FileDate >= @StartDate or ServiceDate >= @StartDate )
	and CL.ClaimLawsuitStatusID = 22
Order By s.State, Year(ServiceDate), Jurisdiction, PrimaryLocalPlaintiffCounsel


Select Year(@StartDate) as OpenYear, Year(@EndDate) as CloseYear, State, Jurisdiction, PrimaryLocalPlaintiffCounsel, 
	Sum(MesoCount) as MesoCount, Sum(LungCancerCount) as LungCancerCount, Sum(OtherCancerCount) as OtherCancerCount,
	Sum(NonMaligCount) as NonMaligCount, Sum(UnknownCount) as UnknownCount
from @DiseaseCount
Group By State, Jurisdiction, PrimaryLocalPlaintiffCounsel

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdiction] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdiction] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdiction] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdiction] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdiction] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdiction] TO [power_user]
GO
