SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[qryfrmSiteType]
as
set nocount on

SELECT *
FROM tblSiteType

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteType] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmSiteType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteType] TO [power_user]
GO
