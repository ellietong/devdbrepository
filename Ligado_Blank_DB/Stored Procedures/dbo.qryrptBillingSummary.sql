SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptBillingSummary] (@SigAllocationID int)
As
Set Nocount on

--Declare @SigAllocationID int
--Set @SigAllocationID = 51

Declare @NonSigAllocationID int
Set @NonSigAllocationID = (Select NonSigAllocationID From tblAllocation where AllocationID = @SigAllocationID)

Declare @Sig int
Set @Sig = (Select SignatoryRun From tblAllocation where AllocationID = @SigAllocationID)

Declare @Header1 nvarchar(max)
Set @Header1 = (Select Convert(nvarchar(50),[Month])+' '+Convert(nvarchar(50),[Year])+' - '+Convert(nvarchar(50),BillNumber) from tblAllocation where AllocationID=@SigAllocationID)


If @Sig=1 begin
Declare @LondonAllocation money
Set @LondonAllocation = (Select (SUM(Allocation)) from tblAllocationResults ar Inner Join tblPolicyDates pd on pd.PolicyDateID = AR.PolicyDateID
Inner Join tblPolicy p on p.PolicyID = pd.PolicyID where AllocationID = @SigAllocationID and carrierid=20) 
End


Declare @AllocationResultsSig table (PolicyDateID int, AllocationDefense money, AllocationDefenseOutsideLimits money, AllocationIndemnity money, Allocation money)
Insert Into @AllocationResultsSig
Select PolicyDateID, Sum(AllocationDefense), Sum(AllocationDefenseOutsideLimits), Sum(AllocationIndemnity), SUM(Allocation)
From tblAllocationResults
Where AllocationID = @SigAllocationID
Group By PolicyDateID

Declare @AllocationResultsNonSig table (PolicyDateID int, AllocationDefense money, AllocationDefenseOutsideLimits money, AllocationIndemnity money, Allocation money)
Insert INto @AllocationResultsNonSig
Select PolicyDateID, Sum(AllocationDefense), Sum(AllocationDefenseOutsideLimits), Sum(AllocationIndemnity), SUM(Allocation)
From tblAllocationResults
Where AllocationID = @NonSigAllocationID
Group By PolicyDateID


--------Query Results
Declare @Results Table(OrderID int, OrderName nvarchar(100), CarrierName nvarchar(100), PolicyNum nvarchar(100), PStartDate datetime, PEndDate datetime,
		Indemnity money, Defense money, Adjustments money, Percentage float)
Insert Into @Results

----London Solvent
Select
CASE When DanaReportingStatus like 'Commuted' Then '2'
	When DanaReportingStatus like 'Solvent' Then '1'
	When DanaReportingStatus like 'Insolvent' Then '3' End as OrderID, 
CASE When DanaReportingStatus like 'Commuted' Then 'DANA/LONDON Insolvency'
	When DanaReportingStatus like 'Solvent' Then 'All Insurers'
	When DanaReportingStatus like 'Insolvent' Then 'DANA/LONDON Insolvency' End as OrderName,
c.carriername, Case when patindex('% %',policynum) = 0 Then PolicyNum else LEFT(policynum, patindex('% %',policynum)) end +
CASE When DanaReportingStatus like 'Commuted' Then '- Commuted'
	When DanaReportingStatus like 'Solvent' Then ' - Solvent'
	When DanaReportingStatus like 'Insolvent'   Then ' - Insolvent' End as PolicyNum,
	pd.pstartdate, pd.penddate, Sum(Coalesce(AllocationIndemnity,0)*Percentage) as Indemnity,
    Sum(Coalesce(AllocationDefense+AllocationDefenseOutsideLimits,0)*Percentage) as Defense,
	0 as Adjustments, Round(SUM(Percentage),4)*PerOccLimit/LayerPerOccLimit as Percentage
	--Sum(Coalesce(Allocation,0)*-1*Percentage) as Adjustments, Round(SUM(Percentage),4)*PerOccLimit/LayerPerOccLimit as Percentage
from tblPolicy p
	Inner Join tblPolicyDates pd on p.PolicyID = pd.policyID
	Inner Join @AllocationResultsSig ar on ar.PolicyDateID = pd.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	inner join tblPolicyLimits pl on pl.PolicyID=p.PolicyID
	Inner join tblSubscriptionCoverage sc on p.PolicyID = sc.PolicyID
	Inner join tblSubscriberCompany subc on sc.SubscriberID=Subc.subscriberID
	LEFT join mtblDanaSubscriberReportingStatus dsrs on dsrs.subscriberid = subc.SubscriberID
	Left join mtblDanaReportingStatus drs on dsrs.Danareportingstatusid = drs.Danareportingstatusid
	
	
where DanaReportingStatus like 'Solvent'

group by CASE When DanaReportingStatus like 'Commuted' Then '2'
	When DanaReportingStatus like 'Solvent' Then '1'
	When DanaReportingStatus like 'Insolvent' Then '3' End , 
CASE When DanaReportingStatus like 'Commuted' Then 'DANA/LONDON Insolvency'
	When DanaReportingStatus like 'Solvent' Then 'All Insurers'
	When DanaReportingStatus like 'Insolvent' Then 'DANA/LONDON Insolvency' End ,
c.carriername, Case when patindex('% %',policynum) = 0 Then PolicyNum else LEFT(policynum, patindex('% %',policynum)) end +
CASE When DanaReportingStatus like 'Commuted' Then ' - Commuted'
	When DanaReportingStatus like 'Solvent' Then ' - Solvent'
	When DanaReportingStatus like 'Insolvent'   Then ' - Insolvent' End ,
	pd.pstartdate, pd.penddate, PerOccLimit, LayerPerOccLimit, p.PolicyNum, DanaReportingStatus
	
UNION

----London Insolvent
Select
CASE When DanaReportingStatus like 'Commuted' Then '2'
	When DanaReportingStatus like 'Solvent' Then '1'
	When DanaReportingStatus like 'Insolvent' Then '3' End as OrderID, 
CASE When DanaReportingStatus like 'Commuted' Then 'DANA/LONDON Insolvency'
	When DanaReportingStatus like 'Solvent' Then 'All Insurers'
	When DanaReportingStatus like 'Insolvent' Then 'DANA/LONDON Insolvency' End as OrderName,
c.carriername, Case when patindex('% %',policynum) = 0 Then PolicyNum else LEFT(policynum, patindex('% %',policynum)) end +
CASE When DanaReportingStatus like 'Commuted' Then '- Commuted'
	When DanaReportingStatus like 'Solvent' Then ' - Solvent'
	When DanaReportingStatus like 'Insolvent'   Then ' - Insolvent' End as PolicyNum,
	pd.pstartdate, pd.penddate, Sum(Coalesce(AllocationIndemnity,0)*Percentage) as Indemnity,
    Sum(Coalesce(AllocationDefense+AllocationDefenseOutsideLimits,0)*Percentage) as Defense,
	0 as Adjustments, Round(SUM(Percentage),4)*PerOccLimit/LayerPerOccLimit as Percentage
from tblPolicy p
	Inner Join tblPolicyDates pd on p.PolicyID = pd.policyID
	Inner Join @AllocationResultsSig ar on ar.PolicyDateID = pd.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	inner join tblPolicyLimits pl on pl.PolicyID=p.PolicyID
	Inner join tblSubscriptionCoverage sc on p.PolicyID = sc.PolicyID
	Inner join tblSubscriberCompany subc on sc.SubscriberID=Subc.subscriberID
	LEFT join mtblDanaSubscriberReportingStatus dsrs on dsrs.subscriberid = subc.SubscriberID
	Left join mtblDanaReportingStatus drs on dsrs.Danareportingstatusid = drs.Danareportingstatusid
	where DanaReportingStatus like 'Insolvent' or DanaReportingStatus like 'Commuted'
group by CASE When DanaReportingStatus like 'Commuted' Then '2'
	When DanaReportingStatus like 'Solvent' Then '1'
	When DanaReportingStatus like 'Insolvent' Then '3' End , 
CASE When DanaReportingStatus like 'Commuted' Then 'DANA/LONDON Insolvency'
	When DanaReportingStatus like 'Solvent' Then 'All Insurers'
	When DanaReportingStatus like 'Insolvent' Then 'DANA/LONDON Insolvency' End ,
c.carriername, Case when patindex('% %',policynum) = 0 Then PolicyNum else LEFT(policynum, patindex('% %',policynum)) end +
CASE When DanaReportingStatus like 'Commuted' Then '- Commuted'
	When DanaReportingStatus like 'Solvent' Then ' - Solvent'
	When DanaReportingStatus like 'Insolvent'   Then ' - Insolvent' End,
	pd.pstartdate, pd.penddate, PerOccLimit, LayerPerOccLimit, p.PolicyNum, DanaReportingStatus
	
UNION

--Non-Sig Payers
Select '1' as OrderID, 'All Insurers', CarrierName, p.policynum, pd.pstartdate, pd.penddate,
    Sum(Coalesce(AllocationIndemnity,0)) as Indemnity,
    Sum(Coalesce(AllocationDefense+AllocationDefenseOutsideLimits,0)) as Defense,
	0 as Adjustments, Null as Percentage
 from tblPolicy p
	Inner Join tblPolicyDates pd on p.PolicyID = pd.policyID
	Inner Join @AllocationResultsNonSig ar on ar.PolicyDateID = pd.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
 Where p.CarrierID in (121,336,107)
 Group by CarrierName, PolicyNum, PStartDate, PEndDate	

Union 
 
--Sig Payers Non-London
Select '1' as OrderID, 'All Insurers', CarrierName, p.policynum, pd.pstartdate, pd.penddate,
    Sum(Coalesce(AllocationIndemnity,0)) as Indemnity,
    Sum(Coalesce(AllocationDefense+AllocationDefenseOutsideLimits,0)) as Defense,
	0 as Adjustments, Null as Percentage
from tblPolicy p
	Inner Join tblPolicyDates pd on p.PolicyID = pd.policyID
	Inner Join @AllocationResultsSig ar on ar.PolicyDateID = pd.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
Where p.CarrierID <> 20
Group by CarrierName, PolicyNum, PStartDate, PEndDate	
 
UNION

--Dana Producer

Select '4' as OrderID, 'DANA/LONDON Insolvency' as OrderName, 'Dana' as carriername, 'Producer' as policynum, 
null as pstartdate, null as penddate,
    Sum(Coalesce(Case when ot.DefenseType = 0 Then UncollapsedPrior+UncollapsedPost End,0)) as Indemnity,
    Sum(Coalesce(Case when ot.DefenseType = 1 Then UncollapsedPrior+UncollapsedPost End,0)) as Defense,
	0 as Adjustments, Null as Percentage
from tblAllocationResultsHorizontal arh
Inner Join tblOccurrence o on o.occurrenceID = arh.occurrenceID
Inner Join tblOccurrenceType ot on ot.occurrenceTypeID = o.occurrenceTypeID
Where AllocationID = @SigAllocationID

Select OrderID, OrderName, CarrierName, PolicyNum, PStartDate, PEndDate, SUM(Indemnity) AS Indemnity, SUM(Defense) AS Defense, SUM(Adjustments) AS Adjustments, @Header1 as Header1
From @Results r
GROUP BY OrderID, OrderName, CarrierName, PolicyNum, PStartDate, PEndDate
Order By OrderID, CarrierName, PStartDate

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [power_user]
GO
