SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[qryfrmMiscCashApproval] (@MiscCashActivityID int)
As
Set NoCount On

Insert Into tblMiscCashActivityApproval (MiscCashActivityID, ApprovedBy, ApprovedDate, IsApproved)
Select @MiscCashActivityID, System_User, GETDATE(), 1 As IsApproved

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMiscCashApproval] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmMiscCashApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMiscCashApproval] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmMiscCashApproval] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMiscCashApproval] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmMiscCashApproval] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMiscCashApproval] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmMiscCashApproval] TO [power_user]
GO
