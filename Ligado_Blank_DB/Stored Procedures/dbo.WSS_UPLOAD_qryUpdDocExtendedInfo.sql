SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] (@DocumentID			  bigint, 
													 @DocumentTypeID		  int, 
													 @DocumentSourceID		  int,  
													 @Accept				  bit, 
													 @EmailAddress			  nvarchar(250), 
													 @ReleaseTypeID			  int = null, 
													 @ReleaseRequestDate	  datetime = null, 
													 @ReleaseSentDate		  datetime = null, 
													 @ExecutedReleaseRcvdDate datetime = null)
AS
SET NOCOUNT ON

UPDATE tblDocuments
	SET ClaimantDocumentTypeID = @DocumentTypeID,
		DocumentSourceID = @DocumentSourceID,
		DateModified = GETDATE(),
		ModifiedBy = @EmailAddress,
		--DismissalProcessDate = CASE WHEN @DocumentTypeID = 16 AND DismissalProcessDate IS NULL THEN GETDATE() WHEN @DocumentTypeID <> 16 AND DismissalProcessDate IS NOT NULL THEN NULL ELSE DismissalProcessDate END,
		--ClaimLawsuitStatusID = CASE WHEN @DocumentTypeID = 16 THEN @DismissalTypeID ELSE NULL END,
		--DismissalDate = CASE WHEN @DocumentTypeID = 16 THEN @DismissalDate ELSE DismissalDate END,
		Accepted = CASE WHEN COALESCE(Accepted, 0) = 1 THEN Accepted WHEN @Accept = 0 AND COALESCE(Accepted, 0) = 0 THEN NULL ELSE 1 END,
		AcceptedBy = Case WHEN AcceptedBy IS NOT NULL THEN AcceptedBy WHEN @Accept = 1 THEN @EmailAddress ELSE NULL END,
		DateAccepted = CASE WHEN DateAccepted IS NOT NULL THEN DateAccepted WHEN @Accept = 1 THEN GETDATE() ELSE NULL END
	WHERE DocumentID = @DocumentID

IF @Accept = 1 AND RIGHT(@EmailAddress, 11) = 'kcicllc.com'
BEGIN
	UPDATE tblMultiClaimantDocument
		SET QCBy = @EmailAddress,
			QCDate = GETDATE()
			WHERE DocumentID = @DocumentID 	
END			

DECLARE @ClaimantPersonalInfoID int

/**Declare cursor to attach documentation to each claimant the document is attached to**/
DECLARE Cur_Claimants CURSOR Local Fast_Forward FOR
	SELECT ClaimantpersonalinfoID 
	FROM tblMultiClaimantDocument 
	WHERE DocumentID = @DocumentID

OPEN Cur_Claimants
FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @ClaimLawsuitID int
	SET @ClaimLawsuitID = (SELECT ClaimlawsuitID 
						   FROM tblClaimantPersonalInfo cpi 
							   INNER JOIN tblClaim c		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID 
							   INNER JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID 
						   WHERE c.IsCurrentClaim = 1 
						       AND cl.IsPrimaryForClaim = 1 
						       AND cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID)


	--if there is no record for this claim in tblSettlePaymentDocumentation then create one
	IF (SELECT COUNT(ClaimLawsuitID) FROM tblClaimSettlement WHERE ClaimLawsuitID = @ClaimLawsuitID) = 0 
	BEGIN
		INSERT INTO tblClaimSettlement (ClaimLawsuitID)
			SELECT @ClaimLawsuitID
	END


	--Update release information
	UPDATE tblClaimSettlement 
	SET	ReleaseRqstDate = COALESCE(@ReleaseRequestDate, ReleaseRqstDate),
		ReleaseToPC = COALESCE(@ReleaseSentDate, ReleaseToPC),
		ExecutedReleaseDate = COALESCE(@ExecutedReleaseRcvdDate, ExecutedReleaseDate),
		ReleaseTypeID = COALESCE(@ReleaseTypeID, ReleaseTypeID),
		SPUsername = @EmailAddress
	WHERE ClaimLawsuitID = @ClaimLawsuitID

	FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID
END

CLOSE Cur_Claimants
DEALLOCATE Cur_Claimants

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [power_user]
GO
