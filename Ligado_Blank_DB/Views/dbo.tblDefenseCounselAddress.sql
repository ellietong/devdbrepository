SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[tblDefenseCounselAddress]
AS
SELECT     F.FirmDisplayName AS DefenseCounsel, FA.FirmAddressID AS DefenseCounselAddressID, FA.FirmID AS DefenseCounselID, 
                      FA.FirmAddress1 AS DefenseCounselAddress1, FA.FirmAddress2 AS DefenseCounselAddress2, FA.FirmCity AS DefenseCounselCity, 
                      FA.FirmZip AS DefenseCounselZip, FA.FirmMailZip AS DefenseCounselMailZip, FA.FirmPhone AS DefenseCounselPhone, S.FullNameState, S.State, S.StateID, FA.PayeeAddress
FROM         [Ligado-ExtranetSecurity].dbo.Firm AS F INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.FirmAddress AS FA ON F.FirmID = FA.FirmID INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite AS FACS ON FA.FirmAddressID = FACS.FirmAddressID INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.ClientSite AS CS ON FACS.ClientSiteID = CS.ClientSiteID LEFT JOIN
                      [Ligado-ExtranetSecurity].dbo.tblState AS S ON FA.FirmStateID = S.StateID
WHERE     (CS.ClientID = 4) --4 = Marley
GROUP BY F.FirmDisplayName, FA.FirmAddressID, FA.FirmID, FA.FirmAddress1, FA.FirmAddress2, FA.FirmCity, FA.FirmZip, FA.FirmMailZip, FA.FirmPhone, S.FullNameState, 
                      S.State, S.StateID, FA.PayeeAddress




GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselAddress] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselAddress] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselAddress] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDefenseCounselAddress] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblDefenseCounselAddress] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblDefenseCounselAddress] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblDefenseCounselAddress] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselAddress] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselAddress] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseCounselAddress] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefenseCounselAddress] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseCounselAddress] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[36] 4[32] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "F"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 110
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FA"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 125
               Right = 406
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FACS"
            Begin Extent = 
               Top = 6
               Left = 444
               Bottom = 125
               Right = 648
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CS"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 233
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "S"
            Begin Extent = 
               Top = 126
               Left = 265
               Bottom = 230
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Out', 'SCHEMA', N'dbo', 'VIEW', N'tblDefenseCounselAddress', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'put = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'tblDefenseCounselAddress', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'tblDefenseCounselAddress', NULL, NULL
GO
