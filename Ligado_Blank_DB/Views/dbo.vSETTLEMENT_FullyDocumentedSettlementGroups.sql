SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vSETTLEMENT_FullyDocumentedSettlementGroups]
AS

SELECT SettlementGroupID, 
CASE WHEN COUNT(ClaimSettlementID) * 4 = SupportingDocumentation + ReleaseExecution + MEdicareForm + MedicareFullyDocumented THEN 1 ELSE 0 END AS SettlementGroupFullDocumented
FROM
(SELECT csg.SettlementGroupID,
CASE WHEN (COALESCE(sd.SettlementConfirmation,0) +COALESCE(sd.ClaimantIDed,0) + COALESCE(sd.DxMedicals,0) + COALESCE(sd.DODCert,0) + COALESCE(sd.estatePapers,0) + COALESCE(sd.ProductID,0) + COALESCE(sd.EmployExpoHist,0))  = 7
		and birthdate is not null and  cpi.SocialSecurityNumber is not null and DiseaseDiagnosisDate is not null and ClaimDiseaseID is not null THEN 1
		WHEN cpi.DeceasedDate IS NULL AND (COALESCE(sd.SettlementConfirmation,0) + COALESCE(sd.ClaimantIDed,0) + COALESCE(sd.DxMedicals,0) + COALESCE(sd.ProductID,0) + COALESCE(sd.EmployExpoHist,0)) = 5
		and birthdate is not null and  cpi.SocialSecurityNumber is not null and DiseaseDiagnosisDate is not null and ClaimDiseaseID is not null  THEN 1
	ELSE 0 END AS SupportingDocumentation,

	CASE WHEN cs.ReleaseRcvd = 1 THEN 1	ELSE 0  END AS ReleaseExecution, 

	--Case when ps.CheckID is not null then cs.SettlementAmount else 0 end as Paid, 
	
	--CASE WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 1 THEN 'Unknown' WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 2 THEN 'Yes - ' + COALESCE(HealthInsuranceClaimNumber, 'HICN Not Provided')WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 3 THEN 'Non-Beneficiary' END AS HICN,
	
	CASE WHEN cs.MedicareFormRcvd IS not null THEN 1 ELSE 0 END As MedicareForm, 
	
	case when sd.MedicareFullyDocumented = 1 then 1 Else 0 end as MedicareFullyDocumented,
	cs.ClaimSettlementID

FROM tblClaimSettlementGroup csg
INNER JOIN tblClaimSettlement cs ON csg.SettlementGroupID = cs.SettlementGroupID
INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimDiseaseID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN vDETAILS_SettlementDocumentation sd ON cs.ClaimSettlementID = sd.ClaimSettlementID) as doc
GROUP BY SettlementGroupID, SupportingDocumentation + ReleaseExecution + MEdicareForm + MedicareFullyDocumented
HAVING CASE WHEN COUNT(ClaimSettlementID) * 4 = SupportingDocumentation + ReleaseExecution + MEdicareForm + MedicareFullyDocumented THEN 1 ELSE 0 END > 0

GO
