SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vREPORT_CarrierPaymentSum]
AS

SELECT di.DefenseInvoiceID,	
	CONVERT(NVARCHAR(150), NULL) AS 'CarrierName',--CASE WHEN m.MatterID = 12 THEN COALESCE(Carriergroupname,'Marsh Pool') ELSE COALESCE(Carriergroupname,'GSX') END AS 'CarrierName',
	CONVERT(MONEY, NULL) AS 'BilledAmount',--SUM(DefenseAllocation + DefenseOutsideLimitsAllocation) AS 'BilledAmount', 
	CONVERT(MONEY, NULL) AS 'AmountReceived',--Coalesce(SUM(Amount),0) AS 'AmountReceived',
	CONVERT(DATETIME, NULL) AS 'DatePaid'--SourceDate as 'DatePaid'
FROM tblDefenseInvoice di
	INNER JOIN tblDefInvClaimantDetail      dic ON dic.DefenseInvoiceID	= di.DefenseInvoiceID
	--INNER JOIN tbloccurrence                o   ON o.InvoiceDetailID = dic.InvoiceDetailID
	--INNER JOIN tblexhaustion                e   ON e.occurrenceid = o.occurrenceid
	--LEFT  JOIN (SELECT MAX(AccountsReceivablePaymentID) as [AccountsReceivablePaymentID], ExhaustionID as [ExhaustionID], SUM(Amount) as [Amount], PaymentDefenseID as [PaymentDefenseID]
	--			FROM tblAccountsReceivablePayment
	--			WHERE COALESCE(IsHistoric, 0) <> 1 
	--			GROUP BY ExhaustionID, IsHistoric,PaymentDefenseID)
	--										arp ON arp.ExhaustionID = e.ExhaustionID
	--INNER JOIN tblpolicydates               d   ON d.policydateid = e.policydateid
	--INNER JOIN tblpolicy                    p   ON p.policyid = d.policyid
	--INNER JOIN vCarrierWithGroup			c   ON c.carrierid = p.carrierid
	INNER JOIN tblMatter                    m   ON m.MatterID = dic.MatterID
	--LEFT  JOIN tblPaymentDefense			pd	ON pd.PaymentDefenseID = arp.PaymentDefenseID
	--LEFT  JOIN tblCheck						ch  ON ch.CheckID = pd.CheckID
WHERE DateApproved IS NOT NULL
GROUP BY di.DefenseInvoiceID, 
		--CarrierGroupName,
		m.MatterID--, SourceDate

GO
