SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vDETAILS_SettlementDetails]
AS
SELECT DISTINCT 
                      cl.ClaimLawsuitID, COALESCE (sd.ClaimantIDed, 0) AS ClaimantIDed, COALESCE (sd.DODCert, 0) AS DODCert, COALESCE (sd.DxMedicals, 0) AS DxMedicals, 
                      COALESCE (sd.EmployExpoHist, 0) AS EmployExpoHist, COALESCE (sd.EstatePapers, 0) AS EstatePapers, COALESCE (sd.ProductID, 0) AS ProductID, 
                      COALESCE (sd.SettlementConfirmation, 0) AS SettlementConfirmation, cs.ExecutedReleaseDate, cs.ReleaseRqstDate, cs.ReleaseToPC, COALESCE (rt.ReleaseType, '') 
                      AS ReleaseType, cpi.MedicareBeneficiaryStatusID, mbs.MedicareBeneficiaryStatus, COALESCE (sd.MMSEA, 0) AS MMSEA, COALESCE (sd.MedicarePlaintiff, 0) 
                      AS MedicarePlaintiff, COALESCE (sd.MedicarePlaintiffCounsel, 0) AS MedicarePlaintiffCounsel, cpi.DeceasedDate, cs.SettlePaymentComments, 
                      COALESCE (cs.MedicareReported, 0) AS MedicareReported, cs.ReleaseTypeID, COALESCE (sd.MedicareFormB, 0) AS MedicareFormB,
					  COALESCE(NULL, 0) AS PreTrialReport, COALESCE(NULL, 0) AS ExecutedRelease, COALESCE(NULL, 0) AS AllstateMMSEAForm, COALESCE(CompServ, 0) AS CompServ
FROM         dbo.tblClaimLawsuit AS cl LEFT OUTER JOIN
                      dbo.tblClaimSettlement AS cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblClaimSettlementDocumentation AS sd ON sd.ClaimLawsuitID = cs.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblReleaseType AS rt ON cs.ReleaseTypeID = rt.ReleaseTypeID LEFT OUTER JOIN
                      dbo.tblClaim AS c ON cl.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblMedicareBeneficiaryStatus AS mbs ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID




GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementDetails] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementDetails] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementDetails] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_SettlementDetails] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_SettlementDetails] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_SettlementDetails] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementDetails] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementDetails] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_SettlementDetails] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_SettlementDetails] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_SettlementDetails] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 234
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sd"
            Begin Extent = 
               Top = 6
               Left = 515
               Bottom = 114
               Right = 712
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rt"
            Begin Extent = 
               Top = 6
               Left = 750
               Bottom = 84
               Right = 903
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 941
               Bottom = 114
               Right = 1152
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mbs"
            Begin Extent = 
               Top = 114
               Left = 297
               Bottom = 192
               Right = 515
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 6
               Left = 272
               Bottom = 114
               Right = 496
            End
            DisplayFlags = 280
            TopColumn = 0
         En', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SettlementDetails', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'd
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 23
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SettlementDetails', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SettlementDetails', NULL, NULL
GO
