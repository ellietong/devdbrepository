SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vREPORT_DefenseSummary]
AS
SELECT     dicd.ClaimantPersonalInfoID, dicd.ClaimLawsuitID, cpi.FullName AS ClaimantName, 
		SUM(CASE WHEN checkfundingsourceid = 3 AND sourceamount < 0 THEN (dicd.fees * - 1) ELSE dicd.fees END) 
		+ SUM(CASE WHEN checkfundingsourceid = 3 AND sourceamount < 0 THEN (dicd.costs * - 1) ELSE (dicd.costs) END) 
        + SUM(CASE WHEN checkfundingsourceid = 3 AND sourceamount < 0 THEN (dicd.adjustments * - 1) ELSE (dicd.adjustments) END) AS DefenseTotal, 
		di.FirmID, c.SourceDate--, di.DefenseCounselAddressID
FROM         dbo.tblDocuments AS d LEFT OUTER JOIN
                      dbo.tblDefenseInvoice AS di ON d.DocumentID = di.DocumentID LEFT OUTER JOIN
                      dbo.tblDefenseInvoiceStatus AS dis ON di.StatusID = dis.StatusID LEFT OUTER JOIN
                      dbo.tblVendorFirm AS ex ON di.VendorFirmID = ex.VendorFirmID LEFT OUTER JOIN
                      dbo.tblPaymentDefense AS pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID LEFT OUTER JOIN
                      dbo.tblDefInvClaimantDetail AS dicd ON dicd.DefenseInvoiceID = di.DefenseInvoiceID LEFT OUTER JOIN
                          (SELECT     CheckID, SourceNumber, SourceDate, SourceAmount, MatterID, CheckFundingTypeID, CheckFundingSourceID, NavClaimResolutionKey, CheckPDF
                            FROM          dbo.tblCheck) AS c ON pd.CheckID = c.CheckID AND dicd.MatterID = c.MatterID LEFT OUTER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = dicd.ClaimantPersonalInfoID
WHERE     (d.ClaimantDocumentTypeID IN (40, 41)) 
GROUP BY dicd.ClaimantPersonalInfoID, dicd.ClaimLawsuitID, cpi.FullName, di.FirmID, c.SourceDate--, di.DefenseCounselAddressID





GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseSummary] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_DefenseSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseSummary] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_DefenseSummary] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_DefenseSummary] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_DefenseSummary] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_DefenseSummary] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseSummary] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_DefenseSummary] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_DefenseSummary] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_DefenseSummary] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_DefenseSummary] TO [power_user]
GO
