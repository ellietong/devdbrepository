SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vDETAILS_DefenseCostsByFirm]
AS
SELECT     InvoiceTotals.ClaimantPersonalInfoID, UPPER(COALESCE (e.VendorFirm, dc.DefenseCounsel, d.DefenseCounsel, 'UNKNOWN')) AS DefenseCounsel, 
                      COALESCE (InvoiceTotals.VendorFirmID, 0) AS VendorFirmID, SUM(InvoiceTotals.DefenseTotal) AS DefenseTotal, COALESCE (InvoiceTotals.FirmID, 0) AS FirmID, 
                      CONVERT(int, NULL) AS ClaimLawsuitID
FROM         (SELECT     CPI.ClaimantPersonalInfoID, DI.VendorFirmID, DI.DefenseCounselAddressID, SUM(dicd.AdjustedTotal) AS DefenseTotal, DI.FirmID
                       FROM          dbo.tblDefenseInvoice AS DI INNER JOIN
                                              dbo.tblDefInvClaimantDetail AS dicd ON DI.DefenseInvoiceID = dicd.DefenseInvoiceID INNER JOIN
                                              dbo.tblClaimantPersonalInfo AS CPI ON CPI.ClaimantPersonalInfoID = dicd.ClaimantPersonalInfoID
                       WHERE      (DI.StatusID IN (1, 2, 5))
                       GROUP BY CPI.ClaimantPersonalInfoID, DI.VendorFirmID, DI.DefenseCounselAddressID, DI.FirmID) AS InvoiceTotals LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress AS d ON InvoiceTotals.DefenseCounselAddressID = d.DefenseCounselAddressID LEFT OUTER JOIN
                      dbo.tblDefenseCounsel AS dc ON InvoiceTotals.FirmID = dc.DefenseCounselID LEFT OUTER JOIN
                      dbo.tblVendorFirm AS e ON InvoiceTotals.VendorFirmID = e.VendorFirmID
GROUP BY InvoiceTotals.ClaimantPersonalInfoID, COALESCE (e.VendorFirm, dc.DefenseCounsel, d.DefenseCounsel, 'UNKNOWN'), InvoiceTotals.VendorFirmID, 
                      InvoiceTotals.FirmID
GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_DefenseCostsByFirm] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[50] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "InvoiceTotals"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 262
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 319
               Bottom = 135
               Right = 562
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dc"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 192
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 6
               Left = 600
               Bottom = 135
               Right = 786
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
     ', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_DefenseCostsByFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'    Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_DefenseCostsByFirm', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_DefenseCostsByFirm', NULL, NULL
GO
