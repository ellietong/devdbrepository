SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vDETAILS_TenderBasicHistory]
AS
SELECT	TenderBasicHistoryID,
		ClaimLawsuitID,
		tbh.TenderBasicEventTypeID,
		tbet.TenderBasicEventType,
		TenderBasicContactID_From,
		TenderBasicContactID_To,
		tbc_from.TenderBasicContact AS TenderFrom,
		tbc_to.TenderBasicContact AS TenderTo,
		Username,
		TenderDate
FROM tblTenderBasicHistory tbh
INNER JOIN tblTenderBasicEventType tbet ON tbh.TenderBasicEventTypeID = tbet.TenderBasicEventTypeID
INNER JOIN vTenderBasicContact tbc_from ON TenderBasicContactID_From = tbc_From.TenderBasicContactID
INNER JOIN vTenderBasicContact tbc_to ON TenderBasicContactID_To = tbc_to.TenderBasicContactID


GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_TenderBasicHistory] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_TenderBasicHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_TenderBasicHistory] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_TenderBasicHistory] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_TenderBasicHistory] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_TenderBasicHistory] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_TenderBasicHistory] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_TenderBasicHistory] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_TenderBasicHistory] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_TenderBasicHistory] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_TenderBasicHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_TenderBasicHistory] TO [power_user]
GO
