SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vREPORT_PotentialSettlementsInProgress]
AS
SELECT		cpi.ClaimantPersonalInfoID,
			cpi.FullName,
			s.State + ', ' + j.Jurisdiction AS Jurisdiction, 
			pca.PlaintiffCounsel, 
			hld.HighLevelDisease, 
			Jobsite, 
			clt.ClaimLawsuitType,  
			cl.ActiveTrialDate, 
			DemandAmount, 
            clsai.SettlementLowAmount, 
			clsai.SettlementHighAmount, 
			clsai.EstimatedClientAllocation, 
			clsai.AuthorityAmountRequested, 
			AuthorityGrantedStatus,
			MatterID,
			cl.ClaimLawsuitTypeID
FROM         dbo.tblClaimantPersonalInfo AS cpi INNER JOIN
                          (SELECT     ClaimantPersonalInfoID, ClaimID, MatterID
                            FROM          dbo.tblClaim) AS c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID INNER JOIN
                          (SELECT     ClaimID, ClaimLawsuitID, LawsuitID, ClaimLawsuitTypeID, ActiveTrialDate, DemandAmount
                            FROM          dbo.tblClaimLawsuit) AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                          (SELECT     NationalPlaintiffCounselID, LawsuitID, JurisdictionID, StateID
                            FROM          dbo.tblLawsuit) AS l ON cl.LawsuitID = l.LawsuitID INNER JOIN
                          (SELECT     PlaintiffCounselAddressID, PlaintiffCounsel
                            FROM          dbo.tblPlaintiffCounselAddress) AS pca ON l.NationalPlaintiffCounselID = pca.PlaintiffCounselAddressID 
						LEFT OUTER JOIN (SELECT DISTINCT ClaimLawsuitID, COALESCE(STUFF((SELECT DISTINCT COALESCE('; ' + CASE WHEN JobsiteStateID IS NOT NULL THEN COALESCE(JobsiteCity, 'UNKNOWN') ELSE JobsiteCity END + CASE WHEN cle.JobsiteStateID IS NOT NULL THEN ', ' ELSE '' END, '') + COALESCE([State], '')
																		 FROM tblClaimLawsuitEmployment cle
																		 LEFT JOIN tblState s ON cle.JobsiteStateID = s.StateID
																		 WHERE cle.ClaimLawsuitID = ce.ClaimLawsuitID
																		 --AND (JobsiteStateID IS NOT NULL OR JobsiteCity IS NOT NULL)
																		 ORDER BY COALESCE('; ' + CASE WHEN JobsiteStateID IS NOT NULL THEN COALESCE(JobsiteCity, 'UNKNOWN') ELSE JobsiteCity END + CASE WHEN cle.JobsiteStateID IS NOT NULL THEN ', ' ELSE '' END, '') + COALESCE([State], '')
																		 FOR XML PATH('')), 1, 2, ''), 'N/A') AS Jobsite
																		FROM tblClaimLawsuitEmployment ce) AS aggregatedJobsites ON cl.ClaimLawsuitID = aggregatedJobsites.ClaimLawsuitID
					INNER JOIN
                      dbo.tblClaimDisease AS cd ON c.ClaimID = cd.ClaimID INNER JOIN
                      dbo.tblDisease AS d ON cd.DiseaseID = d.DiseaseID INNER JOIN
                      dbo.tblHighLevelDisease AS hld ON d.HighLevelDiseaseID = hld.HighLevelDiseaseID INNER JOIN
                      dbo.tblJurisdiction AS j ON l.JurisdictionID = j.JurisdictionID INNER JOIN
                      dbo.tblClaimLawsuitType AS clt ON cl.ClaimLawsuitTypeID = clt.ClaimLawsuitTypeID INNER JOIN
                      dbo.tblClaimLawsuitStatusTracking AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
					  LEFT JOIN tblClaimLawsuitSettlementAuthorityInfo clsai ON cl.ClaimLawsuitID = clsai.ClaimLawsuitID
					  LEFT JOIN tblAuthorityGrantedStatus ags ON clsai.AuthorityGrantedStatusID = ags.AuthorityGrantedStatusID
					  INNER JOIN tblState s ON l.StateID = s.StateID
						
WHERE     (clst.ClaimLawsuitStatusID IN (41, 42)) AND EstimatedClientAllocation IS NOT NULL --DemandAmount IS NOT NULL



GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_PotentialSettlementsInProgress] TO [power_user]
GO
