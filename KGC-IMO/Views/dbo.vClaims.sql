SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vClaims]
AS
SELECT     cpi.ClaimantPersonalInfoID, cpi.FirstName, cpi.MiddleName, cpi.LastName, cpi.SocialSecurityNumber, cpi.DeceasedDate, cpi.MaritalStatusID, ms.MaritalStatus, 
                      cpi.GenderID, g.Gender, cpi.Address1, cpi.Address2, cpi.City, cpi.Zip, cpi.MailZip, cpi.BirthDate, cpi.SuffixID, csuf.Suffix, cpi.Address1 AS Expr1, 
                      cpi.Address2 AS Expr2, cpi.City AS Expr3, cst.State AS ClaimantState, cpi.Zip AS Expr4, cpi.Phone, cpi.Extension, c.ClaimID, c.IsCurrentClaim, c.ClaimSequenceID, 
                      cseq.ClaimSequence, m.Matter, cd.DoctorID, d.DoctorLastName, d.DoctorFirstName, d.DoctorMiddleName, d.DirtyDoctorFlag, d.DoctorNote, cd.DiseaseID, 
                      cd.DiseaseDiagnosisDate, cl.ClaimLawsuitID, cl.LawsuitID, cl.IndividualDocketNumber, cl.IsPrimaryForClaim, CLST.ClaimLawsuitStatusID, cls.ClaimLawsuitStatus, 
                      CLST.StatusDate, CLST.StatusNotes, CLST.IsPrimaryStatus, l.CaseCaption, l.DocketNumber, l.FileDate, l.JurisdictionID, j.Jurisdiction, l.StateID, st.State, 
                      st.FullNameState, l.CourtTypeID, ct.CourtType, l.ServiceDate, l.ReceivedDate, l.DefenseCounselAddressID, l.PlaintiffCounselAddressID, 
                      l.PrimaryPlaintiffNationalCounselID, pnpc.PrimaryPlaintiffNationalCounsel, l.LeadPlaintiffFirstName, l.LeadPlaintiffLastName, l.ActiveTrialDate
FROM         dbo.tblClaimantPersonalInfo AS cpi LEFT OUTER JOIN
                      dbo.tblMaritalStatus AS ms ON cpi.MaritalStatusID = ms.MaritalStatusID LEFT OUTER JOIN
                      dbo.tblGender AS g ON cpi.GenderID = g.GenderID LEFT OUTER JOIN
                      dbo.tblClaimantSuffix AS csuf ON cpi.SuffixID = csuf.SuffixID LEFT OUTER JOIN
                      dbo.tblClaim AS c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblMatter AS m ON m.MatterID = c.MatterID LEFT OUTER JOIN
                      dbo.tblClaimSequence AS cseq ON c.ClaimSequenceID = cseq.ClaimSequenceID LEFT OUTER JOIN
                      dbo.tblClaimDisease AS cd ON cd.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblDoctor AS d ON cd.DoctorID = d.DoctorID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatusTracking AS CLST ON CLST.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatus AS cls ON CLST.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                      dbo.tblJurisdiction AS j ON l.JurisdictionID = j.JurisdictionID LEFT OUTER JOIN
                      dbo.tblState AS st ON l.StateID = st.StateID LEFT OUTER JOIN
                      dbo.tblState AS cst ON cpi.StateID = cst.StateID LEFT OUTER JOIN
                      dbo.tblCourtType AS ct ON l.CourtTypeID = ct.CourtTypeID LEFT OUTER JOIN
                      dbo.tblPrimaryPlaintiffNationalCounsel AS pnpc ON pnpc.PrimaryPlaintiffNationalCounselID = l.PrimaryPlaintiffNationalCounselID
GO
GRANT VIEW DEFINITION ON  [dbo].[vClaims] TO [base_user]
GRANT SELECT ON  [dbo].[vClaims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vClaims] TO [power_user]
GRANT SELECT ON  [dbo].[vClaims] TO [power_user]
GRANT INSERT ON  [dbo].[vClaims] TO [power_user]
GRANT DELETE ON  [dbo].[vClaims] TO [power_user]
GRANT UPDATE ON  [dbo].[vClaims] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[50] 4[25] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2) )"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "cpi"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ms"
            Begin Extent = 
               Top = 6
               Left = 306
               Bottom = 95
               Right = 469
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "g"
            Begin Extent = 
               Top = 96
               Left = 306
               Bottom = 185
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "csuf"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 215
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cseq"
            Begin Extent = 
               Top = 216
               Left = 38
               Bottom = 305
               Right = 210
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 516
               Left = 38
               Bottom = 635
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 516
               Left = 254
               Bottom = 635
               Right = 459
            End
            DisplayFlags = 280
            T', 'SCHEMA', N'dbo', 'VIEW', N'vClaims', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'opColumn = 0
         End
         Begin Table = "cls"
            Begin Extent = 
               Top = 636
               Left = 38
               Bottom = 725
               Right = 230
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "j"
            Begin Extent = 
               Top = 636
               Left = 268
               Bottom = 725
               Right = 428
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "st"
            Begin Extent = 
               Top = 726
               Left = 293
               Bottom = 830
               Right = 453
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cst"
            Begin Extent = 
               Top = 834
               Left = 293
               Bottom = 938
               Right = 453
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ct"
            Begin Extent = 
               Top = 846
               Left = 38
               Bottom = 935
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cd"
            Begin Extent = 
               Top = 6
               Left = 507
               Bottom = 125
               Right = 701
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CLST"
            Begin Extent = 
               Top = 6
               Left = 739
               Bottom = 125
               Right = 971
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 726
               Left = 38
               Bottom = 845
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pnpc"
            Begin Extent = 
               Top = 6
               Left = 1009
               Bottom = 95
               Right = 1258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 6
               Left = 1296
               Bottom = 95
               Right = 1456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 186
               Left = 236
               Bottom = 305
               Right = 438
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 67
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
       ', 'SCHEMA', N'dbo', 'VIEW', N'vClaims', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane3', N'  Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vClaims', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=3
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vClaims', NULL, NULL
GO
