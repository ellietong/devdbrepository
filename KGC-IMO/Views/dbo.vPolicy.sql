SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vPolicy]
AS
SELECT     P.CarrierID, C.CarrierName, C.CarrierNickname, C.CarrierCurrentName, C.CarrierSolvency, C.CarrierSolvencyStatus, C.Signatory, C.CarrierNote, 
                      C.SolvencySource, C.DateInsolvent, C.CarrierSolvencyDetail, C.AMBestNo, C.PrincipalPlaceBusinessCity, C.PrincipalPlaceBusinessState, 
                      C.PrincipalPlaceBusinessCountry, C.PolicyHolder, C.GraceIgnore, C.CarrierGroupID, CG.CarrierGroupName, CG.CarrierGroupNote, P.PolicyID, 
                      P.MinPStartDate, P.NamedInsuredID, ni.NamedInsured, P.InsuranceProgramID, ip.InsuranceProgram, P.PolicyNum, P.PolicyNotes, P.PolicyReviewed, 
                      P.PolicyPDF, P.InitialDataEntry, P.InitialDataEntryDate, P.PolicyReview, P.PolicyReviewDate, PD.PolicyDateID, PD.PStartDate, PD.PEndDate, 
                      PD.ActualPStartDate, PD.ActualPEndDate, PD.AnnualPeriod, PL.LimitID, PL.AttachmentPoint, PL.PerOccLimit, PL.LayerPerOccLimit, PL.SIR, 
                      PL.SIRAggregateLimit, PL.AggregateLimit, PL.PreviouslyExhaustedAggregate, PL.Layer, PL.LimitNote, PL.LimitTypeID, plt.LimitType, 
                      P.DoNotReport
FROM         dbo.tblPolicy AS P LEFT OUTER JOIN
                      dbo.tblCarrier AS C ON C.CarrierID = P.CarrierID LEFT OUTER JOIN
                      dbo.tblCarrierGroup AS CG ON C.CarrierGroupID = CG.CarrierGroupID LEFT OUTER JOIN
                      dbo.tblPolicyDates AS PD ON PD.PolicyID = P.PolicyID LEFT OUTER JOIN
                      dbo.tblPolicyLimits AS PL ON PL.PolicyID = P.PolicyID LEFT OUTER JOIN
                      dbo.tblPolicyLimitType AS plt ON PL.LimitTypeID = plt.LimitTypeID LEFT OUTER JOIN
                      dbo.tblInsuranceProgram AS ip ON P.InsuranceProgramID = ip.InsuranceProgramID LEFT OUTER JOIN
                      dbo.tblNamedInsured AS ni ON P.NamedInsuredID = ni.NamedInsuredID
GO
GRANT VIEW DEFINITION ON  [dbo].[vPolicy] TO [base_user]
GRANT SELECT ON  [dbo].[vPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPolicy] TO [power_user]
GRANT SELECT ON  [dbo].[vPolicy] TO [power_user]
GRANT INSERT ON  [dbo].[vPolicy] TO [power_user]
GRANT DELETE ON  [dbo].[vPolicy] TO [power_user]
GRANT UPDATE ON  [dbo].[vPolicy] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "P"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 117
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "C"
            Begin Extent = 
               Top = 120
               Left = 38
               Bottom = 231
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CG"
            Begin Extent = 
               Top = 6
               Left = 257
               Bottom = 102
               Right = 428
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PD"
            Begin Extent = 
               Top = 102
               Left = 302
               Bottom = 213
               Right = 467
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PL"
            Begin Extent = 
               Top = 234
               Left = 38
               Bottom = 345
               Right = 271
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "plt"
            Begin Extent = 
               Top = 216
               Left = 302
               Bottom = 297
               Right = 455
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ip"
            Begin Extent = 
               Top = 348
               Left = 38
               Bottom = 429
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 0
         ', 'SCHEMA', N'dbo', 'VIEW', N'vPolicy', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'End
         Begin Table = "ni"
            Begin Extent = 
               Top = 300
               Left = 309
               Bottom = 381
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vPolicy', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPolicy', NULL, NULL
GO
