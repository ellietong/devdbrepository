SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vRequiredDocsByClaimant]
AS
SELECT DISTINCT 
                      cpi.ClaimantPersonalInfoID, CASE WHEN (d_1.ClaimantPersonalInfoID IS NOT NULL OR
                      ExecutedReleaseDate IS NOT NULL) THEN 1 ELSE 0 END AS ReleaseReceived, docs.SetPymtID, COALESCE (docs.EstatePapers, 0) AS EstatePapers, 
                      COALESCE (docs.DODCert, 0) AS DODCert
FROM         dbo.tblClaim AS c INNER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                      dbo.tblClaimantSettlement AS cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblSettlementGroup AS sg ON cs.SettlementGroupID = sg.SettlementGroupID LEFT OUTER JOIN
                      dbo.tblSettlePaymentDocumentation AS spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblSettlementDocumentation AS sd ON sd.SetPymtID = spd.SetPymtID INNER JOIN
                          (SELECT     cd1.ClaimDiseaseID, cd1.ClaimID, cd1.DiseaseID, cd1.PrimaryDisease, cd1.DiseaseDiagnosisDate, cd1.DoctorID, cd1.SPUsername
                            FROM          dbo.tblClaimDisease AS cd1 INNER JOIN
                                                   dbo.tblDisease AS d1 ON d1.DiseaseID = cd1.DiseaseID
                            WHERE      (cd1.PrimaryDisease = 1) AND (d1.HighLevelDiseaseID <> 5)) AS cdi ON cdi.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatusTracking AS cls ON cls.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                          (SELECT     mcd.ClaimantPersonalInfoID
                            FROM          dbo.tblMultiClaimantDocument AS mcd INNER JOIN
                                                   dbo.tblDocuments AS d ON d.DocumentID = mcd.DocumentID
                            WHERE      (d.ClaimantDocumentTypeID = 31)
                            GROUP BY mcd.ClaimantPersonalInfoID) AS d_1 ON d_1.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                          (SELECT     spd.SetPymtID, sd.EstatePapers, sd.DODCert
                            FROM          dbo.tblClaimantPersonalInfo AS cpi INNER JOIN
                                                   dbo.tblClaim AS c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID INNER JOIN
                                                   dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                                                   dbo.tblSettlePaymentDocumentation AS spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID LEFT OUTER JOIN
                                                   dbo.tblSettlementDocumentation AS sd ON spd.SetPymtID = sd.SetPymtID
                            WHERE      (cpi.DeceasedDate IS NOT NULL)) AS docs ON spd.SetPymtID = docs.SetPymtID
WHERE     (cls.IsPrimaryStatus = 1) AND (cls.ClaimLawsuitStatusID IN (25, 44)) AND (YEAR(cs.SettlementDate) >= 2013) AND (cdi.PrimaryDisease = 1)

GO
GRANT VIEW DEFINITION ON  [dbo].[vRequiredDocsByClaimant] TO [base_user]
GRANT SELECT ON  [dbo].[vRequiredDocsByClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vRequiredDocsByClaimant] TO [power_user]
GRANT SELECT ON  [dbo].[vRequiredDocsByClaimant] TO [power_user]
GRANT INSERT ON  [dbo].[vRequiredDocsByClaimant] TO [power_user]
GRANT DELETE ON  [dbo].[vRequiredDocsByClaimant] TO [power_user]
GRANT UPDATE ON  [dbo].[vRequiredDocsByClaimant] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -288
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 297
               Bottom = 114
               Right = 508
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 546
               Bottom = 114
               Right = 742
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 267
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sg"
            Begin Extent = 
               Top = 114
               Left = 305
               Bottom = 222
               Right = 509
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "spd"
            Begin Extent = 
               Top = 6
               Left = 780
               Bottom = 114
               Right = 985
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sd"
            Begin Extent = 
               Top = 6
               Left = 1023
               Bottom = 114
               Right = 1216
            End
            DisplayFlags = 280
            TopColumn = 0
      ', 'SCHEMA', N'dbo', 'VIEW', N'vRequiredDocsByClaimant', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'   End
         Begin Table = "cdi"
            Begin Extent = 
               Top = 294
               Left = 38
               Bottom = 402
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cls"
            Begin Extent = 
               Top = 114
               Left = 547
               Bottom = 222
               Right = 770
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d_1"
            Begin Extent = 
               Top = 114
               Left = 808
               Bottom = 177
               Right = 1001
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "docs"
            Begin Extent = 
               Top = 294
               Left = 261
               Bottom = 387
               Right = 412
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vRequiredDocsByClaimant', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vRequiredDocsByClaimant', NULL, NULL
GO
