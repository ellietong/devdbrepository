SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vtblDefenseInvoice]
AS
SELECT     di.DocumentID, di.DefenseInvoiceID, dca.DefenseCounsel, di.InvoiceNumber, di.Period, di.SubmitDate, di.DatePayable, dis.Status, SUM(dic.Fees) AS fees, 
                      SUM(dic.Costs) AS costs, SUM(dic.Adjustments) AS adjustments, COALESCE (SUM(dic.Fees), 0) + COALESCE (SUM(dic.Costs), 0) + COALESCE (SUM(dic.Adjustments), 
                      0) AS InvoiceTotal, ch.SourceNumber, ch.SourceDate, pd.PaymentAmount AS sourceamount, dic.MatterID, di.StatusID, di.InvoiceNotes, di.ApprovedBy, 
                      di.DateApproved, di.QCBy, di.QCDate, di.InvoiceLink, di.VendorFirmID
FROM         dbo.tblDefenseInvoice AS di INNER JOIN
                      dbo.tblDefInvClaimantDetail AS dic ON dic.DefenseInvoiceID = di.DefenseInvoiceID LEFT OUTER JOIN
                      dbo.tblPaymentDefense AS pd ON pd.DefenseInvoiceID = di.DefenseInvoiceID LEFT OUTER JOIN
                      dbo.tblDefenseInvoiceStatus AS dis ON dis.StatusID = di.StatusID LEFT OUTER JOIN
                      dbo.tblCheck AS ch ON ch.CheckID = pd.CheckID LEFT OUTER JOIN
                      dbo.tblVendor AS v ON v.VendorID = di.VendorFirmID LEFT OUTER JOIN
                      dbo.tblDefenseCounsel AS dca ON dca.DefenseCounselID = di.FirmID
GROUP BY di.DocumentID, di.DefenseInvoiceID, dca.DefenseCounsel, v.VendorName, di.InvoiceNumber, di.Period, di.SubmitDate, di.DatePayable, dis.Status, 
                      ch.SourceNumber, ch.SourceDate, pd.PaymentAmount, dic.MatterID, di.StatusID, di.InvoiceNotes, di.ApprovedBy, di.DateApproved, di.QCBy, di.QCDate, di.InvoiceLink, 
                      v.VendorID, di.VendorFirmID
GO
GRANT VIEW DEFINITION ON  [dbo].[vtblDefenseInvoice] TO [base_user]
GRANT SELECT ON  [dbo].[vtblDefenseInvoice] TO [base_user]
GRANT SELECT ON  [dbo].[vtblDefenseInvoice] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[vtblDefenseInvoice] TO [power_user]
GRANT SELECT ON  [dbo].[vtblDefenseInvoice] TO [power_user]
GRANT INSERT ON  [dbo].[vtblDefenseInvoice] TO [power_user]
GRANT DELETE ON  [dbo].[vtblDefenseInvoice] TO [power_user]
GRANT UPDATE ON  [dbo].[vtblDefenseInvoice] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[33] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "di"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 262
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dic"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pd"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 239
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dis"
            Begin Extent = 
               Top = 6
               Left = 300
               Bottom = 84
               Right = 467
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ch"
            Begin Extent = 
               Top = 84
               Left = 300
               Bottom = 192
               Right = 478
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dca"
            Begin Extent = 
               Top = 6
               Left = 719
               Bottom = 95
               Right = 913
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "v"
            Begin Extent = 
               Top = 6
               Left = 505
               Bottom = 125
               Right = 681
            End
            DisplayFlags = 280
            TopColumn = 0
         End', 'SCHEMA', N'dbo', 'VIEW', N'vtblDefenseInvoice', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vtblDefenseInvoice', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vtblDefenseInvoice', NULL, NULL
GO
