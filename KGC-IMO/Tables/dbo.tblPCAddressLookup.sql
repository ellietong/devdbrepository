CREATE TABLE [dbo].[tblPCAddressLookup]
(
[PlaintiffCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselAddressID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPCAddressLookup] TO [base_user]
GRANT SELECT ON  [dbo].[tblPCAddressLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPCAddressLookup] TO [power_user]
GRANT SELECT ON  [dbo].[tblPCAddressLookup] TO [power_user]
GO
