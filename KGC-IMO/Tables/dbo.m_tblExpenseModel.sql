CREATE TABLE [dbo].[m_tblExpenseModel]
(
[Start] [datetime] NULL,
[End] [datetime] NULL,
[Percentage] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblExpenseModel] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblExpenseModel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblExpenseModel] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblExpenseModel] TO [power_user]
GO
