CREATE TABLE [dbo].[PACE_LAWSUIT_FINAL]
(
[LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LWST_ID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CASE_CAPTION] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCKET_NUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILE_DATE] [date] NULL,
[JURISDICTION] [nvarchar] (55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COURT_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EARLIEST_SERVICE_DATE] [date] NULL,
[EARLIEST_RECEIVED_DATE] [date] NULL,
[ACTIVE_TRIAL_DATE] [date] NULL,
[DEFENSE_COUNSEL] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCAL_PLAINTIFF_COUNSEL] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NATIONAL_PLAINTIFF_COUNSEL] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEAD_PLNTF_FIRST_NAME] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEAD_PLNTF_LAST_NAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_MULTI_DISTRICT] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_THIRD_PARTY_COMPLAINT] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[PACE_LAWSUIT_FINAL] TO [base_user]
GRANT SELECT ON  [dbo].[PACE_LAWSUIT_FINAL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[PACE_LAWSUIT_FINAL] TO [power_user]
GRANT SELECT ON  [dbo].[PACE_LAWSUIT_FINAL] TO [power_user]
GO
