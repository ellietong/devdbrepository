CREATE TABLE [dbo].[m_tblPolicyLimits_NotProRated]
(
[PolicyID] [int] NULL,
[PerOccLimit] [money] NULL,
[LayerPerOccLimit] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblPolicyLimits_NotProRated] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPolicyLimits_NotProRated] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPolicyLimits_NotProRated] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblPolicyLimits_NotProRated] TO [power_user]
GO
