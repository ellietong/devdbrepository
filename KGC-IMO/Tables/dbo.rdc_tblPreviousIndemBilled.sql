CREATE TABLE [dbo].[rdc_tblPreviousIndemBilled]
(
[MonthlyBillID] [float] NULL,
[ClaimantLast] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantFirst] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantSSN] [float] NULL,
[Condition] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[SettlementDate] [datetime] NULL,
[ExposureDate] [datetime] NULL,
[DiagnosisDate] [datetime] NULL,
[ClaimAmount] [money] NULL,
[AdjustmentCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[rdc_tblPreviousIndemBilled] TO [base_user]
GRANT SELECT ON  [dbo].[rdc_tblPreviousIndemBilled] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[rdc_tblPreviousIndemBilled] TO [power_user]
GRANT SELECT ON  [dbo].[rdc_tblPreviousIndemBilled] TO [power_user]
GO
