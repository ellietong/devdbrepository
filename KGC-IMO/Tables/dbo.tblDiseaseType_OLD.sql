CREATE TABLE [dbo].[tblDiseaseType_OLD]
(
[DiseaseCategoryID] [bigint] NOT NULL IDENTITY(1, 1),
[DiseaseCategory] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiseaseType_OLD] ADD CONSTRAINT [PK_tblDiseaseType] PRIMARY KEY CLUSTERED  ([DiseaseCategoryID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDiseaseType_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiseaseType_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiseaseType_OLD] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDiseaseType_OLD] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDiseaseType_OLD] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDiseaseType_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblDiseaseType_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblDiseaseType_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDiseaseType_OLD] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDiseaseType_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDiseaseType_OLD', 'COLUMN', N'DiseaseCategoryID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDiseaseType_OLD', 'COLUMN', N'DiseaseCategoryID'
GO
