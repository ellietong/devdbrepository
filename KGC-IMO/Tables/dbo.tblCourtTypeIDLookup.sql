CREATE TABLE [dbo].[tblCourtTypeIDLookup]
(
[JuridictionID] [float] NULL,
[StateID] [float] NULL,
[CourtTypeID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCourtTypeIDLookup] TO [base_user]
GRANT SELECT ON  [dbo].[tblCourtTypeIDLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCourtTypeIDLookup] TO [power_user]
GRANT SELECT ON  [dbo].[tblCourtTypeIDLookup] TO [power_user]
GO
