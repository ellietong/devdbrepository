CREATE TABLE [dbo].[PLCounselLookup]
(
[Local_Plaintiff_Counsel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselAddressID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[PLCounselLookup] TO [base_user]
GRANT SELECT ON  [dbo].[PLCounselLookup] TO [base_user]
GRANT SELECT ON  [dbo].[PLCounselLookup] TO [Claims_User]
GRANT INSERT ON  [dbo].[PLCounselLookup] TO [Claims_User]
GRANT DELETE ON  [dbo].[PLCounselLookup] TO [Claims_User]
GRANT UPDATE ON  [dbo].[PLCounselLookup] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[PLCounselLookup] TO [power_user]
GRANT SELECT ON  [dbo].[PLCounselLookup] TO [power_user]
GRANT INSERT ON  [dbo].[PLCounselLookup] TO [power_user]
GRANT DELETE ON  [dbo].[PLCounselLookup] TO [power_user]
GRANT UPDATE ON  [dbo].[PLCounselLookup] TO [power_user]
GO
