CREATE TABLE [dbo].[Invoice_SubmittedOnBehalfOf]
(
[INVOICE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OnBehalfOf] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Invoice_SubmittedOnBehalfOf] TO [base_user]
GRANT SELECT ON  [dbo].[Invoice_SubmittedOnBehalfOf] TO [base_user]
GRANT SELECT ON  [dbo].[Invoice_SubmittedOnBehalfOf] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[Invoice_SubmittedOnBehalfOf] TO [power_user]
GRANT SELECT ON  [dbo].[Invoice_SubmittedOnBehalfOf] TO [power_user]
GO
