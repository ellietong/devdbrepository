CREATE TABLE [dbo].[tblscenarioid1421_temp]
(
[ScenarioID] [float] NULL,
[PolicyID] [float] NULL,
[InsurerID] [float] NULL,
[InsurerName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierID] [int] NULL,
[PolicyNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[LimitAmount] [money] NULL,
[Layer] [float] NULL,
[TowerYear] [float] NULL,
[PolDefForm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSignatory] [bit] NOT NULL,
[IsPayingNonSig] [bit] NOT NULL,
[IsQuotaShare] [bit] NOT NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblscenarioid1421_temp] TO [base_user]
GRANT SELECT ON  [dbo].[tblscenarioid1421_temp] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblscenarioid1421_temp] TO [power_user]
GRANT SELECT ON  [dbo].[tblscenarioid1421_temp] TO [power_user]
GO
