CREATE TABLE [dbo].[m_tblPACEAllocation$]
(
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScenarioID] [int] NULL,
[AllocationID] [int] NULL,
[MonthlyBillID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonthYear] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEAllocation$] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPACEAllocation$] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEAllocation$] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblPACEAllocation$] TO [power_user]
GO
