CREATE TABLE [dbo].[m_tblAttachmentLookup]
(
[PolicyID] [int] NULL,
[Excess Amount] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblAttachmentLookup] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblAttachmentLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblAttachmentLookup] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblAttachmentLookup] TO [power_user]
GO
