CREATE TABLE [dbo].[m_tblPACEPolicyInfo$]
(
[ScenarioID] [int] NULL,
[PolicyID] [int] NULL,
[InsurerID] [int] NULL,
[InsurerName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[LimitAmount] [money] NULL,
[Layer] [int] NULL,
[TowerYear] [int] NULL,
[PolDefForm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSignatory] [int] NULL,
[IsPayingNonSig] [int] NULL,
[IsQuotaShare] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEPolicyInfo$] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPACEPolicyInfo$] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEPolicyInfo$] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblPACEPolicyInfo$] TO [power_user]
GO
