CREATE TABLE [dbo].[tblTrialNames]
(
[TRIALNAMEID] [bigint] NULL,
[TRIAL_NAME_PACE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTrialNames] TO [base_user]
GRANT SELECT ON  [dbo].[tblTrialNames] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTrialNames] TO [power_user]
GRANT SELECT ON  [dbo].[tblTrialNames] TO [power_user]
GO
