CREATE TABLE [dbo].[tblLedgerSubscriber]
(
[LedgerSubscriberID] [int] NOT NULL IDENTITY(1, 1),
[LedgerSubscriber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLedgerSubscriber] ADD CONSTRAINT [PK__tblLedge__36017319AD58AAD3] PRIMARY KEY CLUSTERED  ([LedgerSubscriberID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLedgerSubscriber] TO [base_user]
GRANT SELECT ON  [dbo].[tblLedgerSubscriber] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLedgerSubscriber] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblLedgerSubscriber] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblLedgerSubscriber] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblLedgerSubscriber] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblLedgerSubscriber] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLedgerSubscriber] TO [power_user]
GRANT SELECT ON  [dbo].[tblLedgerSubscriber] TO [power_user]
GRANT INSERT ON  [dbo].[tblLedgerSubscriber] TO [power_user]
GRANT DELETE ON  [dbo].[tblLedgerSubscriber] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLedgerSubscriber] TO [power_user]
GO
