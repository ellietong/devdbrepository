CREATE TABLE [dbo].[billing_ClaimToInjuredParty_Expense]
(
[ClaimID] [int] NULL,
[INJ_KEY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProducerID] [int] NULL,
[MonthlyBillID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[billing_ClaimToInjuredParty_Expense] TO [base_user]
GRANT SELECT ON  [dbo].[billing_ClaimToInjuredParty_Expense] TO [base_user]
GRANT SELECT ON  [dbo].[billing_ClaimToInjuredParty_Expense] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[billing_ClaimToInjuredParty_Expense] TO [power_user]
GRANT SELECT ON  [dbo].[billing_ClaimToInjuredParty_Expense] TO [power_user]
GO
