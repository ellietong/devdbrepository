CREATE TABLE [dbo].[m_tblPACEPolicyBill$]
(
[ScenarioID] [int] NULL,
[MonthlyBillID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllocationID] [int] NULL,
[PolicyID] [int] NULL,
[PolicyType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConsumptionType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppliedIndemnity] [money] NULL,
[NonAppliedIndemnity] [money] NULL,
[AppliedExpense] [money] NULL,
[NonAppliedExpense] [money] NULL,
[Comment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEPolicyBill$] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPACEPolicyBill$] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEPolicyBill$] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblPACEPolicyBill$] TO [power_user]
GO
