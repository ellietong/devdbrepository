CREATE TABLE [dbo].[tblCLSLookup]
(
[ClaimLawsuitStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitStatusID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCLSLookup] TO [base_user]
GRANT SELECT ON  [dbo].[tblCLSLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCLSLookup] TO [power_user]
GRANT SELECT ON  [dbo].[tblCLSLookup] TO [power_user]
GO
