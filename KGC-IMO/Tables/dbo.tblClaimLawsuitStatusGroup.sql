CREATE TABLE [dbo].[tblClaimLawsuitStatusGroup]
(
[ClaimLawsuitStatusGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitStatusGroup] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusGroup] ADD CONSTRAINT [PK_tblClaimLawsuitStatusGroup] PRIMARY KEY CLUSTERED  ([ClaimLawsuitStatusGroupID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusGroup] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusGroup] TO [power_user]
GO
