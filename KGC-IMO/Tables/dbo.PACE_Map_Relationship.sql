CREATE TABLE [dbo].[PACE_Map_Relationship]
(
[RELATIONSHIP] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RelationshipID] [bigint] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[PACE_Map_Relationship] TO [base_user]
GRANT SELECT ON  [dbo].[PACE_Map_Relationship] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[PACE_Map_Relationship] TO [power_user]
GRANT SELECT ON  [dbo].[PACE_Map_Relationship] TO [power_user]
GO
