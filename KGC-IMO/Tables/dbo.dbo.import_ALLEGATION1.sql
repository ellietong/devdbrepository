CREATE TABLE [dbo].[dbo.import_ALLEGATION1]
(
[CLM_LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALLEGATION] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DAMAGE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MATERIAL] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[dbo.import_ALLEGATION1] TO [base_user]
GRANT SELECT ON  [dbo].[dbo.import_ALLEGATION1] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[dbo.import_ALLEGATION1] TO [power_user]
GRANT SELECT ON  [dbo].[dbo.import_ALLEGATION1] TO [power_user]
GO
