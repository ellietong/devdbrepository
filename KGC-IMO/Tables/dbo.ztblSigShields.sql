CREATE TABLE [dbo].[ztblSigShields]
(
[AllocationID] [int] NULL,
[ColumnID] [bigint] NULL,
[PolicyDateID] [bigint] NULL,
[NonSigPolicyDateID] [bigint] NULL,
[NonSigAttachmentPoint] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblSigShields] TO [base_user]
GRANT SELECT ON  [dbo].[ztblSigShields] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblSigShields] TO [power_user]
GRANT SELECT ON  [dbo].[ztblSigShields] TO [power_user]
GO
