CREATE TABLE [dbo].[blairo_mtblClaimantPersonalInfo_Lookup]
(
[ClaimantPersonalInfoID] [int] NULL,
[INJ_PRTY_KEY] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[blairo_mtblClaimantPersonalInfo_Lookup] TO [base_user]
GRANT SELECT ON  [dbo].[blairo_mtblClaimantPersonalInfo_Lookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[blairo_mtblClaimantPersonalInfo_Lookup] TO [power_user]
GRANT SELECT ON  [dbo].[blairo_mtblClaimantPersonalInfo_Lookup] TO [power_user]
GO
