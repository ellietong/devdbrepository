CREATE TABLE [dbo].[tblIssueType]
(
[IssueTypeID] [int] NOT NULL IDENTITY(1, 1),
[IssueType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblIssueType] ADD CONSTRAINT [PK_tblIssueType] PRIMARY KEY NONCLUSTERED  ([IssueTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblIssueType] TO [base_user]
GRANT SELECT ON  [dbo].[tblIssueType] TO [base_user]
GRANT SELECT ON  [dbo].[tblIssueType] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblIssueType] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblIssueType] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblIssueType] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblIssueType] TO [power_user]
GRANT SELECT ON  [dbo].[tblIssueType] TO [power_user]
GRANT INSERT ON  [dbo].[tblIssueType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblIssueType] TO [power_user]
GO
