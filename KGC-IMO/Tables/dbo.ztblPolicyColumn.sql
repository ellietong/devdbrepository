CREATE TABLE [dbo].[ztblPolicyColumn]
(
[PolicyColumnID] [int] NULL,
[ColumnID] [int] NULL,
[PolicyID] [int] NULL,
[PolicyDaysInColumn] [smallint] NULL,
[PercentPolicyDaysInColumn] [float] NULL,
[PolicyPerOccPerColumn] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyColumn] TO [base_user]
GRANT SELECT ON  [dbo].[ztblPolicyColumn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyColumn] TO [power_user]
GRANT SELECT ON  [dbo].[ztblPolicyColumn] TO [power_user]
GO
