CREATE TABLE [dbo].[m_tblAllocationDefenseTreatment]
(
[DefenseTreatmentID] [int] NOT NULL,
[DefenseTreatment] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProvisionLabelID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblAllocationDefenseTreatment] ADD CONSTRAINT [PK_m_tblAllocationDefenseTreatment] PRIMARY KEY CLUSTERED  ([DefenseTreatmentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblAllocationDefenseTreatment] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblAllocationDefenseTreatment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblAllocationDefenseTreatment] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblAllocationDefenseTreatment] TO [power_user]
GO
