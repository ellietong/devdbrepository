CREATE TABLE [dbo].[tblJurisdictionIDLookup]
(
[jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JuridictionID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblJurisdictionIDLookup] TO [base_user]
GRANT SELECT ON  [dbo].[tblJurisdictionIDLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblJurisdictionIDLookup] TO [power_user]
GRANT SELECT ON  [dbo].[tblJurisdictionIDLookup] TO [power_user]
GO
