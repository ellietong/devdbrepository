CREATE TABLE [dbo].[tblAllocationPolicyDateExclude]
(
[PolicyDateExcludeID] [int] NOT NULL,
[AllocationID] [int] NOT NULL,
[PolicyDateID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationPolicyDateExclude] ADD CONSTRAINT [PK_tblAllocationPolicyDateExclude] PRIMARY KEY CLUSTERED  ([PolicyDateExcludeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationPolicyDateExclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationPolicyDateExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationPolicyDateExclude] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationPolicyDateExclude] TO [power_user]
GO
