CREATE TABLE [dbo].[tblPaymentTerms]
(
[PaymentTermsID] [int] NOT NULL IDENTITY(1, 1),
[PaymentTerms] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPaymentTerms] ADD CONSTRAINT [PK_tblPaymentTerms] PRIMARY KEY CLUSTERED  ([PaymentTermsID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentTerms] TO [base_user]
GRANT SELECT ON  [dbo].[tblPaymentTerms] TO [base_user]
GRANT SELECT ON  [dbo].[tblPaymentTerms] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblPaymentTerms] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblPaymentTerms] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentTerms] TO [power_user]
GRANT SELECT ON  [dbo].[tblPaymentTerms] TO [power_user]
GRANT INSERT ON  [dbo].[tblPaymentTerms] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPaymentTerms] TO [power_user]
GO
