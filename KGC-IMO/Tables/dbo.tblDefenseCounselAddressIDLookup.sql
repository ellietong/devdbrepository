CREATE TABLE [dbo].[tblDefenseCounselAddressIDLookup]
(
[DefenseCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselAddressIDLookup] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselAddressIDLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselAddressIDLookup] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselAddressIDLookup] TO [power_user]
GO
