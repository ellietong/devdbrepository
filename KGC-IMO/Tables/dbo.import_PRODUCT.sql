CREATE TABLE [dbo].[import_PRODUCT]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISPLAY_VALUE] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BRAND] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESCRIPTION] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[START_DATE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[END_DATE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMMENTS] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[import_PRODUCT] TO [base_user]
GRANT SELECT ON  [dbo].[import_PRODUCT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[import_PRODUCT] TO [power_user]
GRANT SELECT ON  [dbo].[import_PRODUCT] TO [power_user]
GO
