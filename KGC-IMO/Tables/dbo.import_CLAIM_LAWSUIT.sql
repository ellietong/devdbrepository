CREATE TABLE [dbo].[import_CLAIM_LAWSUIT]
(
[CLM_LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_PRIMARY_FOR_CLAIM] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INDIVIDUAL_DOCKET_NUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS_DATE] [datetime] NULL,
[ClaimID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[import_CLAIM_LAWSUIT] TO [base_user]
GRANT SELECT ON  [dbo].[import_CLAIM_LAWSUIT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[import_CLAIM_LAWSUIT] TO [power_user]
GRANT SELECT ON  [dbo].[import_CLAIM_LAWSUIT] TO [power_user]
GO
