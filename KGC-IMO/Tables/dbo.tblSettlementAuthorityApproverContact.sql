CREATE TABLE [dbo].[tblSettlementAuthorityApproverContact]
(
[SettlementAuthorityApproverContactID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementAuthorityID] [bigint] NOT NULL,
[SettlementApproverContactID] [bigint] NOT NULL,
[ApprovalRequired] [bit] NOT NULL,
[ResponseWindowDays] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementAuthorityApproverContact] ADD CONSTRAINT [PK_tblSettlementAuthorityApproverContact] PRIMARY KEY CLUSTERED  ([SettlementAuthorityApproverContactID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityApproverContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityApproverContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityApproverContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityApproverContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementAuthorityApproverContact] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementAuthorityApproverContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementAuthorityApproverContact] TO [power_user]
GO
