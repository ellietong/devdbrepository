CREATE TABLE [dbo].[FINAL_ILO]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME_OF_READER] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROFUSION_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[FINAL_ILO] TO [base_user]
GRANT SELECT ON  [dbo].[FINAL_ILO] TO [base_user]
GRANT SELECT ON  [dbo].[FINAL_ILO] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[FINAL_ILO] TO [power_user]
GRANT SELECT ON  [dbo].[FINAL_ILO] TO [power_user]
GO
