CREATE TABLE [dbo].[tblProvisionGenericLanguage]
(
[ProvisionGenericLanguageID] [int] NOT NULL IDENTITY(1, 1),
[GenericLanguageID] [int] NULL,
[ProvisionLabelID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionGenericLanguage] ADD CONSTRAINT [PK_tblProvisionGenericLanguage] PRIMARY KEY CLUSTERED  ([ProvisionGenericLanguageID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionGenericLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionGenericLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionGenericLanguage] TO [Language_User]
GRANT INSERT ON  [dbo].[tblProvisionGenericLanguage] TO [Language_User]
GRANT DELETE ON  [dbo].[tblProvisionGenericLanguage] TO [Language_User]
GRANT UPDATE ON  [dbo].[tblProvisionGenericLanguage] TO [Language_User]
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionGenericLanguage] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionGenericLanguage] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvisionGenericLanguage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvisionGenericLanguage] TO [power_user]
GO
