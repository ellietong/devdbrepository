CREATE TABLE [dbo].[tblSettlementApproverContact]
(
[SettlementApproverContactID] [int] NOT NULL,
[PersonID] [int] NOT NULL,
[IsDefault] [bit] NULL,
[ApprovalRequired_Default] [bit] NOT NULL,
[AuthorityAmountThreshold] [money] NOT NULL,
[ResponseWindowDays_Default] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementApproverContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementApproverContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementApproverContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementApproverContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementApproverContact] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementApproverContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementApproverContact] TO [power_user]
GO
