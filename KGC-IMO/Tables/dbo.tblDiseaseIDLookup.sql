CREATE TABLE [dbo].[tblDiseaseIDLookup]
(
[IMODisease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiseaseID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDiseaseIDLookup] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiseaseIDLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDiseaseIDLookup] TO [power_user]
GRANT SELECT ON  [dbo].[tblDiseaseIDLookup] TO [power_user]
GO
