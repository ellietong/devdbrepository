CREATE TABLE [dbo].[ztblRespreadTracking]
(
[RespreadID] [bigint] NULL,
[AllocationID] [int] NULL,
[OccurrenceID] [bigint] NULL,
[RespreadAmount] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblRespreadTracking] TO [base_user]
GRANT SELECT ON  [dbo].[ztblRespreadTracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblRespreadTracking] TO [power_user]
GRANT SELECT ON  [dbo].[ztblRespreadTracking] TO [power_user]
GO
