CREATE TABLE [dbo].[ztblPolicyDateIDTriggeredSig]
(
[PolicyID] [int] NULL,
[AllocationID] [int] NULL,
[OriginalOccurrenceID] [int] NULL,
[ColumnID] [int] NULL,
[BlockTriggered] [bit] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [base_user]
GRANT SELECT ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [power_user]
GRANT SELECT ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [power_user]
GO
