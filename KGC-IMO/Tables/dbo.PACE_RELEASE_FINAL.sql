CREATE TABLE [dbo].[PACE_RELEASE_FINAL]
(
[CLM_RSLTN_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELEASE_SIGNED_DATE] [date] NULL,
[RELEASE_RCVD_DATE] [date] NULL,
[RELEASE_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELEASE_SENT_DATE] [date] NULL,
[RELEASE_REQUEST_RECEIVED] [date] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[PACE_RELEASE_FINAL] TO [base_user]
GRANT SELECT ON  [dbo].[PACE_RELEASE_FINAL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[PACE_RELEASE_FINAL] TO [power_user]
GRANT SELECT ON  [dbo].[PACE_RELEASE_FINAL] TO [power_user]
GO
