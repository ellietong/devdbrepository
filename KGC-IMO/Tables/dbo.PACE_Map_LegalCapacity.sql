CREATE TABLE [dbo].[PACE_Map_LegalCapacity]
(
[LegalCapacity] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[legalcapacityid] [bigint] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[PACE_Map_LegalCapacity] TO [base_user]
GRANT SELECT ON  [dbo].[PACE_Map_LegalCapacity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[PACE_Map_LegalCapacity] TO [power_user]
GRANT SELECT ON  [dbo].[PACE_Map_LegalCapacity] TO [power_user]
GO
