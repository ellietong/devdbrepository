CREATE TABLE [dbo].[ztblSigShieldsCurrent]
(
[ColumnID] [bigint] NULL,
[PolicyDateID] [bigint] NULL,
[Shields] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblSigShieldsCurrent] TO [base_user]
GRANT SELECT ON  [dbo].[ztblSigShieldsCurrent] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblSigShieldsCurrent] TO [power_user]
GRANT SELECT ON  [dbo].[ztblSigShieldsCurrent] TO [power_user]
GO
