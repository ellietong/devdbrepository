CREATE TABLE [dbo].[m_InternationalNumbers]
(
[REFERENCE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INTERNATIONAL_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_InternationalNumbers] TO [base_user]
GRANT SELECT ON  [dbo].[m_InternationalNumbers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_InternationalNumbers] TO [power_user]
GRANT SELECT ON  [dbo].[m_InternationalNumbers] TO [power_user]
GO
