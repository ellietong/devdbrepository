CREATE TABLE [dbo].[PACE_Map_DefendantCompany]
(
[DefendantCompany] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefendantCompanyID] [bigint] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[PACE_Map_DefendantCompany] TO [base_user]
GRANT SELECT ON  [dbo].[PACE_Map_DefendantCompany] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[PACE_Map_DefendantCompany] TO [power_user]
GRANT SELECT ON  [dbo].[PACE_Map_DefendantCompany] TO [power_user]
GO
