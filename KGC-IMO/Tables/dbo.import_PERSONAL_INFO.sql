CREATE TABLE [dbo].[import_PERSONAL_INFO]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REFERENCE_ID] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_ID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUFFIX] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOCIAL_SECURITY_NUMBER] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BIRTH_DATE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEATH_DATE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MARITAL_STATUS] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GENDER] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP_CODE] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHONE_NUMBER] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SMOKER] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantPersonalInfoID] [int] NULL,
[SuffixID] [int] NULL,
[StateID] [int] NULL,
[MaritalStatusID] [int] NULL,
[GenderID] [int] NULL,
[IsSmoker] [bit] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[import_PERSONAL_INFO] TO [base_user]
GRANT SELECT ON  [dbo].[import_PERSONAL_INFO] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[import_PERSONAL_INFO] TO [power_user]
GRANT SELECT ON  [dbo].[import_PERSONAL_INFO] TO [power_user]
GO
