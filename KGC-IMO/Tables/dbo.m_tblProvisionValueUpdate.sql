CREATE TABLE [dbo].[m_tblProvisionValueUpdate]
(
[PolicyID] [int] NULL,
[ProvisionValueID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblProvisionValueUpdate] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblProvisionValueUpdate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblProvisionValueUpdate] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblProvisionValueUpdate] TO [power_user]
GO
