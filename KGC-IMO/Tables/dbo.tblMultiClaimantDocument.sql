CREATE TABLE [dbo].[tblMultiClaimantDocument]
(
[MultiClaimantDocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[DocumentID] [bigint] NOT NULL,
[ClaimantPersonalInfoID] [bigint] NOT NULL,
[HistoricalDocumentLink] [bigint] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [bigint] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[Reference_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMultiClaimantDocument] ADD CONSTRAINT [PK_tblMultiClaimantDocument] PRIMARY KEY CLUSTERED  ([MultiClaimantDocumentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblMultiClaimantDocument_ClaimantPersonalInfoID] ON [dbo].[tblMultiClaimantDocument] ([ClaimantPersonalInfoID]) INCLUDE ([DocumentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblMultiClaimantDocument_DocumentID] ON [dbo].[tblMultiClaimantDocument] ([DocumentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMultiClaimantDocument] ADD CONSTRAINT [FK_tblMultiClaimantDocument_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID])
GO
ALTER TABLE [dbo].[tblMultiClaimantDocument] ADD CONSTRAINT [FK_tblMultiClaimantDocument_tblDocuments] FOREIGN KEY ([DocumentID]) REFERENCES [dbo].[tblDocuments] ([DocumentID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMultiClaimantDocument] TO [base_user]
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GO
