CREATE TABLE [dbo].[PACE_SETTLEMENT_FINAL]
(
[CLM_RSLTN_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RSLTN_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLEMENT_GROUP_ID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLEMENT_GROUP] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RSLTN_DATE] [date] NULL,
[SETTLE_AMT] [decimal] (15, 2) NULL,
[FIRST_PAYABLE_DATE] [date] NULL,
[PAY_TO] [nvarchar] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHECK_NBR] [decimal] (10, 0) NULL,
[PAID_DATE] [date] NULL,
[INSURER_AMT] [decimal] (15, 2) NULL,
[CLIENT_AMT] [decimal] (15, 2) NULL,
[ALLCTN_AMT] [decimal] (15, 2) NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[PACE_SETTLEMENT_FINAL] TO [base_user]
GRANT SELECT ON  [dbo].[PACE_SETTLEMENT_FINAL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[PACE_SETTLEMENT_FINAL] TO [power_user]
GRANT SELECT ON  [dbo].[PACE_SETTLEMENT_FINAL] TO [power_user]
GO
