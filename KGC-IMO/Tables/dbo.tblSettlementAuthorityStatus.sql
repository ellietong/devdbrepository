CREATE TABLE [dbo].[tblSettlementAuthorityStatus]
(
[SettlementAuthorityStatusID] [int] NOT NULL,
[SettlementAuthorityStatus] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementAuthorityStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementAuthorityStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementAuthorityStatus] TO [power_user]
GO
