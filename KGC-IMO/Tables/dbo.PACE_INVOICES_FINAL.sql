CREATE TABLE [dbo].[PACE_INVOICES_FINAL]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_PERIOD_DATE] [date] NULL,
[INVOICE_NUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_IDENTIFIER] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APPROVAL_DATE] [date] NULL,
[INVOICE_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_STATUS] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FEE_DISBURSEMENT_AMT] [decimal] (15, 2) NULL,
[FEE_AMT] [decimal] (15, 2) NULL,
[CHECK_DATE] [date] NULL,
[CHECK_NUMBER] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHK_AMT] [decimal] (15, 2) NULL,
[SUBMITTED_TO_CLIENT_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUBMITTED_FROM_COMPANY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_DISBURSEMENT_AMT] [decimal] (15, 2) NULL,
[INVOICE_FEE_AMT] [decimal] (15, 2) NULL,
[SUBMITTED_FROM_COMPANY] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADJUSTMENT_AMT] [decimal] (15, 2) NULL,
[ALLOCATION_DISBURSEMENT_AMT] [decimal] (15, 2) NULL,
[ALLOCATION_FEE_AMT] [decimal] (15, 2) NULL,
[SUBMITTAL_DATE] [date] NULL,
[COVERAGE_LINE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_ALLOCATION_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VPD_CLIENT_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUBMITTED_TO_CLIENT] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VENDOR_ID] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VENDOR_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[PACE_INVOICES_FINAL] TO [base_user]
GRANT SELECT ON  [dbo].[PACE_INVOICES_FINAL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[PACE_INVOICES_FINAL] TO [power_user]
GRANT SELECT ON  [dbo].[PACE_INVOICES_FINAL] TO [power_user]
GO
