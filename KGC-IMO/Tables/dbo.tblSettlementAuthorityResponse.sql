CREATE TABLE [dbo].[tblSettlementAuthorityResponse]
(
[SettlementAuthorityResponseID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementAuthorityStatusID] [int] NULL,
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentSettlementAuthorityResponseID] [bigint] NULL,
[CreatedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SettlementAuthorityID] [bigint] NOT NULL,
[DateCreated] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_SettlementAuthorityGranted]
   ON  [dbo].[tblSettlementAuthorityResponse]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	/**
		12/6/2012 - CPC - 
		
		This trigger runs on insert of a new authority response
		It checks if the logic/criteria to mark a given settlement authority record has
		been granted, then flags the tblSettlementAuthority record's ApprovalGranted field
		and triggers the email alert containing the discussion to be sent
	**/
	
	--variables for cursor run
	DECLARE @SettlementAuthorityResponseID bigint = (SELECT SettlementAuthorityResponseID FROM inserted)
	DECLARE @SettlementAuthorityID bigint = (SELECT SettlementAuthorityID FROM inserted)
	DECLARE @ParentSettlementAuthorityResponseID bigint = (SELECT ParentSettlementAuthorityResponseID FROM inserted)
	DECLARE @ResponseFirmAddressID bigint = (SELECT COALESCE(FirmAddressID, 0) FROM inserted i LEFT JOIN vtblPerson p ON i.CreatedBy = p.Email)
	DECLARE @ParentFirmAddressID bigint = (SELECT COALESCE(FirmAddressID, 0)
											FROM inserted i 
											INNER JOIN tblSettlementAuthorityResponse sar ON i.ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID
											LEFT JOIN vtblPerson p ON sar.CreatedBy = p.Email
										   )
	
	--Leader Berkon374,361a
	--Find out who was replying to whom and fire the correct email proc
	IF(@ResponseFirmAddressID <> 361) --If the response was created by an insurer OR KCIC, notify Leader Berkon contacts (include comment)
		EXEC [WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse_NotificationContacts]  @SettlementAuthorityResponseID
	ELSE --Otherwise, it is a LB response to an insurer -- notify the parent being responded to and CC Barbara (Do not include comment)
		EXEC [WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse]  @SettlementAuthorityResponseID

	
	IF object_id('tempdb..#FirmsRequiringApproval') IS NOT NULL
	BEGIN
	   DROP TABLE #FirmsRequiringApproval
	END
	CREATE TABLE #FirmsRequiringApproval (SettlementAuthorityID int, [Status] nvarchar(MAX))
	
	INSERT INTO #FirmsRequiringApproval
	SELECT SettlementAuthorityID, 
	COALESCE(
	'Awaiting approval from: <br/>' + 
	CONVERT(nvarchar(MAX), (
	SELECT DISTINCT CASE WHEN p.Email LIKE '%resolute-midwest.com' THEN DefenseCounsel + ' - Midwest' 
														WHEN p.Email LIKE '%resolute-ne.com' THEN DefenseCounsel + ' - New England' 
														ELSE COALESCE(DefenseCounsel, 'KCIC') END AS DefenseCounsel
	FROm tblSettlementAuthorityApproverContact saac
	INNER JOIN tblSettlementApproverContact sac ON saac.SettlementApproverContactID = sac.SettlementApproverContactID
	INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
	INNER JOIN tblDefenseCounsel dc ON p.FirmID = dc.DefenseCounselID
	WHERE NOT EXISTS (SELECT SettlementAuthorityResponseID
						FROM tblSettlementAuthorityResponse sar
						WHERE SettlementAuthorityID = saac.SettlementAuthorityID
						AND SettlementAuthorityStatusID = 1
						AND sar.CreatedBy = p.email)
	AND saac.SettlementAuthorityID = sa.SettlementAuthorityID
	AND COALESCE(saac.ApprovalRequired, 0) = 1
		FOR XML PATH('div'))), 'Authority Granted')
	FROM tblSettlementAuthority sa

	
	
	--Open a cursor to loop through inserted response record(s)
	DECLARE Cur_Responses CURSOR Local Fast_Forward for
	SELECT SettlementAuthorityResponseID, SettlementAuthorityID
	FROM inserted
	OPEN Cur_Responses
	FETCH NEXT FROM Cur_Responses INTO @SettlementAuthorityResponseID, @SettlementAuthorityID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--Check if authority has already been granted on the associated
		--tblSettlementAuthority record (logic is current > 3 approvals)
		IF(SELECT [Status] FROM #FirmsRequiringApproval WHERE SettlementAuthorityID = @SettlementAuthorityID) = 'Authority Granted'
		BEGIN
			--Fire the email for this settlement authority
			DECLARE @returnValue int
			EXEC @returnValue = WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval @settlementAuthorityID
			--Update the bit flag to show the email has been sent
			IF(@returnValue = 0)
			BEGIN
				UPDATE tblSettlementAuthority
				SET AuthorityGranted = 1
				WHERE SettlementAuthorityID = @SettlementAuthorityID
			END
		END
	FETCH NEXT FROM Cur_Responses INTO @SettlementAuthorityResponseID, @SettlementAuthorityID
	END
	CLOSE Cur_Responses
	DEALLOCATE Cur_Responses 
END
GO
ALTER TABLE [dbo].[tblSettlementAuthorityResponse] ADD CONSTRAINT [PK_tblSettlementAuthorityResponse] PRIMARY KEY CLUSTERED  ([SettlementAuthorityResponseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityResponse] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityResponse] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityResponse] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityResponse] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementAuthorityResponse] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementAuthorityResponse] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementAuthorityResponse] TO [power_user]
GO
