CREATE TABLE [dbo].[ztblNonSigPolicyTracking]
(
[AllocationID] [int] NULL,
[PolicyDateID] [int] NULL,
[OccurrenceIDStart] [bigint] NULL,
[OccurrenceIDEnd] [bigint] NULL,
[Triggered] [bit] NULL,
[Exhausted] [bit] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblNonSigPolicyTracking] TO [base_user]
GRANT SELECT ON  [dbo].[ztblNonSigPolicyTracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblNonSigPolicyTracking] TO [power_user]
GRANT SELECT ON  [dbo].[ztblNonSigPolicyTracking] TO [power_user]
GO
