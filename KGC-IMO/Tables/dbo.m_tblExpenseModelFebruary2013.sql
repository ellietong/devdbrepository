CREATE TABLE [dbo].[m_tblExpenseModelFebruary2013]
(
[Start] [datetime] NULL,
[End] [datetime] NULL,
[Percentage] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblExpenseModelFebruary2013] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblExpenseModelFebruary2013] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblExpenseModelFebruary2013] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblExpenseModelFebruary2013] TO [power_user]
GO
