CREATE TABLE [dbo].[LawsuitID_LWST_KEY_Link]
(
[LawsuitID] [int] NULL,
[Lwst_Key] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[LawsuitID_LWST_KEY_Link] TO [base_user]
GRANT SELECT ON  [dbo].[LawsuitID_LWST_KEY_Link] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[LawsuitID_LWST_KEY_Link] TO [power_user]
GRANT SELECT ON  [dbo].[LawsuitID_LWST_KEY_Link] TO [power_user]
GO
