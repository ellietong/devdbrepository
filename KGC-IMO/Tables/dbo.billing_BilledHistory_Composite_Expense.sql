CREATE TABLE [dbo].[billing_BilledHistory_Composite_Expense]
(
[BILL_PERIOD_ID] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAIM_TYPE] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLE_AMT] [decimal] (15, 2) NULL,
[BILL_EXPOSURE_DATE] [datetime] NULL,
[BILL_DIAGNOSIS_DATE] [datetime] NULL,
[BILL_CLM_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADJUSTABLE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[billing_BilledHistory_Composite_Expense] TO [base_user]
GRANT SELECT ON  [dbo].[billing_BilledHistory_Composite_Expense] TO [base_user]
GRANT SELECT ON  [dbo].[billing_BilledHistory_Composite_Expense] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[billing_BilledHistory_Composite_Expense] TO [power_user]
GRANT SELECT ON  [dbo].[billing_BilledHistory_Composite_Expense] TO [power_user]
GO
