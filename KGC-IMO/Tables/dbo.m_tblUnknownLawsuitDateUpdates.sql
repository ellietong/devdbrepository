CREATE TABLE [dbo].[m_tblUnknownLawsuitDateUpdates]
(
[LawsuitID] [int] NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblUnknownLawsuitDateUpdates] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblUnknownLawsuitDateUpdates] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblUnknownLawsuitDateUpdates] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblUnknownLawsuitDateUpdates] TO [power_user]
GO
