CREATE TABLE [dbo].[tblSavedServiceDateRange]
(
[SavedServiceDateID] [int] NOT NULL IDENTITY(1, 1),
[ServiceDateRange] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedServiceDateRange] ADD CONSTRAINT [PK_tblSavedServiceDateRange] PRIMARY KEY CLUSTERED  ([SavedServiceDateID]) ON [PRIMARY]
GO
