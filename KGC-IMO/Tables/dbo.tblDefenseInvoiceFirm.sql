CREATE TABLE [dbo].[tblDefenseInvoiceFirm]
(
[FirmID] [bigint] NOT NULL IDENTITY(1, 1),
[Firm] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayTo] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmAddress] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseInvoiceFirm] ADD CONSTRAINT [PK__mBtblDefenseInvo__48BAC3E5] PRIMARY KEY CLUSTERED  ([FirmID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoiceFirm] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoiceFirm] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoiceFirm] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseInvoiceFirm] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseInvoiceFirm] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoiceFirm] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoiceFirm] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseInvoiceFirm] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseInvoiceFirm] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', 'COLUMN', N'Firm'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', 'COLUMN', N'Firm'
GO
DECLARE @xp int
SELECT @xp=4785
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', 'COLUMN', N'Firm'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', 'COLUMN', N'PayTo'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', 'COLUMN', N'PayTo'
GO
DECLARE @xp int
SELECT @xp=5385
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceFirm', 'COLUMN', N'PayTo'
GO
