CREATE TABLE [dbo].[import_tblClaimantDocuments]
(
[DocumentNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentType] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTypeID] [int] NULL,
[DocumentURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTitle] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Folder] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileNameInFolder] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[import_tblClaimantDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[import_tblClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[import_tblClaimantDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[import_tblClaimantDocuments] TO [power_user]
GO
