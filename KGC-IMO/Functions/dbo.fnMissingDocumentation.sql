SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE FUNCTION [dbo].[fnMissingDocumentation]
(@ClaimLawsuitID bigint)

RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @CurrentDocumentation TABLE  (ClaimantIDed bit, 
									  DxMedicals bit, 
									  DODCert int, 
									  EstatePaper int, 
									  ProductID bit,
									  ExpoHist bit, 
									  SettleConfirmation bit, 
									  Release bit)
INSERT INTO @CurrentDocumentation
SELECT distinct COALESCE(sd.ClaimantIDed, 0) AS ClaimantIDed,
				COALESCE(sd.DxMedicals, 0) AS DxMedicals,
				CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE(sd.DODCert, 0) END AS DODCert, 
				CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE(sd.EstatePapers, 0) END AS Estatepaper, 
				COALESCE(sd.productid,0) AS ProductID, 
				COALESCE(sd.EmployExpoHist, 0) AS EmployExpoHist, 
				COALESCE(sd.settlementconfirmation, 0) AS SettlementConfirmation,
				CASE WHEN (d.ClaimantPersonalInfoID IS NOT NULL OR ExecutedReleaseDate IS NOT NULL) THEN 1 ELSE 0 END AS ReleaseReceived
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi	 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl	 ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
	LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
	INNER JOIN tblClaimLawsuitStatusTracking cls on cls.ClaimLawsuitID =cl.ClaimLawsuitID 
	LEFT JOIN (SELECT mcd.claimantpersonalinfoid 
			   FROM tblMultiClaimantDocument mcd 
				   INNER JOIN tbldocuments d on d.DocumentID = mcd.DocumentID 
			   WHERE ClaimantDocumentTypeID = 31 
			   GROUP BY mcd.ClaimantPersonalInfoID) d on d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID

DECLARE @MissingDocumentation TABLE  (ID int NOT NULL IDENTITY(1,1), MissingDocumentation nvarchar(500), Remainder int)
INSERT INTO @MissingDocumentation
SELECT 'Claimant Identified', NULL
FROM @CurrentDocumentation
WHERE ClaimantIDed = 0
UNION
SELECT 'Diagnosing Medicals', NULL
FROM @CurrentDocumentation
WHERE DxMedicals = 0
UNION
SELECT 'Death Cerificate', NULL
FROM @CurrentDocumentation
WHERE DODCert = 0
UNION
SELECT 'Estate Paperwork', NULL
FROM @CurrentDocumentation
WHERE EstatePaper = 0
UNION
SELECT 'Product Identification', NULL
FROM @CurrentDocumentation
WHERE ProductID = 0
UNION
SELECT 'Employment/Exposure History', NULL
FROM @CurrentDocumentation
WHERE ExpoHist = 0
UNION
SELECT 'Settlement Confirmation', NULL
FROM @CurrentDocumentation
WHERE SettleConfirmation = 0
UNION
SELECT 'Executed Release', NULL
FROM @CurrentDocumentation
WHERE Release = 0

	--Calculate the remainder for every-other row highlighting
	UPDATE @MissingDocumentation
	SET Remainder = fdp.Remainder 
	FROM (SELECT ID, (ID % 2) AS Remainder FROM @MissingDocumentation) fdp
	INNER JOIN @MissingDocumentation ofdp ON fdp.ID = ofdp.ID
	WHERE ofdp.ID = fdp.ID

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'

DECLARE @MissingDocumentationTable nvarchar(max) = ''

SET @MissingDocumentationTable = @MissingDocumentationTable + N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0;">' + 
	
+
	
	
CAST ( (
SELECT	CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleB END AS 'td/@style', (SELECT COALESCE(MissingDocumentation, '')) as td, ''
	FROM @MissingDocumentation
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	

	RETURN @MissingDocumentationTable



END



GO
