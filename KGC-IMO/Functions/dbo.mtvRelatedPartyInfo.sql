SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[mtvRelatedPartyInfo] 
(@ClaimantPersonalInfoID int)
RETURNS 
@CrpTable TABLE
(
	ClaimantPersonalInfoID int primary key NOT NULL,
	RelatedPartyInfo NVARCHAR(MAX)
)
AS
BEGIN

DECLARE @ClaimantRelatedPartyInfo  TABLE  (ID int NOT NULL,  
										   ClaimantPersonalInfoID bigint,	
										   RelatedPartyName nvarchar(500), 
										   RelationShip nvarchar(100),
										   LegalCapacity nvarchar(100),
										   Remainder int
										   )
INSERT INTO @ClaimantRelatedPartyInfo
SELECT ROW_NUMBER() OVER (ORDER BY (SELECT NULL)),
		ClaimantPersonalInfoID,
		LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + 
						 CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
	   COALESCE(r.Relationship, ' '),
	   COALESCE(lc.LegalCapacity, ' '),
	   ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) % 2
FROM tblClaimantRelatedParty crp
	LEFT JOIN tblRelationship r on crp.RelationshipID = r.RelationShipID
	LEFT JOIN tblLegalCapacity lc ON crp.LegalCapacityID = lc.LegalCapacityID
WHERE ClaimantPersonalInfoID IS NOT NULL

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'

INSERT INTO @CrpTable
SELECT DISTINCT ClaimantPersonalInfoID,
		COALESCE((SELECT N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0; width:200px;">
						   <tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Related Party Name</th>
						   <th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Relationship</th>
						   <th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Legal Capacity</th>' + 			
CAST ( (
SELECT CASE WHEN Remainder = 1 THEN @TDStyleW 
			ELSE @tdStyleB 
			END AS 'td/@style', 
	   (SELECT COALESCE(RelatedPartyName, '')) as td, '',
	   CASE WHEN Remainder = 1 THEN @TDStyleW 
			ELSE @tdStyleB 
			END AS 'td/@style', 
	   (SELECT COALESCE(Relationship, '')) as td, '',
	   CASE WHEN Remainder = 1 THEN @TDStyleW 
			ELSE @tdStyleB 
			END AS 'td/@style', 
	   (SELECT COALESCE(LegalCapacity, '')) as td, ''

FROM @ClaimantRelatedPartyInfo crpi
WHERE crpi.ClaimantPersonalInfoID = rdc.ClaimantPersonalInfoID
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) )) +
	N'</table>' +  N'<br/>'	, '') 
FROM vRequiredDocsByClaimant rdc

	RETURN



END
GO
GRANT VIEW DEFINITION ON  [dbo].[mtvRelatedPartyInfo] TO [base_user]
GRANT SELECT ON  [dbo].[mtvRelatedPartyInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[mtvRelatedPartyInfo] TO [power_user]
GRANT SELECT ON  [dbo].[mtvRelatedPartyInfo] TO [power_user]
GO
