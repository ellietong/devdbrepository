SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[mtvMissingClaimantInfo]
(@ClaimantPersonalInfoID int)
RETURNS 
@MissingClaimantInformation TABLE
(

	ClaimantPersonalInfoID int primary key NOT NULL,
	Missing NVARCHAR(MAX)
)
AS
BEGIN

DECLARE @Product table (ClaimantPersonalInfoID int, ProductEntered int)
INSERT INTO @Product 
SELECT ce.ClaimantPersonalInfoID, CASE WHEN cp.ProductID IS NULL OR cp.StartDate IS NULL OR cp.EndDate IS NULL  THEN 0  ELSE 1  END AS productentered
FROM tblClaimantEmployment ce 
	LEFT JOIN tblClaimantProduct cp on cp.ClaimantEmploymentID =ce.ClaimantEmploymentID 
WHERE ce.IsPrimary = 1 
GROUP BY ce.ClaimantPersonalInfoID , 
	CASE WHEN cp.ProductID IS NULL OR cp.StartDate IS NULL OR cp.EndDate IS NULL  THEN 0  ELSE 1  end 
 

DECLARE @CurrentClaimantInfo TABLE  
(ClaimantPersonalInfoID bigint, 
 SSN bit, 
 BirthDate bit, 
 --Disease bit, 
 FileDate bit,
 DiagnosisDate bit,  
 ProductEntered bit, 
 LegalCapacity bit)
INSERT INTO @CurrentClaimantInfo
SELECT DISTINCT c.ClaimantPersonalInfoID,
				CASE WHEN socialsecuritynumber is NULL THEN 0 ELSE 1 END AS HasSSN, 
				CASE WHEN birthdate is NULL THEN 0 ELSE 1 END AS HasBirthDate,
				CASE WHEN FileDate IS NULL THEN 0 ELSE 1 END AS FileDate,
				CASE WHEN CONVERT(date, DiseaseDiagnosisDate) IS NULL THEN 0 ELSE 1 END AS HasDxDate, 
				CASE WHEN COALESCE(p.ProductEntered, 0) = 0 THEN 0  ELSE 1 END AS HasProductEntered,  
				CASE WHEN DeceasedDate IS NULL THEN 1 WHEN DeceasedDate IS NOT NULL AND LegalCapacityID IS NULL THEN 0 END AS HasLegalCapacity
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimantSettlement cs ON cs.ClaimLawsuitID=cl.ClaimLawsuitID
	LEFT JOIN (
			SELECT DISTINCT ce.ClaimantPersonalInfoID, 1 AS ProductEntered
			FROM tblClaimantEmployment ce 
				INNER JOIN tblClaimantProduct cp on ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
			WHERE ce.IsPrimary = 1
				AND cp.ProductID IS NOT NULL 
				AND cp.StartDate IS NOT NULL 
				AND cp.EndDate IS NOT NULL 	
			 ) p	 ON c.claimantpersonalinfoid =p.ClaimantPersonalInfoID
	LEFT JOIN (SELECT cd1.* 
			   FROM tblClaimDisease cd1 
					INNER JOIN tbldisease d1 on d1.diseaseid = cd1.diseaseid 
			   WHERE PrimaryDisease = 1 
					AND highleveldiseaseid <> 5) cdi ON cdi.ClaimID = c.ClaimID
	INNER JOIN tblClaimLawsuitStatusTracking cls ON cls.ClaimLawsuitID = cl.ClaimLawsuitID 
	LEFT JOIN (SELECT * 
				FROM tblClaimantRelatedParty 
				WHERE LegalCapacityID IN (1,2,3,4,5,6,7,8,15,17,30,31))crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID 
	INNER JOIN vRequiredDocsByClaimant rdc		 ON c.ClaimantPersonalInfoID = rdc.ClaimantPersonalInfoID


DECLARE @MissingClaimantInfo  TABLE  (ID int NOT NULL IDENTITY(1,1), ClaimantPersonalInfoID bigint, MissingClaimantInformation nvarchar(500), Remainder int)
INSERT INTO @MissingClaimantInfo
SELECT ClaimantPersonalInfoID, 'Social Security Number', NULL
FROM @CurrentClaimantInfo
WHERE SSN = 0
UNION
SELECT ClaimantPersonalInfoID, 'Birth Date', NULL
FROM @CurrentClaimantInfo
WHERE BirthDate = 0
UNION
SELECT ClaimantPersonalInfoID, 'File Date', NULL
FROM @CurrentClaimantInfo
WHERE FileDate = 0
UNION
SELECT ClaimantPersonalInfoID, 'Diagnosis Date', NULL
FROM @CurrentClaimantInfo
WHERE DiagnosisDate = 0
UNION
SELECT ClaimantPersonalInfoID, 'Product Entered', NULL
FROM @CurrentClaimantInfo
WHERE ProductEntered = 0
UNION
SELECT ClaimantPersonalInfoID, 'Legal Capacity', NULL
FROM @CurrentClaimantInfo
WHERE LegalCapacity = 0

--Calculate the remainder fOR every-other row highlighting
UPDATE @MissingClaimantInfo
SET Remainder = fdp.Remainder 
FROM (SELECT ID, (ID % 2) AS Remainder FROM @MissingClaimantInfo) fdp
INNER JOIN @MissingClaimantInfo ofdp ON fdp.ID = ofdp.ID
WHERE ofdp.ID = fdp.ID

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'

INSERT INTO @MissingClaimantInformation
SELECT DISTINCT cci.ClaimantPersonalInfoID,
		COALESCE((SELECT N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0; width:200px;">' + 	
		CAST((SELECT CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleB END AS 'td/@style', (SELECT COALESCE(MissingClaimantInformation, '')) as td, ''
		FROM @MissingClaimantInfo mci
		WHERE cci.ClaimantPersonalInfoID = mci.ClaimantPersonalInfoID 
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) )) +  N'</table>' +  N'<br/>'	, 'All required claimant information submitted.') AS Missing
FROM @CurrentClaimantInfo cci
	
	RETURN 
END


GO
GRANT VIEW DEFINITION ON  [dbo].[mtvMissingClaimantInfo] TO [base_user]
GRANT SELECT ON  [dbo].[mtvMissingClaimantInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[mtvMissingClaimantInfo] TO [power_user]
GRANT SELECT ON  [dbo].[mtvMissingClaimantInfo] TO [power_user]
GO
