SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[mtvMissingSettlementDocumentation]
(
	@SetPymtID int
)
RETURNS 
@MissingDocuments TABLE
(
	SetPymtID int,
	MissingDocs NVARCHAR(MAX)
)
AS
BEGIN

DECLARE @CurrentDocumentation TABLE  
(SetPymtID int, 
 ClaimantIDed bit, 
 DxMedicals bit, 
 DODCert int, 
 EstatePaper int, 
 ExpoHist bit, 
 SettleConfirm bit, 
 ProductID bit,
 Release bit)

INSERT INTO @CurrentDocumentation
SELECT DISTINCT spd.SetPymtID, 
				COALESCE(sd.ClaimantIDed, 0) AS ClaimantIDed,
				COALESCE(sd.DxMedicals, 0) AS DxMedicals,
				CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE(sd.DODCert, 0) END AS DODCert, 
				CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE(sd.EstatePapers, 0) END AS Estatepaper,  
				COALESCE(sd.EmployExpoHist, 0) AS EmployExpoHist, 
				COALESCE(sd.settlementconfirmation, 0) AS SettlementConfirmation,
				COALESCE(sd.productid,0) as ProductID,
				CASE WHEN (rdc.ClaimantPersonalInfoID IS NOT NULL OR ExecutedReleaseDate IS NOT NULL) THEN 1 ELSE 0 END AS ReleaseReceived
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs			 ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN tblSettlementDocumentation sd		 ON sd.SetPymtID = spd.SetPymtID
	INNER JOIN vRequiredDocsByClaimant rdc		 ON c.ClaimantPersonalInfoID = rdc.ClaimantPersonalInfoID


DECLARE @MissingDocumentation TABLE  (ID int, SetPymtID bigint, MissingDocumentation nvarchar(500), Remainder int)

INSERT INTO @MissingDocumentation
SELECT ROW_NUMBER() OVER (ORDER BY (SELECT NULL)), *, 
ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) % 2
FROM 
(
SELECT SetPymtID, 'Claimant Identified' AS MissingDocs
FROM @CurrentDocumentation
WHERE ClaimantIDed = 0
UNION
SELECT SetPymtID,'Diagnosing Medicals'
FROM @CurrentDocumentation
WHERE DxMedicals = 0
UNION
SELECT cd.SetPymtID,'Death Cerificate'
FROM @CurrentDocumentation cd
INNER JOIN vRequiredDocsByClaimant rdc ON cd.SetPymtID = rdc.SetPymtID
WHERE rdc.DODCert = 0
UNION
SELECT cd.SetPymtID, 'Estate Paperwork'
FROM @CurrentDocumentation cd
INNER JOIN vRequiredDocsByClaimant rdc ON cd.SetPymtID = rdc.SetPymtID
WHERE rdc.EstatePapers = 0
UNION
SELECT SetPymtID,'Employment/Exposure History'
FROM @CurrentDocumentation
WHERE ExpoHist = 0
UNION
SELECT SetPymtID,'Settlement Confirmation'
FROM @CurrentDocumentation
WHERE SettleConfirm = 0
UNION
SELECT cd.SetPymtID,'Executed Release'
FROM @CurrentDocumentation cd
INNER JOIN vRequiredDocsByClaimant rdc ON cd.SetPymtID = rdc.SetPymtID
WHERE ReleaseReceived = 0
UNION
SELECT SetPymtID,'Product Identification'
FROM @CurrentDocumentation
WHERE ProductID = 0
) d

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'

INSERT INTO @MissingDocuments
SELECT DISTINCT sd2.SetPymtID,
		COALESCE((SELECT N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0; width:200px;">' + 	
  CAST ( (
SELECT	CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleB END AS 'td/@style', (SELECT COALESCE(MissingDocumentation, '')) as td, ''
	FROM @MissingDocumentation sd1
	WHERE sd1.SetPymtID = sd2.SetPymtID 
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) )) +  N'</table>' +  N'<br/>'	, 'All required documents submitted.') AS Missing
FROM @CurrentDocumentation sd2

	RETURN 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[mtvMissingSettlementDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[mtvMissingSettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[mtvMissingSettlementDocumentation] TO [power_user]
GRANT SELECT ON  [dbo].[mtvMissingSettlementDocumentation] TO [power_user]
GO
