SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO




CREATE FUNCTION [dbo].[fnGetRelatedPartyInfo]
(@ClaimantPersonalInfoID bigint)

RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @ClaimantRelatedPartyInfo  TABLE  (ID int NOT NULL IDENTITY(1,1), 
										   RelatedPartyName nvarchar(500), 
										   RelationShip nvarchar(100),
										   LegalCapacity nvarchar(100),
										   Remainder int
										   )
INSERT INTO @ClaimantRelatedPartyInfo
SELECT LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + 
						 CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
	   COALESCE(r.Relationship, ' '),
	   COALESCE(lc.LegalCapacity, ' '),
	   NULL
FROM tblClaimantRelatedParty crp
	INNER JOIN tblRelationship r on crp.RelationshipID = r.RelationShipID
	INNER JOIN tblLegalCapacity lc ON crp.LegalCapacityID = lc.LegalCapacityID
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID

--Calculate the remainder for every-other row highlighting
UPDATE @ClaimantRelatedPartyInfo
SET Remainder = crp.Remainder 
FROM (SELECT ID, (ID % 2) AS Remainder 
		FROM @ClaimantRelatedPartyInfo) crp
	INNER JOIN @ClaimantRelatedPartyInfo crpi ON crp.ID = crpi.ID


DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'

DECLARE @CrpTable nvarchar(max) = ''

SET @CrpTable = @CrpTable + N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0;">' + 
                          + N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Related Party Name</th>
								  <th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Relationship</th>
								  <th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Legal Capacity</th>'
 +
	
	
CAST ( (
SELECT CASE WHEN Remainder = 1 THEN @TDStyleW 
			ELSE @tdStyleB 
			END AS 'td/@style', 
	   (SELECT COALESCE(RelatedPartyName, '')) as td, '',
	   CASE WHEN Remainder = 1 THEN @TDStyleW 
			ELSE @tdStyleB 
			END AS 'td/@style', 
	   (SELECT COALESCE(Relationship, '')) as td, '',
	   CASE WHEN Remainder = 1 THEN @TDStyleW 
			ELSE @tdStyleB 
			END AS 'td/@style', 
	   (SELECT COALESCE(LegalCapacity, '')) as td, ''

FROM @ClaimantRelatedPartyInfo
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	

	RETURN @CrpTable



END




GO
