CREATE TYPE [dbo].[bigint_list_tbltype] AS TABLE
(
[n] [bigint] NULL
)
GO
GRANT CONTROL ON TYPE:: [dbo].[bigint_list_tbltype] TO [base_user]
GRANT VIEW DEFINITION ON TYPE:: [dbo].[bigint_list_tbltype] TO [base_user]
GRANT REFERENCES ON TYPE:: [dbo].[bigint_list_tbltype] TO [base_user]
GRANT CONTROL ON TYPE:: [dbo].[bigint_list_tbltype] TO [power_user]
GRANT VIEW DEFINITION ON TYPE:: [dbo].[bigint_list_tbltype] TO [power_user]
GRANT REFERENCES ON TYPE:: [dbo].[bigint_list_tbltype] TO [power_user]
GO
