SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmPrimaryDefenseCounsel]
as

Select ResponsibleDefenseCounselID, DefenseCounsel as ResponsibleDefenseCounsel
From tblDefenseCounselState dcs 
Inner Join tblDefenseCounsel d on dcs.ResponsibleDefenseCOunselID = d.DefenseCounselID
Where IsActiveCounsel = 1
Group By ResponsibleDefenseCounselID, DefenseCounsel 

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPrimaryDefenseCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPrimaryDefenseCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPrimaryDefenseCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPrimaryDefenseCounsel] TO [power_user]
GO
