SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryAcuityClaimantDataFeed] 

AS
SET NOCOUNT ON;

PRINT 'NAME|ID|LAW_FIRM_KEY|LAW_FIRM_NAME|STATUS|STATUS_DATE|STATE_KEY|LAWSUIT_ID|JRSDTN_KEY'

SELECT LastName + ' ' + FirstName AS NAME, 
	cpi.ReferenceID AS ID, 
	AcuityKey AS LAW_FIRM_KEY, 
	dc.DefenseCounsel AS LAW_FIRM_NAME, 
	cls.ClaimLawsuitStatus AS [STATUS], 
	CONVERT(DATE, StatusDate) AS STATUS_DATE, 
	'' AS STATE_KEY, 
	cl.LawsuitID AS LAWSUIT_ID, 
	1 AS JRSDTN_Key
FROM tblClaimantPersonalInfo cpi
	INNER JOIN tblClaim c							ON cpi.ClaimantPersonalInfoID=c.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl					ON cl.ClaimID = c.ClaimID
	INNER JOIN tblLawsuit l							ON l.LawsuitID=cl.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst	ON clst.ClaimLawsuitID=cl.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatus cls			ON clst.ClaimLawsuitStatusID=cls.ClaimLawsuitStatusID
	INNER JOIN tblDefenseCounselAddress dca			ON dca.DefenseCounselAddressID=l.DefenseCounselAddressID
	INNER JOIN tblState s							ON s.StateID=l.StateID
	INNER JOIN tblDefenseCounsel dc					ON dca.DefenseCounselID = dc.DefenseCounselID
WHERE ProcessedDate = CONVERT(DATE,GETDATE()) 
	AND clst.ClaimLawsuitstatusID=22
GO
GRANT VIEW DEFINITION ON  [dbo].[qryAcuityClaimantDataFeed] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryAcuityClaimantDataFeed] TO [base_user]
GO
