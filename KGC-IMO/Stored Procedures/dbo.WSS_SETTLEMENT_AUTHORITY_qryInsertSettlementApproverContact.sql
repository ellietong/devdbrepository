SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertSettlementApproverContact]
(	@SettlementAuthorityID bigint, 
	@SettlementApproverContactID bigint,
	@ResponseWindow int,
	@ApprovalRequired bit
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO tblSettlementAuthorityApproverContact
    (	SettlementAuthorityID,
		SettlementApproverContactID, 
		ResponseWindowDays, 
		ApprovalRequired
	)
    SELECT	@SettlementAuthorityID,
			@SettlementApproverContactID,
			@ResponseWindow,
			@ApprovalRequired
		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertSettlementApproverContact] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertSettlementApproverContact] TO [base_user]
GO
