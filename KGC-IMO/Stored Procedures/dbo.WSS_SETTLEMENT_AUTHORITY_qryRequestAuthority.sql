SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestAuthority]
(@Emailaddress nvarchar(250) = null, @SettlementGroupID bigint,
 @DemandAmount money, @AuthorityAmount money, @Comment nvarchar(MAX) = NULL, @DocumentID bigint,
 @ResponseDueDate datetime)
AS
BEGIN
	
	----CREATE SETTLEMENT RECORD (DATE IS NULL SINCE SETTLEMENT HAS NOT BEEN APPROVED)
	--INSERT INTO tblClaimantSettlement
	--(ClaimLawsuitID, DemandAmount, AuthorityAmount)
	--SELECT @ClaimLawsuitID, @DemandAmount, @AuthorityAmount
	
	--INSERT SETTLEMENT AUTHORITY RECORD
	INSERT INTO tblSettlementAuthority
	(SettlementGroupID, AuthorityRequestedBy, AuthorityRequestedDate, DemandAmount, AuthorityAmount, Comment, DocumentID, ResponseDueDate)
	SELECT @SettlementGroupID, @Emailaddress, GETDATE(), @DemandAmount, @AuthorityAmount, @Comment, @DocumentID, @ResponseDueDate
	SELECT @@IDENTITY
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestAuthority] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestAuthority] TO [base_user]
GO
