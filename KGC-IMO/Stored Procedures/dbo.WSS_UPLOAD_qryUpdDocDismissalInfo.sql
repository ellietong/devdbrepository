SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryUpdDocDismissalInfo]
    (
      @DocumentID INT ,
      @DocumentTypeID INT ,
      @DocumentSourceID INT ,
      @FirmID INT ,
      @Accept BIT ,
      @DismissalTypeID INT ,
      @DismissalDate DATETIME ,
      @EmailAddress NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON

    UPDATE  tblDocuments
    SET     ClaimantDocumentTypeID = @DocumentTypeID ,
            DocumentSourceID = @DocumentSourceID ,
		--FirmID = @FirmID,
            DateModified = GETDATE() ,
            ModifiedBy = @EmailAddress ,
		--DismissalProcessDate = CASE WHEN @DocumentTypeID = 16 AND DismissalProcessDate IS NULL THEN GETDATE() WHEN @DocumentTypeID <> 16 AND DismissalProcessDate IS NOT NULL THEN NULL ELSE DismissalProcessDate END,
		--ClaimLawsuitStatusID = CASE WHEN @DocumentTypeID = 16 THEN @DismissalTypeID ELSE NULL END,
		--DismissalDate = @DismissalDate,
            Accepted = CASE WHEN COALESCE(Accepted, 0) = 1 THEN Accepted
                            WHEN @Accept = 0
                                 AND COALESCE(Accepted, 0) = 0 THEN NULL
                            ELSE 1
                       END ,
            AcceptedBy = CASE WHEN AcceptedBy IS NOT NULL THEN AcceptedBy
                              WHEN @Accept = 1 THEN @EmailAddress
                              ELSE NULL
                         END ,
            DateAccepted = CASE WHEN DateAccepted IS NOT NULL
                                THEN DateAccepted
                                WHEN @Accept = 1 THEN GETDATE()
                                ELSE NULL
                           END
    WHERE   DocumentID = @DocumentID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocDismissalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocDismissalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocDismissalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocDismissalInfo] TO [power_user]
GO
