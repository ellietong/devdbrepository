SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse_NotificationContacts] 
(@SettlementAuthorityResponseID bigint)
AS
SET NOCOUNT ON

BEGIN


/*
	TEST VARS
*/
--DECLARE @SettlementAuthorityResponseID bigint = 63
DECLARE @SettlementAuthorityID bigint = (SELECT SettlementAuthorityID FROM tblSettlementAuthorityResponse WHERE SettlementAuthorityResponseID = @SettlementAuthorityResponseID)

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)


DECLARE @SettlementGroup nvarchar(500) = (SELECT SettlementGroup
										FROM tblSettlementAuthority sa
										INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
										WHERE sa.SettlementAuthorityID = @SettlementAuthorityID)

										
SET @Subject = 	'IMO Settlement Authority Response ' + CONVERT(varchar, getdate(), 107) + ': ' + COALESCE(@SettlementGroup,'') 

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'
 DECLARE @Status nvarchar(100) 
 DECLARE @Comment nvarchar(MAX)
 SELECT @Status = COALESCE(SettlementAuthorityStatus, 'Unknown Status'), @Comment = COALESCE(sar.Comment, 'No comment.')
						FROM tblSettlementAuthorityResponse sar 
						INNER JOIN tblSettlementAuthorityStatus sas ON sar.SettlementAuthoritystatusID = sas.SettlementAuthorityStatusID 
						WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID
SELECT @Status, @Comment						
--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
set @BodyMessage = @BodyMessage + '<H4>' + (SELECT CASE WHEN p.Email LIKE '%resolute-midwest.com' THEN DefenseCounsel + ' - Midwest' 
														WHEN p.Email LIKE '%resolute-ne.com' THEN DefenseCounsel + ' - New England' 
														ELSE COALESCE(dca.DefenseCounsel, 'KCIC') END
					FROM tblSettlementAuthority sa
					LEFT JOIN tblSettlementAuthorityResponse sar  ON sa.SettlementAuthorityID = sar.SettlementAuthorityID
					LEFT JOIN vtblPerson p on sar.CreatedBy = p.Email
					LEFT JOIN tblDefenseCounselAddress dca ON p.FirmAddressID = dca.DefenseCounselAddressID
					WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID) +  ' has posted the following response with status ' + @Status +
					
					 +  ':<br/><blockquote>' + @Comment + '</blockquote>'+
'To view additional information regarding this authority request click ' + 
'<a href="https://www.kcicextranet.com/clients/imo/cp/Pages/SettlementAuthority.aspx?SettlementAuthorityID=' + CONVERT(nvarchar, @SettlementAuthorityID) + '">here</a>. </H4>'

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

  
DECLARE @RecipientList nvarchar(500)
DECLARE @CCList nvarchar(MAX) --= 'bzimmerman@leaderberkon.com'

DECLARE @ExemptFirmIDs integer_list_tbltype
INSERT INTO @ExemptFirmIDs
SELECT 374 --Leader Berkon
--UNION
--SELECT 269 --IMO

DECLARE @Recipients TABLE (PersonID int)

--Insert creator of settlement authority request overall
INSERT INTO @Recipients
SELECT PersonID 
FROM tblSettlementAuthority sa 
INNER JOIN vtblPerson p ON sa.AuthorityRequestedBy = p.Email 
WHERE SettlementAuthorityID = @SettlementAuthorityID

--Insert Notification contacts
INSERT INTO @Recipients
SELECT DISTINCT p.PersonID
FROM tblSettlementAuthorityResponse sar
LEFT JOIN tblSettlementAuthorityResponse sarParent ON sar.ParentSettlementAuthorityResponseID = sarParent.SettlementAuthorityResponseID
INNER JOIN tblSettlementAuthority sa ON sar.SettlementAuthorityID = sa.SettlementAuthorityID
INNER JOIN tblSettlementAuthorityNotificationContact sanc ON sa.SettlementAuthorityID = sanc.SettlementAuthorityID
INNER JOIN tblSettlementNotificationContact snc ON sanc.SettlementNotificationContactID = snc.SettlementNotificationContactID
INNER JOIN vtblPerson p ON snc.PersonID = p.PersonID
WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID

--Insert Barbara (we can add Joe here if necessary)
INSERT INTO @Recipients
SELECT 4117 --bzimmerman@leaderberkon.com



--Get the unique email addresses contained within
SET @RecipientList=(CAST((SELECT DISTINCT Email + ';'
					FROM @Recipients r
					INNER JOIN vtblPerson p ON r.PersonID = p.PersonID
					for xml path(''), type) AS NVARCHAR(MAX)))
--QC Recipient List
SET @BodyMessage = @BodyMessage + '<br/>Recipients: ' + COALESCE(@RecipientList, '') --+ '</br>CC: ' + @CCList

PRINT @Bodymessage
PRINT @RecipientList
IF(LEN(@RecipientList) > 0)
BEGIN
	--PRINT @RecipientList
	--Execute stored procedure to send the email to that firm
	EXEC msdb.dbo.sp_send_dbmail
	--The Profile_Name sends from [client]@kcicllc.com email address
	@profile_name='Claims',
	--@recipients = 'scottc@kcicllc.com',
	@recipients = 'imo@kcic.com',--'crookc@kcicllc.com',--@RecipientList,
	@copy_recipients = '',--@CCList--'crookc@kcicllc.com',
	--@recipients = 'amanp@kcicllc.com',
	@subject = @subject,
	@body = @BodyMessage,
	@body_format = 'html'

END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse_NotificationContacts] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse_NotificationContacts] TO [base_user]
GO
