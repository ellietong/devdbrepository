SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[m_UpdDocAmazonKey]
	-- Add the parameters for the stored procedure here
(@DocumentID int, @AmazonKey nvarchar(MAX))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblDocuments
	SET AmazonKey = @AmazonKey
	WHERE DocumentID = @DocumentID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_UpdDocAmazonKey] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_UpdDocAmazonKey] TO [base_user]
GO
