SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_REPORTS_qrySettlementGroupList]
@emailaddress nvarchar(250), @SettlementGroupTypeID int = NULL
AS
SET NOCOUNT ON

--DECLARE @EmailAddress nvarchar(250) = 'owenss@kcic.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE sg.SettlementGroup IS NOT NULL
						AND PACE_SettlementGroupID IS NULL '

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max) = ''

IF @SettlementGroupTypeID = 1
BEGIN
		SET @JoinString = ' INNER JOIN vMissingSettlementDocumentation msd on cs.SettlementGroupID = msd.SettlementGroupID'
END

IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = @JoinString
ELSE
BEGIN
	SET @JoinString = @JoinString + ' INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID					
						INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
						LEFT JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						LEFT JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

DECLARE @QueryString NVARCHAR(MAX) = ''
SET @QueryString = '
SELECT DISTINCT cs.SettlementGroupID, sg.SettlementGroup
FROM tblClaimantSettlement cs
	INNER JOIN tblSettlementGroup sg on cs.SettlementGroupID = sg.SettlementGroupID
'
+ @JoinString 
+ @WhereString +
'
UNION
SELECT NULL, ''''
ORDER BY sg.SettlementGroup'

--PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
