SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] ( @LawsuitID BIGINT )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

	
        SELECT  cl.ClaimLawsuitID ,
                COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '')
                + COALESCE(' ' + MiddleName, '') AS ClaimantName ,
                ClaimLawsuitStatus ,
                CONVERT(NVARCHAR(250), ( CASE WHEN ServiceDate IS NULL
                                              THEN CASE WHEN FileDate IS NULL
                                                        THEN CASE
                                                              WHEN ReceivedDate IS NULL
                                                              THEN '1/1/1960'
                                                              ELSE ReceivedDate
                                                             END
                                                        ELSE FileDate
                                                   END
                                              ELSE ServiceDate
                                         END ), 101) AS ServiceDate
        FROM    tblClaimLawsuit cl
                INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
                INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
                INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
                INNER JOIN tblClaimLawsuitStatus cls ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
                INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
        WHERE   cl.LawsuitID = @LawsuitID
                AND IsPrimaryStatus = 1
        ORDER BY LastName
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [power_user]
GO
