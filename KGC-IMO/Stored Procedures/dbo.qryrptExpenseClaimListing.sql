SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptExpenseClaimListing] (@AllocationID int, @ExpenseTotal money)

AS
Set NoCount On



--Declare @AllocationID int
--Set @AllocationID=22


DECLARE @AllocationDetails nvarchar(200)
SET @AllocationDetails = (Select Convert(nvarchar(50),[Month]) + ' ' + Convert(nvarchar(50),[Year]) + ' - ' + Convert(nvarchar(50),BillNumber)
FROM tblallocation
Where AllocationID=@AllocationID)

DECLARE @TotalAllocatedExpense money
SET @TotalAllocatedExpense = @ExpenseTotal--(SELECT SUM(Cost1)
--FROM tblOccurrence o
--INNER JOIN tblAllocationOccurrenceGroupInclude aog on aog.OccurrenceGroupID=o.OccurrenceGroupID
--where AllocationID=@AllocationID)

SELECT de.[Start] as StartDate, de.[End] as EndDate, de.Percentage, @TotalAllocatedExpense AS TotalAllocatedExpense, (@TotalAllocatedExpense* de.Percentage) AS AllocatedExpense, @AllocationDetails as AllocationDetails
FROM m_tblExpenseModel de
ORDER BY de.Start DESC
--Return

--GO


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExpenseClaimListing] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExpenseClaimListing] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExpenseClaimListing] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExpenseClaimListing] TO [power_user]
GO
