SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceSummary] (@emailaddress nvarchar(250))	
AS
SET NOCOUNT ON

--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = ''

DECLARE @UnpaidInvoice TABLE (InvoiceID int, IsFullyPaid bit)
INSERT INTO @UnpaidInvoice
	SELECT i.InvoiceID, 
		CASE WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 0 THEN 0 
			 WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 1 THEN 1
															 ELSE IsFullyPaid END AS IsFullyPaid 
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblPaymentLegalBill pd on i.InvoiceID = pd.InvoiceID
	GROUP BY i.InvoiceID, CASE WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 0 THEN 0 WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 1 THEN 1 ELSE IsFullyPaid END

DECLARE @firmid int
SET @firmid = (SELECT CASE WHEN RIGHT(@emailaddress, 15) = 'smithmurphy.com' THEN '115' ELSE FirmID END 
			   FROM vtblperson 
			   WHERE Email = @emailaddress)

IF (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @firmid = 132) --Update this so it shows all for Vicki, too.
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		CASE WHEN OriginalInvoiceNumber IS NULL THEN 'Unknown' 
												ELSE OriginalInvoiceNumber END AS OriginalInvoiceNumber, 
		MAX(Period) as MaxPeriod,
		SubmittedDate, 
		MAX(IMOApprovalDate) as MaxApprovedDate, 
		CASE WHEN s.LegalBillStatus IS NULL THEN 'Unknown' 
			 WHEN u.IsFullyPaid = 1			THEN 'Paid'
			 WHEN NavPaidInvoice = 1		THEN 'Paid By Navigant' 
											ELSE s.LegalBillStatus END AS 'Status', 
		SourceDate as PaidDate,
		SUM(OriginalTotal) AS OriginalInvoiceTotal, 
		CASE WHEN AppealFlag = 1 THEN SUM(AppealTotal) 
								 ELSE SUM(AdjustedTotal) END AS FinalizedInvoiceTotal
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d	ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m					ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s		ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u				ON i.InvoiceID = u.InvoiceID
		LEFT JOIN tblPaymentLegalBill plb	    ON plb.InvoiceID = i.InvoiceID
		LEFT JOIN tblCheck c				    ON c.CheckID = plb.CheckID
	WHERE COALESCE(submitteddate, IMOapprovaldate, period) >= (GETDATE()-365)
		OR u.IsFullyPaid = 0 
		OR i.NavPaidInvoice = 0
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel, Matter, SubmittedDate, 
		CASE WHEN OriginalInvoiceNumber IS NULL THEN 'Unknown' ELSE OriginalInvoiceNumber END, 
		CASE WHEN s.LegalBillStatus IS NULL THEN 'Unknown' WHEN u.IsFullyPaid = 1 THEN 'Paid' WHEN NavPaidInvoice = 1 THEN 'Paid By Navigant' ELSE s.LegalBillStatus END,
		SourceDate, AppealFlag
	ORDER BY SubmittedDate DESC, DefenseCounsel, Matter
END
ELSE IF ((SELECT PersonID FROM vtblPerson WHERE Email = @emailaddress) IN (1405, 1406))
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		CASE WHEN OriginalInvoiceNumber IS NULL THEN 'Unknown' 
												ELSE OriginalInvoiceNumber END AS OriginalInvoiceNumber, 
		MAX(Period) AS MaxPeriod,
		SubmittedDate, 
		MAX(IMOApprovalDate) AS MaxApprovedDate, 
		CASE WHEN s.LegalBillStatus IS NULL THEN 'Unknown' 
			 WHEN u.IsFullyPaid = 1			THEN 'Paid' 
			 WHEN NavPaidInvoice = 1		THEN 'Paid By Navigant' 
											ELSE s.LegalBillStatus END AS 'Status', 
		SourceDate as PaidDate,
		SUM(OriginalTotal) AS OriginalInvoiceTotal, 
		CASE WHEN AppealFlag = 1 THEN SUM(AppealTotal) 
								 ELSE SUM(AdjustedTotal) END AS FinalizedInvoiceTotal
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d	ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m					ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s		ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u				ON i.InvoiceID = u.InvoiceID
		LEFT JOIN tblPaymentLegalBill plb	    ON plb.InvoiceID = i.InvoiceID
		LEFT JOIN tblCheck c				    ON c.CheckID = plb.CheckID
	WHERE i.DefenseCounselAddressID = 0
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel, Matter, SubmittedDate,
		CASE WHEN OriginalInvoiceNumber IS NULL THEN 'Unknown' ELSE OriginalInvoiceNumber END, 
		CASE WHEN s.LegalBillStatus IS NULL THEN 'Unknown' WHEN u.IsFullyPaid = 1 THEN 'Paid' WHEN NavPaidInvoice = 1 THEN 'Paid By Navigant' ELSE s.LegalBillStatus END,
		SourceDate, AppealFlag
	ORDER BY SubmittedDate DESC, DefenseCounsel, Matter
END
ELSE IF @firmid IN (100,117)
--Celba LLC and Steven Celba LLC invoices
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		CASE WHEN OriginalInvoiceNumber IS NULL THEN 'Unknown' 
												ELSE OriginalInvoiceNumber END AS OriginalInvoiceNumber, 
		MAX(Period) AS MaxPeriod,
		SubmittedDate, 
		MAX(IMOApprovalDate) AS MaxApprovedDate, 
		CASE WHEN s.LegalBillStatus IS NULL THEN 'Unknown' 
			 WHEN u.IsFullyPaid = 1			THEN 'Paid' 
			 WHEN NavPaidInvoice = 1		THEN 'Paid By Navigant' 
											ELSE s.LegalBillStatus END AS 'Status', 
		SourceDate as PaidDate,
		SUM(OriginalTotal) AS OriginalInvoiceTotal, 
		CASE WHEN AppealFlag = 1 THEN SUM(AppealTotal) 
								 ELSE SUM(AdjustedTotal) END AS FinalizedInvoiceTotal
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d	ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m					ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s		ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u				ON i.InvoiceID = u.InvoiceID
		LEFT JOIN tblPaymentLegalBill plb	    ON plb.InvoiceID = i.InvoiceID
		LEFT JOIN tblCheck c				    ON c.CheckID = plb.CheckID

	WHERE (COALESCE(submitteddate, IMOapprovaldate, period) >= (GETDATE()-365)
		OR u.IsFullyPaid = 0 
		OR i.NavPaidInvoice = 0) 
		AND i.DefenseCounselAddressID IN (SELECT DefenseCounselAddressID 
										  FROM tblDefenseCounselAddress 
										  WHERE DefenseCounselID IN (100,117))
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel, Matter, SubmittedDate,
		CASE WHEN OriginalInvoiceNumber IS NULL THEN 'Unknown' ELSE OriginalInvoiceNumber END, 
		CASE WHEN s.LegalBillStatus IS NULL THEN 'Unknown' WHEN u.IsFullyPaid = 1 THEN 'Paid' WHEN NavPaidInvoice = 1 THEN 'Paid By Navigant' ELSE s.LegalBillStatus END,
		SourceDate, AppealFlag
	ORDER BY SubmittedDate DESC, DefenseCounsel, Matter
END
ELSE IF @firmid IN (115)
--Smith Murphy invoices --Smith Murphy users coded as part of Reed Smith Firm in order to view Reed Smith claimants, however should not see Reed Smith invoices
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		CASE WHEN OriginalInvoiceNumber IS NULL THEN 'Unknown' 
												ELSE OriginalInvoiceNumber END AS OriginalInvoiceNumber, 
		MAX(Period) AS MaxPeriod,
		SubmittedDate, 
		MAX(IMOApprovalDate) AS MaxApprovedDate, 
		CASE WHEN s.LegalBillStatus IS NULL THEN 'Unknown' 
			 WHEN u.IsFullyPaid = 1			THEN 'Paid' 
			 WHEN NavPaidInvoice = 1		THEN 'Paid By Navigant' 
											ELSE s.LegalBillStatus END AS 'Status',
		SourceDate as PaidDate, 
		SUM(OriginalTotal) AS OriginalInvoiceTotal, 
		CASE WHEN AppealFlag = 1 THEN SUM(AppealTotal) 
								 ELSE SUM(AdjustedTotal) END AS FinalizedInvoiceTotal
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d	ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m					ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s		ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u				ON i.InvoiceID = u.InvoiceID
		LEFT JOIN tblPaymentLegalBill plb	    ON plb.InvoiceID = i.InvoiceID
		LEFT JOIN tblCheck c				    ON c.CheckID = plb.CheckID
	WHERE (COALESCE(submitteddate, IMOapprovaldate, period) >= (GETDATE()-365)
		OR u.IsFullyPaid = 0 
		OR i.NavPaidInvoice = 0) 
		AND i.DefenseCounselAddressID IN (SELECT DefenseCounselAddressID 
										  FROM tblDefenseCounselAddress 
										  WHERE DefenseCounselID IN (115))
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel, Matter, SubmittedDate,
		CASE WHEN OriginalInvoiceNumber IS NULL THEN 'Unknown' ELSE OriginalInvoiceNumber END, 
		CASE WHEN s.LegalBillStatus IS NULL THEN 'Unknown' WHEN u.IsFullyPaid = 1 THEN 'Paid' WHEN NavPaidInvoice = 1 THEN 'Paid By Navigant' ELSE s.LegalBillStatus END,
		SourceDate, AppealFlag
	ORDER BY SubmittedDate DESC, DefenseCounsel, Matter
END
ELSE
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		CASE WHEN OriginalInvoiceNumber IS NULL THEN 'Unknown' 
												ELSE OriginalInvoiceNumber END AS OriginalInvoiceNumber, 
		MAX(Period) AS MaxPeriod,
		SubmittedDate, 
		MAX(IMOApprovalDate) AS MaxApprovedDate, 
		CASE WHEN s.LegalBillStatus IS NULL THEN 'Unknown' 
			 WHEN u.IsFullyPaid = 1			THEN 'Paid' 
			 WHEN NavPaidInvoice = 1		THEN 'Paid By Navigant' 
											ELSE s.LegalBillStatus END AS 'Status', 
		SourceDate as PaidDate,
		SUM(OriginalTotal) AS OriginalInvoiceTotal, 
		CASE WHEN AppealFlag = 1 THEN SUM(AppealTotal) 
								 ELSE SUM(AdjustedTotal) END AS FinalizedInvoiceTotal
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d	ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m					ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s		ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u				ON i.InvoiceID = u.InvoiceID
		LEFT JOIN tblPaymentLegalBill plb	    ON plb.InvoiceID = i.InvoiceID
		LEFT JOIN tblCheck c				    ON c.CheckID = plb.CheckID
	WHERE (COALESCE(submitteddate, IMOapprovaldate, period) >= (GETDATE()-365)
		OR u.IsFullyPaid = 0 
		OR i.NavPaidInvoice = 0) 
		AND i.DefenseCounselAddressID IN (SELECT DISTINCT FirmAddressID 
										  FROM vtblPerson 
										  WHERE FirmID IN (SELECT FirmID 
														   FROM vtblPerson 
														   WHERE Email = @emailaddress))
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel, Matter, SubmittedDate,
		CASE WHEN OriginalInvoiceNumber IS NULL THEN 'Unknown' ELSE OriginalInvoiceNumber END, 
		CASE WHEN s.LegalBillStatus IS NULL THEN 'Unknown' WHEN u.IsFullyPaid = 1 THEN 'Paid' WHEN NavPaidInvoice = 1 THEN 'Paid By Navigant' ELSE s.LegalBillStatus END,
		SourceDate, AppealFlag
	ORDER BY SubmittedDate DESC, DefenseCounsel, Matter
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceSummary] TO [power_user]
GO
