SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryGetClaimantsByDocumentID] ( @DocumentID BIGINT )
AS 
    SET NOCOUNT ON

    SELECT  CONVERT(NVARCHAR(20), CPI.ClaimantPersonalInfoID) AS ClaimantID ,
            CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN ''
                                       ELSE FirstName + ' '
                                  END + CASE WHEN MiddleName IS NULL THEN ' '
                                             ELSE MiddleName + ' '
                                        END AS ClaimantName ,
            CONVERT(NVARCHAR(20), MultiClaimantDocumentID) AS MultiClaimantDocumentID
    FROM    tblClaimantPersonalInfo cpi
            INNER JOIN tblMultiClaimantDocument mcd ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
    WHERE   DocumentID = @documentID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetClaimantsByDocumentID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetClaimantsByDocumentID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetClaimantsByDocumentID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetClaimantsByDocumentID] TO [power_user]
GO
