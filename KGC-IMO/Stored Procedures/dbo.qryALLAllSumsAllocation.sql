SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE procedure [dbo].[qryALLAllSumsAllocation]
(@allocationid int, @previousallocationid int, @considerpreviousallocation bit, 
@strsortorder nvarchar(500), @triggerstartfield nvarchar(15), @triggerendfield nvarchar(15), 
@defensecosts nvarchar(50), @costfields nvarchar(100), @strwhere nvarchar(2000))
as

set nocount on

declare @strsql nvarchar(4000) -- temporary sql string variable
declare @colloop int -- variable used to loop through all columns
declare @maxcol int -- number of column records created
declare @policyid int -- temporary policy id variable
declare @policydateid int -- temporary policydateid variable
declare @occurrenceid int -- temporary occurrence id variable
declare @corporatehistoryid int -- temporary corporatehistoryid variable
declare @triggerstart datetime -- temporary trigger start variable
declare @triggerend datetime -- temporary trigger end variable
declare @colstart datetime -- temporary column start variable
declare @colend datetime -- temporary column end variable
declare @minattach money -- temporary minimum available attachment point variable
declare @defcost money -- temporary defense cost variable
declare @indcost money -- temporary indemnity cost variable
declare @maxrankindem int -- maximum indemnity rank for considered policies
declare @maxrankdef int -- maximum defense rank for considered policies
declare @indembypolicy money -- indemnity allocation by policy
declare @defbypolicy money -- defense allocation by policy
declare @alloc_sir_indem money -- sir allocation by indemnity to a policy
declare @alloc_sir_def money -- sir allocation by defense to a policy
declare @alloc_indem money -- indemnity allocation to a policy
declare @alloc_def money -- defense allocation to a policy
declare @alloc_defout money -- defense allocation outside limits to a policy
declare @costsremaining bit -- indicator whether costs remain to be allocated for an occurrence
declare @sir money -- temporary sir variable
declare @policypct decimal(27,15) -- policy layer percent variable
declare @defensetreatment int -- defense treatment of policy
declare @occremain money -- temporary occurrence remaining variable
declare @aggremain money -- temporary aggregate remaining variable

declare @columns table (colid int identity (1,1) primary key, startdate datetime, enddate datetime)
	-- column table holds the start and end date of each column considered in the allocation
declare @tmpcolumns table (colid int, enddate datetime)
	-- temporary column table used to populate the @columns table
declare @policies table (polid int identity (1,1) primary key, policydateid int, attachmentpoint money, policypct decimal(27,15))
	-- policy table holds the lowest lying policies considered by each occurrence
declare @tmppolicies table (policydateid int, attachmentpoint money, policypct decimal)
	-- temporary policy table used to populate the @policies table
declare @remaining table (remainid int identity (1,1) primary key, policyid int, policydateid int, 
	alloc_indem money, alloc_def money, alloc_defout money, alloc_sir_indem money, alloc_sir_def money, 
	occurrenceremaining money, aggregateremaining money, sirremaining money, exhausted bit)
	-- temporary table to store remaining limits for policies and temporary allocation amounts for each occurrence, 
	-- created to allow multiple allocations to run simultaneously by eliminating the need to update ztblpolicyallocation

-- Create the occurrence table

declare @occrs table (tmpid int, occurrenceid int, corporatehistoryid int, occurrencegroupid int, 
	triggerstart datetime, triggerend datetime, defensecost money, defenseratio decimal, indemnitycost money)

create table #tmpoccurrence (tmpid int identity (1,1) primary key, occurrenceid int, occurrencename nvarchar(250), occurrencetypeid int,
	aggregateapplies bit, triggerstart datetime, triggerend datetime, defensecost money, defenseratio decimal, corporatehistoryid int, 
	occurrencegroupid int, totalcost money)

set @strSQL = 'insert into #tmpoccurrence (occurrenceid, occurrencename, occurrencetypeid, aggregateapplies, triggerstart, triggerend, ' +
	'defensecost, defenseratio, corporatehistoryid, occurrencegroupid, totalcost) ' +
	'select o.occurrenceid, occurrencename, occurrencetypeid, aggregateapplies, ' + @triggerstartfield +
	' as triggerstart, ' + @triggerendfield + ' as triggerend, (' + @defensecosts + ') as defensecost, case when ' + @costfields +
	' = 0 then 1 else (' + @defensecosts + ')/(' + @costfields + ') end as defenseratio, corporatehistoryid, o.occurrencegroupid, '
-- If considering previous allocation
if @considerpreviousallocation = 1
begin
	set @strsql = @strsql + '(' + @costfields + ') - coalesce(a.allocationtot, 0) as totalcost ' +
		'from tbloccurrence o inner join tbloccurrencegroup og on o.occurrencegroupid = og.occurrencegroupid ' +
		'left join (select allocationid, occurrenceid, sum(allocation) as allocationtot, sum(pv) as pvtot from tblallocationresults ' +
		'where allocationid = ' + convert(nvarchar(50), @previousallocationid) + ' group by allocationid, occurrenceid) a on ' +
		'o.occurrenceid = a.occurrenceid where a.allocationid = ' + convert(nvarchar(50), @previousallocationid) + ' or a.allocationid is null '
	if @strwhere is not null or @strwhere <> ''
		set @strsql = @strsql + ' and ' + @strwhere
end
else
begin
	set @strsql = @strsql + '(' + @costfields + ') as totalcost from tbloccurrence o inner join tbloccurrencegroup og on o.occurrencegroupid = og.occurrencegroupid '
	if @strwhere is not null or @strwhere <> ''
		set @strsql = @strsql + ' where ' + @strwhere
end
set @strsql = @strsql + ' order by ' + @strsortorder
execute sp_executesql @strsql

insert into @occrs (tmpid, occurrenceid, corporatehistoryid, occurrencegroupid, triggerstart, triggerend, defensecost, defenseratio, indemnitycost)
select tmpid, occurrenceid, corporatehistoryid, occurrencegroupid, triggerstart, triggerend, defensecost, defenseratio, totalcost
from #tmpoccurrence
order by tmpid

drop table #tmpoccurrence

-- create occurrence id cursor
declare crsr_occ cursor local fast_forward
for
select occurrenceid, corporatehistoryid, triggerstart, triggerend, defensecost, indemnitycost
from @occrs
order by tmpid

-- create the policy exhaustion / allocation tracking table specifically for this allocation
insert into @remaining (policyid, policydateid, alloc_indem, alloc_def, alloc_defout, alloc_sir_indem, alloc_sir_def, 
	occurrenceremaining, aggregateremaining, sirremaining, exhausted)
select policyid, policydateid, 0, 0, 0, 0, 0, occurrenceremaining, aggregateremaining, sirremaining, 0
from ztblpolicyallocation
where allocationid = @allocationid

update @remaining
set exhausted = 1
where occurrenceremaining = 0

-- create @columns table
insert into @columns (startdate)
select pstartdate
from ztblpolicyallocation
where allocationid = @allocationid and occurrenceremaining > 0
group by pstartdate
union
select penddate
from ztblpolicyallocation
where allocationid = @allocationid and occurrenceremaining > 0
group by penddate
order by pstartdate

insert into @tmpcolumns (colid, enddate)
select colid, startdate
from @columns
order by colid

update @tmpcolumns
set colid = colid - 1

update @columns
set enddate = tc.enddate
from @columns c inner join @tmpcolumns tc on c.colid = tc.colid

delete from @columns
where enddate is null

open crsr_occ

fetch next from crsr_occ into @occurrenceid, @corporatehistoryid, @triggerstart, @triggerend, @defcost, @indcost 

-- for each occurrence
while @@fetch_status = 0 -- crsr_occ fetch status
begin
	-- reset temporary allocation variables
	update @remaining
	set alloc_indem = 0, alloc_def = 0, alloc_defout = 0, alloc_sir_indem = 0, alloc_sir_def = 0
	
	set @costsremaining = 1

	while @costsremaining = 1
	begin
		-- determine columns affected by occurrence
		select @colloop = min(colid), @maxcol = max(colid)
		from @columns 
		where startdate < @triggerend and enddate > @triggerstart

		-- if occurrence does not fall within coverage block
		-- track uncollapsed costs and break loop
		if @colloop is null or @maxcol is null
		begin
			if @triggerend < (select min(startdate) from @columns)
				insert into tblallocationresultshorizontal (allocationid, occurrenceid, uncollapsedPrior, uncollapsedPost)
				select @allocationid, @occurrenceid, @indcost + @defcost, 0
			else if @triggerstart > (select max(enddate) from @columns)
				insert into tblallocationresultshorizontal (allocationid, occurrenceid, uncollapsedPrior, uncollapsedPost)
				select @allocationid, @occurrenceid, 0, @indcost + @defcost
			else
				insert into tblallocationresultsvertical (allocationid, occurrenceid, columnid, uncollapsedvertical)
				select @allocationid, @occurrenceid, null, @indcost + @defcost
			break
		end -- end occurrence outside coverage block

		-- for each column
		while @colloop <= @maxcol
		begin
			-- extract single column information
			select @colstart = startdate, @colend = enddate
			from @columns 
			where colid = @colloop

			-- find lowest lying policies triggered in each column, limited by corporate history profile and insurance program
			set @minattach = (
				select min(attachmentpoint)
				from ztblpolicyallocation pa 
					inner join tblcorphistinsprogram chip on pa.insuranceprogramid = chip.insuranceprogramid
					inner join @remaining r on pa.policydateid = r.policydateid
				where allocationid = @allocationid and pstartdate < @colend and penddate > @colstart and 
					r.exhausted <> 1 and corporatehistoryid = @corporatehistoryid
				)
			
			insert into @tmppolicies (policydateid, attachmentpoint)
			select pa.policydateid, attachmentpoint
			from ztblpolicyallocation pa
				inner join tblcorphistinsprogram chip on pa.insuranceprogramid = chip.insuranceprogramid
				inner join @remaining r on pa.policydateid = r.policydateid
			where allocationid = @allocationid and pstartdate < @colend and penddate > @colstart and 
				r.exhausted <> 1 and corporatehistoryid = @corporatehistoryid and attachmentpoint = @minattach

			-- move to next column	
			set @colloop = @colloop + 1
		end -- end find lowest lying policies

		-- insert lowest lying policies from all columns into @policies table
		insert into @policies (policydateid, attachmentpoint)
		select policydateid, attachmentpoint
		from @tmppolicies
		where policydateid is not null
		group by policydateid, attachmentpoint

		-- empty @tmppolicies
		delete from @tmppolicies

		-- update quota-share percentages
		update @policies
		set policypct = convert(decimal (27, 15), perocclimit) / convert(decimal (27, 15), layerperocclimit)
		from @policies p inner join ztblpolicyallocation pa on p.policydateid = pa.policydateid

		insert into ztblallsumstest2 (allocid, occid, trigstart, trigend, polid, poldateid, pstart, pend, polpct)
		select @allocationid, @occurrenceid, @triggerstart, @triggerend, policyid, pd.policydateid, pstartdate, penddate, policypct
		from @policies p inner join tblpolicydates pd on p.policydateid = pd.policydateid
		
		if (select count(*) from @policies) = 0
			insert into ztblallsumstest2 (allocid, occid, trigstart, trigend, polid, poldateid, pstart, pend, polpct)
			select @allocationid, @occurrenceid, @triggerstart, @triggerend, -1, -1, null, null, null

		-- find maximum ranks
		select @maxrankindem = max(rank_indem), @maxrankdef = max(rank_def)
		from tblallocationallsumsprefs aasp 
			inner join tblpolicydates pd on aasp.policyid = pd.policyid
			inner join @policies p on pd.policydateid = p.policydateid
		where allocationid = @allocationid
	
		-- determine indemnity and defense allocation to policies
		set @indembypolicy = @indcost / (
			select sum(policypct)
			from @policies p
				inner join tblpolicydates pd on p.policydateid = pd.policydateid
				inner join tblallocationallsumsprefs aasp on pd.policyid = aasp.policyid
			where allocationid = @allocationid and rank_indem = @maxrankindem)
		set @defbypolicy = @defcost / (
			select sum(policypct)
			from @policies p
				inner join tblpolicydates pd on p.policydateid = pd.policydateid
				inner join tblallocationallsumsprefs aasp on pd.policyid = aasp.policyid
			where allocationid = @allocationid and rank_def = @maxrankdef)

		-- loop through policies to receive allocation
		declare crsr_pol cursor local fast_forward
		for		
		select p.policydateid, pd.policyid, policypct
		from @policies p inner join tblpolicydates pd on p.policydateid = pd.policydateid
		order by polid

		open crsr_pol

		fetch next from crsr_pol into @policydateid, @policyid, @policypct
	
		while @@fetch_status = 0 -- crsr_pol fetch status
		begin
			-- reset temporary allocation variables
			set @alloc_sir_indem = 0
			set @alloc_sir_def = 0
			set @alloc_indem = 0
			set @alloc_def = 0
			set @alloc_defout = 0

			-- determine sir, occurrence remaining, and aggregate remaining
			select @sir = case when sirremaining < .01 then 0 else sirremaining end, 
				@occremain = case when occurrenceremaining < .01 then 0 else occurrenceremaining end, 
				@aggremain = case when aggregateremaining < .01 then 0 else aggregateremaining end
			from @remaining
			where policyid = @policyid and policydateid = @policydateid

			-- determine defense treatment
			set @defensetreatment = (select provision1 from ztblpolicyallocation
				where allocationid = @allocationid and policyid = @policyid and policydateid = @policydateid)

			-- determine if policy will receive defense allocation
			if @maxrankdef = (select rank_def from tblallocationallsumsprefs 
				where allocationid = @allocationid and policyid = @policyid)
			begin		
				-- if there is a sir
				if @sir > 0
				begin
					-- allocate defense first
					-- defense per policy >= sir
					if @defbypolicy * @policypct >= @sir
						set @alloc_sir_def = @sir
					-- otherwise
					else
						set @alloc_sir_def = @defbypolicy * @policypct
		
					-- reduce remaining defense costs and sir
					set @defcost = @defcost - @alloc_sir_def
					set @sir = @sir - @alloc_sir_def

					update @remaining
					set sirremaining = case when sirremaining - @alloc_sir_def < .01 
						then 0 else sirremaining - @alloc_sir_def end
					where policyid = @policyid and policydateid = @policydateid
				end -- end sir defense allocation
				-- no sir
				else
					set @alloc_sir_def = 0

				-- allocate defense
				-- defense within limits
				if @defensetreatment between 1 and 2
				begin
					-- aggregate remaining is greater than per occurrence remaining
					if @aggremain >= @occremain
						-- remaining defense per policy >= per occurrence remaining
						if (@defbypolicy - @alloc_sir_def / @policypct) * @policypct >= @occremain
							set @alloc_def = @occremain
						else
							set @alloc_def = (@defbypolicy - @alloc_sir_def / @policypct) * @policypct
					-- occurrence remaining is greater than aggregate remaining
					else
						-- defense per policy > aggregate remaining
						if (@defbypolicy - @alloc_sir_def / @policypct) * @policypct >= @aggremain
							set @alloc_def = @aggremain
						else
							set @alloc_def = (@defbypolicy - @alloc_sir_def / @policypct) * @policypct

					-- reduce remaining defense costs
					set @defcost = @defcost - @alloc_def

					-- reduce remaining per occurrence and aggregate limits
					update @remaining
					set occurrenceremaining = case when occurrenceremaining - @alloc_def < .01
							then 0 else occurrenceremaining - @alloc_def end, 
						aggregateremaining = case when aggregateremaining - @alloc_def < .01
							then 0 else aggregateremaining - @alloc_def end
					where policyid = @policyid and policydateid = @policydateid
				end
				-- defense in excess of limits
				else if @defensetreatment = 3
				begin
					set @alloc_defout = (@defbypolicy - @alloc_sir_def / @policypct) * @policypct

					-- reduce remaining defense costs
					set @defcost = @defcost - @defbypolicy
				end -- end defense allocation
				-- no defense
			end -- end defense allocation

			-- update remaining amounts
			select @sir = case when sirremaining < .01 then 0 else sirremaining end, 
				@occremain = case when occurrenceremaining < .01 then 0 else occurrenceremaining end, 
				@aggremain = case when aggregateremaining < .01 then 0 else aggregateremaining end
			from @remaining
			where policyid = @policyid and policydateid = @policydateid

			update @remaining
			set exhausted = 1
			where occurrenceremaining = 0

			if @maxrankindem = (select rank_indem from tblallocationallsumsprefs
				where allocationid = @allocationid and policyid = @policyid)
			begin
				-- if there is a sir or sir remaining
				if @sir > 0
				begin
					-- indemnity per policy >= sir remaining
					if @indembypolicy * @policypct >= @sir
						set @alloc_sir_indem = @sir
					-- otherwise
					else
						set @alloc_sir_indem = @indembypolicy * @policypct
	
					-- reduce remaining indemnity costs and sir
					set @indcost = @indcost - @alloc_sir_indem
					set @sir = @sir - @alloc_sir_indem

					update @remaining
					set sirremaining = case when sirremaining - @alloc_sir_indem < .01 
						then 0 else sirremaining - @alloc_sir_indem end
					where policyid = @policyid and policydateid = @policydateid
				end -- end sir indemnity allocation

				-- aggregate remaining is greater than per occurrence remaining
				if @aggremain >= @occremain
					-- remaining indemnity per policy >= per occurrence remaining
					if (@indembypolicy - @alloc_sir_indem / @policypct) * @policypct >= @occremain
						set @alloc_indem = @occremain
					else
						set @alloc_indem = (@indembypolicy - @alloc_sir_indem / @policypct) * @policypct
				-- occurrence remaining is greater than aggregate remaining
				else
					-- remaining indemnity per policy >= aggregate remaining
					if (@indembypolicy - @alloc_sir_indem / @policypct) * @policypct >= @aggremain
						set @alloc_indem = @aggremain
					else
						set @alloc_indem = (@indembypolicy - @alloc_sir_indem / @policypct) * @policypct

				-- reduce remaining indemnity
				set @indcost = @indcost - @alloc_indem

				-- reduce remaining per occurrence and aggregate limits
				update @remaining
				set occurrenceremaining = case when occurrenceremaining - @alloc_indem < .01
						then 0 else occurrenceremaining - @alloc_indem end, 
					aggregateremaining = case when aggregateremaining - @alloc_indem < .01
						then 0 else aggregateremaining - @alloc_indem end
				where policyid = @policyid and policydateid = @policydateid
				
			end -- end indemnity allocation

			-- update occurrence allocation results
			update @remaining
			set alloc_indem = alloc_indem + @alloc_indem, alloc_def = alloc_def + @alloc_def, 
				alloc_defout = alloc_defout + @alloc_defout,
				alloc_sir_indem = alloc_sir_indem + @alloc_sir_indem, 
				alloc_sir_def = alloc_sir_def + @alloc_sir_def
			where policyid = @policyid and policydateid = @policydateid

			update @remaining
			set exhausted = 1
			where occurrenceremaining = 0

			insert into ztblAllSumsTest1 (allocid, occid, corphistid, trigstart, trigend,
				defcost, indcost, numpolicies, rank_indem, rank_def, defbypol, indbypol, polid, poldateid,
				alloc, sir_alloc)
			select @allocationid, @occurrenceid, @corporatehistoryid, @triggerstart, @triggerend,
				@defcost, @indcost, (select count(*) from @policies), @maxrankindem, @maxrankdef, @defbypolicy, @indembypolicy,
				@policyid, @policydateid, @alloc_indem+@alloc_def+@alloc_defout, @alloc_sir_indem+@alloc_sir_def
			where @alloc_indem+@alloc_def+@alloc_defout > 0 or @alloc_sir_indem+@alloc_sir_def > 0

			-- move to next policy
			fetch next from crsr_pol into @policydateid, @policyid, @policypct
		end

		-- check to see if costs remain
		if @defcost < 0.01 and @indcost < 0.01
			set @costsremaining = 0
	
		delete from @policies
		close crsr_pol
		deallocate crsr_pol
	end -- while costs remain

	-- insert allocation for this occurrence
	insert into tblallocationresults (allocationid, policyid, policydateid, pstartsplit, pendsplit, annualperiodsplit, 
		allocation, allocationindemnity, allocationdefense, allocationdefenseoutsidelimits, occurrenceid, 
		triggerstartdateused, triggerenddateused, sirallocated, sirallocatedindemnity, sirallocateddefense, 
		sirdiscount, [datetime], pv, datetopresentvalue, pvrate)
	select @allocationid, r.policyid, r.policydateid, pstartdate, penddate, annualperiod,
		alloc_indem + alloc_def + alloc_defout, alloc_indem, alloc_def, alloc_defout, @occurrenceid, 
		@triggerstart, @triggerend, alloc_sir_indem + alloc_sir_def, alloc_sir_indem, alloc_sir_def, 
		1, getdate(), 0, null, null
	from @remaining r inner join ztblpolicyallocation pa on r.policydateid = pa.policydateid
	where allocationid = @allocationid and 
		(alloc_indem + alloc_def + alloc_defout > 0 or alloc_sir_indem + alloc_sir_def > 0)
	order by pstartdate

	-- reset allocation amounts
	update @remaining
	set alloc_indem = 0, alloc_defout = 0, alloc_sir_indem = 0, alloc_sir_def = 0

	-- move to next occurrence
	fetch next from crsr_occ into @occurrenceid, @corporatehistoryid, @triggerstart, @triggerend, @defcost, @indcost
end -- for each occurrence

close crsr_occ
deallocate crsr_occ

return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLAllSumsAllocation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLAllSumsAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLAllSumsAllocation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLAllSumsAllocation] TO [power_user]
GO
