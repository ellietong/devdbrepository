SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_RELATED_PARTY_MedicareStatus]
AS

	SELECT

		mbs.MedicareBeneficiaryStatusID AS RPMedicareBeneficiaryStatusID, mbs.MedicareBeneficiaryStatus

	FROM tblMedicareBeneficiaryStatus mbs
GO
