SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval] 
(@SettlementAuthorityID bigint)
AS

SET NOCOUNT ON
---SET XACT_ABORT ON
BEGIN
--DECLARE @SettlementAuthorityID bigint = 41


/* Query run once settlement authority is granted by all parties
*/
--SET ANSI_WARNINGS ONFF
--Declare and set email inputs.
Declare @Subject nvarchar(MAX)
Declare @LogoHeader nvarchar(MAX)

DECLARE @SettlementGroup nvarchar(MAX) = (SELECT SettlementGroup
										FROM tblSettlementAuthority sa
										INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
										WHERE sa.SettlementAuthorityID = @SettlementAuthorityID)
SET @Subject = 	'IMO Settlement Authority Granted ' + CONVERT(varchar, getdate(), 107) + ': ' + @SettlementGroup 

--PRINT @Subject
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
padding:0;}	
--->
</STYLE>'


--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
set @BodyMessage = @BodyMessage + '<H4>Settlement authority has been granted for the following settlement group.  Authority correspondence is included below: </H4>'

--Print SettlementAuthority table first

--Recursively create response thread
DECLARE @out nvarchar(MAX)
EXEC	[dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryPrintAllResponses]
		@SettlementAuthorityID = @SettlementAuthorityID,
		@out = @out OUTPUT
--Print SettlementAuthority table row first...
DECLARE @MainHeaderStyle nvarchar(MAX) = 'background-color: #0079C1; font-weight: bold; color: white; padding: 4px;'
DECLARE @tdStyle nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'
SET @BodyMessage = '
	<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
		<tr>
			<th style="' + @MainHeaderStyle + '">Settlement Group</th>
			<th style="' + @MainHeaderStyle + '">Comments</th>
			<th style="' + @MainHeaderStyle + '">Demand Amount</th>
			<th style="' + @MainHeaderStyle + '">Authority Amount</th>
			<th style="' + @MainHeaderStyle + '">Status</th>
			<th style="' + @MainHeaderStyle + '">Request Date</th>
			<th style="' + @MainHeaderStyle + '">Requested By</th>
		</tr>
		'
DECLARE @newLine nvarchar(5) = CHAR(13) + CHAR(10)

--Generate status temp table
IF object_id('tempdb..#FirmsRequiringApproval') IS NOT NULL
	BEGIN
	   DROP TABLE #FirmsRequiringApproval
	END
	CREATE TABLE #FirmsRequiringApproval (SettlementAuthorityID int, [Status] nvarchar(MAX))
	
	INSERT INTO #FirmsRequiringApproval
	SELECT SettlementAuthorityID, 
	COALESCE(
	'Awaiting approval from: <br/>' + 
	CAST((
	SELECT DISTINCT CASE WHEN p.Email LIKE '%resolute-midwest.com' THEN DefenseCounsel + ' - Midwest' 
														WHEN p.Email LIKE '%resolute-ne.com' THEN DefenseCounsel + ' - New England' 
														ELSE COALESCE(DefenseCounsel, 'KCIC') END AS DefenseCounsel
	FROm tblSettlementAuthorityApproverContact saac
	INNER JOIN tblSettlementApproverContact sac ON saac.SettlementApproverContactID = sac.SettlementApproverContactID
	INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
	INNER JOIN tblDefenseCounsel dc ON p.FirmID = dc.DefenseCounselID
	WHERE NOT EXISTS (SELECT SettlementAuthorityResponseID
						FROM tblSettlementAuthorityResponse sar
						WHERE SettlementAuthorityID = saac.SettlementAuthorityID
						AND SettlementAuthorityStatusID = 1
						AND sar.CreatedBy = p.email)
	AND saac.SettlementAuthorityID = sa.SettlementAuthorityID
	AND COALESCE(saac.ApprovalRequired, 0) = 1
		FOR XML PATH('div')) AS NVARCHAR(MAX)), 'Authority Granted')
	FROM tblSettlementAuthority sa

--SELECT * FROM #FirmsRequiringApproval
--Actual data row 
SET @BodyMessage = @BodyMessage + COALESCE((CAST((SELECT
				[td/@style] = @tdStyle,
				--[td/a/@href]='https://www.kcicextranet.com/clients/imo/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, c.ClaimantPersonalInfoID)+'&claimanttab=1&claimtab=2',
				[td] = sg.SettlementGroup, '',--cpi.LastName + ', ' + cpi.FirstName, @newLine,--ClaimantName
				
				[td/@style] = @tdStyle,
				[td] = REPLACE(sa.Comment, @newLine, (SELECT br = '' FOR XML PATH(''))), @newLine,
				
				[td/@style] = @tdStyle,
				[td] = '$' + CONVERT(varchar(12), DemandAmount, 1), @newLine,
				
				[td/@style] = @tdStyle,
				[td] = '$' + CONVERT(varchar(12), AuthorityAmount, 1), @newLine,
				
				[td/@style] = @tdStyle,
				[td] = CAST(COALESCE([Status], '') AS XML), @newLine,
				
				[td/@style] = @tdStyle,
				[td] = CONVERT(nvarchar, AuthorityRequestedDate, 101), @newLine,
				
				[td/@style] = @tdStyle,
				[td] = AuthorityRequestedBy, @newLine
				
		FROM tblSettlementAuthority sa
		INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
		LEFT JOIN tblClaimantSettlement cs ON sg.SettlementGroupID = cs.SettlementGroupID
		LEFT JOIN #FirmsRequiringApproval fra ON sa.SettlementAuthorityID = fra.SettlementAuthorityID
		--INNER JOIN tblClaimLawsuit cl ON sa.ClaimLawsuitID = cl.ClaimLawsuitID
		--INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		--INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		--LEFT JOIN tblSettlementGroup sg ON cs.SettlementGroupID = sg.SettlementGroupID 
		WHERE sa.SettlementAuthorityID = @SettlementAuthorityID
		GROUP BY	sg.SettlementGroup,
					sa.Comment, 
					CONVERT(varchar(12), DemandAmount, 1),
					CONVERT(varchar(12), AuthorityAmount, 1),
					[Status],
					CONVERT(nvarchar, AuthorityRequestedDate, 101),
					AuthorityRequestedBy
		FOR XML PATH('tr'), type) AS NVARCHAR(MAX))), '')

--...then concatenate output from recursive stored proc inside next TR that spans all columns
SET @BodyMessage = @BodyMessage + '
	</tr>
	<tr>
		<td colspan="7">' + COALESCE(@out, '') + '
		</td>
	</tr>
</table>'

--Create distinct semi-colon delimited list of people involved in discussion (or expected to
--in the case of insurers who do not respond)



--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

  
 
--Grab list of distinct email addresses of settlement approve contacts
DECLARE @RecipientList nvarchar(MAX)
SET @RecipientList=(SELECT DISTINCT p.Email + '; ' AS [text()]
            From tblSettlementAuthorityApproverContact saac
            INNER JOIN tblSettlementApproverContact sac ON saac.SettlementApproverContactID = sac.SettlementApproverContactID
            INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
            Where saac.SettlementAuthorityID = @SettlementAuthorityID
            For XML PATH (''))

--Bring in notification contacts
SET @RecipientList = @RecipientList + (SELECT DISTINCT p.Email + '; ' AS [text()]
            From tblSettlementAuthorityNotificationContact sanc
            INNER JOIN tblSettlementNotificationContact snc ON sanc.SettlementNotificationContactID = snc.SettlementNotificationContactID
            INNER JOIN vtblPerson p ON snc.PersonID = p.PersonID
            Where sanc.SettlementAuthorityID = @SettlementAuthorityID
            For XML PATH (''))
            
--SET @RecipientList=(SELECT DISTINCT sar.CreatedBy + '; ' AS [text()]
--            From tblSettlementAuthorityResponse sar
--            Where sar.SettlementAuthorityID = @SettlementAuthorityID
--            For XML PATH (''))
--Concatenate email of person who originally requested authority and Traci
SET @RecipientList = @RecipientList + (SELECT AuthorityRequestedBy FROM tblSettlementAuthority WHERE SettlementAuthorityID = @SettlementAuthorityID)
DECLARE @CCList nvarchar(MAX) = 'bzimmerman@leaderberkon.com'
--QC Recipient List
Set @Bodymessage = @BodyMessage + 'Recipients: ' + @RecipientList  + '</br>CC: ' + @CCList

--PRINT SUBSTRING(@BodyMessage, 0, 4000)
--PRINT SUBSTRING(@BodyMessage, 4000, 8000)


--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The Profile_Name sends from [client]@kcicllc.com email address
@profile_name='Claims',
@recipients = 'imo@kcic.com',--@RecipientList,
@copy_recipients = 'crookc@kcic.com',--@CCList
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

SET ANSI_WARNINGS ON
--SET XACT_ABORT OFF
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval] TO [base_user]
GO
