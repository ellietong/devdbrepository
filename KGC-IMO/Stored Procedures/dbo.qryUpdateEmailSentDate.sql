SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryUpdateEmailSentDate] 
As

Set nocount on

--Procedure will update the ExternalEmail date for each document being sent in the daily email.

Declare @Today4PM datetime
Declare @Yesterday4PM datetime
SET @Yesterday4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-1))+' 16:00:00.000')
SET @Today4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 16:00:00.000')


--Create a temp table for Complaints which where emailed out.
DECLARE @NewComplaintsTable TABLE (DocID int, FirmAddressID int,  TrackingID int, DateAccepted datetime)

INSERT INTO @NewComplaintsTable
SELECT DISTINCT d.DocumentID,l.DefenseCOunselAddressID,  ctl.ComplaintTrackingID, DateAccepted
FROM tblDocuments d
 JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
 JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
 join tblclaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
 join tblclaimlawsuit cl on cl.claimid = c.claimid	
 join tbllawsuit l on l.lawsuitid =cl.lawsuitid
 join tblComplaintTrackingLawsuit ctl on ctl.LawsuitID = cl.LawsuitID
 join tblDefenseCounselAddress dca on l.DefenseCounselAddressID = dca.DefenseCounselAddressID
WHERE D.DateCreated between @Yesterday4PM AND @Today4PM
AND ClaimantDocumentTypeID IN (8,4,33)
AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
AND DefenseCounselID IS NOT NULL
AND IsPrimaryForClaim = 1
AND IsCurrentClaim = 1
AND DateAccepted is NULL -- Don't Populate Email Sent Date for Complaints or Documents that have already been accepted. 


--select * from @NewComplaintsTable
-- Update the complaint tracking table with the date the email is sent

--Begin tran
Update ct
Set ct.ExternalEmailDate = GETDATE()
	from [kga-ClaimsAdmin].dbo.tblComplaintTracking ct
	join @NewComplaintsTable nct on ct.ComplaintID = nct.TrackingID
	where ct.complaintID IN (select trackingID from @NewComplaintsTable) and ct.ExternalEmailDate is NULL
	
--rollback


Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryUpdateEmailSentDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryUpdateEmailSentDate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryUpdateEmailSentDate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryUpdateEmailSentDate] TO [power_user]
GO
