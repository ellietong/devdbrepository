SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SMOKING_qryInsClaimantSmokingHistory]
    (
      @ClaimantPersonalInfoID INT ,
      @SmokingStart DATE ,
      @SmokingEnd DATE ,
      @PacksPerDay FLOAT ,
      @SPUsername NVARCHAR(250)
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        INSERT  INTO tblClaimantSmokingHistory
                ( ClaimantPersonalInfoID ,
                  SmokingStart ,
                  SmokingEnd ,
                  PacksPerDay ,
                  SPUsername
                )
        VALUES  ( @ClaimantPersonalInfoID ,
                  @SmokingStart ,
                  @SmokingEnd ,
                  @PacksPerDay ,
                  @SPUsername
                )

    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SMOKING_qryInsClaimantSmokingHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SMOKING_qryInsClaimantSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SMOKING_qryInsClaimantSmokingHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SMOKING_qryInsClaimantSmokingHistory] TO [power_user]
GO
