SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryInsEmployment]
    (
      @ClaimantPersonalInfoID BIGINT ,
      @Jobsite NVARCHAR(250) ,
      @City NVARCHAR(100) ,
      @StateID INT ,
      @OccupationID INT ,
      @StartDate DATETIME ,
      @EndDate DATETIME ,
      @Emailaddress NVARCHAR(100)
    )
AS 
    SET NOCOUNT ON

    INSERT  INTO tblClaimantEmployment
            ( ClaimantPersonalInfoID ,
              Jobsite ,
              OccupationID ,
              JobsiteCity ,
              JobsiteStateID ,
              EmpStartDate ,
              EmpEndDate ,
              SPUsername
            )
    VALUES  ( @ClaimantPersonalInfoID ,
              UPPER(@Jobsite) ,
              @OccupationID ,
              UPPER(@City) ,
              @StateID ,
              @StartDate ,
              @EndDate ,
              @Emailaddress
            )
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsEmployment] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsEmployment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsEmployment] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsEmployment] TO [power_user]
GO
