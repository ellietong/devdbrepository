SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptLawsuitEntryQCSheet]
(@LawsuitID bigint)

As
Set NoCount On


Select LawsuitID, CaseCaption, DocketNumber, FileDate, Jurisdiction, S.State, CourtType, ServiceDate,
	ReceivedDate, DefenseCounsel + ' - ' + DefenseCounselCity as DefenseCounsel, PlaintiffCounsel + ' - ' + PlaintiffCounselCity as PlaintiffCounsel, LeadPlaintiffFirstName,
	LeadPlaintiffLastName
From tblLawsuit L
Left Join tblJurisdiction J on J.JurisdictionID=L.JurisdictionID
Left Join tblState S on S.StateID=L.StateID
Left Join tblCourtType CT on CT.CourtTypeID=L.CourtTypeID
Left Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = L.DefenseCounselAddressID
Left Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
Where LawsuitID = @LawsuitID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptLawsuitEntryQCSheet] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptLawsuitEntryQCSheet] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptLawsuitEntryQCSheet] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptLawsuitEntryQCSheet] TO [power_user]
GO
