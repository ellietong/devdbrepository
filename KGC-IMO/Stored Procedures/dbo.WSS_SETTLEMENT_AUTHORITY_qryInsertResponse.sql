SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertResponse]
(	@SettlementAuthorityID bigint, 
	@SettlementAuthorityResponseID bigint = NULL,
	@Comment nvarchar(MAX),
	@SettlementAuthorityStatusID int,
	@Emailaddress nvarchar(250) = null)
AS
BEGIN
	INSERT INTO tblSettlementAuthorityResponse 
	(Comment, CreatedBy, DateCreated, ParentSettlementAuthorityResponseID,
	SettlementAuthorityID, SettlementAuthorityStatusID)
	SELECT @Comment,
	@Emailaddress, GETDATE(), @SettlementAuthorityResponseID,
			@SettlementAuthorityID, @SettlementAuthorityStatusID
			
	SELECT @@IDENTITY
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertResponse] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertResponse] TO [base_user]
GO
