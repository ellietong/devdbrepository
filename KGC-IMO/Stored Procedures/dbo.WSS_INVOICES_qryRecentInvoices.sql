SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryRecentInvoices]
    (
      @emailaddress NVARCHAR(1000) = NULL
    )
AS --DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'mcantieri@smsm.com'



    DECLARE @firmid INT
    SET @firmid = ( SELECT DISTINCT
                            FirmID
                    FROM    vtblperson
                    WHERE   Email = @emailaddress
                  )

    IF ( @FirmID = 188
         OR ( LEFT(@emailaddress, 5) = 'kcic\' )
         OR ( RIGHT(@emailaddress, 11) = 'kcicllc.com' )
       ) 
        BEGIN
            SELECT TOP 200
                    CAST(d.DocumentID AS VARCHAR(50)) AS DocumentId ,
                    ClaimantDocumentType ,
                    dg.ClaimantDocumentGroup ,
                    d.Comments ,
                    d.DocumentURL ,
                    d.DocumentTitle ,
                    d.CreatedBy ,
                    d.DateCreated ,
                    d.ModifiedBy ,
                    d.DateModified ,
                    d.Accepted ,
                    d.AcceptedBy ,
                    d.DateAccepted ,
                    di.ApprovedBy ,
                    di.DateApproved ,
                    f.Firm ,
                    di.InvoiceNumber ,
                    di.Period ,
                    di.DatePayable ,
                    di.fees AS FeeAmount ,
                    di.costs AS DisbursementAmount ,
                    di.adjustments AS Adjustment ,
                    di.InvoiceNotes AS AdjustmentNotes ,
                    dis.Status ,
                    '' AS DefenseInvoiceFirm ,
                    c.sourcedate AS DatePaid ,
                    c.SourceNumber AS CheckNumber ,
                    di.FirmID ,
                    COALESCE(VendorFirm, '') AS VendorFirm ,
                    c.SourceDate ,
                    di.InvoiceTotal AS InvoiceTotal
            FROM    tblDocuments d
                    INNER JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
                    INNER JOIN tblClaimantDocumentGroup dg ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
                    LEFT JOIN vtblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		--LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.VendorFirmID = dif.FirmID 
                    LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID
                    LEFT JOIN vtblFirm f ON di.FirmID = f.FirmID
                    LEFT JOIN tblDefenseInvoice dinv ON dinv.DocumentID = d.DocumentID
                    LEFT JOIN m_tblVendorFirm ex ON dinv.VendorFirmID = ex.VendorFirmID
                    LEFT JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
                    LEFT JOIN tblCheck c ON pd.CheckID = c.CheckID
		--LEFT JOIN tblDefInvClaimantDetail dic on dic.DocumentID=di.DocumentID
            WHERE   dt.ClaimantDocumentGroupID = 2
            ORDER BY DateCreated DESC ,
                    Accepted ASC 
        END 
    ELSE 
        BEGIN
            SELECT TOP 20
                    CAST(d.DocumentID AS VARCHAR(50)) AS DocumentId ,
                    dt.ClaimantDocumentType ,
                    dg.ClaimantDocumentGroup ,
                    d.Comments ,
                    d.DocumentURL ,
                    d.DocumentTitle ,
                    d.CreatedBy ,
                    d.DateCreated ,
                    d.ModifiedBy ,
                    d.DateModified ,
                    d.Accepted ,
                    d.AcceptedBy ,
                    d.DateAccepted ,
                    di.ApprovedBy ,
                    di.DateApproved ,
                    f.Firm ,
                    di.InvoiceNumber ,
                    di.Period ,
                    di.DatePayable ,
                    di.fees AS FeeAmount ,
                    di.costs AS DisbursementAmount ,
                    di.adjustments AS Adjustment ,
                    di.InvoiceNotes AS AdjustmentNotes ,
                    dis.Status ,
                    '' AS DefenseInvoiceFirm ,
                    c.sourcedate AS DatePaid ,
                    c.SourceNumber AS CheckNumber ,
                    di.FirmID ,
                    COALESCE(VendorFirm, '') AS VendorFirm ,
                    c.SourceDate ,
                    di.InvoiceTotal AS InvoiceTotal
            FROM    tblDocuments d
                    INNER JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
                    INNER JOIN tblClaimantDocumentGroup dg ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
                    LEFT JOIN vtblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		--LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.VendorFirmID = dif.FirmID 
                    LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID
                    LEFT JOIN vtblFirm f ON di.FirmID = f.FirmID
                    LEFT JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
                    LEFT JOIN tblDefenseInvoice dinv ON dinv.DocumentID = d.DocumentID
                    LEFT JOIN m_tblVendorFirm ex ON dinv.VendorFirmID = ex.VendorFirmID
                    LEFT JOIN tblCheck c ON pd.CheckID = c.CheckID
		--LEFT JOIN tblDefInvClaimantDetail dic on dic.DocumentID=di.DocumentID
            WHERE   dg.ClaimantDocumentGroupID = 2
                    AND f.FirmID = @firmid
            ORDER BY DateCreated DESC ,
                    Accepted ASC 
        END

--select MAX(datecreated)
--from tblDocuments d
--	JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
--		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
--where dg.DocumentGroupID=2
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryRecentInvoices] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryRecentInvoices] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryRecentInvoices] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryRecentInvoices] TO [power_user]
GO
