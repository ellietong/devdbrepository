SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryPrintResponses_Recursive]
(@SettlementAuthorityID bigint, @SettlementAuthorityResponseID bigint, @out nvarchar(MAX) OUTPUT)
AS
BEGIN

	DECLARE @ImmediateResponses TABLE (SettlementAuthorityResponseID bigint, 
							SettlementAuthorityStatus nvarchar(100), 
							Comment nvarchar(MAX), 
							ParentSettlementAuthorityResponseID bigint,
							CreatedBy nvarchar(250),
							DateCreated datetime)
	INSERT INTO @ImmediateResponses (SettlementAuthorityResponseID, SettlementAuthorityStatus, Comment, ParentSettlementAuthorityResponseID, CreatedBy, DateCreated)
	SELECT SettlementAuthorityResponseID, SettlementAuthorityStatus, Comment, ParentSettlementAuthorityResponseID, CreatedBy, DateCreated
	FROM tblSettlementAuthorityResponse sar
	LEFT JOIN tblSettlementAuthorityStatus sas ON sar.SettlementAuthorityStatusID = sas.SettlementAuthorityStatusID
	WHERE SettlementAuthorityID = @SettlementAuthorityID 
		AND COALESCE(ParentSettlementAuthorityResponseID, 0) = COALESCE(@SettlementAuthorityResponseID, 0)
	
	--Use cursor to loop through each parent record and also print children
	DECLARE @CurrentSettlementAuthorityResponseID int
	DECLARE @newOut nvarchar(MAX) = ''
	
	DECLARE Cur_Complaints CURSOR Local Fast_Forward for
	SELECT DISTINCT SettlementAuthorityResponseID FROM @ImmediateResponses
	OPEN Cur_Complaints
	FETCH NEXT FROM Cur_Complaints INTO @CurrentSettlementAuthorityResponseID--, @Claimant, @ClaimantID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT @CurrentSettlementAuthorityResponseID
		
		--DECLARE @style nvarchar(MAX) = 'margin-left: ' + CONVERT(nvarchar, @depth * 4) + 'em'
		--IF(EXISTS (SELECT SettlementAuthorityResponseID FROM tblSettlementAuthorityResponse WHERE ParentSettlementAuthorityResponseID = @CurrentSettlementAuthorityResponseID))
			
		SET @newOut = @newOut + COALESCE(@out, '') + (SELECT CAST ( (
		SELECT	 
				--@depth AS 'td/@colspan', (SELECT '') as td, '',
				td =  COALESCE(SettlementAuthorityStatus, ' '), '',
				td = REPLACE(Comment, CHAR(13)+CHAR(10), '<br/>'), '',
				td = CreatedBy, '',
				td = CONVERT(nvarchar, DateCreated, 101), ''
		FROM @ImmediateResponses
			WHERE SettlementAuthorityResponseID = @CurrentSettlementAuthorityResponseID
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) ))

		
		
		
		IF(EXISTS (SELECT SettlementAuthorityResponseID FROM tblSettlementAuthorityResponse WHERE ParentSettlementAuthorityResponseID = @CurrentSettlementAuthorityResponseID))
		BEGIN
			SET @newOut = @newOut + '<tr><td colspan="5" style="padding-left: 4em"><table>' 
			PRINT 'call kids'
			EXEC WSS_SETTLEMENT_AUTHORITY_qryPrintResponses_Recursive @SettlementAuthorityID, @CurrentSettlementAuthorityResponseID, @newOut output
			

			SET @newOut = @newOut + '</table>' 
			
		END
		SET @newOut = @newOut + '</td></tr>'
		

	FETCH NEXT FROM Cur_Complaints INTO @CurrentSettlementAuthorityResponseID--, @Claimant, @ClaimantID
	END
	SET @out = COALESCE(@newOut, '')
	
	--SET @out = @newOut
	--SELECT @out
	
	SELECT @out
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryPrintResponses_Recursive] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryPrintResponses_Recursive] TO [base_user]
GO
