SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[m_qryfrmrptAllocationPolicyDateExclude]
(@allocationID int = NULL)
AS
IF @allocationID IS NULL
	SELECT AllocationID, PolicyDateID
	FROM tblAllocationPolicyDateExclude
ELSE
	SELECT AllocationID, PolicyDateID
	FROM tblAllocationPolicyDateExclude
	WHERE AllocationID = @allocationID
RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[m_qryfrmrptAllocationPolicyDateExclude] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qryfrmrptAllocationPolicyDateExclude] TO [base_user]
GO
