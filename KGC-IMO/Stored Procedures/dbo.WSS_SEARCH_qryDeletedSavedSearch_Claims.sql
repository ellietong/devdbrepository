SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryDeletedSavedSearch_Claims]
	-- Add the parameters for the stored procedure here
    @SavedSearchID INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
--Delete Active Trial Date Record(s)    
        DELETE  FROM tblSavedActiveTrialDateRange
        WHERE   SavedSearchID = @SavedSearchID

--Delete Jurisdiction Record(s)
        DELETE  FROM tblSavedJurisdiction
        WHERE   SavedSearchID = @SavedSearchID

--Delete Allegation Type Record(s)
        DELETE  FROM tblSavedAllegation
        WHERE   SavedSearchID = @SavedSearchID

--Delete Lawsuit Status ID Record(s)
        DELETE  FROM tblSavedLawsuitStatus
        WHERE   SavedSearchID = @SavedSearchID

--Delete MatterID Record(s)
        DELETE  FROM tblSavedMatter
        WHERE   SavedSearchID = @SavedSearchID

--Delete PlaintiffCounselAddressID Record(s)
        DELETE  FROM tblSavedPlaintiffCounsel
        WHERE   SavedSearchID = @SavedSearchID

--Delete Disease Record(s)
        DELETE  FROM tblSavedDisease
        WHERE   SavedSearchID = @SavedSearchID

--Delete StateID Record(s)
        DELETE  FROM tblSavedState
        WHERE   SavedSearchID = @SavedSearchID

--Delete IsMDL Record(s)
        DELETE  FROM tblSavedIsMDL
        WHERE   SavedSearchID = @SavedSearchID

--Delete Processed Date Record(s)
        DELETE  FROM tblSavedProcessedDateRange
        WHERE   SavedSearchID = @SavedSearchID

--Delete Status Date Record(s)
        DELETE  FROM tblSavedStatusDateRange
        WHERE   SavedSearchID = @SavedSearchID

--Deleted SavedSearch Record
        DELETE  FROM tblSavedSearch
        WHERE   SavedSearchID = @SavedSearchID
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDeletedSavedSearch_Claims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDeletedSavedSearch_Claims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDeletedSavedSearch_Claims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDeletedSavedSearch_Claims] TO [power_user]
GO
