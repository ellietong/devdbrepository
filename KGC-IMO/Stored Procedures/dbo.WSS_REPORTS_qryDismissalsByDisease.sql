SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryDismissalsByDisease] 
	(@StartDate datetime, @EndDate datetime, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Declare @emailAddress nvarchar(250)
--Declare @StartDate datetime
--Declare @EndDate datetime
--Declare @TortTypeID int

--Set @emailAddress = 'monahanc@kcicllc.com'
--Set @StartDate = '1/1/2012'
--Set @EndDate = '4/1/2012'
--Set @TortTypeID = 1



DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = 0-- (select distinct InsurerRole from vtblPerson where Email = @emailaddress)

--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1)
BEGIN

	select	CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
                                              ELSE hld.HighLevelDisease END AS Disease, 
			COUNT(distinct(cl.ClaimLawsuitID)) as Count
	from tblClaim c 
	inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
	INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
	where	coalesce(ProcessedDate, StatusDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
			and ClaimLawsuitStatusGroupID =2
			and  IsPrimaryStatus=1
			and PrimaryDisease=1
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
	ORDER BY hld.HighLevelDiseaseID

END
ELSE IF(@InsurerRole = 1) --Insurers
BEGIN
	select	CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
                                              ELSE hld.HighLevelDisease END AS Disease,
			COUNT(distinct(cl.ClaimLawsuitID)) as Count
	from tblClaim c 
	inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
	INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
	where	coalesce(ProcessedDate, StatusDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
			and ClaimLawsuitStatusGroupID =2
			and  IsPrimaryStatus=1
			and PrimaryDisease=1
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
	ORDER BY hld.HighLevelDiseaseID

END
ELSE --Local Defense Counsels
BEGIN

	select	CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
                                              ELSE hld.HighLevelDisease END AS Disease,
		COUNT(distinct(cl.ClaimLawsuitID)) as Count
	from tblClaim c 
	inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
	INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
	where	coalesce(ProcessedDate, StatusDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
			and ClaimLawsuitStatusGroupID =2
			and  IsPrimaryStatus=1
			and PrimaryDisease=1
			AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
	ORDER BY hld.HighLevelDiseaseID

END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByDisease] TO [base_user]
GO
