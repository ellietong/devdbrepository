SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SEARCH_qryDefendantCompanyList]
AS 
    SET NOCOUNT ON

    SELECT  DefendantCompanyID,
			DefendantCompany
    
    FROM    tblDefendantCompany
    WHERE   ClientID =37
    UNION
    SELECT  NULL ,
            ''
    
    ORDER BY DefendantCompany
    
    
  
  

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDefendantCompanyList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDefendantCompanyList] TO [base_user]
GO
