SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[WSS_CLAIM_qryUpdClaim]
    (
      --@DiseaseID INT ,
      --@DoctorID BIGINT ,
      --@DiseaseDiagnosisDate DATETIME ,
      @LawsuitID BIGINT ,
      @ActiveTrialDate DATETIME ,
      @PrimaryLawsuit NVARCHAR(250) ,
      @IsMDL NVARCHAR(250) ,
      @TrialGroupID BIGINT,
      @DocketNumber NVARCHAR(MAX),
      @ClaimID INT ,
      @PlaintiffCounselAddressID INT,
      @PrimaryNationalPlaintiffCounselID INT,
      --@ClaimDiseaseID BIGINT ,
      @Username NVARCHAR(250)--, 
	  --@Specialty nvarchar(100)
	)
AS 
    --UPDATE  tblClaimDisease
    --SET     DiseaseID = @DiseaseID ,
    --        SPUsername = @Username ,
    --        DiseaseDiagnosisDate = @DiseaseDiagnosisDate
    --WHERE   ClaimDiseaseID = @ClaimDiseaseID

    UPDATE  tblLawsuit
    SET     SPUsername = @Username,
			DocketNumber = COALESCE(@DocketNumber, DocketNumber),
			PlaintiffCounselAddressID = @PlaintiffCounselAddressID,
			PrimaryNationalPlaintiffCounselID = @PrimaryNationalPlaintiffCounselID
    WHERE   LawsuitID = @LawsuitID
	
	--UPDATE tblDoctor
	--SET DoctorNote = @Specialty
	--WHERE DoctorID= @DoctorID	
	
    UPDATE  tblClaimLawsuit
    SET     ActiveTrialDate = @ActiveTrialDate ,
            IsPrimaryForClaim = CASE WHEN @PrimaryLawsuit = 'YES' THEN 1
                                     ELSE 0
                                END ,
            IsMDL = CASE WHEN @IsMDL = 'YES' THEN 1
                         ELSE 0
                    END ,
            TrialGroupID = @TrialGroupID,
            SPUsername = @Username
    WHERE   LawsuitID = @LawsuitID
            AND ClaimID = @ClaimID


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GO
