SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] 
(
 @emailaddress			nvarchar(250), 
 @SettlementGroupID		int = NULL,
 @SettlementDateStart	datetime,
 @SettlementDateEnd		datetime
)

AS

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 4
--DECLARE @EmailAddress nvarchar(250) = 'owenss@kcic.com'
--DECLARE @SettlementGroupID int = NULL

DECLARE @MaxSettlementGroupID int = (SELECT MAX(SettlementGroupID) FROM tblSettlementGroup)

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE 1 = 1 '

IF @SettlementGroupID		  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND SettlementGroupID = ' + CONVERT(nvarchar(20), @SettlementGroupID) END

IF @SettlementDateStart IS NOT NULL
    OR @SettlementDateEnd IS NOT NULL 
    BEGIN
        SET @WhereString = @WhereString
            + ' AND COALESCE(CONVERT(date,SettlementDate), ''1/1/1900'') >= '''
            + CONVERT(NVARCHAR, COALESCE(@SettlementDateStart, '1/1/1900'))
            + ''' AND COALESCE(CONVERT(date,SettlementDate), CONVERT(date, GETDATE())) <= '''
            + CONVERT(NVARCHAR, COALESCE(@SettlementDateEnd,
                                            CONVERT(DATE, GETDATE()))) + ''''
    END

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END


DECLARE @QueryString NVARCHAR(MAX) = ''
SET @QueryString = '
SELECT DISTINCT msd.ClaimantPersonalInfoID AS ClaimantID, 
				ClaimID,
				FirstName, 
				LastName, 
				msd.ClaimantPersonalInfoID, 
				ClaimantName,
				DOB as Birthdate, 
				DOD as DeceasedDate,
				Disease,
				SettlementDate, 
				SettlementAmount, 
				SettlementGroup,
				SettlementGroupID,
				ClaimLawsuitID,
				ReleaseRqstDate, 
				ReleaseToPC,
				COALESCE(Missing, ''All required claimant information submitted.'') AS MissingClaimantInfo,
				COALESCE(MissingDocs, ''All required documents submitted.'') AS MissingDocumentation,
				COALESCE(RelatedPartyInfo, '''') AS RelatedPartyInfo
FROM vMissingSettlementDocumentation msd
	LEFT JOIN dbo.mtvMissingClaimantInfo(0) mtvc ON msd.ClaimantPersonalInfoID = mtvc.ClaimantPersonalInfoID
	LEFT JOIN dbo.mtvRelatedPartyInfo(0) mtvr ON msd.ClaimantPersonalInfoID = mtvr.ClaimantPersonalInfoID
	LEFT JOIN dbo.mtvMissingSettlementDocumentation(0) mtvd ON msd.SetPymtID = mtvd.SetPymtID
'
+ @JoinString +
@WhereString +
' ORDER BY ClaimantName 
  OPTION (RECOMPILE)'

--PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [power_user]
GO
