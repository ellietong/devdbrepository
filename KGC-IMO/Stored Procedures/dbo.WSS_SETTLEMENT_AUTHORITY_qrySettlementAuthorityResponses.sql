SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityResponses] (@SettlementAuthorityID bigint, @emailaddress nvarchar(250))
AS
BEGIN
	--DECLARE @SettlementAuthorityID int = 1
	SET NOCOUNT ON;

	DECLARE @ClientFirmAddressID bigint = 363 --Colfax Corp
	DECLARE @NCCFirmAddressID bigint = 361 --Leader & Berkon
	DECLARE @UserFirmAddressID bigint = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
	
	DECLARE @ResponsesWithChildren TABLE (SettlementAuthorityResponseID bigint)
	INSERT INTO @ResponsesWithChildren
	SELECT ParentSettlementAuthorityResponseID AS SettlementResponseID
	FROM tblSettlementAuthorityResponse
	GROUP BY ParentSettlementAuthorityResponseID



	SELECT	sar.CreatedBy,
			sar.DateCreated,
			sar.ParentSettlementAuthorityResponseID,
			sar.SettlementAuthorityID,
			sar.SettlementAuthorityResponseID,
			sar.SettlementAuthorityStatusID,
			REPLACE(sar.Comment, CHAR(13)+CHAR(10), '<br/>') AS Comment, --Replace newlines with <br/>s 
			SUM(CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END) OVER (PARTITION BY ParentSettlementAuthorityResponseID) - CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END AS Depth,
			CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END AS HasChildren,
			SettlementAuthorityStatus,
			CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END AS IsParent,
			SUM(CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END) OVER (PARTITION BY ParentSettlementAuthorityResponseID)*2 AS DepthEMs,
			CASE WHEN p.Email LIKE '%resolute-midwest.com' THEN DefenseCounsel + ' - Midwest' 
														WHEN p.Email LIKE '%resolute-ne.com' THEN DefenseCounsel + ' - New England' 
														ELSE COALESCE(DefenseCounsel, 'KCIC') END AS Firm,
			-- KCIC & NCC (Leader Berkon) can always respond
			CASE WHEN @emailaddress LIKE '%@kcicllc.com' OR @UserFirmAddressID = @NCCFirmAddressID THEN 1
			--When the response is created by the same person who is viewing site, they may respond to it
				 WHEN CreatedBy = @emailaddress THEN 1 
			--When the User firmaddressID is not the NCC and the post WAS created by NCC or KCIC
				 WHEN (((SELECT FirmAddressID FROM vtblPerson WHERE Email = CreatedBy) = @NCCFirmAddressID) OR (@emailaddress LIKE '%kcicllc.com')) AND @UserFirmAddressID <> @NCCFirmAddressID THEN 1 
				 ELSE 0 
			END AS CanRespond
				
	FROM tblSettlementAuthorityResponse sar
	LEFT JOIN tblSettlementAuthorityStatus sas ON sar.SettlementAuthorityStatusID = sas.SettlementAuthorityStatusID
	LEFT JOIN @ResponsesWithChildren children ON sar.SettlementAuthorityResponseID = children.SettlementAuthorityResponseID
	LEFT JOIN vtblPerson p ON sar.CreatedBy = p.Email
	LEFT JOIN tblDefenseCounselAddress dca ON p.FirmAddressID = dca.DefenseCounselAddressID
	WHERE SettlementAuthorityID = @SettlementAuthorityID--ParentSettlementAuthorityResponseID IS NULL
			AND ParentSettlementAuthorityResponseID IS NULL
	ORDER BY	CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END DESC,
				SUM(CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END) OVER (PARTITION BY ParentSettlementAuthorityResponseID) - CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END ASC, 
				ParentSettlementAuthorityResponseID ASC
	
	


END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityResponses] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityResponses] TO [base_user]
GO
