SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmLedger]
(@carrierID int = null)
AS
SET NOCOUNT ON

If @carrierID= 0

BEGIN

DECLARE @Carrier TABLE (ledgerid int, ledgerdate datetime, ledgertypeid int,  FormSort int IDENTITY(1,1))

	INSERT INTO @Carrier
		SELECT ledgerid, LedgerDate, LedgerTypeID
		FROM tblLedger l
		join tblCarrier c on c.CarrierID=l.CarrierID 
		--WHERE l.CarrierID = @Carrierid
		ORDER BY c.carriername, LedgerDate, LedgerTypeID

	

select l.ledgerid, l.carrierid, l.LedgerDate,ExhaustionDateID, l.LedgerTypeID, Debit
from tblLedger l
left join @Carrier c on c.ledgerid=l.LedgerID
order by FormSort
--inner join tblcarrier c on c.CarrierID=l.CarrierID
--join tblBillPeriod bp on bp.BillPeriodID=l.BillPeriodID
--order by c.CarrierName, ledgerid desc
--ORDER BY --c.CarrierName,
--   CASE l.BillPeriodID
--      WHEN 7 THEN 1
--      WHEN 1 THEN 2
--      WHEN 2 THEN 3
--      WHEN 3 THEN 4
--      WHEN 4 THEN 5
--	  WHEN 5 THEN 6
--	  WHEN 6 THEN 7
--Else 8 End

END

Else

Begin

select ledgerid, l.carrierid, ledgerdate, ExhaustionDateID, LedgerTypeID, Debit
from tblLedger l 
--inner join tblcarrier c on c.CarrierID=l.CarrierID
--join tblBillPeriod bp on bp.BillPeriodID=l.BillPeriodID
where l.CarrierID=@carrierID 
order by LedgerDate, LedgerTypeID
--order by LedgerID desc
--ORDER BY --c.CarrierName,
--   CASE l.BillPeriodID
--      WHEN 7 THEN 1
--      WHEN 1 THEN 2
--      WHEN 2 THEN 3
--      WHEN 3 THEN 4
--      WHEN 4 THEN 5
--	  WHEN 5 THEN 6
--	  WHEN 6 THEN 7
--Else 8 End

ENd

RETURN 


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger] TO [power_user]
GO
