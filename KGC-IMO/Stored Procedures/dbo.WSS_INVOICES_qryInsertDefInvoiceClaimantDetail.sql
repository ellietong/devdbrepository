SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail]
    (
      @FirmID INT ,
      @InvoiceNumber NCHAR(10) ,
      @InvoiceDate DATE ,
      @ClaimantPersonalInfoID INT ,
      @Fees MONEY ,
      @Costs MONEY ,
      @Adjustments MONEY ,
      @Notes NVARCHAR(MAX) ,
      @PACEClaimID INT ,
      @DocumentID INT
    )
AS 
    BEGIN
        SET NOCOUNT ON;

        INSERT  INTO tblDefInvClaimantDetail
                ( [FirmID] ,
                  [InvoiceNumber] ,
                  [InvoiceDate] ,
                  [ClaimantPersonalInfoID] ,
                  [Fees] ,
                  [Costs] ,
                  [Adjustments] ,
                  [Notes] ,
                  [PACEClaimID] ,
                  [DocumentID]
                )
        VALUES  ( @FirmID ,
                  @InvoiceNumber ,
                  @InvoiceDate ,
                  @ClaimantPersonalInfoID ,
                  @Fees ,
                  @Costs ,
                  @Adjustments ,
                  @Notes ,
                  @PACEClaimID ,
                  @DocumentID
                )

		
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [power_user]
GO
