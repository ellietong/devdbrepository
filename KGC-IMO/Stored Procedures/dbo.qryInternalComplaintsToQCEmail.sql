SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryInternalComplaintsToQCEmail]
AS
BEGIN

/*1-28-12 - CAS - Query run daily to send an internal email listing any Complaints that need to be QCed */

--Declare and set email inputs.
Declare @Today4PM datetime
Declare @Yesterday4PM datetime
SET @Yesterday4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-1))+' 16:00:00.000')
SET @Today4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 16:00:00.000')

DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(max)

SET @Subject =    'Marley: Complaints to be QCed'
--Set body font, insert KCIC logo/banner at the top of the email, and set table styles. 
--These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD    {font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000}
TH {border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
--->
</STYLE>'

DECLARE @TDStyle nvarchar(max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--Create temp table to hold all complaints to be QC'ed
--DocTitle and firmAbb are found by parsing the documentURL
DECLARE @NewComplaintsTable TABLE (DocID int, docTitle nvarchar(200), FirmAddressID int, fname nvarchar(500), ServiceDate date, CreatedDate date,
 AcceptedBy nvarchar (250), QCDate nvarchar(250))
INSERT INTO @NewComplaintsTable
SELECT  d.DocumentID, DocumentTitle, l.defensecounseladdressid, LastName +', '+ FirstName+' ('+convert(nvarchar,cpi.ClaimantPersonalInfoID) +')', 
ServiceDate, DateCreated, coalesce(AcceptedBy, ''), coalesce(convert(varchar, mcd.QCDate, 100), NULL)
FROM tblDocuments d
INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
INNER JOIN tblclaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblclaimlawsuit cl on cl.claimid = c.claimid	
INNER JOIN tbllawsuit l on l.lawsuitid =cl.lawsuitid
INNER JOIN tblDefenseCounselAddress dca on dca.DefenseCounselAddressID=l.DefenseCounselAddressID
WHERE (d.DateCreated is null or  d.DateCreated > @Yesterday4PM)
      AND ClaimantDocumentTypeID IN (8,4,33) AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
--	AND d.FirmID=dca.defensecounselid
      
IF ((SELECT COUNT(DISTINCT docTitle) FROM @NewComplaintsTable))=0  
	Set @BodyMessage = @BodyMessage + '<H4>No Complaints to QC; all processed complaints sent to counsel.<H4>'
Else SET @BodyMessage = @BodyMessage + '<H4>The following firms have complaints to be QCed or have QCed complaints that have not been sent: </H4>'

--Declare variables to be used in the firm cursor to list documents by firm
DECLARE @FirmAddressID nvarchar(10), @firmURL nvarchar(100), @firmname nvarchar(100), @docCount int
SET @firmURL = 'https://admin.kcicextranet.com/clients/marleywylain/cp/Pages/Documents.aspx'
DECLARE firm_Curs CURSOR FOR

SELECT nct.FirmAddressID, dca.DefenseCounsel + ' (' + dca.DefenseCounselCity + ')', COUNT(DISTINCT doctitle) 
FROM tblDefenseCounselAddress dca
INNER JOIN @NewComplaintsTable nct ON nct.FirmAddressID = dca.DefenseCounselAddressID
GROUP BY nct.FirmAddressID,  dca.DefenseCounsel + ' (' + dca.DefenseCounselCity + ')'

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmname, @docCount

WHILE @@FETCH_STATUS = 0

BEGIN
      --Header for each firm with link to the document library
      SET @BodyMessage = @BodyMessage + '<br>'+ CONVERT(nvarchar(5), (SELECT @docCount))+ ' Complaint(s) waiting to be sent or QCed for <A HREF = ' + (select @firmURL)+'> ' + (select @firmName) +'</A>:'
      --List documents in a table               
      SET @BodyMessage = @bodymessage + 
            N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
            --Set column headings and populate cells from the cursor
            N'<tr><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">DocumentID</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Name (ID)</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Service Date</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Created Date</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Accepted By</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">QC Date</th>' +
            CAST ( (
                  SELECT	@TDStyle AS 'td/@style', (SELECT DocID) as td, '',
							@TDStyle AS 'td/@style', (SELECT docTitle) as td, '',
							@TDStyle AS 'td/@style', (SELECT fname) as td, '',
							@TDStyle AS 'td/@style', (SELECT ServiceDate) as td, '',
							@TDStyle AS 'td/@style', (SELECT CreatedDate) as td, '',
							@TDStyle AS 'td/@style', (SELECT AcceptedBy) as td, '',
							@TDStyle AS 'td/@style', (SELECT QCDate) as td, ''
                  FROM @NewComplaintsTable C
                  Where  C.FirmAddressID = (select @FirmAddressID)
                  order by fname
                  for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
            N'</table>' +  N'<br/>' 

--Move to next firm                 
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmname, @doccount
END
CLOSE firm_curs
DEALLOCATE firm_curs

SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC <br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


BEGIN
      --Send email
      EXEC msdb.dbo.sp_send_dbmail
      @profile_name = 'Claims',
      @recipients = 'mwc@kcic.com',
      @copy_recipients = 'scottc@kcic.com; potterm@kcic.com',
      --@recipients = 'amanp@kcic.com',
      @subject= @subject,
      @body= @BodyMessage,
      @body_format = 'html'
END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalComplaintsToQCEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalComplaintsToQCEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalComplaintsToQCEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalComplaintsToQCEmail] TO [power_user]
GO
