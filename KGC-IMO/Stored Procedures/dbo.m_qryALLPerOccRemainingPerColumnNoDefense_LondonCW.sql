SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[m_qryALLPerOccRemainingPerColumnNoDefense_LondonCW]
(@allocationID int, @triggerstartfield nvarchar(250))
AS
SET NOCOUNT ON

SET @triggerstartfield = CONVERT(date,@triggerstartfield)

-- create a table variable to hold the results of the query to find the per occ remaining per column
DECLARE @tbltemp table (ColumnID int, PercentOcc float)
-- insert per occ remaining per column into the temporary table
INSERT INTO @tbltemp (ColumnID, PercentOcc)
-- find the per occ remaining per column
SELECT arc.ColumnID, 
	SUM(
	CASE WHEN (@triggerstartfield > ColumnStartDate) AND (@triggerstartfield < ColumnEndDate) THEN CONVERT(decimal, ColumnEndDate - @triggerstartfield) ELSE (
	CASE WHEN PStartDate = ColumnStartDate
	   THEN CASE WHEN PEndDate > ColumnEndDate 
		  THEN (CONVERT(decimal, ColumnEndDate - PStartDate)) 
		  ELSE (CONVERT(decimal, PEndDate - PStartDate))
	   END
	   ELSE CASE WHEN PEndDate < ColumnEndDate 
		  THEN (CONVERT(decimal, PEndDate - ColumnStartDate))
		  ELSE (CONVERT(decimal, ColumnEndDate - ColumnStartDate))
	   END
	END) END  * PerOccLimit) AS PercentOcc
FROM tblAllocationResultsColumn arc, ztblPolicyAllocation zpa
WHERE (zpa.PStartDate < ColumnEndDate) AND (zpa.PEndDate > ColumnStartDate) AND 
	(zpa.InsuranceProgramID = arc.InsuranceProgramID) AND arc.AllocationID = @allocationID
	AND zpa.AllocationID = @allocationID
	--AND zpa.Provision1 <> 4
GROUP BY arc.ColumnID

-- update the ColumnPerOcc field in ztblColumn with the updated per occ remaining per column
UPDATE tblAllocationResultsColumn
SET ColumnPerOcc = (SELECT PercentOcc FROM @tbltemp WHERE ColumnID = tblAllocationResultsColumn.ColumnID)
WHERE AllocationID = @allocationID

DELETE FROM @tbltemp

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[m_qryALLPerOccRemainingPerColumnNoDefense_LondonCW] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qryALLPerOccRemainingPerColumnNoDefense_LondonCW] TO [base_user]
GO
