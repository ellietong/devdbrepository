SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNotifyNewRequest_Client_DELETE] 
(@SettlementAuthorityID bigint)
AS
SET NOCOUNT ON

BEGIN


/*
	TEST VARS
*/
--DECLARE @SettlementAuthorityID bigint = 1

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)

Declare @Today4PM datetime
Declare @Yesterday4PM datetime
Declare @FiveDaysAgo4PM datetime
SET @Yesterday4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-1))+' 16:00:00.000')
SET @Today4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 16:00:00.000')
SET @FiveDaysAgo4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-5))+' 16:00:00.000')

DECLARE @Threshold money = 100000

SET @Subject = 	'IMO Settlement Authority Request Summary' + CONVERT(varchar, getdate(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'


--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--CSS
DECLARE @MainHeaderStyle nvarchar(MAX) = 'background-color: #0079C1; font-weight: bold; color: white; padding: 4px;'
DECLARE @SubHeaderStyle nvarchar(MAX) = 'background-color: #E36F1E; font-weight: bold; color: white; padding: 4px;'
DECLARE @tdStyle nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'

IF(SELECT COUNT(sa.SettlementGroupID)
FROM tblSettlementAuthority sa
	INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
		WHERE AuthorityRequestedDate BETWEEN @Yesterday4PM AND @Today4PM
		AND AuthorityAmount >= @Threshold) > 0
	BEGIN
		--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
		set @BodyMessage = @BodyMessage + '<H4>The following requests for authority have been made in the last 24 hours.<br/><br/>'
		

		--PRINT SETTLEMENT GROUPS

		--PRINT Settlement Group headers
		SET @BodyMessage = @BodyMessage + '
		<table style="border-collapse: collapse;">
			<thead>
				<tr>
					<th style="' + @MainHeaderStyle + '">Settlement Group</th>
					<th style="' + @MainHeaderStyle + '">Requested By</th>
				</tr>
			</thead>
		'
		--Loop through settlement groups
		DECLARE @SettlementGroup nvarchar(MAX), @AuthorityRequestedBy nvarchar(250), @SettlementGroupID bigint
		DECLARE Curs_SettlementGroups CURSOR LOCAL FAST_FORWARD FOR
		SELECT 
			COALESCE(SettlementGroup, 'Unknown'),
			sa.SettlementGroupID,
			AuthorityRequestedBy
		FROM tblSettlementAuthority sa
			INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
			WHERE AuthorityRequestedDate BETWEEN @Yesterday4PM AND @Today4PM
			AND AuthorityAmount >= @Threshold
			
		OPEN Curs_SettlementGroups
				FETCH NEXT FROM Curs_SettlementGroups INTO @SettlementGroup, @SettlementGroupID, @AuthorityRequestedBy
				WHILE @@FETCH_STATUS = 0 
					BEGIN
						--PRINT Settlement Group info
						SET @BodyMessage = @BodyMessage + '	
						<tr>
							<th style="' + @SubHeaderStyle + '">' + @SettlementGroup  + '</th>
							<th style="' + @SubHeaderStyle + '">' + @AuthorityRequestedBy + '</th>
						</tr>'
						
					--Print claimants related to that settlement group
					SET @BodyMessage = @BodyMessage +

						COALESCE((CAST((SELECT 
							[td/@colspan] = 2,
							[td/@style] = @tdStyle + 'text-align: left;',
							[td/a/@style] = 'color: white;',
							[td/a/@href] = 'https://www.kcicextranet.com/clients/imo/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, c.ClaimantPersonalInfoID)+'&claimanttab=1&claimtab=2',
							[td/a] = cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''),
							[td] = '', ''
						FROM tblClaimantSettlement cs
						INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
						INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
						INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						AND cs.SettlementGroupID = @SettlementGroupID
						FOR XML PATH('tr'), type) AS NVARCHAR(MAX))), '')

						FETCH NEXT FROM Curs_SettlementGroups INTO @SettlementGroup,@SettlementGroupID, @AuthorityRequestedBy
					END      
				CLOSE Curs_SettlementGroups
				DEALLOCATE Curs_SettlementGroups
			
		--CLOSE TABLE
		SET @BodyMessage = @BodyMessage + '</table><H4>To view additional information and approve these settlement authority requests, please click ' + 
		'<a href="http://admin.kcicextranetdev.com/clients/imo/cp/Pages/SettlementAuthority.aspx">here</a>. </H4>'

	END
ELSE
	BEGIN
		SET @BodyMessage = @BodyMessage + '<H4>No authority requests were made today.<H4>'
	END	

IF(SELECT COUNT(sa.SettlementGroupID)
FROM tblSettlementAuthority sa
	INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
	WHERE AuthorityRequestedDate < @FiveDaysAgo4PM
	AND AuthorityAmount >= @Threshold) > 0
	BEGIN
		SET @BodyMessage = @BodyMessage + '<H4>The following requests have been outstanding for more than five days: </H4>'

		--PRINT Settlement Group headers
		SET @BodyMessage = @BodyMessage + '
		<table style="border-collapse: collapse;">
			<thead>
				<tr>
					<th style="' + @MainHeaderStyle + '">Settlement Group</th>
					<th style="' + @MainHeaderStyle + '">Requested By</th>
				</tr>
			</thead>
		'

		DECLARE Curs_SettlementGroups CURSOR LOCAL FAST_FORWARD FOR
		SELECT 
			COALESCE(SettlementGroup, 'Unknown'),
			sa.SettlementGroupID,
			AuthorityRequestedBy
		FROM tblSettlementAuthority sa
			INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
			WHERE AuthorityRequestedDate < @FiveDaysAgo4PM
			AND AuthorityAmount >= @Threshold
			
		OPEN Curs_SettlementGroups
				FETCH NEXT FROM Curs_SettlementGroups INTO @SettlementGroup, @SettlementGroupID, @AuthorityRequestedBy
				WHILE @@FETCH_STATUS = 0 
					BEGIN
						--PRINT Settlement Group info
						SET @BodyMessage = @BodyMessage + '	
						<tr>
							<th style="' + @SubHeaderStyle + '">' + @SettlementGroup  + '</th>
							<th style="' + @SubHeaderStyle + '">' + @AuthorityRequestedBy + '</th>
						</tr>'
						
					--Print claimants related to that settlement group
					SET @BodyMessage = @BodyMessage +

						COALESCE((CAST((SELECT 
							[td/@colspan] = 2,
							[td/@style] = @tdStyle + 'text-align: left;',
							[td/a/@style] = 'color: white;',
							[td/a/@href] = 'https://www.kcicextranet.com/clients/imo/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, c.ClaimantPersonalInfoID)+'&claimanttab=1&claimtab=2',
							[td/a] = cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''),
							[td] = '', ''
						FROM tblClaimantSettlement cs
						INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
						INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
						INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						AND cs.SettlementGroupID = @SettlementGroupID
						FOR XML PATH('tr'), type) AS NVARCHAR(MAX))), '')

						FETCH NEXT FROM Curs_SettlementGroups INTO @SettlementGroup,@SettlementGroupID, @AuthorityRequestedBy
					END      
				CLOSE Curs_SettlementGroups
				DEALLOCATE Curs_SettlementGroups


		--CLOSE TABLE
		SET @BodyMessage = @BodyMessage + '</table>'
	END
--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

DECLARE @RecipientList nvarchar(500)
SET @RecipientList='Traci.Benish@colfaxcorp.com'

SET @BodyMessage = @BodyMessage + '<h4>recipients: ' + @RecipientList + '</h4>'

--Only send it if there are things > 5 days or from the last 24 hours
IF(SELECT COUNT(sa.SettlementGroupID)
FROM tblSettlementAuthority sa
	INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
		WHERE (AuthorityRequestedDate BETWEEN @Yesterday4PM AND @Today4PM OR AuthorityRequestedDate < @FiveDaysAgo4PM) AND AuthorityAmount >= @Threshold) > 0
		BEGIN
			--QC Recipient List
			--Set @Bodymessage = @BodyMessage + 'Recipients' + @RecipientList
			--PRINT @RecipientList
			--Execute stored procedure to send the email to that firm
			EXEC msdb.dbo.sp_send_dbmail
			--The Profile_Name sends from [client]@kcicllc.com email address
			@profile_name='Claims',
			--@recipients = 'scottc@kcicllc.com',
			@recipients = 'crookc@kcicllc.com',--@RecipientList,
			@copy_recipients = '',
			--@recipients = 'amanp@kcicllc.com',
			@subject = @subject,
			@body = @BodyMessage,
			@body_format = 'html'
		END


END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNotifyNewRequest_Client_DELETE] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNotifyNewRequest_Client_DELETE] TO [base_user]
GO
