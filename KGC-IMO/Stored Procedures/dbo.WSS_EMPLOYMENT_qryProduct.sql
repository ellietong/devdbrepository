SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryProduct]
AS 
        SELECT  ProductID ,
            COALESCE(ProductType, 'UNSPECIFIED') + ' - '
            + COALESCE(ProductManufacturer, 'UNSPECIFIED') + ' - '
            + COALESCE(ProductBrand, 'UNSPECIFIED') + ' - '
            + COALESCE(ProductName, 'UNSPECIFIED') AS Product
    FROM    tblProduct p
            LEFT JOIN tblProductBrand pb ON pb.ProductBrandID = p.ProductBrandID
            LEFT JOIN tblProductManufacturer pm ON pm.ProductManufacturerID = p.ProductManufacturerID
            LEFT  JOIN tblProductName pn ON pn.ProductNameID = p.ProductNameID
            LEFT JOIN tblProductType pt ON pt.ProductTypeID = p.ProductTypeID
    UNION
    SELECT  NULL ,
            '' AS Product
    ORDER BY COALESCE(ProductType, 'UNSPECIFIED') + ' - '
            + COALESCE(ProductManufacturer, 'UNSPECIFIED') + ' - '
            + COALESCE(ProductBrand, 'UNSPECIFIED') + ' - '
            + COALESCE(ProductName, 'UNSPECIFIED')
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryProduct] TO [power_user]
GO
