SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] (@SetPymtID INT, 
													@DocumentID int, 
													@DocCategoryID int, 
													@PageNums nvarchar(100), 
													@ClaimantPersonalInfoID bigint = NULL, 
													@Emailaddress nvarchar(250))
AS

--DECLARE @SetPymtID int
--DECLARE @DocumentID int
--DECLARE @DocCategoryID int
--DECLARE @PageNums nvarchar(100)
--DECLARE @ClaimantPersonalInfoID bigint
--DECLARE @EmailAddress nvarchar(100)
--SET @SetPymtID              = NULL
--SET @DocumentID             = NULL
--SET @DocCategoryID          = NULL
--SET @PageNums               = NULL
--SET @ClaimantPersonalInfoID = 70233
--SET @EmailAddress           = 'hubbardb@kcicllc.com' 

DECLARE @ClaimLawsuitID int
SET @ClaimLawsuitID = null

IF(@SetPymtID IS NULL AND @ClaimantPersonalInfoID IS NOT NULL)
BEGIN
	--if there is no record for this claim in tblSettlePaymentDocumentation then create one
	IF (SELECT COUNT(setpymtid) 
		FROM tblSettlePaymentDocumentation 
		WHERE ClaimLawsuitID IN (SELECT ClaimLawsuitID 
								 FROM tblClaimLawsuit cl 
									 INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID 
								 WHERE ClaimantPersonalInfoID  = @ClaimantPersonalInfoID 
									 AND COALESCE(IsPrimaryForClaim, 0) = 1 
									 AND COALESCE(IsCurrentClaim, 0) = 1)) = 0 
	BEGIN
		SET @ClaimLawsuitID = (SELECT cl.ClaimLawsuitID 
							   FROM tblClaimLawsuit cl 
								  INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID 
								  LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
							   WHERE ClaimantPersonalInfoID  = @ClaimantPersonalInfoID 
								  AND COALESCE(IsPrimaryForClaim, 0) = 1 
								  AND COALESCE(IsCurrentClaim, 0) = 1)
									
		--insert claim lawsuit ID for settled record
		IF(@ClaimLawsuitID IS NOT NULL)
		BEGIN
			INSERT INTO tblSettlePaymentDocumentation (ClaimLawsuitID, SPUsername)
			SELECT @ClaimLawsuitID, @Emailaddress
			SET @SetPymtID = @@IDENTITY
		END
	END
END

INSERT INTO tblSettleDocs (SetPymtID, DocumentID, DocCategoryID, PageNums, Spusername)
SELECT @SetPymtID, @DocumentID, @DocCategoryID, @PageNums, @Emailaddress


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [power_user]
GO
