SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE procedure [dbo].[qryALLDefaultAllSumsRanks]
(@allocationid int)
as

set nocount on

-- Checks to see if policies in ranking table match with policies in ztblpolicyallocation
-- Requires an allocation id and an existing (and correct) ztblpolicyallocation as inputs

select sum(case when policyid is null then 1 else 0 end)
from (
	select pa.policyid
	from ztblpolicyallocation pa 
		left join tblallocationallsumsprefs aasp on pa.policyid = aasp.policyid
	where pa.allocationid = @allocationid and aasp.allocationid = @allocationid
	group by pa.policyid
) q

return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLDefaultAllSumsRanks] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLDefaultAllSumsRanks] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLDefaultAllSumsRanks] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLDefaultAllSumsRanks] TO [power_user]
GO
