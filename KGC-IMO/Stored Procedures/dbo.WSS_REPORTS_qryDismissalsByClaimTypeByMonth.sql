SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[WSS_REPORTS_qryDismissalsByClaimTypeByMonth] 
	-- Add the parameters for the stored procedure here
	(@MonthID int, @YearID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @MonthID int
--Set @MonthID=11
--Declare @YearID int
--Set @YearId=2010


DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

    -- Insert statements for procedure here
select	Material as ClaimType, 
		COUNT(distinct(cl.ClaimLawsuitID)) as Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
inner join tblAllegation a on a.ClaimLawsuitID=cl.ClaimLawsuitID
inner join tblMaterial m on m.MaterialID=a.MaterialID
where	YEAR(ProcessedDate)=@year 
		and MONTH(ProcessedDate) =@month
		and ClaimLawsuitStatusGroupID =4
		and  IsPrimaryStatus=1
group by Material
order by Material


END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByClaimTypeByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByClaimTypeByMonth] TO [base_user]
GO
