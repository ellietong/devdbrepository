SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmBilledPaidOutstanding]
(@Billdate datetime ,@paymentdate datetime )
AS
SET NOCOUNT ON
 
-- Declare @Billdate Datetime
-- Set @Billdate = '10/23/2014'

--Declare @paymentdate Datetime
-- Set @paymentdate = '10/23/2014'


Declare @ledger1 table (LedgerID int, CarrierID int, ledgertypeid int, Debit money, ledgerdate datetime, billid int)
Insert Into @Ledger1
Select Ledgerid, CarrierID, LedgerTypeID, 
case when CarrierID = 20 then coalesce(sum(AdjustedDebit),0) else coalesce(sum(Debit),0) end,
ledgerdate, BillID
From tblLedger 
--where @BillDate-LedgerDate >= 0
Group By CarrierID, Ledgerid, LedgerTypeID, ledgerdate, billid

Declare @payment1 table (ledgerid int, carrierid int, ledgertypeid int, Payment money, exhaustiondateid int, billid int)
Insert Into @payment1
Select p.ledgerid, carrierid, LedgerTypeID, coalesce(sum(payment),0), exhaustiondateid, BillID 
From tblPayment p
inner join tblLedger l on l.LedgerID=p.LedgerID
where p.LedgerID in (select LedgerID from @ledger1) and paymentdate<@paymentdate
Group By CarrierID, p.ledgerid, LedgerTypeID, Exhaustiondateid, BillID

Declare @AR1 Table (CarrierID int, ledgerid int, ledgertypeid int, ledgerdate datetime, billid int,
IndemnityBilled money, IndemnityPaid money, IndemnityAccountReceivable money,
DefenseBilled money, DefensePaid money, DefenseAccountReceivable money,
BilledAmount money, PaidAmount money, AccountReceivable money)

Insert Into @AR1

Select l.CarrierID, l.ledgerid, l.ledgertypeid, l.ledgerdate, l.billid,
case when l.ledgertypeid = 7 then Sum(Coalesce(L.Debit,0)) else 0 end as IndemnityBilled,
case when l.ledgertypeid = 7 then sum(coalesce(P.payment,0)) else 0 end as IndemnityPaid,
case when l.ledgertypeid = 7 then Sum(Coalesce(L.Debit,0)-coalesce(P.payment,0)) else 0 end as IndemnityAccountReceivable,
case when l.ledgertypeid = 8 then Sum(Coalesce(L.Debit,0)) else 0 end as DefenseBilled,
case when l.ledgertypeid = 8 then sum(coalesce(P.payment,0)) else 0 end as DefensePaid,
case when l.ledgertypeid = 8 then Sum(Coalesce(L.Debit,0)-coalesce(P.payment,0)) else 0 end as DefenseAccountReceivable,
Sum(Coalesce(L.Debit,0)), sum(coalesce(P.payment,0)), Sum(Coalesce(L.Debit,0)-coalesce(P.payment,0)) AS AccountReceivable   

From @ledger1 L 
Left Join @payment1 P on  L.ledgerID=P.ledgerid
Group By  L.CarrierID, l.ledgerdate, l.ledgerid, l.ledgertypeid, l.billid

Declare @Final table (CarrierGroupID int, CarrierGroupName nvarchar(2000), CarrierName nvarchar(2000), BillNumber int, ledgerdate nvarchar(100), 
IndemnityBilled money, IndemnityPaid money, IndemnityAccountReceivable money,
DefenseBilled money, DefensePaid money, DefenseAccountReceivable money,
BilledAmount money, PaidAmount money, AccountReceivable money)

Insert Into @Final

Select CG.CarrierGroupID, CarrierGroupName, CarrierName, AR1.billid, DATENAME(month,(ledgerdate)) + ' ' + cast(year(ledgerdate) as nvarchar(50)) as ledgerdate,
sum(coalesce(IndemnityBilled,0)) , sum(coalesce(IndemnityPaid,0)) , sum(coalesce(IndemnityAccountReceivable,0)) ,
sum(coalesce(DefenseBilled,0)) , sum(coalesce(DefensePaid,0)) , sum(coalesce(DefenseAccountReceivable,0)) ,
sum(coalesce(BilledAmount,0)) , sum(coalesce(PaidAmount,0)) , sum(coalesce(AccountReceivable,0))

From tblCarrier C
Inner Join @AR1 AR1 on C.CarrierID=AR1.CarrierID
Inner Join tblCarrierGroup CG on CG.CarrierGroupID=C.CarrierGroupID
group by CG.CarrierGroupID, CarrierGroupName, CarrierName,  AR1.billid, DATENAME(month,(ledgerdate)) + ' ' + cast(year(ledgerdate) as nvarchar(50)), ar1.ledgerdate
order by CarrierName, ar1.ledgerdate

select CarrierGroupName, CarrierName, BillNumber, DATENAME(month,(ledgerdate)) + ' ' + cast(year(ledgerdate) as nvarchar(50)) as ledgerdate, (month(ledgerdate)) as Month , year(ledgerdate) as Year,
sum(IndemnityBilled) as IndemnityBilled, sum(IndemnityPaid) as IndemnityPaid, sum(IndemnityAccountReceivable) as IndemnityAccountReceivable,
sum(DefenseBilled) as DefenseBilled, sum(DefensePaid) as DefensePaid, sum(DefenseAccountReceivable) as DefenseAccountReceivable,
sum(BilledAmount) as BilledAmount, sum(PaidAmount) as PaidAmount, sum(AccountReceivable) as AccountReceivable,
@paymentdate as AsOfDate

from @Final
group by CarrierGroupName, CarrierName, BillNumber, DATENAME(month,(ledgerdate)) + ' ' + cast(year(ledgerdate) as nvarchar(50)) , (month(ledgerdate)) , year(ledgerdate) 
 

--Union ALL

--SELECT 'London Market', '5,356.51', '5362.46', '-5.95', 'February 2013', '2', '2013', '2013-09-01'

--Union ALL

--SELECT 'Producer', '350000', '0', '350000', 'July 2012 ', '0', '0', '2013-09-01'

order by CarrierGroupName, CarrierName, year(ledgerdate), month(ledgerdate)
Return
GO
