SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_REPORTS_MONTHLY_qryTopTenNewFilingsByPlaintiffsCounselByMonth] (@MatterID int = NULL, 
                                                                                      @MonthID  int, 
                                                                                      @YearID   int) 
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter 

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

SELECT TOP 10 pca.PlaintiffCounsel, 
	COUNT(DISTINCT(cl.ClaimLawsuitID)) AS Count
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
    LEFT JOIN (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
    INNER JOIN tblPlaintiffCounselAddress pca     ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
WHERE  MONTH(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) = @Month 
	AND YEAR(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) = @Year 
	AND MatterID >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
GROUP BY pca.PlaintiffCounsel
ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC, pca.PlaintiffCounsel


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenNewFilingsByPlaintiffsCounselByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenNewFilingsByPlaintiffsCounselByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenNewFilingsByPlaintiffsCounselByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenNewFilingsByPlaintiffsCounselByMonth] TO [power_user]
GO
