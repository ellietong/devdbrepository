SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qrySettleDocs]
    (
      @SettlePaymentID INT ,
      @emailaddress NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @SettlePaymentID int
--DECLARE @emailaddress nvarchar(250)
--SET @SettlePaymentID = 146
--SET @emailaddress    = 'reichs@dehay.com'

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE d.ClaimantDocumentTypeID <> 40
						AND sd.SetPymtID = '
        + CONVERT(NVARCHAR(20), @SettlePaymentID)

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    DECLARE @JoinString NVARCHAR(MAX)
    IF ( ( RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' )
         OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        SET @JoinString = ' '
    ELSE 
        BEGIN
            SET @JoinString = ' INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
						INNER JOIN tblClaimantDocumentGroup dg ON dg.ClaimantDocumentGroupID = cdt.ClaimantDocumentGroupID '
            SET @WhereString = @WhereString
                + ' AND dg.ClaimantDocumentGroupID NOT IN (2, 4)'
        END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
    DECLARE @QueryString NVARCHAR(MAX)
    SET @QueryString = 'SELECT  --''/clients/IMO/cp/Pages/DocumentProfile.aspx?ItemId='' + CONVERT(nvarchar(100), d.DocumentID) AS DocumentURL, 
		DISTINCT ''/clients/IMO/cp/Pages/DocumentProfile.aspx?ItemId='' + CONVERT(nvarchar(100), d.DocumentID) + ''&source='' + ''/clients/IMO/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='' + CONVERT(nvarchar(100), ClaimantPersonalInfoID) AS DocumentURL, 
		DocumentTitle, 
		PageNums, 
		DocCategory, 
		SettleDocID, 
		sd.SetPymtID, 
		sd.DocumentID, 
		sd.DocCategoryID,
		AmazonKey,
		IsHistorical
	FROM tblSettleDocs sd 
		INNER JOIN tblDocuments d         ON sd.DocumentID = d.DocumentID
		INNER JOIN (SELECT ClaimantPersonalInfoID, SetPymtID  
					FROM tblSettlePaymentDocumentation spd
					INNER JOIN  tblClaimLawsuit cl on spd.ClaimLawsuitID = cl.ClaimLawsuitID
					INNER JOIN  tblClaim c on cl.ClaimID = c.ClaimID) cpi ON sd.SetPymtID = cpi.SetPymtID
		INNER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID '
        + @JoinString + @WhereString + '
    OPTION (RECOMPILE)'
    
    PRINT @Querystring

--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [power_user]
GO
