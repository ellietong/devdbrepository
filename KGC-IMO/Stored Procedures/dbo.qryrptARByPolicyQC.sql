SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByPolicyQC]
(@allocationID int = null)
As
	
SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 25

Declare @ExhaustionTypeID int
Set @exhaustiontypeid = (Select ExhaustionTypeID From tblAllocation Where AllocationID = @allocationID)

Declare @ExhaustionDateID int
set @ExhaustionDateID = (Select ExhaustionDateID from tblAllocation where AllocationID = @allocationid)

DECLARE @exh table (ExhaustionTypeID int, PolicyDateID int, PreviouslyExhaustedAggregate money)
INSERT INTO @exh (ExhaustionTypeID, PolicyDateID, PreviouslyExhaustedAggregate)
SELECT ExhaustionTypeID, pd.PolicyDateID, SUM(IndemnityAllocation + DefenseAllocation)
FROM tblExhaustion e 
	INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID
	INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
WHERE ExhaustionTypeID = @exhaustionTypeID
	AND ExhaustionDate < = (select ExhaustionDate from tblExhaustionDate where ExhaustionDateID = @ExhaustionDateID)
GROUP BY ExhaustionTypeID, pd.PolicyDateID



SELECT ar.DateToPresentValue, MIN(ar.DateTime) AS [DateTime], SUM(ar.Allocation) AS Allocation, Sum(ar.AllocationIndemnity) AS AllocationIndemnity, 
	SUM(ar.AllocationDefense) AS AllocationDefense, SUM(ar.AllocationDefenseOutsideLimits) AS AllocationDefenseOutsideLimits, SUM(ar.PV) AS PV, 
	SUM(ar.SIRAllocated) AS SIRAllocated, pinfo.PolicyDateID, pinfo.PStartDate, pinfo.PEndDate, pinfo.ActualPEndDate, pinfo.ActualPStartDate, 
	pinfo.AnnualPeriod, pinfo.CarrierID, pinfo.CarrierSolvency, pinfo.CarrierName, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END AS 'CarrierSort', pinfo.CarrierGroupID, 
	pinfo.CarrierGroupName, pinfo.PolicyID, 'Defense', 
	Treatment, pinfo.PolicyNum, pinfo.AttachmentPoint, coalesce(e.PreviouslyExhaustedAggregate, 0) as PreviouslyExhaustedAggregate, 
	pinfo.PerOccLimit, pinfo.LayerPerOccLimit, 
	pinfo.SIR, pinfo.AggregateLimit, pinfo.Layer, a.AllocationID, a.AllocationDescription, 
	a.AllocationSchemeID, a.TriggerStartID, a.TriggerStartDate, a.TriggerEndID, a.TriggerEndDate, a.OccurrenceDefId, a.UnCoveredYearstoPH, a.Cost1, a.Cost2, a.Cost3, a.Cost4, a.Cost5, a.Cost6, 
	a.Cost7, a.Cost8, a.Cost9, a.SIRID, a.SIRToPay, a.LessThanAttachmentPoint, a.LessThanLayer, a.OccurrencesInClaim, pinfo.InsuranceProgram, pinfo.InsuranceProgramID
FROM vPolicy pinfo 
	INNER JOIN vAllocationResults ar ON pinfo.PolicyDateID = ar.PolicyDateID
	INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID
	left join @exh e on e.PolicyDateID = pinfo.PolicyDateID 
	Inner Join (Select * From ztblPolicyAllocation Where AllocationID = @AllocationID) z on z.PolicyDateID = pinfo.PolicyDateID
	LEFT JOIN tblTreatment t ON z.Provision1 = t.TreatmentID

WHERE a.AllocationID = COALESCE(@allocationID, 0) AND pinfo.LimitTypeID = (SELECT LimitTypeID FROM tblAllocation WHERE AllocationID = @AllocationID)

GROUP BY ar.DateToPresentValue, pinfo.CarrierID, Treatment, pinfo.CarrierSolvency, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END,
	pinfo.PolicyID, pinfo.InsuranceProgram, pinfo.PolicyDateID, pinfo.PStartDate, pinfo.PEndDate, pinfo.ActualPEndDate, pinfo.ActualPStartDate, 
	pinfo.AnnualPeriod, pinfo.CarrierName, pinfo.PolicyNum, pinfo.AttachmentPoint, e.PreviouslyExhaustedAggregate, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, 
	pinfo.SIR, pinfo.AggregateLimit, pinfo.Layer, a.AllocationID, a.AllocationDescription, a.AllocationSchemeID, a.TriggerStartID, a.TriggerStartDate, a.TriggerEndID, a.TriggerEndDate, a.OccurrenceDefId, 
	a.UnCoveredYearstoPH, a.Cost1, a.Cost2, a.Cost3, a.Cost4, a.Cost5, a.Cost6, a.Cost7, a.Cost8, a.Cost9, a.SIRID, a.SIRToPay, a.LessThanAttachmentPoint, a.LessThanLayer, a.OccurrencesInClaim, 
	pinfo.CarrierGroupID, pinfo.CarrierGroupName, pinfo.InsuranceProgramID, pinfo.AttachmentPoint
	ORDER BY PStartDate, AttachmentPoint

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByPolicyQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByPolicyQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByPolicyQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByPolicyQC] TO [power_user]
GO
