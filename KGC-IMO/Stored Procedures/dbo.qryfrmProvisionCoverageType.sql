SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmProvisionCoverageType]
(@policyID int = NULL)
AS
SET NOCOUNT ON
IF @policyID IS NULL
	SELECT *
	FROM tblProvision
	WHERE ProvisionLabelID = 4
ELSE
	SELECT *
	FROM  tblProvision
	WHERE PolicyID = @policyID
	AND ProvisionLabelID = 4
	ORDER BY ProvisionID
RETURN 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionCoverageType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionCoverageType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionCoverageType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionCoverageType] TO [power_user]
GO
