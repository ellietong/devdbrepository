SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByDiseaseByYear] (@MatterID int = NULL, 
                                                                    @YearID   int, 
                                                                    @MonthID  int) 
AS
SET NOCOUNT ON
	
----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @year int
DECLARE @month int
SET @year = @YearID
SET @month = @monthid

SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
                                              ELSE hld.HighLevelDisease END AS HighLevelDisease, 
	COUNT(distinct(cl.ClaimLawsuitID)) AS 'Count'
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
WHERE  MONTH(ProcessedDate) <= @month 
	AND YEAR(ProcessedDate) = @year
	AND ClaimLawsuitStatusID in (15,16,17,19,36,27) 
	AND IsPrimaryForClaim = 1
	AND IsPrimaryStatus = 1
	AND PrimaryDisease = 1
	AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
ORDER BY hld.HighLevelDiseaseID


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByDiseaseByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByDiseaseByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByDiseaseByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByDiseaseByYear] TO [power_user]
GO
