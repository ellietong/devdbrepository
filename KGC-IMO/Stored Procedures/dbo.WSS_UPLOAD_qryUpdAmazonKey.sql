SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryUpdAmazonKey]
	-- Add the parameters for the stored procedure here
    @AmazonKey NVARCHAR(MAX) ,
    @EmailAddress NVARCHAR(250) ,
    @DocumentID INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        UPDATE  tblDocuments
        SET     AmazonKey = @AmazonKey ,
                ModifiedBy = @EmailAddress
        WHERE   DocumentID = @DocumentID		
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdAmazonKey] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdAmazonKey] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdAmazonKey] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdAmazonKey] TO [power_user]
GO
