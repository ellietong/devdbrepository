SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_Reports_qryTotalActivePending] 
	-- Add the parameters for the stored procedure here
	(@EndDate datetime, @EmailAddress nvarchar(250))	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
--DECLARE @InsurerRole bit
--SET @InsurerRole = (select distinct InsurerRole from vtblPerson where Email = @emailaddress)
--KCIC / noFilters
IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1)
BEGIN

	select COUNT(distinct(cl.claimlawsuitid)) As [Count]
	from tblClaim c 
	inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
	inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	where  coalesce(ServiceDate, ReceivedDate, FileDate, processeddate, statusdate, '1/1/1900') <= @EndDate
	and ((clst.ClaimLawsuitStatusID = 22 and IsPrimaryStatus = 1)
	OR (clst.ClaimLawsuitStatusID not in (22, 29) and IsPrimaryStatus = 1 and coalesce(processeddate, statusdate) > @EndDate))
	
	 

END
--ELSE IF (@InsurerRole = 1)
--BEGIN
	
--	select COUNT(distinct(cl.claimlawsuitid)) As [Count]
--	from tblClaim c 
--	inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
--	inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
--	inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
--	where  coalesce(filedate, servicedate, processeddate, statusdate, '1/1/1900') <= @EndDate
--	and ((clst.ClaimLawsuitStatusID = 22 and IsPrimaryStatus = 1)
--	OR (clst.ClaimLawsuitStatusID not in (22, 29) and IsPrimaryStatus = 1 and coalesce(processeddate, statusdate) >= @EndDate))
--	END
ELSE
BEGIN

	select COUNT(distinct(cl.claimlawsuitid)) As [Count]
	from tblClaim c 
	inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
	inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
	where coalesce(ServiceDate, ReceivedDate, FileDate, processeddate, statusdate, '1/1/1900') <= @EndDate
	and ((clst.ClaimLawsuitStatusID = 22 and IsPrimaryStatus = 1)
	OR (clst.ClaimLawsuitStatusID not in (22, 29) and IsPrimaryStatus = 1 and coalesce(processeddate, statusdate) > @EndDate))
	AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)

END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Reports_qryTotalActivePending] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Reports_qryTotalActivePending] TO [base_user]
GO
