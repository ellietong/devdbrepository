SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryALLSplitPolicies]
(@allocationID int = null, @strWhere varchar(4000) = null, @considerPrevExhaustion bit, @exhaustionTypeID int, @limitTypeID int = null)
AS
SET NOCOUNT ON
DECLARE @strSQL nvarchar(4000)
DELETE 
FROM ztblPolicyAllocation
WHERE AllocationID = @allocationID
SET @strSQL = 
	'DECLARE @previousExhaustion table (PolicyDateID int, AllocationTotal money)
	INSERT INTO @previousExhaustion 
	SELECT PolicyDateID, 
	SUM(IndemnityAllocation) + SUM(DefenseAllocation) AS AllocationTotal 
	FROM tblExhaustion a INNER JOIN tblExhaustionDate b ON a.ExhaustionDateID = b.ExhaustionDateID
	WHERE ExhaustionTypeID = ' + CONVERT(varchar(10), @exhaustionTypeID) + ' 
	GROUP BY PolicyDateID 
	
	INSERT INTO ztblPolicyAllocation (AllocationID, PolicyID, PolicyDateID, Provision1, PStartDate, PEndDate, AnnualPeriod, InsuranceProgramID, 
	AttachmentPoint, SIR, PerOccLimit, LayerPerOccLimit, AggregateLimit, OccurrenceAllocation, OccurrenceRemaining, CumAllocation, 
	CumAllocationIndemnity, CumAllocationDefense, CumAllocationDefenseOutsideLimits, CumSIR, CumSIRIndemnity, CumSIRDefense, 
	AggregateAllocated, AggregateRemaining, SIRAllocated, SIRProRation, SIRremaining, Exhausted, ColumnAllocation, ColumnSIR, DisallowedAllocation, DisallowedPolicy)
	
	SELECT ' + convert(varchar(10), @allocationID) + ', a.PolicyID, c.PolicyDateID, b.ProvisionValueID as Provision1, c.PStartDate, c.PEndDate, 
	c.AnnualPeriod, a.InsuranceProgramID, d.AttachmentPoint, d.SIR, d.PerOccLimit, d.LayerPerOccLimit, d.AggregateLimit - ' 
	+ convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0), 0 AS OccurrenceAllocation, 
   	(CASE WHEN AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0)  <= PerOccLimit 
		THEN CASE WHEN AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) < 0 
			THEN 0 
			ELSE AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) 
		END 	
		ELSE PerOccLimit 
	END)AS OccurrenceRemaining, 0 AS CumAllocation, 0 AS CumAllocationIndemnity, 0 AS CumAllocationDefense, 0 AS CumAllocationDefenseOutsideLimits, 
	0 AS CumSIR, 0 AS CumSIRIndemnity, 0 AS CumSIRDefense, 0 AS AggregateAllocated,
	(CASE WHEN AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) > 0 
		THEN AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) 
		ELSE -1 END) AS AggregateRemaining, 
	0 AS SIRAllocated, 0 AS SIRProRation, SIR AS SIRremaining, 0 AS Exhausted, 0 AS ColumnAllocation, 0 AS ColumnSIR, 0 as DisallowedAllocation, 0 as DisallowedPolicy
	FROM (tblPolicy a INNER JOIN tblProvision b ON a.PolicyID = b.policyID INNER JOIN tblPolicyDates c ON a.PolicyID = c.PolicyID
	INNER JOIN tblPolicyLimits d ON a.PolicyID = d.PolicyID) LEFT JOIN @previousExhaustion e ON e.PolicyDateID = c.PolicyDateID
	WHERE b.ProvisionLabelID = 1 AND (d.AttachmentPoint IS NOT NULL) AND (d.PerOccLimit IS NOT NULL) AND 
	(d.PerOccLimit <> 0) AND (d.LayerPerOccLimit IS NOT NULL) AND (d.LayerPerOccLimit >= PerOccLimit) 
	
	AND (d.SIR IS NOT NULL) AND (d.AggregateLimit IS NOT NULL) AND (c.PStartDate IS NOT NULL) 
	AND LimitTypeID = ' + convert(varchar(10), @limitTypeID)

----NOTE: Limiting to only positive LayerPerOccLimits removed by CY on 10/26/2012 to allow for negative adjustments to column limits.----
--AND (d.LayerPerOccLimit > 0) 

IF @strWhere IS NOT NULL OR @strWhere <> ''
	SET @strSQL = @strSQL + ' AND ' + @strWhere
--print @strSQL
EXEC sp_executesql @strSQL
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLSplitPolicies] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLSplitPolicies] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLSplitPolicies] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLSplitPolicies] TO [power_user]
GO
