SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[WSS_RELATED_PARTY_qryInsClaimantRelatedParty]
    (
      @FirstName NVARCHAR(100) ,
      @MiddleName NVARCHAR(100) ,
      @LastName NVARCHAR(100) ,
      @RelationshipID INT ,
      @LegalCapacityID INT ,
      @Address1 NVARCHAR(250) ,
      @Address2 NVARCHAR(250) ,
      @City NVARCHAR(250) ,
      @StateID INT ,
      @Zip NVARCHAR(100) ,
      @MailZip NVARCHAR(100) ,
      @Phone NVARCHAR(100) ,
      @Extension NVARCHAR(100) ,
      @SSN NVARCHAR(50) ,
      @ClaimantPersonalInfoID BIGINT ,
	  @GenderID INT,
	  @BirthDate Date,
	  @LossofConsortium NVARChar(5),
      @emailaddress NVARCHAR(100)
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        INSERT  INTO tblClaimantRelatedParty(ClaimantPersonalInfoID, FirstName, MiddleName, LastName, RelationshipID, LegalCapacity,LegalCapacityID, RPAddress1, RPAddress2, RPCity, StateID, RPZip, RPMailZip, RPPhone,
		RPExtension, RPSocialSecurityNumber,SPUserName, RPGender, RPBirthDate, LossOfConsortiumClaimant)
                SELECT  @ClaimantPersonalInfoID ,
                        UPPER(@FirstName) ,
                        UPPER(@MiddleName) ,
                        UPPER(@LastName) ,
                        @RelationshipID ,
                        NULL AS LegalCapacity ,
                        @LegalCapacityID ,
                        UPPER(@Address1) ,
                        UPPER(@Address2) ,
                        UPPER(@City) ,
                        @StateID ,
                        @Zip ,
                        @MailZip ,
                        CONVERT(BIGINT, REPLACE(REPLACE(REPLACE(@Phone, '-',
                                                              ''), '(', ''),
                                                ')', '')) ,
                        @Extension ,
                        @SSN ,
                        @Emailaddress,
						@GenderID ,
						@BirthDate,
						CASE WHEN @LossofConsortium like 'YES' Then 1 Else 0 END
						 
	END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryInsClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryInsClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryInsClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryInsClaimantRelatedParty] TO [power_user]
GO
