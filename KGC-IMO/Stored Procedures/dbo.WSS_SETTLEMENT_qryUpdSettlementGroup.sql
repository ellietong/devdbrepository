SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryUpdSettlementGroup]
	-- Add the parameters for the stored procedure here
(@SettlementGroupID int)
AS
BEGIN

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

UPDATE tblSettlementGroup
SET PaymentTermsID = 1
WHERE SettlementGroupID = @SettlementGroupID
	
END

GO
