SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_Search_qryPlaintiffCounselList]
AS 
    SET NOCOUNT ON

    SELECT  pca.PlaintiffCounselID ,
            PlaintiffCounsel 
    FROM    tblPlaintiffCounselAddress pca
			INNER JOIN tblLawsuit l ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
    WHERE   PlaintiffCounsel IS NOT NULL
            AND PlaintiffCounsel <> ''
    UNION
    SELECT  NULL ,
            NULL
    ORDER BY PlaintiffCounsel,
            pca.PlaintiffCounselID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Search_qryPlaintiffCounselList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Search_qryPlaintiffCounselList] TO [base_user]
GO
