SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryDocList]
    (
      @ClaimantPersonalInfoID INT ,
      @emailaddress NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON;

----The following set of variables can be used for testing.
--DECLARE @emailaddress nvarchar(250)
--DECLARE @ClaimantPersonalInfoID bigint
--SET @emailaddress = 'cgong@cetcap.com'
--SET @ClaimantPersonalInfoID = 250339

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE mcd.ClaimantPersonalInfoID = '
        + CONVERT(NVARCHAR(20), @ClaimantPersonalInfoID)


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    DECLARE @JoinString NVARCHAR(MAX)
    IF ( ( RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' )
         OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        SET @JoinString = ' '
    ELSE 
        BEGIN
            SET @JoinString = ' INNER JOIN tblClaimantDocumentGroup dg ON dg.ClaimantDocumentGroupID = cdt.ClaimantDocumentGroupID '
	
            SET @WhereString = @WhereString
                + ' AND dg.ClaimantDocumentGroupID NOT IN (2, 4) AND cdt.ClaimantDocumentTypeID NOT IN (1, 40)'
        END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
    DECLARE @QueryString NVARCHAR(MAX)
    SET @QueryString = 'SELECT d.DocumentID AS DocumentID, COALESCE(ClaimantDocumentType +'': '','''') + DOCUMENTTITLE AS DocumentTitle
	 FROM tblDocuments d 
		 LEFT JOIN  tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
		 INNER JOIN tblMultiClaimantDocument mcd ON mcd.DocumentID=d.DocumentID '
        + @JoinString + @WhereString + '
     UNION
     SELECT NULL AS DocumentID, ''-- Select a Document --''  AS DocumentTitle
     ORDER BY 2
     OPTION (RECOMPILE)'

--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDocList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDocList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDocList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDocList] TO [power_user]
GO
