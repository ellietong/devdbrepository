SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptBillingSummary] (@AllocationID int, @CarrierGroupID INT)
As
Set Nocount on

--DECLARE @AllocationID INT
--SET @AllocationID = 24 

--DECLARE @CarrierGroupID INT
--SET @CarrierGroupID = 31

--SELECT * FROM tblCarrierGroup ORDER BY CarrierGroupName

DECLARE @Month NVARCHAR(MAX)
SET @Month = (SELECT Month FROM tblAllocation WHERE AllocationID=@AllocationID)

DECLARE @Year INT
SET @Year = (SELECT Year FROM tblAllocation WHERE AllocationID=@AllocationID)

DECLARE @BillNumber INT
SET @BillNumber = (SELECT BillNumber FROM tblAllocation WHERE AllocationID=@AllocationID)



Declare @AllocationResults table (PolicyDateID int, AllocationDefense money, AllocationDefenseOutsideLimits money, AllocationIndemnity money, Allocation money)
Insert Into @AllocationResults
Select PolicyDateID, Sum(AllocationDefense), Sum(AllocationDefenseOutsideLimits), Sum(AllocationIndemnity), SUM(Allocation)
From tblAllocationResults ar
Where AllocationID = @AllocationID
Group By PolicyDateID


--------Query Results
Declare @Results Table(CarrierGroupName nvarchar(100), CarrierName nvarchar(100), PolicyNum nvarchar(100), PStartDate datetime, PEndDate datetime,
		Indemnity money, Defense money, Adjustments money, Percentage float)
Insert Into @Results

Select CarrierGroupName, CarrierName, p.policynum, pd.pstartdate, pd.penddate,
    Sum(Coalesce(AllocationIndemnity,0)) as Indemnity,
    Sum(Coalesce(AllocationDefense+AllocationDefenseOutsideLimits,0)) as Defense,
	0 as Adjustments, Null as Percentage
 from tblPolicy p
	Inner Join tblPolicyDates pd on p.PolicyID = pd.policyID
	Inner Join @AllocationResults ar on ar.PolicyDateID = pd.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	INNER JOIN tblCarrierGroup CG ON CG.CarrierGroupID = C.CarrierGroupID
 where C.CarrierGroupID = @CarrierGroupID and c.CarrierID <> 1
 Group by CarrierGroupName, CarrierName, PolicyNum, PStartDate, PEndDate	

Select CarrierGroupName, CarrierName, PolicyNum, PStartDate, PEndDate, SUM(Indemnity) AS Indemnity, SUM(Defense) AS Defense, SUM(Adjustments) AS Adjustments, 
@Month as Month, @Year as Year, @BillNumber as BillNumber
From @Results r
GROUP BY CarrierGroupName, CarrierName, PolicyNum, PStartDate, PEndDate
Order By CarrierName, PStartDate

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [power_user]
GO
