SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryClaimantProduct]
    (
      @ClaimantEmploymentID INT
    )
AS 
    SET NOCOUNT ON

       SELECT  ClaimantEmploymentID ,
            StartDate ,
            EndDate ,
            cp.ProductID ,
            ProductBrand ,
            ProductManufacturer ,
            COALESCE(ProductType + '-' + ProductBrand, 'UNSPECIFIED') AS ProductName ,
            ProductType ,
            ClaimantProductID ,
            Notes
    FROM    tblClaimantProduct cp
            LEFT JOIN tblProduct p ON p.ProductID = cp.ProductID
            LEFT JOIN tblProductBrand pb ON pb.ProductBrandID = p.ProductBrandID
            LEFT JOIN tblProductManufacturer pm ON pm.ProductManufacturerID = p.ProductManufacturerID
            LEFT JOIN tblProductName pn ON pn.ProductNameID = p.ProductNameID
            LEFT JOIN tblProductType pt ON pt.ProductTypeID = p.ProductTypeID
    WHERE   ClaimantEmploymentID = @ClaimantEmploymentID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryClaimantProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryClaimantProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryClaimantProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryClaimantProduct] TO [power_user]
GO
