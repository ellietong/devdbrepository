SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qrySettlementsByDisease] 
	-- Add the parameters for the stored procedure here
	(@StartDate datetime, @EndDate datetime, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


CREATE Table #tempDates (ClaimLawsuitID int, MinDate date)
insert into #tempDates
select ClaimLawsuitID, min(coalesce(ProcessedDate, StatusDate, '1/1/1900'))
from tblClaimLawsuitStatusTracking clst
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID 
where ClaimLawsuitStatusGroupID=1
Group by ClaimLawsuitID

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = 0--(select distinct InsurerRole from vtblPerson where Email = @emailaddress)

--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1)
BEGIN

select CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
                                              ELSE hld.HighLevelDisease END AS Disease, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join #tempDates temp on cl.ClaimLawsuitID=temp.ClaimLawsuitID
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
	INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
where	--coalesce(ProcessedDate, StatusDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		--and ClaimLawsuitStatusGroupID =1
		--and  IsPrimaryStatus=1
		temp.MinDate BETWEEN @StartDate AND @EndDate
		and PrimaryDisease=1
		
GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
ORDER BY hld.HighLevelDiseaseID

END
ELSE IF(@InsurerRole = 1) --Insurers
BEGIN

select CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
                                              ELSE hld.HighLevelDisease END AS Disease, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join #tempDates temp on cl.ClaimLawsuitID=temp.ClaimLawsuitID
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
	INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
where	--coalesce(ProcessedDate, StatusDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		--and ClaimLawsuitStatusGroupID =1
		--and  IsPrimaryStatus=1
		temp.MinDate BETWEEN @StartDate AND @EndDate
		and PrimaryDisease=1
		
GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
ORDER BY hld.HighLevelDiseaseID


END

ELSE --Local Defense Counsels
BEGIN

select CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
                                              ELSE hld.HighLevelDisease END AS Disease, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join #tempDates temp on cl.ClaimLawsuitID=temp.ClaimLawsuitID
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
	INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
inner join tblLawsuit l on l.LawsuitID = cl.LawsuitID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
where	--coalesce(ProcessedDate, StatusDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		--and ClaimLawsuitStatusGroupID =1
		--and  IsPrimaryStatus=1
		temp.MinDate BETWEEN @StartDate AND @EndDate
		and (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
		and PrimaryDisease=1
GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
ORDER BY hld.HighLevelDiseaseID


END

DROP TABLE #tempDates

END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementsByDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementsByDisease] TO [base_user]
GO
