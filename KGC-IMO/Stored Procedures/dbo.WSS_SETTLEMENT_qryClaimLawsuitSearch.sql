SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] 
(
 @ClaimantName nvarchar(500) = null, 
 @StateID int = null, 
 @SSN nvarchar(15) = null, 
 @MatterID int = null, 
 @DocketNumber nvarchar(500) = null,
 @CaseCaption nvarchar(500) = null, 
 @HighLevelDiseaseID int = null, 
 @PlaintiffCounselAddressID int = null, 
 @SettlementGroupID	int = null,
 @Emailaddress nvarchar(250)
)
AS
BEGIN

--DECLARE @ClaimLawsuitID bigint = 226053
--DECLARE @ClaimantName nvarchar(500) = 'smith'
--DECLARE @StateID int = 17
--DECLARE @SSN nvarchar(15) = ''
--DECLARE @MatterID int = null
--DECLARE @DocketNumber nvarchar(500) = null
--DECLARE @CaseCaption nvarchar(500) = null
--DECLARE @HighLevelDiseaseID int = null
--DECLARE @PlaintiffCounselAddressID int = null
--DECLARE @SettlementGroupID int = null

SET NOCOUNT ON

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @WhereString nvarchar(MAX) = ''
--Construct Where String
IF(@ClaimantName IS NOT NULL)
	SET @WhereString = @WhereString + ' AND LastName + COALESCE('' '' + sfx.Suffix, '''') + '', '' + FirstName + COALESCE('' '' + MiddleName, '''') LIKE ''%' + @ClaimantName + '%'''
IF(@StateID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND l.StateID = ' + CONVERT(nvarchar, @StateID)
IF(@SSN IS NOT NULL)
	SET @WhereString = @WhereString + ' AND cpi.SocialSecurityNumber LIKE ''%'+ @SSN + '%'''
IF(@MatterID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar, @MatterID)
IF(@DocketNumber IS NOT NULL)
	SET @WhereString = @WhereString + ' AND l.DocketNumber LIKE ''%' + @DocketNumber + '%'''
IF(@CaseCaption IS NOT NULL)
	SET @WhereString = @WhereString + ' AND l.CaseCaption LIKE ''%' + @CaseCaption + '%'''
IF(@HighLevelDiseaseID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND d.HighLevelDiseaseID = ' + CONVERT(nvarchar, @HighLevelDiseaseID)
IF(@PlaintiffCounselAddressID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND l.PlaintiffCounselAddressID = ' + CONVERT(nvarchar, @PlaintiffCounselAddressID)
IF(@SettlementGroupID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND cs.SettlementGroupID = ' + CONVERT(nvarchar, @SettlementGroupID)



DECLARE @JoinString nvarchar(MAX) = ''
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' LEFT JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						LEFT JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

DECLARE @QueryString nvarchar(MAX) = 
'
SELECT DISTINCT
		c.ClaimantPersonalInfoID,
		cl.ClaimLawsuitID, 
		LastName + COALESCE('' '' + sfx.Suffix, '''') + '', '' + FirstName + COALESCE('' '' + MiddleName, '''')  as ClaimantName,
		DocketNumber,
		HighLevelDisease,
		Matter,
		CaseCaption,
		s.[State],
		ClaimLawsuitStatus,
		NULL as AuthorityStatus,
		COALESCE(cs.SettlementGroupID, 0) AS SettlementGroupID,
		COALESCE(SettlementGroup, '''') AS SettlementGroup
FROM tblClaimLawsuit cl 
	LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblSuffix sfx ON cpi.SuffixID = sfx.SuffixID
	LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID ' + 
	@JoinString 
	+'
	LEFT JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimID
	LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
	LEFT JOIN tblDisease d ON cd.DiseaseID = d.DiseaseID
	LEFT JOIN tblHighLevelDisease hld ON d.HighLevelDiseaseID = hld.HighLevelDiseaseID
	LEFT JOIN tblState s ON l.StateID = s.StateID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	LEFT JOIN tblClaimantSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	LEFT JOIN tblSettlementGroup sg on cs.SettlementGroupID = sg.SettlementGroupID
WHERE	PrimaryDisease = 1 
		AND IsCurrentClaim = 1
		AND IsPrimaryForClaim = 1
		AND IsPrimaryStatus = 1
		AND (cls.ClaimLawsuitStatusGroupID = 3 or cls.ClaimLawsuitStatusID = 44)
		' + @WhereString + '
ORDER BY ClaimantName'
PRINT @QueryString
EXEC sp_executesql @QueryString

END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] TO [base_user]
GO
