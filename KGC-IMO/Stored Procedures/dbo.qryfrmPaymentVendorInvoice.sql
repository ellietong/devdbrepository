SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmPaymentVendorInvoice] (@VendorFirmID int)
As
Set NoCount On

--declare @VendorFirmID int
--set @VendorFirmID = 29


select di.DefenseInvoiceID, di.VendorFirmID as VendorFirmID, di.InvoiceNumber, di.Period, InvoiceTotal, InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount
from vtblDefenseInvoice di
left join tblDefenseInvoice div on div.DefenseInvoiceID = di.DefenseInvoiceID
left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
			 a on a.DefenseInvoiceID = di.DefenseInvoiceID
where di.VendorFirmID is not null and IsApproved = 1 and IsRecommended = 1 
Group By di.DefenseInvoiceID, di.VendorFirmID, di.InvoiceNumber, di.Period, InvoiceTotal, div.VendorFirmID
Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0 and div.VendorFirmID = @VendorFirmID

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [power_user]
GO
