SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceSummary_ByFirm] (@FirmIDFilter int, 
																		 @StatusID	   int = null)
AS
SET NOCOUNT ON

--Adding Paid Date (Check Date) 4/3/2012

DECLARE @FirmFilter int
DECLARE @StatusFilter int
SET @FirmFilter = @FirmIDFilter
SET @StatusFilter = @StatusID

DECLARE @MaxStatusID int
SET @MaxStatusID = (SELECT MAX(LegalBillStatusID) 
					FROM m_tblLegalBillStatus)

DECLARE @UnpaidInvoice TABLE (InvoiceID int, IsFullyPaid bit)
INSERT INTO @UnpaidInvoice
	SELECT i.InvoiceID, 
		CASE WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 0 THEN 0 
			 WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 1 THEN 1
															 ELSE IsFullyPaid END AS IsFullyPaid 
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblPaymentLegalBill lb ON i.InvoiceID = lb.InvoiceID
	GROUP BY i.InvoiceID, CASE WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 0 THEN 0 WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 1 THEN 1 ELSE IsFullyPaid END

IF(@StatusID IS NULL)
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		CASE WHEN OriginalInvoiceNumber IS null THEN 'Unknown' 
												ELSE OriginalInvoiceNumber END AS OriginalInvoiceNumber, 
		MAX(Period) AS MaxPeriod,
		SubmittedDate, 
		MAX(IMOApprovalDate) AS MaxApprovedDate, 
		CASE WHEN s.LegalBillStatus IS null THEN 'Unknown' 
			 When u.IsFullyPaid = 1			THEN 'Paid'
			 When NavPaidInvoice = 1		THEN 'Paid By Navigant' 
											ELSE s.LegalBillStatus END AS Status, 
		SourceDate as PaidDate,
		Sum(OriginalTotal) AS OriginalInvoiceTotal, 
		CASE WHEN AppealFlag = 1 THEN Sum(AppealTotal) 
								 ELSE Sum(AdjustedTotal) END AS FinalizedInvoiceTotal
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m				 ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s	 ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u			 ON i.InvoiceID = u.InvoiceID
		LEFT JOIN tblPaymentLegalBill plb	 ON plb.InvoiceID = i.InvoiceID
		LEFT JOIN tblCheck c				 ON c.CheckID = plb.CheckID
	WHERE (COALESCE(submitteddate, IMOapprovaldate, period) >= (GETDATE()-365)
		OR u.IsFullyPaid = 0 
		OR i.NavPaidInvoice = 0) 
		AND i.DefenseCounselAddressID = @FirmFilter
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel, Matter, SubmittedDate,
		CASE WHEN OriginalInvoiceNumber IS null THEN 'Unknown' ELSE OriginalInvoiceNumber END, 
		CASE WHEN s.LegalBillStatus IS null THEN 'Unknown' WHEN u.IsFullyPaid = 1 THEN 'Paid' WHEN NavPaidInvoice = 1 THEN 'Paid By Navigant' ELSE s.LegalBillStatus END,
		SourceDate, AppealFlag
	ORDER BY SubmittedDate DESC, DefenseCounsel, Matter
END
ELSE
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		CASE WHEN OriginalInvoiceNumber IS null THEN 'Unknown' 
												ELSE OriginalInvoiceNumber END AS OriginalInvoiceNumber, 
		MAX(Period) AS MaxPeriod,
		SubmittedDate, 
		MAX(IMOApprovalDate) AS MaxApprovedDate, 
		CASE WHEN s.LegalBillStatus IS null THEN 'Unknown' 
			 When u.IsFullyPaid = 1			THEN 'Paid'
			 When NavPaidInvoice = 1		THEN 'Paid By Navigant' 
											ELSE s.LegalBillStatus END AS Status, 
		SourceDate as PaidDate,
		Sum(OriginalTotal) AS OriginalInvoiceTotal, 
		CASE WHEN AppealFlag = 1 THEN Sum(AppealTotal) 
								 ELSE Sum(AdjustedTotal) END AS FinalizedInvoiceTotal
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m				 ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s	 ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u			 ON i.InvoiceID = u.InvoiceID
		LEFT JOIN tblPaymentLegalBill plb	 ON plb.InvoiceID = i.InvoiceID
		LEFT JOIN tblCheck c				 ON c.CheckID = plb.CheckID
	WHERE (COALESCE(submitteddate, IMOapprovaldate, period) >= (GETDATE()-365)
		OR u.IsFullyPaid = 0 
		OR i.NavPaidInvoice = 0) 
		AND i.DefenseCounselAddressID = @FirmFilter
		AND i.StatusID = @StatusFilter
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel, Matter, SubmittedDate,
		CASE WHEN OriginalInvoiceNumber IS null THEN 'Unknown' ELSE OriginalInvoiceNumber END, 
		CASE WHEN s.LegalBillStatus IS null THEN 'Unknown' WHEN u.IsFullyPaid = 1 THEN 'Paid' WHEN NavPaidInvoice = 1 THEN 'Paid By Navigant' ELSE s.LegalBillStatus END,
		SourceDate, AppealFlag
	ORDER BY SubmittedDate DESC, DefenseCounsel, Matter
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceSummary_ByFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceSummary_ByFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceSummary_ByFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceSummary_ByFirm] TO [power_user]
GO
