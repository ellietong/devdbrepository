SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverContact]
(@SettlementApproverContactID int = NULL)
AS
BEGIN
	
		SELECT	sac.*,
			LName + COALESCE(', ' + FName, '') + ' - ' + Email + ' (' + dc.DefenseCounsel + ')' AS Contact
		FROM tblSettlementApproverContact sac
		INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
		INNER JOIN tblDefenseCounsel dc ON p.FirmID = dc.DefenseCounselID
		WHERE SettlementApproverContactID = @SettlementApproverContactID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverContact] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverContact] TO [base_user]
GO
