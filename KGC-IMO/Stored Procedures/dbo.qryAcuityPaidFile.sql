SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryAcuityPaidFile] 

AS
SET NOCOUNT ON;

PRINT 'BATCH_ID|FIRM_KEY|CHECK_DATE|CHECK_NUMBER|PAID_AMOUNT'

SELECT InvoiceNumber as BATCH_ID, 
	coalesce(dc.AcuityKey,v.AcuityKey) as FIRM_KEY,
	CONVERT(VARCHAR(10), SourceDate, 101) as CHECK_DATE,
	SourceNumber as CHECK_NUMBER,
	PaymentAmount as PAID_AMOUNT
FROM tblDefenseInvoice di
	INNER JOIN tblPaymentDefense pd on di.DefenseInvoiceID=pd.DefenseInvoiceID
	INNER JOIN tblCheck c on c.CheckID=pd.CheckID
	LEFT JOIN tblDefenseCounselAddress dca on dca.DefenseCounselAddressID=di.DefenseCounselAddressID
	LEFT JOIN tblDefenseCounsel dc on dca.DefenseCounselID=dc.DefenseCounselID
	LEFT JOIN tblVendor v on v.VendorID=di.VendorFirmID
Where SourceDate = CONVERT(DATE,GETDATE()) 
--SourceDate = '12/20/2013'
GO
GRANT VIEW DEFINITION ON  [dbo].[qryAcuityPaidFile] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryAcuityPaidFile] TO [base_user]
GO
