SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_CLAIMANT_qryDelClaimantDocument]
    (
      @ClaimantPersonalInfoID BIGINT ,
      @DocumentID INT ,
      @Emailaddress NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON

--Update the SPUsername field in tblMultiClaimantDocument prior to delete for audit log purposes
    UPDATE  tblMultiClaimantDocument
    SET     SPUsername = @emailaddress
    WHERE   DocumentID = @DocumentID
            AND ClaimantPersonalInfoID = @ClaimantPersonalInfoID

--Delete the the relevant tblMultiClaimantDocument record
    DELETE  FROM tblMultiClaimantDocument
    WHERE   DocumentID = @DocumentID
            AND ClaimantPersonalInfoID = @ClaimantPersonalInfoID

--Update settleDoc record prior to delete for audit log
    UPDATE  tblSettleDocs
    SET     SPUsername = @Emailaddress
    FROM    tblSettleDocs sd
            INNER JOIN tblSettlePaymentDocumentation spd ON sd.SetPymtID = spd.SetPymtID
            INNER JOIN tblClaimLawsuit cl ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
            INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
    WHERE   c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
            AND sd.DocumentID = @DocumentID

--Delete related settledocs record
    DELETE  tblSettleDocs
    FROM    tblSettleDocs sd
            INNER JOIN tblSettlePaymentDocumentation spd ON sd.SetPymtID = spd.SetPymtID
            INNER JOIN tblClaimLawsuit cl ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
            INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
    WHERE   c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
            AND sd.DocumentID = @DocumentID

--If there was only 1 entry in tblMultiClaimantDocument for the document, delete the tblDocuments record as well
    IF ( SELECT COUNT(MultiClaimantDocumentID)
         FROM   tblMultiClaimantDocument
         WHERE  DocumentID = @DocumentID
       ) = 0 
        BEGIN
	--Select the file path
            SELECT  AmazonKey
            FROM    tblDocuments
            WHERE   DocumentID = @DocumentID
	
	--Update the ModifiedBy field in tblDocuments prior to delete for audit log purposes
            UPDATE  tblDocuments
            SET     ModifiedBy = @emailaddress
            WHERE   DocumentID = @DocumentID
	
	--Finally, do the delete
            DELETE  FROM tblDocuments
            WHERE   DocumentID = @DocumentID
        END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryDelClaimantDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryDelClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryDelClaimantDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryDelClaimantDocument] TO [power_user]
GO
