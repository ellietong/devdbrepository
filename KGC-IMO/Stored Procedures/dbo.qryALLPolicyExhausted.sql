SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryALLPolicyExhausted] (@PolicyDateID int, @AllocationID int)
As
Set NoCount on

Select Case When OccurrenceRemaining > 0.01 Then 0 Else 1 End as IsExhausted
From ztblPolicyAllocation
Where AllocationID = @AllocationID And PolicyDateID = @PolicyDateID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLPolicyExhausted] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLPolicyExhausted] TO [base_user]
GO
