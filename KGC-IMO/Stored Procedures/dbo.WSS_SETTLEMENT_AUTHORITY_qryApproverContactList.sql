SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverContactList]
AS
BEGIN
	
		SELECT NULL AS SettlementApproverContactID, NULL as Contact
		UNION
		SELECT	SettlementApproverContactID, 
				LName + COALESCE(', ' + FName, '') + ' - ' + Email + ' (' + dc.DefenseCounsel + ')' AS Contact
		FROM tblSettlementApproverContact sac
		INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
		INNER JOIN tblDefenseCounsel dc ON p.FirmID = dc.DefenseCounselID
		WHERE IsDefault = 1
		ORDER BY Contact
END	

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverContactList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverContactList] TO [base_user]
GO
