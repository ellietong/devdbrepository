SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID_2] (@DocId int)

AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantName              nvarchar(max)
--DECLARE @SSN                       nvarchar(20)
--DECLARE @StateID                   int
--DECLARE @PlaintiffCounselAddressID int
--DECLARE @NavInjPartyID               bigint
--DECLARE @MatterID                  int
--DECLARE @IsMDL                     int
--DECLARE @emailaddress              nvarchar(250)
--DECLARE @DocketNumber              nvarchar(250)
--DECLARE @ClaimLawsuitStatusGroupID int
--DECLARE @ClaimantIDs               TABLE (n int) 
--SET @ClaimantName              = NULL
--SET @SSN                       = NULL
--SET @StateID                   = NULL
--SET @PlaintiffCounselAddressID = NULL
--SET @NavInjPartyID               = NULL
--SET @MatterID                  = NULL
--SET @IsMDL                     = NULL
--SET @emailaddress              = 'youngc@kcicllc.com'
--SET @DocketNumber              = NULL
--SET @ClaimLawsuitStatusGroupID = NULL

DECLARE @DocGroupID int = (SELECT ClaimantDocumentGroupID FROM tblDocuments d
								INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID  
						  WHERE DocumentID = @docid)

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
DECLARE @Field nvarchar(250)
IF @DocGroupID = 2
BEGIN 
	SET @JoinString = ' INNER JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
		INNER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID
		INNER JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID '
	SET @Field = ', StatusID'
END	
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaim c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID '
	SET @Field = ', NULL AS StatusID'			
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT d.CreatedBy,
		COALESCE(d.Accepted, 0) AS Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		DefenseCounselID as FirmID,
		cdt.ClaimantDocumentGroupID,
		AmazonKey AS DocumentURL,
		d.ClaimantDocumentTypeID,
		AmazonKey' + 
		@Field + '
	FROM tblDocuments d
		INNER JOIN tblClaimantDocumentType cdt			ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID' + 
		@JoinString + '
		INNER JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l							ON cl.LawsuitID = l.lawsuitid
		INNER JOIN tblClaimLawsuitStatusTracking clst	ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT JOIN tblDefenseCounselAddress dca			ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	WHERE d.DocumentID = @DocId 
		AND cl.IsPrimaryForClaim = 1
		AND c.IsCurrentClaim = 1 '

--print @QueryString
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@DocID int', @DocId = @DocId





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID_2] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID_2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID_2] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID_2] TO [power_user]
GO
