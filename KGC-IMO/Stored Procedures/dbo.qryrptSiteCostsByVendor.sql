SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[qryrptSiteCostsByVendor] (@vendorid int, @SiteID int)
As 
set nocount on

 --DECLARE @VendorID INT
 --SET @VendorID = 0
 --DECLARE @Siteid INT
 --SET @Siteid = 0
 
DECLARE @MAXVENDORID INT
SELECT @MAXVENDORID = max(vendorid) from tblvendor
DECLARE @MaxSiteID INT
SELECT @MaxSiteID = max(siteid) from tblEnviroSitesDetail


SELECT sd.sitename, Sum(SC.ReimbursableAmount) as SumOfAmount, CE.CostExclusionType,
v.vendorname, ExclusionNotes 
FROM tblSiteCosts SC
inner join tblEnviroSitesDetail sd on sc.siteid=sd.siteid
left join tblCostExclusion CE on SC.CostExclusionTypeID=CE.CostExcludeTypeID
left join tblVendor v on sc.vendorid=v.vendorid
WHERE v.vendorid BETWEEN COALESCE(@vendorid, 0) AND COALESCE(NullIF(@vendorid,0), @MAXVENDORID)AND 
sd.siteid BETWEEN COALESCE(@SiteID, 0) AND COALESCE(NullIF(@SiteID,0), @MaxSiteID)

group by sd.sitename, ce.CostExclusionType, v.vendorname, ExclusionNotes

return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSiteCostsByVendor] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSiteCostsByVendor] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSiteCostsByVendor] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSiteCostsByVendor] TO [power_user]
GO
