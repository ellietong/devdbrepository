SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[WSS_INVOICE_qryInvoiceStatusForApproval]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT statusid, status
FROM tblDefenseInvoiceStatus
WHERE StatusID in (4,6)
UNION
SELECT NULL, ''
ORDER BY Status
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryInvoiceStatusForApproval] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryInvoiceStatusForApproval] TO [base_user]
GO
