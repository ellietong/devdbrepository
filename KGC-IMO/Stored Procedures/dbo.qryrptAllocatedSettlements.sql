SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptAllocatedSettlements] (@AllocationID int)

As
SET NOCOUNT ON


--DECLARE @AllocationID int
--SET @AllocationID=56

DECLARE @AllocationDetails nvarchar(200)
SET @AllocationDetails = (Select Convert(nvarchar(50),[Month]) + ' ' + Convert(nvarchar(50),[Year]) + ' - ' + Convert(nvarchar(50),BillNumber)
FROM tblallocation
Where AllocationID=@AllocationID)


Select 	CPI.ClaimantPersonalInfoID, LastName, FirstName, Firstname+' '+LastName as ClaimantName, sg.settlementgroup,coalesce('XXX-XX-'+ Right([SocialSecurityNumber],4),cpi.internationalnumber) as SocialSecurityNumber, State,
	Substring(HighLevelDisease,1,1) As HighLevelDisease, DocketNumber, FileDate, SourceDate, SettlementDate, MIN(EmpStartDate)as EarliestExposureDate,
	DiseaseDiagnosisDate as DiagnosisDate, SettlementAmount,
	 'B' as Adjustment,
	 @AllocationDetails as AllocationDetails
	
FROM tblClaimantPersonalInfo CPI
LEFT JOIN tblClaimantEmployment CE on CPI.ClaimantPersonalInfoID=CE.ClaimantPersonalInfoID
LEFT JOIN tblClaim C on CPI.ClaimantPersonalInfoID=C.ClaimantPersonalInfoID
LEFT JOIN (select * from tblClaimDisease where PrimaryDisease = 1) cd on cd.ClaimID = c.ClaimID
LEFT JOIN tblDisease D on cd.DiseaseID=D.DiseaseID
LEFT JOIN tblHighLevelDisease HLD on D.HighLevelDiseaseID=HLD.HighLevelDiseaseID
LEFT JOIN tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
INNER JOIN tblClaimantSettlement CS on CL.ClaimLawsuitID=CS.CLaimLawsuitID
left join tblsettlementgroup sg on cs.settlementgroupid = sg.settlementgroupid
LEFT JOIN tblLawsuit L on CL.LawsuitID=L.LawsuitID
LEFT JOIN tblState S on L.StateID=S.StateID
LEFT JOIN tblCourtType ct on ct.CourtTypeID = L.CourtTypeID
LEFT JOIN tblJurisdiction j on j.JurisdictionID = L.JurisdictionID
LEFT JOIN tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID =CL.ClaimLawsuitID
LEFT JOIN tblPaymentSettlement ps on ps.ClaimantSettlementID = CS.ClaimantSettlementID
LEFT JOIN tblCheck chec on chec.CheckID = ps.CheckID
inner join tblOccurrence o on o.claimantpersonalinfoid = cpi.ClaimantPersonalInfoID
inner join tblAllocationOccurrenceGroupInclude aog on o.OccurrenceGroupID = aog.OccurrenceGroupID


WHERE
SettlementAmount <> 0 --and c.MatterID=4
and aog.AllocationID=@AllocationID and c.IsCurrentClaim = 1 and cl.IsPrimaryForClaim = 1 and PrimaryDisease=1

GROUP BY Cpi.ClaimantPersonalInfoID, LastName, FirstName, Firstname+' '+LastName, sg.settlementgroup,coalesce('XXX-XX-'+ Right([SocialSecurityNumber],4),cpi.internationalnumber), State, HighLevelDisease,
	DocketNumber, FileDate, SourceDate, SettlementDate,
	DiseaseDiagnosisDate, 
	SettlementAmount
ORDER BY settlementdate, cpi.claimantpersonalinfoid


--RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocatedSettlements] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocatedSettlements] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocatedSettlements] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocatedSettlements] TO [power_user]
GO
