SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmGender]
as

Select GenderID, Gender
From tblGender
Order By Gender

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmGender] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmGender] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmGender] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmGender] TO [power_user]
GO
