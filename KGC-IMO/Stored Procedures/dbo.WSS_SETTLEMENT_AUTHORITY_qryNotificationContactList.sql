SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryNotificationContactList]
AS
BEGIN
	
		SELECT NULL AS SettlementNotificationContactID, NULL as Contact
		UNION
		SELECT	SettlementNotificationContactID, 
				LName + COALESCE(', ' + FName, '') + ' - ' + Email + ' (' + dc.DefenseCounsel + ')' AS Contact
		FROM tblSettlementNotificationContact snc
		INNER JOIN vtblPerson p ON snc.PersonID = p.PersonID
		INNER JOIN tblDefenseCounsel dc ON p.FirmID = dc.DefenseCounselID
		ORDER BY Contact
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryNotificationContactList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryNotificationContactList] TO [base_user]
GO
