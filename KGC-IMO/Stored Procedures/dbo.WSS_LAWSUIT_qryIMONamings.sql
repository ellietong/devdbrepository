SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryIMONamings]
	-- Add the parameters for the stored procedure here
@LawsuitID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DefendantCompany, a.DefendantCompanyID 
	FROM tblAllegation a
		INNER JOIN tblClaimLawsuit cl on a.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblDefendantCompany dc on a.DefendantCompanyID = dc.DefendantCompanyID
	WHERE LawsuitID = @LawsuitID --AND DefendantCompanyParentID = 1
	GROUP BY DefendantCompany, a.DefendantCompanyID 
	ORDER BY DefendantCompany
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryIMONamings] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryIMONamings] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryIMONamings] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryIMONamings] TO [power_user]
GO
