SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_REPORTS_qryOutstandingSettlementGroups]  (@MatterID int, @SettlementDate datetime)

As

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 1
--DECLARE @SettlementDate datetime
--SET @SettlementDate = '1/1/2009'

DECLARE @MaxMatterID int
SELECT @MaxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @SettlementGroupPaidUnpaid TABLE (SettlementGroupID int, Paid money, Total money)
INSERT INTO @SettlementGroupPaidUnpaid

SELECT SettlementGroupID, SUM( CASE WHEN IsFullyPaid <> 1 OR SourceNumber IS NULL THEN COALESCE(PaymentAmount, 0) ELSE COALESCE(SettlementAmount, 0) END) As Paid,
	SUM(SettlementAmount) as Total	
FROM tblClaimantSettlement cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	left join tblCheck chk ON ps.CheckID = chk.CheckID
	WHERE COALESCE(c.IsCurrentClaim,0) = 1 AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	GROUP BY SettlementGroupID, COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupCount TABLE (SettlementGroupID int, ClaimCount nvarchar(50))
INSERT INTO @SettlementGroupCount	

	
SELECT SettlementGroupID, COUNT(ClaimantSettlementID) as ClaimCount
	FROM tblClaimantSettlement 	cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	
where  COALESCE(c.IsCurrentClaim,0) = 1 AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	GROUP BY SettlementGroupID,  COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupPaidCount TABLE (SettlementGroupID int, PaidCount nvarchar(50))
INSERT INTO @SettlementGroupPaidCount	

SELECT SettlementGroupID, COUNT(ps.CheckID) as PaidCount
	FROM tblClaimantSettlement cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	WHERE SettlementGroupID IN	
(SELECT SettlementGroupID
	
FROM tblClaimantSettlement cs
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID 
	
WHERE COALESCE(IsFullyPaid, 0) = 1 
	
GROUP BY SettlementGroupID)
	and COALESCE(c.IsCurrentClaim,0) = 1 AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	GROUP BY SettlementGroupID, COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID

DECLARE @SettlementGroupMaxDate TABLE (SettlementGroupID int, MaxSettlementDate datetime)
INSERT INTO @SettlementGroupMaxDate
	
SELECT SettlementGroupID, MAX(SettlementDate) as MaxSettlementDate
	
FROM tblClaimantSettlement
	GROUP BY SettlementGroupID 
	ORDER BY MaxSettlementDate

SELECT PlaintiffCounsel, cs.SettlementDate, s.SettlementGroupID, s.SettlementGroup, COALESCE(sgc.ClaimCount, 0) AS ClaimCount, COALESCE(sgpc.PaidCount, 0) PaidCount, COALESCE(sgp.Paid, 0) AS Paid, COALESCE(sgp.Total, 0) AS Total
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
	INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INner JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pl ON l.PlaintiffCounselAddressID = pl.PlaintiffCounselAddressID
	INNER JOIN tblClaimLawsuitStatusTracking cls		ON cl.ClaimLawsuitID = cls.ClaimLawsuitID	
	LEFT join tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblClaimantDocketNumbers cd		ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
LEFT  JOIN @SettlementGroupPaidUnpaid sgp	ON s.SettlementGroupID = sgp.SettlementGroupID
	LEFT  JOIN @SettlementGroupCount sgc	ON s.SettlementGroupID= sgc.SettlementGroupID
	LEFT  JOIN @SettlementGroupPaidCount sgpc	oN s.SettlementGroupID = sgpc.SettlementGroupID
	LEFT  JOIN @SettlementGroupMaxDate sgm		ON s.SettlementGroupID = sgm.SettlementGroupID
	LEFT JOIN  tblReleasetype crr	ON crr.ReleaseTypeID =spd.ReleaseTypeID
	LEFT JOIN tblClaimSequence csq	ON c.ClaimSequenceID = csq.ClaimSequenceID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	left join tblCheck k on k.CheckID =  ps.CheckID
WHERE MaxSettlementDate >= @SettlementDate
	AND COALESCE(IsCurrentClaim,0) = 1 and coalesce(cls.isprimarystatus,0) = 1
	AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	AND COALESCE(PaidCount,0) <> COALESCE(ClaimCount,0)
--AND COALESCE(cpi.jobsiteinfo, 0) = 0  

GROUP BY PlaintiffCounsel, cs.SettlementDate, s.SettlementGroupID, s.SettlementGroup, sgc.ClaimCount, sgpc.PaidCount, sgp.Paid, sgp.Total
ORDER BY PlaintiffCounsel, SettlementDate--, SettlementGroup




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementGroups] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementGroups] TO [base_user]
GO
