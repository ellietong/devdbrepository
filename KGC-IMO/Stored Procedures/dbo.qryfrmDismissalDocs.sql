SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmDismissalDocs]


As
Set NoCount On

SELECT 
	d.DocumentID, 
	Comments, 
	ClaimantDocumentTypeID, 
	DocumentURL, 
	DocumentTitle,
	d.QCDate,
	d.QCBy, 
	d.CreatedBy, 
	AmazonKey

FROM tblDocuments d
	inner join tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
	inner join tblClaim c on c.ClaimantPersonalInfoID=mcd.ClaimantPersonalInfoID
	inner join tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
	inner join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID

WHERE   (clst.ClaimLawsuitStatusID is not null or clst.ProcessedDate is not null) 
		AND (d.QCDate IS NULL) AND (d.ClaimantDocumentTypeID = 16) and CreatedBy not like '%Historical%' 
		AND DateCreated > '6/11/2012' --When new Dismissal Upload was rolled out to Dana, IUNA, MSA
		
Group BY
d.DocumentID, comments, ClaimantDocumentTypeID, DocumentURL, DocumentTitle,
 d.QCDate, d.QCBy,  d.CreatedBy, AmazonKey
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalDocs] TO [power_user]
GO
