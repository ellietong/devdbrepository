SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryfrmSiteNameUnion]
AS
SET CONCAT_NULL_YIELDS_NULL OFF 


SELECT  0, '<<All Sites>>' as SiteName
FROM  tblenvirositesdetail

UNION 

SELECT SiteID, SiteName +coalesce(' ('+coalesce(b.state+', ', '') + coalesce(c.country, '')+')', '')
FROM  tblenvirositesdetail A
LEFT JOIN tblstate B on a.stateid = b.stateid
LEFT JOIN tblCountry C on c.countryId = a.countryid
ORDER BY sitename



RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteNameUnion] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteNameUnion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteNameUnion] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteNameUnion] TO [power_user]
GO
