SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO




CREATE  Procedure [dbo].[qryrptPolicyReportsCollapsed_Provisions]
(@insuranceProgramID int = NULL, @NamedInsuredID INT = Null, @limitTypeID int = NULL, @carrierID int = NULL, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL,
@lowLayer int = NULL, @highLayer int = NULL, @lowAttach nvarchar(50) = NULL, @highAttach nvarchar(50) = NULL, @provisionLabelID int = NULL, @provisionValueID int = NULL,
@startDate int = NULL, @endDate int = NULL)
As

SET NOCOUNT ON

--DECLARE @insuranceProgramID int 
--DECLARE @limitTypeID int
--DECLARE @carrierID int 
--DECLARE @carrierGroupID int 
--DECLARE @carrierSolvency varchar(50) 
--DECLARE @namedInsuredID int 
--DECLARE @lowLayer int 
--DECLARE @highLayer int 
--DECLARE @lowAttach nvarchar(50) 
--DECLARE @highAttach nvarchar(50) 
--DECLARE @provisionLabelID int 
--DECLARE @provisionValueID int 

--SET @insuranceProgramID = 0
--SET @limitTypeID = 0
--SET @carrierID = 0
--SET @carrierGroupID = 0
--SET @carrierSolvency = null
--SET @namedInsuredID = 0
--SET @lowLayer = 0
--SET @highLayer = 0
--SET @lowAttach = null
--SET @highAttach = null
--SET @provisionLabelID = 0
--SET @provisionValueID = 0

-- Variables holding maximum 'where' clause values
DECLARE @maxInsuranceProgram int
DECLARE @maxNamedInsuredID int
DECLARE @maxLimitTypeID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)
DECLARE @maxLayer int
DECLARE @maxAttach money
DECLARE @minProvisionValueID int
DECLARE @maxProvisionValueID int
DECLARE @maxProvisionLabelID int
DECLARE @maxEndDate int
-- Variables holding SQL string and provision-related variables
SELECT @maxProvisionLabelID = MAX(provisionlabelID) FROM tblProvision
SELECT @maxProvisionValueID = MAX(provisionvalueID) FROM tblProvision
SELECT @minProvisionValueID = MIN(provisionvalueID) FROM tblProvision
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxNamedInsuredID = MAX(NamedInsuredID) FROM vPolicy
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxLayer = MAX(Layer) FROM vPolicy
SELECT @maxAttach = MAX(AttachmentPoint) FROM vPolicy
SELECT @maxEndDate = MAX(annualperiod) FROM vPolicy
IF @carrierGroupID = 0 SET @carrierGroupID = NULL

SELECT pinfo.PolicyID, pinfo.PolicyNum, pinfo.PolicyNotes, pr.ProvisionID, pinfo.ActualPStartDate as pstartdate, pinfo.ActualPEndDate as penddate, pinfo.AnnualPeriod, pr.ProvisionValue, pr.Provision As ProvisionLabel, pinfo.CarrierID, pinfo.CarrierName, pinfo.CarrierSolvency, 
	pinfo.CarrierGroupID, pinfo.CarrierGroupName, pinfo.NamedInsured, pinfo.InsuranceProgram, COUNT(pinfo.PolicyID) AS NumOccurrences, MIN(ActualPStartDate) AS MinOfPStartDate, MAX(ActualPEndDate) AS MaxOfPEndDate, 
	MIN(AttachmentPoint) AS AttachmentPoint, MIN(AnnualPeriod) AS MinOfAnnualPeriod, SUM(pinfo.SIR) AS SIR, SUM(pinfo.PerOccLimit) AS PerOccLimit, SUM(pinfo.LayerPerOccLimit) AS LayerPerOccLimit, 
	SUM(pinfo.AggregateLimit) AS AggregateLimit, MIN(pinfo.Layer) AS Layer, SUM(pinfo.PerOccLimit) AS TotalLimitsPerPolicy
FROM vPolicy pinfo INNER JOIN vProvisions pr ON pinfo.PolicyID = pr.PolicyID
WHERE pinfo.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgram) AND
	pinfo.CarrierID >= COALESCE(@CarrierID, 0) AND pinfo.CarrierID <= COALESCE(NULLIF(@CarrierID, 0), @maxCarrierID) AND 
	pinfo.CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND
	(coalesce(pinfo.CarrierGroupID, 0) >= COALESCE(@CarrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID, 0) <= COALESCE(@CarrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID, 0) = @carrierGroupID) AND
	pinfo.NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND pinfo.NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsuredID)AND
	pinfo.LimitTypeID >= COALESCE(@limitTypeID, 0) AND pinfo.LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	pinfo.Layer >= COALESCE(@lowLayer, 0) AND pinfo.Layer <= COALESCE(NULLIF(@highLayer, 0), @maxLayer) AND 
	pinfo.AttachmentPoint BETWEEN COALESCE(CONVERT(int, @lowAttach), 0) AND COALESCE(NULLIF(CONVERT(int, @highAttach), 0), @maxAttach)
	and DoNotReport <>1
	AND
	pr.ProvisionValueID >= COALESCE(NULLIF(@provisionValueID, 0), @minProvisionValueID) AND pr.ProvisionValueID <= COALESCE(NULLIF(@provisionValueID, 0), @maxProvisionValueID) AND
	pr.ProvisionLabelID >= COALESCE(@provisionLabelID, 0) AND pr.ProvisionLabelID <= COALESCE(NULLIF(@provisionLabelID, 0), @maxProvisionLabelID) 
	AND AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
GROUP BY pinfo.PolicyID, pinfo.PolicyNum, pinfo.PolicyNotes, pr.ProvisionID, pinfo.ActualPStartDate, pinfo.ActualPEndDate, pinfo.AnnualPeriod, pr.ProvisionValue, pr.Provision, pinfo.CarrierID, pinfo.CarrierName, pinfo.CarrierSolvency, 
	pinfo.CarrierGroupID, pinfo.CarrierGroupName, pinfo.NamedInsured, pinfo.InsuranceProgram
ORDER BY pinfo.InsuranceProgram

RETURN




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsCollapsed_Provisions] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsCollapsed_Provisions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsCollapsed_Provisions] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsCollapsed_Provisions] TO [power_user]
GO
