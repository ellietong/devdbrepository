SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverFirmList]
AS
BEGIN
	
		SELECT NULL AS DefenseCounselID, NULL as DefenseCounsel
		UNION
		SELECT DISTINCT dc.DefenseCounselID, dc.DefenseCounsel
		FROM tblSettlementApproverContact sac
		INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
		INNER JOIN tblDefenseCounsel dc ON p.FirmID = dc.DefenseCounselID
		WHERE IsDefault = 1
		ORDER BY DefenseCOunsel
END	
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverFirmList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverFirmList] TO [base_user]
GO
