SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryALLColumnApplied] (@PolicyDateID int, @AllocationID int)
As
Set NoCount on

Declare @PStartDate datetime, @PEndDate datetime

Set @PStartDate = (Select PStartDate From tblPolicyDates where PolicyDateID = @PolicyDateID)
Set @PEndDate = (Select PEndDate From tblPolicyDates where PolicyDateID = @PolicyDateID)


Update tblAllocationResultsColumn
Set ColumnApplied = 1
Where ColumnStartDate >= @PStartDate AND ColumnEndDate <= @PEndDate And AllocationID = @AllocationID


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLColumnApplied] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLColumnApplied] TO [base_user]
GO
