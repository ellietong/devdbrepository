SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo]
	-- Add the parameters for the stored procedure here
(@ClaimantPersonalInfoID int, @ClaimLawsuitID int, @SettlementAmount money, @SettlementDate date,
@NotificationDate date, @EmailAddress nvarchar(250), @PlaintiffCounselAddressID int,
@ClaimLawsuitStatusID int, @SettlementGroupID int, @ReleaseTypeID int, @DefenseCounselAddressID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @LawsuitID int
SET @LawsuitID = (SELECT LawsuitID FROM tblClaimLawsuit WHERE ClaimLawsuitID = @ClaimLawsuitID)

--Update plaintiff counsel to the selected one for the lawsuit
UPDATE tblLawsuit
SET PlaintiffCounselAddressID = @PlaintiffCounselAddressID
WHERE LawsuitID = @LawsuitID

IF @ClaimLawsuitID IN (Select ClaimLawsuitID FROM tblClaimantSettlement)
BEGIN
	UPDATE tblClaimantSettlement
	SET SettlementAmount = @SettlementAmount,
		SettlementDate = @SettlementDate,
		SettlementNotificationDate = @NotificationDate,
		SettlementGroupID = @SettlementGroupID,
		PayToFirmAddressID = @DefenseCounselAddressID,
		SPUsername = @EmailAddress
	WHERE ClaimLawsuitID = @ClaimLawsuitID
END
ELSE
BEGIN
	INSERT INTO tblClaimantSettlement (ClaimLawsuitID, SettlementAmount, SettlementDate, SettlementGroupID, PayToFirmAddressID, SPUsername)
	VALUES (@ClaimLawsuitID, @SettlementAmount, @SettlementDate, @SettlementGroupID, @DefenseCounselAddressID, @EmailAddress)
END

UPDATE tblLawsuit	
SET 
	PlaintiffCounselAddressID = @PlaintiffCounselAddressID,
	SPUsername = @EmailAddress
WHERE LawsuitID = @LawsuitID	
	--If status is "continued" just insert the Continued status as non-primary status
	IF @ClaimLawsuitStatusID = 41 --continued
	BEGIN
		INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, ProcessedDate, IsPrimaryStatus)
		VALUES(@ClaimLawsuitID, @ClaimLawsuitStatusID, GETDATE(), 0)
	END
	ELSE IF(@ClaimLawsuitStatusID IS NOT NULL) --insert a new primary status
	BEGIN 
		UPDATE tblClaimLawsuitStatusTracking
		SET IsPrimaryStatus = 0
		WHERE ClaimLawsuitID = @ClaimLawsuitID

		INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, ProcessedDate, IsPrimaryStatus)
		VALUES(@ClaimLawsuitID, @ClaimLawsuitStatusID, @SettlementDate, GETDATE(), 1)
	END
--Insert release type into tblSettlePaymentDocumentation
--Check if SettlePaymentDocumentation exists 	
DECLARE @SetPymtID int = (SELECT SetPymtID FROM tblSettlePaymentDocumentation WHERE ClaimLawsuitID = @ClaimLawsuitID)
IF(@SetPymtID IS NOT NULL)
BEGIN
	UPDATE tblSettlePaymentDocumentation
		SET ReleaseTypeID = @ReleaseTypeID
	WHERE SetPymtID = @SetPymtID

END
ELSE --Create it
BEGIN 
	INSERT INTO tblSettlePaymentDocumentation (ClaimLawsuitID, ReleaseTypeID)
	SELECT @ClaimLawsuitID, @ReleaseTypeID

END






END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [base_user]
GO
