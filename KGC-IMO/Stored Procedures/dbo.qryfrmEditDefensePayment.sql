SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[qryfrmEditDefensePayment] (@CheckID int)
AS
SET NOCOUNT ON

select * from tblPaymentDefense 
where CheckID = @CheckID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditDefensePayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmEditDefensePayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditDefensePayment] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditDefensePayment] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditDefensePayment] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditDefensePayment] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditDefensePayment] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditDefensePayment] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditDefensePayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmEditDefensePayment] TO [power_user]
GO
