SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIM_qryIsClaimPaid]
    (
      @ClaimantPersonalInfoID INT, @ClaimID INT
    )
AS 
    SET NOCOUNT ON

SELECT COUNT(DISTINCT SetPymtID)
FROM tblSettlePaymentDocumentation spd
	LEFT JOIN tblClaimLawsuit cl ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND cl.ClaimID = @ClaimID
	AND COALESCE(MedicareReported, 0) = 1
	AND ClaimantPersonalInfoID NOT IN(SELECT ClaimantPersonalInfoID
									  FROM dbo.tblClaim c
										INNER JOIN dbo.tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
										INNER JOIN dbo.tblClaimantSettlement cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
									  WHERE SettlementGroupID = 70970)
GROUP BY ClaimantPersonalInfoID	
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryIsClaimPaid] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryIsClaimPaid] TO [base_user]
GO
