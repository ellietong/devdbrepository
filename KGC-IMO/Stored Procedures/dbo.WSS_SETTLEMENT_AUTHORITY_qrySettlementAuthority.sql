SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthority]
(
	@ClaimantName nvarchar(250) = NULL,
	@SettlementGroup nvarchar(250) = NULL,
	@PlaintiffCounselAddressID int = NULL,
	@RequestStartDate date = NULL,
	@RequestEndDate date = NULL,
	@Emailaddress nvarchar(250) = NULL, 
	@OrderByString nvarchar(MAX)=NULL,
	@AlreadyGrantedFirmID int = NULL,
	@AwaitingApprovalFirmID int = NULL)
AS
BEGIN
	SET NOCOUNT ON;
	
	--test Vars
	--DECLARE @ClaimantName nvarchar(250) = NULL
	--DECLARE @SettlementGroup nvarchar(250) = NULL
	--DECLARE @PlaintiffCounselAddressID int = NULL
	--DECLARE @RequestStartDate date = NULL
	--DECLARE @RequestEndDate date = NULL
	--DECLARE @Emailaddress nvarchar(250) = 'crookc@kcicllc.com'
	--DECLARE @OrderByString nvarchar(MAX)=NULL
	--DECLARE @AlreadyGrantedFirmID int = 269 -- 269 colfax

	
	
	--Get FirmAddressID to see if user is insurer/NCC
	DECLARE @FirmAddressID bigint = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @Emailaddress)
	DECLARE @WhereString nvarchar(MAX)
	DECLARE @CanRespondString nvarchar(MAX)
	DECLARE @ClientFirmAddressID bigint = 363 --Colfax Corp
	DECLARE @NCCFirmAddressID bigint = 361 --Leader & Berkon
	
	--Either accept the ORDER BY string parameter, or coalesce to most recent request date @ top
	SET @OrderByString = COALESCE(@OrderByString, 'ORDER BY AuthorityRequestedDate DESC')
	
	--******Filter appropriately for each user type ******--
	
	IF((@FirmAddressID IS NULL AND @Emailaddress IS NULL) OR @Emailaddress LIKE '%@kcicllc.com')
	BEGIN
		SET @WhereString = ''
		SET @CanRespondString = '1'
	END
	--NCC
	ELSE IF (@FirmAddressID = @NCCFirmAddressID)
	BEGIN
		--ONLY SHOW DEMAND AMOUNT < 100k without client approval,
		-- OR >100k WITH CLIENT APPROVAL
		--CURRENTLY USING KCIC EMAIL ADDRESS FOR TESTING -- should be client
		SET @WhereString = 'WHERE (sa.AuthorityGranted <> 1 OR AuthorityRequestedDate > (GETDATE()-30))'
		SET @CanRespondString = '1'
		--SET @WhereString = 'WHERE EXISTS (SELECT SettlementAuthorityResponseID 
		--					FROM tblSettlementAuthorityResponse sar
		--					INNER JOIN vtblPerson p on sar.CreatedBy = p.Email
		--					WHERE SettlementAuthorityStatusID = 1
		--					AND p.FirmAddressID = ' + CONVERT(nvarchar, @NCCFirmAddressID) + ')
		--		 '
		--SET @CanRespondString = '0' --NCC is initiating these requests,
		--							-- so they shouldnt be able to respond to their own
    END
    --Client
    ELSE IF (@FirmAddressID = @ClientFirmAddressID)
    BEGIN
		SET @WhereString = 'WHERE EXISTS ( SELECT saac.SettlementApproverContactID
										FROM tblSettlementAuthorityApproverContact saac
										INNER JOIN tblSettlementApproverContact sac ON saac.SettlementApproverContactID = sac.SettlementApproverContactID
										INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
										WHERE saac.SettlementAuthorityID = sa.SettlementAuthorityID
														) AND (sa.AuthorityGranted <> 1 OR AuthorityRequestedDate > (GETDATE()-30))
				'
		--SET @WhereString = 'WHERE (AuthorityAmount >= 100000
		--		AND NOT EXISTS (SELECT SettlementAuthorityResponseID 
		--					FROM tblSettlementAuthorityResponse sar
		--					INNER JOIN vtblPerson p on sar.CreatedBy = p.Email
		--					WHERE SettlementAuthorityStatusID = 1
		--					AND p.FirmAddressID = ' + CONVERT(nvarchar, @ClientFirmAddressID) + ')
		--		)  AND (sa.AuthorityGranted <> 1 OR AuthorityRequestedDate < (GETDATE()-30)) '
		SET @CanRespondString = 'CASE WHEN Status = ''Authority Granted'' THEN 0 
									  WHEN EXISTS (SELECT SettlementAuthorityResponseID 
													FROM tblSettlementAuthorityResponse sr 
													WHERE sr.SettlementAuthorityID = sa.SettlementAuthorityID
													AND ParentSettlementAuthorityResponseID IS NULL
													AND CreatedBy = ''' + @Emailaddress + ''') OR 
										NOT	EXISTS (SELECT SettlementAuthorityApproverContactID
													FROM tblSettlementAuthorityApproverContact saac
													INNER JOIN tblSettlementApproverContact sac ON saac.SettlementApproverContactID = sac.SettlementApproverContactID
													INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
													WHERE SettlementAuthorityID = sa.SettlementAuthorityID
													AND p.Email	= ''' + @emailaddress + ''')
									  THEN 0 ELSE 1 END'
    END
    --Insurers
    ELSE
    BEGIN
    SET @WhereString = 'WHERE EXISTS ( SELECT saac.SettlementApproverContactID
										FROM tblSettlementAuthorityApproverContact saac
										INNER JOIN tblSettlementApproverContact sac ON saac.SettlementApproverContactID = sac.SettlementApproverContactID
										INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
										WHERE saac.SettlementAuthorityID = sa.SettlementAuthorityID
										AND p.FirmAddressID = ' + CONVERT(nvarchar, @FirmAddressID) + '
				) AND (sa.AuthorityGranted <> 1 OR AuthorityRequestedDate > (GETDATE()-30))
				'
		SET @CanRespondString = 'CASE WHEN Status = ''Authority Granted'' THEN 0 
							  WHEN EXISTS (SELECT SettlementAuthorityResponseID 
											FROM tblSettlementAuthorityResponse sr 
											WHERE sr.SettlementAuthorityID = sa.SettlementAuthorityID
											AND ParentSettlementAuthorityResponseID IS NULL
											AND CreatedBy = ''' + @Emailaddress + ''') OR 
								NOT	EXISTS (SELECT SettlementAuthorityApproverContactID
											FROM tblSettlementAuthorityApproverContact saac
											INNER JOIN tblSettlementApproverContact sac ON saac.SettlementApproverContactID = sac.SettlementApproverContactID
											INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
											WHERE SettlementAuthorityID = sa.SettlementAuthorityID
											AND p.Email	= ''' + @emailaddress + ''')
							  THEN 0 ELSE 1 END'
		--SET @CanRespondString = 'CASE WHEN Status = ''Authority Granted'' THEN 0 
		--							  WHEN EXISTS (SELECT SettlementAuthorityResponseID 
		--											FROM tblSettlementAuthorityResponse sr 
		--											WHERE sr.SettlementAuthorityID = sa.SettlementAuthorityID)
		--							  THEN 0 ELSE 1 END'
    END

	--********** Filter based on filter/search parameters *********--
	
	--CLAIMANT NAME
	IF(@ClaimantName IS NOT NULL AND @ClaimantName <> '')
	BEGIN
		SET @WhereString = @WhereString + ' 
			AND (cpi.LastName + '', '' + cpi.FirstName) LIKE ''%' + @ClaimantName + '%'''
	END
	
	--SETTLEMENT GROUP
	IF(@SettlementGroup IS NOT NULL AND @SettlementGroup <> '')
	BEGIN
		SET @WhereString = @WhereString + ' 
			AND SettlementGroup LIKE ''%' + @SettlementGroup + '%'''
	END
	
	--PLAINTIFFCOUNSELADDRESSID
	IF(@PlaintiffCounselAddressID IS NOT NULL)
	BEGIN
		SET @WhereString = @WhereString + ' 
			AND l.PlaintiffCounselAddressID = ' + CONVERT(nvarchar, @PlaintiffCounselAddressID)
	END
	
	--REQUEST DATE RANGE
	IF(@RequestStartDate IS NOT NULL)
	BEGIN
		SET @WhereString = @WhereString + ' 
			AND CONVERT(date, sa.AuthorityRequestedDate) >= ''' + CONVERT(nvarchar, @RequestStartDate) + ''''
	END
	IF(@RequestEndDate IS NOT NULL)
	BEGIN
		SET @WhereString = @WhereString + ' 
			AND CONVERT(date, sa.AuthorityRequestedDate) <= ''' + CONVERT(nvarchar, @RequestEndDate) + ''''
	END
	
	--FIRMS HAVING ALREADY GRANTED APPROVAL
	IF(@AlreadyGrantedFirmID IS NOT NULL)
	BEGIN
		SET @WhereString = @WhereString + ' 
			AND EXISTS (SELECT SettlementAuthorityResponseID 
						FROM tblSettlementAuthorityResponse
						WHERE SettlementAuthorityStatusID = 1
						AND SettlementAuthorityID = sa.SettlementAuthorityID
						AND (SELECT FirmID FROM vtblPerson p
						WHERE Email = CreatedBy) = ' + CONVERT(nvarchar, @AlreadyGrantedFirmID) + ')'
	END
	
	--FIRMS AWAITING APPROVAL (no approval response exists AND a requirement to do so does exist
	IF(@AwaitingApprovalFirmID IS NOT NULL)
	BEGIN
		SET @WhereString = @WhereString + ' 
			AND NOT EXISTS (SELECT SettlementAuthorityResponseID 
						FROM tblSettlementAuthorityResponse sarAwaiting
						INNER JOIN vtblPerson p ON p.Email = CreatedBy
						WHERE SettlementAuthorityStatusID = 1
						AND sarAwaiting.SettlementAuthorityID = sa.SettlementAuthorityID
						AND p.FirmID = ' + CONVERT(nvarchar, @AwaitingApprovalFirmID) + ')
						
						AND EXISTS (SELECT SettlementAuthorityApproverContactID
						FROM tblSettlementAuthorityApproverContact saac
						INNER JOIN tblSettlementApproverContact sacAwaiting ON saac.SettlementApproverContactID = sacAwaiting.SettlementApproverContactID
						INNER JOIN vtblPerson p ON sacAwaiting.PersonID = p.PersonID
						AND saac.SettlementAuthorityID = sa.SettlementAuthorityID
						AND ApprovalRequired = 1
						AND FirmID = ' + CONVERT(nvarchar, @AwaitingApprovalFirmID) + ')
						'
	END
	
	--******Assemble list of claimants with links to details page for us as "tool tip" on Settlement Authority Page***********
	
	IF object_id('tempdb..#ClaimantLists') IS NOT NULL
	BEGIN
	   DROP TABLE #ClaimantLists
	END
	CREATE TABLE #ClaimantLists (SettlementGroupID bigint, ClaimantList nvarchar(MAX))
	--Insurers (no hyperlinks)
	IF(EXISTS (SELECT RolePermissionsID 
				FROM [Ligado-ExtranetSecurity].dbo.vPerson p
					INNER JOIN [Ligado-ExtranetSecurity].dbo.tblPermissionsRole pr ON p.FirmID = pr.FirmID
				WHERE pr.RoleID = 16
					AND p.Email = @Emailaddress))
	BEGIN
		INSERT INTO #ClaimantLists
		SELECT SettlementGroupID,
			'<ul style="display: none; list-style-type: none; padding: 0px; margin: 0px;" id="sgid' + CONVERT(nvarchar, SettlementGroupID) + '">' + CONVERT(nvarchar(MAX), (	SELECT 
						'a' = cpi.LastName + ', ' + cpi.FirstName
				FROM tblSettlementGroup sginner
				INNER JOIN tblClaimantSettlement cs ON sginner.SettlementGroupID = cs.SettlementGroupID
				INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
				INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
				INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
				WHERE sginner.SettlementGroupID = sg.SettlementGroupID
				FOR XML PATH('li')
			)) + '</ul>'
		FROM tblSettlementGroup sg
	END
	ELSE --non-insurers
	BEGIN
		INSERT INTO #ClaimantLists
		SELECT SettlementGroupID,
			'<ul style="display: none; list-style-type: none; padding: 0px; margin: 0px;" id="sgid' + CONVERT(nvarchar, SettlementGroupID) + '">' + CONVERT(nvarchar(MAX), (	SELECT 
						'a/@href' = '/clients/imo/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, c.ClaimantPersonalInfoID),
						'a/@target' = '_blank',
						'a' = cpi.LastName + ', ' + cpi.FirstName
				FROM tblSettlementGroup sginner
				INNER JOIN tblClaimantSettlement cs ON sginner.SettlementGroupID = cs.SettlementGroupID
				INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
				INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
				INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
				WHERE sginner.SettlementGroupID = sg.SettlementGroupID
				FOR XML PATH('li')
			)) + '</ul>'
		FROM tblSettlementGroup sg
	END
	
		
	IF object_id('tempdb..#FirmsRequiringApproval') IS NOT NULL
	BEGIN
	   DROP TABLE #FirmsRequiringApproval
	END
	CREATE TABLE #FirmsRequiringApproval (SettlementAuthorityID int, [Status] nvarchar(MAX))
	
	INSERT INTO #FirmsRequiringApproval
	SELECT SettlementAuthorityID, 
	COALESCE(
	'Awaiting approval from: <br/>' + 
	CONVERT(nvarchar(MAX), (
	SELECT DISTINCT CASE WHEN p.Email LIKE '%resolute-midwest.com' THEN DefenseCounsel + ' - Midwest' 
						WHEN p.Email LIKE '%resolute-ne.com' THEN DefenseCounsel + ' - New England' 
						ELSE DefenseCounsel END AS DefenseCounsel
	FROm tblSettlementAuthorityApproverContact saac
	INNER JOIN tblSettlementApproverContact sac ON saac.SettlementApproverContactID = sac.SettlementApproverContactID
	INNER JOIN vtblPerson p ON sac.PersonID = p.PersonID
	INNER JOIN tblDefenseCounsel dc ON p.FirmID = dc.DefenseCounselID
	WHERE NOT EXISTS (SELECT SettlementAuthorityResponseID
						FROM tblSettlementAuthorityResponse sar
						WHERE SettlementAuthorityID = saac.SettlementAuthorityID
						AND SettlementAuthorityStatusID = 1
						AND sar.CreatedBy = p.email)
	AND saac.SettlementAuthorityID = sa.SettlementAuthorityID
	AND COALESCE(saac.ApprovalRequired, 0) = 1
		FOR XML PATH('div'))), 'Authority Granted')
	FROM tblSettlementAuthority sa

	PRINT @CanRespondString
	PRINT @WhereString
	--ASSEMBLE ACTUAL SELECT STATEMENT

	DECLARE @QueryString nvarchar(MAX) = 'SELECT DISTINCT	
				sa.AuthorityRequestedBy,
				sa.AuthorityRequestedDate,
				--sa.ClaimLawsuitID,
				sa.SettlementGroupID,
				sa.SettlementAuthorityID,
				sa.DemandAmount,
				sa.AuthorityAmount,
				REPLACE(sa.Comment, CHAR(13)+CHAR(10), ''<br/>'') AS Comment, --Replace newlines with <br/>s  
				SettlementGroup,
				--cpi.LastName + '', '' + cpi.FirstName AS ClaimantName,
				--c.ClaimantPersonalInfoID,
				' + @CanRespondString + ' AS CanRespond,
				 CASE WHEN (EXISTS (SELECT SettlementAuthorityResponseID FROM tblSettlementAuthorityResponse sar WHERE sar.SettlementAuthorityID = sa.SettlementAuthorityID)) THEN 1 ELSE 0 END AS HasResponses,
				 --d.DocumentURL,
				 d.AmazonKey,
				 ClaimantList,
				 Status,
				 ResponseDueDate AS ResponseDueDate
		FROM tblSettlementAuthority sa
		INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
		INNER JOIN tblClaimantSettlement cs ON sg.SettlementGroupID = cs.SettlementGroupID
		INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN tblDocuments d ON sa.DocumentID = d.DocumentID
		INNER JOIN #ClaimantLists claimants ON sa.SettlementGroupID = claimants.SettlementGroupID
		LEFT JOIN #FirmsRequiringApproval fra ON sa.SettlementAuthorityID = fra.SettlementAuthorityID
		' + @WhereString + 
		' ' + @OrderByString
		PRINT @QueryString
		EXEC sp_executesql @QueryString

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthority] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthority] TO [base_user]
GO
