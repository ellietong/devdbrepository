SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryALLRecalcQS_AdjustLayerPercents] (@AllocationID nvarchar(5), @OccurrenceTypeID int)

as

Set NoCount On

--Declare @AllocationID nvarchar(5)
--Declare @OccurrenceTypeID int
--Set @AllocationID = 43
--Set @OccurrenceTypeID = 1


DECLARE @strsql nvarchar(4000)


If @OccurrenceTypeID in (1) Begin -- Indemnity types

SET @strsql = ' Update ztblPolicyAllocation 
Set OccurrenceRemaining = 0
Where (OccurrenceRemaining < 0.25) and AllocationID = ' + @AllocationID + '


Declare @tblUpdatedLayerPerOcc table (PStartDate datetime, PEndDate datetime, AttachmentPoint money, InsuranceProgramID int, AllocationID int, UpdatedLayerPerOcc float)
Insert Into @tblUpdatedLayerPerOcc
Select PStartDate, 
	PEndDate, 
	AttachmentPoint, 
	InsuranceProgramID, 
	AllocationID, 
	sum(PerOccLimit) as UpdatedLayerPerOcc
From ztblPolicyAllocation
where OccurrenceRemaining > 0
Group By PStartDate, PEndDate, AttachmentPoint, AllocationID, InsuranceProgramID


Update ztblPolicyAllocation
Set LayerPerOccLimit = UpdatedLayerPerOcc
From ztblPolicyAllocation z Inner Join @tblUpdatedLayerPerOcc u on z.PStartDate = u.PStartDate 
										and z.PEndDate = u.PEndDate
										and z.AttachmentPoint = u.AttachmentPoint
										and z.InsuranceProgramID = u.InsuranceProgramID
										and z.AllocationID = u.AllocationID
Where z.AllocationID = ' + @AllocationID + '


Update ztblPolicyAllocation 
Set LayerPerOccLimit = PerOccLimit
Where (OccurrenceRemaining = 0) and AllocationID = ' + @AllocationID  + '

UPDATE ztblPolicyAllocation
SET LayerPercent = PerOccLimit/LayerPerOccLimit
Where AllocationID = ' + @AllocationID 
						
print @strSQL
EXEC sp_executesql @strSQL

End


If @OccurrenceTypeID in (2)  -- Defense
 Begin
SET @strsql = ' Update ztblPolicyAllocation 
Set OccurrenceRemaining = 0
Where (OccurrenceRemaining < 0.25) and AllocationID = ' + @AllocationID + '



Declare @tblUpdatedLayerPerOcc table (PStartDate datetime, PEndDate datetime, AttachmentPoint money, InsuranceProgramID int, 
AllocationID int, UpdatedLayerPerOcc float)
Insert Into @tblUpdatedLayerPerOcc
Select PStartDate, 
	PEndDate, 
	AttachmentPoint, 
	InsuranceProgramID, 
	AllocationID, 
	sum(PerOccLimit) as UpdatedLayerPerOcc--sum(OccurrenceRemaining) as UpdatedLayerPerOcc
From ztblPolicyAllocation
Where Provision1 <> 4 and OccurrenceRemaining > 0
Group By PStartDate, PEndDate, AttachmentPoint, AllocationID, InsuranceProgramID


Update ztblPolicyAllocation
Set LayerPerOccLimit = UpdatedLayerPerOcc
From ztblPolicyAllocation z Inner Join @tblUpdatedLayerPerOcc u on z.PStartDate = u.PStartDate 
										and z.PEndDate = u.PEndDate
										and z.AttachmentPoint = u.AttachmentPoint
										and z.InsuranceProgramID = u.InsuranceProgramID
										and z.AllocationID = u.AllocationID
Where z.AllocationID = ' + @AllocationID + '


Update ztblPolicyAllocation 
Set LayerPerOccLimit = PerOccLimit--(Case When OccurrenceRemaining = 0 then PerOccLimit else OccurrenceRemaining end)
Where (Provision1 = 4 or OccurrenceRemaining = 0) and AllocationID = ' + @AllocationID
+'
						
UPDATE ztblPolicyAllocation
SET LayerPercent = PerOccLimit/LayerPerOccLimit--OccurrenceRemaining/LayerPerOccLimit
Where AllocationID = ' + @AllocationID 


--print @strSQL
EXEC sp_executesql @strSQL

End


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLRecalcQS_AdjustLayerPercents] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLRecalcQS_AdjustLayerPercents] TO [base_user]
GO
