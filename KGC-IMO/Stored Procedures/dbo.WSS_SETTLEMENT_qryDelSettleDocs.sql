SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_SETTLEMENT_qryDelSettleDocs]
    (
      @SettleDocID INT ,
      @emailaddress NVARCHAR(250)
    )
AS --DECLARE @SettlePaymentID int
--DECLARE @EmailAddress nvarchar(100)
--SET @SettlePaymentID = 23
--set @EmailAddress	   = 'hubbardb@kcicllc.com' 

    UPDATE  tblSettleDocs
    SET     SPUsername = @Emailaddress
    WHERE   SettleDocID = @SettleDocID

    DELETE  FROM tblSettleDocs
    WHERE   SettleDocID = @SettleDocID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryDelSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryDelSettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryDelSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryDelSettleDocs] TO [power_user]
GO
