SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAllocationByClaimDetails] (@AllocationID int)
As
Set NoCount on

--Declare @AllocationID int
--Set @AllocationID = 51

Declare @LimitTypeID int
Set @LimitTypeID=(Select LimitTypeID from tblAllocation Where AllocationID=@AllocationID)

DECLARE @Defense TABLE (PolicyID int, DefenseForm nvarchar(25))
INSERT INTO @Defense
SELECT PolicyID, CASE When pv.provisionvalueID=2 then 'H' when pv.provisionvalueID=3 then 'G' when pv.provisionvalueID=4 then 'I' else 'N/A' end as DefenseForm
FROM tblProvision pv
WHERE pv.ProvisionLabelID = 1
GROUP BY PolicyID, CASE When pv.provisionvalueID=2 then 'H' when pv.provisionvalueID=3 then 'G' when pv.provisionvalueID=4 then 'I' else 'N/A' end

--DECLARE @Subscription TABLE (PolicyID int, Percentage float, Solvency nvarchar(25))
--INSERT INTO @Subscription
--SELECT PolicyID, SUM(Percentage), DanaReportingStatus
--FROM tblSubscriptionCoverage sc
--INNER JOIN tblSubscriberCompany c ON sc.SubscriberID = c.SubscriberID
--Inner Join mtblDanaSubscriberReportingStatus dsrs on dsrs.SubscriberID = sc.SubscriberID
--Inner Join mtblDanaReportingStatus drs on drs.DanaReportingStatusID = dsrs.DanaReportingStatusID
--GROUP BY PolicyID, DanaReportingStatus

------LONDON
----Select 'Signatory & Non-Signatory - 75777' as AllocationDescription, triggerstart1 as EarliestExposureDate, triggerend1 as DiagnosisDate, o.OccurrenceID, OccurrenceGroup, mSegment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
----Solvency, SUM(Percentage) AS TotalPercentage, O.Cost1 + O.Cost3 as CostAllocated, 
----	PolicyNum, DefenseForm, CarrierName, Min(PStartDate)as MinStartDate, Max(PEndDate)as MaxEndDate, Layer, 
----	SUM(AllocationIndemnity*Percentage) as AllocationIndemnity, 
----	SUM(AllocationDefense*Percentage) as AllocationDefense, 
----	0 as Adjustment, 
----	SUM(AllocationDefenseOutsideLimits*Percentage) as AllocationDefenseOutsideLimits
	
----	From tblAllocationResults AR
----	INNER JOIN tblPolicyDates pd ON AR.PolicyDateID = pd.PolicyDateID
----	Inner Join tblPolicy p on pd.PolicyID =p.PolicyID
----	inner join tblPolicyLimits pl on pl.PolicyID = p.PolicyID
----	inner join tblCarrier c on c.CarrierID = p.CarrierID
----	INNER JOIN @Subscription s ON p.PolicyID = s.PolicyID
----	Left Join @Defense d ON p.PolicyID = d.[PolicyID]

----	Inner Join tblOccurrence O on AR.OccurrenceID=O.OccurrenceID
----	Inner Join tblOccurrenceGroup OG on OG.OccurrenceGroupID=O.OccurrenceGroupID
----	Left Join tblOccurrenceType ot on o.OccurrenceTypeID = ot.OccurrenceTypeID
----Where AllocationID=@AllocationID and LimitTypeID = @LimitTypeID
----and c.CarrierID = 20 
----GROUP BY triggerstart1, triggerend1, o.OccurrenceID, OccurrenceGroup, mSegment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
----Solvency,O.Cost1 + O.Cost3, PolicyNum, DefenseForm, CarrierName, Layer

----UNION

--OTHER CARRIERS
Select Convert(nvarchar(50),[Month])+' '+Convert(nvarchar(50),[Year]) + ' - ' +Convert(nvarchar(50),BillNumber)as AllocationDescription1,  triggerstart1 as EarliestExposureDate, triggerend1 as DiagnosisDate, o.OccurrenceID, OccurrenceGroup, mSegment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
NULL As Solvency, NULL AS TotalPercentage, O.Cost1 + O.Cost3 as CostAllocated, 
	PolicyNum, DefenseForm, CarrierName, Min(PStartDate)as MinStartDate, Max(PEndDate)as MaxEndDate, Layer, 
	SUM(AllocationIndemnity) as AllocationIndemnity, 
	SUM(AllocationDefense) as AllocationDefense, 
	0 as Adjustment, 
	SUM(AllocationDefenseOutsideLimits) as AllocationDefenseOutsideLimits
	
	From tblAllocationResults AR
	INNER JOIN tblAllocation A on A.AllocationID=AR.AllocationID
	INNER JOIN tblPolicyDates pd ON AR.PolicyDateID = pd.PolicyDateID
	Inner Join tblPolicy p on pd.PolicyID =p.PolicyID
	inner join tblPolicyLimits pl on pl.PolicyID = p.PolicyID
	inner join tblCarrier c on c.CarrierID = p.CarrierID
	Left Join @Defense d ON p.PolicyID = d.[PolicyID]

	Inner Join tblOccurrence O on AR.OccurrenceID=O.OccurrenceID
	Inner Join tblOccurrenceGroup OG on OG.OccurrenceGroupID=O.OccurrenceGroupID
	Left Join tblOccurrenceType ot on o.OccurrenceTypeID = ot.OccurrenceTypeID
Where ar.AllocationID=@AllocationID and pl.LimitTypeID = @LimitTypeID
GROUP BY BillNumber, [MONTH], [YEAR], triggerstart1, triggerend1, o.OccurrenceID, OccurrenceGroup, mSegment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
	O.Cost1 + O.Cost3, PolicyNum, DefenseForm, CarrierName, Layer
	
UNION

--PRODUCER
SELECT Convert(nvarchar(50),[Month])+' '+Convert(nvarchar(50),[Year]) + ' - ' + Convert(nvarchar(50),BillNumber) as AllocationDescription1,   triggerstart1 as EarliestExposureDate, triggerend1 as DiagnosisDate, o.OccurrenceID, OccurrenceGroup, 
mSegment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype, NULL As Solvency, NULL AS TotalPercentage, O.Cost1 + O.Cost3 as CostAllocated, 
	'' as PolicyNum, 'G' as DefenseForm, 'IMO' as CarrierName,'' as MinStartDate, '' as MaxEndDate, '' as Layer, 
	0 as AllocationIndemnity, 
	0 as AllocationDefense, 
	0 as Adjustment, 
	SUM(UncollapsedPost) as AllocationDefenseOutsideLimits
	
	FROM tblAllocationResultsHorizontal ARH
	INNER JOIN tblAllocation A on A.AllocationID=ARH.AllocationID
	INNER JOIN tblOccurrence O on ARH.OccurrenceID=O.OccurrenceID
	INNER JOIN tblOccurrenceGroup OG on OG.OccurrenceGroupID=O.OccurrenceGroupID
	LEFT JOIN tblOccurrenceType ot on o.OccurrenceTypeID = ot.OccurrenceTypeID
WHERE ARH.AllocationID=@AllocationID
GROUP BY  BillNumber, [MONTH], [YEAR], triggerstart1, triggerend1, o.OccurrenceID, OccurrenceGroup, mSegment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
	O.Cost1 + O.Cost3
	
ORDER BY mSegment,ot.OccurrenceTypeID, EarliestExposureDate
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetails] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptAllocationByClaimDetails] TO [prossc]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetails] TO [prossc]
GO
