SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_RELATED_PARTY_LossOfConsortium]
AS

	SELECT

		0 AS LossOfConsortiumID, 'NO' AS LossOfConsortium
	UNION
	SELECT
		1 AS LossOfConsortiumID,'YES' AS LossOfConsortium

	FROM tblClaimantRelatedParty crp
GO
