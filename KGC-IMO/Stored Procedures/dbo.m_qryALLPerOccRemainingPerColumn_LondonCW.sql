SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[m_qryALLPerOccRemainingPerColumn_LondonCW]
(@allocationID int, @triggerstartfield nvarchar(250))
AS
SET NOCOUNT ON
-- create a table variable to hold the results of the query to find the per occ remaining per column
DECLARE @tbltemp table (ColumnID int, PercentOcc float)

SET @triggerstartfield = CONVERT(date,@triggerstartfield)

-- insert per occ remaining per column into the temporary table
INSERT INTO @tbltemp (ColumnID, PercentOcc)
-- find the per occ remaining per column
SELECT tblAllocationResultsColumn.ColumnID, 
	SUM(
	CASE WHEN (@triggerstartfield > ColumnStartDate) AND (@triggerstartfield < ColumnEndDate) THEN CONVERT(decimal, ColumnEndDate - @triggerstartfield) ELSE (
	CASE WHEN PStartDate = ColumnStartDate THEN 
		CASE WHEN PEndDate > ColumnEndDate THEN (CONVERT(decimal, ColumnEndDate - PStartDate)) 	ELSE (CONVERT(decimal, PEndDate - PStartDate)) END
	ELSE	
		CASE WHEN PEndDate < ColumnEndDate THEN (CONVERT(decimal, PEndDate - ColumnStartDate)) 	ELSE (CONVERT(decimal, ColumnEndDate - ColumnStartDate)) END
	END) END * PerOccLimit) AS PercentOcc
FROM tblAllocationResultsColumn, ztblPolicyAllocation
WHERE (ztblPolicyAllocation.PStartDate < ColumnEndDate) AND (ztblPolicyAllocation.PEndDate > ColumnStartDate) AND 
	(ztblPolicyAllocation.InsuranceProgramID = tblAllocationResultsColumn.InsuranceProgramID) AND tblAllocationResultsColumn.AllocationID = @allocationID
	AND ztblPolicyAllocation.AllocationID = @allocationID
GROUP BY tblAllocationResultsColumn.ColumnID
-- update the ColumnPerOcc field in ztblColumn with the updated per occ remaining per column
UPDATE tblAllocationResultsColumn
SET ColumnPerOcc = (SELECT PercentOcc FROM @tbltemp WHERE ColumnID = tblAllocationResultsColumn.ColumnID)
WHERE AllocationID = @allocationID
DELETE FROM @tbltemp
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[m_qryALLPerOccRemainingPerColumn_LondonCW] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qryALLPerOccRemainingPerColumn_LondonCW] TO [base_user]
GO
