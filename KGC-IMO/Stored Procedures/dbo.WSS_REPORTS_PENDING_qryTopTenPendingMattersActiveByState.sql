SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByState] (@MatterID int = NULL, 
																			 @MonthID  int, 
																			 @YearID   int)
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MatterID int
--DECLARE @MonthID int
--DECLARE @YearID int
--SET @MatterID = NULL
--SET @MonthID  = NULL
--SET @YearID   = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with ProcessedDate, 
--      and all original ProcessedDate references have been replaced with ProcessedDate.

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @Month int
DECLARE @Year int
DECLARE @PendingDate date
SET @Month = @MonthID
SET @Year = @YearID
SET @PendingDate = DATEADD(MONTH,1,CONVERT(date,CONVERT(varchar,@Month)+'/1/'+CONVERT(varchar,@Year)))

--Find all claims with a status change post pending date(2a,2b)
DECLARE @FindClaimantsWithPostPendingDateStatusChange TABLE (Claimant int, 
															 ClaimLawsuitID int)
INSERT INTO @FindClaimantsWithPostPendingDateStatusChange
	SELECT c.ClaimantPersonalInfoID AS Claimant, 
		cl.ClaimLawsuitID
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl				  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l						  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	WHERE ProcessedDate >= @PendingDate
		AND ClaimLawsuitStatusID NOT IN (22) 
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
		AND IsCurrentClaim = 1
		AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
	GROUP BY c.ClaimantPersonalInfoID, cl.ClaimLawsuitID

--For all claims with a status change post pending AND a stausdate before pending date: find the max status date before pending date (2a)
DECLARE @FindMaxProcessedDateBeforePendingDate TABLE (ClaimLawsuitID2 int, 
												   MaxDate		   datetime)
INSERT INTO @FindMaxProcessedDateBeforePendingDate
	SELECT cl.ClaimLawsuitID AS ClaimLawsuitID2,
		MAX(ProcessedDate) AS MaxDate
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl								ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimLawsuitStatusTracking clst				ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN @FindClaimantsWithPostPendingDateStatusChange ft ON ft.claimlawsuitID = cl.ClaimLawsuitID
	WHERE ProcessedDate < @PendingDate
	GROUP BY cl.ClaimLawsuitID

--For claims with a status change post pending AND a ProcessedDate before pending date: find only those claims with a max open status before pending date (2a)
DECLARE @CheckForOnlyOpenStatusBeforePendingDate TABLE (ClaimLawsuitID3 int)
INSERT INTO @CheckForOnlyOpenStatusBeforePendingDate
	SELECT cl.ClaimLawsuitID AS ClaimLawsuitID3
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl					  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l							  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst	  ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimLawsuitStatus cls			  ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		INNER JOIN @FindMaxProcessedDateBeforePendingDate fm ON fm.ClaimLawsuitID2 = clst.ClaimLawsuitID AND clst.ProcessedDate = fm.MaxDate
	WHERE clst.ClaimLawsuitStatusID = 22

--Check to see the status count of all claims with a status change post pending date (2b)
DECLARE @CheckForStatuscount TABLE (ClaimLawsuitID4 int, 
									StatusCount     int)
INSERT INTO @CheckForStatuscount
	SELECT cl.ClaimLawsuitID AS ClaimLawsuitID4, 
		COUNT(ProcessedDate) AS StatusCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl								 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimLawsuitStatusTracking clst				 ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN @FindClaimantsWithPostPendingDateStatusChange fcw ON fcw.claimlawsuitID = cl.ClaimLawsuitID
	GROUP BY cl.ClaimLawsuitID

--Find all claims that only have one status date and check to see if file/service/received date was before pending date (2b)
DECLARE @CheckIfFileDateBeforePendingDateForOnlyOneStatus TABLE (ClaimLawsuitID5 int)
INSERT INTO @CheckIfFileDateBeforePendingDateForOnlyOneStatus
	SELECT cl.ClaimLawsuitID
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl				  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l						  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimLawsuitStatus cls		  ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		INNER JOIN @CheckForStatuscount cfs			  ON cfs.claimlawsuitID4 = clst.ClaimLawsuitID
	WHERE cfs.StatusCount = 1 
		AND COALESCE(ServiceDate,FileDate,ReceivedDate) < @PendingDate
	GROUP BY cl.ClaimLawsuitID

--Union table to combine 3 main components of Pending Count
DECLARE @CombinedTable table (HighLevelDisease nvarchar(100), 
                              State            nvarchar(100), 
                              DiseaseCount     int)
INSERT INTO @CombinedTable
	--Counts All Claims that have their status date before the pending date and are CURRENTLY still open (1)
	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		FullNameState AS 'State',
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS DiseaseCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl				  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l						  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimDisease cd				  ON c.ClaimID = cd.ClaimID
		LEFT  JOIN tblDisease d						  ON d.DiseaseID = cd.DiseaseID
		LEFT  JOIN tblDisease_ALT da				  ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT  JOIN tblHighLevelDisease hld			  ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
		LEFT  JOIN tblState s						  ON s.StateID = l.StateID
	WHERE (ProcessedDate < @PendingDate OR ProcessedDate IS NULL)
		AND ClaimLawsuitStatusID = 22 
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
		AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
		AND cd.PrimaryDisease = 1
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID, FullNameState
	UNION ALL
	--Counts all Claims with a status change post pending date and an open status as of pending date (2a)
	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		FullNameState AS 'State',
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS DiseaseCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl						   ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l								   ON cl.LawsuitID = l.LawsuitID
		INNER JOIN @CheckForOnlyOpenStatusBeforePendingDate cf ON cf.ClaimLawsuitID3 = cl.ClaimLawsuitID
		INNER JOIN tblClaimDisease cd						   ON c.ClaimID = cd.ClaimID
		LEFT  JOIN tblDisease d								   ON d.DiseaseID = cd.DiseaseID
		LEFT  JOIN tblDisease_ALT da						   ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT  JOIN tblHighLevelDisease hld					   ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
		LEFT  JOIN tblState s								   ON s.StateID = l.StateID
	WHERE cd.PrimaryDisease = 1
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID, FullNameState
	UNION ALL
	--Counts all claims with a status change post pending date and only has one status and has a file/service/received date before pending date (2b)
	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		FullNameState AS 'State',
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS DiseaseCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl									  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l											  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN @CheckIfFileDateBeforePendingDateForOnlyOneStatus cifd ON cifd.ClaimLawsuitID5 = cl.ClaimLawsuitID
		INNER JOIN tblClaimDisease cd									  ON c.ClaimID = cd.ClaimID
		LEFT  JOIN tblDisease d											  ON d.DiseaseID = cd.DiseaseID
		LEFT  JOIN tblDisease_ALT da									  ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT  JOIN tblHighLevelDisease hld								  ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
		LEFT  JOIN tblState s											  ON s.StateID = l.StateID
	WHERE cd.PrimaryDisease = 1
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID, FullNameState

--Groups all results into one table
SELECT TOP 10 [State] AS 'State', 
	SUM(ct.DiseaseCount) AS Count
FROM @CombinedTable ct
GROUP BY [State]
ORDER BY SUM(ct.DiseaseCount) DESC


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByState] TO [power_user]
GO
