SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse] 
(@SettlementAuthorityResponseID bigint)
AS
SET NOCOUNT ON

BEGIN


/*
	TEST VARS
*/
DECLARE @SettlementAuthorityID bigint = (SELECT SettlementAuthorityID FROM tblSettlementAuthorityResponse WHERE SettlementAuthorityResponseID = @SettlementAuthorityResponseID)


--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)


DECLARE @SettlementGroup nvarchar(500) = (SELECT SettlementGroup
										FROM tblSettlementAuthority sa
										INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
										WHERE sa.SettlementAuthorityID = @SettlementAuthorityID)

DECLARE @ParentFirmAddressID bigint = (SELECT FirmAddressID FROM tblSettlementAuthorityResponse sar
					INNER JOIN tblSettlementAuthorityResponse sarParent ON sar.ParentSettlementAuthorityResponseID = sarParent.SettlementAuthorityResponseID
					INNER JOIN vtblPerson p ON sarParent.CreatedBy = p.Email
					WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID)

										
SET @Subject = 	'IMO Settlement Authority Response ' + CONVERT(varchar, getdate(), 107) + ': ' + COALESCE(@SettlementGroup,'') 

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'
DECLARE @Status nvarchar(100) 
--Include message text if this is going from LB to LB
IF(@ParentFirmAddressID = 361)
BEGIN

	 DECLARE @Comment nvarchar(MAX)
	 SELECT @Status = COALESCE(SettlementAuthorityStatus, 'Unknown Status'), @Comment = COALESCE(sar.Comment, 'No comment.')
							FROM tblSettlementAuthorityResponse sar 
							INNER JOIN tblSettlementAuthorityStatus sas ON sar.SettlementAuthoritystatusID = sas.SettlementAuthorityStatusID 
							WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID
	SELECT @Status, @Comment						
	--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
	set @BodyMessage = @BodyMessage + '<H4>' + (SELECT CASE WHEN p.Email LIKE '%resolute-midwest.com' THEN DefenseCounsel + ' - Midwest' 
														WHEN p.Email LIKE '%resolute-ne.com' THEN DefenseCounsel + ' - New England' 
														ELSE COALESCE(dca.DefenseCounsel, 'KCIC') END
						FROM tblSettlementAuthority sa
						LEFT JOIN tblSettlementAuthorityResponse sar  ON sa.SettlementAuthorityID = sar.SettlementAuthorityID
						LEFT JOIN vtblPerson p on sar.CreatedBy = p.Email
						LEFT JOIN tblDefenseCounselAddress dca ON p.FirmAddressID = dca.DefenseCounselAddressID
						WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID) +  ' has posted the following response with status ' + @Status +
						
						 +  ':<br/><blockquote>' + @Comment + '</blockquote>'+
	'To view additional information regarding this authority request click ' + 
	'<a href="https://www.kcicextranet.com/clients/imo/cp/Pages/SettlementAuthority.aspx?SettlementAuthorityID=' + CONVERT(nvarchar, @SettlementAuthorityID) + '">here</a>. </H4>'


END
ELSE
BEGIN
	 SET @Status = (SELECT SettlementAuthorityStatus 
							FROM tblSettlementAuthorityResponse sar 
							INNER JOIN tblSettlementAuthorityStatus sas ON sar.SettlementAuthoritystatusID = sas.SettlementAuthorityStatusID 
							WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID)

	--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
	set @BodyMessage = @BodyMessage + '<H4> Your post regarding settlement authority has received a response.' +
						
						 +  '<br/>' + 
	'To view additional information and respond to this authority request, please click ' + 
	'<a href="https://www.kcicextranet.com/clients/imo/cp/Pages/SettlementAuthority.aspx?SettlementAuthorityID=' + CONVERT(nvarchar, @SettlementAuthorityID) + '">here</a>. </H4>'
END

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

  
DECLARE @RecipientList nvarchar(500)
--Only CC barbara on emails that are not her own responses

SET @RecipientList=(SELECT COALESCE(sarParent.CreatedBy, sa.AuthorityRequestedBy) FROM tblSettlementAuthorityResponse sar
					INNER JOIN tblSettlementAuthorityResponse sarParent ON sar.ParentSettlementAuthorityResponseID = sarParent.SettlementAuthorityResponseID
					INNER JOIN tblSettlementAuthority sa ON sar.SettlementAuthorityID = sa.SettlementAuthorityID
					WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID)
DECLARE @CCList nvarchar(MAX) = CASE WHEN (SELECT CreatedBy FROM tblSettlementAuthorityResponse WHERE SettlementAuthorityResponseID = @SettlementAuthorityResponseID)<> 'bzimmerman@leaderberkon.com' AND @recipientlist NOT LIKE '%bzimmerman@leaderberkon.com%' THEN 'bzimmerman@leaderberkon.com' ELSE '' END				
--Only send if creator != recipient
--IF(@RecipientList <> (SELECT CreatedBy FROM tblSettlementAuthorityResponse sar 
--						WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID))
--BEGIN
--QC Recipient List
Set @Bodymessage = @BodyMessage + '<h4>Recipients' + @RecipientList + '</br>CC: ' + @CCList +'</h4>'
--PRINT @RecipientList
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The Profile_Name sends from [client]@kcicllc.com email address
@profile_name='Claims',
--@recipients = 'scottc@kcic.com',
@recipients = 'imo@kcic.com',--@RecipientList,
@copy_recipients = '',--@CCList--'crookc@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

--END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse] TO [base_user]
GO
