SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityStatusList]
AS
BEGIN

	SET NOCOUNT ON;
	SELECT * FROM tblSettlementAuthorityStatus
	UNION
	SELECT NULL, NULL
	ORDER BY SettlementAuthorityStatus
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityStatusList] TO [base_user]
GO
