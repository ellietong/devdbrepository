SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation]
    (
       @SetPymtID INT ,
      @SettlePaymentComments NVARCHAR(MAX) ,
      @ReleaseTypeID INT ,
      @ReleaseSignedDate DATE ,
      --@ExecutedReleaseDate DATE = NULL ,
      @MedicareReported BIT ,
      @MedicareFullyDocumented BIT,
      @MedicareReportedDate datetime
																	--@ReleaseRqstDate date)
																	
    )
AS --DECLARE @ClaimID int
--SET @ClaimID = 1

    UPDATE  tblSettlePaymentDocumentation
    SET     SettlePaymentComments = @SettlePaymentComments ,
            ReleaseTypeID = CASE WHEN @ReleaseTypeID = 0 THEN NULL
                                 ELSE @ReleaseTypeID END ,
            --ExecutedReleaseDate = NULLIF(@ExecutedReleaseDate, '') ,
		    MedicareReported = @MedicareReported ,
            MedicareFullyDocumented = @MedicareFullyDocumented,           
            MedicareFormSent = @MedicareReportedDate,
            ReleaseSignedDate = NULLIF(@ReleaseSignedDate, '')
		
    WHERE   SetPymtID = @SetPymtID


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GO
