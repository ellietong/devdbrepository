SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vADMIN_FirmAddressClientSite]
AS
SELECT  facs.FirmAddressClientSiteID, facs.FirmAddressID, facs.ClientSiteID, ClientDisplayName, SiteName,
ClientDisplayName + ' - ' + SiteName AS ClientSite
FROM FirmAddressClientSite facs
INNER JOIN FirmAddress fa oN facs.FirmAddressID = fa.FirmAddressID
INNER JOIN Firm f ON fa.FirmID = f.FirmID
INNER JOIN tblState s ON fa.FirmStateID = s.StateID
INNER JOIN ClientSite cs on facs.ClientSiteID = cs.ClientSiteID
INNER JOIN Client c ON cs.ClientID = c.ClientID
INNER JOIN Site site ON cs.SiteID = site.SiteID





GO
GRANT SELECT ON  [dbo].[vADMIN_FirmAddressClientSite] TO [Ligado_User]
GO
