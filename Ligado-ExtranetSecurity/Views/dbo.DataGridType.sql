SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[DataGridType]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.DataGridType
GO
GRANT VIEW DEFINITION ON  [dbo].[DataGridType] TO [base_user]
GRANT SELECT ON  [dbo].[DataGridType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DataGridType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DataGridType] TO [Ligado_User]
GO
