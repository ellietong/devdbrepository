SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vADMIN_FirmSearch]
AS
SELECT	f.FirmID, 
		ClientSiteID, 
		FirmDisplayName,
		(
			SELECT
			'/Config/FirmDetails/' + CONVERT(nvarchar, fa1.FirmID) + '?FirmAddressID=' + CONVERT(nvarchar, fa1.FirmAddressID) AS 'div/a/@href', 
			'_blank' AS 'div/a/@target',
			COALESCE(fa1.FirmCity, 'UNKNOWN') + ', ' + COALESCE(s.State, 'UNKNOWN') AS 'div/a'
			FROM FirmAddress fa1 
			LEFT JOIN tblState s ON fa1.FirmStateID = s.StateID
			WHERE fa1.FirmID = f.FirmID 
			
			FOR XML PATH('')
		) AS FirmAddressList
FROM Firm f
INNER JOIN FirmAddress fa ON f.FirmID = fa.FirmID
INNER JOIN FirmAddressClientSite facs ON fa.FirmAddressID = facs.FirmAddressID
GROUP BY f.FirmID, ClientSiteID, FirmDisplayName


GO
GRANT SELECT ON  [dbo].[vADMIN_FirmSearch] TO [Ligado_User]
GO
