SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[DataGridCriterion]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.DataGridCriterion
GO
GRANT VIEW DEFINITION ON  [dbo].[DataGridCriterion] TO [base_user]
GRANT SELECT ON  [dbo].[DataGridCriterion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DataGridCriterion] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DataGridCriterion] TO [Ligado_User]
GO
