SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vTEST_TableColumns]
AS
SELECT col.name AS ColumnName, v.name AS ViewName FROM sys.objects as [v] 
INNER JOIN sys.columns col ON v.object_id = col.object_id
WHERE type_desc = 'USER_TABLE'

GO
GRANT VIEW DEFINITION ON  [dbo].[vTEST_TableColumns] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vTEST_TableColumns] TO [Ligado_User]
GO
