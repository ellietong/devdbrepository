SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Suffix]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.Suffix
GO
GRANT VIEW DEFINITION ON  [dbo].[Suffix] TO [base_user]
GRANT SELECT ON  [dbo].[Suffix] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Suffix] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Suffix] TO [Ligado_User]
GO
