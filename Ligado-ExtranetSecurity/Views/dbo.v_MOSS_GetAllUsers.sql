SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[v_MOSS_GetAllUsers]
AS
SELECT     Email, FName + ' ' + LName AS DisplayName, PersonID, Password, LastLoginDate, LastPasswordChangedDate, IsLockedOut, FailedPasswordAttemptCount
FROM         dbo.vPerson
WHERE     (Status = 1)


GO
