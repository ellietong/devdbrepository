SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Role]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.[Role]
GO
GRANT VIEW DEFINITION ON  [dbo].[Role] TO [base_user]
GRANT SELECT ON  [dbo].[Role] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Role] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Role] TO [Ligado_User]
GO
