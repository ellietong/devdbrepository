SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vADMIN_FirmAddressClientSiteList]
AS
SELECT  facs.FirmAddressClientSiteID, FirmDisplayName + ' (' + COALESCE(fa.FirmCity, 'UNKNOWN') + ', ' + 
s.State + ') - ' + ClientName + ' - ' + SiteName AS FirmAddressClientSite
FROM FirmAddressClientSite facs
INNER JOIN FirmAddress fa oN facs.FirmAddressID = fa.FirmAddressID
INNER JOIN Firm f ON fa.FirmID = f.FirmID
INNER JOIN tblState s ON fa.FirmStateID = s.StateID
INNER JOIN ClientSite cs on facs.ClientSiteID = cs.ClientSiteID
INNER JOIN Client c ON cs.ClientID = c.ClientID
INNER JOIN Site site ON cs.SiteID = site.SiteID




GO
GRANT SELECT ON  [dbo].[vADMIN_FirmAddressClientSiteList] TO [Ligado_User]
GO
