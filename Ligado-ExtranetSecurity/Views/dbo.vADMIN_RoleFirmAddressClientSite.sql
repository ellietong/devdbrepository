SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vADMIN_RoleFirmAddressClientSite]
AS
SELECT  rfacs.*, r.RoleName, r.RoleType
FROM RoleFirmAddressClientSite rfacs
INNER JOIN Role r on rfacs.RoleID = r.RoleID





GO
GRANT SELECT ON  [dbo].[vADMIN_RoleFirmAddressClientSite] TO [Ligado_User]
GO
