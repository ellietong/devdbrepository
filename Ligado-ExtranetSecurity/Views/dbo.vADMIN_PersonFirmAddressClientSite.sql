SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vADMIN_PersonFirmAddressClientSite]
AS
SELECT     dbo.PersonFirmAddressClientSite.PersonFirmAddressClientSiteID, dbo.PersonFirmAddressClientSite.PersonID, 
                      dbo.PersonFirmAddressClientSite.FirmAddressClientSiteID, dbo.PersonFirmAddressClientSite.Active, dbo.PersonFirmAddressClientSite.IsPrimary, 
                      dbo.FirmAddress.FirmID, dbo.FirmAddressClientSite.ClientSiteID, 
					  f.FirmDisplayName + ' (' + firmaddress.FirmCity + ', ' + st.FullNameState + ')' AS FirmAddress,
					  c.ClientDisplayName + ' - ' + s.SiteName AS ClientSite--, PersonFirmAddressClientSite.Active
FROM         dbo.PersonFirmAddressClientSite INNER JOIN
                      dbo.FirmAddressClientSite ON dbo.PersonFirmAddressClientSite.FirmAddressClientSiteID = dbo.FirmAddressClientSite.FirmAddressClientSiteID INNER JOIN
                      dbo.FirmAddress ON dbo.FirmAddressClientSite.FirmAddressID = dbo.FirmAddress.FirmAddressID
					  INNER JOIN Firm f ON FirmAddress.FirmID = f.FirmID
					  INNER JOIN tblState st ON FirmAddress.FirmStateID = st.StateID
					  INNER JOIN dbo.ClientSite cs ON FirmAddressClientSite.ClientSiteID = cs.ClientSiteID
					  INNER JOIN dbo.Client c ON cs.ClientID = c.ClientID
					  INNER JOIN dbo.Site s ON cs.SiteID = s.SiteID
--WHERE     (dbo.PersonFirmAddressClientSite.Active = 1)




GO
GRANT SELECT ON  [dbo].[vADMIN_PersonFirmAddressClientSite] TO [Ligado_User]
GO
