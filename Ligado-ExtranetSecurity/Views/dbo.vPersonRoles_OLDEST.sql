SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vPersonRoles_OLDEST]
AS
SELECT     p1.PersonID, cs1.ClientSiteID, cs1.ClientID, cs1.SiteID, rpfacs1.RoleID, r1.RoleName, rpfacs1.CanEdit, rpfacs1.CanAdd, rpfacs1.CanDelete, 
                      'Person Level' AS RoleLevel
FROM         dbo.Person AS p1 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs1 ON p1.PersonID = pfacs1.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs1 ON pfacs1.FirmAddressClientSiteID = facs1.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs1 ON facs1.ClientSiteID = cs1.ClientSiteID INNER JOIN
                      dbo.RolePersonFirmAddressClientSite AS rpfacs1 ON pfacs1.PersonFirmAddressClientSiteID = rpfacs1.PersonFirmAddressClientSiteID INNER JOIN
                      dbo.Role AS r1 ON rpfacs1.RoleID = r1.RoleID
UNION
SELECT     p2.PersonID, cs2.ClientSiteID, cs2.ClientID, cs2.SiteID, rfacs2.RoleID, r2.RoleName, rfacs2.CanEdit, rfacs2.CanAdd, rfacs2.CanDelete, 
                      'Firm Address Level' AS RoleLevel
FROM         dbo.Person AS p2 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs2 ON p2.PersonID = pfacs2.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs2 ON pfacs2.FirmAddressClientSiteID = facs2.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs2 ON facs2.ClientSiteID = cs2.ClientSiteID INNER JOIN
                      dbo.RoleFirmAddressClientSite AS rfacs2 ON facs2.FirmAddressClientSiteID = rfacs2.FirmAddressClientSiteID INNER JOIN
                      dbo.Role AS r2 ON rfacs2.RoleID = r2.RoleID
WHERE     'P' + CONVERT(nvarchar(100), p2.PersonID) + 'R' + CONVERT(nvarchar(100), rfacs2.RoleID) NOT IN
                          (SELECT     'P' + CONVERT(nvarchar(100), p2a.PersonID) + 'R' + CONVERT(nvarchar(100), rpfacs2a.RoleID)
                            FROM          dbo.Person AS p2a INNER JOIN
                                                   dbo.PersonFirmAddressClientSite AS pfacs2a ON p2a.PersonID = pfacs2a.PersonID INNER JOIN
                                                   dbo.RolePersonFirmAddressClientSite AS rpfacs2a ON pfacs2a.PersonFirmAddressClientSiteID = rpfacs2a.PersonFirmAddressClientSiteID INNER JOIN
                                                   dbo.Role AS r2a ON rpfacs2a.RoleID = r2a.RoleID)
UNION
SELECT     p3.PersonID, cs3.ClientSiteID, cs3.ClientID, cs3.SiteID, rcs3.RoleID, r3.RoleName, rcs3.CanEdit, rcs3.CanAdd, rcs3.CanDelete, 'Client Site Level' AS RoleLevel
FROM         dbo.Person AS p3 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs3 ON p3.PersonID = pfacs3.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs3 ON pfacs3.FirmAddressClientSiteID = facs3.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs3 ON facs3.ClientSiteID = cs3.ClientSiteID INNER JOIN
                      dbo.RoleClientSite AS rcs3 ON cs3.ClientSiteID = rcs3.ClientSiteID INNER JOIN
                      dbo.Role AS r3 ON rcs3.RoleID = r3.RoleID
WHERE     'P' + CONVERT(nvarchar(100), p3.PersonID) + 'R' + CONVERT(nvarchar(100), rcs3.RoleID) NOT IN
                          (SELECT     'P' + CONVERT(nvarchar(100), p3a.PersonID) + 'R' + CONVERT(nvarchar(100), rpfacs3a.RoleID)
                            FROM          dbo.Person AS p3a INNER JOIN
                                                   dbo.PersonFirmAddressClientSite AS pfacs3a ON p3a.PersonID = pfacs3a.PersonID INNER JOIN
                                                   dbo.RolePersonFirmAddressClientSite AS rpfacs3a ON pfacs3a.PersonFirmAddressClientSiteID = rpfacs3a.PersonFirmAddressClientSiteID INNER JOIN
                                                   dbo.Role AS r3a ON rpfacs3a.RoleID = r3a.RoleID) AND 'P' + CONVERT(nvarchar(100), p3.PersonID) + 'R' + CONVERT(nvarchar(100), rcs3.RoleID) 
                      NOT IN
                          (SELECT     'P' + CONVERT(nvarchar(100), p3b.PersonID) + 'R' + CONVERT(nvarchar(100), rfacs3b.RoleID)
                            FROM          dbo.Person AS p3b INNER JOIN
                                                   dbo.PersonFirmAddressClientSite AS pfacs3b ON p3b.PersonID = pfacs3b.PersonID INNER JOIN
                                                   dbo.FirmAddressClientSite AS facs3b ON pfacs3b.FirmAddressClientSiteID = facs3b.FirmAddressClientSiteID INNER JOIN
                                                   dbo.RoleFirmAddressClientSite AS rfacs3b ON facs3b.FirmAddressClientSiteID = rfacs3b.FirmAddressClientSiteID INNER JOIN
                                                   dbo.Role AS r3b ON rfacs3b.RoleID = r3b.RoleID)
UNION
SELECT     0 AS PersonID, cs4.ClientSiteID, cs4.ClientID, cs4.SiteID, r4.RoleID, r4.RoleName, CONVERT(bit, 1) AS CanEdit, CONVERT(bit, 1) AS CanAdd, CONVERT(bit, 1) 
                      AS CanDelete, 'Admin' AS RoleLevel
FROM	RoleClientSite rcs         
	 INNER JOIN dbo.ClientSite AS cs4 ON rcs.ClientSiteID = cs4.ClientSiteID
	 INNER JOIN dbo.Role AS r4 ON rcs.RoleID = r4.RoleID
UNION 
SELECT    DISTINCT 0 AS PersonID, cs2.ClientSiteID, cs2.ClientID, cs2.SiteID, rfacs2.RoleID, r2.RoleName, CASE WHEN SUM(CONVERT(int, CanEdit)) > 1 THEN 1 ELSE SUM(CONVERT(int, CanEdit)) END AS CanEdit,  CASE WHEN SUM(CONVERT(int, CanAdd)) > 1 THEN 1 ELSE SUM(CONVERT(int, CanAdd)) END AS CanAdd,  CASE WHEN SUM(CONVERT(int, CanDelete)) > 1 THEN 1 ELSE SUM(CONVERT(int, CanDelete)) END AS CanDelete,--rfacs2.CanEdit, rfacs2.CanAdd, rfacs2.CanDelete, 
--SELECT    DISTINCT 0 AS PersonID, cs2.ClientSiteID, cs2.ClientID, cs2.SiteID, rfacs2.RoleID, r2.RoleName, CONVERT(bit, 1) AS CanEdit, CONVERT(bit, 1) AS CanAdd, CONVERT(bit, 1) AS CanDelete,--rfacs2.CanEdit, rfacs2.CanAdd, rfacs2.CanDelete, 
                 'Admin' AS RoleLevel
FROM         dbo.Person AS p2 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs2 ON p2.PersonID = pfacs2.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs2 ON pfacs2.FirmAddressClientSiteID = facs2.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs2 ON facs2.ClientSiteID = cs2.ClientSiteID INNER JOIN
                      dbo.RoleFirmAddressClientSite AS rfacs2 ON facs2.FirmAddressClientSiteID = rfacs2.FirmAddressClientSiteID INNER JOIN
                      dbo.Role AS r2 ON rfacs2.RoleID = r2.RoleID
WHERE     'P' + CONVERT(nvarchar(100), p2.PersonID) + 'R' + CONVERT(nvarchar(100), rfacs2.RoleID) NOT IN
                          (SELECT     'P' + CONVERT(nvarchar(100), p2a.PersonID) + 'R' + CONVERT(nvarchar(100), rpfacs2a.RoleID)
                            FROM          dbo.Person AS p2a INNER JOIN
                                                   dbo.PersonFirmAddressClientSite AS pfacs2a ON p2a.PersonID = pfacs2a.PersonID INNER JOIN
                                                   dbo.RolePersonFirmAddressClientSite AS rpfacs2a ON pfacs2a.PersonFirmAddressClientSiteID = rpfacs2a.PersonFirmAddressClientSiteID INNER JOIN
                                                   dbo.Role AS r2a ON rpfacs2a.RoleID = r2a.RoleID)
												   GROUP BY cs2.ClientSiteID, cs2.ClientID, cs2.SiteID, rfacs2.RoleID, r2.RoleName
--UNION
--SELECT     0 AS PersonID, cs3.ClientSiteID, cs3.ClientID, cs3.SiteID, rcs3.RoleID, r3.RoleName, CONVERT(bit, 1) AS CanEdit, CONVERT(bit, 1) AS CanAdd, CONVERT(bit, 1) AS CanDelete, 'Admin' AS RoleLevel--'Client Site Level' AS RoleLevel
--FROM         dbo.Person AS p3 INNER JOIN
--                      dbo.PersonFirmAddressClientSite AS pfacs3 ON p3.PersonID = pfacs3.PersonID INNER JOIN
--                      dbo.FirmAddressClientSite AS facs3 ON pfacs3.FirmAddressClientSiteID = facs3.FirmAddressClientSiteID INNER JOIN
--                      dbo.ClientSite AS cs3 ON facs3.ClientSiteID = cs3.ClientSiteID INNER JOIN
--                      dbo.RoleClientSite AS rcs3 ON cs3.ClientSiteID = rcs3.ClientSiteID INNER JOIN
--                      dbo.Role AS r3 ON rcs3.RoleID = r3.RoleID
--WHERE     'P' + CONVERT(nvarchar(100), p3.PersonID) + 'R' + CONVERT(nvarchar(100), rcs3.RoleID) NOT IN
--                          (SELECT     'P' + CONVERT(nvarchar(100), p3a.PersonID) + 'R' + CONVERT(nvarchar(100), rpfacs3a.RoleID)
--                            FROM          dbo.Person AS p3a INNER JOIN
--                                                   dbo.PersonFirmAddressClientSite AS pfacs3a ON p3a.PersonID = pfacs3a.PersonID INNER JOIN
--                                                   dbo.RolePersonFirmAddressClientSite AS rpfacs3a ON pfacs3a.PersonFirmAddressClientSiteID = rpfacs3a.PersonFirmAddressClientSiteID INNER JOIN
--                                                   dbo.Role AS r3a ON rpfacs3a.RoleID = r3a.RoleID) AND 'P' + CONVERT(nvarchar(100), p3.PersonID) + 'R' + CONVERT(nvarchar(100), rcs3.RoleID) 
--                      NOT IN
--                          (SELECT     'P' + CONVERT(nvarchar(100), p3b.PersonID) + 'R' + CONVERT(nvarchar(100), rfacs3b.RoleID)
--                            FROM          dbo.Person AS p3b INNER JOIN
--                                                   dbo.PersonFirmAddressClientSite AS pfacs3b ON p3b.PersonID = pfacs3b.PersonID INNER JOIN
--                                                   dbo.FirmAddressClientSite AS facs3b ON pfacs3b.FirmAddressClientSiteID = facs3b.FirmAddressClientSiteID INNER JOIN
--                                                   dbo.RoleFirmAddressClientSite AS rfacs3b ON facs3b.FirmAddressClientSiteID = rfacs3b.FirmAddressClientSiteID INNER JOIN
--                                                   dbo.Role AS r3b ON rfacs3b.RoleID = r3b.RoleID)



GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonRoles_OLDEST] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonRoles_OLDEST] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonRoles_OLDEST] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonRoles_OLDEST] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED - as the name indicates, old way of getting roles for a specific person', 'SCHEMA', N'dbo', 'VIEW', N'vPersonRoles_OLDEST', NULL, NULL
GO
