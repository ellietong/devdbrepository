SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vPersonRoles]
AS

SELECT PersonID, ClientSiteID, ClientID, SiteID, RoleID, RoleName, RoleType, 
	
	CASE WHEN (COUNT(DISTINCT RoleLevel) = 1) 
		 THEN MAX(CONVERT(int,CanEdit  ))
		 ELSE CASE WHEN (SELECT COALESCE(MAX(CONVERT(int,CanEdit  )),-1) FROM vPersonRoleDefinition WHERE PersonID = prd.PersonID AND ClientSiteID = prd.ClientSiteID AND RoleID = prd.RoleID AND RoleLevel = 'Person Level') >= 0 
				   THEN (SELECT          MAX(CONVERT(int,CanEdit  ))     FROM vPersonRoleDefinition WHERE PersonID = prd.PersonID AND ClientSiteID = prd.ClientSiteID AND RoleID = prd.RoleID AND RoleLevel = 'Person Level')
				   ELSE (SELECT          MAX(CONVERT(int,CanEdit  ))     FROM vPersonRoleDefinition WHERE PersonID = prd.PersonID AND ClientSiteID = prd.ClientSiteID AND RoleID = prd.RoleID AND RoleLevel = 'Firm Address Level') END END AS CanEdit,
	
	CASE WHEN (COUNT(DISTINCT RoleLevel) = 1) 
		 THEN MAX(CONVERT(int,CanAdd   ))
		 ELSE CASE WHEN (SELECT COALESCE(MAX(CONVERT(int,CanAdd   )),-1) FROM vPersonRoleDefinition WHERE PersonID = prd.PersonID AND ClientSiteID = prd.ClientSiteID AND RoleID = prd.RoleID AND RoleLevel = 'Person Level') >= 0 
				   THEN (SELECT          MAX(CONVERT(int,CanAdd   ))     FROM vPersonRoleDefinition WHERE PersonID = prd.PersonID AND ClientSiteID = prd.ClientSiteID AND RoleID = prd.RoleID AND RoleLevel = 'Person Level')
				   ELSE (SELECT          MAX(CONVERT(int,CanAdd   ))     FROM vPersonRoleDefinition WHERE PersonID = prd.PersonID AND ClientSiteID = prd.ClientSiteID AND RoleID = prd.RoleID AND RoleLevel = 'Firm Address Level') END END AS CanAdd,
	
	CASE WHEN (COUNT(DISTINCT RoleLevel) = 1) 
		 THEN MAX(CONVERT(int,CanDelete))
		 ELSE CASE WHEN (SELECT COALESCE(MAX(CONVERT(int,CanDelete)),-1) FROM vPersonRoleDefinition WHERE PersonID = prd.PersonID AND ClientSiteID = prd.ClientSiteID AND RoleID = prd.RoleID AND RoleLevel = 'Person Level') >= 0 
				   THEN (SELECT          MAX(CONVERT(int,CanDelete))     FROM vPersonRoleDefinition WHERE PersonID = prd.PersonID AND ClientSiteID = prd.ClientSiteID AND RoleID = prd.RoleID AND RoleLevel = 'Person Level')
				   ELSE (SELECT          MAX(CONVERT(int,CanDelete))     FROM vPersonRoleDefinition WHERE PersonID = prd.PersonID AND ClientSiteID = prd.ClientSiteID AND RoleID = prd.RoleID AND RoleLevel = 'Firm Address Level') END END AS CanDelete,
    
	CASE WHEN (COUNT(DISTINCT RoleLevel) = 1 AND COUNT(RoleLevel) = 1) THEN MAX(RoleLevel)
		 WHEN (COUNT(DISTINCT RoleLevel) = 1 AND COUNT(RoleLevel) > 1) THEN MAX(RoleLevel) + ' (Multiple Instances)'
		                                                               ELSE 'Multiple Levels' END AS RoleLevel
FROM vPersonRoleDefinition prd
GROUP BY PersonID, ClientSiteID, ClientID, SiteID, RoleID, RoleName, RoleType


GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonRoles] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonRoles] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonRoles] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonRoles] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Shows Role mapping from all levels (ClientSite, FirmAddress, and Person) consolidated down to the person.  This is what Ligado uses to check what roles an individual has upon logging in.', 'SCHEMA', N'dbo', 'VIEW', N'vPersonRoles', NULL, NULL
GO
