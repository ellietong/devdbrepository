SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vDataGridColumn]
AS
SELECT     DataGridColumnID, DataGridID, SortOrder, c.ColumnID, 
                      c.DisplayName, c.DataType, c.DataField, c.DataFormatString, c.SortExpression, c.HeaderAlignment, c.EditorControlType, c.DataValueField, c.DataTextField, 
                      c.ServiceObject, c.ServiceMethod, c.InputMask, c.LinkURL, dgc.DataKeyField, dgc.ForeignKeyFlag, CONVERT(bit, 
                      CASE WHEN SUM(CONVERT(int, dgc.VisibleNormal)) > 0 THEN 1 ELSE 0 END) AS VisibleNormal, CONVERT(bit, CASE WHEN SUM(CONVERT(int, 
                      dgc.VisibleEdit)) > 0 THEN 1 ELSE 0 END) AS VisibleEdit, 
					  dgc.SortOrder AS DefaultSortOrder, 
                     dgc.ReadOnly AS [ReadOnly] --Updated to allow RoleDataGridColumn readonly flag to override DataGridColumn setting

FROM  
                      dbo.DataGridColumn dgc INNER JOIN 
                      dbo.[Column] AS c ON dgc.ColumnID = c.ColumnID
GROUP BY dgc.DataGridColumnID, dgc.DataGridID, SortOrder, c.ColumnID, c.DisplayName, DataKeyField, dgc.ForeignKeyFlag, c.DataField, c.DataFormatString, c.DataTextField, c.DataType, c.DataValueField, c.EditorControlType, 
                      c.HeaderAlignment, c.InputMask, c.LinkURL, c.ServiceMethod, c.ServiceObject, c.SortExpression, dgc.ReadOnly --Updated to allow RoleDataGridColumn readonly flag to override DataGridColumn setting





GO
GRANT SELECT ON  [dbo].[vDataGridColumn] TO [Ligado_User]
GO
