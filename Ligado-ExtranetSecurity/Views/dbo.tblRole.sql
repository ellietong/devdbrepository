SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblRole]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.tblRole

GO
