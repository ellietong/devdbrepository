SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vADMIN_FirmAddress]
AS
SELECT fa.*, s.*, s.State AS FirmState FROM FirmAddress fa
LEFT JOIN tblState s ON fa.FirmStateID = s.StateID



GO
GRANT SELECT ON  [dbo].[vADMIN_FirmAddress] TO [Ligado_User]
GO
