SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vADMIN_FirmCase]
AS
SELECT fc.*, f.Firm, f.FirmDisplayName, c.CaseName, c.CaseDisplayName
FROM [Ligado-ExtranetSecurity].dbo.tblFirmCase fc
INNER JOIN Firm f ON fc.FirmID = f.FirmID
INNER JOIN [Ligado-ExtranetSecurity].dbo.tblCase c ON fc.CaseID = c.CaseID




GO
GRANT SELECT ON  [dbo].[vADMIN_FirmCase] TO [Ligado_User]
GO
