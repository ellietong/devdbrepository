SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Column]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.[Column]
GO
GRANT VIEW DEFINITION ON  [dbo].[Column] TO [base_user]
GRANT SELECT ON  [dbo].[Column] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Column] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Column] TO [Ligado_User]
GO
