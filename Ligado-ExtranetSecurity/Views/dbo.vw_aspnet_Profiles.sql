SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

  CREATE VIEW [dbo].[vw_aspnet_Profiles]
  AS SELECT [dbo].[aspnet_Profile].[UserId], [dbo].[aspnet_Profile].[LastUpdatedDate],
      [DataSize]=  DATALENGTH([dbo].[aspnet_Profile].[PropertyNames])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesString])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesBinary])
  FROM [dbo].[aspnet_Profile]
  
GO
GRANT SELECT ON  [dbo].[vw_aspnet_Profiles] TO [aspnet_Profile_ReportingAccess]
GRANT VIEW DEFINITION ON  [dbo].[vw_aspnet_Profiles] TO [base_user]
GRANT SELECT ON  [dbo].[vw_aspnet_Profiles] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vw_aspnet_Profiles] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vw_aspnet_Profiles] TO [Ligado_User]
GO
