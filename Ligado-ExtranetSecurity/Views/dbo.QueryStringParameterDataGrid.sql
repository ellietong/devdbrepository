SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[QueryStringParameterDataGrid]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.QueryStringParameterDataGrid
GO
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameterDataGrid] TO [base_user]
GRANT SELECT ON  [dbo].[QueryStringParameterDataGrid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameterDataGrid] TO [Ligado_User]
GRANT SELECT ON  [dbo].[QueryStringParameterDataGrid] TO [Ligado_User]
GO
