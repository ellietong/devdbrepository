SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[Person]
AS
SELECT     p.PersonID, p.MembershipID, p.PrefixID, p.FirstName, '' AS MiddleName, p.LastName, p.SuffixID, p.GenderID, p.DirectPhone, p.DirectFax, p.Email, p.Password, p.PasswordReset,
p.PasswordQuestionID, p.PasswordAnswer, p.TermsAcceptedDate, p.notes,
 pl.Status, pl.PositionID, pl.FirmID, pl.FirmAddressID, pl.CarrierGroupID, pl.PlaintiffCounselAddressID
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.Person p
LEFT JOIN WEBSQL.[Ligado-ExtranetSecurity].dbo.Person_SPFields pl ON p.PersonID = pl.PersonID



GO
GRANT VIEW DEFINITION ON  [dbo].[Person] TO [base_user]
GRANT SELECT ON  [dbo].[Person] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Person] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Person] TO [Ligado_User]
GO
