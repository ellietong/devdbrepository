SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[vADMIN_PersonMembershipInfo]
AS
SELECT     dbo.aspnet_Membership.ApplicationId, dbo.aspnet_Membership.Password, dbo.aspnet_Membership.PasswordFormat, 
                      dbo.aspnet_Membership.PasswordSalt, dbo.aspnet_Membership.MobilePIN, dbo.aspnet_Membership.Email, dbo.aspnet_Membership.LoweredEmail, 
                      dbo.aspnet_Membership.PasswordQuestion, dbo.aspnet_Membership.PasswordAnswer, dbo.aspnet_Membership.IsApproved, dbo.aspnet_Membership.IsLockedOut AS IsLockedOut, 
                      dbo.aspnet_Membership.CreateDate, dbo.aspnet_Membership.LastLoginDate, ---1 AS UserId,
					  --Take the oldest available LastPasswordChangedDate
					  dbo.aspnet_Membership.LastPasswordChangedDate, 
                      dbo.aspnet_Membership.LastLockoutDate, dbo.aspnet_Membership.FailedPasswordAttemptCount, dbo.aspnet_Membership.FailedPasswordAttemptWindowStart, 
                      dbo.aspnet_Membership.FailedPasswordAnswerAttemptCount, dbo.aspnet_Membership.FailedPasswordAnswerAttemptWindowStart, 
                      dbo.aspnet_Membership.Comment, dbo.Person.MembershipID, dbo.Person.PrefixID, dbo.Person.FirstName, dbo.Person.MiddleName, 
                      dbo.Person.LastName, dbo.Person.SuffixID, dbo.Person.GenderID, dbo.Person.DirectPhone, dbo.Person.DirectFax, dbo.Person.Email AS Expr1, 
                      dbo.Person.Password AS Expr2, CONVERT(BIT, COALESCE(dbo.Person.PasswordReset, 0)) AS PasswordReset, dbo.Person.PasswordQuestionID, dbo.Person.PasswordAnswer AS Expr3, 
                      dbo.Person.TermsAcceptedDate, dbo.Person.Notes, dbo.Person.PersonID, UserID, Gender
					  FROM         dbo.aspnet_Membership INNER JOIN
                      dbo.Person ON dbo.aspnet_Membership.Email = dbo.Person.Email
					  LEFT JOIN Gender g ON dbo.Person.GenderID = g.GenderID






GO
GRANT VIEW DEFINITION ON  [dbo].[vADMIN_PersonMembershipInfo] TO [base_user]
GRANT SELECT ON  [dbo].[vADMIN_PersonMembershipInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vADMIN_PersonMembershipInfo] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vADMIN_PersonMembershipInfo] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used in Admin Panel to populate the User Management grid - includes on users who are in both the aspnet Membership tables and table Person.', 'SCHEMA', N'dbo', 'VIEW', N'vADMIN_PersonMembershipInfo', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "aspnet_Membership"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 307
               Right = 329
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Person"
            Begin Extent = 
               Top = 6
               Left = 367
               Bottom = 114
               Right = 547
            End
            DisplayFlags = 280
            TopColumn = 10
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 39
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170', 'SCHEMA', N'dbo', 'VIEW', N'vADMIN_PersonMembershipInfo', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vADMIN_PersonMembershipInfo', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vADMIN_PersonMembershipInfo', NULL, NULL
GO
