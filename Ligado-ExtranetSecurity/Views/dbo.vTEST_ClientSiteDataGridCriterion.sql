SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vTEST_ClientSiteDataGridCriterion]
AS

SELECT rcs.ClientSiteID, dg.AjaxSource, DataGridName, dg.DataGridID,  c.DataField AS PropertyToFilter
FROM RoleClientSite rcs
INNER JOIN RoleDataGridCriterion rdgc ON rcs.RoleID = rdgc.RoleID
INNER JOIN DataGridCriterion dgc ON dgc.DataGridCriterionID = rdgc.DataGridCriterionID
INNER JOIN Criterion c ON dgc.CriterionID = c.CriterionID
INNER JOIN DataGrid dg ON dgc.DataGridID = dg.DataGridID
GO
GRANT VIEW DEFINITION ON  [dbo].[vTEST_ClientSiteDataGridCriterion] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vTEST_ClientSiteDataGridCriterion] TO [Ligado_User]
GO
