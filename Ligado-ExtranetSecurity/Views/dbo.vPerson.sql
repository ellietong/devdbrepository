SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPerson]
AS
SELECT  p.PersonID, 
		pl.FirmID, 
		LastName AS LName, 
		FirstName AS FName, 
		pl.PositionID, 
		p.Email, 
		pl.[Status], 
		p.Password, 
		PasswordReset, 
		pl.FirmAddressID, 
		pl.CarrierGroupID, 
		pl.PlaintiffCounselAddressID, 
		LastLoginDate, 
		LastPasswordChangedDate, 
		LastLockoutDate, 
		IsLockedOut, 
		FailedPasswordAttemptWindowStart,
		FailedPasswordAttemptCount, 
		FailedPasswordAnswerAttemptWindowStart
FROM Person p
	LEFT JOIN Person_SPFields pl ON p.PersonID = pl.PersonID
	LEFT JOIN aspnet_Membership aspm ON p.Email LIKE aspm.Email



GO
