SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Site]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.[Site]
GO
GRANT VIEW DEFINITION ON  [dbo].[Site] TO [base_user]
GRANT SELECT ON  [dbo].[Site] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Site] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Site] TO [Ligado_User]
GO
