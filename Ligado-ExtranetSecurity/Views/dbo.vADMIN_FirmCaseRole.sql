SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[vADMIN_FirmCaseRole]
AS
SELECT	pr.RolePermissionsID, 
		COALESCE(pr.CaseID, 0) AS CaseID, 
		COALESCE(pr.FirmID, 0) AS FirmID,
		COALESCE(pr.RoleID, 0) AS RoleID,
		c.CaseDisplayName, 
		f.FirmDisplayName, 
		r.Role
FROM [Ligado-ExtranetSecurity].dbo.tblPermissionsRole pr
INNER JOIN [Ligado-ExtranetSecurity].dbo.tblCase c ON pr.CaseID = c.CaseID
INNER JOIN [Ligado-ExtranetSecurity].dbo.Firm f ON pr.FirmID = f.FirmID
INNER JOIN [Ligado-ExtranetSecurity].dbo.tblRole r ON pr.RoleID = r.RoleID





GO
GRANT SELECT ON  [dbo].[vADMIN_FirmCaseRole] TO [Ligado_User]
GO
