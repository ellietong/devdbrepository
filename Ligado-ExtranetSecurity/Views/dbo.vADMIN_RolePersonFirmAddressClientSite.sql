SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vADMIN_RolePersonFirmAddressClientSite]
AS
SELECT     r.RoleName, r.RoleType, rpfacs.RolePersonFirmAddressClientSiteID, pfacs.PersonFirmAddressClientSiteID, CanAdd, CanEdit, CanDelete, rpfacs.RoleID
FROM dbo.RolePersonFirmAddressClientSite rpfacs        
						INNER JOIN dbo.PersonFirmAddressClientSite pfacs ON rpfacs.PersonFirmAddressClientSiteID = pfacs.PersonFirmAddressClientSiteID
						INNER JOIN [Role] r ON rpfacs.RoleID = r.RoleID
WHERE     (pfacs.Active = 1)




GO
GRANT SELECT ON  [dbo].[vADMIN_RolePersonFirmAddressClientSite] TO [Ligado_User]
GO
