SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[FirmAddress]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.FirmAddress
GO
GRANT VIEW DEFINITION ON  [dbo].[FirmAddress] TO [base_user]
GRANT SELECT ON  [dbo].[FirmAddress] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[FirmAddress] TO [Ligado_User]
GRANT SELECT ON  [dbo].[FirmAddress] TO [Ligado_User]
GO
