SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[DetailsViewColumn]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.DetailsViewColumn
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailsViewColumn] TO [base_user]
GRANT SELECT ON  [dbo].[DetailsViewColumn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DetailsViewColumn] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DetailsViewColumn] TO [Ligado_User]
GO
