SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblFirmCase_OLD]
AS
SELECT     *
FROM         [FBSecurity].dbo.tblFirmCase 

GO
GRANT SELECT ON  [dbo].[tblFirmCase_OLD] TO [Ligado_User]
GO
