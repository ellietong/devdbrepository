SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPersonRoles_Old]
AS
SELECT     p1.PersonID, cs1.ClientSiteID, cs1.ClientID, cs1.SiteID, rpfacs1.RoleID, r1.RoleName, rpfacs1.CanEdit, rpfacs1.CanAdd, rpfacs1.CanDelete, 
                      'Person Level' AS RoleLevel
FROM         dbo.Person AS p1 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs1 ON p1.PersonID = pfacs1.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs1 ON pfacs1.FirmAddressClientSiteID = facs1.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs1 ON facs1.ClientSiteID = cs1.ClientSiteID INNER JOIN
                      dbo.RolePersonFirmAddressClientSite AS rpfacs1 ON pfacs1.PersonFirmAddressClientSiteID = rpfacs1.PersonFirmAddressClientSiteID INNER JOIN
                      dbo.Role AS r1 ON rpfacs1.RoleID = r1.RoleID
WHERE p1.PersonID <> 0
UNION
SELECT     p2.PersonID, cs2.ClientSiteID, cs2.ClientID, cs2.SiteID, rfacs2.RoleID, r2.RoleName, rfacs2.CanEdit, rfacs2.CanAdd, rfacs2.CanDelete, 
                      'Firm Address Level' AS RoleLevel
FROM         dbo.Person AS p2 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs2 ON p2.PersonID = pfacs2.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs2 ON pfacs2.FirmAddressClientSiteID = facs2.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs2 ON facs2.ClientSiteID = cs2.ClientSiteID INNER JOIN
                      dbo.RoleFirmAddressClientSite AS rfacs2 ON facs2.FirmAddressClientSiteID = rfacs2.FirmAddressClientSiteID INNER JOIN
                      dbo.Role AS r2 ON rfacs2.RoleID = r2.RoleID
WHERE     'P' + CONVERT(nvarchar(100), p2.PersonID) + 'R' + CONVERT(nvarchar(100), rfacs2.RoleID) + 'CS' + CONVERT(nvarchar(100), cs2.ClientSiteID) NOT IN
                          (SELECT     'P' + CONVERT(nvarchar(100), p2a.PersonID) + 'R' + CONVERT(nvarchar(100), rpfacs2a.RoleID) + 'CS' + CONVERT(nvarchar(100), facs2a.ClientSiteID)
                            FROM          dbo.Person AS p2a INNER JOIN
                                                   dbo.PersonFirmAddressClientSite AS pfacs2a ON p2a.PersonID = pfacs2a.PersonID INNER JOIN
                                                   dbo.FirmAddressClientSite AS facs2a ON pfacs2a.FirmAddressClientSiteID = facs2a.FirmAddressClientSiteID INNER JOIN
                                                   dbo.RolePersonFirmAddressClientSite AS rpfacs2a ON pfacs2a.PersonFirmAddressClientSiteID = rpfacs2a.PersonFirmAddressClientSiteID INNER JOIN
                                                   dbo.Role AS r2a ON rpfacs2a.RoleID = r2a.RoleID)
AND p2.PersonID <> 0
UNION
SELECT     p3.PersonID, cs3.ClientSiteID, cs3.ClientID, cs3.SiteID, rcs3.RoleID, r3.RoleName, rcs3.CanEdit, rcs3.CanAdd, rcs3.CanDelete, 'Client Site Level' AS RoleLevel
FROM         dbo.Person AS p3 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs3 ON p3.PersonID = pfacs3.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs3 ON pfacs3.FirmAddressClientSiteID = facs3.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs3 ON facs3.ClientSiteID = cs3.ClientSiteID INNER JOIN
                      dbo.RoleClientSite AS rcs3 ON cs3.ClientSiteID = rcs3.ClientSiteID INNER JOIN
                      dbo.Role AS r3 ON rcs3.RoleID = r3.RoleID
WHERE     'P' + CONVERT(nvarchar(100), p3.PersonID) + 'R' + CONVERT(nvarchar(100), rcs3.RoleID) + 'CS' + CONVERT(nvarchar(100), cs3.ClientSiteID) NOT IN
                          (SELECT     'P' + CONVERT(nvarchar(100), p3a.PersonID) + 'R' + CONVERT(nvarchar(100), rpfacs3a.RoleID) + 'CS' + CONVERT(nvarchar(100), facs3a.ClientSiteID)
                            FROM          dbo.Person AS p3a INNER JOIN
                                                   dbo.PersonFirmAddressClientSite AS pfacs3a ON p3a.PersonID = pfacs3a.PersonID INNER JOIN
                                                   dbo.FirmAddressClientSite AS facs3a ON pfacs3a.FirmAddressClientSiteID = facs3a.FirmAddressClientSiteID INNER JOIN
                                                   dbo.RolePersonFirmAddressClientSite AS rpfacs3a ON pfacs3a.PersonFirmAddressClientSiteID = rpfacs3a.PersonFirmAddressClientSiteID INNER JOIN
                                                   dbo.Role AS r3a ON rpfacs3a.RoleID = r3a.RoleID) AND 'P' + CONVERT(nvarchar(100), p3.PersonID) + 'R' + CONVERT(nvarchar(100), rcs3.RoleID) 
                      + 'CS' + CONVERT(nvarchar(100), cs3.ClientSiteID) NOT IN
                          (SELECT     'P' + CONVERT(nvarchar(100), p3b.PersonID) + 'R' + CONVERT(nvarchar(100), rfacs3b.RoleID) + 'CS' + CONVERT(nvarchar(100), facs3b.ClientSiteID)
                            FROM          dbo.Person AS p3b INNER JOIN
                                                   dbo.PersonFirmAddressClientSite AS pfacs3b ON p3b.PersonID = pfacs3b.PersonID INNER JOIN
                                                   dbo.FirmAddressClientSite AS facs3b ON pfacs3b.FirmAddressClientSiteID = facs3b.FirmAddressClientSiteID INNER JOIN
                                                   dbo.RoleFirmAddressClientSite AS rfacs3b ON facs3b.FirmAddressClientSiteID = rfacs3b.FirmAddressClientSiteID INNER JOIN
                                                   dbo.Role AS r3b ON rfacs3b.RoleID = r3b.RoleID)
AND p3.PersonID <> 0
UNION
SELECT DISTINCT     0 AS PersonID, cs1.ClientSiteID, cs1.ClientID, cs1.SiteID, rpfacs1.RoleID, r1.RoleName, 1 AS CanEdit, 1 AS CanAdd, 1 AS CanDelete, 
                      'Person Level' AS RoleLevel
FROM         dbo.Person AS p1 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs1 ON p1.PersonID = pfacs1.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs1 ON pfacs1.FirmAddressClientSiteID = facs1.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs1 ON facs1.ClientSiteID = cs1.ClientSiteID INNER JOIN
                      dbo.RolePersonFirmAddressClientSite AS rpfacs1 ON pfacs1.PersonFirmAddressClientSiteID = rpfacs1.PersonFirmAddressClientSiteID INNER JOIN
                      dbo.Role AS r1 ON rpfacs1.RoleID = r1.RoleID
UNION
SELECT DISTINCT    0 AS PersonID, cs2.ClientSiteID, cs2.ClientID, cs2.SiteID, rfacs2.RoleID, r2.RoleName, 1 AS CanEdit, 1 AS CanAdd, 1 AS CanDelete, 
                      'Firm Address Level' AS RoleLevel
FROM         dbo.Person AS p2 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs2 ON p2.PersonID = pfacs2.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs2 ON pfacs2.FirmAddressClientSiteID = facs2.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs2 ON facs2.ClientSiteID = cs2.ClientSiteID INNER JOIN
                      dbo.RoleFirmAddressClientSite AS rfacs2 ON facs2.FirmAddressClientSiteID = rfacs2.FirmAddressClientSiteID INNER JOIN
                      dbo.Role AS r2 ON rfacs2.RoleID = r2.RoleID
WHERE     'R' + CONVERT(nvarchar(100), rfacs2.RoleID) + 'CS' + CONVERT(nvarchar(100), cs2.ClientSiteID) NOT IN
                          (SELECT     'R' + CONVERT(nvarchar(100), rpfacs2a.RoleID) + 'CS' + CONVERT(nvarchar(100), facs2a.ClientSiteID)
                            FROM          dbo.Person AS p2a INNER JOIN
                                                   dbo.PersonFirmAddressClientSite AS pfacs2a ON p2a.PersonID = pfacs2a.PersonID INNER JOIN
                                                   dbo.FirmAddressClientSite AS facs2a ON pfacs2a.FirmAddressClientSiteID = facs2a.FirmAddressClientSiteID INNER JOIN
                                                   dbo.RolePersonFirmAddressClientSite AS rpfacs2a ON pfacs2a.PersonFirmAddressClientSiteID = rpfacs2a.PersonFirmAddressClientSiteID INNER JOIN
                                                   dbo.Role AS r2a ON rpfacs2a.RoleID = r2a.RoleID)
UNION
SELECT DISTINCT    0 AS PersonID, cs3.ClientSiteID, cs3.ClientID, cs3.SiteID, rcs3.RoleID, r3.RoleName, 1 AS CanEdit, 1 AS CanAdd, 1 AS CanDelete, 'Client Site Level' AS RoleLevel
FROM         dbo.Person AS p3 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs3 ON p3.PersonID = pfacs3.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs3 ON pfacs3.FirmAddressClientSiteID = facs3.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs3 ON facs3.ClientSiteID = cs3.ClientSiteID INNER JOIN
                      dbo.RoleClientSite AS rcs3 ON cs3.ClientSiteID = rcs3.ClientSiteID INNER JOIN
                      dbo.Role AS r3 ON rcs3.RoleID = r3.RoleID
WHERE     'R' + CONVERT(nvarchar(100), rcs3.RoleID) + 'CS' + CONVERT(nvarchar(100), cs3.ClientSiteID) NOT IN
                          (SELECT     'R' + CONVERT(nvarchar(100), rpfacs3a.RoleID) + 'CS' + CONVERT(nvarchar(100), facs3a.ClientSiteID)
                            FROM          dbo.Person AS p3a INNER JOIN
                                                   dbo.PersonFirmAddressClientSite AS pfacs3a ON p3a.PersonID = pfacs3a.PersonID INNER JOIN
                                                   dbo.FirmAddressClientSite AS facs3a ON pfacs3a.FirmAddressClientSiteID = facs3a.FirmAddressClientSiteID INNER JOIN
                                                   dbo.RolePersonFirmAddressClientSite AS rpfacs3a ON pfacs3a.PersonFirmAddressClientSiteID = rpfacs3a.PersonFirmAddressClientSiteID INNER JOIN
                                                   dbo.Role AS r3a ON rpfacs3a.RoleID = r3a.RoleID) AND 'R' + CONVERT(nvarchar(100), rcs3.RoleID) 
                      + 'CS' + CONVERT(nvarchar(100), cs3.ClientSiteID) NOT IN
                          (SELECT     'R' + CONVERT(nvarchar(100), rfacs3b.RoleID) + 'CS' + CONVERT(nvarchar(100), facs3b.ClientSiteID)
                            FROM          dbo.Person AS p3b INNER JOIN
                                                   dbo.PersonFirmAddressClientSite AS pfacs3b ON p3b.PersonID = pfacs3b.PersonID INNER JOIN
                                                   dbo.FirmAddressClientSite AS facs3b ON pfacs3b.FirmAddressClientSiteID = facs3b.FirmAddressClientSiteID INNER JOIN
                                                   dbo.RoleFirmAddressClientSite AS rfacs3b ON facs3b.FirmAddressClientSiteID = rfacs3b.FirmAddressClientSiteID INNER JOIN
                                                   dbo.Role AS r3b ON rfacs3b.RoleID = r3b.RoleID)

GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonRoles_Old] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonRoles_Old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonRoles_Old] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonRoles_Old] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED - as the name indicates, old way of getting roles for a specific person', 'SCHEMA', N'dbo', 'VIEW', N'vPersonRoles_Old', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[40] 2[35] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[39] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 2250
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vPersonRoles_Old', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonRoles_Old', NULL, NULL
GO
