SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Firm]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.Firm
GO
GRANT VIEW DEFINITION ON  [dbo].[Firm] TO [base_user]
GRANT SELECT ON  [dbo].[Firm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Firm] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Firm] TO [Ligado_User]
GO
