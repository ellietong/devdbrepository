SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Client]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.Client
GO
GRANT VIEW DEFINITION ON  [dbo].[Client] TO [base_user]
GRANT SELECT ON  [dbo].[Client] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Client] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Client] TO [Ligado_User]
GO
