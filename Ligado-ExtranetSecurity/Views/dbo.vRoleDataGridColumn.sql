SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vRoleDataGridColumn]
AS
SELECT     rdgc.DataGridColumnID, DataGridID, rdgc.SortOrder, c.ColumnID, 
                      c.DisplayName, c.DataType, c.DataField, c.DataFormatString, c.SortExpression, c.HeaderAlignment, c.EditorControlType, c.DataValueField, c.DataTextField, 
                      c.ServiceObject, c.ServiceMethod, c.InputMask, c.LinkURL, dgc.DataKeyField, dgc.ForeignKeyFlag, CONVERT(bit, 
                      CASE WHEN SUM(CONVERT(int, rdgc.VisibleNormal)) > 0 THEN 1 ELSE 0 END) AS VisibleNormal, CONVERT(bit, CASE WHEN SUM(CONVERT(int, 
                      dgc.VisibleEdit)) > 0 THEN 1 ELSE 0 END) AS VisibleEdit, 
					  dgc.SortOrder AS DefaultSortOrder, 
                     dgc.ReadOnly AS [ReadOnly], --Updated to allow RoleDataGridColumn readonly flag to override DataGridColumn setting
					 rdgc.RoleID, RoleName
FROM					dbo.RoleDataGridColumn rdgc INNER JOIN
						dbo.Role r ON rdgc.RoleID = r.RoleID INNER JOIN
                      dbo.DataGridColumn dgc ON rdgc.DataGridColumnID = dgc.DataGridColumnID
					  INNER JOIN 
                      dbo.[Column] AS c ON dgc.ColumnID = c.ColumnID
GROUP BY rdgc.DataGridColumnID, dgc.DataGridID, rdgc.SortOrder,dgc.SortOrder, c.ColumnID, c.DisplayName, DataKeyField, dgc.ForeignKeyFlag, c.DataField, c.DataFormatString, c.DataTextField, c.DataType, c.DataValueField, c.EditorControlType, 
                      c.HeaderAlignment, c.InputMask, c.LinkURL, c.ServiceMethod, c.ServiceObject, c.SortExpression, dgc.ReadOnly,rdgc.RoleID, RoleName --Updated to allow RoleDataGridColumn readonly flag to override DataGridColumn setting







GO
GRANT SELECT ON  [dbo].[vRoleDataGridColumn] TO [Ligado_User]
GO
