SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[DataGridColumn]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.DataGridColumn
GO
GRANT VIEW DEFINITION ON  [dbo].[DataGridColumn] TO [base_user]
GRANT SELECT ON  [dbo].[DataGridColumn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DataGridColumn] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DataGridColumn] TO [Ligado_User]
GO
