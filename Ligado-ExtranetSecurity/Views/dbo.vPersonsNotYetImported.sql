SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPersonsNotYetImported]
AS

SELECT * 
FROM Person 
WHERE MembershipID IS NOT NULL

GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonsNotYetImported] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonsNotYetImported] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonsNotYetImported] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonsNotYetImported] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED - vADMIN_UnimportedUsers is used instead.', 'SCHEMA', N'dbo', 'VIEW', N'vPersonsNotYetImported', NULL, NULL
GO
