SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblFirmAddressCase_OLD]
AS
SELECT     *
FROM         [FBSecurity].dbo.tblFirmAddressCase 

GO
GRANT SELECT ON  [dbo].[tblFirmAddressCase_OLD] TO [Ligado_User]
GO
