SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vPersonDataGridColumn]
AS
SELECT     dbo.vPersonRoles.PersonID, dbo.RoleDataGridColumn.DataGridColumnID, dbo.vPersonRoles.ClientSiteID, dbo.vPersonRoles.ClientID, dbo.vPersonRoles.SiteID, 
                      CONVERT(bit, CASE WHEN SUM(CONVERT(int, dbo.vPersonRoles.CanEdit)) > 0 THEN 1 ELSE SUM(CONVERT(int, dbo.vPersonRoles.CanEdit)) END) AS CanEdit, 
                      CONVERT(bit, CASE WHEN SUM(CONVERT(int, dbo.vPersonRoles.CanAdd)) > 0 THEN 1 ELSE SUM(CONVERT(int, dbo.vPersonRoles.CanAdd)) END) AS CanAdd, 
                      CONVERT(bit, CASE WHEN SUM(CONVERT(int, dbo.vPersonRoles.CanDelete)) > 0 THEN 1 ELSE SUM(CONVERT(int, dbo.vPersonRoles.CanDelete)) END) 
                      AS CanDelete, dbo.DataGridColumn.DataGridID, dbo.RoleDataGridColumn.Required, MAX(dbo.RoleDataGridColumn.SortOrder) AS SortOrder, c.ColumnID, 
                      c.DisplayName, c.DataType, c.DataField, c.DataFormatString, c.SortExpression, c.HeaderAlignment, c.EditorControlType, c.DataValueField, c.DataTextField, 
                      c.ServiceObject, c.ServiceMethod, c.InputMask, c.LinkURL, dbo.DataGridColumn.DataKeyField, dbo.DataGridColumn.ForeignKeyFlag, CONVERT(bit, 
                      CASE WHEN SUM(CONVERT(int, dbo.RoleDataGridColumn.VisibleNormal)) > 0 THEN 1 ELSE 0 END) AS VisibleNormal, CONVERT(bit, CASE WHEN SUM(CONVERT(int, 
                      dbo.RoleDataGridColumn.VisibleEdit)) > 0 THEN 1 ELSE 0 END) AS VisibleEdit, 
					  CONVERT(bit, CASE WHEN SUM(CONVERT(int, dbo.RoleDataGridColumn.VisibleCreate)) > 0 THEN 1 ELSE 0 END) AS VisibleCreate, --Added VisibleCreate
					  dbo.DataGridColumn.SortOrder AS DefaultSortOrder, 
                      COALESCE(dbo.RoleDataGridColumn.ReadOnly, dbo.DataGridColumn.ReadOnly) AS [ReadOnly], --Updated to allow RoleDataGridColumn readonly flag to override DataGridColumn setting
					  --Add new fields indicating ability to add/edit lookup column records
					  AllowEntriesToBeInserted,
					  AllowEntriesToBeUpdated
FROM         dbo.vPersonRoles INNER JOIN
                      dbo.RoleDataGridColumn ON dbo.vPersonRoles.RoleID = dbo.RoleDataGridColumn.RoleID INNER JOIN
                      dbo.DataGridColumn ON dbo.RoleDataGridColumn.DataGridColumnID = dbo.DataGridColumn.DataGridColumnID INNER JOIN
                      dbo.[Column] AS c ON dbo.DataGridColumn.ColumnID = c.ColumnID
GROUP BY dbo.vPersonRoles.PersonID, dbo.RoleDataGridColumn.DataGridColumnID, dbo.vPersonRoles.ClientSiteID, dbo.vPersonRoles.ClientID, dbo.vPersonRoles.SiteID, 
                      dbo.DataGridColumn.DataGridID, dbo.RoleDataGridColumn.Required, dbo.DataGridColumn.DataKeyField, dbo.DataGridColumn.ForeignKeyFlag, 
                      dbo.DataGridColumn.SortOrder, c.ColumnID, c.DisplayName, c.DataField, c.DataFormatString, c.DataTextField, c.DataType, c.DataValueField, c.EditorControlType, 
                      c.HeaderAlignment, c.InputMask, c.LinkURL, c.ServiceMethod, c.ServiceObject, c.SortExpression, COALESCE(dbo.RoleDataGridColumn.ReadOnly, dbo.DataGridColumn.ReadOnly), --Updated to allow RoleDataGridColumn readonly flag to override DataGridColumn setting
					  --Add new fields indicating ability to add/edit lookup column records
					  AllowEntriesToBeInserted,
					  AllowEntriesToBeUpdated


GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonDataGridColumn] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonDataGridColumn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonDataGridColumn] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonDataGridColumn] TO [Ligado_User]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'For a given PersonID, provides which Columns are associated with a given DataGrid for each ClientSiteID.', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[9] 2[27] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vPersonRoles"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 214
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RoleDataGridColumn"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 110
               Right = 434
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DataGridColumn"
            Begin Extent = 
               Top = 6
               Left = 472
               Bottom = 125
               Right = 649
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 114
               Left = 236
               Bottom = 222
               Right = 419
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 31
         Width = 284
         Width = 1500
         Width = 1515
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         ', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'([vPersonDataGridColumn].[DataGridColumnID]="148")', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'DataGridColumnID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'DataGridColumnID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'DataGridColumnID'
GO
DECLARE @xp smallint
SELECT @xp=2145
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'DataGridColumnID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'DataGridColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'DataGridID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'DataGridID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'DataGridID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'DataGridID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'DataGridID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'SiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'SiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'SiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'SiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridColumn', 'COLUMN', N'SiteID'
GO
