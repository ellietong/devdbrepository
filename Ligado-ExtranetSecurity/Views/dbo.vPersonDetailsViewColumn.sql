SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPersonDetailsViewColumn]
AS
SELECT     dbo.vPersonRoles.PersonID, dbo.RoleDetailsViewColumn.DetailsViewColumnID, dbo.vPersonRoles.ClientSiteID, dbo.vPersonRoles.ClientID, 
                      dbo.vPersonRoles.SiteID, CONVERT(bit, CASE WHEN SUM(CONVERT(int, dbo.vPersonRoles.CanEdit)) > 0 THEN 1 ELSE 0 END) AS CanEdit, CONVERT(bit, 
                      CASE WHEN SUM(CONVERT(int, dbo.vPersonRoles.CanAdd)) > 0 THEN 1 ELSE 0 END) AS CanAdd, CONVERT(bit, CASE WHEN SUM(CONVERT(int, 
                      dbo.vPersonRoles.CanDelete)) > 0 THEN 1 ELSE 0 END) AS CanDelete, dbo.DetailsViewColumn.DetailsViewID, dbo.RoleDetailsViewColumn.SortOrder, 
                      dbo.RoleDetailsViewColumn.Required, dbo.DetailsViewColumn.DataKey, CONVERT(bit, CASE WHEN SUM(CONVERT(int, dbo.RoleDetailsViewColumn.VisibleNormal)) 
                      > 0 THEN 1 ELSE 0 END) AS VisibleNormal, CONVERT(bit, CASE WHEN SUM(CONVERT(int, dbo.RoleDetailsViewColumn.VisibleEdit)) > 0 THEN 1 ELSE 0 END) 
                      AS VisibleEdit, dbo.DetailsViewColumn.ReadOnly, dbo.DetailsViewColumn.ForeignKeyFlag, dbo.DetailsViewColumn.SortOrder AS DefaultSortOrder, 
                      dbo.DetailsViewColumn.ColumnID, dbo.[Column].DisplayName, dbo.[Column].DataType, dbo.[Column].DataField, dbo.[Column].DataFormatString, 
                      dbo.[Column].SortExpression, dbo.[Column].EditorControlType, dbo.[Column].DataValueField, dbo.[Column].DataTextField, dbo.[Column].ServiceObject, 
                      dbo.[Column].ServiceMethod, dbo.[Column].InputMask, dbo.[Column].LinkURL, dbo.[Column].HeaderAlignment,
					  --Add new fields indicating ability to add/edit lookup column records
					  AllowEntriesToBeInserted,
					  AllowEntriesToBeUpdated
FROM         dbo.vPersonRoles INNER JOIN
                      dbo.RoleDetailsViewColumn ON dbo.vPersonRoles.RoleID = dbo.RoleDetailsViewColumn.RoleID INNER JOIN
                      dbo.DetailsViewColumn ON dbo.RoleDetailsViewColumn.DetailsViewColumnID = dbo.DetailsViewColumn.DetailsViewColumnID INNER JOIN
                      dbo.[Column] ON dbo.DetailsViewColumn.ColumnID = dbo.[Column].ColumnID
GROUP BY dbo.vPersonRoles.PersonID, dbo.RoleDetailsViewColumn.DetailsViewColumnID, dbo.vPersonRoles.ClientSiteID, dbo.vPersonRoles.ClientID, 
                      dbo.vPersonRoles.SiteID, dbo.DetailsViewColumn.DetailsViewID, dbo.RoleDetailsViewColumn.SortOrder, dbo.RoleDetailsViewColumn.Required, 
                      dbo.DetailsViewColumn.DataKey, dbo.DetailsViewColumn.ReadOnly, dbo.DetailsViewColumn.ForeignKeyFlag, dbo.DetailsViewColumn.SortOrder, 
                      dbo.DetailsViewColumn.ColumnID, dbo.[Column].DisplayName, dbo.[Column].DataType, dbo.[Column].DataField, dbo.[Column].DataFormatString, 
                      dbo.[Column].SortExpression, dbo.[Column].EditorControlType, dbo.[Column].DataValueField, dbo.[Column].DataTextField, dbo.[Column].ServiceObject, 
                      dbo.[Column].ServiceMethod, dbo.[Column].InputMask, dbo.[Column].LinkURL, dbo.[Column].HeaderAlignment,
					  --Add new fields indicating ability to add/edit lookup column records
					  AllowEntriesToBeInserted,
					  AllowEntriesToBeUpdated

GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonDetailsViewColumn] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonDetailsViewColumn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonDetailsViewColumn] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonDetailsViewColumn] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Provides for a given PersonID which Columns should appear for a certain DetailsView in a certain ClientSite.', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -471
      End
      Begin Tables = 
         Begin Table = "vPersonRoles"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 214
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RoleDetailsViewColumn"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 110
               Right = 446
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "DetailsViewColumn"
            Begin Extent = 
               Top = 6
               Left = 484
               Bottom = 202
               Right = 673
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Column"
            Begin Extent = 
               Top = 6
               Left = 711
               Bottom = 114
               Right = 894
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 32
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 15', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'00
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'(([vPersonDetailsViewColumn].[ClientSiteID]="3")) AND ([vPersonDetailsViewColumn].[DetailsViewID]="1")', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanAdd'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanAdd'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanAdd'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanAdd'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanDelete'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanDelete'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanDelete'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanDelete'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanEdit'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanEdit'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanEdit'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanEdit'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ColumnID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ColumnID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ColumnID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ColumnID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataField'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataField'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataField'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataField'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataField'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataField'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataKey'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataKey'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataKey'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataKey'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataKey'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataKey'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataTextField'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataTextField'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataTextField'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataTextField'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataTextField'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataTextField'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataType'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataType'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataType'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataType'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataType'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataType'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataValueField'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataValueField'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataValueField'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataValueField'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataValueField'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DataValueField'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp smallint
SELECT @xp=2460
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DisplayName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DisplayName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DisplayName'
GO
DECLARE @xp smallint
SELECT @xp=1650
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DisplayName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DisplayName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'DisplayName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'InputMask'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'InputMask'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'InputMask'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'InputMask'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'InputMask'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'InputMask'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'LinkURL'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'LinkURL'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'LinkURL'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'LinkURL'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'LinkURL'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'LinkURL'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'Required'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'Required'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'Required'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'Required'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'Required'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'Required'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortExpression'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortExpression'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortExpression'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortExpression'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortExpression'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortExpression'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortOrder'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortOrder'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortOrder'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortOrder'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortOrder'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'SortOrder'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsViewColumn', 'COLUMN', N'VisibleNormal'
GO
