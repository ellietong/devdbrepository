SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[RoleType]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.RoleType
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleType] TO [base_user]
GRANT SELECT ON  [dbo].[RoleType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleType] TO [Ligado_User]
GO
