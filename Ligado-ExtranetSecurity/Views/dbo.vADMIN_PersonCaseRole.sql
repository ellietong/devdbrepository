SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vADMIN_PersonCaseRole]
AS
SELECT c.CaseName, c.CaseDisplayName, perm.*
FROM [Ligado-ExtranetSecurity].dbo.tblPermissions perm
INNER JOIN [Ligado-ExtranetSecurity].dbo.tblCase c ON perm.CaseID = c.CaseID






GO
GRANT SELECT ON  [dbo].[vADMIN_PersonCaseRole] TO [Ligado_User]
GO
