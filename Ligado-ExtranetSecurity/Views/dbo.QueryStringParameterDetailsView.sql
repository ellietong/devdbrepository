SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[QueryStringParameterDetailsView]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.QueryStringParameterDetailsView
GO
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameterDetailsView] TO [base_user]
GRANT SELECT ON  [dbo].[QueryStringParameterDetailsView] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameterDetailsView] TO [Ligado_User]
GRANT SELECT ON  [dbo].[QueryStringParameterDetailsView] TO [Ligado_User]
GO
