SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[Person_SPFields]
AS

SELECT [PersonID]
      ,[FirmID]
      ,[PositionID]
      ,[FirmAddressID]
      ,[CarrierGroupID]
      ,[PlaintiffCounselAddressID]
      ,[Status]
  FROM [WEBSQL].[Ligado-ExtranetSecurity].[dbo].[Person_SPFields]




GO
GRANT VIEW DEFINITION ON  [dbo].[Person_SPFields] TO [base_user]
GRANT SELECT ON  [dbo].[Person_SPFields] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Person_SPFields] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Person_SPFields] TO [Ligado_User]
GO
