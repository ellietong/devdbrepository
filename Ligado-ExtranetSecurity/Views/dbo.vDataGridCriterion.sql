SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vDataGridCriterion]
AS
SELECT       DataGridCriterionID, 
                          dbo.DataGridCriterion.DataGridID, 
                         MAX(dbo.DataGridCriterion.SortOrder) AS RDGCSortOrder, c.CriterionID, c.AjaxSource, c.ControlType, c.DataField, c.DataTextField, c.DataType, c.DataValueField,
						 c.DisplayName, c.ServiceMethod, c.ServiceObject
FROM  
                         dbo.DataGridCriterion 
						 INNER JOIN [Criterion] c ON DataGridCriterion.CriterionID = c.CriterionID
GROUP BY
DataGridCriterionID,
dbo.DataGridCriterion.DataGridID, 
c.CriterionID, 
c.AjaxSource,
 c.ControlType,
  c.DataField,
   c.DataTextField, 
   c.DataType,
   c.DataValueField,
c.DisplayName, 
c.ServiceMethod, 
c.ServiceObject






GO
GRANT SELECT ON  [dbo].[vDataGridCriterion] TO [Ligado_User]
GO
