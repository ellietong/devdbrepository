SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vADMIN_UnimportedUsers]
AS

SELECT * 
FROM Person 
WHERE LOWER(Email) NOT IN (SELECT LOWER(LoweredEmail) FROM aspnet_Membership)


GO
GRANT VIEW DEFINITION ON  [dbo].[vADMIN_UnimportedUsers] TO [base_user]
GRANT SELECT ON  [dbo].[vADMIN_UnimportedUsers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vADMIN_UnimportedUsers] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vADMIN_UnimportedUsers] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used in the Admin Panel to pull back users that exist in table Person but not in the aspnet membership tables.', 'SCHEMA', N'dbo', 'VIEW', N'vADMIN_UnimportedUsers', NULL, NULL
GO
