SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Gender]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.Gender
GO
GRANT VIEW DEFINITION ON  [dbo].[Gender] TO [base_user]
GRANT SELECT ON  [dbo].[Gender] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Gender] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Gender] TO [Ligado_User]
GO
