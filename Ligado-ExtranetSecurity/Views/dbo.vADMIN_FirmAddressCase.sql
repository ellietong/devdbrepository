SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vADMIN_FirmAddressCase]
AS
SELECT fac.*, f.Firm, f.FirmID, f.FirmDisplayName, c.CaseName, c.CaseDisplayName
FROM [Ligado-ExtranetSecurity].dbo.tblFirmAddressCase fac
INNER JOIN [Ligado-ExtranetSecurity].dbo.FirmAddress fa ON fac.FirmAddressID = fa.FirmAddressID
INNER JOIN [Ligado-ExtranetSecurity].dbo.Firm f ON fa.FirmID = f.FirmID
INNER JOIN [Ligado-ExtranetSecurity].dbo.tblCase c ON fac.CaseID = c.CaseID


GO
GRANT SELECT ON  [dbo].[vADMIN_FirmAddressCase] TO [Ligado_User]
GO
