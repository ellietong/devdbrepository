SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[ClientSite]
AS
SELECT     ClientSiteID,
			ClientID,
			SiteID,
			CASE WHEN ClientSiteID = 2 THEN 'IUNA' ELSE ConnectionStringKey END AS ConnectionStringKey,
			Bucket,
			Active,
			Notes,
			CASE WHEN ClientSiteID = 2 THEN NULL ELSE SharePointUrl END AS SharePointUrl
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.ClientSite

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientSite] TO [base_user]
GRANT SELECT ON  [dbo].[ClientSite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ClientSite] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ClientSite] TO [Ligado_User]
GO
