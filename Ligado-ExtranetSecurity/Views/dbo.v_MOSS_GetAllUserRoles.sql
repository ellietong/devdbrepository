SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[v_MOSS_GetAllUserRoles]
AS
SELECT     PersonID, Email, 'Everyone' AS RoleName
FROM         dbo.vPerson
WHERE     (Status = 1)
UNION
SELECT     p.PersonID, Person_4.Email, replace(role.Role, ',', ' ') AS RoleName
FROM         dbo.tblPermissions AS p INNER JOIN
                      dbo.vPerson AS Person_4 ON p.PersonID = Person_4.PersonID AND Person_4.Status = 1 INNER JOIN
                      dbo.tblPermissionsRole AS r ON Person_4.FirmID = r.FirmID INNER JOIN
                      dbo.tblRole AS role ON r.RoleID = role.RoleID
UNION
SELECT     p.PersonID, Person_3.Email, replace(f.Firm, ',', ' ') + ' - ' + replace(role.Role, ',', ' ') AS RoleName
FROM         dbo.tblPermissions AS p INNER JOIN
                      dbo.vPerson AS Person_3 ON p.PersonID = Person_3.PersonID AND Person_3.Status = 1 INNER JOIN
                      dbo.tblPermissionsRole AS r ON Person_3.FirmID = r.FirmID INNER JOIN
                      dbo.tblRole AS role ON r.RoleID = role.RoleID INNER JOIN
                      dbo.Firm AS f ON f.FirmID = Person_3.FirmID
UNION
SELECT     p.PersonID, Person_2.Email, replace(c.CaseName, ',', ' ') + ' - ' + replace(f.Firm, ',', ' ') AS RoleName
FROM         dbo.tblPermissions AS p INNER JOIN
                      dbo.vPerson AS Person_2 ON p.PersonID = Person_2.PersonID AND Person_2.Status = 1 INNER JOIN
                      dbo.tblPermissionsRole AS r ON Person_2.FirmID = r.FirmID INNER JOIN
                      dbo.Firm AS f ON f.FirmID = r.FirmID AND f.FirmID = Person_2.FirmID INNER JOIN
                      dbo.tblCase AS c ON c.CaseID = p.CaseID
UNION
SELECT     p.PersonID, vPerson.Email, replace(c.CaseName, ',', ' ') + ' - ' + replace(role.Role, ',', ' ') AS RoleName
FROM         dbo.tblPermissions AS p INNER JOIN
                      dbo.vPerson ON p.PersonID = vPerson.PersonID AND vPerson.Status = 1 INNER JOIN
                      dbo.tblPermissionsRole AS r ON vPerson.FirmID = r.FirmID INNER JOIN
                      dbo.tblRole AS role ON r.RoleID = role.RoleID INNER JOIN
                      dbo.tblCase AS c ON c.CaseID = p.CaseID AND c.CaseID = r.CaseID
UNION
SELECT     p.PersonID, p.Email, replace(f.Firm, ',', ' ') AS RoleName
FROM         dbo.Firm AS f INNER JOIN
                      dbo.vPerson AS p ON f.FirmID = p.FirmID AND p.Status = 1
UNION
SELECT     p.PersonID, Person_1.Email, replace(c.CaseName, ',', ' ') AS RoleName
FROM         dbo.tblCase AS c INNER JOIN
                      dbo.tblPermissions AS p ON c.CaseID = p.CaseID INNER JOIN
                      dbo.vPerson AS Person_1 ON p.PersonID = Person_1.PersonID AND Person_1.Status = 1
UNION
SELECT     p.PersonID, p.Email, replace(f.Firm, ',', ' ') + ' - ' + replace(pos.Position, ',', ' ') AS RoleName
FROM         dbo.vPerson AS p INNER JOIN
                      dbo.Firm AS f ON p.FirmID = f.FirmID INNER JOIN
                      dbo.tblPosition AS pos ON p.PositionID = pos.PositionID
WHERE     (p.Status = 1)
UNION
SELECT     p.PersonID, dbo.vPerson.Email, replace(c.CaseName, ',', ' ') + ' - ' + replace(f.Firm, ',', ' ') + ' - ' + replace(pos.Position, ',', ' ') AS RoleName
FROM         dbo.tblPermissions AS p INNER JOIN
                      dbo.vPerson ON p.PersonID = vPerson.PersonID AND vPerson.Status = 1 INNER JOIN
                      dbo.tblPosition pos ON dbo.vPerson.PositionID = pos.PositionID INNER JOIN
                      dbo.Firm f ON vPerson.FirmID = f.FirmID INNER JOIN
                      dbo.tblCase AS c ON c.CaseID = p.CaseID
			
WHERE     (dbo.vPerson.Status = 1)
--Carve out for Reed Smith firms
UNION
SELECT     p.PersonID, dbo.vPerson.Email, replace(c.CaseName, ',', ' ') + ' - Tender User' AS RoleName
FROM         dbo.tblPermissions AS p INNER JOIN
                      dbo.vPerson ON p.PersonID = vPerson.PersonID AND vPerson.Status = 1 INNER JOIN
                      dbo.tblPosition pos ON dbo.vPerson.PositionID = pos.PositionID INNER JOIN
                      dbo.Firm f ON vPerson.FirmID = f.FirmID INNER JOIN
                      dbo.tblCase AS c ON c.CaseID = p.CaseID
			WHERE vPerson.PersonID IN (3275, 3276, 3277, 3278)  --Reed Smith tender firm
			AND (vPerson.Status = 1)


GO
