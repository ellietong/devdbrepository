SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vTEST_ClientSiteDataGridColumn]
AS

SELECT rcs.ClientSiteID, dg.AjaxSource, DataGridName, dg.DataGridID,  c.DataField
FROM RoleClientSite rcs
INNER JOIN RoleDataGridColumn rdgc ON rcs.RoleID = rdgc.RoleID
INNER JOIN DataGridColumn dgc ON dgc.DataGridColumnID = rdgc.DataGridColumnID
INNER JOIN [Column] c ON dgc.ColumnID = c.ColumnID
INNER JOIN DataGrid dg ON dgc.DataGridID = dg.DataGridID

GO
GRANT VIEW DEFINITION ON  [dbo].[vTEST_ClientSiteDataGridColumn] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vTEST_ClientSiteDataGridColumn] TO [Ligado_User]
GO
