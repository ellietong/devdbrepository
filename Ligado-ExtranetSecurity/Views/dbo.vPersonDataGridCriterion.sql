SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vPersonDataGridCriterion]
AS
SELECT        dbo.vPersonRoles.PersonID, dbo.RoleDataGridCriterion.DataGridCriterionID, dbo.vPersonRoles.ClientSiteID, dbo.vPersonRoles.ClientID, 
                         dbo.vPersonRoles.SiteID, SUM(dbo.vPersonRoles.CanEdit) AS CanEdit, SUM(dbo.vPersonRoles.CanAdd) AS CanAdd, SUM(dbo.vPersonRoles.CanDelete) AS CanDelete, dbo.DataGridCriterion.DataGridID, 
                         MAX(dbo.RoleDataGridCriterion.SortOrder) AS RDGCSortOrder, c.CriterionID, c.AjaxSource, c.ControlType, c.DataField, c.DataTextField, c.DataType, c.DataValueField,
						 c.DisplayName, c.ServiceMethod, c.ServiceObject
FROM            dbo.vPersonRoles INNER JOIN
                         dbo.RoleDataGridCriterion ON dbo.vPersonRoles.RoleID = dbo.RoleDataGridCriterion.RoleID INNER JOIN
                         dbo.DataGridCriterion ON dbo.RoleDataGridCriterion.DataGridCriterionID = dbo.DataGridCriterion.DataGridCriterionID
						 INNER JOIN [Criterion] c ON DataGridCriterion.CriterionID = c.CriterionID
GROUP BY
dbo.vPersonRoles.PersonID,
 dbo.RoleDataGridCriterion.DataGridCriterionID,
  dbo.vPersonRoles.ClientSiteID,
   dbo.vPersonRoles.ClientID, 
dbo.vPersonRoles.SiteID,
dbo.DataGridCriterion.DataGridID, 
c.CriterionID, 
c.AjaxSource,
 c.ControlType,
  c.DataField,
   c.DataTextField, 
   c.DataType,
   c.DataValueField,
c.DisplayName, 
c.ServiceMethod, 
c.ServiceObject




GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonDataGridCriterion] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonDataGridCriterion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonDataGridCriterion] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonDataGridCriterion] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Provides for a given PersonID, which criteria on applicable to specific ClientSiteIDs.', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridCriterion', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vPersonRoles"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 238
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RoleDataGridCriterion"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 110
               Right = 440
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DataGridCriterion"
            Begin Extent = 
               Top = 6
               Left = 478
               Bottom = 110
               Right = 661
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridCriterion', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGridCriterion', NULL, NULL
GO
