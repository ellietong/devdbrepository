SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[PersonFirmAddressClientSite]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite
GO
GRANT VIEW DEFINITION ON  [dbo].[PersonFirmAddressClientSite] TO [base_user]
GRANT SELECT ON  [dbo].[PersonFirmAddressClientSite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[PersonFirmAddressClientSite] TO [Ligado_User]
GRANT SELECT ON  [dbo].[PersonFirmAddressClientSite] TO [Ligado_User]
GO
