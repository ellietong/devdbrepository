SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblPermissionsRole]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.tblPermissionsRole

GO
