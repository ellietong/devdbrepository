SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vPersonRoleDefinition]
AS
SELECT     p1.PersonID, cs1.ClientSiteID, cs1.ClientID, cs1.SiteID, rpfacs1.RoleID, r1.RoleName, r1.RoleType, rpfacs1.CanEdit, rpfacs1.CanAdd, rpfacs1.CanDelete, 'Person Level' AS RoleLevel
FROM         dbo.Person AS p1 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs1 ON p1.PersonID = pfacs1.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs1 ON pfacs1.FirmAddressClientSiteID = facs1.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs1 ON facs1.ClientSiteID = cs1.ClientSiteID INNER JOIN
                      dbo.RolePersonFirmAddressClientSite AS rpfacs1 ON pfacs1.PersonFirmAddressClientSiteID = rpfacs1.PersonFirmAddressClientSiteID INNER JOIN
                      dbo.Role AS r1 ON rpfacs1.RoleID = r1.RoleID
UNION
SELECT     p2.PersonID, cs2.ClientSiteID, cs2.ClientID, cs2.SiteID, rfacs2.RoleID,  r2.RoleName, r2.RoleType, rfacs2.CanEdit,  rfacs2.CanAdd,  rfacs2.CanDelete,  'Firm Address Level' AS RoleLevel
FROM         dbo.Person AS p2 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs2 ON p2.PersonID = pfacs2.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs2 ON pfacs2.FirmAddressClientSiteID = facs2.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs2 ON facs2.ClientSiteID = cs2.ClientSiteID INNER JOIN
                      dbo.RoleFirmAddressClientSite AS rfacs2 ON facs2.FirmAddressClientSiteID = rfacs2.FirmAddressClientSiteID INNER JOIN
                      dbo.Role AS r2 ON rfacs2.RoleID = r2.RoleID
UNION
SELECT     p3.PersonID, cs3.ClientSiteID, cs3.ClientID, cs3.SiteID, rcs3.RoleID,    r3.RoleName, r3.RoleType, rcs3.CanEdit,    rcs3.CanAdd,    rcs3.CanDelete,    'Client Site Level' AS RoleLevel
FROM         dbo.Person AS p3 INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs3 ON p3.PersonID = pfacs3.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs3 ON pfacs3.FirmAddressClientSiteID = facs3.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs3 ON facs3.ClientSiteID = cs3.ClientSiteID INNER JOIN
                      dbo.RoleClientSite AS rcs3 ON cs3.ClientSiteID = rcs3.ClientSiteID INNER JOIN
                      dbo.Role AS r3 ON rcs3.RoleID = r3.RoleID


GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonRoleDefinition] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonRoleDefinition] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonRoleDefinition] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonRoleDefinition] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'VIEW', N'vPersonRoleDefinition', NULL, NULL
GO
