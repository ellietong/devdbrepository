SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[v_MOSS_GetAllRoles]
AS
--The Everyone role
SELECT     'Everyone' AS RoleName
UNION

--Roles from the role table
SELECT     replace(Role,',',' ') AS RoleName
FROM         dbo.tblRole
UNION

--Roles combined with Firms
SELECT DISTINCT replace(f.Firm,',',' ') + ' - ' + replace(r.Role,',',' ') AS RoleName
FROM         dbo.tblPermissionsRole AS p INNER JOIN
                      dbo.tblRole AS r ON p.RoleID = r.RoleID INNER JOIN
                      dbo.Firm AS f ON p.FirmID = f.FirmID
UNION

--Cases combined with Firms
SELECT DISTINCT replace(c.CaseName,',',' ') + ' - ' + replace(f.Firm,',',' ') AS RoleName
FROM		dbo.tblPermissionsRole AS p INNER JOIN
			dbo.Firm AS f ON p.FirmID = f.FirmID INNER JOIN
			dbo.tblCase AS c ON p.CaseID = c.CaseID
UNION

--Roles combined with cases
SELECT DISTINCT replace(c.CaseName,',',' ') + ' - ' + replace(r.Role,',',' ') AS RoleName
FROM         dbo.tblPermissionsRole AS p INNER JOIN
                      dbo.tblRole AS r ON p.RoleID = r.RoleID INNER JOIN
                      dbo.tblCase AS c ON p.CaseID = c.CaseID
UNION

--Firms
SELECT     replace(Firm,',',' ') AS RoleName
FROM         dbo.Firm
UNION

--Cases
SELECT     replace(CaseName,',',' ') AS RoleName
FROM         dbo.tblCase
UNION

--Firms Positions
SELECT DISTINCT replace(f.Firm,',',' ') + ' - ' + replace(pos.Position,',',' ') AS RoleName
FROM         dbo.vPerson AS p INNER JOIN
                      dbo.Firm AS f ON p.FirmID = f.FirmID INNER JOIN
                      dbo.tblPosition AS pos ON p.PositionID = pos.PositionID


GO
