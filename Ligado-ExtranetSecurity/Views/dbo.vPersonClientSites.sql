SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vPersonClientSites]
AS
SELECT DISTINCT 
                      p.PersonID AS PersonID, COALESCE (cs.ClientSiteID, 0) AS ClientSiteID, COALESCE (cs.ClientID, 0) AS ClientID, COALESCE (cs.SiteID, 0) AS SiteID, cs.Bucket, 
                      cs.Active & facs.Active & pfacs.Active AS Active, cs.ConnectionStringKey, cs.Notes, c.ClientName, c.ClientDisplayName, dbo.Site.SiteName, cs.SharePointUrl, ClientLegalName
FROM         dbo.Person AS p INNER JOIN
                      dbo.PersonFirmAddressClientSite AS pfacs ON p.PersonID = pfacs.PersonID INNER JOIN
                      dbo.FirmAddressClientSite AS facs ON pfacs.FirmAddressClientSiteID = facs.FirmAddressClientSiteID INNER JOIN
                      dbo.ClientSite AS cs ON facs.ClientSiteID = cs.ClientSiteID INNER JOIN
                      dbo.Client AS c ON c.ClientID = cs.ClientID INNER JOIN
                      dbo.Site ON cs.SiteID = dbo.Site.SiteID
UNION
SELECT DISTINCT 0 AS PersonID, ClientSiteID, cs.ClientID, cs.SiteID, Bucket, Active, ConnectionStringKey, cs.Notes, c.ClientName, c.ClientDisplayName, s.SiteName, cs.SharePointUrl, ClientLegalName
FROM         ClientSite cs INNER JOIN
                      Client c ON cs.ClientID = c.ClientID INNER JOIN
                      Site s ON cs.SiteID = s.SiteID


GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonClientSites] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonClientSites] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonClientSites] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonClientSites] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Provides for a specific PersonID the ClientSites to which they have access, including bucket and connectionstring information.', 'SCHEMA', N'dbo', 'VIEW', N'vPersonClientSites', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[50] 4[25] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[21] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1[56] 3) )"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[32] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3) )"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 2
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2100
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 3210
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vPersonClientSites', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonClientSites', NULL, NULL
GO
