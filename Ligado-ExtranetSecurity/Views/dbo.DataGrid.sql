SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[DataGrid]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.DataGrid
GO
GRANT VIEW DEFINITION ON  [dbo].[DataGrid] TO [base_user]
GRANT SELECT ON  [dbo].[DataGrid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DataGrid] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DataGrid] TO [Ligado_User]
GO
