SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[DataGridLocation]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.DataGridLocation
GO
GRANT VIEW DEFINITION ON  [dbo].[DataGridLocation] TO [base_user]
GRANT SELECT ON  [dbo].[DataGridLocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DataGridLocation] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DataGridLocation] TO [Ligado_User]
GO
