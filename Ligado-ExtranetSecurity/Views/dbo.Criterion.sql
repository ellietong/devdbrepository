SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Criterion]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.Criterion
GO
GRANT VIEW DEFINITION ON  [dbo].[Criterion] TO [base_user]
GRANT SELECT ON  [dbo].[Criterion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Criterion] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Criterion] TO [Ligado_User]
GO
