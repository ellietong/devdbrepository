SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblPermissionsRole_OLD]
AS
SELECT     *
FROM         [FBSecurity].dbo.tblPermissionsRole 

GO
GRANT SELECT ON  [dbo].[tblPermissionsRole_OLD] TO [Ligado_User]
GO
