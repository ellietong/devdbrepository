SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vADMIN_PersonSearch]
AS
SELECT     dbo.aspnet_Membership.ApplicationId, dbo.aspnet_Membership.Password, dbo.aspnet_Membership.PasswordFormat, 
                      dbo.aspnet_Membership.PasswordSalt, dbo.aspnet_Membership.MobilePIN, dbo.aspnet_Membership.Email, dbo.aspnet_Membership.LoweredEmail, 
                      dbo.aspnet_Membership.PasswordQuestion, dbo.aspnet_Membership.PasswordAnswer, dbo.aspnet_Membership.IsApproved, dbo.aspnet_Membership.IsLockedOut | sharePointPerson.IsLockedOut AS IsLockedOut, 
                      dbo.aspnet_Membership.CreateDate, dbo.aspnet_Membership.LastLoginDate, ---1 AS UserId,
					  --Take the oldest available LastPasswordChangedDate
					  CASE WHEN sharePointPerson.LastPasswordChangedDate IS NULL OR sharePointPerson.LastPasswordChangedDate > dbo.aspnet_Membership.LastPasswordChangedDate THEN
					  dbo.aspnet_Membership.LastPasswordChangedDate ELSE sharePointPerson.LastPasswordChangedDate END AS LastPasswordChangedDate, 
                      dbo.aspnet_Membership.LastLockoutDate, dbo.aspnet_Membership.FailedPasswordAttemptCount, dbo.aspnet_Membership.FailedPasswordAttemptWindowStart, 
                      dbo.aspnet_Membership.FailedPasswordAnswerAttemptCount, dbo.aspnet_Membership.FailedPasswordAnswerAttemptWindowStart, 
                      dbo.aspnet_Membership.Comment, dbo.Person.MembershipID, dbo.Person.PrefixID, dbo.Person.FirstName, dbo.Person.MiddleName, 
                      dbo.Person.LastName, dbo.Person.SuffixID, dbo.Person.GenderID, dbo.Person.DirectPhone, dbo.Person.DirectFax, dbo.Person.Email AS Expr1, 
                      dbo.Person.Password AS Expr2, CONVERT(BIT, COALESCE(dbo.Person.PasswordReset, 0)) AS PasswordReset, dbo.Person.PasswordQuestionID, dbo.Person.PasswordAnswer AS Expr3, 
                      dbo.Person.TermsAcceptedDate, dbo.Person.Notes, dbo.Person.PersonID, UserID, Gender, f.FirmDisplayName,
					  fa.FirmID, facs.ClientSiteID
					  FROM         dbo.aspnet_Membership INNER JOIN
                      dbo.Person ON dbo.aspnet_Membership.Email = dbo.Person.Email
					  LEFT JOIN [Ligado-ExtranetSecurity].dbo.vPerson sharePointPerson ON dbo.Person.Email = sharePointPerson.Email
					  LEFT JOIN Gender g ON dbo.Person.GenderID = g.GenderID
					  LEFT JOIN PersonFirmAddressClientSite pfacs ON dbo.Person.PersonID = pfacs.PersonID
					  LEFT JOIN FirmAddressClientSite facs ON pfacs.FirmAddressClientSiteID = facs.FirmAddressClientSiteID
					  LEFT JOIN FirmAddress fa ON facs.FirmAddressID = fa.FirmAddressID
					  LEFT JOIN Firm f ON fa.FirmID = f.FirmID







GO
GRANT SELECT ON  [dbo].[vADMIN_PersonSearch] TO [Ligado_User]
GO
