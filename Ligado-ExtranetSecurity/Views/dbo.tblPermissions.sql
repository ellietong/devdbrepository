SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblPermissions]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.tblPermissions

GO
