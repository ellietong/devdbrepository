SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[DetailsView]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.DetailsView
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailsView] TO [base_user]
GRANT SELECT ON  [dbo].[DetailsView] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DetailsView] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DetailsView] TO [Ligado_User]
GO
