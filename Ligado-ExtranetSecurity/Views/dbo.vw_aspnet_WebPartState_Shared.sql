SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

  CREATE VIEW [dbo].[vw_aspnet_WebPartState_Shared]
  AS SELECT [dbo].[aspnet_PersonalizationAllUsers].[PathId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationAllUsers].[PageSettings]), [dbo].[aspnet_PersonalizationAllUsers].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationAllUsers]
  
GO
GRANT SELECT ON  [dbo].[vw_aspnet_WebPartState_Shared] TO [aspnet_Personalization_ReportingAccess]
GRANT VIEW DEFINITION ON  [dbo].[vw_aspnet_WebPartState_Shared] TO [base_user]
GRANT SELECT ON  [dbo].[vw_aspnet_WebPartState_Shared] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vw_aspnet_WebPartState_Shared] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vw_aspnet_WebPartState_Shared] TO [Ligado_User]
GO
