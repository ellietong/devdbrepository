SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPersonDetailsView]
AS
SELECT     dbo.vPersonRoles.PersonID, dbo.RoleDetailsView.DetailsViewID, dbo.vPersonRoles.ClientSiteID, dbo.vPersonRoles.ClientID, dbo.vPersonRoles.SiteID, 
                      CASE WHEN SUM(CONVERT(int, dbo.vPersonRoles.CanEdit)) > 0 THEN 1 ELSE 0 END AS CanEdit, 
					  CASE WHEN SUM(CONVERT(int, dbo.vPersonRoles.CanAdd)) > 0 THEN 1 ELSE 0 END AS CanAdd, 
					  CASE WHEN SUM(CONVERT(int, dbo.vPersonRoles.CanDelete)) > 0 THEN 1 ELSE 0 END AS CanDelete, dbo.DetailsView.DetailsViewName, dbo.DetailsView.DisplayName, 
                      dbo.DetailsView.ServiceObject, dbo.DetailsView.ServiceMethod, dbo.DetailsView.DataObjectTypeName, dbo.DetailsView.SelectProc, dbo.DetailsView.InsertProc, 
                      dbo.DetailsView.UpdateProc, dbo.DetailsView.DeleteProc, dbo.DetailsView.Notes, dbo.DetailsView.EmptyDataText
FROM         dbo.RoleDetailsView INNER JOIN
                      dbo.vPersonRoles ON dbo.RoleDetailsView.RoleID = dbo.vPersonRoles.RoleID INNER JOIN
                      dbo.DetailsView ON dbo.RoleDetailsView.DetailsViewID = dbo.DetailsView.DetailsViewID
GROUP BY dbo.vPersonRoles.PersonID, dbo.RoleDetailsView.DetailsViewID, dbo.vPersonRoles.ClientSiteID, dbo.vPersonRoles.ClientID, dbo.vPersonRoles.SiteID, 
		 dbo.DetailsView.DetailsViewName, dbo.DetailsView.DisplayName, 
         dbo.DetailsView.ServiceObject, dbo.DetailsView.ServiceMethod, dbo.DetailsView.DataObjectTypeName, dbo.DetailsView.SelectProc, dbo.DetailsView.InsertProc, 
         dbo.DetailsView.UpdateProc, dbo.DetailsView.DeleteProc, dbo.DetailsView.Notes, dbo.DetailsView.EmptyDataText
GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonDetailsView] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonDetailsView] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonDetailsView] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonDetailsView] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'For a given PersonID, provides which DetailsViews to which they have access for each ClientSite as well as the definitions and effective permissions for those DetailsViews.', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsView', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "RoleDetailsView"
            Begin Extent = 
               Top = 6
               Left = 444
               Bottom = 125
               Right = 619
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vPersonRoles"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 218
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DetailsView"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 114
               Right = 422
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      ', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsView', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsView', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDetailsView', NULL, NULL
GO
