SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[QueryStringParameter]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.QueryStringParameter
GO
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameter] TO [base_user]
GRANT SELECT ON  [dbo].[QueryStringParameter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameter] TO [Ligado_User]
GRANT SELECT ON  [dbo].[QueryStringParameter] TO [Ligado_User]
GO
