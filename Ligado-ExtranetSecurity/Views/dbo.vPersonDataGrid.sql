SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[vPersonDataGrid]
AS
SELECT dbo.vPersonRoles.PersonID, dbo.RoleDataGrid.DataGridID, dbo.vPersonRoles.ClientSiteID, dbo.vPersonRoles.ClientID, dbo.vPersonRoles.SiteID, 
                      CASE WHEN SUM(CONVERT(INT, dbo.vPersonRoles.CanEdit)) > 0 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS CanEdit, 
					  CASE WHEN SUM(CONVERT(INT, dbo.vPersonRoles.CanAdd)) > 0 THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END AS CanAdd, 
					  CASE WHEN SUM(CONVERT(INT, dbo.vPersonRoles.CanDelete)) > 0 THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END AS CanDelete,
						  dbo.DataGrid.AjaxSource, dbo.DataGrid.DataGridName, 
                         dbo.DataGrid.DisplayName, dbo.DataGrid.ServiceObject, dbo.DataGrid.ServiceMethod, dbo.DataGrid.DataObjectTypeName, dbo.DataGrid.Notes, 
                         dbo.DataGrid.UpdateProc, dbo.DataGrid.DeleteProc, dbo.DataGrid.InsertProc, dbo.DataGrid.SelectProc, dbo.DataGrid.PageSize, 
                         dbo.DataGrid.DataGridTypeID, dbo.DataGrid.EmptyDataText, dbo.DataGrid.EnableRowSelection, dbo.DataGrid.Include, dbo.DataGridType.Type,
						 EnableExport, RoleName
FROM            dbo.vPersonRoles INNER JOIN
                         dbo.RoleDataGrid ON dbo.vPersonRoles.RoleID = dbo.RoleDataGrid.RoleID INNER JOIN
                         dbo.DataGrid ON dbo.RoleDataGrid.DataGridID = dbo.DataGrid.DataGridID INNER JOIN
                         dbo.DataGridType ON dbo.DataGrid.DataGridTypeID = dbo.DataGridType.DataGridTypeID
GROUP BY dbo.vPersonRoles.PersonID, dbo.RoleDataGrid.DataGridID, dbo.vPersonRoles.ClientSiteID, dbo.vPersonRoles.ClientID, dbo.vPersonRoles.SiteID, 
		dbo.DataGrid.AjaxSource, dbo.DataGrid.DataGridName, 
        dbo.DataGrid.DisplayName, dbo.DataGrid.ServiceObject, dbo.DataGrid.ServiceMethod, dbo.DataGrid.DataObjectTypeName, dbo.DataGrid.Notes, 
        dbo.DataGrid.UpdateProc, dbo.DataGrid.DeleteProc, dbo.DataGrid.InsertProc, dbo.DataGrid.SelectProc, dbo.DataGrid.PageSize, 
        dbo.DataGrid.DataGridTypeID, dbo.DataGrid.EmptyDataText, dbo.DataGrid.EnableRowSelection, dbo.DataGrid.Include, dbo.DataGridType.Type,
		EnableExport, RoleName





GO
GRANT VIEW DEFINITION ON  [dbo].[vPersonDataGrid] TO [base_user]
GRANT SELECT ON  [dbo].[vPersonDataGrid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPersonDataGrid] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vPersonDataGrid] TO [Ligado_User]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'For a given PersonID, provides which DataGrids to which they have access for each ClientSite as well as the definitions and effective permissions for those DataGrids.', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vPersonRoles"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 216
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RoleDataGrid"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 155
               Right = 399
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DataGrid"
            Begin Extent = 
               Top = 6
               Left = 437
               Bottom = 358
               Right = 787
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DataGridType"
            Begin Extent = 
               Top = 6
               Left = 825
               Bottom = 172
               Right = 997
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3570
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or =', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'(([vPersonDataGrid].[PersonID]="5")) AND ([vPersonDataGrid].[ClientSiteID]="9")', 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanAdd'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanAdd'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanAdd'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanAdd'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanDelete'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanDelete'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanDelete'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanDelete'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanEdit'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanEdit'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanEdit'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanEdit'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'ClientID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'ClientID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'ClientID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'ClientID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'ClientID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'DataGridID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'DataGridID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'DataGridID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'DataGridID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'DataGridID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'PersonID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'PersonID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'PersonID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'PersonID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'SiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'SiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'SiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'SiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vPersonDataGrid', 'COLUMN', N'SiteID'
GO
