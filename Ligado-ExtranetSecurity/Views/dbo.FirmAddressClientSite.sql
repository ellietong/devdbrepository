SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[FirmAddressClientSite]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.FirmAddressClientSite
GO
GRANT VIEW DEFINITION ON  [dbo].[FirmAddressClientSite] TO [base_user]
GRANT SELECT ON  [dbo].[FirmAddressClientSite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[FirmAddressClientSite] TO [Ligado_User]
GRANT SELECT ON  [dbo].[FirmAddressClientSite] TO [Ligado_User]
GO
