SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[Prefix]
AS
SELECT     *
FROM         WEBSQL.[Ligado-ExtranetSecurity].dbo.Prefix
GO
GRANT VIEW DEFINITION ON  [dbo].[Prefix] TO [base_user]
GRANT SELECT ON  [dbo].[Prefix] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Prefix] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Prefix] TO [Ligado_User]
GO
