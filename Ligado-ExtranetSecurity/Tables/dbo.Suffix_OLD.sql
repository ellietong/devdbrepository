CREATE TABLE [dbo].[Suffix_OLD]
(
[SuffixID] [int] NOT NULL IDENTITY(1, 1),
[Suffix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Suffix_OLD] ADD CONSTRAINT [PK_tblSuffix] PRIMARY KEY CLUSTERED  ([SuffixID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Suffix_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Suffix_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Suffix_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Suffix_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Suffix_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Suffix_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Suffix_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lookup table for suffixes', 'SCHEMA', N'dbo', 'TABLE', N'Suffix_OLD', NULL, NULL
GO
