CREATE TABLE [dbo].[RoleFirmAddressClientSite]
(
[RoleFirmAddressClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[FirmAddressClientSiteID] [bigint] NOT NULL,
[CanEdit] [bit] NOT NULL,
[CanAdd] [bit] NOT NULL,
[CanDelete] [bit] NOT NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleFirmAddressClientSite] ADD CONSTRAINT [PK_tblFirmAddressCaseRoleNew] PRIMARY KEY CLUSTERED  ([RoleFirmAddressClientSiteID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleFirmAddressClientSite] TO [base_user]
GRANT SELECT ON  [dbo].[RoleFirmAddressClientSite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleFirmAddressClientSite] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleFirmAddressClientSite] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleFirmAddressClientSite] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleFirmAddressClientSite] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleFirmAddressClientSite] TO [Ligado_User]
GO
