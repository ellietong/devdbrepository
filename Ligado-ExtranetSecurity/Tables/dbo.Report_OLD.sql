CREATE TABLE [dbo].[Report_OLD]
(
[ReportID] [bigint] NOT NULL IDENTITY(1, 1),
[ParentReportID] [bigint] NULL,
[ReportName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportLevel] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Report_OLD] ADD CONSTRAINT [PK_tblReport] PRIMARY KEY CLUSTERED  ([ReportID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblReport_tblReport] ON [dbo].[Report_OLD] ([ParentReportID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Report_OLD] ADD CONSTRAINT [FK_tblReport_tblReport] FOREIGN KEY ([ParentReportID]) REFERENCES [dbo].[Report_OLD] ([ReportID])
GO
GRANT VIEW DEFINITION ON  [dbo].[Report_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Report_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Report_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Report_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Report_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Report_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Report_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Report_OLD', NULL, NULL
GO
