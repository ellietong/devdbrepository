CREATE TABLE [dbo].[zPersonFirmAddressClientSite]
(
[PersonFirmAddressClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[PersonID] [bigint] NOT NULL,
[FirmAddressClientSiteID] [bigint] NOT NULL,
[Active] [bit] NOT NULL CONSTRAINT [DF_PersonFirmAddressClientSite_Active] DEFAULT ((0)),
[IsPrimary] [bit] NOT NULL CONSTRAINT [DF_PersonFirmAddressClientSite_IsPrimary] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zPersonFirmAddressClientSite] ADD CONSTRAINT [PK_tblPersonFirmAddressCase] PRIMARY KEY CLUSTERED  ([PersonFirmAddressClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPersonFirmAddressCase_tblFirmAddressCase] ON [dbo].[zPersonFirmAddressClientSite] ([FirmAddressClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPersonFirmAddressCase_tblPerson] ON [dbo].[zPersonFirmAddressClientSite] ([PersonID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zPersonFirmAddressClientSite] ADD CONSTRAINT [FK_tblPersonFirmAddressCase_tblFirmAddressCase] FOREIGN KEY ([FirmAddressClientSiteID]) REFERENCES [dbo].[zFirmAddressClientSite] ([FirmAddressClientSiteID])
GO
ALTER TABLE [dbo].[zPersonFirmAddressClientSite] ADD CONSTRAINT [FK_tblPersonFirmAddressCase_tblPerson] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[zPerson] ([PersonID])
GO
GRANT VIEW DEFINITION ON  [dbo].[zPersonFirmAddressClientSite] TO [base_user]
GRANT SELECT ON  [dbo].[zPersonFirmAddressClientSite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zPersonFirmAddressClientSite] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zPersonFirmAddressClientSite] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zPersonFirmAddressClientSite] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zPersonFirmAddressClientSite] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zPersonFirmAddressClientSite] TO [Ligado_User]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'Active'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'Active'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'Active'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'Active'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'Active'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=2700
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'PersonID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'PersonID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'PersonID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'PersonID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPersonFirmAddressClientSite', 'COLUMN', N'PersonID'
GO
