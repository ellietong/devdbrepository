CREATE TABLE [dbo].[zRoleFirmAddressClientSite]
(
[RoleFirmAddressClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[FirmAddressClientSiteID] [bigint] NOT NULL,
[CanEdit] [bit] NOT NULL CONSTRAINT [DF_tblRoleFirmAddressCase_CanEdit] DEFAULT ((0)),
[CanAdd] [bit] NOT NULL CONSTRAINT [DF_tblRoleFirmAddressCase_CanAdd] DEFAULT ((0)),
[CanDelete] [bit] NOT NULL CONSTRAINT [DF_tblRoleFirmAddressCase_CanDelete] DEFAULT ((0)),
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[zRoleFirmAddressClientSite] ADD CONSTRAINT [PK_tblFirmAddressCaseRole] PRIMARY KEY CLUSTERED  ([RoleFirmAddressClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblFirmAddressCaseRole_tblFirmAddressCase] ON [dbo].[zRoleFirmAddressClientSite] ([FirmAddressClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblFirmAddressCaseRole_tblRole] ON [dbo].[zRoleFirmAddressClientSite] ([RoleID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zRoleFirmAddressClientSite] ADD CONSTRAINT [FK_tblFirmAddressCaseRole_tblFirmAddressCase] FOREIGN KEY ([FirmAddressClientSiteID]) REFERENCES [dbo].[zFirmAddressClientSite] ([FirmAddressClientSiteID])
GO
ALTER TABLE [dbo].[zRoleFirmAddressClientSite] ADD CONSTRAINT [FK_tblFirmAddressCaseRole_tblRole] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role_OLD] ([RoleID])
GO
GRANT VIEW DEFINITION ON  [dbo].[zRoleFirmAddressClientSite] TO [base_user]
GRANT SELECT ON  [dbo].[zRoleFirmAddressClientSite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zRoleFirmAddressClientSite] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zRoleFirmAddressClientSite] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zRoleFirmAddressClientSite] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zRoleFirmAddressClientSite] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zRoleFirmAddressClientSite] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'([RoleFirmAddressClientSite].[RoleID]="23")', 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanAdd'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanAdd'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanAdd'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanAdd'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanDelete'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanDelete'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanDelete'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanDelete'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanEdit'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanEdit'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanEdit'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanEdit'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=2700
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'Notes'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'Notes'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'Notes'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'Notes'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zRoleFirmAddressClientSite', 'COLUMN', N'RoleID'
GO
