CREATE TABLE [dbo].[aspnet_PersonalizationAllUsers]
(
[PathId] [uniqueidentifier] NOT NULL,
[PageSettings] [image] NOT NULL,
[LastUpdatedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers] ADD CONSTRAINT [PK__aspnet_P__CD67DC595A4F643B] PRIMARY KEY CLUSTERED  ([PathId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__aspnet_Pe__PathI__5C37ACAD] ON [dbo].[aspnet_PersonalizationAllUsers] ([PathId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers] ADD CONSTRAINT [FK__aspnet_Pe__PathI__5C37ACAD] FOREIGN KEY ([PathId]) REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_PersonalizationAllUsers] TO [base_user]
GRANT SELECT ON  [dbo].[aspnet_PersonalizationAllUsers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[aspnet_PersonalizationAllUsers] TO [Ligado_User]
GRANT SELECT ON  [dbo].[aspnet_PersonalizationAllUsers] TO [Ligado_User]
GRANT INSERT ON  [dbo].[aspnet_PersonalizationAllUsers] TO [Ligado_User]
GRANT DELETE ON  [dbo].[aspnet_PersonalizationAllUsers] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[aspnet_PersonalizationAllUsers] TO [Ligado_User]
GO
