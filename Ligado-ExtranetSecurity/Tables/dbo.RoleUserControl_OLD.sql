CREATE TABLE [dbo].[RoleUserControl_OLD]
(
[RoleUserControlID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[UserControlID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleUserControl_OLD] ADD CONSTRAINT [PK_tblRoleUserControl] PRIMARY KEY CLUSTERED  ([RoleUserControlID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleUserControlExclude_tblRole] ON [dbo].[RoleUserControl_OLD] ([RoleID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleUserControlExclude_tblUserControl] ON [dbo].[RoleUserControl_OLD] ([UserControlID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleUserControl_OLD] ADD CONSTRAINT [FK_tblRoleUserControlExclude_tblRole] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role_OLD] ([RoleID])
GO
ALTER TABLE [dbo].[RoleUserControl_OLD] ADD CONSTRAINT [FK_tblRoleUserControlExclude_tblUserControl] FOREIGN KEY ([UserControlID]) REFERENCES [dbo].[UserControl_OLD] ([UserControlID])
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleUserControl_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RoleUserControl_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleUserControl_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleUserControl_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleUserControl_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleUserControl_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleUserControl_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'RoleUserControl_OLD', NULL, NULL
GO
