CREATE TABLE [dbo].[aspnet_PersonalizationPerUser]
(
[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__aspnet_Perso__Id__60FC61CA] DEFAULT (newid()),
[PathId] [uniqueidentifier] NULL,
[UserId] [uniqueidentifier] NULL,
[PageSettings] [image] NOT NULL,
[LastUpdatedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] ADD CONSTRAINT [PK__aspnet_P__3214EC065F141958] PRIMARY KEY NONCLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__aspnet_Pe__PathI__61F08603] ON [dbo].[aspnet_PersonalizationPerUser] ([PathId]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_PersonalizationPerUser_index1] ON [dbo].[aspnet_PersonalizationPerUser] ([PathId], [UserId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__aspnet_Pe__UserI__62E4AA3C] ON [dbo].[aspnet_PersonalizationPerUser] ([UserId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [aspnet_PersonalizationPerUser_ncindex2] ON [dbo].[aspnet_PersonalizationPerUser] ([UserId], [PathId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] ADD CONSTRAINT [FK__aspnet_Pe__PathI__61F08603] FOREIGN KEY ([PathId]) REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] ADD CONSTRAINT [FK__aspnet_Pe__UserI__62E4AA3C] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_PersonalizationPerUser] TO [base_user]
GRANT SELECT ON  [dbo].[aspnet_PersonalizationPerUser] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[aspnet_PersonalizationPerUser] TO [Ligado_User]
GRANT SELECT ON  [dbo].[aspnet_PersonalizationPerUser] TO [Ligado_User]
GRANT INSERT ON  [dbo].[aspnet_PersonalizationPerUser] TO [Ligado_User]
GRANT DELETE ON  [dbo].[aspnet_PersonalizationPerUser] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[aspnet_PersonalizationPerUser] TO [Ligado_User]
GO
