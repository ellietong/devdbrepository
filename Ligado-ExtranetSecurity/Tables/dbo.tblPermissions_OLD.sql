CREATE TABLE [dbo].[tblPermissions_OLD]
(
[PermissionID] [int] NOT NULL IDENTITY(1, 1),
[PersonID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[InvoiceContact] [bit] NULL CONSTRAINT [DF_tblPermissions_InvoiceContact] DEFAULT ((0)),
[ComplaintContact] [bit] NULL CONSTRAINT [DF_tblPermissions_ComplaintContact] DEFAULT ((0)),
[NoFilters] [bit] NOT NULL CONSTRAINT [DF_tblPermissions_NoFilters] DEFAULT ((0)),
[SecurityContact] [bit] NOT NULL CONSTRAINT [DF_tblPermissions_SecurityContact] DEFAULT ((0)),
[TenderContactTo] [bit] NOT NULL CONSTRAINT [DF_tblPermissions_TenderContactTo] DEFAULT ((0)),
[TenderContactCC] [bit] NOT NULL CONSTRAINT [DF_tblPermissions_TenderContact] DEFAULT ((0)),
[TenderContactBCC] [bit] NOT NULL CONSTRAINT [DF_tblPermissions_TenderContactBCC] DEFAULT ((0)),
[TenderContactAlwaysCC] [bit] NOT NULL CONSTRAINT [DF_tblPermissions_TenderContactCC1] DEFAULT ((0)),
[TenderContactAlwaysBCC] [bit] NOT NULL CONSTRAINT [DF_tblPermissions_TenderContactBCC1] DEFAULT ((0)),
[PrimaryContact] [bit] NOT NULL CONSTRAINT [DF_tblPermissions_PrimaryContact] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPermissions_OLD] ADD CONSTRAINT [PK_tblPermissions] PRIMARY KEY CLUSTERED  ([PermissionID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPermissions_OLD] TO [LigadoSecurity]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'(PersonID=3586)', 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', N'[tblPermissions].[PermissionID], [tblPermissions].[PersonID]', 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'CaseID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'CaseID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'CaseID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'CaseID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'CaseID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'ComplaintContact'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'ComplaintContact'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'ComplaintContact'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'ComplaintContact'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'ComplaintContact'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'InvoiceContact'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'InvoiceContact'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'InvoiceContact'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'InvoiceContact'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'InvoiceContact'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'NoFilters'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'NoFilters'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'NoFilters'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'NoFilters'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'NoFilters'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PermissionID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PermissionID'
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PermissionID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PermissionID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PermissionID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PrimaryContact'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PrimaryContact'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PrimaryContact'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PrimaryContact'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'PrimaryContact'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'SecurityContact'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'SecurityContact'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'SecurityContact'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'SecurityContact'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'SecurityContact'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'TenderContactBCC'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'TenderContactBCC'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'TenderContactBCC'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'TenderContactBCC'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'TenderContactBCC'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'TenderContactCC'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'TenderContactCC'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'TenderContactCC'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'TenderContactCC'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblPermissions_OLD', 'COLUMN', N'TenderContactCC'
GO
