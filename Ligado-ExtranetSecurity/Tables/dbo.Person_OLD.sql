CREATE TABLE [dbo].[Person_OLD]
(
[PersonID] [bigint] NOT NULL IDENTITY(1, 1),
[MembershipID] [uniqueidentifier] NULL,
[PrefixID] [int] NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuffixID] [int] NULL,
[GenderID] [int] NULL,
[DirectPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirectFax] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PasswordReset] [bit] NULL CONSTRAINT [DF_Person_PasswordReset] DEFAULT ((1)),
[PasswordQuestionID] [int] NULL,
[PasswordAnswer] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TermsAcceptedDate] [datetime] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Person_OLD] ADD CONSTRAINT [PK_tblPersonNew] PRIMARY KEY CLUSTERED  ([PersonID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Person_OLD] ADD CONSTRAINT [FK_Person_Gender] FOREIGN KEY ([GenderID]) REFERENCES [dbo].[Gender_OLD] ([GenderID])
GO
ALTER TABLE [dbo].[Person_OLD] ADD CONSTRAINT [FK_Person_Prefix] FOREIGN KEY ([PrefixID]) REFERENCES [dbo].[Prefix_OLD] ([PrefixID])
GO
ALTER TABLE [dbo].[Person_OLD] ADD CONSTRAINT [FK_Person_Suffix] FOREIGN KEY ([SuffixID]) REFERENCES [dbo].[Suffix_OLD] ([SuffixID])
GO
GRANT VIEW DEFINITION ON  [dbo].[Person_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Person_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Person_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Person_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Person_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Person_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Person_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains user information - Name, Phone, email, etc.  All other role permissions are based on the PersonID in this table.', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'User''s fax (unused?)', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'DirectFax'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User''s phone (unused?)', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'DirectPhone'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Email'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Email'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Email'
GO
DECLARE @xp int
SELECT @xp=2955
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Email'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User''s email address - used to link to aspnet_ membership tables.', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Email'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Email'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'FirstName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'FirstName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'FirstName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'FirstName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User''s first name', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'FirstName'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'FirstName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lookup for Gender', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'GenderID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'LastName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'LastName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'LastName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'LastName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User''s last name', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'LastName'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'LastName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Currently used by Admin Panel TODO: pull from aspnet_ tables instead of from tblPerson', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'MembershipID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User''s middle name', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'MiddleName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'KCIC internal notes about the user.', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Password'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Password'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Password'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Password'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Hashed and salted password in aspnet_ membership table actually used for authentication. This is only used during user imports.  DOES NOT REFLECT USERS CURRENT PASSWORD!!!', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Password'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'Password'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordAnswer'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordAnswer'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordAnswer'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordAnswer'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED - no longer use questions to reset users password (password reset email sent instead)', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordAnswer'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordAnswer'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordQuestionID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordQuestionID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordQuestionID'
GO
DECLARE @xp int
SELECT @xp=2355
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordQuestionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED - no longer use questions to reset users password (password reset email sent instead)', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordQuestionID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordQuestionID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordReset'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordReset'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordReset'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordReset'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the user is required to reset their password upon their next login.', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordReset'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PasswordReset'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PersonID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PersonID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lookup for prefix', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'PrefixID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lookup for Suffix', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'SuffixID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'TermsAcceptedDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'TermsAcceptedDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'TermsAcceptedDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'TermsAcceptedDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date the user accepted the terms of service to the site.  If null, they will be accepted to accept them upon their next login.', 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'TermsAcceptedDate'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Person_OLD', 'COLUMN', N'TermsAcceptedDate'
GO
