CREATE TABLE [dbo].[tblPosition_OLD]
(
[PositionID] [int] NOT NULL IDENTITY(1, 1),
[Position] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPosition_OLD] ADD CONSTRAINT [PK_tblPosition] PRIMARY KEY CLUSTERED  ([PositionID]) ON [PRIMARY]
GO
