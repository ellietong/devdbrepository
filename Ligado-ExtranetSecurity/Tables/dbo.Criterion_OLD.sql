CREATE TABLE [dbo].[Criterion_OLD]
(
[CriterionID] [bigint] NOT NULL IDENTITY(1, 1),
[DisplayName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataField] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ControlType] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataValueField] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataTextField] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceObject] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceMethod] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjaxSource] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Criterion_OLD] ADD CONSTRAINT [PK_tblDataGridCriterion] PRIMARY KEY CLUSTERED  ([CriterionID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Criterion_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Criterion_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Criterion_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Criterion_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Criterion_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Criterion_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Criterion_OLD] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Search criteria definitions for DataGrids', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Select2 WebAPI endpoint to populate dropdowns', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ControlType'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ControlType'
GO
DECLARE @xp smallint
SELECT @xp=5
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ControlType'
GO
DECLARE @xp smallint
SELECT @xp=11940
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ControlType'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ControlType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type of control to render for the criterion (DropDown, TextBox)', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ControlType'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ControlType'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'CriterionID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'CriterionID'
GO
DECLARE @xp smallint
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'CriterionID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'CriterionID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'CriterionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'CriterionID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'CriterionID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp smallint
SELECT @xp=2550
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataField'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Underlying SQL column name that the criterion queries against (for dropdowns, this should be an ID field)', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp smallint
SELECT @xp=2430
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataTextField'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Text field to which dropdowns should be bound', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp smallint
SELECT @xp=1815
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Data type UNUSED?', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp smallint
SELECT @xp=2745
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataValueField'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ID field to which dropdowns should be bound', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp smallint
SELECT @xp=2520
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DisplayName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Label for the search criterion', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceMethod'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp smallint
SELECT @xp=4095
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceObject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Criterion_OLD', 'COLUMN', N'ServiceObject'
GO
