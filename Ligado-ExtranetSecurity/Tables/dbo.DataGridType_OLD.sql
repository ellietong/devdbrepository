CREATE TABLE [dbo].[DataGridType_OLD]
(
[DataGridTypeID] [int] NOT NULL IDENTITY(1, 1),
[Type] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataGridType_OLD] ADD CONSTRAINT [PK_DataGridType] PRIMARY KEY CLUSTERED  ([DataGridTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_DataGridType_DataGridType] ON [dbo].[DataGridType_OLD] ([DataGridTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataGridType_OLD] ADD CONSTRAINT [FK_DataGridType_DataGridType] FOREIGN KEY ([DataGridTypeID]) REFERENCES [dbo].[DataGridType_OLD] ([DataGridTypeID])
GO
GRANT VIEW DEFINITION ON  [dbo].[DataGridType_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[DataGridType_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DataGridType_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DataGridType_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[DataGridType_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[DataGridType_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[DataGridType_OLD] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type table to prevent issues where grid names matter because they were referenced explicitly in code.', 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp smallint
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'Type'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'Type'
GO
DECLARE @xp smallint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'Type'
GO
DECLARE @xp smallint
SELECT @xp=1725
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'Type'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'Type'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridType_OLD', 'COLUMN', N'Type'
GO
