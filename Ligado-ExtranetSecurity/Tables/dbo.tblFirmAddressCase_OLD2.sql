CREATE TABLE [dbo].[tblFirmAddressCase_OLD2]
(
[FirmAddressCaseID] [int] NOT NULL IDENTITY(1, 1),
[FirmAddressID] [int] NULL,
[CaseID] [int] NULL,
[NoFilters] [bit] NOT NULL CONSTRAINT [DF_tblFirmAddressCase_NoFilters] DEFAULT ((0)),
[PayeeAddress] [bit] NOT NULL CONSTRAINT [DF_tblFirmAddressCase_PayeeAddress] DEFAULT ((0)),
[SettlementACHRoutingNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementACHAccountNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementACHRemittanceEmail] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFirmAddressCase_OLD2] ADD CONSTRAINT [PK_tblFirmAddressCase_1] PRIMARY KEY CLUSTERED  ([FirmAddressCaseID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblFirmAddressCase_OLD2] TO [LigadoSecurity]
GO
