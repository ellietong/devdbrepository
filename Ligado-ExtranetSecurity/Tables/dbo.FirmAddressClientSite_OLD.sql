CREATE TABLE [dbo].[FirmAddressClientSite_OLD]
(
[FirmAddressClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[FirmAddressID] [bigint] NOT NULL,
[ClientSiteID] [bigint] NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmAddressClientSite_OLD] ADD CONSTRAINT [PK_tblFirmAddressCaseNew] PRIMARY KEY CLUSTERED  ([FirmAddressClientSiteID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmAddressClientSite_OLD] ADD CONSTRAINT [FK_FirmAddressClientSite_ClientSite] FOREIGN KEY ([ClientSiteID]) REFERENCES [dbo].[ClientSite_OLD] ([ClientSiteID])
GO
ALTER TABLE [dbo].[FirmAddressClientSite_OLD] ADD CONSTRAINT [FK_FirmAddressClientSite_FirmAddress] FOREIGN KEY ([FirmAddressID]) REFERENCES [dbo].[FirmAddress_OLD] ([FirmAddressID])
GO
GRANT VIEW DEFINITION ON  [dbo].[FirmAddressClientSite_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[FirmAddressClientSite_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[FirmAddressClientSite_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[FirmAddressClientSite_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[FirmAddressClientSite_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[FirmAddressClientSite_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[FirmAddressClientSite_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps a specific firm''s office location as having acces to a certain site for a certain client.', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddressClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Indicates whether or not this permission should be considered effective or not.  Allows KCIC to setup FirmAddress access to sites prior to them going live (so users from that firm address won''t see unfinished or untested sections of the site).  Can also be used to disable access for a specific firm if they no longer work for the client.', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddressClientSite_OLD', 'COLUMN', N'Active'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Client site to which access is granted.', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddressClientSite_OLD', 'COLUMN', N'ClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK indicating which office locaiton for a specific firm has access to the client site.', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressID'
GO
