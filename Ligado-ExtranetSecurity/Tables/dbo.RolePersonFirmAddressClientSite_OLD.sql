CREATE TABLE [dbo].[RolePersonFirmAddressClientSite_OLD]
(
[RolePersonFirmAddressClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[PersonFirmAddressClientSiteID] [bigint] NOT NULL,
[CanEdit] [bit] NOT NULL,
[CanAdd] [bit] NOT NULL,
[CanDelete] [bit] NOT NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPersonFirmAddressCaseRole_tblPersonFirmAddressCase1] ON [dbo].[RolePersonFirmAddressClientSite_OLD] ([PersonFirmAddressClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPersonFirmAddressCaseRole_tblRole] ON [dbo].[RolePersonFirmAddressClientSite_OLD] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RolePersonFirmAddressClientSite_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RolePersonFirmAddressClientSite_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RolePersonFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RolePersonFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RolePersonFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RolePersonFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RolePersonFirmAddressClientSite_OLD] TO [Ligado_User]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps Role access to a specific person, within a specific firm, for a specific ClientSite.', 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the role provides Add permissions to any datagrid controls associated with the role.', 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the role provides delete permissions to any datagrid controls associated with the role.', 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the role provides edit permissions to any detailsview or datagrid controls associated with the role.', 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'KCIC internal notes about the mapping.', 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=3345
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Person who will receive access to the role for a specific client site based on their membership of a specific firm. TODO: Break this out into PersonID, FirmAddressID, ClientSiteID as separate keys.', 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Role to which access will be granted.', 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RolePersonFirmAddressClientSiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RolePersonFirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RolePersonFirmAddressClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RolePersonFirmAddressClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RolePersonFirmAddressClientSiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RolePersonFirmAddressClientSite_OLD', 'COLUMN', N'RolePersonFirmAddressClientSiteID'
GO
