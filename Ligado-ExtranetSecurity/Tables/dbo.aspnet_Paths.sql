CREATE TABLE [dbo].[aspnet_Paths]
(
[ApplicationId] [uniqueidentifier] NOT NULL,
[PathId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__aspnet_Pa__PathI__558AAF1E] DEFAULT (newid()),
[Path] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoweredPath] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Paths] ADD CONSTRAINT [PK__aspnet_P__CD67DC5852AE4273] PRIMARY KEY NONCLUSTERED  ([PathId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__aspnet_Pa__Appli__54968AE5] ON [dbo].[aspnet_Paths] ([ApplicationId]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Paths_index] ON [dbo].[aspnet_Paths] ([ApplicationId], [LoweredPath]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Paths] ADD CONSTRAINT [FK__aspnet_Pa__Appli__54968AE5] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_Paths] TO [base_user]
GRANT SELECT ON  [dbo].[aspnet_Paths] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[aspnet_Paths] TO [Ligado_User]
GRANT SELECT ON  [dbo].[aspnet_Paths] TO [Ligado_User]
GRANT INSERT ON  [dbo].[aspnet_Paths] TO [Ligado_User]
GRANT DELETE ON  [dbo].[aspnet_Paths] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[aspnet_Paths] TO [Ligado_User]
GO
