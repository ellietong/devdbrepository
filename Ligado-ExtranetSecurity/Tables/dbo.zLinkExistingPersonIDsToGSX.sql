CREATE TABLE [dbo].[zLinkExistingPersonIDsToGSX]
(
[PersonID] [int] NULL,
[FirmAddressID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[zLinkExistingPersonIDsToGSX] TO [base_user]
GRANT SELECT ON  [dbo].[zLinkExistingPersonIDsToGSX] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zLinkExistingPersonIDsToGSX] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zLinkExistingPersonIDsToGSX] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zLinkExistingPersonIDsToGSX] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zLinkExistingPersonIDsToGSX] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zLinkExistingPersonIDsToGSX] TO [Ligado_User]
GO
