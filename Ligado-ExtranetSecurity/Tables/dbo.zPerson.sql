CREATE TABLE [dbo].[zPerson]
(
[PersonID] [bigint] NOT NULL IDENTITY(1, 1),
[MembershipID] [uniqueidentifier] NULL,
[PrefixID] [int] NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuffixID] [int] NULL,
[GenderID] [int] NULL,
[DirectPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirectFax] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PasswordReset] [bit] NULL,
[PasswordQuestionID] [int] NULL,
[PasswordAnswer] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TermsAcceptedDate] [datetime] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[zPerson] ADD CONSTRAINT [PK_tblPerson] PRIMARY KEY CLUSTERED  ([PersonID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPerson_tblGender] ON [dbo].[zPerson] ([GenderID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_Person_Membership] ON [dbo].[zPerson] ([MembershipID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPerson_tblPasswordQuestion] ON [dbo].[zPerson] ([PasswordQuestionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPerson_tblPrefix] ON [dbo].[zPerson] ([PrefixID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPerson_tblSuffix] ON [dbo].[zPerson] ([SuffixID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zPerson] ADD CONSTRAINT [FK_tblPerson_tblGender] FOREIGN KEY ([GenderID]) REFERENCES [dbo].[Gender_OLD] ([GenderID])
GO
ALTER TABLE [dbo].[zPerson] WITH NOCHECK ADD CONSTRAINT [FK_tblPerson_tblPasswordQuestion] FOREIGN KEY ([PasswordQuestionID]) REFERENCES [dbo].[PasswordQuestion_OLD] ([PasswordQuestionID])
GO
ALTER TABLE [dbo].[zPerson] ADD CONSTRAINT [FK_tblPerson_tblPrefix] FOREIGN KEY ([PrefixID]) REFERENCES [dbo].[Prefix_OLD] ([PrefixID])
GO
ALTER TABLE [dbo].[zPerson] ADD CONSTRAINT [FK_tblPerson_tblSuffix] FOREIGN KEY ([SuffixID]) REFERENCES [dbo].[Suffix_OLD] ([SuffixID])
GO
GRANT VIEW DEFINITION ON  [dbo].[zPerson] TO [base_user]
GRANT SELECT ON  [dbo].[zPerson] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zPerson] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zPerson] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zPerson] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zPerson] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zPerson] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'Email'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'Email'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'Email'
GO
DECLARE @xp smallint
SELECT @xp=2955
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'Email'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'Email'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'FirstName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'FirstName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'FirstName'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'FirstName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'FirstName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'LastName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'LastName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'LastName'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'LastName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'LastName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'Password'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'Password'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'Password'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'Password'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'Password'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordAnswer'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordAnswer'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordAnswer'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordAnswer'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordAnswer'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordQuestionID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordQuestionID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordQuestionID'
GO
DECLARE @xp smallint
SELECT @xp=2355
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordQuestionID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordQuestionID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordReset'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordReset'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordReset'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordReset'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PasswordReset'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PersonID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PersonID'
GO
DECLARE @xp smallint
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PersonID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PersonID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'TermsAcceptedDate'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'TermsAcceptedDate'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'TermsAcceptedDate'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'TermsAcceptedDate'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zPerson', 'COLUMN', N'TermsAcceptedDate'
GO
