CREATE TABLE [dbo].[QueryStringParameterDataGrid_OLD]
(
[QueryStringParameterDataGridID] [bigint] NOT NULL IDENTITY(1, 1),
[QueryStringParameterID] [bigint] NOT NULL,
[DataGridID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QueryStringParameterDataGrid_OLD] ADD CONSTRAINT [PK_tblQueryStringParameterDataGrid] PRIMARY KEY CLUSTERED  ([QueryStringParameterDataGridID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblQueryStringParameterDataGrid_tblDataGrid] ON [dbo].[QueryStringParameterDataGrid_OLD] ([DataGridID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblQueryStringParameterDataGrid_tblQueryStringParameter] ON [dbo].[QueryStringParameterDataGrid_OLD] ([QueryStringParameterID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QueryStringParameterDataGrid_OLD] ADD CONSTRAINT [FK_tblQueryStringParameterDataGrid_tblDataGrid] FOREIGN KEY ([DataGridID]) REFERENCES [dbo].[DataGrid_OLD] ([DataGridID])
GO
ALTER TABLE [dbo].[QueryStringParameterDataGrid_OLD] ADD CONSTRAINT [FK_tblQueryStringParameterDataGrid_tblQueryStringParameter] FOREIGN KEY ([QueryStringParameterID]) REFERENCES [dbo].[QueryStringParameter_OLD] ([QueryStringParameterID])
GO
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameterDataGrid_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[QueryStringParameterDataGrid_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameterDataGrid_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[QueryStringParameterDataGrid_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[QueryStringParameterDataGrid_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[QueryStringParameterDataGrid_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[QueryStringParameterDataGrid_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps a query string to a data grid.', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameterDataGrid_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Data grid to which the query string parameter should be mapped.', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameterDataGrid_OLD', 'COLUMN', N'DataGridID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameterDataGrid_OLD', 'COLUMN', N'QueryStringParameterDataGridID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Query string parameter to map', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameterDataGrid_OLD', 'COLUMN', N'QueryStringParameterID'
GO
