CREATE TABLE [dbo].[RoleReport_OLD]
(
[RoleReportID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[ReportID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleReport_OLD] ADD CONSTRAINT [PK_tblRoleReportID] PRIMARY KEY CLUSTERED  ([RoleReportID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleReport_tblReport] ON [dbo].[RoleReport_OLD] ([ReportID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleReport_tblRole] ON [dbo].[RoleReport_OLD] ([RoleID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleReport_OLD] ADD CONSTRAINT [FK_tblRoleReport_tblReport] FOREIGN KEY ([ReportID]) REFERENCES [dbo].[Report_OLD] ([ReportID])
GO
ALTER TABLE [dbo].[RoleReport_OLD] ADD CONSTRAINT [FK_tblRoleReport_tblRole] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role_OLD] ([RoleID])
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleReport_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RoleReport_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleReport_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleReport_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleReport_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleReport_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleReport_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'RoleReport_OLD', NULL, NULL
GO
