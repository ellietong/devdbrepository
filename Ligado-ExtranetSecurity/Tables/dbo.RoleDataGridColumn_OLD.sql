CREATE TABLE [dbo].[RoleDataGridColumn_OLD]
(
[RoleDataGridColumnID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[DataGridColumnID] [bigint] NOT NULL,
[Required] [bit] NOT NULL,
[SortOrder] [int] NOT NULL,
[VisibleNormal] [bit] NULL,
[VisibleEdit] [bit] NULL,
[VisibleCreate] [bit] NOT NULL,
[ReadOnly] [bit] NULL,
[AllowEntriesToBeInserted] [bit] NOT NULL,
[AllowEntriesToBeUpdated] [bit] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDataGridColumnLinkExclude_tblDataGridColumnLink] ON [dbo].[RoleDataGridColumn_OLD] ([DataGridColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDataGridColumnLinkExclude_tblRole] ON [dbo].[RoleDataGridColumn_OLD] ([RoleID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140410-105439] ON [dbo].[RoleDataGridColumn_OLD] ([RoleID]) INCLUDE ([AllowEntriesToBeInserted], [AllowEntriesToBeUpdated], [DataGridColumnID], [ReadOnly], [Required], [SortOrder], [VisibleCreate], [VisibleEdit], [VisibleNormal]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGridColumn_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RoleDataGridColumn_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGridColumn_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleDataGridColumn_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleDataGridColumn_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleDataGridColumn_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleDataGridColumn_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps a specific column of a datagrid to a role.  Allows Role to contain a subset of possible columns from the DataGrid definition.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'If the field is a dropdown, whether we should render an "Add" button next to the field in order to open an editing dialog. (i.e. Doctor in diagnosis info).  Can also be used when creating autocomplete dropdowns against text fields as to whether or not we should show the users text as an entry option (if no other records are found).', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'AllowEntriesToBeInserted'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Indicates whether an "Edit" button should be rendered next to a dropdown that would allow the user to edit the currently dropdown selection (i.e Doctor in Diagnosis Information)', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'AllowEntriesToBeUpdated'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK of the record that relates the datagrid to the column.  TODO:  Separate this into the two keys - DataGridID and ColumnID so it is easier to figure out which column something is without going through an intermediate table.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'DataGridColumnID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the control should be read-only if it appears in edit or add record dialogs.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'ReadOnly'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not a field should have a red "required" asterisk appear next to it.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'Required'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'RoleDataGridColumnID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Role to which the column should be mapped.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'RoleID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Relative order that the column should be disaplyed in the data grid.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'SortOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the column should appear when adding records to the grid.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'VisibleCreate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the column should appear when editing a row from the grid.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the column should be rendered when the grid is shown.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
