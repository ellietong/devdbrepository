CREATE TABLE [dbo].[ClientSite_OLD]
(
[ClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[ClientID] [bigint] NOT NULL,
[SiteID] [bigint] NOT NULL,
[ConnectionStringKey] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bucket] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [bit] NOT NULL CONSTRAINT [DF_tblClientSite_Active] DEFAULT ((0)),
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SharePointUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClientSite_OLD] ADD CONSTRAINT [PK_tblClientSite] PRIMARY KEY CLUSTERED  ([ClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClientSite_tblClient] ON [dbo].[ClientSite_OLD] ([ClientID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClientSite_tblSite] ON [dbo].[ClientSite_OLD] ([SiteID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClientSite_OLD] ADD CONSTRAINT [FK_tblClientSite_tblClient] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client_OLD] ([ClientID])
GO
ALTER TABLE [dbo].[ClientSite_OLD] ADD CONSTRAINT [FK_tblClientSite_tblSite] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[Site_OLD] ([SiteID])
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientSite_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[ClientSite_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ClientSite_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ClientSite_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ClientSite_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ClientSite_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ClientSite_OLD] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Relates a client to a type of site (claims admin, policy, etc)', 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Active'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not the client''s site is currently active.', 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Bucket'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Bucket'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Bucket'
GO
DECLARE @xp smallint
SELECT @xp=1590
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Bucket'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Bucket'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Amazon S3 Bucket where site documents should be stored', 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Bucket'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Bucket'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ID of the Client', 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK - used in Ligado to filter nearly everything', 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ConnectionStringKey'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ConnectionStringKey'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ConnectionStringKey'
GO
DECLARE @xp smallint
SELECT @xp=2370
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ConnectionStringKey'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ConnectionStringKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Connection string alias that should appear in web.config', 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ConnectionStringKey'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'ConnectionStringKey'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Internal KCIC notes for this client site', 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SharePointUrl'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SharePointUrl'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SharePointUrl'
GO
DECLARE @xp smallint
SELECT @xp=4515
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SharePointUrl'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SharePointUrl'
GO
EXEC sp_addextendedproperty N'MS_Description', N'SharePoint relative URL for sites not yet transitioned to Ligado.', 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SharePointUrl'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SharePointUrl'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ID of the Site type (policy, claims, plaintiff portal, etc)', 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ClientSite_OLD', 'COLUMN', N'SiteID'
GO
