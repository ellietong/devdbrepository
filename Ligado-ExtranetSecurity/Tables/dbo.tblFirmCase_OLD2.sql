CREATE TABLE [dbo].[tblFirmCase_OLD2]
(
[FirmCaseID] [int] NOT NULL IDENTITY(1, 1),
[FirmID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[FirmCaseURL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFirmCase_OLD2] ADD CONSTRAINT [PK_tblFirmCase] PRIMARY KEY CLUSTERED  ([FirmCaseID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblFirmCase_OLD2] TO [LigadoSecurity]
GO
