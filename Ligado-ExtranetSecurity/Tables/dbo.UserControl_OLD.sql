CREATE TABLE [dbo].[UserControl_OLD]
(
[UserControlID] [bigint] NOT NULL IDENTITY(1, 1),
[UserControlName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserControlPath] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserControl_OLD] ADD CONSTRAINT [PK_tblUserControl] PRIMARY KEY CLUSTERED  ([UserControlID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserControl_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[UserControl_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[UserControl_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[UserControl_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[UserControl_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[UserControl_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[UserControl_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'UserControl_OLD', NULL, NULL
GO
