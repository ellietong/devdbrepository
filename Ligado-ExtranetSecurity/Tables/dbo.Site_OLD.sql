CREATE TABLE [dbo].[Site_OLD]
(
[SiteID] [bigint] NOT NULL IDENTITY(1, 1),
[SiteName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Site_OLD] ADD CONSTRAINT [PK_tblSite] PRIMARY KEY CLUSTERED  ([SiteID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Site_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Site_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Site_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Site_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Site_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Site_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Site_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type of site that a user may have access to (i.e. Claims, Policy, Insolvency)', 'SCHEMA', N'dbo', 'TABLE', N'Site_OLD', NULL, NULL
GO
