CREATE TABLE [dbo].[aspnet_Applications]
(
[ApplicationName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoweredApplicationName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApplicationId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__aspnet_Ap__Appli__02925FBF] DEFAULT (newid()),
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Applications] ADD CONSTRAINT [PK__aspnet_A__C93A4C987AF13DF7] PRIMARY KEY NONCLUSTERED  ([ApplicationId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Applications] ADD CONSTRAINT [UQ__aspnet_A__3091033100AA174D] UNIQUE NONCLUSTERED  ([ApplicationName]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Applications] ADD CONSTRAINT [UQ__aspnet_A__17477DE47DCDAAA2] UNIQUE NONCLUSTERED  ([LoweredApplicationName]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [aspnet_Applications_Index] ON [dbo].[aspnet_Applications] ([LoweredApplicationName]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_Applications] TO [base_user]
GRANT SELECT ON  [dbo].[aspnet_Applications] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[aspnet_Applications] TO [Ligado_User]
GRANT SELECT ON  [dbo].[aspnet_Applications] TO [Ligado_User]
GRANT INSERT ON  [dbo].[aspnet_Applications] TO [Ligado_User]
GRANT DELETE ON  [dbo].[aspnet_Applications] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[aspnet_Applications] TO [Ligado_User]
GO
