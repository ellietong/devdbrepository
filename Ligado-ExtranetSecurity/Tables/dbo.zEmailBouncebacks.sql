CREATE TABLE [dbo].[zEmailBouncebacks]
(
[InactiveEmailAddress] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[zEmailBouncebacks] TO [base_user]
GRANT SELECT ON  [dbo].[zEmailBouncebacks] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zEmailBouncebacks] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zEmailBouncebacks] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zEmailBouncebacks] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zEmailBouncebacks] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zEmailBouncebacks] TO [Ligado_User]
GO
