CREATE TABLE [dbo].[tblPermissionsRole_OLD2]
(
[RolePermissionsID] [int] NOT NULL IDENTITY(1, 1),
[CaseID] [int] NULL,
[FirmID] [int] NULL,
[RoleID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPermissionsRole_OLD2] ADD CONSTRAINT [PK_dbo.tblPermissionsRole] PRIMARY KEY CLUSTERED  ([RolePermissionsID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPermissionsRole_OLD2] TO [LigadoSecurity]
GO
