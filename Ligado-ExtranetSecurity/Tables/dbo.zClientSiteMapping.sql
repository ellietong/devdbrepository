CREATE TABLE [dbo].[zClientSiteMapping]
(
[ClientSiteID] [int] NULL,
[CaseID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[zClientSiteMapping] TO [base_user]
GRANT SELECT ON  [dbo].[zClientSiteMapping] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zClientSiteMapping] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zClientSiteMapping] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zClientSiteMapping] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zClientSiteMapping] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zClientSiteMapping] TO [Ligado_User]
GO
