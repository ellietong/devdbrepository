CREATE TABLE [dbo].[zFirm]
(
[FirmID] [int] NOT NULL IDENTITY(1, 1),
[Firm] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmDisplayName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[zFirm] ADD CONSTRAINT [PK_tblFirm] PRIMARY KEY CLUSTERED  ([FirmID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[zFirm] TO [base_user]
GRANT SELECT ON  [dbo].[zFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zFirm] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zFirm] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zFirm] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zFirm] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zFirm] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_DefaultView', N'2', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_FilterOnLoad', N'False', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_HideNewField', N'False', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_LinkChildFields', N'', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_LinkMasterFields', N'', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', N'[tblFirm].[FirmID]', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderByOn', N'True', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', N'True', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Orientation', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SubdatasheetName', N'', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_TableMaxRecords', N'10000', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_TotalsRow', N'False', 'SCHEMA', N'dbo', 'TABLE', N'zFirm', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'Firm'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'Firm'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'Firm'
GO
DECLARE @xp smallint
SELECT @xp=5580
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'Firm'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'Firm'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'FirmDisplayName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'FirmDisplayName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'FirmDisplayName'
GO
DECLARE @xp smallint
SELECT @xp=6330
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'FirmDisplayName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'FirmDisplayName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'FirmID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'FirmID'
GO
DECLARE @xp smallint
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'FirmID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'FirmID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirm', 'COLUMN', N'FirmID'
GO
