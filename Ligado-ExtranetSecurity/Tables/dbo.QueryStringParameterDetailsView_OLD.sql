CREATE TABLE [dbo].[QueryStringParameterDetailsView_OLD]
(
[QueryStringParameterDetailsViewID] [bigint] NOT NULL IDENTITY(1, 1),
[QueryStringParameterID] [bigint] NOT NULL,
[DetailsViewID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QueryStringParameterDetailsView_OLD] ADD CONSTRAINT [PK_tblQueryStringParameterDetailsView] PRIMARY KEY CLUSTERED  ([QueryStringParameterDetailsViewID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblQueryStringParameterDetailsView_tblDetailsView] ON [dbo].[QueryStringParameterDetailsView_OLD] ([DetailsViewID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblQueryStringParameterDetailsView_tblQueryStringParameter] ON [dbo].[QueryStringParameterDetailsView_OLD] ([QueryStringParameterID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QueryStringParameterDetailsView_OLD] ADD CONSTRAINT [FK_tblQueryStringParameterDetailsView_tblDetailsView] FOREIGN KEY ([DetailsViewID]) REFERENCES [dbo].[DetailsView_OLD] ([DetailsViewID])
GO
ALTER TABLE [dbo].[QueryStringParameterDetailsView_OLD] ADD CONSTRAINT [FK_tblQueryStringParameterDetailsView_tblQueryStringParameter] FOREIGN KEY ([QueryStringParameterID]) REFERENCES [dbo].[QueryStringParameter_OLD] ([QueryStringParameterID])
GO
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameterDetailsView_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[QueryStringParameterDetailsView_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameterDetailsView_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[QueryStringParameterDetailsView_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[QueryStringParameterDetailsView_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[QueryStringParameterDetailsView_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[QueryStringParameterDetailsView_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps a query string parameter control to a details view', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameterDetailsView_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Details view control to which the query string parameter should be mapped.', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameterDetailsView_OLD', 'COLUMN', N'DetailsViewID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameterDetailsView_OLD', 'COLUMN', N'QueryStringParameterDetailsViewID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Query string parameter to map', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameterDetailsView_OLD', 'COLUMN', N'QueryStringParameterID'
GO
