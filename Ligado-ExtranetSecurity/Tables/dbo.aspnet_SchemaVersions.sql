CREATE TABLE [dbo].[aspnet_SchemaVersions]
(
[Feature] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CompatibleSchemaVersion] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsCurrentVersion] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_SchemaVersions] ADD CONSTRAINT [PK__aspnet_S__5A1E6BC10D0FEE32] PRIMARY KEY CLUSTERED  ([Feature], [CompatibleSchemaVersion]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_SchemaVersions] TO [base_user]
GRANT SELECT ON  [dbo].[aspnet_SchemaVersions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[aspnet_SchemaVersions] TO [Ligado_User]
GRANT SELECT ON  [dbo].[aspnet_SchemaVersions] TO [Ligado_User]
GRANT INSERT ON  [dbo].[aspnet_SchemaVersions] TO [Ligado_User]
GRANT DELETE ON  [dbo].[aspnet_SchemaVersions] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[aspnet_SchemaVersions] TO [Ligado_User]
GO
