CREATE TABLE [dbo].[RoleDataGridCriterion]
(
[RoleDataGridCriterionID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[DataGridCriterionID] [bigint] NOT NULL,
[SortOrder] [int] NOT NULL CONSTRAINT [DF_RoleDataGridCriterion_SortOrder] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleDataGridCriterion] ADD CONSTRAINT [PK_tblRoleDataGridCriterionLinkExclude] PRIMARY KEY CLUSTERED  ([RoleDataGridCriterionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDataGridCriterionLinkExclude_tblDataGridCriterionLink] ON [dbo].[RoleDataGridCriterion] ([DataGridCriterionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDataGridCriterionLinkExclude_tblRole] ON [dbo].[RoleDataGridCriterion] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGridCriterion] TO [base_user]
GRANT SELECT ON  [dbo].[RoleDataGridCriterion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGridCriterion] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleDataGridCriterion] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleDataGridCriterion] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleDataGridCriterion] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleDataGridCriterion] TO [Ligado_User]
GO
