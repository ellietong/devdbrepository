CREATE TABLE [dbo].[Role_OLD]
(
[RoleID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoleDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDefault] [bit] NULL,
[IsNotification] [bit] NULL,
[IsFirmAddressCaseLevelOnly] [bit] NULL,
[RoleType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Role_OLD] ADD CONSTRAINT [PK_tblRole] PRIMARY KEY CLUSTERED  ([RoleID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Role_OLD] ADD CONSTRAINT [FK_Role_RoleType] FOREIGN KEY ([RoleType]) REFERENCES [dbo].[RoleType_OLD] ([RoleType])
GO
GRANT VIEW DEFINITION ON  [dbo].[Role_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Role_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Role_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Role_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Role_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Role_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Role_OLD] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Role record to which details views and data grid definitions can be assigned.', 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsDefault'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsDefault'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsDefault'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsDefault'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsDefault'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED? - previously would be given to new users by default.', 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsDefault'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsDefault'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsFirmAddressCaseLevelOnly'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsFirmAddressCaseLevelOnly'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsFirmAddressCaseLevelOnly'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsFirmAddressCaseLevelOnly'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsFirmAddressCaseLevelOnly'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED?', 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsFirmAddressCaseLevelOnly'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsFirmAddressCaseLevelOnly'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsNotification'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsNotification'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsNotification'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsNotification'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsNotification'
GO
EXEC sp_addextendedproperty N'MS_Description', N'? Whether or not the role is used to decide whether or not someone receives an automatic email - will this be irrelevant once we have role types in place?', 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsNotification'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'IsNotification'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleDescription'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleDescription'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleDescription'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleDescription'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Internal text description for what differs about this role and to which client it applies.', 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleDescription'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleDescription'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp smallint
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleName'
GO
DECLARE @xp smallint
SELECT @xp=4020
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of the role.', 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Overarching type of role that this role represents.', 'SCHEMA', N'dbo', 'TABLE', N'Role_OLD', 'COLUMN', N'RoleType'
GO
