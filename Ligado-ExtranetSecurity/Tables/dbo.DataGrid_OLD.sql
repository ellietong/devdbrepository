CREATE TABLE [dbo].[DataGrid_OLD]
(
[DataGridID] [bigint] NOT NULL IDENTITY(1, 1),
[DataGridName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisplayName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjaxSource] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceObject] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceMethod] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataObjectTypeName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Include] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateProc] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteProc] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsertProc] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SelectProc] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PageSize] [int] NULL,
[DataGridTypeID] [int] NOT NULL CONSTRAINT [DF_DataGrid_DataGridTypeID] DEFAULT ((1)),
[EmptyDataText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnableRowSelection] [bit] NULL CONSTRAINT [DF_tblDataGrid_EnableRowSelection] DEFAULT ((0)),
[EnableExport] [bit] NOT NULL CONSTRAINT [DF__DataGrid__Enable__47477CBF] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataGrid_OLD] ADD CONSTRAINT [PK_tblDynamicSearch] PRIMARY KEY CLUSTERED  ([DataGridID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_DataGrid_DataGridType] ON [dbo].[DataGrid_OLD] ([DataGridTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataGrid_OLD] ADD CONSTRAINT [FK_DataGrid_DataGridType] FOREIGN KEY ([DataGridTypeID]) REFERENCES [dbo].[DataGridType_OLD] ([DataGridTypeID])
GO
GRANT VIEW DEFINITION ON  [dbo].[DataGrid_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[DataGrid_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DataGrid_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DataGrid_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[DataGrid_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[DataGrid_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[DataGrid_OLD] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp smallint
SELECT @xp=6390
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'AjaxSource'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WebAPI GetJTable endpoint that provides data to this grid', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'AjaxSource'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp smallint
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridName'
GO
DECLARE @xp smallint
SELECT @xp=4575
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of datagrid without spaces for reference in code.', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp smallint
SELECT @xp=1875
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridTypeID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lookup to Allow Multiple DataGrids for the same info to be queried the same way, even with different DataGridNames', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataGridTypeID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp smallint
SELECT @xp=6195
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataObjectTypeName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED?', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp smallint
SELECT @xp=5490
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DeleteProc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Relative WebAPI URL to which IDs of records should be sent via DELETE verb', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp smallint
SELECT @xp=6255
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DisplayName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name to appear on search tab or fieldset title', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp smallint
SELECT @xp=6195
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EmptyDataText'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Message that appears when the grid is empty', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EmptyDataText'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If enabled, this will render the "Export to CSV" button in the top toolbar of the data grid', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EnableExport'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EnableRowSelection'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EnableRowSelection'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EnableRowSelection'
GO
DECLARE @xp smallint
SELECT @xp=2325
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EnableRowSelection'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EnableRowSelection'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Enabled interaction with individual rows in the grid, which fire events when the selection is changed.  Used for loading grids dependent on other information', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EnableRowSelection'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'EnableRowSelection'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Include'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Include'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Include'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Include'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Include'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED - originally for EntityFramework integration', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Include'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Include'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp smallint
SELECT @xp=5295
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'InsertProc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Relative WebAPI URL to which records should be PUT for insert', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Internal KCIC notes about the grid', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'PageSize'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'PageSize'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'PageSize'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'PageSize'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'PageSize'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Default number of records to show for this grid', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'PageSize'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'PageSize'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp smallint
SELECT @xp=5625
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'SelectProc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Relative WebAPI URL where individual records can be retrieved via ID and GET verb', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp smallint
SELECT @xp=1830
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceMethod'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp smallint
SELECT @xp=5520
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceObject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'UpdateProc'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'UpdateProc'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'UpdateProc'
GO
DECLARE @xp smallint
SELECT @xp=5295
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'UpdateProc'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'UpdateProc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Relative WebAPI URL to which updates to records should be POSTed', 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'UpdateProc'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGrid_OLD', 'COLUMN', N'UpdateProc'
GO
