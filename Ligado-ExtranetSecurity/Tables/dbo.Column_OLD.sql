CREATE TABLE [dbo].[Column_OLD]
(
[ColumnID] [bigint] NOT NULL IDENTITY(1, 1),
[DisplayName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataField] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataFormatString] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortExpression] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HeaderAlignment] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EditorControlType] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataValueField] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataTextField] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceObject] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceMethod] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InputMask] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LinkURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Column_OLD] ADD CONSTRAINT [PK_tblDataGridColumn] PRIMARY KEY CLUSTERED  ([ColumnID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Column_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Column_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Column_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Column_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Column_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Column_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Column_OLD] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Column definitions for DataGrids and DetailsViews', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'(DataField ALike "%nav%")', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', N'[Column].[DisplayName], [Column].[ColumnID], [Column].[DataField]', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp smallint
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp smallint
SELECT @xp=1335
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ColumnID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp smallint
SELECT @xp=2655
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataField'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The underlying SQL column name for this column that is returned from the view or table driving it.', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataField'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp smallint
SELECT @xp=2055
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataFormatString'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataFormatString'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp smallint
SELECT @xp=1710
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataTextField'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Text field to which a dropdown control binds', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataTextField'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp smallint
SELECT @xp=1815
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Data Type the column represents (Int, String, Boolean)', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataType'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp smallint
SELECT @xp=1845
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataValueField'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ID field to which a dropdown control binds', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DataValueField'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp smallint
SELECT @xp=2655
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DisplayName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name to display on site in column headers or as labels in editing controls', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp smallint
SELECT @xp=2115
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'EditorControlType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'TextBox, DropDown', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'EditorControlType'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp smallint
SELECT @xp=2070
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'HeaderAlignment'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'HeaderAlignment'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'InputMask'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'InputMask'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'InputMask'
GO
DECLARE @xp smallint
SELECT @xp=1395
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'InputMask'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'InputMask'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'InputMask'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'InputMask'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'LinkURL'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'LinkURL'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'LinkURL'
GO
DECLARE @xp smallint
SELECT @xp=10005
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'LinkURL'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'LinkURL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'LinkURL'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'LinkURL'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp smallint
SELECT @xp=1830
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceMethod'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp smallint
SELECT @xp=6150
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceObject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Select2 WebAPI relative URL for populating dropdowns RENAME: AjaxSource', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'SortExpression'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'SortExpression'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'SortExpression'
GO
DECLARE @xp smallint
SELECT @xp=1800
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'SortExpression'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'SortExpression'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'SortExpression'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Column_OLD', 'COLUMN', N'SortExpression'
GO
