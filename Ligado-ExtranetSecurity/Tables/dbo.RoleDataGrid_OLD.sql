CREATE TABLE [dbo].[RoleDataGrid_OLD]
(
[RoleDataGridID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[DataGridID] [bigint] NOT NULL,
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDataGridCase_tblDynamicSearch] ON [dbo].[RoleDataGrid_OLD] ([DataGridID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDataGrid_tblRole] ON [dbo].[RoleDataGrid_OLD] ([RoleID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140410-105634] ON [dbo].[RoleDataGrid_OLD] ([RoleID]) INCLUDE ([DataGridID], [SortOrder]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGrid_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RoleDataGrid_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGrid_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleDataGrid_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleDataGrid_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleDataGrid_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleDataGrid_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'DataGrid to which a user with this Role should have access.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGrid_OLD', 'COLUMN', N'DataGridID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGrid_OLD', 'COLUMN', N'RoleDataGridID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Role which should grant access to the datagrid.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGrid_OLD', 'COLUMN', N'RoleID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED? - This may be used for Search grid ordering, but it may no longer be necessary at this point.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGrid_OLD', 'COLUMN', N'SortOrder'
GO
