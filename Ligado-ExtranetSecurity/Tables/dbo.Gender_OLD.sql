CREATE TABLE [dbo].[Gender_OLD]
(
[GenderID] [int] NOT NULL IDENTITY(1, 1),
[Gender] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Gender_OLD] ADD CONSTRAINT [PK_tblGender] PRIMARY KEY CLUSTERED  ([GenderID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Gender_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Gender_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Gender_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Gender_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Gender_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Gender_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Gender_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lookup table for gender', 'SCHEMA', N'dbo', 'TABLE', N'Gender_OLD', NULL, NULL
GO
