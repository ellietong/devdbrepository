CREATE TABLE [dbo].[aspnet_Roles]
(
[ApplicationId] [uniqueidentifier] NOT NULL,
[RoleId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__aspnet_Ro__RoleI__3EA749C6] DEFAULT (newid()),
[RoleName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoweredRoleName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Roles] ADD CONSTRAINT [PK__aspnet_R__8AFACE1B3BCADD1B] PRIMARY KEY NONCLUSTERED  ([RoleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__aspnet_Ro__Appli__3DB3258D] ON [dbo].[aspnet_Roles] ([ApplicationId]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Roles_index1] ON [dbo].[aspnet_Roles] ([ApplicationId], [LoweredRoleName]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Roles] ADD CONSTRAINT [FK__aspnet_Ro__Appli__3DB3258D] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_Roles] TO [base_user]
GRANT SELECT ON  [dbo].[aspnet_Roles] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[aspnet_Roles] TO [Ligado_User]
GRANT SELECT ON  [dbo].[aspnet_Roles] TO [Ligado_User]
GRANT INSERT ON  [dbo].[aspnet_Roles] TO [Ligado_User]
GRANT DELETE ON  [dbo].[aspnet_Roles] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[aspnet_Roles] TO [Ligado_User]
GO
