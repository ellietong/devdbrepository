CREATE TABLE [dbo].[Prefix_OLD]
(
[PrefixID] [int] NOT NULL IDENTITY(1, 1),
[Prefix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Prefix_OLD] ADD CONSTRAINT [PK_tblPrefix] PRIMARY KEY CLUSTERED  ([PrefixID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Prefix_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Prefix_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Prefix_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Prefix_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Prefix_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Prefix_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Prefix_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lookup table', 'SCHEMA', N'dbo', 'TABLE', N'Prefix_OLD', NULL, NULL
GO
