CREATE TABLE [dbo].[zPersonFirmAddressClientSiteIDMapping]
(
[OldPersonFirmAddressClientSiteID] [int] NULL,
[NewPersonFirmAddressClientSiteID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[zPersonFirmAddressClientSiteIDMapping] TO [base_user]
GRANT SELECT ON  [dbo].[zPersonFirmAddressClientSiteIDMapping] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zPersonFirmAddressClientSiteIDMapping] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zPersonFirmAddressClientSiteIDMapping] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zPersonFirmAddressClientSiteIDMapping] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zPersonFirmAddressClientSiteIDMapping] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zPersonFirmAddressClientSiteIDMapping] TO [Ligado_User]
GO
