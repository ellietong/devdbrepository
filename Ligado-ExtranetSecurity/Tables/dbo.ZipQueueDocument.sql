CREATE TABLE [dbo].[ZipQueueDocument]
(
[ZipQueueDocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[ZipQueueID] [uniqueidentifier] NOT NULL,
[Key] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZipQueueDocument] ADD CONSTRAINT [PK_tblZipQueueDocument] PRIMARY KEY CLUSTERED  ([ZipQueueDocumentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_ZipQueueDocument_ZipQueue] ON [dbo].[ZipQueueDocument] ([ZipQueueID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZipQueueDocument] ADD CONSTRAINT [FK_ZipQueueDocument_ZipQueue] FOREIGN KEY ([ZipQueueID]) REFERENCES [dbo].[ZipQueue] ([ZipQueueID])
GO
GRANT VIEW DEFINITION ON  [dbo].[ZipQueueDocument] TO [base_user]
GRANT SELECT ON  [dbo].[ZipQueueDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZipQueueDocument] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ZipQueueDocument] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ZipQueueDocument] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ZipQueueDocument] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ZipQueueDocument] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ZipQueueDocument] TO [Zip_User]
GRANT SELECT ON  [dbo].[ZipQueueDocument] TO [Zip_User]
GRANT INSERT ON  [dbo].[ZipQueueDocument] TO [Zip_User]
GRANT DELETE ON  [dbo].[ZipQueueDocument] TO [Zip_User]
GRANT UPDATE ON  [dbo].[ZipQueueDocument] TO [Zip_User]
GO
