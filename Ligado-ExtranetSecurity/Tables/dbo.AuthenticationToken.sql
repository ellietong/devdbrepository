CREATE TABLE [dbo].[AuthenticationToken]
(
[AuthenticationTokenID] [uniqueidentifier] NOT NULL,
[Username] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsUsed] [bit] NOT NULL CONSTRAINT [DF_AuthenticationToken_IsUsed] DEFAULT ((0)),
[ExpirationDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuthenticationToken] ADD CONSTRAINT [PK_AuthenticationToken] PRIMARY KEY CLUSTERED  ([AuthenticationTokenID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[AuthenticationToken] TO [base_user]
GRANT SELECT ON  [dbo].[AuthenticationToken] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuthenticationToken] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuthenticationToken] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuthenticationToken] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuthenticationToken] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuthenticationToken] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stored GUIDs passed between SharePoint and Ligado for automatic authentication purposes', 'SCHEMA', N'dbo', 'TABLE', N'AuthenticationToken', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'GUID token to pass between servers', 'SCHEMA', N'dbo', 'TABLE', N'AuthenticationToken', 'COLUMN', N'AuthenticationTokenID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/Time the token expires if unused', 'SCHEMA', N'dbo', 'TABLE', N'AuthenticationToken', 'COLUMN', N'ExpirationDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not the one-time-use token has been used for cross-site authentication yet', 'SCHEMA', N'dbo', 'TABLE', N'AuthenticationToken', 'COLUMN', N'IsUsed'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User for whom the token was generated', 'SCHEMA', N'dbo', 'TABLE', N'AuthenticationToken', 'COLUMN', N'Username'
GO
