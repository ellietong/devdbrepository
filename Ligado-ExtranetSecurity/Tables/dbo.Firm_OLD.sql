CREATE TABLE [dbo].[Firm_OLD]
(
[FirmID] [int] NOT NULL IDENTITY(1, 1),
[Firm] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmDisplayName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Firm_OLD] ADD CONSTRAINT [PK_tblFirmNew] PRIMARY KEY CLUSTERED  ([FirmID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Firm_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Firm_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Firm_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Firm_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Firm_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Firm_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Firm_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_DefaultView', N'2', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'High level firms', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_FilterOnLoad', N'False', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_HideNewField', N'False', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_LinkChildFields', N'', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_LinkMasterFields', N'', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', N'[tblFirm].[FirmID]', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderByOn', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', N'True', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Orientation', N'0', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_SubdatasheetName', N'', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_TableMaxRecords', N'10000', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_TotalsRow', N'False', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'Firm'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'Firm'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'Firm'
GO
DECLARE @xp int
SELECT @xp=5580
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'Firm'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Static name for reference (legacyfrom SharePoint permissions that are text-based)', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'Firm'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'Firm'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmDisplayName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmDisplayName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmDisplayName'
GO
DECLARE @xp int
SELECT @xp=6330
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmDisplayName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name to display on site', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmDisplayName'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmDisplayName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'Firm_OLD', 'COLUMN', N'FirmID'
GO
