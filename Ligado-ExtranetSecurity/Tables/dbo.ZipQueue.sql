CREATE TABLE [dbo].[ZipQueue]
(
[ZipQueueID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_tblZipQueue_ZipQueueID] DEFAULT (newid()),
[Bucket] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddedToQueue] [datetime2] NULL,
[StartTime] [datetime2] NULL,
[EndTime] [datetime2] NULL,
[Attempts] [int] NOT NULL CONSTRAINT [DF_ZipQueue_Attempts] DEFAULT ((0)),
[CreatedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipKey] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZipQueue] ADD CONSTRAINT [PK_tblZipQueue] PRIMARY KEY CLUSTERED  ([ZipQueueID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZipQueue] TO [base_user]
GRANT SELECT ON  [dbo].[ZipQueue] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZipQueue] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ZipQueue] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ZipQueue] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ZipQueue] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ZipQueue] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ZipQueue] TO [Zip_User]
GRANT SELECT ON  [dbo].[ZipQueue] TO [Zip_User]
GRANT INSERT ON  [dbo].[ZipQueue] TO [Zip_User]
GRANT DELETE ON  [dbo].[ZipQueue] TO [Zip_User]
GRANT UPDATE ON  [dbo].[ZipQueue] TO [Zip_User]
GO
