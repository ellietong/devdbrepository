CREATE TABLE [dbo].[DataGridLocation_OLD]
(
[DataGridLocationID] [bigint] NOT NULL,
[DataGridID] [bigint] NOT NULL,
[LocationID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataGridLocation_OLD] ADD CONSTRAINT [PK_DataGridLocation] PRIMARY KEY CLUSTERED  ([DataGridLocationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataGridLocation_OLD] ADD CONSTRAINT [FK_DataGridLocation_DataGrid] FOREIGN KEY ([DataGridID]) REFERENCES [dbo].[DataGrid_OLD] ([DataGridID])
GO
ALTER TABLE [dbo].[DataGridLocation_OLD] ADD CONSTRAINT [FK_DataGridLocation_Location] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location_OLD] ([LocationID])
GO
GRANT VIEW DEFINITION ON  [dbo].[DataGridLocation_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[DataGridLocation_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DataGridLocation_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DataGridLocation_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[DataGridLocation_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[DataGridLocation_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[DataGridLocation_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED - originally planned to use this to say which page / slot on a page into which a grid should be rendered', 'SCHEMA', N'dbo', 'TABLE', N'DataGridLocation_OLD', NULL, NULL
GO
