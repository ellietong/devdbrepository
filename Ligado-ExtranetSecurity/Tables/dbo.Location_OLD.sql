CREATE TABLE [dbo].[Location_OLD]
(
[LocationID] [bigint] NOT NULL,
[Location] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Location_OLD] ADD CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED  ([LocationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Location_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Location_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Location_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Location_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Location_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Location_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Location_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Location_OLD', NULL, NULL
GO
