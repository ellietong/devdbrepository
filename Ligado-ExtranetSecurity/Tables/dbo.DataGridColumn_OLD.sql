CREATE TABLE [dbo].[DataGridColumn_OLD]
(
[DataGridColumnID] [bigint] NOT NULL IDENTITY(1, 1),
[DataGridID] [bigint] NOT NULL,
[ColumnID] [bigint] NOT NULL,
[DataKeyField] [bit] NOT NULL CONSTRAINT [DF_tblDataGridColumnLink_DataKeyField] DEFAULT ((0)),
[VisibleNormal] [bit] NOT NULL CONSTRAINT [DF_DataGridColumn_Visible] DEFAULT ((1)),
[VisibleEdit] [bit] NOT NULL CONSTRAINT [DF_DataGridColumn_VisibleEdit] DEFAULT ((0)),
[ReadOnly] [bit] NOT NULL CONSTRAINT [DF_DataGridColumn_ReadOnly] DEFAULT ((0)),
[ForeignKeyFlag] [bit] NOT NULL CONSTRAINT [DF_DataGridColumn_ForeignKeyFlag] DEFAULT ((0)),
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataGridColumn_OLD] ADD CONSTRAINT [PK_tblDataGridColumnLink] PRIMARY KEY CLUSTERED  ([DataGridColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDataGridColumnLink_tblDataGridColumn] ON [dbo].[DataGridColumn_OLD] ([ColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDataGridColumnLink_tblDataGrid] ON [dbo].[DataGridColumn_OLD] ([DataGridID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataGridColumn_OLD] ADD CONSTRAINT [FK_tblDataGridColumnLink_tblDataGridColumn] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column_OLD] ([ColumnID])
GO
ALTER TABLE [dbo].[DataGridColumn_OLD] ADD CONSTRAINT [FK_tblDataGridColumnLink_tblDataGrid] FOREIGN KEY ([DataGridID]) REFERENCES [dbo].[DataGrid_OLD] ([DataGridID])
GO
GRANT VIEW DEFINITION ON  [dbo].[DataGridColumn_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[DataGridColumn_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DataGridColumn_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DataGridColumn_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[DataGridColumn_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[DataGridColumn_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[DataGridColumn_OLD] TO [Ligado_User]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps a column definition to the data grid(s) under which it will appear.', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'([DataGridColumn].[DataGridID]="10")', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ColumnID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK indicating column definition', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridColumnID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridColumnID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridColumnID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridColumnID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridColumnID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK indicating data grid', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataGridID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataKeyField'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataKeyField'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataKeyField'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataKeyField'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNKNOWN', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataKeyField'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'DataKeyField'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Indicates whether this field should be passed during insert or update calls.  Used if an FK must be populated to refer to a parent table (i.e. ClaimantEmploymentID for ClaimantProduct records).', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ReadOnly'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the field is read-only.  If marked "VisibleEdit" then the control will appear and be populated during editing but will be disabled.', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'SortOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Order in which the columns should be rendered on the site in both normal and edit modes.', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED? - Not sure if these roll up by default.  Also set in roles.  Whether the column should appear as an edit control when editing a record.', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED? - not sure if these roll up by default.  Also set in roles. Whether the column should appear when the grid is rendered in read-only mode ', 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DataGridColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
