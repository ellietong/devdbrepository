CREATE TABLE [dbo].[RoleClientSite_OLD]
(
[RoleClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[ClientSiteID] [bigint] NOT NULL,
[CanEdit] [bit] NOT NULL,
[CanAdd] [bit] NOT NULL,
[CanDelete] [bit] NOT NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleClientSite_tblClientSite] ON [dbo].[RoleClientSite_OLD] ([ClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblCaseRole_tblRole1] ON [dbo].[RoleClientSite_OLD] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleClientSite_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RoleClientSite_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleClientSite_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleClientSite_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleClientSite_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleClientSite_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleClientSite_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps the specified role to all users who have access to the given ClientSiteID.', 'SCHEMA', N'dbo', 'TABLE', N'RoleClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not the control to which access is being should allow additional records by the users associated with the clientsite.  Applies only to DataGrids.', 'SCHEMA', N'dbo', 'TABLE', N'RoleClientSite_OLD', 'COLUMN', N'CanAdd'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not the control to which access is being should allow record deletion by the users associated with the clientsite.  Applies only to DataGrids.', 'SCHEMA', N'dbo', 'TABLE', N'RoleClientSite_OLD', 'COLUMN', N'CanDelete'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not the control to which access is being should be editable by the users associated with the clientsite.', 'SCHEMA', N'dbo', 'TABLE', N'RoleClientSite_OLD', 'COLUMN', N'CanEdit'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ID of the ClientSite containing users who should receive access to the given role.', 'SCHEMA', N'dbo', 'TABLE', N'RoleClientSite_OLD', 'COLUMN', N'ClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Internal KCIC notes field', 'SCHEMA', N'dbo', 'TABLE', N'RoleClientSite_OLD', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'RoleClientSite_OLD', 'COLUMN', N'RoleClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ID of Role to which all the users of a ClientSite should have.', 'SCHEMA', N'dbo', 'TABLE', N'RoleClientSite_OLD', 'COLUMN', N'RoleID'
GO
