CREATE TABLE [dbo].[FirmAddressJurisdiction_OLD]
(
[FirmAddressJurisdictionID] [int] NOT NULL IDENTITY(1, 1),
[FirmAddressID] [bigint] NULL,
[JurisdictionID] [int] NULL,
[ClientSiteID] [bigint] NULL,
[CourtTypeID] [int] NULL,
[MatterID] [int] NULL,
[StateID] [int] NULL,
[IsActive] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmAddressJurisdiction_OLD] ADD CONSTRAINT [PK_FirmAddressJurisdiction] PRIMARY KEY CLUSTERED  ([FirmAddressJurisdictionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmAddressJurisdiction_OLD] ADD CONSTRAINT [FK_FirmAddressJurisdiction_ClientSite] FOREIGN KEY ([ClientSiteID]) REFERENCES [dbo].[ClientSite_OLD] ([ClientSiteID])
GO
ALTER TABLE [dbo].[FirmAddressJurisdiction_OLD] ADD CONSTRAINT [FK_FirmAddressJurisdiction_FirmAddress] FOREIGN KEY ([FirmAddressID]) REFERENCES [dbo].[FirmAddress_OLD] ([FirmAddressID])
GO
GRANT VIEW DEFINITION ON  [dbo].[FirmAddressJurisdiction_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[FirmAddressJurisdiction_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[FirmAddressJurisdiction_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[FirmAddressJurisdiction_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[FirmAddressJurisdiction_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[FirmAddressJurisdiction_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[FirmAddressJurisdiction_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED? - I think this was an attempt to centralize DefenseJurisdiction setups in the security DB.', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddressJurisdiction_OLD', NULL, NULL
GO
