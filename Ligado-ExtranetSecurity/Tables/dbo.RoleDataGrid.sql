CREATE TABLE [dbo].[RoleDataGrid]
(
[RoleDataGridID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[DataGridID] [bigint] NOT NULL,
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleDataGrid] ADD CONSTRAINT [PK_tblDataGridCase] PRIMARY KEY CLUSTERED  ([RoleDataGridID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDataGridCase_tblDynamicSearch] ON [dbo].[RoleDataGrid] ([DataGridID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDataGrid_tblRole] ON [dbo].[RoleDataGrid] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGrid] TO [base_user]
GRANT SELECT ON  [dbo].[RoleDataGrid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGrid] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleDataGrid] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleDataGrid] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleDataGrid] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleDataGrid] TO [Ligado_User]
GO
