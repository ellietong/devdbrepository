CREATE TABLE [dbo].[RoleDetailsView]
(
[DetailsViewRoleID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[DetailsViewID] [bigint] NOT NULL,
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleDetailsView] ADD CONSTRAINT [PK_tblDetailsViewRole] PRIMARY KEY CLUSTERED  ([DetailsViewRoleID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDetailsViewRole_tblDetailsViewRole] ON [dbo].[RoleDetailsView] ([DetailsViewID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDetailsViewRole_tblRole] ON [dbo].[RoleDetailsView] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleDetailsView] TO [base_user]
GRANT SELECT ON  [dbo].[RoleDetailsView] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleDetailsView] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleDetailsView] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleDetailsView] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleDetailsView] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleDetailsView] TO [Ligado_User]
GO
