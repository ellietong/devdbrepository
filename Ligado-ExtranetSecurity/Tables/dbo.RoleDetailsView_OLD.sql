CREATE TABLE [dbo].[RoleDetailsView_OLD]
(
[DetailsViewRoleID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[DetailsViewID] [bigint] NOT NULL,
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDetailsViewRole_tblDetailsViewRole] ON [dbo].[RoleDetailsView_OLD] ([DetailsViewID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDetailsViewRole_tblRole] ON [dbo].[RoleDetailsView_OLD] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleDetailsView_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RoleDetailsView_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleDetailsView_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleDetailsView_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleDetailsView_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleDetailsView_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleDetailsView_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps a details view control definition to a role.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsView_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Details view definition that should require the aforementioned role in order to get access.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsView_OLD', 'COLUMN', N'DetailsViewID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsView_OLD', 'COLUMN', N'DetailsViewRoleID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Role ID to which the details view should be mapped in order to receive access.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsView_OLD', 'COLUMN', N'RoleID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsView_OLD', 'COLUMN', N'SortOrder'
GO
