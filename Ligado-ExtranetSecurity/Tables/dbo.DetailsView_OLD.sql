CREATE TABLE [dbo].[DetailsView_OLD]
(
[DetailsViewID] [bigint] NOT NULL IDENTITY(1, 1),
[DetailsViewName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisplayName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceObject] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceMethod] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataObjectTypeName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SelectProc] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsertProc] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateProc] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeleteProc] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmptyDataText] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DetailsView_OLD] ADD CONSTRAINT [PK_tblDetailsView] PRIMARY KEY CLUSTERED  ([DetailsViewID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailsView_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[DetailsView_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DetailsView_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DetailsView_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[DetailsView_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[DetailsView_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[DetailsView_OLD] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Describes definition for details view -- a control with shows multiple fields vertically of a single record at a time.', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DataObjectTypeName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DataObjectTypeName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DeleteProc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED - you''d never really delete something from a detailsview because its only a single record', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DeleteProc'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewName'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name that may be referenced in code.', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DetailsViewName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DisplayName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name that appears on the site in the surroudning fieldset.', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'DisplayName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'EmptyDataText'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED? - Text that should appear if the detailsview is empty.', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'EmptyDataText'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'InsertProc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED - you''d never really insert something into a DetailsView', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'InsertProc'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Internal notes for KCIC purposes.', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'SelectProc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Relative WebAPI URL that GETs a single record to populate the details view.', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'SelectProc'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceMethod'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceMethod'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceObject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'ServiceObject'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'UpdateProc'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'UpdateProc'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'UpdateProc'
GO
DECLARE @xp smallint
SELECT @xp=7095
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'UpdateProc'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'UpdateProc'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsView_OLD', 'COLUMN', N'UpdateProc'
GO
