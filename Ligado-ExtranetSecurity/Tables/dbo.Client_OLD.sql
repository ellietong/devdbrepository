CREATE TABLE [dbo].[Client_OLD]
(
[ClientID] [bigint] NOT NULL IDENTITY(1, 1),
[ClientName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientDisplayName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientLegalName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientEmail] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientDocumentNetworkPath] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Client_OLD] ADD CONSTRAINT [PK_tblClient] PRIMARY KEY CLUSTERED  ([ClientID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Client_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Client_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Client_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Client_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Client_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Client_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Client_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores list of client companies and their associated information.', 'SCHEMA', N'dbo', 'TABLE', N'Client_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Client name to display on Ligado Dashboard tabs', 'SCHEMA', N'dbo', 'TABLE', N'Client_OLD', 'COLUMN', N'ClientDisplayName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED? Not used in Ligado - use by Complaint tracking DB?', 'SCHEMA', N'dbo', 'TABLE', N'Client_OLD', 'COLUMN', N'ClientDocumentNetworkPath'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Email at kcic.com domain to use as account that auto accepts documents.', 'SCHEMA', N'dbo', 'TABLE', N'Client_OLD', 'COLUMN', N'ClientEmail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK Autonumber', 'SCHEMA', N'dbo', 'TABLE', N'Client_OLD', 'COLUMN', N'ClientID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Legal name used for generation of settlement confirmation letters', 'SCHEMA', N'dbo', 'TABLE', N'Client_OLD', 'COLUMN', N'ClientLegalName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Client name without spaces for use in code', 'SCHEMA', N'dbo', 'TABLE', N'Client_OLD', 'COLUMN', N'ClientName'
GO
