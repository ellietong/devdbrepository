CREATE TABLE [dbo].[tblRole_OLD]
(
[RoleID] [int] NOT NULL IDENTITY(1, 1),
[Role] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRole_OLD] ADD CONSTRAINT [PK_tblRole_1] PRIMARY KEY CLUSTERED  ([RoleID]) ON [PRIMARY]
GO
