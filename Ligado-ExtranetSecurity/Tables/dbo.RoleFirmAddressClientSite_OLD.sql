CREATE TABLE [dbo].[RoleFirmAddressClientSite_OLD]
(
[RoleFirmAddressClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[FirmAddressClientSiteID] [bigint] NOT NULL,
[CanEdit] [bit] NOT NULL,
[CanAdd] [bit] NOT NULL,
[CanDelete] [bit] NOT NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleFirmAddressClientSite_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RoleFirmAddressClientSite_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleFirmAddressClientSite_OLD] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Grants access to users in a specific firm address for a specific client site to a role.', 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'([RoleFirmAddressClientSite].[RoleID]="23")', 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the role provides add permissions to any datagrid controls associated with the role.', 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanAdd'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the role provides delete permissions to any datagrid controls associated with the role.', 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanDelete'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the role provides edit permissions to any detailsview or datagrid controls associated with the role.', 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'CanEdit'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=2700
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Indicates which FirmAddress for a specific clientSite of which users are a part should receive access to this role.', 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'KCIC internal notes.', 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'Notes'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RoleID to which users of the firm address for the specified client site should have access.', 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleFirmAddressClientSite_OLD', 'COLUMN', N'RoleID'
GO
