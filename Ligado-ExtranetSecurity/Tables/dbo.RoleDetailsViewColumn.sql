CREATE TABLE [dbo].[RoleDetailsViewColumn]
(
[RoleDetailsViewColumnID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[DetailsViewColumnID] [bigint] NOT NULL,
[Required] [bit] NOT NULL CONSTRAINT [DF_RoleDetailsViewColumn_Required] DEFAULT ((0)),
[SortOrder] [int] NOT NULL CONSTRAINT [DF_RoleDetailsViewColumn_SortOrder] DEFAULT ((0)),
[VisibleNormal] [bit] NULL CONSTRAINT [DF_RoleDetailsViewColumn_VisibleNormal] DEFAULT ((0)),
[VisibleEdit] [bit] NULL CONSTRAINT [DF_RoleDetailsViewColumn_VisibleEdit] DEFAULT ((0)),
[AllowEntriesToBeInserted] [bit] NOT NULL CONSTRAINT [DF_RoleDetailsViewColumn_AllowEntriesToBeInserted] DEFAULT ((0)),
[AllowEntriesToBeUpdated] [bit] NOT NULL CONSTRAINT [DF_RoleDetailsViewColumn_AllowEntriesToBeUpdated] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleDetailsViewColumn] ADD CONSTRAINT [PK_tblRoleDetailsViewFieldLinkExclude] PRIMARY KEY CLUSTERED  ([RoleDetailsViewColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDetailsViewFieldLinkExclude_tblDetailsViewFieldLink] ON [dbo].[RoleDetailsViewColumn] ([DetailsViewColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDetailsViewFieldLinkExclude_tblRole] ON [dbo].[RoleDetailsViewColumn] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleDetailsViewColumn] TO [base_user]
GRANT SELECT ON  [dbo].[RoleDetailsViewColumn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleDetailsViewColumn] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleDetailsViewColumn] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleDetailsViewColumn] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleDetailsViewColumn] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleDetailsViewColumn] TO [Ligado_User]
GO
