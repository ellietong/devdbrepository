CREATE TABLE [dbo].[RoleClientSite]
(
[RoleClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[ClientSiteID] [bigint] NOT NULL,
[CanEdit] [bit] NOT NULL CONSTRAINT [DF_tblRoleCase_CanEdit] DEFAULT ((0)),
[CanAdd] [bit] NOT NULL CONSTRAINT [DF_tblRoleCase_CanAdd] DEFAULT ((0)),
[CanDelete] [bit] NOT NULL CONSTRAINT [DF_tblRoleCase_CanDelete] DEFAULT ((0)),
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleClientSite] ADD CONSTRAINT [PK_tblCaseRole] PRIMARY KEY CLUSTERED  ([RoleClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleClientSite_tblClientSite] ON [dbo].[RoleClientSite] ([ClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblCaseRole_tblRole1] ON [dbo].[RoleClientSite] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleClientSite] TO [base_user]
GRANT SELECT ON  [dbo].[RoleClientSite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleClientSite] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleClientSite] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleClientSite] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleClientSite] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleClientSite] TO [Ligado_User]
GO
