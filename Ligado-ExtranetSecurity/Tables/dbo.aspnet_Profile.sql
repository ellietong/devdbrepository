CREATE TABLE [dbo].[aspnet_Profile]
(
[UserId] [uniqueidentifier] NOT NULL,
[PropertyNames] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PropertyValuesString] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PropertyValuesBinary] [image] NOT NULL,
[LastUpdatedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Profile] ADD CONSTRAINT [PK__aspnet_P__1788CC4C30592A6F] PRIMARY KEY CLUSTERED  ([UserId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__aspnet_Pr__UserI__324172E1] ON [dbo].[aspnet_Profile] ([UserId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Profile] ADD CONSTRAINT [FK__aspnet_Pr__UserI__324172E1] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_Profile] TO [base_user]
GRANT SELECT ON  [dbo].[aspnet_Profile] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[aspnet_Profile] TO [Ligado_User]
GRANT SELECT ON  [dbo].[aspnet_Profile] TO [Ligado_User]
GRANT INSERT ON  [dbo].[aspnet_Profile] TO [Ligado_User]
GRANT DELETE ON  [dbo].[aspnet_Profile] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[aspnet_Profile] TO [Ligado_User]
GO
