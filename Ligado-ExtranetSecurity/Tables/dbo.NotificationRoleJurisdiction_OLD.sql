CREATE TABLE [dbo].[NotificationRoleJurisdiction_OLD]
(
[NotificationRoleJurisdictionID] [int] NOT NULL IDENTITY(1, 1),
[RolePersonFirmAddressClientSiteID] [bigint] NULL,
[JurisdictionID] [int] NULL,
[CourtTypeID] [int] NULL,
[MatterID] [int] NULL,
[StateID] [int] NULL,
[IsActive] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NotificationRoleJurisdiction_OLD] ADD CONSTRAINT [PK_NotificationRoleJurisdiction] PRIMARY KEY CLUSTERED  ([NotificationRoleJurisdictionID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationRoleJurisdiction_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[NotificationRoleJurisdiction_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[NotificationRoleJurisdiction_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[NotificationRoleJurisdiction_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[NotificationRoleJurisdiction_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[NotificationRoleJurisdiction_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[NotificationRoleJurisdiction_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED? - Looks like this was a table intended to setup mappings for certain roles to receive notification emails for only certain jursidictions', 'SCHEMA', N'dbo', 'TABLE', N'NotificationRoleJurisdiction_OLD', NULL, NULL
GO
