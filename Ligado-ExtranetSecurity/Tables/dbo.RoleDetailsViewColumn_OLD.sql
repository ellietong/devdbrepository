CREATE TABLE [dbo].[RoleDetailsViewColumn_OLD]
(
[RoleDetailsViewColumnID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[DetailsViewColumnID] [bigint] NOT NULL,
[Required] [bit] NOT NULL,
[SortOrder] [int] NOT NULL,
[VisibleNormal] [bit] NULL,
[VisibleEdit] [bit] NULL,
[AllowEntriesToBeInserted] [bit] NOT NULL,
[AllowEntriesToBeUpdated] [bit] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDetailsViewFieldLinkExclude_tblDetailsViewFieldLink] ON [dbo].[RoleDetailsViewColumn_OLD] ([DetailsViewColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDetailsViewFieldLinkExclude_tblRole] ON [dbo].[RoleDetailsViewColumn_OLD] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleDetailsViewColumn_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RoleDetailsViewColumn_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleDetailsViewColumn_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleDetailsViewColumn_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleDetailsViewColumn_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleDetailsViewColumn_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleDetailsViewColumn_OLD] TO [Ligado_User]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps access to a certain column of a detailsview to a role.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'If the column is a dropdown control when in edit mode, this determines whether and "Add" button is rendered next to it to launch an entry dialog for the underlying field that the dropdown represents.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'AllowEntriesToBeInserted'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If the column is a dropdown control when in edit mode, this determines whether an "Edit" button is rendered next to it to launch an Edit dialog of the currently selected item in the dropdown.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'AllowEntriesToBeUpdated'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp smallint
SELECT @xp=2835
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK of the record associating a column with a specific details view, to which access will be granted to any users with this role.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not a red "required" asterisk should appear next to the field in edit mode.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'Required'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleDetailsViewColumnID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleDetailsViewColumnID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleDetailsViewColumnID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleDetailsViewColumnID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleDetailsViewColumnID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleDetailsViewColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RoleID to which the user must have access in order to see this column in the given detailsview.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'RoleID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Order in which the column should be displayed in the detailsview.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'SortOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the column should appear when the record shown in the detailsview is being edited.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the column should appear when the detailsview is rendered normally.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDetailsViewColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
