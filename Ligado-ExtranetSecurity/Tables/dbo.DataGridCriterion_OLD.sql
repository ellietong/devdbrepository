CREATE TABLE [dbo].[DataGridCriterion_OLD]
(
[DataGridCriterionID] [bigint] NOT NULL IDENTITY(1, 1),
[DataGridID] [bigint] NOT NULL,
[CriterionID] [bigint] NOT NULL,
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataGridCriterion_OLD] ADD CONSTRAINT [PK_tblDataGridLink] PRIMARY KEY CLUSTERED  ([DataGridCriterionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDataGridLink_tblDataGridCriterion] ON [dbo].[DataGridCriterion_OLD] ([CriterionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDataGridLink_tblDataGrid] ON [dbo].[DataGridCriterion_OLD] ([DataGridID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataGridCriterion_OLD] ADD CONSTRAINT [FK_tblDataGridLink_tblDataGridCriterion] FOREIGN KEY ([CriterionID]) REFERENCES [dbo].[Criterion_OLD] ([CriterionID])
GO
ALTER TABLE [dbo].[DataGridCriterion_OLD] ADD CONSTRAINT [FK_tblDataGridLink_tblDataGrid] FOREIGN KEY ([DataGridID]) REFERENCES [dbo].[DataGrid_OLD] ([DataGridID])
GO
GRANT VIEW DEFINITION ON  [dbo].[DataGridCriterion_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[DataGridCriterion_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DataGridCriterion_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DataGridCriterion_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[DataGridCriterion_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[DataGridCriterion_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[DataGridCriterion_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps a criterion to a data grid.', 'SCHEMA', N'dbo', 'TABLE', N'DataGridCriterion_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Criterion FK', 'SCHEMA', N'dbo', 'TABLE', N'DataGridCriterion_OLD', 'COLUMN', N'CriterionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'DataGridCriterion_OLD', 'COLUMN', N'DataGridCriterionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Data grid FK', 'SCHEMA', N'dbo', 'TABLE', N'DataGridCriterion_OLD', 'COLUMN', N'DataGridID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Order in which the field should be displayed for the grid.', 'SCHEMA', N'dbo', 'TABLE', N'DataGridCriterion_OLD', 'COLUMN', N'SortOrder'
GO
