CREATE TABLE [dbo].[Definition_OLD]
(
[DefinitionID] [bigint] NOT NULL IDENTITY(1, 1),
[ReportID] [bigint] NOT NULL,
[DefinitionName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefinitionLink] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Definition_OLD] ADD CONSTRAINT [PK_tblDefinition] PRIMARY KEY CLUSTERED  ([DefinitionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDefinition_tblReport] ON [dbo].[Definition_OLD] ([ReportID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Definition_OLD] ADD CONSTRAINT [FK_tblDefinition_tblReport] FOREIGN KEY ([ReportID]) REFERENCES [dbo].[Report_OLD] ([ReportID])
GO
GRANT VIEW DEFINITION ON  [dbo].[Definition_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[Definition_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Definition_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Definition_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Definition_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Definition_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Definition_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED', 'SCHEMA', N'dbo', 'TABLE', N'Definition_OLD', NULL, NULL
GO
