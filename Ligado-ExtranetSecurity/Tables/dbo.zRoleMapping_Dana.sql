CREATE TABLE [dbo].[zRoleMapping_Dana]
(
[OldRoleID] [int] NULL,
[NewRoleID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[zRoleMapping_Dana] TO [base_user]
GRANT SELECT ON  [dbo].[zRoleMapping_Dana] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zRoleMapping_Dana] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zRoleMapping_Dana] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zRoleMapping_Dana] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zRoleMapping_Dana] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zRoleMapping_Dana] TO [Ligado_User]
GO
