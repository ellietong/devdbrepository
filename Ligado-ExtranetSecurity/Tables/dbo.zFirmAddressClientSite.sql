CREATE TABLE [dbo].[zFirmAddressClientSite]
(
[FirmAddressClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[FirmAddressID] [bigint] NOT NULL,
[ClientSiteID] [bigint] NOT NULL,
[Active] [bit] NOT NULL CONSTRAINT [DF_tblFirmAddressCase_Active] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zFirmAddressClientSite] ADD CONSTRAINT [PK_tblFirmAddressCase] PRIMARY KEY CLUSTERED  ([FirmAddressClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblFirmAddressClientSite_tblClientSite] ON [dbo].[zFirmAddressClientSite] ([ClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblFirmAddressCase_tblFirmAddress] ON [dbo].[zFirmAddressClientSite] ([FirmAddressID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zFirmAddressClientSite] ADD CONSTRAINT [FK_tblFirmAddressClientSite_tblClientSite] FOREIGN KEY ([ClientSiteID]) REFERENCES [dbo].[ClientSite_OLD] ([ClientSiteID])
GO
ALTER TABLE [dbo].[zFirmAddressClientSite] WITH NOCHECK ADD CONSTRAINT [FK_tblFirmAddressCase_tblFirmAddress] FOREIGN KEY ([FirmAddressID]) REFERENCES [dbo].[zFirmAddress] ([FirmAddressID])
GO
GRANT VIEW DEFINITION ON  [dbo].[zFirmAddressClientSite] TO [base_user]
GRANT SELECT ON  [dbo].[zFirmAddressClientSite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zFirmAddressClientSite] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zFirmAddressClientSite] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zFirmAddressClientSite] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zFirmAddressClientSite] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zFirmAddressClientSite] TO [Ligado_User]
GO
