CREATE TABLE [dbo].[PersonFirmAddressClientSite_OLD]
(
[PersonFirmAddressClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[PersonID] [bigint] NOT NULL,
[FirmAddressClientSiteID] [bigint] NOT NULL,
[Active] [bit] NOT NULL CONSTRAINT [DF_PersonFirmAddressClientSite_Active_1] DEFAULT ((1)),
[IsPrimary] [bit] NOT NULL CONSTRAINT [DF_PersonFirmAddressClientSite_IsPrimary_1] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PersonFirmAddressClientSite_OLD] ADD CONSTRAINT [PK_tblPersonFirmAddressCaseNew] PRIMARY KEY CLUSTERED  ([PersonFirmAddressClientSiteID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PersonFirmAddressClientSite_OLD] ADD CONSTRAINT [FK_PersonFirmAddressClientSite_FirmAddressClientSite] FOREIGN KEY ([FirmAddressClientSiteID]) REFERENCES [dbo].[FirmAddressClientSite_OLD] ([FirmAddressClientSiteID])
GO
ALTER TABLE [dbo].[PersonFirmAddressClientSite_OLD] ADD CONSTRAINT [FK_PersonFirmAddressClientSite_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person_OLD] ([PersonID])
GO
GRANT VIEW DEFINITION ON  [dbo].[PersonFirmAddressClientSite_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[PersonFirmAddressClientSite_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[PersonFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[PersonFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[PersonFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[PersonFirmAddressClientSite_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[PersonFirmAddressClientSite_OLD] TO [Ligado_User]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps a person who is located at a specific firm address to have access to a specific client site.', 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'Active'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not this permission is actively considered by the site or ignored.', 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'Active'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=2700
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK of the combination of firm address and client site through which this user should have access to the given client site.', 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'FirmAddressClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not this is the user''s primary permissions to a client site for a given firm address.', 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'IsPrimary'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonFirmAddressClientSiteID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Person who has access to the client site as a member of a specific firm addressID', 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'PersonFirmAddressClientSite_OLD', 'COLUMN', N'PersonID'
GO
