CREATE TABLE [dbo].[RoleDataGridCriterion_OLD]
(
[RoleDataGridCriterionID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[DataGridCriterionID] [bigint] NOT NULL,
[SortOrder] [int] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDataGridCriterionLinkExclude_tblDataGridCriterionLink] ON [dbo].[RoleDataGridCriterion_OLD] ([DataGridCriterionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDataGridCriterionLinkExclude_tblRole] ON [dbo].[RoleDataGridCriterion_OLD] ([RoleID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140410-105700] ON [dbo].[RoleDataGridCriterion_OLD] ([RoleID]) INCLUDE ([DataGridCriterionID], [SortOrder]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGridCriterion_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RoleDataGridCriterion_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGridCriterion_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleDataGridCriterion_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleDataGridCriterion_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleDataGridCriterion_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleDataGridCriterion_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps a specific grid criterion to a specific role.  Allows role to show only a subset of the possible criteria associated with the grid.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridCriterion_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK of the record mapping the criterion to a datagrid.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridCriterion_OLD', 'COLUMN', N'DataGridCriterionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridCriterion_OLD', 'COLUMN', N'RoleDataGridCriterionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Role to which the criterion should be mapped for the grid.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridCriterion_OLD', 'COLUMN', N'RoleID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Relative order in which the criterion should be rendered.', 'SCHEMA', N'dbo', 'TABLE', N'RoleDataGridCriterion_OLD', 'COLUMN', N'SortOrder'
GO
