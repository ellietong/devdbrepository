CREATE TABLE [dbo].[DetailsViewColumn_OLD]
(
[DetailsViewColumnID] [bigint] NOT NULL IDENTITY(1, 1),
[DetailsViewID] [bigint] NOT NULL,
[ColumnID] [bigint] NOT NULL,
[DataKey] [bit] NOT NULL CONSTRAINT [DF_DetailsViewColumn_DataKey] DEFAULT ((0)),
[VisibleNormal] [bit] NOT NULL CONSTRAINT [DF_DetailsViewColumn_Visible] DEFAULT ((1)),
[VisibleEdit] [bit] NOT NULL CONSTRAINT [DF_DetailsViewColumn_VisibleEdit] DEFAULT ((0)),
[ReadOnly] [bit] NOT NULL CONSTRAINT [DF_DetailsViewColumn_ReadOnly] DEFAULT ((0)),
[ForeignKeyFlag] [bit] NOT NULL CONSTRAINT [DF_DetailsViewColumn_ForeignKeyFlag] DEFAULT ((0)),
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DetailsViewColumn_OLD] ADD CONSTRAINT [PK_tblDetailsViewField] PRIMARY KEY CLUSTERED  ([DetailsViewColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_DetailsViewColumn_Column] ON [dbo].[DetailsViewColumn_OLD] ([ColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_DetailsViewColumn_DetailsView] ON [dbo].[DetailsViewColumn_OLD] ([DetailsViewID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DetailsViewColumn_OLD] ADD CONSTRAINT [FK_DetailsViewColumn_Column] FOREIGN KEY ([ColumnID]) REFERENCES [dbo].[Column_OLD] ([ColumnID])
GO
ALTER TABLE [dbo].[DetailsViewColumn_OLD] ADD CONSTRAINT [FK_DetailsViewColumn_DetailsView] FOREIGN KEY ([DetailsViewID]) REFERENCES [dbo].[DetailsView_OLD] ([DetailsViewID])
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailsViewColumn_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[DetailsViewColumn_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DetailsViewColumn_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[DetailsViewColumn_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[DetailsViewColumn_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[DetailsViewColumn_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[DetailsViewColumn_OLD] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps a specific column to a details view definition.', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', N'([DetailsViewColumn].[DetailsViewID]="7")', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ColumnID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Column which is mapped to the details view - FK', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DataKey'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DataKey'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DataKey'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DataKey'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DataKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'TODO', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DataKey'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DataKey'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DetailsView to which the column is mapped - FK', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'DetailsViewID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
EXEC sp_addextendedproperty N'MS_Description', N'TODO', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ForeignKeyFlag'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ReadOnly'
GO
EXEC sp_addextendedproperty N'MS_Description', N'TODO', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'ReadOnly'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'SortOrder'
GO
EXEC sp_addextendedproperty N'MS_Description', N'TODO', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'SortOrder'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
EXEC sp_addextendedproperty N'MS_Description', N'TODO', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleEdit'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
EXEC sp_addextendedproperty N'MS_Description', N'TODO', 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'DetailsViewColumn_OLD', 'COLUMN', N'VisibleNormal'
GO
