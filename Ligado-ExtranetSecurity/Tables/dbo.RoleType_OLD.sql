CREATE TABLE [dbo].[RoleType_OLD]
(
[RoleType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleType_OLD] ADD CONSTRAINT [PK_RoleType] PRIMARY KEY CLUSTERED  ([RoleType]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleType_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[RoleType_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleType_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleType_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleType_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleType_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleType_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lookup table of possible role types to assign to roles', 'SCHEMA', N'dbo', 'TABLE', N'RoleType_OLD', NULL, NULL
GO
