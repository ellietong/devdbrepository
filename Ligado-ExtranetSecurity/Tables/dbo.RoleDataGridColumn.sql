CREATE TABLE [dbo].[RoleDataGridColumn]
(
[RoleDataGridColumnID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[DataGridColumnID] [bigint] NOT NULL,
[Required] [bit] NOT NULL CONSTRAINT [DF_RoleDataGridColumn_Required] DEFAULT ((0)),
[SortOrder] [int] NOT NULL CONSTRAINT [DF_RoleDataGridColumn_SortOrder] DEFAULT ((0)),
[VisibleNormal] [bit] NULL CONSTRAINT [DF_RoleDataGridColumn_VisibleNormal] DEFAULT ((0)),
[VisibleEdit] [bit] NULL CONSTRAINT [DF_RoleDataGridColumn_VisibleEdit] DEFAULT ((0)),
[VisibleCreate] [bit] NOT NULL CONSTRAINT [DF_RoleDataGridColumn_VisibleCreate] DEFAULT ((0)),
[ReadOnly] [bit] NULL CONSTRAINT [DF_RoleDataGridColumn_ReadOnly_1] DEFAULT ((0)),
[AllowEntriesToBeInserted] [bit] NOT NULL CONSTRAINT [DF_RoleDataGridColumn_AllowEntriesToBeInserted] DEFAULT ((0)),
[AllowEntriesToBeUpdated] [bit] NOT NULL CONSTRAINT [DF_RoleDataGridColumn_AllowEntriesToBeUpdated] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoleDataGridColumn] ADD CONSTRAINT [PK_tblRoleDataGridColumnLinkExclude] PRIMARY KEY CLUSTERED  ([RoleDataGridColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDataGridColumnLinkExclude_tblDataGridColumnLink] ON [dbo].[RoleDataGridColumn] ([DataGridColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblRoleDataGridColumnLinkExclude_tblRole] ON [dbo].[RoleDataGridColumn] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGridColumn] TO [base_user]
GRANT SELECT ON  [dbo].[RoleDataGridColumn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RoleDataGridColumn] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RoleDataGridColumn] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RoleDataGridColumn] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RoleDataGridColumn] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RoleDataGridColumn] TO [Ligado_User]
GO
