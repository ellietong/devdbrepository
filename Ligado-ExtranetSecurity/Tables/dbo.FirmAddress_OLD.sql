CREATE TABLE [dbo].[FirmAddress_OLD]
(
[FirmAddressID] [bigint] NOT NULL IDENTITY(1, 1),
[FirmID] [int] NOT NULL,
[FirmAddress1] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmAddress2] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmCity] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmStateID] [int] NULL,
[FirmZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmMailZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmPhone] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmFax] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayeeAddress] [bit] NOT NULL,
[TaxID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmAddress_OLD] ADD CONSTRAINT [PK_FirmAddress] PRIMARY KEY CLUSTERED  ([FirmAddressID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirmAddress_OLD] ADD CONSTRAINT [FK_FirmAddress_Firm] FOREIGN KEY ([FirmID]) REFERENCES [dbo].[Firm_OLD] ([FirmID])
GO
GRANT VIEW DEFINITION ON  [dbo].[FirmAddress_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[FirmAddress_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[FirmAddress_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[FirmAddress_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[FirmAddress_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[FirmAddress_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[FirmAddress_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains address and TaxID information for a firm''s office locations.', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress1'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress1'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress1'
GO
DECLARE @xp int
SELECT @xp=4590
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Address info', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress1'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress1'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress2'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress2'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress2'
GO
DECLARE @xp int
SELECT @xp=3075
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Address info', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress2'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddress2'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddressID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddressID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddressID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddressID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddressID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmAddressID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmCity'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmCity'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmCity'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmCity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Address info', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmCity'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmCity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Address info', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmFax'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Parent Firm of which this Firm Address is an office location.', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmMailZip'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmMailZip'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmMailZip'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmMailZip'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Address info', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmMailZip'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmMailZip'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmPhone'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmPhone'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmPhone'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmPhone'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Address info', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmPhone'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmPhone'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmStateID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmStateID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmStateID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmStateID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Address info', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmStateID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmStateID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmZip'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmZip'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmZip'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmZip'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Address info', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmZip'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'FirmZip'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether the payment module should utilize this address information for the address to which checks are sent.', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'PayeeAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The TIN of this firm for Medicare reporting and 1099 purposes.', 'SCHEMA', N'dbo', 'TABLE', N'FirmAddress_OLD', 'COLUMN', N'TaxID'
GO
