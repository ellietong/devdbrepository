CREATE TABLE [dbo].[QueryStringParameter_OLD]
(
[QueryStringParameterID] [bigint] NOT NULL IDENTITY(1, 1),
[QueryString] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParameterName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QueryStringParameter_OLD] ADD CONSTRAINT [PK_tblQueryStringParameter] PRIMARY KEY CLUSTERED  ([QueryStringParameterID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameter_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[QueryStringParameter_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[QueryStringParameter_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[QueryStringParameter_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[QueryStringParameter_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[QueryStringParameter_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[QueryStringParameter_OLD] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Defines what parameters from the URL that should be considered by a details view or data grid.  UNUSED? We may not even use the information stored within this at this point, just given how we setup controls.', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'ParameterName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'ParameterName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'ParameterName'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'ParameterName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'ParameterName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Field name that the data pulled from the query string should be passed as.', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'ParameterName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'ParameterName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryString'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryString'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryString'
GO
DECLARE @xp smallint
SELECT @xp=2385
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryString'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryString'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Field name to parse from the query string', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryString'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryString'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryStringParameterID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryStringParameterID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryStringParameterID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryStringParameterID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryStringParameterID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryStringParameterID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'QueryStringParameter_OLD', 'COLUMN', N'QueryStringParameterID'
GO
