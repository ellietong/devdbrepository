CREATE TABLE [dbo].[aspnet_Users]
(
[ApplicationId] [uniqueidentifier] NOT NULL,
[UserId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__aspnet_Us__UserI__084B3915] DEFAULT (newid()),
[UserName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoweredUserName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MobileAlias] [nvarchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__aspnet_Us__Mobil__093F5D4E] DEFAULT (NULL),
[IsAnonymous] [bit] NOT NULL CONSTRAINT [DF__aspnet_Us__IsAno__0A338187] DEFAULT ((0)),
[LastActivityDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Users] ADD CONSTRAINT [PK__aspnet_U__1788CC4D056ECC6A] PRIMARY KEY NONCLUSTERED  ([UserId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__aspnet_Us__Appli__075714DC] ON [dbo].[aspnet_Users] ([ApplicationId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users] ([ApplicationId], [LastActivityDate]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Users_Index] ON [dbo].[aspnet_Users] ([ApplicationId], [LoweredUserName]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_Users] ADD CONSTRAINT [FK_aspnet_Users_aspnet_Applications] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_Users] TO [base_user]
GRANT SELECT ON  [dbo].[aspnet_Users] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[aspnet_Users] TO [Ligado_User]
GRANT SELECT ON  [dbo].[aspnet_Users] TO [Ligado_User]
GRANT INSERT ON  [dbo].[aspnet_Users] TO [Ligado_User]
GRANT DELETE ON  [dbo].[aspnet_Users] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[aspnet_Users] TO [Ligado_User]
GO
