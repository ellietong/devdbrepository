CREATE TABLE [dbo].[zRoleMapping_Demo]
(
[OldRoleID] [int] NULL,
[NewRoleID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[zRoleMapping_Demo] TO [base_user]
GRANT SELECT ON  [dbo].[zRoleMapping_Demo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zRoleMapping_Demo] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zRoleMapping_Demo] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zRoleMapping_Demo] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zRoleMapping_Demo] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zRoleMapping_Demo] TO [Ligado_User]
GO
