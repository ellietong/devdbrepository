CREATE TABLE [dbo].[PasswordQuestion_OLD]
(
[PasswordQuestionID] [int] NOT NULL IDENTITY(1, 1),
[PasswordQuestion] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PasswordQuestion_OLD] ADD CONSTRAINT [PK_tblPasswordQuestion] PRIMARY KEY CLUSTERED  ([PasswordQuestionID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[PasswordQuestion_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[PasswordQuestion_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[PasswordQuestion_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[PasswordQuestion_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[PasswordQuestion_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[PasswordQuestion_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[PasswordQuestion_OLD] TO [Ligado_User]
GO
EXEC sp_addextendedproperty N'MS_Description', N'UNUSED - we no longer use a password question in order to reset users'' passwords.', 'SCHEMA', N'dbo', 'TABLE', N'PasswordQuestion_OLD', NULL, NULL
GO
