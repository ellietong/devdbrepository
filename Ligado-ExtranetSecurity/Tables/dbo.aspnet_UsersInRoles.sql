CREATE TABLE [dbo].[aspnet_UsersInRoles]
(
[UserId] [uniqueidentifier] NOT NULL,
[RoleId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_UsersInRoles] ADD CONSTRAINT [PK__aspnet_U__AF2760AD4183B671] PRIMARY KEY CLUSTERED  ([UserId], [RoleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [aspnet_UsersInRoles_index] ON [dbo].[aspnet_UsersInRoles] ([RoleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__aspnet_Us__RoleI__4460231C] ON [dbo].[aspnet_UsersInRoles] ([RoleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__aspnet_Us__UserI__436BFEE3] ON [dbo].[aspnet_UsersInRoles] ([UserId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[aspnet_UsersInRoles] ADD CONSTRAINT [FK__aspnet_Us__RoleI__4460231C] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[aspnet_UsersInRoles] WITH NOCHECK ADD CONSTRAINT [FK__aspnet_Us__UserI__436BFEE3] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[aspnet_UsersInRoles] NOCHECK CONSTRAINT [FK__aspnet_Us__UserI__436BFEE3]
GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_UsersInRoles] TO [base_user]
GRANT SELECT ON  [dbo].[aspnet_UsersInRoles] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[aspnet_UsersInRoles] TO [Ligado_User]
GRANT SELECT ON  [dbo].[aspnet_UsersInRoles] TO [Ligado_User]
GRANT INSERT ON  [dbo].[aspnet_UsersInRoles] TO [Ligado_User]
GRANT DELETE ON  [dbo].[aspnet_UsersInRoles] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[aspnet_UsersInRoles] TO [Ligado_User]
GO
