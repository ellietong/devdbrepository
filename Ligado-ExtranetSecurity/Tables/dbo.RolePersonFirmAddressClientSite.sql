CREATE TABLE [dbo].[RolePersonFirmAddressClientSite]
(
[RolePersonFirmAddressClientSiteID] [bigint] NOT NULL IDENTITY(1, 1),
[RoleID] [bigint] NOT NULL,
[PersonFirmAddressClientSiteID] [bigint] NOT NULL,
[CanEdit] [bit] NOT NULL CONSTRAINT [DF_tblRolePersonFirmAddressCase_CanEdit] DEFAULT ((0)),
[CanAdd] [bit] NOT NULL CONSTRAINT [DF_tblRolePersonFirmAddressCase_CanAdd] DEFAULT ((0)),
[CanDelete] [bit] NOT NULL CONSTRAINT [DF_tblRolePersonFirmAddressCase_CanDelete] DEFAULT ((0)),
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[RolePersonFirmAddressClientSite] ADD CONSTRAINT [PK_tblPersonFirmAddressCaseRole] PRIMARY KEY CLUSTERED  ([RolePersonFirmAddressClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPersonFirmAddressCaseRole_tblPersonFirmAddressCase1] ON [dbo].[RolePersonFirmAddressClientSite] ([PersonFirmAddressClientSiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPersonFirmAddressCaseRole_tblRole] ON [dbo].[RolePersonFirmAddressClientSite] ([RoleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RolePersonFirmAddressClientSite] TO [base_user]
GRANT SELECT ON  [dbo].[RolePersonFirmAddressClientSite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RolePersonFirmAddressClientSite] TO [Ligado_User]
GRANT SELECT ON  [dbo].[RolePersonFirmAddressClientSite] TO [Ligado_User]
GRANT INSERT ON  [dbo].[RolePersonFirmAddressClientSite] TO [Ligado_User]
GRANT DELETE ON  [dbo].[RolePersonFirmAddressClientSite] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[RolePersonFirmAddressClientSite] TO [Ligado_User]
GO
