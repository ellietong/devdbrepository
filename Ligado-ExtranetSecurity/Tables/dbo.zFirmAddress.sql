CREATE TABLE [dbo].[zFirmAddress]
(
[FirmAddressID] [bigint] NOT NULL IDENTITY(1, 1),
[FirmID] [int] NOT NULL,
[FirmAddress1] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmAddress2] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmCity] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmStateID] [int] NULL,
[FirmZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmMailZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmPhone] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmFax] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayeeAddress] [bit] NOT NULL CONSTRAINT [DF_tblFirmAddress_PayeeAddress] DEFAULT ((0)),
[TaxID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[zFirmAddress] ADD CONSTRAINT [PK_tblFirmAddress] PRIMARY KEY CLUSTERED  ([FirmAddressID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblFirmAddress_tblFirm] ON [dbo].[zFirmAddress] ([FirmID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zFirmAddress] ADD CONSTRAINT [FK_tblFirmAddress_tblFirm] FOREIGN KEY ([FirmID]) REFERENCES [dbo].[zFirm] ([FirmID])
GO
GRANT VIEW DEFINITION ON  [dbo].[zFirmAddress] TO [base_user]
GRANT SELECT ON  [dbo].[zFirmAddress] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zFirmAddress] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zFirmAddress] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zFirmAddress] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zFirmAddress] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zFirmAddress] TO [Ligado_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddress1'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddress1'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddress1'
GO
DECLARE @xp smallint
SELECT @xp=4590
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddress1'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddress1'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddress2'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddress2'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddress2'
GO
DECLARE @xp smallint
SELECT @xp=3075
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddress2'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddress2'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddressID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddressID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddressID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddressID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmAddressID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmCity'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmCity'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmCity'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmCity'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmCity'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmMailZip'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmMailZip'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmMailZip'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmMailZip'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmMailZip'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmPhone'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmPhone'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmPhone'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmPhone'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmPhone'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmStateID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmStateID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmStateID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmStateID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmStateID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmZip'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmZip'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmZip'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmZip'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'zFirmAddress', 'COLUMN', N'FirmZip'
GO
