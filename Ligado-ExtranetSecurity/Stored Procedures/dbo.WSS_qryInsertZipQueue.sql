SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInsertZipQueue]
(
	@ZipQueueID uniqueidentifier, 
	@Bucket nvarchar(100),
	@Emailaddress nvarchar(250) = NULL)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO ZipQueue (ZipQueueID, AddedToQueue, Bucket, CreatedBy)
	SELECT @ZipQueueID, GETDATE(), @Bucket, @Emailaddress
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsertZipQueue] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_qryInsertZipQueue] TO [Ligado_User]
GRANT ALTER ON  [dbo].[WSS_qryInsertZipQueue] TO [Zip_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsertZipQueue] TO [Zip_User]
GRANT EXECUTE ON  [dbo].[WSS_qryInsertZipQueue] TO [Zip_User]
GO
