SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_AMAZON_qryTestZipWorker] (@email nvarchar(250) = 'crookc@kcicllc.com')
AS
BEGIN
	INSERT INTO ZipQueue (Bucket, CreatedBy, AddedToQueue)
	SELECT 'KCICITT', @email, GETDATE()
	
	DECLARE @ZipQueueID nvarchar(250) = (SELECT ZipQueueID FROM ZipQueue WHERE AddedToQueue = (SELECT MAX(AddedToQueue) FROM ZipQueue))
	PRINT @ZipQueueID
	INSERT INTO ZipQueueDocument
	SELECT @ZipQueueID, 'PRODUCTION/CLAIMSADMIN/COMPLAINT/McAuley_Illinois_184854.pdf'
	UNION
	SELECT @ZipQueueID, 'PRODUCTION/CLAIMSADMIN/COMPLAINT/Goldberg_Illinois_184831.pdf'
	
	
END

GO
GRANT ALTER ON  [dbo].[WSS_AMAZON_qryTestZipWorker] TO [Zip_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_AMAZON_qryTestZipWorker] TO [Zip_User]
GRANT EXECUTE ON  [dbo].[WSS_AMAZON_qryTestZipWorker] TO [Zip_User]
GO
