SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_IsUserInRole]
@username nvarchar(80),
@rolename nvarchar(255)
AS
BEGIN

	select
		count(*)
	from
		WSS_GetAllUserRoles
	where
		email = @username and
		rolename = @rolename

END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_IsUserInRole] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_IsUserInRole] TO [base_user]
GO
