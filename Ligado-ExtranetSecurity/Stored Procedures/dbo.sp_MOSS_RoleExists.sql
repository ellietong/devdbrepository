SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_RoleExists]
@rolename nvarchar(255)
AS
BEGIN

	select
		rolename
	from
		v_MOSS_GetAllRoles
	where
		rolename = @rolename

END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_RoleExists] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_RoleExists] TO [base_user]
GO
