SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryUserSearch] (@Username nvarchar(200))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM Person p
	INNER JOIN aspnet_Membership m ON p.MembershipID = m.UserId
	INNER JOIN aspnet_Users u ON m.UserId = u.UserId AND m.ApplicationId = u.ApplicationId
	WHERE p.Email LIKE '%' + COALESCE(@Username, '') + '%'
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryUserSearch] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryUserSearch] TO [Ligado_User]
GO
