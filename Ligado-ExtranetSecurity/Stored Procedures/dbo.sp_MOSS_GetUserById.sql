SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_GetUserById]
@id int,
@isOnline bit = null
AS
BEGIN

select
	email as username, 
	displayname, 
	email,
	personid
from
	v_MOSS_GetAllUsers
where
	personid = @id

END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_GetUserById] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_GetUserById] TO [base_user]
GO
