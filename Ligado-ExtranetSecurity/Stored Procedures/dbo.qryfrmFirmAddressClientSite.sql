SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmFirmAddressClientSite] (@FirmAddressID int)
AS
SET NOCOUNT ON

DECLARE @FirmAddressClientSites TABLE (FirmAddressClientSiteID int, FormSort int IDENTITY(1,1))
INSERT INTO @FirmAddressClientSites
	SELECT FirmAddressClientSiteID
	FROM FirmAddressClientSite rcs
		INNER JOIN ClientSite cs ON rcs.ClientSiteID = cs.ClientSiteID
		INNER JOIN Client c ON cs.ClientID = c.ClientID
		INNER JOIN [Site] s ON cs.SiteID = s.SiteID
	WHERE FirmAddressID = @FirmAddressID
	ORDER BY ClientName, SiteName

SELECT facs.*
FROM FirmAddressClientSite facs
	LEFT JOIN @FirmAddressClientSites facss ON facs.FirmAddressClientSiteID = facss.FirmAddressClientSiteID 
WHERE FirmAddressID = @FirmAddressID
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmFirmAddressClientSite] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmFirmAddressClientSite] TO [Ligado_User]
GO
