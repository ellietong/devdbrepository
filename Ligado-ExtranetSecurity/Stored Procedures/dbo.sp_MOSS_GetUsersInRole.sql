SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_GetUsersInRole]
@rolename nvarchar(255)
AS
BEGIN

	select
		email as username
	from
		WSS_GetAllUserRoles
	where
		rolename = @rolename

END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_GetUsersInRole] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_GetUsersInRole] TO [base_user]
GO
