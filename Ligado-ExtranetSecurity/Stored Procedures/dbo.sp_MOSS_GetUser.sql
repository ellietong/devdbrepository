SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_GetUser]
@username nvarchar(80),
@isOnline bit = null
AS
BEGIN

select
	email as username, 
	Fname + ' ' + Lname AS displayname, 
	email,
	personid,
	COALESCE(lastlogindate, 1/1/1900) AS LastLoginDate,
	COALESCE(lastpasswordchangeddate, 1/1/1900) AS lastpasswordchangeddate,
	1 AS status,
	COALESCE(IsLockedOut, 0) AS IsLockedOut
	--PasswordQuestion,
	--TermsAcceptedDate
from
	vPerson p
	--LEFT JOIN tblPasswordQuestion pq ON p.PasswordQuestionID = p.PasswordQuestionID
where
	email = @username

END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_GetUser] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_GetUser] TO [base_user]
GO
