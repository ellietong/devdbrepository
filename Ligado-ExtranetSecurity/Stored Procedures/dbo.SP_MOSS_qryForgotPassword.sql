SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_MOSS_qryForgotPassword]
(@username nvarchar(200))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--if it's the user's first failed password recovery attempt, set the window start time
	IF(SELECT COALESCE([FailedPasswordAnswer-AttemptCount], 0) FROM vPerson WHERE Email = @username) = 0
	BEGIN
		UPDATE vPerson
		SET [FailedPasswordAttempt-WindowStart] = GETDATE()
		WHERE Email = @username
	END
    -- Insert statements for procedure here
	SELECT FName, Email as Username, fa.FirmStateID AS StateID, p.PasswordQuestionID, PasswordQuestion, PasswordAnswer, [FailedPasswordAnswer-AttemptWindowStart], COALESCE([FailedPasswordAnswer-AttemptCount], 0) AS [FailedPasswordAnswer-AttemptCount] FROM vPerson p
	LEFT JOIN tblFirmAddress fa ON p.FirmAddressID = fa.FirmAddressID
	LEFT JOIN tblPasswordQuestion pq ON p.PasswordQuestionID = pq.PasswordQuestionID
	WHERE Email = @username
	
	IF(SELECT COALESCE([FailedPasswordAnswer-AttemptCount], 0) FROM vPerson WHERE Email = @username AND DATEDIFF(MI, [FailedPasswordAnswer-AttemptWindowStart], GETDATE())<=10 ) >=5
	BEGIN
		UPDATE vPerson
		SET IsLockedOut = 1,
			LastLockoutDate = GETDATE()
		WHERE Email = @username
		
		DECLARE @Subject nvarchar(500)
		SET @Subject = (SELECT 'User Locked Out: ' + @username)
		PRINT 'test'
		EXEC msdb.dbo.sp_send_dbmail
		--The NSIProfile sends from NSI@kcicllc.com
		@profile_name = 'Admin',
		@recipients = 'support@kcicllc.com',
		@blind_copy_recipients = '',
		@subject = @Subject,
		@body = '<html><body></body></html>',
		@body_format = 'html' 
	END
END

GO
