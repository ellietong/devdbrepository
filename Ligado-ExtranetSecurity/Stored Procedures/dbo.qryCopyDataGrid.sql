SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryCopyDataGrid] (@DataGridID int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @DataGridID int = 4

--STEP 1: Duplicate the data grid itself in table DataGrid
INSERT INTO DataGrid (DataGridName, DisplayName, AjaxSource, ServiceObject, ServiceMethod, DataObjectTypeName, [Include], Notes, UpdateProc, DeleteProc, InsertProc, SelectProc, PageSize, DataGridTypeID, EmptyDataText, EnableRowSelection)
	SELECT DataGridName + ' (Copy)' AS DataGridName, DisplayName, AjaxSource, ServiceObject, ServiceMethod, DataObjectTypeName, [Include], Notes, UpdateProc, DeleteProc, InsertProc, SelectProc, PageSize, DataGridTypeID, EmptyDataText, EnableRowSelection
	FROM DataGrid
	WHERE DataGridID = @DataGridID

--STEP 2: Get the DataGridID for the newly created duplicate
DECLARE @NewDataGridID int
SELECT @NewDataGridID = MAX(DataGridID) FROM DataGrid

--STEP 3: Duplicate the data grid settings in table DataGridColumn
INSERT INTO DataGridColumn (DataGridID, ColumnID, DataKeyField, VisibleNormal, VisibleEdit, [ReadOnly], ForeignKeyFlag, SortOrder)
	SELECT @NewDataGridID, ColumnID, DataKeyField, VisibleNormal, VisibleEdit, [ReadOnly], ForeignKeyFlag, SortOrder
	FROM DataGridColumn
	WHERE DataGridID = @DataGridID

--STEP 4: Duplicate the data grid settings in table DataGridCriteria
INSERT INTO DataGridCriterion (DataGridID, CriterionID, SortOrder)
	SELECT @NewDataGridID, CriterionID, SortOrder
	FROM DataGridCriterion
	WHERE DataGridID = @DataGridID

--STEP 5: Duplicate the data grid settings in table QueryStringParameterDataGrid
INSERT INTO QueryStringParameterDataGrid (DataGridID, QueryStringParameterID)
	SELECT @NewDataGridID, QueryStringParameterID
	FROM QueryStringParameterDataGrid
	WHERE DataGridID = @DataGridID
GO
GRANT VIEW DEFINITION ON  [dbo].[qryCopyDataGrid] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryCopyDataGrid] TO [Ligado_User]
GO
