SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInsertZipQueueDocument](@ZipQueueID uniqueidentifier, @Key nvarchar(MAX))
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO ZipQueueDocument (ZipQueueID, [Key])
	SELECT @ZipQueueID, @Key
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsertZipQueueDocument] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_qryInsertZipQueueDocument] TO [Ligado_User]
GRANT ALTER ON  [dbo].[WSS_qryInsertZipQueueDocument] TO [Zip_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsertZipQueueDocument] TO [Zip_User]
GRANT EXECUTE ON  [dbo].[WSS_qryInsertZipQueueDocument] TO [Zip_User]
GO
