SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_FindUsersByName]
@username nvarchar(80)
AS
BEGIN

select
	Email AS username,
	PersonId,
	DisplayName,
	email
FROM v_MOSS_GetAllUsers
WHERE
	email = @username 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_FindUsersByName] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_FindUsersByName] TO [base_user]
GO
