SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qryUpdatePermissionsForFirmAddressClientSite_Add] (@FirmAddressClientSiteID int = NULL, @UpdateType int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @FirmAddressClientSiteID int = 4
--DECLARE @UpdateType int = 1

IF @UpdateType = 1
BEGIN
	UPDATE RoleFirmAddressClientSite
		SET CanAdd = 1
		WHERE FirmAddressClientSiteID = @FirmAddressClientSiteID
END

IF @UpdateType = 0
BEGIN
	UPDATE RoleFirmAddressClientSite
		SET CanAdd = 0
		WHERE FirmAddressClientSiteID = @FirmAddressClientSiteID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryUpdatePermissionsForFirmAddressClientSite_Add] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryUpdatePermissionsForFirmAddressClientSite_Add] TO [Ligado_User]
GO
