SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmUserPlaintiffCounselAddress]
AS
SET NOCOUNT ON
BEGIN

SELECT PlaintiffCounselAddressID,
	PlaintiffCounsel + ' - ' + COALESCE(PlaintiffCounselCity, 'Unknown') + ' (' + CONVERT(nvarchar(10),PlaintiffCounselAddressID) + ')' AS PlaintiffCounselAddress
FROM [KGA-ClaimsAdmin].[dbo].tblPlaintiffCounselAddress pca
	INNER JOIN [KGA-ClaimsAdmin].[dbo].tblPlaintiffCounsel pc ON pca.PlaintiffCounselID = pc.PlaintiffCounselID
	LEFT  JOIN [KGA-ClaimsAdmin].[dbo].tblState s ON pca.PlaintiffCounselStateID = s.StateID
ORDER BY PlaintiffCounsel, PlaintiffCounselCity

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUserPlaintiffCounselAddress] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUserPlaintiffCounselAddress] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUserPlaintiffCounselAddress] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmUserPlaintiffCounselAddress] TO [Ligado_User]
GO
