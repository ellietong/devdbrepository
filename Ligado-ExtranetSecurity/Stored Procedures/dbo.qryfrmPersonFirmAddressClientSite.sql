SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmPersonFirmAddressClientSite] (@PersonID int)
AS
SET NOCOUNT ON

DECLARE @PersonFirmAddressClientSites TABLE (PersonFirmAddressClientSiteID int, FormSort int IDENTITY(1,1))
INSERT INTO @PersonFirmAddressClientSites
	SELECT PersonFirmAddressClientSiteID
	FROM PersonFirmAddressClientSite pfacs
		INNER JOIN FirmAddressClientSite facs ON pfacs.FirmAddressClientSiteID = facs.FirmAddressClientSiteID
		INNER JOIN ClientSite            cs   ON facs.ClientSiteID = cs.ClientSiteID
		INNER JOIN Client                c    ON cs.ClientID = c.ClientID
		INNER JOIN [Site]                s    ON cs.SiteID = s.SiteID
	WHERE PersonID = @PersonID
	ORDER BY ClientName, FirmAddressID, SiteName

SELECT pfacs.*
FROM PersonFirmAddressClientSite pfacs
	LEFT JOIN @PersonFirmAddressClientSites pfacss ON pfacs.PersonFirmAddressClientSiteID = pfacss.PersonFirmAddressClientSiteID 
WHERE PersonID = @PersonID
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPersonFirmAddressClientSite] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPersonFirmAddressClientSite] TO [Ligado_User]
GO
