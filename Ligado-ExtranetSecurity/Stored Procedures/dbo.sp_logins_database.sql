SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [dbo].[sp_logins_database] @login sysname = null

as

declare @cmd varchar(200),@database sysname

create table #logins_users

(database_name varchar(100),

username varchar(100),

loginname varchar(100))

declare cur cursor for

select name from master..sysdatabases

open cur

fetch next from cur into @database

WHILE @@FETCH_STATUS = 0

begin

set @cmd = 'select '''+@database+''', l.loginname as [login name],u.name as [user name] from '+@database+'..sysusers u inner join master..syslogins l

on u.sid=l.sid'

insert into #logins_users

exec (@cmd)

fetch next from cur into @database

end 

close cur

deallocate cur

if @login is null

select loginname,count(*) from #logins_users

group by loginname

order by loginname

if @login is not null

select loginname,count(*) from #logins_users

where loginname = @login

group by loginname

order by loginname

GO
