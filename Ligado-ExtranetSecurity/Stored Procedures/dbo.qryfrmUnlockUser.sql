SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmUnlockUser]
AS
SET NOCOUNT ON
BEGIN

SELECT PersonID, LastName + ', ' + FirstName AS Name, Email
FROM Person p
WHERE PasswordReset = 1
ORDER BY Name

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnlockUser] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmUnlockUser] TO [Ligado_User]
GO
