SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetZipQueueDocuments] 
(@ZipQueueID uniqueidentifier)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM ZipQueueDocument
	WHERE ZipQueueID = @ZipQueueID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetZipQueueDocuments] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_qryGetZipQueueDocuments] TO [Ligado_User]
GRANT ALTER ON  [dbo].[WSS_qryGetZipQueueDocuments] TO [Zip_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetZipQueueDocuments] TO [Zip_User]
GRANT EXECUTE ON  [dbo].[WSS_qryGetZipQueueDocuments] TO [Zip_User]
GO
