SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmRolePersonFirmAddressClientSite] (@PersonFirmAddressClientSiteID int)
AS
SET NOCOUNT ON

DECLARE @RolePersonFirmAddressClientSites TABLE (RolePersonFirmAddressClientSiteID int, FormSort int IDENTITY(1,1))
INSERT INTO @RolePersonFirmAddressClientSites
	SELECT RolePersonFirmAddressClientSiteID
	FROM RolePersonFirmAddressClientSite rpfacs
		INNER JOIN [Role] r ON rpfacs.RoleID = r.RoleID
	WHERE PersonFirmAddressClientSiteID = @PersonFirmAddressClientSiteID
	ORDER BY RoleName

SELECT rpfacs.*
FROM RolePersonFirmAddressClientSite rpfacs
	LEFT JOIN @RolePersonFirmAddressClientSites rpfacss ON rpfacs.RolePersonFirmAddressClientSiteID = rpfacss.RolePersonFirmAddressClientSiteID 
WHERE PersonFirmAddressClientSiteID = @PersonFirmAddressClientSiteID
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmRolePersonFirmAddressClientSite] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmRolePersonFirmAddressClientSite] TO [Ligado_User]
GO
