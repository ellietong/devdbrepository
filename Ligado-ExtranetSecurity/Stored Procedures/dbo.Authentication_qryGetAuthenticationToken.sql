SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Authentication_qryGetAuthenticationToken]
(@Token uniqueidentifier)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Username nvarchar(250) = null

    --Get the Username for the provided token if it is unused
	SET @Username = (SELECT Username FROM AuthenticationToken
	WHERE IsUsed = 0
	AND AuthenticationTokenID = @Token
	AND ExpirationDate >= GETDATE())

	--Update the record marking it as used
	UPDATE AuthenticationToken
	SET IsUsed = 1
	WHERE AuthenticationTokenID = @Token

	SELECT @Username
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Authentication_qryGetAuthenticationToken] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[Authentication_qryGetAuthenticationToken] TO [Ligado_User]
GO
