SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryAddAllDataGridColumns] (@RoleID int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @RoleID int = 28

INSERT INTO RoleDataGridColumn (RoleID, DataGridColumnID)
	SELECT @RoleID AS RoleID, DataGridColumnID
	FROM DataGridColumn
	WHERE DataGridID IN(SELECT DataGridID
						FROM RoleDataGrid
						WHERE RoleID = @RoleID)
		AND DataGridColumnID NOT IN(SELECT DataGridColumnID
									FROM RoleDataGridColumn
									WHERE RoleID = @RoleID)
GO
GRANT VIEW DEFINITION ON  [dbo].[qryAddAllDataGridColumns] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryAddAllDataGridColumns] TO [Ligado_User]
GO
