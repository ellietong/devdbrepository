SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryWriteZipKey] (@ZipQueueID uniqueidentifier, @ZipKey nvarchar(MAX))
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE ZipQueue
	SET ZipKey = @ZipKey,
		EndTime = GETDATE()
	WHERE ZipQueueID = @ZipQueueID
			
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryWriteZipKey] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_qryWriteZipKey] TO [Ligado_User]
GRANT ALTER ON  [dbo].[WSS_qryWriteZipKey] TO [Zip_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryWriteZipKey] TO [Zip_User]
GRANT EXECUTE ON  [dbo].[WSS_qryWriteZipKey] TO [Zip_User]
GO
