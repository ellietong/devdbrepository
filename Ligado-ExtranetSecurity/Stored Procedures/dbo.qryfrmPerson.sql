SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qryfrmPerson]
AS
SET NOCOUNT ON

SELECT *
FROM Person p
ORDER BY LastName


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPerson] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPerson] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPerson] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPerson] TO [Ligado_User]
GO
