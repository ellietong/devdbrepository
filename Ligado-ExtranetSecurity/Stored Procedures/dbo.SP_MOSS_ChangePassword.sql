SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_MOSS_ChangePassword]
(@password nvarchar(200), @username nvarchar(200))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF (SELECT COALESCE(PasswordReset, 0) FROM vPerson WHERE Email = @username) = 1
    BEGIN
		UPDATE vPerson
		SET PasswordReset = 0
		WHERE Email = @username
    END
     IF (SELECT COALESCE(IsLockedOut, 0) FROM vPerson WHERE Email = @username) = 1
    BEGIN
		UPDATE vPerson
		SET IsLockedOut = 0
		WHERE Email = @username
    END
    
	UPDATE vPerson
	SET [Password] = @password,
	[LastPasswordChangedDate] = GETDATE()
	WHERE @username = Email
END

GO
