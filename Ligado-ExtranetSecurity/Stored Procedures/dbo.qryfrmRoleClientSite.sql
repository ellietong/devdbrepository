SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmRoleClientSite] (@ClientSiteID int)
AS
SET NOCOUNT ON

DECLARE @RoleClientSites TABLE (RoleClientSiteID int, FormSort int IDENTITY(1,1))
INSERT INTO @RoleClientSites
	SELECT RoleClientSiteID
	FROM RoleClientSite rcs
		INNER JOIN [Role] r ON rcs.RoleID = r.RoleID
	WHERE ClientSiteID = @ClientSiteID
	ORDER BY RoleName

SELECT rcs.*
FROM RoleClientSite rcs
	LEFT JOIN @RoleClientSites rcss ON rcs.RoleClientSiteID = rcss.RoleClientSiteID 
WHERE ClientSiteID = @ClientSiteID
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmRoleClientSite] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmRoleClientSite] TO [Ligado_User]
GO
