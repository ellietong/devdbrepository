SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryPersonRolesByClientSite] (@PersonID int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @PersonID int
--SET @PersonID = 5

IF @PersonID IS NOT NULL
BEGIN
	SELECT LastName + ', ' + FirstName AS PersonName,
		ClientName + ' - ' + SiteName AS ClientSiteName,
		RoleName,
		RoleLevel,
		CASE WHEN CanEdit = 1 THEN 'Yes' ELSE '' END AS CanEdit,
		CASE WHEN CanAdd = 1 THEN 'Yes' ELSE '' END AS CanAdd,
		CASE WHEN CanDelete = 1 THEN 'Yes' ELSE '' END AS CanDelete
	FROM vPersonRoles pr
		INNER JOIN Person     p  ON pr.PersonID     = p.PersonID
		INNER JOIN ClientSite cs ON pr.ClientSiteID = cs.ClientSiteID
		INNER JOIN Client     c  ON pr.ClientID     = c.ClientID
		INNER JOIN Site       s  ON pr.SiteID       = s.SiteID
	WHERE pr.PersonID = @PersonID
	ORDER BY ClientSiteName, RoleName
END
ELSE
BEGIN
	SELECT LastName + ', ' + FirstName AS PersonName,
		ClientName + ' - ' + SiteName AS ClientSiteName,
		RoleName,
		RoleLevel,
		CASE WHEN CanEdit = 1 THEN 'Yes' ELSE '' END AS CanEdit,
		CASE WHEN CanAdd = 1 THEN 'Yes' ELSE '' END AS CanAdd,
		CASE WHEN CanDelete = 1 THEN 'Yes' ELSE '' END AS CanDelete
	FROM vPersonRoles pr
		INNER JOIN Person     p  ON pr.PersonID     = p.PersonID
		INNER JOIN ClientSite cs ON pr.ClientSiteID = cs.ClientSiteID
		INNER JOIN Client     c  ON pr.ClientID     = c.ClientID
		INNER JOIN Site       s  ON pr.SiteID       = s.SiteID
	ORDER BY PersonName, ClientSiteName, RoleName
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryPersonRolesByClientSite] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryPersonRolesByClientSite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryPersonRolesByClientSite] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryPersonRolesByClientSite] TO [Ligado_User]
GO
