SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryUpdateUserFlags_Comments] (@PersonID bigint, @IsLockedOut bit, @PasswordReset bit, @IsApproved bit, @Comment nvarchar(MAX))
AS
BEGIN
	UPDATE Person
	SET PasswordReset = @PasswordReset
	WHERE PersonID = @PersonID
	
	UPDATE aspnet_Membership
	SET Comment = @Comment,
		IsApproved = @IsApproved,
		IsLockedOut = @IsLockedOut
	FROM aspnet_Membership m
	INNER JOIN Person p ON m.UserId = p.MembershipID
	WHERE p.PersonID = @PersonID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryUpdateUserFlags_Comments] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryUpdateUserFlags_Comments] TO [Ligado_User]
GO
