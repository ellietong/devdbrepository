SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qrysfrmFirmCase] (@FirmID int)
AS
SET NOCOUNT ON
BEGIN

--DECLARE @CaseID int
--SET @CaseID = 18

DECLARE @CaseOrder TABLE (RowIndex int IDENTITY, 
						  CaseID int)

INSERT INTO @CaseOrder(CaseID)
	SELECT CaseID
	FROM tblCase
	ORDER BY CaseName ASC

SELECT fc.FirmCaseID, fc.FirmID, fc.CaseID, fc.FirmCaseURL
FROM tblFirmCase fc
	LEFT JOIN @CaseOrder fo ON fc.CaseID = fo.CaseID
WHERE FirmID = @FirmID
ORDER BY RowIndex

END
GO
