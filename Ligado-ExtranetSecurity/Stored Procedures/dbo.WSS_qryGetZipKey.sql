SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetZipKey](@ZipQueueID uniqueidentifier)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT TOP 1 ZipKey
	FROM ZipQueue
	WHERE ZipQueueID = @ZipQueueID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetZipKey] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_qryGetZipKey] TO [Ligado_User]
GRANT ALTER ON  [dbo].[WSS_qryGetZipKey] TO [Zip_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetZipKey] TO [Zip_User]
GRANT EXECUTE ON  [dbo].[WSS_qryGetZipKey] TO [Zip_User]
GO
