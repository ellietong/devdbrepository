SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryAddAllDetailsViewColumns] (@RoleID int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @RoleID int = 28

INSERT INTO RoleDetailsViewColumn (RoleID, DetailsViewColumnID)
	SELECT @RoleID AS RoleID, DetailsViewColumnID
	FROM DetailsViewColumn
	WHERE DetailsViewID IN(SELECT DetailsViewID
						FROM RoleDetailsView
						WHERE RoleID = @RoleID)
		AND DetailsViewColumnID NOT IN(SELECT DetailsViewColumnID
									FROM RoleDetailsViewColumn
									WHERE RoleID = @RoleID)
GO
GRANT VIEW DEFINITION ON  [dbo].[qryAddAllDetailsViewColumns] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryAddAllDetailsViewColumns] TO [Ligado_User]
GO
