SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_MOSS_qryUnlockUser]
(@Username nvarchar(250))
AS
BEGIN
	UPDATE vPerson 
	SET IsLockedOut = 0, 
		FailedPasswordAttemptCount = 0, 
		[FailedPasswordAnswer-AttemptCount] = 0 
	WHERE Email = @Username
END

GO
