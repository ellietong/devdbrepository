SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_FindUsersInRole]
@rolename nvarchar(255),
@usernameToMatch nvarchar(80) = null
AS

----test variables
--DECLARE @rolename nvarchar(255)
--DECLARE @usernameToMatch nvarchar(80)
--SET @rolename = 'NSI Claims Processing - Gilbert Randolph'
--SET @usernameToMatch = 'quinnk@gilbertrandolph.com'

BEGIN


if (@usernameToMatch is not null)
begin
	select
		u.email as username,
		u.displayname,
		u.personId,
		u.email
	from
		WSS_GetAllUserRoles r
		join v_MOSS_GetAllUsers u on u.personid = r.personid
	where
		rolename = @rolename and
		(u.email like @usernameToMatch OR u.email like '%' + @usernameToMatch + '%')
end
else
begin
	select
		u.email as username,
		u.displayname,
		u.personId,
		u.email
	from
		WSS_GetAllUserRoles r
		join v_MOSS_GetAllUsers u on u.personid = r.personid
	where
		rolename = @rolename
end

END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_FindUsersInRole] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_FindUsersInRole] TO [base_user]
GO
