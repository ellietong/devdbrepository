SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryAddAllDataGridCriteria] (@RoleID int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @RoleID int = 28

INSERT INTO RoleDataGridCriterion (RoleID, DataGridCriterionID)
	SELECT @RoleID AS RoleID, DataGridCriterionID
	FROM DataGridCriterion
	WHERE DataGridID IN(SELECT DataGridID
						FROM RoleDataGrid
						WHERE RoleID = @RoleID)
		AND DataGridCriterionID NOT IN(SELECT DataGridCriterionID
									FROM RoleDataGridCriterion
									WHERE RoleID = @RoleID)
GO
GRANT VIEW DEFINITION ON  [dbo].[qryAddAllDataGridCriteria] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryAddAllDataGridCriteria] TO [Ligado_User]
GO
