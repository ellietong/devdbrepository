SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmDataGridCriterion] (@DataGridID int)
AS
SET NOCOUNT ON

DECLARE @DataGridCriterion TABLE (DataGridCriterionID int, FormSort int IDENTITY(1,1))
INSERT INTO @DataGridCriterion
	SELECT DataGridCriterionID
	FROM DataGridCriterion dgc
		INNER JOIN Criterion c ON dgc.CriterionID = c.CriterionID
	WHERE DataGridID = @DataGridID
	ORDER BY DataField

SELECT dgc.*
FROM DataGridCriterion dgc
	LEFT JOIN @DataGridCriterion dgcs ON dgc.DataGridCriterionID = dgcs.DataGridCriterionID
WHERE DataGridID = @DataGridID
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDataGridCriterion] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmDataGridCriterion] TO [Ligado_User]
GO
