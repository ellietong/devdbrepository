SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmDetailsViewQueryStringParameter] (@DetailsViewID int)
AS
SET NOCOUNT ON

DECLARE @QueryStringParameterDetailsView TABLE (QueryStringParameterDetailsViewID int, FormSort int IDENTITY(1,1))
INSERT INTO @QueryStringParameterDetailsView
	SELECT QueryStringParameterDetailsViewID
	FROM QueryStringParameterDetailsView qspdg
		INNER JOIN QueryStringParameter qsp ON qspdg.QueryStringParameterID = qsp.QueryStringParameterID
	WHERE DetailsViewID = @DetailsViewID
	ORDER BY ParameterName

SELECT qspdg.*
FROM QueryStringParameterDetailsView qspdg
	LEFT JOIN @QueryStringParameterDetailsView qspdgs ON qspdg.QueryStringParameterDetailsViewID = qspdgs.QueryStringParameterDetailsViewID
WHERE DetailsViewID = @DetailsViewID
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDetailsViewQueryStringParameter] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmDetailsViewQueryStringParameter] TO [Ligado_User]
GO
