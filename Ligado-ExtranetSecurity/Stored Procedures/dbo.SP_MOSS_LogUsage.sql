SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[SP_MOSS_LogUsage]
(
	@UserName nvarchar(500),					
	@Site nvarchar(500),
	@Page nvarchar(500),
	@URL nvarchar(max),
	@ActionID int,		
	@ActionDate datetime,
	@ClaimantPersonalInfoID int = null,
	@SchemeID int = null,
	@PolicyID int = null
)

AS


INSERT INTO tblUsageLog (
	UserName,
	[Site],
	Page,
	URL,
	ActionID,
	[ActionDate],
	ClaimantPersonalInfoID,
	SchemeID,
	PolicyID
)
VALUES (	
	@UserName,					
	@Site,
	@Page,
	@URL,
	@ActionID,		
	@ActionDate,
	@ClaimantPersonalInfoID,
	@SchemeID,
	@PolicyID
)


GO
GRANT EXECUTE ON  [dbo].[SP_MOSS_LogUsage] TO [base_user]
GO
