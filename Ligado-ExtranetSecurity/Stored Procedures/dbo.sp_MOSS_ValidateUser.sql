SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_ValidateUser]
@username nvarchar(80),
@password nvarchar(80)
AS
BEGIN

--DECLARE @username nvarchar(80)
--DECLARE @Password nvarchar(80)
--SET @username = 'testuser@harveykruse.com'
--SET @Password = 'Test#1'

--assumption is that the password field is plain text.
DECLARE @count int
SET @count = (select
			count(*)
		from
			v_MOSS_GetAllUsers
		where
			email = @username and
			[password] = @password
			AND COALESCE(IsLockedOut, 0) = 0)
--if successful, log their login time and reset their failure attempt count
IF(@count > 0)
BEGIN
	UPDATE vPerson
	SET LastLoginDate = GETDATE(),
	FailedPasswordAttemptCount = 0
	WHERE Email = @username
END
ELSE
BEGIN
	--If it's the user's first failed login attempt, set the window start time
	IF(SELECT COALESCE(FailedPasswordAttemptCount, 0) FROM vPerson WHERE Email = @username) = 0
	BEGIN
		UPDATE vPerson
		SET FailedPasswordAttemptWindowStart = GETDATE()
		WHERE Email = @username
	END
	--Increment their count if there is a failed attempt
	IF(SELECT COALESCE(FailedPasswordAttemptCount, 0) FROM vPerson WHERE Email = @username) BETWEEN 0 AND 5 
	BEGIN
		UPDATE vPerson
		SET FailedPasswordAttemptCount = (SELECT COALESCE(FailedPasswordAttemptCount, 0) FROM vPerson WHERE Email = @username) + 1
		WHERE Email = @username
	
	END
	--if they've reached 5 attempts, lock the account
	IF(SELECT COALESCE(FailedPasswordAttemptCount, 0) FROM vPerson WHERE Email = @username AND DATEDIFF(MI, [FailedPasswordAttemptWindowStart], GETDATE())<=10 ) >=5
	BEGIN
		UPDATE vPerson
		SET IsLockedOut = 1,
			LastLockoutDate = GETDATE()
		WHERE Email = @username
		
		DECLARE @Subject nvarchar(500)
		SET @Subject = (SELECT 'User Locked Out: ' + @username)
		PRINT 'test'
		EXEC msdb.dbo.sp_send_dbmail
		--The NSIProfile sends from NSI@kcicllc.com
		@profile_name = 'Admin',
		@recipients = 'support@kcicllc.com',
		@blind_copy_recipients = '',
		@subject = @Subject,
		@body = '<html><body></body></html>',
		@body_format = 'html' 
	END
--ADD LATER: which cases user who is locked out is part of
END
SELECT @count



END



GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_ValidateUser] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_ValidateUser] TO [base_user]
GO
