SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[aspnet_SyncTablesToProd_Inserts]
AS

BEGIN

DECLARE @NewEmails TABLE (Email NVARCHAR(256))
INSERT INTO @NewEmails
	SELECT Email
	FROM [WEBSQL].[Ligado-ExtranetSecurity].dbo.aspnet_Membership prod
	WHERE prod.Email NOT IN(SELECT Email FROM dbo.aspnet_Membership)

--Sync dev aspnet_Users table to match the same table on production 
--adding records for any emails that only exist on prod
INSERT [WEBDEV].[Ligado-ExtranetSecurity].dbo.aspnet_Users( [ApplicationId]
															,[UserId]
															,[UserName]
															,[LoweredUserName]
															,[MobileAlias]
															,[IsAnonymous]
															,[LastActivityDate] )
	SELECT [ApplicationId]
		,produ.[UserId]
		,[UserName]
		,[LoweredUserName]
		,[MobileAlias]
		,[IsAnonymous]
		,[LastActivityDate]
	FROM [WEBSQL].[Ligado-ExtranetSecurity].dbo.aspnet_Users produ
	WHERE produ.UserName IN(SELECT Email FROM @NewEmails)

--Sync dev aspnet_Membership table to match the same table on production 
--adding records for any emails that only exist on prod
INSERT dbo.aspnet_Membership([ApplicationId]
							,[UserId]
							,[Password]
							,[PasswordFormat]
							,[PasswordSalt]
							,[MobilePIN]
							,[Email]
							,[LoweredEmail]
							,[PasswordQuestion]
							,[PasswordAnswer]
							,[IsApproved]
							,[IsLockedOut]
							,[CreateDate]
							,[LastLoginDate]
							,[LastPasswordChangedDate]
							,[LastLockoutDate]
							,[FailedPasswordAttemptCount]
							,[FailedPasswordAttemptWindowStart]
							,[FailedPasswordAnswerAttemptCount]
							,[FailedPasswordAnswerAttemptWindowStart])
							--,[Comment])
	SELECT [ApplicationId]
								,[UserId]
								,[Password]
								,[PasswordFormat]
								,[PasswordSalt]
								,[MobilePIN]
								,[Email]
								,[LoweredEmail]
								,[PasswordQuestion]
								,[PasswordAnswer]
								,[IsApproved]
								,[IsLockedOut]
								,[CreateDate]
								,[LastLoginDate]
								,[LastPasswordChangedDate]
								,[LastLockoutDate]
								,[FailedPasswordAttemptCount]
								,[FailedPasswordAttemptWindowStart]
								,[FailedPasswordAnswerAttemptCount]
								,[FailedPasswordAnswerAttemptWindowStart]
								--,[Comment]
	FROM [WEBSQL].[Ligado-ExtranetSecurity].dbo.aspnet_Membership prod
	WHERE prod.Email IN(SELECT Email FROM @NewEmails)



END
GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_SyncTablesToProd_Inserts] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[aspnet_SyncTablesToProd_Inserts] TO [Ligado_User]
GO
