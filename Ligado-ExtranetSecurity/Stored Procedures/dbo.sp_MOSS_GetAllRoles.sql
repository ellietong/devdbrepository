SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_GetAllRoles]
AS
BEGIN

	select
		rolename as role
	from
		WSS_GetAllUserRoles

END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_GetAllRoles] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_GetAllRoles] TO [base_user]
GO
