SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDequeueZip]
AS
BEGIN
	--CPC 1/14/2013
	--Zip proceses that haven't been started or ones that are over 20 minutes old and have only had retries < 5 times
	SET NOCOUNT ON;
	DECLARE @ZipQueueID uniqueidentifier = (SELECT TOP 1 ZipQueueID
	FROM ZipQueue zq
	WHERE	ZipKey IS NULL
			AND DATEADD(ss, 5, AddedToQueue) < GETDATE()
			AND (StartTime IS NULL OR (DATEADD(mi, 45, StartTime) < GETDATE() AND Attempts < 5)) --OR StartTime was > 30 minutes?  Add a column for # of attempts
			--AND ZipQueueID <> 'DC983B33-8C28-481A-AB6B-5313DC1A6EE7'
	ORDER BY AddedToQueue ASC)
	
	SELECT *
	FROM ZipQueue zq
	WHERE	ZipQueueID = @ZipQueueID
	
	UPDATE ZipQueue
	SET StartTime = GETDATE(),
		Attempts = Attempts + 1
	WHERE ZipQueueID = @ZipQueueID
			
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDequeueZip] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_qryDequeueZip] TO [Ligado_User]
GRANT ALTER ON  [dbo].[WSS_qryDequeueZip] TO [Zip_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDequeueZip] TO [Zip_User]
GRANT EXECUTE ON  [dbo].[WSS_qryDequeueZip] TO [Zip_User]
GO
