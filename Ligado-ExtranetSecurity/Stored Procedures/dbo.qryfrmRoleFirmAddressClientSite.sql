SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmRoleFirmAddressClientSite] (@FirmAddressClientSiteID int)
AS
SET NOCOUNT ON

DECLARE @RoleFirmAddressClientSites TABLE (RoleFirmAddressClientSiteID int, FormSort int IDENTITY(1,1))
INSERT INTO @RoleFirmAddressClientSites
	SELECT RoleFirmAddressClientSiteID
	FROM RoleFirmAddressClientSite rfacs
		INNER JOIN [Role] r ON rfacs.RoleID = r.RoleID
	WHERE FirmAddressClientSiteID = @FirmAddressClientSiteID
	ORDER BY RoleName

SELECT rfacs.*
FROM RoleFirmAddressClientSite rfacs
	LEFT JOIN @RoleFirmAddressClientSites rfacss ON rfacs.RoleFirmAddressClientSiteID = rfacss.RoleFirmAddressClientSiteID 
WHERE FirmAddressClientSiteID = @FirmAddressClientSiteID
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmRoleFirmAddressClientSite] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmRoleFirmAddressClientSite] TO [Ligado_User]
GO
