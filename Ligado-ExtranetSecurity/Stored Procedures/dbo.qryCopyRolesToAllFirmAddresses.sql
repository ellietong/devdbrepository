SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryCopyRolesToAllFirmAddresses] (@FirmAddressID int = NULL, @CreateClientSites int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @FirmAddressID int = 4
--DECLARE @CreateClientSites int = 1

--STEP 1: Gather the roles that exist for the selected FirmAddressID, by ClientSiteID
DECLARE @RolesToCopy TABLE (ClientSiteID int, RoleID int)
INSERT INTO @RolesToCopy
	SELECT ClientSiteID, RoleID
	FROM FirmAddressClientSite facs
		INNER JOIN RoleFirmAddressClientSite rfacs ON facs.FirmAddressClientSiteID = rfacs.FirmAddressClientSiteID
	WHERE FirmAddressID = @FirmAddressID
	ORDER BY ClientSiteID, RoleID

--STEP 2: Gather the other FirmAddressIDs that exist for the FirmAddressID's firm
DECLARE @FirmID int
SET @FirmID = (SELECT FirmID 
			   FROM FirmAddress 
			   WHERE FirmAddressID = @FirmAddressID)

DECLARE @FirmAddresses TABLE (FirmAddressID int)
INSERT INTO @FirmAddresses
	SELECT FirmAddressID
	FROM FirmAddress
	WHERE FirmID = @FirmID
		AND FirmAddressID <> @FirmAddressID
	ORDER BY FirmAddressID

--STEP 3: Use a cursor to run through all the gathered FirmAddressIDs and copy to them the gathered roles (where they don't already exist)
DECLARE @CurrentFirmAddressID int
DECLARE @CurrentClientSiteID int
DECLARE @OriginalFirmAddressClientSiteID int
DECLARE @CurrentFirmAddressClientSiteID int

DECLARE @FirmAddressClientSites TABLE (ClientSiteID int)
DECLARE cursFirmAddresses CURSOR FOR
    SELECT FirmAddressID 
	FROM @FirmAddresses

OPEN cursFirmAddresses
FETCH NEXT FROM cursFirmAddresses INTO @CurrentFirmAddressID

WHILE (@@FETCH_STATUS = 0)
BEGIN
    IF @CreateClientSites = 1
	BEGIN
		INSERT INTO FirmAddressClientSite (FirmAddressID, ClientSiteID, Active)
			SELECT @CurrentFirmAddressID, ClientSiteID, Active
			FROM FirmAddressClientSite
			WHERE FirmAddressID = @FirmAddressID
				AND ClientSiteID NOT IN(SELECT ClientSiteID
										FROM FirmAddressClientSite
										WHERE FirmAddressID = @CurrentFirmAddressID)
	END

	INSERT INTO @FirmAddressClientSites
		SELECT ClientSiteID
		FROM FirmAddressClientSite
		WHERE FirmAddressID = @CurrentFirmAddressID

	DECLARE cursClientSites CURSOR FOR
		SELECT ClientSiteID 
		FROM @FirmAddressClientSites

	OPEN cursClientSites
	FETCH NEXT FROM cursClientSites INTO @CurrentClientSiteID

	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @OriginalFirmAddressClientSiteID = (SELECT FirmAddressClientSiteID
											    FROM FirmAddressClientSite
											    WHERE FirmAddressID = @FirmAddressID
												    AND ClientSiteID = @CurrentClientSiteID)
		SET @CurrentFirmAddressClientSiteID = (SELECT FirmAddressClientSiteID
											   FROM FirmAddressClientSite
											   WHERE FirmAddressID = @CurrentFirmAddressID
												   AND ClientSiteID = @CurrentClientSiteID)
		
		INSERT INTO RoleFirmAddressClientSite (FirmAddressClientSiteID, RoleID, CanAdd, CanDelete, CanEdit, Notes)
			SELECT @CurrentFirmAddressClientSiteID, RoleID, CanAdd, CanDelete, CanEdit, Notes
			FROM RoleFirmAddressClientSite
			WHERE FirmAddressClientSiteID = @OriginalFirmAddressClientSiteID
				AND RoleID NOT IN(SELECT RoleID
								  FROM RoleFirmAddressClientSite
								  WHERE FirmAddressClientSiteID = @CurrentFirmAddressClientSiteID)

		FETCH NEXT FROM cursClientSites INTO @CurrentClientSiteID
	END

	CLOSE cursClientSites
	DEALLOCATE cursClientSites

	DELETE FROM @FirmAddressClientSites
    FETCH NEXT FROM cursFirmAddresses INTO @CurrentFirmAddressID
END

CLOSE cursFirmAddresses
DEALLOCATE cursFirmAddresses
GO
GRANT VIEW DEFINITION ON  [dbo].[qryCopyRolesToAllFirmAddresses] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryCopyRolesToAllFirmAddresses] TO [Ligado_User]
GO
