SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MOSS_GetPassword]
	@username nvarchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [Password]
	FROM v_MOSS_GetAllUsers
	WHERE Email = @username
END

GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_GetPassword] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_GetPassword] TO [base_user]
GO
