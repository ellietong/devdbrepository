SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Crook
-- Create date: 11/21/2013
-- Description:	Applies a new role to the same permissions structure an existing role has
-- =============================================
CREATE PROCEDURE [dbo].[qryApplyRole] (@ExistingRoleID bigint, @NewRoleID bigint)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO RoleClientSite (RoleID, ClientSiteID, CanAdd, CanEdit, CanDelete)
	SELECT @NewRoleID, ClientSiteID, CanAdd, CanEdit, CanDelete
	FROM RoleClientSite rcs
	WHERE RoleID = @ExistingRoleID
	

	INSERT INTO RoleFirmAddressClientSite (RoleID, FirmAddressClientSiteID, CanAdd, CanEdit, CanDelete)
	SELECT @NewRoleID, FirmAddressClientSiteID, CanAdd, CanEdit, CanDelete
	FROM RoleFirmAddressClientSite
	WHERE RoleID = @ExistingRoleID

	INSERT INTO RolePersonFirmAddressClientSite (RoleID, PersonFirmAddressClientSiteID, CanAdd, CanEdit, CanDelete)
	SELECT @NewRoleID, RolePersonFirmAddressClientSiteID, CanAdd, CanEdit, CanDelete
	FROM RolePersonFirmAddressClientSite
	WHERE RoleID = @ExistingRoleID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryApplyRole] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryApplyRole] TO [Ligado_User]
GO
