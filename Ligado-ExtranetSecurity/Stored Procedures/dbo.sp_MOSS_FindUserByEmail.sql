SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_FindUserByEmail] 
@email nvarchar(80)
AS
BEGIN
select
	Email username,
	PersonId,
	DisplayName,
	email
from
	v_MOSS_GetAllUsers
where
	email = @email
END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_FindUserByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_FindUserByEmail] TO [base_user]
GO
