SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmDataGridQueryStringParameter] (@DataGridID int)
AS
SET NOCOUNT ON

DECLARE @QueryStringParameterDataGrid TABLE (QueryStringParameterDataGridID int, FormSort int IDENTITY(1,1))
INSERT INTO @QueryStringParameterDataGrid
	SELECT QueryStringParameterDataGridID
	FROM QueryStringParameterDataGrid qspdg
		INNER JOIN QueryStringParameter qsp ON qspdg.QueryStringParameterID = qsp.QueryStringParameterID
	WHERE DataGridID = @DataGridID
	ORDER BY ParameterName

SELECT qspdg.*
FROM QueryStringParameterDataGrid qspdg
	LEFT JOIN @QueryStringParameterDataGrid qspdgs ON qspdg.QueryStringParameterDataGridID = qspdgs.QueryStringParameterDataGridID
WHERE DataGridID = @DataGridID
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDataGridQueryStringParameter] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmDataGridQueryStringParameter] TO [Ligado_User]
GO
