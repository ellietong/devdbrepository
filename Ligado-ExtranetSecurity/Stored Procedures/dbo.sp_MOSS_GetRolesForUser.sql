SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_GetRolesForUser]
@username nvarchar(80)
AS
BEGIN
	--DECLARE @username nvarchar(255)
	--SET @username = 'dpicone@hptylaw.com'
	select
		rolename as Role
	from
	v_MOSS_GetAllUserRoles
		--WSS_GetAllUserRoles
	where
		email = @username

END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_GetRolesForUser] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_GetRolesForUser] TO [base_user]
GO
