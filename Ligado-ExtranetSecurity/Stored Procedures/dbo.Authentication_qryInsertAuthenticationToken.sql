SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Authentication_qryInsertAuthenticationToken]
(@Username nvarchar(250), @Token uniqueidentifier)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    --Get the Username for the provided token if it is unused
	INSERT INTO AuthenticationToken (Username, AuthenticationTokenID, IsUsed, ExpirationDate)
	SELECT @Username, @Token, 0, DATEADD(mi, 5, GETDATE())
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Authentication_qryInsertAuthenticationToken] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[Authentication_qryInsertAuthenticationToken] TO [Ligado_User]
GO
