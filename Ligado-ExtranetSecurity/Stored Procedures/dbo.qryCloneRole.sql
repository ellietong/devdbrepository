SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryCloneRole] (@RoleID int = NULL, @CloneRoleID int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @RoleID int = 5
--DECLARE @CloneRoleID int = 1

DECLARE @RoleName nvarchar(250)
DECLARE @CloneRoleName nvarchar(250)
SELECT @RoleName = RoleName FROM [Role] WHERE RoleID = @RoleID 
SELECT @CloneRoleName = RoleName FROM [Role] WHERE RoleID = @CloneRoleID

--STEP 1: Clone the role settings in table RoleClientSite
DECLARE @CloneRoleClientSites TABLE (ClientSiteID int, CanEdit bit, CanAdd bit, CanDelete bit, Notes nvarchar(max))
INSERT INTO @CloneRoleClientSites
	SELECT DISTINCT ClientSiteID, CanEdit, CanAdd, CanDelete, 'Created by cloning the settings of the ' + @CloneRoleName + ' role to the ' + @RoleName + ' role.' AS Notes
	FROM RoleClientSite
	WHERE RoleID = @CloneRoleID
		AND ClientSiteID NOT IN(SELECT DISTINCT ClientSiteID
								FROM RoleClientSite
								WHERE RoleID = @RoleID)
	ORDER BY ClientSiteID

--STEP 2: Clone the role settings in table RoleFirmAddressClientSite
DECLARE @CloneRoleFirmAddressClientSites TABLE (FirmAddressClientSiteID int, CanEdit bit, CanAdd bit, CanDelete bit, Notes nvarchar(max))
INSERT INTO @CloneRoleFirmAddressClientSites
	SELECT DISTINCT FirmAddressClientSiteID, CanEdit, CanAdd, CanDelete, 'Created by cloning the settings of the ' + @CloneRoleName + ' role to the ' + @RoleName + ' role.' AS Notes
	FROM RoleFirmAddressClientSite
	WHERE RoleID = @CloneRoleID
		AND FirmAddressClientSiteID NOT IN(SELECT DISTINCT FirmAddressClientSiteID
										   FROM RoleFirmAddressClientSite
										   WHERE RoleID = @RoleID)
	ORDER BY FirmAddressClientSiteID

INSERT INTO RoleFirmAddressClientSite (RoleID, FirmAddressClientSiteID, CanEdit, CanAdd, CanDelete, Notes)
	SELECT @RoleID AS RoleID, FirmAddressClientSiteID, CanEdit, CanAdd, CanDelete, Notes
	FROM @CloneRoleFirmAddressClientSites
	ORDER BY FirmAddressClientSiteID

--STEP 3: Clone the role settings in table RolePersonFirmAddressClientSite
DECLARE @CloneRolePersonFirmAddressClientSites TABLE (PersonFirmAddressClientSiteID int, CanEdit bit, CanAdd bit, CanDelete bit, Notes nvarchar(max))
INSERT INTO @CloneRolePersonFirmAddressClientSites
	SELECT DISTINCT PersonFirmAddressClientSiteID, CanEdit, CanAdd, CanDelete, 'Created by cloning the settings of the ' + @CloneRoleName + ' role to the ' + @RoleName + ' role.' AS Notes
	FROM RolePersonFirmAddressClientSite
	WHERE RoleID = @CloneRoleID
		AND PersonFirmAddressClientSiteID NOT IN(SELECT DISTINCT PersonFirmAddressClientSiteID
											     FROM RolePersonFirmAddressClientSite
											     WHERE RoleID = @RoleID)
	ORDER BY PersonFirmAddressClientSiteID

INSERT INTO RolePersonFirmAddressClientSite (RoleID, PersonFirmAddressClientSiteID, CanEdit, CanAdd, CanDelete, Notes)
	SELECT @RoleID AS RoleID, PersonFirmAddressClientSiteID, CanEdit, CanAdd, CanDelete, Notes
	FROM @CloneRolePersonFirmAddressClientSites
	ORDER BY PersonFirmAddressClientSiteID
GO
GRANT VIEW DEFINITION ON  [dbo].[qryCloneRole] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryCloneRole] TO [Ligado_User]
GO
