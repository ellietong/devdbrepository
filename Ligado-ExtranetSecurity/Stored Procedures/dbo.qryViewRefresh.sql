SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryViewRefresh]
AS

BEGIN

-- Create a cursor containing all tables within the current database schema
DECLARE @ViewSchema sysname
DECLARE @ViewName sysname
DECLARE @GrantStatement nvarchar(4000)

DECLARE ViewList CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
	SELECT TABLE_SCHEMA, TABLE_NAME
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_TYPE = 'VIEW' 
		AND OBJECTPROPERTY(OBJECT_ID(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME)), 'IsMSShipped') = 0
		AND LEFT(TABLE_NAME,1) <> 'v'
	ORDER BY TABLE_SCHEMA, TABLE_NAME

-- generate grant statements for each table
OPEN ViewList
FETCH NEXT FROM ViewList INTO @ViewSchema, @ViewName

WHILE @@FETCH_STATUS = 0
BEGIN
    SET @GrantStatement = 'sp_refreshview @viewname = ' + QUOTENAME(@ViewName)
	
	--PRINT @GrantStatement
	EXEC (@GrantStatement)
	
	FETCH NEXT FROM ViewList INTO @ViewSchema, @ViewName
END

CLOSE ViewList
DEALLOCATE ViewList

END

GO
