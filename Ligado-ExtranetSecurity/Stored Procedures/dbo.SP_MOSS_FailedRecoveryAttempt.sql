SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_MOSS_FailedRecoveryAttempt]
(@username nvarchar(80))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE vPerson
	SET [FailedPasswordAnswer-AttemptCount] = COALESCE([FailedPasswordAnswer-AttemptCount], 0) + 1
	WHERE Email = @username
END

GO
