SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmDetailsViewColumn] (@DetailsViewID int)
AS
SET NOCOUNT ON

DECLARE @DetailsViewColumns TABLE (DetailsViewColumnID int, FormSort int IDENTITY(1,1))
INSERT INTO @DetailsViewColumns
	SELECT DetailsViewColumnID
	FROM DetailsViewColumn dgc
		INNER JOIN [Column] c ON dgc.ColumnID = c.ColumnID
	WHERE DetailsViewID = @DetailsViewID
	ORDER BY DataField

SELECT dgc.*
FROM DetailsViewColumn dgc
	LEFT JOIN @DetailsViewColumns dgcs ON dgc.DetailsViewColumnID = dgcs.DetailsViewColumnID 
WHERE DetailsViewID = @DetailsViewID
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDetailsViewColumn] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmDetailsViewColumn] TO [Ligado_User]
GO
