SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[sp_MOSS_LogResults]
AS
--Bring in IUNA-specific log & claimant information
select usagelogid,
	'IUNA/Nosroc Claims Processing' as [Case],
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end as firm, 
	case when username like '' then 'Administrator' else username end as [User],
	site,
	page, 
	replace(url, '/extranet.','/admin.') as url,
	action, 
	actiondate as [Date],
	NULL as Scheme, 
	coalesce(CPI.LastName + ', ','') + Coalesce(CPI.FirstName + ' ','') + coalesce(CPI.MiddleName,'') AS Claimant,
	NULL AS Policy
FROM tblusagelog ul
left join tblusageaction ua on ul.actionid = ua.actionid
left join vPerson p on p.email = ul.username
left join tblfirm f on f.firmid = p.firmid
left join [SPSQL].[kgc-iuna-nosroc].dbo.tblClaimantpersonalinfo cpi on cpi.claimantpersonalinfoid = ul.claimantpersonalinfoid
WHERE actiondate >= getdate()-30 AND url  like '%/iuna_nosroccp/%' 
GROUP BY usagelogid,
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end, 
	case when username like '' then 'Administrator' else username end,
	site,
	page, 
	replace(url, '/extranet.','/admin.'),
	action, 
	actiondate,
	coalesce(CPI.LastName + ', ','') + Coalesce(CPI.FirstName + ' ','') + coalesce(CPI.MiddleName,'')

UNION

--Bring in London Scheme Database usage info
select usagelogid,
	case when url like '%/schemes/%' then 'London Scheme Tracking'
	else 'Other'
	end as [Case],
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end as firm, 
	case when username like '' then 'Administrator' else username end as [User],
	site, 
	page, 
	replace(url, '/extranet.','/admin.') as url,
	action, 
	actiondate as [Date],
	SchemeName as Scheme, 
	NULL AS Claimant,
	NULL AS Policy 
FROM tblusagelog ul
left join tblusageaction ua on ul.actionid = ua.actionid
left join vPerson p on p.email = ul.username
left join tblfirm f on f.firmid = p.firmid
left join [SPSQL].[kga-insolvencytracking].dbo.tblscheme s on s.schemeid = ul.schemeid
where actiondate >= getdate()-30 AND url like '%/schemes/%' 
GROUP BY  usagelogid,
	case when url like '%/schemes/%' then 'London Scheme Tracking'
	else 'Other'end,
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end, 
	case when username like '' then 'Administrator' else username end,
	site, 
	page, 
	replace(url, '/extranet.','/admin.'),
	action, 
	actiondate,
	SchemeName
	
UNION

--Bring in Borg Asbestos usage info
SELECT usagelogid,
	case when url like '%/BWA/%' then 'BorgWarner Asbestos'
	else 'Other'
	end as [Case],
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end as firm, 
	case when username like '' then 'Administrator' else username end as [User],
	site, 
	page, 
	replace(url, '/extranet.','/admin.') as url,
	action, 
	actiondate as [Date],
	NULL as Scheme, 
	NULL AS Claimant,
	pol.carriernickname + ' (' + pol.policynum + ') - ' + convert(nvarchar(15), pol.minpstartdate, 101) AS Policy
FROM tblusagelog ul
left join tblusageaction ua on ul.actionid = ua.actionid
left join vPerson p on p.email = ul.username
left join tblfirm f on f.firmid = p.firmid
left join [SQL2008].[kgc-borgwarnerasbestos_2].dbo.vpolicy pol on ul.policyID = pol.policyID
WHERE actiondate >= getdate()-30 AND url like '%/BWA/%' 
GROUP BY  usagelogid,
	case when url like '%/BWA/%' then 'BorgWarner Asbestos'
	else 'Other'end,
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end, 
	case when username like '' then 'Administrator' else username end,
	site, 
	page, 
	replace(url, '/extranet.','/admin.'),
	action, 
	actiondate,
	pol.carriernickname + ' (' + pol.policynum + ') - ' + convert(nvarchar(15), pol.minpstartdate, 101)
	
UNION

--Bring in IUNA Claimant Review/Audit log information
SELECT usagelogid,
	case when url like '%/crr/%' then 'IUNA/Nosroc Claims Processing'
	else 'Other'
	end as [Case],
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end as firm, 
	case when username like '' then 'Administrator' else username end as [User],
	site, 
	page, 
	replace(url, '/extranet.','/admin.') as url,
	action, 
	actiondate as [Date],
	NULL as Scheme, 
	NULL AS Claimant,
	NULL AS Policy   
FROM tblusagelog ul
left join tblusageaction ua on ul.actionid = ua.actionid
left join vPerson p on p.email = ul.username
left join tblfirm f on f.firmid = p.firmid
WHERE actiondate >= getdate()-30 AND url like '%/crr/%' 
GROUP BY  usagelogid,
	case when url like '%/crr/%' then 'IUNA/Nosroc Claims Processing'
	else 'Other'end,
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end, 
	case when username like '' then 'Administrator' else username end,
	site, 
	page, 
	replace(url, '/extranet.','/admin.'),
	action, 
	actiondate

UNION

--Bring in Dana-specific usage and claimant information
SELECT usagelogid,
	case when url like '%/dana/%' then 'Dana Claims Processing'
	else 'Other'
	end as [Case],
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end as firm, 
	case when username like '' then 'Administrator' else username end as [User],
	site, 
	page, 
	replace(url, '/extranet.','/admin.') as url,
	action, 
	actiondate as [Date],
	NULL as Scheme,
	coalesce(CPINSI.LastName + ', ','') + Coalesce(CPINSI.FirstName + ' ','') + coalesce(CPINSI.MiddleName,'') AS Claimant, 
	NULL AS Policy
FROM tblusagelog ul
left join tblusageaction ua on ul.actionid = ua.actionid
left join vPerson p on p.email = ul.username
left join tblfirm f on f.firmid = p.firmid
left join [SPSQL].[kgc-dana-Claims].dbo.tblClaimantpersonalinfo cpiNSI on cpiNSI.claimantpersonalinfoid = ul.claimantpersonalinfoid
WHERE actiondate >= getdate()-30 AND url like '%/dana/%' 
GROUP BY  usagelogid,
	case when url like '%/dana/%' then 'Dana Claims Processing'
	else 'Other'end,
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end, 
	case when username like '' then 'Administrator' else username end,
	site, 
	page, 
	replace(url, '/extranet.','/admin.'),
	action, 
	actiondate,
	coalesce(CPINSI.LastName + ', ','') + Coalesce(CPINSI.FirstName + ' ','') + coalesce(CPINSI.MiddleName,'')



UNION
--Bring in NSI-specific usage and claimant information
SELECT usagelogid,
	case when url like '%/NSICP/%' then 'NSI Claims Processing'
	else 'Other'
	end as [Case],
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end as firm, 
	case when username like '' then 'Administrator' else username end as [User],
	site, 
	page, 
	replace(url, '/extranet.','/admin.') as url,
	action, 
	actiondate as [Date],
	NULL as Scheme, 
	NULL AS Policy,
	coalesce(CPINSI.LastName + ', ','') + Coalesce(CPINSI.FirstName + ' ','') + coalesce(CPINSI.MiddleName,'') AS Claimant
FROM tblusagelog ul
left join tblusageaction ua on ul.actionid = ua.actionid
left join vPerson p on p.email = ul.username
left join tblfirm f on f.firmid = p.firmid
left join [SPSQL].[kgc-NSI-Claims].dbo.tblClaimantpersonalinfo cpiNSI on cpiNSI.claimantpersonalinfoid = ul.claimantpersonalinfoid
WHERE actiondate >= getdate()-30 AND url like '%/NSICP/%' 
GROUP BY  usagelogid,
	case when url like '%/NSICP/%' then 'NSI Claims Processing'
	else 'Other'end,
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end, 
	case when username like '' then 'Administrator' else username end,
	site, 
	page, 
	replace(url, '/extranet.','/admin.'),
	action, 
	actiondate,
	coalesce(CPINSI.LastName + ', ','') + Coalesce(CPINSI.FirstName + ' ','') + coalesce(CPINSI.MiddleName,'')

UNION
--Bring in Marley log information
select usagelogid,
	'Marley-Wylain Claims Processing' as [Case],
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end as firm, 
	case when username like '' then 'Administrator' else username end as [User],
	site,
	page, 
	replace(url, '/extranet.','/admin.') as url,
	action, 
	actiondate as [Date],
	NULL as Scheme, 
	coalesce(CPI.LastName + ', ','') + Coalesce(CPI.FirstName + ' ','') + coalesce(CPI.MiddleName,'') AS Claimant,
	NULL AS Policy
FROM tblusagelog ul
left join tblusageaction ua on ul.actionid = ua.actionid
left join vPerson p on p.email = ul.username
left join tblfirm f on f.firmid = p.firmid
left join [SPSQL].[kgc-marleywylain].dbo.tblClaimantpersonalinfo cpi on cpi.claimantpersonalinfoid = ul.claimantpersonalinfoid
WHERE actiondate >= getdate()-30 AND url  like '%/cp/mw/%' 
GROUP BY usagelogid,
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end, 
	case when username like '' then 'Administrator' else username end,
	site,
	page, 
	replace(url, '/extranet.','/admin.'),
	action, 
	actiondate,
	coalesce(CPI.LastName + ', ','') + Coalesce(CPI.FirstName + ' ','') + coalesce(CPI.MiddleName,'')

UNION
--Bring in MSA log information
select usagelogid,
	'MSA Document Review' as [Case],
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end as firm, 
	case when username like '' then 'Administrator' else username end as [User],
	site,
	page, 
	replace(url, '/extranet.','/admin.') as url,
	action, 
	actiondate as [Date],
	NULL as Scheme, 
	coalesce(CPI.LastName + ', ','') + Coalesce(CPI.FirstName + ' ','') + coalesce(CPI.MiddleName,'') AS Claimant,
	NULL AS Policy
FROM tblusagelog ul
left join tblusageaction ua on ul.actionid = ua.actionid
left join vPerson p on p.email = ul.username
left join tblfirm f on f.firmid = p.firmid
left join [SQL2008].[kgc-msa-claims_2].dbo.tblClaimantpersonalinfo cpi on cpi.claimantpersonalinfoid = ul.claimantpersonalinfoid
WHERE actiondate >= getdate()-30 AND url  like '%cp/msa/Pages/DocumentReview%' 
GROUP BY usagelogid,
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end, 
	case when username like '' then 'Administrator' else username end,
	site,
	page, 
	replace(url, '/extranet.','/admin.'),
	action, 
	actiondate,
	coalesce(CPI.LastName + ', ','') + Coalesce(CPI.FirstName + ' ','') + coalesce(CPI.MiddleName,'')
UNION	
select usagelogid,
	'MSA Claimant Review' as [Case],
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end as firm, 
	case when username like '' then 'Administrator' else username end as [User],
	site,
	page, 
	replace(url, '/extranet.','/admin.') as url,
	action, 
	actiondate as [Date],
	NULL as Scheme, 
	coalesce(CPI.LastName + ', ','') + Coalesce(CPI.FirstName + ' ','') + coalesce(CPI.MiddleName,'') AS Claimant,
	NULL AS Policy
FROM tblusagelog ul
left join tblusageaction ua on ul.actionid = ua.actionid
left join vPerson p on p.email = ul.username
left join tblfirm f on f.firmid = p.firmid
left join [SQL2008].[kgc-msa-claims_2].dbo.tblClaimantpersonalinfo cpi on cpi.claimantpersonalinfoid = ul.claimantpersonalinfoid
WHERE actiondate >= getdate()-30 AND url  like '%cp/msa/Pages/claimantdetails%' 
GROUP BY usagelogid,
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end, 
	case when username like '' then 'Administrator' else username end,
	site,
	page, 
	replace(url, '/extranet.','/admin.'),
	action, 
	actiondate,
	coalesce(CPI.LastName + ', ','') + Coalesce(CPI.FirstName + ' ','') + coalesce(CPI.MiddleName,'')	
	
	
	
ORDER BY ActionDate DESC

GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_LogResults] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_LogResults] TO [base_user]
GO
