SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_LIGADO_UsersWithRole](@RoleID bigint, @ClientSiteID bigint = null)
AS
BEGIN

	SET NOCOUNT ON;
	--Test vars
	--DECLARE @RoleID int = 19
	--DECLARE @ClientSiteID bigint = null

	DECLARE @MinClientSiteID bigint = (SELECT MIN(ClientSiteID) FROM [ClientSite])
	DECLARE @MaxClientSiteID bigint = (SELECT MAX(ClientSiteID) FROM [ClientSite])

	SELECT RoleID, RoleName, RoleType, RoleDescription
	FROM [Role]
	WHERE RoleID = @RoleID

	--WHO HAS ROLE @RoleID

	--Individual level
	SELECT c.ClientDisplayName, s.SiteName, 'Person-level', p.Email, f.FirmDisplayName, RoleName, RoleType, facs.FirmAddressID, facs.ClientSiteID, r.RoleID
	FROM [Role] r
	INNER JOIN RolePersonFirmAddressClientSite rpfacs ON r.RoleID = rpfacs.roleID
	INNER JOIN PersonFirmAddressClientSite pfacs ON pfacs.PersonFirmAddressClientSiteID = rpfacs.PersonFirmAddressClientSiteID
	INNER JOIN FirmAddressClientSite facs ON pfacs.FirmAddressClientSiteID = facs.FirmAddressClientSiteID
	INNER JOIN Person p ON pfacs.PersonID = p.PersonID
	INNER JOIN FirmAddress fa ON facs.FirmAddressID = fa.FirmAddressID
	INNER JOIN Firm f ON fa.FirmID = f.FirmID
	INNER JOIN ClientSite cs ON facs.ClientSiteID = cs.ClientSiteID
	INNER JOIN Client c ON cs.ClientID = c.ClientID
	INNER JOIN [Site] s ON cs.SiteID = s.SiteID
	WHERE r.RoleID = @RoleID
	AND facs.CLientSiteID >= COALESCE(@ClientSiteID, @MinClientSiteID) AND facs.ClientSiteID <= COALESCE(@ClientSiteID, @MaxClientSiteID)

	--Firm Level
	SELECT c.ClientDisplayName, s.SiteName, 'Firm Address-level', p.Email, f.FirmDisplayName,   RoleName, RoleType, facs.FirmAddressID, facs.ClientSiteID, r.RoleID
	FROM [Role] r
	INNER JOIN RoleFirmAddressClientSite rpfacs ON r.RoleID = rpfacs.roleID
	INNER JOIN FirmAddressClientSite facs ON rpfacs.FirmAddressClientSiteID = facs.FirmAddressClientSiteID
	INNER JOIN PersonFirmAddressClientSite pfacs ON facs.FirmAddressClientSiteID = pfacs.FirmAddressClientSiteID
	INNER JOIN Person p ON pfacs.PersonID = p.PersonID
	INNER JOIN FirmAddress fa ON facs.FirmAddressID = fa.FirmAddressID
	INNER JOIN Firm f ON fa.FirmID = f.FirmID
	INNER JOIN ClientSite cs ON facs.ClientSiteID = cs.ClientSiteID
	INNER JOIN Client c ON cs.ClientID = c.ClientID
	INNER JOIN [Site] s ON cs.SiteID = s.SiteID
	WHERE r.RoleID = @RoleID
	AND facs.CLientSiteID >= COALESCE(@ClientSiteID, @MinClientSiteID) AND facs.ClientSiteID <= COALESCE(@ClientSiteID, @MaxClientSiteID)

	--Client Site Level
	SELECT c.ClientDisplayName, s.SiteName, 'Client Site-level', p.Email, f.FirmDisplayName,   RoleName, RoleType, facs.FirmAddressID, facs.ClientSiteID, r.RoleID
	FROM [Role] r
	INNER JOIN RoleClientSite rcs ON r.RoleID = rcs.roleID
	INNER JOIN FirmAddressClientSite facs ON rcs.ClientSiteID = facs.ClientSiteID
	INNER JOIN PersonFirmAddressClientSite pfacs ON facs.FirmAddressClientSiteID = pfacs.FirmAddressClientSiteID
	INNER JOIN Person p ON pfacs.PersonID = p.PersonID
	INNER JOIN FirmAddress fa ON facs.FirmAddressID = fa.FirmAddressID
	INNER JOIN Firm f ON fa.FirmID = f.FirmID
	INNER JOIN ClientSite cs ON facs.ClientSiteID = cs.ClientSiteID
	INNER JOIN Client c ON cs.ClientID = c.ClientID
	INNER JOIN [Site] s ON cs.SiteID = s.SiteID
	WHERE r.RoleID = @RoleID
	AND rcs.CLientSiteID >= COALESCE(@ClientSiteID, @MinClientSiteID) AND facs.ClientSiteID <= COALESCE(@ClientSiteID, @MaxClientSiteID)
	--vs. DOES user have role?
END
GO
