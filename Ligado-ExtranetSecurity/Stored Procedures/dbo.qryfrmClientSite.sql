SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmClientSite]
AS
SET NOCOUNT ON

DECLARE @ClientSites TABLE (ClientSiteID int, FormSort int IDENTITY(1,1))
INSERT INTO @ClientSites
	SELECT ClientSiteID
	FROM ClientSite cs
		INNER JOIN Client c ON cs.ClientID = c.ClientID
		INNER JOIN [Site] s ON cs.SiteID = s.SiteID
	ORDER BY ClientName, SiteName

SELECT cs.*
FROM ClientSite cs
	LEFT JOIN @ClientSites css ON cs.ClientSiteID = css.ClientSiteID 
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClientSite] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClientSite] TO [Ligado_User]
GO
