SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[aspnet_SyncTablesToProd_Updates]
AS

BEGIN

--Sync dev aspnet_Users table to match the same table on production 
--for only fields that will logically change (unused fields commented out at bottom)
UPDATE dbo.aspnet_Users
	SET [MobileAlias] = u.[MobileAlias]
	   ,[IsAnonymous] = u.[IsAnonymous]
	   ,[LastActivityDate] = u.[LastActivityDate]
		--,[ApplicationId] = @DevApplicationID
		--,[UserId] = u.[UserId]
		--,[UserName] = u.[UserName]
		--,[LoweredUserName] = u.[LoweredUserName]
	FROM [WEBSQL].[Ligado-ExtranetSecurity].dbo.aspnet_Users u
	WHERE aspnet_Users.UserName = u.UserName

--Sync dev aspnet_Membership table to match the same table on production 
--for only fields that will logically change (unused fields commented out at bottom)
UPDATE dbo.aspnet_Membership
	SET  [Password] = m.[Password]
		,[PasswordFormat] = m.[PasswordFormat]
		,[PasswordSalt] = m.[PasswordSalt]
		,[MobilePIN] = m.[MobilePIN]
		,[PasswordQuestion] = m.[PasswordQuestion]
		,[PasswordAnswer] = m.[PasswordAnswer]
		,[IsApproved] = m.[IsApproved]
		,[IsLockedOut] = m.[IsLockedOut]
		,[CreateDate] = m.[CreateDate]
		,[LastLoginDate] = m.[LastLoginDate]
		,[LastPasswordChangedDate] = m.[LastPasswordChangedDate]
		,[LastLockoutDate] = m.[LastLockoutDate]
		,[FailedPasswordAttemptCount] = m.[FailedPasswordAttemptCount]
		,[FailedPasswordAttemptWindowStart] = m.[FailedPasswordAttemptWindowStart]
		,[FailedPasswordAnswerAttemptCount] = m.[FailedPasswordAnswerAttemptCount]
		,[FailedPasswordAnswerAttemptWindowStart] = m.[FailedPasswordAnswerAttemptWindowStart]
	  --,[ApplicationId] = @DevApplicationID
	  --,[UserId] = m.[UserId]
	  --,[Email] = m.[Email]
	  --,[LoweredEmail] = m.[LoweredEmail]
	  --,[Comment] = m.[Comment]
	FROM [WEBSQL].[Ligado-ExtranetSecurity].dbo.aspnet_Membership m
	WHERE aspnet_Membership.Email = m.Email

END

GO
GRANT VIEW DEFINITION ON  [dbo].[aspnet_SyncTablesToProd_Updates] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[aspnet_SyncTablesToProd_Updates] TO [Ligado_User]
GO
