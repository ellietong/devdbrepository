SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmDataGridColumn] (@DataGridID int)
AS
SET NOCOUNT ON

DECLARE @DataGridColumns TABLE (DataGridColumnID int, FormSort int IDENTITY(1,1))
INSERT INTO @DataGridColumns
	SELECT DataGridColumnID
	FROM DataGridColumn dgc
		INNER JOIN [Column] c ON dgc.ColumnID = c.ColumnID
	WHERE DataGridID = @DataGridID
	ORDER BY DataField

SELECT dgc.*
FROM DataGridColumn dgc
	LEFT JOIN @DataGridColumns dgcs ON dgc.DataGridColumnID = dgcs.DataGridColumnID 
WHERE DataGridID = @DataGridID
ORDER BY FormSort
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDataGridColumn] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmDataGridColumn] TO [Ligado_User]
GO
