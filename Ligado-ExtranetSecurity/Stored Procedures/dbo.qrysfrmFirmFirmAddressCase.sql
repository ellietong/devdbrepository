SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qrysfrmFirmFirmAddressCase] (@FirmAddressID int)
AS
SET NOCOUNT ON
BEGIN

--DECLARE @CaseID int
--SET @CaseID = 18

DECLARE @CaseOrder TABLE (RowIndex int IDENTITY, 
						  CaseID int)

INSERT INTO @CaseOrder(CaseID)
	SELECT CaseID
	FROM tblCase
	ORDER BY CaseName ASC

SELECT fac.FirmAddressCaseID, fac.FirmAddressID, fac.CaseID, fac.NoFilters, fac.PayeeAddress, 
	fac.SettlementACHRoutingNumber, fac.SettlementACHAccountNumber, fac.SettlementACHRemittanceEmail
FROM tblFirmAddressCase fac
	LEFT JOIN @CaseOrder fo ON fac.CaseID = fo.CaseID
WHERE FirmAddressID = @FirmAddressID
ORDER BY RowIndex

END
GO
