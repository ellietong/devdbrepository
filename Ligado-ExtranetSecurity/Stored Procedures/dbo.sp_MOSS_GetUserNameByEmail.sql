SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_GetUserNameByEmail] 
@email nvarchar(80)
AS
BEGIN

select
	isnull(email, '') as username
from
	v_MOSS_GetAllUsers
where
	email = @email 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_GetUserNameByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_GetUserNameByEmail] TO [base_user]
GO
