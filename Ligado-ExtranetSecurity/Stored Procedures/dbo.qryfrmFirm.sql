SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmFirm] (@ClientID int = NULL)
AS
SET NOCOUNT ON

IF @ClientID IS NULL
BEGIN
	SELECT * 
	FROM Firm 
	ORDER BY Firm
END
ELSE
BEGIN
	DECLARE @ClientFirms TABLE (FirmID int)
	INSERT INTO @ClientFirms
		SELECT FirmID
		FROM FirmAddress fa
			INNER JOIN FirmAddressClientSite facs ON fa.FirmAddressID = facs.FirmAddressID
			INNER JOIN ClientSite cs ON facs.ClientSiteID = cs.ClientSiteID
		WHERE ClientID = @ClientID
		GROUP BY FirmID
		ORDER BY FirmID

	SELECT *
	FROM Firm
	WHERE FirmID IN(SELECT FirmID FROM @ClientFirms)
	ORDER BY Firm
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmFirm] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmFirm] TO [Ligado_User]
GO
