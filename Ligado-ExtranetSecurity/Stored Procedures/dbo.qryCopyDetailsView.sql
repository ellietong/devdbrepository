SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryCopyDetailsView] (@DetailsViewID int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @DetailsViewID int = 4

--STEP 1: Duplicate the details view itself in table DetailsView
INSERT INTO DetailsView (DetailsViewName, DisplayName, ServiceObject, ServiceMethod, DataObjectTypeName, SelectProc, InsertProc, UpdateProc, DeleteProc, EmptyDataText, Notes)
	SELECT DetailsViewName + ' (Copy)' AS DetailsViewName, DisplayName, ServiceObject, ServiceMethod, DataObjectTypeName, SelectProc, InsertProc, UpdateProc, DeleteProc, EmptyDataText, Notes
	FROM DetailsView
	WHERE DetailsViewID = @DetailsViewID

--STEP 2: Get the DetailsViewID for the newly created duplicate
DECLARE @NewDetailsViewID int
SELECT @NewDetailsViewID = MAX(DetailsViewID) FROM DetailsView

--STEP 3: Duplicate the details view settings in table DetailsViewColumn
INSERT INTO DetailsViewColumn (DetailsViewID, ColumnID, DataKey, VisibleNormal, VisibleEdit, [ReadOnly], ForeignKeyFlag, SortOrder)
	SELECT @NewDetailsViewID, ColumnID, DataKey, VisibleNormal, VisibleEdit, [ReadOnly], ForeignKeyFlag, SortOrder
	FROM DetailsViewColumn
	WHERE DetailsViewID = @DetailsViewID

--STEP 4: Duplicate the details view settings in table QueryStringParameterDetailsView
INSERT INTO QueryStringParameterDetailsView (DetailsViewID, QueryStringParameterID)
	SELECT @NewDetailsViewID, QueryStringParameterID
	FROM QueryStringParameterDetailsView
	WHERE DetailsViewID = @DetailsViewID
GO
GRANT VIEW DEFINITION ON  [dbo].[qryCopyDetailsView] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryCopyDetailsView] TO [Ligado_User]
GO
