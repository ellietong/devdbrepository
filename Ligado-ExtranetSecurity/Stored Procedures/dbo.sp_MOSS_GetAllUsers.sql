SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_MOSS_GetAllUsers]
AS
BEGIN

select
	email as username, 
	displayname, 
	email,
	personid
from
	v_MOSS_GetAllUsers
END
GO
GRANT VIEW DEFINITION ON  [dbo].[sp_MOSS_GetAllUsers] TO [base_user]
GRANT EXECUTE ON  [dbo].[sp_MOSS_GetAllUsers] TO [base_user]
GO
