SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[qryEmailAddressUpdates] (@OldEmail NVARCHAR(256), @NewEmail NVARCHAR(256))
AS

BEGIN

--DECLARE @OldEmail NVARCHAR(256) = 'hagep@kcicllc.com'
--DECLARE @NewEmail NVARCHAR(256) = 'hagep@kcicllc.com'

--WEBSQL--
--UPDATE [WEBSQL].[Ligado-ExtranetSecurity].dbo.Person
--	SET Email = @NewEmail
--	WHERE Email = @OldEmail

--UPDATE [WEBSQL].[Ligado-ExtranetSecurity].dbo.aspnet_Users
--	SET [UserName] = @NewEmail
--	   ,[LoweredUserName] = LOWER(@NewEmail)
--	WHERE aspnet_Users.UserName = @OldEmail

--UPDATE [WEBSQL].[Ligado-ExtranetSecurity].dbo.aspnet_Membership
--	SET [Email] = @NewEmail
--	   ,[LoweredEmail] = LOWER(@NewEmail)
--	WHERE aspnet_Membership.Email = @OldEmail

--WEBDEV--
UPDATE dbo.aspnet_Users
	SET [UserName] = @NewEmail
	   ,[LoweredUserName] = LOWER(@NewEmail)
	WHERE aspnet_Users.UserName = @OldEmail

UPDATE dbo.aspnet_Membership
	SET [Email] = @NewEmail
	   ,[LoweredEmail] = LOWER(@NewEmail)
	WHERE aspnet_Membership.Email = @OldEmail

--SHARESQL--
--UPDATE [SHARESQL].[FBSecurity].dbo.tblPerson
--	SET Email = @NewEmail
--	WHERE Email = @OldEmail




END


GO
GRANT VIEW DEFINITION ON  [dbo].[qryEmailAddressUpdates] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryEmailAddressUpdates] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryEmailAddressUpdates] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryEmailAddressUpdates] TO [Ligado_User]
GO
