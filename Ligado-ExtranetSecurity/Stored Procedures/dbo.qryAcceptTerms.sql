SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryAcceptTerms] (@username nvarchar(500))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE Person 
	SET TermsAcceptedDate = GETDATE()
	WHERE Email = @username
    
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryAcceptTerms] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryAcceptTerms] TO [Ligado_User]
GO
