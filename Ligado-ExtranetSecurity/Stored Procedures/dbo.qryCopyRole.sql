SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryCopyRole] (@RoleID int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @RoleID int = 4

--STEP 1: Duplicate the role itself in table Role
INSERT INTO [Role] (RoleName, RoleType, RoleDescription, IsDefault, IsNotification, IsFirmAddressCaseLevelOnly)
	SELECT RoleName + ' (Copy)' AS RoleName, RoleType, RoleDescription, IsDefault, IsNotification, IsFirmAddressCaseLevelOnly
	FROM [Role]
	WHERE RoleID = @RoleID

--STEP 2: Get the RoleID for the newly created duplicate
DECLARE @NewRoleID int
SELECT @NewRoleID = MAX(RoleID) FROM [Role]

--STEP 3: Duplicate the role settings in table RoleDataGrid
INSERT INTO RoleDataGrid (RoleID, DataGridID, SortOrder)
	SELECT @NewRoleID, DataGridID, SortOrder
	FROM RoleDataGrid
	WHERE RoleID = @RoleID

--STEP 4: Duplicate the role settings in table RoleDataGridColumn
INSERT INTO RoleDataGridColumn (RoleID, DataGridColumnID, SortOrder, VisibleNormal, VisibleEdit, VisibleCreate, [Required], AllowEntriesToBeInserted, AllowEntriesToBeUpdated, [ReadOnly])
	SELECT @NewRoleID, DataGridColumnID, SortOrder, VisibleNormal, VisibleEdit, VisibleCreate, [Required], AllowEntriesToBeInserted, AllowEntriesToBeUpdated, [ReadOnly]
	FROM RoleDataGridColumn
	WHERE RoleID = @RoleID

--STEP 5: Duplicate the role settings in table RoleDataGridCriterion
INSERT INTO RoleDataGridCriterion (RoleID, DataGridCriterionID, SortOrder)
	SELECT @NewRoleID, DataGridCriterionID, SortOrder
	FROM RoleDataGridCriterion
	WHERE RoleID = @RoleID

--STEP 6: Duplicate the role settings in table RoleDetailsView
INSERT INTO RoleDetailsView (RoleID, DetailsViewID, SortOrder)
	SELECT @NewRoleID, DetailsViewID, SortOrder
	FROM RoleDetailsView
	WHERE RoleID = @RoleID

--STEP 7: Duplicate the role settings in table RoleDetailsViewColumn
INSERT INTO RoleDetailsViewColumn (RoleID, DetailsViewColumnID, SortOrder, VisibleNormal, VisibleEdit, [Required], AllowEntriesToBeInserted, AllowEntriesToBeUpdated)
	SELECT @NewRoleID, DetailsViewColumnID, SortOrder, VisibleNormal, VisibleEdit, [Required], AllowEntriesToBeInserted, AllowEntriesToBeUpdated
	FROM RoleDetailsViewColumn
	WHERE RoleID = @RoleID

--STEP 8: Duplicate the role settings in table RoleReport
INSERT INTO RoleReport (RoleID, ReportID)
	SELECT @NewRoleID, ReportID
	FROM RoleReport
	WHERE RoleID = @RoleID

--STEP 9: Duplicate the role settings in table RoleUserControl
INSERT INTO RoleUserControl (RoleID, UserControlID)
	SELECT @NewRoleID, UserControlID
	FROM RoleUserControl
	WHERE RoleID = @RoleID
GO
GRANT VIEW DEFINITION ON  [dbo].[qryCopyRole] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryCopyRole] TO [Ligado_User]
GO
