SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[sp_MOSS_LogResults_archive]


AS



select usagelogid,
	case  when url  like '%/iuna_nosroccp/%' then 'IUNA/Nosroc Claims Processing'
	when url like '%/schemes/%' then 'London Scheme Tracking'
	when url like '%/BWA/%' then 'BorgWarner Asbestos'
	when url like '%/crr/%' then 'IUNA/Nosroc Claims Processing'
	when url like '%/NSICP/%' then 'NSI Claims Processing'
	else 'Other'
	end as [Case],
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end as firm, 
	case when username like '' then 'Administrator' else username end as [User], site, page, replace(url, '/extranet.','/admin.') as url, action, actiondate as [Date],
	 SchemeName as Scheme, 
	 
	 case when url like '%/NSICP/%' then 
		coalesce(CPINSI.LastName + ', ','') + Coalesce(CPINSI.FirstName + ' ','') + coalesce(CPINSI.MiddleName,'')
		when url like '%/iuna_nosroccp/%' then 
	  coalesce(CPI.LastName + ', ','') + Coalesce(CPI.FirstName + ' ','') + coalesce(CPI.MiddleName,'') else '' End AS Claimant,
	  case when url like '%/BWA/%' then pol.carriernickname + ' (' + pol.policynum + ') - ' + convert(nvarchar(15), pol.minpstartdate, 101) end AS Policy--policyID, min start date, carrier
	  
from tblusagelog ul
left join tblusageaction ua on ul.actionid = ua.actionid
left join vPerson p on p.email = ul.username
left join tblfirm f on f.firmid = p.firmid
left join [SPSQL].[kgc-iuna-nosroc].dbo.tblClaimantpersonalinfo cpi on cpi.claimantpersonalinfoid = ul.claimantpersonalinfoid
left join [SPSQL].[kgc-NSI].dbo.mbtblClaimantpersonalinfo cpiNSI on cpiNSI.claimantpersonalinfoid = ul.claimantpersonalinfoid
left join [SPSQL].[kga-insolvencytracking].dbo.tblscheme s on s.schemeid = ul.schemeid
left join [SQL2008].[kgc-borgwarnerasbestos_2].dbo.vpolicy pol on ul.policyID = pol.policyID
where actiondate >= getdate()-30


GROUP BY UsageLogID,
case  when url  like '%/iuna_nosroccp/%' then 'IUNA/Nosroc Claims Processing'
	when url like '%/schemes/%' then 'London Scheme Tracking'
	when url like '%/BWA/%' then 'BorgWarner Asbestos'
	when url like '%/crr/%' then 'IUNA/Nosroc Claims Processing'
	when url like '%/NSICP/%' then 'NSI Claims Processing'
	else 'Other'
	end,
	case when username like '%@kcicllc.com' or username like '' then 'KCIC'
	else firm end, 
	case when username like '' then 'Administrator' else username end, site, page, replace(url, '/extranet.','/admin.'), action, actiondate,
	 SchemeName, 
	 
	 case when url like '%/NSICP/%' then 
		coalesce(CPINSI.LastName + ', ','') + Coalesce(CPINSI.FirstName + ' ','') + coalesce(CPINSI.MiddleName,'')
		when url like '%/iuna_nosroccp/%' then 
	  coalesce(CPI.LastName + ', ','') + Coalesce(CPI.FirstName + ' ','') + coalesce(CPI.MiddleName,'') else '' End ,
	  case when url like '%/BWA/%' then pol.carriernickname + ' (' + pol.policynum + ') - ' + convert(nvarchar(15), pol.minpstartdate, 101) end --policyID, min start date, carrier
	  

order by ActionDate desc




GO
GRANT EXECUTE ON  [dbo].[sp_MOSS_LogResults_archive] TO [base_user]
GO
