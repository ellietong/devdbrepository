SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryClonePerson] (@PersonID int = NULL, @ClonePersonID int = NULL)
AS
SET NOCOUNT ON

----Variables used for testing--
--DECLARE @PersonID int = 5
--DECLARE @ClonePersonID int = 285

DECLARE @PersonName nvarchar(250)
DECLARE @ClonePersonName nvarchar(250)
SELECT @PersonName = RoleName FROM [Role] WHERE RoleID = @PersonID 
SELECT @ClonePersonName = RoleName FROM [Role] WHERE RoleID = @ClonePersonID


--STEP 1: Delete the person's existing settings
DELETE FROM RolePersonFirmAddressClientSite
WHERE PersonFirmAddressClientSiteID IN(SELECT PersonFirmAddressClientSiteID
									   FROM PersonFirmAddressClientSite
									   WHERE PersonID = @PersonID)

DELETE FROM PersonFirmAddressClientSite
WHERE PersonID = @PersonID

--STEP 2: Clone the firm address client site settings in table PersonFirmAddressClientSite and RolePersonFirmAddressClientSite 
DECLARE @CurrentPersonFirmAddressClientSiteID int, @NewPersonFirmAddressClientSiteID int

DECLARE CursorFirmAddressClientSiteMatching CURSOR FOR 
	SELECT PersonFirmAddressClientSiteID 
	FROM PersonFirmAddressClientSite
	WHERE PersonID = @ClonePersonID

OPEN CursorFirmAddressClientSiteMatching
FETCH CursorFirmAddressClientSiteMatching INTO @CurrentPersonFirmAddressClientSiteID

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO PersonFirmAddressClientSite (PersonID, FirmAddressClientSiteID, Active, IsPrimary)
		SELECT @PersonID AS PersonID, FirmAddressClientSiteID, Active, IsPrimary
		FROM PersonFirmAddressClientSite
		WHERE PersonFirmAddressClientSiteID = @CurrentPersonFirmAddressClientSiteID
		ORDER BY FirmAddressClientSiteID
	
	SELECT @NewPersonFirmAddressClientSiteID = MAX(PersonFirmAddressClientSiteID) FROM PersonFirmAddressClientSite WHERE PersonID = @PersonID

	INSERT INTO RolePersonFirmAddressClientSite (RoleID, PersonFirmAddressClientSiteID, CanEdit, CanAdd, CanDelete, Notes)
		SELECT RoleID, @NewPersonFirmAddressClientSiteID AS PersonFirmAddressClientSiteID, CanEdit, CanAdd, CanDelete, Notes
		FROM RolePersonFirmAddressClientSite
		WHERE PersonFirmAddressClientSiteID = @CurrentPersonFirmAddressClientSiteID
			AND RoleID NOT IN(SELECT RoleID FROM [Role] WHERE IsNotification = 1)
		ORDER BY PersonFirmAddressClientSiteID

	FETCH CursorFirmAddressClientSiteMatching INTO @CurrentPersonFirmAddressClientSiteID
END

CLOSE CursorFirmAddressClientSiteMatching
DEALLOCATE CursorFirmAddressClientSiteMatching
GO
GRANT VIEW DEFINITION ON  [dbo].[qryClonePerson] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryClonePerson] TO [Ligado_User]
GO
