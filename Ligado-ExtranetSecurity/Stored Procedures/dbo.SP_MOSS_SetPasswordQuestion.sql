SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_MOSS_SetPasswordQuestion]
	(@username nvarchar(80), @passwordQuestionID int, @answer nvarchar(128))
AS
BEGIN
	
	SET NOCOUNT ON;
	
	UPDATE vPerson
	SET PasswordQuestionID = @passwordQuestionID,
		PasswordAnswer = @answer
	WHERE Email = @username

   END

GO
