SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qrysfrmCaseFirmRole] (@CaseID int, @FirmID int)
AS
SET NOCOUNT ON
BEGIN

--DECLARE @CaseID int
--DECLARE @FirmID int
--SET @CaseID = 18
--SET @FirmID = 235

DECLARE @RoleOrder TABLE (RowIndex int IDENTITY, 
						  RoleID int)

INSERT INTO @RoleOrder(RoleID)
	SELECT RoleID
	FROM tblRole
	ORDER BY Role ASC

SELECT pr.RolePermissionsID, pr.CaseID, pr.FirmID, pr.RoleID
FROM tblPermissionsRole pr
	LEFT JOIN @RoleOrder ro ON pr.RoleID = ro.RoleID
WHERE CaseID = @CaseID
	AND FirmID = @FirmID
ORDER BY RowIndex

END
GO
