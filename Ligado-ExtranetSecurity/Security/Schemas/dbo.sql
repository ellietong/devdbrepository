GRANT VIEW DEFINITION ON SCHEMA:: [dbo] TO [base_user]
GRANT SELECT ON SCHEMA:: [dbo] TO [base_user]
GRANT EXECUTE ON SCHEMA:: [dbo] TO [base_user]
GRANT VIEW DEFINITION ON SCHEMA:: [dbo] TO [Ligado_User]
GRANT SELECT ON SCHEMA:: [dbo] TO [Ligado_User]
GRANT EXECUTE ON SCHEMA:: [dbo] TO [Ligado_User]
GO
