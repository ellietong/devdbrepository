IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'LigadoMarley')
CREATE LOGIN [LigadoMarley] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [LigadoMarley] FOR LOGIN [LigadoMarley]
GO
