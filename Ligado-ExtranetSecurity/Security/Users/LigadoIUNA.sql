IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'LigadoIUNA')
CREATE LOGIN [LigadoIUNA] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [LigadoIUNA] FOR LOGIN [LigadoIUNA]
GO
