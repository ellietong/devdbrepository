IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'LigadoPPG')
CREATE LOGIN [LigadoPPG] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [LigadoPPG] FOR LOGIN [LigadoPPG]
GO
