IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'LigadoSecurity')
CREATE LOGIN [LigadoSecurity] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [LigadoSecurity] FOR LOGIN [LigadoSecurity]
GO
