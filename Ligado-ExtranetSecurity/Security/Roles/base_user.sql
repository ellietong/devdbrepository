CREATE ROLE [base_user]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'base_user', N'ALLKCIC'
GO
EXEC sp_addrolemember N'base_user', N'LigadoMonitoring'
GO
EXEC sp_addrolemember N'base_user', N'lislev'
GO
