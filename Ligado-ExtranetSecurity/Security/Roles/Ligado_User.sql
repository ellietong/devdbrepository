CREATE ROLE [Ligado_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Ligado_User', N'LigadoGSX'
GO
EXEC sp_addrolemember N'Ligado_User', N'LigadoGT'
GO
EXEC sp_addrolemember N'Ligado_User', N'LigadoIUNA'
GO
EXEC sp_addrolemember N'Ligado_User', N'LigadoMarley'
GO
EXEC sp_addrolemember N'Ligado_User', N'LigadoPPG'
GO
EXEC sp_addrolemember N'Ligado_User', N'LigadoSecurity'
GO
EXEC sp_addrolemember N'Ligado_User', N'lislev'
GO
EXEC sp_addrolemember N'Ligado_User', N'spuser'
GO
