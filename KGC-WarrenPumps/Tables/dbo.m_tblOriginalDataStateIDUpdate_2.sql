CREATE TABLE [dbo].[m_tblOriginalDataStateIDUpdate_2]
(
[ReferenceID] [int] NULL,
[StateID] [int] NULL,
[DefenseCounselAddressID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblOriginalDataStateIDUpdate_2] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblOriginalDataStateIDUpdate_2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblOriginalDataStateIDUpdate_2] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblOriginalDataStateIDUpdate_2] TO [power_user]
GO
