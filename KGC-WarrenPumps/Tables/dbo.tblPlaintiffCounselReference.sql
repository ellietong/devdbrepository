CREATE TABLE [dbo].[tblPlaintiffCounselReference]
(
[ReferenceID] [float] NULL,
[PlaintiffCounselAddressID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounselReference] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounselReference] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounselReference] TO [power_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounselReference] TO [power_user]
GO
