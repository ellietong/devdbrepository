CREATE TABLE [dbo].[tblPlaintiffCounsel$]
(
[PlaintiffCounsel] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounsel$] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel$] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounsel$] TO [power_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel$] TO [power_user]
GO
