CREATE TABLE [dbo].[IMPORT_DocXRef]
(
[REFERENCE_ID] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCUMENT_NUMBER] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCUMENT_TITLE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCUMENT_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILENAME] [decimal] (10, 0) NULL,
[FOLDER] [nvarchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[IMPORT_DocXRef] TO [base_user]
GRANT SELECT ON  [dbo].[IMPORT_DocXRef] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[IMPORT_DocXRef] TO [power_user]
GRANT SELECT ON  [dbo].[IMPORT_DocXRef] TO [power_user]
GO
