CREATE TABLE [dbo].[tblOriginalData_Monahan]
(
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOriginalData_Monahan] TO [base_user]
GRANT SELECT ON  [dbo].[tblOriginalData_Monahan] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblOriginalData_Monahan] TO [power_user]
GRANT SELECT ON  [dbo].[tblOriginalData_Monahan] TO [power_user]
GO
