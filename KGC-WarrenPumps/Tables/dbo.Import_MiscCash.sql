CREATE TABLE [dbo].[Import_MiscCash]
(
[Year] [float] NULL,
[Date] [datetime] NULL,
[Transaction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Indemnity] [float] NULL,
[Defense Counsel] [float] NULL,
[Interest Income] [float] NULL,
[Pace Fees (3)] [float] NULL,
[Bank Service Charge] [float] NULL,
[Unposted] [float] NULL,
[Total] [float] NULL,
[Comment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Check Number] [float] NULL,
[Check Status- Cleared] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_MiscCash] TO [base_user]
GRANT SELECT ON  [dbo].[Import_MiscCash] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_MiscCash] TO [power_user]
GRANT SELECT ON  [dbo].[Import_MiscCash] TO [power_user]
GO
