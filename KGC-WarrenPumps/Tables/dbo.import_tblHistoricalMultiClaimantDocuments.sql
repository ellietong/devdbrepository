CREATE TABLE [dbo].[import_tblHistoricalMultiClaimantDocuments]
(
[DocumentNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InjPartyKey] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantPersonalInfoID] [int] NULL,
[DocumentType] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTypeID] [int] NULL,
[Folder] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[import_tblHistoricalMultiClaimantDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[import_tblHistoricalMultiClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[import_tblHistoricalMultiClaimantDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[import_tblHistoricalMultiClaimantDocuments] TO [power_user]
GO
