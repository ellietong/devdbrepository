CREATE TABLE [dbo].[billing_BilledHistory_Expense_Final]
(
[ClaimID] [int] NULL,
[MonthlyBillID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_KEY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimAmount] [money] NULL,
[ExposureDate_Date] [datetime] NULL,
[DiagnosisDate_Date] [datetime] NULL,
[AdjustmentCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[billing_BilledHistory_Expense_Final] TO [base_user]
GRANT SELECT ON  [dbo].[billing_BilledHistory_Expense_Final] TO [base_user]
GRANT SELECT ON  [dbo].[billing_BilledHistory_Expense_Final] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[billing_BilledHistory_Expense_Final] TO [power_user]
GRANT SELECT ON  [dbo].[billing_BilledHistory_Expense_Final] TO [power_user]
GO
