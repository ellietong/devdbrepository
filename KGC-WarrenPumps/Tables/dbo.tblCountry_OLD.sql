CREATE TABLE [dbo].[tblCountry_OLD]
(
[CountryID] [int] NOT NULL,
[Country] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCountry_OLD] ADD CONSTRAINT [PK_mEtblCountry] PRIMARY KEY CLUSTERED  ([CountryID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCountry_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblCountry_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCountry_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblCountry_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblCountry_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCountry_OLD] TO [power_user]
GO
