CREATE TABLE [dbo].[tblSavedIsMDL]
(
[SavedIsMDLID] [int] NOT NULL IDENTITY(1, 1),
[IsMDL] [bit] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedIsMDL] ADD CONSTRAINT [PK_tblSavedIsMDL] PRIMARY KEY CLUSTERED  ([SavedIsMDLID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedIsMDL] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedIsMDL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedIsMDL] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedIsMDL] TO [power_user]
GO
