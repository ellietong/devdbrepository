CREATE TABLE [dbo].[tblSmokeHistory_TEMP]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[START_DATE] [datetime] NULL,
[QUIT_DATE] [datetime] NULL,
[PACKS_PER_DAY] [decimal] (5, 2) NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSmokeHistory_TEMP] TO [base_user]
GRANT SELECT ON  [dbo].[tblSmokeHistory_TEMP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSmokeHistory_TEMP] TO [power_user]
GRANT SELECT ON  [dbo].[tblSmokeHistory_TEMP] TO [power_user]
GO
