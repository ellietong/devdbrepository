CREATE TABLE [dbo].[tblTrialGroup_TEMP]
(
[CLM_LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRIAL_DATE] [datetime] NULL,
[TRIAL_NAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRIAL_DESCRIPTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_ACTIVE] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTrialGroup_TEMP] TO [base_user]
GRANT SELECT ON  [dbo].[tblTrialGroup_TEMP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTrialGroup_TEMP] TO [power_user]
GRANT SELECT ON  [dbo].[tblTrialGroup_TEMP] TO [power_user]
GO
