CREATE TABLE [dbo].[tblCompanyNaming_TEMP]
(
[LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAMED_COMPANY] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCompanyNaming_TEMP] TO [base_user]
GRANT SELECT ON  [dbo].[tblCompanyNaming_TEMP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCompanyNaming_TEMP] TO [power_user]
GRANT SELECT ON  [dbo].[tblCompanyNaming_TEMP] TO [power_user]
GO
