CREATE TABLE [dbo].[tblTreatment]
(
[TreatmentID] [int] NOT NULL IDENTITY(1, 1),
[Treatment] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTreatment] ADD CONSTRAINT [PK_tblTreatment] PRIMARY KEY CLUSTERED  ([TreatmentID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblTreatment] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblTreatment] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblTreatment] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblTreatment] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTreatment] TO [base_user]
GRANT SELECT ON  [dbo].[tblTreatment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTreatment] TO [power_user]
GRANT SELECT ON  [dbo].[tblTreatment] TO [power_user]
GRANT INSERT ON  [dbo].[tblTreatment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTreatment] TO [power_user]
GO
