CREATE TABLE [dbo].[tblSavedIsFederal]
(
[SavedIsFederalID] [int] NOT NULL IDENTITY(1, 1),
[IsFederal] [bit] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedIsFederal] ADD CONSTRAINT [PK_tblSavedIsFederal] PRIMARY KEY CLUSTERED  ([SavedIsFederalID]) ON [PRIMARY]
GO
