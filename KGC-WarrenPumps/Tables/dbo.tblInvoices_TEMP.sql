CREATE TABLE [dbo].[tblInvoices_TEMP]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_PERIOD_DATE] [datetime] NULL,
[INVOICE_NUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_IDENTIFIER] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_STATUS] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FEE_DISBURSEMENT_AMT] [decimal] (15, 2) NULL,
[FEE_AMT] [decimal] (15, 2) NULL,
[CHECK_DATE] [datetime] NULL,
[CHECK_NUMBER] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHK_AMT] [decimal] (15, 2) NULL,
[SUBMITTED_TO_CLIENT_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUBMITTED_FROM_COMPANY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_DISBURSEMENT_AMT] [decimal] (15, 2) NULL,
[INVOICE_FEE_AMT] [decimal] (15, 2) NULL,
[SUBMITTED_FROM_COMPANY] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADJUSTMENT_AMT] [decimal] (15, 2) NULL,
[ALLOCATION_DISBURSEMENT_AMT] [decimal] (15, 2) NULL,
[ALLOCATION_FEE_AMT] [decimal] (15, 2) NULL,
[SUBMITTAL_DATE] [datetime] NULL,
[SUBMITTED_TO_CLIENT] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblInvoices_TEMP] TO [base_user]
GRANT SELECT ON  [dbo].[tblInvoices_TEMP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblInvoices_TEMP] TO [power_user]
GRANT SELECT ON  [dbo].[tblInvoices_TEMP] TO [power_user]
GO
