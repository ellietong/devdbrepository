CREATE TABLE [dbo].[tblEmployment_TEMP]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OCCUPATION] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JOBSITE] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_PRIMARY] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMMENTS] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[START_DATE] [datetime] NULL,
[END_DATE] [datetime] NULL,
[EARLIEST_EXPOSURE_DATE] [datetime] NULL,
[LATEST_EXPOSURE_DATE] [datetime] NULL,
[IS_APPROVED_EXPOSURE] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_SECONDARY] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JOBSITE_CITY] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JOBSITE_STATE] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblEmployment_TEMP] TO [base_user]
GRANT SELECT ON  [dbo].[tblEmployment_TEMP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblEmployment_TEMP] TO [power_user]
GRANT SELECT ON  [dbo].[tblEmployment_TEMP] TO [power_user]
GO
