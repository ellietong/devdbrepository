CREATE TABLE [dbo].[m_tblDecemeberOpentoSettled]
(
[ClaimLawsuitID] [int] NULL,
[PLAINTIFF'S ALLOCATION TO INDIVIDUAL CASE] [money] NULL,
[DATE ON FINAL PAYMENT RECEIVED FROM INSURER] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblDecemeberOpentoSettled] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblDecemeberOpentoSettled] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblDecemeberOpentoSettled] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblDecemeberOpentoSettled] TO [power_user]
GO
