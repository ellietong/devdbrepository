CREATE TABLE [dbo].[m_tblUpdateDecemberPendingSett]
(
[ClaimLawsuitID] [int] NULL,
[ClaimantSettlementID] [int] NULL,
[SettlementAmount] [money] NULL,
[SettlementDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblUpdateDecemberPendingSett] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblUpdateDecemberPendingSett] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblUpdateDecemberPendingSett] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblUpdateDecemberPendingSett] TO [power_user]
GO
