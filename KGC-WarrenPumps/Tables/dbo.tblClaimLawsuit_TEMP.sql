CREATE TABLE [dbo].[tblClaimLawsuit_TEMP]
(
[CLM_LWST_KEY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_KEY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LWST_KEY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_PRIMARY_FOR_CLAIM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INDIVIDUAL_DOCKET_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS_DATE] [datetime] NULL,
[ORIGINAL_FILE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit_TEMP] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit_TEMP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit_TEMP] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit_TEMP] TO [power_user]
GO
