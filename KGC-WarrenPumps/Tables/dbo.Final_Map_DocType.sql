CREATE TABLE [dbo].[Final_Map_DocType]
(
[DOCUMENT_TYPE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantDocumentTypeID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Final_Map_DocType] TO [base_user]
GRANT SELECT ON  [dbo].[Final_Map_DocType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Final_Map_DocType] TO [power_user]
GRANT SELECT ON  [dbo].[Final_Map_DocType] TO [power_user]
GO
