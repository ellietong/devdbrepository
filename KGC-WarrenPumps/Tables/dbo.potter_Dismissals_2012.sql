CREATE TABLE [dbo].[potter_Dismissals_2012]
(
[ClaimantID] [float] NULL,
[Claimant Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Processed Date] [datetime] NULL,
[Status Date] [datetime] NULL,
[ClaimLawsuitID] [float] NULL,
[ClaimLawsuitSTatusTrackingID] [float] NULL,
[PLAINTIFF'S LAST NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLAINTIFF'S FIRST NAME & MIDDLE INITIAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISMISSED _WITH OR WITHOUT PREJUDICE?] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DATE OF VERBAL AGREEMENT TO DISMISS] [datetime] NULL,
[DATE ON DISMISSAL DOCUMENTATION] [datetime] NULL,
[DATE DISMISSAL DOCUMENTATION RECEIVED] [datetime] NULL,
[COMMENTS (FOUR LINES OR LESS)] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F15] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F16] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[potter_Dismissals_2012] TO [base_user]
GRANT SELECT ON  [dbo].[potter_Dismissals_2012] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[potter_Dismissals_2012] TO [power_user]
GRANT SELECT ON  [dbo].[potter_Dismissals_2012] TO [power_user]
GO
