CREATE TABLE [dbo].[tblLawsuit_TEMP]
(
[LWST_KEY] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LWST_ID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CASE_CAPTION] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCKET_NUMBER] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILE_DATE] [datetime] NULL,
[JURISDICTION] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COURT_TYPE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EARLIEST_SERVICE_DATE] [datetime] NULL,
[EARLIEST_RECEIVED_DATE] [datetime] NULL,
[ACTIVE_TRIAL_DATE] [datetime] NULL,
[DEFENSE_COUNSEL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCAL_PLAINTIFF_COUNSEL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NATIONAL_PLAINTIFF_COUNSEL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEAD_PLNTF_FIRST_NAME] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEAD_PLNTF_LAST_NAME] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_MULTI_DISTRICT] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_THIRD_PARTY_COMPLAINT] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLawsuit_TEMP] TO [base_user]
GRANT SELECT ON  [dbo].[tblLawsuit_TEMP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLawsuit_TEMP] TO [power_user]
GRANT SELECT ON  [dbo].[tblLawsuit_TEMP] TO [power_user]
GO
