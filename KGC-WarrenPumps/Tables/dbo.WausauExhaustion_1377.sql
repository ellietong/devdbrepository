CREATE TABLE [dbo].[WausauExhaustion_1377]
(
[MonthlyBillID] [float] NULL,
[Month Year] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [float] NULL,
[SumOfTotalIndemnity] [float] NULL,
[SumOfTotalExpense] [float] NULL,
[INJ_PRTY_KEY] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[WausauExhaustion_1377] TO [base_user]
GRANT SELECT ON  [dbo].[WausauExhaustion_1377] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WausauExhaustion_1377] TO [power_user]
GRANT SELECT ON  [dbo].[WausauExhaustion_1377] TO [power_user]
GO
