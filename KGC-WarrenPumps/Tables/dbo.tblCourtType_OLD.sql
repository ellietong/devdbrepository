CREATE TABLE [dbo].[tblCourtType_OLD]
(
[CourtTypeID] [int] NOT NULL IDENTITY(1, 1),
[CourtType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCourtType_OLD] ADD CONSTRAINT [PK_mBtblCourtType] PRIMARY KEY CLUSTERED  ([CourtTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCourtType_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblCourtType_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCourtType_OLD] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblCourtType_OLD] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCourtType_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblCourtType_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblCourtType_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCourtType_OLD] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType_OLD', 'COLUMN', N'CourtType'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType_OLD', 'COLUMN', N'CourtType'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType_OLD', 'COLUMN', N'CourtType'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType_OLD', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType_OLD', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType_OLD', 'COLUMN', N'CourtTypeID'
GO
