CREATE TABLE [dbo].[m_tblMultiClaimantDocument_Through20130131]
(
[MultiClaimantDocumentID] [int] NULL,
[ClaimantPersonalInfoID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblMultiClaimantDocument_Through20130131] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblMultiClaimantDocument_Through20130131] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblMultiClaimantDocument_Through20130131] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblMultiClaimantDocument_Through20130131] TO [power_user]
GO
