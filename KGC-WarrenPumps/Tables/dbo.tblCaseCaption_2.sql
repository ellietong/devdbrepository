CREATE TABLE [dbo].[tblCaseCaption_2]
(
[CaseCaption] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCaseCaption_2] TO [base_user]
GRANT SELECT ON  [dbo].[tblCaseCaption_2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCaseCaption_2] TO [power_user]
GRANT SELECT ON  [dbo].[tblCaseCaption_2] TO [power_user]
GO
