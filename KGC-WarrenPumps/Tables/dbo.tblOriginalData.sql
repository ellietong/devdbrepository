CREATE TABLE [dbo].[tblOriginalData]
(
[ReferenceID] [float] NULL,
[LastName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalCourt] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [float] NULL,
[Jurisdiction] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JurisdictionID] [float] NULL,
[CourtTypeID] [float] NULL,
[DocketNumber] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CaseCaption] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounsel] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deceased] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DummyDeceasedDate] [datetime] NULL,
[Disease] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrialDate] [datetime] NULL,
[NoticeOfClaimDate] [datetime] NULL,
[ClaimStatusCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResolutionDate] [datetime] NULL,
[SettlementPaidDate] [datetime] NULL,
[SettlementAmount] [money] NULL,
[Comments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOriginalData] TO [base_user]
GRANT SELECT ON  [dbo].[tblOriginalData] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblOriginalData] TO [power_user]
GRANT SELECT ON  [dbo].[tblOriginalData] TO [power_user]
GO
