CREATE TABLE [dbo].[FINAL_SETTLEMENT]
(
[CLM_RSLTN_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RSLTN_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLEMENT_GROUP_ID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLEMENT_GROUP] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RSLTN_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLE_AMT] [decimal] (15, 2) NULL,
[FIRST_PAYABLE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAY_TO] [nvarchar] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHECK_NBR] [decimal] (10, 0) NULL,
[PAID_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHECK_AMT] [decimal] (17, 2) NULL,
[ALLCTN_AMT] [decimal] (15, 2) NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[FINAL_SETTLEMENT] TO [base_user]
GRANT SELECT ON  [dbo].[FINAL_SETTLEMENT] TO [base_user]
GRANT SELECT ON  [dbo].[FINAL_SETTLEMENT] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[FINAL_SETTLEMENT] TO [power_user]
GRANT SELECT ON  [dbo].[FINAL_SETTLEMENT] TO [power_user]
GO
