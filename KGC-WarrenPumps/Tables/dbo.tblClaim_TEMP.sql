CREATE TABLE [dbo].[tblClaim_TEMP]
(
[CLM_KEY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_KEY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_CURRENT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_SEQ] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLAINTIFF_DISEASE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLAINTIFF_DIAGNOSTIC_DATE] [datetime] NULL,
[DEFENSE_DISEASE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEFENSE_DIAGNOSTIC_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MATTER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NEW_FILING_DATE] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaim_TEMP] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaim_TEMP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaim_TEMP] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaim_TEMP] TO [power_user]
GO
