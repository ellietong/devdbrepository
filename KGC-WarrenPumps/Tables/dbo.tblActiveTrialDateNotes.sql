CREATE TABLE [dbo].[tblActiveTrialDateNotes]
(
[ActiveTrialDateNoteID] [int] NOT NULL IDENTITY(1, 1),
[ActiveTrialDateNote] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblActiveTrialDateNotes] ADD CONSTRAINT [PK_tblActiveTrialDateNotes] PRIMARY KEY CLUSTERED  ([ActiveTrialDateNoteID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblActiveTrialDateNotes] TO [base_user]
GRANT SELECT ON  [dbo].[tblActiveTrialDateNotes] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblActiveTrialDateNotes] TO [power_user]
GRANT SELECT ON  [dbo].[tblActiveTrialDateNotes] TO [power_user]
GO
