CREATE TABLE [dbo].[tblMonth_OLD]
(
[MonthID] [int] NOT NULL IDENTITY(1, 1),
[MonthName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMonth_OLD] ADD CONSTRAINT [PK_mtblMonth] PRIMARY KEY CLUSTERED  ([MonthID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMonth_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblMonth_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMonth_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblMonth_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblMonth_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMonth_OLD] TO [power_user]
GO
