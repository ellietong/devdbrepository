CREATE TABLE [dbo].[m_tblWarNumberLink]
(
[ClaimantPersonalInfoID] [int] NULL,
[LastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReferenceID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblWarNumberLink] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblWarNumberLink] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblWarNumberLink] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblWarNumberLink] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblWarNumberLink] TO [power_user]
GO
