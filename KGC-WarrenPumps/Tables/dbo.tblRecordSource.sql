CREATE TABLE [dbo].[tblRecordSource]
(
[RecordSourceID] [int] NOT NULL IDENTITY(1, 1),
[RecordSource] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceDescription] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcludeRecordSource] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRecordSource] ADD CONSTRAINT [PK_tblRecordSource] PRIMARY KEY CLUSTERED  ([RecordSourceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblRecordSource] TO [base_user]
GRANT SELECT ON  [dbo].[tblRecordSource] TO [base_user]
GRANT SELECT ON  [dbo].[tblRecordSource] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblRecordSource] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblRecordSource] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblRecordSource] TO [Enviro_User]
GRANT VIEW DEFINITION ON  [dbo].[tblRecordSource] TO [power_user]
GRANT SELECT ON  [dbo].[tblRecordSource] TO [power_user]
GRANT INSERT ON  [dbo].[tblRecordSource] TO [power_user]
GRANT UPDATE ON  [dbo].[tblRecordSource] TO [power_user]
GO
