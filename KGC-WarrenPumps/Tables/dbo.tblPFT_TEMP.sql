CREATE TABLE [dbo].[tblPFT_TEMP]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TEST_DATE] [datetime] NULL,
[PHYSICIAN_NAME] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PFT_TYPE] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FVC_ACTUAL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FEV1_ACTUAL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FEV1_FVC_ACTUAL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLC_ACTUAL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DLCO_ACTUAL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TV] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPFT_TEMP] TO [base_user]
GRANT SELECT ON  [dbo].[tblPFT_TEMP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPFT_TEMP] TO [power_user]
GRANT SELECT ON  [dbo].[tblPFT_TEMP] TO [power_user]
GO
