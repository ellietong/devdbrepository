CREATE TABLE [dbo].[tblAcuityStateLink]
(
[StateID] [int] NULL,
[StateAcuityKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAcuityStateLink] TO [base_user]
GRANT SELECT ON  [dbo].[tblAcuityStateLink] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAcuityStateLink] TO [power_user]
GRANT SELECT ON  [dbo].[tblAcuityStateLink] TO [power_user]
GO
