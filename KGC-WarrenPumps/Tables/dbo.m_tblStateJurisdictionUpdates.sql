CREATE TABLE [dbo].[m_tblStateJurisdictionUpdates]
(
[ReferenceID] [int] NULL,
[StateID] [int] NULL,
[DefenseCounselAddressID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblStateJurisdictionUpdates] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblStateJurisdictionUpdates] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblStateJurisdictionUpdates] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblStateJurisdictionUpdates] TO [power_user]
GO
