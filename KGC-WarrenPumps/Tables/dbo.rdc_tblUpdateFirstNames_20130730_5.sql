CREATE TABLE [dbo].[rdc_tblUpdateFirstNames_20130730_5]
(
[ClaimantPersonalInfoID] [bigint] NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuffixID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[rdc_tblUpdateFirstNames_20130730_5] TO [base_user]
GRANT SELECT ON  [dbo].[rdc_tblUpdateFirstNames_20130730_5] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[rdc_tblUpdateFirstNames_20130730_5] TO [power_user]
GRANT SELECT ON  [dbo].[rdc_tblUpdateFirstNames_20130730_5] TO [power_user]
GO
