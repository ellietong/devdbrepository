CREATE TABLE [dbo].[m_ClaimantsNotLinkedFromPACEWithDocs]
(
[INJ_PRTY_KEY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REFERENCE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOCIAL_SECURITY_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_CURRENT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_PRIMARY_FOR_CLAIM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCKET_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JURISDICTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NEW_FILING_DATE] [date] NULL,
[ORIGINAL_FILE_DATE] [date] NULL,
[EARLIEST_SERVICE_DATE] [date] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_ClaimantsNotLinkedFromPACEWithDocs] TO [base_user]
GRANT SELECT ON  [dbo].[m_ClaimantsNotLinkedFromPACEWithDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_ClaimantsNotLinkedFromPACEWithDocs] TO [power_user]
GRANT SELECT ON  [dbo].[m_ClaimantsNotLinkedFromPACEWithDocs] TO [power_user]
GO
