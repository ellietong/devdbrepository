CREATE TABLE [dbo].[tblOccurrenceType]
(
[OccurrenceTypeID] [int] NOT NULL IDENTITY(1, 1),
[OccurrenceType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceTypeNotes] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseType] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccurrenceType] ADD CONSTRAINT [PK_tblOccurrenceType] PRIMARY KEY CLUSTERED  ([OccurrenceTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrenceType] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrenceType] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrenceType] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[tblOccurrenceType] TO [Occurrence_User]
GRANT DELETE ON  [dbo].[tblOccurrenceType] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[tblOccurrenceType] TO [Occurrence_User]
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrenceType] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccurrenceType] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccurrenceType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccurrenceType] TO [power_user]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceType'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceType'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceType'
GO
DECLARE @xp smallint
SELECT @xp=5415
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceType'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceType'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceTypeNotes'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceTypeNotes'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceTypeNotes'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceTypeNotes'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceType', 'COLUMN', N'OccurrenceTypeNotes'
GO
