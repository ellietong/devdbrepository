CREATE TABLE [dbo].[rdc_tblFirstNameCorrectionsLookup]
(
[ClaimantPersonalInfoID] [int] NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[rdc_tblFirstNameCorrectionsLookup] TO [base_user]
GRANT SELECT ON  [dbo].[rdc_tblFirstNameCorrectionsLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[rdc_tblFirstNameCorrectionsLookup] TO [power_user]
GRANT SELECT ON  [dbo].[rdc_tblFirstNameCorrectionsLookup] TO [power_user]
GO
