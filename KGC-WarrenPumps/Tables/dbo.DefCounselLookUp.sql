CREATE TABLE [dbo].[DefCounselLookUp]
(
[Defense_Counsel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[DefCounselLookUp] TO [base_user]
GRANT SELECT ON  [dbo].[DefCounselLookUp] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[DefCounselLookUp] TO [power_user]
GRANT SELECT ON  [dbo].[DefCounselLookUp] TO [power_user]
GO
