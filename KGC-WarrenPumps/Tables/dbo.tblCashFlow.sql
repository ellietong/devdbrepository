CREATE TABLE [dbo].[tblCashFlow]
(
[CashFlowID] [bigint] NOT NULL IDENTITY(1, 1),
[BankID] [int] NULL,
[TransactionTypeID] [int] NULL,
[Date] [datetime] NULL,
[Indemnity] [money] NOT NULL CONSTRAINT [DF_mBtblCashFlow_Indemnity] DEFAULT ((0)),
[DefenseCounsel] [money] NOT NULL CONSTRAINT [DF_mBtblCashFlow_DefenseCounsel] DEFAULT ((0)),
[Nonbillable_DefenseCounsel] [money] NOT NULL CONSTRAINT [DF_mBtblCashFlow_Nonbillable_DefenseCounsel] DEFAULT ((0)),
[MiscFees] [money] NOT NULL CONSTRAINT [DF_mBtblCashFlow_MiscFees] DEFAULT ((0)),
[Shortfall] [money] NOT NULL CONSTRAINT [DF_mBtblCashFlow_Shortfall] DEFAULT ((0)),
[Bundled] [money] NOT NULL CONSTRAINT [DF_mBtblCashFlow_Bundled] DEFAULT ((0)),
[InvestmentInterest] [money] NOT NULL CONSTRAINT [DF_mBtblCashFlow_InvestmentInterest] DEFAULT ((0)),
[UnpostedInsurer] [money] NOT NULL CONSTRAINT [DF_mBtblCashFlow_UnpostedAIG] DEFAULT ((0)),
[UnpostedSelf] [money] NOT NULL CONSTRAINT [DF_mBtblCashFlow_UnpostedNSISelf] DEFAULT ((0)),
[UnpostedCashReceipts] [money] NOT NULL CONSTRAINT [DF_mBtblCashFlow_UnpostedCashReceipts] DEFAULT ((0)),
[Total] [money] NOT NULL CONSTRAINT [DF_tblCashFlow_Total] DEFAULT ((0)),
[Comment] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCashFlow] ADD CONSTRAINT [PK_mBtblCashFlow] PRIMARY KEY CLUSTERED  ([CashFlowID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCashFlow] TO [base_user]
GRANT SELECT ON  [dbo].[tblCashFlow] TO [base_user]
GRANT SELECT ON  [dbo].[tblCashFlow] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblCashFlow] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblCashFlow] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCashFlow] TO [power_user]
GRANT SELECT ON  [dbo].[tblCashFlow] TO [power_user]
GRANT INSERT ON  [dbo].[tblCashFlow] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCashFlow] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblCashFlow', 'COLUMN', N'TransactionTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblCashFlow', 'COLUMN', N'TransactionTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblCashFlow', 'COLUMN', N'TransactionTypeID'
GO
