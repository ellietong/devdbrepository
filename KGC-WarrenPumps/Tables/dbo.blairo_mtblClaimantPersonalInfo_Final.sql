CREATE TABLE [dbo].[blairo_mtblClaimantPersonalInfo_Final]
(
[ClaimantPersonalInfoID] [int] NOT NULL IDENTITY(1, 1),
[INJ_PRTY_KEY] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REFERENCE_ID] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_ID] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUFFIX] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOCIAL_SECURITY_NUMBER] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BIRTH_DATE] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEATH_DATE] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MARITAL_STATUS] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GENDER] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS1] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS2] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP_CODE] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHONE_NUMBER] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SMOKER] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuffixID] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaritalStatusID] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GenderID] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSmoker] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[blairo_mtblClaimantPersonalInfo_Final] TO [base_user]
GRANT SELECT ON  [dbo].[blairo_mtblClaimantPersonalInfo_Final] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[blairo_mtblClaimantPersonalInfo_Final] TO [power_user]
GRANT SELECT ON  [dbo].[blairo_mtblClaimantPersonalInfo_Final] TO [power_user]
GO
