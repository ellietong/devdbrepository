CREATE TABLE [dbo].[tblClaimantNamesTemp]
(
[lastname] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[firstname] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[middlename] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[claimantpersonalinfoid] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantNamesTemp] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantNamesTemp] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantNamesTemp] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantNamesTemp] TO [power_user]
GO
