CREATE TABLE [dbo].[tblProductType]
(
[ProductTypeID] [int] NOT NULL IDENTITY(1, 1),
[ProductType] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProductType] ADD CONSTRAINT [PK_tblClaimantProductType] PRIMARY KEY CLUSTERED  ([ProductTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProductType] TO [base_user]
GRANT SELECT ON  [dbo].[tblProductType] TO [base_user]
GRANT SELECT ON  [dbo].[tblProductType] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblProductType] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblProductType] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblProductType] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblProductType] TO [power_user]
GRANT SELECT ON  [dbo].[tblProductType] TO [power_user]
GRANT INSERT ON  [dbo].[tblProductType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProductType] TO [power_user]
GO
