CREATE TABLE [dbo].[monahanc_2012NewFilingUpdates]
(
[LawsuitID] [int] NULL,
[ClaimLawsuitStatusTrackingID] [int] NULL,
[ServiceDate] [date] NULL,
[StatusNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[monahanc_2012NewFilingUpdates] TO [base_user]
GRANT SELECT ON  [dbo].[monahanc_2012NewFilingUpdates] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[monahanc_2012NewFilingUpdates] TO [power_user]
GRANT SELECT ON  [dbo].[monahanc_2012NewFilingUpdates] TO [power_user]
GO
