CREATE TABLE [dbo].[m_InvSubmissionUpdate]
(
[INVOICE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirmID] [float] NULL,
[VendorFirmID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_InvSubmissionUpdate] TO [base_user]
GRANT SELECT ON  [dbo].[m_InvSubmissionUpdate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_InvSubmissionUpdate] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_InvSubmissionUpdate] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[m_InvSubmissionUpdate] TO [power_user]
GRANT SELECT ON  [dbo].[m_InvSubmissionUpdate] TO [power_user]
GO
