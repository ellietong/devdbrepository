CREATE TABLE [dbo].[m_tblOriginalDataStateIDUpdate]
(
[ReferenceID] [int] NULL,
[StateID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblOriginalDataStateIDUpdate] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblOriginalDataStateIDUpdate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblOriginalDataStateIDUpdate] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblOriginalDataStateIDUpdate] TO [power_user]
GO
