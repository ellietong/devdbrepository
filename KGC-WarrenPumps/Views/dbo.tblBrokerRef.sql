SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblBrokerRef]
AS
SELECT BrokerRefID, BrokerRefNum
FROM [SQL2008].[KGA-Carrier].dbo.tblBrokerRefNum bl


GO
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerRef] TO [base_user]
GRANT SELECT ON  [dbo].[tblBrokerRef] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerRef] TO [power_user]
GRANT SELECT ON  [dbo].[tblBrokerRef] TO [power_user]
GO
