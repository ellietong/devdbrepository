SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vIncludedSiteNames] AS
SELECT s.siteid, s.sitename, SiteAddress, s.StatusID, [Status], SiteCity, s.StateID, [State], FullNameState, ZipCode, s.CountryID, Country,
s.SiteActivityID, SiteActivity, Division, OwnBegDate, OwnEndDate, OpBegDate, OpEndDate, ContaminationDate, SiteNotes,
TriggerStartDate, TriggerEndDate, LiabilityShare, ExcludedSite, ExcludedSiteReason, s.SiteTypeID, SiteType, s.CorporateHistoryID, CorporateHistoryProfile
FROM tblEnvirositesdetail s
LEFT JOIN tblStatus st ON s.StatusID = st.StatusID
LEFT JOIN tblState se ON s.StateID = se.StateID
LEFT JOIN tblCountry c ON s.CountryID = c.CountryID
LEFT JOIN tblSiteActivity sa ON s.SiteActivityID = sa.SiteActivityID
LEFT JOIN tblCorporateHistoryProfile hp ON s.CorporateHistoryID = hp.CorporateHistoryID
LEFT JOIN tblSiteType sit ON s.SiteTypeID = sit.SiteTypeID
WHERE ExcludedSite <> 1


GO
GRANT VIEW DEFINITION ON  [dbo].[vIncludedSiteNames] TO [base_user]
GRANT SELECT ON  [dbo].[vIncludedSiteNames] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vIncludedSiteNames] TO [power_user]
GRANT SELECT ON  [dbo].[vIncludedSiteNames] TO [power_user]
GO
