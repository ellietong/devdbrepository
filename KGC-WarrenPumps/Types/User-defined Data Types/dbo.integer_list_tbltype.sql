CREATE TYPE [dbo].[integer_list_tbltype] AS TABLE
(
[n] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([n])
)
GO
GRANT CONTROL ON TYPE:: [dbo].[integer_list_tbltype] TO [base_user]
GRANT VIEW DEFINITION ON TYPE:: [dbo].[integer_list_tbltype] TO [base_user]
GRANT REFERENCES ON TYPE:: [dbo].[integer_list_tbltype] TO [base_user]
GRANT CONTROL ON TYPE:: [dbo].[integer_list_tbltype] TO [power_user]
GRANT VIEW DEFINITION ON TYPE:: [dbo].[integer_list_tbltype] TO [power_user]
GRANT REFERENCES ON TYPE:: [dbo].[integer_list_tbltype] TO [power_user]
GO
