SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_MATTER_qryMatterFilterList]
AS 
    SET NOCOUNT ON

    SELECT  MatterID ,
            Matter
    FROM    tblMatter
    UNION
    SELECT  NULL ,
            '<< All Matters >>'
    ORDER BY Matter
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_MATTER_qryMatterFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_MATTER_qryMatterFilterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_MATTER_qryMatterFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_MATTER_qryMatterFilterList] TO [power_user]
GO
