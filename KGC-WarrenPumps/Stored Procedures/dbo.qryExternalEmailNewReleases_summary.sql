SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryExternalEmailNewReleases_summary]
AS
BEGIN
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/


--Declare and set email inputs.
DECLARE @Subject NVARCHAR (100)
DECLARE @LogoHeader NVARCHAR (2000)

DECLARE @Today4PM DATETIME
DECLARE @Yesterday4PM DATETIME
SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE()
- 1)) + ' 16:00:00.000')
SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE()))
+ ' 16:00:00.000')



SET @Subject = 'Warren Claims: New Releases Uploaded '
+ CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewReleasesTable TABLE
(
DocID INT,
docTitle NVARCHAR (500),
FirmID INT,
lName NVARCHAR (50),
fName NVARCHAR (50),
ClaimantPersonalInfoID BIGINT,
ReleaseSignedDate DATE,
[State] NVARCHAR (5),
ClaimLawsuitStatus NVARCHAR (500),
DefenseCounsel NVARCHAR (400),
LawsuitID INT,
UserName NVARCHAR (300)
)
INSERT INTO @NewReleasesTable
	SELECT
		d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/IMO/cp/Pages/DocumentProfile.aspx?ItemId='
		+ COALESCE(CONVERT(nvarchar(50), d.DocumentID), '')
		+ '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		DefenseCounselID,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		COALESCE(MAX(ReleaseSignedDate), '1/1/1900'),
		s.State,
		cls.ClaimLawsuitStatus,
		COALESCE(DefenseCounsel, ''),
		cl.LawsuitID,
		CreatedBy
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
	LEFT JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblSettlePaymentDocumentation spd ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatus cls ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
	--LEFT JOIN tblAllegation a					ON a.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblState s ON s.StateID = l.StateID
	LEFT JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	--LEFT JOIN tblDefendantCompany dc			ON a.DefendantCompanyID = dc.DefendantCompanyID
	WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
	AND d.ClaimantDocumentTypeID IN (31)
	--AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	--AND RIGHT(COALESCE(AcceptedBy, ''), 11) <> 'kcicllc.com'
	--AND l.DefenseCounselAddressID IS NOT NULL
	--AND ( c.MatterID <> 4
	--    OR a.AllegationTypeID IN ( 2, 3 )
	-- )
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	AND IsPrimaryStatus = 1
	AND CreatedBy NOT LIKE 'HISTORICAL'

	GROUP BY	s.State,
				LastName,
				d.documentID,
				d.DocumentTitle,
				DefenseCounselID,
				FirstName,
				cpi.ClaimantPersonalInfoID,
				ClaimLawsuitStatus,
				DefenseCounsel,
				cl.LawsuitID,
				CreatedBy



DECLARE @ClaimantPerDoc TABLE
(
DocID INT,
ClaimantList NVARCHAR (MAX)
)
DECLARE @DocID INT,
@LastDocID INT,
@Claimant NVARCHAR (MAX),
@ClaimantID BIGINT,
@ClaimantList NVARCHAR (MAX)

DECLARE Cur_Releases CURSOR Local Fast_Forward
FOR SELECT
	DocID,
	COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
	claimantpersonalInfoID
FROM @NewReleasesTable
OPEN Cur_Releases
FETCH NEXT FROM Cur_Releases INTO @DocID, @Claimant, @ClaimantID
WHILE @@FETCH_STATUS = 0
BEGIN


IF (LEN (@ClaimantList) = 0
OR @ClaimantList IS NULL
)
BEGIN

SET @ClaimantList = '<ul style="margin:0; padding:0;"> <a href="https://www.kcicextranet.com/clients/IMO/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
+ CONVERT(nvarchar(50), @claimantID) + '">'
+ @Claimant + COALESCE(' ('
+ CONVERT(nvarchar(50), @claimantID)
+ ')', '') + '</a></ul>'
END
ELSE
BEGIN

SET @ClaimantList = COALESCE(@ClaimantList, '')
+ '<ul style="margin:0; padding:0;">  <a href="https://www.kcicextranet.com/clients/IMO/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
+ CONVERT(nvarchar(50), @claimantID) + '">'
+ COALESCE(@Claimant, '') + COALESCE(' ('
+ CONVERT(nvarchar(50), @claimantID)
+ ')', '')
+ '</a></ul>'

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Releases INTO @DocID, @Claimant,
@ClaimantID
PRINT @@FETCH_STATUS
IF @DocID < > @LastDocID
OR @@FETCH_STATUS < > 0
BEGIN

INSERT INTO @ClaimantPerDoc
	SELECT
		@LastDocID,
		@ClaimantList

SET @ClaimantList = NULL
END
END
CLOSE Cur_Releases
DEALLOCATE Cur_Releases



--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage NVARCHAR (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader
+ '<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	
 
 
--->
</STYLE>'

DECLARE @LastLawsuitID NVARCHAR (250) = ''

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

IF (SELECT
	COUNT(docTitle)
FROM @NewReleasesTable) > 0
BEGIN
SET @BodyMessage = @BodyMessage
+ '<H4>The following releases have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage
+ N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">'
SET @BodyMessage = @BodyMessage
+ N'<tr><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Document Title</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Linked Claimants (ClaimantID)</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Release Signed Date*</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">State</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Defense Counsel</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Status</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">UserName</th>'

DECLARE @DocTitle NVARCHAR (250),
@clClaimantList NVARCHAR (500),
@ReleaseSignedDate NVARCHAR (300),
@State NVARCHAR (250),
@ClaimLawsuitStatus NVARCHAR (400),
@DefenseCounsel NVARCHAR (400),
@LawsuitID INT,
@UserName NVARCHAR (300),
@Namings NVARCHAR (500),
@NamingsRowID NVARCHAR (10)

DECLARE Doc_List CURSOR
FOR SELECT
	docTitle,
	cl.ClaimantList,
	CAST(MAX(ReleaseSignedDate) AS varchar(MAX)),
	[state],
	ClaimLawsuitStatus,
	DefenseCounsel,
	LawsuitID,
	UserName
FROM @NewReleasesTable C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
GROUP BY	[state],
			cl.docid,
			docTitle,
			cl.ClaimantList,
			ClaimLawsuitStatus,
			DefenseCounsel,
			LawsuitID,
			UserName
ORDER BY cl.DocID

OPEN Doc_List
FETCH NEXT FROM Doc_List INTO @DocTitle, @clClaimantList,
@ReleaseSignedDate, @State, @ClaimLawsuitStatus, @DefenseCounsel,
@LawsuitID, @UserName

WHILE @@FETCH_STATUS = 0
BEGIN


SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @DocTitle + ' </td>'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @clClaimantList + ' </td>'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @ReleaseSignedDate + ' </td>'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @State + ' </td>'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @DefenseCounsel + ' </td>'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @ClaimLawsuitStatus + ' </td>'
SET @BodyMessage = @BodyMessage
+ '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
+ @UserName + ' </td>'


SET @BodyMessage = @BodyMessage + '</td></tr>'

SET @LastLawsuitID = @LawsuitID

FETCH NEXT FROM Doc_List INTO @DocTitle,
@clClaimantList, @ReleaseSignedDate, @State,
@ClaimLawsuitStatus, @DefenseCounsel, @LawsuitID,
@UserName

END
CLOSE Doc_List
DEALLOCATE Doc_List


SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'


END
ELSE
BEGIN
SET @BodyMessage = @BodyMessage
+ '<H4>No releases have been uploaded in the last 24 hours. </H4>'
END



--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;',
'>')

----Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/IMO/cp/Pages/Documents.aspx">here</A> to access the document(s).</b><br><br>'

--Service Date Assumption 

IF (SELECT
	COUNT(docTitle)
FROM @NewReleasesTable) > 0
BEGIN
SET @BodyMessage = @BodyMessage
+ '<br>*Note: Release signed date is the date the release was executed.<br><br>'
END

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'



--IF (Select COUNT(docTitle) from @NewReleasesTable) >0 
--begin
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'settlements@piercedavis.com',
								--@recipients = 'hagep@kcic.com',
								@copy_recipients = '',
								@blind_copy_recipients = 'potterm@kcic.com;warren@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'
END

GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewReleases_summary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewReleases_summary] TO [base_user]
GRANT ALTER ON  [dbo].[qryExternalEmailNewReleases_summary] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewReleases_summary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewReleases_summary] TO [power_user]
GO
