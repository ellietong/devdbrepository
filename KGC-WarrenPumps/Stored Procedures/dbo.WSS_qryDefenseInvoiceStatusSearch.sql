SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROC [dbo].[WSS_qryDefenseInvoiceStatusSearch]
AS
SET NOCOUNT ON

SELECT statusid, status
FROM tblDefenseInvoiceStatus
where StatusID <>5
UNION
	--SELECT null as StatusID, '<< All Status >>' As status
	--UNION
	SELECT null, ''
order by Status
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDefenseInvoiceStatusSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDefenseInvoiceStatusSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDefenseInvoiceStatusSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDefenseInvoiceStatusSearch] TO [power_user]
GO
