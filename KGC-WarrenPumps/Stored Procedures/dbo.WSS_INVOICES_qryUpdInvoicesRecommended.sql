SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_INVOICES_qryUpdInvoicesRecommended] (@DefenseInvoiceID BigInt, @EmailAddress nvarchar(1000), @IsRecommended bit = null, @ApprovalNotes nvarchar(4000))
AS

BEGIN
	UPDATE tblDefenseInvoice
	SET RecommendedBy = @EmailAddress,
		IsRecommended = @IsRecommended,
		RecommendedDate = Getdate(),
		PaymentApprovalNotes = @ApprovalNotes
	WHERE DefenseInvoiceID = @DefenseInvoiceID
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdInvoicesRecommended] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdInvoicesRecommended] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdInvoicesRecommended] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdInvoicesRecommended] TO [power_user]
GO
