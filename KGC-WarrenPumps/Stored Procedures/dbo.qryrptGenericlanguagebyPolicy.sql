SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryrptGenericlanguagebyPolicy]
(@selectedProvisions nvarchar(1000))
AS
SET NOCOUNT ON

------Declare the lists of selected IDs; note: these will be passed in, and thus should be commented out in actual stored proc
--DECLARE @selectedProvisions nvarchar(1000)
--SET @selectedProvisions = '0'

--Declare backup strings of the passed in lists to be used in the ultimate where clauses below
DECLARE @selectedProvisionsList nvarchar(1000)
SET @selectedProvisionsList = @selectedProvisions

--Declare the tables to be used for the separated provision and provision value IDs
DECLARE @selectedProvisionsTable TABLE(provisionLabelID int)

--Prepare flags if there is only a zero in the table (and thus there should be no filter)
DECLARE @flagProvisionFilter int
SET @flagProvisionFilter = 0

--Parse out provision and provision value ID strings	
WHILE LEN(@selectedProvisions)>0 BEGIN
	IF CHARINDEX(',',@selectedProvisions) = 0 BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, @selectedProvisions)
		IF CONVERT(int,@selectedProvisions) <> 0
			SET @flagProvisionFilter = 1
		SET @selectedProvisions = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, LEFT(@selectedProvisions, CHARINDEX(',',@selectedProvisions)-1))
		SET @selectedProvisions = RIGHT(@selectedProvisions,LEN(@selectedProvisions)-CHARINDEX(',',@selectedProvisions))
		SET @flagProvisionFilter = 1
	END
END


IF @flagProvisionFilter = 1 
SELECT GenericLanguage, gl.genericlanguageID 
FROM tblExactLanguage el
INNER JOIN tblGenericLanguage gl on gl.GenericLanguageID=el.GenericLanguageID
INNER JOIN tblProvisionLanguage PL on PL.ExactLanguageID=el.ExactLanguageID
WHERE ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable)
group by GenericLanguage, gl.GenericLanguageID

else
SELECT GenericLanguage, gl.genericlanguageID 
FROM tblExactLanguage el
INNER JOIN tblGenericLanguage gl on gl.GenericLanguageID=el.GenericLanguageID
INNER JOIN tblProvisionLanguage PL on PL.ExactLanguageID=el.ExactLanguageID
group by GenericLanguage, gl.GenericLanguageID



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptGenericlanguagebyPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptGenericlanguagebyPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptGenericlanguagebyPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptGenericlanguagebyPolicy] TO [power_user]
GO
