SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryCreateSettlementGroup]
	-- Add the parameters for the stored procedure here
(@SettlementGroupName nvarchar(500), @PaymentTermsID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO tblSettlementGroup (SettlementGroup, PaymentTermsID)
	SELECT @SettlementGroupName, @PaymentTermsID
	
	SELECT MAX(SettlementGroupID) FROM tblSettlementGroup

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryCreateSettlementGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryCreateSettlementGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryCreateSettlementGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryCreateSettlementGroup] TO [power_user]
GO
