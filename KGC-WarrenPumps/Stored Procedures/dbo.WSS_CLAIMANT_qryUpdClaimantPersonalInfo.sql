SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryUpdClaimantPersonalInfo]
    (
      @ClaimantPersonalInfoID BIGINT ,
      @Address1 NVARCHAR(250) ,
      @Address2 NVARCHAR(250) ,
      @StateID INT ,
      @City NVARCHAR(250) ,
      @Zip NVARCHAR(5) ,
      @Mailzip NVARCHAR(4) ,
      @BirthDate DATETIME ,
      @DeceasedDate DATETIME ,
      @GenderID INT ,
      @MaritalStatusID INT ,
      @IsSmoker BIT ,
      @SmokerStart DATETIME ,
      @SmokerEnd DATETIME ,
      @PacksPerDay FLOAT ,
      @Username NVARCHAR(250) ,
      @FullSSN NVARCHAR(150),
      @ReferenceID NVARCHAR(150)
	)
AS 
    SET NOCOUNT ON

    UPDATE  tblClaimantPersonalInfo
    SET     Address1 = UPPER(@Address1) ,
            Address2 = UPPER(@Address2) ,
            StateID = @StateID ,
            City = UPPER(@City) ,
            Zip = @Zip ,
            MailZip = @Mailzip ,
            BirthDate = @BirthDate ,
            DeceasedDate = @DeceasedDate ,
            GenderID = @GenderID ,
            MaritalStatusID = @MaritalStatusID ,
            IsSmoker = @IsSmoker ,
            SmokerStart = @SmokerStart ,
            SmokerEnd = @SmokerEnd ,
            PacksPerDay = @PacksPerDay ,
            SPUsername = @Username ,
            SocialSecurityNumber = REPLACE(REPLACE(@FullSSN, '-', ''), ' ', '')
            --ReferenceID = UPPER(@ReferenceID)
    WHERE   ClaimantPersonalInfoID = @ClaimantPersonalInfoID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantPersonalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantPersonalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantPersonalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantPersonalInfo] TO [power_user]
GO
