SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmContactInfoSelect]
As

SET NOCOUNT ON

SELECT c.ContactID, cct.ContactClaimType AS 'Claim Type', c.Company AS Company,	ct.ContactType AS 'Contact Type', 
	COALESCE (c.LastName, '') + ', ' + COALESCE (c.FirstName, '') AS Name, 
	COALESCE (c.AddressLine1, '') + ' ' + COALESCE (c.AddressLine2, '') + ' - ' + COALESCE (c.City, '') + ' ' + 
	COALESCE (s.State, '') + ' ' + COALESCE (c.Zip, '') AS Address, c.Phone AS 'Phone #', c.Fax AS 'Fax #', c.Email AS 'Email Address' 

FROM dbo.tblContact c 
INNER JOIN dbo.tblContactClaimType cct ON cct.ContactClaimTypeID = c.ContactClaimTypeID 
LEFT JOIN dbo.tblState s ON s.StateID = c.StateID 
INNER JOIN dbo.tblContactType ct ON ct.ContactTypeID = c.ContactTypeID 

ORDER BY cct.ContactClaimType, c.Company, ct.ContactType, COALESCE (c.LastName, '') + ', ' + COALESCE (c.FirstName, '')

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmContactInfoSelect] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmContactInfoSelect] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmContactInfoSelect] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmContactInfoSelect] TO [power_user]
GO
