SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation]
    (
      @SetPymtID INT ,
      @SettlePaymentComments NVARCHAR(MAX) ,
      @ReleaseTypeID INT ,
      @ReleaseSigned DATE ,
      @ExecutedReleaseDate DATE = NULL ,
      @MedicareReported BIT ,
      @MedicareFullyDocumented BIT,
	  @MedicareReportedDate datetime
																	--@ReleaseRqstDate date)
																	
    )
AS --DECLARE @ClaimID int
--SET @ClaimID = 1

    UPDATE  tblSettlePaymentDocumentation
    SET     SettlePaymentComments = @SettlePaymentComments ,
            ReleaseTypeID = CASE WHEN @ReleaseTypeID = 0 THEN NULL
                                 ELSE @ReleaseTypeID END ,
            ReleaseSignedDate = NULLIF(@ReleaseSigned, '') ,
		    MedicareReported = @MedicareReported ,
            MedicareFullyDocumented = @MedicareFullyDocumented,           
            MedicareFormSent = @MedicareReportedDate,
            ExecutedReleaseDate = @ExecutedReleaseDate--COALESCE(@ExecutedReleaseDate, Coalesce(ExecutedReleaseDate,''))--CASE WHEN @ReleaseSigned IS NULL THEN ExecutedReleaseDate ELSE GETDATE() END
		
    WHERE   SetPymtID = @SetPymtID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GO
