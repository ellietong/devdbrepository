SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryfrmViewClaimantComplaints] (@ClaimantPersonalInfoID int)
AS

--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 55

SELECT
	d.DocumentID AS ClaimantDocumentID,
	mcd.ClaimantPersonalInfoID,
	CASE
		WHEN DateCreated IS NULL THEN ClaimantDocumentType + ': ' + DocumentTitle ELSE ClaimantDocumentType + ': ' + DocumentTitle + ': ' + CONVERT(nvarchar, MONTH(DateCreated)) + '/' + CONVERT(nvarchar, DAY(DateCreated)) + '/' + CONVERT(nvarchar, YEAR(DateCreated))
	END AS DocumentTypeWithDate,
	'https://admin.kcicllc.com' + DocumentURL AS DocumentLink
FROM tblMultiClaimantDocument mcd
INNER JOIN tblDocuments d ON mcd.DocumentID = d.DocumentID
INNER JOIN tblClaimantDocumentType ct ON d.ClaimantDocumentTypeID = ct.ClaimantDocumentTypeID
WHERE mcd.ClaimantPersonalInfoID = @CLaimantPersonalInfoID
AND d.ClaimantDocumentTypeID IN (8, 4, 33)
AND mcd.QCBy IS NULL

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewClaimantComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewClaimantComplaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewClaimantComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewClaimantComplaints] TO [power_user]
GO
