SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID] ( @ClaimID BIGINT )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @AllegationCount INT = ( SELECT COUNT(A.AllegationID)
                                         FROM   tblAllegation a
                                                INNER JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = a.ClaimLawsuitID
                                         WHERE  ClaimID = @ClaimID
                                       )

        SELECT  ClaimID ,
                AllegationID ,
                DefendantCompany ,
                CASE WHEN SettlementShare IS NULL
                          AND @AllegationCount = 1 THEN 100
                     ELSE COALESCE(SettlementShare, 0)
                END AS SettlementShare ,
                SettlementAmount
                * ( CASE WHEN SettlementShare IS NULL
                              AND @AllegationCount = 1
                         THEN COALESCE(SettlementShare / 100, 1)
                         ELSE SettlementShare / 100
                    END ) AS DollarShare
        FROM    tblAllegation a
                INNER JOIN tblClaimLawsuit cl ON a.ClaimLawsuitID = cl.ClaimLawsuitID
                LEFT JOIN tblDefendantCompany defcom ON a.DefendantCompanyID = defcom.DefendantCompanyID
                LEFT JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
        WHERE   ClaimID = @ClaimID
                AND COALESCE(IsPrimaryForClaim, 0) = 1
        GROUP BY ClaimID ,
                AllegationID ,
                DefendantCompany ,
                SettlementShare ,
                SettlementAmount
        ORDER BY ClaimID ,
                DefendantCompany
    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID] TO [power_user]
GO
