SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SEARCH_qryDocumentTypesComplaint](@EmailAddress nvarchar(max)=null)
AS 
    SET NOCOUNT ON
    
    DECLARE @FirmAddressID INT
    SET @FirmAddressID = ( SELECT   FirmAddressID
                           FROM     vtblPerson
                           WHERE    Email = @emailaddress
                         )
    
IF @FirmAddressID IN (433, 434, 511, 512, 513, 514, 515, 285, 249, 516, 531,534)
	BEGIN
    SELECT  ClaimantDocumentTypeID ,
            ClaimantDocumentType
    FROM    tblClaimantDocumentType
    WHERE   ClaimantDocumentTypeID = 8
    
   END
   ELSE
   SELECT  ClaimantDocumentTypeID ,
            ClaimantDocumentType
    FROM    tblClaimantDocumentType
    WHERE   ClaimantDocumentGroupID <> 2
    UNION
    SELECT  NULL ,
            '<< All Document Types >>'
    ORDER BY ClaimantDocumentType ASC

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentTypesComplaint] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentTypesComplaint] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentTypesComplaint] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentTypesComplaint] TO [power_user]
GO
