SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryUpdClaim]
    (
      @LawsuitID BIGINT ,
      @ActiveTrialDate DATETIME ,
      @PrimaryLawsuit NVARCHAR(250) ,
      @IsMDL NVARCHAR(250) ,
	  @IsFederal NVARCHAR(250) ,
      @TrialGroupID BIGINT,
      @ClaimID INT ,
      @ActiveTrialDateNoteID INT,
      @Username NVARCHAR(250), 
      @PlaintiffCounselAddressID INT,
      @PrimaryNationalPlaintiffCounselID INT,
      @DocketNumber NVARCHAR(250)
	)
AS 


    UPDATE  tblLawsuit
    SET     SPUsername = @Username,
			DocketNumber = @DocketNumber,
			PlaintiffCounselAddressID = @PlaintiffCounselAddressID,
			PrimaryNationalPlaintiffCounselID = @PrimaryNationalPlaintiffCounselID
    WHERE   LawsuitID = @LawsuitID	
	
    UPDATE  tblClaimLawsuit
    SET     ActiveTrialDate = @ActiveTrialDate ,
            IsPrimaryForClaim = CASE WHEN @PrimaryLawsuit = 'YES' THEN 1
                                     ELSE 0
                                END ,
            IsMDL = CASE WHEN @IsMDL = 'YES' THEN 1
                         ELSE 0
                    END ,
			IsFederal = CASE WHEN @IsFederal = 'YES' THEN 1
                         ELSE 0
                    END ,
            TrialGroupID = @TrialGroupID,
            SPUsername = @Username,
            ActiveTrialDateNoteID = @ActiveTrialDateNoteID
    WHERE   LawsuitID = @LawsuitID
            AND ClaimID = @ClaimID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GO
