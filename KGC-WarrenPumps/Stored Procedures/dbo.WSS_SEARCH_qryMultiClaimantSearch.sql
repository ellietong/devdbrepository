
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ellie Tong
-- change date: 03/05/2015
-- Description: Change cpi.ReferenceID to c.ReferenceID
-- =============================================

 
CREATE PROC [dbo].[WSS_SEARCH_qryMultiClaimantSearch]
    (
      @ClaimantName NVARCHAR(MAX) ,
      @SSN NVARCHAR(20) ,
      @StateID INT ,
      @PlaintiffCounselID INT ,
	  @NationalPlaintiffCounselID INT ,
      @ReferenceID NVARCHAR(250) ,
      @IsMDL INT ,
	  @IsFederal INT ,
      @emailaddress NVARCHAR(250) ,
      @DocketNumber NVARCHAR(250) ,
      @ClaimLawsuitStatusGroupID INT ,
      @ClaimantIDs integer_list_tbltype READONLY
    )
AS 
    SET NOCOUNT ON
 
----The following set of variables can be used for testing.
--DECLARE @ClaimantName              nvarchar(max)
--DECLARE @SSN                       nvarchar(20)
--DECLARE @StateID                   int
--DECLARE @PlaintiffCounselAddressID int
--,@PlaintiffCounselID int
--,@NationalPlaintiffCounselID int
--DECLARE @ReferenceID               nvarchar(250)
----DECLARE @MatterID                  int
--DECLARE @IsMDL                     int
--DECLARE @IsFederal                     int
--DECLARE @emailaddress              nvarchar(250)
--DECLARE @DocketNumber              nvarchar(250)
--DECLARE @ClaimLawsuitStatusGroupID int
--DECLARE @ClaimantIDs               TABLE (n int) 
--SET @ClaimantName              = NULL
--SET @SSN                       = NULL
--SET @StateID                   = NULL
--SET @PlaintiffCounselAddressID = NULL
--SET @ReferenceID               = 'WAR'
----SET @MatterID                  = NULL
--SET @IsMDL                     = NULL
--SET @IsFederal                     = NULL
--SET @emailaddress              = 'tonge@kcic.com'
--SET @DocketNumber              = NULL
--SET @ClaimLawsuitStatusGroupID = NULL
 
    DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )
 
--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
    IF OBJECT_ID('tempdb..#ClaimantIDs') IS NOT NULL 
        BEGIN
            DROP TABLE #ClaimantIDs
        END
    CREATE TABLE #ClaimantIDs ( n INT )
    INSERT  INTO #ClaimantIDs
            SELECT  n
            FROM    @ClaimantIDs
	
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE 1=1'
    IF @ClaimantName IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND ((CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' end) like ''%'' + @ClaimantNameParam + ''%'')'
        END
    IF @SSN IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND COALESCE(socialsecurityNumber, '''') like ''%'' + @SSNParam + ''%'''
        END
    IF @StateID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND l.StateID = '
                + CONVERT(NVARCHAR(20), @StateID)
        END
    IF @PlaintiffCounselID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND COALESCE(pca.PlaintiffCounselID,0) = '
                + CONVERT(NVARCHAR(20), @PlaintiffCounselID)
        END
    IF @NationalPlaintiffCounselID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND COALESCE(npca.PlaintiffCounselID,0) = '
                + CONVERT(NVARCHAR(20), @NationalPlaintiffCounselID)
        END
    IF @ReferenceID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND c.ReferenceID LIKE ''%'' +  @ReferenceIDParam + ''%'''
        END
 
    IF @IsMDL IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND COALESCE(IsMDL, 0) = '
                + CONVERT(NVARCHAR(20), @IsMDL)
        END

    IF @IsFederal IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND COALESCE(IsFederal, 0) = '
                + CONVERT(NVARCHAR(20), @IsFederal)
        END

    IF @DocketNumber IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND DocketNumber LIKE ''%'' + @DocketNumberParam + ''%'''
        END
    IF @ClaimLawsuitStatusGroupID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND clst.ClaimLawsuitStatusID = '
                + CONVERT(NVARCHAR(20), @ClaimLawsuitStatusGroupID)
        END
    IF ( SELECT COUNT(n)
         FROM   #ClaimantIDs
       ) <> 0 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)'
        END
 
--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    DECLARE @JoinString NVARCHAR(MAX)
    IF ( ( RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' )
         OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        SET @JoinString = ''
    ELSE 
        BEGIN
            SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
            SET @WhereString = @WhereString
                + ' AND (dj.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ' OR l.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ')'
        END
 
--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
    DECLARE @QueryString NVARCHAR(MAX)
    SET @QueryString = 'SELECT DISTINCT CPI.ClaimantPersonalInfoID AS ClaimantID, 
		CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '' '' ELSE MiddleName + '' '' END AS ClaimantName, 
		CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
		BirthDate, Coalesce(pca.PlaintiffCounsel, ''Unknown'') + '' ('' + Coalesce(pca.PlaintiffCounselCity, ''Unknown'') + '')'' As PlaintiffCounsel,
		Coalesce(npca.PlaintiffCounsel, ''Unknown'') + '' ('' + Coalesce(npca.PlaintiffCounselCity, ''Unknown'') + '')'' As NationalPlaintiffCounsel,
		c.ReferenceID, 
		s.[State], 
		IsMDL, 
		ClaimLawsuitStatus, 
		DocketNumber ,
		pca.PlaintiffCounselID,
		npca.PlaintiffCounselID
	 FROM dbo.tblClaimantPersonalInfo cpi
		INNER JOIN dbo.tblClaim c				 ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl			 ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l					 ON cl.LawsuitID = l.LawsuitID '
        + @JoinString
        + '
		
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		LEFT JOIN tblPlaintiffCounselAddress npca ON l.PrimaryNationalPlaintiffCounselID = npca.PlaintiffCounselAddressID
		LEFT JOIN tblState s					 ON l.StateID = s.StateID
		LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls		 ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN tblClaimLawsuitStatusGroup clsg	   ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID'
        + @WhereString + '
	 ORDER BY ClaimantName 
	 OPTION (RECOMPILE)'
 
    PRINT @querystring
--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString,
        N'@ClaimantNameParam nvarchar(max), @SSNParam nvarchar(max), @DocketNumberParam nvarchar(max), @ReferenceIDParam nvarchar(max)',
        @ClaimantNameParam = @ClaimantName, @SSNParam = @SSN,
        @DocketNumberParam = @DocketNumber, @ReferenceIDParam = @ReferenceID
 



GO

GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [power_user]
GO
