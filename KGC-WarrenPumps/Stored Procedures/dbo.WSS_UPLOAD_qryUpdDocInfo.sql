SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryUpdDocInfo]
    (
      @DocumentID INT ,
      @DocumentTypeID INT ,
      @DocumentSourceID INT ,
      @Accept BIT ,
      @EmailAddress NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON

    IF @Accept = 1
        AND RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' 
        BEGIN
            UPDATE  tblDocuments
            SET     ClaimantDocumentTypeID = @DocumentTypeID ,
                    DocumentSourceID = @DocumentSourceID ,
		--FirmID = @FirmID,
                    DateModified = GETDATE() ,
                    ModifiedBy = @EmailAddress ,
                    Accepted = CASE WHEN COALESCE(Accepted, 0) = 1
                                    THEN Accepted
                                    WHEN @Accept = 0
                                         AND COALESCE(Accepted, 0) = 0
                                    THEN NULL
                                    ELSE 1
                               END ,
                    AcceptedBy = CASE WHEN AcceptedBy IS NOT NULL
                                      THEN AcceptedBy
                                      WHEN @Accept = 1 THEN @EmailAddress
                                      ELSE NULL
                                 END ,
                    DateAccepted = CASE WHEN DateAccepted IS NOT NULL
                                        THEN DateAccepted
                                        WHEN @Accept = 1 THEN GETDATE()
                                        ELSE NULL
                                   END ,
                    QCBy = CASE WHEN QCBy IS NOT NULL THEN QCBy
                                WHEN @Accept = 1 THEN @EmailAddress
                                ELSE NULL
                           END ,
                    QCDate = CASE WHEN QCDate IS NOT NULL THEN QCDate
                                  WHEN @Accept = 1 THEN GETDATE()
                                  ELSE NULL
                             END
            WHERE   DocumentID = @DocumentID

            UPDATE  tblMultiClaimantDocument
            SET     QCBy = @EmailAddress ,
                    QCDate = GETDATE()
            WHERE   DocumentID = @DocumentID
        END
    ELSE 
        UPDATE  tblDocuments
        SET     ClaimantDocumentTypeID = @DocumentTypeID ,
                DocumentSourceID = @DocumentSourceID ,
		--FirmID = @FirmID,
                DateModified = GETDATE() ,
                ModifiedBy = @EmailAddress ,
                Accepted = CASE WHEN COALESCE(Accepted, 0) = 1 THEN Accepted
                                WHEN @Accept = 0
                                     AND COALESCE(Accepted, 0) = 0 THEN NULL
                                ELSE 1
                           END ,
                AcceptedBy = CASE WHEN AcceptedBy IS NOT NULL THEN AcceptedBy
                                  WHEN @Accept = 1 THEN @EmailAddress
                                  ELSE NULL
                             END ,
                DateAccepted = CASE WHEN DateAccepted IS NOT NULL
                                    THEN DateAccepted
                                    WHEN @Accept = 1 THEN GETDATE()
                                    ELSE NULL
                               END
        WHERE   DocumentID = @DocumentID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocInfo] TO [power_user]
GO
