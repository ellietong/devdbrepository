SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_MONTHLY_qryTopTenNewFilingsByStateByMonth] (@MatterID int = NULL, 
                                                                          @MonthID  int, 
                                                                          @YearID   int)  
AS
SET NOCOUNT ON
    
--Declare @month int
--Declare @year int
--Set @month = 11
--Set @year  = 2010

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

SELECT TOP 10 FullNameState As 'State',
	COUNT(DISTINCT(cl.ClaimLawsuitID)) As 'Count'
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
    LEFT JOIN (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
    LEFT  JOIN tblState s                         ON l.StateID = s.StateID
WHERE  month(coalesce(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) = @Month 
	AND year(coalesce(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) = @Year 
	AND MatterID >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
GROUP BY FullNameState
ORDER BY COUNT(distinct(cl.ClaimLawsuitID)) DESC


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenNewFilingsByStateByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenNewFilingsByStateByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenNewFilingsByStateByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenNewFilingsByStateByMonth] TO [power_user]
GO
