SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryRecentInvoices_Filtered]
    (
      @emailaddress NVARCHAR(1000) = NULL ,
      @FilterStatusID INT = NULL ,
      @FilterDocumentTypeID INT = NULL ,
      @FilterFirmID INT = NULL ,
      @FilterVendorFirmID INT = NULL
    )
AS --DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'hagep@kcicllc.com'
--DECLARE @FilterStatusID int
--DECLARE @FilterDocumentTypeID int
--DECLARE @FilterFirmID int
--DECLARE @FilterVendorFirmID int = null
 
--SET @FilterStatusID = null
--SET @FilterDocumentTypeID = null
--SET @FilterFirmID = null
 
    DECLARE @firmid INT
    SET @firmid = ( SELECT DISTINCT
                            FirmID
                    FROM    vtblperson
                    WHERE   Email = @emailaddress
                  )
 
    DECLARE @MaxStatusID INT
    SET @MaxStatusID = ( SELECT MAX(StatusID)
                         FROM   tblDefenseInvoiceStatus
                       )
 
    DECLARE @MaxDocType INT
    SET @MaxDocType = ( SELECT  MAX(ClaimantDocumentTypeID)
                        FROM    tblClaimantDocumentType
                      )
 
    DECLARE @MaxFirmID INT
    SET @MaxFirmID = ( SELECT   MAX(DefenseCounselID)
                       FROM     tblDefenseCounsel
                     )
 
    DECLARE @MaxVendorFirmID INT
    SET @MaxVendorFirmID = ( SELECT MAX(VendorFirmID)
                             FROM   m_tblVendorFirm
                           )
 
    IF ( ( @firmid IN ( 188 ) )
         OR ( LEFT(@emailaddress, 5) = 'kcic\' )
         OR ( RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' )
       ) 
        BEGIN
            SELECT DISTINCT TOP 200
                    CAST(d.DocumentID AS VARCHAR(50)) AS DocumentId ,
                    dt.ClaimantDocumentType ,
                    dg.ClaimantDocumentGroup ,
                    d.Comments ,
                    d.DocumentURL ,
                    d.DocumentTitle ,
                    d.CreatedBy ,
                    d.DateCreated ,
                    d.ModifiedBy ,
                    d.DateModified ,
                    d.Accepted ,
                    d.AcceptedBy ,
                    d.DateAccepted ,
                    di.ApprovedBy ,
                    di.DateApproved ,
                    f.Firm ,
                    di.InvoiceNumber ,
                    di.Period ,
                    di.DatePayable ,
                    di.fees AS FeeAmount ,
                    di.costs AS DisbursementAmount ,
                    di.adjustments AS Adjustment ,
                    di.Invoicenotes AS AdjustmentNotes ,
                    dis.Status ,
                    '' AS DefenseInvoiceFirm , 				
		--m.Matter,
                    c.sourcedate AS DatePaid ,
                    c.SourceNumber AS CheckNumber ,
                    di.FirmID ,
                    COALESCE(VendorFirm, '') AS VendorFirm ,
                    c.SourceDate ,
                    di.InvoiceTotal AS InvoiceTotal
            FROM    tblDocuments d
                    INNER JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
                    INNER JOIN tblClaimantDocumentGroup dg ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
                    LEFT JOIN vtblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		--LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON dinv.VendorFirmID = dif.FirmID 
                    LEFT JOIN tblDefenseInvoiceStatus dis ON di.statusid = dis.StatusID 
		--LEFT JOIN tblMatter m ON di.MatterID = m.MatterID
                    LEFT JOIN tblDefenseInvoice dinv ON dinv.DocumentID = d.DocumentID
                    LEFT JOIN vtblFirm f ON dinv.FirmID = f.FirmID
                    LEFT JOIN m_tblVendorFirm ex ON dinv.VendorFirmID = ex.VendorFirmID
                    LEFT JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
                    LEFT JOIN tblCheck c ON pd.CheckID = c.CheckID
		--Left join tblDefInvClaimantDetail dic on dic.DocumentID=di.DocumentID
            WHERE   dt.ClaimantDocumentGroupID = 2
                    AND dt.ClaimantDocumentTypeID >= COALESCE(@FilterDocumentTypeID,
                                                              0)
                    AND dt.ClaimantDocumentTypeID <= COALESCE(@FilterDocumentTypeID,
                                                              @maxDocType)
                    AND di.FirmID >= COALESCE(@FilterFirmID, 0)
                    AND di.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
                    AND COALESCE(di.statusid, 0) >= COALESCE(@FilterStatusID,
                                                             0)
                    AND di.statusid <= COALESCE(@FilterStatusID, @MaxStatusID)
                    AND COALESCE(dinv.VendorFirmID, 0) >= COALESCE(@FilterVendorFirmID,
                                                              0)
                    AND COALESCE(dinv.VendorFirmID, 0) <= COALESCE(@FilterVendorFirmID,
                                                              @maxVendorFirmID)
            ORDER BY DateCreated DESC ,
                    Accepted ASC
        END 
    ELSE 
        BEGIN
            SELECT DISTINCT TOP 200
                    CAST(d.DocumentID AS VARCHAR(50)) AS DocumentId ,
                    dt.ClaimantDocumentType ,
                    dg.ClaimantDocumentGroup ,
                    d.Comments ,
                    d.DocumentURL ,
                    d.DocumentTitle ,
                    d.CreatedBy ,
                    d.DateCreated ,
                    d.ModifiedBy ,
                    d.DateModified ,
                    d.Accepted ,
                    d.AcceptedBy ,
                    d.DateAccepted ,
                    di.ApprovedBy ,
                    di.DateApproved ,
                    f.Firm ,
                    di.InvoiceNumber ,
                    di.Period ,
                    di.DatePayable ,
                    di.fees AS FeeAmount ,
                    di.costs AS DisbursementAmount ,
                    di.adjustments AS Adjustment ,
                    di.invoicenotes AS AdjustmentNotes ,
                    dis.Status ,
                    '' AS DefenseInvoiceFirm , 				
		--m.Matter,
                    c.sourcedate AS DatePaid ,
                    c.SourceNumber AS CheckNumber ,
                    di.FirmID ,
                    COALESCE(VendorFirm, '') AS VendorFirm ,
                    c.SourceDate ,
                    di.InvoiceTotal AS InvoiceTotal
            FROM    tblDocuments d
                    INNER JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
                    INNER JOIN tblClaimantDocumentGroup dg ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
                    LEFT JOIN vtblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		--LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON dinv.VendorFirmID = dif.FirmID 
                    LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID 
		--LEFT JOIN tblMatter m ON di.MatterID = m.MatterID
                    LEFT JOIN tblDefenseInvoice dinv ON dinv.DocumentID = d.DocumentID
                    LEFT JOIN vtblFirm f ON dinv.FirmID = f.FirmID
                    LEFT JOIN m_tblVendorFirm ex ON dinv.VendorFirmID = ex.VendorFirmID
                    LEFT JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
                    LEFT JOIN tblCheck c ON pd.CheckID = c.CheckID
		--Left join tblDefInvClaimantDetail dic on dic.DocumentID=di.DocumentID
            WHERE   dg.ClaimantDocumentGroup = 2
                    AND f.FirmID = @firmid
                    AND dt.ClaimantDocumentTypeID >= COALESCE(@FilterDocumentTypeID,
                                                              0)
                    AND dt.ClaimantDocumentTypeID <= COALESCE(@FilterDocumentTypeID,
                                                              @maxDocType)
                    AND di.FirmID >= COALESCE(@FilterFirmID, 0)
                    AND di.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
                    AND COALESCE(di.StatusID, 0) >= COALESCE(@FilterStatusID,
                                                             0)
                    AND di.statusid <= COALESCE(@FilterStatusID, @MaxStatusID)
                    AND COALESCE(dinv.VendorFirmID, 0) >= COALESCE(@FilterVendorFirmID,
                                                              0)
                    AND COALESCE(dinv.VendorFirmID, 0) <= COALESCE(@FilterVendorFirmID,
                                                              @maxVendorFirmID)
                    AND ( ( COALESCE(di.statusid, 0) != 5 )
                          OR ( COALESCE(di.StatusID, 0) != 5 )
                        )
            ORDER BY DateCreated DESC ,
                    Accepted ASC
        END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryRecentInvoices_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryRecentInvoices_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryRecentInvoices_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryRecentInvoices_Filtered] TO [power_user]
GO
