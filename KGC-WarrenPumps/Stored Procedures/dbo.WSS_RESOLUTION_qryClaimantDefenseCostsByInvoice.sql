SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] 


	
-- Add the parameters for the stored procedure here
    (
      @ClaimantPersonalInfoID INT ,
      @DefenseCounselID NVARCHAR(250) ,
      @VendorFirmID INT
    )
AS 
    BEGIN
	
	
--DECLARE @ClaimantPersonalInfoID bigint
--DECLARE @DefenseCounselID int
--DECLARE @VendorFirmID int

--SET @ClaimantPersonalInfoID = 30661
--SET @DefenseCounselID = 138
--SET @VendorFirmID = 0
---- SET NOCOUNT ON added to prevent extra result sets from
	
-- interfering with SELECT statements.
	
        SET NOCOUNT ON;
	

----Approved Expert and General Invoices with a ClaimantID link
        SELECT  CPI.ClaimantPersonalInfoID ,
                di.VendorFirmID AS DefenseCounselID ,
                Di.InvoiceNumber AS InvoiceNumber ,
                SUM(COALESCE(Costs, 0) + COALESCE(Fees, 0)
                    + COALESCE(Adjustments, 0)) AS DefenseTotal ,
                CASE WHEN IsFullyPaid = 1 THEN 'Paid'
                     ELSE Status
                END AS Status ,
                SourceDate
        FROM    tblClaimantPersonalInfo cpi
                LEFT JOIN tblDefInvClaimantDetail DICD ON cpi.ClaimantPersonalInfoId = DICD.ClaimantpersonalInfoID
                LEFT JOIN tblDefenseInvoice di ON DICD.DefenseInvoiceID = DI.DefenseInvoiceID
                LEFT JOIN tblDefenseInvoiceStatus ds ON di.StatusID = ds.StatusID
                LEFT JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
                LEFT JOIN tblCheck c ON pd.CheckID = c.CheckID
        WHERE   cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
                AND DI.StatusID IN ( 1, 2,3,4 )
                AND (di.VendorFirmID = @VendorFirmID
                OR DefenseCounselAddressID = @DefenseCounselID)
        GROUP BY CPI.ClaimantPersonalInfoID ,
                di.VendorFirmID ,
                Di.InvoiceNumber ,
                CASE WHEN IsFullyPaid = 1 THEN 'Paid'
                     ELSE Status
                END ,
                SourceDate
        HAVING  SUM(COALESCE(Costs, 0) + COALESCE(Fees, 0)
                    + COALESCE(Adjustments, 0)) <> 0
        ORDER BY SourceDate DESC
    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [power_user]
GO
