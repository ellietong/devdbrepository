SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryTrialGroupClaimants]
(@TrialGroupID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT  CL.ClaimLawsuitID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + '' END + CASE WHEN MiddleName IS NULL THEN '' ELSE + ' ' + MiddleName END AS ClaimantName, 
	CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SocialSecurityNumber,
			CaseCaption, s.State, ClaimLawsuitStatus, Jurisdiction, cl.ActiveTrialDate, DocketNumber, TrialGroup, c.ClaimantPersonalInfoID
	FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	INNER JOIN tblState s ON l.StateID = s.StateID
	INNER JOIN tblJurisdiction j ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblTrialGroup tg ON cl.TrialGroupID = tg.TrialGroupID
	WHERE	COALESCE(IsPrimaryForClaim, 0) = 1 AND COALESCE(IsCurrentClaim, 0) = 1 AND IsPrimaryStatus = 1 AND cl.TrialGroupID = @TrialGroupID
	
END	

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryTrialGroupClaimants] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryTrialGroupClaimants] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryTrialGroupClaimants] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryTrialGroupClaimants] TO [power_user]
GO
