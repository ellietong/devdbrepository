SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryGetClaimantInvByDocID] ( @DocumentID INT )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        SELECT  dicd.ClaimantPersonalInfoID AS ClaimantID ,
                dicd.* ,
                cpi.LastName + ', ' + COALESCE(cpi.FirstName, '')
                + COALESCE(' ' + MiddleName, '') AS ClaimantName ,
                '' AS LegalBillStatus ,
                Matter ,
                d.DocumentID
        FROM    tblDefInvClaimantDetail dicd
                LEFT JOIN tblMatter m ON dicd.MatterID = m.MatterID
	--LEFT JOIN [kga-demodb].dbo.tblClaimantPersonalInfo cpi ON dicd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
                LEFT JOIN tblClaimantPersonalInfo cpi ON dicd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
                INNER JOIN tblDocuments d ON dicd.DocumentID = d.DocumentID
        WHERE   dicd.DocumentID = @DocumentID
        ORDER BY cpi.LastName + ', ' + COALESCE(cpi.FirstName, '')
                + COALESCE(' ' + MiddleName, '')
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryGetClaimantInvByDocID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryGetClaimantInvByDocID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryGetClaimantInvByDocID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryGetClaimantInvByDocID] TO [power_user]
GO
