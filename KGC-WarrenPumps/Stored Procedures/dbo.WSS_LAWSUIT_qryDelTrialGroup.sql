SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryDelTrialGroup]
(@TrialGroupID int, @SPUsername nvarchar(250))
AS
BEGIN

	UPDATE tblClaimLawsuit
	SET TrialGroupID = NULL,
		ActiveTrialDate = NULL,
		SPusername = @SPUsername
	WHERE TrialGroupID = @TrialGroupID
	
	UPDATE tblTrialGroup
	SET spusername = @SPUsername
	WHERE TrialGroupID = @TrialGroupID

	DELETE 
	FROM tblTrialGroup 
	WHERE TrialGroupID = @TrialGroupID	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryDelTrialGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryDelTrialGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryDelTrialGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryDelTrialGroup] TO [power_user]
GO
