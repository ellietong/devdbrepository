SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser]
	-- Add the parameters for the stored procedure here
	@emailaddress nvarchar(500), @SavedSearchID int
AS
BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT ClaimantName AS ParamValue, '' AS ID, 'CNParam' AS ParamType 
FROM tblSavedClaimantName scn
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = scn.SavedSearchID
WHERE SPUserName = @emailaddress AND scn.SavedSearchID = @SavedSearchID
UNION
SELECT SSN as SSN, '', 'SSNParam' 
FROM tblSavedSSN ssn
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = ssn.SavedSearchID
WHERE SPUserName = @emailaddress AND ssn.SavedSearchID = @SavedSearchID
UNION
SELECT DocketNumber AS Param, '', 'DNParam'
FROM tblSavedDocketNumber dn
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = dn.SavedSearchID
WHERE SPUserName = @emailaddress AND dn.SavedSearchID = @SavedSearchID
UNION
SELECT ClaimLawsuitStatus AS Param2, LawsuitStatusID, 'CLSParam' 
FROM tblSavedLawsuitStatus sls
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = sls.SavedSearchID
	INNER JOIN tblClaimLawsuitStatus cls ON sls.LawsuitStatusID = cls.ClaimLawsuitStatusID
WHERE SPUserName = @emailaddress AND sls.SavedSearchID = @SavedSearchID
UNION
SELECT CONVERT(nvarchar(15),ClaimantID), '', 'CIDParam'
FROM tblSavedClaimantIds scid
	INNER JOIN tblSavedSearch ss ON scid.SavedSearchID = ss.SavedSearchID
WHERE SPUserName = @emailaddress AND scid.SavedSearchID = @SavedSearchID
UNION
SELECT State, s.StateID, 'StParam' 
FROM tblSavedState s
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = s.SavedSearchID
	INNER JOIN tblState st ON s.StateID = st.StateID
WHERE SPUserName = @emailaddress AND s.SavedSearchID = @SavedSearchID
UNION
SELECT PlaintiffCounsel, pc.PlaintiffCounselAddressID, 'PCParam' 
FROM tblSavedPlaintiffCounsel pc
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = pc.SavedSearchID
	INNER JOIN tblPlaintiffCounselAddress pca ON pc.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE SPUserName = @emailaddress AND pc.SavedSearchID = @SavedSearchID
UNION
SELECT Matter, sm.MatterID, 'MParam' 
FROM tblSavedMatter sm
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = sm.SavedSearchID
	INNER JOIN tblMatter m ON sm.MatterID = m.MatterID
WHERE SPUserName = @emailaddress AND sm.SavedSearchID = @SavedSearchID
UNION
SELECT CONVERT(nvarchar(15),IsMDL), '', 'MDLParam' 
FROM tblSavedIsMDL mdl
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = mdl.SavedSearchID
WHERE SPUserName = @emailaddress AND mdl.SavedSearchID = @SavedSearchID
UNION
SELECT CONVERT(nvarchar(15),IsFederal), '', 'FedParam' 
FROM tblSavedIsFederal fed
	INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = fed.SavedSearchID
WHERE SPUserName = @emailaddress AND fed.SavedSearchID = @SavedSearchID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser] TO [power_user]
GO
