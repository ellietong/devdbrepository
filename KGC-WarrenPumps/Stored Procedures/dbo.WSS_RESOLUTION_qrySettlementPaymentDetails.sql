SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_RESOLUTION_qrySettlementPaymentDetails]
    (
      @ClaimantSettlementID BIGINT = NULL
    )
AS 
    SELECT  cs.ClaimantSettlementID ,
            ps.PaymentAmount ,
            cs.CheckNumber ,
            cs.PaidDate AS PaymentDate ,
            CASE WHEN CheckNumber > 0 THEN 'Payment Made'--SettlementAmount = PaymentAmount THEN 'Settlement Fully Paid'
                 --WHEN PaymentAmount = 0
                 --     AND SUM(PaymentAmount) > 0
                 --THEN 'Settlement Not Fully Paid'
                 ELSE 'No Payments Made'
            END AS SettlementPaymentStatus
    FROM    tblClaimantSettlement cs
            LEFT JOIN tblPaymentSettlement ps ON cs.ClaimantSettlementID = ps.ClaimantSettlementID
            LEFT JOIN tblCheck c ON ps.CheckID = c.CheckID
    WHERE   cs.ClaimantSettlementID = @ClaimantSettlementID
    GROUP BY cs.ClaimantSettlementID ,
            ps.PaymentAmount ,
             cs.CheckNumber ,
            cs.PaidDate,
--            IsFullyPaid,
            SettlementAmount
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qrySettlementPaymentDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qrySettlementPaymentDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qrySettlementPaymentDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qrySettlementPaymentDetails] TO [power_user]
GO
