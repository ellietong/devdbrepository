SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding]
    (
      @emailaddress NVARCHAR(1000) = NULL
    )
AS --DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'hagep@kcicllc.com'

    DECLARE @DefenseCounselID INT
    SET @DefenseCounselID = ( SELECT DISTINCT
                                        FirmID
                              FROM      vtblperson
                              WHERE     Email = @emailaddress
                            )

    IF ( ( @DefenseCounselID = 132 )
         OR ( LEFT(@emailaddress, 5) = 'kcic\' )
         OR ( RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' )
       ) 
        BEGIN
            SELECT  CAST(d.DocumentID AS VARCHAR(50)) AS DocumentId ,
                    d.DocumentTitle ,
                    d.DocumentURL ,
                    di.FirmID ,
                    f.Firm ,
                    dt.ClaimantDocumentType ,
                    di.Period ,
                    dinv.InvoiceTotal ,
                    di.DatePayable ,
                    dis.[Status] ,
                    d.AcceptedBy ,
                    d.DateAccepted ,
                    d.DateCreated ,
                    v.VendorName AS VendorFirm
            FROM    tblDocuments d
                    INNER JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
                    INNER JOIN tblClaimantDocumentGroup dg ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
                    LEFT JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID
                    LEFT JOIN vtblDefenseInvoice dinv ON dinv.DocumentID = d.DocumentID
                    LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID 
		--LEFT JOIN  tblMatter m					ON d.MatterID = m.MatterID
                    LEFT JOIN vtblFirm f ON di.FirmID = f.FirmID
                    LEFT JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
                    LEFT JOIN tblCheck c ON pd.CheckID = c.CheckID
                    LEFT JOIN tblVendor v ON v.VendorID = di.VendorFirmID
            WHERE   dt.ClaimantDocumentGroupID = 2 
		--AND d.StatusID IS NULL 
		--AND ((coalesce(d.statusid,0)!=5) or (coalesce(di.StatusID,0)!=5))
            ORDER BY Accepted ASC ,
                    DateCreated DESC
        END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [power_user]
GO
