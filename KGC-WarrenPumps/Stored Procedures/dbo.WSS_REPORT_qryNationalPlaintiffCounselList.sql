SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_REPORT_qryNationalPlaintiffCounselList]
AS 
    SET NOCOUNT ON

    SELECT  DISTINCT
			p.PlaintiffCounselID as NationalPlaintiffCounselID
            ,PlaintiffCounsel as NationalPlaintiffCounsel
    FROM    tblPlaintiffCounselAddress p
	INNER JOIN tblLawsuit l on l.PrimaryNationalPlaintiffCounselID = p.PlaintiffCounselAddressID
    WHERE   PlaintiffCounsel IS NOT NULL
            AND PlaintiffCounsel <> ''
    UNION
    SELECT  NULL ,
            NULL
    ORDER BY PlaintiffCounsel

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORT_qryNationalPlaintiffCounselList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORT_qryNationalPlaintiffCounselList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORT_qryNationalPlaintiffCounselList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORT_qryNationalPlaintiffCounselList] TO [power_user]
GO
