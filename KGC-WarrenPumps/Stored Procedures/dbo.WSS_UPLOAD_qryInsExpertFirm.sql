SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryInsExpertFirm]
    (
      @ExpertFirm NVARCHAR(250) ,
      @Address1 NVARCHAR(250) ,
      @Address2 NVARCHAR(250) ,
      @City NVARCHAR(250) ,
      @StateID INT ,
      @Zip NVARCHAR(10) ,
      @TaxID NVARCHAR(50) ,
      @SPUsername NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON

    INSERT  INTO m_tblExpertFirm
            SELECT  @ExpertFirm ,
                    @Address1 ,
                    @Address2 ,
                    @City ,
                    @StateID ,
                    @Zip ,
                    @TaxID ,
                    @SPUsername
	
    SELECT  MAX(ExpertFirmID)
    FROM    m_tblExpertFirm
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryInsExpertFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryInsExpertFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryInsExpertFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryInsExpertFirm] TO [power_user]
GO
