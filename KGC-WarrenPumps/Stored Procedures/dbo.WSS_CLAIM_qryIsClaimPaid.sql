SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIM_qryIsClaimPaid]
    (
      @ClaimantPersonalInfoID INT, @ClaimID INT
    )
AS 
    SET NOCOUNT ON

SELECT SUM(DISTINCT CASE WHEN PaidDate IS NOT NULL THEN 1 ELSE 0 END)
FROM tblClaimantSettlement cs 
    LEFT JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
    LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
WHERE   ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND cl.ClaimID = @ClaimID
GROUP BY c.ClaimantPersonalInfoID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryIsClaimPaid] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryIsClaimPaid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryIsClaimPaid] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryIsClaimPaid] TO [power_user]
GO
