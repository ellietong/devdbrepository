SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryRemoveTrialGroup]
(@ClaimLawsuitID bigint, @TrialGroupID int, @SPUsername nvarchar(250))
AS
BEGIN
	IF @ClaimLawsuitID IS NOT NULL
		BEGIN
			UPDATE tblClaimLawsuit
			SET TrialGroupID = NULL,
				ActiveTrialDate = NULL,
				SPusername = @SPUsername
			WHERE ClaimLawsuitID = @ClaimLawsuitID
		END
	ELSE
		BEGIN
			UPDATE tblClaimLawsuit
			SET TrialGroupID = NULL,
				ActiveTrialDate = NULL,
				SPusername = @SPUsername
			WHERE TrialGroupID = @TrialGroupID
		END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryRemoveTrialGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryRemoveTrialGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryRemoveTrialGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryRemoveTrialGroup] TO [power_user]
GO
