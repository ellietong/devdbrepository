SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryDismissalsByMonth] 
	(@StartDate datetime, @EndDate datetime, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @emailAddress nvarchar(250)
--Declare @StartDate datetime
--Declare @EndDate datetime
--Declare @TortTypeID int
 
--Set @emailAddress = 'monahanc@kcicllc.com'
--Set @StartDate = '1/1/2012'
--Set @EndDate = '4/1/2012'
--Set @TortTypeID = 1
 
 
DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
 
DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = 0--(select distinct InsurerRole from vtblPerson where Email = @emailaddress)
 
--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1)
BEGIN
 
select coalesce(DATENAME("mm", ProcessedDate) + ' ' + CONVERT(nvarchar, YEAR(ProcessedDate)), 'Unknown') AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS Count, MONTH(ProcessedDate) As MonthID
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
where	coalesce(ProcessedDate, StatusDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and 
		ClaimLawsuitStatusGroupID = 2
		and  IsPrimaryStatus= 1
		
group by DATENAME("mm", ProcessedDate) + ' ' + CONVERT(nvarchar, YEAR(ProcessedDate)), MONTH(ProcessedDate), year(ProcessedDate)
order by year(ProcessedDate), MONTH(ProcessedDate)
 
END
ELSE IF(@InsurerRole = 1) --Insurers
BEGIN
 
select coalesce(DATENAME("mm", ProcessedDate) + ' ' + CONVERT(nvarchar, YEAR(ProcessedDate)), 'Unknown') AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS Count, MONTH(ProcessedDate) As MonthID
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
where	coalesce(ProcessedDate, StatusDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID = 2
		and  IsPrimaryStatus=1
		
group by DATENAME("mm", ProcessedDate) + ' ' + CONVERT(nvarchar, YEAR(ProcessedDate)), MONTH(ProcessedDate), year(ProcessedDate)
order by year(ProcessedDate), MONTH(ProcessedDate)
 
END
ELSE
BEGIN
 
select coalesce(DATENAME("mm", ProcessedDate) + ' ' + CONVERT(nvarchar, YEAR(ProcessedDate)), 'Unknown') AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS Count, MONTH(ProcessedDate) As MonthID
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
where	coalesce(ProcessedDate, StatusDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID = 2
		and  IsPrimaryStatus=1
		and (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
		
group by DATENAME("mm", ProcessedDate) + ' ' + CONVERT(nvarchar, YEAR(ProcessedDate)), MONTH(ProcessedDate), year(ProcessedDate)
order by year(ProcessedDate), MONTH(ProcessedDate)
 
END
 
 
END
 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByMonth] TO [power_user]
GO
