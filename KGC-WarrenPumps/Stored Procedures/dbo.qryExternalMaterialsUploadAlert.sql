
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ellie Tong
-- change date: 03/05/2015
-- Description: Change cpi.ReferenceID to c.ReferenceID
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalMaterialsUploadAlert]
AS
BEGIN

/*Query run daily to send a separate alert email to the designated Invoice Contacts for each State when
an invoice has been accepted or rejected. - BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @Header nvarchar (2000)

Declare @Today4PM datetime
Declare @Yesterday4PM datetime
SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')
SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')

SET @Subject = 'Warren Claims: Materials Upload ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    
SET @Header = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>
<H4>The following materials have been uploaded in the last 24 hours:</H4>'

--Create a temp table for any documents uploaded in last 24 hours, organized by claimant
CREATE TABLE #Documents (DocumentID bigint, DocumentTitle nvarchar (MAX), DateCreated datetime,
ClaimantDocumentType nvarchar (MAX), ClaimantName nvarchar (MAX), ClaimantPersonalInfoID bigint, CreatedBy nvarchar (MAX), State nvarchar (MAX), StateID int, REFERENCEID nvarchar (max))
INSERT INTO #Documents
	SELECT
		d.DocumentID,
		DocumentTitle,
		DateCreated,
		ClaimantDocumentType,
		CPI.LastName + ', ' + CASE
			WHEN FirstName IS NULL THEN '' ELSE FirstName + ' '
		END + CASE
			WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' '
		END AS ClaimantName,
		cpi.ClaimantPersonalInfoID,
		CreatedBy,
		FullNameState,
		l.StateID,
		COALESCE(c.REFERENCEID, 'N/A')
	FROM tblDocuments d
	LEFT JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	LEFT JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
	LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
	LEFT JOIN tblState s ON s.StateID = l.StateID
	WHERE DateCreated BETWEEN GETDATE() - 1 AND GETDATE()
	AND (d.ClaimantDocumentTypeID <> 8)
	GROUP BY	State,
				d.DocumentID,
				DocumentTitle,
				DateCreated,
				ClaimantDocumentType,
				CPI.LastName + ', ' + CASE
					WHEN FirstName IS NULL THEN '' ELSE FirstName + ' '
				END + CASE
					WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' '
				END,
				cpi.ClaimantPersonalInfoID,
				CreatedBy,
				FullNameState,
				l.StateID,
				c.REFERENCEID
--AND RIGHT(CreatedBy, 11) <> 'kcicllc.com'
--ORDER BY [State]



--print CSS styling and beginning of table (headers, etc)
DECLARE @BodyMessage nvarchar (max)
SET @BodyMessage = @Header
SET @BodyMessage = @BodyMessage

DECLARE @DocumentID bigint, @DocumentTitle nvarchar (MAX), @ClaimantDocumentType nvarchar (MAX), @ClaimantName nvarchar (MAX),
@ClaimantPersonalInfoID bigint, @CreatedBy nvarchar (MAX), @State nvarchar (MAX), @StateID bigint, @LastStateID int, @REFERENCEID nvarchar (max)

--initialize lastCPIID
SET @LastStateID = 0

--Declare a cursor to loop through claimant documents
DECLARE doc_Curs CURSOR FOR SELECT
	DocumentID,
	DocumentTitle,
	ClaimantDocumentType,
	COALESCE(ClaimantName, 'ERROR: No Claimant'),
	COALESCE(ClaimantPersonalInfoID, 99999999),
	CreatedBy,
	State,
	COALESCE(StateID, 0) AS StateID,
	REFERENCEID
FROM #Documents
ORDER BY State

OPEN doc_Curs
FETCH NEXT FROM doc_Curs INTO @DocumentID, @DocumentTitle, @ClaimantDocumentType, @ClaimantName, @ClaimantPersonalInfoID, @CreatedBy, @State, @StateID, @REFERENCEID

While @@FETCH_STATUS = 0

BEGIN
IF (@StateID < > @LastStateID)
BEGIN
IF (@LastStateID < > 0)
SET @BodyMessage = @BodyMessage + N'</table>' + N'<br>'
--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid rgb(0,121,193); color: #ffffff; width: 800px;">' +
N'<tr><th colspan="4" style="font-family: Goudy Old Style; font-size: 12pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">' + @State + '</th></tr><tr><td style="border: 1px solid rgb(0,121,193); padding: .3em; color: #F6ECF0; background: rgb(0,121,193); v-align: center; text-align: middle;" class="subHeader"><b>Title</b></td><td style="border: 1px solid rgb(0,121,193); padding: .3em; color: #F6ECF0; background: rgb(0,121,193); v-align: center; text-align: middle;" class="subHeader"class="subHeader"><b>Type</b></td><td style="border: 1px solid rgb(0,121,193); padding: .3em; color: #F6ECF0; background: rgb(0,121,193); v-align: center; text-align: middle;" class="subHeader" class="subHeader"><b>Claimant(Reference ID)</b></td><td style="border: 1px solid rgb(0,121,193); padding: .3em; color: #F6ECF0; background: rgb(0,121,193); v-align: center; text-align: middle;" class="subHeader" class="subHeader"><b>Created By</b></td></tr>'
--SET @BodyMessage = @BodyMessage + '<tr><td colspan="2"><b><a href="https://admin.kcicllc.com/cp/wp/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(MAX), @ClaimantPersonalInfoID) + '">'  + @ClaimantName + '</a></b></td></tr>' + 
--			
END
--start the table row
SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: left; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000"><a href="https://www.kcicextranet.com/clients/wp/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(MAX), @DocumentID) + '">' + @DocumentTitle + '</a></td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: left; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">' + @ClaimantDocumentType + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: left; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">' + @ClaimantName + '(' + CONVERT(nvarchar(MAX), @REFERENCEID) + ')' + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: left; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">' + @CreatedBy + '</td></tr>'

SET @LastStateID = @StateID



FETCH NEXT FROM doc_Curs INTO @DocumentID, @DocumentTitle, @ClaimantDocumentType, @ClaimantName, @ClaimantPersonalInfoID, @CreatedBy, @State, @StateID, @REFERENCEID
END
CLOSE doc_Curs
DEALLOCATE doc_Curs

--final table closing tag
SET @BodyMessage = @BodyMessage + N'</table>' + N'<br>'



SET @BodyMessage = @BodyMessage + N'<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--only send if there are documents...
IF (SELECT
	COUNT(*)
FROM #Documents) > 0
BEGIN
--Execute stored procedure to send email
EXEC msdb.dbo.sp_send_dbmail --The Demo ClientProfile sends from Demo Client@kcic.com
@profile_name = 'Claims',
@recipients = 'DHunter@piercedavis.com; AConley@piercedavis.com; EBertrand@piercedavis.com;RWeller@piercedavis.com; MMalloy@piercedavis.com',
@blind_copy_recipients = 'Warren@kcic.com;potterm@kcic.com',
--@recipients = 'tonge@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
--print @BodyMessage
END

END


GO

GRANT VIEW DEFINITION ON  [dbo].[qryExternalMaterialsUploadAlert] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalMaterialsUploadAlert] TO [base_user]
GRANT ALTER ON  [dbo].[qryExternalMaterialsUploadAlert] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalMaterialsUploadAlert] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalMaterialsUploadAlert] TO [power_user]
GO
