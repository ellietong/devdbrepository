SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_INVOICES_DefenseInvoiceLookup]
AS 
    SET NOCOUNT ON

    SELECT  statusid ,
            status
    FROM    tblDefenseInvoiceStatus
    WHERE   StatusID <> 5
    UNION
	--SELECT null as StatusID, '<< All Status >>' As status
	--UNION
    SELECT  NULL ,
            '<<Any Status>>'
    ORDER BY Status
    RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_DefenseInvoiceLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_DefenseInvoiceLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_DefenseInvoiceLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_DefenseInvoiceLookup] TO [power_user]
GO
