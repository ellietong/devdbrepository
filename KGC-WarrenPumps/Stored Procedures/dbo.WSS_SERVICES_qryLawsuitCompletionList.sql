SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryLawsuitCompletionList]
    (
      @PrefixText NVARCHAR(100) ,
      @Emailaddress NVARCHAR(100)
    )
AS 
    BEGIN

--DECLARE @emailaddress nvarchar(250) = 'crookc@kcicllc.com'
--DECLARE @PrefixText nvarchar(100) = '10 L 1173'

        SET NOCOUNT ON

        DECLARE @FirmAddressID INT
        SET @FirmAddressID = ( SELECT   FirmAddressID
                               FROM     vtblPerson
                               WHERE    Email = @emailaddress
                             )

        DECLARE @NoFilters BIT
        SET @NoFilters = ( SELECT   NoFilters
                           FROM     vtblPerson
                           WHERE    Email = @emailaddress
                         )

        IF ( RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'
             OR @NoFilters = 1
           ) 
            BEGIN
                SELECT DISTINCT
                        l.LawsuitID ,
                        DocketNumber + ' (' + CaseCaption + ')' AS CaseCaption
                FROM    tblClaim c
                        INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
                        INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
                        LEFT JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimID
                        LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
                        INNER JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                WHERE   (DocketNumber LIKE COALESCE('%' + @PrefixText, '')
                        + '%') AND clst.ClaimLawsuitStatusID IN (select ClaimLawsuitStatusID from tblclaimlawsuitstatus where ClaimLawsuitStatusID NOT IN(15,16,23,29,30,35,41,42))
                        AND IsPrimaryStatus = 1
                ORDER BY DocketNumber + ' (' + CaseCaption + ')' 
            END
        ELSE 
            BEGIN
                SELECT DISTINCT
                        l.LawsuitID ,
                        DocketNumber + ' (' + CaseCaption + ')' AS CaseCaption
                FROM    tblClaim c
                        LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
                        LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
                        LEFT JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimID
                        LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
                        INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID
                                                              AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                WHERE   (DocketNumber LIKE COALESCE('%' + @PrefixText, '')
                        + '%') AND clst.ClaimLawsuitStatusID NOT IN(15,16,23,29,30,35,41,42)
                        AND dj.DefenseCounselAddressID = @FirmAddressID AND IsPrimaryStatus = 1
                ORDER BY DocketNumber + ' (' + CaseCaption + ')'

 
            END
    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [power_user]
GO
