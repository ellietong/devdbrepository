SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantRelease] (@ClaimantSettlementID int)
as

Select ClaimantReleaseID, 
	ClaimantSettlementID, 
	ReleaseSignedDate, 
	ReleaseReceivedDate, 
	ReleaseTypeID
From tblClaimantRelease
Where ClaimantSettlementID = @CLaimantSettlementID
Order By ReleaseSignedDate

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantRelease] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantRelease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantRelease] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantRelease] TO [power_user]
GO
