SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryIsClaimantPaid]
    (
      @ClaimantPersonalInfoID INT
    )
AS 
    SET NOCOUNT ON

SELECT COUNT(DISTINCT SetPymtID)
FROM tblSettlePaymentDocumentation spd
	LEFT JOIN tblClaimLawsuit cl ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND COALESCE(MedicareReported, 0) = 1
GROUP BY ClaimantPersonalInfoID	
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [power_user]
GO
