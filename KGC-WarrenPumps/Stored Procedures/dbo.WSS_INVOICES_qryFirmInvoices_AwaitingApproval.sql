SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_INVOICES_qryFirmInvoices_AwaitingApproval] (@emailaddress nvarchar(1000) = NULL)
AS
SET NOCOUNT ON;
--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'hagep@kcicllc.com'

DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

DECLARE @WhereString nvarchar(max)
SET @WhereString = 'WHERE di.StatusID IN (1)'
--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF (@DefenseCounselID IN (269)) OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com')
	SET @JoinString = ' '
ELSE IF (@DefenseCounselID IN (374))
BEGIN
	SET @JoinString = ' '
	--SET @WhereString = @WhereString + ' AND IsRecommended <> 1'
END	
--ELSE IF (@DefenseCounselID IN (63))
--BEGIN
--	SET @JoinString = ' '
--	SET @WhereString = @WhereString + ' AND di.FirmID IN (340)'
--END
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND di.FirmID = ' + CONVERT(nvarchar(20),@DefenseCounselID)
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
    'SELECT DISTINCT
		di.FirmID,
		COALESCE(f.DefenseCounsel, ''N/A'') as firm,		
		di.Period,
		di.InvoiceNumber,
		coalesce(fees,0) as fees,
		coalesce(costs,0) as costs,
		coalesce(adjustments,0) as adjustments,
		 COALESCE (SUM(Fees), 0) + COALESCE (SUM(Costs), 0) 
                      - COALESCE (SUM(Adjustments), 0) AS InvoiceTotal,
		di.DatePayable,
		dis.[Status],
		COALESCE(ex.VendorName, ''N/A'') as VendorFirm,
		di.SubmitDate,
		di.InvoiceNotes,
		CASE WHEN IsRecommended = 1 THEN ''Yes'' ELSE '''' END AS IsRecommended,
		IsRecommended AS IsRecommendedID,
		IsApproved,
		di.DefenseInvoiceID
	FROM tblDefenseInvoice di	
		LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID 
		Left JOIN tblDefInvClaimantDetail dcd ON di.DefenseInvoiceID = dcd.DefenseInvoiceID
		LEFT JOIN tblDefenseCounsel dc		  ON di.FirmID = dc.DefenseCounselID
		LEFT JOIN tbldefensecounsel f on f.defensecounselid = di.firmid
		LEFT JOIN tblVendor ex on ex.VendorID = di.VendorfirmID
		LEFT JOIN tblPaymentDefense pd		  ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN tblCheck c				  ON pd.CheckID = c.CheckID 
' +
    @WhereString + '
    
    GROUP BY
		di.FirmID,
		f.DefenseCounsel ,
		di.Period,
		di.InvoiceNumber,
		fees,
		costs,
		adjustments,
		di.DatePayable,
		dis.[Status],
		ex.VendorName ,
		di.SubmitDate,
		di.InvoiceNotes,
		CASE WHEN IsRecommended = 1 THEN ''Yes'' ELSE '''' END ,
		IsRecommended ,
		IsApproved,
		di.DefenseInvoiceID
	ORDER BY IsRecommended desc, SubmitDate
	OPTION (RECOMPILE)'

--Print @DefenseCounselID

--PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryFirmInvoices_AwaitingApproval] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryFirmInvoices_AwaitingApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryFirmInvoices_AwaitingApproval] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryFirmInvoices_AwaitingApproval] TO [power_user]
GO
