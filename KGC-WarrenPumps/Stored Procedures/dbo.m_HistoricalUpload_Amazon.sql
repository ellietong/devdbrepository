SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[m_HistoricalUpload_Amazon]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT d.DocumentID, DocumentURL, DocumentTitle, d.ClaimantDocumentTypeID, REPLACE(REPLACE(ClaimantDocumentType, '\', ' '), '/', ' ') AS ClaimantDocumentType,--,AmazonKey,
	'' AS DefenseCounsel, '' as DefenseCounselID
	FROM tblDocuments d
	--INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
	where  AmazonKey IS NULL --(DocumentURL like 'J:\%secondary%' OR DocumentURL like 'J:\%merged%') AND
END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_HistoricalUpload_Amazon] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_HistoricalUpload_Amazon] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_HistoricalUpload_Amazon] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_HistoricalUpload_Amazon] TO [power_user]
GO
