SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryTrialDateNote]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ActiveTrialDateNoteID, ActiveTrialDateNote
	FROM tblActiveTrialDateNotes
	UNION
	SELECT NULL, ''
	ORDER BY ActiveTrialDateNote
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryTrialDateNote] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryTrialDateNote] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryTrialDateNote] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryTrialDateNote] TO [power_user]
GO
