SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROC [dbo].[WSS_DOCUMENT_PROFILE_qryDocuments]
    (
      @DocID INT ,
      @emailaddress NVARCHAR(250)
    )
AS --DECLARE @DocID int
--SET @DocID = 52257374
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'hagep@kcicllc.com'
 
 
--GET LIST OF CLAIMANTS FOR DOCUMENT
 
    DECLARE @FirmID INT
    SET @FirmID = ( SELECT  FirmID
                    FROM    vtblPerson
                    WHERE   email = @emailaddress
                  )
 
    DECLARE @Claimants NVARCHAR(MAX)
    SET @Claimants = '' 
 
    DECLARE @SettleDocs NVARCHAR(MAX)
    SET @SettleDocs = '' 
 
    DECLARE @addClaimant NVARCHAR(MAX)
    DECLARE @addClaimantID INT
 
 
    DECLARE @DocCat NVARCHAR(MAX)
    DECLARE @PageNum NVARCHAR(MAX)
 
    DECLARE @FirmAddressID INT
    SET @FirmAddressID = ( SELECT   FirmAddressID
                           FROM     vtblPerson
                           WHERE    Email = @emailaddress
                         )
 
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' 1 = 1'
    IF @DocID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND d.DocID = '
                + CONVERT(NVARCHAR(20), @DocID)
        END	
 
    IF ( RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'
         OR @FirmAddressID IN ( SELECT  DefenseCounselAddressID
                                FROM    tblDefenseCounselAddress
                                WHERE   DefenseCounselID IN (433, 434, 511, 512, 513, 514, 515, 285, 249, 516, 531,534))
       ) 
        BEGIN
            DECLARE Cur_Claimants CURSOR Local Fast_Forward
            FOR
                SELECT  COALESCE(mcd.Claimantpersonalinfoid, '') ,
                        COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '')
                        + ' ('
                        + COALESCE(CONVERT(NVARCHAR(10), mcd.ClaimantPersonalInfoID),
                                   '') + ')'
                FROM    tblMultiClaimantDocument mcd
                        LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
                WHERE   mcd.DocumentID = @DocID
                GROUP BY mcd.Claimantpersonalinfoid ,
                        LastName + ', ' + FirstName + ' ('
                        + CONVERT(NVARCHAR(10), mcd.ClaimantPersonalInfoID)
                        + ')' ,
                        LastName ,
                        FirstName
 
        END
 
    ELSE 
        BEGIN
            DECLARE Cur_Claimants CURSOR Local Fast_Forward
            FOR
                SELECT  mcd.Claimantpersonalinfoid ,
                        LastName + ', ' + FirstName + ' ('
                        + CONVERT(NVARCHAR(10), mcd.ClaimantPersonalInfoID)
                        + ')'
                FROM    tblMultiClaimantDocument mcd
                        LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
                        LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.claimantpersonalInfoID
                        LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
                        LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
                        INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID
                                                              AND l.JurisdictionID = dj.JurisdictionID
                                                              AND c.MatterID = dj.matterID
                        INNER JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
                WHERE   mcd.DocumentID = @DocID
                        AND dj.DefenseCounselAddressID = @FirmAddressID
                GROUP BY mcd.Claimantpersonalinfoid ,
                        LastName + ', ' + FirstName + ' ('
                        + CONVERT(NVARCHAR(10), mcd.ClaimantPersonalInfoID)
                        + ')' ,
                        LastName ,
                        FirstName
 
            SET @WhereString = @WhereString + '(l.DefenseCounselAddressID ='
                + @FirmAddressID + 'OR d.FirmID =' + @FirmID + ')'
 
        END
 
--------------------------------------------------------------------
 
--Open Docket Cursor
    OPEN Cur_Claimants
 
--Fetch Data
    FETCH NEXT FROM Cur_Claimants INTO @addClaimantID, @addClaimant
 
    WHILE @@Fetch_Status = 0 
        BEGIN
--assemble the string
            IF LEN(@Claimants) = 0 
                SET @Claimants = '<a target="_blank" href="/clients/wp/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='
                    + COALESCE(CONVERT(NVARCHAR(10), @addClaimantID), '')
                    + '">' + COALESCE(@addClaimant, '') + '</a>'
            ELSE 
                SET @Claimants = @Claimants + '<br />'
                    + '<a target="_blank" href="/clients/wp/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='
                    + COALESCE(CONVERT(NVARCHAR(10), @addClaimantID), '')
                    + '">' + COALESCE(@addClaimant, '') + '</a>'
            PRINT @Claimants
 
 
            FETCH NEXT FROM Cur_Claimants INTO @addClaimantID, @addClaimant
 
        END
 
    CLOSE Cur_Claimants 
    DEALLOCATE Cur_Claimants
 
 
-----------------------------------------------------------------
 
    DECLARE Cur_SettleDoc CURSOR Local Fast_Forward
    FOR
        SELECT  DocCategory ,
                PageNums
        FROM    tblSettleDocs sd
                LEFT JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
        WHERE   DocumentID = @DocID
 
--Open Docket Cursor
    OPEN Cur_SettleDoc
 
--Fetch Data
    FETCH NEXT FROM Cur_SettleDoc 
	  INTO @DocCat, @PageNum
 
    WHILE @@Fetch_Status = 0 
        BEGIN
      --assemble the string
            IF LEN(@SettleDocs) = 0 
                SET @SettleDocs = COALESCE(@DocCat, '')
                    + '&nbsp;&nbsp;pg(s): ' + COALESCE(@PageNum, '')
            ELSE 
                SET @SettleDocs = @SettleDocs + '<br />' + COALESCE(@DocCat,
                                                              '')
                    + '&nbsp;&nbsp;pg(s): ' + COALESCE(@PageNum, '')
            PRINT @SettleDocs
 
 
            FETCH NEXT FROM Cur_SettleDoc 
      INTO @DocCat, @PageNum
      
        END
 
 
    CLOSE Cur_SettleDoc 
    DEALLOCATE Cur_SettleDoc
 
------------------------------------------------------------------------------------------------------------------
    DECLARE @QueryString NVARCHAR(MAX)
    IF @FirmAddressID IN (433, 434, 511, 512, 513, 514, 515, 285, 249, 516, 531,534)
    BEGIN
    
    SET @QueryString = 'SELECT DISTINCT
cast(d.DocumentID as varchar(50)) as DocumentId,
dt.ClaimantDocumentType, 
dg.DocumentGroup, 
d.Comments,
d.DocumentURL,
d.DocumentTitle,
d.SPDocId,
d.CreatedBy,
d.DateCreated,
d.ModifiedBy,
d.DateModified,
d.Accepted,
d.AcceptedBy,
d.DateAccepted,
d.ApprovedBy,
d.DateApproved,
f.Firm,
'''' AS Claimants,
CASE WHEN LEN(@SettleDocs)=0 THEN ''N/A'' ELSE COALESCE(@SettleDocs, ''N/A'') END AS SettleDocs,
d.DocumentID,
d.DismissalDate,
d.QCDate AS DismissalProcessDate,
ClaimLawsuitStatus AS DismissalType,
DocumentSource,
d.DocumentSourceID,
NULL as AutoAccept,
di.InvoiceNumber,
di.Period,
di.DatePayable,
di.fees as FeeAmount,
di.costs as DisbursementAmount,
di.adjustments as Adjustment,
di.InvoiceNotes as AdjustmentNotes,
dis.Status, 
COALESCE(ef.VendorFirm, f.Firm) VendorFirm, 
di.SubmitDate,
'''' as PrivateComments,
Coalesce(IsFullyPaid,0) as IsFullyPaid
 
 
FROM tblDocuments d
LEFT OUTER JOIN tblClaimantDocumentType dt		ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
LEFT OUTER JOIN tblDocumentGroup dg				ON dt.DocumentGroupID = dg.DocumentGroupID
LEFT OUTER JOIN tblMultiClaimantDocument mcd	ON d.DocumentID = mcd.DocumentID
LEFT OUTER JOIN tblDefInvClaimantDetail dicd	ON d.DocumentID = dicd.DocumentID
LEFT OUTER JOIN tblClaimantPersonalInfo cpi		ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID OR dicd.ClaimantPersonalInfOID = cpi.ClaimantpersonalInfoID
LEFT OUTER JOIN tblClaim c						ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
LEFT OUTER JOIN tblClaimLawsuit cl				ON c.ClaimID = cl.ClaimID
LEFT OUTER JOIN tblLawsuit l					ON cl.lawsuitid = l.lawsuitid
LEFT OUTER JOIN tblJurisdiction j				ON l.jurisdictionid = j.jurisdictionid
LEFT OUTER JOIN tblState s						ON l.stateid = s.stateid
LEFT OUTER JOIN tblDefenseCounselAddress dca	ON l.defensecounseladdressid = dca.defensecounseladdressid 
LEFT OUTER JOIN vtblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
LEFT OUTER JOIN tblDefenseInvoiceStatus dis		ON di.StatusID = dis.StatusID 
LEFT OUTER JOIN tblPaymentDefense pd			ON di.DefenseInvoiceID=pd.DefenseInvoiceID
LEFT OUTER JOIN m_tblVendorFirm ef				ON d.VendorFirmID = ef.VendorFirmID 
LEFT OUTER JOIN vtblFirm f						ON d.FirmID = f.FirmID
LEFT OUTER JOIN tblClaimantDocketNUmbers cdn	ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
LEFT OUTER JOIN tblPlaintiffCounselAddress pca	ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
LEFT OUTER JOIN tblDocumentSource ds			ON d.DocumentSourceID = ds.DocumentSourceID
LEFT OUTER JOIN tblClaimlawsuitstatus cls		ON d.claimlawsuitstatusID = cls.claimlawsuitstatusID'
            + @WhereString + '
OPTION (RECOMPILE)'
 
    END
    ELSE
    BEGIN
    SET @QueryString = 'SELECT DISTINCT
cast(d.DocumentID as varchar(50)) as DocumentId,
dt.ClaimantDocumentType, 
dg.DocumentGroup, 
d.Comments,
d.DocumentURL,
d.DocumentTitle,
d.SPDocId,
d.CreatedBy,
d.DateCreated,
d.ModifiedBy,
d.DateModified,
d.Accepted,
d.AcceptedBy,
d.DateAccepted,
d.ApprovedBy,
d.DateApproved,
f.Firm,
COALESCE(@Claimants, '''') AS Claimants,
CASE WHEN LEN(@SettleDocs)=0 THEN ''N/A'' ELSE COALESCE(@SettleDocs, ''N/A'') END AS SettleDocs,
d.DocumentID,
d.DismissalDate,
d.QCDate AS DismissalProcessDate,
ClaimLawsuitStatus AS DismissalType,
DocumentSource,
d.DocumentSourceID,
NULL as AutoAccept,
di.InvoiceNumber,
di.Period,
di.DatePayable,
di.fees as FeeAmount,
di.costs as DisbursementAmount,
di.adjustments as Adjustment,
di.InvoiceNotes as AdjustmentNotes,
dis.Status, 
COALESCE(ef.VendorFirm, f.Firm) VendorFirm, 
di.SubmitDate,
'''' as PrivateComments,
Coalesce(IsFullyPaid,0) as IsFullyPaid
 
 
FROM tblDocuments d
LEFT OUTER JOIN tblClaimantDocumentType dt		ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
LEFT OUTER JOIN tblDocumentGroup dg				ON dt.DocumentGroupID = dg.DocumentGroupID
LEFT OUTER JOIN tblMultiClaimantDocument mcd	ON d.DocumentID = mcd.DocumentID
LEFT OUTER JOIN tblDefInvClaimantDetail dicd	ON d.DocumentID = dicd.DocumentID
LEFT OUTER JOIN tblClaimantPersonalInfo cpi		ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID OR dicd.ClaimantPersonalInfOID = cpi.ClaimantpersonalInfoID
LEFT OUTER JOIN tblClaim c						ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
LEFT OUTER JOIN tblClaimLawsuit cl				ON c.ClaimID = cl.ClaimID
LEFT OUTER JOIN tblLawsuit l					ON cl.lawsuitid = l.lawsuitid
LEFT OUTER JOIN tblJurisdiction j				ON l.jurisdictionid = j.jurisdictionid
LEFT OUTER JOIN tblState s						ON l.stateid = s.stateid
LEFT OUTER JOIN tblDefenseCounselAddress dca	ON l.defensecounseladdressid = dca.defensecounseladdressid 
LEFT OUTER JOIN vtblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
LEFT OUTER JOIN tblDefenseInvoiceStatus dis		ON di.StatusID = dis.StatusID 
LEFT OUTER JOIN tblPaymentDefense pd			ON di.DefenseInvoiceID=pd.DefenseInvoiceID
LEFT OUTER JOIN m_tblVendorFirm ef				ON d.VendorFirmID = ef.VendorFirmID 
LEFT OUTER JOIN vtblFirm f						ON d.FirmID = f.FirmID
LEFT OUTER JOIN tblClaimantDocketNUmbers cdn	ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
LEFT OUTER JOIN tblPlaintiffCounselAddress pca	ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
LEFT OUTER JOIN tblDocumentSource ds			ON d.DocumentSourceID = ds.DocumentSourceID
LEFT OUTER JOIN tblClaimlawsuitstatus cls		ON d.claimlawsuitstatusID = cls.claimlawsuitstatusID'
            + @WhereString + '
OPTION (RECOMPILE)'
 
    END
    PRINT @querystring
    RETURN
 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDocuments] TO [power_user]
GO
