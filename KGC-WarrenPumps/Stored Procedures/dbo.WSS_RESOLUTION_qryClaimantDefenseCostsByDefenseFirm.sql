SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] 
	-- Add the parameters for the stored procedure here
    (
      @ClaimantPersonalInfoID INT
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 41635
        DECLARE @InvoiceTotal TABLE
            (
              ClaimantPersonalInfoID INT ,
              VendorFirmID INT ,
              DefenseCounselAddressID INT ,
              DefenseTotal MONEY
            )
        INSERT  INTO @InvoiceTotal

----Approved Expert and General Invoices with a ClaimantID link
                SELECT  dicd.ClaimantPersonalInfoID ,
                        di.VendorFirmID ,
                        Di.DefenseCounselAddressID ,
                        SUM(COALESCE(Costs, 0) + COALESCE(Fees, 0)
                            + COALESCE(Adjustments, 0)) AS DefenseTotal
                FROM    tblDefenseInvoice DI
                        LEFT JOIN tblDefInvClaimantDetail dicd ON di.DefenseInvoiceID = dicd.DefenseInvoiceID
                WHERE   DICD.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
                        AND DI.StatusID IN ( 1, 2,3,4 )
                GROUP BY dicd.ClaimantPersonalInfoID ,
                        di.VendorFirmID ,
                        Di.DefenseCounselAddressID

        SELECT  ClaimantPersonalInfoID ,
                V.DefenseCounsel ,
                COALESCE(i.DefenseCounselAddressID, 0) AS DefenseCounselID ,
                i.VendorFirmID ,
                SUM(DefenseTotal) AS DefenseTotal
        FROM    @InvoiceTotal i
                LEFT JOIN tblDefenseCounselAddress v ON v.DefenseCounselAddressID = i.DefenseCounselAddressID
                --LEFT JOIN tblDefenseCounsel vi ON v.DefenseCounsel = i.DefenseCounselAddressID
        GROUP BY ClaimantPersonalInfoID ,
                DefenseCounsel ,
                i.DefenseCounselAddressID ,
                i.VendorFirmID
        HAVING  SUM(DefenseTotal) <> 0

    END
    



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GO
