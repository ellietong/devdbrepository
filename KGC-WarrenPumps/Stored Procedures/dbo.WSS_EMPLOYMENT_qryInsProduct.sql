SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryInsProduct]
    (
      @ProductID BIGINT ,
      @ClaimantEmploymentID BIGINT ,
      @StartDate DATETIME ,
      @EndDate DATETIME ,
      @Emailaddress NVARCHAR(100) ,
      @Notes NVARCHAR(MAX)
    )
AS 
    SET NOCOUNT ON

    INSERT  INTO tblClaimantProduct
            ( ProductID ,
              ClaimantEmploymentID ,
              StartDate ,
              EndDate ,
              SPUsername,
              Notes
            )
    VALUES  ( @ProductID ,
              @ClaimantEmploymentID ,
              @StartDate ,
              @EndDate ,
              @Emailaddress,
              @Notes
            )

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [power_user]
GO
