SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryClaimLawsuits]
    (
      @StateID INT ,
      @JurisdictionID INT ,
      @ClaimLawsuitStatusID INT
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @cmd NVARCHAR(MAX)
        SET @cmd = '
	SELECT  CL.ClaimLawsuitID, 
	CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '''' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE + '' '' + MiddleName END AS CombinedName, 
	CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SocialSecurityNumber,
	CaseCaption, 
	s.State, 
	ClaimLawsuitStatus, 
	Jurisdiction, 
	cl.ActiveTrialDate, 
	DocketNumber, 
	TrialGroup, 
	c.ClaimantPersonalInfoID
	
	FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	INNER JOIN tblState s ON l.StateID = s.StateID
	INNER JOIN tblJurisdiction j ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblTrialGroup tg ON cl.TrialGroupID = tg.TrialGroupID
	WHERE	COALESCE(IsPrimaryForClaim, 0) = 1 AND COALESCE(IsCurrentClaim, 0) = 1 AND IsPrimaryStatus = 1' 
        IF ( @StateID IS NOT NULL ) 
            SET @cmd = @cmd + ' AND 	l.StateID = @StateID'
	
        IF ( @JurisdictionID IS NOT NULL ) 
            SET @cmd = @cmd + ' AND l.JurisdictionID = @JurisdictionID'
	
        IF ( @ClaimLawsuitStatusID IS NOT NULL ) 
            SET @cmd = @cmd
                + ' AND clst.ClaimLawsuitStatusID = @ClaimLawsuitStatusID'
	
        EXEC sp_executesql @cmd,
            N'@JurisdictionID int, @StateID int, @ClaimLawsuitStatusID int',
            @JurisdictionID = @JurisdictionID, @StateID = @StateID,
            @ClaimLawsuitStatusID = @ClaimLawsuitStatusID
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryClaimLawsuits] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryClaimLawsuits] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryClaimLawsuits] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryClaimLawsuits] TO [power_user]
GO
