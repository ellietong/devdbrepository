SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROC [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceFirms] (@emailaddress nvarchar(250))
AS
SET NOCOUNT ON
 
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'johnn@kcicllc.com'
 
DECLARE @UnpaidInvoice TABLE (InvoiceID int, IsFullyPaid bit)
INSERT INTO @UnpaidInvoice
	SELECT InvoiceID, 
		CASE WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 0 THEN 0 
			 WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 1 THEN 1
			 ELSE IsFullyPaid END AS IsFullyPaid 
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblPaymentDefense pd ON i.InvoiceID = pd.DefenseInvoiceID
	GROUP BY InvoiceID, CASE WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 0 THEN 0 WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 1 THEN 1 ELSE IsFullyPaid END
 
DECLARE @firmid int
SET @firmid = (SELECT CASE WHEN RIGHT(@emailaddress, 15) = 'smithmurphy.com' THEN '115' ELSE FirmID END 
			   FROM vtblperson 
			   WHERE Email = @emailaddress)
 
IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @firmid = 132) --Update this so it shows all for Vicki, too.
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel + ' (' + COALESCE(d.DefenseCounselCity, 'Unknown City') + ')'  AS DefenseCounsel
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d	ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m					ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s		ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u				ON i.InvoiceID = u.InvoiceID
	WHERE COALESCE(submitteddate, wpapprovaldate, period) >= (GETDATE()-365)
		OR u.IsFullyPaid = 0 
		OR i.NavPaidInvoice = 0 
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel + ' (' + COALESCE(d.DefenseCounselCity, 'Unknown City') + ')' 
	ORDER BY DefenseCounsel
END
ELSE IF(SELECT PersonID 
		FROM vtblPerson 
		WHERE Email = @emailaddress) IN (1405, 1406)
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel 
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d	ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m					ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s		ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u				ON i.InvoiceID = u.InvoiceID
	WHERE i.DefenseCounselAddressID = 0
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel
	ORDER BY DefenseCounsel
END
ELSE IF @firmid IN (100,117)
--Celba LLC and Steven Celba LLC invoices
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel 
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d	ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m					ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s		ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u				ON i.InvoiceID = u.InvoiceID
	WHERE i.DefenseCounselAddressID IN (SELECT DefenseCounselAddressID 
										FROM tblDefenseCounselAddress 
										WHERE DefenseCounselID IN (100,117))
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel
	ORDER BY DefenseCounsel
END
ELSE IF @firmid IN (115)
--Smith Murphy invoices --Smith Murphy users coded as part of Reed Smith Firm in order to view Reed Smith claimants, however should not see Reed Smith invoices
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel + ' (' + COALESCE(d.DefenseCounselCity, 'Unknown City') + ')'  AS DefenseCounsel
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d	ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m					ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s		ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u				ON i.InvoiceID = u.InvoiceID
	WHERE i.DefenseCounselAddressID IN (SELECT DefenseCounselAddressID 
										FROM tblDefenseCounselAddress 
										WHERE DefenseCounselID IN (115))
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel + ' (' + COALESCE(d.DefenseCounselCity, 'Unknown City') + ')' 
	ORDER BY DefenseCounsel
END
ELSE
BEGIN
	SELECT i.DefenseCounselAddressID, 
		DefenseCounsel + ' (' + COALESCE(d.DefenseCounselCity, 'Unknown City') + ')'  AS DefenseCounsel	
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblDefenseCounselAddress d	ON d.DefenseCounselAddressID = i.DefenseCounselAddressID
		LEFT JOIN tblMatter m					ON i.MatterID = m.MatterID
		LEFT JOIN m_tblLegalBillStatus s		ON i.StatusID = s.LegalBillStatusID
		LEFT JOIN @UnpaidInvoice u				ON i.InvoiceID = u.InvoiceID
	WHERE i.DefenseCounselAddressID IN (SELECT DISTINCT FirmAddressID 
										FROM vtblPerson 
										WHERE FirmID IN (SELECT FirmID 
														 FROM vtblPerson 
														 WHERE Email = @emailaddress))
	GROUP BY i.DefenseCounselAddressID, DefenseCounsel + ' (' + COALESCE(d.DefenseCounselCity, 'Unknown City') + ')' 
	ORDER BY DefenseCounsel
END
 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceFirms] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceFirms] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceFirms] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryLegalBillDefenseInvoiceFirms] TO [power_user]
GO
