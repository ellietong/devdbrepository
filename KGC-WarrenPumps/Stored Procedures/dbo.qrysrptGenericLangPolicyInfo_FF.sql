SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[qrysrptGenericLangPolicyInfo_FF]
(@GenericLanguageID int = null)
as
set nocount on



SELECT     ffp.PolicyID, ffc.CarrierName, ffp.PolicyNum, 
	convert(nvarchar,MIN(ffpd.PStartDate), 103) + ' - ' + CONVERT(nvarchar, MAX(ffpd.PEndDate), 103) AS PolicyDates
FROM   tblPolicy p
		INNER JOIN tblExactLanguage el on el.PolicyID = p.policyid
        INNER JOIN tblGenericLanguage gl on gl.GenericLanguageID=el.GenericLanguageID               
        Inner Join tblPolicyDates PD ON p.PolicyID = pd.PolicyID 
        inner join tblFollowsForm ff on FFPolicyDateID = PD.PolicyDateID
        inner join tblPolicyDates FFpd on FFpd.PolicyDateID = ff.PolicyDateID 
        inner join tblPolicy ffp on ffp.PolicyID = FFpd.policyid
        inner join tblCarrier ffc on ffc.CarrierID = ffp.CarrierID 
WHERE   el.GenericLanguageID = @GenericLanguageID
GROUP BY ffp.PolicyID, ffc.CarrierName, ffp.PolicyNum
ORDER BY MIN(ffpd.pstartdate)










GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptGenericLangPolicyInfo_FF] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptGenericLangPolicyInfo_FF] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptGenericLangPolicyInfo_FF] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptGenericLangPolicyInfo_FF] TO [power_user]
GO
