SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/****** Object:  Stored Procedure dbo.qryrptPolicyReportsSplitGroupedQCAll    Script Date: 9/5/2006 10:59:23 AM ******/
CREATE Procedure [dbo].[qryrptPolicyReportsSplitGroupedQCAll]
(@policyid int = null)
As
set nocount on

DECLARE @MAXpolicyid INT
-- Find max ID values
SELECT @MAXpolicyid = MAX(policyid) FROM tblPolicy

DECLARE @Policy TABLE (PolicyID int, PolicyNum nvarchar(100), CarrierID int, PolicyNotes nvarchar (255),
InitialDataEntry nvarchar(50), InitialDataEntryDate datetime, PolicyReview nvarchar(50), 
PolicyReviewDate datetime, NamedInsuredID int, InsuranceProgramID int)
INSERT INTO @Policy

SELECT PolicyID, PolicyNum, CarrierID, PolicyNotes, InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate,
NamedInsuredID, InsuranceProgramID
FROM vPolicy
GROUP BY PolicyID, PolicyNum, CarrierID, PolicyNotes, InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate,
NamedInsuredID, InsuranceProgramID

SELECT
tblInsuranceProgram.InsuranceProgram,
A.Policyid, tblCarrier.CarrierName,
PolicyNum, b.ProvisionLabelID, c.provisionvalue, d.provision,
tblPolicyDates.PStartDate AS Splitstart,
tblPolicyDates.PEndDate AS splitend,
tblPolicyDates.AnnualPeriod AS SplitAnnualPeriod,
tblNamedInsured.NamedInsured,
PolicyNotes,
InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate

FROM @Policy A
JOIN tblProvision B on A.policyID = b.policyID
JOIN tblprovisionvalue C on B.provisionvalueid = C.provisionvalueid
INNER JOIN tblProvisionlabel D ON b.ProvisionLabelID = d.ProvisionLabelID
INNER JOIN tblCarrier ON A.CarrierID = tblCarrier.CarrierID
left JOIN tblNamedInsured ON tblNamedInsured.NamedInsuredID = A.NamedInsuredID
INNER JOIN tblInsuranceProgram ON tblInsuranceProgram.InsuranceProgramID = A.InsuranceProgramID
INNER JOIN tblPolicyDates ON A.PolicyID = tblPolicyDates.PolicyID

WHERE a.policyid >= COALESCE(@policyid, 0) AND
a.policyid <= COALESCE(NULLIF(@policyid, 0), @MAXpolicyid)

GROUP BY  tblInsuranceProgram.InsuranceProgram, A.Policyid, tblCarrier.CarrierName,
PolicyNum, b.ProvisionLabelID, c.provisionvalue,d.provision,
tblPolicyDates.PStartDate,
tblPolicyDates.PEndDate,
tblPolicyDates.AnnualPeriod,
tblNamedInsured.NamedInsured,
InitialDataEntry, InitialDataEntryDate,
PolicyReview, PolicyReviewDate, PolicyNotes

ORDER BY tblInsuranceProgram.InsuranceProgram, tblPolicyDates.AnnualPeriod, tblPolicyDates.PStartDate
return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplitGroupedQCAll] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplitGroupedQCAll] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplitGroupedQCAll] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplitGroupedQCAll] TO [power_user]
GO
