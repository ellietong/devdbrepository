SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryBILLInsertExhaustion]
(@exhaustionDate datetime, @allocationID int, @description varchar(100), @exhaustionDateID int OUT, @exhaustionTypeID int)
AS
-- Procedure takes exhaustion date, allocation ID, description, and bill group ID inputs and populates tblExhaustion and tblExhaustionDate, which memorialize and track the exhaustion 
-- If a billing module is included, tblLedger (and the bill) is also updated using the stored procedure UpdateLedger
SET NOCOUNT ON
-- Insert data to tblExhaustionDate
INSERT INTO tblExhaustionDate (ExhaustionDate, ExhaustionTypeID, AllocationID, Description)
VALUES (@exhaustionDate, @exhaustionTypeID, @allocationID, @description)
-- Capture exhaustion date ID
SET @exhaustionDateID = @@IDENTITY
-- Insert data to tblExhaustion from tblAllocationResults

INSERT INTO tblExhaustion (ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation, DefenseAllocation, DefenseOutsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation)
SELECT @exhaustionDateID, OccurrenceID, PolicyDateID, AllocationIndemnity, AllocationDefense, AllocationDefenseOutsideLimits, SIRAllocatedIndemnity, SIRAllocatedDefense
FROM tblAllocationResults
WHERE AllocationID = @allocationID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryBILLInsertExhaustion] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryBILLInsertExhaustion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryBILLInsertExhaustion] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryBILLInsertExhaustion] TO [power_user]
GO
