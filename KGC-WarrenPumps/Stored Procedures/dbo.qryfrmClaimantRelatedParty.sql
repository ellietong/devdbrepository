SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantRelatedParty] (@ClaimantPersonalInfoID int)
as

Select *
From tblClaimantRelatedParty
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By LastName, FirstName, RelationshipID

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantRelatedParty] TO [power_user]
GO
