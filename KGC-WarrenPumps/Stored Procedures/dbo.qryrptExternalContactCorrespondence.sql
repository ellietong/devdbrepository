SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptExternalContactCorrespondence] 
(@CorrespondenceID int)
AS
SET NOCOUNT ON

SELECT  COALESCE (con.firstname, '') + ' ' + COALESCE (con.LastName, '') + ' ' + COALESCE (AddressLine1, '') + ' ' + COALESCE (AddressLine2, '') + 
			' ' + COALESCE (City, '') 
               + ', ' + COALESCE (State, '') AS ExContactMerge, ccc.CorrespondenceID, r.RecipientType
FROM     tblContact con INNER JOIN
		tblState s on s.StateID = con.StateID inner join
		tblCarrierContact cc on cc.ContactID=con.ContactID
		inner join tblCorrCarrierContact ccc on ccc.carriercontactid = cc.carriercontactid inner join
         tblRecipientType r ON ccc.RecipientTypeID = r.RecipientTypeID
WHERE @CorrespondenceID = ccc.CorrespondenceID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExternalContactCorrespondence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExternalContactCorrespondence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExternalContactCorrespondence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExternalContactCorrespondence] TO [power_user]
GO
