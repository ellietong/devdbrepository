SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_RELATED_PARTY_LossOfConsortium]
AS

	SELECT

		0 AS LossOfConsortiumID, 'NO' AS LossOfConsortium
	UNION
	SELECT
		1 AS LossOfConsortiumID,'YES' AS LossOfConsortium

	FROM tblClaimantRelatedParty crp

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_LossOfConsortium] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_LossOfConsortium] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_LossOfConsortium] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_LossOfConsortium] TO [power_user]
GO
