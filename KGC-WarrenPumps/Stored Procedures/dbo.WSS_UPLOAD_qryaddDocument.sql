SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryaddDocument] (@ClaimantDocumentTypeID		int,					--aka ClaimantDocumentTypeID
											  @CreatedBy				nvarchar(250),
											  @DateCreated				datetime,
											  @DocumentURL				nvarchar(4000),
											  @DocumentTitle			nvarchar(4000),
											  @Comments					nvarchar(4000) = NULL,	--comments are optional	
											  @DocumentSourceID			int,
    										  @MatterID					int = NULL,
    										  @DateApproved				datetime = NULL)

AS

DECLARE @MaxDocID int
SET @MaxDocID = (SELECT IDENT_CURRENT('tblDocuments')+IDENT_INCR('tblDocuments'))

--Accept Dismissals Uploaded by KCIC
IF (RIGHT(@CreatedBy, 11) = 'kcicllc.com') AND @ClaimantDocumentTypeID = 16
BEGIN
INSERT INTO tblDocuments (ClaimantDocumentTypeID,	
						  Comments,
						  DocumentURL,
						  DocumentTitle,
						  CreatedBy,
						  DateCreated,
						  ModifiedBy,
						  DateModified,
						  DocumentSourceID,
						  MatterID,
						  Accepted,
						  AcceptedBy,
						  DateAccepted)
	VALUES (@ClaimantDocumentTypeID,
			@Comments,
			REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
			@DocumentTitle,
			@CreatedBy,
			@DateCreated,	
			@CreatedBy,
			@DateCreated,	
			@DocumentSourceID,
			@MatterID,
			1,
			'warren@kcicllc.com',
			GETDATE())

SELECT @MaxDocID
END

ELSE IF (RIGHT(@CreatedBy, 11) = 'kcicllc.com') OR (@ClaimantDocumentTypeID IN (40, 41, 42))
BEGIN
INSERT INTO tblDocuments (ClaimantDocumentTypeID,	
						  Comments,
						  DocumentURL,
						  DocumentTitle,
						  CreatedBy,
						  DateCreated,
						  ModifiedBy,
						  DateModified,
						  DocumentSourceID,
						  MatterID)
	VALUES (@ClaimantDocumentTypeID,
			@Comments,
			REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
			@DocumentTitle,
			@CreatedBy,
			@DateCreated,
			@CreatedBy,
			@DateCreated,	
			@DocumentSourceID,
			@MatterID)

SELECT @MaxDocID
END

ELSE
BEGIN
INSERT INTO tblDocuments (ClaimantDocumentTypeID,	
						  Comments,
						  DocumentURL,
						  DocumentTitle,
						  CreatedBy,
						  DateCreated,
						  ModifiedBy,
						  DateModified,
						  DocumentSourceID,
						  MatterID,
						  Accepted,
						  AcceptedBy,
						  DateAccepted)
	VALUES (@ClaimantDocumentTypeID,
			@Comments,
			REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
			@DocumentTitle,
			@CreatedBy,
			@DateCreated,
			@CreatedBy,
			@DateCreated,	
			@DocumentSourceID,
			@MatterID,
			1,
			'warren@kcicllc.com',
			GETDATE())
SELECT @MaxDocID
END			

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryaddDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryaddDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryaddDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryaddDocument] TO [power_user]
GO
