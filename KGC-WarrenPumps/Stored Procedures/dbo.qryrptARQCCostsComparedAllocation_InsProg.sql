SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE   PROCEDURE [dbo].[qryrptARQCCostsComparedAllocation_InsProg]
(@allocationID int = null, @insuranceProgramID int = null)
AS
SET NOCOUNT ON
/* PART 1. COSTS CONSIDERED IN THE ALLOCATION */
-------------------------------------------------
DECLARE @CostsConsidered table (AllocationID int, InsuranceProgramID int, OccurrenceID bigint,
OccurrenceGroupID int, OccurrenceName nvarchar(256), 
Cost1 money, Cost2 money, Cost3 money, Cost4 money, Cost5 money, Cost6 money, Cost7 money, 
Cost8 money, Cost9 money, Cost10 money, Cost11 money, Cost12 money, TotalCostsConsidered money)
INSERT INTO @CostsConsidered (AllocationID, InsuranceProgramID, OccurrenceID, OccurrenceGroupID, OccurrenceName,
Cost1, Cost2, Cost3, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, TotalCostsConsidered)
SELECT @AllocationID, @InsuranceProgramID, OccurrenceID, OccurrenceGroupID, OccurrenceName, 
COALESCE(Cost1,0), COALESCE(Cost2,0), COALESCE(Cost3,0), COALESCE(Cost4,0), COALESCE(Cost5,0), COALESCE(Cost6,0), 
COALESCE(Cost7,0), COALESCE(Cost8,0), COALESCE(Cost9,0), COALESCE(Cost10,0), COALESCE(Cost11,0), COALESCE(Cost12,0), 
SUM(COALESCE(Cost1,0) + COALESCE(Cost2,0) + COALESCE(Cost3,0) + COALESCE(Cost4,0) + COALESCE(Cost5,0) + COALESCE(Cost6,0) + 
COALESCE(Cost7,0) +  COALESCE(Cost8,0) + COALESCE(Cost9,0) + COALESCE(Cost10,0) + COALESCE(Cost11,0) + COALESCE(Cost12,0))
FROM tblOccurrence o INNER JOIN tblCorpHistInsProgram chip ON o.CorporateHistoryID = chip.CorporateHistoryID 
WHERE OccurrenceID NOT IN
	(SELECT aoe.OccurrenceID 
	FROM tblAllocation a 
		INNER JOIN tblAllocationOccurrenceExclude aoe ON a.AllocationID = aoe.AllocationID 
	WHERE a.AllocationID = @allocationID
	) AND 
OccurrenceTypeID IN
	(SELECT aote.OccurrenceTypeID 
	FROM tblAllocation a INNER JOIN tblAllocationOccurrenceTypeInclude aote ON a.AllocationID = aote.AllocationID 
	WHERE a.AllocationID = @allocationID
	) AND 
OccurrenceGroupID IN
	(SELECT aoge.OccurrenceGroupID
	FROM tblAllocation a INNER JOIN tblAllocationOccurrenceGroupInclude aoge ON a.AllocationID = aoge.AllocationID
	WHERE a.AllocationID = @allocationID) AND
InsuranceProgramID = @insuranceProgramID and OccurrenceID is not null
GROUP BY OccurrenceID, OccurrenceGroupID, OccurrenceName, InsuranceprogramID, 
Cost1, Cost2, Cost3, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12
-- Update for cost discounts
UPDATE @CostsConsidered
SET Cost1 = c.Cost1 * Cost1Discount, Cost2 = c.Cost2 * Cost2Discount, Cost3 = c.Cost3 * Cost3Discount,
Cost4 = c.Cost4 * Cost4Discount, Cost5 = c.Cost5 * Cost5Discount, Cost6 = c.Cost6 * Cost6Discount,
Cost7 = c.Cost7 * Cost7Discount, Cost8 = c.Cost8 * Cost8Discount, Cost9 = c.Cost9 * Cost9Discount,
Cost10 = c.Cost10 * Cost10Discount, Cost11 = c.Cost11 * Cost11Discount, Cost12 = c.Cost12 * Cost12Discount,
TotalCostsConsidered = c.Cost1 * Cost1Discount + c.Cost2 * Cost2Discount + c.Cost3 * Cost3Discount + c.Cost4 * Cost4Discount + 
c.Cost5 * Cost5Discount + c.Cost6 * Cost6Discount + c.Cost7 * Cost7Discount + c.Cost8 * Cost8Discount + 
c.Cost9 * Cost9Discount + c.Cost10 * Cost10Discount + c.Cost11 * Cost11Discount + c.Cost12 * Cost12Discount
FROM @CostsConsidered c INNER JOIN tblAllocation a ON c.AllocationID = a.AllocationID
/* PART 2. UNALLOCATED DOLLARS DUE TO HORIZONTAL UNCOLLAPSE */
--------------------------------------------------------------
DECLARE @UnallocatedHorizontal table (AllocationID int, OccurrenceID bigint, uncollapsedPrior money, uncollapsedPost money)
INSERT INTO @UnallocatedHorizontal (AllocationID, OccurrenceID, uncollapsedPrior, uncollapsedPost)
SELECT AllocationID, OccurrenceID, SUM(COALESCE(UncollapsedPrior,0)), SUM(COALESCE(UncollapsedPost,0))
FROM tblAllocationResultsHorizontal
WHERE AllocationID = @allocationID
GROUP BY AllocationID, OccurrenceID
/* PART 3. UNALLOCATED DOLLARS DUE TO VERTICAL UNCOLLAPSE */
------------------------------------------------------------
DECLARE @UnallocatedVertical table (AllocationID int, OccurrenceID bigint, uncollapsedVertical money)
INSERT INTO @UnallocatedVertical (AllocationID, OccurrenceID, uncollapsedVertical)
SELECT arv.AllocationID, arv.OccurrenceID, SUM(COALESCE(uncollapsedVertical,0))
FROM tblAllocationResultsVertical arv 
	INNER JOIN tblAllocationResultsColumn arc ON arv.AllocationID = arc.AllocationID and arv.ColumnID = arc.ColumnID
WHERE arv.allocationID = @allocationID AND arc.InsuranceProgramID = @insuranceProgramID
GROUP BY arv.AllocationID, arv.OccurrenceID
/* PART 4. TOTAL DOLLARS ALLOCATED TO POLICIES / SIRS */
--------------------------------------------------------
DECLARE @AllocationTotals table (AllocationID int, AllocationDescription nvarchar(256), InsuranceProgramID int, OccurrenceID bigint,
IndemnityAllocation money, DefenseWithinLimitsAllocation money, DefenseOutsideLimitsAllocation money,
TotalAllocation money, SIRIndemnityAllocation money, SIRDefenseAllocation money, TotalSIRAllocation money)
INSERT INTO @AllocationTotals (AllocationID, AllocationDescription, InsuranceProgramID, OccurrenceID,
	IndemnityAllocation, DefenseWithinLimitsAllocation, DefenseOutsideLimitsAllocation, TotalAllocation,
	SIRIndemnityAllocation, SIRDefenseAllocation, TotalSIRAllocation)
SELECT @allocationID, AllocationDescription, @InsuranceProgramID, ar.OccurrenceID, 
SUM(AllocationIndemnity), SUM(AllocationDefense), SUM(AllocationDefenseOutsideLimits), SUM(Allocation),
SUM(SIRAllocatedIndemnity), SUM(SIRAllocatedDefense), SUM(SIRAllocated)
FROM tblAllocationResults ar INNER JOIN tblPolicy p ON p.PolicyID = ar.PolicyID
	INNER JOIN tblAllocation a ON a.AllocationID = ar.AllocationID
WHERE ar.AllocationID = @allocationID AND InsuranceProgramID = @insuranceProgramID
GROUP BY a.AllocationID, ar.OccurrenceID, InsuranceProgramID, AllocationDescription
/* PART 5. SUMMARIZE RESULTS */
-------------------------------
SELECT @AllocationID AS AllocationID, a.AllocationDescription, @InsuranceProgramID AS InsuranceProgramID,
InsuranceProgram, c.OccurrenceID, OccurrenceName, c.Cost1, c.Cost2, c.Cost3, c.Cost4, c.Cost5, c.Cost6, 
c.Cost7, c.Cost8, c.Cost9, c.Cost10, c.Cost11, c.Cost12, TotalCostsConsidered,
COALESCE(IndemnityAllocation,0) AS IndemnityAllocation, COALESCE(DefenseWithinLimitsAllocation,0) AS DefenseWithinLimitsAllocation,
COALESCE(DefenseOutsideLimitsAllocation,0) AS DefenseOutsideLimitsAllocation, COALESCE(TotalAllocation,0) AS TotalAllocation,
COALESCE(SIRIndemnityAllocation,0) AS SIRIndemnityAllocation, COALESCE(SIRDefenseAllocation,0) AS SIRDefenseAllocation,
COALESCE(TotalSIRAllocation,0) AS TotalSIRAllocation, COALESCE(uncollapsedPrior,0) AS UnallocatedPriortoCoverage,
COALESCE(uncollapsedPost,0) AS UnallocatedPostCoverage, COALESCE(uncollapsedVertical,0) AS UnallocatedVertical,
(COALESCE(uncollapsedPrior,0) + COALESCE(uncollapsedPost,0) + COALESCE(uncollapsedVertical,0)) AS TotalUnallocatedDollars
FROM @CostsConsidered c
LEFT JOIN @AllocationTotals a ON c.OccurrenceID = a.OccurrenceID
LEFT JOIN @UnallocatedHorizontal ah ON c.OccurrenceID = ah.OccurrenceID
LEFT JOIN @UnallocatedVertical av ON c.OccurrenceID = av.OccurrenceID
LEFT JOIN tblInsuranceProgram i ON c.InsuranceProgramID = i.InsuranceProgramID
--INNER JOIN tblAllocation al ON c.AllocationID = al.AllocationID
WHERE c.Allocationid = @allocationID AND c.InsuranceProgramID = @insuranceProgramID
GROUP BY a.AllocationID, a.AllocationDescription, InsuranceProgram, c.OccurrenceID, OccurrenceName, 
c.Cost1, c.Cost2, c.Cost3, c.Cost4, c.Cost5, c.Cost6, c.Cost7, c.Cost8, c.Cost9, c.Cost10, c.Cost11, c.Cost12, TotalCostsConsidered, 
IndemnityAllocation, DefenseWithinLimitsAllocation, DefenseOutsideLimitsAllocation, TotalAllocation, 
SIRIndemnityAllocation, SIRDefenseAllocation, TotalSIRAllocation, uncollapsedPrior, uncollapsedPost, uncollapsedVertical
ORDER BY OccurrenceName, c.OccurrenceID
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARQCCostsComparedAllocation_InsProg] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARQCCostsComparedAllocation_InsProg] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARQCCostsComparedAllocation_InsProg] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARQCCostsComparedAllocation_InsProg] TO [power_user]
GO
