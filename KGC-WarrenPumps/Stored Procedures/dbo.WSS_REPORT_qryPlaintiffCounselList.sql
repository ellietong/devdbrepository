SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORT_qryPlaintiffCounselList]
AS 
    SET NOCOUNT ON

    SELECT  DISTINCT
			PlaintiffCounselID ,
            PlaintiffCounsel 
    FROM    tblPlaintiffCounselAddress b
	INNER JOIN tblLawsuit l on l.PlaintiffCounselAddressID = b.PlaintiffCounselAddressID
    WHERE   PlaintiffCounsel IS NOT NULL
            AND PlaintiffCounsel <> ''
    UNION
    SELECT  NULL ,
            NULL
    ORDER BY PlaintiffCounsel 
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORT_qryPlaintiffCounselList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORT_qryPlaintiffCounselList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORT_qryPlaintiffCounselList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORT_qryPlaintiffCounselList] TO [power_user]
GO
