SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByDisease] 
	-- Add the parameters for the stored procedure here
	(@StartDate datetime, @EndDate datetime, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 
 
DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
 
DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole =0-- (select distinct InsurerRole from vtblPerson where Email = @emailaddress)
 
DECLARE @InactiveTemp table (InactiveClaimlawsuitID int)
INSERT INTO @InactiveTemp
select ClaimLawsuitID
from tblClaimLawsuitStatusTracking
where IsPrimaryStatus=1 and ClaimLawsuitStatusID in (41,29)
 
--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1)
BEGIN
 
select CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
                                              ELSE hld.HighLevelDisease END AS Disease, 
		COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
where coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		AND PrimaryDisease=1 AND cl.ClaimLawsuitID NOT IN (Select i.InactiveClaimLawsuitID from @InactiveTemp i)
GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
ORDER BY hld.HighLevelDiseaseID
 
 
END
ELSE IF(@InsurerRole = 1) --Insurers
BEGIN
 
select CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
                                              ELSE hld.HighLevelDisease END AS Disease, 
		COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
where coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		AND PrimaryDisease=1 AND cl.ClaimLawsuitID NOT IN (Select i.InactiveClaimLawsuitID from @InactiveTemp i)
GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
ORDER BY hld.HighLevelDiseaseID
 
END
ELSE
BEGIN
 
select CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
                                              ELSE hld.HighLevelDisease END AS Disease, 
		COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
where coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
	AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
	AND PrimaryDisease=1 AND cl.ClaimLawsuitID NOT IN (Select i.InactiveClaimLawsuitID from @InactiveTemp i)
GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
ORDER BY hld.HighLevelDiseaseID
 
END
 
END
 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByDisease] TO [power_user]
GO
