SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--USE [KGC-MarleyWylain]
--GO
--/****** Object:  StoredProcedure [dbo].[WSS_INVOICES_InvoiceSearchByClaimant]    Script Date: 06/29/2012 15:09:47 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE PROC [dbo].[WSS_REPORTS_qryDefenseSummary]
(@PaidDateStart date, @PaidDateEnd date, @emailaddress nvarchar(250), @FirmID int= null)
AS

----VARIABLES FOR TESTING----
--DECLARE @FirmID int
--DECLARE @VendorFirmID int
--DECLARE @StatusID bigint
--DECLARE @PaidDateStart nvarchar(50)
--DECLARE @PaidDateEnd nvarchar(50)
--DECLARE @Period nvarchar(50)
--DECLARE @emailaddress nvarchar(250)
--SET @FirmID		   = NULL
--SET @VendorFirmID  = NULL
--SET @StatusID	   = NULL
--SET @PaidDateStart = NULL
--SET @PaidDateEnd   = NULL
--SET @Period		   = NULL
--SET @emailaddress  = 'Gary.McKenna@spx.com'



--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE d.detaildocumenttypeid in (40, 41)'
IF @PaidDateStart	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND C.SourceDate >= ''' + CONVERT(nvarchar(20),@PaidDateStart) + '''' END
IF @PaidDateEnd		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND C.SourceDate <= ''' + CONVERT(nvarchar(20),@PaidDateEnd) + '''' END

--These IF statements determine additional WHERE statements by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @FirmByEmailID int
SET @FirmByEmailID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)
IF ((LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') or @FirmByEmailID=188)
	SET @WhereString = @WhereString
ELSE
	SET @WhereString = @WhereString + ' AND f.firmid= '+ convert(varchar,@FirmByEmailID)


DECLARE @QueryString nvarchar(max)
SET @QueryString = '
SELECT 		lastname+'', ''+coalesce(firstname, '' '') as Claimantname,
			sum(coalesce(dicd.fees,0))+ sum(coalesce(dicd.costs,0))+sum(coalesce(dicd.adjustments,0)) AS DefenseTotal, 
			dicd.claimantpersonalinfoID
FROM tblDocuments d
		LEFT JOIN tblDefenseInvoice di				ON d.DocumentID = di.DocumentID 
		LEFT JOIN tblDefenseInvoiceStatus dis		ON di.StatusID = dis.StatusID 
		LEFT JOIN vtblFirm f						ON d.FirmID = f.FirmID
		LEFT JOIN vtblFirmsAndVendors ex			ON d.VendorFirmID = ex.FirmID
		INNER JOIN tblPaymentDefense pd				ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		INNER JOIN (select * 
				   from tblCheck
				   WHERE fundingsourceID not in (3,4,5)) c	ON pd.CheckID = c.CheckID 
		LEFT JOIN tblDefInvClaimantDetail dicd		ON dicd.DocumentID = d.DocumentID 
		LEFT JOIN tblClaimantPersonalInfo cpi		ON cpi.ClaimantPersonalInfoID = dicd.ClaimantPersonalInfoID'
 +	@WhereString + '
	GROUP BY dicd.claimantpersonalinfoid,
			lastname+'', ''+coalesce(firstname, '' '')
	--ORDER BY DateCreated DESC, Accepted ASC' 
	--OPTION (RECOMPILE)'
PRINT @Querystring
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString--, N'@PeriodParam nvarchar(max)', @PeriodParam = @Period

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDefenseSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDefenseSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDefenseSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDefenseSummary] TO [power_user]
GO
