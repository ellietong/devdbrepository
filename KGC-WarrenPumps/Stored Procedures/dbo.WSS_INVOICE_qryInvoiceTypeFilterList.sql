SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_INVOICE_qryInvoiceTypeFilterList] ( @clientFlag BIT = 0 )
AS 
    SET NOCOUNT ON

    IF @clientFlag = 1 
        BEGIN
            SELECT  ClaimantDocumentTypeID ,
                    ClaimantDocumentType ,
                    ClaimantDocumentGroupID
            FROM    tblClaimantDocumentType
            WHERE   ClaimantDocumentGroupID = 2
            UNION
            SELECT  NULL ,
                    '<< All Invoice Types >>' ,
                    NULL
            ORDER BY ClaimantDocumentType
        END
    ELSE 
        BEGIN
            SELECT  ClaimantDocumentTypeID ,
                    ClaimantDocumentType ,
                    ClaimantDocumentGroupID
            FROM    tblClaimantDocumentType
            WHERE   ClaimantDocumentGroupID = 2
                    AND ClaimantDocumentTypeID NOT IN ( 41, 42 )
            UNION
            SELECT  NULL ,
                    '<< All Invoice Types >>' ,
                    NULL
            ORDER BY ClaimantDocumentType
        END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryInvoiceTypeFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryInvoiceTypeFilterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryInvoiceTypeFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryInvoiceTypeFilterList] TO [power_user]
GO
