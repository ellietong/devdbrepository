SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetJobsiteCityCompletionList]
    (
      @prefix NVARCHAR(MAX) ,
      @count INT = 0
    )
AS 
    SET NOCOUNT ON

    SET ROWCOUNT @count

    SELECT DISTINCT
            JobsiteCity
    FROM    tblClaimantEmployment
    WHERE   JobsiteCity LIKE @prefix + '%'

    SET ROWCOUNT 0
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetJobsiteCityCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetJobsiteCityCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetJobsiteCityCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetJobsiteCityCompletionList] TO [power_user]
GO
