SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryfrmCopyDocumentToDefenseInvoice]
(@DocumentID int,  @DefenseCounselAddressID int)
AS

--declare @documentID int
--declare @defensecounselAddressID int

--set @documentID = 52252886
--set @defensecounselAddressID = null

SET NOCOUNT ON

--Copy Document Record to tblDefenseInvoice

INSERT INTO tblDefenseInvoice (StatusID, VendorFirmID, FirmID, InvoiceNumber, Period, 
FeeAmount, DisbursementAmount, Adjustment, InvoiceTotal, SubmitDate, DatePayable, CheckNumber,
FundsRequestDate, DatePaid, DocumentID, AdjustmentNotes, InvoiceLink, CheckLink, DefenseCounselAddressID,ClaimantPersonalInfoID, MatterID)

SELECT StatusID, 
VendorFirmID AS VendorFirmID,
Firmid, 
InvoiceNumber AS InvoiceNumber, 
Period AS Period, 
FeeAmount, 
DisbursementAmount, 
Adjustment, 
(FeeAmount + DisbursementAmount - Adjustment) AS InvoiceTotal,
DateCreated AS SubmitDate, 
DatePayable AS DatePayable,
NULL AS CheckNumber,
NULL AS FundsRequestDate,
NULL AS DatePaid,
DocumentID,
AdjustmentNotes,
DocumentURL AS InvoiceLink,
'' AS CheckLink, @DefenseCounselAddressID as DefenseCounselAddressID, null as ClaimantPersonalInfoID, Null as MatterID
FROM tblDocuments 
WHERE DocumentID = @DocumentID

--Update Document Record in tblDocuments
UPDATE tblDocuments
SET QCDate = GETDATE()
WHERE DocumentID = @DocumentID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [power_user]
GO
