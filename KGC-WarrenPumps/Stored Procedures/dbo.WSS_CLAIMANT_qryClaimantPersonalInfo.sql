
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryClaimantPersonalInfo]
    (
      @ClaimantPersonalInfoID BIGINT = NULL ,
      @emailaddress NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON

--The following set of variables can be used for testing.
--DECLARE @emailaddress nvarchar(250)
--DEcLARE @ClaimantPersonalInfoID bigint
--SET @emailaddress = 'aarmenteros@meagher.com'
--SET @ClaimantPersonalInfoID = 38765

    DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE cpi.ClaimantPersonalInfoID = '
        + CONVERT(NVARCHAR(20), @ClaimantPersonalInfoID)
						  
						   
						   


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    DECLARE @JoinString NVARCHAR(MAX)
    IF ( ( RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' )
         OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        SET @JoinString = ' '
    ELSE 
        BEGIN
            SET @JoinString = ' LEFT  JOIN tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
                        LEFT  JOIN tblLawsuit l					ON cl.LawsuitID = l.LawsuitID
						INNER JOIN tblDefenseJurisdiction dj	ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID '
            SET @WhereString = @WhereString
                + ' AND (dj.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ' OR l.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ')'
        END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
    DECLARE @QueryString NVARCHAR(MAX)
    SET @QueryString = 'SELECT DISTINCT cpi.ClaimantPersonalInfoID, 
		cpi.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + 
			CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' END AS ClaimantName, 
        CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN,
        Gender, 
        COALESCE(cpi.GenderID, 4) AS GenderID, 
        COALESCE(cpi.MaritalStatusID, 1) AS MaritalStatusID, 
        MaritalStatus, 
        COALESCE(nullif(COALESCE(Address1 + CHAR(13) + CHAR(10), '''') + COALESCE(Address2 + CHAR(13) + CHAR(10),'''') + 
			COALESCE(City + '', '','''') + COALESCE(s.State + '' '','''') + COALESCE(CONVERT(varchar,Zip),'''') + 
			COALESCE(''-''+CONVERT(varchar,mailzip),''''),''''),'''') as CompleteAddress, 
        BirthDate, 
        DeceasedDate, 
		City, 
		s.StateID, 
		Zip, 
		MailZip, 
		Address1, 
		Address2, 
		IsSmoker, 
		SmokerStart,
		SmokerEnd, 
		PacksPerDay, 
		CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE SocialSecurityNumber END AS FullSSN,
		c.ReferenceID,
		HealthInsuranceClaimNumber as HICN
    FROM tblClaimantPersonalInfo cpi 
		 LEFT JOIN tblGender g ON g.GenderID = cpi.GenderID
         LEFT JOIN tblMaritalStatus ms ON ms.MaritalStatusID = cpi.MaritalStatusID
         LEFT JOIN tblState s ON cpi.StateID = s.StateID 
		 LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID' + @JoinString
        + @WhereString + '
    OPTION (RECOMPILE)'

--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString


GO

GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantPersonalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantPersonalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantPersonalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantPersonalInfo] TO [power_user]
GO
