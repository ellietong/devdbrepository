SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ACUITY_DefenseFirmLookup]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @newline nvarchar(5) = CHAR(13)+CHAR(10)
	DECLARE @openingTags nvarchar(MAX)= '<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE pace_lookups SYSTEM "pace_lookups_BillingWeb.dtd">
	<pace_lookups run_timestamp="' + CONVERT(nvarchar, GETDATE(), 121) + '">
		<clients>
			<client client_name="WARREN PUMPS" client_code="WAR" pace_key="040B8D12-049E-49F4-AC64-856FEB6E9A77"/>
		</clients>'
	DECLARE @closingTag nvarchar(MAX) = '</pace_lookups>'
	DECLARE @Firms nvarchar(MAX)

	SET @Firms = CONVERT(nvarchar(MAX), 
					(SELECT	
					[vendor/@pace_key] = COALESCE(AcuityKey, ''),
					[vendor/@active] = 'Y',
					[vendor/@vendor_name] = DefenseCounsel,
					[vendor/@vendor_id] = '',
					[vendor] = (SELECT	[defense_firm/@pace_key] = COALESCE(AcuityKey, ''),
										[defense_firm/@active] = 'Y',
										[defense_firm/@firm_name] = DefenseCounsel,
										[defense_firm/@firm_id] = '',
										[defense_firm] = ''
								FROM tblDefenseCounsel dc1 
								WHERE dc.DefenseCounselID = dc1.DefenseCounselID
								FOR XML PATH(''), type)
			FROM tblDefenseCounsel dc
			WHERE AcuityKey is not null
			for xml path( '' ), root('vendors'), type))

	SELECT @openingTags+ @Firms + @ClosingTag
	--PRINT SUBSTRING(@openingTags + @Firms + @ClosingTag, 0, 4000) + SUBSTRING(@openingTags + @Firms + @ClosingTag, 4000, 8000) + 
		--	SUBSTRING(@openingTags + @Firms + @ClosingTag, 8000, 12000)  + SUBSTRING(@openingTags + @Firms + @ClosingTag, 12000, 16000)
	--PRINT SUBSTRING(@openingTags + @Firms + @ClosingTag, 4000, 8000)
	--PRINT SUBSTRING(@openingTags + @Firms + @ClosingTag, 8000, 12000) 
	--PRINT SUBSTRING(@openingTags + @Firms + @ClosingTag, 12000, 16000)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ACUITY_DefenseFirmLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[ACUITY_DefenseFirmLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ACUITY_DefenseFirmLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[ACUITY_DefenseFirmLookup] TO [power_user]
GO
