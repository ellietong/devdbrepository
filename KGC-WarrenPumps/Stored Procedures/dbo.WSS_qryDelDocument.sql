SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDelDocument]
(@DocumentID int, @Emailaddress nvarchar(250))
AS
BEGIN
	SET NOCOUNT ON;
	
	--Select the DocumentURL so C# code knows the path of the file which is to be deleted
	SELECT DocumentURL FROM tblDocuments WHERE DocumentID = @DocumentID
	
	--Update the SPUsername field in tblMultiClaimantDocument prior to delete for audit log purposes
	UPDATE tblMultiClaimantDocument
		SET SPUsername = @emailaddress
	WHERE DocumentID = @DocumentID
	
	--Update the ModifiedBy field in tblDocuments prior to delete for audit log purposes
	UPDATE tblDocuments
		SET ModifiedBy = @emailaddress
	WHERE DocumentID = @DocumentID
	
	--update settleDoc record prior to delete for audit log
	UPDATE tblSettleDocs
		SET SPUsername = @Emailaddress
	FROM tblSettleDocs sd
	WHERE sd.DocumentID = @DocumentID
	
	--delete related settledocs records
	DELETE tblSettleDocs FROM tblSettleDocs sd
	WHERE sd.DocumentID = @DocumentID
	
	--update tblDefenseInvoice record prior to delete for audit log
	UPDATE tblDefenseInvoice
		SET QCBy = @Emailaddress
	FROM tblDefenseInvoice di
	WHERE di.DocumentID = @DocumentID
	
	--Delete invoice records associated with the document
	DELETE tblDefInvClaimantDetail FROM tblDefInvClaimantDetail dc
	WHERE dc.DocumentID = @DocumentID
	
	DELETE tblDefenseInvoice FROM tblDefenseInvoice di
	WHERE di.DocumentID = @DocumentID
	
	--This is used on ViewDoc to delete **ALL** tblDocument and tblMultiClaimantDocument records for a given DocumentID
	DELETE FROM tblMultiClaimantDocument WHERE DocumentID = @DocumentID
	DELETE FROM tblDocuments WHERE DocumentID = @DocumentID
	
	
		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelDocument] TO [power_user]
GO
