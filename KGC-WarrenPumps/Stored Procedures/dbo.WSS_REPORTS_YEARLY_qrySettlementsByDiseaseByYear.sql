SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] (@MatterID int = NULL, 
                                                                     @YearID   int, 
                                                                     @MonthID  int) 
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @year int
DECLARE @Month int
SET @year = @YearID
SET @Month = @MonthID

DECLARE @Settlement TABLE (HighLevelDisease nvarchar(25), Count int, Settlement money, AverageIndemnity money)
INSERT INTO @Settlement
	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
	                                              ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS 'Count', 
		SUM(SettlementAmount) AS IndemnityPaid, 
		SUM(SettlementAmount) / COUNT(DISTINCT(cl.ClaimLawsuitID)) AS AverageIndemnity
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
		INNER JOIN tblClaimantSettlement cs           ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
		LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
	WHERE  MONTH(COALESCE(SettlementDate,ProcessedDate)) <= @Month
		AND YEAR(COALESCE(SettlementDate,ProcessedDate)) = @year 
		AND ClaimLawsuitStatusID IN (23,24,25,28) 
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
		AND cd.PrimaryDisease = 1
		AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID

SELECT hld.HighLevelDisease, 
	COALESCE([Count],0) AS 'Count', 
	COALESCE(Settlement,0) AS IndemnityPaid, 
	COALESCE(AverageIndemnity,0) AS AverageIndemnity
FROM tblHighLevelDisease hld
	LEFT JOIN @Settlement s ON hld.HighLevelDisease = s.HighLevelDisease
WHERE HighLevelDiseaseID !=6
ORDER BY hld.HighLevelDiseaseID


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [power_user]
GO
