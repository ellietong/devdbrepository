SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryInsClaimant] 
	-- Add the parameters for the stored procedure here
    (
      @Address1 NVARCHAR(250) ,
      @Address2 NVARCHAR(250) ,
      @StateID INT ,
      @City NVARCHAR(250) ,
      @Zip NVARCHAR(5) ,
      @Mailzip NVARCHAR(4) ,
      @BirthDate DATETIME ,
      @DeceasedDate DATETIME ,
      @GenderID INT ,
      @MaritalStatusID INT , 
																 --@IsSmoker				 bit, 
																 --@SmokerStart			 datetime, 
																 --@SmokerEnd				 datetime, 
																 --@PacksPerDay			 float,
      @EmailAddress NVARCHAR(250) ,
      @FullSSN NVARCHAR(150) ,
      @FirstName NVARCHAR(250) ,
      @MiddleName NVARCHAR(250) = NULL ,
      @LastName NVARCHAR(250) ,
      @SuffixID INT ,
      @IsSmoker BIT	
    )
																 --@Notes					 nvarchar (1000)) --@DOFE datetime, @ClaimTypeID int, @Notes nvarchar (1000), @SuffixID int, @Emailaddress nvarchar(250))
AS 
    BEGIN

        DECLARE @MaxCPID INT
        SET @MaxCPID = ( SELECT IDENT_CURRENT('tblClaimantPersonalInfo')
                                + IDENT_INCR('tblClaimantPersonalInfo')
                       )

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        INSERT  INTO tblClaimantPersonalInfo
                ( FirstName ,
                  MiddleName ,
                  LastName , 
										--Notes, 
                  SuffixID ,
                  GenderID ,
                  BirthDate ,
                  DeceasedDate ,
                  SocialSecurityNumber ,
                  MaritalStatusID ,
                  Address1 ,
                  Address2 ,
                  City ,
                  StateID ,
                  Zip ,
                  MailZip ,
                  SPUsername ,
                  IsSmoker
                ) --DOFE, ClaimTypeID, Notes, SuffixID, SPUsername)
        VALUES  ( ( UPPER(@FirstName) ) ,
                  ( UPPER(@MiddleName) ) ,
                  ( UPPER(@LastName) ) , 
			--@Notes, 
                  @SuffixID ,
                  @GenderID ,
                  @BirthDate ,
                  @DeceasedDate ,
                  @FullSSN ,
                  @MaritalStatusID ,
                  @Address1 ,
                  @Address2 ,
                  @City ,
                  @StateID ,
                  @Zip ,
                  @Mailzip ,
                  @Emailaddress ,
                  @IsSmoker
                ) --@DOFE, @ClaimTypeID, @Notes, @SuffixID, @Emailaddress)
	
        SELECT  @MaxCPID	
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryInsClaimant] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryInsClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryInsClaimant] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryInsClaimant] TO [power_user]
GO
