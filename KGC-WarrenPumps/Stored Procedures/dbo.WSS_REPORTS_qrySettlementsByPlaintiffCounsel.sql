SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qrySettlementsByPlaintiffCounsel] 
	-- Add the parameters for the stored procedure here
	(@StartDate datetime, @EndDate datetime, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 
 
DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
 
DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = 0--(select distinct InsurerRole from vtblPerson where Email = @emailaddress)
 
--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1)
BEGIN
 
select coalesce(pca.PlaintiffCounsel, 'Unknown') as PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
left join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
where	coalesce(StatusDate, ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID =1
		and  IsPrimaryStatus=1
		
group by pca.PlaintiffCounsel
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, pca.PlaintiffCounsel
 
end
ELSE IF(@InsurerRole = 1) --Insurers
BEGIN
 
select coalesce(pca.PlaintiffCounsel, 'Unknown') as PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
left join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
where	coalesce(StatusDate, ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID =1
		and  IsPrimaryStatus=1
		
group by pca.PlaintiffCounsel
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, pca.PlaintiffCounsel
 
END
 
ELSE --Local Defense Counsels
BEGIN
 
select coalesce(pca.PlaintiffCounsel, 'Unknown') as PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
left join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
where	coalesce(StatusDate, ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID =1
		and  IsPrimaryStatus=1
		and (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
		
group by pca.PlaintiffCounsel
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, pca.PlaintiffCounsel
 
END
 
END
 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementsByPlaintiffCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementsByPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementsByPlaintiffCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementsByPlaintiffCounsel] TO [power_user]
GO
