SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qrysrptNoticeDocumentLink_2]
(@CorrespondenceID int)
AS
SET NOCOUNT ON


Select sd.CorrespondenceID, SupportingDocumentType, Description + ' - ' + InternalLink as ExternalLink
From tblSupportingDocument sd
Left Join tblSupportingDocumentType sdt on sd.SupportingDocumentTypeID=sdt.SupportingDocumentTypeID
Where sd.CorrespondenceID = @CorrespondenceID 
Group By sd.SupportingDocumentTypeID, sd.CorrespondenceID, SupportingDocumentType, Description, InternalLink
Order By sd.SupportingDocumentTypeID, sd.CorrespondenceID, SupportingDocumentType, Description, InternalLink
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptNoticeDocumentLink_2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptNoticeDocumentLink_2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptNoticeDocumentLink_2] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptNoticeDocumentLink_2] TO [power_user]
GO
