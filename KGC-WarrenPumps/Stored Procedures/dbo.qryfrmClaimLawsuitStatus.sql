SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmClaimLawsuitStatus]

as

Select ClaimLawsuitStatusID, ClaimLawsuitStatus
From tblClaimLawsuitStatus
Order By ClaimLawsuitStatus

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitStatus] TO [power_user]
GO
