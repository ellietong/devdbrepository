SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryTrialGroup]
(@PrefixText nvarchar(200), @Emailaddress nvarchar(250) )
AS
BEGIN
	SELECT TrialGroupID, TrialGroup --+ ' (' + CONVERT(nvarchar, TrialGroupID) + ')'
	FROM tblTrialGroup
	WHERE TrialGroup LIKE '%' + @PrefixText + '%'
	UNION
	SELECT -1, 'Add New Trial Group'
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryTrialGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryTrialGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryTrialGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryTrialGroup] TO [power_user]
GO
