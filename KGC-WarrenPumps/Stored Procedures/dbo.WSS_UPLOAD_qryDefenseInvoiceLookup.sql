SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup]
AS 
    SET NOCOUNT ON

--SELECT StatusID, [Status]
--FROM tblDefenseInvoiceStatus
--WHERE StatusID <> 3
--RETURN

    SELECT  statusid ,
            status
    FROM    tblDefenseInvoiceStatus
    WHERE   StatusID <> 5
--UNION
--	--SELECT null as StatusID, '<< All Status >>' As status
--	--UNION
--	SELECT null, '<<Any Status>>'
ORDER BY    Status
    RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [power_user]
GO
