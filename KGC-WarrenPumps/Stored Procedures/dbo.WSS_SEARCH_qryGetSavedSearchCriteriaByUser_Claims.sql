SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims]
	-- Add the parameters for the stored procedure here
    @emailaddress NVARCHAR(500) ,
    @SavedSearchID INT
AS 
    BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT  Jurisdiction AS ParamValue ,
                sj.JurisdictionID AS ID ,
                'JParam' AS ParamType
        FROM    tblSavedJurisdiction sj
                INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = sj.SavedSearchID
                INNER JOIN tblJurisdiction j ON sj.JurisdictionID = j.JurisdictionID
        WHERE   SPUserName = @emailaddress
                AND sj.SavedSearchID = @SavedSearchID
        UNION
        SELECT  HighLevelDisease AS Disease ,
                sd.DiseaseID ,
                'DParam'
        FROM    tblSavedDisease sd
                INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = sd.SavedSearchID
                INNER JOIN tblHighLevelDisease d ON sd.DiseaseID = d.HighLevelDiseaseID
        WHERE   SPUserName = @emailaddress
                AND sd.SavedSearchID = @SavedSearchID
        UNION
        SELECT  AllegationType AS Param ,
                sa.AllegationTypeID ,
                'AParam'
        FROM    tblSavedAllegation sa
                INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = sa.SavedSearchID
                INNER JOIN tblAllegationType at ON sa.AllegationTypeID = at.AllegationTypeID
        WHERE   SPUserName = @emailaddress
                AND sa.SavedSearchID = @SavedSearchID
        UNION
        SELECT  ClaimLawsuitStatus AS Param2 ,
                LawsuitStatusID ,
                'LSParam'
        FROM    tblSavedLawsuitStatus sls
                INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = sls.SavedSearchID
                INNER JOIN tblClaimLawsuitStatus cls ON sls.LawsuitStatusID = cls.ClaimLawsuitStatusID
        WHERE   SPUserName = @emailaddress
                AND sls.SavedSearchID = @SavedSearchID
        UNION
        SELECT  ActiveTrialDateRange ,
                '' ,
                'ATDRParam'
        FROM    tblSavedActiveTrialDateRange satdr
                INNER JOIN tblSavedSearch ss ON satdr.SavedSearchID = ss.SavedSearchID
        WHERE   SPUserName = @emailaddress
                AND satdr.SavedSearchID = @SavedSearchID
        UNION
        SELECT  State ,
                s.StateID ,
                'StParam'
        FROM    tblSavedState s
                INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = s.SavedSearchID
                INNER JOIN tblState st ON s.StateID = st.StateID
        WHERE   SPUserName = @emailaddress
                AND s.SavedSearchID = @SavedSearchID
        UNION
        SELECT  PlaintiffCounsel ,
                pc.PlaintiffCounselAddressID ,
                'PCParam'
        FROM    tblSavedPlaintiffCounsel pc
                INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = pc.SavedSearchID
                INNER JOIN tblPlaintiffCounselAddress pca ON pc.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
        WHERE   SPUserName = @emailaddress
                AND pc.SavedSearchID = @SavedSearchID
        UNION
        SELECT  Matter ,
                sm.MatterID ,
                'MParam'
        FROM    tblSavedMatter sm
                INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = sm.SavedSearchID
                INNER JOIN tblMatter m ON sm.MatterID = m.MatterID
        WHERE   SPUserName = @emailaddress
                AND sm.SavedSearchID = @SavedSearchID
        UNION
        SELECT  CONVERT(NVARCHAR(15), IsMDL) ,
                '' ,
                'MDLParam'
        FROM    tblSavedIsMDL mdl
                INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = mdl.SavedSearchID
        WHERE   SPUserName = @emailaddress
                AND mdl.SavedSearchID = @SavedSearchID
        UNION
        SELECT  CONVERT(NVARCHAR(15), IsFederal) ,
                '' ,
                'FedParam'
        FROM    tblSavedIsFederal fed
                INNER JOIN tblSavedSearch ss ON ss.SavedSearchID = fed.SavedSearchID
        WHERE   SPUserName = @emailaddress
                AND fed.SavedSearchID = @SavedSearchID
        UNION
        SELECT  ProcessedDateRange ,
                '' ,
                'PDRParam'
        FROM    tblSavedProcessedDateRange spdr
                INNER JOIN tblSavedSearch ss ON spdr.SavedSearchID = ss.SavedSearchID
        WHERE   SPUserName = @emailaddress
                AND spdr.SavedSearchID = @SavedSearchID
        UNION
        SELECT  StatusDateRange ,
                '' ,
                'SDRParam'
        FROM    tblSavedStatusDateRange ssdr
                INNER JOIN tblSavedSearch ss ON ssdr.SavedSearchID = ss.SavedSearchID
        WHERE   SPUserName = @emailaddress
                AND ssdr.SavedSearchID = @SavedSearchID


    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryGetSavedSearchCriteriaByUser_Claims] TO [power_user]
GO
