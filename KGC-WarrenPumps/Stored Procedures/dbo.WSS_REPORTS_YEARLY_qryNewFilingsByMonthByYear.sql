SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByMonthByYear] (@MatterID int = NULL,
                                                                  @YearID   int, 
                                                                  @MonthID  int)  
AS
SET NOCOUNT ON
    
----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @year int
DECLARE @Month int
SET @year = @YearID
SET @Month = @MonthID

SELECT DATENAME("mm", COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) AS [MonthName], 
	COUNT(DISTINCT(cl.ClaimLawsuitID)) AS MonthCount, 
	MONTH(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) As MonthID
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
    LEFT JOIN (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
    LEFT  JOIN tblState s                         ON l.StateID = s.StateID
WHERE  MONTH(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) <= @Month
	AND YEAR(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) = @Year 
	AND MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
GROUP BY DATENAME("mm", COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)), MONTH(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate))
ORDER BY MONTH(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate))


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByMonthByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByMonthByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByMonthByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByMonthByYear] TO [power_user]
GO
