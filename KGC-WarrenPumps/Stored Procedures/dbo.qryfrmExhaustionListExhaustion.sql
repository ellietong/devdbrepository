SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmExhaustionListExhaustion]
(@strBillGroup nvarchar(2000), @exhaustionTypeID int)
AS

SET NOCOUNT ON

DECLARE @strSQL nvarchar (4000)

SET @strSQL = '

SELECT ed.ExhaustionDateID, ExhaustionDate, AllocationID, SUM(IndemnityAllocation) + SUM(DefenseAllocation) + SUM(DefenseOutsideLimitsAllocation) AS AllocationTotal, l.LedgerID
FROM (tblExhaustion e INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID) INNER JOIN tblLedger l ON ed.ExhaustionDateID = l.ExhaustionDateID
WHERE BillGroupID IN ( ' + @strBillGroup + ' ) AND ExhaustionTypeID = ' + CONVERT(nvarchar(10), @exhaustionTypeID) + ' 
GROUP BY ed.ExhaustionDateID, ExhaustionDate, AllocationID, l.LedgerID
ORDER BY ExhaustionDate DESC, l.LedgerID'

EXEC sp_executeSQL @strSQL

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustionListExhaustion] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustionListExhaustion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustionListExhaustion] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustionListExhaustion] TO [power_user]
GO
