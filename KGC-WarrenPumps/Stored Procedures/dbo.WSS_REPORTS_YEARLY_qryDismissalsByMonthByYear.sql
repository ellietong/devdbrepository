SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByMonthByYear] (@MatterID int = NULL, 
                                                                  @YearID   int, 
                                                                  @MonthID  int) 
AS
SET NOCOUNT ON
	
----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @year int
DECLARE @Month int
SET @year = @YearID
SET @Month = @MonthID

SELECT DATENAME("mm", ProcessedDate) AS [MonthName], 
	COUNT(DISTINCT(cl.ClaimLawsuitID)) AS 'Count', 
	MONTH(ProcessedDate) AS MonthID
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT  JOIN tblState s                         ON l.StateID = s.StateID
WHERE  MONTH(ProcessedDate) <= @Month
	AND YEAR(ProcessedDate) = @Year 
	AND ClaimLawsuitStatusID IN (15,16,17,19,36,27) 
	AND IsPrimaryForClaim = 1 
	AND IsPrimaryStatus = 1
	AND MatterID  >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
GROUP BY DATENAME("mm", ProcessedDate), MONTH(ProcessedDate)
ORDER BY MONTH(ProcessedDate)


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByMonthByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByMonthByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByMonthByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByMonthByYear] TO [power_user]
GO
