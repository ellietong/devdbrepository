SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryInternalNewComplaintsPast5DayAcceptanceEmail]
AS
BEGIN

/*Query run daily to send an internal email listing any Complaints which are over 5
days old and have not yet been accepted. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (max)


Declare @5DaysOld datetime
SET @5DaysOld = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 5)) + ' 16:00:00.000')



SET @Subject = 'Warren Pumps: Unaccepted Complaints Over 5 Days Old'
--Set body font, insert KCIC logo/banner at the top of the email, and set table styles. 
--These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>The following firms have unaccepted complaints are over 5 days old: </H4>
<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--Create temp table to hold all complaints over 5 days old which have not been accepted
--DocTitle and firmAbb are found by parsing the documentURL
DECLARE @NewComplaintsTable TABLE (docTitle nvarchar (200), FirmAddressID int, lName nvarchar (50), fname nvarchar (200))
INSERT INTO @NewComplaintsTable
	SELECT
		DocumentTitle,
		DefenseCounselAddressID,
		LastName,
		FirstName
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
	WHERE d.DateCreated < @5DaysOld
	AND ClaimantDocumentTypeID = 8
	AND Accepted IS NULL

--Declare variables to be used in the firm cursor to list documents by firm
DECLARE @FirmAddressID nvarchar (10), @firmURL nvarchar (100), @firmname nvarchar (100), @docCount int
SET @firmURL = 'https://admin.kcicextranet.com/clients/wp/cp/Pages/Documents.aspx'
DECLARE firm_Curs CURSOR FOR SELECT
	nct.FirmAddressID,
	dca.DefenseCounsel + ' (' + dca.DefenseCounselCity + ')',
	COUNT(DISTINCT doctitle)
FROM tblDefenseCounselAddress dca
INNER JOIN @NewComplaintsTable nct ON nct.FirmAddressID = dca.DefenseCounselAddressID
GROUP BY	nct.FirmAddressID,
			dca.DefenseCounsel + ' (' + dca.DefenseCounselCity + ')'

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmname, @docCount

WHILE @@FETCH_STATUS = 0

BEGIN
--Header for each firm with link to the document library
SET @BodyMessage = @BodyMessage + '<br>' + CONVERT(nvarchar(5), (SELECT
	@docCount)
) + ' Unaccepted Complaint(s) for <A HREF = ' + (SELECT
	@firmURL)
+ '> ' + (SELECT
	@firmName)
+ '</A>:'
--List documents in a table               
SET @BodyMessage = @bodymessage +
N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
--Set column headings and populate cells from the cursor
N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Last Name</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant First Name</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		docTitle)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		lname)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		fname)
	AS td,
	''
FROM @NewComplaintsTable C
WHERE C.FirmAddressID = (SELECT
	@FirmAddressID)
ORDER BY lname
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'

--Move to next firm                 
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmname, @doccount
END
CLOSE firm_curs
DEALLOCATE firm_curs

SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--Check if the email needs to be sent
IF (SELECT
	COUNT(DISTINCT docTitle)
FROM @NewComplaintsTable) < > 0

BEGIN
--Send email
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'warren@kcic.com',
								--@blind_copy_recipients = 'shockleym@kcic.com',
								--@recipients = 'amanp@kcic.com.test-google-a.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'
END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalNewComplaintsPast5DayAcceptanceEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalNewComplaintsPast5DayAcceptanceEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalNewComplaintsPast5DayAcceptanceEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalNewComplaintsPast5DayAcceptanceEmail] TO [power_user]
GO
