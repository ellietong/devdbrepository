SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] (@DefenseInvoiceID Bigint, @Emailaddress nvarchar(1000), @IsApproved bit = null, @ApprovalNotes nvarchar(4000))
AS

BEGIN
	UPDATE tblDefenseInvoice
	SET IsApproved = @IsApproved,
		PaymentApprovedBy = @EmailAddress,
		DatePaymentApproved = GETDATE(),
		ApprovalNotes = @ApprovalNotes
	WHERE DefenseInvoiceID = @DefenseInvoiceID
END








GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [power_user]
GO
