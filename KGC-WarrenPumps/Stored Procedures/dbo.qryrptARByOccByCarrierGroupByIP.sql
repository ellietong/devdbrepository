SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByOccByCarrierGroupByIP]
(@allocationID int = NULL, @CarrierID INT = NULL, @Solvency varchar (50)= NULL, @CarrierGroupID int = NULL, @InsuranceProgramID INT = NULL, @OccurrenceTypeIDs nvarchar(100) = NULL)
 
As
 
SET NOCOUNT ON
 
--DECLARE @AllocationID int
--SET @AllocationID = 56
--DECLARE @CarrierID int
--SET @CarrierID = NULL
--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = NULL
--DECLARE @Solvency varchar (50)
--SET @Solvency = NULL
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID = NULL
--DECLARE @OccurrenceTypeIDs nvarchar(100)
--SET @OccurrenceTypeIDs = NULL

DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @maxInsuranceProgramID int
DECLARE @strSQL nvarchar (4000)
DECLARE @prmDef nvarchar (1000)
DECLARE @tmpSolvency nvarchar (50)
DECLARE @strOccurrences nvarchar (4000)
DECLARE @carrierGroupInfo nvarchar (50)
DECLARE @maxOccurrenceTypeID int
DECLARE @index int
DECLARE @LimitTypeID int
 
--SET Variables to initial values
SET @carrierGroupInfo = ''
SET @strSQL = ''
SET @strOccurrences = ''
SET @tmpSolvency = ''
SET @prmDef = ''
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
 SELECT @maxCarrierGroupID = MAX(Coalesce(CarrierGroupID,0)) FROM vPolicy
SELECT @maxInsuranceProgramID  = MAX (InsuranceProgramID) FROM vPolicy
SELECT @maxOccurrenceTypeID = MAX(OccurrenceTypeID) FROM vOccurrence
SET @LimitTypeID = (SELECT LimitTypeID FROM tblAllocation WHERE AllocationID = @AllocationID GROUP BY LimitTypeID)
 
--Update Null Strings Set above that aren't resolved in WHERE clause (1)solvency, 2)Occurrence Types, 3)Carrier Group Info)
--      1       Update Solvency Info
IF @Solvency = '<<ALL>>' OR @Solvency IS NULL OR @Solvency = ''
        SET @tmpSolvency = '%'
ELSE
        SET @tmpSolvency = @Solvency
 
---------------------------------------------------------------------------------------
--      2       Setup the Occurrence String.
-- STILL NEED TO UPDATE AND test if @OccurrenceTypeIDs is null, if so, 
--update @strOccurrences to work with that.
--Test if Null, if null, then build a string of all occurrence types
IF (LTRIM(RTRIM(@OccurrenceTypeIDs)) <> '' AND @OccurrenceTypeIDs IS NOT NULL)  
        SET @strOccurrences = @OccurrenceTypeIDs  
ELSE 
        BEGIN
                SET @strOccurrences = '1, '
                SET @index = 2
                
                WHILE @index <= @maxOccurrenceTypeID
                BEGIN
                        SET @strOccurrences = @strOccurrences + CONVERT(nvarchar, @index) + ', '
                        SET @index = @index + 1
                END
                SET @strOccurrences = LEFT(@strOccurrences, LEN(@strOccurrences) - 1)
        END
 
---------------------------------------------------------------------------------------
--      3       Update Carrier Group Info.
SELECT @carrierGroupInfo = CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END 
FROM vPolicy pinfo
 
 
CREATE TABLE #A (AllocationID int, TotalAggregateLimit money, TotalSIR money, TotalAllocation money, PVTotalAllocation money)
INSERT INTO #A
SELECT AllocationID, SUM(AggregateLimit) AS AggregateLimit, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID

CREATE TABLE #CA(AllocationID int, CarrierID int, CarrierAggregateLimit money, CarrierTotalSIR money, CarrierTotalAllocation money, PVCarrierTotalAllocation money)
INSERT INTO #CA
SELECT AllocationID, CarrierID, SUM(AggregateLimit) AS AggregateLimit, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVCarrierTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, CarrierID

CREATE TABLE #CGA (AllocationID int, CarrierGroupID int, CarrierGroupAggregateLimit money, CarrierGroupTotalSIR money, CarrierGroupTotalAllocation money, PVCarrierGroupTotalAllocation money)
INSERT INTO #CGA
SELECT AllocationID, CarrierGroupID, SUM(AggregateLimit) AS AggregateLimit, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVCarrierGroupTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, CarrierGroupID

 
--Build the SQL String
 
SET @strSQL = N'
SELECT o.OccurrenceTypeID,o.TriggerStart1,o.OccurrenceName,pinfo.InsuranceProgramID,pinfo.InsuranceProgram,a.AllocationID,a.AllocationDescription,pinfo.CarrierSolvency,pinfo.CarrierID,pinfo.CarrierName,COALESCE(pinfo.CarrierGroupName,pinfo.CarrierName)AS CarrierGroupName,CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END AS CarrierGroupInfo,
ar.DateToPresentValue,ar.OccurrenceID,ar.TriggerStartDateUsed,ar.DateTime,SUM(ar.Allocation)AS SumOfAllocation,CONVERT(money,SUM(ar.PV))AS SumOfPV,SUM(ar.SIRAllocated)AS SumOfSIRAllocated,ar.TriggerEndDateUsed,YieldCurveDate as PVRateDate,COALESCE(o.Cost1,0)*a.Cost1+COALESCE(o.Cost2,0)*a.Cost2+COALESCE(o.Cost3,0)*a.Cost3+COALESCE(o.Cost4,0)*a.cost4+COALESCE(o.Cost5,0)*a.cost5+COALESCE(o.Cost6,0)*a.cost6+COALESCE(o.Cost7,0)*a.cost7+
COALESCE(o.Cost8,0)*a.cost8+COALESCE(o.Cost9,0)*a.cost9+COALESCE(o.Cost10,0)*a.cost10+COALESCE(o.Cost11,0)*a.cost11+COALESCE(o.Cost12,0)*a.cost12 as CostsConsidered,COALESCE(TotalSIR,0)AS TotalSIR,COALESCE(CarrierTotalSIR,0)AS CarrierTotalSIR,COALESCE(CarrierGroupTotalSIR,CarrierTotalSIR)AS CarrierGroupTotalSIR,
COALESCE(TotalAllocation,0)AS TotalAllocation,COALESCE(CarrierTotalAllocation,0)AS CarrierTotalAllocation,COALESCE(CarrierGroupTotalAllocation,CarrierTotalAllocation)AS CarrierGroupTotalAllocation,
COALESCE(PVTotalAllocation,0)AS PVTotalAllocation,COALESCE(PVCarrierTotalAllocation,0)AS PVCarrierTotalAllocation,COALESCE(PVCarrierGroupTotalAllocation,PVCarrierTotalAllocation)AS PVCarrierGroupTotalAllocation
FROM (SELECT PolicyID,PolicyDateID,CarrierGroupID,CarrierGroupName,CarrierID,CarrierName,InsuranceProgramID,InsuranceProgram,CarrierSolvency
FROM vPolicy GROUP BY PolicyID,PolicyDateID,CarrierGroupID,CarrierGroupName,CarrierID,CarrierName,InsuranceProgramID,InsuranceProgram,CarrierSolvency) pinfo 
INNER JOIN vAllocationResults ar ON pinfo.PolicyDateID=ar.PolicyDateID INNER JOIN vAllocation a ON a.AllocationID=ar.AllocationID INNER JOIN vOccurrence o ON ar.OccurrenceID=o.OccurrenceID
LEFT JOIN tblYieldCurve yc on yc.YieldCurveID=ar.YieldCurveID LEFT JOIN #A at ON ar.AllocationID=at.AllocationID LEFT JOIN #CA cat ON ar.AllocationID=cat.AllocationID AND pinfo.CarrierID=cat.CarrierID
LEFT JOIN #CGA cgat ON ar.AllocationID=cgat.AllocationID AND pinfo.CarrierGroupID=cgat.CarrierGroupID WHERE a.AllocationID=' + CONVERT(nvarchar,@AllocationID) + ' AND
pinfo.CarrierID>=COALESCE('+ CONVERT (nvarchar, @CarrierID) +', 0) AND pinfo.CarrierID<=COALESCE(NULLIF(' + CONVERT (nvarchar, @CarrierID) + ', 0), ' + CONVERT (nvarchar, @MAXCarrierID) + ' ) AND
pinfo.CarrierSolvency LIKE @SolvencyPrm AND '
IF @CarrierGroupID <> 0 
SET @strSQL = @strSQL + 'pinfo.CarrierGroupID=' + CONVERT(nvarchar, @CarrierGroupID) + ' AND '
SET @strSQL = @strSQL + 'pinfo.InsuranceProgramID>=COALESCE('+ CONVERT (nvarchar, @InsuranceProgramID) +', 0) AND 
pinfo.InsuranceProgramID<=COALESCE(NULLIF(' + CONVERT (nvarchar, @InsuranceProgramID) + ', 0), ' + CONVERT (nvarchar, @maxInsuranceProgramID) + ' ) AND
o.OccurrenceTypeID IN (' + CONVERT (nvarchar (4000),@strOccurrences) + ')
GROUP BY o.OccurrenceTypeID,o.TriggerStart1,o.OccurrenceName,pinfo.InsuranceProgramID,pinfo.InsuranceProgram,a.AllocationID,a.AllocationDescription,pinfo.CarrierSolvency,pinfo.CarrierID,pinfo.CarrierName,COALESCE(pinfo.CarrierGroupName,pinfo.CarrierName),CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END,
ar.DateToPresentValue,ar.OccurrenceID,ar.TriggerStartDateUsed,ar.DateTime,ar.TriggerEndDateUsed,YieldCurveDate,COALESCE(o.Cost1,0)*a.Cost1+COALESCE(o.Cost2,0)*a.Cost2+COALESCE(o.Cost3,0)*a.Cost3+COALESCE(o.Cost4,0)*a.cost4+COALESCE(o.Cost5,0)*a.cost5+COALESCE(o.Cost6,0)*a.cost6+COALESCE(o.Cost7,0)*a.cost7+COALESCE(o.Cost8,0)*a.cost8+
COALESCE(o.Cost9,0)*a.cost9+COALESCE(o.Cost10,0)*a.cost10+COALESCE(o.Cost11,0)*a.cost11+COALESCE(o.Cost12,0)*a.cost12,TotalSIR,CarrierTotalSIR,COALESCE(CarrierGroupTotalSIR,CarrierTotalSIR),TotalAllocation,CarrierTotalAllocation,COALESCE(CarrierGroupTotalAllocation,CarrierTotalAllocation),
PVTotalAllocation,PVCarrierTotalAllocation,COALESCE(PVCarrierGroupTotalAllocation,PVCarrierTotalAllocation)'
 
--Declare the Parameters that will be passed to the created SQL string.
SET @prmDef = N'@SolvencyPrm nvarchar (50)'
 
--Run/Execute the SQL Statement Created
EXEC sp_executeSQL @strSQL, @prmDef, @SolvencyPrm = @tmpSolvency

DROP TABLE #A
DROP TABLE #CA
DROP TABLE #CGA

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccByCarrierGroupByIP] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccByCarrierGroupByIP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccByCarrierGroupByIP] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccByCarrierGroupByIP] TO [power_user]
GO
