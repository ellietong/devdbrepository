SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryDocumentTypes]
AS 
    SET NOCOUNT ON

    SELECT  ClaimantDocumentTypeID ,
            ClaimantDocumentType
    FROM    tblClaimantDocumentType
    WHERE   ClaimantDocumentGroupID <> 2
    UNION
    SELECT  NULL ,
            '<< All Document Types >>'
    ORDER BY ClaimantDocumentType ASC
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentTypes] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentTypes] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentTypes] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentTypes] TO [power_user]
GO
