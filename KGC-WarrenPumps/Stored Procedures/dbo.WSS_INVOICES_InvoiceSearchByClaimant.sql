SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROC [dbo].[WSS_INVOICES_InvoiceSearchByClaimant]
(@FirmID int, @VendorFirmID int, @StatusID bigint, @PaidDateStart nvarchar(50), @PaidDateEnd nvarchar(50), @Period nvarchar(50),@emailaddress nvarchar(250))
AS
 
----VARIABLES FOR TESTING----
--DECLARE @FirmID int
--DECLARE @VendorFirmID int
--DECLARE @StatusID bigint
--DECLARE @PaidDateStart nvarchar(50)
--DECLARE @PaidDateEnd nvarchar(50)
--DECLARE @Period nvarchar(50)
--DECLARE @emailaddress nvarchar(250)
--SET @FirmID		   = NULL
--SET @VendorFirmID  = NULL
--SET @StatusID	   = NULL
--SET @PaidDateStart = '1/1/1900'
--SET @PaidDateEnd   = '10/1/2012'
--SET @Period		   = NULL
--SET @emailaddress  = 'Gary.McKenna@spx.com'
 
 
 
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE d.detaildocumenttypeid in (40, 41)'
IF @FirmID			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND d.FirmID = ' + CONVERT(nvarchar(20),@FirmID) END
IF @VendorFirmID	IS NOT NULL BEGIN IF @VendorFirmID = 0 SET @WhereString = @WhereString + ' AND d.VendorFirmID <= 10000' ELSE SET @WhereString = @WhereString + ' AND d.VendorFirmID = ' + CONVERT(nvarchar(20),@VendorFirmID) END
IF @StatusID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND dis.StatusID = ' + CONVERT(nvarchar(20),@StatusID) END
IF @PaidDateStart	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND C.sourcedate >= ''' + CONVERT(nvarchar(20),@PaidDateStart) + '''' END
IF @PaidDateEnd		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND c.sourcedate <= ''' + CONVERT(nvarchar(20),@PaidDateEnd) + '''' END
IF @Period			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.Period LIKE ''%'' + @PeriodParam + ''%''' END
 
--These IF statements determine additional WHERE statements by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @FirmByEmailID int
SET @FirmByEmailID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)
IF ((@firmbyemailid in(188)) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'))
	SET @WhereString = @WhereString
ELSE
	SET @WhereString = @WhereString + ' AND f.firmid= @FirmByEmailID'
 
DECLARE @QueryString nvarchar(max)
SET @QueryString = '
SELECT CAST(d.DocumentID AS varchar(50)) AS DocumentId, 
 		   d.Comments,
			d.DocumentURL, 
			d.DocumentTitle, 
			d.SPDocId, 
			d.CreatedBy, 
			d.DateCreated, 
			d.ModifiedBy, 
			d.DateModified,
			d.Accepted, 
			d.AcceptedBy, 
			d.DateAccepted, 
			d.ApprovedBy, 
			d.DateApproved, 
			f.Firm, 
			di.InvoiceNumber, 
			di.Period,
			di.DatePayable, 
			dicd.fees as feeamount, 
			dicd.costs as disbursementamount, 
			dicd.adjustments as Adjustment,
			di.InvoiceNotes as AdjustmentNotes, 
			dis.Status, 
			'''' AS DefenseInvoiceFirm, 
			c.SourceDate as PaidDate, 
			c.SourceNumber as CheckNumber, 
			d.FirmID, 
			COALESCE(ex.Firm, '''') AS VendorFirm, 
			c.SourceDate, 
			sum(coalesce(dicd.fees,0))+ sum(coalesce(dicd.costs,0))+sum(coalesce(dicd.adjustments,0)) AS InvoiceTotal, 
			dicd.claimantpersonalinfoid,
			lastname+'', ''+firstname as Claimant
	FROM tblDocuments d
		LEFT JOIN tblDefenseInvoice di				ON d.DocumentID = di.DocumentID 
		LEFT JOIN tblDefenseInvoiceStatus dis		ON di.StatusID = dis.StatusID 
		LEFT JOIN vtblFirm f						ON d.FirmID = f.FirmID
		LEFT JOIN vtblFirmsAndVendors ex			ON d.VendorFirmID = ex.FirmID
		LEFT JOIN tblPaymentDefense pd				ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN (select *
				   from tblCheck
				   WHERE fundingsourceID not in (3,4,5)) c	ON pd.CheckID = c.CheckID 
		LEFT JOIN tblDefInvClaimantDetail dicd		ON dicd.DocumentID = d.DocumentID 
		LEFT JOIN tblClaimantPersonalInfo cpi		ON cpi.ClaimantPersonalInfoID = dicd.ClaimantPersonalInfoID'
 +	@WhereString + ' 
	GROUP BY CAST(d.DocumentID AS varchar(50)), 
 		   d.Comments,
			d.DocumentURL, 
			d.DocumentTitle, 
			d.SPDocId, 
			d.CreatedBy, 
			d.DateCreated, 
			d.ModifiedBy, 
			d.DateModified,
			d.Accepted, 
			d.AcceptedBy, 
			d.DateAccepted, 
			d.ApprovedBy, 
			d.DateApproved, 
			f.Firm, 
			di.InvoiceNumber, 
			di.Period,
			di.DatePayable, 
			dicd.fees, 
			dicd.costs, 
			dicd.adjustments,
			di.InvoiceNotes, 
			dis.Status, 
			c.SourceDate, 
			c.SourceNumber, 
			d.FirmID, 
			COALESCE(ex.Firm, ''''), 
			c.SourceDate, 
			dicd.claimantpersonalinfoid,
			lastname+'', ''+firstname
	ORDER BY DateCreated DESC, Accepted ASC' 
	--OPTION (RECOMPILE)'
 
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@PeriodParam nvarchar(max)', @PeriodParam = @Period

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_InvoiceSearchByClaimant] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_InvoiceSearchByClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_InvoiceSearchByClaimant] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_InvoiceSearchByClaimant] TO [power_user]
GO
