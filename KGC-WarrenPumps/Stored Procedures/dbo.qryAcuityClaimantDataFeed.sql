
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryAcuityClaimantDataFeed]
WITH EXECUTE AS CALLER
AS
SET NOCOUNT ON;

PRINT 'NAME|ID|LAW_FIRM_KEY|LAW_FIRM_NAME|STATUS|STATUS_DATE|STATE_KEY|LAWSUIT_ID'

SELECT LastName + ' ' + FirstName AS NAME, 
c.ReferenceID AS ID, 
AcuityKey AS LAW_FIRM_KEY, 
DefenseCounsel AS LAW_FIRM_NAME, 
cls.ClaimLawsuitStatus AS [STATUS], 
CONVERT(DATE, StatusDate) AS STATUS_DATE, 
StateAcuityKey AS STATE_KEY, 
cl.LawsuitID AS LAWSUIT_ID 
FROM tblClaimantPersonalInfo cpi
INNER JOIN tblClaim c	 ON cpi.ClaimantPersonalInfoID=c.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl	 ON cl.ClaimID = c.ClaimID
INNER JOIN tblLawsuit l	 ON l.LawsuitID=cl.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst	ON clst.ClaimLawsuitID=cl.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls	 ON clst.ClaimLawsuitStatusID=cls.ClaimLawsuitStatusID
INNER JOIN tblDefenseCounselAddress dca	 ON dca.DefenseCounselAddressID=l.DefenseCounselAddressID
INNER JOIN tblState s	 ON s.StateID=l.StateID
WHERE (CONVERT(DATE,ProcessedDate) = CONVERT(DATE,GETDATE()) and clst.ClaimLawsuitStatusID=22)
--OR (CONVERT(DATE,ProcessedDate) >='9/23/2013' and (CONVERT(DATE,ProcessedDate) <='10/18/2013' and clst.ClaimLawsuitStatusID=22))
			--OR cpi.ClaimantPersonalInfoID in (89892)
GO

GRANT VIEW DEFINITION ON  [dbo].[qryAcuityClaimantDataFeed] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryAcuityClaimantDataFeed] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryAcuityClaimantDataFeed] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryAcuityClaimantDataFeed] TO [power_user]
GO
