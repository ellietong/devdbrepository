SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDelClaimantDocument]
(@ClaimantPersonalInfoID bigint, @DocumentID int, @Emailaddress nvarchar(250))
AS
BEGIN
	SET NOCOUNT ON;
	
		
	--Update the SPUsername field in tblMultiClaimantDocument prior to delete for audit log purposes
	UPDATE tblMultiClaimantDocument
		SET SPUsername = @emailaddress
	WHERE DocumentID = @DocumentID AND ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
	--Delete the the relevant tblMultiClaimantDocument record
	DELETE FROM tblMultiClaimantDocument
	WHERE DocumentID = @DocumentID
	AND ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
	--update settleDoc record prior to delete for audit log
	UPDATE tblSettleDocs
		SET SPUsername = @Emailaddress
	FROM tblSettleDocs sd
	INNER JOIN tblSettlePaymentDocumentation spd ON sd.SetPymtID = spd.SetPymtID
	INNER JOIN tblClaimLawsuit cl ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND sd.DocumentID = @DocumentID
	
	
	--delete related settledocs record
	DELETE tblSettleDocs FROM tblSettleDocs sd
	INNER JOIN tblSettlePaymentDocumentation spd ON sd.SetPymtID = spd.SetPymtID
	INNER JOIN tblClaimLawsuit cl ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND sd.DocumentID = @DocumentID
	
	--If there was only 1 entry in tblMultiClaimantDocument for the document, delete the tblDocuments record as well
	IF(SELECT COUNT(MultiClaimantDocumentID) FROM tblMultiClaimantDocument WHERE DocumentID = @DocumentID) = 0
	BEGIN
		--Select the file path
		SELECT DocumentURL FROM tblDocuments WHERE DocumentID = @DocumentID
		
		--Update the ModifiedBy field in tblDocuments prior to delete for audit log purposes
		UPDATE tblDocuments
			SET ModifiedBy = @emailaddress
		WHERE DocumentID = @DocumentID
		
		--Finally, do the delete
		DELETE FROM tblDocuments WHERE DocumentID = @DocumentID
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelClaimantDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelClaimantDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelClaimantDocument] TO [power_user]
GO
