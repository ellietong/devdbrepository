SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryUpdSettleDocs]
    (
      @SettleDocID INT ,
      @DocumentID INT ,
      @DocCategoryID INT ,
      @PageNums NVARCHAR(100) ,
	  @DocumentURL nvarchar(500) = NULL ,
	  @AmazonKey nvarchar(max) = NULL ,
	  @IsHistorical bit = NULL ,
      @EmailAddress NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON

--DECLARE @SettlePaymentID int
--DECLARE @EmailAddress nvarchar(100)
--SET @SettlePaymentID = 23
--SET @EmailAddress = 'owenss@kcicllc.com' 

    UPDATE  tblSettleDocs
    SET     DocumentID = @DocumentID ,
            DocCategoryID = @DocCategoryID ,
            PageNums = @PageNums ,
            SPUsername = @EmailAddress
    WHERE   SettleDocID = @SettleDocID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettleDocs] TO [power_user]
GO
