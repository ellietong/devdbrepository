SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE FUNCTION [dbo].[fnSubscriptionDateProportionActual]
	(@PolicyID int, @StartDate datetime, @EndDate datetime)
RETURNS float
AS

---- Declare and set variables for testing purposes
--DECLARE @PolicyID int
--DECLARE @StartDate datetime
--DECLARE @EndDate datetime
--SET @PolicyID = 4
--SET @StartDate = '1/1/1976'
--SET @EndDate = '1/1/1978'

BEGIN
	-- Declare the temporary field used to track the ultimate subscription date proportion
	DECLARE @SubDatePortion float

	-- Declare the temporary fields used to track loop and percentage
	DECLARE @DatePeriods int
	DECLARE @Counter int
	DECLARE @Percentage float
	
	-- Declare standing variables for the policy dates
	DECLARE @SubStartDate date
	DECLARE @SubEndDate date
	DECLARE @PolicyDateID int
	DECLARE @PStartDate date
	DECLARE @PEndDate date
	DECLARE @CompareStart date
	DECLARE @CompareEnd date
	
	-- Cast the subscriber dates as type "date" for comparisons below
	SET @SubStartDate = CAST(@StartDate AS date)
	SET @SubEndDate = CAST(@EndDate AS date)

	-- Declare the temporary tables for policy dates, one to summarize all date IDs and one to track one record at a time
	DECLARE @zPolicyAllDates TABLE (PolicyDateID int,
									PStartDate date, 
									PEndDate date)
	DECLARE @zPolicyOneDates TABLE (PolicyDateID int,
									PStartDate date, 
									PEndDate date)
								    
	-- Populate the temporary table for all policy date IDs
	INSERT INTO @zPolicyAllDates
		SELECT PolicyDateID, CAST(ActualPStartDate AS date) AS PStartDate, CAST(ActualPEndDate AS date) AS PEndDate
		FROM tblPolicyDates
		WHERE PolicyID = @PolicyID
	
	-- Prepare the temporary fields for the loop
	SET @SubDatePortion = 0.000
	SET @DatePeriods = (SELECT COUNT(PolicyDateID) FROM @zPolicyAllDates)
	SET @Counter = 0
	SET @Percentage = 0.000
	
	-- Loop through the temporary table of policy date records, one records at a time
	WHILE @Counter < @DatePeriods
	BEGIN
		-- Grab a single set of policy dates from the temporary table
		INSERT INTO @zPolicyOneDates
			SELECT TOP 1 * 
			FROM @zPolicyAllDates
			
		-- Parse out the information from the record
		SET @PolicyDateID = (SELECT PolicyDateID FROM @zPolicyOneDates)
		SET @PStartDate = (SELECT CAST(PStartDate AS date) FROM @zPolicyOneDates)
		SET @PEndDate = (SELECT CAST(PEndDate AS date) FROM @zPolicyOneDates)
		
		-- Check if the actual policy dates fall fully with the subscription dates
		IF ((COALESCE(@SubStartDate, @PStartDate) <= @PStartDate) AND (COALESCE(@SubEndDate, @PEndDate) >= @PEndDate)) BEGIN
			-- If so, set the percentage for this date record to 100%
			SET @Percentage = 1.000
		END
		-- Check if the subscription dates are totally outside the actual policy dates
		ELSE IF ((COALESCE(@SubStartDate, @PStartDate) > @PEndDate) OR (COALESCE(@SubEndDate, @PEndDate) < @PStartDate)) BEGIN
			-- If so, set the percentage for this date record to 0% (this should be redundant to the default value)
			SET @Percentage = 0.000
		END
		-- Otherwise, proportion the percentage based on the number of subscription days falling within actual policy dates
		ELSE BEGIN
			IF COALESCE(@SubStartDate, @PStartDate) > @PStartDate BEGIN SET @CompareStart = @SubStartDate END
				ELSE											  BEGIN SET @CompareStart = @PStartDate   END
			IF COALESCE(@SubEndDate,   @PEndDate)   < @PEndDate   BEGIN SET @CompareEnd   = @SubEndDate   END
				ELSE											  BEGIN SET @CompareEnd   = @PEndDate     END
			SET @Percentage = ((CAST(DATEDIFF(day, @CompareStart, @CompareEnd) AS float)) / 
							   (CAST(DATEDIFF(day, @PStartDate,   @PEndDate)   AS float)))
		END
		
		-- Update the subscription date proportion to include this date period's percentage (this is proportioned later)
		SET @SubDatePortion = @SubDatePortion + @Percentage
		
		-- Clear the reviewed record from the temporary table being used to track all date records
		DELETE FROM @zPolicyAllDates WHERE PolicyDateID = @PolicyDateID
		
		-- Clear the reviewed record from the temporary table used to track a single date record
		DELETE FROM @zPolicyOneDates
		
		-- Increment the counter and reset the temporary percentage field
		
		SET @Counter = @Counter + 1
		SET @Percentage = 0.000
	END
	
	-- Proportion the summed subscription date proportion by the number of date periods
	SET @SubDatePortion = @SubDatePortion / @DatePeriods
	
	RETURN @SubDatePortion

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fnSubscriptionDateProportionActual] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnSubscriptionDateProportionActual] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnSubscriptionDateProportionActual] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnSubscriptionDateProportionActual] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[fnSubscriptionDateProportionActual] TO [Subscription_User]
GRANT EXECUTE ON  [dbo].[fnSubscriptionDateProportionActual] TO [Subscription_User]
GO
