CREATE ROLE [Occurrence_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Occurrence_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'Occurrence_User', N'cornellt'
GO
EXEC sp_addrolemember N'Occurrence_User', N'gutzlern'
GO
EXEC sp_addrolemember N'Occurrence_User', N'kanga'
GO
EXEC sp_addrolemember N'Occurrence_User', N'khanm'
GO
EXEC sp_addrolemember N'Occurrence_User', N'labellj'
GO
EXEC sp_addrolemember N'Occurrence_User', N'potterm'
GO
EXEC sp_addrolemember N'Occurrence_User', N'prossc'
GO
EXEC sp_addrolemember N'Occurrence_User', N'shockleym'
GO
