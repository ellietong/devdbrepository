CREATE ROLE [Billing_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Billing_User', N'blairo'
GO
EXEC sp_addrolemember N'Billing_User', N'gutzlern'
GO
EXEC sp_addrolemember N'Billing_User', N'labellj'
GO
EXEC sp_addrolemember N'Billing_User', N'potterm'
GO
EXEC sp_addrolemember N'Billing_User', N'shockleym'
GO
