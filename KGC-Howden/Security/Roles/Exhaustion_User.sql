CREATE ROLE [Exhaustion_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Exhaustion_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'Exhaustion_User', N'kanga'
GO
EXEC sp_addrolemember N'Exhaustion_User', N'khanm'
GO
EXEC sp_addrolemember N'Exhaustion_User', N'labellj'
GO
EXEC sp_addrolemember N'Exhaustion_User', N'prossc'
GO
EXEC sp_addrolemember N'Exhaustion_User', N'shockleym'
GO
