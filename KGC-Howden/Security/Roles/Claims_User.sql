CREATE ROLE [Claims_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Claims_User', N'casillaw'
GO
EXEC sp_addrolemember N'Claims_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'Claims_User', N'colek'
GO
EXEC sp_addrolemember N'Claims_User', N'cornellt'
GO
EXEC sp_addrolemember N'Claims_User', N'eastlacka'
GO
EXEC sp_addrolemember N'Claims_User', N'hughesr'
GO
EXEC sp_addrolemember N'Claims_User', N'husainm'
GO
EXEC sp_addrolemember N'Claims_User', N'kanga'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\carthyl'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\hashemik'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\jacobsb'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\kennedyl'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\lislev'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\marshc'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\ohaverk'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\pikeg'
GO
EXEC sp_addrolemember N'Claims_User', N'khanm'
GO
EXEC sp_addrolemember N'Claims_User', N'labellj'
GO
EXEC sp_addrolemember N'Claims_User', N'madlp'
GO
EXEC sp_addrolemember N'Claims_User', N'mollardj'
GO
EXEC sp_addrolemember N'Claims_User', N'monahanc'
GO
EXEC sp_addrolemember N'Claims_User', N'onyiukeo'
GO
EXEC sp_addrolemember N'Claims_User', N'potterm'
GO
EXEC sp_addrolemember N'Claims_User', N'prossc'
GO
EXEC sp_addrolemember N'Claims_User', N'shockleym'
GO
EXEC sp_addrolemember N'Claims_User', N'zongc'
GO
