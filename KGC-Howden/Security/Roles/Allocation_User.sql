CREATE ROLE [Allocation_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Allocation_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'Allocation_User', N'cornellt'
GO
EXEC sp_addrolemember N'Allocation_User', N'gutzlern'
GO
EXEC sp_addrolemember N'Allocation_User', N'kanga'
GO
EXEC sp_addrolemember N'Allocation_User', N'khanm'
GO
EXEC sp_addrolemember N'Allocation_User', N'labellj'
GO
EXEC sp_addrolemember N'Allocation_User', N'potterm'
GO
EXEC sp_addrolemember N'Allocation_User', N'prossc'
GO
EXEC sp_addrolemember N'Allocation_User', N'shockleym'
GO
