CREATE TABLE [dbo].[tblVendor]
(
[VendorID] [int] NOT NULL IDENTITY(1, 1),
[VendorName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcuityKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID_Payments] [int] NULL,
[VendorType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[ZipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUserName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACHRoutingNumber] [nchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACHAccountNumber] [nchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACHRemittanceEmail] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblVendor] ADD CONSTRAINT [PK_mEtblVendor] PRIMARY KEY CLUSTERED  ([VendorID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblVendor] TO [base_user]
GRANT SELECT ON  [dbo].[tblVendor] TO [base_user]
GRANT SELECT ON  [dbo].[tblVendor] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblVendor] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblVendor] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblVendor] TO [Enviro_User]
GRANT VIEW DEFINITION ON  [dbo].[tblVendor] TO [power_user]
GRANT SELECT ON  [dbo].[tblVendor] TO [power_user]
GRANT INSERT ON  [dbo].[tblVendor] TO [power_user]
GRANT UPDATE ON  [dbo].[tblVendor] TO [power_user]
GO
