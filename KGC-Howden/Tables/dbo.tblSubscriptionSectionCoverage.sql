CREATE TABLE [dbo].[tblSubscriptionSectionCoverage]
(
[SubscriptionSectionCoverageID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NOT NULL,
[SubscriptionSectionID] [int] NOT NULL,
[SubscriptionSectionPercentage] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriptionSectionCoverage] ADD CONSTRAINT [PK_tblSubscriptionSectionCoverage] PRIMARY KEY CLUSTERED  ([SubscriptionSectionCoverageID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionSectionCoverage] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriptionSectionCoverage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionSectionCoverage] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblSubscriptionSectionCoverage] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionSectionCoverage] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriptionSectionCoverage] TO [power_user]
GRANT INSERT ON  [dbo].[tblSubscriptionSectionCoverage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSubscriptionSectionCoverage] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionSectionCoverage] TO [Subscription_User]
GRANT SELECT ON  [dbo].[tblSubscriptionSectionCoverage] TO [Subscription_User]
GRANT INSERT ON  [dbo].[tblSubscriptionSectionCoverage] TO [Subscription_User]
GRANT DELETE ON  [dbo].[tblSubscriptionSectionCoverage] TO [Subscription_User]
GRANT UPDATE ON  [dbo].[tblSubscriptionSectionCoverage] TO [Subscription_User]
GO
