CREATE TABLE [dbo].[m_tblVendorFirm]
(
[VendorFirmID] [int] NOT NULL IDENTITY(1, 1),
[VendorFirm] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[ZipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblVendorFirm] ADD CONSTRAINT [PK_m_tblVendorFirm] PRIMARY KEY CLUSTERED  ([VendorFirmID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblVendorFirm] ADD CONSTRAINT [FK_m_tblVendorFirm_m_tblVendorFirm] FOREIGN KEY ([VendorFirmID]) REFERENCES [dbo].[m_tblVendorFirm] ([VendorFirmID])
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblVendorFirm] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblVendorFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblVendorFirm] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblVendorFirm] TO [power_user]
GO
