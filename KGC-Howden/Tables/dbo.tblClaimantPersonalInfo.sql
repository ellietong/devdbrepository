CREATE TABLE [dbo].[tblClaimantPersonalInfo]
(
[ClaimantPersonalInfoID] [bigint] NOT NULL IDENTITY(1, 1),
[ReferenceID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialSecurityNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InternationalNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeceasedDate] [datetime] NULL,
[MaritalStatusID] [int] NULL,
[GenderID] [int] NULL,
[BirthDate] [datetime] NULL,
[SuffixID] [int] NULL,
[HealthInsuranceClaimNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicareBeneficiaryStatusID] [int] NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[Zip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [bigint] NULL,
[Extension] [int] NULL,
[CMSQueryDate] [datetime] NULL,
[IsSmoker] [bit] NULL,
[SmokerStart] [datetime] NULL,
[SmokerEnd] [datetime] NULL,
[PacksPerDay] [float] NULL,
[Suffix_PACE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State_PACE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REFERENCE_ID] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_ID] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_KEY] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE TRIGGER [dbo].[del_DocketNumbers]
   ON  [dbo].[tblClaimantPersonalInfo]
   AFTER DELETE
AS 
BEGIN
      SET NOCOUNT ON;
      DELETE FROM tblClaimantDocketNumbers WHERE ClaimantPersonalInfoID IN (SELECT ClaimantPersonalInfoID FROM deleted)
      
       
    -- Insert statements for trigger here

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE TRIGGER [dbo].[ins_DocketNumbers]
   ON  [dbo].[tblClaimantPersonalInfo]
   AFTER INSERT
AS 
BEGIN
      SET NOCOUNT ON;
      
            INSERT INTO tblClaimantDocketNumbers
      SELECT DISTINCT ClaimantPersonalInfoID, '' as DocketNumbers FROM inserted
       

END
GO
ALTER TABLE [dbo].[tblClaimantPersonalInfo] ADD CONSTRAINT [PK__tblClaimantPerso__451F3D2B] PRIMARY KEY CLUSTERED  ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantPersonalInfo] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantPersonalInfo] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantPersonalInfo] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantPersonalInfo] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantPersonalInfo] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantPersonalInfo] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantPersonalInfo] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantPersonalInfo] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantPersonalInfo] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantPersonalInfo] TO [power_user]
GO
