SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryOccupationInfo] (@ClaimantPersonalInfoID int, 
														  @emailaddress			  nvarchar(250))
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantPersonalInfoID int
--DECLARE @emailaddress nvarchar(250)
--SET @ClaimantPersonalInfoID = 251129
--SET @emailaddress = 'cgong@cetcap.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR (SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1
BEGIN
	SELECT ce.ClaimantEmploymentID, 
		COALESCE(o.OccupationID, NULL) AS OccupationID, 
		o.Occupation, 
		Jobsite, 
		EmpStartDate, 
		EmpEndDate, 
		cpi.FirstName + ' ' + cpi.LastName AS Claimant,
		JobsiteCity, 
		JobsiteStateID,
		s.State AS JobsiteState, 
		IsPrimary AS IsPrimaryJobsite,
		County,
		ce.CountyID
	FROM tblClaimantEmployment ce 
		INNER JOIN tblClaimantPersonalInfo cpi	ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN  tblOccupation o				ON o.OccupationID = ce.OccupationID
		LEFT JOIN  tblClaim c					ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN  tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
		LEFT JOIN  tblLawsuit l					ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN  tblState s					ON ce.JobsiteStateID = s.StateID
		LEFT JOIN  tblCounty cy					ON cy.CountyID = ce.CountyID
	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	GROUP BY ce.ClaimantEmploymentID, 
		COALESCE(o.OccupationID, NULL), 
		o.Occupation, 
		Jobsite, 
		EmpStartDate, 
		EmpEndDate, 
		cpi.FirstName + ' ' + cpi.LastName,
		JobsiteCity, 
		JobsiteStateID,
		s.State, 
		IsPrimary,
		County,
		ce.CountyID
	ORDER BY IsPrimary DESC
END
ELSE
BEGIN
	SELECT DISTINCT ce.ClaimantEmploymentID, 
		COALESCE(o.OccupationID, NULL) AS OccupationID, 
		o.Occupation, 
		Jobsite, 
		EmpStartDate, 
		EmpEndDate, 
		cpi.FirstName + ' ' + cpi.LastName AS Claimant,
		JobsiteCity, 
		JobsiteStateID, 
		s.State AS JobsiteState, 
		IsPrimary AS IsPrimaryJobsite,
		County,
		ce.CountyID
	FROM tblClaimantEmployment ce 
		INNER JOIN tblClaimantPersonalInfo cpi	ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN  tblOccupation o				ON o.OccupationID = ce.OccupationID
		LEFT JOIN  tblClaim c					ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN  tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
		LEFT JOIN  tblLawsuit l					ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN  tblState s					ON ce.JobsiteStateID = s.StateID
		LEFT JOIN  tblCounty cy					ON cy.CountyID = ce.CountyID
		INNER JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID=dca.DefenseCounselAddressID
		INNER JOIN tblDefenseJurisdiction dj	ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID

	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
		AND (dj.DefenseCounselAddressID  = @DefenseCounselAddressID 
			OR l.DefenseCounselAddressID = @DefenseCounselAddressID)
	GROUP BY ce.ClaimantEmploymentID, 
		COALESCE(o.OccupationID, NULL), 
		o.Occupation, 
		Jobsite, 
		EmpStartDate, 
		EmpEndDate, 
		cpi.FirstName + ' ' + cpi.LastName,
		JobsiteCity, 
		JobsiteStateID,
		s.State, 
		IsPrimary,
		County		,
		ce.CountyID								
	ORDER BY IsPrimary DESC
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryOccupationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryOccupationInfo] TO [power_user]
GO
