SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[qryfrmCorrespondenceAddressComboBox]
as
set nocount on
select a.addressid, c.contactlname, coalesce(c.contactfname + ' ','') + coalesce(c.contactlname + '   ', '') + coalesce(addressline1 + '   ', '') + COALESCE( addressline2+ '  ','')   + coalesce(city + ', ', '') + coalesce(state, '') as contactmerge
from tbladdress a 
	left join tblcorrespondenceaddress ca on ca.addressid = a.addressid
	left join tblcontact c on c.contactid = a.contactid
group by a.addressid, c.contactlname, coalesce(c.contactfname + ' ','') + coalesce(c.contactlname + '   ', '') + coalesce(addressline1 + '   ', '') + COALESCE( addressline2+ '  ','')   + coalesce(city + ', ', '') + coalesce(state, '')
order by c.contactlname
return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorrespondenceAddressComboBox] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorrespondenceAddressComboBox] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorrespondenceAddressComboBox] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorrespondenceAddressComboBox] TO [power_user]
GO
