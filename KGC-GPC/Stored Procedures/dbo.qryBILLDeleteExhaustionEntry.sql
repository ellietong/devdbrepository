SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryBILLDeleteExhaustionEntry]
(@exhaustionDateID int)
AS

SET NOCOUNT ON

DELETE
FROM tblExhaustion
WHERE ExhaustionDateID = @exhaustionDateID

DELETE
FROM tblExhaustionDate
WHERE ExhaustionDateID = @exhaustionDateID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryBILLDeleteExhaustionEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryBILLDeleteExhaustionEntry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryBILLDeleteExhaustionEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryBILLDeleteExhaustionEntry] TO [power_user]
GO
