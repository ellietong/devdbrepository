SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimLawsuitStatusTrackingQC] (@ClaimLawsuitID int)
as

Select 
cl.ClaimLawsuitID,
clst.StatusDate,
clst.StatusNotes,
cls.ClaimLawsuitStatus,
IsPrimaryStatus, 
ProcessedDate,
rt.ReleaseType

From tblClaimLawsuit cl
	Left Join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=cl.ClaimLawsuitID
	Left Join tblClaimLawsuitStatus cls on cls.claimlawsuitstatusid = clst.claimlawsuitstatusID
	left join tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblReleaseType rt on rt.ReleaseTypeID = spd.ReleaseTypeID
Where cl.ClaimLawsuitID = @ClaimLawsuitID
Order By IsPrimaryStatus desc, StatusDate desc

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitStatusTrackingQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitStatusTrackingQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitStatusTrackingQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitStatusTrackingQC] TO [power_user]
GO
