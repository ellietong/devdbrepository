SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[WSS_Upload_qryCheckExposure](@ClaimantPersonalInfoID bigint=null, @ExposureCoded int output)
AS
SET NOCOUNT ON

--DECLARE @ClaimantPersonalInfoID bigint = 11075
--DECLARE @ExposureCoded int = 0

SELECT
	@ExposureCoded = (SELECT COALESCE((SELECT
		CASE
			WHEN JobSiteStateID IS NULL THEN 0
			--WHEN Jobsite LIKE 'Various Jobsites' THEN 0
			--WHEN Jobsite LIKE 'UNKNOWN' THEN 0 
			ELSE 1
		END AS ExposureCoded
	FROM tblClaimantEmployment
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND IsPrimary = 1
	GROUP BY	ClaimantPersonalInfoID,
				JobSiteStateID), 0))

RETURN @ExposureCoded

RETURN
GO
