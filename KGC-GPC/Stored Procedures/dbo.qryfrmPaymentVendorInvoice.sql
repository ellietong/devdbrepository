SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmPaymentVendorInvoice] (@VendorFirmID int)

As
Set NoCount On

select di.DefenseInvoiceID, VendorFirmID, MatterID, InvoiceNumber, Period, InvoiceTotal, InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount
from tblDefenseInvoice di
left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
			 a on a.DefenseInvoiceID = di.DefenseInvoiceID
where VendorFirmID = 8 --and FundsRequestDate is not null
Group By di.DefenseInvoiceID, VendorFirmID, InvoiceNumber, Period, InvoiceTotal, MatterID
Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [power_user]
GO
