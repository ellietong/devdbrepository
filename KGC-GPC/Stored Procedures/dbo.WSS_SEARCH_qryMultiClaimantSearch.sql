SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SEARCH_qryMultiClaimantSearch] (@ClaimantName				nvarchar(max), 
														   @SSN							nvarchar(20), 
														   @StateID						int, 
														   @PlaintiffCounselAddressID	int, 
														   @IsMDL						int,
														   @ReferenceNumber				nvarchar(4000) = NULL, 
														   @emailaddress				nvarchar(250), 
														   @DocketNumber				nvarchar(250), 
														   @ClaimLawsuitStatusID		int,
														   @DefendantCompanyID			int = NULL,
														   @MatterID					int,
														   @MaterialID					int,
														   @ClaimantIDs					integer_list_tbltype READONLY)
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantName              nvarchar(max)
--DECLARE @SSN                       nvarchar(20)
--DECLARE @StateID                   int
--DECLARE @PlaintiffCounselAddressID int
--DECLARE @NavReferenceID               bigint
--DECLARE @MatterID                  int
--DECLARE @IsMDL                     int
--DECLARE @emailaddress              nvarchar(250)
--DECLARE @DocketNumber              nvarchar(250)
--DECLARE @ClaimLawsuitStatusGroupID int
--DECLARE @ClaimantIDs               TABLE (n int) 
--SET @ClaimantName              = NULL
--SET @SSN                       = NULL
--SET @StateID                   = NULL
--SET @PlaintiffCounselAddressID = NULL
--SET @NavReferenceID               = NULL
--SET @MatterID                  = NULL
--SET @IsMDL                     = NULL
--SET @emailaddress              = 'youngc@kcicllc.com'
--SET @DocketNumber              = NULL
--SET @ClaimLawsuitStatusGroupID = NULL

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
IF object_id('tempdb..#ClaimantIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimantIDs
END
CREATE TABLE #ClaimantIDs (n int)
INSERT INTO #ClaimantIDs
	SELECT n FROM @ClaimantIDs
	
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE 1=1'
IF @ClaimantName			  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND ((CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' end) like ''%'' + @ClaimantNameParam + ''%'')' END
IF @SSN						  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(socialsecurityNumber, '''') like ''%'' + @SSNParam + ''%''' END
IF @MatterID				  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar(20),@MatterID) END
IF @MaterialID				  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND a.MaterialID = ' + CONVERT(nvarchar(20),@MaterialID) END
IF @StateID					  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND l.StateID = ' + CONVERT(nvarchar(20), @StateID) END
IF @PlaintiffCounselAddressID IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(l.PlaintiffCounselAddressID,0) = ' + CONVERT(nvarchar(20),@PlaintiffCounselAddressID) END
IF @ReferenceNumber			  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(cpi.Reference_ID, '''') LIKE ''%'' + @ReferenceNumberParam + ''%''' END
IF @IsMDL					  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(IsMDL, 0) = ' + CONVERT(nvarchar(20),@IsMDL) END
IF @DocketNumber			  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND DocketNumber LIKE ''%'' + @DocketNumberParam + ''%''' END
IF @ClaimLawsuitStatusID	  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND cls.ClaimLawsuitStatusID = ' + CONVERT(nvarchar(20),@ClaimLawsuitStatusID) END
IF @DefendantCompanyID		  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND a.DefendantCompanyID = ' + CONVERT(nvarchar(20),@DefendantCompanyID) END
IF (SELECT COUNT(n) FROM #ClaimantIDs) <> 0 BEGIN SET @WhereString = @WhereString + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)' END

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

-- Commented out because Whyte can't see Wisconsin complaints with this incorrect code in place
--IF @DefenseCounselAddressID = 494
--	SET @WhereString = @WhereString + ' AND l.LawsuitID IN(SELECT LawsuitID FROM tblClaimLawsuit WHERE IsMDL = 1) '

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT CPI.ClaimantPersonalInfoID AS ClaimantID, 
		CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '' '' ELSE MiddleName + '' '' END AS ClaimantName, 
		CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
		BirthDate, 
		Coalesce(PlaintiffCounsel, ''Unknown'') + '' ('' + Coalesce(pca.PlaintiffCounselCity, ''Unknown'') + '')'' As PlaintiffCounsel,
		s.[State], 
		IsMDL, 
		ClaimLawsuitStatus, 
		DocketNumber,
		cl.ClaimLawsuitID,
		Reference_ID AS ReferenceNumber,
		Matter,
		HighLevelDisease
	 FROM dbo.tblClaimantPersonalInfo cpi
		INNER JOIN dbo.tblClaim c				 ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl			 ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l					 ON cl.LawsuitID = l.LawsuitID ' + 
		@JoinString + '
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		LEFT JOIN tblState s					 ON l.StateID = s.StateID
		LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls		 ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN tblClaimLawsuitStatusGroup clsg	   ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID
		LEFT JOIN tblAllegation a on cl.ClaimLawsuitID = a.ClaimLawsuitID
		LEFT JOIN (select * 
			   from tblClaimDisease 
			   where PrimaryDisease = 1) cd	ON cd.ClaimID = c.ClaimID
		LEFT JOIN tblDisease d					ON d.DiseaseID = cd.DiseaseID
		LEFT JOIN tblHighLevelDisease hd		ON hd.HighLevelDiseaseID = d.HighLevelDiseaseID 
		LEFT JOIN tblMatter m					 ON c.MatterID = m.MatterID
		LEFT JOIN tblDefendantCompany dc on a.DefendantCompanyID = dc.DefendantCompanyID' + 
	 @WhereString + '
	 ORDER BY ClaimantName 
	 OPTION (RECOMPILE)'

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@ClaimantNameParam nvarchar(max), @SSNParam nvarchar(max), @ReferenceNumberParam nvarchar(max), @DocketNumberParam nvarchar(max)', @ClaimantNameParam = @ClaimantName, @SSNParam = @SSN, @ReferenceNumberParam = @ReferenceNumber, @DocketNumberParam = @DocketNumber

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [power_user]
GO
