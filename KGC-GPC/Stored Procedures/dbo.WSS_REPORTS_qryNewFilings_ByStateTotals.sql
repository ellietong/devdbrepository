SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_REPORTS_qryNewFilings_ByStateTotals] (@StartDate datetime, @ENDDate datetime,@MatterID int)

AS
SET NOCOUNT ON

--DECLARE @MatterID int
--set @MatterID = null
--DECLARE @startdate datetime
--set @startdate = '1/1/1900'
--DECLARE @ENDdate datetime
--set @ENDdate = '1/1/2012'


----Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @DiseASeCount Table (ClaimID int, Matter nvarchar(100), MatterID int, YearFiled nvarchar(50), MoYearFiled nvarchar(50), MonthID int, MesoCount int, LungCancerCount int, 
OtherCancerCount int, NonMaligCount int,  UnknownCount int, unknowncount1 int, [state] nvarchar(100), stateid int)
INSERT INTO @DiseASeCount

SELECT  C.ClaimID, Matter, C.MatterID, Coalesce(Year(servicedate), Year(fileDate), Year(statusdate)) AS YearFiled, 
		COALESCE(DateName(Month,servicedate), DateName(Month,filedate), DateName(Month,statusdate)) AS MonthFiled,
		COALESCE(Month(servicedate), Month(filedate), Month(statusdate)) AS MonthID,
		--Meso
		CASE WHEN COALESCE(DMeso.HighLevelDiseASeID, 0) = 1 
			 THEN 1
			 ELSE 0 
		END AS MesoCount,
		--Lung Cancer
		CASE WHEN COALESCE(DLC.HighLevelDiseASeID, 0) = 2 
			 THEN 1
			 ELSE 0 
		END AS LungCancerCount,
		--Other Cancer
		CASE WHEN COALESCE(DOC.HighLevelDiseASeID, 0) = 3 
			 THEN 1
			 ELSE 0 
		END AS OtherCancerCount,
		--NonMalig
		CASE WHEN COALESCE(DNM.HighLevelDiseASeID, 0) IN (4, 7) 
			 THEN 1
			 ELSE 0 
		END AS NonMaligCount,
		--Silicosis
		--CASE	WHEN COALESCE(DNM.HighLevelDiseASeID, 0) = 7 THEN 1
		--ELSE 0 END AS Silicosis,
		--Unknown
		CASE WHEN COALESCE(DUnk.HighLevelDiseASeID, 0) in (5,6) 
			 THEN 1
			 ELSE 0 
		END AS UnknownCount,
		CASE WHEN COALESCE(DUnk.HighLevelDiseASeID, 0) IS NULL 
			 THEN 1
			 ELSE 0 
		END AS UnknownCount1, FullNameState,
		l.StateID

FROM tblClaimantPersonalInfo CPI
	LEFT JOIN tblClaim C ON CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
	LEFT JOIN (SELECT CD.ClaimDiseASeID,CD.ClaimID, CD.DiseASeDiagnosisDate, CD.DiseASeID, CD.DoctorID, CD.PrimaryDiseASe FROM tblClaimDiseASe CD WHERE PrimaryDiseASe = 1) CD ON CD.ClaimID=C.ClaimID
	LEFT JOIN tblMatter M ON c.MatterID = M.MatterID
	LEFT JOIN tblClaimSequence csq ON c.ClaimSequenceID = csq.ClaimSequenceID
	--Meso
	LEFT JOIN tblDiseASe DMeso ON CD.DiseASeID=DMeso.DiseASeID
	--Lung Cancer
	LEFT JOIN tblDiseASe DLC ON CD.DiseASeID=DLC.DiseASeID
	--Other Cancer
	LEFT JOIN tblDiseASe DOC ON CD.DiseASeID=DOC.DiseASeID
	--Non-Malig
	LEFT JOIN tblDiseASe DNM ON CD.DiseASeID=DNM.DiseASeID
	--Unknown
	LEFT JOIN tblDiseASe DUnk ON CD.DiseASeID=DUnk.DiseASeID
	LEFT JOIN tblClaimLawsuit CL ON C.ClaimID=CL.ClaimID
	LEFT JOIN tblLawsuit L ON CL.LawsuitID=L.LawsuitID
	LEFT JOIN tblJurisdictiON J ON L.JurisdictionID=J.JurisdictionID
	LEFT JOIN tblState s ON l.StateID = s.StateID
	LEFT JOIN tblClaimLawsuitStatusTracking cst ON cst.ClaimLawsuitID = cl.ClaimLawsuitID
WHERE IsPrimaryForClaim = 1 
	AND C.MatterID >= COALESCE(@MatterID,0) 
	AND C.MatterID <= COALESCE(NULLIF(@MatterID,0),@maxMatterID) 
	AND processeddate BETWEEN COALESCE(@StartDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) 
	AND COALESCE(@ENDDate, GETDATE())
	AND ClaimLawsuitStatusID = 22
ORDER BY COALESCE(YEAR(serviceDate), YEAR(fileDate), YEAR(StatusDate))


SELECT  COUNT(ClaimID) AS NewFilings, @StartDate AS StartDate, @ENDDate AS EndDate,
		SUM(MesoCount) AS MesoFilings, SUM(LungCancerCount) AS LCFilings, SUM(OtherCancerCount) AS OCFilings,
		SUM(NonMaligCount) AS NMFILINGS,  SUM(UnknownCount+unknowncount1) AS UnknownFilings, [state], stateid,
		SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + Unknowncount1) AS TotalCount
FROM @DiseASeCount DC
GROUP BY  [state], stateid
ORDER BY [State]

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilings_ByStateTotals] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilings_ByStateTotals] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilings_ByStateTotals] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilings_ByStateTotals] TO [power_user]
GO
