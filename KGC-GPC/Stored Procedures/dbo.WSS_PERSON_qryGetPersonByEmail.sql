SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_PERSON_qryGetPersonByEmail]
    (
      @emailaddress NVARCHAR(1000)
    )
AS -- Declare the temporary fields used to track firm and email domain
    DECLARE @PersonID INT


        BEGIN
            SET @PersonID = (SELECT DISTINCT
                                    PersonID
                            FROM    vtblPerson
                            WHERE   Email LIKE '%' + @emailaddress + '%'
                          )
        END

--Return the @PersonID
    SELECT  @PersonID AS PersonID


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetPersonByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetPersonByEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetPersonByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetPersonByEmail] TO [power_user]
GO
