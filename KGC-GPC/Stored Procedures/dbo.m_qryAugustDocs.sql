SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_qryAugustDocs]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DocumentID AS DocumentID, DocumentURL, DocumentTitle, d.DetailDocumentTypeID, ClaimantDocumentType,
	'' AS DefenseCounsel, '' as DefenseCounselID
	FROM tblDocuments d
	INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID AND SPDocId IS NOT NULL AND Comments = 'Historical document provided by PACE - August'
END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_qryAugustDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qryAugustDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_qryAugustDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_qryAugustDocs] TO [power_user]
GO
