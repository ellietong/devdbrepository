SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptDetailDefenseBill_ACE]
(@CarrierName Nvarchar(50), @InvoiceNumber Nvarchar(50))
As
Set Nocount on

--declare @CarrierName Nvarchar(50)
--declare @InvoiceNumber Nvarchar(50)

--set @CarrierName= 'ACE'
--set @InvoiceNumber = 'GPC20120831'



Declare @Reference_IDs table (claimantpersonalinfoid bigint, reference_id_parse nvarchar(500))
insert @Reference_IDs

---this is parsing out all claimants with only 1 referenceID and also the left referenceID if a claimant has >1
select claimantpersonalinfoid, CASE WHEN REFERENCE_ID like '%,%' then (left(REFERENCE_ID,(PATINDEX('%,%',REFERENCE_ID))-1))
			WHEN REFERENCE_ID is NULL then '10000000' 
			else REFERENCE_ID  end as REFERENCE_ID_Parse
from tblClaimantPersonalInfo
UNION ALL
---this is parsing out right referenceID if a claimant has 2 reference IDs and middle reference ID when claimant has 3
select claimantpersonalinfoid, 
CASE WHEN len(REFERENCE_ID) - len(replace(REFERENCE_ID,',','')) = 1 then (right(REFERENCE_ID,(PATINDEX('%,%',REFERENCE_ID))-1))
	WHEN len(REFERENCE_ID) - len(replace(REFERENCE_ID,',','')) = 2 then SUBSTRING(REFERENCE_ID, CHARINDEX(',', REFERENCE_ID) +2  ,   LEN(REFERENCE_ID) - CHARINDEX(',', REFERENCE_ID) - CHARINDEX(',', REVERSE(REFERENCE_ID))-1) 		
	WHEN REFERENCE_ID is NULL then '10000000'
			else '10000000'  end as REFERENCE_ID_Parse
from tblClaimantPersonalInfo
UNION ALL
---this is parsing out right referenceID if claimant has 3 reference IDs
select claimantpersonalinfoid, 
CASE WHEN len(REFERENCE_ID) - len(replace(REFERENCE_ID,',','')) = 2 then (right(REFERENCE_ID,CHARINDEX(',', REVERSE(REFERENCE_ID))-1))
	WHEN REFERENCE_ID is NULL then '10000000'
			else '10000000'  end as REFERENCE_ID_Parse
from tblClaimantPersonalInfo



Declare @Employment table (claimantpersonalinfoid bigint, iscolyear int, gpcacquisitionyear int, FirstExposure int)
insert @Employment

select ClaimantPersonalInfoID,  
max(case when PriorOwner like '%colyear%' then 1
else 0 end) as iscolyear,
MIN(gpcacquisitionyear) as gpcacquisitionyear,
Year(Case when min(coalesce(ce.empstartdate,'1/1/2100'))<=
MIN(coalesce(cp.startdate,'1/1/2100')) then min(ce.empstartdate)
else MIN(cp.startdate) end) as DOFE
from tblClaimantEmployment ce
left join tblState s on s.StateID=ce.JobSiteStateID
left join tblGPCEntityAcquisition gea on gea.State=s.State
left join tblClaimantProduct cp on cp.ClaimantEmploymentID=ce.ClaimantEmploymentID
group by ClaimantPersonalInfoID




declare @Data table (Code nvarchar(250), ClaimantPersonalInfoID bigint, plaintiff nvarchar(100),  
Jurisdiction nvarchar(100), State nvarchar(5),
LocalCounsel nvarchar(500), invoiceid_lawfirm nvarchar(100), invoicedate date, fees money, costs money,
 invoiceapproveddate date, motiononly int, GPCAcquisitionDate int, IsColyear int, firstexposure int)
 
insert into @Data

select Coalesce(CODE,'0'), Coalesce(cpi.claimantpersonalinfoid,'0'),
case when MotionOnly = 1 then 'Unknown Plaintiff' 
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County: Asbestos'	 
	 when CODE IN ('20531', '19822399') and NCC=0 then 'Master Asbestos File'
	 else LastName+', '+FirstName end as Plaintiff, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County'	 
	 else j.Jurisdiction end as Jurisdiction, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
	 else st.State end as State,	 	 
d.LawFirmOffice,InvoiceID_LawFirm, d.InvoiceDate,  
sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
InvoiceApprovedDate, coalesce(motiononly,0), MIN(GPCAcquisitionYear) as GPCAcquisitionDate, ee.iscolyear, FirstExposure
from [CounselLink_DataBilled_2012Q2-2012Q4] d

---JL note- embedded query needed (along with @referenceID temp table) to parse ReferenceIDs with >1 listed in tblclaimantpersonalinfo- taking min CPID because some duplicate IRNs not consolidated if cases were not open
LEFT JOIN (select min(claimantpersonalinfoid) as claimantpersonalinfoid, REFERENCE_ID_Parse from @Reference_IDs where REFERENCE_ID_Parse <> '10000000' group by  REFERENCE_ID_Parse )  r on r.reference_id_parse=d.Code
left join tblClaimantPersonalInfo cpi on r.claimantpersonalinfoid=cpi.ClaimantPersonalInfoID
left join (select * from tblClaim where IsCurrentClaim=1) c on c.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
left join (select * from tblClaimLawsuit where IsPrimaryForClaim=1) cl on cl.ClaimID =c.ClaimID 
left join tblLawsuit l on l.LawsuitID =cl.LawsuitID 
left join tblState st on st.StateID=l.StateID
left join (select * from tblClaimLawsuitStatusTracking where IsPrimaryStatus=1) clst on clst.ClaimLawsuitID=cl.ClaimLawsuitID
left join @Employment ee on ee.claimantpersonalinfoid=cpi.ClaimantPersonalInfoID
left join (select * from tblClaimDisease where PrimaryDisease=1) cd on c.ClaimID=cd.ClaimID
left join TblJurisdiction j on j.JurisdictionID=l.JurisdictionID
left join TblDisease dis on dis.DiseaseID=cd.DiseaseID
where 
coalesce(DoNotBill,0)=0 and  --Excludes Alston Bird -- no alston bird cases in the table
InvoiceApprovedDate is not null --Excludes invoices that have not been approved
and coalesce(alreadybilled,0)=0 --Excludes invoices we can tell Riley already billed
and NCC=0 --and chargedate>= '6/1/2012'



group by  Coalesce(CODE,'0'), cpi.ClaimantPersonalInfoID, InvoiceID_LawFirm, d.InvoiceDate, lawfirmoffice, ChargeType, 
case when MotionOnly = 1 then 'Unknown Plaintiff'
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County: Asbestos'	 
	 when CODE IN ('20531', '19822399') and NCC=0 then 'Master Asbestos File'
	 else LastName+', '+FirstName end,
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County'	 
	 else j.Jurisdiction end, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
	 else st.State end,  
	 InvoiceApprovedDate,coalesce(motiononly,0), ee.iscolyear, FirstExposure

	 
	 
	 
declare @final table (Code nvarchar(250), ClaimantPersonalInfoID bigint, plaintiff nvarchar(100),  
Jurisdiction nvarchar(100), State nvarchar(5),
LocalCounsel nvarchar(500), invoiceid_lawfirm nvarchar(100), invoicedate date, fees money, costs money,
 invoiceapproveddate date, motiononly int, GPCAcquisitionDate int, IsColyear int, firstexposure int, billperiod nvarchar(50),
 FirstStateFees float, FirstStateCosts float, RoyalFees float, RoyalCosts float, SentryFees float, SentryCosts float, TravelersFees float, TravelersCosts float, WausauFees float, WausauCosts float,
 ACEFees float, ACECosts float, CNAFees float, CNACosts float)
 
 insert into @final

--NOW PULL IN INSURER SHARES


Select D.*, datename(month, dateadd(month, month(invoiceapproveddate), 0)-1) + ' 2012' as BillPeriod,
--FIRST STATE
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0357)*fees)
	 when GPCAcquisitionDate >1986 then 0
		else .0357*fees end as FirstStateFees,
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0357)*Costs)
	 when GPCAcquisitionDate >1986 then 0
		else .0357*Costs end as FirstStateCosts,
--ROYAL		
case when motiononly = 1 then 0
	 when Coalesce(IsColyear,0)=1  and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0215)*fees)
	 when ((Coalesce(iscolyear,0)= 1 and GPCAcquisitionDate >1986) Or coalesce(IsColyear,0) <>1) then 0
		else .0215*fees end as RoyalFees,	
case when motiononly = 1 then 0
	 when Coalesce(IsColyear,0)=1 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0215)*Costs)
	 when (( Coalesce(iscolyear,0) = 1 and GPCAcquisitionDate >1986) Or Coalesce(IsColyear,0) <>1) then 0
		else .0215*Costs end as RoyalCosts,		
--SENTRY		
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*fees)
	 when  GPCAcquisitionDate >1986 then 0
		else .1275*fees end as SentryFees,
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*Costs)
	 when GPCAcquisitionDate >1986 then 0
		else .1275*Costs end as SentryCosts,
--TRAVELERS		
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >1986 then 0
		else .085*fees end as TravelersFees,
case when motiononly = 1 then 0
	 when  GPCAcquisitionDate >1986 then 0
		else .085*costs end as TravelersCosts,
--WAUSAU
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*fees)
	 when GPCAcquisitionDate >1986 then 0
		else .1275*fees end as WAUSAUFees,
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*Costs)
	 when GPCAcquisitionDate >1986 then 0
		else .1275*Costs end as WAUSAUCosts,
--ACE		
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >1986 then 0
		else .0425*fees end as ACEFees,	
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >1986 then 0
		else .0425*Costs end as ACECosts,	
--HARBOR/CNA		
case when motiononly = 1 then 0
	 when coalesce(GPCAcquisitionDate, 0) <=1979 then .0425*fees else 0 end as CNAFees,
case when motiononly = 1 then 0
	 when coalesce(GPCAcquisitionDate, 0) <=1979 then .0425*Costs else 0 end as CNACosts								

From @data d
order by LocalCounsel, invoiceid_lawfirm, code,  costs desc, fees desc


select ClaimantPersonalInfoID as file#, plaintiff, LocalCounsel, invoicedate, invoiceapproveddate, InvoiceID_lawfirm, fees, Costs, 
case
when  @CarrierName ='First State' then FirstStateFees
when @CarrierName ='Royal' then RoyalFees
when @CarrierName ='Sentry' then SentryFees
when @CarrierName ='Travelers' then TravelersFees
when @CarrierName ='Wausau' then WausauFees
when @CarrierName ='ACE' then ACEFees
when @CarrierName ='CNA' then CNAFees end as InsurerShareFees, 
case
when  @CarrierName ='First State' then FirstStateCosts
when @CarrierName ='Royal' then RoyalCosts
when @CarrierName ='Sentry' then SentryCosts
when @CarrierName ='Travelers' then TravelersCosts
when @CarrierName ='Wausau' then WausauCosts
when @CarrierName ='ACE' then ACECosts
when @CarrierName ='CNA' then CNACosts end as InsurerShareCosts , @InvoiceNumber as GPCInvoiceNumber, @CarrierName as CarrierName	
from @final

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailDefenseBill_ACE] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailDefenseBill_ACE] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailDefenseBill_ACE] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailDefenseBill_ACE] TO [power_user]
GO
