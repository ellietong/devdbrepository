SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create procedure [dbo].[qrysrptClientFollowUp]
(@CorrespondenceID int)
as
SET NOCOUNT ON

--DECLARE @CorrespondenceID int
--SET @CorrespondenceID=1

DECLARE @kcic table(correspondenceid int, followup nvarchar(4000), taskcategoryid int, taskcategory nvarchar(100), 
	completed bit, datecompleted date)
DECLARE @Lawfirm table (correspondenceid int, followup nvarchar(4000), taskcategoryid int, taskcategory nvarchar(100), 
	 completed bit, datecompleted date)
DECLARE @Client table(correspondenceid int, followup nvarchar(4000), taskcategoryid int, taskcategory nvarchar(100), 
	 completed bit, datecompleted date)
DECLARE @final table(correspondenceid int, followup nvarchar(4000), taskcategory nvarchar(100))

INSERT INTO @KCIC
select Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory, completed, datecompleted
from tblCorrespondence cor
INNER JOIN tblFollowUp f on cor.CorrespondenceID = f.CorrespondenceID
INNER JOIN tblTaskCategoryLookup tc on f.TaskCategoryID = tc.TaskCategoryID
inner join tblInternalContact ic on f.InternalContactID = ic.InternalContactID
inner join tblInternalCompany ico on ic.InternalCompanyID = ico.InternalCompanyID
where ico.InternalCompanyID = 1 and Completed = 0
Group by Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory, completed, datecompleted

INSERT INTO @Lawfirm
select Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory, completed, datecompleted
from tblCorrespondence cor
INNER JOIN tblFollowUp f on cor.CorrespondenceID = f.CorrespondenceID
INNER JOIN tblTaskCategoryLookup tc on f.TaskCategoryID = tc.TaskCategoryID
inner join tblInternalContact ic on f.InternalContactID = ic.InternalContactID
inner join tblInternalCompany ico on ic.InternalCompanyID = ico.InternalCompanyID
where ico.InternalCompanyID = 2 and Completed = 0
Group by Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory, completed, datecompleted

INSERT INTO @Client
select Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory, completed, datecompleted
from tblCorrespondence cor
INNER JOIN tblFollowUp f on cor.CorrespondenceID = f.CorrespondenceID
INNER JOIN tblTaskCategoryLookup tc on f.TaskCategoryID = tc.TaskCategoryID
inner join tblInternalContact ic on f.InternalContactID = ic.InternalContactID
inner join tblInternalCompany ico on ic.InternalCompanyID = ico.InternalCompanyID
where ico.InternalCompanyID = 3 and Completed = 0
Group By Cor.correspondenceid, followup, f.TaskCategoryID, tc.taskcategory, completed, datecompleted

Insert into @final

select Cor.correspondenceid,
Case	when coalesce(k.followup,g.followup) is not null and p.followup IS null Then 'N/A' 
		Else p.followup end as Followup, p.taskcategory
from tblCorrespondence cor
Left JOIN @Lawfirm g on cor.CorrespondenceID=g.correspondenceid
Left JOIN @Client p on cor.CorrespondenceID=p.correspondenceid
Left JOIN @kcic k on cor.CorrespondenceID=k.correspondenceid
where cor.CorrespondenceID=@CorrespondenceID 
Group by Cor.correspondenceid,p.taskcategory,g.followup, k.followup,p.followup

Select correspondenceid, followup , taskcategory
from @Final
Group By correspondenceid, followup , taskcategory


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClientFollowUp] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptClientFollowUp] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClientFollowUp] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptClientFollowUp] TO [power_user]
GO
