SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[qryInternalSBNDClaimsWithAllDocs]
AS
BEGIN

SET NOCOUNT ON
/*Query run daily to send an internal email listing any SBND claims that contain full documentation
and data for settled status. - CAS 12-5-12*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (max)


Declare @Today datetime
SET @Today = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')



SET @Subject = 'GPC: SBND Claims with Full Data/Documentation'
--Set body font, insert KCIC logo/banner at the top of the email, and set table styles. 
--These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>
<H4>The following claimants are currently SBND but are fully Documented: </H4>
<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'


--Create temp table of Claimants who are SBND but are fully documented

--7/1/13 - CAS - Removed product ID references as GPC doesn't require product ID for settlement, just location of exposure ID.
--DECLARE @Product table (claimantpersonalinfoid int, productentered int)
--INSERT INTO @Product
--	SELECT
--		ce.ClaimantPersonalInfoID,
--		CASE
--			WHEN cp.ProductID IS NULL OR cp.StartDate IS NULL OR cp.EndDate IS NULL THEN 0 ELSE 1
--		END AS productentered
--	FROM tblClaimantEmployment ce
--	LEFT JOIN tblClaimantProduct cp		ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
--	WHERE ce.IsPrimary = 1
--	GROUP BY	ce.ClaimantPersonalInfoID,
--				CASE WHEN cp.ProductID IS NULL OR cp.StartDate IS NULL OR cp.EndDate IS NULL THEN 0 ELSE 1
--				END


DECLARE @employment table (claimantpersonalinfoid int, ExposureLocationEntered int)
insert into @employment 
select ce.ClaimantPersonalInfoID, case when max(JobSiteStateID) is null  then 0  else 1  end as ExposureLocationEntered
from tblClaimantEmployment ce 
group by ce.ClaimantPersonalInfoID 



DECLARE @ClaimantDocs TABLE (ClaimantID int, FName nvarchar (50), LName nvarchar (200),
ClaimantIDed int, ClaimantSSN int, ClaimantBirthdate int, DxMedicals int,
Disease int, DiseaseDiagnosisdate datetime, DODCert int, EstatePapers int, --productid int, enteredproductid int, 
EmployExpoHist int, settlementconfirmation int, 
releasereceived int, medicareforms int, checkrequest int, ExposureLocationEntered int, PreTrialReport int)


INSERT INTO @ClaimantDocs
	SELECT
		cpi.ClaimantPersonalInfoID,
		cpi.FirstName,
		cpi.LastName,
		sd.ClaimantIDed,
		CASE
			WHEN socialsecuritynumber IS NULL THEN 0 ELSE 1
		END AS SSN,
		CASE
			WHEN birthdate IS NULL THEN 0 ELSE 1
		END AS birthdate,
		
		sd.DxMedicals,
		cdi.DiseaseID,
		DiseaseDiagnosisDate,
		CASE
			WHEN DeceasedDate IS NULL THEN 2 ELSE sd.DODCert
		END AS DODCert,
		CASE
			WHEN DeceasedDate IS NULL THEN 2 ELSE sd.EstatePapers
		END AS Estatepaper,
		--sd.productid,
		--p.productentered,
		sd.EmployExpoHist,
		sd.settlementconfirmation,
		CASE
			WHEN (d.ClaimantPersonalInfoID IS NOT NULL OR ExecutedReleaseDate IS NOT NULL) THEN 1 ELSE 0
		END AS releasereceived,
		sd.MedicareForms,
		sd.CheckRequest, ee.ExposureLocationEntered,
		case when (l.StateID in (1,45)) 
		and ((dise.HighLevelDiseaseID = 1 and  cs.SettlementAmount < 35000)
		or (dise.HighLevelDiseaseID = 2 and  cs.SettlementAmount < 20000)
		or (dise.HighLevelDiseaseID = 3 and  cs.SettlementAmount < 5000)
		or (dise.HighLevelDiseaseID = 4 and  cs.SettlementAmount < 5000))
		then 1 else sd.PreTrialReport end as PreTrialReport
	FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	inner join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	INNER JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID
	inner join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblSettlementDocumentation sd ON sd.SetPymtID = spd.SetPymtID
	INNER JOIN tblClaimDisease cdi ON cdi.ClaimID = c.ClaimID
	inner join tbldisease dise on dise.DiseaseID = cdi.DiseaseID
	inner join tblHighLevelDisease hld on hld.HighLevelDiseaseID = dise.HighLevelDiseaseID
	INNER JOIN tblClaimLawsuitStatusTracking cls ON cls.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaimantRelatedParty crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantEmployment ce ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join @employment ee	 ON ee.claimantpersonalinfoid =cpi.ClaimantPersonalInfoID 
	LEFT JOIN (SELECT
		ClaimantPersonalInfoID
	FROM tblMultiClaimantDocument mcd
	INNER JOIN tbldocuments d ON d.DocumentID = mcd.DocumentID
	WHERE ClaimantDocumentTypeID = 31
	GROUP BY ClaimantPersonalInfoID)
	d ON d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	--INNER JOIN @Product p		ON p.claimantpersonalinfoid = cpi.ClaimantPersonalInfoID
	WHERE IsPrimaryStatus = 1
	AND ClaimLawsuitStatusID IN (39)
--AND crp.StateID IS NOT NULL
--AND IsPrimaryForClaim = 1



DECLARE @ClaimantsDocumented TABLE (ClaimantID nvarchar (max), DocStatus nvarchar (50))
INSERT INTO @ClaimantsDocumented
	SELECT
		Lname + ', ' + Fname + ' (' + '<a href="https://admin.kcicextranet.com/clients/gpc/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + COALESCE(CONVERT(nvarchar(50), claimantID), '') + '">' + COALESCE(CONVERT(nvarchar(50), claimantID), '') + '</a>' + ')',
		'Claimant Fully Documented'
	FROM @ClaimantDocs
	WHERE ClaimantIDed IN (1, 2) AND ClaimantSSN = 1 AND claimantBirthdate = 1 AND
	DxMedicals <> 0 AND COALESCE(Disease, 0) <> 0 AND DiseaseDiagnosisdate IS NOT NULL AND DODCert <> 0 AND
	EstatePapers <> 0 --AND ProductID <> 0 AND EnteredProductID = 1 
	AND EmployExpoHist <> 0 AND settlementconfirmation <> 0  AND releasereceived <> 0 AND medicareforms <> 0 AND checkrequest <> 0
	AND ExposureLocationEntered <> 0 AND PreTrialReport <> 0
	GROUP BY Lname + ', ' + Fname + ' (' + '<a href="https://admin.kcicextranet.com/clients/gpc/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + COALESCE(CONVERT(nvarchar(50), claimantID), '') + '">' + COALESCE(CONVERT(nvarchar(50), claimantID), '') + '</a>' + ')'

select * from
@ClaimantsDocumented
-------------------------------------------------------------------------
--Create email table of claimants
SET @BodyMessage = @bodymessage +
N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
--Set column headings and populate cells from the cursor
N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Documentation Status</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		ClaimantID)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		DocStatus)
	AS td,
	''
FROM @ClaimantsDocumented C
ORDER BY claimantID
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')


SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--Check if the email needs to be sent
IF (SELECT
	COUNT(DISTINCT claimantID)
FROM @ClaimantsDocumented) < > 0

BEGIN
--Send email
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								--@profile_name='Claims',
								--@recipients = 'scottc@kcic.com', 
								@recipients = 'gpc@kcic.com',
								@blind_copy_recipients = 'kennedyl@kcic.com; gpc@kcic.com; scottc@kcic.com; onyiukeo@kcic.com; casillaw@kcic.com; zongc@kcic.com; jacobsb@kcic.com; nguyenn@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'
END

END


GO
GRANT TAKE OWNERSHIP ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [power_user]
GO
