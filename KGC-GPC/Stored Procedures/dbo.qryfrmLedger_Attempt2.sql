SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmLedger_Attempt2]
(@carrierID int = null)
AS
SET NOCOUNT ON

--Declare @CarrierID bigint
--set @CarrierID= '0'


DECLARE @Carrier TABLE (CarrierID int, carriername nvarchar (50),FormSort int IDENTITY(1,1))

	INSERT INTO @Carrier
		SELECT CarrierID, Carriername
		FROM tblcarrier c
		WHERE carrierID = @CarrierID
		ORDER BY carriername


If @carrierID= '0'
select ledgerid, l.carrierid, BillPeriodID, Indemnity, Defense, NCC , TotalDefense
from tblLedger l
left join @Carrier c on c.CarrierID=l.CarrierID
order by FormSort
--inner join tblcarrier c on c.CarrierID=l.CarrierID
--join tblBillPeriod bp on bp.BillPeriodID=l.BillPeriodID
--order by c.CarrierName, ledgerid desc
--ORDER BY --c.CarrierName,
--   CASE l.BillPeriodID
--      WHEN 7 THEN 1
--      WHEN 1 THEN 2
--      WHEN 2 THEN 3
--      WHEN 3 THEN 4
--      WHEN 4 THEN 5
--	  WHEN 5 THEN 6
--	  WHEN 6 THEN 7
--Else 8 End

DECLARE @Carrier2 TABLE (CarrierID int, carriername nvarchar (50),FormSort int IDENTITY(1,1))

	INSERT INTO @Carrier2
		SELECT CarrierID, Carriername
		FROM tblcarrier c
		WHERE CarrierID <> @CarrierID
		ORDER BY carriername

select ledgerid, l.carrierid, BillPeriodID, Indemnity, Defense, NCC, TotalDefense
from tblLedger l 
left join @Carrier2 c on c.CarrierID=l.CarrierID
--inner join tblcarrier c on c.CarrierID=l.CarrierID
--join tblBillPeriod bp on bp.BillPeriodID=l.BillPeriodID
where l.CarrierID=@carrierID 
order by FormSort
--order by LedgerID desc
--ORDER BY --c.CarrierName,
--   CASE l.BillPeriodID
--      WHEN 7 THEN 1
--      WHEN 1 THEN 2
--      WHEN 2 THEN 3
--      WHEN 3 THEN 4
--      WHEN 4 THEN 5
--	  WHEN 5 THEN 6
--	  WHEN 6 THEN 7
--Else 8 End


RETURN 


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger_Attempt2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger_Attempt2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger_Attempt2] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger_Attempt2] TO [power_user]
GO
