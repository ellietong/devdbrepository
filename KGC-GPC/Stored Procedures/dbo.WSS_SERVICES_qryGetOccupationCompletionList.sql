SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] (@prefix nvarchar(MAX), 
																	 @count  int = 0)
AS
SET NOCOUNT ON

SET ROWCOUNT @count

SELECT DISTINCT Occupation 
FROM tblOccupation 
WHERE Occupation LIKE @prefix + '%'

SET ROWCOUNT 0
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [power_user]
GO
