SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptExhaustedLimitsSummary]
(@ExhaustionDateID int, @ExhaustionTypeID int, @carrierid int, @InsuranceProgramID int)
As
Set Nocount on


--Select * from tblExhaustionDate where ExhaustionDateID = 12
--select * from tblInsuranceProgram

 --Declare @ExhaustionDateID int
 --Declare @ExhaustionTypeID int
 --Declare @CarrierID int
 --Declare @InsuranceProgramID int
 
 -- Set @ExhaustionDateID = 12
 --Set @ExhaustionTypeID = 11
 --Set @CarrierID = 17
 --set @InsuranceProgramID = 2

DECLARE @maxCarrierID int
SELECT @maxCarrierID = MAX(CarrierID) FROM tblPolicy


--Previous Exhaustion
declare @otherexhaustion table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint, OtherExhaustion money)
	Insert Into @otherexhaustion
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID, coalesce(OutsideConsumption,0)
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<=@exhaustiondateid 


Declare @OtherExhaustionTotals table (ExhaustionTypeID int, PolicyDateID int, TotalOtherExhaustion money)
Insert Into @OtherExhaustionTotals 
Select ExhaustionTypeID, PolicyDateID,	coalesce(Sum(OtherExhaustion), 0)
From @otherexhaustion
Group By ExhaustionTypeID, PolicyDateID


Declare @PreviousExhaustionTotals table (ExhaustionTypeID int, PolicyDateID int, Indemnity money, Defense money,
	DefenseOutsideLimits money, TotalDefense money, PreviousAmtExcludingDefOutsideLimits money, PreviousAllocationAmt money,
	SIRIndemnityAllocation money, SIRDefenseAllocation money)
Insert Into @PreviousExhaustionTotals
Select ExhaustionTypeID, PolicyDateID, 
coalesce(Sum(IndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(DefenseAllocation), 0) as Defense,
	coalesce(Sum(DefenseOutsideLimitsAllocation),0) as DefenseOutsideLimits,
	coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAmtExcludingDefOutsideLimits,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAllocationAmt,
	coalesce(Sum(SIRIndemnityAllocation), 0) as SIRIndemnity,
	coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense

From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<=@ExhaustionDateID 
Group By ExhaustionTypeID, PolicyDateID


----Query Results
BEGIN
IF @insuranceprogramID = 1 or @InsuranceProgramID = 2 
	select  p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, convert(nvarchar(100), PStartDate, 101) as pstartdate, convert(nvarchar(100), PEndDate, 101)  as penddate,
		pl.attachmentpoint, pl.perocclimit, pl.aggregatelimit,
	CASE	When provisionvalueID=2 then 'H'
		when provisionvalueID=3 then 'G'
		when provisionvalueID=4 then 'I'
		end as DefenseForm,
		coalesce(Indemnity,0) as Indemnity, 
		coalesce(Defense,0) as Defense, 
		coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
		coalesce(TotalDefense,0) as TotalDefense, 
		coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
		coalesce(PreviousAmtExcludingDefOutsideLimits,0) as PreviousAmtExcludingDefOutsideLimits,
		coalesce(TotalOtherExhaustion,0) as TotalOtherExhaustion,
		case when pd.policydateid in (500,630,628,629) then 0
		else (coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)-coalesce(totalotherexhaustion,0)) end  as Balance,
		cg.CarrierGroupName
	from tblpolicy p 
		left join tblpolicydates pd on p.policyid = pd.policyid
		Left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
		left join @OtherExhaustionTotals   OT on Ot.PolicyDateID=pd.PolicyDateID
		inner join tblcarrier c on c.carrierid = p.carrierid
		join tblCarrierGroup cg on cg.CarrierGroupID = c.CarrierGroupID
		left join tblpolicylimits pl on p.policyid = pl.policyid
		left join tblprovision pro on p.policyID=pro.policyID
	where c.CarrierID >= COALESCE(@CarrierID, 0) AND c.CarrierID <= COALESCE(nullif(@CarrierID,0), @maxCarrierID)
	and InsuranceProgramID in (1,2) 	and (coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)-coalesce(totalotherexhaustion,0))<>0
	and year(cast(pstartdate as date))<1985
	order by  c.carriername,   cast(pstartdate as date) , Cast(penddate as DATE),  policynum, attachmentpoint, perocclimit

ELSE 
	select  p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, convert(nvarchar(100), PStartDate, 101) as pstartdate, convert(nvarchar(100), PEndDate, 101)  as penddate,
		pl.attachmentpoint, pl.perocclimit, pl.aggregatelimit,
	CASE	When provisionvalueID=2 then 'H'
		when provisionvalueID=3 then 'G'
		when provisionvalueID=4 then 'I'
		end as DefenseForm,
		coalesce(Indemnity,0) as Indemnity, 
		coalesce(Defense,0) as Defense, 
		coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
		coalesce(TotalDefense,0) as TotalDefense, 
		coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
		coalesce(PreviousAmtExcludingDefOutsideLimits,0) as PreviousAmtExcludingDefOutsideLimits,
		coalesce(TotalOtherExhaustion,0) as TotalOtherExhaustion,
		case when pd.policydateid in (500,630,628,629) then 0
		else (coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)-coalesce(totalotherexhaustion,0)) end  as Balance,
		cg.CarrierGroupName
	from tblpolicy p 
		left join tblpolicydates pd on p.policyid = pd.policyid
		Left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
		left join @OtherExhaustionTotals   OT on Ot.PolicyDateID=pd.PolicyDateID
		inner join tblcarrier c on c.carrierid = p.carrierid
		join tblCarrierGroup cg on cg.CarrierGroupID = c.CarrierGroupID
		left join tblpolicylimits pl on p.policyid = pl.policyid
		left join tblprovision pro on p.policyID=pro.policyID
	where c.CarrierID >= COALESCE(@CarrierID, 0) AND c.CarrierID <= COALESCE(nullif(@CarrierID,0), @maxCarrierID)
	and InsuranceProgramID =@InsuranceProgramID
	and (coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)-coalesce(totalotherexhaustion,0))<>0
	order by  c.carriername,   cast(pstartdate as date) , Cast(penddate as DATE),  policynum, attachmentpoint, perocclimit

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExhaustedLimitsSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExhaustedLimitsSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExhaustedLimitsSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExhaustedLimitsSummary] TO [power_user]
GO
