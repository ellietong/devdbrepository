SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_EVENT_qryGetEventsByClaimLawsuitID] 
(
@ClaimLawsuitID bigint,
@EmailAddress nvarchar(20)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT EventTrackingID, etr.EventTypeID, EventType, EventDate, EventDescription
FROM tblEventTracking etr
INNER JOIN tblEventType ety ON etr.EventTypeID = ety.EventTypeID
WHERE ClaimLawsuitID = @ClaimLawsuitID

END
GO
