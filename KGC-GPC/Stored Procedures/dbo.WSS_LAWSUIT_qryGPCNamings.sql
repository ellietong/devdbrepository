SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryGPCNamings]
	-- Add the parameters for the stored procedure here
@LawsuitID int=null, @ClaimID int=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT UPPER(DefendantCompany) AS DefendantCompany, a.DefendantCompanyID, a.ServiceDate
	FROM tblAllegation a
		INNER JOIN tblClaimLawsuit cl on a.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblDefendantCompany dc on a.DefendantCompanyID = dc.DefendantCompanyID
	WHERE LawsuitID = @LawsuitID AND cl.ClaimID = @ClaimID--AND DefendantCompanyParentID = 1
	GROUP BY DefendantCompany, a.DefendantCompanyID, a.ServiceDate
	ORDER BY DefendantCompany
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryGPCNamings] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryGPCNamings] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryGPCNamings] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryGPCNamings] TO [power_user]
GO
