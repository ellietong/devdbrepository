SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Proc [dbo].[qryrptDetailIndemnityBill_AllCarriers_2014Q1]
(@occurrencegroupid int, @InvoiceNumber Nvarchar(50),@Carrierid int, @billtype Nvarchar(1), @carriername Nvarchar(50)  )
As
Set Nocount on

--declare @occurrencegroupid int
--declare @Carrierid int
--declare @billtype Nvarchar(1)
--declare @InvoiceNumber Nvarchar(50)
--declare @carriername Nvarchar(50) 
--set @InvoiceNumber = '20140331'
--set @billtype=' '
--SET @occurrencegroupid = 13
--set @Carrierid= 359
--set @carriername = 'Travelers'

DECLARE @data TABLE (BillPeriod Nvarchar(50) , FileNumber Nvarchar(50), plaintiff Nvarchar(50), disease nvarchar(50), ss# nvarchar(50), DOB datetime,
SettlementDate datetime, amount float, InsurerShare float) 


insert into @data

SELECT datename(month, dateadd(month, month(ProcessedDate), 0)-1) + ' 2014' as BillPeriod, 
cpi.ClaimantPersonalInfoID as FileNumber, 
LastName +', '+FirstName as plaintiff,
Case when Disease like '%asbestosis%' then 'ASBESTOSIS' else Upper(disease) end as disease,
case when SocialSecurityNumber not like '%-%' then LEFT(SocialSecurityNumber,3) +'-'+ SUBSTRING(SocialSecurityNumber,4,2)+'-'+RIGHT(SocialSecurityNumber,4)
else SocialSecurityNumber end as ss#, 
BirthDate, 
SettlementDate as SettlementDate, 
cs.SettlementAmount as Amount, 
 sum(indemnityallocation) AS InsurerShare

from tblClaimantPersonalInfo cpi 
inner join tblClaim c on c.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
inner join tblClaimLawsuit cl on cl.ClaimID =c.ClaimID 
inner join tblLawsuit l on l.LawsuitID =cl.LawsuitID 
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID 
inner join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=cl.ClaimLawsuitID
inner join tbloccurrence o on o.claimid = cpi.claimantpersonalinfoid
inner join tblExhaustion  e on e.occurrenceid = o.OccurrenceID
INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
INNER JOIN tblPolicy p ON p.PolicyID = pd.PolicyID
INNER JOIN tblCarrier c1 ON p.CarrierID = c1.CarrierID
left join tblClaimDisease cd on cd.ClaimID=c.ClaimID 
left join TblDisease d on d.DiseaseID=cd.DiseaseID
left join tblState s on l.StateID=s.StateID
left join tblGender g on g.GenderID=cpi.GenderID
where  occurrencegroupid = @occurrencegroupid and o.OccurrenceTypeID = 1 AND cl.IsPrimaryForClaim = 1 and clst.IsPrimaryStatus = 1 AND cd.PrimaryDisease = 1
AND c1.CarrierID = @Carrierid
GROUP BY datename(month, dateadd(month, month(ProcessedDate), 0)-1) + ' 2014', 
cpi.ClaimantPersonalInfoID, 
LastName +', '+FirstName,
State,
Case when Disease like '%asbestosis%' then 'ASBESTOSIS' else Upper(disease) end,
case when SocialSecurityNumber not like '%-%' then LEFT(SocialSecurityNumber,3) +'-'+ SUBSTRING(SocialSecurityNumber,4,2)+'-'+RIGHT(SocialSecurityNumber,4)
else SocialSecurityNumber END,
BirthDate, 
gender, 
SettlementDate,
cs.SettlementAmount



if @billtype ='d'
select BillPeriod, FileNumber, plaintiff, UPPER(disease) as disease,ss#, DoB,  SettlementDate,  amount,
  InsurerShare, @InvoiceNumber as InvoiceNumber
from @data
order by MONTH(BillPeriod), plaintiff

else
select BillPeriod,SUM(amount) AS Amount, SUM(InsurerShare) AS InsurerShare
from @data
group by BillPeriod
order by MONTH(BillPeriod)


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailIndemnityBill_AllCarriers_2014Q1] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailIndemnityBill_AllCarriers_2014Q1] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailIndemnityBill_AllCarriers_2014Q1] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailIndemnityBill_AllCarriers_2014Q1] TO [power_user]
GO
