SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[WSS_EMPLOYMENT_qryCounty](@StateID int=null)
AS
SET NOCOUNT ON

--SELECT CountyID, 
--	County,
--	StateID 	
--FROM tblCounty
--WHERE StateID = COALESCE(@StateID,0)
--UNION 
--SELECT NULL, '', NULL
--ORDER BY County

--RETURN

IF (@StateID IS NOT NULL AND @StateID <> 0)
	BEGIN
		SELECT CountyID, 
			County,
			StateID 	
		FROM tblCounty
		WHERE StateID = COALESCE(@StateID,0)
		UNION 
		SELECT NULL, '', NULL
		ORDER BY County
	END
	ELSE
	BEGIN
		SELECT CountyID, 
			County,
			StateID 	
		FROM tblCounty
		UNION 
		SELECT NULL, '', NULL
		ORDER BY County
	END

GO
