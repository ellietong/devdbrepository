SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Proc [dbo].[qryrptDetailIndemnityBill_Travelers_2012Q2 re-bill]
(@CarrierName Nvarchar(50), @InvoiceNumber Nvarchar(50),@billtype nvarchar(1))
As
Set Nocount on


--declare @CarrierName Nvarchar(50)
--declare @InvoiceNumber Nvarchar(50)
--declare @billtype Nvarchar(1)
--set @CarrierName= 'Travelers'
--set @InvoiceNumber = 'gpc4443'
--set @billtype='d'

declare @data table (BillPeriod Nvarchar(50) , FileNumber Nvarchar(50), plaintiff Nvarchar(50), disease nvarchar(50), ss# varchar(50), StateOfFiling Nvarchar(50), SettlementDate datetime, CheckRequestDate datetime, Amount float, billedbyriley bit, gpcacquisitiondate Nvarchar(50), 
DOFE Nvarchar(50), iscolyear bit, gpc Nvarchar(50), colyear Nvarchar(50), totalyearsofexposure Nvarchar(50),
[First State] float, Royal float,  Sentry float, Travelers float, Wausau float, International float, CNA float)

insert into @data

select '2012 Q2' as BillPeriod,
			billingFile# as FileNumber, plaintiffname as plaintiff, s.Disease, d.SS#,
s.state as StateOfFiling, settledate as SettlementDate, CheckRequestDate, Amount, billedbyriley, gpcacquisitiondate, year(firstexposure) as DOFE, iscolyear,r.gpc, r.colyear, r.totalyearsofexposure,
--FIRST STATE
case when BilledByRiley = 1 then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.0357)*Amount 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0357)*Amount)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .0357*amount end as [First State],
--ROYAL		
case when BilledByRiley = 1 then (((r.colyear)/TotalYearsOfExposure)*.0215)*Amount 
	 when BilledByRiley = 0 and IsColyear=1 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0215)*Amount)
	 when ((BilledByRiley = 0 and iscolyear = 1 and GPCAcquisitionDate >1986) Or IsColyear <>1) then 0
		else .0215*amount end as Royal,		
--SENTRY		
case when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*Amount 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*Amount)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*amount end as Sentry,
--TRAVELERS		
case when BilledByRiley = 1 then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.085)*Amount 
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .085*amount end as Travelers,
--WAUSAU
case when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*Amount 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*Amount)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*amount end as Wausau,
--ACE		
case when GPCAcquisitionDate >1986 then 0
	 when CheckRequestDate <='5/31/2012' then 0
		else .0425*Amount end as International,	
--HARBOR/CNA		
Case when coalesce(GPCAcquisitionDate, 0) <=1979 then .0425*Amount else 0 end as CNA									
		--select * from CLAIMS_DATA where GPCAcquisitionDate between 1980 and 1981 (16 claims with GPCAcquisitionDate of 1980-1981)
		
from dbo.CLAIMS_2012Q2_Settlements s
inner join CLAIMS_DATA d on d.InternalReferenceNumber=s.billingfile#
inner join Claims_AdditionalClaimNumbers a on a.File#=d.File# and a.NewFileNo = s.BillingFile#
left join RileyBillingPercentages r on r.File#=d.File#

group by billingFile#, plaintiffname,  s.Disease, d.SS#, s.state, settledate, CheckRequestDate, amount, billedbyriley, gpcacquisitiondate, year(firstexposure), IsColyear,iscolyear,r.gpc, r.colyear, r.totalyearsofexposure,
case when BilledByRiley = 1 then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.0357)*Amount 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0357)*Amount)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .0357*amount end,
--ROYAL		
case when BilledByRiley = 1 then (((r.colyear)/TotalYearsOfExposure)*.0215)*Amount 
	 when BilledByRiley = 0 and Coalesce(IsColyear,0)=1 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0215)*Amount)
	 when ((BilledByRiley = 0 and Coalesce(iscolyear,0) = 1 and GPCAcquisitionDate >1986) Or Coalesce(IsColyear,0) <>1) then 0
		else .0215*amount end,		
--SENTRY		
case when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*Amount 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*Amount)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*amount end,
--TRAVELERS		
case when BilledByRiley = 1 then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.085)*Amount 
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .085*amount end,
--WAUSAU
case when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*Amount 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*Amount)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*amount end,
--ACE		
case when GPCAcquisitionDate >1986 then 0
	 when CheckRequestDate <='5/31/2012' then 0
		else .0425*Amount end,		
--HARBOR/CNA		
Case when coalesce(GPCAcquisitionDate, 0) <=1979 then .0425*Amount else 0 end								
order by PlaintiffName

if @billtype ='d'
select BillPeriod, FileNumber, plaintiff, disease,ss#, StateOfFiling,  SettlementDate, CheckRequestDate, Amount, billedbyriley, gpcacquisitiondate, 
DOFE, iscolyear, gpc, colyear, totalyearsofexposure, case 
when @CarrierName ='First State' then [First State]
when @CarrierName ='Royal' then Royal
when @CarrierName ='Sentry' then Sentry
when @CarrierName ='Travelers' then Travelers
when @CarrierName ='Wausau' then Wausau
when @CarrierName ='International' then International
when @CarrierName ='CNA' then CNA end as InsurerShare, @InvoiceNumber as InvoiceNumber
from @data
--order by MONTH(BillPeriod)

else
select BillPeriod, SUM(amount) as amount, case 
when @CarrierName ='First State' then sum([First State])
when @CarrierName ='Royal' then sum(Royal)
when @CarrierName ='Sentry' then sum(Sentry)
when @CarrierName ='Travelers' then sum(Travelers)
when @CarrierName ='Wausau' then sum(Wausau)
when @CarrierName ='International' then sum(International)
when @CarrierName ='CNA' then sum(CNA) end as InsurerShare, @InvoiceNumber as InvoiceNumber, @CarrierName as carriername
from @data
group by BillPeriod
--order by MONTH(BillPeriod)
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailIndemnityBill_Travelers_2012Q2 re-bill] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailIndemnityBill_Travelers_2012Q2 re-bill] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailIndemnityBill_Travelers_2012Q2 re-bill] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailIndemnityBill_Travelers_2012Q2 re-bill] TO [power_user]
GO
