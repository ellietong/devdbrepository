SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[qryExternalEmailEvents_FutureEvents]
AS 
BEGIN
        DECLARE @Subject NVARCHAR(100)
        DECLARE @LogoHeader NVARCHAR(2000)

        SET @Subject = 'GPC Claims: Upcoming Events'          

        SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

DECLARE @TodaysDate date = (SELECT DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))

--Per client request, emails will go out each Friday and show activity for next 30 days. CAS 10-22-14
DECLARE @DaysFuture date = (SELECT DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))+30)

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: verdana,arial,helvetica,sans-serif; font-size: 8pt; text-align: left; border: none; padding: .1em; vertical-align: left; background-color: transparent;'

DECLARE @Events TABLE 
(
	ClaimantName NVARCHAR(500),
	EventTrackingID INT, 
	EventTypeID INT, 
	EventType NVARCHAR(100), 
	EventDate NVARCHAR(15), 
	EventDescription NVARCHAR(MAX),
	Status NVARCHAR(100),
	StateFiled nvarchar(2)
)
INSERT INTO @Events
SELECT DISTINCT
				'<a href="https://www.kcicextranet.com/clients/GPC/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), cpi.ClaimantPersonalInfoID) + '">' + COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') + COALESCE(' (' + CONVERT(nvarchar(50), cpi.ClaimantPersonalInfoID) + ')', '') + '</a>',
				EventTrackingID, 
				etr.EventTypeID, 
				EventType, 
				COALESCE(CONVERT(NVARCHAR, CONVERT(DATE, EventDate)), 'TBD') AS EventDate,
				COALESCE(EventDescription, '') AS EventDesciption,
				ClaimLawsuitStatus, s.State
FROM tblEventTracking etr
INNER JOIN tblClaimLawsuit cl ON etr.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblEventType ety ON etr.EventTypeID = ety.EventTypeID
INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
join tbllawsuit l on l.lawsuitID = cl.lawsuitID
join tblstate s on s.stateID = l.stateID
--WHERE ((EventDate >= @TodaysDate ) OR EventDate IS NULL)
WHERE ((EventDate BETWEEN @TodaysDate AND @DaysFuture) )--OR EventDate IS NULL)
and IsPrimaryStatus = 1 and cpi.claimantpersonalinfoID <> 13357
ORDER BY EventDate, State

--SELECT *
--FROM @Events
	
DECLARE @BodyMessage NVARCHAR(MAX)

SET @BodyMessage = @LogoHeader
    + '<STYLE TYPE="text/css">
		<!--
		TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
		TD	
		{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
		}
		TH	
		{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
		UL {margin:0; 
	
		padding:0;}	


		--->
		</STYLE>'

        DECLARE @LastLawsuitID NVARCHAR(250) = ''

        IF ( SELECT COUNT(EventTrackingID)
             FROM   @Events
           ) > 0 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<H4>Please see the upcoming events for GPC: </H4>' 
                SET @BodyMessage = @BodyMessage
                    + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' 
				SET @BodyMessage = @BodyMessage 
					+ N'<tr>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Event Date</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">State Filed</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Event Type</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Claimants (ClaimantID)</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Event Description</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Status</th>
						'
	
                DECLARE 
					@EventDate NVARCHAR(15), 
					@StateFiled nvarchar(2),
					@ClaimantName NVARCHAR(500),
					@EventTrackingID INT, 
					@EventTypeID INT, 
					@EventType NVARCHAR(100), 
					@EventDescription NVARCHAR(MAX),
					@Status NVARCHAR(100)
	
                DECLARE Deadline_List CURSOR
                FOR
                    SELECT 	
							EventDate, 
							StateFiled,
							EventType, 
							ClaimantName,
							EventTrackingID, 
							EventTypeID, 
							Status , 		
							EventDescription
					FROM @Events
                OPEN Deadline_List
                FETCH NEXT FROM Deadline_List INTO	
													@EventDate,
													@StateFiled,
													@EventType, 
													@ClaimantName,
													@EventTrackingID, 
													@EventTypeID, 
													@Status,
													@EventDescription
												

                WHILE @@FETCH_STATUS = 0 
                    BEGIN
	
		
                        SET @BodyMessage = @BodyMessage + '<tr>'
						SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @EventDate + ' </td>'
						SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @StateFiled + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top; width:150px;"> '
                            + @EventType + ' </td>'
						SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @ClaimantName + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top; max-width: 500px"> '
                            + @EventDescription + ' </td>'
						SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @Status + ' </td><tr/>' 
		  

                        FETCH NEXT FROM Deadline_List INTO
													@EventDate,
													@StateFiled,
													@EventType, 
													@ClaimantName,
													@EventTrackingID, 
													@EventTypeID, 
													@Status,
													@EventDescription

                    END
                CLOSE Deadline_List
                DEALLOCATE Deadline_List
	
	
                SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'
	

            
			END
        ELSE 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<H4>There are no upcoming events.</H4>'
            END

        SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;',
                                   '>')


SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

DECLARE @RecipientList nvarchar(MAX) = ' '
SET @RecipientList = 'ernie_wetzler@genpt.com; jenifer.keenan@alston.com; phewitt@rhwrlaw.com; jdioszeghy@rwdmlaw.com; cdickey@rwdmlaw.com;'

--SET @BodyMessage = @BodyMessage + '<br/> @RecipientList = ' + @RecipientList

SELECT @BodyMessage

--Execute stored procedure to send the email to that specific users
EXEC msdb.dbo.sp_send_dbmail	
@profile_name = 'Claims',
@recipients = @RecipientList,
@copy_recipients = 'scottc@kcic.com; ANolf@rhwrlaw.com',
@blind_copy_recipients = '',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
END

GO
