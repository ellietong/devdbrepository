SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetDefendantCompanyList] (@PrefixText nvarchar(MAX))
AS
SET NOCOUNT ON


SELECT DISTINCT DefendantCompanyID , DefendantCompany
FROM tblDefendantCompany 
WHERE DefendantCompany LIKE @PrefixText + '%' 
	AND ClientID = 40
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetDefendantCompanyList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetDefendantCompanyList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetDefendantCompanyList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetDefendantCompanyList] TO [power_user]
GO
