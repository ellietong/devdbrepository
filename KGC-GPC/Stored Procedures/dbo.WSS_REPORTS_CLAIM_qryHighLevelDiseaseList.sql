SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_REPORTS_CLAIM_qryHighLevelDiseaseList]

AS
SET NOCOUNT ON

SELECT HighLevelDiseaseID, 
	HighLevelDisease 
FROM tblHighLevelDisease
UNION
SELECT NULL, ''
ORDER BY HighLevelDisease
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_CLAIM_qryHighLevelDiseaseList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_CLAIM_qryHighLevelDiseaseList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_CLAIM_qryHighLevelDiseaseList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_CLAIM_qryHighLevelDiseaseList] TO [power_user]
GO
