SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qrySBNDTotal]  (@matterID int, @EndDate datetime=null)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @yearID int
--Set @YearId=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter WITH (READCOMMITTED)


select COUNT(distinct cl.ClaimLawsuitID) as Count
from tblClaim c WITH (READCOMMITTED)
inner join tblClaimLawsuit cl WITH (READCOMMITTED) ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l WITH (READCOMMITTED) ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst WITH (READCOMMITTED) ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s WITH (READCOMMITTED) ON l.StateID=s.StateID
join (select distinct ClaimLawsuitID, MaterialID from tblAllegation where materialID not in (11,12))a on a.ClaimLawsuitID = cl.ClaimLawsuitID

Where ClaimLawsuitStatusID = 39 and c.ClaimantPersonalInfoID <>13357
and  IsPrimaryStatus=1 
and a.MaterialID not  in (11,12)

		--and ProcessedDate <= COALESCE(@EndDate,'12/31/2100')
order by COUNT(cl.ClaimLawsuitID) desc


END





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qrySBNDTotal] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qrySBNDTotal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qrySBNDTotal] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qrySBNDTotal] TO [power_user]
GO
