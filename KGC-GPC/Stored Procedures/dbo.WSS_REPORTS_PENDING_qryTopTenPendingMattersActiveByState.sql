SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByState] (@MatterID int=null,@EndDate datetime=null)
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter
    
    
select Top 10 FullNameState As State, COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s ON l.StateID=s.StateID
join (select distinct ClaimLawsuitID, MaterialID from tblAllegation where materialID not in (11,12))a on a.ClaimLawsuitID = cl.ClaimLawsuitID
Where ClaimLawsuitStatusID in (22) and c.ClaimantPersonalInfoID <>13357
and  IsPrimaryStatus=1 
and a.MaterialID not  in (11,12)
		--and ProcessedDate <= COALESCE(@EndDate,'12/31/2100')
group by FullNameState
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, FullNameState


END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByState] TO [power_user]
GO
