SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] (@ClaimantPersonalInfoID int)
AS

SET NOCOUNT ON

--DECLARE @ClaimantPersonalInfoID int = 12208

IF (SELECT COUNT(ClaimantPersonalInfoID) FROM tblClaimantPersonalInfo WHERE ClaimantPersonalInfoID IN ( 11354, 
																										11645,
																										10840,
																										12393,
																										12208,
																										12230,
																										10069,
																										9152,
																										11771,
																										11720,
																										10430,
																										11966,
																										12481,
																										12347,
																										10192,
																										97,
																										12430)
																										AND ClaimantPersonalInfoID = @ClaimantPersonalInfoID) > 0
BEGIN 
	SELECT COUNT(DISTINCT paymentSettlementID) 
	FROM tblPaymentSettlement ps
		LEFT JOIN tblClaimantSettlement cs	ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
		LEFT JOIN tblClaimLawsuit cl		ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblClaim c				ON cl.ClaimID = c.ClaimID
	WHERE ClaimantPersonalInfoID = NULL
	GROUP BY c.ClaimantPersonalInfoID
END 
ELSE
BEGIN
	SELECT COUNT(DISTINCT paymentSettlementID) 
	FROM tblPaymentSettlement ps
		LEFT JOIN tblClaimantSettlement cs	ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
		LEFT JOIN tblClaimLawsuit cl		ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblClaim c				ON cl.ClaimID = c.ClaimID
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	GROUP BY c.ClaimantPersonalInfoID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [power_user]
GO
