SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_REPORTS_qryPendingClaimsCountByJurisdictionByYear_Monthly] (@StartDate datetime, @ENDDate datetime, @MatterID int)
AS

--Set NoCount ON

--DECLARE @StartDate datetime
--DECLARE @ENDDate datetime
DECLARE @StateID int
--DECLARE @Matter int

--SET @StartDate = '1/1/1900'
--SET @ENDDate = '9/30/2010'
SET @StateID = null
--SET @Matter = null

-- Find Max ID Values

DECLARE @MaxStateID int

--Define Max State ID
SELECT @MaxStateID = MAX(stateid) FROM tblState

-- Find Max ID Values
DECLARE @MaxResponsibleDefenseCounselID int

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @DiseaseCount Table (Matter nvarchar(100), MatterID int, State nvarchar(150), YearFiled datetime, 
							 MonthFiledID int, MonthFiledName nvarchar(100), statusdate datetime, MesoCount int, 
							 LungCancerCount int, OtherCancerCount int, NonMaligCount int,  UnknownCount int, UnknownCount1 int)


INSERT INTO @DiseaseCount

SELECT  Matter, C.MatterID, FullNameState AS state,  
		CASE WHEN ProcessedDate IS NULL 
			 THEN  '1/1/1900' 
			 ELSE ProcessedDate 
		END AS YearFiled, MONTH(COALESCE(ProcessedDate, '1/1/1900')), Datename(Month, COALESCE(ProcessedDate, '1/1/1900')),
		COALESCE(settlementdate,statusdate,'1/1/1900'),
		--Meso
		CASE WHEN DMeso.HighLevelDiseASeID = 1 
			 THEN 1
			 ELSE 0 
		END AS MesoCount,
		--Lung Cancer
		CASE WHEN DLC.HighLevelDiseASeID = 2 
			 THEN 1
			 ELSE 0 
	    END AS LungCancerCount,
		--Other Cancer
		CASE WHEN DOC.HighLevelDiseASeID = 3 
			 THEN 1
		     ELSE 0 
	    END AS OtherCancerCount,
		--NonMalig & silicosis
		CASE WHEN DNM.HighLevelDiseASeID IN (4, 7) 
			 THEN 1
			 ELSE 0 
		END AS NonMaligCount,
		--Unknown & no diseASe
		CASE WHEN DUnk.HighLevelDiseASeID IN (5, 6) 
			 THEN 1
			 ELSE 0 
		END AS UnknownCount,
		--NULLs
		CASE WHEN DUnk.HighLevelDiseASeID IS NULL 
			 THEN 1
			 ELSE 0 
		END AS UnknownCount1

FROM tblClaimantPersonalInfo CPI
	LEFT JOIN tblClaim C ON CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
	LEFT JOIN (SELECT CD.ClaimDiseASeID, CD.ClaimID, CD.DiseASeDiagnosisDate, CD.DiseASeID, CD.DoctorID, CD.PrimaryDiseASe 
			   FROM tblClaimDiseASe CD 
			   WHERE PrimaryDiseASe = 1) CD ON CD.ClaimID=C.ClaimID
	LEFT JOIN tblMatter M ON c.MatterID = M.MatterID
	LEFT JOIN tblClaimSequence csq ON c.ClaimSequenceID = csq.ClaimSequenceID
	--Meso
	LEFT JOIN tblDiseASe DMeso ON CD.DiseASeID=DMeso.DiseASeID
	--Lung Cancer
	LEFT JOIN tblDiseASe DLC ON CD.DiseASeID=DLC.DiseASeID
	--Other Cancer
	LEFT JOIN tblDiseASe DOC ON CD.DiseASeID=DOC.DiseASeID
	--Non-Malig
	LEFT JOIN tblDiseASe DNM ON CD.DiseASeID=DNM.DiseASeID
	--Silicosis
	LEFT JOIN tblDiseASe DSC ON CD.DiseASeID=DSC.DiseASeID
	--No DiseASe
	LEFT JOIN tblDiseASe DND ON CD.DiseASeID=DND.DiseASeID
	--Unknown
	LEFT JOIN tblDiseASe DUnk ON CD.DiseASeID=DUnk.DiseASeID
	LEFT JOIN tblClaimLawsuit CL ON C.ClaimID=CL.ClaimID
	LEFT JOIN tblLawsuit L ON CL.LawsuitID=L.LawsuitID
	LEFT JOIN tblState S ON L.StateID=S.StateID
	LEFT JOIN tblClaimantSettlement cs ON cs.ClaimLawsuitID = CL.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatusTracking cst ON cst.ClaimLawsuitID = cl.ClaimLawsuitID
--LEFT JOIN tblClaimLawsuitStatusgroup csg ON csg.claimlawsuitstatusid = cst.ClaimLawsuitStatusID

WHERE  s.StateID >= COALESCE(@StateID, 0) 
	AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) 
	AND IsPrimaryForClaim = 1 
	AND COALESCE(cst.IsPrimaryStatus,0) = 1
	AND C.MatterID >= COALESCE(@MatterID,0) 
	AND C.MatterID <= COALESCE(NULLIF(@MatterID,0),@maxMatterID)
	AND cst.ClaimLawsuitStatusID in (22,29) 
	AND (COALESCE(ProcessedDate,servicedate, filedate, '1/1/1900')<=@ENDDate)  --OR (cst.ClaimLawsuitStatusID NOT IN (15,16,27,18,19,20,21,24,30,17,26) AND ProcessedDate > @ENDDate))
	--AND COALESCE(cpi.jobsiteinfo, 0) = 0


SELECT YEAR(@ENDDate) AS CloseYear, State,  SUM(MesoCount) AS MesoCount,
	   SUM(LungCancerCount) AS LungCancerCount, SUM(OtherCancerCount) AS OtherCancerCount ,@StartDate AS StartDate, @EndDate AS EndDate,
	   SUM(NonMaligCount) AS NonMaligCount, SUM(UnknownCount+UnknownCount1) AS UnknownCount, SUM(UnknownCount+UnknownCount1+MesoCount+LungCancerCount+OtherCancerCount+NonMaligCount) AS Total
FROM @DiseASeCount
WHERE Yearfiled BETWEEN COALESCE(@StartDate, '1/1/1900') AND COALESCE(@EndDate, GETDATE())
GROUP BY  State
ORDER BY  State
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [power_user]
GO
