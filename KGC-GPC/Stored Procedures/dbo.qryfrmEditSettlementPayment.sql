SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create procedure [dbo].[qryfrmEditSettlementPayment] (@CheckID int)
AS
SET NOCOUNT ON

select * from tblPaymentSettlement
where CheckID = @CheckID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditSettlementPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmEditSettlementPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditSettlementPayment] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditSettlementPayment] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditSettlementPayment] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditSettlementPayment] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditSettlementPayment] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditSettlementPayment] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditSettlementPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmEditSettlementPayment] TO [power_user]
GO
