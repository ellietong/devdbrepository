SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_EVENT_qryUpdateEvent]
(
	@EventTrackingID int,
	@EventTypeID int,
	@EventDate datetime,
	@EventDescription nvarchar(max),
	@EmailAddress nvarchar(250)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE tblEventTracking
SET EventTypeID = @EventTypeID,
	EventDate = @EventDate,
	EventDescription = @EventDescription,
	Username = @EmailAddress
WHERE EventTrackingID = @EventTrackingID

END
GO
