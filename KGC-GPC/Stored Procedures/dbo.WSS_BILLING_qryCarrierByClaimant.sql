SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_BILLING_qryCarrierByClaimant](@ClaimantPersonalInfoID BIGINT = null, @emailaddress nvarchar(200)=null)
AS
SET NOCOUNT ON


--Claimants with NJ, VA, and WV
DECLARE @StatesToExclude table (ClaimantPersonalInfoID bigint, JobSiteStateID int)
INSERT INTO @StatesToExclude

	SELECT
		ClaimantPersonalInfoID,
		JobSiteStateID
	FROM tblClaimantEmployment
	WHERE JobSiteStateID IN (54, 12, 3)
	GROUP BY	ClaimantPersonalInfoID,
				JobSiteStateID
	ORDER BY ClaimantPersonalInfoID

--Claimants with NJ, VA, and WV coded as the only location
Declare @CPIDExcludeDueToMultipleStateAcquisitions table (ClaimantPersonalInfoID bigint)
INSERT INTO @CPIDExcludeDueToMultipleStateAcquisitions

	SELECT
		ce.ClaimantPersonalInfoID
	FROM tblClaimantEmployment ce
	JOIN @StatesToExclude s ON s.ClaimantPersonalInfoID = ce.ClaimantPersonalInfoID
	GROUP BY ce.ClaimantPersonalInfoID
	HAVING COUNT(DISTINCT ce.jobsitestateid) = 1
	ORDER BY ce.ClaimantPersonalInfoID


---Al states except NJ, VA, WV 
Declare @Employment1 table (claimantpersonalinfoid bigint,  iscolyear int, GPCAcquisitionDate int,bill bit)
INSERT @Employment1

	SELECT
		ClaimantPersonalInfoID, 
		MAX(CASE
			WHEN PriorOwner LIKE '%colyear%' THEN 1 ELSE 0
		END) AS iscolyear,
		MIN(gea.GPCAcquisitionYear) AS GPCAcquisitionDate, case when min(gea.GPCAcquisitionYear) > 1986 then 0 else 1 end as bill
	FROM tblClaimantEmployment ce
	LEFT JOIN tblState s ON s.StateID = ce.JobSiteStateID
	LEFT JOIN (SELECT
		*
	FROM tblGPCEntityAcquisition 
	WHERE GPCEntityID NOT IN (96, 97, 117, 118, 121, 122)) gea ON gea.State = s.State
	LEFT JOIN tblClaimantProduct cp ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
	WHERE ClaimantPersonalInfoID NOT IN (SELECT
		ClaimantPersonalInfoID
	FROM @CPIDExcludeDueToMultipleStateAcquisitions)
	GROUP BY ClaimantPersonalInfoID
	ORDER BY ClaimantPersonalInfoID



---NJ, VA, WV where county is coded
Declare @Employment2 table (claimantpersonalinfoid bigint,  iscolyear int, GPCAcquisitionDate int, Bill bit)
INSERT @Employment2

	SELECT
		ClaimantPersonalInfoID,
		MAX(CASE
			WHEN gea.PriorOwner LIKE '%colyear%' THEN 1 ELSE 0
		END) AS iscolyear,
		MIN(gea.GPCAcquisitionYear) AS GPCAcquisitionDate , bill
	FROM tblClaimantEmployment ce
	LEFT JOIN tblState s ON s.StateID = ce.JobSiteStateID
	inner JOIN tblCounty c on c.CountyID=ce.CountyID
	LEFT JOIN (SELECT *
	FROM tblGPCEntityAcquisition
	WHERE GPCEntityID  IN (96, 97, 117, 118, 121, 122)) gea ON gea.State = s.State
	LEFT JOIN tblClaimantProduct cp ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
	WHERE ce.JobSiteStateID in (3,12,54)
	GROUP BY ClaimantPersonalInfoID, bill
	ORDER BY ClaimantPersonalInfoID


---For 3rd employment table below, this temp pulls in Claimants with NJ, VA, and WV coded as the only location along with claimantemploymentID so records for these state with
--no county coded can accurately be found
Declare @CPIDExcludeDueToMultipleStateAcquisitionsWithClaimantEmploymentID table (ClaimantPersonalInfoID bigint, claimantemploymentid bigint)
INSERT INTO @CPIDExcludeDueToMultipleStateAcquisitionsWithClaimantEmploymentID

	SELECT
		ce.ClaimantPersonalInfoID, claimantemploymentid
	FROM tblClaimantEmployment ce
	JOIN @StatesToExclude s ON s.ClaimantPersonalInfoID = ce.ClaimantPersonalInfoID
	GROUP BY ce.ClaimantPersonalInfoID, claimantemploymentid
	HAVING COUNT(DISTINCT ce.jobsitestateid) = 1
	ORDER BY ce.ClaimantPersonalInfoID



---NJ, VA, WV where county is not coded
Declare @Employment3 table (claimantpersonalinfoid bigint,  iscolyear int, GPCAcquisitionDate int, Bill bit)
INSERT @Employment3

	SELECT
		edm.ClaimantPersonalInfoID,
		0 AS iscolyear,
		MAX(gea.GPCAcquisitionYear) AS GPCAcquisitionDate , 0
	FROM tblClaimantEmployment ce
	join @CPIDExcludeDueToMultipleStateAcquisitionsWithClaimantEmploymentID edm on edm.claimantemploymentid=ce.ClaimantEmploymentID
	LEFT JOIN tblState s ON s.StateID = ce.JobSiteStateID
	LEFT JOIN (SELECT *
	FROM tblGPCEntityAcquisition gea
	WHERE GPCEntityID  IN (96, 97, 117, 118, 121, 122)) gea ON gea.State = s.State
	WHERE countyid is null
	GROUP BY edm.ClaimantPersonalInfoID
	ORDER BY edm.ClaimantPersonalInfoID



Declare @employmentCombined table (claimantpersonalinfoid bigint,  iscolyear int, GPCAcquisitionDate int, Bill bit)
insert into @employmentCombined

select * 
from @Employment1 

UNION ALL 

select * 
from @Employment2

UNION ALL 

select * 
from @Employment3



Declare @employmentFinal table (claimantpersonalinfoid bigint,  iscolyear int, GPCAcquisitionDate int, Bill bit)
insert into @employmentFinal

select claimantpersonalinfoid, max(iscolyear) as iscolyear, min(GPCAcquisitionDate) as GPCAcquisitionDate, max(convert(int,Bill)) as Bill
from @employmentCombined 
group by claimantpersonalinfoid

--select * from @employment1
--where claimantpersonalinfoid=12802

--select * from @employment2
--where claimantpersonalinfoid=12802

--select * from @employmentFinal
--where claimantpersonalinfoid=12802


SELECT
	claimantpersonalinfoid,
	CASE
		WHEN GPCAcquisitionDate IS NULL and bill=1 THEN 'ACE, CNA, First State, Royal, Sentry, Traveler, Wausau'
		WHEN GPCAcquisitionDate <= 1979 AND COALESCE(iscolyear, 0) = 1 and bill=1 THEN 'ACE, CNA, First State, Royal, Sentry, Traveler, Wausau'
		WHEN GPCAcquisitionDate <= 1979 AND COALESCE(iscolyear, 0) = 0 and bill=1 THEN 'ACE, CNA, First State, Sentry, Traveler, Wausau'
		WHEN GPCAcquisitionDate > 1979 AND GPCAcquisitionDate <= 1982 AND COALESCE(iscolyear, 0) = 1 and bill=1 THEN 'ACE, First State, Royal, Sentry, Traveler, Wausau'
		WHEN GPCAcquisitionDate > 1979 AND GPCAcquisitionDate <= 1982 AND COALESCE(iscolyear, 0) = 0 and bill=1 THEN 'ACE, First State, Sentry, Traveler, Wausau' ELSE 'No Insurance Coverage'
	END AS Carrier
FROM @employmentFinal
WHERE claimantpersonalinfoid = @ClaimantPersonalInfoID
--WHERE claimantpersonalinfoid =12802
GROUP BY	claimantpersonalinfoid,
			CASE
				WHEN GPCAcquisitionDate IS NULL and bill=1 THEN 'ACE, CNA, First State, Royal, Sentry, Traveler, Wausau'
				WHEN GPCAcquisitionDate <= 1979 AND COALESCE(iscolyear, 0) = 1 and bill=1 THEN 'ACE, CNA, First State, Royal, Sentry, Traveler, Wausau'
				WHEN GPCAcquisitionDate <= 1979 AND COALESCE(iscolyear, 0) = 0 and bill=1 THEN 'ACE, CNA, First State, Sentry, Traveler, Wausau'
				WHEN GPCAcquisitionDate > 1979 AND GPCAcquisitionDate <= 1982 AND COALESCE(iscolyear, 0) = 1 and bill=1 THEN 'ACE, First State, Royal, Sentry, Traveler, Wausau'
				WHEN GPCAcquisitionDate > 1979 AND GPCAcquisitionDate <= 1982 AND COALESCE(iscolyear, 0) = 0 and bill=1 THEN 'ACE, First State, Sentry, Traveler, Wausau' ELSE 'No Insurance Coverage'
			END
GO
