SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROCEDURE [dbo].[qryfrmLedger_original]
(@carrierID int = null)
AS
SET NOCOUNT ON

If @carrierID= '0'
select * from tblLedger l
inner join tblcarrier c on c.CarrierID=l.CarrierID
--join tblBillPeriod bp on bp.BillPeriodID=l.BillPeriodID
--order by c.CarrierName, ledgerid desc
ORDER BY c.CarrierName,
   CASE l.BillPeriodID
      WHEN 7 THEN 1
      WHEN 1 THEN 2
      WHEN 2 THEN 3
      WHEN 3 THEN 4
      WHEN 4 THEN 5
	  WHEN 5 THEN 6
	  WHEN 6 THEN 7
Else 8 End

select * from tblLedger  l
inner join tblcarrier c on c.CarrierID=l.CarrierID
--join tblBillPeriod bp on bp.BillPeriodID=l.BillPeriodID
where c.CarrierID=@carrierID 
--order by LedgerID desc
ORDER BY c.CarrierName,
   CASE l.BillPeriodID
      WHEN 7 THEN 1
      WHEN 1 THEN 2
      WHEN 2 THEN 3
      WHEN 3 THEN 4
      WHEN 4 THEN 5
	  WHEN 5 THEN 6
	  WHEN 6 THEN 7
Else 8 End


RETURN 


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger_original] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger_original] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger_original] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger_original] TO [power_user]
GO
