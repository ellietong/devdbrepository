SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROC [dbo].[WSS_REPORTS_SUMMARY_qrySettledByPlaintiffCounsel] ( @SettlementDateStart  nvarchar(250),	@SettlementDateEnd  nvarchar(250), @MatterID int) 

as

Set NoCount On

----parameters
 --Declare @SettlementDateStart datetime
 --Declare @SettlementDateEnd datetime

 --Set  @SettlementDateStart  = '1/1/1900'
 --Set @SettlementDateEnd = '11/14/2013'



Declare @SettlementSummary table (ClaimNumber int, 
							
				SettlementAmount money,
				SettlementAverage money,
				
				Meso_Count int,
				Meso_SettlementAmount money,
				Meso_Avg money,

				LungCancer_Count int,
				LungCancer_SettlementAmount money,
				LungCancer_Avg money,

				Leukemia_Count int,
				Leukemia_SettlementAmount money,
				Leukemia_Avg money,

				OtherCancer_Count int,
				OtherCancer_SettlementAmount money,
				OtherCancer_Avg money,

				NonMalig_Count int,
				NonMalig_SettlementAmount money,
				NonMalig_Avg money,

				Other_Count int,
				Other_SettlementAmount money,

				NoDisease_Count int,
				NoDisease_SettlementAmount money,

				Null_Count int,
				Null_SettlementAmount money,
				PlaintiffCounsel nvarchar(200))


Insert Into @SettlementSummary 
Select count(distinct l.ClaimLawsuitID) as ClaimantNumber,
		coalesce(sum(SettlementAmount),0) as SettlementAmount,
	coalesce(sum(SettlementAmount),0)/count(c.ClaimID) as SettlementAverage, 
	
	
sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) as Meso_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end) as Meso_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = (1) then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) end  as Meso_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) as LungCancer_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end) as LungCancer_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = (2) then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = (2) then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) end  as LungCancer_Avg,

	sum(Case When HLD.HighLevelDiseaseID = 9 then 1 else 0 end) as Leukemia_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 9 then SettlementAmount else 0 end) as Leukemia_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = (9) then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = (9) then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 9 then 1 else 0 end) end  as Leukemia_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) as OtherCancer_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end) as OtherCancer_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) end  as OtherCancer_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID in (4,7, 8) then 1 else 0 end) as NonMalig_Count,	
	sum(Case When HLD.HighLevelDiseaseID in (4,7,8) then SettlementAmount else 0 end) as NonMalig_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID in (4,7,8) then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID in (4,7,8)then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID in (4,7,8) then 1 else 0 end) end  as NonMalig_Avg,
			
	Sum(Case When coalesce(HLD.HighLevelDiseaseID,0) IN (0, 5, 6) then 1 else 0 end) as Other_Count,	
	Sum(Case When coalesce(HLD.HighLevelDiseaseID,0) IN (0, 5, 6) then SettlementAmount else 0 end) as Other_SettlementAmount,
			
	0 as NoDisease_Count,
	0 as NoDisease_SettlementAmount,		
			
	Sum(Case When HLD.HighLevelDiseaseID is null then 1 else 0 end) as Null_Count,				
	Sum(Case When HLD.HighLevelDiseaseID  is null then SettlementAmount else 0 end) as Null_SettlementAmount,
	Coalesce(UPPER(PlaintiffCounsel), 'UNKNOWN') AS PlaintiffCounsel
	
From tblClaim c 
	LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease 
			   From tblClaimDisease CD 
			   Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
	INNER Join tblClaimLawsuit l on l.claimID = c.claimID
	INNER Join tblLawsuit law on l.lawsuitid = law.lawsuitid
	INNER Join tblCLaimantSettlement s on s.claimlawsuitID = l.claimlawsuitID
	Left Join tblDisease D on CD.DiseaseID = D.DiseaseID
	left join tblHighLevelDisease HLD on HLD.HighLevelDiseaseID=D.HighLevelDiseaseID
	INNER join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = l.ClaimLawsuitID
	LEFT JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = law.NationalPlaintiffCounselAddressID

Where 
 coalesce(settlementdate, statusdate, '1/1/1900') >= COALESCE(@SettlementDateStart, '1/1/1900') and  coalesce(settlementdate, statusdate, '1/1/1900') <= COALESCE(@SettlementDateEnd, GETDATE())
	 and ClaimLawsuitStatusID in (23)--,28, 39)
	 and coalesce(cst.IsPrimaryStatus,0) = 1
Group By PlaintiffCounsel



Select 
 	PlaintiffCounsel,
SUM(coalesce(ClaimNumber,0)) as ClaimNumber, 
	SUM(coalesce(SettlementAmount,0)) as SettlementAmount,
	SUM(coalesce(SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Claimnumber, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Claimnumber, 0)) END AS SettlementAverage,
	SUM(coalesce(Meso_Count,0)) as Meso_Count,
	SUM(coalesce(Meso_SettlementAmount,0)) as Meso_SettlementAmount,
	SUM(coalesce(Meso_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Meso_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Meso_Count, 0)) END AS Meso_Avg,
	SUM(coalesce(LungCancer_Count,0)) as LungCancer_Count,
	SUM(coalesce(LungCancer_SettlementAmount,0)) as LungCancer_SettlementAmount,
	SUM(coalesce(LungCancer_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(LungCancer_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(LungCancer_Count, 0)) END AS LungCancer_Avg,
	SUM(coalesce(Leukemia_Count,0)) as Leukemia_Count,
	SUM(coalesce(Leukemia_SettlementAmount,0)) as Leukemia_SettlementAmount,
	SUM(coalesce(Leukemia_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(OtherCancer_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(OtherCancer_Count, 0)) END AS Leukemia_Avg,	
	SUM(coalesce(OtherCancer_Count,0)) as OtherCancer_Count,
	SUM(coalesce(OtherCancer_SettlementAmount,0)) as OtherCancer_SettlementAmount,
	SUM(coalesce(OtherCancer_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(OtherCancer_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(OtherCancer_Count, 0)) END AS Othercancer_Avg,
	SUM(coalesce(NonMalig_Count,0))as NonMalig_Count,
	SUM(coalesce(NonMalig_SettlementAmount,0))as NonMalig_SettlementAmount,
	SUM(coalesce(NonMalig_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(NonMalig_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(NonMalig_Count, 0)) END AS NonMalig_Avg,
	SUM(coalesce(Other_Count,0)) as Unknown_Count,
	SUM(coalesce(Other_SettlementAmount,0)) as Unknown_SettlementAmount,
	SUM(coalesce(Other_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Other_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Other_Count, 0)) END AS Unknown_Avg
From @SettlementSummary 
GROUP BY PlaintiffCounsel

RETURN




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qrySettledByPlaintiffCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qrySettledByPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qrySettledByPlaintiffCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qrySettledByPlaintiffCounsel] TO [power_user]
GO
