SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryDocumentType] (@invoiceFlag    int = 0, 
											   @settlementFlag bit = 0)
AS
SET NOCOUNT ON

--DECLARE @invoiceFlag bit
--DECLARE @settlementFlag bit
--SET @invoiceFlag    = 0
--SET @settlementFlag = 0

IF @invoiceFlag = 0
BEGIN
	IF @settlementFlag = 1
	BEGIN
		SELECT ClaimantDocumentTypeID, 
			ClaimantDocumentType, 
			ClaimantDocumentGroupID
		FROM tblClaimantDocumentType
		WHERE ClaimantDocumentGroupID IN (1, 3, 4)
			AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <>16
		ORDER BY ClaimantDocumentType
	END
	ELSE
	BEGIN
		SELECT ClaimantDocumentTypeID, 
			ClaimantDocumentType, 
			ClaimantDocumentGroupID
		FROM tblClaimantDocumentType
		WHERE ClaimantDocumentGroupID IN (1, 3, 4)
			AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <> 16
		ORDER BY ClaimantDocumentType
	END
END
ELSE IF @invoiceFlag = 1
BEGIN
		SELECT ClaimantDocumentTypeID, 
			ClaimantDocumentType, 
			ClaimantDocumentGroupID
		FROM tblClaimantDocumentType
		WHERE ClaimantDocumentGroupID = 2 
			AND ClaimantDocumentTypeID NOT IN (41, 42, 254)
		ORDER BY ClaimantDocumentType
END
ELSE
BEGIN
		SELECT ClaimantDocumentTypeID, 
			ClaimantDocumentType, 
			ClaimantDocumentGroupID
		FROM tblClaimantDocumentType
		WHERE ClaimantDocumentGroupID = 2
			AND ClaimantDocumentTypeID NOT IN (41, 42, 254)
		ORDER BY ClaimantDocumentType
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [power_user]
GO
