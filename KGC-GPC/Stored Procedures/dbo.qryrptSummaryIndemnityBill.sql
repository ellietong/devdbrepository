SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Proc [dbo].[qryrptSummaryIndemnityBill]
(@CarrierName Nvarchar(50), @InvoiceNumber Nvarchar(50))
As
Set Nocount on

--select BillMonth, sum(TotalSettlementAmount) as Total, convert(money,sum((.0425*TotalSettlementAmount))) as CNAShare, @InvoiceNumber as InvoiceNumber
--from Bill_CombinedIndemnity_CNACatchup
--group by BillMonth
--order by right(BillMonth, 4), left(BillMonth, 2)

declare @detail table (BillingFile# nvarchar(100), OurFile# nvarchar(100), BillMonth nvarchar(100), CaseName nvarchar(100), PLast nvarchar(100), PFirst nvarchar(100), StateofFiling nvarchar(100), GPCAcquisitionDate datetime, IsColyear bit, SettleDate datetime, SettlementShareReportDate datetime, TotalSettlementAmount float, SettlementAmount float,
									CNAShare float)
									
insert @detail
select BillingFile#, c.OurFile#, BillMonth, CaseName as Plaintiff, PLast, PFirst, StateofFiling as State,GPCAcquisitionDate,IsColyear, SettleDate, SettlementShareReportDate as SettlementDate, TotalSettlementAmount as Total, SettlementAmount,
Case when  gpcacquisitiondate >=1979 and coalesce(iscolyear,0)<>1 then 0 else (totalsettlementamount *.0425) end as CNAShare
from Bill_CombinedIndemnity_CNACatchup c
left join CLAIMS_DATA d on d.InternalReferenceNumber=c.BillingFile#
group by BillingFile#, OurFile#, BillMonth, CaseName, PLast, PFirst, StateofFiling, SettleDate, SettlementShareReportDate, TotalSettlementAmount, SettlementAmount,GPCAcquisitionDate,IsColyear,
gpcacquisitiondate, iscolyear
order by right(BillMonth, 4), left(BillMonth, 2), SettlementShareReportDate

select BillMonth, sum(TotalSettlementAmount) as Total, SUM(cnashare) as CNAShare, @InvoiceNumber as InvoiceNumber
from @detail
group by BillMonth
order by right(BillMonth, 4), left(BillMonth, 2)

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryIndemnityBill] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryIndemnityBill] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryIndemnityBill] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryIndemnityBill] TO [power_user]
GO
