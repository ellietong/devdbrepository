SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qryfrmDefenseScenarioCopy]
(@OldScenario int, @Treatments bit)
as

set nocount on


----Will be input scenario
--DECLARE @OldScenario int
--SET @OldScenario = 2
--DECLARE @Treatments bit
--SET @Treatments = 0


DECLARE @NewScenario int


INSERT INTO tblScenario (Scenario, ScenarioNotes, DateCreated)
SELECT 'Copy Of ' + Scenario as Scenario, 'Copy Created ' + CONVERT(nvarchar(20), getdate(), 101) + ' of ScenarioID ' + convert(nvarchar(20), @OldScenario) + ' - ' +Scenario, GETDATE()
from tblScenario
Where ScenarioID = @OldScenario 

SET @NewScenario = (select MAX(scenarioID) from tblScenario)


INSERT INTO tblScenarioProvisions (ScenarioID,ProvisionLabelID)
SELECT @NewScenario as ScenarioID, ProvisionLabelID
FROM tblScenarioProvisions 
WHERE ScenarioID = @OldScenario


IF @Treatments = 0 
BEGIN

INSERT INTO tblCombinationTreatment (ScenarioID, Combination)

SELECT @NewScenario as ScenarioID, Combination 
FROM tblCombinationTreatment
WHERE ScenarioID = @OldScenario 
END
ELSE

INSERT INTO tblCombinationTreatment (ScenarioID, Combination, TreatmentID)

SELECT @NewScenario as ScenarioID, Combination, TreatmentID 
FROM tblCombinationTreatment
WHERE ScenarioID = @OldScenario 

return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefenseScenarioCopy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDefenseScenarioCopy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefenseScenarioCopy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDefenseScenarioCopy] TO [power_user]
GO
