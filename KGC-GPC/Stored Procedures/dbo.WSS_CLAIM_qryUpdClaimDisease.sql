SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryUpdClaimDisease] 
	-- Add the parameters for the stored procedure here
(@ClaimDiseaseID bigint, @DiseaseID int, @DiseaseDiagnosisDate datetime, @chkIsPrimaryDisease bit = 0, @Emailaddress nvarchar(250), @Specialty nvarchar(250), @DoctorID int = null, @chkIsDefenseDisease bit = 0)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

IF @chkIsPrimaryDisease <> 0
BEGIN
DECLARE @ClaimID int = (SELECT ClaimID FROM tblClaimDisease WHERE ClaimDiseaseID = @ClaimDiseaseID)

UPDATE tblClaimDisease
SET PrimaryDisease = 0
WHERE ClaimID = @ClaimID AND ClaimDiseaseID <> @ClaimDiseaseID
END

	UPDATE tblClaimDisease
	SET DiseaseID = @DiseaseID,
	    DiseaseDiagnosisDate = @DiseaseDiagnosisDate,
	    PrimaryDisease = @chkIsPrimaryDisease,
	    SPUsername = @Emailaddress,
	    DoctorID = @DoctorID
	WHERE ClaimDiseaseID = @ClaimDiseaseID
	
UPDATE tblDoctor
	SET DoctorNote = @Specialty
WHERE DoctorID = @DoctorID		
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaimDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaimDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaimDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaimDisease] TO [power_user]
GO
