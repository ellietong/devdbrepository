SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptLetterInvoiceDetail]
(@carrier nvarchar(255))
As
Set Nocount on


--declare @carrier nvarchar(255)
--set @carrier= 'Royal'

select BillingDate, PaymentDueDate, BillingDateLetterhead, BillingPeriod, InvoiceNumber, CarrierContact,
Carrier, Indemnity, defense, (TotalDefense) as TotalDefense, CarrierPercentage, pastdueamount as Arrears, UPPER(Carrier) as Cup
from tblLetterInvoiceDetail
where carrier = @carrier and invoiceentryid>29


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptLetterInvoiceDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptLetterInvoiceDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptLetterInvoiceDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptLetterInvoiceDetail] TO [power_user]
GO
