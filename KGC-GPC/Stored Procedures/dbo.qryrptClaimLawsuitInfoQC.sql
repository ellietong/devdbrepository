SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimLawsuitInfoQC] (@ClaimantPersonalInfoID int)
as

Select --Claim Info
	ClaimantPersonalInfoID,
	IsCurrentClaim,
	ClaimSequence,
	c.ClaimID, 
	cl.ClaimLawsuitID,
	cl.LawsuitID,
	IndividualDocketNumber,
	IsPrimaryForClaim, NCCReferenceID, TortType

From tblClaim c
	Left Join tblClaimLawsuit cl on c.claimID = cl.claimID
	Left Join tblClaimSequence cs on cs.claimsequenceID = c.claimsequenceID
	Left Join tblTortType t on t.torttypeid = c.torttypeid
Where c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By IsPrimaryForClaim

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [power_user]
GO
