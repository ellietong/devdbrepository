SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qryDBNDByPlaintiffsCounsel] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null,@EndDate datetime=null)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

    -- Insert statements for procedure here
select top 10 coalesce(pca.PlaintiffCounsel, 'UNKNOWN') as PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) as Count
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID=l.NationalPlaintiffCounselAddressID
join (select distinct ClaimLawsuitID, MaterialID from tblAllegation where materialID not in (11,12))a on a.ClaimLawsuitID = cl.ClaimLawsuitID
Where ClaimLawsuitStatusID in (38) and c.ClaimantPersonalInfoID <>13357
and  IsPrimaryStatus=1 
and a.MaterialID not  in (11,12)

			
group by coalesce(pca.PlaintiffCounsel, 'UNKNOWN')
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, coalesce(pca.PlaintiffCounsel, 'UNKNOWN')

END





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryDBNDByPlaintiffsCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryDBNDByPlaintiffsCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryDBNDByPlaintiffsCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryDBNDByPlaintiffsCounsel] TO [power_user]
GO
