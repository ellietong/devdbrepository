SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[WSS_SMOKING_qryClaimantSmokingHistory]
(@ClaimantPersonalInfoID bigint, @emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimantSmokingHistoryID, ClaimantPersonalInfoID, SmokingStart, SmokingEnd, COALESCE(PacksPerDay, 0) AS PacksPerDay, Notes
	FROM tblClaimantSmokingHistory
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SMOKING_qryClaimantSmokingHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SMOKING_qryClaimantSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SMOKING_qryClaimantSmokingHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SMOKING_qryClaimantSmokingHistory] TO [power_user]
GO
