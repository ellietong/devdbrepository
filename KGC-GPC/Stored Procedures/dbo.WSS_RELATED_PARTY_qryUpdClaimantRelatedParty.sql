SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty]   (@ClaimantRelatedPartyID bigint, 
																		@FirstName nvarchar(500), 
																		@MiddleName nvarchar(500), 
																		@LastName nvarchar(500), 
																		@RelationshipID int, 
																		@LegalCapacityID int, 
																		@FullSSN nvarchar(15),
																		@RPAddress1 nvarchar(250), 
																		@RPAddress2 nvarchar(250), 
																		@RPCity nvarchar(250), 
																		@StateID int, 
																		@RPZip nvarchar(5), 
																		@RPMailZip nvarchar(5),
																		@RPPhone nvarchar(20), 
																		@RPExtension nvarchar(10))
AS
SET NOCOUNT ON

UPDATE tblClaimantRelatedParty
SET FirstName = UPPER(@FirstName),
	MiddleName = UPPER(@MiddleName),
	LastName = UPPER(@LastName),
	RelationshipID = @RelationshipID, 
	LegalCapacityID = @LegalCapacityID,
	RPSocialSecurityNumber = CASE WHEN @FullSSN = '0' OR LEN(@FullSSN) = 0 THEN NULL ELSE REPLACE(REPLACE(@FullSSN, '-', ''), ' ', '')  END ,
	RPAddress1 = UPPER(@RPAddress1),
	RPAddress2 = UPPER(@RPAddress2),
	RPCity = UPPER(@RPCity),
	StateID = @StateID,
	RPZip = @RPZip,
	RPMailZip = @RPMailZip,
	RPPhone = CONVERT(bigint, REPLACE(REPLACE(REPLACE(@RPPhone, '-', ''), '(', ''), ')', '')),
	RPExtension = @RPExtension
WHERE ClaimantRelatedPartyID = @ClaimantRelatedPartyID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [power_user]
GO
