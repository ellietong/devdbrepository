SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_REPORTS_qryOutstandingSettlementGroups]  (@MatterID int, @SettlementDate datetime)

As

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 1
--DECLARE @SettlementDate datetime
--SET @SettlementDate = '1/1/2000'

DECLARE @MaxMatterID int
SELECT @MaxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @SettlementGroupPaidUnpaid TABLE (SettlementGroupID int, PlaintiffCounselID int, Paid money, PaidCount int, ClaimCount int, Total money)
INSERT INTO @SettlementGroupPaidUnpaid

SELECT SettlementGroupID, PlaintiffCounselID, SUM(CASE WHEN IsFullyPaid <> 1 OR SourceNumber IS NULL THEN COALESCE(PaymentAmount, 0) ELSE COALESCE(SettlementAmount, 0) END) ASPaid,
	   COUNT(PaymentAmount) AS PaidCount, COUNT(SettlementAmount) AS ClaimCount,
	   SUM(SettlementAmount) AS Total	
FROM tblClaimantSettlement cs
	INNER JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblClaim c ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
	Inner JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	LEFT JOIN tblPaymentSettlement ps ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
	LEFT JOIN tblCheck chk ON ps.CheckID = chk.CheckID
WHERE COALESCE(c.IsCurrentClaim,0) = 1 AND c.MatterID >= COALESCE(@MatterID, 0) 
	AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
GROUP BY SettlementGroupID, COALESCE(c.IsCurrentClaim,0), PlaintiffCounselID
ORDER BY SettlementGroupID

DECLARE @SettlementGroupMaxDate TABLE (SettlementGroupID int, MaxSettlementDate datetime)
INSERT INTO @SettlementGroupMaxDate
	
SELECT SettlementGroupID, MAX(SettlementDate) AS MaxSettlementDate
	
FROM tblClaimantSettlement
	GROUP BY SettlementGroupID 
	ORDER BY MaxSettlementDate

SELECT CONVERT(bigint,CONVERT(nvarchar(25),pl.PlaintiffCounselID) + '111' + CONVERT(nvarchar(25),s.SettlementGroupID)) AS GroupAndFirmID, PlaintiffCounsel, cs.SettlementDate, s.SettlementGroupID, s.SettlementGroup, COALESCE(PaidCount,0) AS PaidCount, ClaimCount, COALESCE(Paid, 0) AS Paid, COALESCE(Total, 0) AS Total
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
	INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INner JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
	Inner JOIN tblPlaintiffCounselAddress pl ON l.PlaintiffCounselAddressID = pl.PlaintiffCounselAddressID
	INNER JOIN tblClaimLawsuitStatusTracking cls		ON cl.ClaimLawsuitID = cls.ClaimLawsuitID	
	LEFT JOIN tblClaimLawsuitStatus cst ON cst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	LEFT JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblClaimantDocketNumbers cd		ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
	LEFT  JOIN tblSettlePaymentDocumentatiON spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN @SettlementGroupPaidUnpaid sgp	ON s.SettlementGroupID = sgp.SettlementGroupID AND sgp.PlaintiffCounselID = pl.PlaintiffCounselID
	LEFT JOIN @SettlementGroupMaxDate sgm		ON s.SettlementGroupID = sgm.SettlementGroupID 
	LEFT JOIN  tblReleASetype crr	ON crr.ReleASeTypeID =spd.ReleASeTypeID
	LEFT JOIN tblClaimSequence csq	ON c.ClaimSequenceID = csq.ClaimSequenceID
	LEFT JOIN tblPaymentSettlement ps ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
	LEFT JOIN tblCheck k ON k.CheckID =  ps.CheckID
WHERE COALESCE(IsCurrentClaim,0) = 1 
	AND COALESCE(cls.isprimarystatus,0) = 1
	AND c.MatterID >= COALESCE(@MatterID, 0) 
	AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	AND COALESCE(PaidCount,0) <> COALESCE(ClaimCount,0)
--AND COALESCE(cpi.jobsiteinfo, 0) = 0  
GROUP BY PlaintiffCounsel, cs.SettlementDate, s.SettlementGroupID, s.SettlementGroup, ClaimCount, PaidCount, sgp.Paid, sgp.Total, pl.PlaintiffCounselID
ORDER BY SettlementGroupID, PlaintiffCounsel, SettlementDate--, SettlementGroup
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementGroups] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementGroups] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementGroups] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementGroups] TO [power_user]
GO
