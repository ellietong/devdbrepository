SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_qryFirmDocuments](@emailaddress nvarchar (1000)=null)
AS

SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'scelba@celba.com'

DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM  vtblPerson WHERE Email = @emailaddress)

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40) AND HistoricalDocumentID IS NULL AND COALESCE(IsPrimaryForClaim, 0) = 1 AND COALESCE(IsCurrentClaim, 0) = 1'


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF (@DefenseCounselID IN (132, 133)) OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com')
	SET @JoinString = ' '
ELSE IF (@DefenseCounselID IN (10, 114) OR @emailaddress = 'scelba@celba.com')
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND cdt.ClaimantDocumentGroupID <> 4'
END
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')
										AND cdt.ClaimantDocumentGroupID <> 4'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT d.DocumentID, 
		d.DocumentTitle, 
		cdt.ClaimantDocumentType, 
		d.DocumentURL AS DirectDocumentURL, 
		d.DateCreated, 
		d.AcceptedBy, 
		d.DateAccepted, 
		dca.DefenseCounsel + '' ('' + DefenseCounselCity + '')'' AS Firm, 
		Accepted,
		''/GPC/cp/Pages/DocumentProfile.aspx?ItemId='' + CONVERT(nvarchar(100), d.DocumentID) + ''&source='' + ''/GPC/cp/Pages/Documents.aspx'' AS DocumentURL		
	 FROM dbo.tblDocuments d 
		INNER JOIN tblMultiClaimantDocument mcd		ON d.DocumentID = mcd.DocumentID
		INNER JOIN dbo.tblClaimantDocumentType cdt	ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
		LEFT  JOIN tblClaim c						ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT  JOIN tblClaimLawsuit cl				ON c.ClaimID = cl.ClaimID
		LEFT  JOIN tblLawsuit l						ON cl.LawsuitID = l.LawsuitID
		LEFT  JOIN tblDefenseJurisdiction dj		ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID 
		LEFT  JOIN tblDefenseCounselAddress dca		ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID ' +
		@JoinString +
     @WhereString + '
	 ORDER BY Accepted ASC, DateCreated DESC
	 OPTION (RECOMPILE)'
	 
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments] TO [power_user]
GO
