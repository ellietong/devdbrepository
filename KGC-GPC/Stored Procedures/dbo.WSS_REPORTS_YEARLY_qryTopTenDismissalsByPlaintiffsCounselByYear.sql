SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @YearID int
--Set @YearId=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--declare @StartDate datetime=null, @EndDate datetime=null


    -- Insert statements for procedure here
select coalesce(pca.PlaintiffCounsel, 'UNKNOWN') as PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) as Count
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID=l.NationalPlaintiffCounselAddressID

join (select distinct ClaimLawsuitID, MaterialID from tblAllegation where materialID not in (11,12))a on a.ClaimLawsuitID = cl.ClaimLawsuitID

where coalesce(Statusdate, '1/1/1900') between coalesce(@StartDate,'1/1/1900') and coalesce(@EndDate, '1/1/2100')
		and ClaimLawsuitStatusID in (15, 16, 17, 18, 26, 27, 19, 30, 42)
		and  IsPrimaryStatus=1
		and c.ClaimantPersonalInfoID <>13357 and a.MaterialID not  in (11,12)

group by pca.PlaintiffCounsel
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, pca.PlaintiffCounsel

END






GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [power_user]
GO
