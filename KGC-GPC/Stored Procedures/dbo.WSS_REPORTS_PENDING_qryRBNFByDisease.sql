SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_REPORTS_PENDING_qryRBNFByDisease] (@MatterID int = NULL, 
														 @EndDate datetime=null)
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MatterID int
--SET @MatterID = NULL
--DECLARE @EndDate datetime
--SET @EndDate = '12/12/2012'

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with ProcessedDate, 
--      and all original ProcessedDate references have been replaced with ProcessedDate.

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)


DECLARE @PendingDate date
SET @PendingDate = @EndDate

SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END AS HighLevelDisease, 
	COUNT(DISTINCT(cl.ClaimLawsuitID)) AS 'Count'
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
WHERE ProcessedDate < @PendingDate
	AND ClaimLawsuitStatusID IN (34) 
	AND IsPrimaryForClaim = 1 
	AND IsPrimaryStatus = 1
	AND cd.PrimaryDisease = 1
	AND MatterID  >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
ORDER BY hld.HighLevelDiseaseID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByDisease] TO [power_user]
GO
