SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptSummaryDefenseBill_AllCarriers_20130423]
(@CarrierName Nvarchar(50), @InvoiceNumber Nvarchar(50))
As
Set Nocount on

--declare @CarrierName Nvarchar(50)
--declare @InvoiceNumber Nvarchar(50)

--set @CarrierName= 'First State'
--set @InvoiceNumber = 'GPC20120831'


declare @Data table (file# int, code nvarchar(250), plaintiff nvarchar(100), disease nvarchar(50), Jurisdiction nvarchar(100),
 State nvarchar(5),LocalCounsel nvarchar(500), invoiceid_lawfirm nvarchar(100), invoicedate date, fees float, costs float, billedbyRiley bit,
 invoiceapproveddate date, motiononly int)
 
insert into @Data
select coalesce(c.file#, 0), Coalesce(CODE,'0'),
case when MotionOnly = 1 then 'Unknown Plaintiff' 
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County: Asbestos'
	 when CODE IN ('20531', '19822399') and NCC=0 then 'Master Asbestos File'
	 else PLast+', '+PFirst end as Plaintiff, 
	 Disease, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County'	 
	 else c.Jurisdiction end as Jurisdiction, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
	 else c.State end as State,	 	 
d.LawFirmOffice,InvoiceID_LawFirm, d.InvoiceDate,  
sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
BilledByRiley, InvoiceApprovedDate, coalesce(motiononly,0)

from CounselLink_Data_ToBill d
left join (select plaintiff, newfileno, file#, billedbyriley 
				from Claims_AdditionalClaimNumbers 
				where file# is not null
				group by plaintiff, newfileno, file#, billedbyriley) a on a.NewFileNo = d.CODE
left join CLAIMS_DATA_UPDATED c on c.file#=a.file#

where
--coalesce(DoNotBill,0)=0 --Excludes Alston Bird
 coalesce(alreadybilled,0)=0 --Excludes invoices we can tell Riley already billed
and InvoiceApprovedDate is not null --Excludes invoices that have not been approved
and ncc=0 --and coalesce(donotbillsentry,0)<>1
group by coalesce(c.file#, 0), CODE, InvoiceID_LawFirm, d.InvoiceDate, lawfirmoffice, ChargeType, 
case when MotionOnly = 1 then 'Unknown Plaintiff'
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County: Asbestos'
	 when CODE IN ('20531', '19822399') and NCC=0 then 'Master Asbestos File'
	 else PLast+', '+PFirst end,
 Disease, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County'	 
	 else c.Jurisdiction end, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
	 else c.State end , BilledByRiley, InvoiceApprovedDate,coalesce(motiononly,0)

--Union ALL

--select coalesce(c.file#, 0), Coalesce(CODE,'0'),
--case when MotionOnly = 1 then 'Unknown Plaintiff' 
--	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
--	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
--	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
--	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
--	 when CODE IN ('20531', '19822399') and NCC=0 then 'Master Asbestos File'
--	 else PLast+', '+PFirst end as Plaintiff, 
--	 Disease, 
--case when MotionOnly = 1 then ''
--	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
--	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
--	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
--	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
--	 else c.Jurisdiction end as Jurisdiction, 
--case when MotionOnly = 1 then ''
--	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
--	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
--	 else c.State end as State,	 	 
--d.LawFirmOffice,InvoiceID_LawFirm, d.InvoiceDate,  
--sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
--sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
--BilledByRiley, InvoiceApprovedDate,coalesce(motiononly,0)

--from CounselLink_Data2 d
--left join (select plaintiff, newfileno, file#, billedbyriley 
--				from Claims_AdditionalClaimNumbers 
--				where file# is not null
--				group by plaintiff, newfileno, file#, billedbyriley) a on a.NewFileNo = d.CODE
--left join CLAIMS_DATA c on c.file#=a.file#

--where
--coalesce(DoNotBill,0)=0 --Excludes Alston Bird
--and InvoiceApprovedDate is not null --Excludes invoices that have not been approved
--and NCC=0
--group by coalesce(c.file#, 0), CODE, InvoiceID_LawFirm, d.InvoiceDate, lawfirmoffice, ChargeType, 
--case when MotionOnly = 1 then 'Unknown Plaintiff'
--	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
--	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
--	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
--	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
--	 when CODE IN ('20531', '19822399') and NCC=0 then 'Master Asbestos File'
--	 else PLast+', '+PFirst end,
-- Disease, 
--case when MotionOnly = 1 then ''
--	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
--	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
--	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
--	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
--	 else c.Jurisdiction end, 
--case when MotionOnly = 1 then ''
--	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
--	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
--	 else c.State end, BilledByRiley, InvoiceApprovedDate,coalesce(motiononly,0)
	 

	 
	 
declare @final table (file# int, code nvarchar(250), plaintiff nvarchar(100), disease nvarchar(50), Jurisdiction nvarchar(100),
 State nvarchar(5),LocalCounsel nvarchar(500), invoiceid_lawfirm nvarchar(100), invoicedate date, fees float, costs float, billedbyRiley bit, invoiceapproveddate date, motiononly int, BillPeriod nvarchar(50),
 FirstStateFees float, FirstStateCosts float, RoyalFees float, RoyalCosts float, SentryFees float, SentryCosts float, TravelersFees float, TravelersCosts float, WausauFees float, WausauCosts float,
 InternationalFees float, InternationalCosts float, CNAFees float, CNACosts float)
 
 insert into @final

--NOW PULL IN INSURER SHARES

Select D.*, case when invoiceapproveddate between '10/1/2012' and '10/31/2012' then 'October 2012'
			when invoiceapproveddate between '11/1/2012' and '11/30/2012' then 'November 2012'
			when invoiceapproveddate between '12/1/2012' and '12/31/2012' then 'December 2012' end as BillPeriod,
--FIRST STATE
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.0357)*fees 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0357)*fees)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .0357*fees end as FirstStateFees,
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.0357)*Costs 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0357)*Costs)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .0357*Costs end as FirstStateCosts,
--ROYAL		
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.colyear)/TotalYearsOfExposure)*.0215)*fees 
	 when BilledByRiley = 0 and Coalesce(IsColyear,0)=1 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0215)*fees)
	 when ((BilledByRiley = 0 and Coalesce(iscolyear,0)= 1 and GPCAcquisitionDate >1986) Or coalesce(IsColyear,0) <>1) then 0
		else .0215*fees end as RoyalFees,	
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.colyear)/TotalYearsOfExposure)*.0215)*Costs 
	 when BilledByRiley = 0 and Coalesce(IsColyear,0)=1 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0215)*Costs)
	 when ((BilledByRiley = 0 and Coalesce(iscolyear,0) = 1 and GPCAcquisitionDate >1986) Or Coalesce(IsColyear,0) <>1) then 0
		else .0215*Costs end as RoyalCosts,		
--SENTRY		
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*fees 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*fees)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*fees end as SentryFees,
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*Costs 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*Costs)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*Costs end as SentryCosts,
--TRAVELERS		
case when motiononly = 1 then 0
	 when BilledByRiley = 1 and invoiceapproveddate <='5/31/2012' then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.1275)*fees 
	 when BilledByRiley = 1 and invoiceapproveddate >'5/31/2012' then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.085)*fees 
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
	 when BilledByRiley = 0 and invoiceapproveddate <='5/31/2012' then .1275*fees	
		else .085*fees end as TravelersFees,
case when motiononly = 1 then 0
	 when BilledByRiley = 1 and invoiceapproveddate <='5/31/2012' then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.1275)*Costs 
	 when BilledByRiley = 1 and invoiceapproveddate >'5/31/2012' then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.085)*Costs 
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
	 when BilledByRiley = 0 and invoiceapproveddate <='5/31/2012' then .1275*Costs	
		else .085*Costs end as TravelersCosts,
--WAUSAU
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*fees 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*fees)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*fees end as WassauFees,
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*Costs 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*Costs)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*Costs end as WassauCosts,
--ACE		
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >1986 then 0
	 when invoiceapproveddate <='5/31/2012' then 0
		else .0425*fees end as InternationalFees,	
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >1986 then 0
	 when invoiceapproveddate <='5/31/2012' then 0
		else .0425*Costs end as InternationalCosts,	
--HARBOR/CNA		
case when motiononly = 1 then 0
	 when coalesce(GPCAcquisitionDate, 0) <=1979 then .0425*fees else 0 end as CNAFees,
case when motiononly = 1 then 0
	 when coalesce(GPCAcquisitionDate, 0) <=1979 then .0425*Costs else 0 end as CNACosts								

From @data d
left join CLAIMS_DATA_UPDATED cd on cd.File#=d.file#
left join RileyBillingPercentages r on r.file#=d.file#


Declare @summary table (billPeriod nvarchar(50), fees float, costs float, insurershareFees float, InsurerShareCosts float)
insert into @summary

select billPeriod, SUM(fees) as fees, SUM(costs) as costs, 
case
when  @CarrierName ='First State' then sum(FirstStateFees) 
when @CarrierName ='Royal' then sum(RoyalFees)
when @CarrierName ='Sentry' then sum(SentryFees)
when @CarrierName ='Travelers' then sum(TravelersFees)
when @CarrierName ='Wausau' then sum(WausauFees)
when @CarrierName ='International' then sum(InternationalFees)
when @CarrierName ='CNA' then sum(CNAFees) end as InsurerShareFees, 
case
when  @CarrierName ='First State' then sum(FirstStateCosts)
when @CarrierName ='Royal' then sum(RoyalCosts)
when @CarrierName ='Sentry' then sum(SentryCosts)
when @CarrierName ='Travelers' then sum(TravelersCosts)
when @CarrierName ='Wausau' then sum(WausauCosts)
when @CarrierName ='International' then sum(InternationalCosts)
when @CarrierName ='CNA' then sum(CNACosts) end as InsurerShareCosts 
from @final
group by BillPeriod
order by MONTH(BillPeriod)

--select * from @summary


Declare @data2 table (invoicedate date, invoicenumber nvarchar(100), LocalCounsel nvarchar(100),
TotalFees money, TotalCosts money, Invoiceapproveddate date)
insert into @data2
Select InvoiceDate, InvoiceID_LawFirm, LawFirmOffice, 
sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
InvoiceApprovedDate
from CounselLink_Data_ToBill
where --Coalesce(DoNotBill,0)=0 
 coalesce(AlreadyBilled, 0)=0
and InvoiceApprovedDate is not null
and coalesce(code, '0') in ('20531', '19822399')
and NCC=1
group by invoicedate, invoiceid_lawfirm, lawfirmoffice, InvoiceApprovedDate

--union

--Select InvoiceDate, InvoiceID_LawFirm, LawFirmOffice, 
--sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
--sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
--InvoiceApprovedDate
--from CounselLink_Data2
--where Coalesce(DoNotBill,0)=0 
--and InvoiceApprovedDate is not null
--and coalesce(code, '0') in ('20531', '19822399')
--and NCC=1
--group by invoicedate, invoiceid_lawfirm, lawfirmoffice, InvoiceApprovedDate

declare @NCCfinal table (invoicedate date, invoicenumber nvarchar(100), LocalCounsel nvarchar(100),
TotalNCCFees money, TotalNCCCosts money, Invoiceapproveddate date, billperiod nvarchar(50), FirstStateNCCFees money, FirstStateNCCCosts money, RoyalNCCFees money, RoyalNCCCosts money,
SentryNCCFees money, SentryNCCCosts money, TravelersNCCFees money, TravelersNCCCosts money, WausauNCCFees money, WausauNCCCosts money, ACENCCFees money, ACENCCCosts money, 
CNANCCFees money, CNANCCcosts money)

insert into @NCCfinal
select d.*, case when invoiceapproveddate between '10/1/2012' and '10/31/2012' then 'October 2012'
			when invoiceapproveddate between '11/1/2012' and '11/30/2012' then 'November 2012'
			when invoiceapproveddate between '12/1/2012' and '12/31/2012' then 'December 2012' end as BillPeriod,
--FIRST STATE
.0357*TotalFees as FirstStateFees, .0357*TotalCosts as FirstStateCosts,
--ROYAL
.0215*TotalFees as RoyalFees, .0215*TotalCosts as RoyalCosts,
--SENTRY
.1275*TotalFees as SentryFees, .1275*TotalCosts as SentryCosts,
--TRAVELERS
case when Invoiceapproveddate <='5/31/2012' then .1275*TotalFees else .085*TotalFees end as TravelersFees, 
case when Invoiceapproveddate <='5/31/2012' then .1275*TotalCosts else .085*TotalCosts end as TravelersCosts,
--WAUSAU
.1275*TotalFees as WausauFees, .1275*TotalCosts as WausauCosts,
--ACE
Case when Invoiceapproveddate >='5/31/2012' then .0425*TotalFees else 0 end as ACEFees, 
Case when Invoiceapproveddate >='5/31/2012' then .0425*TotalCosts else 0 end as ACECosts,
--HARBOR/CNA
.0425*TotalFees as HarborFees, .0425*TotalCosts as HarborFees
from @data2 d

order by LocalCounsel, invoicedate, invoicenumber



Declare @NCCSUmmary table (billPeriod nvarchar(50), TotalNCCFees money, TotalNCCCosts money, InsurerShareNCCFees money, InsurerShareNCCCosts money)
insert into @NCCSUmmary

select billPeriod, SUM(TotalNCCFees) as TotalNCCFees, SUM(TotalNCCCosts) as TotalNCCCosts, 
case
when  @CarrierName ='First State' then sum(FirstStateNCCFees)
when @CarrierName ='Royal' then sum(RoyalNCCFees)
when @CarrierName ='Sentry' then sum(SentryNCCFees)
when @CarrierName ='Travelers' then sum(TravelersNCCFees)
when @CarrierName ='Wausau' then sum(WausauNCCFees)
when @CarrierName ='International' then sum(ACENCCFees)
when @CarrierName ='CNA' then sum(CNANCCFees) end as InsurerShareNCCFees, 
case
when  @CarrierName ='First State' then sum(FirstStateNCCCosts)
when @CarrierName ='Royal' then sum(RoyalNCCCosts)
when @CarrierName ='Sentry' then sum(SentryNCCCosts)
when @CarrierName ='Travelers' then sum(TravelersNCCCosts)
when @CarrierName ='Wausau' then sum(WausauNCCCosts)
when @CarrierName ='International' then sum(ACENCCCosts)
when @CarrierName ='CNA' then sum(CNANCCCosts) end as InsurerShareNCCCosts 
from @NCCfinal
group by billperiod


select s.Billperiod, coalesce(fees,0) as fees, coalesce(costs,0) as costs,  coalesce(TotalNCCFees,0) as TotalNCCFees, coalesce(TotalNCCCosts,0) as TotalNCCCosts ,  coalesce(insurershareFees,0) as insurershareFees, coalesce(InsurerShareCosts,0) as InsurerShareCosts, 
coalesce(InsurerShareNCCFees,0) as InsurerShareNCCFees, coalesce(InsurerShareNCCCosts,0) as InsurerShareNCCCosts, @InvoiceNumber as InvoiceNumber, @CarrierName as CarrierName
from @summary s
left join @NCCSUmmary ncc on ncc.Billperiod=s.Billperiod
group by s.Billperiod, fees, costs,  TotalNCCFees, TotalNCCCosts,  insurershareFees, InsurerShareCosts, InsurerShareNCCFees, InsurerShareNCCCosts
order by MONTH(s.billPeriod)

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryDefenseBill_AllCarriers_20130423] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryDefenseBill_AllCarriers_20130423] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryDefenseBill_AllCarriers_20130423] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryDefenseBill_AllCarriers_20130423] TO [power_user]
GO
