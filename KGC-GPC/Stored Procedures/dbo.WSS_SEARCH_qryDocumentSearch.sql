SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SEARCH_qryDocumentSearch] (@ClaimantName			nvarchar(max), 
													  @DocumentTitle		nvarchar(max), 
													  @DocumentTypeID		int,
													  --@FirmID				int,
													  @CreatedBy			nvarchar(250), 
													  @DateCreatedStart		datetime, 
													  @DateCreatedEnd		datetime, 
													  @DateSettlementStart		datetime, 
													  @DateSettlementEnd		datetime, 
													  @IncludeHistorical	bit,
													  @ClaimantIDs			integer_list_tbltype READONLY,
													  @emailaddress			nvarchar(250))
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantName      nvarchar(max)
--DECLARE @DocumentTitle     nvarchar(max)
--DECLARE @DocumentTypeID    int
--DECLARE @CreatedBy         nvarchar(250)
--DECLARE @DateCreatedStart  datetime
--DECLARE @DateCreatedEnd    datetime
--DECLARE @IncludeHistorical bit
--DECLARE @ClaimantIDs       TABLE (n int) 
--DECLARE @emailaddress      nvarchar(250)
--SET @ClaimantName      = NULL
--SET @DocumentTitle     = NULL
--SET @DocumentTypeID    = 8
--SET @CreatedBy         = NULL
--SET @DateCreatedStart  = NULL
--SET @DateCreatedEnd    = NULL
--SET @IncludeHistorical = 0
----SET @ClaimantIDs     = NULL
--SET @emailaddress      = 'cpegler@roecassidy.com'--'MDCochran@mdwcg.com'--'snyderd@kcicllc.com'  --'cpegler@roecassidy.com'
--DECLARE @DateSettlementStart		datetime
--DECLARE @DateSettlementEnd		datetime
--SET @DateSettlementStart		= NULL
--SET @DateSettlementEnd		= NULL
 

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

IF object_id('tempdb..#AccessibleClaimants') IS NOT NULL DROP TABLE #AccessibleClaimants
CREATE TABLE #AccessibleClaimants (DocumentID int, ClaimantName nvarchar(max))

IF object_id('tempdb..#DocsToConsider') IS NOT NULL DROP TABLE #DocsToConsider
CREATE TABLE #DocsToConsider (DocumentID int, DocumentTitle nvarchar(4000),  ClaimantDocumentType nvarchar(100),CreatedBy nvarchar(250), DateCreated datetime, AcceptedBy nvarchar(250), DateAccepted datetime, AmazonKey nvarchar(max), IsHistorical bit)

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
IF object_id('tempdb..#ClaimantIDs') IS NOT NULL DROP TABLE #ClaimantIDs 
CREATE TABLE #ClaimantIDs (n int)
INSERT INTO #ClaimantIDs
	SELECT n FROM @ClaimantIDs
	
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @SelectStringAccessibleClaimants nvarchar(max)
DECLARE @GroupByAccessibleClaimants nvarchar(max)
DECLARE @SelectStringDocsToConsider nvarchar(max)
DECLARE @JoinString nvarchar(max)
DECLARE @WhereStringClaimants nvarchar(max)
DECLARE @WhereStringClaimantName nvarchar(max)
DECLARE @WhereStringDocuments nvarchar(max)
DECLARE @QueryString nvarchar(max)

SET @SelectStringAccessibleClaimants = 'INSERT INTO #AccessibleClaimants
											SELECT mcd.DocumentID, 
												''<a target="_blank" href="/clients/GPC/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + ''">'' + (COALESCE(LastName, '''') + '', '' + COALESCE(FirstName, '''') + '' ('' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + '')'') + ''</a>''
											FROM tblMultiClaimantDocument mcd
												INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID '

SET @GroupByAccessibleClaimants =	   'GROUP BY mcd.DocumentID, 
											''<a target="_blank" href="/clients/GPC/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + ''">'' + (COALESCE(LastName, '''') + '', '' + COALESCE(FirstName, '''') + '' ('' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + '')'') + ''</a>'',
											LastName, FirstName	
										ORDER BY LastName, FirstName '

SET @SelectStringDocsToConsider =	   'INSERT INTO #DocsToConsider
											SELECT DISTINCT d.DocumentID,
												DocumentTitle,
												ClaimantDocumentType,
												CreatedBy,
												DateCreated,
												AcceptedBy,
												DateAccepted,
												AmazonKey,
												--REPLACE(AmazonKey, '''''''', ''\'''''') AS AmazonKey,
												IsHistorical
											FROM tblDocuments d 
												INNER JOIN tblClaimantDocumentType cdt  ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
												INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
												INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID '

SET @JoinString = ' '
SET @WhereStringClaimantName = ' '
SET @WhereStringClaimants = ' WHERE 1=1 '
SET @WhereStringDocuments = ' WHERE 1=1 '

IF @ClaimantName	  IS NOT NULL           BEGIN SET @WhereStringClaimantName = @WhereStringClaimantName + ' AND ClaimantNames like ''%'' + @ClaimantNameParam + ''%'' ' END
IF (SELECT COUNT(n) FROM #ClaimantIDs) <> 0 BEGIN SET @WhereStringClaimants = @WhereStringClaimants + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)' END

IF (@DateSettlementStart  IS NOT NULL 
   OR @DateSettlementEnd IS NOT NULL )
   BEGIN 
	SET @WhereStringDocuments = @WhereStringDocuments + ' AND clst.claimlawsuitstatusID = 23 AND  COALESCE(CONVERT(date,cset.SettlementDate), ''1/1/1900'') >= ''' + CONVERT(nvarchar,COALESCE(@DateSettlementStart, '1/1/1900'))+''' AND COALESCE(CONVERT(date,cset.SettlementDate), CONVERT(date, GETDATE())) <= ''' + CONVERT(nvarchar,COALESCE(@DateSettlementEnd,CONVERT(date, GETDATE())))+'''' 
	SET @JoinString = ' INNER JOIN tblClaim c                  ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						INNER JOIN tblClaimLawsuit cl          ON c.ClaimID = cl.ClaimID
						INNER join tblClaimantSettlement cset		on cset.ClaimLawsuitID = cl.ClaimLawsuitID
						INNER JOIN tblClaimLawsuitStatusTracking clst on clst.claimlawsuitID = cl.claimlawsuitID'
   END

IF @DocumentTitle	  IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND DocumentTitle LIKE ''%'' + @DocumentTitleParam + ''%''' END
IF @DocumentTypeID	  IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND d.ClaimantDocumentTypeID = ' + CONVERT(nvarchar(20), @DocumentTypeID) END
IF @CreatedBy		  IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND d.CreatedBy LIKE ''%'' + @CreatedByParam + ''%''' END
IF @DateCreatedStart  IS NOT NULL 
   OR @DateCreatedEnd IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND COALESCE(CONVERT(date,d.DateCreated), ''1/1/1900'') >= ''' + CONVERT(nvarchar,COALESCE(@DateCreatedStart, '1/1/1900'))+''' AND COALESCE(CONVERT(date,d.DateCreated), CONVERT(date, GETDATE())) <= ''' + CONVERT(nvarchar,COALESCE(@DateCreatedEnd,CONVERT(date, GETDATE())))+'''' END
IF @IncludeHistorical IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND CASE WHEN IsHistorical IS NULL THEN 0 ELSE 1 END <= ' + CONVERT(nvarchar(20),@IncludeHistorical) END

IF((RIGHT(@emailaddress, 11) <> 'kcicllc.com' OR RIGHT(@emailaddress, 8) <> 'kcic.com') AND ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) <> 1))
BEGIN
	SET @WhereStringClaimants = @WhereStringClaimants + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @JoinString = @JoinString + ' 
						INNER JOIN tblClaim c                  ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						INNER JOIN tblClaimLawsuit cl          ON c.ClaimID = cl.ClaimID
						INNER JOIN tblLawsuit l                ON cl.LawsuitID = l.LawsuitID
						INNER JOIN tblDefenseJurisdiction dj   ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID '
	SET @QueryString = @SelectStringAccessibleClaimants + 
					   @JoinString + 
					   @WhereStringClaimants + 
					   @GroupByAccessibleClaimants +
					   @SelectStringDocsToConsider +
					   @WhereStringDocuments +
					   ' AND d.DocumentID IN(SELECT ac.DocumentID FROM #AccessibleClaimants ac) '			   
END
ELSE
BEGIN
	SET @QueryString = @SelectStringDocsToConsider +
				
					   @JoinString + 
					   @WhereStringDocuments +
					   @SelectStringAccessibleClaimants +  
					   @WhereStringClaimants + 
					   ' AND DocumentID IN(SELECT dtc.DocumentID FROM #DocsToConsider dtc) ' +
					   @GroupByAccessibleClaimants			   	
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
SET @QueryString = @QueryString + ' 
	SELECT DISTINCT d.DocumentID,
		AmazonKey,
		--REPLACE(AmazonKey, '''''''', ''\'''''') AS AmazonKey,
		''/clients/GPC/cp/Pages/DocumentProfile.aspx?ItemId='' + CONVERT(nvarchar(100), d.DocumentID) + ''&source='' + ''/clients/GPC/cp/Pages/Search.aspx'' AS DocumentURL,
		DocumentTitle,
		ClaimantDocumentType AS DocumentType,
		COALESCE(ClaimantNames, '''') AS Claimants,
		DateCreated,
		CreatedBy  + ''</br>'' + ''('' + CONVERT(nvarchar,CONVERT(DATE, DateCreated))  + '')'' AS Created,
		AcceptedBy + ''</br>'' + ''('' + CONVERT(nvarchar,CONVERT(DATE, DateAccepted)) + '')'' AS Accepted,
		IsHistorical 
		
	FROM #DocsToConsider d
		CROSS APPLY (SELECT ClaimantName + ''</br> ''
					 FROM #AccessibleClaimants ac
					 WHERE ac.DocumentID = d.DocumentID
					 FOR XML PATH('''') ) Q ( ClaimantNames )
	WHERE d.DocumentID IN(SELECT ac.DocumentID FROM #AccessibleClaimants ac) ' +
		@WhereStringClaimantName + '
	ORDER BY DateCreated DESC
	OPTION (RECOMPILE)'

PRINT @Querystring

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@ClaimantNameParam nvarchar(max), @DocumentTitleParam nvarchar(max), @CreatedByParam nvarchar(250)', @ClaimantNameParam = @ClaimantName, @DocumentTitleParam = @DocumentTitle, @CreatedByParam = @CreatedBy


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [power_user]
GO
