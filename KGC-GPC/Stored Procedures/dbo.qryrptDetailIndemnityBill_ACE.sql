SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptDetailIndemnityBill_ACE]
(@CarrierName Nvarchar(50), @InvoiceNumber Nvarchar(50),@billtype nvarchar(1), @BillStartDate datetime,@BillEndDate datetime )
As
Set Nocount on


--declare @CarrierName Nvarchar(50)
--declare @InvoiceNumber Nvarchar(50)
--declare @billtype Nvarchar(1)
--declare @BillStartDate datetime
--declare @BillEndDate datetime
--set @CarrierName= 'ACE'
--set @InvoiceNumber = 'gpc4443'
--set @billtype='d'
--set @BillStartDate ='4/1/2012'
--set @BillEndDate ='12/31/2012'


Declare @Employment table (claimantpersonalinfoid bigint, iscolyear int, GPCAcquisitionDate int, FirstExposure int)
insert @Employment

select ClaimantPersonalInfoID,  
max(case when PriorOwner like '%colyear%' then 1
else 0 end) as iscolyear,
MIN(gpcacquisitionyear) as GPCAcquisitionDate,
Year(Case when min(coalesce(ce.empstartdate,'1/1/2100'))<=
MIN(coalesce(cp.startdate,'1/1/2100')) then min(ce.empstartdate)
else MIN(cp.startdate) end) as DOFE
from tblClaimantEmployment ce
left join tblState s on s.StateID=ce.JobSiteStateID
left join tblGPCEntityAcquisition gea on gea.State=s.State
left join tblClaimantProduct cp on cp.ClaimantEmploymentID=ce.ClaimantEmploymentID
group by ClaimantPersonalInfoID



declare @data table (BillPeriod Nvarchar(50) , FileNumber Nvarchar(50), plaintiff Nvarchar(50), StateOfFiling Nvarchar(50), disease nvarchar(50), ss# nvarchar(50), DOB datetime, gender nvarchar(10),  SettlementDate datetime, Amount float, gpcacquisitiondate Nvarchar(50), 
DOFE Nvarchar(50), iscolyear bit, [First State] float, Royal float,  Sentry float, Travelers float, Wausau float, ACE float, CNA float)

insert into @data

select datename(month, dateadd(month, month(SettlementDate), 0)-1) + ' 2012' as BillPeriod, cpi.ClaimantPersonalInfoID as FileNumber, LastName +', '+FirstName as plaintiff,
state as StateOfFiling, disease, 
case when SocialSecurityNumber not like '%-%' then LEFT(SocialSecurityNumber,3) +'-'+ SUBSTRING(SocialSecurityNumber,4,2)+'-'+RIGHT(SocialSecurityNumber,4)
else SocialSecurityNumber end as ss#, 
BirthDate, gender, SettlementDate as SettlementDate, cs.SettlementAmount, e.GPCAcquisitionDate, firstexposure, 
coalesce(iscolyear,0) as iscolyear,
--FIRST STATE
Case
	 when e.GPCAcquisitionDate >= 1982 and e.GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,e.GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0357)*cs.SettlementAmount)
	 when  e.GPCAcquisitionDate >1986 then 0
		else .0357*cs.SettlementAmount end as [First State],
--ROYAL		
case when IsColyear=1 and e.GPCAcquisitionDate >= 1982 and e.GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,e.GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0215)*cs.SettlementAmount)
	 when ((iscolyear = 1 and e.GPCAcquisitionDate >1986) Or IsColyear <>1) then 0
		else .0215*cs.SettlementAmount end as Royal,		
--SENTRY		
case when e.GPCAcquisitionDate >= 1982 and e.GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,e.GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*cs.SettlementAmount)
	 when e.GPCAcquisitionDate >1986 then 0
		else .1275*cs.SettlementAmount end as Sentry,
--TRAVELERS		
case when e.GPCAcquisitionDate >1986 then 0
		else .085*cs.SettlementAmount end as Travelers,
--WAUSAU
case when e.GPCAcquisitionDate >= 1982 and e.GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,e.GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*cs.SettlementAmount)
	 when e.GPCAcquisitionDate >1986 then 0
		else .1275*cs.SettlementAmount end as Wausau,
--ACE		
case when e.GPCAcquisitionDate >1986 then 0
		else .0425*cs.SettlementAmount end as ACE,	
--HARBOR/CNA		
Case when coalesce(e.GPCAcquisitionDate, 0) <=1979 then .0425*cs.SettlementAmount else 0 end as CNA									


--This will be used going forward, with dates updated
from tblClaimantPersonalInfo cpi 
inner join tblClaim c on c.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
inner join tblClaimLawsuit cl on cl.ClaimID =c.ClaimID 
inner join tblLawsuit l on l.LawsuitID =cl.LawsuitID 
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID 
inner join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=cl.ClaimLawsuitID
inner join @Employment e on e.claimantpersonalinfoid=cpi.ClaimantPersonalInfoID
left join tblClaimDisease cd on cd.ClaimID=c.ClaimID 
left join TblDisease d on d.DiseaseID=cd.DiseaseID
left join tblState s on l.StateID=s.StateID
left join tblGender g on g.GenderID=cpi.GenderID
where (cs.SettlementDate between @BillStartDate and @BillEndDate) and IsPrimaryStatus=1 and IsPrimaryForClaim=1 and ClaimLawsuitStatusID=23


group by datename(month, dateadd(month, month(SettlementDate), 0)-1) + ' 2012' , cpi.ClaimantPersonalInfoID, LastName +', '+FirstName,
state , disease, 
case when SocialSecurityNumber not like '%-%' then LEFT(SocialSecurityNumber,3) +'-'+ SUBSTRING(SocialSecurityNumber,4,2)+'-'+RIGHT(SocialSecurityNumber,4)
else SocialSecurityNumber end,
BirthDate, gender, SettlementDate, cs.SettlementAmount, e.GPCAcquisitionDate, firstexposure, 
coalesce(iscolyear,0), 
--FIRST STATE
Case
	 when e.GPCAcquisitionDate >= 1982 and e.GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,e.GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0357)*cs.SettlementAmount)
	 when  e.GPCAcquisitionDate >1986 then 0
		else .0357*cs.SettlementAmount end,
--ROYAL		
case when IsColyear=1 and e.GPCAcquisitionDate >= 1982 and e.GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,e.GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0215)*cs.SettlementAmount)
	 when ((iscolyear = 1 and e.GPCAcquisitionDate >1986) Or IsColyear <>1) then 0
		else .0215*cs.SettlementAmount end,		
--SENTRY		
case when e.GPCAcquisitionDate >= 1982 and e.GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,e.GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*cs.SettlementAmount)
	 when e.GPCAcquisitionDate >1986 then 0
		else .1275*cs.SettlementAmount end,
--TRAVELERS		
case when e.GPCAcquisitionDate >1986 then 0
		else .085*cs.SettlementAmount end,
--WAUSAU
case when e.GPCAcquisitionDate >= 1982 and e.GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,e.GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*cs.SettlementAmount)
	 when e.GPCAcquisitionDate >1986 then 0
		else .1275*cs.SettlementAmount end ,
--ACE		
case when e.GPCAcquisitionDate >1986 then 0
		else .0425*cs.SettlementAmount end,	
--HARBOR/CNA		
Case when coalesce(e.GPCAcquisitionDate, 0) <=1979 then .0425*cs.SettlementAmount else 0 end

if @billtype ='d'
select BillPeriod, FileNumber, plaintiff, UPPER(disease) as disease,ss#, DoB, Gender, StateOfFiling,  SettlementDate,  Amount,  gpcacquisitiondate, 
DOFE, iscolyear, case 
when @CarrierName ='First State' then [First State]
when @CarrierName ='Royal' then Royal
when @CarrierName ='Sentry' then Sentry
when @CarrierName ='Travelers' then Travelers
when @CarrierName ='Wausau' then Wausau
when @CarrierName ='ACE' then ACE
when @CarrierName ='CNA' then CNA end as InsurerShare, @InvoiceNumber as InvoiceNumber
from @data
order by MONTH(BillPeriod)

else
select BillPeriod, SUM(amount) as amount, case 
when @CarrierName ='First State' then sum([First State])
when @CarrierName ='Royal' then sum(Royal)
when @CarrierName ='Sentry' then sum(Sentry)
when @CarrierName ='Travelers' then sum(Travelers)
when @CarrierName ='Wausau' then sum(Wausau)
when @CarrierName ='ACE' then sum(ACE)
when @CarrierName ='CNA' then sum(CNA) end as InsurerShare, @InvoiceNumber as InvoiceNumber, @CarrierName as carriername
from @data
group by BillPeriod
order by MONTH(BillPeriod)
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailIndemnityBill_ACE] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailIndemnityBill_ACE] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailIndemnityBill_ACE] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailIndemnityBill_ACE] TO [power_user]
GO
