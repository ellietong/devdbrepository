SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList]
AS
SET NOCOUNT ON

SELECT cs.ClaimLawsuitStatusID, 
	ClaimLawsuitStatus 
FROM tblClaimLawsuitStatus cs
inner join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitStatusID=cs.ClaimLawsuitStatusID
WHERE cs.ClaimLawsuitStatusID NOT IN (40)
UNION
SELECT NULL, ''
UNION
SELECT cs.ClaimLawsuitStatusID, 
	ClaimLawsuitStatus 
FROM tblClaimLawsuitStatus cs
WHERE ClaimLawsuitStatusID = 41
ORDER BY ClaimLawsuitStatus
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [power_user]
GO
