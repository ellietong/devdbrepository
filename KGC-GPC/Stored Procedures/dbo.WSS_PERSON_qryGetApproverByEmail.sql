SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_PERSON_qryGetApproverByEmail]
    (
      @ApproverTypeID int
    )
AS

IF @ApproverTypeID = 1 --Invoice Approvers
BEGIN
	SELECT DISTINCT CONVERT(nvarchar(10),PersonID) AS PersonID
	FROM vtblPerson
	WHERE PersonID IN (4389, 4536, 4537, 4427, 4428, 4438, 4439) --S. Smith, J. Morgan, P. Green, P. Riley, P. Hewitt, L. Barbour, A. Nolf
END 
ELSE IF @ApproverTypeID = 2 -- Settlement Approvers
BEGIN
	SELECT DISTINCT CONVERT(nvarchar(10),PersonID) AS PersonID
	FROM vtblPerson 
	WHERE PersonID IN (4389, 4536, 4537, 4427, 4428, 4438, 4439) --S. Smith, J. Morgan, P. Green, P. Riley, P. Hewitt, L. Barbour, A. Nolf
END
ELSE IF @ApproverTypeID = 3 --Can edit particular claim details
BEGIN
	SELECT DISTINCT CONVERT(nvarchar(10),PersonID) AS PersonID
	FROM vtblPerson 
	WHERE PersonID IN (4438, 4439, 4429) --L. Barbour, A. Nolf, A. Romano
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [power_user]
GO
