SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_EVENT_qryInsNewEvent] 
	-- Add the parameters for the stored procedure here
(
	@ClaimLawsuitID bigint,
	@EventTypeID int,
	@EventDate datetime,
	@EventDescription nvarchar(max),
	@EmailAddress nvarchar(250)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
INSERT INTO tblEventTracking
SELECT @ClaimLawsuitID, @EventTypeID, @EventDate, @EventDescription, @EmailAddress

END
GO
