SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryIsPrimaryForClaim]
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        --SELECT DISTINCT
        --        CASE WHEN IsPrimaryForClaim = 1 THEN 'YES'
        --             ELSE 'NO'
        --        END AS IsPrimaryForClaim ,
        --        CASE WHEN IsMDL = 1 THEN 'YES'
        --             ELSE 'NO'
        --        END AS IsMDL
        --FROM    tblClaimLawsuit cl
        SELECT 
                
                'NO' AS IsPrimaryForClaim, 'NO' AS IsMDL, 'NO' AS TenDefendants
                UNION 
                SELECT 'YES' AS IsPrimaryForClaim, 'YES' AS IsMDL, 'YES' AS TenDefendants
                                                
        FROM    tblClaimLawsuit cl


    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryIsPrimaryForClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryIsPrimaryForClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryIsPrimaryForClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryIsPrimaryForClaim] TO [power_user]
GO
