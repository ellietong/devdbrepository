SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_RELATED_PARTY_qryLegalCapacityList]
AS

SELECT LegalCapacityID,
	LegalCapacity
FROM tblLegalCapacity
UNION 
SELECT NULL, ''
ORDER BY LegalCapacity


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryLegalCapacityList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryLegalCapacityList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryLegalCapacityList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryLegalCapacityList] TO [power_user]
GO
