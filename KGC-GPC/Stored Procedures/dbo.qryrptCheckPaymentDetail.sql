SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCheckPaymentDetail] (@SourceDate datetime, @sourcenumber bigint)

As

SET NOCOUNT ON

--Declare @SourceDate datetime
--DECLARE @sourcenumber bigint
--Set @SourceDate = '4/22/2013'
--set @sourcenumber = '3344'

Declare @IndemnityFunding table(CheckID bigint, PlaintiffCounselAddressID bigint, PlaintiffCounsel nvarchar(250), ClaimantPersonalInfoID bigint, ClaimantName nvarchar(250), SSN nvarchar(250),
								SettlementAmount money, settlementdate datetime,  Matter nvarchar(250), SourceNumber bigint)
Insert into @IndemnityFunding
Select ck.CheckID, l.PlaintiffCounselAddressID, PlaintiffCounsel, cpi.ClaimantPersonalInfoID,Coalesce(LastName,'') + ', ' + Coalesce(FirstName, '') as ClaimantName, 
coalesce('XXX-XX-'+ Right(socialsecuritynumber,4),'XXX-XX-') as SSN, SettlementAmount, SettlementDate, Matter, SourceNumber
From tblClaim c
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
inner join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID 
inner join tblPaymentSettlement ps on ps.ClaimantSettlementID=cs.ClaimantSettlementID
inner join tblCheck ck on ck.CheckID=ps.CheckID
inner join tblMatter m on m.MatterID=c.MatterID
--LEFT JOIN tblSettlementGroup s ON cs.SettlementGroupID = s.SettlementGroupID
--LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID
--LEFT JOIN tblSettlementDocumentation sd on sd.SetPymtID = spd.SetPymtID
--LEFT JOIN (Select ClaimantSettlementID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentSettlement ps on ca.CheckID = ps.CheckID)
--a on a.ClaimantSettlementID = cs.ClaimantSettlementID

	Where ck.SourceDate = @SourceDate and ck.SourceNumber=@sourcenumber and IsPrimaryStatus = 1
AND COALESCE(IsCurrentClaim,0) = 1

select * from @IndemnityFunding

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentDetail] TO [power_user]
GO
