SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryClaimLawsuits] (@ClaimID bigint, @LawsuitID bigint)
AS
SET NOCOUNT ON

SELECT cl.LawsuitID,
	CaseCaption + ' (' + DocketNumber + ')' AS CaseCaption
FROM tblClaimLawsuit cl
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
WHERE ClaimID = @ClaimID
AND cl.LawsuitID = @LawsuitID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryClaimLawsuits] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryClaimLawsuits] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryClaimLawsuits] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryClaimLawsuits] TO [power_user]
GO
