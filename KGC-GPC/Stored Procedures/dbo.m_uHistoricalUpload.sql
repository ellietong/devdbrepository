SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_uHistoricalUpload]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DocumentID AS DocumentID, DocumentURL + '\' + DocumentTitle AS DocumentURL, DocumentTitle, d.ClaimantDocumentTypeID, ClaimantDocumentType,
	'' AS DefenseCounsel, '' as DefenseCounselID
	FROM tblDocuments d
	INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID

WHERE DocumentURL LIKE 'G:%' AND AmazonKey IS NULL
	--INNER JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
--	SELECT CASE WHEN Folder = '\TIF' THEN REPLACE(DocumentLink, '\TIF\', '\TIF\MIGRATE\0\' + CONVERT(Nvarchar(5), CONVERT(int, DocumentName)/1024) + '\') ELSE DocumentURL END FROM tblClaimantDocuments
--WHERE DocumentType = 'CHECK'

END

--SELECT CASE WHEN Folder = '\TIF' THEN REPLACE(DocumentLink, '\TIF\', '\TIF\MIGRATE\0\' + CONVERT(Nvarchar(5), CONVERT(int, DocumentName)/1024) + '\') WHEN (Folder='\NEWIMAGES' AND RIGHT(DocumentURL, 3) = 'bin') THEN Folder + '\' + DocumentName + '.tif' ELSE DocumentURL END
--FROM tblClaimantDocuments

----WHERE DocumentURL <> DocumentLink AND RIGHT(DocumentURL, 3) = 'bin'
--BEGIN TRAN
--UPDATE tblClaimantDocuments
--SET DocumentURL = DocumentLink
--WHERE DocumentURL <> DocumentLink AND RIGHT(DocumentURL, 3) = 'bin'
--COMMIT TRAN
----SELECT * FROM tblClaimantDocuments WHERE DocumentURL <> DocumentLink AND RIGHT(DocumentURL, 3) = 'bin'
--BEGIN TRAN
--UPDATE tblDocuments 
--SET DocumentURL = REPLACE(DocumentURL, 'F:\', 'X:\')
--WHERE DocumentURL LIKE 'F:%' AND AmazonKey IS NULL

--SELECT * FROM tblDocuments WHERE DocumentURL LIKE 'X:%'

--COMMIT
--G:\GPC Settlements\Settlement Packets\Clark Benjamin Franklin, III Estate.pdf
--G:\GPC Settlements\Settlement Packets\Clark Benjamin Franklin, III Estate.pdf

--UPDATE tblDocuments


--SET DocumentTitle = 'Brunk Joseph Gary G-11-00484 Pre-Trial Report and Exhibits.pdf
--WHERE DocumentID = 8191
GO
GRANT VIEW DEFINITION ON  [dbo].[m_uHistoricalUpload] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_uHistoricalUpload] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_uHistoricalUpload] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_uHistoricalUpload] TO [power_user]
GO
