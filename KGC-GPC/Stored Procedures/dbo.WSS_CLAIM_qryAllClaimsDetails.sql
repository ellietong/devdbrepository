SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_CLAIM_qryAllClaimsDetails] (@ClaimID		 bigint = NULL, 
												  @emailaddress nvarchar(250))
AS

SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimID int
--DECLARE @emailaddress nvarchar(250)
--SET @ClaimID = 14123
--SET @emailaddress = 'owenss@kcicllc.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @10Defendants nvarchar(10)
Set @10Defendants = (SELECT CASE WHEN MAX(d.IsLessThanTenDefendantCompanies+0) >= 1 THEN 'YES' ELSE 'NO' END FROM tblDocuments D INNER JOIN tblMultiClaimantDocument MCD ON MCD.DocumentID = D.DocumentID INNER JOIN tblClaimantPersonalInfo CPI ON CPI.ClaimantPersonalInfoID = MCD.ClaimantPersonalInfoID INNER JOIN tblClaim C ON C.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID WHERE ClaimID = @ClaimID AND ClaimantDocumentTypeID = 8 AND IsCurrentClaim = 1)

DECLARE @LawsuitID bigint
DECLARE @CaseCaptions nvarchar(max)
DECLARE @CaseCaptionText TABLE (LawsuitID int, CaseCaptionText nvarchar(max))
DECLARE @CaseCaptionID int = 0

IF object_id('tempdb..#CaseCaptionText') IS NOT NULL
BEGIN
   DROP TABLE #CaseCaptionText
END
CREATE TABLE #CaseCaptionText (CaseCaptionID int, LawsuitID int, CaseCaptionText nvarchar(max))

DECLARE CaseCaption_Cursor CURSOR Local Fast_Forward FOR
	 SELECT l.LawsuitID, CaseCaption
	 FROM tblClaimantPersonalInfo cpi 
		LEFT JOIN tblClaim c							ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l							ON cl.LawsuitID = l.LawsuitID
	 WHERE c.ClaimID = @ClaimID AND LEN(CaseCaption) >= 1

OPEN CaseCaption_Cursor

FETCH NEXT FROM CaseCaption_Cursor 
INTO @LawsuitID, @CaseCaptions

WHILE @@FETCH_STATUS = 0
BEGIN

	DECLARE @CaseCaption nvarchar(max) = (
	 SELECT TOP 1 CaseCaption
	 FROM tblClaimantPersonalInfo cpi 
		LEFT JOIN tblClaim c							ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l							ON cl.LawsuitID = l.LawsuitID
	 WHERE l.LawsuitID = @LawsuitID
	)

	DECLARE @CaseCaptionLength int 
	= (
		SELECT TOP 1 LEN(CaseCaption)
	 FROM tblClaimantPersonalInfo cpi 
		LEFT JOIN tblClaim c							ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l							ON cl.LawsuitID = l.LawsuitID
	 WHERE l.LawsuitID = @LawsuitID
	)

	DECLARE @Index int = -1

	DECLARE @MoveNote nvarchar(max) = (
	SELECT RIGHT(@CaseCaption, (LEN(@CaseCaption)-@Index))
	)

	DECLARE	@Fragment50 nvarchar(150) 
	DECLARE @CaseCaptionRight nvarchar(max)
	DECLARE @Remainder int 
	DECLARE @Count int = 0

	WHILE 1=1
		BEGIN
			SET @Count = @Count + 1
			SET @CaseCaptionID = @CaseCaptionID + 1
			SET @Fragment50 = (SELECT LEFT(@CaseCaption, 49)) 

			IF (LEN(@CaseCaption)-49)>= 1
				BEGIN

					SET @CaseCaptionRight = (SELECT RIGHT(@CaseCaption, LEN(@CaseCaption)-49))
					SET @Remainder = (SELECT CHARINDEX(' ', @CaseCaptionRight))
					SET @Fragment50 =  @Fragment50 + LEFT(@CaseCaptionRight, @Remainder) + '<br/>'
					SET @CaseCaptionRight = (SELECT RIGHT(@CaseCaption, LEN(@CaseCaption)-49- @Remainder))
					SET @CaseCaption = (RIGHT(@CaseCaptionRight, LEN(@CaseCaptionRIGHT)+1))

					INSERT INTO #CaseCaptionText
						SELECT @CaseCaptionID, @LawsuitID, LTRIM(@Fragment50)

				END
				ELSE
				BEGIN
					SET @Fragment50 = @CaseCaption
					INSERT INTO #CaseCaptionText
						SELECT @CaseCaptionID, @LawsuitID, LTRIM(@Fragment50) + '<br/>'
					BREAK
				END
		END

	FETCH NEXT FROM CaseCaption_Cursor
	INTO @LawsuitID, @CaseCaptions

END
CLOSE CaseCaption_Cursor
DEALLOCATE CaseCaption_Cursor

	--SELECT * FROM #CaseCaptionText 


DECLARE @FullCaseCaption nvarchar(max) = ''


IF object_id('tempdb..#FullCaseCaptionText') IS NOT NULL
BEGIN
   DROP TABLE #FullCaseCaptionText
END
CREATE TABLE #FullCaseCaptionText (LawsuitID bigint, FullCaseCaptionText nvarchar(max))


DECLARE @FullCaseCaptions nvarchar(500)
DECLARE @FullCaseCaptionID int

DECLARE FullCaseCaption_Cursor CURSOR Local Fast_Forward FOR
SELECT DISTINCT max(CaseCaptionID), LawsuitID
FROM #CaseCaptionText
GROUP BY LawsuitID

OPEN FullCaseCaption_Cursor

FETCH NEXT FROM FullCaseCaption_Cursor 
INTO @FullCaseCaptionID, @LawsuitID

WHILE @@FETCH_STATUS = 0
	BEGIN

	DECLARE @CountID int = 0
		SET @CaseCaptionID = @FullCaseCaptionID - (SELECT count(CaseCaptionid) from #CaseCaptionText WHERE LawsuitID = @LawsuitID) + 1 --@CaseCaptionID + 1
	WHILE (SELECT MAX(CaseCaptionID) FROM #CaseCaptionText WHERE LawsuitID =  @LawsuitID)  = @FullCaseCaptionID
		BEGIN
			SET @FullCaseCaption = @FullCaseCaption + (SELECT CaseCaptionText FROM #CaseCaptionText WHERE LawsuitID = @LawsuitID AND CaseCaptionID = @CaseCaptionID)
			IF @CaseCaptionID = @FullCaseCaptionID
				BEGIN
					IF (SELECT Count(LawsuitID) FROM #FullCaseCaptionText WHERE LawsuitID = @LawsuitID) < 1
						BEGIN
							INSERT INTO #FullCaseCaptionText
								SELECT @LawsuitID, @FullCaseCaption
							--SELECT @FullCaseCaption, @LawsuitID
							SET @FullCaseCaption = ''
							--SELECT @CaseCaptionID
						END
					IF @CaseCaptionID = @FullCaseCaptionID
						BEGIN	
							--SELECT @CaseCaptionID
							BREAK					
						END
				END		

				SET @CaseCaptionID = @CaseCaptionID + 1
		END

		FETCH NEXT FROM FullCaseCaption_Cursor
		INTO @FullCaseCaptionID, @LawsuitID

	END
CLOSE FullCaseCaption_Cursor
DEALLOCATE FullCaseCaption_Cursor

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE c.ClaimID = ' + CONVERT(nvarchar(20),@ClaimID)


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' LEFT JOIN  tblDefenseCounselAddress dca	ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID '
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT c.ClaimID, 
		hddis.HighLevelDisease AS Disease, 
		S.State, 
		J.Jurisdiction, 
		cs.ClaimSequence, 
        cls.ClaimLawsuitStatus, 
        CONVERT(date, A.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, 
        L.DocketNumber, 
		--L.CaseCaption, 
		COALESCE(FullCaseCaptionText, '''') AS CaseCaption,
		L.LeadPlaintiffLastName + '', '' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  
		CASE WHEN NationalPlaintiffCounselAddressID IS NULL THEN l.PlaintiffCounselAddressID ELSE NationalPlaintiffCounselAddressID END AS NationalPlaintiffCounselAddressID,
		CASE WHEN npca.PlaintiffCounsel IS NULL THEN pca.PlaintiffCounsel ELSE npca.PlaintiffCounsel END AS NationalPlaintiffCounsel,
		pca.PlaintiffCounsel AS PrimaryLocalPlaintiffCounsel, 
		l.PlaintiffCounselAddressID AS PrimaryLocalPlaintiffCounselAddressID,
		DefenseCounsel AS PrimaryDefenseCounsel, 
		ClaimDiseaseID, 
		cl.LawsuitID, 
		ActiveTrialDate,
		CASE WHEN IsMDL = 1 THEN ''YES'' ELSE ''NO'' END AS IsMDL,
		doc.DoctorLastName + '', '' + COALESCE(doc.DoctorFirstName, '''') + COALESCE('' '' + doc.DoctorMiddleName, '''') AS Doctor,CauseOfInjuryID,
		CASE WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + '', '' + COALESCE(ilodoc.DoctorFirstName, '''') + COALESCE('' '' + ilodoc.DoctorMiddleName, '''') END AS ILODoctor,
		CASE WHEN PFTDoctorID IS NULL THEN NULL ELSE pftdoc.DoctorLastName + '', '' + COALESCE(pftdoc.DoctorFirstName, '''') + COALESCE('' '' + pftdoc.DoctorMiddleName, '''')END AS PFTDoctor, 
		ILODoctorID, 
		ILOProfusion, 
		ILODate, 
		PFTDoctorID,
		PFTDate, 
		PFTTypeID, 
		PFTFVC_Actual, 
		PFTFVC_Predicted, 
		CASE WHEN PFTFVC_Predicted <> 0 THEN (PFTFVC_Actual/PFTFVC_Predicted) ELSE 0 END AS PFTFVC_Pct,
		PFTFEV1_Actual, 
		PFTFEV1_Predicted, 
		CASE WHEN PFTFEV1_Predicted <> 0 THEN (PFTFEV1_Actual/PFTFEV1_Predicted) ELSE 0 END AS PFTFEV1_Pct,
		[PFTFEV1andFVC_Actual] AS PFTFEV1FVC_Actual, 
		[PFTFEV1andFVC_Predicted] AS PFTFEV1FVC_Predicted,
		CASE WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN ([PFTFEV1andFVC_Actual]/[PFTFEV1andFVC_Predicted]) ELSE 0 END AS PFTFEV1FVC_Pct,
		PFTTLC_Actual, 
		PFTTLC_Predicted, 
		CASE WHEN PFTTLC_Predicted <> 0 THEN (PFTTLC_Actual/PFTTLC_Predicted) ELSE 0 END AS PFTTLC_Pct, 
		PFTDLCO_Actual, 
		PFTDLCO_Predicted, 
		CASE WHEN PFTDLCO_Predicted <> 0 THEN (PFTDLCO_Actual/PFTDLCO_Predicted) ELSE 0 END AS PFTDLCO_Pct, 
		PFTTV, 
		AllegationType + '' - '' + Material AS Allegation, 
		CASE WHEN IsPrimaryForClaim = 1 THEN ''YES'' ELSE ''NO'' END AS PrimaryLawsuit, IsPrimaryForClaim, DiseaseDiagnosisDate,
		COALESCE(IsPrimaryForClaim,0),
		cd.DiseaseID,
		cd.DoctorID,
		'''+COALESCE(@10Defendants,'NO')+''' AS TenDefendants,
		cl.ClaimLawsuitID 
     FROM tblClaimantPersonalInfo cpi 
		LEFT JOIN tblClaim c							ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l							ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblState s							ON s.StateID = l.StateID
		INNER JOIN tblJurisdiction j					ON j.JurisdictionID = l.JurisdictionID
		INNER JOIN tblClaimSequence cs					ON cs.ClaimSequenceID = c.ClaimSequenceID ' +
		@JoinString + ' 
        LEFT JOIN  (SELECT * 
					FROM tblClaimDisease 
					WHERE PrimaryDisease = 1) cd		ON cd.ClaimID = c.ClaimID
		LEFT JOIN  tblDisease ddis						ON ddis.DiseaseID = cd.DiseaseID
		LEFT JOIN  tblDisease_ALT disa					ON disa.DiseaseID_Alt = cd.DiseaseID
		LEFT JOIN  tblHighLevelDisease hddis			ON hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
		LEFT JOIN  (SELECT * 
					FROM tblClaimLawsuitStatusTracking 
					WHERE IsPrimaryStatus = 1) clst		ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
        LEFT JOIN  tblClaimLawsuitStatus cls			ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
        LEFT JOIN  tblplaintiffcounseladdress pca		ON pca.plaintiffcounseladdressid = l.plaintiffcounseladdressid
		LEFT JOIN  tblplaintiffcounseladdress npca		ON npca.plaintiffcounseladdressid = l.nationalplaintiffcounseladdressid
        LEFT JOIN  tblDoctor doc						ON cd.DoctorID = doc.DoctorID 
        LEFT JOIN  tblDoctor ILODoc						ON ILODoctorID=ILODoc.DoctorID
        LEFT JOIN  tblDoctor PFTDoc						ON PFTDoctorID=PFTDoc.DoctorID
        LEFT JOIN  (SELECT MIN(servicedate) AS ServiceDate, 
						  ClaimLawsuitID, 
						  AllegationTypeID, 
						  MaterialID 
					FROM tblAllegation
					GROUP BY ClaimLawsuitID,
							 AllegationTypeID, 
							 MaterialID)  a				ON cl.ClaimLawsuitID = a.ClaimLawsuitID
        INNER JOIN tblAllegationType at					ON a.AllegationTypeID = at.AllegationTypeID
        INNER JOIN tblMaterial mat						ON a.MaterialID = mat.MaterialID
		LEFT JOIN  #FullCaseCaptionText fcct ON l.LawsuitID = fcct.LawsuitID ' +
     @WhereString + '
     ORDER BY COALESCE(IsPrimaryForClaim,0) DESC
	 OPTION (RECOMPILE)'

PRINT Substring(@QueryString, 1, 4000)
PRINT Substring(@QueryString, 4001, 4000)

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [power_user]
GO
