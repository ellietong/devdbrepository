SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryfrmCheckPrimaryEmploymentExists]
(@ClaimantPersonalInfoID int, @PrimaryEmploymentExists int OUTPUT)
AS
SET NOCOUNT ON

--DECLARE @PrimaryEmploymentExists int
--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 10864

DECLARE @IsPrimary TABLE (ClaimantPersonalInfoID int, IsPrimary int)
INSERT INTO @IsPrimary
	SELECT ClaimantPersonalInfoID, IsPrimary
	FROM tblClaimantEmployment

SELECT @PrimaryEmploymentExists = SUM(IsPrimary)
FROM @IsPrimary
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
GROUP BY ClaimantPersonalInfoID
ORDER BY ClaimantPersonalInfoID

SET @PrimaryEmploymentExists = COALESCE(@PrimaryEmploymentExists, 0)

SELECT @PrimaryEmploymentExists
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckPrimaryEmploymentExists] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckPrimaryEmploymentExists] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckPrimaryEmploymentExists] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckPrimaryEmploymentExists] TO [power_user]
GO
