SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAgingAccountsReceivable] (@RecentPeriod nvarchar(50))
AS
Set NoCount On



 --Declare @RecentPeriod nvarchar(50)
 --Set @RecentPeriod = '2014 Q1'
 
DECLARE @BillPeriod TABLE (ExhaustionDateID int, ExhaustionDate datetime, Description nvarchar(50),  FormSort int IDENTITY(1,1))

INSERT INTO @BillPeriod
select ExhaustionDateID, ExhaustionDate, Description 
from tblExhaustionDate
where ExhaustionDateID not in (10,6,1,9)
order by ExhaustionDate

Declare @RecentPeriodID int
Set @RecentPeriodID = (select FormSort from @BillPeriod where description = @RecentPeriod group by FormSort)



declare @payments table (carrierid int , ExhaustionDateID int , PaymentAmount float)
insert into @payments

select carrierid, l.ExhaustionDateID, sum(BillPeriodAmount)
from tblPayment p
inner join tblLedger l on l.LedgerID=p.LedgerID
inner join tblExhaustionDate e on e.ExhaustionDateID=l.ExhaustionDateID
group by carrierid, l.ExhaustionDateID

Declare @paymentDetail table (ledgerID bigint, PaymentAmount float)
insert @paymentDetail 

select LedgerID, BillPeriodAmount
from tblPayment p

declare @MostRecentQuarter table (carriername nvarchar(250), carrierid int, BillPeriod nvarchar(50),totalbilled float, paymentamount float, outstanding float, duedate datetime)
insert into @MostRecentQuarter

select c.CarrierName,c.carrierid,Description, sum(debit), coalesce((PaymentAmount),0) as PaymentAmount,
	CASE WHEN sum(debit)- (coalesce(PaymentAmount,0)) between -.30 and .03 then 0.00
	ELSE sum(debit)- (coalesce(PaymentAmount,0)) END as Outstanding,
	ExhaustionDate+60 as billduedate 
from tblLedger l
join tblCarrier c on c.CarrierID=l.CarrierID
left join @payments p on p.CarrierID=l.CarrierID and p.ExhaustionDateID=l.ExhaustionDateID
join tblExhaustionDate ed on ed.ExhaustionDateID=l.ExhaustionDateID
where l.ExhaustionDateID= (select ExhaustionDateID from @BillPeriod where FormSort=@RecentPeriodID)
group by c.CarrierName,c.carrierid,Description ,coalesce((PaymentAmount),0),
	--CASE WHEN sum(debit)- sum(coalesce(PaymentAmount,0)) between -.03 and .03 then 0.00
	--ELSE sum(debit)- sum(coalesce(PaymentAmount,0)) END ,
	ExhaustionDate+60



declare @LastQuarter table (carriername nvarchar(250), carrierid int, BillPeriod nvarchar(50), totalbilled float, paymentamount float, outstanding float, duedate datetime)
insert into @LastQuarter

select c.CarrierName,c.carrierid,Description, sum(debit), coalesce((PaymentAmount),0) as PaymentAmount,
	CASE WHEN sum(debit)- (coalesce(PaymentAmount,0)) between -.30 and .03 then 0.00
	ELSE sum(debit)- (coalesce(PaymentAmount,0)) END as Outstanding,
	ExhaustionDate+60 as billduedate 
from tblLedger l
join tblCarrier c on c.CarrierID=l.CarrierID
left join @payments p on p.CarrierID=l.CarrierID and p.ExhaustionDateID=l.ExhaustionDateID
join tblExhaustionDate ed on ed.ExhaustionDateID=l.ExhaustionDateID
where l.ExhaustionDateID= (select ExhaustionDateID from @BillPeriod where FormSort=@RecentPeriodID-1)
group by c.CarrierName,c.carrierid,Description ,coalesce((PaymentAmount),0),
	--CASE WHEN sum(debit)- sum(coalesce(PaymentAmount,0)) between -.03 and .03 then 0.00
	--ELSE sum(debit)- sum(coalesce(PaymentAmount,0)) END ,
	ExhaustionDate+60


declare @PriorQuartersBills table (carriername nvarchar(250), carrierid int, totalbilled float)
insert into @PriorQuartersBills

select c.CarrierName,c.carrierid, sum(debit)
from tblLedger l
left join @payments p on p.CarrierID=l.CarrierID and p.ExhaustionDateID=l.ExhaustionDateID
join tblCarrier c on c.CarrierID=l.CarrierID
join tblExhaustionDate ed on ed.ExhaustionDateID=l.ExhaustionDateID
where l.ExhaustionDateID<= (select ExhaustionDateID from @BillPeriod where FormSort=@RecentPeriodID-2)
and l.ExhaustionDateID not in (8)
group by c.CarrierName,c.carrierid

declare @PriorQuartersPayments table (carriername nvarchar(250), carrierid int,  paymentamount float)
insert into @PriorQuartersPayments

select c.CarrierName,c.carrierid, coalesce(sum(PaymentAmount),0) as PaymentAmount
from tblLedger l
 left  join @paymentDetail p on p.ledgerID=l.LedgerID
join tblCarrier c on c.CarrierID=l.CarrierID
join tblExhaustionDate ed on ed.ExhaustionDateID=l.ExhaustionDateID
where l.ExhaustionDateID<= (select ExhaustionDateID from @BillPeriod where FormSort=@RecentPeriodID-2)
and l.ExhaustionDateID not in (8)
group by c.CarrierName,c.carrierid

declare @PriorQuarters table (carriername nvarchar(250), carrierid int, totalbilled float, paymentamount float, outstanding float)
insert into @PriorQuarters

select pb.CarrierName,pb.carrierid, sum(pb.totalbilled), coalesce(sum(pp.PaymentAmount),0) as PaymentAmount,
	CASE WHEN sum(pb.totalbilled)- coalesce(sum(pp.PaymentAmount),0) between -.30 and .03 then 0.00
	ELSE sum(pb.totalbilled)- coalesce(sum(pp.PaymentAmount),0) END as Outstanding
from @PriorQuartersBills pb 
left join @PriorQuartersPayments pp on pp.carrierid=pb.carrierid
group by pb.CarrierName,pb.carrierid


declare @AllKCICBills table (carriername nvarchar(250), carrierid int, totalbilled float)
insert into @AllKCICBills

select c.CarrierName,c.carrierid, sum(debit)
from tblLedger l
left join @payments p on p.CarrierID=l.CarrierID and p.ExhaustionDateID=l.ExhaustionDateID
join tblCarrier c on c.CarrierID=l.CarrierID
join tblExhaustionDate ed on ed.ExhaustionDateID=l.ExhaustionDateID
where l.ExhaustionDateID<= (select ExhaustionDateID from @BillPeriod where FormSort=@RecentPeriodID)
and l.ExhaustionDateID not in (8)
group by c.CarrierName,c.carrierid



declare @AllKCICBillPayments table (carriername nvarchar(250), carrierid int,  paymentamount float)
insert into @AllKCICBillPayments

select c.CarrierName,c.carrierid, coalesce(sum(PaymentAmount),0) as PaymentAmount
from tblLedger l
 left  join @paymentDetail p on p.ledgerID=l.LedgerID
join tblCarrier c on c.CarrierID=l.CarrierID
join tblExhaustionDate ed on ed.ExhaustionDateID=l.ExhaustionDateID
where l.ExhaustionDateID<= (select ExhaustionDateID from @BillPeriod where FormSort=@RecentPeriodID)
and l.ExhaustionDateID not in (8)
group by c.CarrierName,c.carrierid



declare @AllKCICBillings table (carriername nvarchar(250), carrierid int, totalbilled float, paymentamount float, outstanding float)
insert into @AllKCICBillings

select pb.CarrierName,pb.carrierid, sum(pb.totalbilled), coalesce(sum(pp.PaymentAmount),0) as PaymentAmount,
	CASE WHEN sum(pb.totalbilled)- coalesce(sum(pp.PaymentAmount),0) between -.30 and .03 then 0.00
	ELSE sum(pb.totalbilled)- coalesce(sum(pp.PaymentAmount),0) END as Outstanding
from @AllKCICBills pb 
left join @AllKCICBillPayments pp on pp.carrierid=pb.carrierid
group by pb.CarrierName,pb.carrierid



declare @HistoricalBillings table (carriername nvarchar(250), carrierid int, Outstanding float)
insert into @historicalbillings


select  CarrierName,  l.carrierid, case when l.carrierid=359 then 0 else 0 END as Outstanding
from tblLedger l
join tblCarrier c on c.CarrierID=l.CarrierID
group by CarrierName, l.carrierid


Declare @MRDueDate nvarchar(50)
set @MRDueDate = (select CASE WHEN duedate > getdate() then 'Due ' + convert(nvarchar(50), duedate,101) 
							ELSE 'Past Due' End
					from @MostRecentQuarter
					group by CASE WHEN duedate > getdate() then 'Due ' + convert(nvarchar(50), duedate,101) 
							ELSE 'Past Due' End)

Declare @LDueDate nvarchar(50)
set @LDueDate = (select CASE WHEN duedate > getdate() then 'Due ' + convert(nvarchar(50), duedate,101) 
							ELSE 'Past Due' End
					from @LastQuarter
					group by CASE WHEN duedate > getdate() then'Due ' + convert(nvarchar(50), duedate,101) 
							ELSE 'Past Due' End)

Declare @CurrentDate nvarchar(50)
set @CurrentDate = convert(nvarchar(50), getdate(),101) 


select Case When mr.carrierid=208 then 'ACE'
			When mr.carrierid=66 then 'Wausau'
			When mr.carrierid=9 then 'Hartford'
			When mr.carrierid=13 then 'CNA'
			When mr.carrierid=227 then 'Royal'
			When mr.carrierid=357 then 'Sentry'
			Else 'Travelers [1]' END as Carriername,
mr.carrierid, mr.BillPeriod as MRBillPeriod, mr.totalbilled as MRTotalBilled, mr.paymentamount as MRTotalPaid, mr.outstanding as MROutstanding, 
l.BillPeriod as LBillPeriod, l.totalbilled as LTotalBilled, l.paymentamount as LTotalPaid, l.outstanding as LOutstanding, 
p.totalbilled as PTotalBilled, p.paymentamount as PtotalPaid, p.outstanding as POutstanding, 
k.totalbilled as KTotalBilled, k.outstanding as KOutstanding, h.Outstanding as HOutstanding,
@MRDueDate as MRDueDate, @LDueDate as LDUeDate, @CurrentDate as CurrentDate

from @MostRecentQuarter mr 
left join @LastQuarter l on l.carrierid=mr.carrierid
left join @PriorQuarters p on p.carrierid=mr.carrierid
left join @AllKCICBillings k on k.carrierid=mr.carrierid
left join @HistoricalBillings h on h.carrierid=mr.carrierid
group by mr.carriername, mr.carrierid, mr.BillPeriod, mr.totalbilled, mr.paymentamount, mr.outstanding, 
l.BillPeriod, l.totalbilled, l.paymentamount, l.outstanding, 
p.totalbilled, p.totalbilled, p.paymentamount, p.outstanding,
k.totalbilled, k.outstanding, h.Outstanding
order by Carriername

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAgingAccountsReceivable] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAgingAccountsReceivable] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAgingAccountsReceivable] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAgingAccountsReceivable] TO [power_user]
GO
