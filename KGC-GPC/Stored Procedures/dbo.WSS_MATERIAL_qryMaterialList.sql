SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_MATERIAL_qryMaterialList]
AS
SET NOCOUNT ON

SELECT MaterialID, 
	Material 
FROM tblMaterial
Where MaterialID IN(1,11,12)
UNION
SELECT NULL, ''
ORDER BY Material

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_MATERIAL_qryMaterialList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_MATERIAL_qryMaterialList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_MATERIAL_qryMaterialList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_MATERIAL_qryMaterialList] TO [power_user]
GO
