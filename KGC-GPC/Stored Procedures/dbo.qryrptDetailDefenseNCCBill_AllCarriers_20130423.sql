SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Proc [dbo].[qryrptDetailDefenseNCCBill_AllCarriers_20130423]
(@CarrierName Nvarchar(50), @InvoiceNumber Nvarchar(50))
As
Set Nocount on

--declare @CarrierName Nvarchar(50)
--declare @InvoiceNumber Nvarchar(50)

--set @CarrierName= 'Royal'
--set @InvoiceNumber = 'gpc4443'

Declare @data table (invoicedate date, invoicenumber nvarchar(100), LocalCounsel nvarchar(100),
TotalFees money, TotalCosts money, Invoiceapproveddate date)
insert into @data
Select InvoiceDate, InvoiceID_LawFirm, LawFirmOffice, 
sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
InvoiceApprovedDate
from CounselLink_Data_ToBill
--where Coalesce(DoNotBill,0)=0 
where coalesce(AlreadyBilled, 0)=0
and InvoiceApprovedDate is not null
and coalesce(code, '0') in ('20531', '19822399')
and NCC=1
group by invoicedate, invoiceid_lawfirm, lawfirmoffice, InvoiceApprovedDate

--union

--Select InvoiceDate, InvoiceID_LawFirm, LawFirmOffice, 
--sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
--sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
--InvoiceApprovedDate
--from CounselLink_Data2
--where Coalesce(DoNotBill,0)=0 
--and InvoiceApprovedDate is not null
--and coalesce(code, '0') in ('20531', '19822399')
--and NCC=1
--group by invoicedate, invoiceid_lawfirm, lawfirmoffice, InvoiceApprovedDate

declare @final table (invoicedate date, invoicenumber nvarchar(100), LocalCounsel nvarchar(100),
TotalFees money, TotalCosts money,Invoiceapproveddate date,  FirstStateFees money, FirstStateCosts money, RoyalFees money, RoyalCosts money,
SentryFees money, SentryCosts money, TravelersFees money, TravelersCosts money, WausauFees money, WausauCosts money, ACEFees money, ACECosts money, 
HarborFees money, Harborcosts money)

insert into @final
select d.*, 
--FIRST STATE
.0357*TotalFees as FirstStateFees, .0357*TotalCosts as FirstStateCosts,
--ROYAL
.0215*TotalFees as RoyalFees, .0215*TotalCosts as RoyalCosts,
--SENTRY
.1275*TotalFees as SentryFees, .1275*TotalCosts as SentryCosts,
--TRAVELERS
case when Invoiceapproveddate <='5/31/2012' then .1275*TotalFees else .085*TotalFees end as TravelersFees, 
case when Invoiceapproveddate <='5/31/2012' then .1275*TotalCosts else .085*TotalCosts end as TravelersCosts,
--WAUSAU
.1275*TotalFees as WausauFees, .1275*TotalCosts as WausauCosts,
--ACE
Case when Invoiceapproveddate >='5/31/2012' then .0425*TotalFees else 0 end as ACEFees, 
Case when Invoiceapproveddate >='5/31/2012' then .0425*TotalCosts else 0 end as ACECosts,
--HARBOR/CNA
.0425*TotalFees as HarborFees, .0425*TotalCosts as HarborFees
from @data d

order by LocalCounsel, invoicedate, invoicenumber

select InvoiceDate, Invoiceapproveddate,invoicenumber, TotalFees, TotalCosts, 
case
when  @CarrierName ='First State' then FirstStateFees
when @CarrierName ='Royal' then RoyalFees
when @CarrierName ='Sentry' then SentryFees
when @CarrierName ='Travelers' then TravelersFees
when @CarrierName ='Wausau' then WausauFees
when @CarrierName ='International' then ACEFees
when @CarrierName ='CNA' then HarborFees end as InsurerShareFees, 
case
when  @CarrierName ='First State' then FirstStateCosts
when @CarrierName ='Royal' then RoyalCosts
when @CarrierName ='Sentry' then SentryCosts
when @CarrierName ='Travelers' then TravelersCosts
when @CarrierName ='Wausau' then WausauCosts
when @CarrierName ='International' then ACECosts
when @CarrierName ='CNA' then HarborCosts end as InsurerShareCosts , @InvoiceNumber as GPCInvoiceNumber
from @final


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailDefenseNCCBill_AllCarriers_20130423] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailDefenseNCCBill_AllCarriers_20130423] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailDefenseNCCBill_AllCarriers_20130423] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailDefenseNCCBill_AllCarriers_20130423] TO [power_user]
GO
