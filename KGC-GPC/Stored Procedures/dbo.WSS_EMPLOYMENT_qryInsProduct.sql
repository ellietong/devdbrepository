SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryInsProduct] (@ProductID			bigint, 
													  @ClaimantEmploymentID bigint, 
													  @StartDate			datetime, 
													  @EndDate				datetime,
													  @IsPrimaryProduct		int, 
													  @Emailaddress			nvarchar(100),
													  @Notes				nvarchar(max))
AS
SET NOCOUNT ON

INSERT INTO tblClaimantProduct (ProductID, ClaimantEmploymentID, StartDate, PrimaryProduct, EndDate, Notes, SPUsername)
VALUES (@ProductID, @ClaimantEmploymentID, @StartDate, @IsPrimaryProduct, @EndDate, @Notes, @Emailaddress)
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [power_user]
GO
