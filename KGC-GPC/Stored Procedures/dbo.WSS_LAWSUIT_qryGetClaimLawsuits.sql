SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits](@LawsuitID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN	
		SELECT cl.ClaimLawsuitID, 
			   COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') + COALESCE(' ' + MiddleName, '')AS ClaimantName, 
			   ClaimLawsuitStatus, 
			   CONVERT(nvarchar(250), (CASE WHEN A.ServiceDate IS NULL 
											THEN CASE WHEN FileDate is null 
												 THEN CASE WHEN a.ReceivedDate is null 
													  THEN '1/1/1960' 
													  ELSE a.ReceivedDate 
													  end 
												 ELSE FileDate 
												 END 
											 ELSE A.ServiceDate 
											 END), 101) AS ServiceDate,
				Matter
		FROM tblClaimLawsuit cl
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		INNER JOIN tblLawsuit l on l.LawsuitID = cl.LawsuitID
		INNER JOIN tblMatter m on m.MatterID = c.MatterID
        LEFT JOIN  (SELECT MIN(servicedate) AS ServiceDate, ReceivedDate, ClaimLawsuitID, AllegationTypeID, MaterialID 
					FROM tblAllegation
					group by ClaimLawsuitID, ReceivedDate, AllegationTypeID, MaterialID)  a						ON cl.ClaimLawsuitID = a.ClaimLawsuitID		
		WHERE cl.LawsuitID = @LawsuitID
		AND IsPrimaryStatus=1
		ORDER BY LastName
	END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [power_user]
GO
