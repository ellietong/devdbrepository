SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryInternalNewDocumentEmail]
As
Begin

/*Query run daily to send an internal email listing any new documents. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(2000)

SET @Subject =    'MSA: New Documents Uploaded as of ' + convert(varchar, getdate(), 107) 
--Set body font, insert KCIC logo/banner at the top of the email  
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>Below are the new uploaded documents for review.</H4>'

--Create temp table to hold all new documents
DECLARE @NewDocuments TABLE (docID nvarchar(200), firm nvarchar(100),  claimantpersonalinfoid int,
							fname nvarchar(100), lname nvarchar(100), documenttype nvarchar(100), NCCReferenceID nvarchar(20),
							doctitle nvarchar(100), person nvarchar(100))
INSERT INTO @NewDocuments
SELECT d.DocumentID, f.firm,
      cpi.ClaimantPersonalInfoID,
      cpi.FirstName, cpi.LastName, ClaimantDocumentType, NCCReferenceID, DocumentTitle, Email
FROM tblDocuments d
inner join tblMultiClaimantDocument mcd on mcd.DocumentID =d.DocumentID
inner join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID=d.DetailDocumentTypeID
inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID=mcd.ClaimantPersonalInfoID
inner join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN vtblPerson p ON p.Email = d.CreatedBy
INNER JOIN vtblFirm f ON f.FirmID = p.firmid
WHERE datecreated BETWEEN GETDATE()-1 AND GETDATE() 
      --Do not include any documents uploaded by us.
      AND RIGHT(CreatedBy, 11) != 'kcicllc.com'
      AND detailDocumentTypeID IN (11,25,29,34, 44, 50)
      
GROUP BY d.DocumentID, f.firm,
    cpi.ClaimantPersonalInfoID,
      cpi.FirstName, cpi.LastName, ClaimantDocumentType, NCCReferenceID, DocumentTitle, Email


DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar(max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar(max), @ClaimantID bigint, @ClaimantList nvarchar(max), @NCCRef nvarchar(11)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for
SELECT DocID, COALESCE(Lname, '') + ', ' +  COALESCE(Fname, ''), ClaimantPersonalInfoID, NCCReferenceID FROM @NewDocuments
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID, @NCCRef
WHILE @@FETCH_STATUS = 0
BEGIN

IF(LEN(@ClaimantList)=0 OR @ClaimantList IS NULL)
BEGIN
	
SET @ClaimantList = '<ul> <a href="admin.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+'">'  + @Claimant + coalesce(' ('+ CONVERT(nvarchar(50), @NCCRef) + ')', '') +  '</a></ul>'
END
ELSE
BEGIN
	
SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="admin.kcicextranet.com/clients/msa/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+'">' + COALESCE(@Claimant, '') + coalesce(' ('+ CONVERT(nvarchar(50), @NCCRef) + ')', '') + '</a></ul>'

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID, @NCCRef
PRINT @@FETCH_STATUS
IF @DocID <> @LastDocID OR @@FETCH_STATUS <> 0
BEGIN
	
INSERT INTO @ClaimantPerDoc
	
SELECT @LastDocID, @ClaimantList
	
SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints
	


--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @BodyMessage + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'

--SELECT docTitle, cl.ClaimantList, documenttype, firm, person
--FROM @NewDocuments C
--INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
--GROUP BY docTitle, cl.ClaimantList, documenttype, firm, person

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable


SET @BodyMessage = @BodyMessage + N'<table>' + 
	
	
N'<tr><th>Document Title</th><th>Linked Claimants (NCC #) </th><th>Document Type</th><th>Firm</TH><th>User</TH>' +
	
	
CAST ( (
SELECT td = docTitle, '', td = cl.ClaimantList, '', td = documenttype, '', td = firm, '', td = person, ''
FROM @NewDocuments C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
GROUP BY docTitle, cl.ClaimantList, documenttype, firm, person

for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
N'</table>' +  N'<br/>'	







            
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person respoDemo Clientble for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

--Check for new documents
IF(SELECT COUNT(DISTINCT docid)
FROM @newdocuments) <> 0      
BEGIN
      --Send email
      EXEC msdb.dbo.sp_send_dbmail
      @profile_name='MSAProfile',
      @recipients = 'hubbardb@kcicllc.com', --'MSA@kcicllc.com',
      @copy_recipients = '',--'selfL@kcicllc.com; shockleym@kcicllc.com; johnn@kcicllc.com; montgomeryi@kcicllc.com; dayc@kcicllc.com',
      @subject= @subject,
      @body= @BodyMessage,
      @body_Format = 'html'
      END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalNewDocumentEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalNewDocumentEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalNewDocumentEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalNewDocumentEmail] TO [power_user]
GO
