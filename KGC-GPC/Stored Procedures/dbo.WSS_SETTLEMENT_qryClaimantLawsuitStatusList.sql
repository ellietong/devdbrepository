SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryClaimantLawsuitStatusList](@emailaddress nvarchar(max))
AS
SET NOCOUNT ON

IF(SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) = 481 OR @emailaddress LIKE '%@kcicllc.com'--NCC/KCIC
BEGIN

SELECT ClaimLawsuitStatusID, 
	ClaimLawsuitStatus 
FROM tblClaimLawsuitStatus
WHERE ClaimLawsuitStatusID IN (39)
--UNION
--SELECT NULL, ''
--ORDER BY ClaimLawsuitStatus

END 
ELSE
SELECT ClaimLawsuitStatusID, 
	ClaimLawsuitStatus 
FROM tblClaimLawsuitStatus
WHERE ClaimLawsuitStatusID IN (39)
--UNION
--SELECT NULL, ''
--ORDER BY ClaimLawsuitStatus


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryClaimantLawsuitStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryClaimantLawsuitStatusList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryClaimantLawsuitStatusList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryClaimantLawsuitStatusList] TO [power_user]
GO
