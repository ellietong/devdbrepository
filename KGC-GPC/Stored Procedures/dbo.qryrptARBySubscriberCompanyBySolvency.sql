SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptARBySubscriberCompanyBySolvency]
(@AllocationID int = NULL)
As

SET NOCOUNT ON

--DECLARE @allocationid int
--SET @allocationid = 1

DECLARE @qryARByAllocationID TABLE (AllocationID int, 
									AllocationAmtActual money, 
									AllocationAmtAllocation money, 
									SubscriptionCompanySolvency varchar(256) )
DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM tblAllocation

DECLARE @Policy TABLE (PolicyID int)
INSERT INTO @Policy
	SELECT PolicyID
	FROM vPolicy
	GROUP BY PolicyID

INSERT INTO @qryARByAllocationID
	SELECT a.AllocationID, 
		SUM(s.SubscriptionActualPercentOfPolicy     * ar.Allocation) AS AllocationAmtActual, 
		SUM(s.SubscriptionAllocationPercentOfPolicy * ar.Allocation) AS AllocationAmtAllocation,
		s.SubscriptionCompanySolvency 
	FROM ((@Policy p 
		INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID) 
		INNER JOIN tblAllocationResults ar ON p.PolicyID = ar.PolicyID) 
		LEFT JOIN tblAllocation a ON a.AllocationID = ar.AllocationID
	GROUP BY a.AllocationID, s.SubscriptionCompanySolvency
	ORDER BY a.AllocationID

SELECT AllocationID, SUM(AllocationAmtActual) as SumOfAllocationActual, SUM(AllocationAmtAllocation) as SumOfAllocationAllocation, 
	CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END AS 'SubscriberSolvencyGroup'
FROM @qryARByAllocationID
WHERE AllocationID >= COALESCE(@allocationID, 0) AND AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID)
GROUP BY AllocationID, CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyBySolvency] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyBySolvency] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyBySolvency] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyBySolvency] TO [power_user]
GO
