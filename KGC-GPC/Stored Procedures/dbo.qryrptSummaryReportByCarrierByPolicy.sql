SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptSummaryReportByCarrierByPolicy]
(@ExhaustionDateID int, @LimitTypeID int, @ExhaustionTypeID int)
As
Set Nocount on

 --Declare @LimitTypeID int
 --Declare @ExhaustionDateID int
 --Declare @ExhaustionTypeID int
 --Set @LimitTypeID = 2
 --Set @ExhaustionDateID = 1262
 --Set @ExhaustionTypeID = 6


--Previous Exhaustion
Declare @Exhaustion Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint)
Insert Into @Exhaustion
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation,
	DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation,
	LedgerID
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<@ExhaustionDateID

Declare @PreviousExhaustionTotals table (ExhaustionTypeID int, PolicyDateID int, PreviousAllocationAmt money)
Insert Into @PreviousExhaustionTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAllocationAmt
From @Exhaustion
Group By ExhaustionTypeID, PolicyDateID


--Current Allocation
Declare @CurrentAllocation Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint)
Insert Into @CurrentAllocation
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation,
	DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation,
	LedgerID
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID=@ExhaustionDateID

Declare @CurrentAllocationTotals table (ExhaustionTypeID int, PolicyDateID int, Indemnity money, Defense money,
	DefenseOutsideLimits money, TotalDefense money, SIRIndemnityAllocation money, SIRDefenseAllocation money)
Insert Into @CurrentAllocationTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(IndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(DefenseAllocation), 0) as Defense,
	coalesce(Sum(DefenseOutsideLimitsAllocation),0) as DefenseOutsideLimits,
	coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
	coalesce(Sum(SIRIndemnityAllocation), 0) as SIRIndemnity,
	coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense
From @CurrentAllocation
Group By ExhaustionTypeID, PolicyDateID

-- Header Summary
Declare @HeaderSummary Table (ExhaustionType nvarchar(100), Description nvarchar(100), DefenseForm nvarchar(10))
Insert into @HeaderSummary
Select ExhaustionType, [Description], 'H'
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, [Description], 'I'
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, [Description], 'G'
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, [Description], ''
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID

--Query Results
Declare @Results Table(OrderID int,policyid bigint, policydateid bigint, carriername nvarchar(100), layer int, policynum nvarchar(100), pstartdate datetime, penddate datetime, attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), Solvency nvarchar(100), TotalPercentage float, Indemnity money, Defense money, DefenseOutsideLimits money, TotalDefense money, PreviousAllocationAmt money,
	Balance money)
Insert Into @Results

--London
select  '1' as OrderID, p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, pd.pstartdate, pd.penddate,
	pl.attachmentpoint, pl.perocclimit*Round(Sum(Percentage), 5) as perocclimit, pl.aggregatelimit*Round(Sum(Percentage), 5) as aggregatelimit,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
CASE	When subscriptionCompanySolvency like 'Solvent' or  subscriptionCompanySolvency like 'unknown' Then 'Solvent'
	When subscriptionCompanySolvency like 'insolvent' Then 'Insolvent'
	End As Solvency,
	Round(Sum(Percentage), 5) as TotalPercentage,
	coalesce(Indemnity,0)*Round(Sum(Percentage), 5) as Indemnity, 
	coalesce(Defense,0)*Round(Sum(Percentage), 5) as Defense, 
	coalesce(DefenseOutsideLimits,0)*Round(Sum(Percentage), 5) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0)*Round(Sum(Percentage), 5) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0)*Round(Sum(Percentage), 5) as PreviousAllocationAmt,
 	(coalesce(AggregateLimit,0)*Round(Sum(Percentage), 5) - coalesce(Indemnity,0)*Round(Sum(Percentage), 5) - coalesce(Defense,0)*Round(Sum(Percentage), 5) - coalesce(PreviousAllocationAmt,0)*Round(Sum(Percentage), 5)) as Balance
from tblpolicy p 
	Left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblSubscriptionCoverage sc on p.PolicyID=sc.PolicyID
	left join tblSubscriberCompany subc on sc.SubscriberID=Subc.subscriberID
	Left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where limittypeid = @LimitTypeID and CAT.ExhaustionTypeID = @ExhaustionTypeID and provisionlabelID=1 and C.CarrierID=20
group by p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, indemnity, defense,
	defenseoutsidelimits, pd.pstartdate, pd.penddate, pl.attachmentpoint,
	pl.perocclimit, pl.aggregatelimit, totaldefense, pro.ProvisionValueID,
	subc.SubscriptionCompanySolvency, PreviousAllocationAmt
Union
--Most Carriers
select '1' as OrderID, p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, pd.pstartdate, pd.penddate,
	pl.attachmentpoint, pl.perocclimit, pl.aggregatelimit,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	'' As Solvency,
	'' as TotalPercentage,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
 	(coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)  - coalesce(PreviousAllocationAmt,0)) as Balance
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where limittypeid = @LimitTypeID and CAT.ExhaustionTypeID = @ExhaustionTypeID and provisionlabelID=1 and c.carrierid not in (10,20,20001,20002)
Union
--NSI Producer
select '2' as OrderID, p.policyiD, pd.policydateid, c.carriername, null as layer, p.policynum as policynum, null as pstartdate, null as penddate,
	null as attachmentpoint, null as perocclimit, null as aggregatelimit, '' as DefenseForm,
	'' As Solvency,
	'' as TotalPercentage,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
 	null as Balance
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
Where limittypeid = @LimitTypeID and CAT.ExhaustionTypeID = @ExhaustionTypeID  and c.carrierid in (20002)
Union
--ACF Refund
select '2' as OrderID, p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, pd.pstartdate, pd.penddate,
	pl.attachmentpoint, pl.perocclimit, pl.aggregatelimit,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	'' As Solvency,
	'' as TotalPercentage,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
 	(coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)  - coalesce(PreviousAllocationAmt,0)) as Balance
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where limittypeid = @LimitTypeID and CAT.ExhaustionTypeID = @ExhaustionTypeID and provisionlabelID=1 and c.carrierid in (20001)

Select R.*, ExhaustionType, [Description]
From @Results R
Inner Join @HeaderSummary HS on R.DefenseForm=HS.DefenseForm

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GO
