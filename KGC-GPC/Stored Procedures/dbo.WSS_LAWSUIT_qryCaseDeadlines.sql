SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryCaseDeadlines]
	-- Add the parameters for the stored procedure here
@LawsuitID int=null, @ClaimID int=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


--DECLARE @ClaimID int = 13420
--DECLARE @LawsuitID int = 34755

DECLARE @ClaimLawsuitID int, @TrialNotes nvarchar(max)
DECLARE @TrialNoteText TABLE (ClaimLawsuitID int, TrialNoteText nvarchar(max))
DECLARE @TrialNoteID int = 0

IF object_id('tempdb..#TrialNoteText') IS NOT NULL
BEGIN
   DROP TABLE #TrialNoteText
END
CREATE TABLE #TrialNoteText (TrialNoteID int, ClaimLawsuitID int, TrialNoteText nvarchar(max))

DECLARE TrialNote_Cursor CURSOR Local Fast_Forward FOR
	SELECT ClaimLawsuitID, TrialNotes
	FROM tblClaimLawsuit
	WHERE ClaimID = @ClaimID AND LawsuitID = @LawsuitID AND LEN(TrialNotes) >= 1

OPEN TrialNote_Cursor

FETCH NEXT FROM TrialNote_Cursor 
INTO @ClaimLawsuitID, @TrialNotes

WHILE @@FETCH_STATUS = 0
BEGIN

	DECLARE @TrialNote nvarchar(max) = (
		SELECT TrialNotes
		FROM tblClaimLawsuit
		WHERE ClaimLawsuitID = @ClaimLawsuitID
	)

	DECLARE @TrialNoteLength int 
	= (
		SELECT LEN(TrialNotes)
		FROM tblClaimLawsuit
		WHERE ClaimLawsuitID = @ClaimLawsuitID
	)

	DECLARE @Index int = -1

	DECLARE @MoveNote nvarchar(max) = (
	SELECT RIGHT(@TrialNote, (LEN(@TrialNote)-@Index))
	)

	DECLARE	@Fragment100 nvarchar(150) 
	DECLARE @TrialNoteRight nvarchar(max)
	DECLARE @Remainder int 
	DECLARE @Count int = 0

	WHILE 1=1
		BEGIN
			SET @Count = @Count + 1
			SET @TrialNoteID = @TrialNoteID + 1
			SET @Fragment100 = (SELECT LEFT(@TrialNote, 99)) 

			IF (LEN(@TrialNote)-99)>= 1
				BEGIN

					SET @TrialNoteRight = (SELECT RIGHT(@TrialNote, LEN(@TrialNote)-99))
					SET @Remainder = (SELECT CHARINDEX(' ', @TrialNoteRight))
					SET @Fragment100 =  @Fragment100 + LEFT(@TrialNoteRight, @Remainder) + '<br/>'
					SET @TrialNoteRight = (SELECT RIGHT(@TrialNote, LEN(@TrialNote)-99- @Remainder))
					SET @TrialNote = (RIGHT(@TrialNoteRight, LEN(@TrialNoteRIGHT)+1))

					INSERT INTO #TrialNoteText
						SELECT @TrialNoteID, @ClaimLawsuitID, LTRIM(@Fragment100)

				END
				ELSE
				BEGIN
					SET @Fragment100 = @TrialNote
					INSERT INTO #TrialNoteText
						SELECT @TrialNoteID, @ClaimLawsuitID, LTRIM(@Fragment100) + '<br/>'
					BREAK
				END
		END

	FETCH NEXT FROM TrialNote_Cursor
	INTO @ClaimLawsuitID, @TrialNotes

END
CLOSE TrialNote_Cursor
DEALLOCATE TrialNote_Cursor

	--SELECT * FROM #TrialNoteText 


DECLARE @FullTrialNote nvarchar(max) = ''


IF object_id('tempdb..#FullTrialNoteText') IS NOT NULL
BEGIN
   DROP TABLE #FullTrialNoteText
END
CREATE TABLE #FullTrialNoteText (ClaimLawsuitID int, FullTrialNoteText nvarchar(max))


DECLARE @FullTrialNotes nvarchar(1000)
DECLARE @FullTrialNoteID int

DECLARE FullTrialNote_Cursor CURSOR Local Fast_Forward FOR
SELECT DISTINCT max(TrialNoteID), ClaimLawsuitID
FROM #TrialNoteText
GROUP BY ClaimLawsuitID

OPEN FullTrialNote_Cursor

FETCH NEXT FROM FullTrialNote_Cursor 
INTO @FullTrialNoteID, @ClaimLawsuitID

WHILE @@FETCH_STATUS = 0
	BEGIN

	DECLARE @CountID int = 0
		SET @TrialNoteID = @FullTrialNoteID - (SELECT count(trialnoteid) from #TrialNoteText WHERE ClaimLawsuitID = @ClaimLawsuitID) + 1 --@TrialNoteID + 1
	WHILE (SELECT MAX(TrialNoteID) FROM #TrialNoteText WHERE ClaimLawsuitID =  @ClaimLawsuitID)  = @FullTrialNoteID
		BEGIN
			SET @FullTrialNote = @FullTrialNote + (SELECT TrialNoteText FROM #TrialNoteText WHERE ClaimLawsuitID = @ClaimLawsuitID AND TrialNoteID = @TrialNoteID)
			IF @TrialNoteID = @FullTrialNoteID
				BEGIN
					IF (SELECT Count(ClaimLawsuitID) FROM #FullTrialNoteText WHERE ClaimLawsuitID = @ClaimLawsuitID) < 1
						BEGIN
							INSERT INTO #FullTrialNoteText
								SELECT @ClaimLawsuitID, @FullTrialNote
							--SELECT @FullTrialNote, @ClaimLawsuitID
							SET @FullTrialNote = ''
							--SELECT @TrialNoteID
						END
					IF @TrialNoteID = @FullTrialNoteID
						BEGIN	
							--SELECT @TrialNoteID
							BREAK					
						END
				END		

				SET @TrialNoteID = @TrialNoteID + 1
		END

		FETCH NEXT FROM FullTrialNote_Cursor
		INTO @FullTrialNoteID, @ClaimLawsuitID

	END
CLOSE FullTrialNote_Cursor
DEALLOCATE FullTrialNote_Cursor


--SELECT * FROM #FullTrialNoteText




	SELECT cl.ClaimLawsuitID,
			ExpertDesignationDate,
			ExpertReportDate,
			ExpertDepositionDate,
			ExhibitListDate,
			StatusConferenceDate,
			TrialNotes,
			FullTrialNoteText 
	FROM tblClaimLawsuit cl
		LEFT JOIN #FullTrialNoteText ftnt ON cl.ClaimLawsuitID = ftnt.ClaimLawsuitID
	WHERE LawsuitID = @LawsuitID AND ClaimID = @ClaimID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryCaseDeadlines] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryCaseDeadlines] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryCaseDeadlines] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryCaseDeadlines] TO [power_user]
GO
