SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Proc [dbo].[qryrptDetailDefenseBill_Travelers_2012Q1Q2 re-bill]
(@CarrierName Nvarchar(50), @InvoiceNumber Nvarchar(50))
As
Set Nocount on

--declare @CarrierName Nvarchar(50)
--declare @InvoiceNumber Nvarchar(50)

--set @CarrierName= 'First State'
--set @InvoiceNumber = 'GPC20120831'



declare @Data table (file# int, code nvarchar(250), plaintiff nvarchar(100), disease nvarchar(50), Jurisdiction nvarchar(100),
 State nvarchar(5),LocalCounsel nvarchar(500), invoiceid_lawfirm nvarchar(100), invoicedate date, fees float, costs float, billedbyRiley bit,
 invoiceapproveddate date, motiononly int)
 
insert into @Data
select coalesce(c.file#, 0), Coalesce(CODE,'0'),
case when MotionOnly = 1 then 'Unknown Plaintiff' 
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
	 when CODE IN ('20531', '19822399') and NCC=0 then 'Master Asbestos File'
	 else PLast+', '+PFirst end as Plaintiff, 
	 Disease, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
	 else c.Jurisdiction end as Jurisdiction, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
	 else c.State end as State,	 	 
d.LawFirmOffice,InvoiceID_LawFirm, d.InvoiceDate,  
sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
BilledByRiley, InvoiceApprovedDate, coalesce(motiononly,0)

from CounselLink_Data1 d
left join (select plaintiff, newfileno, file#, billedbyriley 
				from Claims_AdditionalClaimNumbers 
				where file# is not null
				group by plaintiff, newfileno, file#, billedbyriley) a on a.NewFileNo = d.CODE
left join CLAIMS_DATA c on c.file#=a.file#

where
coalesce(DoNotBill,0)=0 --Excludes Alston Bird
and coalesce(alreadybilled,0)=0 --Excludes invoices we can tell Riley already billed
and InvoiceApprovedDate is not null --Excludes invoices that have not been approved
and ncc=0
group by coalesce(c.file#, 0), CODE, InvoiceID_LawFirm, d.InvoiceDate, lawfirmoffice, ChargeType, 
case when MotionOnly = 1 then 'Unknown Plaintiff'
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
	 when CODE IN ('20531', '19822399') and NCC=0 then 'Master Asbestos File'
	 else PLast+', '+PFirst end,
 Disease, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
	 else c.Jurisdiction end, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
	 else c.State end , BilledByRiley, InvoiceApprovedDate,coalesce(motiononly,0)

Union ALL

select coalesce(c.file#, 0), Coalesce(CODE,'0'),
case when MotionOnly = 1 then 'Unknown Plaintiff' 
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
	 when CODE IN ('20531', '19822399') and NCC=0 then 'Master Asbestos File'
	 else PLast+', '+PFirst end as Plaintiff, 
	 Disease, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
	 else c.Jurisdiction end as Jurisdiction, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
	 else c.State end as State,	 	 
d.LawFirmOffice,InvoiceID_LawFirm, d.InvoiceDate,  
sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
BilledByRiley, InvoiceApprovedDate,coalesce(motiononly,0)

from CounselLink_Data2 d
left join (select plaintiff, newfileno, file#, billedbyriley 
				from Claims_AdditionalClaimNumbers 
				where file# is not null
				group by plaintiff, newfileno, file#, billedbyriley) a on a.NewFileNo = d.CODE
left join CLAIMS_DATA c on c.file#=a.file#

where
coalesce(DoNotBill,0)=0 --Excludes Alston Bird
and InvoiceApprovedDate is not null --Excludes invoices that have not been approved
and NCC=0
group by coalesce(c.file#, 0), CODE, InvoiceID_LawFirm, d.InvoiceDate, lawfirmoffice, ChargeType, 
case when MotionOnly = 1 then 'Unknown Plaintiff'
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
	 when CODE IN ('20531', '19822399') and NCC=0 then 'Master Asbestos File'
	 else PLast+', '+PFirst end,
 Disease, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
	 else c.Jurisdiction end, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
	 else c.State end, BilledByRiley, InvoiceApprovedDate,coalesce(motiononly,0)
	 
	 
	 
declare @final table (file# int, code nvarchar(250), plaintiff nvarchar(100), disease nvarchar(50), Jurisdiction nvarchar(100),
 State nvarchar(5),LocalCounsel nvarchar(500), invoiceid_lawfirm nvarchar(100), invoicedate date, fees float, costs float, billedbyRiley bit, invoiceapproveddate date, motiononly int,
 FirstStateFees float, FirstStateCosts float, RoyalFees float, RoyalCosts float, SentryFees float, SentryCosts float, TravelersFees float, TravelersCosts float, WausauFees float, WausauCosts float,
 InternationalFees float, InternationalCosts float, CNAFees float, CNACosts float)
 
 insert into @final

--NOW PULL IN INSURER SHARES

Select D.*,--, case when invoiceapproveddate between '1/1/2012' and '3/31/2012' then '2012 Q1' else '2012 Q2' end as BillPeriod,
--FIRST STATE
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.0357)*fees 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0357)*fees)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .0357*fees end as FirstStateFees,
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.0357)*Costs 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0357)*Costs)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .0357*Costs end as FirstStateCosts,
--ROYAL		
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.colyear)/TotalYearsOfExposure)*.0215)*fees 
	 when BilledByRiley = 0 and Coalesce(IsColyear,0)=1 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0215)*fees)
	 when ((BilledByRiley = 0 and Coalesce(iscolyear,0)= 1 and GPCAcquisitionDate >1986) Or coalesce(IsColyear,0) <>1) then 0
		else .0215*fees end as RoyalFees,	
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.colyear)/TotalYearsOfExposure)*.0215)*Costs 
	 when BilledByRiley = 0 and Coalesce(IsColyear,0)=1 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.0215)*Costs)
	 when ((BilledByRiley = 0 and Coalesce(iscolyear,0) = 1 and GPCAcquisitionDate >1986) Or Coalesce(IsColyear,0) <>1) then 0
		else .0215*Costs end as RoyalCosts,		
--SENTRY		
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*fees 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*fees)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*fees end as SentryFees,
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*Costs 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*Costs)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*Costs end as SentryCosts,
--TRAVELERS		
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.1275)*fees 
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*fees end as TravelersFees,
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear +r.CNA)/TotalYearsOfExposure)*.1275)*Costs 
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*Costs end as TravelersCosts,
--WAUSAU
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*fees 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*fees)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*fees end as WassauFees,
case when motiononly = 1 then 0
	 when BilledByRiley = 1 then (((r.gpc + r.colyear+r.CNA)/TotalYearsOfExposure)*.1275)*Costs 
	 when BilledByRiley = 0 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR(FirstExposure))) *.1275)*Costs)
	 when BilledByRiley = 0 and GPCAcquisitionDate >1986 then 0
		else .1275*Costs end as WassauCosts,
--ACE		
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >1986 then 0
	 when invoiceapproveddate <='5/31/2012' then 0
		else .0425*fees end as InternationalFees,	
case when motiononly = 1 then 0
	 when GPCAcquisitionDate >1986 then 0
	 when invoiceapproveddate <='5/31/2012' then 0
		else .0425*Costs end as InternationalCosts,	
--HARBOR/CNA		
case when motiononly = 1 then 0
	 when coalesce(GPCAcquisitionDate, 0) <=1979 then .0425*fees else 0 end as CNAFees,
case when motiononly = 1 then 0
	 when coalesce(GPCAcquisitionDate, 0) <=1979 then .0425*Costs else 0 end as CNACosts								

From @data d
left join CLAIMS_DATA cd on cd.File#=d.file#
left join RileyBillingPercentages r on r.file#=d.file#


select code as file#, plaintiff, LocalCounsel, invoicedate,invoiceapproveddate, InvoiceID_lawfirm, fees, Costs, 
case
when  @CarrierName ='First State' then FirstStateFees
when @CarrierName ='Royal' then RoyalFees
when @CarrierName ='Sentry' then SentryFees
when @CarrierName ='Travelers' then TravelersFees
when @CarrierName ='Wausau' then WausauFees
when @CarrierName ='International' then InternationalFees
when @CarrierName ='CNA' then CNAFees end as InsurerShareFees, 
case
when  @CarrierName ='First State' then FirstStateCosts
when @CarrierName ='Royal' then RoyalCosts
when @CarrierName ='Sentry' then SentryCosts
when @CarrierName ='Travelers' then TravelersCosts
when @CarrierName ='Wausau' then WausauCosts
when @CarrierName ='International' then InternationalCosts
when @CarrierName ='CNA' then CNACosts end as InsurerShareCosts , @InvoiceNumber as GPCInvoiceNumber, @CarrierName as CarrierName	
from @final

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailDefenseBill_Travelers_2012Q1Q2 re-bill] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailDefenseBill_Travelers_2012Q1Q2 re-bill] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailDefenseBill_Travelers_2012Q1Q2 re-bill] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailDefenseBill_Travelers_2012Q1Q2 re-bill] TO [power_user]
GO
