SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] (@ClaimID int)
AS

--DECLARE @ClaimID int
--SET @ClaimID = 1

SELECT pd.SetPymtID, 
	COALESCE(sd.ClaimantIDed, 0) AS ClaimantIDed, 
	COALESCE(sd.DODCert, 0) AS DODCert, 
	COALESCE(sd.DxMedicals, 0) AS DxMedicals,  
	COALESCE(sd.EmployExpoHist,0) AS EmployExpoHist,  
	COALESCE(sd.EstatePapers,0) AS EstatePapers, 
	COALESCE(sd.productid,0) AS ProductID, 
	COALESCE(sd.SettlementConfirmation, 0) AS SettlementConfirmation, 
    COALESCE(sd.CheckRequest, 0) AS CheckRequest ,
	COALESCE(sd.PreTrialReport, 0) AS PreTrialReport ,
	COALESCE(sd.MedicareForms, 0) AS MedicareForms ,
	pd.ExecutedReleaseDate, 
	pd.ReleaseRqstDate,
	pd.ReleaseToPC, 
	COALESCE(rt.ReleaseType, '') as ReleaseType, 
	cpi.MedicareBeneficiaryStatusID, 
	MedicareBeneficiaryStatus,
	ReleaseToPC, 
		DeceasedDate, 
	SettlePaymentComments,
	COALESCE(MedicareReported, 0) AS MedicareReported, 
	pd.ReleaseTypeID,
	MedicareFullyDocumented
FROM tblSettlePaymentDocumentation pd
	LEFT JOIN tblSettlementDocumentation sd		ON sd.setpymtID=PD.SetPymtID
	LEFT JOIN tblReleaseType rt					ON pd.ReleaseTypeID = rt.ReleaseTypeID
	LEFT JOIN tblClaimLawsuit cl				ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaim c						ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblMedicareBeneficiaryStatus mbs	ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID
WHERE pd.ClaimLawsuitID IN (SELECT DISTINCT ClaimLawsuitID 
							FROM tblClaimLawsuit 
							WHERE ClaimID = @ClaimID)


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlePaymentDocumentation] TO [power_user]
GO
