SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_REPORTS_YEARLY_qryYearlyNewFilings] (@YearID int, 
                                                           @MatterID int) 
AS
SET NOCOUNT ON
    
----The following variables can be used for testing.
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @PreviousMonthYTD int
SET @PreviousMonthYTD = CASE WHEN @yearID = YEAR(GETDATE()) THEN (MONTH(GETDATE())-1) ELSE 12 END

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

SELECT COUNT(distinct(cl.ClaimLawsuitID))
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
    LEFT JOIN (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
    LEFT  JOIN tblState s                         ON l.StateID = s.StateID
WHERE  MONTH(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) <= @PreviousMonthYTD
	AND YEAR(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) = @YearID 
	AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryYearlyNewFilings] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryYearlyNewFilings] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryYearlyNewFilings] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryYearlyNewFilings] TO [power_user]
GO
