SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--USE [KGC-GPC]
--GO
--/****** Object:  StoredProcedure [dbo].[qryrptCashFlowRecentActivity]    Script Date: 9/24/2013 10:45:24 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE Procedure [dbo].[qryrptCashFlowRecentActivity_JournalEntry] (@MatterID int, 
@BalanceStartDate datetime, 
@BalanceEndDate datetime,
@currentdate datetime)

As
Set NoCount On

--Declare @MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime, @currentdate datetime
--Set @MatterID = 1
--Set @BalanceStartDate = '5/24/2014'
--Set @BalanceEndDate = '5/30/2014'

--Declare @StartingBalance money
--Set @StartingBalance = '0.00'  + (
----Coalesce((Select SUM(CashActivityAmount) From tblMiscCashActivity mca 
----Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID 
----where CashActivityDate >= '4/28/2013' and CashActivityDate <= @BalanceEndDate and MatterID = @MatterID),0)
----+
--Coalesce((Select SUM(SourceAmount)*-1 From tblCheck c
--Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
--Where SourceDate >= '4/28/2013' and SourceDate <= @BalanceEndDate and MatterID = @MatterID),0)
--)



DECLARE @List TABLE (CheckID int, CheckInfo nvarchar(max))
DECLARE @LastCheckID int
DECLARE @CheckID int
DECLARE @CheckInfo nvarchar(max)
Declare @CheckInfoConCat nvarchar(max)
Declare @PlaintiffCounsel nvarchar(max)
declare @LastPlaintiffCounsel nvarchar(max)


DECLARE Check_curs CURSOR FOR

Select c.CheckID,  'FLEET & GL - ' , FirstName + ' ' + LastName + ' (ID: ' +convert(nvarchar(50),cpi.ClaimantPersonalInfoID)  
--+')' + ', ' +  Disease + ', $' + convert(nvarchar(50),convert(money,ps.PaymentAmount),1)
From tblCheck c
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
Inner Join tblMatter m on c.MatterID = m.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
inner join tblClaim cla on cla.ClaimID = cl.ClaimID
inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = cla.ClaimantPersonalInfoID
inner join tblClaimDisease cd on cd.ClaimID = cla.ClaimID
inner join tblDisease d on d.DiseaseID = cd.DiseaseID
Where PrimaryDisease = 1  and IsPrimaryForClaim = 1 and IsCurrentClaim = 1
and (SourceDate >= @BalanceStartDate and 
(SourceDate <= @BalanceEndDate) --AND c.MatterID = @MatterID
 AND c.FundingTypeID = 2)
group by c.CheckID, PlaintiffCounsel + ' - (FOR ' , FirstName + ' ' + LastName + ' (ID: ' +convert(nvarchar(50),cpi.ClaimantPersonalInfoID)  
order by c.CheckID, PlaintiffCounsel + ' - (FOR ' , FirstName + ' ' + LastName + ' (ID: ' +convert(nvarchar(50),cpi.ClaimantPersonalInfoID)  

OPEN Check_curs
FETCH NEXT FROM Check_curs INTO @CheckID, @PlaintiffCounsel, @CheckInfo
WHILE @@FETCH_STATUS=0 
BEGIN

		IF @CheckID = @LastCheckID
		BEGIN

		Set @CheckInfoConCat = @CheckInfoConCat + '), '  + @CheckInfo

		END
		ELSE
		BEGIN
		
		IF @LastCheckID is not null
		BEGIN
		Insert into @List
		VALUES (@LastCheckID,@LastPlaintiffCounsel + ' ' + @CheckInfoConCat+ ')')
		END

		

		Set @CheckInfoConCat = @Checkinfo
		SET @LastCheckID = @CheckID
		set @LastPlaintiffCounsel = @PlaintiffCounsel

		END

FETCH NEXT FROM Check_curs INTO @CheckID,@PlaintiffCounsel , @CheckInfo


END
CLOSE Check_curs
DEALLOCATE Check_curs


Insert into @List
		VALUES (@LastCheckID,@LastPlaintiffCounsel + ' ' + @CheckInfoConCat+ ')')

--SELECT * FROM @List
			

Declare @Debit money
set @Debit = '0.00' +
(Coalesce((Select SUM(sourceAmount)*-1  
From tblCheck c
Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
Where SourceDate >= '4/28/2013' and SourceDate < @BalanceStartDate and MatterID = @MatterID),0))

Declare @Credit money
Set @Credit = @Debit*-1

--set @Credit = '0.00' +
--(Coalesce((Select SUM(sourceAmount)*-1 
--From tblCheck c
--Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
--Where SourceDate >= '4/28/2013' and SourceDate < @BalanceStartDate and MatterID = @MatterID),0))



--Declare @Matter nvarchar(250)
--Set @Matter = (Select Matter from tblMatter where MatterID = @MatterID)

Declare @Detail table --(Matter nvarchar(250), 
(FundingType nvarchar(250), FundingSource nvarchar(250), SourceDate datetime, SourceNumber nvarchar(250), CreditAmount money,DebitAmount money,   Comment1 nvarchar(Max), UniqueCheck int IDENTITY(1,1))
Insert Into @Detail



--Settlement
Select-- Matter, 
FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1 as CreditAmount, SourceAmount as DebitAmount, CheckInfo
From tblCheck c
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
Inner Join tblMatter m on c.MatterID = m.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
inner join tblClaim cla on cla.ClaimID = cl.ClaimID
inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = cla.ClaimantPersonalInfoID
inner join tblClaimDisease cd on cd.ClaimID = cla.ClaimID
inner join tblDisease d on d.DiseaseID = cd.DiseaseID
left join @List list on list.CheckID = c.CheckID
Where  (SourceDate >= @BalanceStartDate and 
(SourceDate <= @BalanceEndDate) --AND c.MatterID = @MatterID
 AND c.FundingTypeID = 2)
Group by --Matter, 
FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1,SourceAmount, cft.FundingTypeID, list.CheckInfo, IsCheckCleared



Select
FundingType , 
FundingSource , 
SourceDate , 
SourceNumber , 
Case when FundingSource = 'Void' then DebitAmount else CreditAmount end as CreditAmount ,
Case when FundingSource = 'Void' then CreditAmount else DebitAmount end as DebitAmount ,   
Case when FundingSource = 'Void' then 'KCIC CASH' else Comment1 end as Comment1 , 
UniqueCheck,
Case when FundingSource = 'Void' then Comment1 else 'KCIC CASH' end as Comment2,
--'KCIC CASH' as Comment2, 
90500 as BU, 999500 as OU,'USD' as Curr, 
Case when FundingSource = 'Void' then 100030 else 154500 end as DebitAcct, 
Case when FundingSource = 'Void' then 154500 else 100030 end as CreditAcct,
@BalanceStartDate as BalanceStartDate, 
@BalanceEndDate as BalanceEndDate , @currentdate
from @Detail 
Order By SourceDate, SourceNumber 

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity_JournalEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity_JournalEntry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity_JournalEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity_JournalEntry] TO [power_user]
GO
