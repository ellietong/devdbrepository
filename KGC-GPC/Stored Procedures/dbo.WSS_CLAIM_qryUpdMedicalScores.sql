SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryUpdMedicalScores]
	(@ILODoctorID int, @PFTDoctorID int, 
	@ILOProfusion nvarchar(100), @ILODate datetime,
    @PFTDate datetime, 
    @PFTFVC_Actual float, @PFTFVC_Predicted float,
    @PFTFEV1_Actual float, @PFTFEV1_Predicted float,
    @PFTFEV1FVC_Actual float, @PFTFEV1FVC_Predicted float,
    @PFTTLC_Actual float, @PFTTLC_Predicted float,
    @PFTDLCO_Actual float, @PFTDLCO_Predicted float,
    @PFTTV float,
    @ClaimID bigint,  @Username nvarchar(250)
	
	)
AS
BEGIN

	UPDATE tblClaim
	SET ILODoctorID = @ILODoctorID,
      ILOProfusion = @ILOProfusion,
      ILODate = @ILODate,
      PFTDoctorID = @PFTDoctorID,
      PFTDate = @PFTDate,
      PFTFVC_Actual = @PFTFVC_Actual,
      PFTFVC_Predicted = @PFTFVC_Predicted,
      PFTFEV1_Actual = @PFTFEV1_Actual,
      PFTFEV1_Predicted = @PFTFEV1_Predicted,
      [PFTFEV1andFVC_Actual] = @PFTFEV1FVC_Actual,
      [PFTFEV1andFVC_Predicted] = @PFTFEV1FVC_Predicted,
      PFTTLC_Actual = @PFTTLC_Actual,
      PFTTLC_Predicted = @PFTTLC_Predicted,
      PFTDLCO_Actual = @PFTDLCO_Actual,
      PFTDLCO_Predicted = @PFTDLCO_Predicted,
      PFTTV = @PFTTV,
      SPUsername = @Username
      WHERE ClaimID = @ClaimID
      
   
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdMedicalScores] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdMedicalScores] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdMedicalScores] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdMedicalScores] TO [power_user]
GO
