SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_updDocument]
(
	@ClaimantDocumentTypeID int,					
	
	@Comments nvarchar(4000) = NULL,			--comments are optional		
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@DocID int,
	
	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = NULL,
	@Period nvarchar(50) = NULL,
	@DatePayable datetime = NULL,
	@FeeAmount money = NULL,
	@DisbursementAmount money = NULL,

	@ModifiedBy nvarchar(250),
	@DateModified datetime,
	@Approved bit = NULL,
	@ApprovedBy nvarchar(50) = NULL,
	@DateApproved	datetime = NULL,
	@DocumentSourceID int,
	@MatterID int = NULL
	
)

AS
UPDATE tblDocuments 
SET
	ClaimantDocumentTypeID	= @ClaimantDocumentTypeID,	
	Comments				= @Comments,	
	DocumentURL				= @DocumentURL,	
	DocumentTitle			= @DocumentTitle,
	ModifiedBy				= @ModifiedBy,
	DateModified			= @DateModified,
	DocumentSourceID		= @DocumentSourceID,
	MatterID				= @MatterID
	
WHERE DocumentID = @DocID

UPDATE tblDefenseInvoice
SET
	InvoiceNumber			= @InvoiceNumber,
	Period					= @Period,
	DatePayable				= @DatePayable
WHERE DocumentID = @DocID

Select DocumentID From tblDocuments Where DocumentID = @DocID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocument] TO [power_user]
GO
