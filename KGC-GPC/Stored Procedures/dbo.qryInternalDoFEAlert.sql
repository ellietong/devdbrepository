SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryInternalDoFEAlert] (@ClaimantPersonalInfoID bigint, @ClaimantEmploymentID bigint = null, @ClaimantProductID bigint = null, @Action nvarchar (3))
As
Begin

/*Query run daily to send an internal email listing any new documents. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (2000)

SET @Subject = 'MSA: ' +
CASE
	WHEN @ClaimantEmploymentID IS NOT NULL THEN 'Employment Start Date ' ELSE 'Product Start Date '
END +
CASE
	WHEN @Action = 'INS' THEN 'Inserted'
	WHEN @Action = 'UPD' THEN 'Updated'
	WHEN @Action = 'DEL' THEN 'Deleted'
END +
' ' + CONVERT(varchar, GETDATE(), 107)
--Set body font, insert KCIC logo/banner at the top of the email  
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_solid.PNG" alt="logo"/>'

----Create temp table to hold all new documents
--DECLARE @NewDocuments TABLE (docID nvarchar(200), firm nvarchar(100), firmURL nvarchar(200))
--INSERT INTO @NewDocuments
--SELECT spdocid, f.firm,
--      'https://admin.kcicllc.com/clients/marleywylain/cp/Pages/Documents.aspx' AS FirmURL
--FROM tblDocuments d
--INNER JOIN vtblPerson p ON p.Email = d.CreatedBy
--INNER JOIN vtblFirm f ON f.FirmID = p.firmid
--WHERE datecreated BETWEEN GETDATE()-1 AND GETDATE() 
--      --Do not include any documents uploaded by us.
--      AND RIGHT(CreatedBy, 11) != 'kcicllc.com'
--      AND detailDocumentTypeID NOT IN (40, 41)
--GROUP BY SPDocId, Firm


----Create cursor for each firm
--DECLARE @DocCount int, @firm nvarchar(200), @firmURL nvarchar(200)

--DECLARE Document_Curs CURSOR FOR

--SELECT COUNT(DISTINCT docid), firm, FirmURL
--FROM @newdocuments
--GROUP BY firm, firmurl
--ORDER BY firm
----Open
--OPEN Document_Curs
----Fetch
--FETCH NEXT FROM Document_Curs INTO @DocCount, @firm, @firmURL

--WHILE @@fetch_status = 0
--BEGIN
--      --List which firm, how many documents, and the firm url
--      SET @BodyMessage = @BodyMessage + convert(nvarchar(10), (Select @DocCount)) + ' New Document(s) for <A HREF =' 
--            + @firmURL+'>' + @firm + '</A><br><br>'   
--FETCH NEXT FROM Document_Curs INTO @DocCount, @firm, @firmURL
--      END
--CLOSE Document_Curs
--DEALLOCATE Document_Curs
SET @BodyMessage = @BodyMessage + '<br/><br/><a href="https://admin.kcicextranet.com/clients/msa/cp/pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, @ClaimantPersonalInfoID) + '&ClaimantTab=2">Click here to view employment and product exposure information.</a>'

SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

--Check for new documents
--IF(SELECT COUNT(DISTINCT docid)
--FROM @newdocuments) <> 0      
BEGIN
--Send email
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = '', --'msa@kcic.com; potterm@kcic.com',--'hubbardb@kcic.com', --
								@copy_recipients = '', --'selfL@kcic.com; shockleym@kcic.com; johnn@kcic.com; montgomeryi@kcic.com; dayc@kcic.com',
								@blind_copy_recipients = 'gpc@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_Format = 'html'
END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalDoFEAlert] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalDoFEAlert] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalDoFEAlert] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalDoFEAlert] TO [power_user]
GO
