SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByDiseaseByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Declare @MatterID int =null
--Declare @StartDate datetime = null
--Declare @EndDate datetime = null

     -- Insert statements for procedure here
    
--Declare @year int
--Set @Year=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--DECLARE @year int
--SET @year = @YearID

select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end as HighLevelDisease, 
		COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
--inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join (select * from tblClaimDisease where PrimaryDisease =1) cd ON c.ClaimID=cd.ClaimID
left join tblDisease d ON d.DiseaseID=cd.DiseaseID
left join tblHighLevelDisease hld ON hld.HighLevelDiseaseID=d.HighLevelDiseaseID
join (select distinct ClaimLawsuitID, MaterialID from tblAllegation where materialID not in (11,12))a on a.ClaimLawsuitID = cl.ClaimLawsuitID

where	coalesce(FileDate,'1/1/1900')between coalesce(@StartDate,'1/1/1900')
		and coalesce(@EndDate, '1/1/2100')
		and c.ClaimantPersonalInfoID <>13357 and a.MaterialID not  in (11,12)
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end
order by COUNT(distinct(cl.ClaimLawsuitID)) desc


END





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByDiseaseByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByDiseaseByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByDiseaseByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByDiseaseByYear] TO [power_user]
GO
