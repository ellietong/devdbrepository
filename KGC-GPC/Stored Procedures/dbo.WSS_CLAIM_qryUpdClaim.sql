SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryUpdClaim] 
	(	
		@LawsuitID bigint, 
		@PrimaryLawsuit nvarchar(250), 
		@IsMDL nvarchar(250),
		@ClaimLawsuitID bigint,
		@ClaimID bigint,
		@TenDefendants nvarchar(10),
		@NationalPlaintiffCounselAddressID int,
		@PrimaryLocalPlaintiffCounselAddressID int,
		@Username nvarchar(250)
	)

AS


	UPDATE tblLawsuit
	SET SPUsername = @Username,
	PlaintiffCounselAddressID = @PrimaryLocalPlaintiffCounselAddressID,
	NationalPlaintiffCounselAddressID = @NationalPlaintiffCounselAddressID
	WHERE LawsuitID = @LawsuitID
	
	UPDATE tblClaimLawsuit
	SET IsPrimaryForClaim = Case When @PrimaryLawsuit= 'YES' Then 1 else 0 END,
		IsMDL = Case When @IsMDL= 'YES' Then 1 else 0 END,
		SPUsername = @Username
	WHERE ClaimLawsuitID = @ClaimLawsuitID
	
	--DECLARE @CLAIMID INT = 2
	--DECLARE @LAWSUITID INT = 7696
	--DECLARE @TenDefendants nvarchar(10) = 'NO'
	DECLARE @UpdTenDefendants bit = CASE WHEN @TenDefendants LIKE '%YES%' THEN 1 ELSE 0 END
	
	UPDATE tblDocuments
	SET IsLessThanTenDefendantCompanies = @UpdTenDefendants
	FROM tblDocuments D
		INNER JOIN tblMultiClaimantDocument MCD ON MCD.DocumentID = D.DocumentID 
		INNER JOIN tblClaimantPersonalInfo CPI ON CPI.ClaimantPersonalInfoID = MCD.ClaimantPersonalInfoID 
		LEFT JOIN tblClaim C ON C.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID 
		LEFT JOIN tblClaimLawsuit cl on cl.claimid = c.ClaimID 
		LEFT Join tblLawsuit l on l.LawsuitID = cl.LawsuitID 
	WHERE c.ClaimID = @ClaimID AND l.LawsuitID = @LawsuitID 
	AND ClaimantDocumentTypeID = 8 
	



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GO
