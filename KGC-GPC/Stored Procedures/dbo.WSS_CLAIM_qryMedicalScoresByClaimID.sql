SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_CLAIM_qryMedicalScoresByClaimID] (@ClaimID		 bigint = NULL, 
												  @emailaddress nvarchar(250))
AS

SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimID int
--DECLARE @emailaddress nvarchar(250)
--SET @ClaimID = 14123
--SET @emailaddress = 'owenss@kcicllc.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE c.ClaimID = ' + CONVERT(nvarchar(20),@ClaimID)


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' LEFT JOIN  tblDefenseCounselAddress dca	ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID '
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT c.ClaimID, 
		CASE WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + '', '' + COALESCE(ilodoc.DoctorFirstName, '''') + COALESCE('' '' + ilodoc.DoctorMiddleName, '''') END AS ILODoctor,
		CASE WHEN PFTDoctorID IS NULL THEN NULL ELSE pftdoc.DoctorLastName + '', '' + COALESCE(pftdoc.DoctorFirstName, '''') + COALESCE('' '' + pftdoc.DoctorMiddleName, '''')END AS PFTDoctor, 
		ILODoctorID, 
		ILOProfusion, 
		ILODate, 
		PFTDoctorID,
		PFTDate, 
		PFTTypeID, 
		PFTFVC_Actual, 
		PFTFVC_Predicted, 
		CASE WHEN PFTFVC_Predicted <> 0 THEN (PFTFVC_Actual/PFTFVC_Predicted) ELSE 0 END AS PFTFVC_Pct,
		PFTFEV1_Actual, 
		PFTFEV1_Predicted, 
		CASE WHEN PFTFEV1_Predicted <> 0 THEN (PFTFEV1_Actual/PFTFEV1_Predicted) ELSE 0 END AS PFTFEV1_Pct,
		[PFTFEV1andFVC_Actual] AS PFTFEV1FVC_Actual, 
		[PFTFEV1andFVC_Predicted] AS PFTFEV1FVC_Predicted,
		CASE WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN ([PFTFEV1andFVC_Actual]/[PFTFEV1andFVC_Predicted]) ELSE 0 END AS PFTFEV1FVC_Pct,
		PFTTLC_Actual, 
		PFTTLC_Predicted, 
		CASE WHEN PFTTLC_Predicted <> 0 THEN (PFTTLC_Actual/PFTTLC_Predicted) ELSE 0 END AS PFTTLC_Pct, 
		PFTDLCO_Actual, 
		PFTDLCO_Predicted, 
		CASE WHEN PFTDLCO_Predicted <> 0 THEN (PFTDLCO_Actual/PFTDLCO_Predicted) ELSE 0 END AS PFTDLCO_Pct, 
		PFTTV
     FROM tblClaimantPersonalInfo cpi 
		LEFT JOIN tblClaim c							ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l							ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblState s							ON s.StateID = l.StateID
		INNER JOIN tblJurisdiction j					ON j.JurisdictionID = l.JurisdictionID
		' +
		@JoinString + ' 
        LEFT JOIN  tblDoctor ILODoc						ON ILODoctorID=ILODoc.DoctorID
        LEFT JOIN  tblDoctor PFTDoc						ON PFTDoctorID=PFTDoc.DoctorID
		' +
		@WhereString + '
	 OPTION (RECOMPILE)'

PRINT Substring(@QueryString, 1, 4000)
PRINT Substring(@QueryString, 4001, 4000)

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString



GO
