SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SMOKING_qryUpdClaimantSmokingHistory]
(@ClaimantSmokingHistoryID int, @SmokingStart date, @SmokingEnd date, @PacksPerDay float, @Notes nvarchar(max), @SPUsername nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblClaimantSmokingHistory
	SET SmokingStart = @SmokingStart,
		SmokingEnd = @SmokingEnd,
		PacksPerDay = @PacksPerDay,
		Notes = @Notes,
		SPUsername = @SPUsername
	WHERE ClaimantSmokingHistoryID = @ClaimantSmokingHistoryID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SMOKING_qryUpdClaimantSmokingHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SMOKING_qryUpdClaimantSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SMOKING_qryUpdClaimantSmokingHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SMOKING_qryUpdClaimantSmokingHistory] TO [power_user]
GO
