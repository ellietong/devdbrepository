SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmDisease]
as

Select DiseaseID, Disease, HighLevelDiseaseID
From tblDisease
Order By Disease

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDisease] TO [power_user]
GO
