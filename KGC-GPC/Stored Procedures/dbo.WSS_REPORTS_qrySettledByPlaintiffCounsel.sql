SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_REPORTS_qrySettledByPlaintiffCounsel] ( @SettlementDateStart  nvarchar(250),	@SettlementDateEND  nvarchar(250), @MatterID int) 

AS

Set NoCount On

------Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

 --DECLARE @SettlementDateStart datetime
 --DECLARE @SettlementDateEND datetime
 --DECLARE @matterid int
 --Set  @SettlementDateStart  = '1/1/1900'
 --Set @SettlementDateEND = '6/30/2010'
 --set @matterid = 1


DECLARE @SettlementSummary TABLE (ClaimNumber int, 
							
				SettlementAmount money,
				SettlementAverage money,
				
				Meso_Count int,
				Meso_SettlementAmount money,
				Meso_Avg money,
				LungCancer_Count int,
				LungCancer_SettlementAmount money,
				LungCancer_Avg money,
				OtherCancer_Count int,
				OtherCancer_SettlementAmount money,
				OtherCancer_Avg money,
				NonMalig_Count int,
				NonMalig_SettlementAmount money,
				NonMalig_Avg money,
				Other_Count int,
				Other_SettlementAmount money,

				NoDisease_Count int,
				NoDisease_SettlementAmount money,

				Null_Count int,
				Null_SettlementAmount money,
				PlaintiffCounsel nvarchar(200))


INSERT INTO @SettlementSummary 
SELECT count(c.ClaimID) AS ClaimantNumber,
		COALESCE(SUM(SettlementAmount),0) AS SettlementAmount,
	COALESCE(SUM(SettlementAmount),0)/count(c.ClaimID) AS SettlementAverage, 
	
	
	SUM(CASE WHEN HLD.HighLevelDiseASeID = 1 
			 THEN 1 
			 ELSE 0 
		 END) AS Meso_Count,
		 	
	SUM(CASE WHEN HLD.HighLevelDiseASeID = 1 
			 THEN SettlementAmount 
			 ELSE 0 
		 END) AS Meso_SettlementAmount,	
		 
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseASeID = 1 
					   THEN 1 
					   ELSE 0 
				  END) = 0 
		 THEN 0 
		 ELSE SUM(CASE WHEN HLD.HighLevelDiseASeID = (1) 
					   THEN SettlementAmount 
					   ELSE 0 
				  END)/ 
			SUM(CASE WHEN HLD.HighLevelDiseASeID = 1 
					 THEN 1 
					 ELSE 0 
				 END) 
	 END  AS Meso_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseASeID = 2 
			 THEN 1 
			 ELSE 0 
	END) AS LungCancer_Count,	
	
	SUM(CASE WHEN HLD.HighLevelDiseASeID = 2 
			 THEN SettlementAmount 
			 ELSE 0 
	    END) AS LungCancer_SettlementAmount,
	    	
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseASeID = (2) 
					   THEN 1 
					   ELSE 0 
				  END) = 0 
		 THEN 0 
		 ELSE SUM(CASE WHEN HLD.HighLevelDiseASeID = (2) 
					   THEN SettlementAmount 
					   ELSE 0 
				  END)/ 
			  SUM(CASE WHEN HLD.HighLevelDiseASeID = 2 
					   THEN 1 
					   ELSE 0 
			  END) 
    END  AS LungCancer_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseASeID = 3 
			 THEN 1 
			 ELSE 0 
	END) AS OtherCancer_Count,	
	
	SUM(CASE WHEN HLD.HighLevelDiseASeID = 3 
			 THEN SettlementAmount 
			 ELSE 0 
		END) AS OtherCancer_SettlementAmount,
			
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseASeID = 3 
					   THEN 1 
					   ELSE 0 
				  END) = 0 
		 THEN 0 
		 ELSE SUM(CASE WHEN HLD.HighLevelDiseASeID = 3 
					   THEN SettlementAmount 
					   ELSE 0 
				  END)/ 
			  SUM(CASE WHEN HLD.HighLevelDiseASeID = 3 
					   THEN 1 
					   ELSE 0 END) END  AS OtherCancer_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseASeID in (4,7) 
			 THEN 1 
			 ELSE 0 
	    END) AS NonMalig_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseASeID in (4,7) 
			 THEN SettlementAmount 
			 ELSE 0 
	    END) AS NonMalig_SettlementAmount,	
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseASeID in (4,7) 
					   THEN 1 
					   ELSE 0 
			      END) = 0 THEN 0 
		 ELSE SUM(CASE WHEN HLD.HighLevelDiseASeID in (4,7) 
					   THEN SettlementAmount 
					   ELSE 0 
				  END)/ 
			  SUM(CASE WHEN HLD.HighLevelDiseASeID in (4,7) 
					   THEN 1 
					   ELSE 0 
			      END) END  AS NonMalig_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseASeID IN (5, 6) 
			 THEN 1 
			 ELSE 0 
	    END) AS Other_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseASeID IN (5, 6) 
			 THEN SettlementAmount 
			 ELSE 0 
	    END) AS Other_SettlementAmount,
			
	0 AS NoDiseASe_Count,
	
	0 AS NoDiseASe_SettlementAmount,		
	--SUM(CASE WHEN HLD.HighLevelDiseASeID = 6 THEN 1 ELSE 0 END) AS NoDiseASe_Count,	
	--SUM(CASE WHEN HLD.HighLevelDiseASeID = 6 THEN SettlementAmount ELSE 0 END) AS NoDiseASe_SettlementAmount,
			
	SUM(CASE WHEN HLD.HighLevelDiseASeID IS NULL 
			 THEN 1 
			 ELSE 0 
	    END) AS Null_Count,				
	SUM(CASE WHEN HLD.HighLevelDiseASeID  IS NULL 
			 THEN SettlementAmount 
			 ELSE 0 
		END) AS Null_SettlementAmount,
		
	UPPER(PlaintiffCounsel) AS PlaintiffCounsel
	
FROM tblClaimantPersonalInfo i 
	LEFT JOIN tblClaim c ON i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN (SELECT CD.ClaimDiseASeID, CD.ClaimID, CD.DiseASeDiagnosisDate, CD.DiseASeID, CD.DoctorID, CD.PrimaryDiseASe FROM tblClaimDiseASe CD WHERE PrimaryDiseASe = 1) CD ON CD.ClaimID=C.ClaimID
	LEFT JOIN tblMatter M ON c.MatterID = m.MatterID
	LEFT JOIN tblClaimLawsuit l ON l.claimID = c.claimID
	LEFT JOIN tblLawsuit law ON l.lawsuitid = law.lawsuitid
	INNER JOIN tblCLaimantSettlement s ON s.claimlawsuitID = l.claimlawsuitID
	LEFT JOIN tblDiseASe D ON CD.DiseASeID = D.DiseASeID
	LEFT JOIN tblHighLevelDiseASe HLD ON HLD.HighLevelDiseASeID=D.HighLevelDiseASeID
	INNER JOIN tblClaimLawsuitStatusTracking cst ON cst.ClaimLawsuitID = l.ClaimLawsuitID
	LEFT JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = law.NationalPlaintiffCounselAddressID

WHERE IsPrimaryForClaim = 1 
	AND ProcessedDate >= COALESCE(@SettlementDateStart, '1/1/'+CONVERT(nvarchar(4), YEAR(GETDATE()))) 
	AND ProcessedDate <= COALESCE(@SettlementDateEND, GETDATE())
	AND C.MatterID >= COALESCE(@MatterID,0) 
	AND C.MatterID <= COALESCE(NULLIF(@MatterID,0),@maxMatterID) 
	AND ClaimLawsuitStatusID in (23,28,25)
	AND COALESCE(cst.IsPrimaryStatus,0) = 1

GROUP BY PlaintiffCounsel


SELECT 	SUM(COALESCE(ClaimNumber,0)) AS ClaimNumber, 
		--Coalesce(resolveddate, 0) AS resolvedDate,
		SUM(COALESCE(SettlementAmount,0)) AS SettlementAmount,
		SUM(COALESCE(SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Claimnumber, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Claimnumber, 0)) END AS SettlementAverage,
		--COALESCE(SettlementAverage,0) AS SettlementAverage,
		SUM(COALESCE(Meso_Count,0)) AS Meso_Count,
		SUM(COALESCE(Meso_SettlementAmount,0)) AS Meso_SettlementAmount,
		--COALESCE(Meso_Avg,0) AS Meso_Avg,
		SUM(COALESCE(Meso_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Meso_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Meso_Count, 0)) END AS Meso_Avg,
		SUM(COALESCE(LungCancer_Count,0)) AS LungCancer_Count,
		SUM(COALESCE(LungCancer_SettlementAmount,0)) AS LungCancer_SettlementAmount,
		--COALESCE(LungCancer_Avg,0) AS LungCancer_Avg,
		SUM(COALESCE(LungCancer_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(LungCancer_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(LungCancer_Count, 0)) END AS LungCancer_Avg,
		SUM(COALESCE(OtherCancer_Count,0)) AS OtherCancer_Count,
		SUM(COALESCE(OtherCancer_SettlementAmount,0)) AS OtherCancer_SettlementAmount,
		--COALESCE(OtherCancer_Avg,0) AS OtherCancer_Avg,
		SUM(COALESCE(OtherCancer_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(OtherCancer_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(OtherCancer_Count, 0)) END AS Othercancer_Avg,
		SUM(COALESCE(NonMalig_Count,0))AS NonMalig_Count,
		SUM(COALESCE(NonMalig_SettlementAmount,0))AS NonMalig_SettlementAmount,
		--COALESCE(NonMalig_Avg,0) AS NonMalig_Avg,
		SUM(COALESCE(LungCancer_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(LungCancer_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(LungCancer_Count, 0)) END AS NonMalig_Avg,
		SUM(COALESCE(Other_Count,0)+ COALESCE(Null_Count,0)) AS Unknown_Count,
		SUM(COALESCE(Other_SettlementAmount,0)+COALESCE(Null_SettlementAmount,0)) AS Unknown_SettlementAmount,
 	
 	PlaintiffCounsel--,
 	--resolveddate AS MoYearSettled
FROM @SettlementSUMmary 
GROUP BY PlaintiffCounsel

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettledByPlaintiffCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettledByPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettledByPlaintiffCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettledByPlaintiffCounsel] TO [power_user]
GO
