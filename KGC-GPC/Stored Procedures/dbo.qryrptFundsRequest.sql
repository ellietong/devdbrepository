SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC  [dbo].[qryrptFundsRequest] 

As

SET NOCOUNT ON


SELECT Firm, InvoiceTotal
FROM tblDefenseInvoice DI
Inner Join tblDefenseInvoiceFirm DIF on DI.FirmID=DIF.FirmID

Where StatusID in (1,2) and DatePayable <= 
(SELECT CONVERT(datetime, CONVERT(varchar, GETDATE(), 101), 101) + 30 AS OneMonthOut)
AND FundsRequestDate is Null

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequest] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequest] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequest] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequest] TO [power_user]
GO
