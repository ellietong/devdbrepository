SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_REPORTS_CLAIM_qryAllegationTypeList]
AS
SET NOCOUNT ON

SELECT AllegationTypeID, 
	AllegationType 
FROM tblAllegationType
UNION
SELECT NULL, ''
ORDER BY AllegationType
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_CLAIM_qryAllegationTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_CLAIM_qryAllegationTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_CLAIM_qryAllegationTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_CLAIM_qryAllegationTypeList] TO [power_user]
GO
