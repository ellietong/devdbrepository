SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_FIRM_qryGetFirmAddressByEmail] (@emailaddress nvarchar(1000))
AS

DECLARE @firmID int
SET @firmID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)
BEGIN
	
DECLARE @emaildomain nvarchar(1000)
	
SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress), LEN(@emailaddress)-CHARINDEX('@', @emailaddress)+1)
	
IF @emaildomain = '@kcicllc.com' OR @emailDomain='@kcic.com' OR @firmID IN (539)
	
BEGIN
	
	
SELECT DISTINCT DefenseCounselID as FirmID, DefenseCounsel as Firm, DefenseCounselAddressID AS FirmAddressID
	
	
FROM tblDefenseCounselAddress
	
	
ORDER BY DefenseCounsel
	
END
	
ELSE
	
BEGIN
	
	
SELECT DISTINCT FirmID, DefenseCounsel as Firm, DefenseCounselAddressID AS FirmAddressID FROM vtblPerson 
	
	
INNER JOIN tblDefenseCounselAddress ON vtblPerson.FirmAddressID = tblDefenseCounselAddress.DefenseCounselAddressID
	
	
WHERE @emailaddress = vtblPerson.Email
	
	
ORDER BY DefenseCounsel
	
END
END

--IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
--BEGIN
--	SELECT DISTINCT DefenseCounselID as FirmID, DefenseCounsel as Firm 
--	FROM tblDefenseCounsel 
--	WHERE DefenseCounselID NOT IN (134, 135, 136,268)
--	ORDER BY DefenseCounsel
--END
--ELSE
--BEGIN
--	SELECT DISTINCT FirmID, DefenseCounsel as Firm 
--	FROM vtblPerson p
--		INNER JOIN tblDefenseCounsel dc ON p.FirmID = dc.DefenseCounselID
--	WHERE @emailaddress = Email
--	ORDER BY DefenseCounsel
--END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail] TO [power_user]
GO
