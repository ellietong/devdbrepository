SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryInsertSavedParameter_Claim]
(@Type nvarchar(max), @Value nvarchar(MAX), @SavedSearchID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	IF(@Type = 'DParam')
	BEGIN
		INSERT INTO tblSavedDisease 
		SELECT Convert(int, @Value), @SavedSearchID
	
	END
	ELSE IF (@Type = 'JParam')
	BEGIN
		INSERT INTO tblSavedJurisdiction 
		SELECT Convert(int, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'AParam')
	BEGIN
		INSERT INTO tblSavedAllegation 
		SELECT Convert(int, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'StParam')
	BEGIN
		INSERT INTO tblSavedState
		SELECT Convert(int, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'PCParam')
	BEGIN
		INSERT INTO tblSavedPlaintiffCOunsel 
		SELECT Convert(bigint, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'LSParam')
	BEGIN
		INSERT INTO tblSavedLawsuitStatus 
		SELECT Convert(int, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'ATDRParam')
	BEGIN
		INSERT INTO tblSavedActiveTrialDateRange 
		SELECT @Value, @SavedSearchID
	END
	ELSE IF (@Type = 'MDLParam')
	BEGIN
		INSERT INTO tblSavedIsMDL 
		SELECT Convert(bit, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'PDRParam')
	BEGIN
		INSERT INTO tblSavedProcessedDateRange 
		SELECT @Value, @SavedSearchID
	END
	ELSE IF (@Type = 'SDRParam')
	BEGIN
		INSERT INTO tblSavedStatusDateRange 
		SELECT @Value, @SavedSearchID
	END	
	ELSE IF (@Type = 'MatParam')
	BEGIN
		INSERT INTO tblSavedMaterial
		SELECT @Value, @SavedSearchID
	END				
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryInsertSavedParameter_Claim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryInsertSavedParameter_Claim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryInsertSavedParameter_Claim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryInsertSavedParameter_Claim] TO [power_user]
GO
