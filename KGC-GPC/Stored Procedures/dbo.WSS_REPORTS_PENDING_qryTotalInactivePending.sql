SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qryTotalInactivePending]
	-- Add the parameters for the stored procedure here
(@MatterID int,@EndDate datetime=null)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
   DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter WITH (READCOMMITTED)) 
select COUNT(cl.ClaimLawsuitID) AS [Count]
from tblClaim c WITH (READCOMMITTED)
inner join tblClaimLawsuit cl WITH (READCOMMITTED) ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l WITH (READCOMMITTED) ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst WITH (READCOMMITTED) ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblPlaintiffCounselAddress pca WITH (READCOMMITTED) ON pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
join (select distinct ClaimLawsuitID, MaterialID from tblAllegation where materialID not in (11,12))a on a.ClaimLawsuitID = cl.ClaimLawsuitID
--inner join tblPlaintiffCounsel_ALT pc WITH (READCOMMITTED) ON pc.PlaintiffCounselID=pca.PlaintiffCounselID
where ClaimLawsuitStatusID in (29)  and c.ClaimantPersonalInfoID <>13357
and  IsPrimaryStatus=1 
and a.MaterialID not  in (11,12)
--and ProcessedDate <= COALESCE(@EndDate,'12/31/2100')
order by COUNT(cl.ClaimLawsuitID) desc


END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTotalInactivePending] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTotalInactivePending] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTotalInactivePending] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTotalInactivePending] TO [power_user]
GO
