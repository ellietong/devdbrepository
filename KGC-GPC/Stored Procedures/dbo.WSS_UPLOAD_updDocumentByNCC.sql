SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_updDocumentByNCC]
(
	@ClaimantDocumentTypeID int,					--aka ClaimantDocumentTypeID
	--@ClaimantPersonalInfoID bigint = null,	
	@Comments nvarchar(4000) = null,			--comments are optional		
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@MatterID int=null,					--this is the guid assigned by SharePoint
	@FirmID int = null,
	
	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,
    @Adjustment money = null,
    @AdjustmentNotes nvarchar(4000) = null,
    @StatusID int = null,

	@ModifiedBy nvarchar(250),
	@DateModified datetime,
	@ApprovedBy nvarchar(50) = null,
	@DateApproved	datetime = null,
	@DocID int=null,
	@DocumentSourceID int,
	@PrivateComments nvarchar(4000)=null,
	@VendorFirmID int=null
)

AS
BEGIN
Update tblDocuments 
SET
	ClaimantDocumentTypeID	= @ClaimantDocumentTypeID,	
	
	Comments				= @Comments,	
	DocumentURL				= @DocumentURL,	
	DocumentTitle			= @DocumentTitle,   
	ModifiedBy				= @ModifiedBy,
	DateModified			= @DateModified,
	DocumentSourceID		= @DocumentSourceID,
	MatterID				= @MatterID
	
WHERE DocumentID = @DocID

Select @DocID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [power_user]
GO
