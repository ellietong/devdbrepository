SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[qry_2012Complaints] as

Select d.DocumentURL, d.DocumentTitle, d.DocumentID, cdt.ClaimantDocumentType, '' AS DefenseCounselID, '' AS DefenseCounsel, d.ClaimantDocumentTypeID
from tblDocuments d 
join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
where HistoricalDocumentID is not null
GO
GRANT VIEW DEFINITION ON  [dbo].[qry_2012Complaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[qry_2012Complaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qry_2012Complaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[qry_2012Complaints] TO [power_user]
GO
