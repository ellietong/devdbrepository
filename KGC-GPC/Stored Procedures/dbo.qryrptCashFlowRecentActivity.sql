SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCashFlowRecentActivity] (@MatterID int, 
@BalanceStartDate datetime, 
@BalanceEndDate datetime)

As
Set NoCount On

--Declare @MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime
--Set @MatterID = 1
--Set @BalanceStartDate = '7/27/2013'
--Set @BalanceEndDate = '8/2/2013'

--Declare @StartingBalance money
--Set @StartingBalance = '0.00'  + (
----Coalesce((Select SUM(CashActivityAmount) From tblMiscCashActivity mca 
----Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID 
----where CashActivityDate >= '4/28/2013' and CashActivityDate <= @BalanceEndDate and MatterID = @MatterID),0)
----+
--Coalesce((Select SUM(SourceAmount)*-1 From tblCheck c
--Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
--Where SourceDate >= '4/28/2013' and SourceDate <= @BalanceEndDate and MatterID = @MatterID),0)
--)



DECLARE @List TABLE (CheckID int, CheckInfo nvarchar(max))
DECLARE @LastCheckID int
DECLARE @CheckID int
DECLARE @CheckInfo nvarchar(max)
Declare @CheckInfoConCat nvarchar(max)
Declare @PlaintiffCounsel nvarchar(max)
declare @LastPlaintiffCounsel nvarchar(max)


DECLARE Check_curs CURSOR FOR

Select c.CheckID, PlaintiffCounsel + ' - (FOR ' , FirstName + ' ' + LastName + ' (ID: ' +convert(nvarchar(50),cpi.ClaimantPersonalInfoID)   +')' + ', ' +  Disease + ', $' + convert(nvarchar(50),convert(money,ps.PaymentAmount),1)
From tblCheck c
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
Inner Join tblMatter m on c.MatterID = m.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
inner join tblClaim cla on cla.ClaimID = cl.ClaimID
inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = cla.ClaimantPersonalInfoID
inner join tblClaimDisease cd on cd.ClaimID = cla.ClaimID
inner join tblDisease d on d.DiseaseID = cd.DiseaseID
Where PrimaryDisease = 1  and IsPrimaryForClaim = 1 and IsCurrentClaim = 1
and (SourceDate >= @BalanceStartDate and 
(SourceDate <= @BalanceEndDate) --AND c.MatterID = @MatterID
 AND c.FundingTypeID = 2)
group by c.CheckID, PlaintiffCounsel + ' - (FOR ' , FirstName + ' ' + LastName + ' (ID: ' +convert(nvarchar(50),cpi.ClaimantPersonalInfoID)   +')' + ', ' +  Disease + ', $' + convert(nvarchar(50),convert(money,ps.PaymentAmount),1)
order by c.CheckID, PlaintiffCounsel + ' - (FOR ' , FirstName + ' ' + LastName + ' (ID: ' +convert(nvarchar(50),cpi.ClaimantPersonalInfoID)   +')' + ', ' +  Disease + ', $' + convert(nvarchar(50),convert(money,ps.PaymentAmount),1)

OPEN Check_curs
FETCH NEXT FROM Check_curs INTO @CheckID, @PlaintiffCounsel, @CheckInfo
WHILE @@FETCH_STATUS=0 
BEGIN

		IF @CheckID = @LastCheckID
		BEGIN

		Set @CheckInfoConCat = @CheckInfoConCat + ' AND '  + @CheckInfo

		END
		ELSE
		BEGIN
		
		IF @LastCheckID is not null
		BEGIN
		Insert into @List
		VALUES (@LastCheckID,@LastPlaintiffCounsel + ' ' + @CheckInfoConCat+ ')')
		END

		

		Set @CheckInfoConCat = @Checkinfo
		SET @LastCheckID = @CheckID
		set @LastPlaintiffCounsel = @PlaintiffCounsel

		END

FETCH NEXT FROM Check_curs INTO @CheckID,@PlaintiffCounsel , @CheckInfo


END
CLOSE Check_curs
DEALLOCATE Check_curs


Insert into @List
		VALUES (@LastCheckID,@LastPlaintiffCounsel + ' ' + @CheckInfoConCat+ ')')

--SELECT * FROM @List
			

Declare @Indemnity money
set @Indemnity = '0.00' +
(Coalesce((Select SUM(sourceAmount)*-1 From tblCheck c
Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
Where SourceDate >= '4/28/2013' and SourceDate < @BalanceStartDate and MatterID = @MatterID),0))

Declare @miscfees money
set @miscfees = '0.00' +
(Coalesce((Select SUM(cashactivityamount) From tblMiscCashActivity mca
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID
Where cashactivitytypeid = 4 and cashactivitydate >= '4/28/2013' and cashactivitydate < @BalanceStartDate and MatterID = @MatterID),0))


Declare @cashreceipts money
set @cashreceipts = '0.00' +
(Coalesce((Select SUM(cashactivityamount) From tblMiscCashActivity mca
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID
Where cashactivitytypeid = 1 and cashactivitydate >= '4/28/2013' and cashactivitydate < @BalanceStartDate and MatterID = @MatterID),0))


--Declare @Matter nvarchar(250)
--Set @Matter = (Select Matter from tblMatter where MatterID = @MatterID)

Declare @Detail table --(Matter nvarchar(250), 
(FundingType nvarchar(250), FundingSource nvarchar(250), SourceDate datetime, SourceNumber nvarchar(250), Indemnity money, Defense money, MiscFees money, CashReceipts money, Comment nvarchar(Max), CheckStatus bit)
Insert Into @Detail

--Defense
Select --Matter, 
FundingType, FundingSource, SourceDate, SourceNumber, 0, SourceAmount*-1 as SourceAmount, 0, 0, coalesce(defensecounsel, vendorfirm), IsCheckCleared
From tblCheck c
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Inner Join tblPaymentDefense pd on pd.CheckID = c.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
--Inner Join tblDefenseInvoiceFirm dif on dif.FirmID = di.FirmID
Left join tblDefenseCounselAddress da on da.DefenseCounselAddressID=di.DefenseCounselAddressID
left join m_tblVendorFirm v on v.VendorFirmID=di.VendorFirmID
Inner Join tblMatter m on c.MatterID = m.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @BalanceStartDate and 
(SourceDate <= @BalanceEndDate) AND --c.MatterID = @MatterID AND 
c.FundingTypeID = 1)
Group by --Matter, 
FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, DefenseCounsel, vendorfirm, IsCheckCleared

Union ALL

--Settlement
Select-- Matter, 
FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1 as SourceAmount, 0, 0, 0, CheckInfo, IsCheckCleared
From tblCheck c
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
Inner Join tblMatter m on c.MatterID = m.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
inner join tblClaim cla on cla.ClaimID = cl.ClaimID
inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = cla.ClaimantPersonalInfoID
inner join tblClaimDisease cd on cd.ClaimID = cla.ClaimID
inner join tblDisease d on d.DiseaseID = cd.DiseaseID
left join @List list on list.CheckID = c.CheckID
Where  (SourceDate >= @BalanceStartDate and 
(SourceDate <= @BalanceEndDate) --AND c.MatterID = @MatterID
 AND c.FundingTypeID = 2)
Group by --Matter, 
FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, cft.FundingTypeID, list.CheckInfo, IsCheckCleared

Union ALL

Select --Matter, 
CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, CashActivityAmount, 0, CashActivityNotes, Null
From tblMiscCashActivity mca
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @BalanceStartDate and 
(CashActivityDate <= @BalanceEndDate) AND mca.MatterID = @MatterID 
AND CashActivityAmount < 0)

Union ALL

Select --Matter, 
CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, 0, CashActivityAmount, CashActivityNotes, Null
From tblMiscCashActivity mca
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @BalanceStartDate and 
(CashActivityDate <= @BalanceEndDate) AND mca.MatterID = @MatterID 
AND CashActivityAmount > 0)

Union ALL

--Indemnity money, Defense money, MiscFees money, CashReceipts money,

Select --@Matter, 


'Starting Balance', 'N/A', @BalanceStartDate , 'N/A', coalesce(@Indemnity,0), 0, Coalesce(@miscfees,0), Coalesce(@cashreceipts,0), 'Starting Balance', Null

Select *, @BalanceStartDate as BalanceStartDate, 
@BalanceEndDate as BalanceEndDate from @Detail Order By SourceDate, FundingType desc

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity] TO [power_user]
GO
