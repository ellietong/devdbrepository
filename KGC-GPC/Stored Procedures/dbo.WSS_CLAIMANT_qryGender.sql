SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_CLAIMANT_qryGender]
AS

SELECT GenderID, 
	Gender
FROM tblGender
ORDER BY Gender

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryGender] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryGender] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryGender] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryGender] TO [power_user]
GO
