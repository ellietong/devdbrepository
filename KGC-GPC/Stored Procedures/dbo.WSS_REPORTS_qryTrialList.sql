SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_REPORTS_qryTrialList]
(@TrialDateStart nvarchar (50), @TrialDateEnd nvarchar (50), @ClaimLawsuitStatusID int = null, @emailaddress nvarchar (250))
AS
--declare @TrialDateStart nvarchar (50)
--declare @TrialDateEnd nvarchar (50)
--declare @ClaimLawsuitStatusID int = null
--declare @emailaddress nvarchar (250)



DECLARE @StatusIDs Table (Claimlawsuitstatusid int)
IF @ClaimLawsuitStatusID IS NULL
BEGIN
INSERT INTO @StatusIDs
	SELECT
		ClaimLawsuitStatusID
	FROM tblClaimLawsuitStatus
END
IF @ClaimLawsuitStatusID IS NOT NULL
BEGIN
INSERT INTO @StatusIDs
	SELECT
		ClaimLawsuitStatusID
	FROM tblClaimLawsuitStatus
	WHERE tblClaimLawsuitStatus.ClaimLawsuitStatusID = @ClaimLawsuitStatusID
END

SELECT DISTINCT
	cpi.ClaimantPersonalInfoID,
	LastName + ', ' + FirstName AS ClaimantName,
	et.EventDate AS TrialDate,
	Jurisdiction,
	S.State,
	Disease,
	PlaintiffCounsel,
	et.EventDescription as TrialNotes,
	CASE
		WHEN Ten > 0 THEN 'YES' ELSE 'NO'
	END AS LessThanTenDefendantCompanies,
	cls.ClaimLawsuitStatus
FROM tblClaimantPersonalInfo CPI
JOIN tblClaim C ON C.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
JOIN tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID
JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
JOIN tblLawsuit L ON L.LawsuitID = CL.LawsuitID
JOIN tblState S ON S.StateID = L.StateID
LEFT JOIN tblClaimDisease cd ON cd.ClaimID = c.ClaimID
LEFT JOIN TblDisease d ON d.DiseaseID = cd.DiseaseID
LEFT JOIN tblJurisdiction j ON j.JurisdictionID = l.JurisdictionID
LEFT JOIN TblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.NationalPlaintiffCounselAddressID
LEFT JOIN tblMultiClaimantDocument mcd ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuitStatus cls ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
INNER JOIN tblEventTracking et ON cl.ClaimLawsuitID = et.ClaimLawsuitID
LEFT JOIN (SELECT MAX(IsLessThanTenDefendantCompanies + 0) AS Ten,
				  DocumentID
			FROM tblDocuments
			WHERE ClaimantDocumentTypeID = 8
			GROUP BY DocumentID)
			doc ON doc.DocumentID = mcd.DocumentID
WHERE   et.EventTypeID = 11 and cpi.claimantpersonalinfoID <> 13357
		AND EventDate BETWEEN COALESCE(CONVERT(date, @TrialDateStart), CONVERT(date, '1/1/1900')) AND COALESCE(CONVERT(date, @TrialDateEnd), CONVERT(date, '12/31/2025'))
		AND clst.ClaimLawsuitStatusID IN (SELECT [@StatusIDs].Claimlawsuitstatusid
										  FROM @StatusIDs)
		AND CL.IsPrimaryForClaim = 1 
		And clst.IsPrimaryStatus=1 and cd.PrimaryDisease = 1
GROUP BY	cpi.ClaimantPersonalInfoID,
			LastName + ', ' + FirstName,
			et.EventDate,
			Jurisdiction,
			S.State,
			Disease,
			PlaintiffCounsel,
			et.EventDescription,
			Ten,
			cls.ClaimLawsuitStatus
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryTrialList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryTrialList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryTrialList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryTrialList] TO [power_user]
GO
