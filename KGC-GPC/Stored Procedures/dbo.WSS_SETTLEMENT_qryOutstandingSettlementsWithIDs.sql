SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs]  (@emailaddress nvarchar(250), @SettlementGroupID int = NULL)

As

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 4
--DECLARE @EmailAddress nvarchar(250) = 'randy@gorijulianlaw.com'
--DECLARE @SettlementGroupID int = NULL

DECLARE @MaxSettlementGroupID int = (SELECT MAX(SettlementGroupID) FROM tblSettlementGroup)

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE IsPrimaryStatus = 1 
						AND ClaimLawsuitStatusID = 39'

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END


DECLARE @QueryString NVARCHAR(MAX) = ''
SET @QueryString = '
SELECT distinct cpi.ClaimantPersonalInfoID AS ClaimantID, 
				c.ClaimID,
				cpi.FirstName, 
				cpi.LastName, 
				cpi.ClaimantPersonalInfoID, 
				cpi.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + 
					CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' END AS ClaimantName,
				CONVERT(date,cpi.BirthDate) as Birthdate, 
				CONVERT(date, cpi.DeceasedDate) as DeceasedDate,
				cs.SettlementDate, 
				cs.SettlementAmount, 
				SettlementGroup,
				cs.SettlementGroupID,
				cl.ClaimLawsuitID,
				COALESCE(dbo.fnMissingClaimantInformation(cl.ClaimLawsuitID), ''All required claimant information submitted.'') AS MissingClaimantInfo,
				COALESCE(dbo.fnMissingDocumentation(cl.ClaimLawsuitID), ''All required documents submitted.'') AS MissingDocumentation
FROM tblClaim c
INNER JOIN tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID = l.LawsuitID
LEFT JOIN tblClaimantSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID = clst.ClaimLawsuitID
LEFT JOIN tblSettlementGroup sg on cs.SettlementGroupID = sg.SettlementGroupID'
+ @JoinString +
@WhereString +
' ORDER BY ClaimantName 
  OPTION (RECOMPILE)'

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [power_user]
GO
