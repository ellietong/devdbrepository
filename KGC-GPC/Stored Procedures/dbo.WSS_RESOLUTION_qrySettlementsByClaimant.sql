SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RESOLUTION_qrySettlementsByClaimant] (@ClaimantPersonalInfoID bigint = NULL)
AS

--DECLARE @ClaimantpersonalinfoID int
--SET @ClaimantpersonalinfoID = 68670

SELECT C.ClaimID, 
	hddis.HighLevelDisease AS Disease, 
	S.State, 
	J.Jurisdiction, 
	cs.ClaimSequence, 
	CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
	CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, 
	CONVERT(date, CPI.BirthDate) AS BirthDate, 
	CPI.DeceasedDate, 
	CASE WHEN Month(BirthDate) < Month(DeceasedDate)													THEN DateDiff(yy, BirthDate, DeceasedDate) 
		 WHEN Month(BirthDate) > Month(DeceasedDate)													THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 
		 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) > Day(DeceasedDate)  THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 
																										ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
	cls.ClaimLawsuitStatus, 
	CONVERT(date, L.ServiceDate) AS ServiceDate, 
	CONVERT(date, L.FileDate) AS FileDate, 
	L.DocketNumber, 
	L.CaseCaption, 
	L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,
	pca.PlaintiffCounsel, 
	DefenseCounsel AS PrimaryDefenseCounsel,
	CPI.ClaimantPersonalInfoID,
	ReleaseSignedDate,
	rt.ReleaseType, 
	CLst.StatusNotes,
	SettlementGroupID,
	ExecutedReleaseDate as ReleaseReceivedDate,
	SettlementDueDate,
	PaidDate,
	CheckNumber,
	'' AS Matter,
	SettlementAmount,
	ClaimDiseaseID,
	CASE WHEN IsMDL = 0 THEN 'No' WHEN IsMDL = 1 THEN 'Yes' ELSE 'Unknown' END AS IsMDL,
	CASE WHEN IsThirdPartyComplaint = 0 THEN 'No' WHEN IsThirdPartyComplaint = 1 THEN 'Yes' ELSE 'Unknown' END AS IsThirdPartyComplaint,
	ClaimantSettlementID
FROM tblClaimantPersonalInfo CPI 
	INNER JOIN tblClaim C								ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID 
	--INNER JOIN tblMatter m								ON m.MatterID = c.MatterID 
	Inner Join tblClaimLawsuit CL						ON C.ClaimID = CL.ClaimID 
	INNER JOIN tblLawsuit L								ON CL.LawsuitID = L.LawsuitID 
	INNER JOIN tblState S								ON S.StateID = L.StateID 
	INNER JOIN tblJurisdiction J						ON J.JurisdictionID = L.JurisdictionID 
	INNER JOIN tblClaimSequence cs						ON cs.ClaimSequenceID = C.ClaimSequenceID 
	LEFT  JOIN tbldefensecounseladdress dca				ON dca.defensecounseladdressid = l.DefenseCounseladdressid
	LEFT  JOIN (SELECT * 
				FROM tblClaimDisease 
				WHERE PrimaryDisease = 1) CD			ON CD.ClaimID = c.ClaimID
	LEFT  JOIN tblDisease ddis							ON ddis.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblHighLevelDisease hddis				ON hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID 
	LEFT  JOIN (SELECT * 
				FROM dbo.tblClaimLawsuitStatusTracking 
				WHERE IsPrimaryStatus = 1) clst			ON clst.ClaimLawsuitID = cl.ClaimLawsuitID 
	LEFT  JOIN tblClaimLawsuitStatus cls				ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID 
	LEFT  JOIN tblClaimantSettlement ClaimSett			ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID 
	LEFT  JOIN tblSettlePaymentDocumentation spd		ON spd.ClaimLawsuitID = cl.ClaimLawsuitID 
	LEFT  JOIN tblReleaseType rt						ON rt.ReleaseTypeID = spd.ReleaseTypeID
	LEFT  JOIN tblplaintiffcounseladdress pca			ON pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
WHERE CPI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND cl.IsPrimaryForClaim = 1
	AND SettlementDate IS NOT NULL 
	AND SettlementAmount IS NOT NULL
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qrySettlementsByClaimant] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qrySettlementsByClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qrySettlementsByClaimant] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qrySettlementsByClaimant] TO [power_user]
GO
