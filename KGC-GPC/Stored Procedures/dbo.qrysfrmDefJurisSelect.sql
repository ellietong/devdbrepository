SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[qrysfrmDefJurisSelect](@DefenseJurisdictionID int)
As
Set NoCount On

SELECT DefenseCounselAddressID, dj.JurisdictionID, dj.CourtTypeID, dj.StateID
from tblDefenseJurisdiction dj
WHERE dj.DefenseJurisdictionID = @DefenseJurisdictionID 
-- Case When Defaults to Huddlestone for West Virginia
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDefJurisSelect] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmDefJurisSelect] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDefJurisSelect] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmDefJurisSelect] TO [power_user]
GO
