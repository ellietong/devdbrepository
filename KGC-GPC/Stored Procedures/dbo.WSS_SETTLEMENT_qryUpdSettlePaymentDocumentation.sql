SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] 
(
@SetPymtID int, 
@SettlePaymentComments nvarchar(max), 
@ReleaseTypeID int,
@ExecutedReleaseDate date, 
@MedicareReported bit ,
@MedicareFullyDocumented bit
)

AS

--DECLARE @SetPymtID int
--SET @SetPymtID = 1

UPDATE tblSettlePaymentDocumentation
	SET SettlePaymentComments = @SettlePaymentComments, 
		ReleaseTypeID = CASE WHEN @ReleaseTypeID = 0 THEN NULL ELSE @ReleaseTypeID END, 
		ExecutedReleaseDate = NULLIF(@ExecutedReleaseDate,''),
		MedicareReported = @MedicareReported,
		MedicareFullyDocumented = @MedicareFullyDocumented
	WHERE SetPymtID = @SetPymtID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GO
