SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryALLCreateColumns]
(@insuranceProgramID int, @allocationID int)
AS
SET NOCOUNT ON

DECLARE @tblTemp table (AllocationID int, InsuranceProgramID int, ColumnStartDate datetime, ColumnEndDate datetime, ColumnDays int)

-- Insert into the temporary table the allocation ID, insurance program ID, all distinct combinations of policy start and end dates for the ColumnStartDate, the next distinct policy start or end date 
-- (using fnNextRecVal, which finds the subsequent distinct policy start or end date) as the ColumnEndDate
INSERT INTO @tblTemp (AllocationID, InsuranceProgramID, ColumnStartDate, ColumnEndDate)
SELECT AllocationID, InsuranceProgramID, PStartDate, dbo.fnNextRecVal(PStartDate, @insuranceProgramID, @allocationID)
FROM ztblPolicyAllocation
WHERE InsuranceProgramID = @insuranceProgramID AND AllocationID = @allocationID
GROUP BY AllocationID, InsuranceProgramID, PStartDate, dbo.fnNextRecVal(PStartDate, @insuranceProgramID, @allocationID)

UNION

SELECT AllocationID, InsuranceProgramID, PEndDate, dbo.fnNextRecVal(PEndDate, @insuranceProgramID, @allocationID)
FROM ztblPolicyAllocation
WHERE InsuranceProgramID = @insuranceProgramID AND AllocationID = @allocationID
GROUP BY AllocationID, InsuranceProgramID, PEndDate, 	dbo.fnNextRecVal(PEndDate, @insuranceProgramID, @allocationID)

-- Update the temporary table with ColumnDays, which is the ColumnEndDate - the ColumnStartDate
UPDATE @tblTemp
SET ColumnDays = CONVERT(int, ColumnEndDate - ColumnStartDate)
-- Insert all rows but the last row (typically), where the column end date is null (and is not a valid column), into ztblColumn

INSERT INTO tblAllocationResultsColumn (AllocationID, InsuranceProgramID, ColumnStartDate, ColumnEndDate, ColumnDays)
SELECT AllocationID, InsuranceProgramID, ColumnStartDate, ColumnEndDate, ColumnDays
FROM @tblTemp 
WHERE ColumnEndDate IS NOT NULL

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLCreateColumns] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLCreateColumns] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLCreateColumns] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLCreateColumns] TO [power_user]
GO
