SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qryExternalEmailNewDocuments]
AS
BEGIN

/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/


--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

Declare @Today4PM datetime
Declare @Yesterday4PM datetime
SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')
SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')



SET @Subject =
'GPC Claims: New Documents Uploaded ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewDocumentsTable TABLE (DocID int, docTitle nvarchar (500), FirmAddressID int, lName nvarchar (50),
fName nvarchar (50), DocType nvarchar (50), ClaimantPersonalInfoID bigint)
INSERT INTO @NewDocumentsTable
	SELECT
		d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/gpc/cp/Pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		l.DefenseCOunselAddressID,
		cpi.LastName,
		cpi.FirstName,
		ClaimantDocumentType,
		cpi.claimantpersonalinfoid
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantDocumentType ct ON ct.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
	LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
	WHERE datecreated BETWEEN @Yesterday4PM AND @Today4PM
	AND d.ClaimantDocumentTypeID IN (7, 11, 23, 25, 6, 28)
	AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	AND COALESCE(Accepted, 0) <> 1
	--AND FirmID IS NOT NULL
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	GROUP BY	d.DocumentID,
				d.DocumentTitle,
				DefenseCOunselAddressID,
				LastName,
				FirstName,
				ClaimantDocumentType,
				cpi.claimantpersonalinfoid



DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar (max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar (max), @ClaimantID bigint, @ClaimantList nvarchar (max)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for SELECT
	DocID,
	COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
	claimantpersonalInfoID
FROM @NewDocumentsTable
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
WHILE @@FETCH_STATUS = 0
BEGIN

IF (LEN (@ClaimantList) = 0 OR @ClaimantList IS NULL)
BEGIN

SET @ClaimantList = '<ul style="margin:0; padding:0;"> <a href="https://www.kcicextranet.com/clients/gpc/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + @Claimant + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
END
ELSE
BEGIN

SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul style="margin:0; padding:0;">  <a href="https://www.kcicextranet.com/clients/gpc/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + COALESCE(@Claimant, '') + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
PRINT @@FETCH_STATUS
IF @DocID < > @LastDocID OR @@FETCH_STATUS < > 0
BEGIN

INSERT INTO @ClaimantPerDoc

	SELECT
		@LastDocID,
		@ClaimantList

SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints


--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

DECLARE @firmaddressid nvarchar (10)

DECLARE firm_Curs CURSOR FOR SELECT
	nct.FirmAddressID
FROM @NewDocumentsTable nct
GROUP BY nct.FirmAddressID


OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @firmaddressid
WHILE @@FETCH_STATUS = 0
BEGIN



--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	
 
 
--->
</STYLE>'



--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

IF (SELECT
	COUNT(docTitle)
FROM @NewDocumentsTable
WHERE FirmAddressID = @firmaddressid) > 0
begin
SET @BodyMessage = @BodyMessage + '<H4>The following documents have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +


N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants (ClaimantID)</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th>' +


CAST((SELECT
	'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle"> ' + docTitle + ' </td>',
	'',
	'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle"> ' + cl.ClaimantList + ' </td>',
	'',
	'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle"> ' + c.DocType + ' </td>',
	''
FROM @NewDocumentsTable C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
WHERE FirmAddressID = @firmaddressid
GROUP BY	docTitle,
			cl.ClaimantList,
			DocType
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +


N'</table>' + N'<br/>'


end
ELSE
begin
SET @BodyMessage = @BodyMessage + '<H4>No Complaints have been uploaded in the last 24 hours. </H4>'
end



--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')



--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

Print Substring (@BodyMessage, 1, 4000)
Print Substring (@BodyMessage, 4001, 4000)
Print Substring (@BodyMessage, 8001, 4000)

DECLARE @RecipientList nvarchar (500)
SET @RecipientList = ''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar (500)
DECLARE contact_curs CURSOR
LOCAL FORWARD_ONLY STATIC READ_ONLY
FOR SELECT
	email
FROM vtblPerson p
WHERE FirmAddressID = @FirmAddressID
AND complaintContact = 1 AND status = 1

OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS = 0
BEGIN

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

print @Recipientlist

IF (SELECT
	COUNT(docTitle)
FROM @NewDocumentsTable
WHERE FirmAddressID = @firmaddressid) > 0
begin
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'owenss@kcic.com', --@RecipientList,
								@copy_recipients = '', --'',
								@blind_copy_recipients = 'gpc@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'
end

--Move to the next firm to create a new email
FETCH NEXT FROM firm_curs INTO @FirmAddressID
END
CLOSE firm_curs
DEALLOCATE firm_curs

end



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewDocuments] TO [power_user]
GO
