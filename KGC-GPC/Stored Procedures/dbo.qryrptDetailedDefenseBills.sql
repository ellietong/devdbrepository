SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[qryrptDetailedDefenseBills] 
as
set nocount on

Select Lawfirmoffice, InvoiceID_LawFirm, InvoiceDate,ChargeNumber, ChargeDate, ChargeTimekeeperName, ChargeType, 
ChargeBilledAmt, ChargeRecommendedAmt, ltrim(ChargeDescription) as ChargeDescr
from CounselLink_Data_ToBill
where coalesce(AlreadyBilled,0)=0
--and coalesce(DoNotBill, 0)=0
and InvoiceApprovedDate is not null
--and coalesce(CODE, '0') not in ('20531', '19822399')
--and NCC=0
--and MotionOnly = 0
--and InvoiceDate between '1/1/2012' and '1/31/2012'


--Union all

--Select Lawfirmoffice, InvoiceID_LawFirm, InvoiceDate,ChargeNumber, ChargeDate, ChargeTimekeeperName, ChargeType, 
--ChargeBilledAmt, ChargeRecommendedAmt, ltrim(ChargeDescription)as ChargeDescr
--from CounselLink_Data2
--where coalesce(DoNotBill, 0)=0
--and InvoiceApprovedDate is not null
----and coalesce(CODE, '0') not in ('20531', '19822399')
----and NCC=0
----and MotionOnly = 0
--order by LawFirmOffice,
-- InvoiceDate, InvoiceID_LawFirm, ChargeNumber

--compute sum(chargerecommendedamt)
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailedDefenseBills] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailedDefenseBills] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailedDefenseBills] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailedDefenseBills] TO [power_user]
GO
