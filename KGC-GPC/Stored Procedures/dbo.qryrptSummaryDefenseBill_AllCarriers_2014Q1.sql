SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptSummaryDefenseBill_AllCarriers_2014Q1]
(@CarrierName Nvarchar(50), @InvoiceNumber Nvarchar(50), @occurrencegroupid int)
As
Set Nocount on

--declare @CarrierName Nvarchar(50)
--declare @InvoiceNumber Nvarchar(50)
--DECLARE @occurrencegroupid int

--set @CarrierName= 'ACE'
--set @InvoiceNumber = 'GPC20140331'
--set @occurrencegroupid = 13


Declare @Reference_IDs table (claimantpersonalinfoid bigint, reference_id_parse nvarchar(500))
insert @Reference_IDs

---this is parsing out all claimants with only 1 referenceID and also the left referenceID if a claimant has >1
select claimantpersonalinfoid, CASE WHEN REFERENCE_ID like '%,%' then (left(REFERENCE_ID,(PATINDEX('%,%',REFERENCE_ID))-1))
			WHEN REFERENCE_ID is NULL then '10000000' 
			else REFERENCE_ID  end as REFERENCE_ID_Parse
from tblClaimantPersonalInfo
UNION ALL
---this is parsing out right referenceID if a claimant has 2 reference IDs and middle reference ID when claimant has 3
select claimantpersonalinfoid, 
CASE WHEN len(REFERENCE_ID) - len(replace(REFERENCE_ID,',','')) = 1 then (right(REFERENCE_ID,(PATINDEX('%,%',REFERENCE_ID))-1))
	WHEN len(REFERENCE_ID) - len(replace(REFERENCE_ID,',','')) = 2 then SUBSTRING(REFERENCE_ID, CHARINDEX(',', REFERENCE_ID) +2  ,   LEN(REFERENCE_ID) - CHARINDEX(',', REFERENCE_ID) - CHARINDEX(',', REVERSE(REFERENCE_ID))-1) 		
	WHEN REFERENCE_ID is NULL then '10000000'
			else '10000000'  end as REFERENCE_ID_Parse
from tblClaimantPersonalInfo
UNION ALL
---this is parsing out right referenceID if claimant has 3 reference IDs
select claimantpersonalinfoid, 
CASE WHEN len(REFERENCE_ID) - len(replace(REFERENCE_ID,',','')) = 2 then (right(REFERENCE_ID,CHARINDEX(',', REVERSE(REFERENCE_ID))-2))
	WHEN REFERENCE_ID is NULL then '10000000'
			else '10000000'  end as REFERENCE_ID_Parse
from tblClaimantPersonalInfo
Union All
select ClaimantPersonalInfoID, convert(nvarchar(500),ClaimantPersonalInfoID)
from tblClaimantPersonalInfo
--where ClaimantPersonalInfoID not in (20607,21860,21910,21933,21988,22366)

Declare @Employment table (claimantpersonalinfoid bigint, iscolyear int, gpcacquisitionyear int, FirstExposure int)
insert @Employment


SELECT o.ClaimID AS ClaimantPersonalInfoID, o.IsColyear, o.GPCAcquisitionDate AS gpcacquisitionyear, YEAR(o.TriggerStart1) as FirstExposure
FROM tblOccurrence o
WHERE o.OccurrenceGroupID = @occurrencegroupid and o.OccurrenceTypeID = 2



declare @Data table (Code nvarchar(250), ClaimantPersonalInfoID bigint, plaintiff nvarchar(100),  
Jurisdiction nvarchar(100), State nvarchar(5),
LocalCounsel nvarchar(500), [InvoiceID(LawFirm)] nvarchar(100), invoicedate date, fees money, costs money,
 invoiceapproveddate date, motiononly int, GPCAcquisitionDate int, IsColyear int, firstexposure int, donotbill int)
 
insert into @Data

select Coalesce(CODE,'0'), Coalesce(cpi.claimantpersonalinfoid,'0'),
case when MotionOnly = 1 or DoNotBill=1 then 'Unknown Plaintiff' 
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County: Asbestos'	 
	 when CODE IN ('20531', '19822399') and  coalesce(NCC,0)=0 then 'Master Asbestos File'
	 else LastName+', '+FirstName end as Plaintiff, 
case when MotionOnly = 1 or DoNotBill=1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County'	 
	 else j.Jurisdiction end as Jurisdiction, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
	 else st.State end as State,	 	 
d.LawFirmOffice,[InvoiceID(LawFirm)], d.InvoiceDate,  
sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
InvoiceApprovedDate, coalesce(motiononly,0), MIN(GPCAcquisitionYear) as GPCAcquisitionDate, ee.iscolyear, FirstExposure, coalesce(donotbill,0)
from CounselLink_Data_ToBill d

---JL note- embedded query needed (along with @referenceID temp table) to parse ReferenceIDs with >1 listed in tblclaimantpersonalinfo- taking min CPID because some duplicate IRNs not consolidated if cases were not open
LEFT JOIN (select min(claimantpersonalinfoid) as claimantpersonalinfoid, REFERENCE_ID_Parse from @Reference_IDs where REFERENCE_ID_Parse <> '10000000' group by  REFERENCE_ID_Parse )  r on r.reference_id_parse=d.Code
left join tblClaimantPersonalInfo cpi on r.claimantpersonalinfoid=cpi.ClaimantPersonalInfoID
left join (select * from tblClaim where IsCurrentClaim=1) c on c.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
left join (select * from tblClaimLawsuit where IsPrimaryForClaim=1) cl on cl.ClaimID =c.ClaimID 
left join tblLawsuit l on l.LawsuitID =cl.LawsuitID 
left join tblState st on st.StateID=l.StateID
left join (select * from tblClaimLawsuitStatusTracking where IsPrimaryStatus=1) clst on clst.ClaimLawsuitID=cl.ClaimLawsuitID
left join @Employment ee on ee.claimantpersonalinfoid=cpi.ClaimantPersonalInfoID
left join (select * from tblClaimDisease where PrimaryDisease=1) cd on c.ClaimID=cd.ClaimID
left join TblJurisdiction j on j.JurisdictionID=l.JurisdictionID
left join TblDisease dis on dis.DiseaseID=cd.DiseaseID
where 
InvoiceApprovedDate is not null --Excludes invoices that have not been approved
and coalesce(alreadybilled,0)=0 --Excludes invoices we can tell Riley already billed
and coalesce(NCC,0)=0 
--and ChargeDate >= '6/1/2012'


group by  Coalesce(CODE,'0'), cpi.ClaimantPersonalInfoID, [InvoiceID(LawFirm)], d.InvoiceDate, lawfirmoffice, ChargeType, 
case when MotionOnly = 1 or DoNotBill=1 then 'Unknown Plaintiff' 
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then 'Miscellaneous/Administrative Expense'
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County: Asbestos'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County: Asbestos'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County: Asbestos'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County: Asbestos'	 
	 when CODE IN ('20531', '19822399') and  coalesce(NCC,0)=0 then 'Master Asbestos File'
	 else LastName+', '+FirstName end,
case when MotionOnly = 1 or DoNotBill=1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%Broward County%' and coalesce(code,'0')='0' then 'Broward County'
	 when ChargeDescription like '%Dade County%' and coalesce(code,'0')='0' then 'Dade County'
	 when ChargeDescription like '%Hillsborough County%' and coalesce(code,'0')='0' then 'Hillsborough County'
	 when ChargeDescription like '%Palm Beach County%' and coalesce(code,'0')='0' then 'Palm Beach County'	 
	 else j.Jurisdiction end, 
case when MotionOnly = 1 then ''
	 when coalesce(code,'0') ='0' and chargedescription not like '%County: Asbestos%' then ''
	 when ChargeDescription like '%County: Asbestos%' and coalesce(code,'0')='0' then 'FL'
	 else st.State end,  
	 InvoiceApprovedDate,coalesce(motiononly,0), ee.iscolyear, FirstExposure, coalesce(donotbill,0)

	 
	 
	 
declare @final table (Code nvarchar(250), ClaimantPersonalInfoID bigint, plaintiff nvarchar(100),  
Jurisdiction nvarchar(100), State nvarchar(5),
LocalCounsel nvarchar(500), [InvoiceID(LawFirm)] nvarchar(100), invoicedate date, fees money, costs money,
 invoiceapproveddate date, motiononly int, GPCAcquisitionDate int, IsColyear int, firstexposure int, donotbill int, billperiod nvarchar(50),
 FirstStateFees float, FirstStateCosts float, RoyalFees float, RoyalCosts float, SentryFees float, SentryCosts float, TravelersFees float, TravelersCosts float, WausauFees float, WausauCosts float,
 ACEFees float, ACECosts float, CNAFees float, CNACosts float)
 
 insert into @final

--NOW PULL IN INSURER SHARES


Select D.*, datename(month, dateadd(month, month(invoiceapproveddate), 0)-1) + ' 2014' as BillPeriod,
--FIRST STATE
case when motiononly = 1 or coalesce(DoNotBill,0)=1 or ClaimantPersonalInfoID in (11510) then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 and ClaimantPersonalInfoID not in (11510) then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR((FirstExposure)))) *.0357)*fees)
	 when GPCAcquisitionDate >1986 then 0
		else .0357*fees end as FirstStateFees,
case when motiononly = 1 or coalesce(DoNotBill,0)=1 or ClaimantPersonalInfoID in (11510)  then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 and ClaimantPersonalInfoID not in (11510) then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR((FirstExposure)))) *.0357)*Costs)
	 when GPCAcquisitionDate >1986 then 0
		else .0357*Costs end as FirstStateCosts,
--ROYAL		
case when motiononly = 1 or coalesce(DoNotBill,0)=1 or ClaimantPersonalInfoID in (11510) then 0
	 when Coalesce(IsColyear,0)=1  and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 and ClaimantPersonalInfoID not in (11510) then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR((FirstExposure)))) *.0215)*fees)
	 when ((Coalesce(iscolyear,0)= 1 and GPCAcquisitionDate >1986) Or coalesce(IsColyear,0) <>1) then 0
		else .0215*fees end as RoyalFees,	
case when motiononly = 1 or coalesce(DoNotBill,0)=1 or ClaimantPersonalInfoID in (11510) then 0
	 when Coalesce(IsColyear,0)=1 and GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 and ClaimantPersonalInfoID not in (11510) then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR((FirstExposure)))) *.0215)*Costs)
	 when (( Coalesce(iscolyear,0) = 1 and GPCAcquisitionDate >1986) Or Coalesce(IsColyear,0) <>1) then 0
		else .0215*Costs end as RoyalCosts,		
--SENTRY		
case when motiononly = 1 or coalesce(DoNotBill,0)=1 or ClaimantPersonalInfoID in (11510) then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 and ClaimantPersonalInfoID not in (11510) then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR((FirstExposure)))) *.1275)*fees)
	 when  GPCAcquisitionDate >1986 then 0
		else .1275*fees end as SentryFees,
case when motiononly = 1 or coalesce(DoNotBill,0)=1 or ClaimantPersonalInfoID in (11510) then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 and ClaimantPersonalInfoID not in (11510) then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR((FirstExposure)))) *.1275)*Costs)
	 when GPCAcquisitionDate >1986 then 0
		else .1275*Costs end as SentryCosts,
--TRAVELERS		
case when motiononly = 1 or coalesce(DoNotBill,0)=1  then 0
	 when GPCAcquisitionDate >1986 then 0
		else .085*fees end as TravelersFees,
case when motiononly = 1 or coalesce(DoNotBill,0)=1  then 0
	 when  GPCAcquisitionDate >1986 then 0
		else .085*costs end as TravelersCosts,
--WAUSAU
case when motiononly = 1 or coalesce(DoNotBill,0)=1 or ClaimantPersonalInfoID in (11510) then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 and ClaimantPersonalInfoID not in (11510) then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR((FirstExposure)))) *.1275)*fees)
	 when GPCAcquisitionDate >1986 then 0
		else .1275*fees end as WAUSAUFees,
case when motiononly = 1 or coalesce(DoNotBill,0)=1 or ClaimantPersonalInfoID in (11510) then 0
	 when GPCAcquisitionDate >= 1982 and GPCAcquisitionDate <= 1986 and ClaimantPersonalInfoID not in (11510)  then (((1986.0 - convert(float,GPCAcquisitionDate))/(1986.0- convert(float,YEAR((FirstExposure)))) *.1275)*Costs)
	 when GPCAcquisitionDate >1986 then 0
		else .1275*Costs end as WAUSAUCosts,
--ACE		
case when motiononly = 1 or coalesce(DoNotBill,0)=1  then 0
	 when GPCAcquisitionDate >1986 then 0
		else .0425*fees end as ACEFees,	
case when motiononly = 1 or coalesce(DoNotBill,0)=1  then 0
	 when GPCAcquisitionDate >1986 then 0
		else .0425*Costs end as ACECosts,	
--HARBOR/CNA		
case when motiononly = 1 or coalesce(DoNotBill,0)=1  then 0
	 when coalesce(GPCAcquisitionDate, 0) <=1979 then .0425*fees else 0 end as CNAFees,
case when motiononly = 1 or coalesce(DoNotBill,0)=1  then 0
	 when coalesce(GPCAcquisitionDate, 0) <=1979 then .0425*Costs else 0 end as CNACosts								

From @data d
order by LocalCounsel,[InvoiceID(LawFirm)], code,  costs desc, fees desc


Declare @summary table (billPeriod nvarchar(50), fees float, costs float, insurershareFees float, InsurerShareCosts float)
insert into @summary

select billPeriod, SUM(fees) as fees, SUM(costs) as costs, 
case
when  @CarrierName ='First State' then sum(FirstStateFees) 
when @CarrierName ='Royal' then sum(RoyalFees)
when @CarrierName ='Sentry' then sum(SentryFees)
when @CarrierName ='Travelers' then sum(TravelersFees)
when @CarrierName ='Wausau' then sum(WausauFees)
when @CarrierName ='ACE' then sum(ACEFees)
when @CarrierName ='CNA' then sum(CNAFees) end as InsurerShareFees, 
case
when  @CarrierName ='First State' then sum(FirstStateCosts)
when @CarrierName ='Royal' then sum(RoyalCosts)
when @CarrierName ='Sentry' then sum(SentryCosts)
when @CarrierName ='Travelers' then sum(TravelersCosts)
when @CarrierName ='Wausau' then sum(WausauCosts)
when @CarrierName ='ACE' then sum(ACECosts)
when @CarrierName ='CNA' then sum(CNACosts) end as InsurerShareCosts 
from @final
group by BillPeriod
order by MONTH(BillPeriod)

--select * from @summary


Declare @data2 table (invoicedate date, invoicenumber nvarchar(100), LocalCounsel nvarchar(100),
TotalFees money, TotalCosts money, Invoiceapproveddate date)
insert into @data2
Select InvoiceDate, [InvoiceID(LawFirm)] as invoiceid_lawfirm, LawFirmOffice, 
sum(case when chargetype = 'legal fee' then (chargerecommendedamt) else 0 end) as Fees, 
sum(case when chargetype = 'Expense' then (chargerecommendedamt) else 0 end) as Costs,
InvoiceApprovedDate
from CounselLink_Data_2014Q1
where --Coalesce(DoNotBill,0)=0 
 coalesce(AlreadyBilled, 0)=0
and InvoiceApprovedDate is not null
and coalesce(code, '0') in ('20531', '19822399')
and NCC=1
--and ChargeDate >= '6/1/2012'
group by invoicedate,[InvoiceID(LawFirm)], lawfirmoffice, InvoiceApprovedDate



declare @NCCfinal table (invoicedate date, invoicenumber nvarchar(100), LocalCounsel nvarchar(100),
TotalNCCFees money, TotalNCCCosts money, Invoiceapproveddate date, billperiod nvarchar(50), FirstStateNCCFees money, FirstStateNCCCosts money, RoyalNCCFees money, RoyalNCCCosts money,
SentryNCCFees money, SentryNCCCosts money, TravelersNCCFees money, TravelersNCCCosts money, WausauNCCFees money, WausauNCCCosts money, ACENCCFees money, ACENCCCosts money, 
CNANCCFees money, CNANCCcosts money)

insert into @NCCfinal
select d.*, datename(month, dateadd(month, month(invoiceapproveddate), 0)-1) + ' 2014' as BillPeriod,
--FIRST STATE
.0357*TotalFees as FirstStateFees, .0357*TotalCosts as FirstStateCosts,
--ROYAL
.0215*TotalFees as RoyalFees, .0215*TotalCosts as RoyalCosts,
--SENTRY
.1275*TotalFees as SentryFees, .1275*TotalCosts as SentryCosts,
--TRAVELERS
case when Invoiceapproveddate <='5/31/2012' then .1275*TotalFees else .085*TotalFees end as TravelersFees, 
case when Invoiceapproveddate <='5/31/2012' then .1275*TotalCosts else .085*TotalCosts end as TravelersCosts,
--WAUSAU
.1275*TotalFees as WausauFees, .1275*TotalCosts as WausauCosts,
--ACE
.0425*TotalFees as ACEFees, .0425*TotalCosts as ACEFees,
--HARBOR/CNA
.0425*TotalFees as HarborFees, .0425*TotalCosts as HarborFees
from @data2 d

order by LocalCounsel, invoicedate, invoicenumber



Declare @NCCSUmmary table (billPeriod nvarchar(50), TotalNCCFees money, TotalNCCCosts money, InsurerShareNCCFees money, InsurerShareNCCCosts money)
insert into @NCCSUmmary

select billPeriod, SUM(TotalNCCFees) as TotalNCCFees, SUM(TotalNCCCosts) as TotalNCCCosts, 
case
when  @CarrierName ='First State' then sum(FirstStateNCCFees)
when @CarrierName ='Royal' then sum(RoyalNCCFees)
when @CarrierName ='Sentry' then sum(SentryNCCFees)
when @CarrierName ='Travelers' then sum(TravelersNCCFees)
when @CarrierName ='Wausau' then sum(WausauNCCFees)
when @CarrierName ='ACE' then sum(ACENCCFees)
when @CarrierName ='CNA' then sum(CNANCCFees) end as InsurerShareNCCFees, 
case
when  @CarrierName ='First State' then sum(FirstStateNCCCosts)
when @CarrierName ='Royal' then sum(RoyalNCCCosts)
when @CarrierName ='Sentry' then sum(SentryNCCCosts)
when @CarrierName ='Travelers' then sum(TravelersNCCCosts)
when @CarrierName ='Wausau' then sum(WausauNCCCosts)
when @CarrierName ='ACE' then sum(ACENCCCosts)
when @CarrierName ='CNA' then sum(CNANCCCosts) end as InsurerShareNCCCosts 
from @NCCfinal
group by billperiod


select s.Billperiod, coalesce(fees,0) as fees, coalesce(costs,0) as costs,  coalesce(TotalNCCFees,0) as TotalNCCFees, coalesce(TotalNCCCosts,0) as TotalNCCCosts ,  coalesce(insurershareFees,0) as insurershareFees, coalesce(InsurerShareCosts,0) as InsurerShareCosts, 
coalesce(InsurerShareNCCFees,0) as InsurerShareNCCFees, coalesce(InsurerShareNCCCosts,0) as InsurerShareNCCCosts, @InvoiceNumber as InvoiceNumber, @CarrierName as CarrierName
from @summary s
left join @NCCSUmmary ncc on ncc.Billperiod=s.Billperiod
group by s.Billperiod, fees, costs,  TotalNCCFees, TotalNCCCosts,  insurershareFees, InsurerShareCosts, InsurerShareNCCFees, InsurerShareNCCCosts
order by MONTH(s.billPeriod)

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryDefenseBill_AllCarriers_2014Q1] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryDefenseBill_AllCarriers_2014Q1] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryDefenseBill_AllCarriers_2014Q1] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryDefenseBill_AllCarriers_2014Q1] TO [power_user]
GO
