SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_REPORTS_qryDismissedCasesByJurisdiction]
(@MatterID int,@BeginDate datetime, @ENDDate datetime)

As
Set NoCount On

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


DECLARE @DiseASeCount Table (Matter nvarchar(100), MatterID int, claimlawsuitstatus nvarchar (500),
	MoYearDismissed datetime, YearDismissed datetime,MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int, unknowncount1 int)
INSERT INTO @DiseASeCount

SELECT  Matter, C.MatterID,  css.claimlawsuitstatus,
		CONVERT(datetime,(CONVERT(nvarchar,Month(cst.statusDate))+'/1/'+ CONVERT(nvarchar,Year(cst.StatusDate)))) AS MoYearDismissed,
		CONVERT(nvarchar,Year(cst.StatusDate)) AS YearDismissed,
		--Meso
		CASE WHEN DMeso.HighLevelDiseASeID = 1 
			 THEN 1
			 ELSE 0 
		END AS MesoCount,
		--Lung Cancer
		CASE WHEN DLC.HighLevelDiseASeID = 2 
			 THEN 1
			 ELSE 0 
		END AS LungCancerCount,
		--Other Cancer
		CASE WHEN DOC.HighLevelDiseASeID = 3 
			 THEN 1
			 ELSE 0 
		END AS OtherCancerCount,
		--NonMalig
		CASE WHEN DNM.HighLevelDiseASeID in (4,7) 
			 THEN 1
			 ELSE 0 
		END AS NonMaligCount,
		--Unknown
		CASE WHEN DUnk.HighLevelDiseASeID in (5,6) 
			 THEN 1
			 ELSE 0 
	    END AS UnknownCount,
		CASE WHEN DUnk.HighLevelDiseASeID IS NULL 
			 THEN 1
			 ELSE 0 
		END AS UnknownCount1
		
FROM tblClaimantPersonalInfo CPI
	LEFT JOIN tblClaim C ON CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
	LEFT JOIN (SELECT CD.ClaimDiseASeID,CD.ClaimID, CD.DiseASeDiagnosisDate, CD.DiseASeID, CD.DoctorID, CD.PrimaryDiseASe FROM tblClaimDiseASe CD WHERE PrimaryDiseASe = 1) CD ON CD.ClaimID=C.ClaimID
	LEFT JOIN tblMatter M ON c.MatterID = M.MatterID
	LEFT JOIN tblClaimSequence csq ON c.ClaimSequenceID = csq.ClaimSequenceID
	--Meso
	LEFT JOIN tblDiseASe DMeso ON CD.DiseASeID=DMeso.DiseASeID
	--Lung Cancer
	LEFT JOIN tblDiseASe DLC ON CD.DiseASeID=DLC.DiseASeID
	--Other Cancer
	LEFT JOIN tblDiseASe DOC ON CD.DiseASeID=DOC.DiseASeID
	--Non-Malig
	LEFT JOIN tblDiseASe DNM ON CD.DiseASeID=DNM.DiseASeID
	--Unknown
	LEFT JOIN tblDiseASe DUnk ON CD.DiseASeID=DUnk.DiseASeID
	LEFT JOIN tblClaimLawsuit CL ON C.ClaimID=CL.ClaimID
	LEFT JOIN tblLawsuit L ON CL.LawsuitID=L.LawsuitID
	LEFT JOIN tblClaimantSettlement cs ON cs.ClaimLawsuitID = CL.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatusTracking cst ON cst.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatus css ON css.ClaimLawsuitStatusID = cst.ClaimLawsuitStatusID
	--LEFT JOIN tblclaimlawsuitstatusgroup csg ON csg.claimlawsuitstatusid = cst.ClaimLawsuitStatusID

WHERE COALESCE(isprimaryforclaim, 0) = 1   
	AND COALESCE(IsPrimaryStatus, 0) = 1
	--AND csg.statusgroupid = 3
	AND cst.ClaimLawsuitStatusID NOT IN (15,16,27,18,19,20,21,24,30,17,26)
	AND C.MatterID >= COALESCE(@MatterID,0) AND C.MatterID <= COALESCE(NULLIF(@MatterID,0),@maxMatterID) 
	AND cst.StatusDate BETWEEN COALESCE(@BeginDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) AND COALESCE(@ENDDate, GETDATE())
	--AND COALESCE(cpi.jobsiteinfo, 0) = 0 
ORDER BY  Year(cst.StatusDate)

SELECT  SUM(MesoCount) AS MesoCount,
		SUM(LungCancerCount) AS LungCancerCount, SUM(OtherCancerCount) AS OtherCancerCount,
		SUM(NonMaligCount) AS NonMaligCount, SUM(UnknownCount+unknowncount1) AS UnknownCount, @BeginDate AS StartDate, @ENDDate AS ENDDate,
		MoYearDismissed, YearDismissed, DATENAME(Month, MoYearDismissed) AS Month, DATENAME(Year, MoYearDismissed) ASYear,
		SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + unknowncount1) AS MonthTotal , 
		DATENAME(Month, MoYearDismissed) + ' ' + DATENAME(Year, MoYearDismissed) AS MonthYear
FROM @DiseASeCount
GROUP BY MoYearDismissed, YearDismissed

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissedCasesByJurisdiction] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissedCasesByJurisdiction] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissedCasesByJurisdiction] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissedCasesByJurisdiction] TO [power_user]
GO
