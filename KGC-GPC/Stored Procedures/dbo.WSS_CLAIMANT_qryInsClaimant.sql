SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryInsClaimant] 
	-- Add the parameters for the stored procedure here
																(@Address1				 nvarchar(250), 
																 @Address2				 nvarchar(250), 
																 @StateID				 int, 
																 @City					 nvarchar(250), 
																 @Zip					 nvarchar(5), 
																 @Mailzip				 nvarchar(4), 
																 @BirthDate				 datetime, 
																 @DeceasedDate			 datetime, 
																 @GenderID				 int, 
																 @MaritalStatusID		 int, 
																 --@IsSmoker				 bit, 
																 --@SmokerStart			 datetime, 
																 --@SmokerEnd				 datetime, 
																 --@PacksPerDay			 float,
																 @EmailAddress			 nvarchar(250), 
																 @FullSSN				 nvarchar(150),
																 @FirstName				 nvarchar (250), 
																 @MiddleName			 nvarchar (250) = NULL, 
																 @LastName				 nvarchar (250),
																 @SuffixID				 int,
																 @IsSmoker				 bit	)
																 --@Notes					 nvarchar (1000)) --@DOFE datetime, @ClaimTypeID int, @Notes nvarchar (1000), @SuffixID int, @Emailaddress nvarchar(250))
AS
BEGIN

DECLARE @MaxCPID int
SET @MaxCPID = (SELECT IDENT_CURRENT('tblClaimantPersonalInfo')+IDENT_INCR('tblClaimantPersonalInfo'))

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblClaimantPersonalInfo (FirstName, 
										MiddleName, 
										LastName, 
										--Notes, 
										SuffixID,
										GenderID,
										BirthDate,
										DeceasedDate,
										SocialSecurityNumber,
										MaritalStatusID,
										Address1,
										Address2,
										City,
										StateID,
										Zip,
										MailZip, 
										SPUsername,
										IsSmoker) --DOFE, ClaimTypeID, Notes, SuffixID, SPUsername)
	VALUES ((UPPER(@FirstName)),
			(UPPER(@MiddleName)), 
			(UPPER(@LastName)), 
			--@Notes, 
			@SuffixID,
			@GenderID,
			@BirthDate,
			@DeceasedDate,
			@FullSSN, 
			@MaritalStatusID,
			@Address1,
			@Address2,
			@City,
			@StateID,
			@Zip,
			@Mailzip, 
			@Emailaddress,
			@IsSmoker) --@DOFE, @ClaimTypeID, @Notes, @SuffixID, @Emailaddress)
	
SELECT @MaxCPID	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryInsClaimant] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryInsClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryInsClaimant] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryInsClaimant] TO [power_user]
GO
