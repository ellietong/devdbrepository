SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_CLAIM_RECENT_qryClaimReports]
(
--@ClaimantNames				nvarchar_list_tbltype READONLY, 
														   @HighLevelDiseaseIDs			integer_list_tbltype READONLY, 	
														   --@SSNs						nvarchar_list_tbltype READONLY, 
														   @StateIDs					integer_list_tbltype READONLY, 
														   @AllegationIDs				integer_list_tbltype READONLY,
														   @JurisdictionIDs				integer_list_tbltype READONLY,
														   @PlaintiffCounselIDs			integer_list_tbltype READONLY, --PlaintiffCounselAddressIDs
														   @MatterIDs					integer_list_tbltype READONLY, 
														   @MaterialIDs					integer_list_tbltype READONLY, 
														   @IsMDL						integer_list_tbltype READONLY, 
														   @emailaddress				nvarchar(250), 
														   --@DocketNumbers				nvarchar_list_tbltype READONLY, 
														   @ClaimLawsuitStatusIDs		integer_list_tbltype READONLY, 
														   @ActiveTrialGroupIDs			integer_list_tbltype READONLY,
														   @ActiveTrialDate				nvarchar(250) = null,
														   @ProcessedDate				nvarchar(250) = null,
														   @StatusDate					nvarchar(250) = null,
														   @OrderByString				nvarchar(MAX),
														   @RowStart					bigint,
														   @RowCount					bigint)
AS
BEGIN
SET NOCOUNT ON


/*** !!!! IMPORTANT NOTE: The QueryString is close to reaching its max char limit. 
		  Any additions to the QueryString may cause portions of the string to be cut off when it is executed. - SDO !!! ***/


------The following set of variables can be used for testing.
--DECLARE @ClaimantName              nvarchar(max)
--DECLARE @SSN                       nvarchar(20)
--DECLARE @StateID                   int
--DECLARE @PlaintiffCounselAddressID int
--DECLARE @MatterID                  int
----DECLARE @IsMDL                     int
--DECLARE @emailaddress              nvarchar(250)
--DECLARE @DocketNumber              nvarchar(250)
--DECLARE @ClaimLawsuitStatusID      int
--DECLARE @ClaimantIDs               TABLE (n int) 
--DECLARE @HighLevelDiseaseIDs integer_list_tbltype
--DECLARE @StateIDs integer_list_tbltype
--DECLARE @JurisdictionIDs integer_list_tbltype
--DECLARE @AllegationIDs integer_list_tbltype
--DECLARE @PlaintiffCounselIDs integer_list_tbltype
--DECLARE @ActiveTrialGroupIDs integer_list_tbltype
--DECLARE @MaterialIDs integer_list_tbltype
--DECLARE @MatterIDs integer_list_tbltype
--DECLARE @IsMDL integer_list_tbltype
--DECLARE @ClaimLawsuitStatusIDs integer_list_tbltype
--DECLARE @ActiveTrialDate datetime
--DECLARE @ProcessedDate				nvarchar(250) = null
--DECLARE @StatusDate					nvarchar(250) = null
--DECLARE @RowCount int = 65
--DECLARE @OrderByString nvarchar(MAX) = 'lastname'
--DECLARE @RowStart int = 1

--SET @ClaimantName         = NULL
--SET @SSN                  = NULL

--insert into @HighLevelDiseaseIDs
--select 2
--INSERT INTO @StateIDs 
--SELECT 45
--insert into @PlaintiffCounselIDs
--select 332

--SET @PlaintiffCounselAddressID = NULL
--SET @MatterID                  = NULL
----SET @IsMDL                     = NULL
--SET @emailaddress              = 'crookc@kcicllc.com'
--SET @DocketNumber         = NULL
--SET @ClaimLawsuitStatusID      = NULL

--DefenseCounselAddressID Parameter
DECLARE @DefenseCounselAddressID int
SET		@DefenseCounselAddressID = (SELECT FirmAddressID 
									FROM vtblPerson 
									WHERE email = @emailaddress 
									GROUP BY FirmAddressID)

DECLARE @StartATDR int
--DECLARE @ClaimsBeforeATDR date

DECLARE @StartPDR int
--SET @StartPDR = (SELECT CHARINDEX('-', LTRIM(RTRIM(@ProcessedDate))))+ 1

DECLARE @StartSDR int
--SET @StartSDR = (SELECT CHARINDEX('-', LTRIM(RTRIM(@StatusDate))))+ 1

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
	
IF object_id('tempdb..#HighLevelDiseaseIDs') IS NOT NULL
BEGIN
   DROP TABLE #HighLevelDiseaseIDs
END
CREATE TABLE #HighLevelDiseaseIDs (dis int)
INSERT INTO #HighLevelDiseaseIDs
	SELECT n FROM @HighLevelDiseaseIDs

IF object_id('tempdb..#StateIDs') IS NOT NULL
BEGIN
   DROP TABLE #StateIDs
END
CREATE TABLE #StateIDs (s int)
INSERT INTO #StateIDs
	SELECT n FROM @StateIDs	
	
IF object_id('tempdb..#ActiveTrialGroupIDs') IS NOT NULL
BEGIN
   DROP TABLE #ActiveTrialGroupIDs
END
CREATE TABLE #ActiveTrialGroupIDs (tg int)
INSERT INTO #ActiveTrialGroupIDs
	SELECT n FROM @ActiveTrialGroupIDs	
	
IF object_id('tempdb..#JurisdictionIDs') IS NOT NULL
BEGIN
   DROP TABLE #JurisdictionIDs
END
CREATE TABLE #JurisdictionIDs (j int)
INSERT INTO #JurisdictionIDs
	SELECT n FROM @JurisdictionIDs
	
IF object_id('tempdb..#AllegationIDs') IS NOT NULL
BEGIN
   DROP TABLE #AllegationIDs
END
CREATE TABLE #AllegationIDs (a int)
INSERT INTO #AllegationIDs
	SELECT n FROM @AllegationIDs
	
IF object_id('tempdb..#PlaintiffCounselIDs') IS NOT NULL
BEGIN
   DROP TABLE #PlaintiffCounselIDs
END
CREATE TABLE #PlaintiffCounselIDs (pca int)
INSERT INTO #PlaintiffCounselIDs
	SELECT n FROM @PlaintiffCounselIDs
		
IF object_id('tempdb..#MatterIDs') IS NOT NULL
BEGIN
   DROP TABLE #MatterIDs
END
CREATE TABLE #MatterIDs (m int)
INSERT INTO #MatterIDs
	SELECT n FROM @MatterIDs

IF object_id('tempdb..#MaterialIDs') IS NOT NULL
BEGIN
   DROP TABLE #MaterialIDs
END
CREATE TABLE #MaterialIDs (mat int)
INSERT INTO #MaterialIDs
	SELECT n FROM @MaterialIDs
	
IF object_id('tempdb..#IsMDL') IS NOT NULL
BEGIN
   DROP TABLE #IsMDL
END
CREATE TABLE #IsMDL (mdl int)
INSERT INTO #IsMDL
	SELECT n FROM @IsMDL	
		
IF object_id('tempdb..#ClaimLawsuitStatusIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimLawsuitStatusIDs
END
CREATE TABLE #ClaimLawsuitStatusIDs (cls int)
INSERT INTO #ClaimLawsuitStatusIDs
	SELECT n FROM @ClaimLawsuitStatusIDs
		
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE 1=1 /*AND IsPrimaryForClaim=1*/'

IF (SELECT COUNT(mdl) FROM #IsMDL) <> 0 BEGIN SET @WhereString = @WhereString + ' AND IsMDL IN(SELECT mdl FROM #IsMDL)' END--' AND COALESCE(IsMDL, 0) IN(SELECT mdl FROM #IsMDL)' END


IF @ActiveTrialDate IS NOT NULL AND CHARINDEX('Claims after',@ActiveTrialDate) > 0
	BEGIN
		SET @StartATDR = (SELECT CHARINDEX('r ', LTRIM(RTRIM(@ActiveTrialDate))))+ 1
	DECLARE @ClaimsAfterATDR date
	SET @ClaimsAfterATDR = (SELECT CONVERT(nvarchar(1000),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@ActiveTrialDate)), (SELECT @StartATDR), (CHARINDEX('  ', LTRIM(RTRIM( @ActiveTrialDate)))+1)))) , CONVERT(nvarchar(250), '1/1/2100')))+'')
	SET @WhereString = @WhereString + ' AND COALESCE(et.EventDate, CONVERT(date, ''1/1/1900'')) >= ''' + CONVERT(nvarchar(100), @ClaimsAfterATDR) +'''' END
ELSE IF @ActiveTrialDate IS NOT NULL AND CHARINDEX('Claims before',@ActiveTrialDate) > 0
	BEGIN
		SET @StartATDR = (SELECT CHARINDEX('e ', LTRIM(RTRIM(@ActiveTrialDate))))+ 1
	DECLARE @ClaimsBeforeATDR date
	SET @ClaimsBeforeATDR = (SELECT CONVERT(nvarchar(1000),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@ActiveTrialDate)), (SELECT @StartATDR), (CHARINDEX('  ', LTRIM(RTRIM( @ActiveTrialDate)))+1)))) , CONVERT(nvarchar(250), '1/1/2100')))+'')
	SET @WhereString = @WhereString + ' AND COALESCE(et.EventDate, CONVERT(date, ''1/1/1900'')) <=''' + CONVERT(nvarchar(100), @ClaimsBeforeATDR) +'''' END
ELSE IF @ActiveTrialDate IS NOT NULL AND CHARINDEX('Claims before',@ActiveTrialDate) = 0 AND CHARINDEX('Claims after',@ActiveTrialDate) = 0
	BEGIN
		SET @StartATDR = (SELECT CHARINDEX('-', LTRIM(RTRIM(@ActiveTrialDate))))+ 1
	IF @ActiveTrialDate		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(et.EventDate, CONVERT(date, ''1/1/1900'')) >= ''' + CONVERT(nvarchar(100),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@ActiveTrialDate)), 1, (CHARINDEX('-', LTRIM(RTRIM(@ActiveTrialDate)))-1)))) , CONVERT(date, '1/1/1900')))+'''' END
	IF @ActiveTrialDate		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(et.EventDate, CONVERT(date, ''1/1/1900'')) <=''' + CONVERT(nvarchar(1000),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@ActiveTrialDate)), (SELECT @StartATDR), (CHARINDEX('-', LTRIM(RTRIM( @ActiveTrialDate)))+1)))) , CONVERT(date, '1/1/2100')))+'''' END
	END

IF @ProcessedDate IS NOT NULL AND CHARINDEX('Claims after',@ProcessedDate) > 0
	BEGIN
		SET @StartPDR = (SELECT CHARINDEX('r ', LTRIM(RTRIM(@ProcessedDate))))+ 1
	DECLARE @ClaimsAfterPDR date
	SET @ClaimsAfterPDR = (SELECT CONVERT(nvarchar(1000),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@ProcessedDate)), (SELECT @StartPDR), (CHARINDEX('  ', LTRIM(RTRIM( @ProcessedDate)))+1)))) , CONVERT(nvarchar(250), '1/1/2100')))+'')
	SET @WhereString = @WhereString + ' AND COALESCE(ProcessedDate, CONVERT(date, ''1/1/1900'')) >= ''' + CONVERT(nvarchar(100), @ClaimsAfterPDR) +'''' END
ELSE IF @ProcessedDate IS NOT NULL AND CHARINDEX('Claims before',@ProcessedDate) > 0
	BEGIN
		SET @StartPDR = (SELECT CHARINDEX('e ', LTRIM(RTRIM(@ProcessedDate))))+ 1
	DECLARE @ClaimsBeforePDR date
	SET @ClaimsBeforePDR = (SELECT CONVERT(nvarchar(1000),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@ProcessedDate)), (SELECT @StartPDR), (CHARINDEX('  ', LTRIM(RTRIM( @ProcessedDate)))+1)))) , CONVERT(nvarchar(250), '1/1/2100')))+'')
	SET @WhereString = @WhereString + ' AND COALESCE(ProcessedDate, CONVERT(date, ''1/1/1900'')) <=''' + CONVERT(nvarchar(100), @ClaimsBeforePDR) +'''' END
ELSE IF @ProcessedDate IS NOT NULL AND CHARINDEX('Claims before',@ProcessedDate) = 0 AND CHARINDEX('Claims after',@ProcessedDate) = 0
	BEGIN
		SET @StartPDR = (SELECT CHARINDEX('-', LTRIM(RTRIM(@ProcessedDate))))+ 1
	IF @ProcessedDate		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(ProcessedDate, CONVERT(date, ''1/1/1900'')) >= ''' + CONVERT(nvarchar(100),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@ProcessedDate)), 1, (CHARINDEX('-', LTRIM(RTRIM(@ProcessedDate)))-1)))) , CONVERT(date, '1/1/1900')))+'''' END
	IF @ProcessedDate		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(ProcessedDate, CONVERT(date, ''1/1/1900'')) <=''' + CONVERT(nvarchar(1000),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@ProcessedDate)), (SELECT @StartPDR), (CHARINDEX('-', LTRIM(RTRIM( @ProcessedDate)))+1)))) , CONVERT(date, '1/1/2100')))+'''' END
	END


IF @StatusDate IS NOT NULL AND CHARINDEX('Claims after',@StatusDate) > 0
	BEGIN
		SET @StartSDR = (SELECT CHARINDEX('r ', LTRIM(RTRIM(@StatusDate))))+ 1
	DECLARE @ClaimsAfterSDR date
	SET @ClaimsAfterSDR = (SELECT CONVERT(nvarchar(1000),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@StatusDate)), (SELECT @StartSDR), (CHARINDEX('  ', LTRIM(RTRIM( @StatusDate)))+1)))) , CONVERT(nvarchar(250), '1/1/2100')))+'')
	SET @WhereString = @WhereString + ' AND COALESCE(StatusDate, CONVERT(date, ''1/1/1900'')) >= ''' + CONVERT(nvarchar(100), @ClaimsAfterSDR) +'''' END
ELSE IF @StatusDate IS NOT NULL AND CHARINDEX('Claims before',@StatusDate) > 0
	BEGIN
		SET @StartSDR = (SELECT CHARINDEX('e ', LTRIM(RTRIM(@StatusDate))))+ 1
	DECLARE @ClaimsBeforeSDR date
	SET @ClaimsBeforeSDR = (SELECT CONVERT(nvarchar(1000),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@StatusDate)), (SELECT @StartSDR), (CHARINDEX('  ', LTRIM(RTRIM( @StatusDate)))+1)))) , CONVERT(nvarchar(250), '1/1/2100')))+'')
	SET @WhereString = @WhereString + ' AND COALESCE(StatusDate, CONVERT(date, ''1/1/1900'')) <=''' + CONVERT(nvarchar(100), @ClaimsBeforeSDR) +'''' END
ELSE IF @StatusDate IS NOT NULL AND CHARINDEX('Claims before',@StatusDate) = 0 AND CHARINDEX('Claims after',@StatusDate) = 0
	BEGIN
		SET @StartSDR = (SELECT CHARINDEX('-', LTRIM(RTRIM(@StatusDate))))+ 1
	IF @StatusDate		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(StatusDate, CONVERT(date, ''1/1/1900'')) >= ''' + CONVERT(nvarchar(100),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@StatusDate)), 1, (CHARINDEX('-', LTRIM(RTRIM(@StatusDate)))-1)))) , CONVERT(date, '1/1/1900')))+'''' END
	IF @StatusDate		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(StatusDate, CONVERT(date, ''1/1/1900'')) <=''' + CONVERT(nvarchar(1000),COALESCE(LTRIM(RTRIM(Substring(LTRIM(RTRIM(@StatusDate)), (SELECT @StartSDR), (CHARINDEX('-', LTRIM(RTRIM( @StatusDate)))+1)))) , CONVERT(date, '1/1/2100')))+'''' END
	END


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) 
	--+ ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

IF (SELECT COUNT(dis) FROM #HighLevelDiseaseIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #HighLevelDiseaseIDs hld ON d.HighLevelDiseaseID = hld.dis' END
IF (SELECT COUNT(s) FROM #StateIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #StateIDs si ON l.StateID = si.s' END
IF (SELECT COUNT(pca) FROM #PlaintiffCounselIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #PlaintiffCounselIDs pc ON pa.PlaintiffCounselAddressID = pc.pca' END
IF (SELECT COUNT(m) FROM #MatterIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #MatterIDs mid ON c.MatterID = mid.m' END
IF (SELECT COUNT(mat) FROM #MaterialIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #MaterialIDs mat ON a.MaterialID = mat.mat' END
--IF @MaterialID				  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND a.MaterialID = ' + CONVERT(nvarchar(20),@MaterialID) END
IF (SELECT COUNT(cls) FROM #ClaimLawsuitStatusIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #ClaimLawsuitStatusIDs clsid ON clst.ClaimLawsuitStatusID = clsid.cls' END
IF (SELECT COUNT(j) FROM #JurisdictionIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #JurisdictionIDs jur ON l.jurisdictionid = jur.j' END
IF (SELECT COUNT(a) FROM #AllegationIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #AllegationIDs alle ON a.AllegationTypeID = alle.a' END


--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'DECLARE @totalRows bigint
	SELECT @totalRows = COUNT(*) 
	FROM (SELECT cpi.claimantpersonalinfoid, lastname, firstname, 
			CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SocialSecurityNumber, 
			CONVERT(nvarchar(100),(BirthDate), 101) AS Birthdate, CONVERT(nvarchar(100),(deceaseddate), 101) AS DeceasedDate, c.claimid, HighLevelDisease AS HighLevelDisease, docketnumber, s.state, 
			jurisdiction, dca.defensecounsel AS PrimaryDefenseCounsel, l.LeadPlaintiffFirstName+'' ''+l.LeadPlaintiffLastName AS LeadPlaintiffName ,
			plaintiffcounsel, cl.ClaimLawsuitID, IsPrimaryForClaim, claimsequence, claimlawsuitstatus, 
			CONVERT(nvarchar(100),(StatusDate), 101) AS StatusDate, CONVERT(nvarchar(100),(et.EventDate), 101) AS ActiveTrialDate, CONVERT(NVARCHAR(100), Coalesce(FileDate, a.receiveddate), 101) AS FileDate, 
			CONVERT(nvarchar(100), (a.servicedate), 101) AS ServiceDate, CONVERT(nvarchar(100),(processeddate), 101) AS ProcessedDate, YEAR(coalesce(filedate, a.receiveddate)) AS YearFiled, 
			CONVERT(nvarchar(100),(settlementdate), 101) AS SettlementDate,''$ ''+CONVERT(varchar(12), settlementamount,1) AS SettlementAmount, CONVERT(nvarchar(100), min(sourcedate), 101) AS sourcedate, Coalesce(min(Occupation),''Unknown'') AS Occupation,
			CONVERT(NVARCHAR(100), ExposureStart, 101) AS ExposureStart, CONVERT(NVARCHAR(100), ExposureEnd, 101) AS ExposureEnd, ProductName
	FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi	ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl			ON cl.ClaimID = c.ClaimID
	INNER JOIN tblLawsuit l					ON l.LawsuitID = cl.LawsuitID
	INNER JOIN (select * 
				from tblclaimlawsuitstatustracking 
				where IsPrimaryStatus = 1) clst	ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatus cls		ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
	INNER JOIN tblClaimSequence cs				ON cs.ClaimSequenceID = c.ClaimSequenceID
	LEFT JOIN (SELECT MAX(EventDate) AS EventDate, ClaimLawsuitID FROM tblEventTracking WHERE EventTypeID = 11 GROUP BY ClaimLawsuitID) et ON cl.ClaimLawsuitID = et.ClaimLawsuitID
	/*LEFT JOIN Final_Claim_NF_Date f				ON f.clm_key = c.clm_key*/
	LEFT JOIN (select * 
			   from tblClaimDisease 
			   where PrimaryDisease = 1) cd	ON cd.ClaimID = c.ClaimID
	LEFT JOIN tblDisease d					ON d.DiseaseID = cd.DiseaseID
	LEFT JOIN tblHighLevelDisease hd		ON hd.HighLevelDiseaseID = d.HighLevelDiseaseID
	LEFT JOIN tblState s	ON s.StateID = l.StateID
	LEFT JOIN tblJurisdiction j				ON j.JurisdictionID = l.JurisdictionID
	LEFT JOIN tblPlaintiffCounselAddress pa	ON pa.PlaintiffCounselAddressID = l.NationalPlaintiffCounselAddressID
	LEFT JOIN tblClaimantSettlement settle	ON settle.ClaimLawsuitID = cl.ClaimLawsuitID'
	SET	 @Querystring = @QueryString + '	LEFT JOIN (SELECT ClaimantSettlementID, MIN(SourceDate) as SourceDate FROM tblPaymentSettlement pset	
					INNER JOIN tblCheck ch ON pset.CheckID = ch.CheckID 
					GROUP BY ClaimantSettlementID) 	ps	ON ps.ClaimantSettlementID = settle.ClaimantSettlementID
	LEFT JOIN vallegation a					ON a.claimlawsuitid = cl.claimlawsuitid ' + 
	
	' LEFT JOIN (SELECT ClaimantPersonalInfoID, 
					  Coalesce(min(Occupation),''Unknown'') AS Occupation, 
					  CASE WHEN min(coalesce(cemp.empstartdate,''1/1/2100''))<=  MIN(coalesce(cprod.startdate,''1/1/2100'')) 
					  THEN min(cemp.empstartdate)
					  ELSE MIN(cprod.startdate) 
					  END AS ExposureStart,
					  CASE WHEN MAX(coalesce(cemp.empenddate, ''1/1/1900'')) >= MAX(coalesce(cprod.enddate,''1/1/1900'')) 
					  THEN MAX(cemp.empenddate)
					  ELSE MAX(cprod.enddate) 
					  END AS ExposureEnd,
					  ProductName
				FROM tblClaimantEmployment cemp
				LEFT JOIN tblClaimantProduct cprod ON cemp.ClaimantEmploymentID = cprod.ClaimantEmploymentID
				LEFT JOIN tblProduct p ON p.ProductID = cprod.ProductID
				LEFT JOIN tblOccupation o ON cemp.OccupationID = o.OccupationID
				LEFT JOIN tblProductName pn ON pn.ProductNameID = p.ProductNameID
				WHERE IsPrimary = 1 --AND PrimaryProduct = 1 
				GROUP BY ClaimantPersonalInfoID, ProductName) cp ON cp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID '+	
	@JoinString + 
		
	 @WhereString + ' GROUP BY cpi.claimantpersonalinfoid, 
	lastname,
	firstname,
	SocialSecurityNumber, 
	birthdate, 
	deceaseddate, 
	c.claimid,
	HighLevelDisease,
	docketnumber, 
	s.state, 
	jurisdiction,
	dca.defensecounsel, 
	l.LeadPlaintiffFirstName+'' ''+l.LeadPlaintiffLastName,plaintiffcounsel, 
	cl.ClaimLawsuitID, 
	IsPrimaryForClaim, 
	claimsequence, 
	claimlawsuitstatus, 
	StatusDate, 
	et.EventDate,
	Coalesce(FileDate, a.receiveddate), 
	filedate,
	a.ServiceDate, 
	ProcessedDate, 
	YEAR(coalesce(filedate, a.receiveddate)), 
	settlementdate, 
	settlementamount,
	clst.ClaimLawsuitStatusID, Occupation, ExposureStart, ExposureEnd, SourceDate, ProductName) cpi 
	 
	SET ROWCOUNT ' + CASE WHEN (@RowCount < 0) THEN CONVERT(NVARCHAR(100), 0) ELSE CONVERT(nvarchar, @RowCount) END+''
	 
	SET	 @Querystring = @QueryString + '	 SELECT  cpi.claimantpersonalinfoid, (LastName + '', '' +FirstName) AS ClaimantName, SocialSecurityNumber, HighLevelDisease AS Disease, state, jurisdiction, claimid AS "c.claimid", claimsequence,  birthdate, deceaseddate, occupation, ProductName, ExposureStart, ExposureEnd,
			claimlawsuitstatus, 
			activetrialdate, servicedate, FileDate, 
			ProcessedDate, statusdate, docketnumber, plaintiffcounsel, 
			settlementdate, settlementamount, sourcedate,
			@totalrows as TotalRows FROM (
	
		SELECT ROW_NUMBER() OVER (ORDER BY ' + @OrderByString + ') AS RowNum,
				cpi.claimantpersonalinfoid, lastname, firstname, 
			CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SocialSecurityNumber, 
			CONVERT(nvarchar(100),(BirthDate), 101) AS Birthdate, CONVERT(nvarchar(100),(deceaseddate), 101) AS DeceasedDate, c.claimid, HighLevelDisease AS HighLevelDisease, docketnumber, s.state, 
			jurisdiction, dca.defensecounsel AS PrimaryDefenseCounsel, l.LeadPlaintiffFirstName+'' ''+l.LeadPlaintiffLastName AS LeadPlaintiffName ,
			plaintiffcounsel, cl.ClaimLawsuitID, IsPrimaryForClaim, claimsequence, claimlawsuitstatus, 
			CONVERT(nvarchar(100),(StatusDate), 101) AS StatusDate, CONVERT(nvarchar(100),(et.EventDate), 101) AS ActiveTrialDate, CONVERT(NVARCHAR(100), Coalesce(FileDate, a.receiveddate), 101) AS FileDate, 
			CONVERT(nvarchar(100), (a.servicedate), 101) AS ServiceDate, CONVERT(nvarchar(100),(processeddate), 101) AS ProcessedDate, YEAR(coalesce(filedate, a.receiveddate)) AS YearFiled, 
			CONVERT(nvarchar(100),(settlementdate), 101) AS SettlementDate,''$ ''+CONVERT(varchar(12), settlementamount,1) AS SettlementAmount, CONVERT(nvarchar(100), min(sourcedate), 101) AS sourcedate, Coalesce(min(Occupation),''UNKNOWN'') AS Occupation,
			CONVERT(NVARCHAR(100), ExposureStart, 101) AS ExposureStart, CONVERT(NVARCHAR(100), ExposureEnd, 101) AS ExposureEnd, ProductName
		FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl				ON cl.ClaimID = c.ClaimID
		INNER JOIN tblLawsuit l						ON l.LawsuitID = cl.LawsuitID
		INNER JOIN (select * 
					from tblclaimlawsuitstatustracking 
					where IsPrimaryStatus = 1) clst	ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaimLawsuitStatus cls		ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		INNER JOIN tblClaimSequence cs				ON cs.ClaimSequenceID = c.ClaimSequenceID
		LEFT JOIN (SELECT MAX(EventDate) AS EventDate, ClaimLawsuitID FROM tblEventTracking WHERE EventTypeID = 11 GROUP BY ClaimLawsuitID) et ON cl.ClaimLawsuitID = et.ClaimLawsuitID
		/*LEFT JOIN Final_Claim_NF_Date f				ON f.clm_key = c.clm_key*/
		LEFT JOIN (select * 
				   from tblClaimDisease 
				   where PrimaryDisease = 1) cd	ON cd.ClaimID = c.ClaimID
		LEFT JOIN tblDisease d					ON d.DiseaseID = cd.DiseaseID
		LEFT JOIN tblHighLevelDisease hd		ON hd.HighLevelDiseaseID = d.HighLevelDiseaseID
		LEFT JOIN tblState s					ON s.StateID = l.StateID
		LEFT JOIN tblJurisdiction j				ON j.JurisdictionID = l.JurisdictionID
		LEFT JOIN tblPlaintiffCounselAddress pa	ON pa.PlaintiffCounselAddressID = l.NationalPlaintiffCounselAddressID
		LEFT JOIN tblClaimantSettlement settle	ON settle.ClaimLawsuitID = cl.ClaimLawsuitID'
	SET	 @Querystring = @QueryString + '		LEFT JOIN (SELECT ClaimantSettlementID, MIN(SourceDate) as SourceDate FROM tblPaymentSettlement pset	
					INNER JOIN tblCheck ch ON pset.CheckID = ch.CheckID 
					GROUP BY ClaimantSettlementID) 	ps	ON ps.ClaimantSettlementID = settle.ClaimantSettlementID
		LEFT JOIN vallegation a					ON a.claimlawsuitid = cl.claimlawsuitid
		LEFT JOIN (SELECT ClaimantPersonalInfoID, 
					  Coalesce(min(Occupation),''Unknown'') AS Occupation, 
					  CASE WHEN min(coalesce(cemp.empstartdate,''1/1/2100''))<=  MIN(coalesce(cprod.startdate,''1/1/2100'')) 
					  THEN min(cemp.empstartdate)
					  ELSE MIN(cprod.startdate) 
					  END AS ExposureStart,
					  CASE WHEN MAX(coalesce(cemp.empenddate, ''1/1/1900'')) >= MAX(coalesce(cprod.enddate,''1/1/1900'')) 
					  THEN MAX(cemp.empenddate)
					  ELSE MAX(cprod.enddate) 
					  END AS ExposureEnd,
					  ProductName
				FROM tblClaimantEmployment cemp
				LEFT JOIN tblClaimantProduct cprod ON cemp.ClaimantEmploymentID = cprod.ClaimantEmploymentID
				LEFT JOIN tblProduct p ON p.ProductID = cprod.ProductID
				LEFT JOIN tblOccupation o ON cemp.OccupationID = o.OccupationID
				LEFT JOIN tblProductName pn ON pn.ProductNameID = p.ProductNameID
				WHERE IsPrimary = 1 --AND PrimaryProduct = 1 
				GROUP BY ClaimantPersonalInfoID, ProductName) cp ON cp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID' + 
		@JoinString 
		+
		
	 @WhereString + '
	 GROUP BY cpi.claimantpersonalinfoid, 
	lastname,
	firstname,
	SocialSecurityNumber, 
	birthdate, 
	deceaseddate, 
	c.claimid,
	HighLevelDisease,
	docketnumber, 
	s.state, 
	jurisdiction,
	dca.defensecounsel, 
	l.LeadPlaintiffFirstName+'' ''+l.LeadPlaintiffLastName,plaintiffcounsel, 
	cl.ClaimLawsuitID, 
	IsPrimaryForClaim, 
	claimsequence, 
	claimlawsuitstatus, 
	StatusDate, 
	et.EventDate,
	Coalesce(FileDate, a.receiveddate), 
	filedate,
	a.ServiceDate, 
	ProcessedDate, 
	YEAR(coalesce(filedate, a.receiveddate)), 
	settlementdate, 
	settlementamount,
	clst.ClaimLawsuitStatusID, Occupation, ExposureStart, ExposureEnd, SourceDate, ProductName) cpi'
 	 IF @RowCount > 0 BEGIN
SET	 @Querystring = @QueryString + '
	 WHERE RowNum BETWEEN ' + CONVERT(nvarchar, @RowStart) + ' AND ' + CONVERT(nvarchar, @RowStart + @RowCount ) 
	 END
	 --WHERE RowNum BETWEEN ' + CONVERT(nvarchar, @RowStart) + ' AND ' + CONVERT(nvarchar, @RowStart + @RowCount ) + '
SET	 @Querystring= @Querystring +	 
	 ' ORDER BY ' + CASE WHEN @OrderByString LIKE '%s.State%' THEN 'STATE' 
						 WHEN @OrderByString LIKE '%c.ClaimID%' THEN 'CLAIMID' 
						 WHEN @OrderByString LIKE '%cast(a.ServiceDate as date) desc%' THEN 'cast(ServiceDate as date) desc'
						 WHEN @OrderByString LIKE '%cast(a.ServiceDate as date) asc' THEN 'cast(ServiceDate as date)' 
						 ELSE @OrderByString END + ' 
	 OPTION (RECOMPILE)
	 SET ROWCOUNT 0
	
'
Print Substring(@QueryString, 1, 4000)
Print Substring(@QueryString, 4001, 4000)
Print Substring(@QueryString, 8001, 4000)
--Finally, the query created above is executed.
--SELECT @QueryString
EXEC sp_executesql @QueryString--, N'@ClaimantNameParam nvarchar(max), @SSNParam nvarchar(max), @DocketNumberParam nvarchar(max)', @ClaimantNameParam = @ClaimantNames, @SSNParam = @SSNs, @DocketNumberParam = @DocketNumbers


END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_CLAIM_RECENT_qryClaimReports] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_CLAIM_RECENT_qryClaimReports] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_CLAIM_RECENT_qryClaimReports] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_CLAIM_RECENT_qryClaimReports] TO [power_user]
GO
