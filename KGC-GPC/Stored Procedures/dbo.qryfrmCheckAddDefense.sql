SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmCheckAddDefense] (@DefenseInvoiceID int, @CheckID int, @PaymentAmount money)
As
Set NoCount On

--Declare @DefenseInvoiceID int
--Declare @CheckID int
--Declare @paymentAmount money
--Set @DefenseInvoiceID = 2
--Set @CheckID = 2
--Set @PaymentAmount = 10000

Declare @CurrentFirmID int
Set @CurrentFirmID = (select DefenseCounselAddressID from tblDefenseInvoice where DefenseInvoiceID = @DefenseInvoiceID)
Declare @AcceptableFirmID int
Set @AcceptableFirmID = (Select Coalesce(DefenseCounselAddressID,@CurrentFirmID) From tblPaymentDefense pd Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID Where CheckID = @CheckID Group By Coalesce(DefenseCounselAddressID,@CurrentFirmID))
Declare @CurrentMatterID int
Set @CurrentMatterID = (Select MatterID From tblDefenseInvoice where DefenseInvoiceID = @DefenseInvoiceID)
Declare @AcceptableMatterID int
Set @AcceptableMatterID = (Select Coalesce(MatterID,@CurrentMatterID) From tblPaymentDefense pd Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID Where CheckID = @CheckID Group By Coalesce(MatterID,@CurrentMatterID))

Declare @SettlementCheck int
Set @SettlementCheck = (Select COUNT(*) from tblPaymentSettlement where CheckID = @CheckID)

Declare @ApprovedCheck int
Set @ApprovedCheck = (Select Case when ca.CheckID is not null Then 1 Else 0 End From tblCheck c Left Join tblCheckApproval ca on c.CheckID = ca.CheckID where c.CheckID = @CheckID)

Declare @InvoiceAlreadyAdded int
Set @InvoiceAlreadyAdded = Coalesce((Select COUNT(*) from tblPaymentDefense where CheckID = @CheckID and DefenseInvoiceID = @DefenseInvoiceID),0)

If ((@AcceptableFirmID = @CurrentFirmID AND @CurrentMatterID = @AcceptableMatterID AND @ApprovedCheck = 0) Or (@AcceptableFirmID is Null)) AND @SettlementCheck = 0 AND @InvoiceAlreadyAdded = 0 Begin
	Insert Into tblPaymentDefense (DefenseInvoiceID, CheckID, PaymentAmount)
	Select @DefenseInvoiceID, @CheckID, @PaymentAmount
End


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddDefense] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddDefense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddDefense] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddDefense] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddDefense] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddDefense] TO [power_user]
GO
