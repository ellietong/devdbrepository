SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qryTotalActivePending] 
	-- Add the parameters for the stored procedure here
(@MatterID int, @EndDate datetime)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--DECLARE @matterID int
--SET @MatterID = null
    -- Insert statements for procedure here
DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter WITH (READCOMMITTED))

--Declare @EndDate datetime = '3/5/2012'
--Declare @MatterID int = NULL

select COUNT(distinct cl.ClaimLawsuitID) As [Count]
from tblClaim c WITH (READCOMMITTED)
inner join tblClaimLawsuit cl WITH (READCOMMITTED) ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l WITH (READCOMMITTED) ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst WITH (READCOMMITTED) ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblPlaintiffCounselAddress pca WITH (READCOMMITTED) ON pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
join (select distinct ClaimLawsuitID, MaterialID from tblAllegation where materialID not in (11,12))a on a.ClaimLawsuitID = cl.ClaimLawsuitID
Where ClaimLawsuitStatusID in (22) and c.ClaimantPersonalInfoID <>13357
and  IsPrimaryStatus=1 
and a.MaterialID not  in (11,12)
--and ProcessedDate <= COALESCE(@EndDate,GETDATE())
order by COUNT(distinct cl.ClaimLawsuitID) desc


END





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTotalActivePending] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTotalActivePending] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTotalActivePending] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTotalActivePending] TO [power_user]
GO
