SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_REPORTS_MONTHLY_qrySettlementsByDiseaseByMonth] (@MatterID int = NULL, 
                                                                       @MonthID  int, 
                                                                       @YearID   int) 
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

DECLARE @Settlement TABLE (HighLevelDisease nvarchar(25), Count int, Settlement money, AverageIndemnity money)
INSERT INTO @Settlement
	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
	                                              ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS Count, 
		SUM(SettlementAmount) AS IndemnityPaid, 
		SUM(SettlementAmount) / COUNT(DISTINCT(cl.ClaimLawsuitID)) AS AverageIndemnity
	FROM tblClaim c 
		inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
		inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
		inner join tblClaimDisease cd on c.ClaimID=cd.ClaimID
		inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
		left join tblDisease d on d.DiseaseID=cd.DiseaseID
		left join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
		left join tblHighLevelDisease hld on hld.HighLevelDiseaseID=d.HighLevelDiseaseID or hld.HighLevelDiseaseID=da.HighLevelDiseaseID
	WHERE  MONTH(COALESCE(SettlementDate,ProcessedDate)) = @month 
		and YEAR(COALESCE(SettlementDate,ProcessedDate)) = @year 
		AND ClaimLawsuitStatusID in (23,24,25,28) 
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
		AND cd.PrimaryDisease = 1
		AND MatterID >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID

SELECT hld.HighLevelDisease, 
	COALESCE(COUNT,0) AS 'Count', 
	COALESCE(Settlement,0) AS IndemnityPaid, 
	COALESCE(AverageIndemnity,0) AS AverageIndemnity
FROM tblHighLevelDisease hld
	LEFT JOIN @Settlement s ON hld.HighLevelDisease=s.HighLevelDisease
WHERE HighLevelDiseaseID != 6
ORDER BY hld.HighLevelDiseaseID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qrySettlementsByDiseaseByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qrySettlementsByDiseaseByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qrySettlementsByDiseaseByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qrySettlementsByDiseaseByMonth] TO [power_user]
GO
