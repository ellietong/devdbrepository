IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'spuser')
CREATE LOGIN [spuser] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [spuser] FOR LOGIN [spuser]
GO
