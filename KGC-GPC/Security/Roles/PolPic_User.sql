CREATE ROLE [PolPic_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'PolPic_User', N'base_user'
GO
EXEC sp_addrolemember N'PolPic_User', N'eastlacka'
GO
EXEC sp_addrolemember N'PolPic_User', N'hughesr'
GO
