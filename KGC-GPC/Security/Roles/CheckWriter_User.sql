CREATE ROLE [CheckWriter_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'CheckWriter_User', N'casillaw'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'colek'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'eastlacka'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'hughesr'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'husainm'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'KCICLLC\kennedyl'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'madlp'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'mollardj'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'zongc'
GO
