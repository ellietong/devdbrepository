CREATE TABLE [dbo].[tblPreviousDefenseAdjustments]
(
[BillMonth] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurFile#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalCounsel ] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date] [datetime] NULL,
[TotalCosts] [float] NULL,
[TotalFees] [float] NULL,
[FirstState%] [float] NULL,
[Royal%] [float] NULL,
[Sentry%] [float] NULL,
[Trav%] [float] NULL,
[Wausau%] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousDefenseAdjustments] TO [base_user]
GRANT SELECT ON  [dbo].[tblPreviousDefenseAdjustments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousDefenseAdjustments] TO [power_user]
GRANT SELECT ON  [dbo].[tblPreviousDefenseAdjustments] TO [power_user]
GRANT INSERT ON  [dbo].[tblPreviousDefenseAdjustments] TO [power_user]
GRANT DELETE ON  [dbo].[tblPreviousDefenseAdjustments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPreviousDefenseAdjustments] TO [power_user]
GO
