CREATE TABLE [dbo].[tblLetterInvoiceDetail]
(
[BillingDate] [datetime] NULL,
[PaymentDueDate] [datetime] NULL,
[BillingDateLetterhead] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingPeriod] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierContact] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Carrier] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Indemnity] [float] NULL,
[Defense] [float] NULL,
[NCC] [float] NULL,
[TotalDefense] [float] NULL,
[CarrierPercentage] [float] NULL,
[ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceEntryID] [int] NOT NULL IDENTITY(1, 1),
[PastDueAmount] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLetterInvoiceDetail] TO [base_user]
GRANT SELECT ON  [dbo].[tblLetterInvoiceDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLetterInvoiceDetail] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblLetterInvoiceDetail] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblLetterInvoiceDetail] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblLetterInvoiceDetail] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblLetterInvoiceDetail] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLetterInvoiceDetail] TO [power_user]
GRANT SELECT ON  [dbo].[tblLetterInvoiceDetail] TO [power_user]
GRANT INSERT ON  [dbo].[tblLetterInvoiceDetail] TO [power_user]
GRANT DELETE ON  [dbo].[tblLetterInvoiceDetail] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLetterInvoiceDetail] TO [power_user]
GO
