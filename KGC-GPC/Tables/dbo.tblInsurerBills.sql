CREATE TABLE [dbo].[tblInsurerBills]
(
[Bill Period] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillPeriodID] [int] NULL,
[CarrierName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierID] [int] NULL,
[Indemnity] [money] NULL,
[Defense] [money] NULL,
[NCC] [money] NULL,
[TotalDefense] [money] NULL,
[InsurerBillID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblInsurerBills] TO [base_user]
GRANT SELECT ON  [dbo].[tblInsurerBills] TO [base_user]
GRANT ALTER ON  [dbo].[tblInsurerBills] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblInsurerBills] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblInsurerBills] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblInsurerBills] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblInsurerBills] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblInsurerBills] TO [Billing_User]
GO
