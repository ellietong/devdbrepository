CREATE TABLE [dbo].[tblPayment]
(
[BillPeriodAmount] [float] NULL,
[PaymentDate] [datetime] NULL,
[Note] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentID] [int] NOT NULL IDENTITY(1, 1),
[LedgerID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPayment] ADD CONSTRAINT [PK_tblPayment] PRIMARY KEY CLUSTERED  ([PaymentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPayment] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblPayment] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblPayment] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblPayment] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblPayment] TO [Claims_User]
GO
