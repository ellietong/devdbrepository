CREATE TABLE [dbo].[TblDisease_MAP]
(
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiseaseID] [bigint] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TblDisease_MAP] TO [base_user]
GRANT SELECT ON  [dbo].[TblDisease_MAP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TblDisease_MAP] TO [power_user]
GRANT SELECT ON  [dbo].[TblDisease_MAP] TO [power_user]
GRANT INSERT ON  [dbo].[TblDisease_MAP] TO [power_user]
GRANT DELETE ON  [dbo].[TblDisease_MAP] TO [power_user]
GRANT UPDATE ON  [dbo].[TblDisease_MAP] TO [power_user]
GO
