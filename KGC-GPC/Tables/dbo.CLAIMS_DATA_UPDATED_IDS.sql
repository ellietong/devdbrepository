CREATE TABLE [dbo].[CLAIMS_DATA_UPDATED_IDS]
(
[INJ_PRTY_ID] [int] NULL,
[ClaimantPersonalInfoID] [int] NULL,
[ClaimantRelatedPartyID] [int] NULL,
[ClaimantEmploymentID] [int] NULL,
[ClaimID] [int] NULL,
[ClaimDiseaseID] [int] NULL,
[ClaimLawsuitID] [int] NULL,
[ClaimLawsuitStatusTrackingID] [int] NULL,
[ClaimantSettlementID] [int] NULL,
[AllegationID] [int] NULL,
[LawsuitID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_DATA_UPDATED_IDS] TO [power_user]
GRANT SELECT ON  [dbo].[CLAIMS_DATA_UPDATED_IDS] TO [power_user]
GRANT INSERT ON  [dbo].[CLAIMS_DATA_UPDATED_IDS] TO [power_user]
GRANT DELETE ON  [dbo].[CLAIMS_DATA_UPDATED_IDS] TO [power_user]
GRANT UPDATE ON  [dbo].[CLAIMS_DATA_UPDATED_IDS] TO [power_user]
GO
