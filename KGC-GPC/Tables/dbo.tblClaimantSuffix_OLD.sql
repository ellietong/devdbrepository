CREATE TABLE [dbo].[tblClaimantSuffix_OLD]
(
[SuffixID] [int] NOT NULL IDENTITY(1, 1),
[Suffix] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSuffix_OLD] ADD CONSTRAINT [PK_mBtblClaimantSuffix] PRIMARY KEY CLUSTERED  ([SuffixID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSuffix_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSuffix_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSuffix_OLD] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantSuffix_OLD] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantSuffix_OLD] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSuffix_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantSuffix_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantSuffix_OLD] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantSuffix_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantSuffix_OLD] TO [power_user]
GO
