CREATE TABLE [dbo].[tblReportStoredProc]
(
[ReportStoredProcID] [int] NOT NULL IDENTITY(1, 1),
[ReportID] [int] NULL,
[StoredProcID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblReportStoredProc] ADD CONSTRAINT [PK_TblReportStoredProc] PRIMARY KEY CLUSTERED  ([ReportStoredProcID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblReportStoredProc] ADD CONSTRAINT [FK_tblReportStoredProc_tblStoredProc] FOREIGN KEY ([StoredProcID]) REFERENCES [dbo].[tblStoredProc] ([StoredProcID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblReportStoredProc] TO [base_user]
GRANT SELECT ON  [dbo].[tblReportStoredProc] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblReportStoredProc] TO [power_user]
GRANT SELECT ON  [dbo].[tblReportStoredProc] TO [power_user]
GRANT INSERT ON  [dbo].[tblReportStoredProc] TO [power_user]
GRANT DELETE ON  [dbo].[tblReportStoredProc] TO [power_user]
GRANT UPDATE ON  [dbo].[tblReportStoredProc] TO [power_user]
GO
