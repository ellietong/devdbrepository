CREATE TABLE [dbo].[tblPreviousDefenseBills]
(
[OurFile#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date] [datetime] NULL,
[BillingDate] [datetime] NULL,
[TotalCosts] [float] NULL,
[GPCFirstStateCosts] [float] NULL,
[GPCRoyalCosts] [float] NULL,
[GPCSentryCosts] [float] NULL,
[GPCTravelersCosts] [float] NULL,
[GPCWausauCosts] [float] NULL,
[GPC1978-79 (Harbor)Costs] [float] NULL,
[ColyearFirstStateCosts] [float] NULL,
[ColyearRoyalCosts] [float] NULL,
[ColyearSentryCosts] [float] NULL,
[ColyearTravelersCosts] [float] NULL,
[ColyearWausauCosts] [float] NULL,
[Colyear1978-79 (Harbor)Costs] [float] NULL,
[TotalFees] [float] NULL,
[GPCFirstStateFees] [float] NULL,
[GPCRoyalFees] [float] NULL,
[GPCSentryFees] [float] NULL,
[GPCTravelersFees] [float] NULL,
[GPCWausauFees] [float] NULL,
[GPC1978-79 (Harbor)Fees] [float] NULL,
[ColyearFirstStateFees] [float] NULL,
[ColyearRoyalFees] [float] NULL,
[ColyearSentryFees] [float] NULL,
[ColyearTravelersFees] [float] NULL,
[ColyearWausauFees] [float] NULL,
[Colyear1978-79 (Harbor)Fees] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousDefenseBills] TO [base_user]
GRANT SELECT ON  [dbo].[tblPreviousDefenseBills] TO [base_user]
GRANT ALTER ON  [dbo].[tblPreviousDefenseBills] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousDefenseBills] TO [power_user]
GRANT SELECT ON  [dbo].[tblPreviousDefenseBills] TO [power_user]
GRANT INSERT ON  [dbo].[tblPreviousDefenseBills] TO [power_user]
GRANT DELETE ON  [dbo].[tblPreviousDefenseBills] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPreviousDefenseBills] TO [power_user]
GO
