CREATE TABLE [dbo].[tblTenderBlockPolicy]
(
[TenderBlockPolicyID] [int] NOT NULL IDENTITY(1, 1),
[TenderBlockID] [int] NULL,
[PolicyID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderBlockPolicy] ADD CONSTRAINT [PK_tblTenderBlockPolicy] PRIMARY KEY CLUSTERED  ([TenderBlockPolicyID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBlockPolicy] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderBlockPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBlockPolicy] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderBlockPolicy] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderBlockPolicy] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderBlockPolicy] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderBlockPolicy] TO [power_user]
GO
