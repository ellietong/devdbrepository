CREATE TABLE [dbo].[tblSavedState]
(
[SavedStateSearchID] [bigint] NOT NULL IDENTITY(1, 1),
[StateID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedState] ADD CONSTRAINT [PK_tblSavedStateSearch] PRIMARY KEY CLUSTERED  ([SavedStateSearchID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedState] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedState] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedState] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedState] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedState] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedState] TO [power_user]
GO
