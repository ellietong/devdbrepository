CREATE TABLE [dbo].[Bill_CombinedIndemnity_CNACatchup]
(
[BillMonth] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CaseName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurFile#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateofFiling] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementShareReportDate] [datetime] NULL,
[TotalSettlementAmount] [money] NULL,
[BillingFile#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_CombinedIndemnity_CNACatchup] TO [base_user]
GRANT SELECT ON  [dbo].[Bill_CombinedIndemnity_CNACatchup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Bill_CombinedIndemnity_CNACatchup] TO [power_user]
GRANT SELECT ON  [dbo].[Bill_CombinedIndemnity_CNACatchup] TO [power_user]
GRANT INSERT ON  [dbo].[Bill_CombinedIndemnity_CNACatchup] TO [power_user]
GRANT DELETE ON  [dbo].[Bill_CombinedIndemnity_CNACatchup] TO [power_user]
GRANT UPDATE ON  [dbo].[Bill_CombinedIndemnity_CNACatchup] TO [power_user]
GO
