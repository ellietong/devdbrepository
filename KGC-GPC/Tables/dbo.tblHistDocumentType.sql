CREATE TABLE [dbo].[tblHistDocumentType]
(
[ClaimantDocumentTypeID] [int] NOT NULL,
[ClaimantDocumentType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentGroupID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblHistDocumentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblHistDocumentType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblHistDocumentType] TO [power_user]
GRANT SELECT ON  [dbo].[tblHistDocumentType] TO [power_user]
GRANT INSERT ON  [dbo].[tblHistDocumentType] TO [power_user]
GRANT DELETE ON  [dbo].[tblHistDocumentType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblHistDocumentType] TO [power_user]
GO
