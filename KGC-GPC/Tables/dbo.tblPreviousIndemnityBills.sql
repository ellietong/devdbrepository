CREATE TABLE [dbo].[tblPreviousIndemnityBills]
(
[Plaintiff] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurFile#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementShareReportDate] [datetime] NULL,
[GPCShare] [float] NULL,
[FirstStateShare] [float] NULL,
[RoyalShare] [float] NULL,
[SentryShare] [float] NULL,
[Travelers(Hartford)Share] [float] NULL,
[Travelers(Denver)Share] [float] NULL,
[Travelers(Dallas)Share] [float] NULL,
[Travelers(Baltimore)Share] [float] NULL,
[TravelersTotal] [float] NULL,
[WausauShare] [float] NULL,
[HarborShare] [float] NULL,
[TotalSettlementAmount] [float] NULL,
[BillingDate] [datetime] NULL,
[BillingFile#notused] [nchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingFile#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousIndemnityBills] TO [base_user]
GRANT SELECT ON  [dbo].[tblPreviousIndemnityBills] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousIndemnityBills] TO [power_user]
GRANT SELECT ON  [dbo].[tblPreviousIndemnityBills] TO [power_user]
GRANT INSERT ON  [dbo].[tblPreviousIndemnityBills] TO [power_user]
GRANT DELETE ON  [dbo].[tblPreviousIndemnityBills] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPreviousIndemnityBills] TO [power_user]
GO
