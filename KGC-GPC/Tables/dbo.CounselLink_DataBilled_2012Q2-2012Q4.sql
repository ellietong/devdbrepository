CREATE TABLE [dbo].[CounselLink_DataBilled_2012Q2-2012Q4]
(
[MatterID_CounselLink] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID_Customer] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterTitle] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirmOffice] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[InvoiceID_CounselLink] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceID_LawFirm] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceApprovedDate] [datetime] NULL,
[ChargeNumber] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeTimekeeperName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDate] [datetime] NULL,
[ChargeType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeRate] [money] NULL,
[HoursBilled] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeBilledAmt] [money] NULL,
[ChargeRecommendedAmt] [money] NULL,
[Code] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCC] [bit] NULL,
[AlreadyBilled] [bit] NULL,
[MotionOnly] [bit] NULL,
[ChargeDescription] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoNotBill] [bit] NULL,
[ID] [int] NOT NULL,
[ID2] [int] NOT NULL IDENTITY(1, 1),
[BillPeriod] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_DataBilled_2012Q2-2012Q4] TO [base_user]
GRANT SELECT ON  [dbo].[CounselLink_DataBilled_2012Q2-2012Q4] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_DataBilled_2012Q2-2012Q4] TO [power_user]
GRANT SELECT ON  [dbo].[CounselLink_DataBilled_2012Q2-2012Q4] TO [power_user]
GRANT INSERT ON  [dbo].[CounselLink_DataBilled_2012Q2-2012Q4] TO [power_user]
GRANT DELETE ON  [dbo].[CounselLink_DataBilled_2012Q2-2012Q4] TO [power_user]
GRANT UPDATE ON  [dbo].[CounselLink_DataBilled_2012Q2-2012Q4] TO [power_user]
GO
