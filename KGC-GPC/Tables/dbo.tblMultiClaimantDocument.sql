CREATE TABLE [dbo].[tblMultiClaimantDocument]
(
[MultiClaimantDocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[DocumentID] [bigint] NOT NULL,
[ClaimantPersonalInfoID] [bigint] NOT NULL,
[HistoricalDocumentLink] [bigint] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[ClaimLawsuitID] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRIG_DeleteMultiDoc]
   ON  [dbo].[tblMultiClaimantDocument]
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE tblTenderHistory FROM tblTenderHistory th
    INNER JOIN tblClaim c ON th.ClaimID = c.ClaimID
    INNER JOIN deleted d ON d.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	WHERE ComplaintDocumentID  = d.DocumentID

	
	
END
GO
ALTER TABLE [dbo].[tblMultiClaimantDocument] ADD CONSTRAINT [PK_tblMultiClaimantDocument] PRIMARY KEY CLUSTERED  ([MultiClaimantDocumentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblMultiClaimantDocument_ClaimantPersonalInfoID] ON [dbo].[tblMultiClaimantDocument] ([ClaimantPersonalInfoID]) INCLUDE ([DocumentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblMultiClaimantDocument_DocumentID] ON [dbo].[tblMultiClaimantDocument] ([DocumentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMultiClaimantDocument] TO [base_user]
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [base_user]
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GRANT INSERT ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GRANT DELETE ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMultiClaimantDocument] TO [power_user]
GO
