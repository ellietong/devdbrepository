CREATE TABLE [dbo].[tblPreviousDefenseNCC_Archive2]
(
[InvoiceDate] [datetime] NULL,
[BillingDate] [datetime] NULL,
[Invoice#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost] [float] NULL,
[Fees] [float] NULL,
[Total] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousDefenseNCC_Archive2] TO [base_user]
GRANT SELECT ON  [dbo].[tblPreviousDefenseNCC_Archive2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousDefenseNCC_Archive2] TO [power_user]
GRANT SELECT ON  [dbo].[tblPreviousDefenseNCC_Archive2] TO [power_user]
GRANT INSERT ON  [dbo].[tblPreviousDefenseNCC_Archive2] TO [power_user]
GRANT DELETE ON  [dbo].[tblPreviousDefenseNCC_Archive2] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPreviousDefenseNCC_Archive2] TO [power_user]
GO
