CREATE TABLE [dbo].[import_Notes]
(
[File#] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InternalReferenceNumber] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Other] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[import_Notes] TO [base_user]
GRANT SELECT ON  [dbo].[import_Notes] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[import_Notes] TO [power_user]
GRANT SELECT ON  [dbo].[import_Notes] TO [power_user]
GRANT INSERT ON  [dbo].[import_Notes] TO [power_user]
GRANT DELETE ON  [dbo].[import_Notes] TO [power_user]
GRANT UPDATE ON  [dbo].[import_Notes] TO [power_user]
GO
