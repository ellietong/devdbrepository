CREATE TABLE [dbo].[tblOccurrence_08142013]
(
[OccurrenceID] [bigint] NOT NULL,
[AggregateApplies] [bit] NOT NULL,
[ExpectedPayDate] [datetime] NULL,
[InClaim] [bit] NOT NULL,
[OccurrenceName] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceAddress] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateBilled] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceStateID] [int] NULL,
[OccurrenceGroupID] [int] NULL,
[CorporateHistoryID] [int] NULL,
[OccurrenceTypeID] [int] NULL,
[ClaimID] [bigint] NULL,
[Segment] [int] NULL,
[TriggerStart1] [datetime] NULL,
[TriggerStart1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriggerEnd1] [datetime] NULL,
[TriggerEnd1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriggerStart2] [datetime] NULL,
[TriggerEnd2] [datetime] NULL,
[TriggerStart3] [datetime] NULL,
[TriggerEnd3] [datetime] NULL,
[TriggerStart4] [datetime] NULL,
[TriggerEnd4] [datetime] NULL,
[Cost1] [money] NULL,
[Cost1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost2] [money] NULL,
[Cost2Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost3] [money] NULL,
[Cost3Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost4] [money] NULL,
[Cost5] [money] NULL,
[Cost6] [money] NULL,
[Cost7] [money] NULL,
[Cost8] [money] NULL,
[Cost9] [money] NULL,
[Cost10] [money] NULL,
[Cost11] [money] NULL,
[Cost12] [money] NULL,
[InternalReferenceNumber] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsColyear] [bit] NULL,
[GPCAcquisitionDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrence_08142013] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccurrence_08142013] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccurrence_08142013] TO [power_user]
GRANT DELETE ON  [dbo].[tblOccurrence_08142013] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccurrence_08142013] TO [power_user]
GO
