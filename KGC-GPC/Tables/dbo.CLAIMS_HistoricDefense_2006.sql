CREATE TABLE [dbo].[CLAIMS_HistoricDefense_2006]
(
[DefenseFirm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingDate] [datetime] NULL,
[Fees] [money] NULL,
[Expenses] [money] NULL,
[Total] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_HistoricDefense_2006] TO [base_user]
GRANT SELECT ON  [dbo].[CLAIMS_HistoricDefense_2006] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_HistoricDefense_2006] TO [power_user]
GRANT SELECT ON  [dbo].[CLAIMS_HistoricDefense_2006] TO [power_user]
GRANT INSERT ON  [dbo].[CLAIMS_HistoricDefense_2006] TO [power_user]
GRANT DELETE ON  [dbo].[CLAIMS_HistoricDefense_2006] TO [power_user]
GRANT UPDATE ON  [dbo].[CLAIMS_HistoricDefense_2006] TO [power_user]
GO
