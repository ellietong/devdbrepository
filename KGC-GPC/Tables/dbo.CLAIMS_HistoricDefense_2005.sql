CREATE TABLE [dbo].[CLAIMS_HistoricDefense_2005]
(
[DefenseFirm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingMonth] [datetime] NULL,
[Fees] [money] NULL,
[Expenses] [money] NULL,
[Total] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_HistoricDefense_2005] TO [base_user]
GRANT SELECT ON  [dbo].[CLAIMS_HistoricDefense_2005] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_HistoricDefense_2005] TO [power_user]
GRANT SELECT ON  [dbo].[CLAIMS_HistoricDefense_2005] TO [power_user]
GRANT INSERT ON  [dbo].[CLAIMS_HistoricDefense_2005] TO [power_user]
GRANT DELETE ON  [dbo].[CLAIMS_HistoricDefense_2005] TO [power_user]
GRANT UPDATE ON  [dbo].[CLAIMS_HistoricDefense_2005] TO [power_user]
GO
