CREATE TABLE [dbo].[tblDiscountProvision]
(
[DiscountProvisionID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionID] [int] NULL,
[ProvisionValueID] [int] NULL,
[ProvisionIDProvisionValueID] [int] NULL,
[ProvisionDiscount] [money] NULL,
[discountid] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountProvision] ADD CONSTRAINT [PK_tblDiscountProvision] PRIMARY KEY CLUSTERED  ([DiscountProvisionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountProvision] ADD CONSTRAINT [FK_tblDiscountProvision_tblProvisionValue] FOREIGN KEY ([ProvisionValueID]) REFERENCES [dbo].[tblProvisionValue] ([ProvisionValueID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDiscountProvision] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiscountProvision] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDiscountProvision] TO [power_user]
GRANT SELECT ON  [dbo].[tblDiscountProvision] TO [power_user]
GRANT INSERT ON  [dbo].[tblDiscountProvision] TO [power_user]
GRANT DELETE ON  [dbo].[tblDiscountProvision] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDiscountProvision] TO [power_user]
GO
