CREATE TABLE [dbo].[Claims_import_Q3]
(
[Prefix] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[File#] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InternalReferenceNumber] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLast] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PFirst] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SS#] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Executor] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Decedent] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoB] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkSite] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FiledDate] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Judge] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Docket#] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCounsel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BBACounsel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BBASued] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoD] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrialDate] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Smoke] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Conditions] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Trade] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deposed] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Interrogatory] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Probable DC] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Defense Costs] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settlement Amount] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settle Date] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Years of Exposure] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[1st Date of Exposure] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPCAcquisitionDate] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Other] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Claims_import_Q3] TO [base_user]
GRANT SELECT ON  [dbo].[Claims_import_Q3] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Claims_import_Q3] TO [power_user]
GRANT SELECT ON  [dbo].[Claims_import_Q3] TO [power_user]
GRANT INSERT ON  [dbo].[Claims_import_Q3] TO [power_user]
GRANT DELETE ON  [dbo].[Claims_import_Q3] TO [power_user]
GRANT UPDATE ON  [dbo].[Claims_import_Q3] TO [power_user]
GO
