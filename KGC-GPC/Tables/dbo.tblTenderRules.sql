CREATE TABLE [dbo].[tblTenderRules]
(
[TenderRuleID] [int] NOT NULL IDENTITY(1, 1),
[StartDate] [date] NULL,
[EndDate] [date] NULL,
[AllowAsbestos] [bit] NULL,
[TenderBlockID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderRules] ADD CONSTRAINT [PK_tblTenderRules] PRIMARY KEY CLUSTERED  ([TenderRuleID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderRules] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderRules] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderRules] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderRules] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderRules] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderRules] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderRules] TO [power_user]
GO
