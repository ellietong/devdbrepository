CREATE TABLE [dbo].[tblClaimDocuments]
(
[ClaimDocumentID] [int] NOT NULL,
[ClaimID] [int] NULL,
[DocumentID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimDocuments] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimDocuments] TO [power_user]
GO
