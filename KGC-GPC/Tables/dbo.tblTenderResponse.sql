CREATE TABLE [dbo].[tblTenderResponse]
(
[TenderResponseID] [bigint] NOT NULL IDENTITY(1, 1),
[DocumentID] [int] NULL,
[TenderContactID] [int] NULL,
[TenderHistoryID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderResponse] ADD CONSTRAINT [PK_tblTenderResponse] PRIMARY KEY CLUSTERED  ([TenderResponseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderResponse] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderResponse] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderResponse] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderResponse] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderResponse] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderResponse] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderResponse] TO [power_user]
GO
