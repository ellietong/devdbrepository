CREATE TABLE [dbo].[CounselLink_Data2]
(
[MatterID_CounselLink] [float] NULL,
[MatterID_Customer] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirmOffice] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[InvoiceID_CounselLink] [float] NULL,
[InvoiceID_LawFirm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeNumber] [float] NULL,
[ChargeTimekeeperName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDescription] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDate] [datetime] NULL,
[ChargeType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeBilledAmt] [money] NULL,
[ChargeRecommendedAmt] [money] NULL,
[CODE] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoNotBill] [bit] NULL,
[UniqueID] [int] NOT NULL IDENTITY(1, 1),
[UPDATE_ID] [int] NULL,
[InvoiceApprovedDate] [date] NULL,
[MotionOnly] [bit] NULL,
[NCC] [bit] NULL,
[AlreadyBilled] [bit] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data2] TO [base_user]
GRANT SELECT ON  [dbo].[CounselLink_Data2] TO [base_user]
GRANT SELECT ON  [dbo].[CounselLink_Data2] TO [Claims_User]
GRANT INSERT ON  [dbo].[CounselLink_Data2] TO [Claims_User]
GRANT UPDATE ON  [dbo].[CounselLink_Data2] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data2] TO [power_user]
GRANT SELECT ON  [dbo].[CounselLink_Data2] TO [power_user]
GRANT INSERT ON  [dbo].[CounselLink_Data2] TO [power_user]
GRANT DELETE ON  [dbo].[CounselLink_Data2] TO [power_user]
GRANT UPDATE ON  [dbo].[CounselLink_Data2] TO [power_user]
GO
