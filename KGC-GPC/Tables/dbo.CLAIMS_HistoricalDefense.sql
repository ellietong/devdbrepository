CREATE TABLE [dbo].[CLAIMS_HistoricalDefense]
(
[Prefix] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[File#] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date_Clean] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Firm] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice#] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLast] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PFirst] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Costs] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fees] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InternalReferenceNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalCosts] [money] NULL,
[TotalFees] [money] NULL,
[Date_Converted] [date] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_HistoricalDefense] TO [base_user]
GRANT SELECT ON  [dbo].[CLAIMS_HistoricalDefense] TO [Billing_User]
GRANT ALTER ON  [dbo].[CLAIMS_HistoricalDefense] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_HistoricalDefense] TO [power_user]
GRANT SELECT ON  [dbo].[CLAIMS_HistoricalDefense] TO [power_user]
GRANT INSERT ON  [dbo].[CLAIMS_HistoricalDefense] TO [power_user]
GRANT DELETE ON  [dbo].[CLAIMS_HistoricalDefense] TO [power_user]
GRANT UPDATE ON  [dbo].[CLAIMS_HistoricalDefense] TO [power_user]
GO
