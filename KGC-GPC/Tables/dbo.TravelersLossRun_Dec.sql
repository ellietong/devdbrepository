CREATE TABLE [dbo].[TravelersLossRun_Dec]
(
[PY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LINE ] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAIM NO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAIMANT NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOSS PAID] [money] NULL,
[EXP PAID] [money] NULL,
[ALLEGATIONS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TravelersLossRun_Dec] TO [base_user]
GRANT SELECT ON  [dbo].[TravelersLossRun_Dec] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TravelersLossRun_Dec] TO [power_user]
GRANT SELECT ON  [dbo].[TravelersLossRun_Dec] TO [power_user]
GRANT INSERT ON  [dbo].[TravelersLossRun_Dec] TO [power_user]
GRANT DELETE ON  [dbo].[TravelersLossRun_Dec] TO [power_user]
GRANT UPDATE ON  [dbo].[TravelersLossRun_Dec] TO [power_user]
GO
