CREATE TABLE [dbo].[CLAIMS_HistoricalIndemnity]
(
[Plaintiff Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date of Check Request to GPC] [datetime] NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Docket No#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Our File] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [money] NULL,
[BillingFileNo] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_HistoricalIndemnity] TO [base_user]
GRANT SELECT ON  [dbo].[CLAIMS_HistoricalIndemnity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_HistoricalIndemnity] TO [power_user]
GRANT SELECT ON  [dbo].[CLAIMS_HistoricalIndemnity] TO [power_user]
GRANT INSERT ON  [dbo].[CLAIMS_HistoricalIndemnity] TO [power_user]
GRANT DELETE ON  [dbo].[CLAIMS_HistoricalIndemnity] TO [power_user]
GRANT UPDATE ON  [dbo].[CLAIMS_HistoricalIndemnity] TO [power_user]
GO
