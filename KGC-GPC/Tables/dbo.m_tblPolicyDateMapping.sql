CREATE TABLE [dbo].[m_tblPolicyDateMapping]
(
[CarrierName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyDateID] [float] NULL,
[Portion] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblPolicyDateMapping] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPolicyDateMapping] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPolicyDateMapping] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblPolicyDateMapping] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblPolicyDateMapping] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblPolicyDateMapping] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblPolicyDateMapping] TO [power_user]
GO
