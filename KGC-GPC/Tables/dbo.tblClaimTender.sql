CREATE TABLE [dbo].[tblClaimTender]
(
[ClaimTenderID] [int] NOT NULL,
[ClaimID] [int] NULL,
[TenderCarrierID] [int] NULL,
[ShareofTender] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimTender] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimTender] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimTender] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimTender] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimTender] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimTender] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimTender] TO [power_user]
GO
