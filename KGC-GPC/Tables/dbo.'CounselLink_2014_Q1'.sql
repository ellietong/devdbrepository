CREATE TABLE [dbo].['CounselLink_2014_Q1']
(
[MatterID(CounselLink)] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID(Customer)] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirmOffice] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [date] NULL,
[InvoiceID(CounselLink)] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceID(Law Firm)] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceApprovedDate] [date] NULL,
[ChargeNumber] [int] NULL,
[ChargeTimekeeperName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDate] [date] NULL,
[ChargeType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeEffectiveRate] [decimal] (38, 0) NULL,
[ChargeRate] [decimal] (38, 0) NULL,
[HoursBilled] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeBilledAmt] [decimal] (38, 0) NULL,
[ChargeRecommendedAmt] [decimal] (38, 0) NULL,
[ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
