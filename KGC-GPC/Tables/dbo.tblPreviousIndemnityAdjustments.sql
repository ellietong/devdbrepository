CREATE TABLE [dbo].[tblPreviousIndemnityAdjustments]
(
[BillMonth] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CaseName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurFile#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateofFiling] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementShareReportDate] [datetime] NULL,
[TotalSettlementAmount] [float] NULL,
[FirstStateShare] [float] NULL,
[RoyalShare] [float] NULL,
[SentryShare] [float] NULL,
[Travelers(Hartford)Share] [float] NULL,
[Travelers(Denver)Share] [float] NULL,
[Travelers(Dallas)Share] [float] NULL,
[Travelers(Baltimore)Share] [float] NULL,
[WausauShare] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousIndemnityAdjustments] TO [base_user]
GRANT SELECT ON  [dbo].[tblPreviousIndemnityAdjustments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousIndemnityAdjustments] TO [power_user]
GRANT SELECT ON  [dbo].[tblPreviousIndemnityAdjustments] TO [power_user]
GRANT INSERT ON  [dbo].[tblPreviousIndemnityAdjustments] TO [power_user]
GRANT DELETE ON  [dbo].[tblPreviousIndemnityAdjustments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPreviousIndemnityAdjustments] TO [power_user]
GO
