CREATE TABLE [dbo].[m_tblClaimantPersonalInfo_Lookup]
(
[LookupID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NOT NULL,
[InternalReferenceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GO
