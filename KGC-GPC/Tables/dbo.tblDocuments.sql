CREATE TABLE [dbo].[tblDocuments]
(
[DocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantDocumentTypeID] [int] NOT NULL,
[DocumentSourceID] [int] NULL,
[HistoricalDocumentID] [bigint] NULL,
[ComplaintID] [bigint] NULL,
[MatterID] [int] NULL,
[DocumentTitle] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AmazonKey] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NOT NULL,
[CreatedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateModified] [datetime] NOT NULL,
[ModifiedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Accepted] [bit] NULL,
[DateAccepted] [datetime] NULL,
[AcceptedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsHistorical] [bit] NULL,
[IsLessThanTenDefendantCompanies] [bit] NULL,
[DismissalForQC] [bit] NULL,
[DismissalQCIssue] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDocuments] ADD CONSTRAINT [PK_tblDocuments_GPC] PRIMARY KEY CLUSTERED  ([DocumentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[tblDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblDocuments] TO [power_user]
GRANT DELETE ON  [dbo].[tblDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [power_user]
GO
