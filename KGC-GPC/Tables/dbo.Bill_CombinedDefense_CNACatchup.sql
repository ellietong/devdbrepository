CREATE TABLE [dbo].[Bill_CombinedDefense_CNACatchup]
(
[BillMonth] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurFile#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date] [datetime] NULL,
[TotalCosts] [money] NULL,
[TotalFees] [money] NULL,
[OurFile#_AsBilled] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_CombinedDefense_CNACatchup] TO [base_user]
GRANT SELECT ON  [dbo].[Bill_CombinedDefense_CNACatchup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Bill_CombinedDefense_CNACatchup] TO [power_user]
GRANT SELECT ON  [dbo].[Bill_CombinedDefense_CNACatchup] TO [power_user]
GRANT INSERT ON  [dbo].[Bill_CombinedDefense_CNACatchup] TO [power_user]
GRANT DELETE ON  [dbo].[Bill_CombinedDefense_CNACatchup] TO [power_user]
GRANT UPDATE ON  [dbo].[Bill_CombinedDefense_CNACatchup] TO [power_user]
GO
