CREATE TABLE [dbo].[CLAIMS_2012Q4_Settlements]
(
[PlaintiffName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckRequestDate] [datetime] NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocketNo] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurFile#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [float] NULL,
[BillingFIle#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gender] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_2012Q4_Settlements] TO [base_user]
GRANT SELECT ON  [dbo].[CLAIMS_2012Q4_Settlements] TO [base_user]
GRANT ALTER ON  [dbo].[CLAIMS_2012Q4_Settlements] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_2012Q4_Settlements] TO [power_user]
GRANT SELECT ON  [dbo].[CLAIMS_2012Q4_Settlements] TO [power_user]
GRANT INSERT ON  [dbo].[CLAIMS_2012Q4_Settlements] TO [power_user]
GRANT DELETE ON  [dbo].[CLAIMS_2012Q4_Settlements] TO [power_user]
GRANT UPDATE ON  [dbo].[CLAIMS_2012Q4_Settlements] TO [power_user]
GO
