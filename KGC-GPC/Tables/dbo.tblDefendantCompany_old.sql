CREATE TABLE [dbo].[tblDefendantCompany_old]
(
[DefendantCompanyID] [int] NOT NULL IDENTITY(1, 1),
[DefendantCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefendantCompany_old] ADD CONSTRAINT [PK_mBtblDefendantCompany] PRIMARY KEY CLUSTERED  ([DefendantCompanyID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompany_old] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefendantCompany_old] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefendantCompany_old] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefendantCompany_old] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefendantCompany_old] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefendantCompany_old] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblDefendantCompany_old] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblDefendantCompany_old] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblDefendantCompany_old] TO [Notice_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompany_old] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefendantCompany_old] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefendantCompany_old] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefendantCompany_old] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefendantCompany_old] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompany_old', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompany_old', 'COLUMN', N'DefendantCompany'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompany_old', 'COLUMN', N'DefendantCompany'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompany_old', 'COLUMN', N'DefendantCompany'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompany_old', 'COLUMN', N'DefendantCompanyID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompany_old', 'COLUMN', N'DefendantCompanyID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompany_old', 'COLUMN', N'DefendantCompanyID'
GO
