CREATE TABLE [dbo].[tblCounty]
(
[CountyID] [int] NOT NULL IDENTITY(1, 1),
[County] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[PriorOwner] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bill] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCounty] ADD CONSTRAINT [PK_tblCounty] PRIMARY KEY CLUSTERED  ([CountyID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCounty] TO [base_user]
GRANT SELECT ON  [dbo].[tblCounty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCounty] TO [power_user]
GRANT SELECT ON  [dbo].[tblCounty] TO [power_user]
GRANT INSERT ON  [dbo].[tblCounty] TO [power_user]
GRANT DELETE ON  [dbo].[tblCounty] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCounty] TO [power_user]
GO
