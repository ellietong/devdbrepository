CREATE TABLE [dbo].[tblPreviousDefenseNCC_Archive]
(
[InvoiceDate] [datetime] NULL,
[Invoice#] [float] NULL,
[Cost] [float] NULL,
[Fees] [float] NULL,
[Total] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousDefenseNCC_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblPreviousDefenseNCC_Archive] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousDefenseNCC_Archive] TO [power_user]
GRANT SELECT ON  [dbo].[tblPreviousDefenseNCC_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblPreviousDefenseNCC_Archive] TO [power_user]
GRANT DELETE ON  [dbo].[tblPreviousDefenseNCC_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPreviousDefenseNCC_Archive] TO [power_user]
GO
