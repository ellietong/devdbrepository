CREATE TABLE [dbo].[tblStatus]
(
[StatusID] [int] NOT NULL IDENTITY(1, 1),
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblStatus] ADD CONSTRAINT [PK_mEtblStatus] PRIMARY KEY CLUSTERED  ([StatusID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblStatus] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblStatus] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblStatus] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblStatus] TO [Enviro_User]
GRANT SELECT ON  [dbo].[tblStatus] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblStatus] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblStatus] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblStatus] TO [Notice_User]
GRANT VIEW DEFINITION ON  [dbo].[tblStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblStatus] TO [power_user]
GO
