CREATE TABLE [dbo].[RileyBillingPercentages]
(
[File#] [float] NULL,
[Colyear] [float] NULL,
[GPC] [float] NULL,
[No Insurance] [float] NULL,
[CNA] [float] NULL,
[Corel] [float] NULL,
[TotalYearsOfExposure] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[RileyBillingPercentages] TO [base_user]
GRANT SELECT ON  [dbo].[RileyBillingPercentages] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[RileyBillingPercentages] TO [power_user]
GRANT SELECT ON  [dbo].[RileyBillingPercentages] TO [power_user]
GRANT INSERT ON  [dbo].[RileyBillingPercentages] TO [power_user]
GRANT DELETE ON  [dbo].[RileyBillingPercentages] TO [power_user]
GRANT UPDATE ON  [dbo].[RileyBillingPercentages] TO [power_user]
GO
