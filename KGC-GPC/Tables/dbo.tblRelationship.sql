CREATE TABLE [dbo].[tblRelationship]
(
[RelationshipID] [int] NOT NULL IDENTITY(1, 1),
[Relationship] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRelationship] ADD CONSTRAINT [PK_mBtblRelationship] PRIMARY KEY CLUSTERED  ([RelationshipID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblRelationship] TO [base_user]
GRANT SELECT ON  [dbo].[tblRelationship] TO [base_user]
GRANT SELECT ON  [dbo].[tblRelationship] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblRelationship] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblRelationship] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblRelationship] TO [power_user]
GRANT SELECT ON  [dbo].[tblRelationship] TO [power_user]
GRANT INSERT ON  [dbo].[tblRelationship] TO [power_user]
GRANT DELETE ON  [dbo].[tblRelationship] TO [power_user]
GRANT UPDATE ON  [dbo].[tblRelationship] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship', 'COLUMN', N'Relationship'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship', 'COLUMN', N'Relationship'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship', 'COLUMN', N'Relationship'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship', 'COLUMN', N'RelationshipID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship', 'COLUMN', N'RelationshipID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship', 'COLUMN', N'RelationshipID'
GO
