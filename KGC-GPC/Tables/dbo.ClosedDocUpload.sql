CREATE TABLE [dbo].[ClosedDocUpload]
(
[ClosingDocsID] [float] NULL,
[Path] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalFileName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentCategory] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KCICClaimantID] [float] NULL,
[ClaimantDocumentTypeID] [float] NULL
) ON [PRIMARY]
GO
