CREATE TABLE [dbo].[tblPlaintiffCounselAddress_Local]
(
[PlaintiffCounselAddressID] [int] NOT NULL IDENTITY(1, 1),
[PlaintiffCounselID] [float] NULL,
[PlaintiffCounselAddress1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselAddress2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselCity] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselStateID] [float] NULL,
[PlaintiffCounselZip] [float] NULL,
[PlaintiffCounselMailZip] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselPhone] [float] NULL,
[PlaintiffCounselExtension] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPlaintiffCounselAddress_Local] ADD CONSTRAINT [PK_tblPlaintiffCounselAddress_Local] PRIMARY KEY CLUSTERED  ([PlaintiffCounselAddressID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [power_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [power_user]
GRANT INSERT ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [power_user]
GRANT DELETE ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPlaintiffCounselAddress_Local] TO [power_user]
GO
