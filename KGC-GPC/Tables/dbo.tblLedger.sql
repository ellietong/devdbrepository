CREATE TABLE [dbo].[tblLedger]
(
[CarrierID] [int] NOT NULL,
[Debit] [money] NULL,
[LedgerID] [int] NOT NULL IDENTITY(1, 1),
[Credit] [money] NULL,
[ExhaustionDateID] [bigint] NULL,
[LedgerNote] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppliedToBalance] [bit] NULL,
[SubscriberID] [bigint] NULL,
[ledgertypeID] [int] NULL,
[LedgerDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLedger] ADD CONSTRAINT [PK_tblLedger] PRIMARY KEY CLUSTERED  ([LedgerID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLedger] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblLedger] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblLedger] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblLedger] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblLedger] TO [Claims_User]
GO
