CREATE TABLE [dbo].[tblMiscCashActivity]
(
[MiscCashActivityID] [bigint] NOT NULL IDENTITY(1, 1),
[CashActivityTypeID] [int] NULL,
[CashActivityAmount] [money] NULL,
[CashActivityNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CashActivityDate] [datetime] NULL,
[MatterID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMiscCashActivity] ADD CONSTRAINT [PK_tblMiscCashActivity] PRIMARY KEY CLUSTERED  ([MiscCashActivityID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivity] TO [base_user]
GRANT SELECT ON  [dbo].[tblMiscCashActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivity] TO [CheckApproval_User]
GRANT SELECT ON  [dbo].[tblMiscCashActivity] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivity] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[tblMiscCashActivity] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivity] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblMiscCashActivity] TO [CheckWriter_User]
GRANT INSERT ON  [dbo].[tblMiscCashActivity] TO [CheckWriter_User]
GRANT UPDATE ON  [dbo].[tblMiscCashActivity] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivity] TO [power_user]
GRANT SELECT ON  [dbo].[tblMiscCashActivity] TO [power_user]
GRANT INSERT ON  [dbo].[tblMiscCashActivity] TO [power_user]
GRANT DELETE ON  [dbo].[tblMiscCashActivity] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMiscCashActivity] TO [power_user]
GO
