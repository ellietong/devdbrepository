CREATE TABLE [dbo].[tblExternalFiles]
(
[ExternalFileID] [int] NOT NULL IDENTITY(1, 1),
[ExternalFileTypeID] [int] NOT NULL,
[ExternalFileName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExternalFileLocation] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExternalFiles] ADD CONSTRAINT [PK_tblExternalFiles] PRIMARY KEY CLUSTERED  ([ExternalFileID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExternalFiles] TO [base_user]
GRANT SELECT ON  [dbo].[tblExternalFiles] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblExternalFiles] TO [power_user]
GRANT SELECT ON  [dbo].[tblExternalFiles] TO [power_user]
GRANT INSERT ON  [dbo].[tblExternalFiles] TO [power_user]
GRANT DELETE ON  [dbo].[tblExternalFiles] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExternalFiles] TO [power_user]
GRANT SELECT ON  [dbo].[tblExternalFiles] TO [Reports_User]
GRANT INSERT ON  [dbo].[tblExternalFiles] TO [Reports_User]
GRANT DELETE ON  [dbo].[tblExternalFiles] TO [Reports_User]
GRANT UPDATE ON  [dbo].[tblExternalFiles] TO [Reports_User]
GO
