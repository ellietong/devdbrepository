CREATE TABLE [dbo].[tblPolicy_07-09-2013]
(
[PolicyID] [int] NOT NULL,
[MinPStartDate] [datetime] NULL,
[CarrierID] [int] NOT NULL,
[NamedInsuredID] [int] NULL,
[InsuranceProgramID] [int] NULL,
[PolicyNum] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PolicyNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyReviewed] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyPDF] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitialDataEntry] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitialDataEntryDate] [datetime] NULL,
[PolicyReview] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyReviewDate] [datetime] NULL,
[PolPicPDFLink] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoNotReport] [bit] NOT NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicy_07-09-2013] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicy_07-09-2013] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicy_07-09-2013] TO [power_user]
GRANT DELETE ON  [dbo].[tblPolicy_07-09-2013] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicy_07-09-2013] TO [power_user]
GO
