CREATE TABLE [dbo].[tblGPCEntityAcquisition]
(
[GPCEntityID] [int] NOT NULL IDENTITY(1, 1),
[PriorOwner] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPCAcquisitionYear] [int] NULL,
[Jurisdiction] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsColyear] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblGPCEntityAcquisition] ADD CONSTRAINT [PK_tblGPCEntityAcquisition] PRIMARY KEY CLUSTERED  ([GPCEntityID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblGPCEntityAcquisition] TO [base_user]
GRANT SELECT ON  [dbo].[tblGPCEntityAcquisition] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblGPCEntityAcquisition] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblGPCEntityAcquisition] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblGPCEntityAcquisition] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblGPCEntityAcquisition] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblGPCEntityAcquisition] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblGPCEntityAcquisition] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblGPCEntityAcquisition] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblGPCEntityAcquisition] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblGPCEntityAcquisition] TO [power_user]
GRANT SELECT ON  [dbo].[tblGPCEntityAcquisition] TO [power_user]
GRANT INSERT ON  [dbo].[tblGPCEntityAcquisition] TO [power_user]
GRANT DELETE ON  [dbo].[tblGPCEntityAcquisition] TO [power_user]
GRANT UPDATE ON  [dbo].[tblGPCEntityAcquisition] TO [power_user]
GO
