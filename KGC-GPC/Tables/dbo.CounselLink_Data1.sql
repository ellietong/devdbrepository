CREATE TABLE [dbo].[CounselLink_Data1]
(
[ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID_CounselLink] [float] NULL,
[MatterID_Customer] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterTitle] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirmOffice] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [date] NULL,
[InvoiceID_CounselLink] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceID_LawFirm] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeTimekeeperName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeNumber] [float] NULL,
[ChargeDescription] [varchar] (1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDate] [date] NULL,
[ChargeType] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Metrics] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeBilledAmt] [money] NULL,
[ChargeRecommendedAmt] [money] NULL,
[ClientCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_ID] [int] NULL,
[CODE] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoNotBill] [bit] NULL,
[InvoiceApprovedDate] [date] NULL,
[AlreadyBilled] [bit] NULL,
[MotionOnly] [bit] NULL,
[NCC] [bit] NULL,
[DoNotBillSentry] [bit] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data1] TO [base_user]
GRANT SELECT ON  [dbo].[CounselLink_Data1] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data1] TO [power_user]
GRANT SELECT ON  [dbo].[CounselLink_Data1] TO [power_user]
GRANT INSERT ON  [dbo].[CounselLink_Data1] TO [power_user]
GRANT DELETE ON  [dbo].[CounselLink_Data1] TO [power_user]
GRANT UPDATE ON  [dbo].[CounselLink_Data1] TO [power_user]
GO
