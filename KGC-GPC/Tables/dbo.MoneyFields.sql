CREATE TABLE [dbo].[MoneyFields]
(
[id] [float] NULL,
[billed] [float] NULL,
[chargebilledamt] [money] NULL,
[Recommended] [float] NULL,
[ChargeRecommendedAmt] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[MoneyFields] TO [base_user]
GRANT SELECT ON  [dbo].[MoneyFields] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[MoneyFields] TO [power_user]
GRANT SELECT ON  [dbo].[MoneyFields] TO [power_user]
GRANT INSERT ON  [dbo].[MoneyFields] TO [power_user]
GRANT DELETE ON  [dbo].[MoneyFields] TO [power_user]
GRANT UPDATE ON  [dbo].[MoneyFields] TO [power_user]
GO
