CREATE TABLE [dbo].[tblClaimantCoverageLine]
(
[ClaimantCoverageLineID] [int] NULL,
[ClaimantPersonalInfoID] [int] NULL,
[ExposureStart] [date] NULL,
[GPCEntityID] [int] NULL,
[FullCoverage] [bit] NULL,
[First Exposure - Entity
First Exposure - Entity] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimantCoverageLine] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantCoverageLine] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantCoverageLine] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantCoverageLine] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantCoverageLine] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantCoverageLine] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantCoverageLine] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantCoverageLine] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantCoverageLine] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantCoverageLine] TO [power_user]
GO
