CREATE TABLE [dbo].[IMPORT]
(
[MatterID_CounselLink] [float] NULL,
[LawFirmOffice] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[InvoiceID_CounselLink] [float] NULL,
[InvoiceID_LawFirm] [float] NULL,
[ChargeTimekeeperName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeNumber] [float] NULL,
[ChargeDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDate] [datetime] NULL,
[ChargeType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeBilledAmt] [money] NULL,
[InvoiceApprovedDate] [datetime] NULL,
[CODEupdate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[IMPORT] TO [base_user]
GRANT SELECT ON  [dbo].[IMPORT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[IMPORT] TO [power_user]
GRANT SELECT ON  [dbo].[IMPORT] TO [power_user]
GRANT INSERT ON  [dbo].[IMPORT] TO [power_user]
GRANT DELETE ON  [dbo].[IMPORT] TO [power_user]
GRANT UPDATE ON  [dbo].[IMPORT] TO [power_user]
GO
