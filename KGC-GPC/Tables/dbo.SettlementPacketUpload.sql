CREATE TABLE [dbo].[SettlementPacketUpload]
(
[Path] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalDocumentName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantID] [float] NULL,
[SettlementPAcketID] [float] NULL
) ON [PRIMARY]
GO
