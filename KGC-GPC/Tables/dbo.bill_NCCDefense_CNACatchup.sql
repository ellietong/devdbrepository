CREATE TABLE [dbo].[bill_NCCDefense_CNACatchup]
(
[Date] [datetime] NULL,
[Invoice#] [float] NULL,
[Cost] [float] NULL,
[Fees] [float] NULL,
[BillMonth] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[bill_NCCDefense_CNACatchup] TO [base_user]
GRANT SELECT ON  [dbo].[bill_NCCDefense_CNACatchup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[bill_NCCDefense_CNACatchup] TO [power_user]
GRANT SELECT ON  [dbo].[bill_NCCDefense_CNACatchup] TO [power_user]
GRANT INSERT ON  [dbo].[bill_NCCDefense_CNACatchup] TO [power_user]
GRANT DELETE ON  [dbo].[bill_NCCDefense_CNACatchup] TO [power_user]
GRANT UPDATE ON  [dbo].[bill_NCCDefense_CNACatchup] TO [power_user]
GO
