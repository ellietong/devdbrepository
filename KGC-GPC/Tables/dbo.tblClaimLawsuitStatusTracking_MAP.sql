CREATE TABLE [dbo].[tblClaimLawsuitStatusTracking_MAP]
(
[Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitStatusID] [bigint] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking_MAP] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking_MAP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking_MAP] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking_MAP] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking_MAP] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatusTracking_MAP] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTracking_MAP] TO [power_user]
GO
