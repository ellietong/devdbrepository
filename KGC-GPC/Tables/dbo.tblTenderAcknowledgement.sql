CREATE TABLE [dbo].[tblTenderAcknowledgement]
(
[TenderAckID] [bigint] NOT NULL IDENTITY(1, 1),
[TenderHistoryID] [bigint] NULL,
[AcknowledgementDate] [datetime] NULL,
[Hash] [char] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenderContactID] [int] NULL,
[Acknowledged] [bit] NOT NULL CONSTRAINT [DF_tblTenderAcknowledgement_Acknowledged] DEFAULT ((0)),
[DateNotified] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderAcknowledgement] ADD CONSTRAINT [PK_tblTenderAcknowledgement] PRIMARY KEY CLUSTERED  ([TenderAckID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderAcknowledgement] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderAcknowledgement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderAcknowledgement] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderAcknowledgement] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderAcknowledgement] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderAcknowledgement] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderAcknowledgement] TO [power_user]
GO
