CREATE TABLE [dbo].[tblOccurrence_import]
(
[OccurrenceID] [float] NULL,
[AggregateApplies] [float] NULL,
[ExpectedPayDate] [datetime] NULL,
[InClaim] [float] NULL,
[OccurrenceName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceGroupID] [float] NULL,
[OccurrenceTypeID] [float] NULL,
[ClaimID] [float] NULL,
[TriggerStart1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriggerStart1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost1] [float] NULL,
[Cost3] [float] NULL,
[IsColyear] [float] NULL,
[GPCAcquisitionDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateBilled] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrence_import] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccurrence_import] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccurrence_import] TO [power_user]
GRANT DELETE ON  [dbo].[tblOccurrence_import] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccurrence_import] TO [power_user]
GO
