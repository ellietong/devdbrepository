CREATE TABLE [dbo].[tblPolicyLimits_07-09-2013]
(
[LimitID] [int] NOT NULL,
[PolicyID] [int] NULL,
[LimitTypeID] [int] NULL,
[AttachmentPoint] [money] NULL,
[PerOccLimit] [money] NULL,
[LayerPerOccLimit] [money] NULL,
[SIR] [money] NULL,
[SIRAggregateLimit] [money] NULL,
[AggregateLimit] [money] NULL,
[PreviouslyExhaustedAggregate] [money] NULL,
[Layer] [tinyint] NULL,
[LimitNote] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyLimits_07-09-2013] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicyLimits_07-09-2013] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicyLimits_07-09-2013] TO [power_user]
GRANT DELETE ON  [dbo].[tblPolicyLimits_07-09-2013] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicyLimits_07-09-2013] TO [power_user]
GO
