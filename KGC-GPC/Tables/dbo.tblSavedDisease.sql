CREATE TABLE [dbo].[tblSavedDisease]
(
[SavedDiseaseID] [bigint] NOT NULL IDENTITY(1, 1),
[DiseaseID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedDisease] ADD CONSTRAINT [PK_tblSavedDiseaseSearch_Claims] PRIMARY KEY CLUSTERED  ([SavedDiseaseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedDisease] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedDisease] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedDisease] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedDisease] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedDisease] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedDisease] TO [power_user]
GO
