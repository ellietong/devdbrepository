CREATE TABLE [dbo].[tblInsurerPayments]
(
[CarrierID] [int] NULL,
[BillPeriodID] [int] NULL,
[BillPeriodAmount] [float] NULL,
[PaymentDate] [datetime] NULL,
[FullPaymentAmount] [float] NULL,
[PaymentType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Note] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsurerPaymentID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblInsurerPayments] TO [base_user]
GRANT SELECT ON  [dbo].[tblInsurerPayments] TO [base_user]
GRANT ALTER ON  [dbo].[tblInsurerPayments] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblInsurerPayments] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblInsurerPayments] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblInsurerPayments] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblInsurerPayments] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblInsurerPayments] TO [Billing_User]
GO
