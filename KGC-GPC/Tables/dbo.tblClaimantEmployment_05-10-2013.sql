CREATE TABLE [dbo].[tblClaimantEmployment_05-10-2013]
(
[ClaimantEmploymentID] [bigint] NOT NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[OccupationID] [int] NULL,
[Jobsite] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmpStartDate] [datetime] NULL,
[EmpEndDate] [datetime] NULL,
[EarliestExposureDate] [datetime] NULL,
[LatestExposureDate] [datetime] NULL,
[IsPrimary] [bit] NULL,
[JobSiteCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobSiteStateID] [int] NULL,
[IsSecondaryExposure] [bit] NULL,
[SPUsername] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EarliestExposureDate_TenderDOFE] [datetime] NULL,
[EarliestExposureDate_MSA] [datetime] NULL,
[EarliestExposureDate_NCC] [datetime] NULL,
[EarliestExposureDate_KIPSDOFU] [datetime] NULL,
[Occupation_OLD] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
