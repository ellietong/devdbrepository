CREATE TABLE [dbo].[tblDamage_OLD]
(
[DamageID] [int] NOT NULL IDENTITY(1, 1),
[Damage] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDamage_OLD] ADD CONSTRAINT [PK_mBtblDamage] PRIMARY KEY CLUSTERED  ([DamageID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDamage_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblDamage_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblDamage_OLD] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDamage_OLD] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDamage_OLD] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDamage_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblDamage_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblDamage_OLD] TO [power_user]
GRANT DELETE ON  [dbo].[tblDamage_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDamage_OLD] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDamage_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage_OLD', 'COLUMN', N'Damage'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage_OLD', 'COLUMN', N'Damage'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage_OLD', 'COLUMN', N'Damage'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage_OLD', 'COLUMN', N'DamageID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage_OLD', 'COLUMN', N'DamageID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage_OLD', 'COLUMN', N'DamageID'
GO
