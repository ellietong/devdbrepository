CREATE TABLE [dbo].[tblMatter]
(
[MatterID] [int] NOT NULL IDENTITY(1, 1),
[Matter] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMatter] ADD CONSTRAINT [PK_tblMatter] PRIMARY KEY CLUSTERED  ([MatterID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMatter] TO [base_user]
GRANT SELECT ON  [dbo].[tblMatter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMatter] TO [power_user]
GRANT SELECT ON  [dbo].[tblMatter] TO [power_user]
GRANT INSERT ON  [dbo].[tblMatter] TO [power_user]
GRANT DELETE ON  [dbo].[tblMatter] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMatter] TO [power_user]
GO
