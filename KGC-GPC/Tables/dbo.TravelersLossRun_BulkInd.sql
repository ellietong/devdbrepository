CREATE TABLE [dbo].[TravelersLossRun_BulkInd]
(
[Plaintiff] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPC FILE NO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concatenated Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Middle Initial] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settlement Amount] [float] NULL,
[Travelers 17% Share] [float] NULL,
[Payment Range] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reimbursement Date] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TravelersLossRun_BulkInd] TO [base_user]
GRANT SELECT ON  [dbo].[TravelersLossRun_BulkInd] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TravelersLossRun_BulkInd] TO [power_user]
GRANT SELECT ON  [dbo].[TravelersLossRun_BulkInd] TO [power_user]
GRANT INSERT ON  [dbo].[TravelersLossRun_BulkInd] TO [power_user]
GRANT DELETE ON  [dbo].[TravelersLossRun_BulkInd] TO [power_user]
GRANT UPDATE ON  [dbo].[TravelersLossRun_BulkInd] TO [power_user]
GO
