CREATE TABLE [dbo].[tblBillYear]
(
[YearID] [int] NOT NULL IDENTITY(1, 1),
[BillYear] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillYear] ADD CONSTRAINT [PK_mtblBillYear] PRIMARY KEY CLUSTERED  ([YearID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBillYear] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillYear] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillYear] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBillYear] TO [power_user]
GRANT SELECT ON  [dbo].[tblBillYear] TO [power_user]
GRANT INSERT ON  [dbo].[tblBillYear] TO [power_user]
GRANT DELETE ON  [dbo].[tblBillYear] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBillYear] TO [power_user]
GO
