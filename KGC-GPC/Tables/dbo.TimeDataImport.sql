CREATE TABLE [dbo].[TimeDataImport]
(
[MatterID_CounselLink] [float] NULL,
[MatterID_Customer] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirmOffice] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[InvoiceID_CounselLink] [float] NULL,
[InvoiceID_LawFirm] [float] NULL,
[ChargeNumber] [float] NULL,
[ChargeTimekeeperName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDescription] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDate] [datetime] NULL,
[ChargeType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Metrics] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeBilledAmt] [money] NULL,
[ChargeRecommendedAmt] [money] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TimeDataImport] TO [base_user]
GRANT SELECT ON  [dbo].[TimeDataImport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TimeDataImport] TO [power_user]
GRANT SELECT ON  [dbo].[TimeDataImport] TO [power_user]
GRANT INSERT ON  [dbo].[TimeDataImport] TO [power_user]
GRANT DELETE ON  [dbo].[TimeDataImport] TO [power_user]
GRANT UPDATE ON  [dbo].[TimeDataImport] TO [power_user]
GO
