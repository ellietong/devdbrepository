CREATE TABLE [dbo].[claims_import]
(
[file#] [float] NULL,
[gpcacquisitiondate] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[claims_import] TO [base_user]
GRANT SELECT ON  [dbo].[claims_import] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[claims_import] TO [power_user]
GRANT SELECT ON  [dbo].[claims_import] TO [power_user]
GRANT INSERT ON  [dbo].[claims_import] TO [power_user]
GRANT DELETE ON  [dbo].[claims_import] TO [power_user]
GRANT UPDATE ON  [dbo].[claims_import] TO [power_user]
GO
