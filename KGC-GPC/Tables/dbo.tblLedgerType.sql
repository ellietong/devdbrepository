CREATE TABLE [dbo].[tblLedgerType]
(
[LedgerTypeID] [int] NOT NULL IDENTITY(1, 1),
[LedgerType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLedgerType] ADD CONSTRAINT [PK_tblLedgerType] PRIMARY KEY NONCLUSTERED  ([LedgerTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLedgerType] TO [base_user]
GRANT SELECT ON  [dbo].[tblLedgerType] TO [base_user]
GRANT SELECT ON  [dbo].[tblLedgerType] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblLedgerType] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblLedgerType] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblLedgerType] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblLedgerType] TO [power_user]
GRANT SELECT ON  [dbo].[tblLedgerType] TO [power_user]
GRANT INSERT ON  [dbo].[tblLedgerType] TO [power_user]
GRANT DELETE ON  [dbo].[tblLedgerType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLedgerType] TO [power_user]
GO
