CREATE TABLE [dbo].[CounselLink_ApprovedInvoices_Q32012_CSV]
(
[ID] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID_CounselLink] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID_Customer] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterTitle] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirmOffice] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceID_CounselLink] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceID_LawFirm] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceApprovedDate] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeNumber] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeTimekeeperName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDescription] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDate] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeType] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeRate] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoursBilled] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeBilledAmt] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeRecommendedAmt] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_ApprovedInvoices_Q32012_CSV] TO [base_user]
GRANT SELECT ON  [dbo].[CounselLink_ApprovedInvoices_Q32012_CSV] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_ApprovedInvoices_Q32012_CSV] TO [power_user]
GRANT SELECT ON  [dbo].[CounselLink_ApprovedInvoices_Q32012_CSV] TO [power_user]
GRANT INSERT ON  [dbo].[CounselLink_ApprovedInvoices_Q32012_CSV] TO [power_user]
GRANT DELETE ON  [dbo].[CounselLink_ApprovedInvoices_Q32012_CSV] TO [power_user]
GRANT UPDATE ON  [dbo].[CounselLink_ApprovedInvoices_Q32012_CSV] TO [power_user]
GO
