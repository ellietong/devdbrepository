CREATE TABLE [dbo].[claims_data_updated2]
(
[Prefix] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[File#] [float] NULL,
[InternalReferenceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLast] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PFirst] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SS#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Executor] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Decedent] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoB] [datetime] NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkSite] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FiledDate] [datetime] NULL,
[Judge] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Docket#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BBACounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BBASued] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrialDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Smoke] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Conditions] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Trade] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deposed] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Interrogatory] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProbableDC] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCosts] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementAmount] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettleDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExposureYears] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstExposure] [datetime] NULL,
[Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT ALTER ON  [dbo].[claims_data_updated2] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[claims_data_updated2] TO [power_user]
GRANT SELECT ON  [dbo].[claims_data_updated2] TO [power_user]
GRANT INSERT ON  [dbo].[claims_data_updated2] TO [power_user]
GRANT DELETE ON  [dbo].[claims_data_updated2] TO [power_user]
GRANT UPDATE ON  [dbo].[claims_data_updated2] TO [power_user]
GO
