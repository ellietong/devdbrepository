CREATE TABLE [dbo].[tblClaimantDocuments]
(
[ClaimantDocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[DocumentNumber] [bigint] NULL,
[ClaimantDocumentTypeID] [int] NULL,
[Folder] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentLink] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTimeEntered] [datetime] NULL,
[SPDocID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTitle] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentType] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantDocuments] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantDocuments] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantDocuments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantDocuments] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantDocuments] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantDocuments] TO [power_user]
GO
