CREATE TABLE [dbo].[CounselLink_Data_2013Q4]
(
[MatterID(CounselLink)] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID(Customer)] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterTitle] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirmOffice] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[InvoiceID(CounselLink)] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceID(LawFirm)] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceApprovedDate] [datetime] NULL,
[ChargeNumber] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeTimekeeperName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDate] [datetime] NULL,
[ChargeType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeEffectiveRate] [money] NULL,
[ChargeRate] [money] NULL,
[HoursBilled] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeBilledAmt] [money] NULL,
[ChargeRecommendedAmt] [money] NULL,
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[ChargeDescription] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[1] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[2] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[3] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[4] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[5] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[6] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[7] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[8] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[9] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[10] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCC] [bit] NULL,
[AlreadyBilled] [bit] NULL,
[MotionOnly] [bit] NULL,
[Adjustment] [bit] NULL,
[DoNotBill] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data_2013Q4] TO [base_user]
GRANT SELECT ON  [dbo].[CounselLink_Data_2013Q4] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data_2013Q4] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[CounselLink_Data_2013Q4] TO [CheckQC_User]
GRANT INSERT ON  [dbo].[CounselLink_Data_2013Q4] TO [CheckQC_User]
GRANT UPDATE ON  [dbo].[CounselLink_Data_2013Q4] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data_2013Q4] TO [power_user]
GRANT SELECT ON  [dbo].[CounselLink_Data_2013Q4] TO [power_user]
GRANT INSERT ON  [dbo].[CounselLink_Data_2013Q4] TO [power_user]
GRANT UPDATE ON  [dbo].[CounselLink_Data_2013Q4] TO [power_user]
GO
