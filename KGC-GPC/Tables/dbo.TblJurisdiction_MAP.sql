CREATE TABLE [dbo].[TblJurisdiction_MAP]
(
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [bigint] NULL,
[JurisdictionID] [bigint] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TblJurisdiction_MAP] TO [base_user]
GRANT SELECT ON  [dbo].[TblJurisdiction_MAP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TblJurisdiction_MAP] TO [power_user]
GRANT SELECT ON  [dbo].[TblJurisdiction_MAP] TO [power_user]
GRANT INSERT ON  [dbo].[TblJurisdiction_MAP] TO [power_user]
GRANT DELETE ON  [dbo].[TblJurisdiction_MAP] TO [power_user]
GRANT UPDATE ON  [dbo].[TblJurisdiction_MAP] TO [power_user]
GO
