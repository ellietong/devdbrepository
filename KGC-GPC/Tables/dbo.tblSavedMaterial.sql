CREATE TABLE [dbo].[tblSavedMaterial]
(
[SavedMaterialID] [bigint] NOT NULL IDENTITY(1, 1),
[MaterialID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedMaterial] ADD CONSTRAINT [PK_tblSavedMaterial] PRIMARY KEY CLUSTERED  ([SavedMaterialID]) ON [PRIMARY]
GO
