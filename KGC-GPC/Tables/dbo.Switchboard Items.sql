CREATE TABLE [dbo].[Switchboard Items]
(
[SwitchboardID] [int] NOT NULL,
[ItemNumber] [smallint] NULL,
[ItemText] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Command] [smallint] NULL,
[Argument] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[m_Filter] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Switchboard Items] TO [base_user]
GRANT SELECT ON  [dbo].[Switchboard Items] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Switchboard Items] TO [power_user]
GRANT SELECT ON  [dbo].[Switchboard Items] TO [power_user]
GRANT INSERT ON  [dbo].[Switchboard Items] TO [power_user]
GRANT DELETE ON  [dbo].[Switchboard Items] TO [power_user]
GRANT UPDATE ON  [dbo].[Switchboard Items] TO [power_user]
GRANT SELECT ON  [dbo].[Switchboard Items] TO [Reports_User]
GRANT INSERT ON  [dbo].[Switchboard Items] TO [Reports_User]
GRANT UPDATE ON  [dbo].[Switchboard Items] TO [Reports_User]
GO
