CREATE TABLE [dbo].[Import_map_activecounsel]
(
[DefenseJurisdictionID] [float] NULL,
[IsActive] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_map_activecounsel] TO [base_user]
GRANT SELECT ON  [dbo].[Import_map_activecounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_map_activecounsel] TO [power_user]
GRANT SELECT ON  [dbo].[Import_map_activecounsel] TO [power_user]
GRANT INSERT ON  [dbo].[Import_map_activecounsel] TO [power_user]
GRANT DELETE ON  [dbo].[Import_map_activecounsel] TO [power_user]
GRANT UPDATE ON  [dbo].[Import_map_activecounsel] TO [power_user]
GO
