CREATE TABLE [dbo].[tblHistoricalMultiClaimantDocuments]
(
[ClaimantDocumentLinkID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[ClaimantDocumentID] [bigint] NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDocumentNumber] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDocumentNameInFolder] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblHistoricalMultiClaimantDocuments] ADD CONSTRAINT [PK_tblHistoricalMultiClaimantDocuments] PRIMARY KEY CLUSTERED  ([ClaimantDocumentLinkID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [power_user]
GRANT DELETE ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [power_user]
GO
