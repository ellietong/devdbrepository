CREATE TABLE [dbo].[tblExternalFileType]
(
[ExternalFileTypeID] [int] NOT NULL IDENTITY(1, 1),
[ExternalFileType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExternalFileType] ADD CONSTRAINT [PK_tblExternalFileType] PRIMARY KEY CLUSTERED  ([ExternalFileTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExternalFileType] TO [base_user]
GRANT SELECT ON  [dbo].[tblExternalFileType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblExternalFileType] TO [power_user]
GRANT SELECT ON  [dbo].[tblExternalFileType] TO [power_user]
GRANT INSERT ON  [dbo].[tblExternalFileType] TO [power_user]
GRANT DELETE ON  [dbo].[tblExternalFileType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExternalFileType] TO [power_user]
GRANT SELECT ON  [dbo].[tblExternalFileType] TO [Reports_User]
GRANT INSERT ON  [dbo].[tblExternalFileType] TO [Reports_User]
GRANT DELETE ON  [dbo].[tblExternalFileType] TO [Reports_User]
GRANT UPDATE ON  [dbo].[tblExternalFileType] TO [Reports_User]
GO
