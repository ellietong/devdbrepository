CREATE TABLE [dbo].[TravelersLossRun_Apr]
(
[TYPE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAIMANT NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRIMARY CLAIM NO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[1982 Primary Indemnity] [float] NULL,
[1983 Primary Indemnity] [float] NULL,
[1983 CUP Indemnity] [float] NULL,
[1984 Primary Indemnity] [float] NULL,
[1985 Primary Indemnity] [float] NULL,
[Total Indemnity Paid] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TravelersLossRun_Apr] TO [base_user]
GRANT SELECT ON  [dbo].[TravelersLossRun_Apr] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TravelersLossRun_Apr] TO [power_user]
GRANT SELECT ON  [dbo].[TravelersLossRun_Apr] TO [power_user]
GRANT INSERT ON  [dbo].[TravelersLossRun_Apr] TO [power_user]
GRANT DELETE ON  [dbo].[TravelersLossRun_Apr] TO [power_user]
GRANT UPDATE ON  [dbo].[TravelersLossRun_Apr] TO [power_user]
GO
