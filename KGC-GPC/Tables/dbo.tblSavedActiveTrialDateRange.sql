CREATE TABLE [dbo].[tblSavedActiveTrialDateRange]
(
[SavedActiveTrialDateID] [int] NOT NULL IDENTITY(1, 1),
[ActiveTrialDateRange] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedActiveTrialDateRange] ADD CONSTRAINT [PK_tblSavedActiveTrialDateRange] PRIMARY KEY CLUSTERED  ([SavedActiveTrialDateID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedActiveTrialDateRange] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedActiveTrialDateRange] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedActiveTrialDateRange] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedActiveTrialDateRange] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedActiveTrialDateRange] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedActiveTrialDateRange] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedActiveTrialDateRange] TO [power_user]
GO
