CREATE TABLE [dbo].[ComplaintUpload]
(
[DocName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Path] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantID] [float] NULL,
[HisDocID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
