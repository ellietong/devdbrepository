CREATE TABLE [dbo].[Import_DOFE]
(
[Claimant Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOFE] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_DOFE] TO [base_user]
GRANT SELECT ON  [dbo].[Import_DOFE] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_DOFE] TO [power_user]
GRANT SELECT ON  [dbo].[Import_DOFE] TO [power_user]
GRANT INSERT ON  [dbo].[Import_DOFE] TO [power_user]
GRANT DELETE ON  [dbo].[Import_DOFE] TO [power_user]
GRANT UPDATE ON  [dbo].[Import_DOFE] TO [power_user]
GO
