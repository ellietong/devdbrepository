CREATE TABLE [dbo].[tblBillPeriod]
(
[BillPeriodID] [int] NOT NULL,
[BillPeriod] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillDueDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBillPeriod] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillPeriod] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillPeriod] TO [Billing_User]
GO
