CREATE TABLE [dbo].[Claims_MotionData]
(
[InternalReferenceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plast] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pfirst] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Executor] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOB] [datetime] NULL,
[Disease] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FiledDate] [datetime] NULL,
[Docket#] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pcounsel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOD] [datetime] NULL,
[SettlementAmount] [float] NULL,
[file#] [bigint] NOT NULL IDENTITY(100000, 1)
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Claims_MotionData] TO [base_user]
GRANT SELECT ON  [dbo].[Claims_MotionData] TO [base_user]
GRANT ALTER ON  [dbo].[Claims_MotionData] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[Claims_MotionData] TO [power_user]
GRANT SELECT ON  [dbo].[Claims_MotionData] TO [power_user]
GRANT INSERT ON  [dbo].[Claims_MotionData] TO [power_user]
GRANT DELETE ON  [dbo].[Claims_MotionData] TO [power_user]
GRANT UPDATE ON  [dbo].[Claims_MotionData] TO [power_user]
GO
