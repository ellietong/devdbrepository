CREATE TABLE [dbo].[tblDefInvClaimantDetail]
(
[InvoiceDetailID] [int] NOT NULL IDENTITY(1, 1),
[DefenseInvoiceID] [int] NULL,
[ClaimID] [int] NULL,
[FirmID] [int] NULL,
[InvoiceNumber] [nchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [date] NULL,
[Fees] [money] NULL,
[Costs] [money] NULL,
[Adjustments] [money] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantPersonalInfoID] [int] NULL,
[DocumentID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefInvClaimantDetail] ADD CONSTRAINT [PK_tblDefInvClaimantDetail] PRIMARY KEY CLUSTERED  ([InvoiceDetailID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [base_user]
GRANT ALTER ON  [dbo].[tblDefInvClaimantDetail] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblDefInvClaimantDetail] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblDefInvClaimantDetail] TO [Billing_User]
GRANT ALTER ON  [dbo].[tblDefInvClaimantDetail] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefInvClaimantDetail] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefInvClaimantDetail] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GO
