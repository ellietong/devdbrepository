CREATE TABLE [dbo].[tblLedger_08-27-2013]
(
[BillPeriodID] [int] NULL,
[CarrierID] [int] NULL,
[Indemnity] [money] NULL,
[Defense] [money] NULL,
[NCC] [money] NULL,
[LedgerID] [int] NOT NULL,
[Credit] [money] NULL,
[ExhaustionDateID] [bigint] NULL,
[LedgerNote] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppliedToBalance] [bit] NULL,
[SubscriberID] [bigint] NULL
) ON [PRIMARY]
GO
GRANT ALTER ON  [dbo].[tblLedger_08-27-2013] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLedger_08-27-2013] TO [power_user]
GRANT SELECT ON  [dbo].[tblLedger_08-27-2013] TO [power_user]
GRANT INSERT ON  [dbo].[tblLedger_08-27-2013] TO [power_user]
GRANT DELETE ON  [dbo].[tblLedger_08-27-2013] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLedger_08-27-2013] TO [power_user]
GO
