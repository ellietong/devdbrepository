CREATE TABLE [dbo].[tblLedger_08232013]
(
[Bill Period] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillPeriodID] [int] NULL,
[CarrierName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierID] [int] NULL,
[Indemnity] [money] NULL,
[Defense] [money] NULL,
[NCC] [money] NULL,
[TotalDefense] [money] NULL,
[LedgerID] [int] NOT NULL
) ON [PRIMARY]
GO
GRANT ALTER ON  [dbo].[tblLedger_08232013] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLedger_08232013] TO [power_user]
GRANT SELECT ON  [dbo].[tblLedger_08232013] TO [power_user]
GRANT INSERT ON  [dbo].[tblLedger_08232013] TO [power_user]
GRANT DELETE ON  [dbo].[tblLedger_08232013] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLedger_08232013] TO [power_user]
GO
