CREATE TABLE [dbo].[tblTenderType]
(
[TenderTypeID] [int] NOT NULL IDENTITY(1, 1),
[TenderType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderType] ADD CONSTRAINT [PK_tblTenderType] PRIMARY KEY CLUSTERED  ([TenderTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderType] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderType] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderType] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderType] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderType] TO [power_user]
GO
