CREATE TABLE [dbo].[tblNCCMaterial]
(
[NCCMaterialID] [int] NOT NULL IDENTITY(1, 1),
[PersonID] [int] NOT NULL,
[MaterialID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblNCCMaterial] ADD CONSTRAINT [PK_tblNCCTortTypeID] PRIMARY KEY CLUSTERED  ([NCCMaterialID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblNCCMaterial] TO [base_user]
GRANT SELECT ON  [dbo].[tblNCCMaterial] TO [base_user]
GRANT SELECT ON  [dbo].[tblNCCMaterial] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblNCCMaterial] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblNCCMaterial] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblNCCMaterial] TO [power_user]
GRANT SELECT ON  [dbo].[tblNCCMaterial] TO [power_user]
GRANT INSERT ON  [dbo].[tblNCCMaterial] TO [power_user]
GRANT DELETE ON  [dbo].[tblNCCMaterial] TO [power_user]
GRANT UPDATE ON  [dbo].[tblNCCMaterial] TO [power_user]
GO
