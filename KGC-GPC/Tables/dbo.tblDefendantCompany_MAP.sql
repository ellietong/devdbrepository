CREATE TABLE [dbo].[tblDefendantCompany_MAP]
(
[DefendantCompany] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefendantCompanyID1] [bigint] NULL,
[DefendantCompanyID2] [bigint] NULL,
[DefendantCompanyID3] [bigint] NULL,
[DefendantCompanyID4] [bigint] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompany_MAP] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefendantCompany_MAP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompany_MAP] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefendantCompany_MAP] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefendantCompany_MAP] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefendantCompany_MAP] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefendantCompany_MAP] TO [power_user]
GO
