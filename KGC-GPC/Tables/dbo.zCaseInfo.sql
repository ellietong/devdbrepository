CREATE TABLE [dbo].[zCaseInfo]
(
[CaseInfoID] [int] NOT NULL IDENTITY(1, 1),
[CaseName] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePathPolicies] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultReportFooterID] [int] NULL,
[FilePathExternalFiles] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePathChecks] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePathClaimantDocs] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePathCorrespondence] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilePathEnviroInvoice] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zCaseInfo] ADD CONSTRAINT [PK_zCaseInfo] PRIMARY KEY NONCLUSTERED  ([CaseInfoID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[zCaseInfo] TO [base_user]
GRANT SELECT ON  [dbo].[zCaseInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zCaseInfo] TO [power_user]
GRANT SELECT ON  [dbo].[zCaseInfo] TO [power_user]
GRANT INSERT ON  [dbo].[zCaseInfo] TO [power_user]
GRANT DELETE ON  [dbo].[zCaseInfo] TO [power_user]
GRANT UPDATE ON  [dbo].[zCaseInfo] TO [power_user]
GRANT SELECT ON  [dbo].[zCaseInfo] TO [Reports_User]
GRANT INSERT ON  [dbo].[zCaseInfo] TO [Reports_User]
GRANT DELETE ON  [dbo].[zCaseInfo] TO [Reports_User]
GRANT UPDATE ON  [dbo].[zCaseInfo] TO [Reports_User]
GO
