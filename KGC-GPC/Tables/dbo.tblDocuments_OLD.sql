CREATE TABLE [dbo].[tblDocuments_OLD]
(
[DocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[DetailDocumentTypeID] [int] NULL,
[LawsuitID] [bigint] NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[Comments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTitle] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPDocId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NULL,
[SettlementGroupID] [bigint] NULL,
[SettlementDate] [datetime] NULL,
[SettlementAmount] [money] NULL,
[InvoiceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatePayable] [datetime] NULL,
[FeeAmount] [money] NULL,
[DateOfFiling] [datetime] NULL,
[DateOfDeath] [datetime] NULL,
[ModifiedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateModified] [datetime] NULL,
[Accepted] [bit] NULL,
[AcceptedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAccepted] [datetime] NULL,
[DisbursementAmount] [money] NULL,
[Adjustment] [money] NULL,
[AdjustmentNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusID] [int] NULL,
[ApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateApproved] [datetime] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[FirmID] [int] NULL,
[DocumentSourceID] [int] NULL,
[VendorFirmID] [int] NULL,
[ClaimLawsuitStatusID] [int] NULL,
[DismissalDate] [date] NULL,
[DismissalProcessDate] [date] NULL,
[MatterID] [int] NULL,
[HistoricalDocumentID] [bigint] NULL,
[RS_BegNo] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_EndNo] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_Privileged] [bit] NULL,
[RS_NCCNumber] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_Claimant] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_MultiClaimants] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_CaseName] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_Category] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_FileName] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RS_Source] [int] NULL,
[rs_HistTenderLtr] [bit] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocuments_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocuments_OLD] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDocuments_OLD] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblDocuments_OLD] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDocuments_OLD] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblDocuments_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblDocuments_OLD] TO [power_user]
GRANT DELETE ON  [dbo].[tblDocuments_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDocuments_OLD] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'DetailDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'DetailDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'DetailDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'SettlementGroupID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'SettlementGroupID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments_OLD', 'COLUMN', N'SettlementGroupID'
GO
