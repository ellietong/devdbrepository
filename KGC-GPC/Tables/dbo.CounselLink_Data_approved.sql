CREATE TABLE [dbo].[CounselLink_Data_approved]
(
[Matter_ID_CounselLink] [float] NULL,
[Matter_ID_Customer] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Matter_Title] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirmOffice] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[InvoiceID_CounselLink] [float] NULL,
[InvoiceID_LawFirm] [float] NULL,
[ApprovedDate] [datetime] NULL,
[ChargeNumber] [float] NULL,
[ChargeTimeKeeperName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDate] [datetime] NULL,
[ChargeType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeBilledAmt] [money] NULL,
[ChargeRecommendedAmt] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data_approved] TO [base_user]
GRANT SELECT ON  [dbo].[CounselLink_Data_approved] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data_approved] TO [power_user]
GRANT SELECT ON  [dbo].[CounselLink_Data_approved] TO [power_user]
GRANT INSERT ON  [dbo].[CounselLink_Data_approved] TO [power_user]
GRANT DELETE ON  [dbo].[CounselLink_Data_approved] TO [power_user]
GRANT UPDATE ON  [dbo].[CounselLink_Data_approved] TO [power_user]
GO
