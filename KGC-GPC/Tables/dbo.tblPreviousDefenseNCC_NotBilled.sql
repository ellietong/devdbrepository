CREATE TABLE [dbo].[tblPreviousDefenseNCC_NotBilled]
(
[BillingDate] [datetime] NULL,
[TotalCosts] [float] NULL,
[TotalFees] [float] NULL,
[FirstStateNCCCosts] [float] NULL,
[RoyalNCCCosts] [float] NULL,
[SentryNCCCosts] [float] NULL,
[TravelersNCCCosts] [float] NULL,
[WausauNCCCosts] [float] NULL,
[FirstStateNCCFees] [float] NULL,
[RoyalNCCFees] [float] NULL,
[SentryNCCFees] [float] NULL,
[TravelersNCCFees] [float] NULL,
[WausauNCCFees] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousDefenseNCC_NotBilled] TO [base_user]
GRANT SELECT ON  [dbo].[tblPreviousDefenseNCC_NotBilled] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousDefenseNCC_NotBilled] TO [power_user]
GRANT SELECT ON  [dbo].[tblPreviousDefenseNCC_NotBilled] TO [power_user]
GRANT INSERT ON  [dbo].[tblPreviousDefenseNCC_NotBilled] TO [power_user]
GRANT DELETE ON  [dbo].[tblPreviousDefenseNCC_NotBilled] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPreviousDefenseNCC_NotBilled] TO [power_user]
GO
