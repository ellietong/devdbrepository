CREATE TABLE [dbo].[tblAllocationPolicyExclude]
(
[PolicyExcludeID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[PolicyID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationPolicyExclude] ADD CONSTRAINT [PK_tblAllocationPolicyExclude] PRIMARY KEY NONCLUSTERED  ([PolicyExcludeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationPolicyExclude] ADD CONSTRAINT [FK_tblAllocationPolicyExclude_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblAllocationPolicyExclude] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationPolicyExclude] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationPolicyExclude] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationPolicyExclude] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationPolicyExclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationPolicyExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationPolicyExclude] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationPolicyExclude] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationPolicyExclude] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationPolicyExclude] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationPolicyExclude] TO [power_user]
GO
