CREATE TABLE [dbo].[CLAIMS_DATA_UPDATED]
(
[Prefix] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[File#] [int] NULL,
[InternalReferenceNumber] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLast] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PFirst] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SS#] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Executor] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Decedent] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoB] [datetime] NULL,
[Disease] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkSite] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FiledDate] [datetime] NULL,
[Judge] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Docket#] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCounsel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BBACounsel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BBASued] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoD] [datetime] NULL,
[TrialDate] [datetime] NULL,
[Smoke] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Conditions] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Trade] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deposed] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Interrogatory] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProbableDC] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCosts] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementAmount] [money] NULL,
[SettleDate] [datetime] NULL,
[ExposureYears] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstExposure] [date] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPCAcquisitiondates_Carrier] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPCAcquisitionDates_Exposure] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes_Exposure] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes_Carrier] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPCAcquisitionDate] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsColyear] [bit] NULL,
[State] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_DATA_UPDATED] TO [base_user]
GRANT SELECT ON  [dbo].[CLAIMS_DATA_UPDATED] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_DATA_UPDATED] TO [power_user]
GRANT SELECT ON  [dbo].[CLAIMS_DATA_UPDATED] TO [power_user]
GRANT INSERT ON  [dbo].[CLAIMS_DATA_UPDATED] TO [power_user]
GRANT DELETE ON  [dbo].[CLAIMS_DATA_UPDATED] TO [power_user]
GRANT UPDATE ON  [dbo].[CLAIMS_DATA_UPDATED] TO [power_user]
GO
