CREATE TABLE [dbo].[claims_data_12-5-2012]
(
[Prefix] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[File#] [int] NULL,
[InternalReferenceNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLast] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PFirst] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SS#] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Executor] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Decedent] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoB] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkSite] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FiledDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Judge] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Docket#] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCounsel] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BBACounsel] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BBASued] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrialDate] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Smoke] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Conditions] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Trade] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deposed] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Interrogatory] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Probable DC] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Defense Costs] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settlement Amount] [money] NULL,
[Settle Date] [date] NULL,
[ExposureYears] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstExposure] [date] NULL,
[Other] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[claims_data_12-5-2012] TO [base_user]
GRANT SELECT ON  [dbo].[claims_data_12-5-2012] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[claims_data_12-5-2012] TO [power_user]
GRANT SELECT ON  [dbo].[claims_data_12-5-2012] TO [power_user]
GRANT INSERT ON  [dbo].[claims_data_12-5-2012] TO [power_user]
GRANT DELETE ON  [dbo].[claims_data_12-5-2012] TO [power_user]
GRANT UPDATE ON  [dbo].[claims_data_12-5-2012] TO [power_user]
GO
