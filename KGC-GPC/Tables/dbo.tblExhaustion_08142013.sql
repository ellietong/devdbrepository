CREATE TABLE [dbo].[tblExhaustion_08142013]
(
[ExhaustionID] [int] NOT NULL,
[ExhaustionDateID] [int] NOT NULL,
[OccurrenceID] [bigint] NULL,
[PolicyDateID] [int] NOT NULL,
[IndemnityAllocation] [float] NOT NULL,
[DefenseAllocation] [float] NOT NULL,
[DefenseOutsideLimitsAllocation] [float] NOT NULL,
[SIRIndemnityAllocation] [float] NOT NULL,
[SIRDefenseAllocation] [float] NOT NULL,
[LedgerID] [int] NULL,
[OutsideConsumption] [float] NULL
) ON [PRIMARY]
GO
