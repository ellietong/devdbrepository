CREATE TABLE [dbo].[tblPolicyDates_07-09-2013]
(
[PolicyDateID] [int] NOT NULL,
[PolicyID] [int] NOT NULL,
[PStartDate] [datetime] NOT NULL,
[PEndDate] [datetime] NOT NULL,
[ActualPStartDate] [datetime] NOT NULL,
[ActualPEndDate] [datetime] NOT NULL,
[AnnualPeriod] [int] NULL,
[InsurerShare] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDates_07-09-2013] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicyDates_07-09-2013] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicyDates_07-09-2013] TO [power_user]
GRANT DELETE ON  [dbo].[tblPolicyDates_07-09-2013] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicyDates_07-09-2013] TO [power_user]
GO
