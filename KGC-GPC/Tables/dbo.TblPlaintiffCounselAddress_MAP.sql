CREATE TABLE [dbo].[TblPlaintiffCounselAddress_MAP]
(
[PlaintiffCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselAddressID] [bigint] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TblPlaintiffCounselAddress_MAP] TO [base_user]
GRANT SELECT ON  [dbo].[TblPlaintiffCounselAddress_MAP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TblPlaintiffCounselAddress_MAP] TO [power_user]
GRANT SELECT ON  [dbo].[TblPlaintiffCounselAddress_MAP] TO [power_user]
GRANT INSERT ON  [dbo].[TblPlaintiffCounselAddress_MAP] TO [power_user]
GRANT DELETE ON  [dbo].[TblPlaintiffCounselAddress_MAP] TO [power_user]
GRANT UPDATE ON  [dbo].[TblPlaintiffCounselAddress_MAP] TO [power_user]
GO
