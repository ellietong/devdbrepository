CREATE TABLE [dbo].[CLAIMS_DATA_OtherField]
(
[FileNo] [int] NULL,
[InternalReference] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherField] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_DATA_OtherField] TO [base_user]
GRANT SELECT ON  [dbo].[CLAIMS_DATA_OtherField] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CLAIMS_DATA_OtherField] TO [power_user]
GRANT SELECT ON  [dbo].[CLAIMS_DATA_OtherField] TO [power_user]
GRANT INSERT ON  [dbo].[CLAIMS_DATA_OtherField] TO [power_user]
GRANT DELETE ON  [dbo].[CLAIMS_DATA_OtherField] TO [power_user]
GRANT UPDATE ON  [dbo].[CLAIMS_DATA_OtherField] TO [power_user]
GO
