CREATE TABLE [dbo].[tblSettlementGroup]
(
[SettlementGroupID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementGroup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentTermsID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementGroup] ADD CONSTRAINT [PK_mBtblSettlementGroup_1] PRIMARY KEY CLUSTERED  ([SettlementGroupID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementGroup] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblSettlementGroup] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblSettlementGroup] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementGroup] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementGroup] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementGroup] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementGroup] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementGroup] TO [power_user]
GO
