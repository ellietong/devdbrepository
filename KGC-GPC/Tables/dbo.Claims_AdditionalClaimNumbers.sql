CREATE TABLE [dbo].[Claims_AdditionalClaimNumbers]
(
[ID] [int] NULL,
[PLAINTIFF] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JURISDICTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CASE NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILE NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Person#] [int] NULL,
[File#] [int] NULL,
[NewFileNo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoGPCdate] [int] NULL,
[BilledByRiley] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Claims_AdditionalClaimNumbers] TO [base_user]
GRANT SELECT ON  [dbo].[Claims_AdditionalClaimNumbers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Claims_AdditionalClaimNumbers] TO [power_user]
GRANT SELECT ON  [dbo].[Claims_AdditionalClaimNumbers] TO [power_user]
GRANT INSERT ON  [dbo].[Claims_AdditionalClaimNumbers] TO [power_user]
GRANT DELETE ON  [dbo].[Claims_AdditionalClaimNumbers] TO [power_user]
GRANT UPDATE ON  [dbo].[Claims_AdditionalClaimNumbers] TO [power_user]
GO
