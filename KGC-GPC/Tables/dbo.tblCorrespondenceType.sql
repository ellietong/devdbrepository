CREATE TABLE [dbo].[tblCorrespondenceType]
(
[CorrespondenceTypeID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorrespondenceType] ADD CONSTRAINT [PK_tblCorrespondenceType] PRIMARY KEY CLUSTERED  ([CorrespondenceTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCorrespondenceType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorrespondenceType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorrespondenceType] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblCorrespondenceType] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblCorrespondenceType] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblCorrespondenceType] TO [Notice_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCorrespondenceType] TO [power_user]
GRANT SELECT ON  [dbo].[tblCorrespondenceType] TO [power_user]
GRANT INSERT ON  [dbo].[tblCorrespondenceType] TO [power_user]
GRANT DELETE ON  [dbo].[tblCorrespondenceType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCorrespondenceType] TO [power_user]
GO
