CREATE TABLE [dbo].[TblDefenseCounselAddress_Map2]
(
[DefenseCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TblDefenseCounselAddress_Map2] TO [base_user]
GRANT SELECT ON  [dbo].[TblDefenseCounselAddress_Map2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TblDefenseCounselAddress_Map2] TO [power_user]
GRANT SELECT ON  [dbo].[TblDefenseCounselAddress_Map2] TO [power_user]
GRANT INSERT ON  [dbo].[TblDefenseCounselAddress_Map2] TO [power_user]
GRANT DELETE ON  [dbo].[TblDefenseCounselAddress_Map2] TO [power_user]
GRANT UPDATE ON  [dbo].[TblDefenseCounselAddress_Map2] TO [power_user]
GO
