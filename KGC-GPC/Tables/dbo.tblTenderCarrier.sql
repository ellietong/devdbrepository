CREATE TABLE [dbo].[tblTenderCarrier]
(
[TenderCarrierID] [int] NOT NULL,
[TenderCarrier] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderCarrier] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderCarrier] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderCarrier] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderCarrier] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderCarrier] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderCarrier] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderCarrier] TO [power_user]
GO
