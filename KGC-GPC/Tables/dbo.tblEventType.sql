CREATE TABLE [dbo].[tblEventType]
(
[EventTypeID] [int] NOT NULL IDENTITY(1, 1),
[EventType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEventType] ADD CONSTRAINT [PK_tblEventType] PRIMARY KEY CLUSTERED  ([EventTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblEventType] TO [base_user]
GRANT SELECT ON  [dbo].[tblEventType] TO [base_user]
GO
