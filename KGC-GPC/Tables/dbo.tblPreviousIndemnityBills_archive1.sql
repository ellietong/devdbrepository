CREATE TABLE [dbo].[tblPreviousIndemnityBills_archive1]
(
[Plaintiff] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurFile#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [float] NULL,
[ResolvedDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousIndemnityBills_archive1] TO [base_user]
GRANT SELECT ON  [dbo].[tblPreviousIndemnityBills_archive1] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPreviousIndemnityBills_archive1] TO [power_user]
GRANT SELECT ON  [dbo].[tblPreviousIndemnityBills_archive1] TO [power_user]
GRANT INSERT ON  [dbo].[tblPreviousIndemnityBills_archive1] TO [power_user]
GRANT DELETE ON  [dbo].[tblPreviousIndemnityBills_archive1] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPreviousIndemnityBills_archive1] TO [power_user]
GO
