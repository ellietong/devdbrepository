CREATE TABLE [dbo].[tblNoticeDefense]
(
[NoticeDefenseID] [int] NOT NULL IDENTITY(1, 1),
[NoticeDefense] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblNoticeDefense] ADD CONSTRAINT [PK_tblNoticeDefense] PRIMARY KEY CLUSTERED  ([NoticeDefenseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblNoticeDefense] TO [base_user]
GRANT SELECT ON  [dbo].[tblNoticeDefense] TO [base_user]
GRANT SELECT ON  [dbo].[tblNoticeDefense] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblNoticeDefense] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblNoticeDefense] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblNoticeDefense] TO [Notice_User]
GRANT VIEW DEFINITION ON  [dbo].[tblNoticeDefense] TO [power_user]
GRANT SELECT ON  [dbo].[tblNoticeDefense] TO [power_user]
GRANT INSERT ON  [dbo].[tblNoticeDefense] TO [power_user]
GRANT DELETE ON  [dbo].[tblNoticeDefense] TO [power_user]
GRANT UPDATE ON  [dbo].[tblNoticeDefense] TO [power_user]
GO
