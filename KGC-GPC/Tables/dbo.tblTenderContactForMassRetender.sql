CREATE TABLE [dbo].[tblTenderContactForMassRetender]
(
[TenderContactID] [int] NOT NULL IDENTITY(1, 1),
[CarrierID] [int] NULL,
[FName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Position] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[Zip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentContact] [bit] NOT NULL CONSTRAINT [DF_tblTenderContactForMassRetender_CurrentContact] DEFAULT ((0)),
[To] [bit] NOT NULL CONSTRAINT [DF_tblTenderContactForMassRetender_To] DEFAULT ((0)),
[CC] [bit] NOT NULL CONSTRAINT [DF_tblTenderContactForMassRetender_CC] DEFAULT ((0)),
[BCC] [bit] NOT NULL CONSTRAINT [DF_tblTenderContactForMassRetender_BCC] DEFAULT ((0)),
[AlwaysCC] [bit] NOT NULL CONSTRAINT [DF_tblTenderContactForMassRetender_AlwaysCC] DEFAULT ((0)),
[AlwaysBCC] [bit] NOT NULL CONSTRAINT [DF_tblTenderContactForMassRetender_AlwaysBCC] DEFAULT ((0)),
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderContactForMassRetender] ADD CONSTRAINT [PK_tblTenderContactForMassRetender] PRIMARY KEY CLUSTERED  ([TenderContactID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderContactForMassRetender] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderContactForMassRetender] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderContactForMassRetender] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderContactForMassRetender] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderContactForMassRetender] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderContactForMassRetender] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderContactForMassRetender] TO [power_user]
GO
