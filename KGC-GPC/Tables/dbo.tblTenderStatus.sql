CREATE TABLE [dbo].[tblTenderStatus]
(
[TenderStatusID] [int] NOT NULL IDENTITY(1, 1),
[TenderStatus] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderStatus] ADD CONSTRAINT [PK_tblTenderStatus] PRIMARY KEY CLUSTERED  ([TenderStatusID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderStatus] TO [power_user]
GO
