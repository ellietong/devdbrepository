CREATE TABLE [dbo].[CounselLink_Data_2014Q1]
(
[MatterID(CounselLink)] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID(Customer)] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterTitle] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawFirmOffice] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL,
[InvoiceID(CounselLink)] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceID(LawFirm)] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceApprovedDate] [datetime] NULL,
[ChargeNumber] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeTimekeeperName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeDate] [datetime] NULL,
[ChargeType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeEffectiveRate] [money] NULL,
[ChargeRate] [money] NULL,
[HoursBilled] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeBilledAmt] [money] NULL,
[ChargeRecommendedAmt] [money] NULL,
[ID] [bigint] NULL,
[ChargeDescription] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCC] [bit] NULL,
[AlreadyBilled] [bit] NULL,
[MotionOnly] [bit] NULL,
[Adjustment] [bit] NULL,
[DoNotBill] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data_2014Q1] TO [base_user]
GRANT SELECT ON  [dbo].[CounselLink_Data_2014Q1] TO [base_user]
GRANT INSERT ON  [dbo].[CounselLink_Data_2014Q1] TO [base_user]
GRANT DELETE ON  [dbo].[CounselLink_Data_2014Q1] TO [base_user]
GRANT UPDATE ON  [dbo].[CounselLink_Data_2014Q1] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[CounselLink_Data_2014Q1] TO [power_user]
GRANT SELECT ON  [dbo].[CounselLink_Data_2014Q1] TO [power_user]
GO
