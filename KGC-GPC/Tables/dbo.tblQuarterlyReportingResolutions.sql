CREATE TABLE [dbo].[tblQuarterlyReportingResolutions]
(
[ClaimantPersonalInfoID] [bigint] NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [float] NULL,
[ReportedStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportedProcessedDate] [datetime] NULL,
[ReportedSettlementAmount] [money] NULL,
[ReportedResolutionDate] [datetime] NULL,
[OriginalReport] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitStatusID] [bigint] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblQuarterlyReportingResolutions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblQuarterlyReportingResolutions] TO [labellj]
GRANT SELECT ON  [dbo].[tblQuarterlyReportingResolutions] TO [labellj]
GRANT INSERT ON  [dbo].[tblQuarterlyReportingResolutions] TO [labellj]
GRANT DELETE ON  [dbo].[tblQuarterlyReportingResolutions] TO [labellj]
GRANT UPDATE ON  [dbo].[tblQuarterlyReportingResolutions] TO [labellj]
GRANT ALTER ON  [dbo].[tblQuarterlyReportingResolutions] TO [power_user]
GRANT SELECT ON  [dbo].[tblQuarterlyReportingResolutions] TO [power_user]
GRANT INSERT ON  [dbo].[tblQuarterlyReportingResolutions] TO [power_user]
GRANT DELETE ON  [dbo].[tblQuarterlyReportingResolutions] TO [power_user]
GRANT UPDATE ON  [dbo].[tblQuarterlyReportingResolutions] TO [power_user]
GO
