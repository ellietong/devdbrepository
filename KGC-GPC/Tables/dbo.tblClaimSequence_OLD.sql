CREATE TABLE [dbo].[tblClaimSequence_OLD]
(
[ClaimSequenceID] [int] NOT NULL IDENTITY(1, 1),
[ClaimSequence] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimSequence_OLD] ADD CONSTRAINT [PK_mBtblClaimSequence] PRIMARY KEY CLUSTERED  ([ClaimSequenceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSequence_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSequence_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSequence_OLD] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimSequence_OLD] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimSequence_OLD] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSequence_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimSequence_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimSequence_OLD] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimSequence_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimSequence_OLD] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence_OLD', 'COLUMN', N'ClaimSequence'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence_OLD', 'COLUMN', N'ClaimSequence'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence_OLD', 'COLUMN', N'ClaimSequence'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence_OLD', 'COLUMN', N'ClaimSequenceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence_OLD', 'COLUMN', N'ClaimSequenceID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence_OLD', 'COLUMN', N'ClaimSequenceID'
GO
