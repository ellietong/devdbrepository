CREATE TABLE [dbo].[tblMedicareBeneficiaryStatus]
(
[MedicareBeneficiaryStatusID] [int] NOT NULL IDENTITY(1, 1),
[MedicareBeneficiaryStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMedicareBeneficiaryStatus] ADD CONSTRAINT [PK_tblMedicareBeneficiaryStatus] PRIMARY KEY CLUSTERED  ([MedicareBeneficiaryStatusID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMedicareBeneficiaryStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblMedicareBeneficiaryStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMedicareBeneficiaryStatus] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblMedicareBeneficiaryStatus] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblMedicareBeneficiaryStatus] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblMedicareBeneficiaryStatus] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMedicareBeneficiaryStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblMedicareBeneficiaryStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblMedicareBeneficiaryStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblMedicareBeneficiaryStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMedicareBeneficiaryStatus] TO [power_user]
GO
