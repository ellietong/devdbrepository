SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE FUNCTION [dbo].[fnMissingDocumentation]
(@ClaimLawsuitID bigint)

RETURNS NVARCHAR(MAX)
AS
BEGIN

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 24663


--IF object_id('tempdb..@CurrentDocumentation') IS NOT NULL
--BEGIN
--   DROP TABLE @CurrentDocumentation
--END
DECLARE @CurrentDocumentation TABLE  (ClaimantIDed bit, DxMedicals bit, DODCert int, EstatePaper int, ExpoHist bit, SettleConfirm bit, Release bit, CheckRequest bit, PreTrialReport int, MedicareForms bit)
INSERT INTO @CurrentDocumentation
SELECT distinct COALESCE(sd.ClaimantIDed, 0) AS ClaimantIDed,
				COALESCE(sd.DxMedicals, 0) AS DxMedicals,
				CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE(sd.DODCert, 0) END AS DODCert, 
				CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE(sd.EstatePapers, 0) END AS Estatepaper,  
				COALESCE(sd.EmployExpoHist, 0) AS EmployExpoHist, 
				COALESCE(sd.settlementconfirmation, 0) AS SettlementConfirmation,
				CASE WHEN (d.ClaimantPersonalInfoID IS not NULL OR ExecutedReleaseDate IS not NULL) THEN 1 ELSE 0 END AS releasereceived,
				COALESCE(sd.CheckRequest, 0) AS CheckRequestForm,
				COALESCE(sd.PreTrialReport, 0) AS PreTrialReport,
				COALESCE(sd.MedicareForms, 0) AS MedicareForms
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi	 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl	 ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
	LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
	INNER JOIN tblClaimLawsuitStatusTracking cls on cls.ClaimLawsuitID =cl.ClaimLawsuitID 
	LEFT JOIN (SELECT claimantpersonalinfoid 
			   FROM tblMultiClaimantDocument mcd 
				   INNER JOIN tbldocuments d on d.DocumentID = mcd.DocumentID 
			   WHERE ClaimantDocumentTypeID = 31 
			   GROUP BY ClaimantPersonalInfoID) d on d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID

--SELECT * FROM @CurrentDocumentation

--IF object_id('tempdb..@MissingDocumentation') IS NOT NULL
--BEGIN
--   DROP TABLE @MissingDocumentation
--END
DECLARE @MissingDocumentation TABLE  (ID int NOT NULL IDENTITY(1,1), MissingDocumentation nvarchar(500), Remainder int)
INSERT INTO @MissingDocumentation
SELECT 'Claimant Identified', NULL
FROM @CurrentDocumentation
WHERE ClaimantIDed = 0
UNION
SELECT 'Diagnosing Medicals', NULL
FROM @CurrentDocumentation
WHERE DxMedicals = 0
UNION
SELECT 'Death Cerificate', NULL
FROM @CurrentDocumentation
WHERE DODCert = 0
UNION
SELECT 'Estate Papers', NULL
FROM @CurrentDocumentation
WHERE EstatePaper = 0
UNION
SELECT 'Employment/Exposure History', NULL
FROM @CurrentDocumentation
WHERE ExpoHist = 0
UNION
SELECT 'Settlement Confirmation', NULL
FROM @CurrentDocumentation
WHERE SettleConfirm = 0
UNION
SELECT 'Executed Release', NULL
FROM @CurrentDocumentation
WHERE Release = 0
UNION
SELECT 'Check Request Form', NULL
FROM @CurrentDocumentation
WHERE CheckRequest = 0
UNION
SELECT 'Pre-Trial Report', NULL
FROM @CurrentDocumentation
WHERE PreTrialReport = 0
UNION
SELECT 'Medicare Forms', NULL
FROM @CurrentDocumentation
WHERE MedicareForms = 0

--SELECT *
--FROM @MissingDocumentation

	--Calculate the remainder for every-other row highlighting
	UPDATE @MissingDocumentation
	SET Remainder = fdp.Remainder 
	FROM (SELECT ID, (ID % 2) AS Remainder FROM @MissingDocumentation) fdp
	INNER JOIN @MissingDocumentation ofdp ON fdp.ID = ofdp.ID
	WHERE ofdp.ID = fdp.ID

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'

DECLARE @MissingDocumentationTable nvarchar(max) = ''

SET @MissingDocumentationTable = @MissingDocumentationTable + N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0;">' + 
	

N'<tr>
' +
	
	
CAST ( (
SELECT	CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleB END AS 'td/@style', (SELECT COALESCE(MissingDocumentation, '')) as td, ''
	FROM @MissingDocumentation
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	

	RETURN @MissingDocumentationTable --AS MissingDocumentation



END



GO
GRANT VIEW DEFINITION ON  [dbo].[fnMissingDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnMissingDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnMissingDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnMissingDocumentation] TO [power_user]
GO
