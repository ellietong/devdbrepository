SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[tblDefenseCosts]
AS
SELECT a.DefenseFirm, a.BillingMonth as InvoiceDate, 'Historical Invoices' as InvoiceNumber, 
'Aggregated Claimants' as ClaimantID, 
a.Fees, a.Expenses as Costs
from CLAIMS_HistoricDefense_2005 a

Union all

select c.DefenseFirm, c.BillingDate as InvoiceDate, 'Historical Invoices' as InvoiceNumber, 
'Aggregated Claimants' as ClaimantID, 
c.Fees, c.Expenses as Costs
from CLAIMS_HistoricDefense_2006 c

Union all

select d.Firm as DefenseFirm, d.Date_Converted as InvoiceDate, d.Invoice# as InvoiceNumber, 
File# as ClaimantID, TotalFees as Fees, TotalCosts as Costs
from CLAIMS_HistoricalDefense d
where (TotalFees>0 or TotalCosts>0)

Union all
select LawFirmOffice as DefenseFirm, invoicedate, invoiceid_lawfirm as InvoiceNumber, 
Code as ClaimantID, 
case when chargetype = 'legal fee' then ChargerecommendedAmt else 0 end as Fees,
case when ChargeType = 'Expense' then ChargeRecommendedAmt else 0 end as Costs
from CounselLink_Data1
where chargerecommendedamt<>0







GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCosts] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCosts] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCosts] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseCosts] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseCosts] TO [power_user]
GO
