SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblSuffix]
AS
SELECT     SuffixID, Suffix
FROM         [KGA-ClaimsAdmin].dbo.tblSuffix

GO
