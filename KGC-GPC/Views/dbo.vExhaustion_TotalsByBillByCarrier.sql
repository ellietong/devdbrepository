SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create VIEW [dbo].[vExhaustion_TotalsByBillByCarrier]
AS

SELECT  c.CarrierName, ex.ExhaustionDateID,  ExhaustionType, ExhaustionDate, 
[Description], sum(IndemnityAllocation) as Indemnity, sum(DefenseAllocation) as DefenseWithin, sum(DefenseOutsideLimitsAllocation) as DefenseOutside, 
Sum(SIRIndemnityAllocation) as SIRIndemnity, sum(SIRDefenseAllocation) as SIRDefense
FROM tblExhaustion EX
left join tblPolicyDates pd on pd.PolicyDateID = ex.PolicyDateID
join tblPolicy p on p.PolicyID = pd.Policyid
join tblCarrier c on c.CarrierID = p.CarrierID
LEFT JOIN tblExhaustionDate EXD ON EXD.ExhaustionDateID = EX.ExhaustionDateID
LEFT JOIN tblExhaustionType EXT ON EXT.ExhaustionTypeID = EXD.ExhaustionTypeID
where ex.ExhaustionDateID  not in  (1,15)
group by  c.CarrierName, ex.ExhaustionDateID,  ExhaustionType, ExhaustionDate, 
[Description]
--order by ex.exhaustiondateID, c.CarrierName

GO
