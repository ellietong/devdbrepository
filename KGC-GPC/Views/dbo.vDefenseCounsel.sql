SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vDefenseCounsel]
AS
SELECT     d.defensecounselid, d.defensecounsel, da.defensecounseladdress1, da.defensecounseladdress2, da.defensecounselcity, 
s.state, da.defensecounselzip, da.defensecounselphone
FROM        [SPSQL].[KGA-ClaimsAdmin].dbo.Tbldefensecounsel d
left join [SPSQL].[KGA-ClaimsAdmin].dbo.tbldefensecounseladdress da on da.defensecounselID = d.defensecounselID
left join [SPSQL].[KGA-ClaimsAdmin].dbo.tblstate s on s.stateid = da.defensecounselstateid
GO
GRANT VIEW DEFINITION ON  [dbo].[vDefenseCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[vDefenseCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDefenseCounsel] TO [power_user]
GRANT SELECT ON  [dbo].[vDefenseCounsel] TO [power_user]
GO
