SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblLawsuitDefendantCompany_NonClient]
AS
SELECT     ctl.LawsuitID, ldc.DefendantCompanyID, dc.DefendantCompany
FROM         dbo.tblComplaintTrackingLawsuit AS ctl INNER JOIN
                      [KGA-ClaimsAdmin].dbo.tblComplaintTracking AS ct ON ctl.ComplaintTrackingID = ct.ComplaintID INNER JOIN
                      [KGA-ClaimsAdmin].dbo.tblLawsuitDefendantCompany AS ldc ON ct.LawsuitID = ldc.LawsuitID INNER JOIN
                      [KGA-ClaimsAdmin].dbo.tblDefendantCompany AS dc ON ldc.DefendantCompanyID = dc.DefendantCompanyID
WHERE     (dc.ClientID <> 40) OR
                      (dc.ClientID IS NULL)


GO
GRANT VIEW DEFINITION ON  [dbo].[tblLawsuitDefendantCompany_NonClient] TO [base_user]
GRANT SELECT ON  [dbo].[tblLawsuitDefendantCompany_NonClient] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLawsuitDefendantCompany_NonClient] TO [power_user]
GRANT SELECT ON  [dbo].[tblLawsuitDefendantCompany_NonClient] TO [power_user]
GRANT INSERT ON  [dbo].[tblLawsuitDefendantCompany_NonClient] TO [power_user]
GRANT DELETE ON  [dbo].[tblLawsuitDefendantCompany_NonClient] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLawsuitDefendantCompany_NonClient] TO [power_user]
GO
