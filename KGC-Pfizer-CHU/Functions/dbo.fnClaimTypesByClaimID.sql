SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE FUNCTION [dbo].[fnClaimTypesByClaimID]
(@ClaimID bigint)

RETURNS NVARCHAR(MAX)
AS
BEGIN

--DECLARE @ClaimID int
--SET @ClaimID = 1852152

DECLARE @tdStyle nvarchar(MAX) = 'font-family: verdana,arial,helvetica,sans-serif; font-size: 8pt; text-align: left; padding: .1em; vertical-align: left;'

DECLARE @ClaimTypeTable nvarchar(max) = ''

SET @ClaimTypeTable = @ClaimTypeTable + N'<table style="border-collapse: collapse; color: @F6ECF0;">' + 
	

N'<tr>
' +
	
CAST ( (
SELECT @tdStyle AS 'td/@style', (SELECT COALESCE(ClaimTypeValue, '')) as td, ''
        FROM    tblClaimType CT
			INNER JOIN tblClaimTypeValue CTV ON CT.ClaimTypeValueID = CTV.ClaimTypeValueID
        WHERE   ClaimID = @ClaimID
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	
	--SELECT @ClaimTypeTable
	RETURN @ClaimTypeTable



END


GO
