SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Function [dbo].[fnDetermineReleaseTemplateType] (@ClaimantPersonalInfoID bigint)
Returns Int

as

Begin
--Declare @ClaimantPersonalInfoID bigint
--Set @ClaimantPersonalInfoID = 296045


Declare @DeceasedTest bit
Declare @MarriedTest bit
Declare @SpouseNotAsExecutor bit
Declare @ReleaseTypeID int
Declare @ReleaseFormat int
Declare @ReleaseTemplateTypeID int

SET @ReleaseTypeID = (SELECT
	CASE
		WHEN Max(ReleaseTypeID) LIKE '2' THEN 1 ELSE 0
	END
FROM tblClaimantPersonalInfo cpi
INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
INNER JOIN tblSettlePaymentDocumentation spd ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND IsPrimaryForClaim = 1 AND IsCurrentClaim=1)

SET @DeceasedTest = (SELECT
	CASE
		WHEN Max(DeceasedDate) IS NOT NULL THEN 1 ELSE 0
	END
FROM tblClaimantPersonalInfo
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID)

SET @MarriedTest = (
SELECT
	CASE
		WHEN Max(a.ClaimantRelatedPartyID) IS NOT NULL THEN 1 ELSE 0
	END
FROM tblClaimantPersonalInfo cpi
LEFT JOIN (SELECT
	ClaimantPersonalInfoID,
	ClaimantRelatedPartyID
FROM tblClaimantRelatedParty
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND RelationshipID = 9)
a ON a.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID)

SET @SpouseNotAsExecutor = (SELECT
	CASE
		WHEN Max(a.ClaimantRelatedPartyID) IS NOT NULL THEN 1 ELSE 0
	END
FROM tblClaimantPersonalInfo cpi
LEFT JOIN (SELECT
	ClaimantPersonalInfoID,
	ClaimantRelatedPartyID
FROM tblClaimantRelatedParty
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND RelationshipID = 9 AND LegalCapacityID IN (19, 0))
a ON a.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID)

IF @DeceasedTest = 1 AND @MarriedTest = 1 AND @SpouseNotAsExecutor = 0 BEGIN
--Insert Full Spouse Release
SET @ReleaseFormat = 1
END
IF @DeceasedTest = 1 AND @MarriedTest = 1 AND @SpouseNotAsExecutor = 1 BEGIN
--Insert Full Executor Spouse
SET @ReleaseFormat = 2
END
IF @DeceasedTest = 1 AND @MarriedTest = 0 BEGIN
--Insert Full Executor Third
SET @ReleaseFormat = 3
END
IF @DeceasedTest = 0 AND @ReleaseTypeID = 1 AND @MarriedTest = 1 BEGIN
--Insert Full HW
SET @ReleaseFormat = 4
END
IF @DeceasedTest = 0 AND @ReleaseTypeID = 1 AND @MarriedTest = 0 BEGIN
--Insert Full Single
SET @ReleaseFormat = 5
END
IF @DeceasedTest = 0 AND @ReleaseTypeID = 0 AND @MarriedTest = 1 BEGIN
--Insert Limited HW
SET @ReleaseFormat = 6
END
IF @DeceasedTest = 0 AND @ReleaseTypeID = 0 AND @MarriedTest = 0 BEGIN
--Insert Limited Single
SET @ReleaseFormat = 7
END

SET @ReleaseTemplateTypeID = (SELECT
	@ReleaseFormat AS ReleaseFormat)



RETURN @ReleaseTemplateTypeID

End
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDetermineReleaseTemplateType] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnDetermineReleaseTemplateType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnDetermineReleaseTemplateType] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[fnDetermineReleaseTemplateType] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[fnDetermineReleaseTemplateType] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnDetermineReleaseTemplateType] TO [power_user]
GO
