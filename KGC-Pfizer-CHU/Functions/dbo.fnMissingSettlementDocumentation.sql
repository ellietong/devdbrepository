SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE FUNCTION [dbo].[fnMissingSettlementDocumentation]
(@ClaimLawsuitID bigint)

RETURNS NVARCHAR(MAX)
AS
BEGIN

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 24663

DECLARE @CurrentDocumentation TABLE  (DxMedicals bit, 
									  DODCert int, 
									  EstatePaper int)

INSERT INTO @CurrentDocumentation
SELECT distinct COALESCE(sd.DxMedicals, 0) AS DxMedicals,
				CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE(sd.DODCert, 0) END AS DODCert, 
				CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE(sd.EstatePapers, 0) END AS Estatepaper
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi	 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl	 ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
	LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID

--SELECT * FROM @CurrentDocumentation

DECLARE @MissingDocumentation TABLE  (ID int NOT NULL IDENTITY(1,1), MissingDocumentation nvarchar(500), Remainder int)
INSERT INTO @MissingDocumentation
SELECT 'Diagnosing Medicals', NULL
FROM @CurrentDocumentation
WHERE DxMedicals = 0
UNION
SELECT 'Death Cerificate', NULL
FROM @CurrentDocumentation
WHERE DODCert = 0
UNION
SELECT 'Estate Papers', NULL
FROM @CurrentDocumentation
WHERE EstatePaper = 0


--SELECT *
--FROM @MissingDocumentation

	--Calculate the remainder for every-other row highlighting
	UPDATE @MissingDocumentation
	SET Remainder = fdp.Remainder 
	FROM (SELECT ID, (ID % 2) AS Remainder FROM @MissingDocumentation) fdp
	INNER JOIN @MissingDocumentation ofdp ON fdp.ID = ofdp.ID
	WHERE ofdp.ID = fdp.ID

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: none; padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'

DECLARE @MissingDocumentationTable nvarchar(max) = ''

SET @MissingDocumentationTable = @MissingDocumentationTable + N'<table style="border-collapse: collapse; border: none; color: @F6ECF0; width: 100%;">' + 
	

N'<tr>
' +
	
	
CAST ( (
SELECT	CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleW END AS 'td/@style', (SELECT COALESCE(MissingDocumentation, '')) as td, ''
	FROM @MissingDocumentation
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	

	RETURN @MissingDocumentationTable 



END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnMissingSettlementDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnMissingSettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnMissingSettlementDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnMissingSettlementDocumentation] TO [power_user]
GO
