SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE FUNCTION [dbo].[fnMissingClaimantInformation]
(@ClaimLawsuitID bigint)

RETURNS NVARCHAR(MAX)
AS
BEGIN

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 25776

DECLARE @Product table (ClaimantPersonalInfoID int, ProductEntered int)
insert into @Product 
SELECT ce.ClaimantPersonalInfoID, CASE WHEN MAX(ProductID) IS NULL  THEN 0  ELSE 1  END AS ProductEntered
FROM tblClaimantEmployment ce 
INNER JOIN tblClaim c on ce.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
INNER JOIN tblClaimantProduct cp on ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
GROUP BY ce.ClaimantPersonalInfoID 

DECLARE @CurrentClaimantInfo TABLE  (
									 ClaimantName bit,
									 SSN bit, 
									 BirthDate bit, 
									 GenderID bit,
									 Disease bit, 
									 DiagnosisDate bit,  
									 ProductID bit
									)
INSERT INTO @CurrentClaimantInfo
SELECT DISTINCT CASE WHEN cpi.LastName IS NULL THEN 0 WHEN cpi.FirstName IS NULL THEN 0 ELSE 1 END AS HasClaimantName,
				CASE WHEN SocialSecurityNumber is NULL THEN 0 ELSE 1 END AS HasSSN, 
				CASE WHEN BirthDate is NULL THEN 0 ELSE 1 END AS HasBirthDate,
				CASE WHEN COALESCE(GenderID, 0) = 0 THEN 0 WHEN COALESCE(GenderID, 0) = 4 THEN 0 ELSE 1 END AS HasGenderID,
				CASE WHEN DiseaseID IS NULL THEN 0 ELSE 1 END AS HasDiseaseID,
				CASE WHEN CONVERT(date, DiseaseDiagnosisDate) IS NULL THEN 0 ELSE 1 END AS HasDxDate, 
				CASE WHEN COALESCE(p.ProductEntered, 0) = 0 THEN 0  ELSE 1 END AS HasProductEntered  			
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi	 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl	 ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
	LEFT JOIN @Product p	 ON p.claimantpersonalinfoid =cpi.ClaimantPersonalInfoID 
	LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
	LEFT JOIN (SELECT * FROM tblClaimDisease WHERE PrimaryDisease = 1) cdi	 ON cdi.ClaimID = c.ClaimID
	INNER JOIN tblClaimLawsuitStatusTracking cls on cls.ClaimLawsuitID =cl.ClaimLawsuitID 
	LEFT JOIN (SELECT * FROM tblClaimantRelatedParty WHERE LegalCapacityID in (1,2,3,4,5,6,7,8,15,17,30,31))crp	 ON crp.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID


--SELECT * FROM @CurrentClaimantInfo

--IF object_id('tempdb..#MissingClaimantInfo') IS NOT NULL
--BEGIN
--   DROP TABLE #MissingClaimantInfo
--END
DECLARE @MissingClaimantInfo  TABLE  (ID int NOT NULL IDENTITY(1,1), MissingClaimantInformation nvarchar(500), Remainder int)
INSERT INTO @MissingClaimantInfo
SELECT 'Claimant Name', NULL
FROM @CurrentClaimantInfo
WHERE ClaimantName = 0
UNION
SELECT 'Social Security Number', NULL
FROM @CurrentClaimantInfo
WHERE SSN = 0
UNION
SELECT 'Birth Date', NULL
FROM @CurrentClaimantInfo
WHERE BirthDate = 0
UNION
SELECT 'Gender', NULL
FROM @CurrentClaimantInfo
WHERE GenderID = 0
UNION
SELECT 'Disease Diagnosis', NULL
FROM @CurrentClaimantInfo
WHERE Disease = 0
UNION
SELECT 'Diagnosis Date', NULL
FROM @CurrentClaimantInfo
WHERE DiagnosisDate = 0
UNION
SELECT 'Product ID', NULL
FROM @CurrentClaimantInfo
WHERE ProductID = 0

	--Calculate the remainder for every-other row highlighting
	UPDATE @MissingClaimantInfo
	SET Remainder = fdp.Remainder 
	FROM (SELECT ID, (ID % 2) AS Remainder FROM @MissingClaimantInfo) fdp
	INNER JOIN @MissingClaimantInfo ofdp ON fdp.ID = ofdp.ID
	WHERE ofdp.ID = fdp.ID

--SELECT *
--FROM @MissingClaimantInfo

	DECLARE @Remainder TABLE (arID int, RowCounter int, Remainder int)
	INSERT INTO @Remainder
	SELECT ID,
		ROW_NUMBER() OVER (ORDER BY ID) AS Row_Counter,  ROW_NUMBER() OVER (ORDER BY ID)% 2 AS Remainder    
	FROM @MissingClaimantInfo

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: none; padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'


DECLARE @MissingClaimantInfoTable nvarchar(max) = ''

SET @MissingClaimantInfoTable = @MissingClaimantInfoTable + N'<table style="border-collapse: collapse; border: none; color: @F6ECF0; width: 100%;">' + 
	

N'<tr>
' +
	
	
CAST ( (
SELECT CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @TDStyleW END AS 'td/@style', COALESCE((SELECT COALESCE(MissingClaimantInformation, '')), 'All required claimant information submitted.') as td, ''
	FROM @MissingClaimantInfo
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	
	--SELECT @MissingClaimantInfoTable
	RETURN @MissingClaimantInfoTable --AS MissingClaimantInfo



END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnMissingClaimantInformation] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnMissingClaimantInformation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnMissingClaimantInformation] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnMissingClaimantInformation] TO [power_user]
GO
