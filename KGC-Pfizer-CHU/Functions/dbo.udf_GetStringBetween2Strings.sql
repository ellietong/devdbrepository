SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--select substring(DocumentTitle, (len('Document ')+2),(len(DocumentTitle)-len('Document '))) As ConcatenatedTitle, * from tblDocuments
--where documentID > 142

CREATE FUNCTION [dbo].[udf_GetStringBetween2Strings] 
(
    @String varchar(max), 
    @FirstString varchar(256), 
    @SecondString varchar(256)
)
RETURNS VARCHAR(max) 
AS
BEGIN
 
    DECLARE @FirstPosition int,  @SecondPosition int
    SET @FirstPosition = CHARINDEX(@FirstString,@String) + LEN(@FirstString)
    SET @SecondPosition = CHARINDEX(@SecondString,@String) 
    RETURN (SELECT SUBSTRING(@String, @FirstPosition, @SecondPosition - @FirstPosition))
 
END
GO
