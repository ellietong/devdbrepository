SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vREPORTS_StateJurisdictionCombinations]
AS
--California Jurisdictions
SELECT DISTINCT s.StateID, FullNameState, State, State + 
CASE 
	WHEN j.JurisdictionID = 8 THEN ' - ' + Jurisdiction 
	WHEN j.JurisdictionID = 378 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 1048 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 565 THEN ' - ' + Jurisdiction
	  END AS Combination, 
	  CASE 
	WHEN j.JurisdictionID = 8 THEN ' - ' + j.JurisdictionID 
	WHEN j.JurisdictionID = 378 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 1048 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 565 THEN ' - ' + j.JurisdictionID
	  END AS JurisdictionID, Jurisdiction
FROM tblState s
	INNER JOIN tblDefenseJurisdiction dj on s.StateID = dj.StateID
	INNER JOIN tblJurisdiction j on dj.JurisdictionID = j.JurisdictionID 
WHERE S.StateID IN (31)
UNION 
--Illinois Jurisdictions
SELECT DISTINCT s.StateID, FullNameState, State, State + 
CASE 
	WHEN j.JurisdictionID = 125 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 387 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 614 THEN ' - ' + Jurisdiction
	  END AS Combination, 
	  CASE 
	WHEN j.JurisdictionID = 125 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 387 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 614 THEN ' - ' + j.JurisdictionID
	  END AS JurisdictionID, Jurisdiction
FROM tblState s
	INNER JOIN tblDefenseJurisdiction dj on s.StateID = dj.StateID
	INNER JOIN tblJurisdiction j on dj.JurisdictionID = j.JurisdictionID 
WHERE S.StateID IN (45)
UNION
--Pennsylvania Jurisdictions
SELECT DISTINCT s.StateID, FullNameState, State, State + 
CASE 
	WHEN j.JurisdictionID = 13 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 160 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 477 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 519 THEN ' - ' + Jurisdiction
	  END AS Combination, 
	  CASE 
	WHEN j.JurisdictionID = 13 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 160 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 477 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 519 THEN ' - ' + j.JurisdictionID
	  END AS JurisdictionID, Jurisdiction
FROM tblState s
	INNER JOIN tblDefenseJurisdiction dj on s.StateID = dj.StateID
	INNER JOIN tblJurisdiction j on dj.JurisdictionID = j.JurisdictionID 
WHERE S.StateID IN (17)
UNION
--Washington Jurisdictions
SELECT DISTINCT s.StateID, FullNameState, State, State + 
CASE 
	WHEN j.JurisdictionID = 347 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 521 THEN ' - ' + Jurisdiction
	  END AS Combination, 
	  CASE 
	WHEN j.JurisdictionID = 347 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 521 THEN ' - ' + j.JurisdictionID
	  END AS JurisdictionID, Jurisdiction
FROM tblState s
	INNER JOIN tblDefenseJurisdiction dj on s.StateID = dj.StateID
	INNER JOIN tblJurisdiction j on dj.JurisdictionID = j.JurisdictionID 
WHERE S.StateID IN (37)
UNION
SELECT DISTINCT s.StateID,  FullNameState, State, State, NULL, NULL
FROM tblState s
	INNER JOIN tblDefenseJurisdiction dj on s.StateID = dj.StateID
	INNER JOIN tblJurisdiction j on dj.JurisdictionID = j.JurisdictionID 
WHERE  S.StateID NOT IN (17, 31, 37, 45)           


GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vREPORTS_StateJurisdictionCombinations', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vREPORTS_StateJurisdictionCombinations', NULL, NULL
GO
