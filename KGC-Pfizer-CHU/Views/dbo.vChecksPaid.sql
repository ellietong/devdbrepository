SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE View [dbo].[vChecksPaid]  as

select c.CheckID, c.SourceNumber, c.SourceAmount, c.SourceDate, dca.DefenseCounsel as Payee
from tblCheck c
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
where FundingTypeID=1

union
select c.CheckID, c.SourceNumber, c.SourceAmount, c.SourceDate, pca.PlaintiffCounsel
From tblCheck c
Inner Join tblPaymentSettlement ps on c.CheckID = ps.CheckID
Inner Join tblClaimantSettlement cs on cs.ClaimantSettlementID = ps.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
where fundingtypeid=2

union
select c.CheckID, c.SourceNumber, c.SourceAmount, c.SourceDate, v.Firm
From tblCheck c
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join vtblFirmsAndVendors v on v.FirmID=di.VendorFirmID
where FundingTypeID=3


GO
GRANT VIEW DEFINITION ON  [dbo].[vChecksPaid] TO [base_user]
GRANT SELECT ON  [dbo].[vChecksPaid] TO [base_user]
GRANT SELECT ON  [dbo].[vChecksPaid] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[vChecksPaid] TO [power_user]
GRANT SELECT ON  [dbo].[vChecksPaid] TO [power_user]
GO
