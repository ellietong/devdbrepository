SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vSchemeCarriers]
AS
SELECT s.SchemeID, SchemeName, CarrierID
FROM [SPSQL].[KGA-InsolvencyTracking].dbo.tblScheme s
LEFT JOIN [SPSQL].[KGA-InsolvencyTracking].dbo.tblCarrierCompany c ON s.SchemeID = c.SchemeID
GO
GRANT VIEW DEFINITION ON  [dbo].[vSchemeCarriers] TO [base_user]
GRANT SELECT ON  [dbo].[vSchemeCarriers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSchemeCarriers] TO [power_user]
GRANT SELECT ON  [dbo].[vSchemeCarriers] TO [power_user]
GO
