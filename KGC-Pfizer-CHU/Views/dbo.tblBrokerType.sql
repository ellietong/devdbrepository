SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[tblBrokerType]
AS
SELECT BrokerTypeID, BrokerType
FROM [SQL2008].[KGA-Carrier].dbo.tblBrokerType


GO
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerType] TO [base_user]
GRANT SELECT ON  [dbo].[tblBrokerType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerType] TO [power_user]
GRANT SELECT ON  [dbo].[tblBrokerType] TO [power_user]
GO
