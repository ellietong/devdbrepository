SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblSubscriberAgency]
AS

SELECT AgencyID, AgencyName
FROM [SQL2008].[KGA-Carrier].dbo.tblSubscriberAgency
UNION
SELECT AgencyID, AgencyName
FROM tblSubscriberAgency_ALT


GO
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriberAgency] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriberAgency] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriberAgency] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriberAgency] TO [power_user]
GO
