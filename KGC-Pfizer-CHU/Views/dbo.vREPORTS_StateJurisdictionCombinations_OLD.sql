SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vREPORTS_StateJurisdictionCombinations_OLD]
AS
SELECT DISTINCT s.StateID, FullNameState, State, State + 
CASE 
	WHEN j.JurisdictionID = 8 THEN ' - ' + Jurisdiction 
	WHEN j.JurisdictionID = 378 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 1048 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 565 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 125 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 387 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 614 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 13 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 160 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 477 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 519 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 347 THEN ' - ' + Jurisdiction
	WHEN j.JurisdictionID = 521 THEN ' - ' + Jurisdiction
	  END AS Combination, 
	  CASE 
	WHEN j.JurisdictionID = 8 THEN ' - ' + j.JurisdictionID 
	WHEN j.JurisdictionID = 378 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 1048 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 565 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 125 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 387 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 614 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 13 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 160 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 477 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 519 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 347 THEN ' - ' + j.JurisdictionID
	WHEN j.JurisdictionID = 521 THEN ' - ' + j.JurisdictionID
	  END AS JurisdictionID, Jurisdiction
FROM tblState s
	INNER JOIN tblDefenseJurisdiction dj on s.StateID = dj.StateID
	INNER JOIN tblJurisdiction j on dj.JurisdictionID = j.JurisdictionID 
WHERE S.StateID IN (17, 31, 37, 45)
UNION 
SELECT DISTINCT s.StateID,  FullNameState, State, State, NULL, NULL
FROM tblState s
	INNER JOIN tblDefenseJurisdiction dj on s.StateID = dj.StateID
	INNER JOIN tblJurisdiction j on dj.JurisdictionID = j.JurisdictionID 
WHERE  S.StateID NOT IN (17, 31, 37, 45)         

GO
