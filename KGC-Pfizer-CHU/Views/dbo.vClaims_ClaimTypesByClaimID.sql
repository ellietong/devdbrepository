SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vClaims_ClaimTypesByClaimID]
AS
SELECT DISTINCT ClaimID,
LEFT(ClaimTypeIDList,LEN(ClaimTypeIDList)-1) AS ClaimTypeIDs,
LEFT(ClaimTypeValueList,LEN(ClaimTypeValueList)-1) AS ClaimTypeValues
FROM  (
SELECT DISTINCT ct.ClaimID, 
	(
		SELECT  DISTINCT '' + CONVERT(NVARCHAR, ct.ClaimTypeValueID) + '' + ', ' AS [text()] 
		FROM    tblClaimType CT
			INNER JOIN tblClaimTypeValue CTV ON CT.ClaimTypeValueID = CTV.ClaimTypeValueID	
		WHERE ct.ClaimID = c.ClaimID 
		FOR XML PATH('')	
	) AS ClaimTypeIDList,
	(
		SELECT  DISTINCT '' + CONVERT(NVARCHAR, ClaimTypeValue) + '' + ', ' AS [text()] 
		FROM    tblClaimType CT
			INNER JOIN tblClaimTypeValue CTV ON CT.ClaimTypeValueID = CTV.ClaimTypeValueID	
		WHERE ct.ClaimID = c.ClaimID 
		FOR XML PATH('')	
	) AS ClaimTypeValueList
FROM tblClaim c
	INNER JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
) ct

GO
