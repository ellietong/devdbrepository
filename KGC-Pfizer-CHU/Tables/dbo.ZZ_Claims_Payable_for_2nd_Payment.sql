CREATE TABLE [dbo].[ZZ_Claims_Payable_for_2nd_Payment]
(
[KCIC_ID] [int] NULL,
[IP#] [bigint] NULL,
[Claim Seq#] [int] NULL,
[Last Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Middle Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date of Birth] [datetime] NULL,
[Pltf Law Firm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settling Law Firm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Payee Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Grp#] [int] NULL,
[Sett Date] [datetime] NULL,
[Sett Amt] [money] NULL,
[Paid Amt] [money] NULL,
[Payable Amt] [money] NULL,
[Payment Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[subSSN] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_Claims_Payable_for_2nd_Payment] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_Claims_Payable_for_2nd_Payment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_Claims_Payable_for_2nd_Payment] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_Claims_Payable_for_2nd_Payment] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_Claims_Payable_for_2nd_Payment] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_Claims_Payable_for_2nd_Payment] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_Claims_Payable_for_2nd_Payment] TO [power_user]
GO
