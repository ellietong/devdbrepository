CREATE TABLE [dbo].[tblClaimType]
(
[ClaimTypeID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID] [bigint] NULL,
[ClaimTypeValueID] [int] NULL,
[IsPrimaryClaimType] [bit] NULL CONSTRAINT [DF_tblClaimType_IsPrimaryClaimType] DEFAULT ((0)),
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT ALTER ON  [dbo].[tblClaimType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimType] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimType] TO [base_user]
GRANT INSERT ON  [dbo].[tblClaimType] TO [base_user]
GRANT ALTER ON  [dbo].[tblClaimType] TO [kanga]
GRANT SELECT ON  [dbo].[tblClaimType] TO [kanga]
GRANT INSERT ON  [dbo].[tblClaimType] TO [kanga]
GRANT ALTER ON  [dbo].[tblClaimType] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimType] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimType] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimType] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimType] TO [power_user]
GO
