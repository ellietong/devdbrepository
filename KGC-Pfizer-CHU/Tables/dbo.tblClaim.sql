CREATE TABLE [dbo].[tblClaim]
(
[ClaimID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[IsCurrentClaim] [bit] NULL,
[ClaimSequenceID] [int] NOT NULL,
[ClaimTypeID] [int] NULL,
[FullyDocumented] [bit] NULL,
[CauseOfInjuryID] [int] NULL,
[MatterID] [int] NULL,
[ILODoctorID] [int] NULL,
[ILOProfusion] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILODate] [datetime] NULL,
[PFTDoctorID] [int] NULL,
[PFTDate] [datetime] NULL,
[PFTTypeID] [int] NULL,
[PFTFVC_Actual] [float] NULL,
[PFTFVC_Predicted] [float] NULL,
[PFTFEV1_Actual] [float] NULL,
[PFTFEV1_Predicted] [float] NULL,
[PFTFEV1AndFVC_Actual] [float] NULL,
[PFTFEV1AndFVC_Predicted] [float] NULL,
[PFTTLC_Actual] [float] NULL,
[PFTTLC_Predicted] [float] NULL,
[PFTDLCO_Actual] [float] NULL,
[PFTDLCO_Predicted] [float] NULL,
[PFTTV] [float] NULL,
[CLIENT_COMPANY_NBR] [int] NULL,
[CLAIM_SEQUENCE_NBR] [int] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NotifyCounsel] [bit] NULL CONSTRAINT [DF_tblClaim_NotifyCounsel] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [PK_mBtblClaim] PRIMARY KEY CLUSTERED  ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__tblClaim__Claima__60E81E88] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaim_ClaimantPersonalInfoID] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID]) INCLUDE ([ClaimID], [IsCurrentClaim], [MatterID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaim_MatterID] ON [dbo].[tblClaim] ([MatterID]) INCLUDE ([ClaimantPersonalInfoID], [ClaimID], [ClaimSequenceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaim_MatterID_ClaimID] ON [dbo].[tblClaim] ([MatterID]) INCLUDE ([CLAIM_SEQUENCE_NBR], [ClaimantPersonalInfoID], [ClaimID], [ClaimSequenceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [FK__tblClaim__Claima__60E81E88] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaim] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaim] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaim] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaim] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaim] TO [power_user]
GO
