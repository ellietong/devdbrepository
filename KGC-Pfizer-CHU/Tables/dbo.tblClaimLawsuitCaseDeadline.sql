CREATE TABLE [dbo].[tblClaimLawsuitCaseDeadline]
(
[ClaimLawsuitCaseDeadlineID] [bigint] NOT NULL IDENTITY(1, 1),
[CaseDeadlineID] [bigint] NOT NULL,
[ClaimLawsuitID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitCaseDeadline] ADD CONSTRAINT [PK_tblClaimLawsuitCaseDeadline] PRIMARY KEY CLUSTERED  ([ClaimLawsuitCaseDeadlineID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitCaseDeadline] ADD CONSTRAINT [FK__tblClaimLawsuitCD__tblCaseDeadline] FOREIGN KEY ([CaseDeadlineID]) REFERENCES [dbo].[tblCaseDeadline] ([CaseDeadlineID])
GO
ALTER TABLE [dbo].[tblClaimLawsuitCaseDeadline] ADD CONSTRAINT [FK__tblClaimLawsuitCaseDeadline__tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitCaseDeadline] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitCaseDeadline] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitCaseDeadline] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitCaseDeadline] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitCaseDeadline] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitCaseDeadline] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitCaseDeadline] TO [power_user]
GO
