CREATE TABLE [dbo].[tblSavedInjuredPartyIds]
(
[SavedInjuredPartyID] [bigint] NOT NULL IDENTITY(1, 1),
[InjuredPartyID] [bigint] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedInjuredPartyIds] ADD CONSTRAINT [PK_tblSavedInjuredPartyIDs] PRIMARY KEY CLUSTERED  ([SavedInjuredPartyID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedInjuredPartyIds] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedInjuredPartyIds] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedInjuredPartyIds] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedInjuredPartyIds] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedInjuredPartyIds] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedInjuredPartyIds] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedInjuredPartyIds] TO [power_user]
GO
