CREATE TABLE [dbo].[tblDeadlineType]
(
[DeadlineTypeID] [int] NOT NULL IDENTITY(1, 1),
[DeadlineType] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDeadlineType] ADD CONSTRAINT [PK_tblDeadlineType] PRIMARY KEY CLUSTERED  ([DeadlineTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDeadlineType] TO [base_user]
GRANT SELECT ON  [dbo].[tblDeadlineType] TO [base_user]
GRANT ALTER ON  [dbo].[tblDeadlineType] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDeadlineType] TO [power_user]
GRANT SELECT ON  [dbo].[tblDeadlineType] TO [power_user]
GRANT INSERT ON  [dbo].[tblDeadlineType] TO [power_user]
GRANT DELETE ON  [dbo].[tblDeadlineType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDeadlineType] TO [power_user]
GO
