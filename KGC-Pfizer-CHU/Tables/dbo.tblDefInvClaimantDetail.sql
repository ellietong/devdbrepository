CREATE TABLE [dbo].[tblDefInvClaimantDetail]
(
[InvoiceDetailID] [int] NOT NULL IDENTITY(1, 1),
[DocumentID] [bigint] NULL,
[FirmID] [int] NULL,
[InvoiceNumber] [nchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [date] NULL,
[ClaimantPersonalInfoID] [int] NULL,
[Fees] [money] NULL,
[Costs] [money] NULL,
[Adjustments] [money] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PACEClaimID] [bigint] NULL,
[MatterID] [int] NULL,
[InvoiceIdentifier] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseInvoiceID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefInvClaimantDetail] ADD CONSTRAINT [PK_tblDefInvClaimantDetail] PRIMARY KEY CLUSTERED  ([InvoiceDetailID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_2896_2895_tblDefInvClaimantDetail] ON [dbo].[tblDefInvClaimantDetail] ([DefenseInvoiceID]) INCLUDE ([Adjustments], [Costs], [Fees]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblDefInvClaimantDetail_tblDocuments] ON [dbo].[tblDefInvClaimantDetail] ([DocumentID]) INCLUDE ([Adjustments], [Costs], [Fees], [MatterID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [base_user]
GRANT ALTER ON  [dbo].[tblDefInvClaimantDetail] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblDefInvClaimantDetail] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblDefInvClaimantDetail] TO [Billing_User]
GRANT ALTER ON  [dbo].[tblDefInvClaimantDetail] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefInvClaimantDetail] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefInvClaimantDetail] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_Description', N'DocumentID for "Master Invoice" document pdf/doc/etc', 'SCHEMA', N'dbo', 'TABLE', N'tblDefInvClaimantDetail', 'COLUMN', N'DocumentID'
GO
