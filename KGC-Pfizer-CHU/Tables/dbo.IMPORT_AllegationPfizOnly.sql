CREATE TABLE [dbo].[IMPORT_AllegationPfizOnly]
(
[DefendantCompanyID] [float] NULL,
[AllegationTypeID] [float] NULL,
[Materialid] [float] NULL,
[DamageID] [float] NULL,
[ClaimLawsuitID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[IMPORT_AllegationPfizOnly] TO [base_user]
GRANT SELECT ON  [dbo].[IMPORT_AllegationPfizOnly] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[IMPORT_AllegationPfizOnly] TO [power_user]
GRANT SELECT ON  [dbo].[IMPORT_AllegationPfizOnly] TO [power_user]
GRANT INSERT ON  [dbo].[IMPORT_AllegationPfizOnly] TO [power_user]
GRANT DELETE ON  [dbo].[IMPORT_AllegationPfizOnly] TO [power_user]
GRANT UPDATE ON  [dbo].[IMPORT_AllegationPfizOnly] TO [power_user]
GO
