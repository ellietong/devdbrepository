CREATE TABLE [dbo].[m_tblTuckerEllisDocUpdates]
(
[MultiClaimantDocumentID] [int] NULL,
[TE_DocumentID] [int] NULL,
[ClaimantPersonalInfoID] [int] NULL,
[CORRECTKCICDOCID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblTuckerEllisDocUpdates] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblTuckerEllisDocUpdates] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblTuckerEllisDocUpdates] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblTuckerEllisDocUpdates] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblTuckerEllisDocUpdates] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblTuckerEllisDocUpdates] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblTuckerEllisDocUpdates] TO [power_user]
GO
