CREATE TABLE [dbo].[tblBillGroupCarriers]
(
[BillGroupCarrierID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupID] [int] NOT NULL,
[CarrierID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillGroupCarriers] ADD CONSTRAINT [PK_tblBillGroupCarriers] PRIMARY KEY NONCLUSTERED  ([BillGroupCarrierID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblBillGroupCarriers_tblBillGroup] ON [dbo].[tblBillGroupCarriers] ([BillGroupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillGroupCarriers] ADD CONSTRAINT [FK_tblBillGroupCarriers_tblBillGroup] FOREIGN KEY ([BillGroupID]) REFERENCES [dbo].[tblBillGroup] ([BillGroupID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupCarriers] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillGroupCarriers] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillGroupCarriers] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblBillGroupCarriers] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblBillGroupCarriers] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblBillGroupCarriers] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupCarriers] TO [power_user]
GRANT SELECT ON  [dbo].[tblBillGroupCarriers] TO [power_user]
GRANT INSERT ON  [dbo].[tblBillGroupCarriers] TO [power_user]
GRANT DELETE ON  [dbo].[tblBillGroupCarriers] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBillGroupCarriers] TO [power_user]
GO
