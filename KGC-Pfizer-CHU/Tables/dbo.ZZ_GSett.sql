CREATE TABLE [dbo].[ZZ_GSett]
(
[InjuredPartyID] [bigint] NULL,
[ClaimSeqNo] [int] NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialSecurityNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_GSett] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_GSett] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_GSett] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_GSett] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_GSett] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_GSett] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_GSett] TO [power_user]
GO
