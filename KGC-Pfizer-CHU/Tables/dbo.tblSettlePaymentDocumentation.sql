CREATE TABLE [dbo].[tblSettlePaymentDocumentation]
(
[SetPymtID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [int] NULL,
[SettlementConfirmation] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_SettlementConfirmation] DEFAULT ((0)),
[ClaimantIDed] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_ClaimantIDed] DEFAULT ((0)),
[DxMedicals] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_DxMedicals] DEFAULT ((0)),
[DODCert] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_DODCert] DEFAULT ((0)),
[EstatePapers] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_EstatePapers] DEFAULT ((0)),
[ProductID] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_ProductID] DEFAULT ((0)),
[EmployExpoHist] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_EmployExpoHist] DEFAULT ((0)),
[ReleaseRqstDate] [datetime] NULL,
[ReleaseToPC] [datetime] NULL,
[ExecutedReleaseDate] [datetime] NULL,
[SettlePaymentComments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseRcvd] [bit] NULL,
[MedicareFullyDocumented] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_MedicareFullyDocumented] DEFAULT ((0)),
[MedicareReported] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_MedicareReported] DEFAULT ((0)),
[ReleaseSignedDate] [datetime] NULL,
[ReleaseTypeID] [int] NULL,
[ReleaseTemplateTypeID] [int] NULL,
[MedicareFormSent] [datetime] NULL,
[MedicareFormRcvd] [datetime] NULL,
[SPUsername] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPLDate] [date] NULL,
[CPLAmount] [money] NULL,
[FDLDate] [date] NULL,
[FDLAmount] [money] NULL,
[MedicareSatisfaction] [date] NULL,
[MedicaidLienAmount] [money] NULL,
[PrivateLienAmount] [money] NULL,
[MedicareAdvantageLienAmount] [money] NULL,
[ReleaseFormCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseRcvdDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlePaymentDocumentation] ADD CONSTRAINT [PK_tblSettlePaymentDocumentation] PRIMARY KEY CLUSTERED  ([SetPymtID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_239_238_tblSettlePaymentDocumentation] ON [dbo].[tblSettlePaymentDocumentation] ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_6998_6997_tblSettlePaymentDocumentation] ON [dbo].[tblSettlePaymentDocumentation] ([ClaimLawsuitID]) INCLUDE ([ExecutedReleaseDate], [ReleaseSignedDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_227_226_tblSettlePaymentDocumentation] ON [dbo].[tblSettlePaymentDocumentation] ([ClaimLawsuitID]) INCLUDE ([ExecutedReleaseDate], [ReleaseSignedDate], [ReleaseTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_222_221_tblSettlePaymentDocumentation] ON [dbo].[tblSettlePaymentDocumentation] ([ClaimLawsuitID]) INCLUDE ([ExecutedReleaseDate], [MedicareFormSent], [MedicareFullyDocumented], [MedicareReported], [ReleaseRqstDate], [ReleaseSignedDate], [ReleaseToPC], [ReleaseTypeID], [SetPymtID], [SettlePaymentComments]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_231_230_tblSettlePaymentDocumentation] ON [dbo].[tblSettlePaymentDocumentation] ([ClaimLawsuitID]) INCLUDE ([ReleaseTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_2516_2515_tblSettlePaymentDocumentation] ON [dbo].[tblSettlePaymentDocumentation] ([ClaimLawsuitID], [ExecutedReleaseDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_2520_2519_tblSettlePaymentDocumentation] ON [dbo].[tblSettlePaymentDocumentation] ([ExecutedReleaseDate]) INCLUDE ([ClaimLawsuitID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlePaymentDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlePaymentDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlePaymentDocumentation] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblSettlePaymentDocumentation] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblSettlePaymentDocumentation] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GO
