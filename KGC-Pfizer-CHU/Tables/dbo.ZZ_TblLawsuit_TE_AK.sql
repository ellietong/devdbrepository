CREATE TABLE [dbo].[ZZ_TblLawsuit_TE_AK]
(
[ID] [int] NULL,
[ClaimLawsuitID] [int] NULL,
[LawsuitID] [int] NULL,
[PlaintiffCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselAddressID] [float] NULL,
[PlaintiffCounselID] [int] NULL,
[DocketNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourtType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourTypeID] [int] NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JurisdictionID] [int] NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[ServiceDate] [datetime] NULL,
[DefenseCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[FileDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_TblLawsuit_TE_AK] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_TblLawsuit_TE_AK] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_TblLawsuit_TE_AK] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_TblLawsuit_TE_AK] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_TblLawsuit_TE_AK] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_TblLawsuit_TE_AK] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_TblLawsuit_TE_AK] TO [power_user]
GO
