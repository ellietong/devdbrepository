CREATE TABLE [dbo].[tblSettlementGroupType]
(
[SettlementGroupTypeID] [int] NOT NULL,
[SettlementGroupType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementGroupType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementGroupType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementGroupType] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementGroupType] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementGroupType] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementGroupType] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementGroupType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementGroupType] TO [power_user]
GO
