CREATE TABLE [dbo].[tblCaseDeadline]
(
[CaseDeadlineID] [bigint] NOT NULL IDENTITY(1, 1),
[TrialGroupID] [int] NULL,
[EventTypeID] [int] NOT NULL,
[DetailedEventTypeID] [int] NULL,
[DeadlineDate] [date] NULL,
[DeadlineTime] [time] NULL,
[TimeZoneID] [int] NULL,
[TimePeriodID] [int] NULL,
[EventLocationID] [int] NULL,
[Deponent] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attendees] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttendanceModeID] [int] NULL,
[CreatedBy] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NULL,
[Comments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeadlineStatusID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCaseDeadline] ADD CONSTRAINT [PK_tblCaseDeadline] PRIMARY KEY CLUSTERED  ([CaseDeadlineID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCaseDeadline] TO [base_user]
GRANT SELECT ON  [dbo].[tblCaseDeadline] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCaseDeadline] TO [power_user]
GRANT SELECT ON  [dbo].[tblCaseDeadline] TO [power_user]
GRANT INSERT ON  [dbo].[tblCaseDeadline] TO [power_user]
GRANT DELETE ON  [dbo].[tblCaseDeadline] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCaseDeadline] TO [power_user]
GO
