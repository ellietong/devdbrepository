CREATE TABLE [dbo].[tblSettlementApproverContact]
(
[SettlementApproverContactID] [int] NOT NULL IDENTITY(1, 1),
[PersonID] [int] NOT NULL,
[IsDefault] [bit] NULL CONSTRAINT [DF_tblSettlementApproverContacts_IsActive] DEFAULT ((0)),
[ApprovalRequired_Default] [bit] NOT NULL CONSTRAINT [DF_tblSettlementApproverContacts_ApprovalRequired] DEFAULT ((0)),
[AuthorityAmountThreshold] [money] NOT NULL CONSTRAINT [DF_tblSettlementApproverContacts_AuthorityAmountThreshold] DEFAULT ((0)),
[ResponseWindowDays_Default] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementApproverContact] ADD CONSTRAINT [PK_tblSettlementApproverContacts] PRIMARY KEY CLUSTERED  ([SettlementApproverContactID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementApproverContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementApproverContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementApproverContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementApproverContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementApproverContact] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementApproverContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementApproverContact] TO [power_user]
GO
