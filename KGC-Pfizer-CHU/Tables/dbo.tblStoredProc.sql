CREATE TABLE [dbo].[tblStoredProc]
(
[StoredProcID] [int] NOT NULL IDENTITY(1, 1),
[StoredProcedure] [char] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblStoredProc] ADD CONSTRAINT [PK_TblStoredProc] PRIMARY KEY CLUSTERED  ([StoredProcID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblStoredProc] TO [base_user]
GRANT SELECT ON  [dbo].[tblStoredProc] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblStoredProc] TO [power_user]
GRANT SELECT ON  [dbo].[tblStoredProc] TO [power_user]
GRANT INSERT ON  [dbo].[tblStoredProc] TO [power_user]
GRANT DELETE ON  [dbo].[tblStoredProc] TO [power_user]
GRANT UPDATE ON  [dbo].[tblStoredProc] TO [power_user]
GRANT SELECT ON  [dbo].[tblStoredProc] TO [Reports_User]
GRANT INSERT ON  [dbo].[tblStoredProc] TO [Reports_User]
GRANT DELETE ON  [dbo].[tblStoredProc] TO [Reports_User]
GRANT UPDATE ON  [dbo].[tblStoredProc] TO [Reports_User]
GO
