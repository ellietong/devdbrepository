CREATE TABLE [dbo].['Weitz_New Payments$']
(
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Allocation] [money] NULL,
[Historical Changes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date Approval Initiated for First Payment] [datetime] NULL,
[Amount of First Payment] [money] NULL,
[Date of Approval Initiated for Supplemental First Payment] [datetime] NULL,
[Amount of Supplemental First Payment] [money] NULL,
[Date Approval Initiated for Second Payment] [datetime] NULL,
[Amount of Second Payment] [money] NULL,
[Date Approval Initiated for 100% Payment] [datetime] NULL,
[Amount of 100% Payment] [money] NULL,
[Total Amount of Payments] [money] NULL,
[F14] [money] NULL,
[F15] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[claimantsettlementid] [float] NULL,
[CPI] [float] NULL,
[F18] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[1st payment date] [datetime] NULL,
[1st payment amount] [money] NULL,
[F21] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F22] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F23] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantPersonalInfoID] [float] NULL,
[claimantsettlementid1] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].['Weitz_New Payments$'] TO [base_user]
GRANT SELECT ON  [dbo].['Weitz_New Payments$'] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].['Weitz_New Payments$'] TO [power_user]
GRANT SELECT ON  [dbo].['Weitz_New Payments$'] TO [power_user]
GRANT INSERT ON  [dbo].['Weitz_New Payments$'] TO [power_user]
GRANT DELETE ON  [dbo].['Weitz_New Payments$'] TO [power_user]
GRANT UPDATE ON  [dbo].['Weitz_New Payments$'] TO [power_user]
GO
