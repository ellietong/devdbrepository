CREATE TABLE [dbo].[ZZ_Claims_NOT_Payable_for_2nd]
(
[KCIC_ID2] [int] NULL,
[IP#] [bigint] NULL,
[CSN] [int] NULL,
[Last Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SS#] [int] NULL,
[Grp#] [int] NULL,
[Sett# Group] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sett# Amt] [money] NULL,
[EXCEPTION_NBR] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Payable Exception] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_Claims_NOT_Payable_for_2nd] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_Claims_NOT_Payable_for_2nd] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_Claims_NOT_Payable_for_2nd] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_Claims_NOT_Payable_for_2nd] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_Claims_NOT_Payable_for_2nd] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_Claims_NOT_Payable_for_2nd] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_Claims_NOT_Payable_for_2nd] TO [power_user]
GO
