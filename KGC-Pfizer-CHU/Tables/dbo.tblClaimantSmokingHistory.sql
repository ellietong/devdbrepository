CREATE TABLE [dbo].[tblClaimantSmokingHistory]
(
[ClaimantSmokingHistoryID] [int] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[SmokingStart] [date] NULL,
[SmokingEnd] [date] NULL,
[PacksPerDay] [float] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmokingNotes] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSmokingHistory] ADD CONSTRAINT [PK_tblClaimantSmokingHistory] PRIMARY KEY CLUSTERED  ([ClaimantSmokingHistoryID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSmokingHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSmokingHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSmokingHistory] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantSmokingHistory] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantSmokingHistory] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSmokingHistory] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantSmokingHistory] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantSmokingHistory] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantSmokingHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantSmokingHistory] TO [power_user]
GO
