CREATE TABLE [dbo].[tblAttendanceMode]
(
[AttendanceModeID] [int] NOT NULL IDENTITY(1, 1),
[AttendanceMode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAttendanceMode] ADD CONSTRAINT [PK_tblAttendanceMode] PRIMARY KEY CLUSTERED  ([AttendanceModeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAttendanceMode] TO [base_user]
GRANT SELECT ON  [dbo].[tblAttendanceMode] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAttendanceMode] TO [power_user]
GRANT SELECT ON  [dbo].[tblAttendanceMode] TO [power_user]
GRANT INSERT ON  [dbo].[tblAttendanceMode] TO [power_user]
GRANT DELETE ON  [dbo].[tblAttendanceMode] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAttendanceMode] TO [power_user]
GO
