CREATE TABLE [dbo].[tblSavedSSN]
(
[SavedSSNID] [bigint] NOT NULL IDENTITY(1, 1),
[SSN] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SavedSearchID] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedSSN] ADD CONSTRAINT [PK_tblSavedSSN] PRIMARY KEY CLUSTERED  ([SavedSSNID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedSSN] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedSSN] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedSSN] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedSSN] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedSSN] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedSSN] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedSSN] TO [power_user]
GO
