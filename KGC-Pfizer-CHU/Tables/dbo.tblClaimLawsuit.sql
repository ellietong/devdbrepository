CREATE TABLE [dbo].[tblClaimLawsuit]
(
[ClaimLawsuitID] [bigint] NOT NULL IDENTITY(1, 1),
[LawsuitID] [bigint] NULL,
[IndividualDocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimaryForClaim] [bit] NULL,
[IsMDL] [bit] NULL CONSTRAINT [DF_tblClaimLawsuit_IsMDL] DEFAULT ((0)),
[ClaimID] [bigint] NULL,
[ActiveTrialDate] [date] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrialGroupID] [bigint] NULL,
[LossOfConsortium] [bit] NULL CONSTRAINT [DF_tblClaimLawsuit_LossOfConsortium] DEFAULT ((0)),
[CaseNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [PK_mBtblClaimLawsuitNEW] PRIMARY KEY CLUSTERED  ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimLawsuit_tblClaim] ON [dbo].[tblClaimLawsuit] ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuit_ClaimID_IsMDL] ON [dbo].[tblClaimLawsuit] ([ClaimID], [IsMDL]) INCLUDE ([ClaimLawsuitID], [IsPrimaryForClaim], [LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuit_IsMDL] ON [dbo].[tblClaimLawsuit] ([IsMDL]) INCLUDE ([ClaimID], [ClaimLawsuitID], [IsPrimaryForClaim], [LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimLawsuit_tblLawsuit] ON [dbo].[tblClaimLawsuit] ([LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuit_LawsuitID] ON [dbo].[tblClaimLawsuit] ([LawsuitID]) INCLUDE ([ClaimID], [IsPrimaryForClaim]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [FK_tblClaimLawsuit_tblClaim] FOREIGN KEY ([ClaimID]) REFERENCES [dbo].[tblClaim] ([ClaimID])
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [FK_tblClaimLawsuit_tblLawsuit] FOREIGN KEY ([LawsuitID]) REFERENCES [dbo].[tblLawsuit] ([LawsuitID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuit] TO [power_user]
GO
