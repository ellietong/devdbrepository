CREATE TABLE [dbo].[tblOccurrenceDef]
(
[OccurrenceDefID] [int] NOT NULL,
[OccurrenceDef] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccurrenceDef] ADD CONSTRAINT [PK_tblOccurrenceDef] PRIMARY KEY CLUSTERED  ([OccurrenceDefID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrenceDef] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrenceDef] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrenceDef] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[tblOccurrenceDef] TO [Occurrence_User]
GRANT DELETE ON  [dbo].[tblOccurrenceDef] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[tblOccurrenceDef] TO [Occurrence_User]
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrenceDef] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccurrenceDef] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccurrenceDef] TO [power_user]
GRANT DELETE ON  [dbo].[tblOccurrenceDef] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccurrenceDef] TO [power_user]
GO
