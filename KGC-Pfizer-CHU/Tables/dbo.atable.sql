CREATE TABLE [dbo].[atable]
(
[id] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[atable] TO [base_user]
GRANT SELECT ON  [dbo].[atable] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[atable] TO [power_user]
GRANT SELECT ON  [dbo].[atable] TO [power_user]
GRANT INSERT ON  [dbo].[atable] TO [power_user]
GRANT DELETE ON  [dbo].[atable] TO [power_user]
GRANT UPDATE ON  [dbo].[atable] TO [power_user]
GO
