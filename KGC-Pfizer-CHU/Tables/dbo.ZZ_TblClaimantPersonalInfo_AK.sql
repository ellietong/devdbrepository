CREATE TABLE [dbo].[ZZ_TblClaimantPersonalInfo_AK]
(
[ID] [int] NULL,
[ClaimantPersonalInfoID] [int] NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuffixID] [int] NULL,
[Address1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[Zip] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [datetime] NULL,
[DeceasedDate] [datetime] NULL,
[SSN] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialSecurityNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_TblClaimantPersonalInfo_AK] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_TblClaimantPersonalInfo_AK] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_TblClaimantPersonalInfo_AK] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_TblClaimantPersonalInfo_AK] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_TblClaimantPersonalInfo_AK] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_TblClaimantPersonalInfo_AK] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_TblClaimantPersonalInfo_AK] TO [power_user]
GO
