CREATE TABLE [dbo].[tblExhaustion]
(
[ExhaustionID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionDateID] [int] NOT NULL,
[OccurrenceID] [bigint] NULL,
[PolicyDateID] [int] NOT NULL,
[IndemnityAllocation] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_IndemnityAllocation] DEFAULT ((0)),
[DefenseAllocation] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_DefenseAllocation] DEFAULT ((0)),
[DefenseOutsideLimitsAllocation] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_DefenseOutsideLimitsAllocation] DEFAULT ((0)),
[SIRIndemnityAllocation] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_SIRIndemnityAllocation] DEFAULT ((0)),
[SIRDefenseAllocation] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_SIRDefenseAllocation] DEFAULT ((0)),
[LedgerID] [int] NULL,
[OutsideConsumption] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_OutsideConsumption] DEFAULT ((0)),
[ConsumptionType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustion] ADD CONSTRAINT [PK_tblExhaustion] PRIMARY KEY CLUSTERED  ([ExhaustionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__tblExhaus__Exhau__65ACD3A5] ON [dbo].[tblExhaustion] ([ExhaustionDateID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblExhaustion_tblPolicyDates] ON [dbo].[tblExhaustion] ([PolicyDateID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustion] ADD CONSTRAINT [FK__tblExhaus__Exhau__65ACD3A5] FOREIGN KEY ([ExhaustionDateID]) REFERENCES [dbo].[tblExhaustionDate] ([ExhaustionDateID])
GO
ALTER TABLE [dbo].[tblExhaustion] ADD CONSTRAINT [FK__tblExhaus__Polic__66A0F7DE] FOREIGN KEY ([PolicyDateID]) REFERENCES [dbo].[tblPolicyDates] ([PolicyDateID])
GO
ALTER TABLE [dbo].[tblExhaustion] ADD CONSTRAINT [FK_tblExhaustion_tblPolicyDates] FOREIGN KEY ([PolicyDateID]) REFERENCES [dbo].[tblPolicyDates] ([PolicyDateID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustion] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustion] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustion] TO [Exhaustion_User]
GRANT INSERT ON  [dbo].[tblExhaustion] TO [Exhaustion_User]
GRANT DELETE ON  [dbo].[tblExhaustion] TO [Exhaustion_User]
GRANT UPDATE ON  [dbo].[tblExhaustion] TO [Exhaustion_User]
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustion] TO [power_user]
GRANT SELECT ON  [dbo].[tblExhaustion] TO [power_user]
GRANT INSERT ON  [dbo].[tblExhaustion] TO [power_user]
GRANT DELETE ON  [dbo].[tblExhaustion] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExhaustion] TO [power_user]
GO
