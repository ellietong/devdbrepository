CREATE TABLE [dbo].[m_tblServiceDatePopulate]
(
[AllegationID] [int] NULL,
[ClaimLawsuitID] [int] NULL,
[DefendantCompanyID] [int] NULL,
[DefendantCompany] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceDate1] [date] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblServiceDatePopulate] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblServiceDatePopulate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblServiceDatePopulate] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblServiceDatePopulate] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblServiceDatePopulate] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblServiceDatePopulate] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblServiceDatePopulate] TO [power_user]
GO
