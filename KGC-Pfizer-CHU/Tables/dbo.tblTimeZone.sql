CREATE TABLE [dbo].[tblTimeZone]
(
[TimeZoneID] [int] NOT NULL IDENTITY(1, 1),
[TimeZone] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Abbreviation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTimeZone] ADD CONSTRAINT [PK_tblTimeZone] PRIMARY KEY CLUSTERED  ([TimeZoneID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTimeZone] TO [base_user]
GRANT SELECT ON  [dbo].[tblTimeZone] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTimeZone] TO [power_user]
GRANT SELECT ON  [dbo].[tblTimeZone] TO [power_user]
GRANT INSERT ON  [dbo].[tblTimeZone] TO [power_user]
GRANT DELETE ON  [dbo].[tblTimeZone] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTimeZone] TO [power_user]
GO
