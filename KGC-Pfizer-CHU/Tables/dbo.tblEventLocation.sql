CREATE TABLE [dbo].[tblEventLocation]
(
[EventLocationID] [int] NOT NULL IDENTITY(1, 1),
[LocationName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[Zip] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEventLocation] ADD CONSTRAINT [PK_tblEventLocation] PRIMARY KEY CLUSTERED  ([EventLocationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblEventLocation] TO [base_user]
GRANT SELECT ON  [dbo].[tblEventLocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblEventLocation] TO [power_user]
GRANT SELECT ON  [dbo].[tblEventLocation] TO [power_user]
GRANT INSERT ON  [dbo].[tblEventLocation] TO [power_user]
GRANT DELETE ON  [dbo].[tblEventLocation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblEventLocation] TO [power_user]
GO
