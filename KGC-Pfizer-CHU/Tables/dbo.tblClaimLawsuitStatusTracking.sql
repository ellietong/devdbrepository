CREATE TABLE [dbo].[tblClaimLawsuitStatusTracking]
(
[ClaimLawsuitStatusTrackingID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[ClaimLawsuitStatusID] [int] NULL,
[StatusDate] [datetime] NULL,
[StatusNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimaryStatus] [bit] NULL,
[ProcessedDate] [datetime] NULL,
[ClaimLawsuitStatusIDold] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [PK_tblClaimLawsuitStatusTracking] PRIMARY KEY CLUSTERED  ([ClaimLawsuitStatusTrackingID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuitStatusTracking_ClaimLawsuitID] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitID], [ClaimLawsuitStatusID]) INCLUDE ([IsPrimaryStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK__tblClaimL__Claim__6E4219A6] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitStatusID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuitStatusTracking_ClaimLawsuitStatusID] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitStatusID]) INCLUDE ([ClaimLawsuitID], [IsPrimaryStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_122_121_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitStatusID], [IsPrimaryStatus]) INCLUDE ([ClaimLawsuitID], [ProcessedDate], [StatusDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_119_118_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([IsPrimaryStatus]) INCLUDE ([ClaimLawsuitID], [ClaimLawsuitStatusID], [ProcessedDate], [StatusDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [FK__tblClaimL__Claim__73FAF2FC] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID])
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [FK__tblClaimL__Claim__6E4219A6] FOREIGN KEY ([ClaimLawsuitStatusID]) REFERENCES [dbo].[tblClaimLawsuitStatus] ([ClaimLawsuitStatusID])
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [FK__tblClaimL__Claim__702A6218] FOREIGN KEY ([ClaimLawsuitStatusID]) REFERENCES [dbo].[tblClaimLawsuitStatus] ([ClaimLawsuitStatusID])
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [FK__tblClaimL__Claim__74EF1735] FOREIGN KEY ([ClaimLawsuitStatusID]) REFERENCES [dbo].[tblClaimLawsuitStatus] ([ClaimLawsuitStatusID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GO
