CREATE TABLE [dbo].[tblSuffixIDLookup]
(
[Suffix] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSuffixIDLookup] TO [base_user]
GRANT SELECT ON  [dbo].[tblSuffixIDLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSuffixIDLookup] TO [power_user]
GRANT SELECT ON  [dbo].[tblSuffixIDLookup] TO [power_user]
GRANT INSERT ON  [dbo].[tblSuffixIDLookup] TO [power_user]
GRANT DELETE ON  [dbo].[tblSuffixIDLookup] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSuffixIDLookup] TO [power_user]
GO
