CREATE TABLE [dbo].[tblDocuments]
(
[DocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantDocumentTypeID] [int] NOT NULL,
[DocumentSourceID] [int] NULL,
[HistoricalDocumentID] [bigint] NULL,
[ComplaintID] [bigint] NULL,
[MatterID] [int] NULL,
[DocumentTitle] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmazonKey] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NOT NULL,
[CreatedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateModified] [datetime] NOT NULL,
[ModifiedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Accepted] [bit] NULL,
[DateAccepted] [datetime] NULL,
[AcceptedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsHistorical] [bit] NULL,
[ForQC] [bit] NULL,
[Issues] [bit] NULL,
[Original_Document_Nbr] [int] NULL,
[Original_Document_ID] [int] NULL,
[Original_Document_Type] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Original_Document_type_NBR] [int] NULL,
[Original_FilePath] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Document_Origin] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentDate] [datetime] NULL,
[DismissalForQC] [bit] NULL,
[DismissalQCIssue] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDocuments] ADD CONSTRAINT [PK_tblDocuments_Marley] PRIMARY KEY CLUSTERED  ([DocumentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[tblDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblDocuments] TO [power_user]
GRANT DELETE ON  [dbo].[tblDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Original Pfizer Document ID. Was appended to the document number in the file names we received.', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'Original_Document_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Original Pfizer document number. This is linked to throughout Pfizer''s initial database. It is the first identifier of their files, which were in the format "Document [DocumentNumber]-[DocumentID].[extension]"', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'Original_Document_Nbr'
GO
