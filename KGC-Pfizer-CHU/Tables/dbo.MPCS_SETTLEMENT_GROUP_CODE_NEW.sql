CREATE TABLE [dbo].[MPCS_SETTLEMENT_GROUP_CODE_NEW]
(
[SETTLEMENT_GROUP_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_TIME_STAMP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADD_TIME_STAMP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADD_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESCRIPTION] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLOBAL_INVENTORY_STATUS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GROUP_TYPE_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NUMBER_CLAIMS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLEMENT_AMT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLEMENT_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAIM_TYPE_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAYMENT_TERM_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GRACE_PERIOD_DAYS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLEMENT_SUMMARY] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARALEGAL_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARALEGAL_ASSIGN_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_DOC_RECEIVED_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_DOC_REC_DATE_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELEASE_TRACKOUT_FLAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_DOC_REC_DATE_USER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EXCLUDE_FROM_REPORT_FLAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPGRADE_AMT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_GROUP_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UNDOCUMENTED_FLAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEFICIENCY_REPORT_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REQ_LETT_SENT_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LETTER_SETTLEMENT_FLAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GROUP_PROCESSING_LAW_SUIT_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GROUP_PROCESSING_VERSION_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INITIAL_RESPONSE_DAYS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FOLLOWUP_RESPONSE_DAYS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETT_GROUP_STATUS_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLEMENT_GROUP_ROLLUP_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELEASE_PDF_FLAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNSEL_RELEASE_ALLOWED_FLAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEFICIENCY_WAIVER_TYPE_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELEASE_FILE_TYPE_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEF_COUNSEL_RELEASE_ALLOW_FLAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AGREEMENT_TYPE_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLEMENT_DATE_DateFormatted] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[MPCS_SETTLEMENT_GROUP_CODE_NEW] TO [base_user]
GRANT SELECT ON  [dbo].[MPCS_SETTLEMENT_GROUP_CODE_NEW] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[MPCS_SETTLEMENT_GROUP_CODE_NEW] TO [power_user]
GRANT SELECT ON  [dbo].[MPCS_SETTLEMENT_GROUP_CODE_NEW] TO [power_user]
GRANT INSERT ON  [dbo].[MPCS_SETTLEMENT_GROUP_CODE_NEW] TO [power_user]
GRANT DELETE ON  [dbo].[MPCS_SETTLEMENT_GROUP_CODE_NEW] TO [power_user]
GRANT UPDATE ON  [dbo].[MPCS_SETTLEMENT_GROUP_CODE_NEW] TO [power_user]
GO
