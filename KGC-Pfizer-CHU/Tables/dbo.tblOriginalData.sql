CREATE TABLE [dbo].[tblOriginalData]
(
[CLIENT_CODE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_NAME] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REFERENCE_ID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ID] [decimal] (28, 0) NULL,
[CLAIM_SEQUENCE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUFFIX] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuffID] [int] NULL,
[SOCIAL_SECURITY_NUMBER] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BIRTH_DATE] [datetime] NULL,
[DEATH_DATE] [datetime] NULL,
[PRIMARY_DEFENSE_DISEASE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRIMARY_PLAINTIFF_DISEASE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiseaseCoalesce] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiseaseID] [int] NULL,
[DISEASE_CATEGORY] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRIMARY_JOBSITE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRIMARY_OCCUPATION] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_EXPOSURE_DATE] [datetime] NULL,
[LAST_EXPOSURE_DATE] [datetime] NULL,
[CLAIM_NEW_FILING_DATE] [datetime] NULL,
[CLAIM_FIRST_RESOLUTION_DATE] [datetime] NULL,
[TOTAL_CLAIM_RESOLUTION_AMOUNT] [money] NULL,
[LAWSUIT_ID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CASE_CAPTION] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCKET_NUMBER] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAWSUIT_FILE_DATE] [datetime] NULL,
[LAWSUIT_FIRST_SERVICE_DATE] [datetime] NULL,
[LEAD_PLAINTIFF_FIRST_NAME] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEAD_PLAINTIFF_LAST_NAME] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JurisdictionID] [int] NULL,
[JURISDICTION] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[STATE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourtTypeID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[PRIMARY_DEFENSE_COUNSEL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselAddressID] [int] NULL,
[PRIMARY_PLAINTIFF_COUNSEL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJURED_PARTY_LAWSUIT_STATUS_CATEGORY] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitStatusID] [int] NULL,
[INJURED_PARTY_LAWSUIT_STATUS] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJURED_PARTY_LAWSUIT_STATUS_DATE] [datetime] NULL,
[INJURED_PARTY_LAWSUIT_CREATE_DATE] [datetime] NULL,
[PRIMARY_FOR_CLAIM] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOriginalData] TO [base_user]
GRANT SELECT ON  [dbo].[tblOriginalData] TO [base_user]
GRANT SELECT ON  [dbo].[tblOriginalData] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblOriginalData] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblOriginalData] TO [power_user]
GRANT SELECT ON  [dbo].[tblOriginalData] TO [power_user]
GRANT INSERT ON  [dbo].[tblOriginalData] TO [power_user]
GRANT DELETE ON  [dbo].[tblOriginalData] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOriginalData] TO [power_user]
GO
