CREATE TABLE [dbo].[tblLawsuit]
(
[LawsuitID] [bigint] NOT NULL IDENTITY(1, 1),
[CaseCaption] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[JurisdictionID] [int] NOT NULL,
[StateID] [int] NULL,
[CourtTypeID] [int] NOT NULL,
[ServiceDate] [datetime] NULL,
[ReceivedDate] [datetime] NULL,
[LeadPlaintiffFirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffLastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [int] NULL,
[PlaintiffCounselAddressID] [int] NULL,
[PrimaryNationalPlaintiffCounselID] [int] NULL,
[IsThirdPartyComplaint] [bit] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAWSUIT_NBR] [bigint] NULL,
[VERSION_NBR] [int] NULL,
[Judge] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AnswerFiled] [date] NULL,
[RegionalCounselAddressID] [int] NULL,
[ClientFileDate] [date] NULL,
[LawsuitNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLawsuit] ADD CONSTRAINT [PK__tblLawsuit1__5A1A5A11] PRIMARY KEY CLUSTERED  ([LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblLawsuit_DefenseCounselAddressID] ON [dbo].[tblLawsuit] ([DefenseCounselAddressID]) INCLUDE ([JurisdictionID], [LawsuitID], [StateID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblLawsuit_DefCounselAddress_Docket_State] ON [dbo].[tblLawsuit] ([DefenseCounselAddressID], [DocketNumber], [StateID]) INCLUDE ([JurisdictionID], [LawsuitID], [PlaintiffCounselAddressID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_9294_9293_tblLawsuit] ON [dbo].[tblLawsuit] ([DocketNumber]) INCLUDE ([CaseCaption], [LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblLawsuit_DocketNumber_StateID] ON [dbo].[tblLawsuit] ([DocketNumber], [StateID]) INCLUDE ([LawsuitID], [PlaintiffCounselAddressID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_TBLLAWSUIT_StateIDDocketNumberPlaintiffCounselAddressID] ON [dbo].[tblLawsuit] ([StateID]) INCLUDE ([DocketNumber], [LawsuitID], [PlaintiffCounselAddressID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblLawsuit] TO [power_user]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [power_user]
GRANT INSERT ON  [dbo].[tblLawsuit] TO [power_user]
GRANT DELETE ON  [dbo].[tblLawsuit] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLawsuit] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'JurisdictionID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'JurisdictionID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'StateID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'StateID'
GO
