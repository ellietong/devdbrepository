CREATE TABLE [dbo].[ZZ_TblClaimLawsuit_TE_AK]
(
[ID] [int] NULL,
[ClaimID] [int] NULL,
[ClaimLawsuitID] [int] NULL,
[LawsuitID] [int] NULL,
[ActiveTrialDate] [datetime] NULL,
[LossOfConsortium] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CaseNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_TblClaimLawsuit_TE_AK] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_TblClaimLawsuit_TE_AK] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_TblClaimLawsuit_TE_AK] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_TblClaimLawsuit_TE_AK] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_TblClaimLawsuit_TE_AK] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_TblClaimLawsuit_TE_AK] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_TblClaimLawsuit_TE_AK] TO [power_user]
GO
