CREATE TABLE [dbo].[tblSavedLawsuitStatus]
(
[SavedLawsuitStatusID] [bigint] NOT NULL IDENTITY(1, 1),
[LawsuitStatusID] [int] NULL,
[SavedSearchID] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedLawsuitStatus] ADD CONSTRAINT [PK_tblSavedLawsuitStatus] PRIMARY KEY CLUSTERED  ([SavedLawsuitStatusID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedLawsuitStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedLawsuitStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedLawsuitStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedLawsuitStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedLawsuitStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedLawsuitStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedLawsuitStatus] TO [power_user]
GO
