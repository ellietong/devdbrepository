CREATE TABLE [dbo].[m_tblTuckerEllisLawsuitUpdateAdHoc]
(
[ClaimLawsuitID] [int] NULL,
[LawsuitID] [int] NULL,
[NewLawsuitID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblTuckerEllisLawsuitUpdateAdHoc] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblTuckerEllisLawsuitUpdateAdHoc] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblTuckerEllisLawsuitUpdateAdHoc] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblTuckerEllisLawsuitUpdateAdHoc] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblTuckerEllisLawsuitUpdateAdHoc] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblTuckerEllisLawsuitUpdateAdHoc] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblTuckerEllisLawsuitUpdateAdHoc] TO [power_user]
GO
