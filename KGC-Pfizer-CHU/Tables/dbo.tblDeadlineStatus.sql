CREATE TABLE [dbo].[tblDeadlineStatus]
(
[DeadlineStatusID] [int] NOT NULL IDENTITY(1, 1),
[DeadlineStatus] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDeadlineStatus] ADD CONSTRAINT [PK_tblDeadlineStatus] PRIMARY KEY CLUSTERED  ([DeadlineStatusID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDeadlineStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblDeadlineStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDeadlineStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblDeadlineStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblDeadlineStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblDeadlineStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDeadlineStatus] TO [power_user]
GO
