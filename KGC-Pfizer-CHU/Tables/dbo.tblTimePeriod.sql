CREATE TABLE [dbo].[tblTimePeriod]
(
[TimePeriodID] [int] NOT NULL IDENTITY(1, 1),
[TimePeriod] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTimePeriod] ADD CONSTRAINT [PK_tblTimePeriod] PRIMARY KEY CLUSTERED  ([TimePeriodID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTimePeriod] TO [base_user]
GRANT SELECT ON  [dbo].[tblTimePeriod] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTimePeriod] TO [power_user]
GRANT SELECT ON  [dbo].[tblTimePeriod] TO [power_user]
GRANT INSERT ON  [dbo].[tblTimePeriod] TO [power_user]
GRANT DELETE ON  [dbo].[tblTimePeriod] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTimePeriod] TO [power_user]
GO
