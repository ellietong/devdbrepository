CREATE TABLE [dbo].[tblSavedSettlementGroupType]
(
[SavedSettlementGroupTypeID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementGroupTypeID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedSettlementGroupType] ADD CONSTRAINT [PK_tblSavedSettlementGroupType] PRIMARY KEY CLUSTERED  ([SavedSettlementGroupTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedSettlementGroupType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedSettlementGroupType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedSettlementGroupType] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedSettlementGroupType] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedSettlementGroupType] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedSettlementGroupType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedSettlementGroupType] TO [power_user]
GO
