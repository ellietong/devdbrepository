CREATE TABLE [dbo].[tblSavedMatter]
(
[SavedMatterID] [bigint] NOT NULL IDENTITY(1, 1),
[MatterID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedMatter] ADD CONSTRAINT [PK_tblSavedMatter] PRIMARY KEY CLUSTERED  ([SavedMatterID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedMatter] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedMatter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedMatter] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedMatter] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedMatter] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedMatter] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedMatter] TO [power_user]
GO
