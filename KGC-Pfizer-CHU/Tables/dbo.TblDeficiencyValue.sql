CREATE TABLE [dbo].[TblDeficiencyValue]
(
[DeficiencyValueID] [bigint] NOT NULL IDENTITY(1, 1),
[Deficiency] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TblDeficiencyValue] ADD CONSTRAINT [PK_TblDeficiencyValue] PRIMARY KEY CLUSTERED  ([DeficiencyValueID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TblDeficiencyValue] TO [base_user]
GRANT SELECT ON  [dbo].[TblDeficiencyValue] TO [base_user]
GRANT ALTER ON  [dbo].[TblDeficiencyValue] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[TblDeficiencyValue] TO [power_user]
GRANT SELECT ON  [dbo].[TblDeficiencyValue] TO [power_user]
GRANT INSERT ON  [dbo].[TblDeficiencyValue] TO [power_user]
GRANT DELETE ON  [dbo].[TblDeficiencyValue] TO [power_user]
GRANT UPDATE ON  [dbo].[TblDeficiencyValue] TO [power_user]
GO
