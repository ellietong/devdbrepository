CREATE TABLE [dbo].[ZZ_Temp_SettlementGroupImport]
(
[SettlementGroupID] [float] NULL,
[SettlementGroup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementGroupTypeID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_Temp_SettlementGroupImport] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_Temp_SettlementGroupImport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_Temp_SettlementGroupImport] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_Temp_SettlementGroupImport] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_Temp_SettlementGroupImport] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_Temp_SettlementGroupImport] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_Temp_SettlementGroupImport] TO [power_user]
GO
