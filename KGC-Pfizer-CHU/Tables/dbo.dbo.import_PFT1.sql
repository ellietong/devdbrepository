CREATE TABLE [dbo].[dbo.import_PFT1]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TEST_DATE] [datetime] NULL,
[PHYSICIAN_NAME] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FVC_ACTUAL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FVC_PREDICTED] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FEV1_ACTUAL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FEV1_PREDICTED] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FEV1_FVC_ACTUAL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FEV1_FVC_PREDICTED] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLC_ACTUAL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TLC_PREDICTED] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DLCO_ACTUAL] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DLCO_PREDICTED] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TV] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[dbo.import_PFT1] TO [base_user]
GRANT SELECT ON  [dbo].[dbo.import_PFT1] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[dbo.import_PFT1] TO [power_user]
GRANT SELECT ON  [dbo].[dbo.import_PFT1] TO [power_user]
GRANT INSERT ON  [dbo].[dbo.import_PFT1] TO [power_user]
GRANT DELETE ON  [dbo].[dbo.import_PFT1] TO [power_user]
GRANT UPDATE ON  [dbo].[dbo.import_PFT1] TO [power_user]
GO
