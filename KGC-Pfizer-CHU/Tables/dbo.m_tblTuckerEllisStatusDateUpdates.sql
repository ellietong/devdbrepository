CREATE TABLE [dbo].[m_tblTuckerEllisStatusDateUpdates]
(
[ClaimLawsuitID] [float] NULL,
[DismissalDate] [datetime] NULL,
[KCIC Dismissal Date] [datetime] NULL,
[StatusNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblTuckerEllisStatusDateUpdates] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblTuckerEllisStatusDateUpdates] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblTuckerEllisStatusDateUpdates] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblTuckerEllisStatusDateUpdates] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblTuckerEllisStatusDateUpdates] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblTuckerEllisStatusDateUpdates] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblTuckerEllisStatusDateUpdates] TO [power_user]
GO
