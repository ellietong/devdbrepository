CREATE TABLE [dbo].[tblSavedClaimType]
(
[SavedClaimTypeID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimTypeID] [int] NOT NULL,
[SavedSearchID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedClaimType] ADD CONSTRAINT [PK_tblSavedClaimType] PRIMARY KEY CLUSTERED  ([SavedClaimTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedClaimType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedClaimType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedClaimType] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedClaimType] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedClaimType] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedClaimType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedClaimType] TO [power_user]
GO
