CREATE TABLE [dbo].[tblSettlementNotificationContactState]
(
[SettlementNotificationContactStateID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementNotificationContactID] [bigint] NOT NULL,
[StateID] [int] NOT NULL,
[JurisdictionID] [int] NULL,
[IsDefault] [bit] NOT NULL CONSTRAINT [DF_tblSettlementApproverContactStateID_IsDefault] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementNotificationContactState] ADD CONSTRAINT [PK_tblSettlementApproverContactStateID] PRIMARY KEY CLUSTERED  ([SettlementNotificationContactStateID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementNotificationContactState] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementNotificationContactState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementNotificationContactState] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementNotificationContactState] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementNotificationContactState] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementNotificationContactState] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementNotificationContactState] TO [power_user]
GO
