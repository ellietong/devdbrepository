CREATE TABLE [dbo].[ZZ_Gsettlement]
(
[ClaimantPersonalInfoID] [int] NULL,
[ClaimantSettlementID] [int] NULL,
[Payment Amount] [money] NULL,
[SettlementAmount] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_Gsettlement] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_Gsettlement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_Gsettlement] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_Gsettlement] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_Gsettlement] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_Gsettlement] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_Gsettlement] TO [power_user]
GO
