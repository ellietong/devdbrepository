CREATE TABLE [dbo].[tblAllocationResults]
(
[AllocationResultID] [bigint] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[PolicyID] [int] NULL,
[PolicyDateID] [bigint] NULL,
[PStartSplit] [datetime] NULL,
[PEndSplit] [datetime] NULL,
[AnnualPeriodSplit] [int] NULL,
[Allocation] [float] NULL,
[AllocationIndemnity] [float] NULL,
[AllocationDefense] [float] NULL,
[AllocationDefenseOutsideLimits] [float] NULL,
[OccurrenceID] [bigint] NULL,
[TriggerStartDateUsed] [datetime] NULL,
[TriggerEndDateUsed] [datetime] NULL,
[SIRAllocated] [float] NULL,
[SIRAllocatedIndemnity] [float] NULL,
[SIRAllocatedDefense] [float] NULL,
[SIRDiscount] [float] NULL,
[DateTime] [datetime] NULL,
[PV] [decimal] (38, 15) NULL,
[DateToPresentValue] [datetime] NULL,
[PVRate] [decimal] (18, 15) NULL,
[YieldCurveID] [int] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblAllocationResults_tblAllocation] ON [dbo].[tblAllocationResults] ([AllocationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblAllocationResults] ON [dbo].[tblAllocationResults] ([AllocationResultID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_allocationid] ON [dbo].[tblAllocationResults] ([AllocationResultID], [AllocationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_psplitstart] ON [dbo].[tblAllocationResults] ([PStartSplit], [PEndSplit]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResults] ADD CONSTRAINT [FK_tblAllocationResults_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblAllocationResults] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationResults] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationResults] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationResults] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResults] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResults] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResults] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationResults] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationResults] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationResults] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationResults] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResults', 'COLUMN', N'AllocationResultID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResults', 'COLUMN', N'OccurrenceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResults', 'COLUMN', N'OccurrenceID'
GO
