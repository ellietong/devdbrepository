CREATE TABLE [dbo].[tblSettleDocs]
(
[SettleDocID] [bigint] NOT NULL IDENTITY(1, 1),
[SetPymtID] [bigint] NULL,
[DocumentID] [bigint] NULL,
[ClaimantDocumentID] [bigint] NULL,
[DocCategoryID] [int] NULL,
[PageNums] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettleDocs] ADD CONSTRAINT [PK_tblSettleDocs] PRIMARY KEY CLUSTERED  ([SettleDocID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_224_223_tblSettleDocs] ON [dbo].[tblSettleDocs] ([SetPymtID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettleDocs] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettleDocs] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettleDocs] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblSettleDocs] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblSettleDocs] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblSettleDocs] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettleDocs] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettleDocs] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettleDocs] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettleDocs] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettleDocs] TO [power_user]
GO
