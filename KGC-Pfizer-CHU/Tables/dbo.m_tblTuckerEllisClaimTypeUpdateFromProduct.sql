CREATE TABLE [dbo].[m_tblTuckerEllisClaimTypeUpdateFromProduct]
(
[NewClaimID] [int] NULL,
[ClaimTypeID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblTuckerEllisClaimTypeUpdateFromProduct] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblTuckerEllisClaimTypeUpdateFromProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblTuckerEllisClaimTypeUpdateFromProduct] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblTuckerEllisClaimTypeUpdateFromProduct] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblTuckerEllisClaimTypeUpdateFromProduct] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblTuckerEllisClaimTypeUpdateFromProduct] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblTuckerEllisClaimTypeUpdateFromProduct] TO [power_user]
GO
