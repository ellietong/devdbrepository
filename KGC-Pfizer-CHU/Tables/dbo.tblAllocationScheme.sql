CREATE TABLE [dbo].[tblAllocationScheme]
(
[AllocationSchemeID] [int] NOT NULL IDENTITY(1, 1),
[AllocationScheme] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationScheme] ADD CONSTRAINT [PK_tblAllocationScheme] PRIMARY KEY NONCLUSTERED  ([AllocationSchemeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationScheme] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationScheme] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationScheme] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationScheme] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationScheme] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationScheme] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationScheme] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationScheme] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationScheme] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationScheme] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationScheme] TO [power_user]
GO
