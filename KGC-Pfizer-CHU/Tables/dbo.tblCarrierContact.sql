CREATE TABLE [dbo].[tblCarrierContact]
(
[CarrierContactID] [int] NOT NULL IDENTITY(1, 1),
[CarrierID] [int] NOT NULL,
[ContactID] [int] NOT NULL,
[CurrentContact] [bit] NOT NULL CONSTRAINT [DF_tblCarrierContact_CurrentContact] DEFAULT ((0)),
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCarrierContact] ADD CONSTRAINT [PK_tblCarrierContact] PRIMARY KEY CLUSTERED  ([CarrierContactID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCarrierContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblCarrierContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblCarrierContact] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblCarrierContact] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblCarrierContact] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblCarrierContact] TO [Notice_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCarrierContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblCarrierContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblCarrierContact] TO [power_user]
GRANT DELETE ON  [dbo].[tblCarrierContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCarrierContact] TO [power_user]
GO
