CREATE TABLE [dbo].[ZZ_TblClaimantRelatedParty_AK]
(
[ID] [int] NULL,
[ClaimantPersonalInfoID] [int] NULL,
[SpouseName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RelationshipID] [int] NULL,
[BirthDate] [datetime] NULL,
[SpouseSSN] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialSecurityNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_TblClaimantRelatedParty_AK] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_TblClaimantRelatedParty_AK] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_TblClaimantRelatedParty_AK] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_TblClaimantRelatedParty_AK] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_TblClaimantRelatedParty_AK] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_TblClaimantRelatedParty_AK] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_TblClaimantRelatedParty_AK] TO [power_user]
GO
