CREATE TABLE [dbo].[tblClaimantSettlementStatus]
(
[ClaimantSettlementStatusID] [int] NOT NULL,
[ClaimantSettlementStatus] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSettlementStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlementStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSettlementStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlementStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantSettlementStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantSettlementStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantSettlementStatus] TO [power_user]
GO
