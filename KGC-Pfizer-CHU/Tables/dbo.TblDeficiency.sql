CREATE TABLE [dbo].[TblDeficiency]
(
[DeficiencyID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantSettlementID] [bigint] NULL,
[DeficiencyValueID] [bigint] NULL,
[Comments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeficiencyCuredID] [bigint] NULL,
[DateCured] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TblDeficiency] ADD CONSTRAINT [PK_TblDeficiency] PRIMARY KEY CLUSTERED  ([DeficiencyID]) ON [PRIMARY]
GO
GRANT ALTER ON  [dbo].[TblDeficiency] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TblDeficiency] TO [base_user]
GRANT SELECT ON  [dbo].[TblDeficiency] TO [base_user]
GRANT UPDATE ON  [dbo].[TblDeficiency] TO [base_user]
GRANT ALTER ON  [dbo].[TblDeficiency] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[TblDeficiency] TO [power_user]
GRANT SELECT ON  [dbo].[TblDeficiency] TO [power_user]
GRANT INSERT ON  [dbo].[TblDeficiency] TO [power_user]
GRANT DELETE ON  [dbo].[TblDeficiency] TO [power_user]
GRANT UPDATE ON  [dbo].[TblDeficiency] TO [power_user]
GO
