CREATE TABLE [dbo].[tblSettlementNotificationContact]
(
[SettlementNotificationContactID] [bigint] NOT NULL IDENTITY(1, 1),
[PersonID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementNotificationContact] ADD CONSTRAINT [PK_tblSettlementNotificationContact] PRIMARY KEY CLUSTERED  ([SettlementNotificationContactID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementNotificationContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementNotificationContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementNotificationContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementNotificationContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementNotificationContact] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementNotificationContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementNotificationContact] TO [power_user]
GO
