CREATE TABLE [dbo].[tblAllegation]
(
[AllegationID] [int] NOT NULL IDENTITY(1, 1),
[DefendantCompanyID] [int] NULL,
[AllegationTypeID] [int] NULL,
[MaterialID] [int] NULL,
[DamageID] [int] NULL,
[ClaimLawsuitID] [bigint] NULL,
[ServiceDate] [date] NULL,
[TenderDate] [date] NULL,
[PfizerFileDate] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegation] ADD CONSTRAINT [PK__tblAlleg__8BB43A0FC00131E6] PRIMARY KEY CLUSTERED  ([AllegationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblAllegation_TEMP] ON [dbo].[tblAllegation] ([AllegationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegation] ADD CONSTRAINT [FK_tblAllegationNEW_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllegation] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllegation] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllegation] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllegation] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllegation] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllegation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllegation] TO [power_user]
GO
