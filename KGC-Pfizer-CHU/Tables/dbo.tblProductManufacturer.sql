CREATE TABLE [dbo].[tblProductManufacturer]
(
[ProductManufacturerID] [int] NOT NULL IDENTITY(1, 1),
[ProductManufacturer] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProductManufacturer] ADD CONSTRAINT [PK_tblClaimantProductManufacturer] PRIMARY KEY CLUSTERED  ([ProductManufacturerID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProductManufacturer] TO [base_user]
GRANT SELECT ON  [dbo].[tblProductManufacturer] TO [base_user]
GRANT SELECT ON  [dbo].[tblProductManufacturer] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblProductManufacturer] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblProductManufacturer] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblProductManufacturer] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblProductManufacturer] TO [power_user]
GRANT SELECT ON  [dbo].[tblProductManufacturer] TO [power_user]
GRANT INSERT ON  [dbo].[tblProductManufacturer] TO [power_user]
GRANT DELETE ON  [dbo].[tblProductManufacturer] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProductManufacturer] TO [power_user]
GO
