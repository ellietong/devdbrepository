CREATE TABLE [dbo].[tblDetailedEventType]
(
[DetailedEventTypeID] [int] NOT NULL IDENTITY(1, 1),
[DetailedEventType] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeadlineTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDetailedEventType] ADD CONSTRAINT [PK_tblEventDetail] PRIMARY KEY CLUSTERED  ([DetailedEventTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDetailedEventType] TO [base_user]
GRANT SELECT ON  [dbo].[tblDetailedEventType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDetailedEventType] TO [power_user]
GRANT SELECT ON  [dbo].[tblDetailedEventType] TO [power_user]
GRANT INSERT ON  [dbo].[tblDetailedEventType] TO [power_user]
GRANT DELETE ON  [dbo].[tblDetailedEventType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDetailedEventType] TO [power_user]
GO
