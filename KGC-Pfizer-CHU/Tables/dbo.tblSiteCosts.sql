CREATE TABLE [dbo].[tblSiteCosts]
(
[CostRecordID] [int] NOT NULL IDENTITY(1, 1),
[SiteID] [int] NOT NULL,
[DocumentTypeID] [int] NOT NULL,
[VendorID] [int] NULL,
[InvoiceDate] [smalldatetime] NULL,
[PaidDate] [smalldatetime] NULL,
[InvoiceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WONumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobNumber] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostTypeID] [int] NOT NULL CONSTRAINT [DF_tblSiteCosts_CostTypeID] DEFAULT ((10)),
[Notes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReimbursableAmount] [money] NOT NULL,
[WorkDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayerID] [int] NULL,
[Billed] [bit] NOT NULL,
[DateBilled] [smalldatetime] NULL,
[InvoiceAmount] [money] NULL,
[PercentReimbursable] [float] NULL,
[CostExclusionTypeID] [int] NULL,
[ExclusionNotes] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordSourceID] [int] NULL,
[InvoicePDFLink] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcludedCost] [bit] NOT NULL CONSTRAINT [DF_tblSiteCosts_ExcludedCost] DEFAULT ((0)),
[CostID] [int] NULL,
[Recoverable] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSiteCosts] ADD CONSTRAINT [PK_tblSiteCosts] PRIMARY KEY CLUSTERED  ([CostRecordID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSiteCosts] TO [base_user]
GRANT SELECT ON  [dbo].[tblSiteCosts] TO [base_user]
GRANT SELECT ON  [dbo].[tblSiteCosts] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblSiteCosts] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblSiteCosts] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblSiteCosts] TO [Enviro_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSiteCosts] TO [power_user]
GRANT SELECT ON  [dbo].[tblSiteCosts] TO [power_user]
GRANT INSERT ON  [dbo].[tblSiteCosts] TO [power_user]
GRANT DELETE ON  [dbo].[tblSiteCosts] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSiteCosts] TO [power_user]
GO
