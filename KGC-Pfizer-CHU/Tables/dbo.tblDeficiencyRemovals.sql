CREATE TABLE [dbo].[tblDeficiencyRemovals]
(
[DeficiencyRemovalID] [bigint] NOT NULL IDENTITY(1, 1),
[LastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialSecurityNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJURED_PARTY_ID] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAIM_SEQUENCE_NBR] [int] NULL,
[SettlementAmount] [money] NULL,
[DeficiencyValueID] [bigint] NULL,
[Comments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeficiencyCuredID] [bigint] NULL,
[DateCured] [datetime] NULL,
[SettlementGroupID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDeficiencyRemovals] ADD CONSTRAINT [PK_tblDeficiencyRemovals] PRIMARY KEY CLUSTERED  ([DeficiencyRemovalID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDeficiencyRemovals] TO [base_user]
GRANT SELECT ON  [dbo].[tblDeficiencyRemovals] TO [base_user]
GRANT ALTER ON  [dbo].[tblDeficiencyRemovals] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDeficiencyRemovals] TO [power_user]
GRANT SELECT ON  [dbo].[tblDeficiencyRemovals] TO [power_user]
GRANT INSERT ON  [dbo].[tblDeficiencyRemovals] TO [power_user]
GRANT DELETE ON  [dbo].[tblDeficiencyRemovals] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDeficiencyRemovals] TO [power_user]
GO
