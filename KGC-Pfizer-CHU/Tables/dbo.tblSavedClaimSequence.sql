CREATE TABLE [dbo].[tblSavedClaimSequence]
(
[SavedClaimSequenceID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimSequenceID] [int] NOT NULL,
[SavedSearchID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedClaimSequence] ADD CONSTRAINT [PK_tblSavedClaimSequence] PRIMARY KEY CLUSTERED  ([SavedClaimSequenceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedClaimSequence] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedClaimSequence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedClaimSequence] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedClaimSequence] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedClaimSequence] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedClaimSequence] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedClaimSequence] TO [power_user]
GO
