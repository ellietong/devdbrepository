CREATE TABLE [dbo].[tblClaimantPersonalInfo]
(
[ClaimantPersonalInfoID] [bigint] NOT NULL IDENTITY(1, 1),
[FirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialSecurityNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeceasedDate] [datetime] NULL,
[MaritalStatusID] [int] NULL,
[GenderID] [int] NULL,
[BirthDate] [datetime] NULL,
[SuffixID] [int] NULL,
[HealthInsuranceClaimNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicareBeneficiaryStatusID] [int] NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[Zip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MailZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [bigint] NULL,
[Extension] [int] NULL,
[CMSQueryDate] [datetime] NULL,
[IsSmoker] [bit] NULL,
[SmokerStart] [datetime] NULL,
[SmokerEnd] [datetime] NULL,
[PacksPerDay] [float] NULL,
[INJURED_PARTY_ID] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MarriageDate] [date] NULL,
[ClaimantNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullName] AS ((coalesce([LastName]+', ','')+coalesce([FirstName],''))+coalesce(space((1))+[MiddleName],''))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantPersonalInfo] ADD CONSTRAINT [PK_tblClaimantPersonalInfo] PRIMARY KEY CLUSTERED  ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantPersonalInfo] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantPersonalInfo] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantPersonalInfo] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantPersonalInfo] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantPersonalInfo] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantPersonalInfo] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantPersonalInfo] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantPersonalInfo] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantPersonalInfo] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantPersonalInfo] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantPersonalInfo] TO [power_user]
GO
CREATE FULLTEXT INDEX ON [dbo].[tblClaimantPersonalInfo] KEY INDEX [PK_tblClaimantPersonalInfo] ON [ClaimantPersonalInfo]
GO
ALTER FULLTEXT INDEX ON [dbo].[tblClaimantPersonalInfo] ADD ([FirstName] LANGUAGE 1033)
GO
ALTER FULLTEXT INDEX ON [dbo].[tblClaimantPersonalInfo] ADD ([MiddleName] LANGUAGE 1033)
GO
ALTER FULLTEXT INDEX ON [dbo].[tblClaimantPersonalInfo] ADD ([LastName] LANGUAGE 1033)
GO
ALTER FULLTEXT INDEX ON [dbo].[tblClaimantPersonalInfo] ADD ([FullName] LANGUAGE 1033)
GO
