CREATE TABLE [dbo].[tblClaimTypeValue]
(
[ClaimTypeValueID] [int] NOT NULL IDENTITY(1, 1),
[ClaimTypeValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryClaimTypeRank] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimTypeValue] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimTypeValue] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimTypeValue] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimTypeValue] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimTypeValue] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimTypeValue] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimTypeValue] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimTypeValue] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimTypeValue] TO [power_user]
GO
