CREATE TABLE [dbo].[tblPlaintiffCounsel]
(
[PlaintiffCounselID] [bigint] NOT NULL IDENTITY(1, 1),
[PlaintiffCounsel] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselTaxID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPlaintiffCounsel] ADD CONSTRAINT [PK_tblPlaintiffCounsel] PRIMARY KEY CLUSTERED  ([PlaintiffCounselID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounsel] TO [power_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel] TO [power_user]
GRANT INSERT ON  [dbo].[tblPlaintiffCounsel] TO [power_user]
GRANT DELETE ON  [dbo].[tblPlaintiffCounsel] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPlaintiffCounsel] TO [power_user]
GO
