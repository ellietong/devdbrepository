CREATE TABLE [dbo].[mtblLawsuitCounsel_CHU]
(
[LawsuitID] [int] NULL,
[LAWSUIT_NBR] [bigint] NULL,
[CLIENT_COMPANY_NBR] [int] NULL,
[DEFENSE_LAW_FIRM_NBR] [int] NULL,
[DESCRIPTION] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VERSION_NBR] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblLawsuitCounsel_CHU] TO [base_user]
GRANT SELECT ON  [dbo].[mtblLawsuitCounsel_CHU] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[mtblLawsuitCounsel_CHU] TO [power_user]
GRANT SELECT ON  [dbo].[mtblLawsuitCounsel_CHU] TO [power_user]
GRANT INSERT ON  [dbo].[mtblLawsuitCounsel_CHU] TO [power_user]
GRANT DELETE ON  [dbo].[mtblLawsuitCounsel_CHU] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblLawsuitCounsel_CHU] TO [power_user]
GO
