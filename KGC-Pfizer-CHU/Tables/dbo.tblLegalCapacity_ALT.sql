CREATE TABLE [dbo].[tblLegalCapacity_ALT]
(
[LegalCapacityID] [int] NULL,
[LegalCapacity] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicareRelationshipID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLegalCapacity_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblLegalCapacity_ALT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLegalCapacity_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblLegalCapacity_ALT] TO [power_user]
GRANT INSERT ON  [dbo].[tblLegalCapacity_ALT] TO [power_user]
GRANT DELETE ON  [dbo].[tblLegalCapacity_ALT] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLegalCapacity_ALT] TO [power_user]
GO
