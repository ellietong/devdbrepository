CREATE TABLE [dbo].[tblClaimantProduct]
(
[ClaimantProductID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantEmploymentID] [bigint] NULL,
[ProductID] [int] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantProduct] ADD CONSTRAINT [PK__mBtblCla__3B9049C42E06CDA9] PRIMARY KEY CLUSTERED  ([ClaimantProductID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimantProduct_tblClaimantProduct] ON [dbo].[tblClaimantProduct] ([ClaimantEmploymentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_Claimantproduct_ClaimantEmployment] ON [dbo].[tblClaimantProduct] ([ClaimantEmploymentID]) INCLUDE ([StartDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantProduct] ADD CONSTRAINT [FK_tblClaimantProduct_tblClaimantProduct] FOREIGN KEY ([ClaimantEmploymentID]) REFERENCES [dbo].[tblClaimantEmployment] ([ClaimantEmploymentID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantProduct] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantProduct] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantProduct] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'ClaimantProductID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'ClaimantProductID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'ClaimantProductID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'EndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'EndDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'EndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'StartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'StartDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'StartDate'
GO
