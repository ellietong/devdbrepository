CREATE TABLE [dbo].[ZZ_Employment_AK]
(
[ClaimantPersonalInfoID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_Employment_AK] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_Employment_AK] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_Employment_AK] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_Employment_AK] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_Employment_AK] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_Employment_AK] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_Employment_AK] TO [power_user]
GO
