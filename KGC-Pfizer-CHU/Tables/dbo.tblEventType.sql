CREATE TABLE [dbo].[tblEventType]
(
[EventTypeID] [int] NOT NULL IDENTITY(1, 1),
[EventType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeadlineTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEventType] ADD CONSTRAINT [PK_tblEventType] PRIMARY KEY CLUSTERED  ([EventTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblEventType] TO [base_user]
GRANT SELECT ON  [dbo].[tblEventType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblEventType] TO [power_user]
GRANT SELECT ON  [dbo].[tblEventType] TO [power_user]
GRANT INSERT ON  [dbo].[tblEventType] TO [power_user]
GRANT DELETE ON  [dbo].[tblEventType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblEventType] TO [power_user]
GO
