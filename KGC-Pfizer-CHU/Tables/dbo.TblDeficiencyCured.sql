CREATE TABLE [dbo].[TblDeficiencyCured]
(
[DeficiencyCuredID] [bigint] NOT NULL IDENTITY(1, 1),
[DeficiencyCured] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TblDeficiencyCured] ADD CONSTRAINT [PK_TblDeficiencyCured] PRIMARY KEY CLUSTERED  ([DeficiencyCuredID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TblDeficiencyCured] TO [base_user]
GRANT SELECT ON  [dbo].[TblDeficiencyCured] TO [base_user]
GRANT ALTER ON  [dbo].[TblDeficiencyCured] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[TblDeficiencyCured] TO [power_user]
GRANT REFERENCES ON  [dbo].[TblDeficiencyCured] TO [power_user]
GRANT SELECT ON  [dbo].[TblDeficiencyCured] TO [power_user]
GRANT INSERT ON  [dbo].[TblDeficiencyCured] TO [power_user]
GRANT DELETE ON  [dbo].[TblDeficiencyCured] TO [power_user]
GRANT UPDATE ON  [dbo].[TblDeficiencyCured] TO [power_user]
GO
