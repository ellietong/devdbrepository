CREATE TABLE [dbo].[tblRlsRelationStatus]
(
[RlsRelationStatusID] [int] NOT NULL IDENTITY(1, 1),
[RlsRelationStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRlsRelationStatus] ADD CONSTRAINT [PK_tblRlsRelationStatus] PRIMARY KEY CLUSTERED  ([RlsRelationStatusID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblRlsRelationStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblRlsRelationStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblRlsRelationStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblRlsRelationStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblRlsRelationStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblRlsRelationStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblRlsRelationStatus] TO [power_user]
GO
