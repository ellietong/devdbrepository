CREATE TABLE [dbo].[tblSiteTransfer]
(
[TransferID] [int] NOT NULL IDENTITY(1, 1),
[SiteID] [int] NULL,
[CorpHistoryID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSiteTransfer] ADD CONSTRAINT [PK_mEtblSiteTransfer] PRIMARY KEY CLUSTERED  ([TransferID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblSiteTransfer_tblEnviroSitesDetail] ON [dbo].[tblSiteTransfer] ([SiteID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSiteTransfer] ADD CONSTRAINT [FK_tblSiteTransfer_tblEnviroSitesDetail] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[tblEnviroSitesDetail] ([SiteID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSiteTransfer] TO [base_user]
GRANT SELECT ON  [dbo].[tblSiteTransfer] TO [base_user]
GRANT SELECT ON  [dbo].[tblSiteTransfer] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblSiteTransfer] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblSiteTransfer] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblSiteTransfer] TO [Enviro_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSiteTransfer] TO [power_user]
GRANT SELECT ON  [dbo].[tblSiteTransfer] TO [power_user]
GRANT INSERT ON  [dbo].[tblSiteTransfer] TO [power_user]
GRANT DELETE ON  [dbo].[tblSiteTransfer] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSiteTransfer] TO [power_user]
GO
