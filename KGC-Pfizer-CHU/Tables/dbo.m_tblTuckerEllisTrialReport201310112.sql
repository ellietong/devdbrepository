CREATE TABLE [dbo].[m_tblTuckerEllisTrialReport201310112]
(
[ClaimantPersonalInfoID] [float] NULL,
[ClaimID] [float] NULL,
[NewClaimID] [float] NULL,
[count] [float] NULL,
[ClaimLawsuitID] [float] NULL,
[TE_ID] [float] NULL,
[PLAINTIFFNAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLAINTIFFCOUNSEL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCALDEFENSECOUNSEL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TOTALPLTFS] [float] NULL,
[ActiveTrialDate] [datetime] NULL,
[Updated Active Trial Date] [datetime] NULL,
[WORKSite] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISEASE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimTypeID] [float] NULL,
[ClaimSequenceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F20] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblTuckerEllisTrialReport201310112] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblTuckerEllisTrialReport201310112] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblTuckerEllisTrialReport201310112] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblTuckerEllisTrialReport201310112] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblTuckerEllisTrialReport201310112] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblTuckerEllisTrialReport201310112] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblTuckerEllisTrialReport201310112] TO [power_user]
GO
