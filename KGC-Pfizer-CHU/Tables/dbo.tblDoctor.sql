CREATE TABLE [dbo].[tblDoctor]
(
[DoctorID] [int] NOT NULL IDENTITY(1, 1),
[DoctorLastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoctorFirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoctorMiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirtyDoctorFlag] [bit] NOT NULL CONSTRAINT [DF_tblDoctor_DirtyDoctorFlag] DEFAULT ((0)),
[DoctorNote] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDoctor] ADD CONSTRAINT [PK_mBtblDoctor] PRIMARY KEY CLUSTERED  ([DoctorID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDoctor] TO [base_user]
GRANT SELECT ON  [dbo].[tblDoctor] TO [base_user]
GRANT SELECT ON  [dbo].[tblDoctor] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDoctor] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblDoctor] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDoctor] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDoctor] TO [power_user]
GRANT SELECT ON  [dbo].[tblDoctor] TO [power_user]
GRANT INSERT ON  [dbo].[tblDoctor] TO [power_user]
GRANT DELETE ON  [dbo].[tblDoctor] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDoctor] TO [power_user]
GO
