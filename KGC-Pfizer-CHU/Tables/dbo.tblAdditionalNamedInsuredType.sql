CREATE TABLE [dbo].[tblAdditionalNamedInsuredType]
(
[AdditionalNamedInsTypeID] [int] NOT NULL IDENTITY(1, 1),
[AdditonalNamedInsType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAdditionalNamedInsuredType] ADD CONSTRAINT [PK_tblAdditionalNamedInsuredType] PRIMARY KEY CLUSTERED  ([AdditionalNamedInsTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAdditionalNamedInsuredType] TO [base_user]
GRANT SELECT ON  [dbo].[tblAdditionalNamedInsuredType] TO [base_user]
GRANT SELECT ON  [dbo].[tblAdditionalNamedInsuredType] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblAdditionalNamedInsuredType] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblAdditionalNamedInsuredType] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblAdditionalNamedInsuredType] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAdditionalNamedInsuredType] TO [power_user]
GRANT SELECT ON  [dbo].[tblAdditionalNamedInsuredType] TO [power_user]
GRANT INSERT ON  [dbo].[tblAdditionalNamedInsuredType] TO [power_user]
GRANT DELETE ON  [dbo].[tblAdditionalNamedInsuredType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAdditionalNamedInsuredType] TO [power_user]
GO
