CREATE TABLE [dbo].[tblPolicyLimits]
(
[LimitID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NULL,
[LimitTypeID] [int] NULL,
[AttachmentPoint] [money] NULL CONSTRAINT [DF_tblPolicyLimits_AttachmentPoint] DEFAULT ((0)),
[PerOccLimit] [money] NULL CONSTRAINT [DF_tblPolicyLimits_PerOccLimit] DEFAULT ((0)),
[LayerPerOccLimit] [money] NULL CONSTRAINT [DF_tblPolicyLimits_LayerPerOccLimit] DEFAULT ((0)),
[SIR] [money] NULL CONSTRAINT [DF_tblPolicyLimits_SIR] DEFAULT ((0)),
[SIRAggregateLimit] [money] NULL CONSTRAINT [DF_tblPolicyLimits_SIRAggregateLimit] DEFAULT ((0)),
[AggregateLimit] [money] NULL CONSTRAINT [DF_tblPolicyLimits_AggregateLimit] DEFAULT ((0)),
[PreviouslyExhaustedAggregate] [money] NULL CONSTRAINT [DF_tblPolicyLimits_PreviouslyExhaustedAggregate] DEFAULT ((0)),
[Layer] [tinyint] NULL CONSTRAINT [DF_tblPolicyLimits_Layer] DEFAULT ((0)),
[LimitNote] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyLimits] ADD CONSTRAINT [PK_tblPolicyLimits] PRIMARY KEY CLUSTERED  ([LimitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPolicyLimits_tblPolicy] ON [dbo].[tblPolicyLimits] ([PolicyID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyLimits] ADD CONSTRAINT [FK_tblPolicyLimits_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy] ([PolicyID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyLimits] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyLimits] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyLimits] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblPolicyLimits] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblPolicyLimits] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblPolicyLimits] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyLimits] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicyLimits] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicyLimits] TO [power_user]
GRANT DELETE ON  [dbo].[tblPolicyLimits] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicyLimits] TO [power_user]
GO
