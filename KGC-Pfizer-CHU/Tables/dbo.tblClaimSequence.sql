CREATE TABLE [dbo].[tblClaimSequence]
(
[ClaimSequenceID] [int] NOT NULL IDENTITY(1, 1),
[ClaimSequence] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimSequence] ADD CONSTRAINT [PK_tblClaimSequence] PRIMARY KEY CLUSTERED  ([ClaimSequenceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSequence] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSequence] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSequence] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSequence] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimSequence] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimSequence] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimSequence] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimSequence] TO [power_user]
GO
