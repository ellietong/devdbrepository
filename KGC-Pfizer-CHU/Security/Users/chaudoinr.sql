IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'KCICLLC\chaudoinr')
CREATE LOGIN [KCICLLC\chaudoinr] FROM WINDOWS
GO
CREATE USER [chaudoinr] FOR LOGIN [KCICLLC\chaudoinr]
GO
GRANT CREATE SCHEMA TO [chaudoinr]
GRANT CREATE TABLE TO [chaudoinr]
GRANT CREATE VIEW TO [chaudoinr]
GRANT DELETE TO [chaudoinr]
GRANT EXECUTE TO [chaudoinr]
GRANT INSERT TO [chaudoinr]
GRANT SELECT TO [chaudoinr]
GRANT TAKE OWNERSHIP TO [chaudoinr]
GRANT UPDATE TO [chaudoinr]
GRANT VIEW DEFINITION TO [chaudoinr]
