SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_REPORTS_qryOutstandingSettlementsByPeriod] (@MatterID int, @SettlementDate datetime)

As

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID =  1
--DECLARE @SettlementDate datetime
SET @SettlementDate = '1/1/2009'

DECLARE @Counts TABLE(Matter nvarchar(50), SettlementDateAsOf datetime, Count30 int, Count60 int, Count90 int, Count120 int, CountLong int, Amount30 money, Amount60 money, Amount90 money, Amount120 money, AmountLong money)

DECLARE @Matter nvarchar(50)
DECLARE @MaxMatterID int

DECLARE @Count30 int
Declare @Amount30 int
DECLARE @Count60 int
DECLARE @Amount60 int
DECLARE @Count90 int
DECLARE @Amount90 int
DECLARE @Count120 int
DECLARE @Amount120 int
DECLARE @CountLong int
DECLARE @AmountLong int

SELECT @Matter = Matter FROM tblMatter WHERE MatterID = @MatterID
SELECT @MaxMatterID = MAX(MatterID) FROM tblMatter

SELECT @Count30 = COUNT(m.ClaimID), @amount30 = SUM(settlementamount) 
FROM tblClaim m
INNER JOIN tblClaimLawsuit cl       ON m.ClaimID = cl.ClaimID
INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
INNER JOIN tblClaimSequence csq	
ON m.ClaimSequenceID = csq.ClaimSequenceID
INNER JOIN tblClaimantPersonalInfo cpi ON m.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
WHERE IsPrimaryForClaim =1 and 
m.ClaimID IN(
      SELECT c.ClaimID
      FROM tblClaim c   
            INNER JOIN tblClaimLawsuit cl       ON c.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
            INNER JOIN tblClaimSequence csq	
	
ON m.ClaimSequenceID = csq.ClaimSequenceID
            left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
            LEFT JOIN tblCheck chk ON ps.CheckID = chk.CheckID
      WHERE GETDATE() - SettlementDueDate > 0 AND GETDATE() - settlementduedate <= 30
            AND SettlementDate >= @SettlementDate
            AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
                  --AND COALESCE(cpi.jobsiteinfo, 0) = 0
      GROUP BY c.ClaimID, SettlementAmount
      HAVING SettlementAmount - SUM(CASE WHEN IsFullyPaid <> 1 OR SourceNumber IS NULL THEN COALESCE(PaymentAmount, 0) ELSE SettlementAmount END) > 0) 

SELECT @Count60 = COUNT(m.ClaimID),  @amount60 =SUM(settlementamount) FROM tblClaim m
INNER JOIN tblClaimLawsuit cl       ON m.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
            INNER JOIN tblClaimantPersonalInfo cpi ON m.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
            left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
WHERE IsPrimaryForClaim =1 and  m.ClaimID IN(
      SELECT c.ClaimID
      FROM tblClaim c
            INNER JOIN tblClaimLawsuit cl       ON c.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
            INNER JOIN tblClaimSequence csq	
	
ON m.ClaimSequenceID = csq.ClaimSequenceID
             left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
             LEFT JOIN tblCheck chk ON ps.CheckID = chk.CheckID
      WHERE GETDATE() - SettlementDueDate > 30 AND GETDATE() - SettlementDueDate <= 60
            AND SettlementDate >= @SettlementDate
            AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
                 --AND COALESCE(cpi.jobsiteinfo, 0) = 0
      GROUP BY c.ClaimID, SettlementAmount
      HAVING SettlementAmount - SUM(CASE WHEN IsFullyPaid <> 1 OR SourceNumber IS NULL THEN COALESCE(PaymentAmount, 0) ELSE SettlementAmount END) > 0) 

SELECT @Count90 = COUNT(m.ClaimID), @amount90 = SUM(settlementamount) FROM tblClaim m
INNER JOIN tblClaimLawsuit cl       ON m.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
            INNER JOIN tblClaimSequence csq	
	
ON m.ClaimSequenceID = csq.ClaimSequenceID
            INNER JOIN tblClaimantPersonalInfo cpi ON m.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
             left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
WHERE IsPrimaryForClaim =1 and  m.ClaimID IN(
      SELECT c.ClaimID
      FROM tblClaim c
            INNER JOIN tblClaimLawsuit cl       ON c.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
            left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID	
			LEFT JOIN tblCheck chk ON ps.CheckID = chk.CheckID
      WHERE GETDATE() - settlementduedate > 60 AND GETDATE() - settlementduedate <= 90
            AND SettlementDate >= @SettlementDate
            AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
                     --AND COALESCE(cpi.jobsiteinfo, 0) = 0
      GROUP BY c.ClaimID, SettlementAmount
      HAVING SettlementAmount - SUM(CASE WHEN IsFullyPaid <> 1 OR SourceNumber IS NULL THEN COALESCE(PaymentAmount, 0) ELSE SettlementAmount END) > 0) 

SELECT @Count120 = COUNT(m.ClaimID),  @amount120 =SUM(settlementamount) 
FROM tblClaim m
	
	
	
INNER JOIN tblClaimLawsuit cl       ON m.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
            INNER JOIN tblClaimantPersonalInfo cpi ON m.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
             left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
WHERE IsPrimaryForClaim =1 and  m.ClaimID IN(
      SELECT c.ClaimID
      FROM tblClaim c
            INNER JOIN tblClaimLawsuit cl       ON c.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
            INNER JOIN tblClaimSequence csq	
	
	ON m.ClaimSequenceID = csq.ClaimSequenceID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	 LEFT JOIN tblCheck chk ON ps.CheckID = chk.CheckID
      WHERE GETDATE() - settlementduedate > 90 AND GETDATE() - settlementduedate <= 120
            AND SettlementDate >= @SettlementDate
            AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
             --AND COALESCE(cpi.jobsiteinfo, 0) = 0
      GROUP BY c.ClaimID, SettlementAmount
      HAVING SettlementAmount - SUM(CASE WHEN IsFullyPaid <> 1 OR SourceNumber IS NULL THEN COALESCE(PaymentAmount, 0) ELSE SettlementAmount END) > 0) 

SELECT @CountLong = COUNT(m.ClaimID),  @AmountLong =SUM(settlementamount) 
FROM tblClaim m
	
	
	
INNER JOIN tblClaimLawsuit cl       ON m.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
            INNER JOIN tblClaimSequence csq	
	
ON m.ClaimSequenceID = csq.ClaimSequenceID
            	
INNER JOIN tblClaimantPersonalInfo cpi ON m.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
            	
left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
WHERE IsPrimaryForClaim =1 and m.ClaimID IN(
      SELECT c.ClaimID
      FROM tblClaim c
            INNER JOIN tblClaimLawsuit cl       ON c.ClaimID = cl.ClaimID
            INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
            left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
            LEFT JOIN tblCheck chk ON ps.CheckID = chk.CheckID
      WHERE GETDATE() - settlementduedate > 120
            AND SettlementDate >= @SettlementDate
            AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
                    --AND COALESCE(cpi.jobsiteinfo, 0) = 0
      GROUP BY c.ClaimID, SettlementAmount
      HAVING SettlementAmount - SUM(CASE WHEN IsFullyPaid <> 1 OR SourceNumber IS NULL THEN COALESCE(PaymentAmount, 0) ELSE SettlementAmount END) > 0) 

INSERT INTO @Counts
      SELECT @Matter, @SettlementDate, COALESCE(@Count30,0), COALESCE(@Count60,0), COALESCE(@Count90,0), COALESCE(@Count120,0), COALESCE(@CountLong,0), coalesce(@Amount30, 0), coalesce(@Amount60, 0), coalesce(@Amount90,0),
      coalesce(@Amount120,0),coalesce(@AmountLong,0)

SELECT * FROM @Counts




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementsByPeriod] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementsByPeriod] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementsByPeriod] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementsByPeriod] TO [power_user]
GO
