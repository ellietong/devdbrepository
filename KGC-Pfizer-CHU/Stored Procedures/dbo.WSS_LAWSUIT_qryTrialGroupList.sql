SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_LAWSUIT_qryTrialGroupList]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TrialGroupID, TrialGroup
	FROM tblTrialGroup
	UNION
	SELECT NULL, ''
	ORDER BY TrialGroup
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryTrialGroupList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryTrialGroupList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryTrialGroupList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryTrialGroupList] TO [power_user]
GO
