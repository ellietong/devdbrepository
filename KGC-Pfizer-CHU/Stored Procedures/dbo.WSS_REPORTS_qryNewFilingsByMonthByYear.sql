SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByMonthByYear] 
	-- Add the parameters for the stored procedure here
	(@YearID int, @MonthID int)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @year int
--Set @Year=2010

DECLARE @year int
SET @year = @YearID

DECLARE @Month int
set @Month = @MonthID

select DATENAME("mm", coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS MonthCount, MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) As MonthID
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s on l.StateID=s.StateID
where	year(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate, Statusdate))= @Year 
		and MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) <=@Month
group by DATENAME("mm", coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)), MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))
order by MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))



END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonthByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonthByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonthByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonthByYear] TO [power_user]
GO
