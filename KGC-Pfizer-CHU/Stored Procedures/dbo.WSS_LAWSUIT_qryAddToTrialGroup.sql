SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryAddToTrialGroup]
    (
      @ClaimLawsuitID BIGINT ,
      @TrialGroupID INT ,
      @ActiveTrialDate DATE = NULL,
      @SPUsername NVARCHAR(250)
    )
AS 
    BEGIN
	--Update TrialGroupID, ActiveTrialDate & SPUsername fields
        UPDATE  tblClaimLawsuit
        SET     TrialGroupID = @TrialGroupID ,
                ActiveTrialDate = @ActiveTrialDate ,
                SPusername = @SPUsername
        WHERE   ClaimLawsuitID = @ClaimLawsuitID
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryAddToTrialGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryAddToTrialGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryAddToTrialGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryAddToTrialGroup] TO [power_user]
GO
