SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryUpdSettlementInfoDocument]
    (
      @SettleDocID BIGINT ,
      @DocCategoryID INT ,
      @PageNums NVARCHAR(20)
    )
AS 
    SET NOCOUNT ON

    UPDATE  tblSettleDocs
    SET     DocCategoryID = @DocCategoryID ,
            PageNums = @PageNums
    WHERE   SettleDocID = @SettleDocID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdSettlementInfoDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdSettlementInfoDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdSettlementInfoDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdSettlementInfoDocument] TO [power_user]
GO
