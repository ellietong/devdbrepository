SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantEmploymentInfo] (@ClaimantPersonalInfoID int)
as

Select *
From tblClaimantEmployment
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By EmpStartDate, ClaimantEmploymentID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [power_user]
GO
