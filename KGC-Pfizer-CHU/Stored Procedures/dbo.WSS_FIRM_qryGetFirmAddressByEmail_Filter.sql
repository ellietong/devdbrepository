SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_FIRM_qryGetFirmAddressByEmail_Filter]
    (
      @emailaddress NVARCHAR(1000)
    )
AS 
    DECLARE @firmID INT
    SET @firmID = ( SELECT  FirmID
                    FROM    vtblPerson
                    WHERE   Email = @emailaddress
                  )
    BEGIN
	
        DECLARE @emaildomain NVARCHAR(1000)
	
        SET @emaildomain = SUBSTRING(@emailaddress,
                                     CHARINDEX('@', @emailaddress),
                                     LEN(@emailaddress) - CHARINDEX('@',
                                                              @emailaddress)
                                     + 1)
	
        IF @emaildomain = '@kcicllc.com'
		   OR @emaildomain = 'kcic.com'
            OR @firmID = 132 
            BEGIN

                SELECT DISTINCT
                        DefenseCounselAddressID AS FirmAddressID ,
                        DefenseCounsel AS Firm
                FROM    tblDefenseCounselAddress
                WHERE   DefenseCounselID NOT IN ( 134, 135, 136, 268 )
                ORDER BY DefenseCounsel
	
            END	
        ELSE 
            BEGIN	
	
                SELECT DISTINCT
                        FirmAddressID ,
                        DefenseCounsel AS Firm
                FROM    vtblPerson
                        INNER JOIN tblDefenseCounselAddress dca ON vtblPerson.FirmAddressID = dca.DefenseCounselAddressID
                WHERE   @emailaddress = vtblPerson.Email
                ORDER BY DefenseCounsel
	
            END
    END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail_Filter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail_Filter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail_Filter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail_Filter] TO [power_user]
GO
