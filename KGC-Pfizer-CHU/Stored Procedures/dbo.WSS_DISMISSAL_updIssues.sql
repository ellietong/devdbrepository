SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_DISMISSAL_updIssues]
(@Issues bit, @DocumentID bigint, @CaseID int)
AS

DECLARE @ConnectionString nvarchar(max)
SET @ConnectionString = '[' + (SELECT MAX(ConnectionStringKey) FROM tblCase WHERE CaseID = @CaseID) + '].[DBO].'

DECLARE @QueryString nvarchar(max)
SET @QueryString = '
UPDATE ' + @ConnectionString + 'tblDocuments
SET Issues = @IssuesParam
WHERE DocumentID = @DocumentIDParam'

Print @QueryString
EXEC sp_executesql @QueryString, N'@DocumentIDParam bigint, @IssuesParam nvarchar(50)',
@DocumentIDParam = @DocumentID, @IssuesParam = @Issues
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_updIssues] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_updIssues] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_updIssues] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_updIssues] TO [power_user]
GO
