SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_Search_qryPlaintiffCounselAddressList]
AS 
    SET NOCOUNT ON

    SELECT  pca.PlaintiffCounselID ,
            PlaintiffCounsel 
            --+ ' ('
            --+ CASE WHEN PlaintiffCounselCity IS NULL THEN 'UNKNOWN'
            --       ELSE PlaintiffCounselCity
            --  END + ')' AS PlaintiffCounsel
    FROM    tblPlaintiffCounselAddress pca
			INNER JOIN tblLawsuit l ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
    WHERE   PlaintiffCounsel IS NOT NULL
            AND PlaintiffCounsel <> ''
    UNION
    SELECT  NULL ,
            NULL
    ORDER BY PlaintiffCounsel 
    --+ ' ('
    --        + CASE WHEN PlaintiffCounselCity IS NULL THEN 'UNKNOWN'
    --               ELSE PlaintiffCounselCity
    --          END + ')'
               ,
            pca.PlaintiffCounselID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Search_qryPlaintiffCounselAddressList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Search_qryPlaintiffCounselAddressList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_Search_qryPlaintiffCounselAddressList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_Search_qryPlaintiffCounselAddressList] TO [power_user]
GO
