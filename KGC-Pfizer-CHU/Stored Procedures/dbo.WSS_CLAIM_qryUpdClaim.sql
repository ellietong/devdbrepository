SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[WSS_CLAIM_qryUpdClaim]
    (

      @LawsuitID BIGINT ,
      @ActiveTrialDate DATETIME ,
      @PrimaryLawsuit NVARCHAR(250) ,
      @IsMDL NVARCHAR(250) ,
      @TrialGroupID BIGINT,
      @DocketNumber NVARCHAR(MAX),
      @ClaimID INT ,
      @PlaintiffCounselAddressID INT,
      @PrimaryNationalPlaintiffCounselID INT,
	  @RegionalCounselAddressID INT,
	  @Judge NVARCHAR(500),
	  @AnswerFiledDate DATE,
	  @IsLossOfConsortium NVARCHAR(50),
	  @CaseNotes NVARCHAR(MAX),
      @Username NVARCHAR(250)
	)
AS 

    UPDATE  tblLawsuit
    SET     SPUsername = @Username,
			DocketNumber = COALESCE(@DocketNumber, DocketNumber),
			PlaintiffCounselAddressID = @PlaintiffCounselAddressID,
			PrimaryNationalPlaintiffCounselID = @PrimaryNationalPlaintiffCounselID,
			RegionalCounselAddressID = @RegionalCounselAddressID,	
			AnswerFiled = @AnswerFiledDate,
			Judge = @Judge
    WHERE   LawsuitID = @LawsuitID
	
	
    UPDATE  tblClaimLawsuit
    SET     ActiveTrialDate = @ActiveTrialDate ,
            IsPrimaryForClaim = CASE WHEN @PrimaryLawsuit = 'YES' THEN 1
                                     ELSE 0
                                END ,
            IsMDL = CASE WHEN @IsMDL = 'YES' THEN 1
                         ELSE 0
                    END ,
            LossOfConsortium = CASE WHEN @IsLossOfConsortium = 'YES' THEN 1
									ELSE 0
								END ,
            TrialGroupID = @TrialGroupID,
			CaseNotes = @CaseNotes,
            SPUsername = @Username
    WHERE   LawsuitID = @LawsuitID
            AND ClaimID = @ClaimID



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GO
