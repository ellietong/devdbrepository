SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_SEARCH_qryDocumentGroupList](@EmailAddress nvarchar(max)=null)
AS 
    SET NOCOUNT ON
    
    
    
    SELECT  ClaimantDocumentGroupID ,
            ClaimantDocumentGroup
    FROM    tblClaimantDocumentGroup
    WHERE   ClaimantDocumentGroupID <> 2
    UNION
    SELECT  NULL ,
            '<< All Document Groups >>'
    ORDER BY ClaimantDocumentGroup ASC
    


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentGroupList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentGroupList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentGroupList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentGroupList] TO [power_user]
GO
