SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryClaimTypeList] 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimTypeValueID, ClaimTypeValue
	FROM tblClaimTypeValue
	UNION
	SELECT NULL, ''
	ORDER BY ClaimTypeValue
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryClaimTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryClaimTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryClaimTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryClaimTypeList] TO [power_user]
GO
