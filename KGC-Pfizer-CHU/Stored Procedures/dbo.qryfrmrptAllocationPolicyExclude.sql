SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmrptAllocationPolicyExclude]
(@allocationID int = NULL)
AS
IF @allocationID IS NULL
	SELECT AllocationID, PolicyID
	FROM tblAllocationPolicyExclude
ELSE
	SELECT AllocationID, PolicyID
	FROM tblAllocationPolicyExclude
	WHERE AllocationID = @allocationID
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmrptAllocationPolicyExclude] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmrptAllocationPolicyExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmrptAllocationPolicyExclude] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmrptAllocationPolicyExclude] TO [power_user]
GO
