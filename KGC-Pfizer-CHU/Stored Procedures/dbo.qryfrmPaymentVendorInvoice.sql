SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmPaymentVendorInvoice] (@VendorFirmID int)
As
Set NoCount On

--declare @VendorFirmID int
--set @VendorFirmID = 29


select di.DefenseInvoiceID, FirmID as VendorFirmID, InvoiceNumber, Period, InvoiceTotal, InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount
from vtblDefenseInvoice di
left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
			 a on a.DefenseInvoiceID = di.DefenseInvoiceID
where FirmID is not null
Group By di.DefenseInvoiceID, FirmID, InvoiceNumber, Period, InvoiceTotal
Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0 and FirmID = @VendorFirmID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [power_user]
GO
