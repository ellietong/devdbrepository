SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[qryInternalEmailSettlmentsAwaitingDismissal]
AS
BEGIN

--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

SET @Subject =
'Pfizer Claims: Settlements Missing Dismissals ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

DECLARE @SettledNotDimissedTable TABLE 
(
	ClaimantPersonalInfoID bigint,
	ClaimantName nvarchar(500),
	LawsuitStatus nvarchar(100),
	StatusDate nvarchar(20),
	ProcessedDate nvarchar(20),
	DefenseCounsel nvarchar(500),
	State nvarchar(10),
	Jurisdiction nvarchar(250)
)
INSERT INTO @SettledNotDimissedTable
	SELECT 	cpi.ClaimantPersonalInfoID,
			'<a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
					+ CONVERT(NVARCHAR(50), cpi.ClaimantPersonalInfoID) + '">'
					+ cpi.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END + COALESCE(' ('
											+ CONVERT(NVARCHAR(50), cpi.ClaimantPersonalInfoID)
											+ ')', '') + '</a>',
			ClaimLawsuitStatus,
			CONVERT(nvarchar(250), StatusDate, 101),
			CONVERT(nvarchar(250), ProcessedDate, 101),
			DefenseCounsel,
			s.State,
			Jurisdiction
	FROM tblClaim c 
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l on cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		INNER JOIN tblSettlementGroup sg ON cs.SettlementGroupID = sg.SettlementGroupID
		INNER JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID = dca.DefenseCounselAddressID
		INNER JOIN tblState s ON l.StateID = s.StateID
		INNER JOIN tblJurisdiction j ON l.JurisdictionID = j.JurisdictionID
		INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	WHERE SettlementGroupTypeID = 5
		AND clst.ClaimLawsuitStatusID = 23
		AND ProcessedDate >= '10/1/2013'
		AND IsPrimaryStatus = 1
		AND cs.SettlementDate + 30 < GETDATE()
		AND NOT EXISTS (SELECT ClaimLawsuitID
						FROM tblClaimLawsuitStatusTracking clst2
							INNER JOIN tblClaimLawsuitStatus cls on clst2.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
						WHERE ClaimLawsuitStatusGroupID = 2
							AND cl.ClaimLawsuitID = clst2.ClaimLawsuitID
						)
	ORDER BY cl.ClaimLawsuitID, SettlementDate

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	
 
 
--->
</STYLE>'



--Set table column headings and populate the cells

DECLARE @TDStyleCenter nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'
DECLARE @TDStyleLeft nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: left; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

IF (SELECT
	COUNT(ClaimantPersonalInfoID)
FROM @SettledNotDimissedTable) > 0
begin
SET @BodyMessage = @BodyMessage + '<H4>The following claimants were settled 30 days ago and are missing a dismissal record: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +


N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Name (ClaimantID)</th>
	  <th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Lawsuit Status</th>
	  <th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Status Date</th>
	  <th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Processed Date</th>
	  <th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Local Counsel</th>
	  <th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">State</th>
	  <th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jurisdiction</th>' +


CAST((SELECT
	@TDStyleLeft AS 'td/@style',
	(SELECT
		ClaimantName)
	AS td,
	'',
	@TDStyleCenter AS 'td/@style',
	(SELECT
		LawsuitStatus)
	AS td,
	'',
	@TDStyleCenter AS 'td/@style',
	(SELECT
		StatusDate)
	AS td,
	'',
	@TDStyleCenter AS 'td/@style',
	(SELECT
		ProcessedDate)
	AS td,
	'',
	@TDStyleLeft AS 'td/@style',
	(SELECT
		DefenseCounsel)
	AS td,
	'',
	@TDStyleCenter AS 'td/@style',
	(SELECT
		State)
	AS td,
	'',
	@TDStyleLeft AS 'td/@style',
	(SELECT
		Jurisdiction)
	AS td,
	''
FROM @SettledNotDimissedTable C

FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +


N'</table>' + N'<br/>'


end
ELSE
BEGIN
SET @BodyMessage = @BodyMessage
    + '<H4>No settled claims are currently awaiting dismissal. </H4>'
END

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

SELECT @BodyMessage

--IF (SELECT
--	COUNT(ClaimantPersonalInfoID)
--FROM @SettledNotDimissedTable) > 0
begin
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail 
@profile_name = 'Claims',
@recipients = 'Pfizer@kcic.com',
@copy_recipients = '',
@blind_copy_recipients = 'monahanc@kcic.com; kanga@kcic.com',
@subject = @subject, 
@body = @BodyMessage, 
@body_format = 'html'
end

END






GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailSettlmentsAwaitingDismissal] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailSettlmentsAwaitingDismissal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailSettlmentsAwaitingDismissal] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailSettlmentsAwaitingDismissal] TO [power_user]
GO
