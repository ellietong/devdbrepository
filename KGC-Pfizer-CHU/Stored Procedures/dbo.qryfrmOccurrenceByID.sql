SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmOccurrenceByID] (@AllocationID int)
As

SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 3

SELECT OccurrenceID, OccurrenceName
FROM tblOccurrence

WHERE OccurrenceGroupID in (SELECT OccurrenceGroupID FROM tblAllocationOccurrenceGroupInclude WHERE AllocationID = @AllocationID)
AND OccurrenceTypeID in (SELECT OccurrenceTypeID FROM tblAllocationOccurrenceTypeInclude WHERE AllocationID = @AllocationID)

ORDER BY OccurrenceName




RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceByID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceByID] TO [power_user]
GO
