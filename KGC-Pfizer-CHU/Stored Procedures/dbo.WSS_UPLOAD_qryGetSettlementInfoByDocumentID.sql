SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryGetSettlementInfoByDocumentID] ( @DocumentID BIGINT )
AS 
    SET NOCOUNT ON

    SELECT  CONVERT(NVARCHAR(MAX), sd.SettleDocID) AS SettleDocID ,
            CONVERT(NVARCHAR(MAX), sd.DocCategoryID) AS DocCategoryID ,
            CONVERT(NVARCHAR(MAX), PageNums) AS PageNums ,
            CONVERT(NVARCHAR(MAX), SetPymtID) AS SetPymtID
    FROM    tblDocuments d
            INNER JOIN tblSettleDocs sd ON d.DocumentID = sd.DocumentID
            INNER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
    WHERE   d.DocumentID = @DocumentID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetSettlementInfoByDocumentID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetSettlementInfoByDocumentID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetSettlementInfoByDocumentID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetSettlementInfoByDocumentID] TO [power_user]
GO
