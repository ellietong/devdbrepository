SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DEADLINE_qryInsEventLocation] 
	-- Add the parameters for the stored procedure here
@LocationName NVARCHAR(4000),
@Address1 NVARCHAR(250) ,
@Address2 NVARCHAR(250) ,
@City NVARCHAR(250) ,
@StateID INT ,
@Zip NVARCHAR(100) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
INSERT INTO tblEventLocation
SELECT @LocationName,
        @Address1,
        @Address2,
        @City,
        @StateID,
        @Zip 

SELECT MAX(EventLocationID)
FROM tblEventLocation
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryInsEventLocation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryInsEventLocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryInsEventLocation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryInsEventLocation] TO [power_user]
GO
