SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_REPORTS_qryOutstandingSettlements]  (@MatterID int, @SettlementDate datetime)

As

--SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 1
--DECLARE @SettlementDate datetime
--SET @SettlementDate = '4/1/2010'

DECLARE @MaxMatterID int
SELECT @MaxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @SettlementGroupPaidUnpaid TABLE (SettlementGroupID int, Paid money, Total money)
INSERT INTO @SettlementGroupPaidUnpaid

SELECT SettlementGroupID, SUM( CASE WHEN ps.CheckID IS NULL THEN 0 ELSE SettlementAmount END) As Paid,
	SUM(SettlementAmount) as Total	
FROM tblClaimantSettlement cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	WHERE COALESCE(c.IsCurrentClaim,0) = 1 AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	GROUP BY SettlementGroupID, COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupCount TABLE (SettlementGroupID int, ClaimCount nvarchar(50))
INSERT INTO @SettlementGroupCount	

	
SELECT SettlementGroupID, COUNT(SettlementDate) as ClaimCount
	FROM tblClaimantSettlement 	cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	
where  COALESCE(c.IsCurrentClaim,0) = 1 AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	GROUP BY SettlementGroupID,  COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupPaidCount TABLE (SettlementGroupID int, PaidCount nvarchar(50))
INSERT INTO @SettlementGroupPaidCount	

SELECT SettlementGroupID, COUNT(ps.CheckID) as PaidCount
	FROM tblClaimantSettlement cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	WHERE SettlementGroupID IN	
(SELECT SettlementGroupID
	
FROM tblClaimantSettlement cs
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID 
	
WHERE ps.CheckID IS NOT NULL 
	
GROUP BY SettlementGroupID)
	and COALESCE(c.IsCurrentClaim,0) = 1 AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	GROUP BY SettlementGroupID, COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID

DECLARE @SettlementGroupMaxDate TABLE (SettlementGroupID int, MaxSettlementDate datetime)
INSERT INTO @SettlementGroupMaxDate
	
SELECT SettlementGroupID, MAX(SettlementDate) as MaxSettlementDate
	
FROM tblClaimantSettlement
	GROUP BY SettlementGroupID 
	ORDER BY MaxSettlementDate

SELECT pl.PlaintiffCounsel, cs.SettlementDate, COALESCE(SettlementDueDate, 1/1/1900) as DatePayable, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	
cs.SettlementAmount, ps.CheckID, spd.ReleaseSignedDate, FullyDocumented, c.MatterID, Matter, cl.LawsuitID, k.SourceDate as 'paiddate',
	
@SettlementDate AS SettlementDateAsOf,  DocketNumbers, COALESCE(sgpc.PaidCount,0) AS PaidCount, sgc.ClaimCount, MaxSettlementDate,
	
CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0) + CASE WHEN medicareformrcvd IS NULL THEN 0 ELSE 1 END) = 8
	THEN 'Complete'
	WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)+ CASE WHEN medicareformrcvd IS NULL THEN 0 ELSE 1 END) = 6
	THEN 'Complete'
	ELSE 'Documentation Needed' END AS SupportingDocumentation,
	CASE WHEN spd.ReleaseRcvd = 1
	THEN coalesce( crr.ReleaseType,'Executed' )
	ELSE 'Release Missing'  END AS ReleaseExecution, Case when ps.CheckID is not null then cs.SettlementAmount else 0 end as Paid, CASE WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 1 THEN 'Unknown' WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 2 THEN 'Yes - ' + COALESCE(HealthInsuranceClaimNumber, 'HICN Not Provided')WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 3 THEN 'Non-Beneficiary' END AS HICN,
CASE WHEN spd.MedicareFormRcvd IS not null THEN 'Yes' ELSE 'No' END As MedicareForm, case when MedicareFullyDocumented = 1 then 'Yes' Else 'No' end as MedicareFullyDocumented
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
	INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INner JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pl ON l.PlaintiffCounselAddressID = pl.PlaintiffCounselAddressID
	INNER JOIN tblClaimLawsuitStatusTracking cls		ON cl.ClaimLawsuitID = cls.ClaimLawsuitID	
	LEFT join tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblClaimantDocketNumbers cd		ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
LEFT  JOIN @SettlementGroupPaidUnpaid sgp	ON s.SettlementGroupID = sgp.SettlementGroupID
	LEFT  JOIN @SettlementGroupCount sgc	ON s.SettlementGroupID= sgc.SettlementGroupID
	LEFT  JOIN @SettlementGroupPaidCount sgpc	oN s.SettlementGroupID = sgpc.SettlementGroupID
	LEFT  JOIN @SettlementGroupMaxDate sgm		ON s.SettlementGroupID = sgm.SettlementGroupID
	LEFT JOIN  tblReleasetype crr	ON crr.ReleaseTypeID =spd.ReleaseTypeID
	LEFT JOIN tblClaimSequence csq	ON c.ClaimSequenceID = csq.ClaimSequenceID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	left join tblCheck k on k.CheckID =  ps.CheckID
WHERE MaxSettlementDate >= @SettlementDate
	AND COALESCE(IsCurrentClaim,0) = 1 and coalesce(cls.isprimarystatus,0) = 1
	AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	AND COALESCE(PaidCount,0) <> COALESCE(ClaimCount,0)
--AND COALESCE(cpi.jobsiteinfo, 0) = 0  
GROUP BY cs.SettlementDate, SettlementDueDate, cpi.ClaimantPersonalInfoID, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
cs.SettlementAmount, ps.Checkid, spd.ReleaseSignedDate, FullyDocumented, c.MatterID, Matter, cl.LawsuitID,k.SourceDate,pl.PlaintiffCounsel,
	  DocketNumbers, sgpc.PaidCount, sgc.ClaimCount, MaxSettlementDate,spd.ReleaseRcvd,
CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0) + CASE WHEN medicareformrcvd IS NULL THEN 0 ELSE 1 END) = 8
	THEN 'Complete'
	WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)+ CASE WHEN medicareformrcvd IS NULL THEN 0 ELSE 1 END) = 6
	THEN 'Complete' ELSE 'Documentation Needed' END,
	CASE WHEN spd.ReleaseRcvd = 1 	THEN coalesce( crr.ReleaseType,'Executed' ) 
	ELSE 'Release Missing'  END, CASE WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 1 THEN 'Unknown' WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 2 THEN 'Yes - ' + COALESCE(HealthInsuranceClaimNumber, 'HICN Not Provided')WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 3 THEN 'Non-Beneficiary' END,
CASE WHEN spd.MedicareFormRcvd IS not null THEN 'Yes' ELSE 'No' END, case when MedicareFullyDocumented = 1 then 'Yes' Else 'No' end
ORDER BY MaxSettlementDate, SettlementGroup, SettlementDate, LastName




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingSettlements] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingSettlements] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingSettlements] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingSettlements] TO [power_user]
GO
