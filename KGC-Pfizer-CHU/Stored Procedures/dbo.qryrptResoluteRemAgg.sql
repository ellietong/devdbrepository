SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[qryrptResoluteRemAgg]
(@BillExhaustionDateID int)
As
Set Nocount on


--Declare @BillExhaustionDateID int
--Set @BillExhaustionDateID = 118

Declare @Header nvarchar(100)
Set @Header = (select Datename(MONTH,exhaustiondate) +' ' + Datename(Year,exhaustiondate) from tblexhaustiondate where ExhaustionDateID=@BillExhaustionDateID)


declare @exhaustedPolicies table (CarrierName nvarchar(100), PolicyNum nvarchar(50), PStartDate datetime, PEndDate datetime, Layer int, AggregateLimit float, PolicyDateID int,
OutsideConsumption float, IndemnityConsumption float, TotalExpense float)
insert @exhaustedPolicies
select CarrierName, PolicyNum, PStartDate, PEndDate, Layer+1, AggregateLimit, e.PolicyDateID,
case when e.PolicyDateID=10 then 44730.06
		when e.PolicyDateID=11 then 44761.49
		when e.PolicyDateID=12 then 44777.04
		when e.PolicyDateID=13 then 12353.74 end as OutsideConsumption,
case when e.PolicyDateID=10 then 2589173.88
		when e.PolicyDateID=11 then 2578167.10
		when e.PolicyDateID=12 then 2570990.26
		when e.PolicyDateID=13 then 2603679.17 end as IndemnityConsumption,
case when e.PolicyDateID=10 then 2366096.06
		when e.PolicyDateID=11 then 2377071.41
		when e.PolicyDateID=12 then 2384232.70
		when e.PolicyDateID=13 then 2383967.09 end as TotalExpense
from tblCarrier c
inner join tblPolicy p on p.CarrierID=c.CarrierID
inner join tblPolicyDates pd on pd.PolicyID=p.PolicyID
inner join tblPolicyLimits pl on pl.PolicyID=p.PolicyID
inner join tblExhaustion e on e.PolicyDateID=pd.PolicyDateID
inner join tblExhaustionDate ed on ed.ExhaustionDateID=e.ExhaustionDateID
where PStartDate >= '1/1/1973' and PEndDate <= '1/1/1977' and Layer>0 and c.CarrierID in (9,397)
group by CarrierName, PolicyNum, PStartDate, PEndDate, Layer, AggregateLimit,  e.PolicyDateID
order by PStartDate, PEndDate

declare @OutsideConsumption table (CarrierName nvarchar(100), PolicyNum nvarchar(50), PStartDate datetime, PEndDate datetime, Layer int, AggregateLimit float, PolicyDateID int,
OutsideConsumption float, IndemnityConsumption float, TotalExpense float)
insert @OutsideConsumption
select CarrierName, PolicyNum, PStartDate, PEndDate, Layer, AggregateLimit, e.PolicyDateID,
case when e.PolicyDateID=27 then (-120744.53) + SUM(indemnityallocation+defenseallocation)
		when e.PolicyDateID=28 then (-215929.42) + SUM(indemnityallocation+defenseallocation)
		when e.PolicyDateID=29 then (-217398.01) + SUM(indemnityallocation+defenseallocation)
		when e.PolicyDateID=30 then (-131401.3) + SUM(indemnityallocation+defenseallocation) 
		when e.PolicyDateID=52 and sum(indemnityallocation+defenseallocation)=0 then 0.00
		when e.PolicyDateID=58 and sum(indemnityallocation+defenseallocation)=0 then 0.00
		else SUM(indemnityallocation+defenseallocation)  end as OutsideConsumption,
0 as IndemnityConsumption, 0 as TotalExpense
from tblCarrier c
inner join tblPolicy p on p.CarrierID=c.CarrierID
inner join tblPolicyDates pd on pd.PolicyID=p.PolicyID
inner join tblPolicyLimits pl on pl.PolicyID=p.PolicyID
inner join tblExhaustion e on e.PolicyDateID=pd.PolicyDateID
inner join tblExhaustionDate ed on ed.ExhaustionDateID=e.ExhaustionDateID
where PStartDate >= '1/1/1973' and PEndDate <= '1/1/1977' and Layer>0 and Description like '%adjustment%'
group by CarrierName, PolicyNum, PStartDate, PEndDate, Layer, AggregateLimit,  e.PolicyDateID
order by PStartDate, PEndDate

declare @IndemnityExpense table (CarrierName nvarchar(100), PolicyNum nvarchar(50), PStartDate datetime, PEndDate datetime, Layer int, AggregateLimit float, PolicyDateID int, OutsideConsumption float,
IndemnityConsumption float, TotalExpense float)
insert @IndemnityExpense
select CarrierName, PolicyNum, PStartDate, PEndDate, Layer+1, AggregateLimit, e.PolicyDateID, 0 as outsideconsumption,
case when e.PolicyDateID=27 then (1512525.35) + SUM(indemnityallocation)
		when e.PolicyDateID=28 then (1654437.61) + SUM(indemnityallocation)
		when e.PolicyDateID=29 then (1743221.33) + SUM(indemnityallocation)
		when e.PolicyDateID=30 then (1878047.06) + SUM(indemnityallocation) 
		when e.PolicyDateID=52 and sum(indemnityallocation)=0 then 0.00
		when e.PolicyDateID=58 and sum(indemnityallocation)=0 then 0.00
		else SUM(indemnityallocation) end as IndemnityConsumption,
case when e.PolicyDateID=27 then (867669.83) + SUM(defenseallocation)
		when e.PolicyDateID=28 then (892087.08) + SUM(defenseallocation)
		when e.PolicyDateID=29 then (915907.77) + SUM(defenseallocation)
		when e.PolicyDateID=30 then (981314.62) + SUM(defenseallocation) 
		when e.PolicyDateID=52 and sum(defenseallocation)=0 then 0.00
		when e.PolicyDateID=58 and sum(defenseallocation)=0 then 0.00
		else SUM(defenseallocation) end as TotalExpense
from tblCarrier c
inner join tblPolicy p on p.CarrierID=c.CarrierID
inner join tblPolicyDates pd on pd.PolicyID=p.PolicyID
inner join tblPolicyLimits pl on pl.PolicyID=p.PolicyID
inner join tblExhaustion e on e.PolicyDateID=pd.PolicyDateID
inner join tblExhaustionDate ed on ed.ExhaustionDateID=e.ExhaustionDateID
where PStartDate >= '1/1/1973' and PEndDate <= '1/1/1977' and Layer>0 and Description not like '%adjustment%' and e.ExhaustionDateID>=100 and e.ExhaustionDateID <=@BillExhaustionDateID
group by CarrierName, PolicyNum, PStartDate, PEndDate, Layer, AggregateLimit,  e.PolicyDateID
order by PStartDate, PEndDate


--This query should be adjusted to not bring these policies in once they are triggered
declare @UnexhaustedPolicies table (CarrierName nvarchar(100), PolicyNum nvarchar(50), PStartDate datetime, PEndDate datetime, Layer int, AggregateLimit float, PolicyDateID int, OutsideConsumption float,
IndemnityConsumption float, TotalExpense float)
insert into @UnexhaustedPolicies
select oc.CarrierName, oc.PolicyNum, oc.PStartDate, oc.PEndDate, oc.Layer+1, oc.AggregateLimit, oc.PolicyDateID, SUM(oc.outsideconsumption) as outsideconsumption, 
SUM(ie.IndemnityConsumption) as IndemnityConsumption, SUM(ie.TotalExpense) as TotalExpense
from @OutsideConsumption oc
inner join @IndemnityExpense ie on ie.PolicyDateID=oc.PolicyDateID
group by oc.CarrierName, oc.PolicyNum, oc.PStartDate, oc.PEndDate, oc.Layer, oc.AggregateLimit, oc.PolicyDateID


declare @UntriggeredPolicies table (CarrierName nvarchar(100), PolicyNum nvarchar(50), PStartDate datetime, PEndDate datetime, Layer int, AggregateLimit float, PolicyDateID int, OutsideConsumption float,
IndemnityConsumption float, TotalExpense float)
insert into @UntriggeredPolicies
select CarrierName, PolicyNum, PStartDate, PEndDate, Layer+1, AggregateLimit, pd.PolicyDateID, 0 as outsideconsumption, 0 as IndemnityConsumption, 0 as TotalExpense
from tblCarrier c
inner join tblPolicy p on p.CarrierID=c.CarrierID
inner join tblPolicyDates pd on pd.PolicyID=p.PolicyID
inner join tblPolicyLimits pl on pl.PolicyID=p.PolicyID
--left join tblExhaustion e on e.PolicyDateID=pd.PolicyDateID
--inner join tblExhaustionDate ed on ed.ExhaustionDateID=e.ExhaustionDateID
where PStartDate >= '1/1/1973' and PEndDate <= '1/1/1977' and Layer>0 and pd.policydateid in (52,58) 


declare @final	table (CarrierName nvarchar(100), PolicyNum nvarchar(50), PStartDate datetime, PEndDate datetime, Layer int, AggregateLimit float, PolicyDateID int, OutsideConsumption float,
IndemnityConsumption float, TotalExpense float)
insert into @final
select * from @exhaustedPolicies

UNION

select * from @UnexhaustedPolicies

UNION 

select * from @UntriggeredPolicies
order by PStartDate, PEndDate, Layer

select f.*, YEAR(PStartDate) as SubTotalYear, @Header as Headerdate
from @final f


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptResoluteRemAgg] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptResoluteRemAgg] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptResoluteRemAgg] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptResoluteRemAgg] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptResoluteRemAgg] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptResoluteRemAgg] TO [power_user]
GO
