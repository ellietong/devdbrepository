SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryExternalEmailNewComplaints_summary]
AS 
    BEGIN
--Declare and set email inputs.
        DECLARE @Subject NVARCHAR(100)
        DECLARE @LogoHeader NVARCHAR(2000)

        DECLARE @Today4PM DATETIME
        DECLARE @Yesterday4PM DATETIME
        SET @Yesterday4PM = CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(DATE, GETDATE()
            - 1)) + ' 16:00:00.000')
        SET @Today4PM = CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(DATE, GETDATE()))
            + ' 16:00:00.000')



        SET @Subject = 'Pfizer Claims: New Complaints\Documents Uploaded '
            + CONVERT(VARCHAR, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


        SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (DocID int, 
								   docTitle nvarchar (500), FirmID int,
								   lName nvarchar (50), 
								   fName nvarchar (50), 
								   ClaimantPersonalInfoID bigint, 
								   ServiceDate date, 
								   [State] nvarchar (5), 
								   LocalPlaintiffCounsel nvarchar (500), 
								   NationalPlaintiffCounsel nvarchar (500), 
								   DefenseCounsel nvarchar (400), 
								   LawsuitID int,
								   ClaimID bigint,
								   Matter nvarchar (250),
								   --ClaimType nvarchar (250),
								   ComplaintID bigint)
INSERT INTO @NewComplaintsTable
	SELECT
		COALESCE(d.DocumentID,0),
		'<a href="https://www.kcicextranet.com/pfizer/cp/pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		COALESCE(DefenseCounselID,0),
		COALESCE(cpi.LastName, ' '),
		COALESCE(cpi.FirstName, ' '),
		COALESCE(cpi.ClaimantPersonalInfoID,0),
		COALESCE(MAX(a.ServiceDate),'1/1/1900'),
		COALESCE(s.State,''),
		COALESCE(lpca.PlaintiffCounsel, ' '),
		COALESCE(npca.PlaintiffCounsel, ' '),
		COALESCE(DefenseCounsel,''),
		COALESCE(cl.LawsuitID,0),
		COALESCE(cl.ClaimID,0),
		COALESCE(Matter,''),
		--COALESCE(ClaimType, ' '),
		COALESCE(ComplaintID, 0)
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID 
		INNER JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
		INNER JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
		INNER JOIN tblMatter m ON c.MatterID = m.MatterID
		--LEFT JOIN tblClaimType ct on c.ClaimTypeID = ct.ClaimTypeID
		LEFT JOIN tblAllegation a ON a.ClaimLawsuitID = cl.ClaimLawsuitID 
		--INNER JOIN dbo.vAllegation va ON va.ClaimLawsuitID = cl.claimlawsuitid
		LEFT JOIN tblState s ON s.StateID = l.StateID
		LEFT JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
		LEFT JOIN tblPlaintiffCounselAddress lpca ON l.PlaintiffCounselAddressID = lpca.PlaintiffCounselAddressID
		LEFT JOIN tblPlaintiffCounselAddress npca ON l.PrimaryNationalPlaintiffCounselID = npca.PlaintiffCounselAddressID
		--LEFT JOIN tblDefendantCompany dc ON a.DefendantCompanyID = dc.DefendantCompanyID
	WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
		AND d.ClaimantDocumentTypeID IN (8, 25, 33, 4, 2, 6, 9, 16, 28)
		AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
		--AND (RIGHT(COALESCE(AcceptedBy, ''), 11) <> 'kcicllc.com' AND RIGHT(COALESCE(AcceptedBy, ''), 8) <> 'kcic.com')
		AND l.DefenseCounselAddressID IS NOT NULL
		AND IsPrimaryForClaim = 1
		AND IsCurrentClaim = 1
		AND c.MatterID = 1
	--WHEre c.ClaimantPersonalInfoID = 296048
	GROUP BY	d.documentID,
				d.DocumentTitle,
				s.State,
				DefenseCounselID,
				LastName,
				FirstName,
				cpi.ClaimantPersonalInfoID,
				lpca.PlaintiffCounsel,
				npca.PlaintiffCounsel,
				DefenseCounsel,
				cl.LawsuitID,
				cl.ClaimID,
				--ClaimType,
				Matter,
				ComplaintID
	



        DECLARE @ClaimantPerDoc TABLE
            (
              DocID INT ,
              ClaimantList NVARCHAR(MAX)
            )
        DECLARE @DocID INT ,
            @LastDocID INT ,
            @Claimant NVARCHAR(MAX) ,
            @ClaimantID BIGINT ,
            @ClaimantList NVARCHAR(MAX)

        DECLARE Cur_Complaints CURSOR Local Fast_Forward
        FOR
            SELECT  DocID ,
                    COALESCE(Lname, '') + ', ' + COALESCE(Fname, '') ,
                    claimantpersonalInfoID
            FROM    @NewComplaintsTable
        OPEN Cur_Complaints
        FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
        WHILE @@FETCH_STATUS = 0 
            BEGIN


                IF ( LEN(@ClaimantList) = 0
                     OR @ClaimantList IS NULL
                   ) 
                    BEGIN
	
                        SET @ClaimantList = '<ul style="margin:0; padding:0;"> <a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                            + CONVERT(NVARCHAR(50), @claimantID) + '">'
                            + @Claimant + COALESCE(' ('
                                                   + CONVERT(NVARCHAR(50), @claimantID)
                                                   + ')', '') + '</a></ul>'
                    END
                ELSE 
                    BEGIN
	
                        SET @ClaimantList = COALESCE(@ClaimantList, '')
                            + '<ul style="margin:0; padding:0;">  <a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                            + CONVERT(NVARCHAR(50), @claimantID) + '">'
                            + COALESCE(@Claimant, '') + COALESCE(' ('
                                                              + CONVERT(NVARCHAR(50), @claimantID)
                                                              + ')', '')
                            + '</a></ul>'

                    END
                SET @LastDocID = @DocID
                FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant,
                    @ClaimantID
                PRINT @@FETCH_STATUS
                IF @DocID <> @LastDocID
                    OR @@FETCH_STATUS <> 0 
                    BEGIN
	
                        INSERT  INTO @ClaimantPerDoc
                                SELECT  @LastDocID ,
                                        @ClaimantList
	
                        SET @ClaimantList = NULL
                    END
            END
        CLOSE Cur_Complaints
        DEALLOCATE Cur_Complaints
	


--Declare and populate the body of the email for individual firms
        DECLARE @BodyMessage NVARCHAR(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
        SET @BodyMessage = @LogoHeader
            + '<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'

        DECLARE @LastLawsuitID NVARCHAR(250) = ''

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

        IF ( SELECT COUNT(docTitle)
             FROM   @NewComplaintsTable
           ) > 0 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<H4>The following complaints\documents have been uploaded in the last 24 hours: </H4>'
                SET @BodyMessage = @BodyMessage
                    + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' 
				SET @BodyMessage = @BodyMessage 
					+ N'<tr><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Document Title</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Linked Claimants (ClaimantID)</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Date Served*</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">State</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Defense Counsel</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Local Plaintiff Counsel</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">National Plaintiff Counsel</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Matter</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Claim Type</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Defendant Companies</th>'
	
                DECLARE @DocTitle NVARCHAR(250) ,
                    @GetClaimantList NVARCHAR(500) ,
                    @ServiceDate NVARCHAR(300) ,
                    @State NVARCHAR(250) ,
                    @LocalPlaintiffCounsel NVARCHAR(400) ,
					@NationalPlaintiffCounsel NVARCHAR(400) ,
                    @DefenseCounsel NVARCHAR(400) ,
                    @LawsuitID INT ,					
					@Matter NVARCHAR(200),
					@ClaimID BIGINT,
					@GetClaimTypes NVARCHAR(200),
                    @GetDefendantCompanies NVARCHAR(500) ,
                    @GetDefendantCompaniesRowID NVARCHAR(10)
	
                DECLARE Doc_List CURSOR
                FOR
                    SELECT  docTitle ,
                            cl.ClaimantList ,
                            CAST(MAX(ServiceDate) AS VARCHAR(MAX)) ,
                            [state] ,
							LocalPlaintiffCounsel,
                            NationalPlaintiffCounsel ,
                            DefenseCounsel ,
                            LawsuitID,
							Matter,
							ClaimID--,
							--ClaimType
                    FROM    @NewComplaintsTable C
                            INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
                    GROUP BY [state] ,
                            cl.docid ,
                            docTitle ,
                            cl.ClaimantList ,
							LocalPlaintiffCounsel,
                            NationalPlaintiffCounsel ,
                            DefenseCounsel ,
                            LawsuitID,
							Matter,
							ClaimID--,
							--ClaimType
                    ORDER BY cl.DocID	

                OPEN Doc_List
                FETCH NEXT FROM Doc_List INTO @DocTitle, 
											  @GetClaimantList,
											  @ServiceDate, 
											  @State, 
											  @LocalPlaintiffCounsel, 
											  @NationalPlaintiffCounsel,
											  @DefenseCounsel,
											  @LawsuitID,
											  @Matter,
											  @ClaimID
											  --@ClaimType

                WHILE @@FETCH_STATUS = 0 
                    BEGIN
	
		
                        SET @BodyMessage = @BodyMessage + '<tr>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @DocTitle + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @GetClaimantList + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @ServiceDate + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @State + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @DefenseCounsel + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @LocalPlaintiffCounsel + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @NationalPlaintiffCounsel + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @Matter + ' </td>'
                        --SET @BodyMessage = @BodyMessage
                        --    + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                        --    + @ClaimType + ' </td>'

                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top">'

		
                        DECLARE @ClaimTypes TABLE
                            (
                              ID INT NOT NULL
                                     IDENTITY(1, 1)
                                     PRIMARY KEY ,
                              ClaimID BIGINT ,
                              ClaimTypeValue NVARCHAR(500) ,
                              ClaimTypeValueID INT
                            )
                        INSERT  INTO @ClaimTypes
                                SELECT  c.ClaimID,										
										ClaimTypeValue,
										ct.ClaimTypeValueID
                                FROM    tblClaim c
                                        INNER JOIN tblClaimType ct		ON c.ClaimID = ct.ClaimID
                                        INNER JOIN tblClaimTypeValue ctv	ON ct.ClaimTypeValueID = ctv.ClaimTypeValueID
                                WHERE   c.ClaimID = @ClaimID
                                GROUP BY c.ClaimID,
										ct.ClaimTypeValueID,
										ClaimTypeValue
                                ORDER BY ClaimTypeValue
		
                        DECLARE ClaimTypes_curs CURSOR
                        FOR
                            SELECT DISTINCT
                                    ClaimTypeValue
                            FROM    @ClaimTypes
                            WHERE   ClaimID = @ClaimID
			

                        OPEN ClaimTypes_curs
                        FETCH NEXT FROM ClaimTypes_curs INTO @GetClaimTypes
			
                        WHILE @@FETCH_STATUS = 0 
                            BEGIN		
				
                                SET @BodyMessage = @BodyMessage + @GetClaimTypes
                                    + ' <br/>'
				
                                FETCH NEXT FROM ClaimTypes_curs INTO @GetClaimTypes
			
                            END
                        CLOSE ClaimTypes_curs
                        DEALLOCATE ClaimTypes_curs

                        --SET @BodyMessage = @BodyMessage
                        --    + '</td><td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top">'


                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top">'

		
                        DECLARE @DefendantCompanies TABLE
                            (
                              ID INT NOT NULL
                                     IDENTITY(1, 1)
                                     PRIMARY KEY ,
                              LawsuitID INT ,
                              DefendantCompany NVARCHAR(500) ,
                              DefendantCompanyID INT
                            )
                        INSERT  INTO @DefendantCompanies
                                SELECT  cl.LawsuitID ,
                                        DefendantCompany ,
                                        a.DefendantCompanyID
                                FROM    tblAllegation a
                                        INNER JOIN tblClaimLawsuit cl		ON a.ClaimLawsuitID = cl.ClaimLawsuitID
                                        INNER JOIN tblDefendantCompany dc	ON a.DefendantCompanyID = dc.DefendantCompanyID
                                WHERE   LawsuitID = @LawsuitID AND a.ServiceDate is not null
                                GROUP BY cl.LawsuitID ,
                                        DefendantCompany ,
                                        a.DefendantCompanyID
                                ORDER BY lawsuitid ,
                                        DefendantCompany
		
                        DECLARE DefendantCompanies_curs CURSOR
                        FOR
                            SELECT DISTINCT
                                    DefendantCompany
                            FROM    @DefendantCompanies
                            WHERE   LawsuitID = @LawsuitID
			

                        OPEN DefendantCompanies_curs
                        FETCH NEXT FROM DefendantCompanies_curs INTO @GetDefendantCompanies
			
                        WHILE @@FETCH_STATUS = 0 
                            BEGIN		
				
                                SET @BodyMessage = @BodyMessage + @GetDefendantCompanies
                                    + ' <br/>'
				
                                FETCH NEXT FROM DefendantCompanies_curs INTO @GetDefendantCompanies
			
                            END
                        CLOSE DefendantCompanies_curs
                        DEALLOCATE DefendantCompanies_curs
 
                        SET @BodyMessage = @BodyMessage + '</td></tr>'
																	
                        SET @LastLawsuitID = @LawsuitID

                        FETCH NEXT FROM Doc_List INTO @DocTitle, 
													  @GetClaimantList,
													  @ServiceDate, 
													  @State, 
													  @LocalPlaintiffCounsel, 
													  @NationalPlaintiffCounsel,
													  @DefenseCounsel,
													  @LawsuitID,
													  @Matter,
													  @ClaimID
													  --@ClaimType

                    END
                CLOSE Doc_List
                DEALLOCATE Doc_List
	
	
                SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'
	

            END
        ELSE 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<H4>No Complaints have been uploaded in the last 24 hours. </H4>'
            END



--Undo the HTML stripping from the xml path function above so embedded tags work
        SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;',
                                   '>')

----Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/Pfizer/cp/Pages/Documents.aspx">here</A> to access the document(s).</b><br><br>'

--Service Date Assumption 

        IF ( SELECT COUNT(docTitle)
             FROM   @NewComplaintsTable
           ) > 0 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<br>*Note: Date served is the latest service date for claimants'' lawsuits.<br><br>'
            END

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--SELECT @BodyMessage

SELECT SUBSTRING(@BodyMessage, 1, 4000)
SELECT SUBSTRING(@BodyMessage, 4001, 4000)

--Execute stored procedure to send the email to that specific users/firms
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'debra.littler@tuckerellis.com;marie.hughes@tuckerellis.com',
								@copy_recipients = '',
								@blind_copy_recipients = 'monahanc@kcic.com;pfizer@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'
END

UPDATE [KGA-ClaimsAdmin].dbo.tblComplaintTracking
SET	ExternalEmailDate = GETDATE(),
	ExternalRecipient = 'Pfizer Static Recipients'
WHERE ComplaintID IN (SELECT
	COALESCE(ComplaintID,0)
FROM @NewComplaintsTable
WHERE ComplaintID IS NOT NULL)
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints_summary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints_summary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints_summary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints_summary] TO [power_user]
GO
