SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
Create procedure [dbo].[qryALLSetMaxExhaustionDate]
(@allocationid int)
as

set nocount on

DECLARE @ExhaustionTypeID int
SET @ExhaustionTypeID = (Select exhaustiontypeid from tblAllocation where AllocationID = @AllocationID)


update tblAllocation 
Set ExhaustionDateID =
(select exhaustiondateid from tblExhaustionDate 
where ExhaustionDate = 
(select MAX(exhaustiondate) from tblExhaustionDate where ExhaustionTypeID = @exhaustionTypeID)
and ExhaustionTypeID = @exhaustionTypeID)
where AllocationID = @AllocationID


return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLSetMaxExhaustionDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLSetMaxExhaustionDate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLSetMaxExhaustionDate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLSetMaxExhaustionDate] TO [power_user]
GO
