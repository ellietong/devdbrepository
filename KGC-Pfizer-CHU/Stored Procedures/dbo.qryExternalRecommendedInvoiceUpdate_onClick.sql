SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---- =============================================
---- Author:            <Author,,Name>
---- Create date: <Create Date,,>
---- Description: <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalRecommendedInvoiceUpdate_onClick] 
AS
BEGIN

/*Query run daily to send a separate alert email to the designated Invoice Contacts for each firm when
an invoice has been accepted or rejected. - BEH*/

--Declare and set email inputs.
    DECLARE @Subject NVARCHAR(100)
    DECLARE @Header NVARCHAR(2000)

    SET @Subject =    'Pfizer: New Defense Invoices Recommended as of ' + CONVERT(varchar, GETDATE(), 107) 
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    


--Create a temp table for any invoices that have had a status change in the past day.
    DECLARE @NewRecommendedInvoices TABLE
        (
          invoiceNum NVARCHAR(50) ,
          status NVARCHAR(50) ,
          dateapproved DATETIME ,
          firmid INT ,
          feeAmount MONEY ,
          disbursementamount MONEY ,
          adjustment MONEY ,
          adjustmentnotes NVARCHAR(500)
        )
    INSERT  INTO @NewRecommendedInvoices
            SELECT  di.invoicenumber ,
                    status ,
                    RecommendedDate ,
                    COALESCE(di.firmid, di.VendorFirmID, 0) AS Firmid ,
                    SUM(COALESCE(dic.Fees, 0)) ,
                    SUM(COALESCE(dic.Costs, 0)) ,
                    SUM(COALESCE(dic.adjustments, 0)) ,
                    COALESCE(di.InvoiceNotes, '')                    
            FROM    tblDefenseInvoice di
                    INNER JOIN tblDefenseInvoiceStatus s ON s.StatusID = di.StatusID
                    INNER JOIN tblDefInvClaimantDetail dic ON dic.DefenseInvoiceID = di.DefenseInvoiceID
                    
            WHERE IsRecommended = 1 AND IsApproved = 0 AND di.StatusID = 1
            GROUP BY di.invoicenumber ,
                    status ,
                    RecommendedDate ,
                    COALESCE(di.firmid, di.VendorFirmID, 0),
                    di.InvoiceNotes ,
                    COALESCE(dic.Fees, 0) ,
                    COALESCE(dic.Costs, 0) ,
                    COALESCE(dic.adjustments, 0)

--Declare a cursor to create a separate email for each firm


            DECLARE @BodyMessage NVARCHAR(MAX)
            

SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>Below are any recommended invoices which have not been approved for review.</H4>'

            SET @BodyMessage = @BodyMessage
                + '<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid rgb(0,121,193); color: #F6ECF0; width: 700px;}
TD    {font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000}
TH    {border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
#extra {
      text-align: left;
      padding: .3em 0 .3em 0;
      margin-left: 5px;
      margin-right: 5px;
      }
--->
</STYLE>'


            SET @BodyMessage = @BodyMessage + '<br>'
                + 'Recommended Invoice(s):'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
            SET @BodyMessage = @BodyMessage
                + N'<table style="border-collapse: collapse; border: 1px solid rgb(0,121,193); color: #F6ECF0; width: 700px;">'
                + N'<tr><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Batch Number</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Firm</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Recommended</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Fee Amount</th>'
                + '<th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disbursement Amount</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Adjustment Amount</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Payment<br>Pending</th>'
            
            --create a cursor do loop through invoices and print appropriate HTML
            DECLARE @invoicenum NVARCHAR(50) ,
                @status NVARCHAR(100) ,
                @dateApproved NVARCHAR(100) ,
                @feeAmount NVARCHAR(100) ,
                @disbursementAmount NVARCHAR(100) ,
                @adjustment NVARCHAR(100) ,
                @pmtPending NVARCHAR(100) ,
                @adjustmentnotes NVARCHAR(MAX)
            
            DECLARE invoice_Curs CURSOR
            FOR
                SELECT  CONVERT(NVARCHAR(50), invoicenum, 1) ,
                        COALESCE(dca.DefenseCounsel, v.VendorName,'') AS FIRM ,
                        CONVERT(VARCHAR, C.dateapproved, 101) AS dateApproved ,
                        CONVERT(NVARCHAR(30), COALESCE(C.feeAmount, 0), 1) AS feeAmount ,
                        CONVERT(NVARCHAR(30), COALESCE(C.disbursementamount, 0), 1) AS disbursementAmount ,
                        CONVERT(NVARCHAR(30), COALESCE(adjustment, 0), 1) AS adjustment ,
                        CONVERT(NVARCHAR(30), COALESCE(( feeAmount
                                                         + disbursementAmount
                                                         - adjustment ), 0), 1) AS pmtPending ,
                        COALESCE('<u><b style="font-size: 11pt;" >Adjustment Notes</b></u>:   '
                                 + C.adjustmentnotes, '') AS adjustmentNotes
                FROM    @NewRecommendedInvoices C
                LEFT JOIN tblDefenseCounselAddress dca on dca.DefenseCounselID = C.FirmID
                LEFT JOIN tblVendor v on v.VendorID = C.firmid
               GROUP BY COALESCE(dca.DefenseCounsel, v.VendorName,'')  ,CONVERT(NVARCHAR(50), invoicenum, 1) ,
                        CONVERT(VARCHAR, C.dateapproved, 101)  ,
                        CONVERT(NVARCHAR(30), COALESCE(C.feeAmount, 0), 1)  ,
                        CONVERT(NVARCHAR(30), COALESCE(C.disbursementamount, 0), 1)  ,
                        CONVERT(NVARCHAR(30), COALESCE(adjustment, 0), 1) ,
                        CONVERT(NVARCHAR(30), COALESCE(( feeAmount
                                                         + disbursementAmount
                                                         - adjustment ), 0), 1) ,
                        COALESCE('<u><b style="font-size: 11pt;" >Adjustment Notes</b></u>:   '
                                 + C.adjustmentnotes, '') 
             ORDER BY FIRM, dateapproved 
                        

            OPEN invoice_curs
            FETCH NEXT FROM invoice_curs INTO @invoicenum, @status,
                @dateApproved, @feeAmount, @disbursementAmount, @adjustment,
                @pmtPending, @adjustmentnotes

            WHILE @@FETCH_STATUS = 0 
                BEGIN
            --start the table row
                    SET @BodyMessage = @BodyMessage + '<tr>'
                    SET @BodyMessage = @BodyMessage
                        + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">'
                        + @invoicenum + '</td>'
                    SET @BodyMessage = @BodyMessage
                        + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">'
                        + @status + '</td>'
                    SET @BodyMessage = @BodyMessage
                        + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">'
                        + @dateApproved + '</td>'
                    SET @BodyMessage = @BodyMessage
                        + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">$'
                        + @feeAmount + '</td>'
                    SET @BodyMessage = @BodyMessage
                        + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">$'
                        + @disbursementAmount + '</td>'
                    SET @BodyMessage = @BodyMessage
                        + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">$'
                        + @adjustment + '</td>'
                    SET @BodyMessage = @BodyMessage
                        + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">$'
                        + @pmtPending + '</td></tr>'
            --only have a separate row for notes if rec has an adjustment
                    IF @status = 'Approved with Adjustment' 
                        BEGIN
                            SET @BodyMessage = @BodyMessage
                                + '<tr><td colspan=7 style="text-align: left; margin-left: 93px;">'
                                + @adjustmentnotes + '</td></tr>'
                        END
                    FETCH NEXT FROM invoice_curs INTO @invoicenum, @status,
                        @dateApproved, @feeAmount, @disbursementAmount,
                        @adjustment, @pmtPending, @adjustmentnotes
                END
            CLOSE invoice_curs
            DEALLOCATE invoice_curs
            SET @BodyMessage = @BodyMessage + N'</table>' + N'<br>'    


--Link to access invoice library
            SET @BodyMessage = @BodyMessage + N'<b>Click <A Href = '
                + ( SELECT  'https://www.kcicextranet.com/clients/Pfizer/cp/Pages/Invoices.aspx'
                  ) + '>here</A> to access the invoice(s).</b><br><br>'


--SET @BodyMessage = @BodyMessage + N'<b>If you have any questions regarding the approval status of the above invoice(s), please contact ___.</b><br><br>'


            SET @BodyMessage = @BodyMessage + N'<br><br>KCIC, LLC<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333'




            

--Execute stored procedure to send email
EXEC msdb.dbo.sp_send_dbmail
@profile_name = 'Admin',
@recipients = 'owenss@kcic.com',
@subject = @subject, @body = @BodyMessage,
@body_format = 'html'
--print @BodyMessage
--Move to next firm
   

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalRecommendedInvoiceUpdate_onClick] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalRecommendedInvoiceUpdate_onClick] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalRecommendedInvoiceUpdate_onClick] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalRecommendedInvoiceUpdate_onClick] TO [power_user]
GO
