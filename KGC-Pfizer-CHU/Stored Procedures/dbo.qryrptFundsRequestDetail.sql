SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptFundsRequestDetail]

As
/*
10/17/11 - CAS - updated query to pull from payment module tables.
2/7/12 - CAS  - updated to pull data from vtblDefenseInvoice which incorporates allother needed tables with the new invoice structure.
*/

SET NOCOUNT ON

SELECT Firm, InvoiceNumber, Period, InvoiceTotal,convert(date, SubmitDate)as SubmitDate ,convert(date,DatePayable) as DatePayable, Fees,
	Costs, Adjustments as Adjustment, DI.StatusID, dis.Status,
coalesce(fees+costs, 0) as Subtotal,
coalesce(fees+costs-Adjustments, 0) as AmountPayable, 
DI.SourceDate as DatePaid, DI.SourceNumber as CheckNumber,
DI.sourceamount as AmtPaid, QCDate, QCBy
FROM	vtblDefenseInvoice DI
left JOIN	tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
WHERE	((SourceNumber IS NULL) AND 
DI.DatePayable <= (SELECT CONVERT(datetime, CONVERT(varchar, GETDATE(), 101), 101) + 30 AS OneMonthOut)
	AND (DI.StatusID IN (1, 2)))
	
ORDER BY firm, Status, DatePayable

--select * from vtblDefenseInvoice
--coalesce (vendorfirm, DefenseCounsel)
Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequestDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequestDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequestDetail] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequestDetail] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequestDetail] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequestDetail] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequestDetail] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequestDetail] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequestDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequestDetail] TO [power_user]
GO
