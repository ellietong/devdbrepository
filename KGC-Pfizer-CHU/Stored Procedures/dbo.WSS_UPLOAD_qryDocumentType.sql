SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryDocumentType]
    (
      @invoiceFlag INT = 0 ,
      @settlementFlag BIT = 0
    )
AS 
    SET NOCOUNT ON

--DECLARE @invoiceFlag bit
--DECLARE @settlementFlag bit
--SET @invoiceFlag    = 0
--SET @settlementFlag = 1

    IF @invoiceFlag = 0 
        BEGIN
            IF @settlementFlag = 1 
                BEGIN
                    SELECT  ClaimantDocumentTypeID ,
                            ClaimantDocumentType + ' (' + UPPER(ClaimantDocumentGroup) +')' AS ClaimantDocumentType,
                            cdt.ClaimantDocumentGroupID
                    FROM    tblClaimantDocumentType cdt
						INNER JOIN tblClaimantDocumentGroup  cdg on cdt.ClaimantDocumentGroupID =  cdg.ClaimantDocumentGroupID
                    WHERE   cdt.ClaimantDocumentGroupID IN (8,9,10,11,12,13,14,15,16,17,18,19,20,21,23)
                            AND ClaimantDocumentTypeID <> 1
                            AND ClaimantDocumentTypeID <> 16
                    ORDER BY ClaimantDocumentType
                END
            ELSE 
                BEGIN
                    SELECT  ClaimantDocumentTypeID ,
                            ClaimantDocumentType + ' (' + UPPER(ClaimantDocumentGroup) +')' AS ClaimantDocumentType,
                            cdt.ClaimantDocumentGroupID
                    FROM    tblClaimantDocumentType cdt
						INNER JOIN tblClaimantDocumentGroup  cdg on cdt.ClaimantDocumentGroupID =  cdg.ClaimantDocumentGroupID
                    WHERE   cdt.ClaimantDocumentGroupID IN (8,9,10,11,12,13,14,15,16,17,18,19,20,21,23)
                            AND ClaimantDocumentTypeID <> 1
                            AND ClaimantDocumentTypeID <> 16
                    ORDER BY ClaimantDocumentType
                END
        END
    ELSE 
        IF @invoiceFlag = 1 
            BEGIN
                SELECT  ClaimantDocumentTypeID ,
                        ClaimantDocumentType ,
                        ClaimantDocumentGroupID
                FROM    tblClaimantDocumentType
                WHERE   ClaimantDocumentGroupID = 2
                        AND ClaimantDocumentTypeID NOT IN ( 41, 42 )
                ORDER BY ClaimantDocumentType
            END
        ELSE 
            BEGIN
                SELECT  ClaimantDocumentTypeID ,
                        ClaimantDocumentType ,
                        ClaimantDocumentGroupID
                FROM    tblClaimantDocumentType
                WHERE   ClaimantDocumentGroupID = 2
                        AND ClaimantDocumentTypeID <> 42
                ORDER BY ClaimantDocumentType
            END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [power_user]
GO
