SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_REPORTS_qryNewFilingsByPlaintiffsCounselByJurisdictionByMonth] 

	(@StartDate datetime, @EndDate datetime, @ClaimTypeValueID int, @EmailAddress nvarchar(250))

 AS
 BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--DECLARE @ClaimTypeValueID int = null,
--@EmailAddress nvarchar(250) = 'owenss@kcic.com'

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)

DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )

DECLARE @QueryString NVARCHAR(MAX)
DECLARE @WhereString NVARCHAR(MAX) =  ' WHERE ((c.MatterID =1 and a.servicedate between ''' + CONVERT(NVARCHAR, @StartDate) + ''' and ''' + CONVERT(NVARCHAR, @EndDate) + ''' and DocketNumber <>''UNFILED'' ) 
											OR (cl.ClaimLawsuitID  in (3617371,3930921,4104422,4150163,3727290,3746595,3740572,3781433,3906063,3920744,3937709,4087312,4978702) 
												AND a.ServiceDate between ''' + CONVERT(NVARCHAR, @StartDate) + ''' and ''' + CONVERT(NVARCHAR, @EndDate) + '''))
												AND c.ClaimSequenceID in (4,5)
												AND (COALESCE(IsPrimaryClaimType, 0) = 1 OR ct.ClaimTypeID IS NULL) '
DECLARE @JoinString NVARCHAR(MAX)

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    IF ( ( RIGHT(@emailaddress, 11) = 'kcicllc.com' )
		   OR ( RIGHT(@emailaddress, 8) = 'kcic.com' )	
           OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        BEGIN
            SET @JoinString = ''
            --SET @WhereString = @WhereString + ' AND cl.IsPrimaryForClaim = 1'
        END
    ELSE 
        BEGIN
            SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
            SET @WhereString = @WhereString
                + ' AND (dj.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ' OR l.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ')'
        END

IF @ClaimTypeValueID IS NOT NULL
BEGIN
	IF @ClaimTypeValueID = 7
		BEGIN
			SET @WhereString =  @WhereString + ' AND (ct.ClaimTypeValueID IS NULL OR ct.ClaimTypeValueID = ' + CONVERT(NVARCHAR, @ClaimTypeValueID) + ')'
		END
	ELSE 
		BEGIN
			SET @WhereString =  @WhereString + ' AND ct.ClaimTypeValueID = ' + CONVERT(NVARCHAR, @ClaimTypeValueID)
		END
END

SET @QueryString = '
SELECT
  DISTINCT PlaintiffCounsel, 
	  Combination AS Jurisdiction, 
	  SUM(JanuaryCount) AS JanuaryCount, 
	  SUM(FebruaryCount) AS FebruaryCount, 
	  SUM(MarchCount) AS MarchCount,
	  SUM(AprilCount) AS AprilCount,
	  SUM(MayCount) AS MayCount,
	  SUM(JuneCount) AS JuneCount,
	  SUM(JulyCount) AS JulyCount,
	  SUM(AugustCount) AS AugustCount, 
	  SUM(SeptemberCount) AS SeptemberCount,
	  SUM(OctoberCount) AS OctoberCount,
	  SUM(NovemberCount) AS NovemberCount, 
	  SUM(DecemberCount) AS DecemberCount,
	  SUM(
		JanuaryCount +	
		FebruaryCount +	
		MarchCount +	 
		AprilCount +	
		MayCount +	
		JuneCount +	
		JulyCount +	
		AugustCount +	
		SeptemberCount +	
		OctoberCount +	
		NovemberCount +	
		DecemberCount
			  ) AS Total
FROM
(

SELECT DISTINCT CASE WHEN PlaintiffCounsel IS NULL THEN ''UNKNOWN'' ELSE PlaintiffCounsel END AS PlaintiffCounsel, 
		s.FullNameState, 
		CASE WHEN Combination IS NULL THEN CASE WHEN l.StateID IN (17,31,37,45) THEN s.State + '' - OTHER'' ELSE s.State END ELSE Combination END AS Combination, 
		DATENAME("mm", a.ServiceDate) + ''Count''  AS [MonthName],
		MONTH(a.ServiceDate) As MonthID,
		cl.ClaimLawsuitID--,
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
	LEFT JOIN (SELECT * from tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	LEFT JOIN tblState s on l.StateID=s.StateID
	LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
	INNER JOIN tblAllegation a on cl.ClaimLawsuitID = a.ClaimLawsuitID
	LEFT OUTER JOIN vREPORTS_StateJurisdictionCombinations sjc on  l.JurisdictionID = sjc.JurisdictionID
	LEFT JOIN tblPlaintiffCounselAddress pca ON l.PrimaryNationalPlaintiffCounselID = pca.PlaintiffCounselAddressID
'
+ @JoinString +
+ @WhereString +  
'
) AS details
PIVOT
(
COUNT(ClaimLawsuitID)
	FOR monthname IN(
					[JanuaryCount], 
					[FebruaryCount],
					[MarchCount],
					[AprilCount],
					[MayCount],
					[JuneCount],
					[JulyCount],
					[AugustCount],
					[SeptemberCount],
					[OctoberCount],
					[NovemberCount],
					[DecemberCount])
) AS Counts
GROUP BY PlaintiffCounsel, Combination
ORDER BY PlaintiffCounsel, Combination
'

--SELECT @QueryString
PRINT @QueryString
EXEC sp_executesql @QueryString	


END
GO
