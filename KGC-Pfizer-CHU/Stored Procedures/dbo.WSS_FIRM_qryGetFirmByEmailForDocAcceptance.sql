SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_FIRM_qryGetFirmByEmailForDocAcceptance]
    (
      @emailaddress NVARCHAR(1000)
    )
AS 
----Test Variables
--DECLARE @EmailAddress NVARCHAR(1000) = 'pfizer@kcic.com'

-- Declare the temporary fields used to track firm and email domain
    DECLARE @firmID INT
    DECLARE @emaildomain NVARCHAR(1000)

--Parse out the @emaildomain from the @emailaddress
    SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress),
                                 LEN(@emailaddress) - CHARINDEX('@',
                                                              @emailaddress)
                                 + 1)
--SELECT @emaildomain
--Set @firmID based on the @emaildomain
    IF ((@emaildomain = '@kcicllc.com') OR (@emaildomain = '@kcic.com'))
        BEGIN
            SET @firmID = 0 
        END
    ELSE 
        BEGIN
            SET @firmID = ( SELECT DISTINCT
                                    FirmID
                            FROM    vtblPerson
                                    INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
                            WHERE   @emailaddress = vtblPerson.Email
                          )
        END

--Return the @firmID
    SELECT  @firmID AS FirmID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmByEmailForDocAcceptance] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmByEmailForDocAcceptance] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmByEmailForDocAcceptance] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmByEmailForDocAcceptance] TO [power_user]
GO
