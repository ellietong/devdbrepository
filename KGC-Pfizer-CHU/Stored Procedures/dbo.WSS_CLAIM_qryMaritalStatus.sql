SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_CLAIM_qryMaritalStatus]
AS 
    SELECT  MaritalStatusID ,
            MaritalStatus
    FROM    tblMaritalStatus
    ORDER BY MaritalStatus

    RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryMaritalStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryMaritalStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryMaritalStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryMaritalStatus] TO [power_user]
GO
