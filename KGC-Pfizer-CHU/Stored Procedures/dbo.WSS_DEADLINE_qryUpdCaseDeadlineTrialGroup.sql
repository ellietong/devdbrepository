SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DEADLINE_qryUpdCaseDeadlineTrialGroup] 
	-- Add the parameters for the stored procedure here
	  @CaseDeadlineID BIGINT,
	  @TrialGroupID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

IF @CaseDeadlineID IS NOT NULL
BEGIN
	UPDATE tblCaseDeadline
	SET TrialGroupID = @TrialGroupID
	WHERE CaseDeadlineID = @CaseDeadlineID
END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryUpdCaseDeadlineTrialGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryUpdCaseDeadlineTrialGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryUpdCaseDeadlineTrialGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryUpdCaseDeadlineTrialGroup] TO [power_user]
GO
