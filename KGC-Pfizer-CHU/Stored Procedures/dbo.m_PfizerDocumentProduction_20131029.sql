SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_PfizerDocumentProduction_20131029]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent etra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
select DISTINCT d.documentID, d.documentTitle,d.ClaimantDocumentTypeID, cdt.ClaimantDocumentType, d.documentURL, AmazonKey			
from tblMultiClaimantDocument mcd			
inner join tblDocuments d on mcd.DocumentID = d.DocumentID
inner join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
where claimantpersonalinfoid = 531250 and d.documentid>1676130

END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_PfizerDocumentProduction_20131029] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_PfizerDocumentProduction_20131029] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_PfizerDocumentProduction_20131029] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_PfizerDocumentProduction_20131029] TO [power_user]
GO
