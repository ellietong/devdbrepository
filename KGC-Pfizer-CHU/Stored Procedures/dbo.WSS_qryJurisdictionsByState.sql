SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryJurisdictionsByState] ( @StateID INT )
AS 
    BEGIN
        SET NOCOUNT ON;

	--SELECT DISTINCT dj.JurisdictionID, Jurisdiction
	--FROM tblDefenseJurisdiction dj
	--INNER JOIN tblJurisdiction j ON dj.JurisdictionID = j.JurisdictionID
	--WHERE StateID = @StateID
	
        SELECT DISTINCT
                l.JurisdictionID ,
                Jurisdiction
        FROM    tblLawsuit l
                INNER JOIN tblJurisdiction j ON l.JurisdictionID = j.JurisdictionID
        WHERE   StateID = @StateID
        UNION
        SELECT  NULL ,
                NULL
		ORDER BY Jurisdiction
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryJurisdictionsByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryJurisdictionsByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryJurisdictionsByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryJurisdictionsByState] TO [power_user]
GO
