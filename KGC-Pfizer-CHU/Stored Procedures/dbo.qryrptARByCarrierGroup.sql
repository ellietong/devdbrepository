SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByCarrierGroup]
(@allocationID int =  null, @CarrierGroupID int = NULL)
As

SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 56
--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = 0

DECLARE @maxAllocationid int
DECLARE @maxCarrierGroupID int

SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
IF @CarrierGroupID = 0 SET @CarrierGroupID = NULL


DECLARE @Allocation TABLE (AllocationID int, TotalSIR money, TotalAllocation money, PVTotalAllocation money)
INSERT INTO @Allocation

SELECT AllocationID, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
WHERE AllocationID = @allocationID
GROUP BY AllocationID


DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
CarrierID int, CarrierName nvarchar(250))
INSERT INTO @Policy

SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName
FROM vPolicy
GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName

SELECT COALESCE(pinfo.CarrierGroupName, pinfo.CarrierName) AS 'CarrierGroupInfo', ar.DateToPresentValue, pinfo.CarrierGroupID, MAX(ar.DateTime) AS [DateTime], 
	pinfo.CarrierGroupName, CONVERT(money, SUM(ar.PV)) AS PV, SUM(ar.Allocation) AS Allocation, SUM(ar.SIRAllocated) AS SIRAllocated, a.AllocationID, 
	a.AllocationDescription, YieldCurveDate as PVRateDate, TotalSIR, TotalAllocation, PVTotalAllocation
FROM vAllocationResults ar 
	LEFT JOIN @Policy pinfo ON ar.PolicyDateID = pinfo.PolicyDateID
	LEFT JOIN vAllocation a ON a.AllocationID = ar.AllocationID
	LEFT JOIN tblYieldCurve yc on yc.YieldCurveID = ar.YieldCurveID 
	LEFT JOIN @Allocation at ON ar.AllocationID = at.AllocationID
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) AND
	(coalesce(pinfo.CarrierGroupID,0) >= COALESCE(@CarrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID,0) <= COALESCE(@CarrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID,0) = @CarrierGroupID)
GROUP BY COALESCE(pinfo.CarrierGroupName, pinfo.CarrierName) , ar.DateToPresentValue, pinfo.CarrierGroupID, 
	pinfo.CarrierGroupName, a.AllocationID, a.AllocationDescription, YieldCurveDate, TotalSIR, TotalAllocation, PVTotalAllocation
ORDER BY COALESCE (pinfo.CarrierGroupName, pinfo.CarrierName)

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByCarrierGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByCarrierGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByCarrierGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByCarrierGroup] TO [power_user]
GO
