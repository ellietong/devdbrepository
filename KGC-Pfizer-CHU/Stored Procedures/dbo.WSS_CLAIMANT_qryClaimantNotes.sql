SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryClaimantNotes]
    (
      @ClaimantPersonalInfoID BIGINT = NULL ,
      @emailaddress NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @emailaddress nvarchar(250)
--DECLARE @ClaimantPersonalInfoID bigint
--SET @emailaddress = 'owenss@kcic.com'
--SET @ClaimantPersonalInfoID = 953358

    DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE cpi.ClaimantPersonalInfoID = '
        + CONVERT(NVARCHAR(20), @ClaimantPersonalInfoID)
						  
						   
						   


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    DECLARE @JoinString NVARCHAR(MAX)
    IF ( ( RIGHT(@emailaddress, 11) = 'kcicllc.com' )
		 OR ( RIGHT(@emailaddress, 8) = 'kcic.com' )
         OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        SET @JoinString = ' '
    ELSE 
        BEGIN
            SET @JoinString = ' LEFT  JOIN tblClaim c					ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						LEFT  JOIN tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
                        LEFT  JOIN tblLawsuit l					ON cl.LawsuitID = l.LawsuitID
						INNER JOIN tblDefenseJurisdiction dj	ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID '
            SET @WhereString = @WhereString
                + ' AND (dj.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ' OR l.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ')'
        END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
    DECLARE @QueryString NVARCHAR(MAX)
    SET @QueryString = 'SELECT DISTINCT cpi.ClaimantPersonalInfoID, 
										ClaimantNotes,
										CASE WHEN ClaimantNotes IS NULL THEN ''There are no claimant notes available.'' ELSE REPLACE(REPLACE(ClaimantNotes, CHAR(13), ''</br>''), CHAR(10), ''</br>'') END AS ClaimantNotesFormat,
										CASE WHEN ClaimantNotes IS NULL THEN ''Add'' ELSE ''Edit'' END AS btnText
    FROM tblClaimantPersonalInfo cpi 
	  ' + @JoinString
        + @WhereString + '
    OPTION (RECOMPILE)'

--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString

--SELECT 1 AS ClaimantPersonalInfoID, 'ALL' AS ClaiamantNotes, 'ALL' AS ClaimantNotesFormat
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantNotes] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantNotes] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantNotes] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantNotes] TO [power_user]
GO
