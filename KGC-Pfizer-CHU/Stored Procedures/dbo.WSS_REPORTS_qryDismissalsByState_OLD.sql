SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryDismissalsByState_OLD] 
	(@StartDate datetime, @EndDate datetime, @ClaimTypeValueID int, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Declare @emailAddress nvarchar(250)
--Declare @StartDate datetime
--Declare @EndDate datetime
--Declare @ClaimTypeValueID int

--Set @emailAddress = 'owenss@kcic.com'
--Set @StartDate = '1/1/2012'
--Set @EndDate = '4/1/2012'
--Set @ClaimTypeValueID = 1

DECLARE @MaxClaimTypeValueID int
SET @MaxClaimTypeValueID = (SELECT MAX(ClaimTypeValueID) FROM tblClaimTypeValue)

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = 0--(select distinct InsurerRole from vtblPerson where Email = @emailaddress)

--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@EmailAddress, 11) = 'kcicllc.com' OR RIGHT(@EmailAddress, 8) = 'kcic.com' OR @NoFilters = 1)
BEGIN

select FullNameState, NULL AS Combination,  COUNT(distinct(cl.ClaimLawsuitID)) AS Count, COUNT(distinct(cl.ClaimLawsuitID)) AS StateCount
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
where coalesce(ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID =2 and ClaimSequenceID in (4,5) 
		and  IsPrimaryStatus=1
		and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
group by FullNameState
--order by COUNT(distinct(cl.ClaimLawsuitID)) desc
UNION
select s.FullNameState, Jurisdiction AS Combination, COUNT(distinct(cl.ClaimLawsuitID)) as Count, 0
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct					  ON c.ClaimID = ct.ClaimID
INNER JOIN vREPORTS_StateJurisdictionCombinations sjc on l.JurisdictionID = sjc.JurisdictionID
where coalesce(ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID =2 and ClaimSequenceID in (4,5) 
		and  IsPrimaryStatus=1
		and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
group by s.FullNameState, Jurisdiction
order by s.FullNameState, Combination


END
ELSE IF(@InsurerRole = 1) --Insurers
BEGIN

select FullNameState, NULL AS Combination,  COUNT(distinct(cl.ClaimLawsuitID)) AS Count, COUNT(distinct(cl.ClaimLawsuitID)) AS StateCount
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
where coalesce(ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID =2 and ClaimSequenceID in (4,5) 
		and  IsPrimaryStatus=1
		and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
group by FullNameState
--order by COUNT(distinct(cl.ClaimLawsuitID)) desc
UNION
select s.FullNameState, Jurisdiction AS Combination, COUNT(distinct(cl.ClaimLawsuitID)) as Count, 0
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct					  ON c.ClaimID = ct.ClaimID
INNER JOIN vREPORTS_StateJurisdictionCombinations sjc on l.JurisdictionID = sjc.JurisdictionID
where coalesce(ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID =2 and ClaimSequenceID in (4,5) 
		and  IsPrimaryStatus=1
		and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
group by s.FullNameState, Jurisdiction
order by s.FullNameState, Combination

END
ELSE
BEGIN

select FullNameState As State, NULL AS Combination,  COUNT(distinct(cl.ClaimLawsuitID)) AS Count, COUNT(distinct(cl.ClaimLawsuitID)) AS StateCount
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
where coalesce(ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID =2 and ClaimSequenceID in (4,5) 
		and  IsPrimaryStatus=1
		and (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
		and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
group by FullNameState
--order by COUNT(distinct(cl.ClaimLawsuitID)) desc
UNION
select s.FullNameState, Jurisdiction AS Combination, COUNT(distinct(cl.ClaimLawsuitID)) as Count, 0
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
LEFT JOIN tblClaimType ct					  ON c.ClaimID = ct.ClaimID
INNER JOIN vREPORTS_StateJurisdictionCombinations sjc on l.JurisdictionID = sjc.JurisdictionID
where coalesce(ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and ClaimLawsuitStatusGroupID =2 and ClaimSequenceID in (4,5) 
		and  IsPrimaryStatus=1
		and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
group by s.FullNameState, Jurisdiction
order by s.FullNameState, Combination

END



END



GO
