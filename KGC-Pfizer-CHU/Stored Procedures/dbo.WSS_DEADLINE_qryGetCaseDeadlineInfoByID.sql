SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_DEADLINE_qryGetCaseDeadlineInfoByID]
    (
	  @CaseDeadlineID int
	)
AS 
    SET NOCOUNT ON
   
--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
IF OBJECT_ID('tempdb..#CaseDeadlineClaimants') IS NOT NULL 
    DROP TABLE #CaseDeadlineClaimants
CREATE TABLE #CaseDeadlineClaimants
    (
		CaseDeadlineID int,
		ClaimLawsuitID bigint
	)
INSERT INTO #CaseDeadlineClaimants
SELECT DISTINCT CaseDeadlineID, MAX(clcd.ClaimLawsuitID) AS ClaimLawsuitID
FROM tblClaimLawsuitCaseDeadline clcd
INNER JOIN tblClaimLawsuit cl ON clcd.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID						 
WHERE CaseDeadlineID = @CaseDeadlineID
GROUP BY CaseDeadlineID

IF object_id('tempdb..#TrialGroupClaimantLists') IS NOT NULL
BEGIN
	DROP TABLE #TrialGroupClaimantLists
END
CREATE TABLE #TrialGroupClaimantLists 
	(
		CaseDeadlineID int,
		ClaimLawsuitID bigint
	)
BEGIN
	INSERT INTO #TrialGroupClaimantLists
	SELECT DISTINCT CaseDeadlineID, MAX(ClaimLawsuitID) AS ClaimLawsuitID
	FROM tblCaseDeadline cd
		INNER JOIN tblClaimLawsuit cl ON cd.TrialGroupID = cl.TrialGroupID
	WHERE CaseDeadlineID = @CaseDeadlineID 
	GROUP BY CaseDeadlineID

END

IF OBJECT_ID('tempdb..#CaseNames') IS NOT NULL 
    DROP TABLE #CaseNames
CREATE TABLE #CaseNames
    (
		CaseDeadlineID bigint,
		Jurisdiction NVARCHAR(250),
		JurisdictionID int,
		DefenseCounsel NVARCHAR(250),
		DefenseCounselAddressID int,
		PlaintiffCounsel NVARCHAR(250),
		PlaintiffCounselID int,
		State NVARCHAR(25),
		StateID int,
		ClaimType NVARCHAR(max)
    )
INSERT INTO #CaseNames
SELECT DISTINCT CaseDeadlineID, 
		Jurisdiction, 
		l.JurisdictionID,
		DefenseCounsel, 
		l.DefenseCounselAddressID,
		PlaintiffCounsel,
		PlaintiffCounselID,
		s.state, 
		l.StateID,
		dbo.fnClaimTypesByClaimID(c.ClaimID) AS ClaimType
FROM tblClaimantPersonalInfo cpi
	INNER JOIN dbo.tblClaim                  c    ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit               cl   ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit                    l    ON cl.LawsuitID = l.LawsuitID 
	LEFT JOIN tblPlaintiffCounselAddress     pca  ON l.PrimaryNationalPlaintiffCounselID = pca.PlaintiffCounselAddressID
	LEFT JOIN tblDefenseCounselAddress		 dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN tblJurisdiction 				 j	ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblState                       s    ON l.StateID = s.StateID
	INNER JOIN #CaseDeadlineClaimants cdc ON cl.ClaimLawsuitID = cdc.ClaimLawsuitID 
UNION
SELECT  CaseDeadlineID,
		Jurisdiction, 
		l.JurisdictionID,
		DefenseCounsel, 
		l.DefenseCounselAddressID,
		PlaintiffCounsel,
		PlaintiffCounselID,
		s.state, 
		l.StateID,
		dbo.fnClaimTypesByClaimID(cl.ClaimID) AS ClaimType
FROM tblClaimLawsuit cl              
	INNER JOIN tblLawsuit							 l    ON cl.LawsuitID = l.LawsuitID 
	LEFT JOIN tblPlaintiffCounselAddress     pca  ON l.PrimaryNationalPlaintiffCounselID = pca.PlaintiffCounselAddressID
	LEFT JOIN tblDefenseCounselAddress				dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN tblJurisdiction 						j	ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblState                                 s    ON l.StateID = s.StateID
	INNER JOIN #TrialGroupClaimantLists tgcl ON tgcl.ClaimLawsuitID = cl.ClaimLawsuitID


--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE 1=1 '
                
    IF @CaseDeadlineID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND cd.CaseDeadlineID = '
                + CONVERT(NVARCHAR(20), @CaseDeadlineID)
        END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
    DECLARE @QueryString NVARCHAR(MAX)
    SET @QueryString = '
SELECT DISTINCT
	cd.CaseDeadlineID,
	dt.DeadlineTypeID,
	UPPER(DeadlineType) AS DeadlineType,
	cd.EventTypeID,
	UPPER(EventType) AS EventType,
	cd.DetailedEventTypeID,
	UPPER(DetailedEventType) AS DetailedEventType,
	CASE WHEN DeadlineDate IS NOT NULL 
		 THEN CONVERT(NVARCHAR(15), DeadlineDate, 101) + '' '' ELSE ''TBD'' END +  
	CASE WHEN DeadlineTime IS NOT NULL 
		THEN + '' @ '' + 		
	CASE WHEN DATEPART(hour,DeadlineTime) in (10, 11, 12, 22, 23, 24) 
		THEN CONVERT(varchar(5),CAST(DeadlineTime AS TIME),109)
	ELSE CONVERT(varchar(4),CAST(DeadlineTime AS TIME),109) END
		 ELSE '''' END + 
	CASE WHEN DeadlineTime IS NOT NULL AND TimePeriod IS NOT NULL 
		THEN + '' '' + TimePeriod ELSE '''' END + 
	CASE WHEN DeadlineTime IS NOT NULL AND Abbreviation IS NOT NULL 
		THEN  + '' '' + Abbreviation ELSE '''' END AS FullDeadlineDate,
	DeadlineDate,
	LEFT(DeadlineTime, 5) AS DeadlineTime,
	cd.TimePeriodID,
	TimePeriod,
	cd.TimeZoneID,
	TimeZone,
	UPPER(PlaintiffCounsel) AS PlaintiffCounsel,
	PlaintiffCounselID,
	UPPER(DefenseCounsel) AS DefenseCounsel,
	DefenseCounselAddressID,
	cn.State, 
	UPPER(Jurisdiction) AS County,
	cd.EventLocationID,
	CASE WHEN cd.EventLocationID IS NOT NULL THEN 
		UPPER(
		CASE WHEN LocationName IS NULL THEN '''' ELSE LocationName END +
		CASE WHEN LocationName IS NOT NULL AND (Address1 IS NULL AND Address2 IS NULL AND City IS NULL AND s.State IS NULL AND Zip IS NULL) THEN ''''
			 WHEN LocationName IS NULL AND (Address1 IS NOT NULL OR Address2 IS NOT NULL OR City IS NOT NULL OR s.State IS NOT NULL OR Zip IS NOT NULL) THEN '''' 
			 ELSE '': '' END +
		+ COALESCE(Address1 + '' '', '''') 
		+ COALESCE(Address2 + '' '', '''') 
		+ COALESCE(City, '''') 
		+ CASE WHEN City IS NULL OR (s.State IS NULL AND Zip IS NULL) THEN '''' ELSE '', '' END 
		+ COALESCE(s.State + '' '', '''')  + COALESCE(Zip, '''')
		) END AS EventLocation,	 
	LocationName,
	Deponent,
	Attendees,
	cd.AttendanceModeID,
	AttendanceMode,
	CreatedBy,
	DateCreated,
	Comments,
	UPPER(ClaimType) AS ClaimType,
	cd.DeadlineStatusID,
	UPPER(DeadlineStatus) AS DeadlineStatus,
	cd.TrialGroupID
FROM tblCaseDeadline cd
INNER JOIN tblEventType et ON cd.EventTypeID = et.EventTypeID
LEFT JOIN tblDetailedEventType det ON cd.DetailedEventTypeID = det.DetailedEventTypeID
INNER JOIN tblDeadlineType dt on et.DeadlineTypeID = dt.DeadlineTypeID
LEFT JOIN tblTimeZone tz ON cd.TimeZoneID = tz.TimeZoneID
LEFT JOIN tblTimePeriod tp ON cd.TimePeriodID = tp.TimePeriodID
LEFT JOIN tblEventLocation el ON cd.EventLocationID = el.EventLocationID
LEFT JOIN tblAttendanceMode am ON cd.AttendanceModeID = am.AttendanceModeID
LEFT JOIN tblState s ON el.StateID = s.StateID
LEFT JOIN tblDeadlineStatus ds on cd.DeadlineStatusID = ds.DeadlineStatusID
LEFT JOIN tblTrialGroup tg ON cd.TrialGroupID = tg.TrialGroupID
INNER JOIN #CaseNames cn ON cd.CaseDeadlineID = cn.CaseDeadlineID
' + @WhereString + '
OPTION (RECOMPILE)'

    --SELECT @QueryString
--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryGetCaseDeadlineInfoByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryGetCaseDeadlineInfoByID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryGetCaseDeadlineInfoByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryGetCaseDeadlineInfoByID] TO [power_user]
GO
