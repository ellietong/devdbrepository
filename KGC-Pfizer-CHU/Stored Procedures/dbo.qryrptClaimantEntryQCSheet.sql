SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptClaimantEntryQCSheet]
(@ClaimantPersonalInfoID bigint)

As
Set NoCount On

Select	CPI.ClaimantPersonalInfoID, FirstName, LastName, MiddleName, SocialSecurityNumber, MaritalStatus, Gender, CompleteAddress,
	PrimD.Disease as PlaintiffDisease, DefD.Disease as DefenseDisease, C.PlaintiffDiseaseDiagnosisDate,
	C.DefenseDiseaseDiagnosisDate, BirthDate, DeceasedDate
From tblClaimantPersonalInfo CPI
LEFT JOIN tblClaim C on CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
LEFT Join tblDisease PrimD on PrimD.DiseaseID= C.PlaintiffDiseaseID
LEFT Join tblDisease DefD on DefD.DiseaseID=C.DefenseDiseaseID
LEFT Join tblMaritalStatus MS on MS.MaritalStatusID=CPI.MaritalStatusID
LEFT Join tblGender G on G.GenderID=CPI.GenderID

Where CPI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantEntryQCSheet] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantEntryQCSheet] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantEntryQCSheet] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantEntryQCSheet] TO [power_user]
GO
