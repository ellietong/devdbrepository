SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_DISMISSAL_qryClaimsForDismissal]
(@CaseID int = 0)
As
Set NoCount On

--Any error syntax highlighting on ##DismissalDocs is incorrect
--Global temp table is created in WSS_DISMISSAL_qryDismissalDocs
--The code here should be correct when used following that query

DECLARE @ConnectionString nvarchar(max)
SET @ConnectionString = '[' + (SELECT MAX(ConnectionStringKey) FROM tblCase WHERE CaseID = @CaseID) + '].[DBO].'

DECLARE @QueryString nvarchar(max) = ''
SET @QueryString = '
SELECT 
	mcd.DocumentID,
	LastName + coalesce('', '' + FirstName + '' '' + MiddleName,'', '' + FirstName ,'', '' +  MiddleName, '''') as ClaimantName,
	SocialSecurityNumber,
	IndividualDocketNumber,
	CaseCaption,
	DocketNumber,
	clst.StatusNotes,
	cpi.ClaimantPersonalInfoID,
	State,
	cl.ClaimLawsuitID,
	clst.ClaimLawsuitStatusTrackingID,
	ProcessedDate,
	StatusDate,
	ClaimLawsuitStatus,
	clst.ClaimLawsuitStatusID,
	CASE WHEN IsPrimaryStatus = 1 THEN ''Yes'' ELSE ''No'' END as IsPrimary,
	clst.ClaimLawsuitStatusTrackingID,
	cls.ClaimLawsuitStatusID
FROM ' + @ConnectionString + 'tblClaimantPersonalInfo cpi
	INNER JOIN ' + @ConnectionString + 'tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	INNER JOIN ' + @ConnectionString + 'tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	INNER JOIN ' + @ConnectionString + 'tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
	LEFT JOIN  (select * from ' + @ConnectionString + 'tblClaimLawsuitStatusTracking where IsPrimaryStatus =1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
	--LEFT JOIN  tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN  ' + @ConnectionString + 'tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
	INNER JOIN ' + @ConnectionString + 'tblLawsuit l on l.LawsuitID=cl.LawsuitID
	INNER JOIN ' + @ConnectionString + 'tblState s on s.StateID=l.StateID
	INNER JOIN ' + @ConnectionString + 'tblDocuments d on mcd.DocumentID = d.DocumentID
WHERE d.ForQC = 1 AND d.QCBy IS NULL AND d.QCDate IS NULL
GROUP BY
	DocketNumber, IndividualDocketNumber,LastName, MiddleName, FirstName,cpi.ClaimantPersonalInfoID, CaseCaption, DocketNumber, clst.StatusNotes,IsPrimaryStatus,
	State, cl.ClaimLawsuitID, SocialSecurityNumber,clst.ClaimLawsuitStatusTrackingID, ProcessedDate, ClaimLawsuitStatus, StatusDate, clst.ClaimLawsuitStatusID, mcd.DocumentID,
	clst.ClaimLawsuitStatusTrackingID, cls.ClaimLawsuitStatusID'

Print @QueryString
EXEC sp_executesql @QueryString
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_qryClaimsForDismissal] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_qryClaimsForDismissal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_qryClaimsForDismissal] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_qryClaimsForDismissal] TO [power_user]
GO
