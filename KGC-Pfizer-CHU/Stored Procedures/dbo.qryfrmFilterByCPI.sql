SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmFilterByCPI]
(@CPIID bigint)
As
SET NOCOUNT ON

--declare @CPIID bigint=973728


UPDATE tblSettlePaymentDocumentation
SET ReleaseTemplateTypeID =
	CASE
		WHEN ReleaseTemplateTypeID IS NULL THEN dbo.fnDetermineReleaseTemplateType(cpi.ClaimantPersonalInfoID) ELSE ReleaseTemplateTypeID
	END
FROM tblClaimantPersonalInfo cpi
WHERE cpi.ClaimantPersonalInfoID = @CPIID


SELECT
	LastName,
	FirstName,
	cpi.ClaimantPersonalInfoID,
	DocketNumber,
	LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4) AS SSN,
	CASE
		WHEN DeceasedDate IS NOT NULL AND (COALESCE(SettlementConfirmation, 0) + COALESCE(ClaimantIDed, 0) + COALESCE(DxMedicals, 0) + COALESCE(DODCert, 0) + COALESCE(EstatePapers, 0) + COALESCE(ProductID, 0) + COALESCE(EmployExpoHist, 0)) = 7 THEN 'Complete'
		WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation, 0) + COALESCE(ClaimantIDed, 0) + COALESCE(DxMedicals, 0) + COALESCE(ProductID, 0) + COALESCE(EmployExpoHist, 0)) = 5 THEN 'Complete' ELSE 'Documentation Needed'
	END AS SupportingDocumentation,
	sd.ReleaseTemplateTypeID,
	ReleaseTemplateType,
	cl.ClaimLawsuitID,
	HighLevelDisease,
	SettlementGroup,
	cs.SettlementAmount
FROM tblClaimantPersonalInfo cpi
LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
LEFT JOIN (SELECT
	*
FROM tblClaimDisease
WHERE PrimaryDisease = 1)
cd ON cd.ClaimID = c.ClaimID
LEFT JOIN tblDisease d ON d.DiseaseID = cd.DiseaseID
LEFT JOIN tblHighLevelDisease hd ON hd.HighLevelDiseaseID = d.HighLevelDiseaseID
LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
LEFT JOIN tblClaimantSettlement cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblSettlementGroup sg ON sg.SettlementGroupID = cs.SettlementGroupID
LEFT JOIN tblSettlePaymentDocumentation sd ON sd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblReleaseTemplateType rtt ON rtt.ReleaseTemplateTypeID = sd.ReleaseTemplateTypeID
WHERE cpi.ClaimantPersonalInfoID = @CPIID
GROUP BY	LastName,
			cpi.ClaimantPersonalInfoID,
			FirstName,
			HighLevelDisease,
			SettlementGroup,
			sd.ReleaseTemplateTypeID,
			ReleaseTemplateType,
			cl.ClaimLawsuitID,
			DocketNumber,
			LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4),
			SettlementConfirmation,
			ClaimantIDed,
			DxMedicals,
			DODCert,
			EstatePapers,
			ProductID,
			EmployExpoHist,
			DeceasedDate,
			cs.SettlementAmount
Return
GO
GRANT TAKE OWNERSHIP ON  [dbo].[qryfrmFilterByCPI] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmFilterByCPI] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmFilterByCPI] TO [base_user]
GRANT CONTROL ON  [dbo].[qryfrmFilterByCPI] TO [power_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qryfrmFilterByCPI] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmFilterByCPI] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmFilterByCPI] TO [power_user]
GO
