SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[WSS_SERVICES_qryGetDetailedEventTypes]
(@PrefixText nvarchar(200), @Emailaddress nvarchar(250) = null, @DeadlineTypeID int )
AS
BEGIN
	SELECT DetailedEventTypeID, UPPER(DetailedEventType) AS DetailedEventType
	FROM tblDetailedEventType
	WHERE DetailedEventType LIKE '%' + @PrefixText + '%'
	AND DeadlineTypeID = @DeadlineTypeID
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetDetailedEventTypes] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetDetailedEventTypes] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetDetailedEventTypes] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetDetailedEventTypes] TO [power_user]
GO
