SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmCheckAddSettlement] (@ClaimantSettlementID bigint, @CheckID int, @PaymentAmount money)
As
Set NoCount On

Declare @CurrentFirmID int
Set @CurrentFirmID = (select PlaintiffCounselAddressID from tblClaimLawsuit cl 
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID 
Inner Join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
where ClaimantSettlementID = @ClaimantSettlementID)

Declare @AcceptableFirmID int
Set @AcceptableFirmID = (Select Coalesce(PlaintiffCounselAddressID,@CurrentFirmID) From tblPaymentSettlement ps 
Inner Join tblClaimantSettlement cs on cs.ClaimantSettlementID = ps.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID 
Where CheckID = @CheckID
Group By Coalesce(PlaintiffCounselAddressID,@CurrentFirmID))

Declare @CurrentMatterID int
Set @CurrentMatterID = (Select MatterID From tblClaim c
Inner Join tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
Inner Join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
where ClaimantSettlementID = @ClaimantSettlementID)

Declare @AcceptableMatterID int
Set @AcceptableMatterID = (Select Coalesce(MatterID,@CurrentMatterID) From tblPaymentSettlement ps
Inner Join tblClaimantSettlement cs on cs.ClaimantSettlementID = ps.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblClaim c on c.ClaimID = cl.ClaimID
Where CheckID = @CheckID Group By Coalesce(MatterID,@CurrentMatterID))

Declare @DefenseCheck int
Set @DefenseCheck = (Select COUNT(*) from tblPaymentDefense where CheckID = @CheckID)

Declare @ApprovedCheck int
Set @ApprovedCheck = (Select Case when ca.CheckID is not null Then 1 Else 0 End
From tblCheck c Left Join tblCheckApproval ca on c.CheckID = ca.CheckID where c.CheckID = @CheckID)

Declare @ClaimantAlreadyAdded int
Set @ClaimantAlreadyAdded = Coalesce((Select COUNT(*) from tblPaymentSettlement where CheckID = @CheckID and ClaimantSettlementID = @ClaimantSettlementID),0)

If ((@AcceptableFirmID = @CurrentFirmID AND @CurrentMatterID = @AcceptableMatterID AND @ApprovedCheck = 0) Or (@AcceptableFirmID is Null)) AND @DefenseCheck = 0 AND @ClaimantAlreadyAdded = 0 Begin
	Insert Into tblPaymentSettlement (ClaimantSettlementID, CheckID, PaymentAmount)
	Select @ClaimantSettlementID, @CheckID, @PaymentAmount
End

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddSettlement] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddSettlement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddSettlement] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddSettlement] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddSettlement] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddSettlement] TO [power_user]
GO
