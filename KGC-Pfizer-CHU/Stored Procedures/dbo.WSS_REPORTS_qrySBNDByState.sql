SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qrySBNDByState] 
	-- Add the parameters for the stored procedure here
	(@StartDate datetime, @EndDate datetime, @ClaimTypeValueID int, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--declare @StartDate datetime = '1/1/2010'
--, @EndDate datetime = '7/6/2014'
--, @ClaimTypeValueID int, @EmailAddress nvarchar(250) = 'owenss@kcic.com'
	
DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)

DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )

DECLARE @QueryString NVARCHAR(MAX)
DECLARE @WhereString NVARCHAR(MAX) =  ' WHERE coalesce(StatusDate, ProcessedDate, ''1/1/1900'') between ''' + CONVERT(NVARCHAR, @StartDate) + ''' and ''' + CONVERT(NVARCHAR, @EndDate) + '''
												AND clst.ClaimLawsuitStatusID = 25 and ClaimSequenceID in (4,5) 
												AND IsPrimaryStatus=1
												AND (COALESCE(IsPrimaryClaimType, 0) = 1 OR ct.ClaimTypeID IS NULL)
												AND sg.SettlementGroupTypeID = 5'
DECLARE @JoinString NVARCHAR(MAX)

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    IF ( ( RIGHT(@emailaddress, 11) = 'kcicllc.com' )
		   OR ( RIGHT(@emailaddress, 8) = 'kcic.com' )	
           OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        BEGIN
            SET @JoinString = ''
            --SET @WhereString = @WhereString + ' AND cl.IsPrimaryForClaim = 1'
        END
    ELSE 
        BEGIN
            SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
            SET @WhereString = @WhereString
                + ' AND (dj.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ' OR l.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ')'
        END

IF @ClaimTypeValueID IS NOT NULL
BEGIN
	IF @ClaimTypeValueID = 7
		BEGIN
			SET @WhereString =  @WhereString + ' AND (ct.ClaimTypeValueID IS NULL OR ct.ClaimTypeValueID = ' + CONVERT(NVARCHAR, @ClaimTypeValueID) + ')'
		END
	ELSE 
		BEGIN
			SET @WhereString =  @WhereString + ' AND ct.ClaimTypeValueID = ' + CONVERT(NVARCHAR, @ClaimTypeValueID)
		END
END

SET @QueryString = '
SELECT	s.FullNameState, 
		CASE WHEN Combination IS NULL THEN CASE WHEN l.StateID IN (17,31,37,45) THEN s.State + '' - OTHER'' ELSE s.State END ELSE Combination END AS Combination, 
		COUNT(distinct(cl.ClaimLawsuitID)) AS Count,
		sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity,
		COUNT(distinct(cl.ClaimLawsuitID)) as StateCount, 
		sum(COALESCE(SettlementAmount, 0)) as StateIndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as StateAverageIndemnity
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
INNER JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct on c.ClaimID = ct.ClaimID
INNER JOIN tblSettlementGroup sg on cs.SettlementGroupID = sg.SettlementGroupID
LEFT OUTER JOIN vREPORTS_StateJurisdictionCombinations sjc on  l.JurisdictionID = sjc.JurisdictionID
'
+ @JoinString +
+ @WhereString +  
'
group by s.FullNameState, CASE WHEN Combination IS NULL THEN CASE WHEN l.StateID IN (17,31,37,45) THEN s.State + '' - OTHER'' ELSE s.State END ELSE Combination END
order by s.FullNameState, Combination
'

--SELECT @QueryString
PRINT @QueryString
EXEC sp_executesql @QueryString	


END





GO
