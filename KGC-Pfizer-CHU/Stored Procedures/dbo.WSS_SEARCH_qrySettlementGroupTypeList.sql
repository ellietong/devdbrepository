SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qrySettlementGroupTypeList] 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT SettlementGroupTypeID, SettlementGroupType
	FROM tblSettlementGroupType
	UNION
	SELECT NULL, ''
	ORDER BY SettlementGroupType

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qrySettlementGroupTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qrySettlementGroupTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qrySettlementGroupTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qrySettlementGroupTypeList] TO [power_user]
GO
