SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_DEADLINE_qryDeadlineLocationList]
AS 
    SET NOCOUNT ON

SELECT EventLocationID, 
		UPPER(
		CASE WHEN LocationName IS NULL THEN '' ELSE LocationName END +
		CASE WHEN LocationName IS NOT NULL AND (Address1 IS NULL AND Address2 IS NULL AND City IS NULL AND State IS NULL AND Zip IS NULL) THEN ''
			 WHEN LocationName IS NULL AND (Address1 IS NOT NULL OR Address2 IS NOT NULL OR City IS NOT NULL OR State IS NOT NULL OR Zip IS NOT NULL) THEN '' 
			 ELSE ': ' END +
		+ COALESCE(Address1 + ' ', '') 
		+ COALESCE(Address2 + ' ', '') 
		+ COALESCE(City, '') 
		+ CASE WHEN City IS NULL OR (State IS NULL AND Zip IS NULL) THEN '' ELSE ', ' END 
		+ COALESCE(State + ' ', '')  + COALESCE(Zip, '')
		) AS EventLocation
FROM tblEventLocation el
	LEFT JOIN tblState s on el.StateID = s.StateID
	UNION
	SELECT NULL, ''
ORDER BY 
		UPPER(
				CASE WHEN LocationName IS NULL THEN '' ELSE LocationName END +
				CASE WHEN LocationName IS NOT NULL AND (Address1 IS NULL AND Address2 IS NULL AND City IS NULL AND State IS NULL AND Zip IS NULL) THEN ''
					 WHEN LocationName IS NULL AND (Address1 IS NOT NULL OR Address2 IS NOT NULL OR City IS NOT NULL OR State IS NOT NULL OR Zip IS NOT NULL) THEN '' 
					 ELSE ': ' END +
				+ COALESCE(Address1 + ' ', '') 
				+ COALESCE(Address2 + ' ', '') 
				+ COALESCE(City, '') 
				+ CASE WHEN City IS NULL OR (State IS NULL AND Zip IS NULL) THEN '' ELSE ', ' END 
				+ COALESCE(State + ' ', '')  + COALESCE(Zip, '')
				)

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryDeadlineLocationList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryDeadlineLocationList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryDeadlineLocationList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryDeadlineLocationList] TO [power_user]
GO
