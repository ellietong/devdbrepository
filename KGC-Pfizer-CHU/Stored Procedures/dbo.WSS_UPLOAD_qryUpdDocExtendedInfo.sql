SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo]
    (
      @DocumentID INT ,
      @DocumentTypeID INT ,
      @DocumentSourceID INT ,
      @Accept BIT ,
      @EmailAddress NVARCHAR(250) ,
      @ReleaseTypeID INT ,
      @ReleaseRequestDate DATETIME ,
      @ReleaseSentDate DATETIME ,
	  @ReleaseReceivedDate DATETIME,
      @ReleaseSignedDate DATETIME,
	  @MedicareSatisfaction	DATE,
	  @CPLDate DATE,
	  @CPLAmount MONEY,
	  @FDLDate DATE,
	  @FDLAmount MONEY,
	  @MedicaidLienAmount MONEY,
	  @PrivateLienAmount MONEY,
	  @MedicareAdvantageLienAmount MONEY,
	  @Comments NVARCHAR(4000),
	  @DocumentDate DATETIME,
	  @PaymentDate DATETIME,
	  @CheckNumber NVARCHAR(1000),
	  @PaymentAmount MONEY
    )
AS 
    SET NOCOUNT ON

    UPDATE  tblDocuments
    SET     ClaimantDocumentTypeID = @DocumentTypeID ,
            DocumentSourceID = @DocumentSourceID ,
			Comments = @Comments,
            DateModified = GETDATE() ,
            ModifiedBy = @EmailAddress ,
            Accepted = CASE WHEN COALESCE(Accepted, 0) = 1 THEN Accepted
                            WHEN @Accept = 0
                                 AND COALESCE(Accepted, 0) = 0 THEN NULL
                            ELSE 1
                       END ,
            AcceptedBy = CASE WHEN AcceptedBy IS NOT NULL THEN AcceptedBy
                              WHEN @Accept = 1 THEN @EmailAddress
                              ELSE NULL
                         END ,
            DateAccepted = CASE WHEN DateAccepted IS NOT NULL
                                THEN DateAccepted
                                WHEN @Accept = 1 THEN GETDATE()
                                ELSE NULL
                           END,
			DocumentDate = @DocumentDate
    WHERE   DocumentID = @DocumentID

    IF @Accept = 1
        AND (RIGHT(@EmailAddress, 11) = 'kcicllc.com' OR RIGHT(@EmailAddress, 8) = 'kcic.com') 
        BEGIN
            UPDATE  tblMultiClaimantDocument
            SET     QCBy = @EmailAddress ,
                    QCDate = GETDATE()
            WHERE   DocumentID = @DocumentID 	
        END			

    DECLARE @ClaimantPersonalInfoID INT

/**Declare cursor to attach documentation to each claimant the document is attached to**/
    DECLARE Cur_Claimants CURSOR Local Fast_Forward
    FOR
        SELECT  ClaimantpersonalinfoID
        FROM    tblMultiClaimantDocument
        WHERE   DocumentID = @DocumentID

    OPEN Cur_Claimants
    FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID

    WHILE @@FETCH_STATUS = 0 
        BEGIN
            DECLARE @ClaimLawsuitID INT
            SET @ClaimLawsuitID = ( SELECT  ClaimlawsuitID
                                    FROM    tblClaimantPersonalInfo cpi
                                            INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
                                            INNER JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
                                    WHERE   c.IsCurrentClaim = 1
                                            AND cl.IsPrimaryForClaim = 1
                                            AND cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
                                  )

            DECLARE @NewSettlePaymentDocumentationID INT

	--if there is no record for this claim in tblSettlePaymentDocumentation then create one
            IF ( SELECT COUNT(setpymtid)
                 FROM   tblSettlePaymentDocumentation
                 WHERE  ClaimLawsuitID = @ClaimLawsuitID
               ) = 0 
                BEGIN
                    INSERT  INTO tblSettlePaymentDocumentation
                            ( ClaimLawsuitID )
                            SELECT  @ClaimLawsuitID
                    SET @NewSettlePaymentDocumentationID = ( SELECT
                                                              MAX(SetPymtID)
                                                             FROM
                                                              tblSettlePaymentDocumentation
                                                           )
                END
            ELSE 
                BEGIN
                    SET @NewSettlePaymentDocumentationID = ( SELECT
                                                              setpymtID
                                                             FROM
                                                              tblSettlePaymentDocumentation
                                                             WHERE
                                                              ClaimLawsuitID = @ClaimLawsuitID
                                                           )
                END

	--Update check
	IF(SELECT @DocumentTypeID) = 29
	BEGIN
		--If there is no claimant settlement record for this ClaimLawsuitID in tblClaimantSettlement then create one
		DECLARE @NewClaimantSettlementID INT
		IF (select COUNT(ClaimantSettlementID) from tblClaimantSettlement where ClaimLawsuitID = @ClaimLawsuitID) = 0 
		BEGIN
		INSERT INTO tblClaimantSettlement (ClaimLawsuitID)
		SELECT @ClaimLawsuitID

		SET @NewClaimantSettlementID = (select MAX(ClaimantSettlementID) from tblClaimantSettlement)
		END
		ELSE
		BEGIN
		SET @NewClaimantSettlementID = (Select ClaimantSettlementID from tblClaimantSettlement where ClaimLawsuitID = @ClaimLawsuitID)
		END
			
		UPDATE tblClaimantSettlement 
		SET	--CheckNumber = COALESCE(@CheckNumber, CheckNumber),
			PaidDate = COALESCE(@PaymentDate, PaidDate),
			SPUsername = @EmailAddress
		WHERE ClaimantSettlementID = @NewClaimantSettlementID

		INSERT INTO tblCheck (SourceNumber, SourceDate, SourceAmount, MatterID, FundingTypeID, FundingSourceID)
		SELECT @CheckNumber, @PaymentDate, @PaymentAmount, 1, 2, 2 -- Pfizer, Check, Settlement

		DECLARE @CheckID bigint = (SELECT MAX(CheckID) FROM tblCheck)

		INSERT INTO tblPaymentSettlement (CheckID, ClaimantSettlementID, PaymentAmount)
		SELECT @CheckID, @NewClaimantSettlementID, @PaymentAmount
	END
	--Update release information
            UPDATE  tblSettlePaymentDocumentation
            SET     ReleaseRqstDate = COALESCE(@ReleaseRequestDate,
                                               ReleaseRqstDate) ,
                    ReleaseToPC = COALESCE(@ReleaseSentDate, ReleaseToPC) ,
                    ReleaseSignedDate = COALESCE(@ReleaseSignedDate,
                                                   ReleaseSignedDate), 
					ReleaseRcvdDate = COALESCE(@ReleaseReceivedDate, ReleaseRcvdDate),												                             
                    ReleaseTypeID = COALESCE(@ReleaseTypeID, ReleaseTypeID) ,
					CPLDate = COALESCE(@CPLDate, CPLDate),
					CPLAmount = COALESCE(@CPLAmount, CPLAmount),
					FDLDate = COALESCE(@FDLDate, FDLDate),
					FDLAmount = COALESCE(@FDLAmount, FDLAmount),
					MedicareSatisfaction = COALESCE(@MedicareSatisfaction, MedicareSatisfaction),
					MedicaidLienAmount = COALESCE(@MedicaidLienAmount, MedicaidLienAmount),
					PrivateLienAmount = COALESCE(@PrivateLienAmount, PrivateLienAmount),
					MedicareAdvantageLienAmount = COALESCE(@MedicareAdvantageLienAmount, MedicareAdvantageLienAmount),
                    SPUsername = @EmailAddress
            WHERE   SetPymtID = @NewSettlePaymentDocumentationID

            FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID
        END

    CLOSE Cur_Claimants
    DEALLOCATE Cur_Claimants

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [power_user]
GO
