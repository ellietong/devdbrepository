SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryInsClaimType] 
	-- Add the parameters for the stored procedure here
    (
      @ClaimID INT ,
      @ClaimTypeValueID INT,
	  @chkNotifyCounsel BIT,
	  @EmailAddress NVARCHAR(250)
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        INSERT  INTO tblClaimType
                ( ClaimID ,
				  ClaimTypeValueID,
				  SPUsername				  
                )
        VALUES  ( @ClaimID ,
		          @ClaimTypeValueID,
				  @EmailAddress
                )

        IF @chkNotifyCounsel = 1
		BEGIN
		UPDATE tblClaim
		SET NotifyCounsel = @chkNotifyCounsel
		From tblclaim c 
		--INNER JOIN tblClaimType ct on ct.claimtypeid = c.claimtypeid
		WHERE   c.ClaimID = (Select ClaimID from tblClaimType where ClaimTypeID = (SELECT MAX(ClaimTypeID) from tblClaimType))
		END
    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsClaimType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsClaimType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsClaimType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsClaimType] TO [power_user]
GO
