SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_DEADLINE_qryGetClaimLawsuitsByDeadlineID] ( @DeadlineID BIGINT )
AS 
    SET NOCOUNT ON

    SELECT  CONVERT(NVARCHAR(20), clcd.ClaimLawsuitID) AS ClaimLawsuitID ,
	CONVERT(NVARCHAR(20), cpi.ClaimantPersonalInfoID) AS ClaimantID ,
CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN ''
                                       ELSE FirstName + ' '
                                  END + CASE WHEN MiddleName IS NULL THEN ' '
                                             ELSE MiddleName + ' '
                                        END AS ClaimantName 
										--,
          --  CONVERT(NVARCHAR(20), ClaimLawsuitCaseDeadlineID) AS ClaimLawsuitCaseDeadlineID
    FROM    tblCaseDeadline cd
		INNER JOIN tblClaimLawsuitCaseDeadline  clcd ON cd.CaseDeadlineID = clcd.CaseDeadlineID
		INNER JOIN tblClaimLawsuit cl ON clcd.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
    WHERE   clcd.CaseDeadlineID = @DeadlineID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryGetClaimLawsuitsByDeadlineID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryGetClaimLawsuitsByDeadlineID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryGetClaimLawsuitsByDeadlineID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryGetClaimLawsuitsByDeadlineID] TO [power_user]
GO
