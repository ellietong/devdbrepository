SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryInsertSavedParameter_Claim]
    (
      @Type NVARCHAR(MAX) ,
      @Value NVARCHAR(MAX) ,
      @SavedSearchID BIGINT
    )
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	IF ( @Type = 'DParam' ) 
	BEGIN
	INSERT  INTO tblSavedDisease
	SELECT  CONVERT(INT, @Value) ,
			@SavedSearchID
	
	END
	ELSE 
	IF ( @Type = 'JParam' ) 
	BEGIN
	INSERT  INTO tblSavedJurisdiction
	SELECT  CONVERT(INT, @Value) ,
	@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'AParam' ) 
	BEGIN
	INSERT  INTO tblSavedAllegation
	SELECT  CONVERT(INT, @Value) ,
	@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'StParam' ) 
	BEGIN
	INSERT  INTO tblSavedState
	SELECT  CONVERT(INT, @Value) ,
	@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'PCParam' ) 
	BEGIN
	INSERT  INTO tblSavedPlaintiffCOunsel
	SELECT  CONVERT(BIGINT, @Value) ,
	@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'LSParam' ) 
	BEGIN
	INSERT  INTO tblSavedLawsuitStatus
	SELECT  CONVERT(INT, @Value) ,
	@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'MParam' ) 
	BEGIN
	INSERT  INTO tblSavedMatter
	SELECT  CONVERT(INT, @Value) ,
	@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'ATDRParam' ) 
	BEGIN
	INSERT  INTO tblSavedActiveTrialDateRange
	SELECT  @Value ,
	@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'MDLParam' ) 
	BEGIN
	INSERT  INTO tblSavedIsMDL
	SELECT
	CONVERT(BIT, @Value) ,
	@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'PDRParam' ) 
	BEGIN
	INSERT  INTO tblSavedProcessedDateRange
	SELECT
	@Value ,
	@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'SDRParam' ) 
	BEGIN
	INSERT
	INTO tblSavedStatusDateRange
	SELECT
	@Value ,
	@SavedSearchID
	END		

	ELSE 
	IF ( @Type = 'CSQParam' ) 
	BEGIN
	INSERT  INTO tblSavedClaimSequence
		SELECT  CONVERT(BIGINT, @Value) ,
				@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'SGTParam' ) 
	BEGIN
	INSERT  INTO tblSavedSettlementGroupType
			SELECT  CONVERT(BIGINT, @Value) ,
					@SavedSearchID
	END
	ELSE 
	IF ( @Type = 'CTParam' ) 
	BEGIN
		INSERT  INTO tblSavedClaimType
				SELECT  CONVERT(BIGINT, @Value) ,
						@SavedSearchID
	END
	ELSE
	IF(@Type = 'SSNParam')
	BEGIN
		INSERT INTO tblSavedSSN 
		SELECT @Value, @SavedSearchID
	
	END
	ELSE IF (@Type = 'CNParam')
	BEGIN
		INSERT INTO tblSavedClaimantName 
		SELECT @Value, @SavedSearchID
	END
	ELSE IF (@Type = 'DNParam')
	BEGIN
		INSERT INTO tblSavedDocketNumber 
		SELECT @Value, @SavedSearchID
	END
	ELSE 
	IF (@Type = 'CIDParam')
	BEGIN
		INSERT INTO tblSavedClaimantIDs 
		SELECT Convert(bigint, @Value), @SavedSearchID
	END
	ELSE 
	IF (@Type = 'IPIDParam')
	BEGIN
		INSERT INTO tblSavedInjuredPartyIDs 
		SELECT Convert(bigint, @Value), @SavedSearchID
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryInsertSavedParameter_Claim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryInsertSavedParameter_Claim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryInsertSavedParameter_Claim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryInsertSavedParameter_Claim] TO [power_user]
GO
