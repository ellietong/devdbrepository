SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SEARCH_qryDefenseCounselList]
AS 
    SET NOCOUNT ON

    SELECT  dca.DefenseCounselAddressID ,
            DefenseCounsel 
    FROM    tblDefenseCounselAddress dca
			INNER JOIN tblLawsuit l ON dca.DefenseCounselAddressID = l.DefenseCounselAddressID
    WHERE   DefenseCounsel IS NOT NULL
            AND DefenseCounsel <> ''
    UNION
    SELECT  NULL ,
            NULL
    ORDER BY DefenseCounsel,
            dca.DefenseCounselAddressID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDefenseCounselList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDefenseCounselList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDefenseCounselList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDefenseCounselList] TO [power_user]
GO
