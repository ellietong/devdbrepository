SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAccountsReceivableActivity] (@BillDate datetime, @paymentdate datetime, @paymentdate2 datetime)
As
Set NoCount On
 
-- Declare @Billdate Datetime
-- Set @Billdate = '8/1/2011'

--Declare @paymentdate Datetime
-- Set @paymentdate = '9/1/2011'

-- Declare @paymentdate2 Datetime
-- Set @paymentdate2 = '10/1/2011'

--Find Date of Last Payment Received
Declare @LastPayment Table (CarrierID int, LatestPaymentDate datetime)
insert into @LastPayment
select carrierid, MAX(paymentdate)
from tblPayment p
left join tblLedger l on l.LedgerID=p.LedgerID
where PaymentDate < @paymentdate2 and CarrierID not in (9)
group by CarrierID


--As of DATE Accounts Receivable
Declare @Ledger1 Table ( CarrierID int, Debit money)
Insert into @Ledger1
Select CarrierID, sum(Debit)
From tblLedger
Where LedgerDate <@Billdate
group by  CarrierID

Declare @Payment1 Table (carrierid int, Payment money)
Insert into @Payment1
Select carrierid, Sum(coalesce(Payment,0))
From tblPayment p
inner join tblLedger l on l.LedgerID=p.LedgerID
Where PaymentDate < @paymentdate
Group By carrierid

Declare @AR table (CarrierID int, AccountReceivable money)
Insert Into @AR
Select l.CarrierID, Sum(Coalesce(L.Debit,0)-coalesce(P.payment,0)) AS AccountReceivable
From @Ledger1 L
Left Join @Payment1 P on L.CarrierID=P.carrierid
Group By L.CarrierID
HAVING Sum(Coalesce(L.Debit,0)-coalesce(P.payment,0)) <> 0



--New Activity Accounts Receivable
Declare @Ledger2 Table (CarrierID int, NewBillings money)
Insert into @Ledger2
Select CarrierID,  Sum(coalesce(Debit,0))
From tblLedger
Where LedgerDate >= @Billdate and LedgerDate<@paymentdate
Group BY  CarrierID

Declare @Payment2 Table (CarrierID int, NewPayments money)
Insert into @Payment2
Select CarrierID, Sum(coalesce(Payment,0))
From tblPayment P
inner join tblLedger l on l.LedgerID=p.LedgerID
Where PaymentDate >= @paymentdate and PaymentDate< @paymentdate2
Group By CarrierID


Declare @NewAR table (CarrierID int, NewBillings money, NewPayments money)
Insert Into @NewAR
Select L.CarrierID, SUM(coalesce(NewBillings,0)), SUM(coalesce(NewPayments,0))
From @Ledger2 L
Left Join @Payment2 P on L.CarrierID=P.CarrierID
Group By L.CarrierID



--Report Data

Declare @Final table (CarrierName nvarchar(2000), DatedAccountsReceivable money, NewBilledAmounts money, NewPayments money,  LatestPayment datetime)
Insert Into @Final

Select CarrierName, AccountReceivable as DatedAccountsReceivable, 
            NewBillings as NewBilledAmounts,NewPayments as NewPayments, LatestPaymentDate as LatestPayment
From @AR a
left join @NewAR  ar on a.CarrierID=ar.CarrierID
left join @LastPayment l on l.CarrierID=a.CarrierID
inner join tblCarrier c on c.CarrierID=a.CarrierID 
Where c.CarrierID not in (100000)
GROUP BY CarrierName, AccountReceivable, 
            NewBillings,NewPayments, LatestPaymentDate
    Having (SUM(NewPayments) <>0 OR SUM(NewBillings) <>0 or AccountReceivable<>0)     

Declare @DatedAccountsReceivableProducer money
select @DatedAccountsReceivableProducer =coalesce(AccountReceivable,0) from @AR a where  a.CarrierID=100000 

Declare @NewBilledAmountsProducer money
select @NewBilledAmountsProducer =coalesce(NewBillings,0) from  @NewAR  ar where  ar.CarrierID=100000 

Declare @NewPaymentsProducer money
select @NewPaymentsProducer = coalesce(NewPayments,0) from  @NewAR  ar where  ar.CarrierID=100000 

Declare @LatestProducer datetime
select @LatestProducer = LatestPaymentDate from  @LastPayment  ar where  ar.CarrierID=100000 

Select @paymentdate as StartBillDate, @paymentdate2 as EndBilldate, CarrierName, Sum(coalesce(DatedAccountsReceivable,0)) as DatedAccountsReceivable,
      Sum(Coalesce(NewBilledAmounts,0)) as NewBilledAmounts, Sum(Coalesce(NewPayments,0)) as NewPayments, LatestPayment,
         DATEADD(DAY,30,@BillDate) as CurrentDate, coalesce(@DatedAccountsReceivableProducer,0) as DatedAccountsReceivableProducer, coalesce(@NewBilledAmountsProducer,0) as NewBilledAmountsProducer, coalesce(@NewPaymentsProducer,0) as NewPaymentsProducer, @LatestProducer as LatestProducer
From @Final
Group By CarrierName, LatestPayment
Having Sum(coalesce(DatedAccountsReceivable,0)) <> 0 OR Sum(Coalesce(NewBilledAmounts,0)) > 0 OR Sum(Coalesce(NewPayments,0)) > 0
Order By CarrierName

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAccountsReceivableActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAccountsReceivableActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAccountsReceivableActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAccountsReceivableActivity] TO [power_user]
GO
