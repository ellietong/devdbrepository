SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_DISMISSAL_ddlClaimLawsuitStatus]
AS

SELECT ClaimLawsuitStatusID, ClaimLawsuitStatus FROM tblClaimLawsuitStatus
UNION ALL
SELECT 0, ''
ORDER BY ClaimLawsuitStatusID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_ddlClaimLawsuitStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_ddlClaimLawsuitStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_ddlClaimLawsuitStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_ddlClaimLawsuitStatus] TO [power_user]
GO
