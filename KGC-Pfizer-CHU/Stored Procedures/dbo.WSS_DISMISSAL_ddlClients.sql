SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_DISMISSAL_ddlClients]
As
Set NoCount On

SELECT DISTINCT CaseID, CaseDisplayName as Client
FROM [KGA-ClaimsAdmin].[dbo].tblCase
WHERE CaseDisplayName IS NOT NULL AND CaseID <> 30
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_ddlClients] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_ddlClients] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_ddlClients] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_ddlClients] TO [power_user]
GO
