SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_DEADLINE_qryInsCaseDeadline] 
(
@TrialGroupID	INT,
@DeadlineTypeID INT, 	
@EventTypeID INT,
@DetailedEventType NVARCHAR(MAX),
@Emailaddress NVARCHAR(250) ,
@DeadlineDate DATE ,
@DeadlineTime TIME,
@TimeZoneID INT,
@TimePeriodID INT,
@EventLocationID INT = NULL,
@EventLocation NVARCHAR(MAX) = NULL,
@Comments NVARCHAR(MAX),	
@Deponent NVARCHAR(4000),
@Attendee NVARCHAR(4000),
@AttendanceModeID INT
)

AS

DECLARE @DetailedEventTypeID INT

IF (@DetailedEventType IS NOT NULL)
BEGIN
	IF (SELECT COUNT(DetailedEventTypeID)
		FROM tblDetailedEventType
		WHERE DetailedEventType LIKE '%'+ RTRIM(LTRIM(@DetailedEventType)) +'%'
		) = 0
		BEGIN
			INSERT INTO tblDetailedEventType (DetailedEventType, DeadlineTypeID)
			VALUES (@DetailedEventType, @DeadlineTypeID)
			SET @DetailedEventTypeID = (SELECT MAX(DetailedEventTypeID) FROM tblDetailedEventType)
		END
	ELSE 
		BEGIN
			SET @DetailedEventTypeID = (SELECT TOP 1 DetailedEventTypeID
											FROM tblDetailedEventType
											WHERE DetailedEventType LIKE '%'+ RTRIM(LTRIM(@DetailedEventType)) +'%')
		END
END


DECLARE @MaxCaseDeadlineID int
SET @MaxCaseDeadlineID = (SELECT IDENT_CURRENT('tblCaseDeadline')+IDENT_INCR('tblCaseDeadline'))

BEGIN
INSERT INTO tblCaseDeadline
						 (
						  TrialGroupID,
						  EventTypeID,	
						  DetailedEventTypeID,
						  DeadlineDate,
						  DeadlineTime,
						  TimeZoneID,
						  TimePeriodID,
						  EventLocationID,
						  Deponent,
						  Attendees,
						  AttendanceModeID,
						  CreatedBy,
						  DateCreated,
						  Comments,
						  DeadlineStatusID
						  )

	VALUES (
			@TrialGroupID,
			@EventTypeID,
			@DetailedEventTypeID,
			@DeadlineDate, 
			@DeadlineTime,
			@TimeZoneID,
			@TimePeriodID,
			@EventLocationID,
			@Deponent,
			@Attendee,
			@AttendanceModeID,
			@Emailaddress,
			GETDATE(),
			@Comments,
			1
			)		

SELECT @MaxCaseDeadlineID
END			

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryInsCaseDeadline] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryInsCaseDeadline] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryInsCaseDeadline] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryInsCaseDeadline] TO [power_user]
GO
