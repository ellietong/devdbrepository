SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryGetClaimLawsuitID]
	-- Add the parameters for the stored procedure here
@ClaimantPersonalInfoID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT MAX(ClaimLawsuitID) AS ClaimLawsuitID
	FROM tblClaimLawsuit cl
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	WHERE COALESCE(isCurrentClaim, 0) = 1 
		AND COALESCE(isPrimaryForClaim, 0) = 1 
		AND ClaimantPersonalInfoID = @ClaimantPersonalInfoID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryGetClaimLawsuitID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryGetClaimLawsuitID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryGetClaimLawsuitID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryGetClaimLawsuitID] TO [power_user]
GO
