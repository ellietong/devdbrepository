SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByMonth] 
	-- Add the parameters for the stored procedure here
	(@StartDate datetime, @EndDate datetime, @ClaimTypeValueID int, @EmailAddress nvarchar(250))
AS
BEGIN

--DECLARE @StartDate datetime
--DECLARE @EndDate datetime
--DECLARE @ClaimTypeValueID int 
--DECLARE @EmailAddress nvarchar(250)

--SET @StartDate = '1/1/2013'
--SET @EndDate = '10/15/2013'
--SET @ClaimTypeValueID =NULL 
--SET @EmailAddress ='scottc@kcic.com'


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)

DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )

DECLARE @QueryString NVARCHAR(MAX)
DECLARE @WhereString NVARCHAR(MAX) =  ' WHERE ((c.MatterID =1 and a.servicedate between ''' + CONVERT(NVARCHAR, @StartDate) + ''' and ''' + CONVERT(NVARCHAR, @EndDate) + ''' and DocketNumber <>''UNFILED'' ) 
											OR (cl.ClaimLawsuitID  in (3617371,3930921,4104422,4150163,3727290,3746595,3740572,3781433,3906063,3920744,3937709,4087312,4978702) 
												AND a.ServiceDate between ''' + CONVERT(NVARCHAR, @StartDate) + ''' and ''' + CONVERT(NVARCHAR, @EndDate) + '''))
												AND c.ClaimSequenceID in (4,5)
												AND (COALESCE(IsPrimaryClaimType, 0) = 1 OR ct.ClaimTypeID IS NULL) '
DECLARE @JoinString NVARCHAR(MAX)

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    IF ( ( RIGHT(@emailaddress, 11) = 'kcicllc.com' )
		   OR ( RIGHT(@emailaddress, 8) = 'kcic.com' )	
           OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        BEGIN
            SET @JoinString = ''
            --SET @WhereString = @WhereString + ' AND cl.IsPrimaryForClaim = 1'
        END
    ELSE 
        BEGIN
            SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
            SET @WhereString = @WhereString
                + ' AND (dj.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ' OR l.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ')'
        END

--DECLARE @MaxClaimTypeValueID int
--SET @MaxClaimTypeValueID = (SELECT MAX(ClaimTypeValueID) FROM tblClaimTypeValue)
IF @ClaimTypeValueID IS NOT NULL
BEGIN
	IF @ClaimTypeValueID = 7
		BEGIN
			SET @WhereString =  @WhereString + ' AND (ct.ClaimTypeValueID IS NULL OR ct.ClaimTypeValueID = ' + CONVERT(NVARCHAR, @ClaimTypeValueID) + ')'
		END
	ELSE 
		BEGIN
			SET @WhereString =  @WhereString + ' AND ct.ClaimTypeValueID = ' + CONVERT(NVARCHAR, @ClaimTypeValueID)
		END
END

SET @QueryString = '
	SELECT DATENAME("mm", coalesce(a.ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) + '' '' + CONVERT(nvarchar, YEAR(coalesce(a.ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))) AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS MonthCount, MONTH(coalesce(a.ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) As MonthID, 
	YEAR(coalesce(a.ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) AS [Year]
	from tblClaim c 
	INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
	LEFT JOIN (SELECT * from tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	LEFT JOIN tblState s on l.StateID=s.StateID
	LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
	INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	'
	+ @JoinString +
	+ @WhereString +  
	'		
	GROUP BY DATENAME("mm", coalesce(a.ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) + '' '' +  CONVERT(nvarchar, YEAR(coalesce(a.ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))) , MONTH(coalesce(a.ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)),YEAR(coalesce(a.ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))
	ORDER BY YEAR(coalesce(a.ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)), MONTH(coalesce(a.ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))
'
PRINT @QueryString
EXEC sp_executesql @QueryString	


END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [power_user]
GO
