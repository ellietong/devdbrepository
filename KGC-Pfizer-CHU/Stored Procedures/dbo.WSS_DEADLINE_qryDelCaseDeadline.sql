SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DEADLINE_qryDelCaseDeadline] 
	-- Add the parameters for the stored procedure here
	  @CaseDeadlineID BIGINT,
      @Emailaddress NVARCHAR(250) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
IF @CaseDeadlineID IS NOT NULL
BEGIN

DELETE FROM tblClaimLawsuitCaseDeadline
WHERE CaseDeadlineID = @CaseDeadlineID

DELETE FROM tblCaseDeadline
WHERE CaseDeadlineID = @CaseDeadlineID

END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryDelCaseDeadline] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryDelCaseDeadline] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryDelCaseDeadline] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryDelCaseDeadline] TO [power_user]
GO
