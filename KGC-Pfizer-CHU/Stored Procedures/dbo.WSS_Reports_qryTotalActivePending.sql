SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_Reports_qryTotalActivePending] 
	-- Add the parameters for the stored procedure here
	(@ClaimTypeValueID int = NULL, @EmailAddress nvarchar(250))	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--declare @enddate datetime
--Make Pending Reports as of today

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)

DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )

DECLARE @QueryString NVARCHAR(MAX)
DECLARE @WhereString NVARCHAR(MAX) =  ' WHERE (IsPrimaryStatus = 1 
												AND cls.ClaimLawsuitStatusGroupID = 3)  
												AND c.ClaimSequenceID in (4,5)
												AND (COALESCE(IsPrimaryClaimType, 0) = 1 OR ct.ClaimTypeID IS NULL) '
DECLARE @JoinString NVARCHAR(MAX)

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    IF ( ( RIGHT(@emailaddress, 11) = 'kcicllc.com' )
		   OR ( RIGHT(@emailaddress, 8) = 'kcic.com' )	
           OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        BEGIN
            SET @JoinString = ''
            --SET @WhereString = @WhereString + ' AND cl.IsPrimaryForClaim = 1'
        END
    ELSE 
        BEGIN
            SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
            SET @WhereString = @WhereString
                + ' AND (dj.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ' OR l.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ')'
        END

IF @ClaimTypeValueID IS NOT NULL
BEGIN
	IF @ClaimTypeValueID = 7
		BEGIN
			SET @WhereString =  @WhereString + ' AND (ct.ClaimTypeValueID IS NULL OR ct.ClaimTypeValueID = ' + CONVERT(NVARCHAR, @ClaimTypeValueID) + ')'
		END
	ELSE 
		BEGIN
			SET @WhereString =  @WhereString + ' AND ct.ClaimTypeValueID = ' + CONVERT(NVARCHAR, @ClaimTypeValueID)
		END
END

SET @QueryString = '
	select COUNT(distinct(cl.claimlawsuitid)) As [Count]
	from tblClaim c 
	inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
	inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	inner join tblClaimLawsuitStatus cls on clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
'
+ @JoinString +
+ @WhereString +  
'
'

--SELECT @QueryString
PRINT @QueryString
EXEC sp_executesql @QueryString	


END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Reports_qryTotalActivePending] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Reports_qryTotalActivePending] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_Reports_qryTotalActivePending] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_Reports_qryTotalActivePending] TO [power_user]
GO
