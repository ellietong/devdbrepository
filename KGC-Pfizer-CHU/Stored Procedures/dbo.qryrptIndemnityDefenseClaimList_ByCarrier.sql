SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] (@OccurrenceGroupID int, @CarrierID int)
As
SET NOCOUNT ON

/*Marley bills Settlements Paid (and not pre-billed) and Defense Paid in the current month.
10/5/11 - Per conversation with G. McKenna, defense expenses will be billed/reported on using the following methodology:
 - Defense for SETTLED claims will use exposure/DX dates in system for known MARLEY products.
 - Red Jacket & Marley Pump - use 1/1/67 default date (if not settled)
 - Hydromatick - use 1/1/70 default date (if not settled)
 - Weil-Mclain - use composite dates (if not settled)
*/

--Declare @OccurrenceGroupID int
--Declare @CarrierID int
--Set @OccurrenceGroupID = 5
--SET @CarrierID = 20

--Pull allocation amounts for requested Carrier
DECLARE @CarrierAllocation Table (OccurrenceID int, AllocatedAmount float, CarrierName Nvarchar (100))
Insert into @CarrierAllocation
SELECT OC.OccurrenceID, sum(a.Allocation) as AllocatedAmount, CarrierName
from tblAllocationResults A
Join tblOccurrence OC on OC.OccurrenceID = A.OccurrenceID
Join tblPolicy P on p.PolicyID = a.PolicyID
Join tblCarrier C on c.CarrierID = p.CarrierID
where oc.OccurrenceGroupID = @OccurrenceGroupID AND p.CarrierID = @CarrierID
Group by OC.OccurrenceID, CarrierName


--Select claimant information associated with expenses to be billed
select CPI.ClaimantPersonalInfoID, Lastname+', '+FirstName as ClaimantName, coalesce('XXX-XX-'+ Right([SocialSecurityNumber],4),'XXX-XX-') as SocialSecurityNumber, State,
	left(HighLevelDisease,1) as Disease, FileDate, SettlementDate, 
	Coalesce(TriggerStart1, TriggerStart2) as  EarliestExposureDate,
	Coalesce(triggerEnd1, TriggerEnd2) as DiagnosisDate, 
	CASE when OccurrenceTypeID = 1 THEN 'I' Else 'E'END  as AmountType,o.Cost1+o.Cost3+o.Cost2+o.Cost4 as Amount,
	case when inclaim = 1 AND Triggerstart1 = TriggerStart1 THEN ''
WHen Inclaim = 1 AND Triggerstart1 is null THEN '+'
When INclaim = 0 Then '*'
End as ExpNotation,
Case when TriggerEnd1 is not null Then '' Else '*' END as DxNotation,
CASE When o.Cost2 <>0 or o.Cost4 <>0 Then 'A' ELSE 'B' END as ADJ, convert(money,CA.AllocatedAmount) as AllocAmount, CarrierName
from tblOccurrence O
join tblOccurrenceGroup OG  on O.OccurrenceGroupID = OG.OccurrenceGroupID
left Join tblClaimantPersonalInfo CPI on CPI.ClaimantPersonalInfoID = o.ClaimID
inner join (select * from tblClaim where iscurrentclaim=1) c on c.ClaimantPersonalInfoID= o.ClaimID 
Left Join (select * from tblClaimDisease where PrimaryDisease = 1) cd on cd.ClaimID = c.ClaimID
Left Join tblDisease D on cd.DiseaseID=D.DiseaseID
Left Join tblHighLevelDisease HLD on D.HighLevelDiseaseID=HLD.HighLevelDiseaseID
Left Join (select * from tblClaimLawsuit where IsPrimaryForClaim =1) cl on cl.ClaimID=c.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID 
Left Join tblState S on L.StateID=S.StateID
left Join tblClaimantSettlement CS on CL.ClaimLawsuitID=CS.CLaimLawsuitID
left join tblAllegation A on a.ClaimLawsuitID = CL.ClaimLawsuitID
left join tblDefendantCompany dc on dc.DefendantCompanyID = a.DefendantCompanyID
left join @CarrierAllocation CA on ca.OccurrenceID = o.OccurrenceID
Where O.OccurrenceGroupID = @OccurrenceGroupID and cl.IsPrimaryForClaim = 1 and c.IsCurrentClaim = 1 and o.matterID = c.MatterID and o.OccurrenceTypeID = 1
group by CPI.ClaimantPersonalInfoID, Lastname+', '+FirstName, coalesce('XXX-XX-'+ Right([SocialSecurityNumber],4),'XXX-XX-') , 
	State,CASE when OccurrenceTypeID = 1 THEN 'I' Else 'E'END ,Coalesce(TriggerStart1, TriggerStart2),
	left(HighLevelDisease,1), FileDate, SettlementDate, Coalesce(triggerEnd1, TriggerEnd2) ,o.Cost1+o.Cost3+o.Cost2+o.Cost4,
	Case when TriggerEnd1 is not null Then '' Else '*' END,case when inclaim = 1 AND Triggerstart1 = TriggerStart1 THEN ''
WHen Inclaim = 1 AND Triggerstart1 is null THEN '+'
When INclaim = 0 Then '*' End  , CASE When o.Cost2 <>0 or o.Cost4 <>0 Then 'A' ELSE 'B' END,CA.AllocatedAmount, CarrierName
	


RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [power_user]
GO
