SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DEADLINE_qryRemoveCaseDeadline] 
	-- Add the parameters for the stored procedure here
	  @CaseDeadlineID bigint,
	  @DeadlineTypeID int = NULL, 
	  @ClaimLawsuitID bigint,
	  @TrialGroupID int = NULL,
	  @ClaimLawsuitCount int = NULL


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF @CaseDeadlineID IS NOT NULL
	BEGIN
		IF @ClaimLawsuitID IS NOT NULL AND @TrialGroupID IS NULL
		BEGIN
			
			DELETE FROM tblClaimLawsuitCaseDeadline WHERE CaseDeadlineID = @CaseDeadlineID AND ClaimLawsuitID = @ClaimLawsuitID
			
			IF (SELECT COUNT(ClaimLawsuitCaseDeadlineID) FROM tblClaimLawsuitCaseDeadline WHERE CaseDeadlineID = @CaseDeadlineID) = 0
			BEGIN
				DELETE FROM tblCaseDeadline WHERE CaseDeadlineID = @CaseDeadlineID
			END
		END
		ELSE IF @ClaimLawsuitID IS NOT NULL AND @TrialGroupID IS NOT NULL
			BEGIN
				UPDATE tblClaimLawsuit SET TrialGroupID = NULL WHERE ClaimLawsuitID = @ClaimLawsuitID

				IF (SELECT COUNT(ClaimLawsuitID) FROM tblClaimLawsuit WHERE TrialGroupID = @TrialGroupID) = 0
				BEGIN
					DELETE FROM tblCaseDeadline WHERE TrialGroupID = @TrialGroupID
				END
			END
		--IF @CaseDeadlineID IS NOT NULL
		--BEGIN

		--DELETE FROM tblClaimLawsuitCaseDeadline
		--WHERE CaseDeadlineID = @CaseDeadlineID

		--DELETE FROM tblCaseDeadline
		--WHERE CaseDeadlineID = @CaseDeadlineID

		--END

	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryRemoveCaseDeadline] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryRemoveCaseDeadline] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryRemoveCaseDeadline] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryRemoveCaseDeadline] TO [power_user]
GO
