SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_updDocumentByNCC]
    (
      @DetailDocumentTypeID INT ,					--aka ClaimantDocumentTypeID
	--@ClaimantPersonalInfoID bigint = null,	
      @Comments NVARCHAR(4000) = NULL ,			--comments are optional		
      @DocumentURL NVARCHAR(4000) ,
      @DocumentTitle NVARCHAR(4000) ,
      @MatterID INT = NULL ,					--this is the guid assigned by SharePoint
      @FirmID INT ,
	  @DocumentDate DATETIME,
	
	--These are null unless the document is an Invoice document
      @InvoiceNumber NVARCHAR(50) = NULL ,
      @Period NVARCHAR(50) = NULL ,
      @DatePayable DATETIME = NULL ,
      @FeeAmount MONEY = NULL ,
      @DisbursementAmount MONEY = NULL ,
      @Adjustment MONEY = NULL ,
      @AdjustmentNotes NVARCHAR(4000) = NULL ,
      @StatusID INT = NULL ,
      @ModifiedBy NVARCHAR(250) ,
      @DateModified DATETIME ,
	--@Approved bit = null,
      @ApprovedBy NVARCHAR(50) = NULL ,
      @DateApproved DATETIME = NULL ,
      @DocID INT = NULL ,
      @DocumentSourceID INT ,
      @PrivateComments NVARCHAR(4000) = NULL ,
      @VendorFirmID INT = NULL
    )
AS 
    BEGIN
        UPDATE  tblDocuments
        SET     ClaimantDocumentTypeID = @DetailDocumentTypeID ,
                Comments = @Comments ,
                DocumentURL = @DocumentURL ,
                DocumentTitle = @DocumentTitle ,
                ModifiedBy = @ModifiedBy ,
                DateModified = @DateModified ,
                DocumentSourceID = @DocumentSourceID ,
                MatterID = @MatterID,
				DocumentDate = @DocumentDate
        WHERE   DocumentID = @DocID

--1/31/12 EMS added query to update tblDefenseInvoice
        UPDATE  tblDefenseInvoice
        SET     InvoiceNumber = @InvoiceNumber ,
                Period = @Period ,
                DatePayable = CASE WHEN @StatusID IN ( 1, 2 )
                                        AND VendorFirmID IS NOT NULL
                                        AND @VendorFirmID IS NOT NULL
                                   THEN GETDATE() + 1
                                   ELSE @DatePayable
                              END ,
                InvoiceNotes = @AdjustmentNotes ,
                StatusID = @StatusID ,
                DateApproved = @DateApproved ,
                ApprovedBy = @ApprovedBy
        WHERE   DocumentID = @DocID

        SELECT  @DocID
    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [power_user]
GO
