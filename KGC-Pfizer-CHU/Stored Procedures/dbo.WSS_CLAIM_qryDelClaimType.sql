SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryDelClaimType] 
	-- Add the parameters for the stored procedure here
    (
      @ClaimTypeID BIGINT ,
	  @EmailAddress NVARCHAR(250) 
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        UPDATE  tblClaimType
        SET     SPUsername = @Emailaddress
        WHERE   ClaimTypeID = @ClaimTypeID


        DELETE  FROM tblClaimType
        WHERE   ClaimTypeID = @ClaimTypeID
    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDelClaimType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDelClaimType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDelClaimType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDelClaimType] TO [power_user]
GO
