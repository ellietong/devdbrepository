SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SEARCH_qryDocumentTypesComplaint](@EmailAddress nvarchar(max)=null)
AS 
    SET NOCOUNT ON
    
    
    
    SELECT  ClaimantDocumentTypeID ,
            ClaimantDocumentType + ' (' + UPPER(ClaimantDocumentGroup) +')' AS ClaimantDocumentType
    FROM    tblClaimantDocumentType cdt
		INNER JOIN tblClaimantDocumentGroup  cdg on cdt.ClaimantDocumentGroupID =  cdg.ClaimantDocumentGroupID
    WHERE   cdt.ClaimantDocumentGroupID NOT IN (2, 22)
    UNION
    SELECT  NULL ,
            '<< All Document Types >>'
    ORDER BY ClaimantDocumentType ASC
        
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentTypesComplaint] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentTypesComplaint] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentTypesComplaint] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentTypesComplaint] TO [power_user]
GO
