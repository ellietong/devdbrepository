SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryUpdClaimantProduct]
    (
      @ClaimantProductID INT ,
      @ProductID NVARCHAR(150) ,
      @StartDate DATETIME ,
      @EndDate DATETIME ,
      @Notes NVARCHAR(MAX) ,
      @Username NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON
	
    UPDATE  tblClaimantProduct
    SET     ProductID = @ProductID ,
            StartDate = @StartDate ,
            EndDate = @EndDate ,
            Notes = @Notes ,
            SPUsername = @Username
    WHERE   ClaimantProductID = @ClaimantProductID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryUpdClaimantProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryUpdClaimantProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryUpdClaimantProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryUpdClaimantProduct] TO [power_user]
GO
