SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_EMAIL_qryInternalEmailNewSettlements]
AS 
    BEGIN
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/


--Declare and set email inputs.
        DECLARE @Subject NVARCHAR(100)
        DECLARE @LogoHeader NVARCHAR(2000)

        DECLARE @Today4PM DATETIME
        DECLARE @Yesterday4PM DATETIME
        SET @Yesterday4PM = CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(DATE, GETDATE()
            - 1)) + ' 16:00:00.000')
        SET @Today4PM = CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(DATE, GETDATE()))
            + ' 16:00:00.000')



        SET @Subject = 'Pfizer Claims: New Settlements '
            + CONVERT(VARCHAR, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


        SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
        DECLARE @NewSettlements TABLE
            (
              DocID INT ,
              docTitle NVARCHAR(500) ,
              FirmID INT ,
              lName NVARCHAR(50) ,
              fName NVARCHAR(50) ,
              ClaimantPersonalInfoID BIGINT ,
              ServiceDate DATE ,
              [State] NVARCHAR(5) ,
              PlaintiffCounsel NVARCHAR(500) ,
              DefenseCounsel NVARCHAR(400) ,
              LawsuitID INT
            )
        INSERT  INTO @NewSettlements
                SELECT  d.DocumentID ,
                        '<a href="https://www.kcicextranet.com/clients/Pfizer/cp/Pages/DocumentProfile.aspx?ItemId='
                        + COALESCE(CONVERT(NVARCHAR(50), d.DocumentID), '')
                        + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle ,
                        DefenseCounselID ,
                        cpi.LastName ,
                        cpi.FirstName ,
                        cpi.ClaimantPersonalInfoID ,
                        MAX(ServiceDate) ,
                        s.State ,
                        PlaintiffCounsel ,
                        DefenseCounsel ,
                        cl.LawsuitID
                FROM    tblDocuments d
                        INNER JOIN tblMultiClaimantDocument mcd		ON d.DocumentID = mcd.DocumentID
                        INNER JOIN tblClaimantPersonalInfo cpi		ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
                        LEFT JOIN tblclaim c						ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
                        LEFT JOIN tblclaimlawsuit cl				ON cl.claimid = c.claimid
                        LEFT JOIN tbllawsuit l						ON l.lawsuitid = cl.lawsuitid
                        LEFT JOIN tblAllegation a					ON a.ClaimLawsuitID = cl.ClaimLawsuitID
                        LEFT JOIN tblState s						ON s.StateID = l.StateID
                        LEFT JOIN tblDefenseCounselAddress dca		ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
                        LEFT JOIN tblPlaintiffCounselAddress pca	ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
                        LEFT JOIN tblDefendantCompany dc			ON a.DefendantCompanyID = dc.DefendantCompanyID
						inner join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=cl.ClaimLawsuitID
						inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
						WHERE ProcessedDate BETWEEN @Yesterday4PM AND @Today4PM and clst.ClaimLawsuitStatusID=23
						
						GROUP BY s.State ,
                        LastName ,
                        d.documentID ,
                        d.DocumentTitle ,
                        DefenseCounselID ,
                        FirstName ,
                        cpi.ClaimantPersonalInfoID ,
                        PlaintiffCounsel ,
                        DefenseCounsel ,
                        cl.LawsuitID 
	



        DECLARE @ClaimantPerDoc TABLE
            (
              DocID INT ,
              ClaimantList NVARCHAR(MAX)
            )
        DECLARE @DocID INT ,
            @LastDocID INT ,
            @Claimant NVARCHAR(MAX) ,
            @ClaimantID BIGINT ,
            @ClaimantList NVARCHAR(MAX)

        DECLARE Cur_Complaints CURSOR Local Fast_Forward
        FOR
            SELECT  DocID ,
                    COALESCE(Lname, '') + ', ' + COALESCE(Fname, '') ,
                    claimantpersonalInfoID
            FROM    @NewSettlements
        OPEN Cur_Complaints
        FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
        WHILE @@FETCH_STATUS = 0 
            BEGIN


                IF ( LEN(@ClaimantList) = 0
                     OR @ClaimantList IS NULL
                   ) 
                    BEGIN
	
                        SET @ClaimantList = '<ul style="margin:0; padding:0;"> <a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                            + CONVERT(NVARCHAR(50), @claimantID) + '">'
                            + @Claimant + COALESCE(' ('
                                                   + CONVERT(NVARCHAR(50), @claimantID)
                                                   + ')', '') + '</a></ul>'
                    END
                ELSE 
                    BEGIN
	
                        SET @ClaimantList = COALESCE(@ClaimantList, '')
                            + '<ul style="margin:0; padding:0;">  <a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                            + CONVERT(NVARCHAR(50), @claimantID) + '">'
                            + COALESCE(@Claimant, '') + COALESCE(' ('
                                                              + CONVERT(NVARCHAR(50), @claimantID)
                                                              + ')', '')
                            + '</a></ul>'

                    END
                SET @LastDocID = @DocID
                FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant,
                    @ClaimantID
                PRINT @@FETCH_STATUS
                IF @DocID <> @LastDocID
                    OR @@FETCH_STATUS <> 0 
                    BEGIN
	
                        INSERT  INTO @ClaimantPerDoc
                                SELECT  @LastDocID ,
                                        @ClaimantList
	
                        SET @ClaimantList = NULL
                    END
            END
        CLOSE Cur_Complaints
        DEALLOCATE Cur_Complaints
	


--Declare and populate the body of the email for individual firms
        DECLARE @BodyMessage NVARCHAR(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
        SET @BodyMessage = @LogoHeader
            + '<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'

        DECLARE @LastLawsuitID NVARCHAR(250) = ''

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

        IF ( SELECT COUNT(docTitle)
             FROM   @NewSettlements
           ) > 0 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<H4>The following settlements have been uploaded in the last 24 hours: </H4>'
                SET @BodyMessage = @BodyMessage
                    + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' 
                SET @BodyMessage = @BodyMessage
                    + N'<tr><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Document Title</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Linked Claimants (ClaimantID)</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">State</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Defense Counsel</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Plaintiff Counsel</th>'
	
                DECLARE @DocTitle NVARCHAR(250) ,
                    @clClaimantList NVARCHAR(500) ,
                    @ServiceDate NVARCHAR(300) ,
                    @State NVARCHAR(250) ,
                    @PlaintiffCounsel NVARCHAR(400) ,
                    @DefenseCounsel NVARCHAR(400) ,
                    @LawsuitID INT ,
                    @Namings NVARCHAR(500) ,
                    @NamingsRowID NVARCHAR(10)
	
                DECLARE Doc_List CURSOR
                FOR
                    SELECT  docTitle ,
                            cl.ClaimantList ,
                            CAST(MAX(ServiceDate) AS VARCHAR(MAX)) ,
                            [state] ,
                            PlaintiffCounsel ,
                            DefenseCounsel ,
                            LawsuitID
                    FROM    @NewSettlements C
                            INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
                    GROUP BY [state] ,
                            cl.docid ,
                            docTitle ,
                            cl.ClaimantList ,
                            PlaintiffCounsel ,
                            DefenseCounsel ,
                            LawsuitID
                    ORDER BY cl.DocID	

                OPEN Doc_List
                FETCH NEXT FROM Doc_List INTO @DocTitle, @clClaimantList,
                    @ServiceDate, @State, @PlaintiffCounsel, @DefenseCounsel,
                    @LawsuitID

                WHILE @@FETCH_STATUS = 0 
                    BEGIN
	
		
                        SET @BodyMessage = @BodyMessage + '<tr>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @DocTitle + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @clClaimantList + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @State + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @DefenseCounsel + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @PlaintiffCounsel + ' </td>'
                        --SET @BodyMessage = @BodyMessage
                        --    + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top">'

		
                        --DECLARE @ITTNamings TABLE
                        --    (
                        --      ID INT NOT NULL
                        --             IDENTITY(1, 1)
                        --             PRIMARY KEY ,
                        --      LawsuitID INT ,
                        --      DefendantCompany NVARCHAR(500) ,
                        --      DefendantCompanyID INT
                        --    )
                        --INSERT  INTO @ITTNamings
                        --        SELECT  cl.LawsuitID ,
                        --                DefendantCompany ,
                        --                a.DefendantCompanyID
                        --        FROM    tblAllegation a
                        --                INNER JOIN tblClaimLawsuit cl		ON a.ClaimLawsuitID = cl.ClaimLawsuitID
                        --                INNER JOIN tblDefendantCompany dc	ON a.DefendantCompanyID = dc.DefendantCompanyID
                        --        WHERE   LawsuitID = @LawsuitID
                        --        GROUP BY cl.LawsuitID ,
                        --                DefendantCompany ,
                        --                a.DefendantCompanyID
                        --        ORDER BY lawsuitid ,
                        --                DefendantCompany
		
                        --DECLARE ITTNamings_curs CURSOR
                        --FOR
                        --    SELECT DISTINCT
                        --            DefendantCompany
                        --    FROM    @ITTNamings
                        --    WHERE   LawsuitID = @LawsuitID
			

                        --OPEN ITTNamings_curs
                        --FETCH NEXT FROM ITTNamings_curs INTO @Namings
			
                        --WHILE @@FETCH_STATUS = 0 
                        --    BEGIN		
				
                        --        SET @BodyMessage = @BodyMessage + @Namings
                        --            + ' <br/>'
				
                        --        FETCH NEXT FROM ITTNamings_curs INTO @Namings
			
                        --    END
                        --CLOSE ITTNamings_curs
                        --DEALLOCATE ITTNamings_curs
 
                        SET @BodyMessage = @BodyMessage + '</td></tr>'
																	
                        SET @LastLawsuitID = @LawsuitID

                        FETCH NEXT FROM Doc_List INTO @DocTitle,
                            @clClaimantList, @ServiceDate, @State,
                            @PlaintiffCounsel, @DefenseCounsel, @LawsuitID

                    END
                CLOSE Doc_List
                DEALLOCATE Doc_List
	
	
                SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'
	

            END
        ELSE 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<H4>No settlements have been uploaded in the last 24 hours. </H4>'
            END



--Undo the HTML stripping from the xml path function above so embedded tags work
        SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;',
                                   '>')

----Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/Pfizer/cp/Pages/Documents.aspx">here</A> to access the document(s).</b><br><br>'

--Service Date Assumption 

        IF ( SELECT COUNT(docTitle)
             FROM   @NewSettlements
           ) > 0 
            BEGIN
            --    --SET @BodyMessage = @BodyMessage
            --    --    + '<br><br><br>'
        

--KCIC address and disclaimer
        SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333'



--IF (Select COUNT(docTitle) from @NewSettlements) >0 
--begin
--Execute stored procedure to send the email to that firm
 EXEC msdb.dbo.sp_send_dbmail @profile_name = 'Admin',
@recipients = 'owenss@kcic.com',--'Pfizer@kcic.com;potterm@kcic.com; monahanc@kcic.com',
@copy_recipients = '',
@subject = @subject, @body = @BodyMessage, @body_format = 'html'
   
   END
   END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMAIL_qryInternalEmailNewSettlements] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMAIL_qryInternalEmailNewSettlements] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMAIL_qryInternalEmailNewSettlements] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMAIL_qryInternalEmailNewSettlements] TO [power_user]
GO
