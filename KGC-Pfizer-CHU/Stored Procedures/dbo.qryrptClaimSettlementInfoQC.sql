SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimSettlementInfoQC] (@CLaimLawsuitID int)
as

Select cs.ClaimantSettlementID, 
	cs.ClaimantSettlementID as SettlementHeader,
	ClaimLawsuitID, 
	SettlementGroupID, 
	SettlementDate, 
	SettlementAmount, 
	PayToCompany, 
	c.SourceNumber, 
	c.SourceDate, c.SourceAmount ,
	SettlementNotes,
	SettlementDueDate
From tblCLaimantSettlement cs 
left join tblpaymentsettlement ps on ps.ClaimantSettlementID =cs.ClaimantSettlementID 
left join tblcheck c on c.CheckID =ps.CheckID 
Where ClaimLawsuitID = @ClaimLawsuitID
Order By SettlementDate

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimSettlementInfoQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimSettlementInfoQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimSettlementInfoQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimSettlementInfoQC] TO [power_user]
GO
