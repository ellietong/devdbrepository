SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qrysfrmClaimsForDismissal2]

As
Set NoCount On

DECLARE @DocumentIDTable TABLE (DocumentID int)
INSERT INTO @DocumentIDTable
SELECT
	d.DocumentID
FROM tblDocuments d
	inner join tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
	inner join tblClaim c on c.ClaimantPersonalInfoID=mcd.ClaimantPersonalInfoID
	inner join tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
	inner join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
WHERE   (clst.ClaimLawsuitStatusID is not null or clst.ProcessedDate is not null) 
		AND (d.QCDate IS NULL) AND (d.ClaimantDocumentTypeID = 16) and CreatedBy not like '%Historical%' 
		AND DateCreated > '6/11/2012'


SELECT LastName + coalesce(', ' + FirstName + ' ' + MiddleName,', ' + FirstName ,', ' +  MiddleName, '') as ClaimantName,
 SocialSecurityNumber, IndividualDocketNumber, CaseCaption, DocketNumber, clst.StatusNotes, cpi.ClaimantPersonalInfoID,
 State, cl.ClaimLawsuitID,clst.ClaimLawsuitStatusTrackingID,  ProcessedDate, StatusDate,
  ClaimLawsuitStatus, clst.ClaimLawsuitStatusID, IsPrimaryStatus as IsPrimary, mcd.DocumentID 
 FROM tblClaimantPersonalInfo cpi
	INNER JOIN tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
	LEFT JOIN  (select * from tblClaimLawsuitStatusTracking where IsPrimaryStatus =1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN  tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
	INNER JOIN tblLawsuit l on l.LawsuitID=cl.LawsuitID
	INNER JOIN tblState s on s.StateID=l.StateID
	INNER JOIN @DocumentIDTable did on mcd.DocumentID = did.DocumentID
GROUP BY	DocketNumber, IndividualDocketNumber,LastName, MiddleName, FirstName,cpi.ClaimantPersonalInfoID, CaseCaption, DocketNumber, clst.StatusNotes,IsPrimaryStatus,
			State, cl.ClaimLawsuitID, SocialSecurityNumber,clst.ClaimLawsuitStatusTrackingID, ProcessedDate, ClaimLawsuitStatus, StatusDate, clst.ClaimLawsuitStatusID, mcd.DocumentID
--Order By ClaimantName, cpi.ClaimantPersonalInfoID, DocketNumber
  
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForDismissal2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForDismissal2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForDismissal2] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForDismissal2] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForDismissal2] TO [power_user]
GO
