SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_DEADLINE_qryTimeZoneList]
AS 
    SET NOCOUNT ON

    SELECT  TimeZoneID ,
            Abbreviation AS TimeZone
    FROM    tblTimeZone
	UNION
	SELECT NULL, ''
	ORDER BY Abbreviation

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryTimeZoneList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryTimeZoneList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryTimeZoneList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryTimeZoneList] TO [power_user]
GO
