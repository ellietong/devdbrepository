SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByStateByYear] 
	-- Add the parameters for the stored procedure here
	(@YearID int, @MonthID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @year int
--Set @Year=2010

DECLARE @year int
SET @year = @YearID

DECLARE @Month int
SET @Month = @MonthID

select FullNameState AS State, COUNT(distinct(cl.ClaimLawsuitID)) As Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s on l.StateID=s.StateID
where year(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))=@Year 
		and MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) <=@month
		
group by FullNameState
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByStateByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByStateByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByStateByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByStateByYear] TO [power_user]
GO
