SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryUpdClaimType] 
	-- Add the parameters for the stored procedure here
    (
      @ClaimTypeID BIGINT ,
      @ClaimTypeValueID INT,
	  @chkNotifyCounsel BIT,
	  @EmailAddress NVARCHAR(250) 
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        UPDATE  tblClaimType
        SET     ClaimTypeValueID = @ClaimTypeValueID,
				SPUsername = @Emailaddress
        WHERE   ClaimTypeID = @ClaimTypeID	
		
		UPDATE tblClaim
		SET NotifyCounsel = @chkNotifyCounsel
		From tblclaim c 
		WHERE   c.ClaimID = (Select MAX(ClaimID) from tblClaimType where ClaimTypeID = @ClaimTypeID)

    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaimType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaimType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaimType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaimType] TO [power_user]
GO
