SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_DEADLINE_qryEventTypeListFilter]
@DeadlineTypeID INT = NULL

AS 
    SET NOCOUNT ON

    SELECT  EventTypeID ,
            UPPER(EventType) AS EventType
    FROM    tblEventType
	WHERE DeadlineTypeID = @DeadlineTypeID
	UNION
	SELECT NULL, ''
	ORDER BY EventType

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryEventTypeListFilter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryEventTypeListFilter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryEventTypeListFilter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryEventTypeListFilter] TO [power_user]
GO
