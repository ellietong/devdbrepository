SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_FIRM_qryGetFirmAddressByEmail]
    (
      @emailaddress NVARCHAR(1000)
    )
AS 
    DECLARE @firmID INT
    SET @firmID = ( SELECT  FirmID
                    FROM    vtblPerson
                    WHERE   Email = @emailaddress
                  )
    BEGIN
	
        DECLARE @emaildomain NVARCHAR(1000)
	
        --SET @emaildomain = SUBSTRING(@emailaddress,
        --                             CHARINDEX('@', @emailaddress),
        --                             LEN(@emailaddress) - CHARINDEX('@',
        --                                                      @emailaddress)
        --                             + 1)
	
     --   IF @emaildomain = '@kcicllc.com'
		   --OR @emaildomain = 'kcic.com'
     --       OR @firmID = 132 
            BEGIN

                SELECT DISTINCT
                        DefenseCounselAddressID AS FirmAddressID ,
                        DefenseCounsel + ' (' + State + ')'  AS Firm
                FROM    tblDefenseCounselAddress 
                WHERE   DefenseCounselID NOT IN ( 134, 135, 136, 268 )
				UNION
				SELECT  NULL ,
						''
                ORDER BY DefenseCounsel + ' (' + State + ')' 
	
            END	
    --    ELSE 
    --        BEGIN	
	
    --            SELECT DISTINCT
    --                    FirmAddressID ,
    --                    DefenseCounsel AS Firm
    --            FROM    vtblPerson
    --                    INNER JOIN tblDefenseCounselAddress dca ON vtblPerson.FirmAddressID = dca.DefenseCounselAddressID
    --            WHERE   @emailaddress = vtblPerson.Email
				--UNION
				--SELECT  NULL ,
				--		''
    --            ORDER BY DefenseCounsel
	
    --        END
    END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmAddressByEmail] TO [power_user]
GO
