SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation]
    (
    @SetPymtID INT ,
    @SettlePaymentComments NVARCHAR(MAX) ,
    @ReleaseTypeID INT ,
	@ReleaseRcvdDate DATETIME,
    @ReleaseSignedDate DATE ,
    @MedicareReported BIT ,
    @MedicareFullyDocumented BIT,
    @MedicareReportedDate DATETIME,
	@CPLDate DATE,
	@CPLAmount MONEY,
	@FDLDATE DATE,
	@FDLAmount MONEY,
	@MedicareSatisfaction DATE,
	@MedicaidLienAmount MONEY,
	@PrivateLienAmount MONEY,
	@MedicareAdvantageLienAmount MONEY
																			
    )
AS --DECLARE @ClaimID int
--SET @ClaimID = 1

    UPDATE  tblSettlePaymentDocumentation
    SET     SettlePaymentComments = @SettlePaymentComments ,
            ReleaseTypeID = CASE WHEN @ReleaseTypeID = 0 THEN NULL
                                 ELSE @ReleaseTypeID END ,
		    MedicareReported = @MedicareReported ,
            MedicareFullyDocumented = @MedicareFullyDocumented,           
            MedicareFormSent = NULLIF(@MedicareReportedDate, ''),
			ReleaseRcvdDate = NULLIF(@ReleaseRcvdDate, ''),
            ReleaseSignedDate = NULLIF(@ReleaseSignedDate, ''),
			CPLDate = NULLIF(@CPLDate, ''),
			CPLAmount = NULLIF(@CPLAmount, ''),
			FDLDate = NULLIF(@FDLDATE, ''),
			FDLAmount = NULLIF(@FDLAmount, ''),
			MedicareSatisfaction = NULLIF(@MedicareSatisfaction, ''),
			MedicaidLienAmount = NULLIF(@MedicaidLienAmount, ''),
			PrivateLienAmount = NULLIF(@PrivateLienAmount, ''),
			MedicareAdvantageLienAmount = NULLIF(@MedicareAdvantageLienAmount, '')
						
		
    WHERE   SetPymtID = @SetPymtID



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GO
