SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmReleaseTemplateFile] 
as

Set NoCount On

Select RIGHT(FilePath,12), FilePath, TemplateID
From tblReleaseTemplate
Order By FilePath

Return
GO
GRANT TAKE OWNERSHIP ON  [dbo].[qryfrmReleaseTemplateFile] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmReleaseTemplateFile] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmReleaseTemplateFile] TO [base_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qryfrmReleaseTemplateFile] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmReleaseTemplateFile] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmReleaseTemplateFile] TO [power_user]
GO
