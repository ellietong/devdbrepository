SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_DEADLINE_qryEventTypeList]
AS 
    SET NOCOUNT ON

    SELECT  EventTypeID ,
            UPPER(EventType) AS EventType
    FROM    tblEventType
	UNION
	SELECT NULL, ''
	ORDER BY EventType

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryEventTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryEventTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryEventTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryEventTypeList] TO [power_user]
GO
