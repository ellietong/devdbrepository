SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_DEADLINE_qryTimePeriodList]
AS 
    SET NOCOUNT ON

    SELECT  TimePeriodID ,
            TimePeriod
    FROM    tblTimePeriod
	UNION
	SELECT NULL, ''
	ORDER BY TimePeriod


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryTimePeriodList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryTimePeriodList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryTimePeriodList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryTimePeriodList] TO [power_user]
GO
