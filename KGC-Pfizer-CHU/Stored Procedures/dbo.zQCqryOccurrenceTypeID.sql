SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[zQCqryOccurrenceTypeID]
As

SET NOCOUNT ON

SELECT ot.OccurrenceType, ot.OccurrenceTypeID, MIN(o.OccurrenceName) AS MinOfOccurrenceName, MAX(o.OccurrenceName) AS MaxOfOccurrenceName
FROM tblOccurrenceType ot INNER JOIN tblOccurrence o ON ot.OccurrenceTypeID = o.OccurrenceTypeID
GROUP BY ot.OccurrenceType, ot.OccurrenceTypeID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[zQCqryOccurrenceTypeID] TO [base_user]
GRANT EXECUTE ON  [dbo].[zQCqryOccurrenceTypeID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryOccurrenceTypeID] TO [power_user]
GRANT EXECUTE ON  [dbo].[zQCqryOccurrenceTypeID] TO [power_user]
GO
