SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_qryDocumentTypeFilterList]
    (
      @invoiceFlag INT = 0 ,
      @settlementFlag BIT = 0
    )
AS 
    SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @invoiceFlag bit
--DECLARE @settlementFlag bit
--SET @invoiceFlag    = 0
--SET @settlementFlag = 0

--These IF statements determine the WHERE based on the filters (note the nested IF statements).
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE 1=1'
    IF @invoiceFlag = 0 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND ClaimantDocumentTypeID <> 1'
            IF @settlementFlag = 1 
                BEGIN
                    SET @WhereString = @WhereString
                        + ' AND ClaimantDocumentGroupID IN (1, 3, 4)'
                END
            ELSE 
                BEGIN
                    SET @WhereString = @WhereString
                        + ' AND ClaimantDocumentGroupID IN (1, 3)'
                END
        END
    ELSE 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND ClaimantDocumentGroupID = 2'
            IF @invoiceFlag = 1 
                BEGIN
                    SET @WhereString = @WhereString
                        + ' AND ClaimantDocumentTypeID NOT IN (41, 42)'
                END
        END

--Here the final query is pieced together using the WHERE string created above, including a RECOMPILE statement.
    DECLARE @QueryString NVARCHAR(MAX)
    SET @QueryString = '
		SELECT ClaimantDocumentTypeID, 
			ClaimantDocumentType, 
			ClaimantDocumentGroupID
		FROM tblClaimantDocumentType' + @WhereString + '
		UNION
		SELECT NULL, ''<< All Document Types >>'', NULL
		ORDER BY ClaimantDocumentType
		OPTION (RECOMPILE)'

--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryDocumentTypeFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryDocumentTypeFilterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryDocumentTypeFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryDocumentTypeFilterList] TO [power_user]
GO
