SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_PERSON_qryGetApproverByEmail]
    (
      @ApproverTypeID int
    )
AS -- Declare the temporary fields used to track firm and email domain
    DECLARE @PersonID INT


IF @ApproverTypeID = 1 --Document Profile
BEGIN
	SELECT DISTINCT CONVERT(nvarchar(10),PersonID) AS PersonID
	FROM vtblPerson 
	WHERE PersonID IN (5098, 5087, 5099)
END
ELSE
BEGIN
	RETURN
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [power_user]
GO
