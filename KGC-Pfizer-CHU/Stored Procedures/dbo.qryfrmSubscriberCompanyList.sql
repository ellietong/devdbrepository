SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmSubscriberCompanyList]
AS
SET NOCOUNT ON

DECLARE @SubscriptionSummaryList TABLE(SubscriberID int, SubscriptionCompany nvarchar(255))

INSERT INTO @SubscriptionSummaryList
	SELECT SubscriberID, SubscriptionCompany 
	FROM vSubscription 
	UNION 
	SELECT 0 AS SubscriberID, '<<All Subscription Companies>>' AS SubscriptionCompany 
	FROM vSubscription 
	GROUP BY SubscriberID, SubscriptionCompany

SELECT SubscriberID, SubscriptionCompany 
FROM @SubscriptionSummaryList
ORDER BY CASE WHEN LEFT(SubscriptionCompany,1)='"' THEN SUBSTRING(SubscriptionCompany,2,LEN(SubscriptionCompany)-1) ELSE SubscriptionCompany END

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriberCompanyList] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriberCompanyList] TO [base_user]
GRANT ALTER ON  [dbo].[qryfrmSubscriberCompanyList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriberCompanyList] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriberCompanyList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriberCompanyList] TO [Subscription_User]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriberCompanyList] TO [Subscription_User]
GO
