SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryGetClaimantDetails] 
	-- Add the parameters for the stored procedure here
(@ClaimantPersonalInfoID int, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--DECLARE @ClaimantPersonalInfoID int = 296048,--845337,--444555,
--		@EmailAddress nvarchar(250) = --'sara.egide@tuckerellis.com'
--									 --'jeffrey.healy@tuckerellis.com'
--									 'owenss@kcic.com'

DECLARE @ClaimID int = (SELECT ClaimID FROM tblClaim WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND IsCurrentClaim = 1)
--SELECT @ClaimID
    DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )

    DECLARE @ClaimsCount INT
    IF ( ( RIGHT(@emailaddress, 11) = 'kcicllc.com' )
		 OR ( RIGHT(@emailaddress, 8) = 'kcic.com' )
         OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        SET @ClaimsCount = 1
    ELSE 
        BEGIN

			SET @ClaimsCount = 
			(
				SELECT c.ClaimID
				FROM tblClaim c 
				INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
				INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
				INNER JOIN tblLawsuit l on cl.LawsuitID = l.LawsuitID
				INNER JOIN tblDefenseJurisdiction dj	ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
				WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
					AND (dj.DefenseCounselAddressID = @DefenseCounselAddressID OR l.DefenseCounselAddressID = @DefenseCounselAddressID)
			)

        END


IF (SELECT COUNT(@ClaimsCount)) > 0
	BEGIN
DECLARE @Content nvarchar(max) = '',
@Header nvarchar(max) = '',
@InjuryPartySection nvarchar(max) = '<br/><b>Injured Party</b><br/><hr>',
@CounselInfoSection nvarchar(max) = '<br/><b>Counsel Information</b><br/><hr>',
@CurrentCourtInformation nvarchar(max) = '<br/><b>Current Court Information</b><br/><hr>',
@PreviousCourtInformation nvarchar(max) = '<br/><b>Previous Court Information</b><br/><hr>',
@AdditionalInjPartyInfo nvarchar(max) = '<br/><b>Injured Party Information</b><br/><hr>',
@RelatedPartyInfo nvarchar(max) = '<b>Related Party Information</b><br/><hr>',
@MedicareInfo nvarchar(max) = '<br/><b>Medicare Information</b><br/><hr>',
@SmokingHistory nvarchar(max) = '<b>Smoking History</b><br/><hr>',
@MedicalHistory nvarchar(max) = '<br/><b>Medical History</b><br/><hr>',
@WorkHistory nvarchar(max) = '<b>Work History</b><br/><hr>',
@ProductInfo nvarchar(max) = '',
@CaseInfo nvarchar(max) = '<br/><p><b>Case Information</b><br/><hr>'

DECLARE @tdStyle nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left; page-break-inside:avoid;'
DECLARE @tdStyleNoBorder nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: left; padding: .3em; color: #000000; vertical-align: left;'
DECLARE @tdStyleNoBorderBold nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: left; font-weight: bold; padding: .3em; color: #000000; vertical-align: left;'

SET @Content = '
<html>
	<body style="margin: 0; padding: 0;">
		<p style="text-align:center">			
			<b>Pfizer Claimant At a Glance</b><br/>
				' + CONVERT(nvarchar, GETDATE(), 101) +'
		<br/>
		</p>


'

BEGIN 
	SET @InjuryPartySection = @InjuryPartySection + '
		<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
			' 
	SET @InjuryPartySection = @InjuryPartySection + (
	SELECT (select [td/@style] = @tdStyleNoBorderBold, field as 'td' for xml path(''), type), (select [td/@style] = @tdStyleNoBorder, Information as 'td' for xml path(''), type)
	from (
	select Field, Information
	from (
			SELECT DISTINCT 
				   CONVERT(nvarchar(max),COALESCE(cpi.LastName + ', ' + 
													CASE WHEN FirstName IS NULL 
														THEN '' 
														ELSE FirstName + ' ' END + 
													CASE WHEN MiddleName IS NULL 
														THEN '' 
														ELSE MiddleName + ' ' END, '')) AS [Claimant Name],
				   CONVERT(nvarchar(max), COALESCE(c.ClaimantPersonalInfoID, '')) AS [Claimant ID],
				   CONVERT(nvarchar(max), COALESCE(ClaimLawsuitStatus + ' (' + COALESCE(ClaimLawsuitStatusGroup, 'UNKNOWN') + ') ', '')) AS [Case Status],
				   CONVERT(nvarchar(max), COALESCE(ClaimantNotes, '')) AS [Claimant Notes]

	FROM tblClaim c 
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID =  cls.ClaimLawsuitStatusID
	LEFT JOIN  tblClaimLawsuitStatusGroup clsg on cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND IsCurrentClaim = 1
		AND IsPrimaryForClaim = 1
		AND IsPrimaryStatus = 1

	) ss
	unpivot (Information FOR Field in ( [Claimant Name], [Claimant ID], [Case Status], [Claimant Notes]
		   )) as unpv ) tbl FOR XML PATH('tr'))
	SET @InjuryPartySection = @InjuryPartySection + '
	</tr>
</table>
<br/>'
END

BEGIN 
	SET @CounselInfoSection = @CounselInfoSection + '
		<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
			' 
	SET @CounselInfoSection = @CounselInfoSection + (
	SELECT (select [td/@style] = @tdStyleNoBorderBold, field as 'td' for xml path(''), type), (select [td/@style] = @tdStyleNoBorder, Information as 'td' for xml path(''), type)
	from (
	select Field, Information
	from (
			SELECT DISTINCT 
				   CONVERT(nvarchar(max),COALESCE(lpca.PlaintiffCounsel, '')) AS [Local Plaintiff Counsel],
				   CONVERT(nvarchar(max), COALESCE(npca.PlaintiffCounsel, '')) AS [National Plaintiff Counsel],
				   CONVERT(nvarchar(max), COALESCE(ldca.DefenseCounsel, '')) AS [Local Counsel],
				   CONVERT(nvarchar(max), COALESCE(rdca.DefenseCounsel, '')) AS [Regional Counsel]

	FROM tblClaim c 
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l	ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblPlaintiffCounselAddress lpca on l.PlaintiffCounselAddressID =  lpca.PlaintiffCounselAddressID
	LEFT JOIN tblPlaintiffCounselAddress npca ON l.PrimaryNationalPlaintiffCounselID = npca.PlaintiffCounselAddressID
	LEFT JOIN tblDefenseCounselAddress ldca on	l.DefenseCounselAddressID = ldca.DefenseCounselAddressID
	LEFT JOIN tblDefenseCounselAddress rdca ON l.RegionalCounselAddressID = rdca.DefenseCounselAddressID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND IsCurrentClaim = 1
		AND IsPrimaryForClaim = 1
		AND IsPrimaryStatus = 1

	) ss
	unpivot (Information FOR Field in ( [Local Plaintiff Counsel], [National Plaintiff Counsel], [Local Counsel], [Regional Counsel]
		   )) as unpv ) tbl FOR XML PATH('tr'))
	SET @CounselInfoSection = @CounselInfoSection + '
	</tr>
</table>
<br/>'
END

DECLARE @br nvarchar = '<br/>'

BEGIN
SET @CurrentCourtInformation = @CurrentCourtInformation + '
	<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
		'
SET @CurrentCourtInformation = @CurrentCourtInformation +
( 
SELECT REPLACE((
SELECT 
(select [td/@style] = @tdStyleNoBorderBold, field as 'td' for xml path(''), type), (select [td/@style] = @tdStyleNoBorder, Information as 'td' for xml path(''), type)
	from (
	select Field, Information
	from (
			SELECT DISTINCT 
					CONVERT(nvarchar(max),COALESCE(CASE WHEN COALESCE(ClaimantCount, 0) > 1 THEN 'Yes' ELSE 'No' END, '')) AS [Multiple Plaintiffs on Complaint?],
					CONVERT(nvarchar(max), COALESCE(Matter, '')) AS [Matter],
					CONVERT(nvarchar(max), COALESCE(ClaimSequence, '')) AS [Claim Sequence],
					--CONVERT(nvarchar(max), COALESCE(dbo.fnClaimTypesByClaimID(c.ClaimID), '')) AS [Claim Type],
					--CONVERT(nvarchar(max), COALESCE(REPLACE(ClaimTypeValue, '|', CHAR(10)), '')) AS [Claim Type],
					CONVERT(nvarchar(max), COALESCE(ClaimTypeValue, '')) AS [Claim Type],
					CONVERT(nvarchar(max), COALESCE(DocketNumber, '')) AS [Docket Number],
					CONVERT(nvarchar(max), COALESCE(CaseCaption, '')) AS [Case Caption],
					CONVERT(nvarchar(max), COALESCE(CourtType, '')) AS [Court Type],
					CONVERT(nvarchar(max), COALESCE(Jurisdiction, '')) AS [Jurisdiction],
					CONVERT(nvarchar(max), COALESCE(s.State, '')) AS [State],
					CONVERT(nvarchar(max), COALESCE(Judge, '')) AS [Judge],
					COALESCE(CONVERT(nvarchar(max), NULLIF(ActiveTrialDate, ''), 101), '') AS [Trial Date],
					CONVERT(nvarchar(max), COALESCE(Material, '')) AS [Material],
					CONVERT(nvarchar(max), COALESCE(AllegationType, '')) AS [Allegation Type],
					CONVERT(nvarchar(max), COALESCE(Damage, '')) AS [Damage],
					CONVERT(nvarchar(max), COALESCE(CASE WHEN COALESCE(LossOfConsortium, 0) = 1 THEN 'Yes' ELSE 'No' END, '')) AS [Loss of Consortium?],
					CONVERT(nvarchar(max), COALESCE(cl.CaseNotes, '')) AS [Case Notes]

	FROM tblClaim c 
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l	ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblPlaintiffCounselAddress lpca on l.PlaintiffCounselAddressID =  lpca.PlaintiffCounselAddressID
	LEFT JOIN tblPlaintiffCounselAddress npca ON l.PrimaryNationalPlaintiffCounselID = npca.PlaintiffCounselAddressID
	LEFT JOIN tblDefenseCounselAddress ldca on	l.DefenseCounselAddressID = ldca.DefenseCounselAddressID
	LEFT JOIN tblDefenseCounselAddress rdca ON l.RegionalCounselAddressID = rdca.DefenseCounselAddressID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID =  cls.ClaimLawsuitStatusID
	LEFT JOIN (SELECT c.ClaimID, COUNT(c.ClaimantPersonalInfoID) AS ClaimantCount
				FROM    tblClaim c
						INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
						INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
				WHERE c.ClaimID = @ClaimID
				GROUP BY c.ClaimID) MultiClaimantLawuit ON c.ClaimID =  MultiClaimantLawuit.ClaimID
	INNER JOIN tblMatter m ON C.MatterID = m.MatterID
	INNER JOIN tblClaimSequence cs on c.ClaimSequenceID = cs.ClaimSequenceID
	INNER JOIN tblJurisdiction j on l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblCourtType ct on l.CourtTypeID = ct.CourtTypeID
	INNER JOIN tblState s on l.StateID = s.StateID
	INNER JOIN tblAllegation a on cl.ClaimLawsuitID = a.ClaimLawsuitID
	INNER JOIN tblAllegationType at on a.AllegationTypeID = at.AllegationTypeID
	INNER JOIN tblMaterial mat on a.MaterialID = mat.MaterialID
	LEFT JOIN tblDamage d on a.DamageID = d.DamageID
		LEFT JOIN (SELECT DISTINCT c.ClaimID,
					(SELECT  DISTINCT '' + ClaimTypeValue + '' + '| ' AS [text()] 
					FROM    tblClaimType CT
						INNER JOIN tblClaimTypeValue CTV ON CT.ClaimTypeValueID = CTV.ClaimTypeValueID	
					WHERE ct.ClaimID = c.ClaimID 
					FOR XML PATH('')) AS ClaimTypeValue
					FROM tblClaim c) ctv ON c.ClaimID = ctv.ClaimID
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND IsCurrentClaim = 1
		AND IsPrimaryForClaim = 1
		AND IsPrimaryStatus = 1

	) ss
	unpivot (Information FOR Field in ( [Multiple Plaintiffs on Complaint?], 
										[Matter], 
										[Claim Sequence], 
										[Claim Type], 
										[Docket Number], 
										[Case Caption],
										[Court Type], 
										[Jurisdiction], 
										[State], 
										[Judge], 
										[Trial Date], 
										[Material], 
										[Allegation Type], 
										[Damage], 
										[Loss of Consortium?],
										[Case Notes]
		   )) as unpv ) tbl FOR XML PATH('tr'))
		   , '|', '<br/>')
	   
	   )
SET @CurrentCourtInformation = @CurrentCourtInformation + '
</tr>
</table>
<br/>'
END

IF (SELECT COUNT(cl.ClaimLawsuitID)
FROM tblClaim c 
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
INNER JOIN tblLawsuit l	ON cl.LawsuitID = l.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND (IsCurrentClaim = 0
	OR IsPrimaryForClaim = 0)
	AND IsPrimaryStatus = 1
) > 0
BEGIN
SET @PreviousCourtInformation = @PreviousCourtInformation + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	

N'<tr><th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claim Sequence</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Status Date</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Status</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Docket Number</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Court Type</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jurisdiction</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">State</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Judge</th>' +
	
	
CAST ( (
SELECT DISTINCT	@TDStyle AS 'td/@style', (SELECT Matter) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(ClaimSequence, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(max), NULLIF(StatusDate, ''), 101), '')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(ClaimLawsuitStatus, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(DocketNumber, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CourtType, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(Jurisdiction, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(s.State, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(Judge, ' ')) as td, ''
FROM tblClaim c 
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
INNER JOIN tblLawsuit l	ON cl.LawsuitID = l.LawsuitID
LEFT JOIN tblPlaintiffCounselAddress lpca on l.PlaintiffCounselAddressID =  lpca.PlaintiffCounselAddressID
LEFT JOIN tblPlaintiffCounselAddress npca ON l.PrimaryNationalPlaintiffCounselID = npca.PlaintiffCounselAddressID
LEFT JOIN tblDefenseCounselAddress ldca on	l.DefenseCounselAddressID = ldca.DefenseCounselAddressID
LEFT JOIN tblDefenseCounselAddress rdca ON l.RegionalCounselAddressID = rdca.DefenseCounselAddressID
INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID =  cls.ClaimLawsuitStatusID
LEFT JOIN (SELECT c.ClaimID, COUNT(c.ClaimantPersonalInfoID) AS ClaimantCount
			FROM    tblClaim c
					INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
					INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
					INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
			WHERE c.ClaimID = @ClaimID
			GROUP BY c.ClaimID) MultiClaimantLawuit ON c.ClaimID =  MultiClaimantLawuit.ClaimID
INNER JOIN tblMatter m ON C.MatterID = m.MatterID
INNER JOIN tblClaimSequence cs on c.ClaimSequenceID = cs.ClaimSequenceID
INNER JOIN tblJurisdiction j on l.JurisdictionID = j.JurisdictionID
LEFT JOIN tblCourtType ct on l.CourtTypeID = ct.CourtTypeID
INNER JOIN tblState s on l.StateID = s.StateID
INNER JOIN tblAllegation a on cl.ClaimLawsuitID = a.ClaimLawsuitID
INNER JOIN tblAllegationType at on a.AllegationTypeID = at.AllegationTypeID
INNER JOIN tblMaterial mat on a.MaterialID = mat.MaterialID
LEFT JOIN tblDamage d on a.DamageID = d.DamageID
WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND (IsCurrentClaim = 0
	OR IsPrimaryForClaim = 0)
	AND IsPrimaryStatus = 1


for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
N'</table>' +  N'<br/>'	
END
ELSE
BEGIN
	SET @PreviousCourtInformation = @PreviousCourtInformation + '<p font-family: Calibri; font-size: 11pt;>No previous court information available for this claimant.</p>' 
END

BEGIN 
	SET @AdditionalInjPartyInfo = @AdditionalInjPartyInfo + '
		<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
			' 
	SET @AdditionalInjPartyInfo = @AdditionalInjPartyInfo + (
	SELECT (select [td/@style] = @tdStyleNoBorderBold, field as 'td' for xml path(''), type), (select [td/@style] = @tdStyleNoBorder, Information as 'td' for xml path(''), type)
	from (
	select Field, Information
	from (
			SELECT DISTINCT 
				   CONVERT(nvarchar(max),COALESCE(CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + COALESCE(MiddleName, '') + ' ' + LastName, ' ')) AS [Name],
				   CONVERT(nvarchar(max), CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE LEFT(socialsecuritynumber, 3) +'-'+  LEFT(RIGHT(socialsecuritynumber, 6), 2) +'-'+  RIGHT(socialsecuritynumber, 4) END) AS [SSN],
				   COALESCE(CONVERT(nvarchar(max), NULLIF(BirthDate, ''), 101), '') AS [DOB],
				   COALESCE(CONVERT(nvarchar(max), NULLIF(DeceasedDate, ''), 101), '') AS [DOD],
				   CONVERT(nvarchar(max), COALESCE(COALESCE(nullif(COALESCE(Address1 + CHAR(13) + CHAR(10), '') + 
												   COALESCE(Address2 + CHAR(13) + CHAR(10),'') + 
												   COALESCE(City + ', ', '') + COALESCE(s.State + ' ','') + 
												   COALESCE(CONVERT(varchar,Zip),'') + 
												   COALESCE('-'+CONVERT(varchar,mailzip),''),''),''), '')) AS [Address],
				   COALESCE(CONVERT(nvarchar(max), NULLIF(MarriageDate, ' '), 101), '') AS [Date of Marriage]
	FROM tblClaim c 
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID =  cls.ClaimLawsuitStatusID
	LEFT JOIN tblState s on cpi.StateID = s.StateID
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND IsCurrentClaim = 1
		AND IsPrimaryForClaim = 1
		AND IsPrimaryStatus = 1

	) ss
	unpivot (Information FOR Field in ( [Name], [SSN], [DOB], [DOD], [Address], [Date of Marriage]
		   )) as unpv ) tbl FOR XML PATH('tr'))
	SET @AdditionalInjPartyInfo = @AdditionalInjPartyInfo + '
	</tr>
</table>
<br/>
<br/>'
END

IF (SELECT COUNT(ClaimantRelatedPartyID)
	FROM tblClaimantRelatedParty WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	) > 0
BEGIN
SET @RelatedPartyInfo = @RelatedPartyInfo + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0";>'+--; page-break-after:always;">' + 
	
N'<tr><th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Name</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Relationship</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Legal Capacity</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">SSN</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">DOB</th>' +
	
CAST ( (
SELECT DISTINCT	@TDStyle AS 'td/@style', (SELECT COALESCE(CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + COALESCE(MiddleName, '') + ' ' + LastName, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(Relationship, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(lc.LegalCapacity, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT CASE WHEN RPSocialSecurityNumber IS NULL THEN '''' ELSE LEFT(RPSocialSecurityNumber, 3) +'-'+  LEFT(RIGHT(RPSocialSecurityNumber, 6), 2) +'-'+  RIGHT(RPSocialSecurityNumber, 4) END) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(max), NULLIF(RPBirthDate, ''), 101), '')) as td, ''
	FROM tblClaimantRelatedParty crp 
		LEFT JOIN tblRelationship r		ON r.RelationshipID=crp.RelationshipID
		LEFT JOIN tblLegalCapacity lc	ON lc.LegalCapacityID = crp.LegalCapacityID
		LEFT JOIN tblState s ON crp.StateID = s.StateID  
	WHERE crp.ClaimantPersonalInfoID = @ClaimantPersonalInfoID

for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
N'</table>' +  N'<br/>'	
END
ELSE
BEGIN
	SET @RelatedPartyInfo = @RelatedPartyInfo + 'No related party information available for this claimant.<br/>' 
END

BEGIN 
	SET @MedicareInfo = @MedicareInfo + '
		<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
			' 
	SET @MedicareInfo = @MedicareInfo + (
	SELECT (select [td/@style] = @tdStyleNoBorderBold, field as 'td' for xml path(''), type), (select [td/@style] = @tdStyleNoBorder, Information as 'td' for xml path(''), type)
	from (
	select Field, Information
	from (
			SELECT DISTINCT 
				   CONVERT(nvarchar(max), COALESCE(MedicareEligible, '')) AS [Medicare Eligible],
				   CONVERT(nvarchar(max), COALESCE(CASE WHEN MedicareSatisfaction IS NULL THEN 'No' ELSE 'Yes' END, '')) AS [Proof of Medicare Satisfaction Date],
				   CONVERT(nvarchar(max), COALESCE(PostExposure, '')) AS [Post 12/5/1980 Exposure],
				   CONVERT(nvarchar(max), COALESCE(CONVERT(nvarchar(max), NULLIF(CPLDate, ''), 101), '')) AS [CPL Date],
				   CONVERT(nvarchar(max), CASE WHEN CPLAmount IS NULL THEN '' ELSE '$' + REPLACE(convert(nvarchar(20), cast(CPLAmount as money), 1), '.00', '') END) AS [CPL Amount],
				   CONVERT(nvarchar(max), COALESCE(CONVERT(nvarchar(max), NULLIF(FDLDate, ''), 101), '')) AS [FDL Date],
				   CONVERT(nvarchar(max), CASE WHEN FDLAmount IS NULL THEN '' ELSE '$' + REPLACE(convert(nvarchar(20), cast(FDLAmount as money), 1), '.00', '') END) AS [FDL Amount],
				   CONVERT(nvarchar(max), CASE WHEN MedicaidLienAmount IS NULL THEN '' ELSE '$' + REPLACE(convert(nvarchar(20), cast(MedicaidLienAmount as money), 1), '.00', '') END) AS [Medicaid Lien Amount],
				   CONVERT(nvarchar(max), CASE WHEN PrivateLienAmount IS NULL THEN '' ELSE '$' + REPLACE(convert(nvarchar(20), cast(PrivateLienAmount as money), 1), '.00', '') END) AS [Private Lien Amount],
				   CONVERT(nvarchar(max), CASE WHEN MedicareAdvantageLienAmount IS NULL THEN '' ELSE '$' + REPLACE(convert(nvarchar(20), cast(MedicareAdvantageLienAmount as money), 1), '.00', '') END) AS [Medicare Advantage Lien Amount]

FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l	ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT JOIN tblSettlePaymentDocumentation spd on cl.ClaimLawsuitID = spd.ClaimLawsuitID
	LEFT JOIN (SELECT cpi.ClaimantPersonalInfoID, CASE WHEN COALESCE(MedicareBeneficiaryStatusID, 0) = 2 THEN 'Yes' ELSE 'No' END AS MedicareEligible,
				CASE WHEN COALESCE(dbo.fnLatestExposure(cpi.ClaimantPersonalInfoID), '1/1/1900') > '12/5/1980' THEN 'Yes' ELSE 'No' END AS PostExposure
				FROM tblClaimantPersonalInfo cpi
				WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID) AS cpi on cpi.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND IsCurrentClaim = 1
	AND IsPrimaryForClaim = 1
	AND IsPrimaryStatus = 1
	) ss
	unpivot (Information FOR Field in ( [Medicare Eligible], 
										[Proof of Medicare Satisfaction Date], 
										[Post 12/5/1980 Exposure], 
										[CPL Date], 
										[CPL Amount], 
										[FDL Date], 
										[FDL Amount], 
										[Medicaid Lien Amount], 
										[Private Lien Amount], 
										[Medicare Advantage Lien Amount]
		   )) as unpv ) tbl FOR XML PATH('tr'))
	SET @MedicareInfo = @MedicareInfo + '
	</tr>
</table>
<br/>
<br/>'
END

IF (SELECT Count(ClaimantSmokingHistoryID) FROM tblClaimantSmokingHistory WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID) > 0
	BEGIN

SET @SmokingHistory = @SmokingHistory + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	

N'<tr><th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Packs Per Day</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Start Date</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri;padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">End Date</th>
<th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Packs Per Year</th>' +
	
	
CAST ( (
SELECT DISTINCT	@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(decimal(10,1), (PacksPerDay)), 0)) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(25), SmokingStart, 101), ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(25), SmokingEnd, 101), ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(decimal(10,2), ((DATEDIFF(DAY, SmokingStart, SmokingEnd))/365.00)*PacksPerDay), 0)) as td, ''

		FROM tblClaimantSmokingHistory
		WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	
			
	END
ELSE
	BEGIN
		SET @SmokingHistory = @SmokingHistory + 'No smoking history data available for this claimant.<br/>'
	END

IF (SELECT Count(ClaimDiseaseID) FROM tblClaimDisease WHERE ClaimID = @ClaimID) > 0
	BEGIN

SET @MedicalHistory = @MedicalHistory + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	

N'<tr><th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disease</th>
	  <th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Diagnosis Date</th>
	  <th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Primary Disease</th>' +
	
	
CAST ( (
SELECT DISTINCT	@TDStyle AS 'td/@style', (SELECT COALESCE(Disease, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(25), DiseaseDiagnosisDate, 101), ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT CONVERT(nvarchar(max), COALESCE(CASE WHEN COALESCE(PrimaryDisease, 0) = 0 THEN 'No' ELSE 'Yes' END, ''))) as td, ''
		FROM tblClaimDisease CD
		INNER JOIN tblClaim C ON C.ClaimID = CD.ClaimID
		LEFT JOIN (select * from tblDisease union select * from tbldisease_alt) ddis on ddis.DiseaseID = CD.DiseaseID
		LEFT JOIN tblDoctor DOC on CD.DoctorID = DOC.DoctorID
		WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID	
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	
			
	END
ELSE
	BEGIN
		SET @MedicalHistory = 'No diagnosis information available for this claimant.<br/>'
	END

IF (SELECT Count(ClaimantEmploymentID) FROM tblclaimantemployment WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID) > 0
BEGIN
SET @WorkHistory = @WorkHistory + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	
N'<tr><th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Years</th>
	  <th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Employer/Jobsite</th>
	  <th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">City</th>
	  <th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">State</th>
	  <th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Occupation</th>
' +	
	
CAST ( (
SELECT DISTINCT	@TDStyle AS 'td/@style', (SELECT CONVERT(NVARCHAR(5), COALESCE(CONVERT(NVARCHAR(50) ,FLOOR(DATEDIFF(day,EmpStartDate,EmpEndDate)/365.242199)), ' '))) as td, '',
		@TDStyle AS 'td/@style', (SELECT Jobsite) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(JobsiteCity, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(s.State, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(o.Occupation, ' ')) as td, ''
	FROM tblClaimantEmployment ce 
	INNER JOIN tblClaimantPersonalInfo cpi ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT Join tblOccupation o on o.OccupationID = ce.OccupationID
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblState s ON ce.JobsiteStateID = s.StateID
	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID


for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
N'</table>' +  N'<br/>'	
END
ELSE
BEGIN
	SET @WorkHistory = @WorkHistory + 'No employment information available for this claimant.<br/>' 
END

	DECLARE @ClaimantEmploymentIDs TABLE (ceIDs int)
	INSERT INTO @ClaimantEmploymentIDs
	SELECT DISTINCT ClaimantEmploymentID
	FROM tblClaimantEmployment
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID

IF (SELECT Count(ClaimantProductID) FROM tblClaimantProduct cp INNER JOIN @ClaimantEmploymentIDs ceid ON cp.ClaimantEmploymentID = ceid.ceIDs) > 0
BEGIN

SET @ProductInfo = @ProductInfo + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	

N'<tr>
	  <th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jobsite</th>
	  <th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Product Name</th>
	  <th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Exposure Start Date</th>
	  <th style="border: 1px solid #ffffff; font-size: 11pt; font-family: Calibri; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Exposure End Date</th>
' +
	
	
CAST ( (
SELECT	@TDStyle AS 'td/@style', (SELECT COALESCE(Jobsite, ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(ProductName, 'UNSPECIFIED')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(25), StartDate, 101), ' ')) as td, '',
		@TDStyle AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(25), EndDate, 101), ' ')) as td, ''
	FROM tblClaimantProduct cp
	INNER JOIN tblClaimantEmployment ce on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
	INNER Join tblProduct p on p.ProductID = cp.ProductID
	LEFT Join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
	LEFT Join tblProductManufacturer pm on pm.ProductManufacturerID = p.ProductManufacturerID
	LEFT Join tblProductName pn on pn.ProductNameID = p.ProductNameID
	LEFT Join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
	INNER JOIN (
				SELECT DISTINCT ceIDs
				FROM @ClaimantEmploymentIDs
				) ceid ON cp.ClaimantEmploymentID = ceid.ceIDs
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	

END
ELSE
BEGIN
	SET @ProductInfo = @ProductInfo + '<p font-family: Calibri; font-size: 11pt;>No product information available for this claimant.</p>'
END

BEGIN 
	SET @CaseInfo = @CaseInfo + '
		<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
			' 
	SET @CaseInfo = @CaseInfo + (
	SELECT (select [td/@style] = @tdStyleNoBorderBold, field as 'td' for xml path(''), type), (select [td/@style] = @tdStyleNoBorder, Information as 'td' for xml path(''), type)
	from (
	select Field, Information
	from (
			SELECT DISTINCT 
				   COALESCE(CONVERT(nvarchar(max), NULLIF(FileDate, ''), 101), '') AS [Complaint Filed Date],
				   COALESCE(CONVERT(nvarchar(max), NULLIF(a.PfizerFileDate, ''), 101), '') AS [Pfizer File Date],
				   COALESCE(CONVERT(nvarchar(max), NULLIF(a.ServiceDate, ''), 101), '') AS [Service Date],
				   COALESCE(CONVERT(nvarchar(max), NULLIF(AnswerFiled, ''), 101), '') AS [Answer Filed Date],
				   CONVERT(nvarchar(max),COALESCE(CASE WHEN AmendedComplaint > 0 THEN 'Yes' ELSE 'No' END, '')) AS [Amended Complaint Filed],
				   CONVERT(nvarchar(max), CASE WHEN DemandAmount IS NULL THEN '' ELSE '$' + REPLACE(convert(nvarchar(20), cast(DemandAmount as money), 1), '.00', '') END) AS [Demand Amount],
				   COALESCE(CONVERT(nvarchar(max), NULLIF(DemandDate, ''), 101), '') AS [Date of Demand],
				   CONVERT(nvarchar(max),COALESCE(CASE WHEN ClaimLawsuitStatusID IN ('15', '16', '17', '21', '24', '34', '35') THEN COALESCE(CONVERT(nvarchar(max), NULLIF(StatusDate, ''), 101), '') ELSE NULL END, '')) AS [Dismissed Date],
				   CONVERT(nvarchar(max), COALESCE(SettlementGroup, '')) AS [Settlement Group],
				   CONVERT(nvarchar(max), COALESCE(SettlementGroupType, '')) AS [Settlement Group Type],
				   CONVERT(nvarchar(max), COALESCE(CASE WHEN ClaimLawsuitStatusID  IN ('19', '23', '25', '28', '42') THEN COALESCE(CONVERT(nvarchar(max), NULLIF(StatusDate, ''), 101), '') ELSE '' END, '')) AS [Settlement Date],
				   CONVERT(nvarchar(max), CASE WHEN SettlementAmount IS NULL THEN '' ELSE '$' + REPLACE(convert(nvarchar(20), cast(SettlementAmount as money), 1), '.00', '') END) AS [Settlement Amount]
FROM tblClaim c	
	INNER JOIN tblClaimLawsuit cl on c.claimid = cl.ClaimID
	INNER JOIN tblLawsuit l	ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT JOIN tblClaimantSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	LEFT JOIN tblSettlementGroup sg on cs.SettlementGroupID = sg.SettlementGroupID
	LEFT JOIN tblSettlementGroupType sgt on sg.SettlementGroupTypeID = sgt.SettlementGroupTypeID
	LEFT JOIN (SELECT mcd.ClaimantPersonalInfoID, COUNT(d.DocumentID) AS AmendedComplaint
				FROM tblMultiClaimantDocument mcd	
					INNER JOIN tblDocuments d on mcd.DocumentID = d.DocumentID
				WHERE d.ClaimantDocumentTypeID = 4
				GROUP BY mcd.ClaimantPersonalInfoID) mcd on mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID 
	LEFT JOIN vAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND IsCurrentClaim = 1
	AND IsPrimaryForClaim = 1
	AND IsPrimaryStatus = 1
	) ss
	unpivot (Information FOR Field in ( [Complaint Filed Date],
										[Pfizer File Date],
										[Service Date],
										[Answer Filed Date],										
										[Amended Complaint Filed],
										[Demand Amount],
										[Date of Demand],
										[Dismissed Date],
										[Settlement Group],
										[Settlement Group Type],
										[Settlement Date],
										[Settlement Amount]
		   )) as unpv ) tbl FOR XML PATH('tr'))
	SET @CaseInfo = @CaseInfo + '
	</tr>
</table>
</p>
<br/>
<br/>'
END

--SELECT @InjuryPartySection 
--SELECT @CounselInfoSection 
--SELECT @CurrentCourtInformation  
--SELECT @PreviousCourtInformation 
--SELECT @AdditionalInjPartyInfo 
--SELECT @RelatedPartyInfo 
--SELECT @MedicareInfo 
--SELECT @SmokingHistory 
--SELECT @MedicalHistory  
--SELECT @WorkHistory  
--SELECT @ProductInfo 
--SELECT @CaseInfo


SET @Content = @Content +
			   @InjuryPartySection + 
			   @CounselInfoSection + 
			   @CurrentCourtInformation + 
			   @PreviousCourtInformation +
			   @AdditionalInjPartyInfo +
			   @RelatedPartyInfo +
			   @MedicareInfo +
			   @SmokingHistory + 
			   @MedicalHistory + 
			   @WorkHistory + 
			   @ProductInfo +
			   @CaseInfo

SET @Content = @Content + '
</body>
</html>
'

SELECT @Content AS Content



END
ELSE
	BEGIN
		SELECT 'ACCESS DENIED' AS Content
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryGetClaimantDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryGetClaimantDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryGetClaimantDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryGetClaimantDetails] TO [power_user]
GO
