SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_FIRM_qryGetFirmByEmail]
    (
      @emailaddress NVARCHAR(1000)
    )
AS 
    DECLARE @firmID INT
    SET @firmID = ( SELECT  FirmID
                    FROM    vtblPerson
                    WHERE   Email = @emailaddress
                  )
    BEGIN
	
        DECLARE @emaildomain NVARCHAR(1000)
	
        SET @emaildomain = SUBSTRING(@emailaddress,
                                     CHARINDEX('@', @emailaddress),
                                     LEN(@emailaddress) - CHARINDEX('@',
                                                              @emailaddress)
                                     + 1)
	
        IF @emaildomain = '@kcicllc.com'
		   OR @emaildomain = '@kcic.com'
            BEGIN
	
	
                SELECT DISTINCT
                        DefenseCounselID AS FirmID ,
                        DefenseCounsel AS Firm
                FROM    tblDefenseCounsel
                ORDER BY DefenseCounsel
	
            END
	
        ELSE 
            BEGIN
	
	
                SELECT DISTINCT
                        FirmID ,
                        DefenseCounsel AS Firm
                FROM    vtblPerson
                        INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
                WHERE   @emailaddress = vtblPerson.Email
                ORDER BY DefenseCounsel
	
            END
    END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmByEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmByEmail] TO [power_user]
GO
