SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[WSS_CLAIMANT_qryClaimantDOFE]
	-- Add the parameters for the stored procedure here
@ClaimantPersonalInfoID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT COALESCE(CONVERT(nvarchar(50), (CASE WHEN MIN(empstartdate) < MIN(startdate) THEN MIN(empstartdate) ELSE MIN(startdate) END), 101), 'UNKNOWN') AS DOFE
	--FROM tblClaimantEmployment ce
	--LEFT JOIN tblClaimantProduct cp on ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
	--WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
	SELECT COALESCE (CONVERT(nvarchar(50), (CASE WHEN CONVERT(nvarchar(50), COALESCE(MIN(empstartdate), MIN(startdate)), 101) IS NULL
											THEN 'UNKNOWN'
											ELSE
												(CASE WHEN COALESCE(MIN(empstartdate), Getdate()) < COALESCE(MIN(startdate), Getdate()) 
													  THEN CONVERT(nvarchar(50),MIN(empstartdate), 101) 
													  ELSE CONVERT(nvarchar(50),MIN(startdate), 101)
												 END)
											END), 101), 'UNKNOWN') AS DOFE
	FROM tblClaimantEmployment ce
	LEFT JOIN tblClaimantProduct cp on ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantDOFE] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantDOFE] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantDOFE] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantDOFE] TO [power_user]
GO
