SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_DEADLINE_qryCaseDeadlineTypeList]
AS 
    SET NOCOUNT ON

    SELECT  DeadlineTypeID ,
            DeadlineType
    FROM    tblDeadlineType
	UNION
	SELECT NULL, ''
	ORDER BY DeadlineType


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryCaseDeadlineTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryCaseDeadlineTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryCaseDeadlineTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryCaseDeadlineTypeList] TO [power_user]
GO
