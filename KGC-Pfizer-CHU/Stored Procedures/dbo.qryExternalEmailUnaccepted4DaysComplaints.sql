SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailUnaccepted4DaysComplaints]
AS 
   BEGIN

    SET nocount ON
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH

1/30/12 - CAS - Updated to run off the QC date instead of the Created Date of the document for type IDs 8,4,33 only. Others stayed the same.
*/

--Declare and set email inputs.
    DECLARE @Subject NVARCHAR(100)
    DECLARE @LogoHeader NVARCHAR(2000)


    DECLARE @FourDaysAgo DATETIME
    SET @FourDaysAgo = CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(DATE, GETDATE()
        - 4)) + ' 16:00:00.000')


    SET @Subject = 'Pfizer Claims: Unaccepted Complaints/Documents Over 4 Days Old'
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    

    SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.

    DECLARE @NewComplaintsTable TABLE
        (
          DocID INT ,
          docTitle NVARCHAR(500) ,
          FirmAddressID INT ,
          lName NVARCHAR(50) ,
          fName NVARCHAR(50) ,
          ClaimantPersonalInfoID BIGINT ,
          ServiceDate DATE ,
          PlaintiffCounsel NVARCHAR(500)
        )
    INSERT  INTO @NewComplaintsTable
            SELECT  d.DocumentID ,
                    '<a href="https://www.kcicextranet.com/clients/Pfizer/cp/Pages/DocumentProfile.aspx?ItemId='
                    + COALESCE(CONVERT(NVARCHAR(50), d.DocumentID), '') + '">'
                    + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle ,
                    DefenseCounselAddressID ,
                    cpi.LastName ,
                    cpi.FirstName ,
                    cpi.ClaimantPersonalInfoID ,
                    MAX(COALESCE(ServiceDate,'1/1/1900')) ,
                    PlaintiffCounsel
            FROM    tblDocuments d
                    INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
                    INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
                    LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
                    LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
                    LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
                    LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
            WHERE   d.DateCreated < @FourDaysAgo
                    AND d.ClaimantDocumentTypeID IN ( 8, 4, 33 )
                    AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
                    AND COALESCE(Accepted, 0) <> 1
                    AND COALESCE(IsPrimaryForClaim, 0) = 1
                    AND COALESCE(IsCurrentClaim, 0) = 1
            GROUP BY d.documentID ,
                    d.DocumentTitle ,
                    DefenseCounselAddressID ,
                    LastName ,
                    FirstName ,
                    cpi.ClaimantPersonalInfoID ,
                    PlaintiffCounsel

 
    DECLARE @NewDocumentsTable TABLE
        (
          DocID INT ,
          docTitle NVARCHAR(500) ,
          FirmAddressID INT ,
          lName NVARCHAR(50) ,
          fName NVARCHAR(50) ,
          DocType NVARCHAR(50) ,
          ClaimantPersonalInfoID BIGINT
        )
    INSERT  INTO @NewDocumentsTable
            SELECT  d.DocumentID ,
                    '<a href="https://www.kcicextranet.com/clients/Pfizer/cp/Pages/DocumentProfile.aspx?ItemId='
                    + COALESCE(CONVERT(NVARCHAR(50), d.DocumentID), '') + '">'
                    + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle ,
                    DefenseCounselAddressID ,
                    cpi.LastName ,
                    cpi.FirstName ,
                    ClaimantDocumentType ,
                    cpi.claimantpersonalinfoid
            FROM    tblDocuments d
                    INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
                    INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
                    LEFT JOIN tblClaimantDocumentType ct ON ct.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
                    LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
                    LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
                    LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
            WHERE   datecreated < @FourDaysAgo
                    AND d.ClaimantDocumentTypeID IN ( 7, 11, 23, 25, 6 )
                    AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
                    AND COALESCE(Accepted, 0) <> 1
                    AND COALESCE(IsPrimaryForClaim, 0) = 1
                    AND COALESCE(IsCurrentClaim, 0) = 1
            GROUP BY d.DocumentID ,
                    d.DocumentTitle ,
                    DefenseCounselAddressID ,
                    LastName ,
                    FirstName ,
                    ClaimantDocumentType ,
                    cpi.claimantpersonalinfoid
      
    DECLARE @ClaimantPerDoc TABLE
        (
          DocID INT ,
          ClaimantList NVARCHAR(MAX)
        )
    DECLARE @DocID INT ,
        @LastDocID INT ,
        @Claimant NVARCHAR(MAX) ,
        @ClaimantID BIGINT ,
        @ClaimantList NVARCHAR(MAX)

    DECLARE Cur_Complaints CURSOR Local Fast_Forward
    FOR
        SELECT  DocID ,
                COALESCE(Lname, '') + ', ' + COALESCE(Fname, '') ,
                claimantpersonalInfoID
        FROM    @NewComplaintsTable
        UNION
        SELECT  DocID ,
                COALESCE(Lname, '') + ', ' + COALESCE(Fname, '') ,
                claimantpersonalInfoID
        FROM    @NewDocumentsTable
    OPEN Cur_Complaints
    FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
    WHILE @@FETCH_STATUS = 0 
        BEGIN

            IF ( LEN(@ClaimantList) = 0
                 OR @ClaimantList IS NULL
               ) 
                BEGIN
                    SET @ClaimantList = '<ul style="margin:0; padding:0;"> <a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                        + CONVERT(NVARCHAR(50), @claimantID) + '">'
                        + @Claimant + '</a></ul>'
                END
            ELSE 
                BEGIN
                    SET @ClaimantList = COALESCE(@ClaimantList, '')
                        + '<ul style="margin:0; padding:0;">  <a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                        + CONVERT(NVARCHAR(50), @claimantID) + '">'
                        + COALESCE(@Claimant, '') + '</a></ul>'

                END
            SET @LastDocID = @DocID
            FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
            PRINT @@FETCH_STATUS
            IF @DocID <> @LastDocID
                OR @@FETCH_STATUS <> 0 
                BEGIN
                    INSERT  INTO @ClaimantPerDoc
                            SELECT  @LastDocID ,
                                    @ClaimantList
                    SET @ClaimantList = NULL
                END
        END
    CLOSE Cur_Complaints
    DEALLOCATE Cur_Complaints
      
      
--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

    DECLARE @FirmAddressID NVARCHAR(10) ,
        @firmURL NVARCHAR(100)

    DECLARE firm_Curs CURSOR
    FOR
        SELECT  nct.FirmAddressID ,
                'https://www.kcicextranet.com/clients/Pfizer/cp/Pages/Documents.aspx'
        FROM    @NewComplaintsTable nct
        GROUP BY nct.FirmAddressID
        UNION
        SELECT  doc.FirmAddressID ,
                'https://www.kcicextranet.com/clients/Pfizer/cp/Pages/Documents.aspx'
        FROM    @NewDocumentsTable doc
        GROUP BY doc.FirmAddressID


    OPEN firm_curs
    FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmURL
    WHILE @@FETCH_STATUS = 0 
        BEGIN

--Declare and populate the body of the email for individual firms
            DECLARE @BodyMessage NVARCHAR(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
            SET @BodyMessage = @LogoHeader + '<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH    {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
UL {margin:0; 
	padding:0;}	      
--->
</STYLE>'

            DECLARE @TDStyle NVARCHAR(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
            IF ( SELECT COUNT(docTitle)
                 FROM   @NewComplaintsTable
                 WHERE  FirmAddressID = @FirmAddressID
               ) > 0 
                BEGIN
                    SET @BodyMessage = @BodyMessage
                        + '<H4>The following complaints were uploaded more than 4 days ago and have not yet been accepted: </H4>'
                    SET @BodyMessage = @BodyMessage
                        + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">'
                        + N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Served*</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Plaintiff Counsel</th>'
                        + CAST(( SELECT '<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">'
                                        + docTitle + ' </td>' ,
                                        '' ,
                                        '<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">'
                                        + cl.ClaimantList + ' </td>' ,
                                        '' ,
                                        '<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">'
                                        + CAST(MAX(ServiceDate) AS VARCHAR(MAX))
                                        + ' </td>' ,
                                        '' ,
                                        '<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">'
                                        + PlaintiffCounsel + ' </td>' ,
                                        ''
                                 FROM   @NewComplaintsTable C
                                        INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
                                 WHERE  c.FirmAddressID = ( SELECT
                                                              @FirmAddressID
                                                          )
                                 GROUP BY docTitle ,
                                        cl.ClaimantList ,
                                        PlaintiffCounsel
                               FOR
                                 XML PATH('tr') ,
                                     TYPE
                               ) AS NVARCHAR(MAX)) + N'</table>' + N'<br/>'		
                END
            ELSE 
                BEGIN
                    SET @BodyMessage = @BodyMessage
                        + '<H4>There are currently no outstanding complaints. </H4>'
                END


            IF ( SELECT COUNT(docTitle)
                 FROM   @NewDocumentsTable
                 WHERE  FirmAddressID = @FirmAddressID
               ) > 0 
                BEGIN		
                    SET @BodyMessage = @BodyMessage
                        + '<H4>The following documents were uploaded more than 4 days ago and have not yet been accepted: </H4>'
		
                    SET @BodyMessage = @BodyMessage
                        + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">'
                        + N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th>'
                        + CAST(( SELECT @TDStyle AS 'td/@style' ,
                                        ( SELECT    docTitle
                                        ) AS td ,
                                        '' ,
                                        @TDStyle AS 'td/@style' ,
                                        ( SELECT    cl.ClaimantList
                                        ) AS td ,
                                        '' ,
                                        @TDStyle AS 'td/@style' ,
                                        ( SELECT    DocType
                                        ) AS td ,
                                        ''
                                 FROM   @NewDocumentsTable d
                                        INNER JOIN @ClaimantPerDoc cl ON d.DocID = cl.DocID
                                 WHERE  FirmAddressID = ( SELECT
                                                              @FirmAddressID
                                                        )
                                 GROUP BY docTitle ,
                                        cl.ClaimantList ,
                                        DocType
                               FOR
                                 XML PATH('tr') ,
                                     TYPE
                               ) AS NVARCHAR(MAX)) + N'</table>' + N'<br/>'				
                END
            ELSE 
                BEGIN
                    SET @BodyMessage = @BodyMessage
                        + '<H4>There are currently no other documents awaiting acceptance.</H4>'
                END

--Undo the HTML stripping from the xml path function above so embedded tags work
            SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'),
                                       '&gt;', '>')

----Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

--KCIC address and disclaimer
            SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

  
            DECLARE @RecipientList NVARCHAR(500)
            SET @RecipientList = ''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
            DECLARE @Contact NVARCHAR(500)
            DECLARE contact_curs CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY
            FOR
                SELECT  email
                FROM    vtblPerson p
                WHERE   FirmAddressID = @FirmAddressID
                        AND complaintContact = 1
                        AND p.status = 1
 
      

      
            OPEN contact_curs
            FETCH NEXT FROM contact_curs INTO @contact
            WHILE @@FETCH_STATUS = 0 
                BEGIN 

                    SET @RecipientList = @RecipientList + @Contact + '; '

                    FETCH NEXT FROM contact_curs INTO @contact
                END
            CLOSE contact_curs
            DEALLOCATE contact_curs


--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail @profile_name = 'Admin',
@recipients = 'owenss@kcic.com',--@RecipientList, 
@copy_recipients = '',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

--Move to the next firm to create a new email
            FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmurl
        END
    CLOSE firm_curs
    DEALLOCATE firm_curs

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GO
