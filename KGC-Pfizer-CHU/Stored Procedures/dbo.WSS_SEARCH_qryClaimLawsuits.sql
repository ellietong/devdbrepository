SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryClaimLawsuits]
    (
      @StateID INT,
      @JurisdictionID INT,
      @ClaimLawsuitStatusGroupID INT,
      @ClaimantName NVARCHAR(MAX),
	  @SSN NVARCHAR(20),
	  @MatterID INT,
      --@emailaddress NVARCHAR(250),
	  @ClaimSequenceID INT,
	  @ClaimTypeValueID INT,
      @ClaimantIDs integer_list_tbltype READONLY
    )
AS 
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

----The following set of variables can be used for testing.
--DECLARE @ClaimantName              nvarchar(max)
--DECLARE @StateID                   int
--DECLARE @JurisdictionID			   int 
--DECLARE @ClaimTypeValueID		   int
--DECLARE @emailaddress              nvarchar(250)
--DECLARE @ClaimLawsuitStatusID	   int
--DECLARE @TrialGroupID			   int
--DECLARE @ClaimSequenceID		   int
--DECLARE @ClaimantIDs               TABLE (n int) 

--SET @ClaimantName              = NULL
--SET @StateID                   = NULL
--SET @JurisdictionID			   = NULL
--SET @ClaimTypeValueID		   = NULL
--SET @emailaddress              = 'owenss@kcic.com'
--SET @ClaimLawsuitStatusID	   = NULL
--SET @TrialGroupID			   = NULL
--SET @ClaimSequenceID		   = NULL
--INSERT INTO @ClaimantIDs
--SELECT 958175

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
    IF OBJECT_ID('tempdb..#ClaimantIDs') IS NOT NULL 
        BEGIN
            DROP TABLE #ClaimantIDs
        END
    CREATE TABLE #ClaimantIDs ( n INT )
    INSERT  INTO #ClaimantIDs
            SELECT  n
            FROM    @ClaimantIDs

DECLARE @cmd NVARCHAR(MAX)
SET @cmd = '
	SELECT DISTINCT CL.ClaimLawsuitID, 
	CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '''' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE + '' '' + MiddleName END AS CombinedName, 
	CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SocialSecurityNumber,
	CaseCaption, 
	s.State, 
	ClaimLawsuitStatus, 
	Jurisdiction, 
	cl.ActiveTrialDate, 
	DocketNumber, 
	TrialGroup, 
	c.ClaimantPersonalInfoID,
	CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
	Matter,
	dbo.fnClaimTypesByClaimID(c.ClaimID) AS ClaimType,
	ClaimSequence,
	ClaimLawsuitStatus, 
	cpi.ClaimantPersonalInfoID AS ClaimantID
	FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	INNER JOIN tblState s ON l.StateID = s.StateID
	INNER JOIN tblJurisdiction j ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblTrialGroup tg ON cl.TrialGroupID = tg.TrialGroupID
	LEFT JOIN tblClaimType ct	ON c.ClaimID = ct.ClaimID
	LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
	LEFT JOIN tblClaimSequence csq ON c.ClaimSequenceID = csq.ClaimSequenceID
	WHERE COALESCE(IsPrimaryForClaim, 0) = 1 AND COALESCE(IsCurrentClaim, 0) = 1 AND IsPrimaryStatus = 1' 
        IF ( @StateID IS NOT NULL ) 
            SET @cmd = @cmd + ' AND 	l.StateID = @StateID'
	
        IF ( @JurisdictionID IS NOT NULL ) 
            SET @cmd = @cmd + ' AND l.JurisdictionID = @JurisdictionID'
	
        IF ( @ClaimLawsuitStatusGroupID IS NOT NULL ) 
            SET @cmd = @cmd
                + ' AND cls.ClaimLawsuitStatusGroupID = @ClaimLawsuitStatusGroupID'

    IF @ClaimantName IS NOT NULL 
        BEGIN
            SET @cmd = @cmd
                + ' AND ((CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' end) like ''%'' + @ClaimantNameParam + ''%'')'
        END

    IF @ClaimSequenceID IS NOT NULL 
        BEGIN
            SET @cmd = @cmd
                + ' AND c.ClaimSequenceID =  @ClaimSequenceID'
        END
    IF @ClaimTypeValueID IS NOT NULL 
        BEGIN
            SET @cmd = @cmd + ' AND ct.ClaimTypeValueID =  @ClaimTypeValueID'
        END
    IF ( SELECT COUNT(n)
         FROM   #ClaimantIDs
       ) <> 0 
        BEGIN
            SET @cmd = @cmd
                + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)'
        END

    IF @MatterID IS NOT NULL 
        BEGIN
            SET @cmd = @cmd 
			+ ' AND c.MatterID = @MatterID'
        END

    IF @SSN IS NOT NULL 
        BEGIN
            SET @cmd = @cmd
                + ' AND COALESCE(socialsecurityNumber, '''') like ''%'' + @SSNParam + ''%'''
        END
	
	EXEC sp_executesql @cmd,
		N'@JurisdictionID INT, @StateID INT, @ClaimLawsuitStatusGroupID INT, @ClaimantNameParam NVARCHAR(MAX), @ClaimSequenceID INT, @ClaimTypeValueID INT, @MatterID INT, @SSNParam NVARCHAR(20)',
		@JurisdictionID = @JurisdictionID,
		@StateID = @StateID,
		@ClaimLawsuitStatusGroupID = @ClaimLawsuitStatusGroupID,
		@ClaimantNameParam = @ClaimantName, 
		@ClaimSequenceID = @ClaimSequenceID,
		@ClaimTypeValueID = @ClaimTypeValueID,
		@MatterID = @MatterID,
		@SSNParam = @SSN

SELECT @cmd
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryClaimLawsuits] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryClaimLawsuits] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryClaimLawsuits] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryClaimLawsuits] TO [power_user]
GO
