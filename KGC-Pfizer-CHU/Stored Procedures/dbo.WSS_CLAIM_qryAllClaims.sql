SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_CLAIM_qryAllClaims]
    (
      @ClaimantPersonalInfoID BIGINT = NULL ,
      @emailaddress NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantpersonalinfoID int
--DECLARE @emailaddress nvarchar(250)
--SET @ClaimantpersonalinfoID = 250962
--SET @emailaddress = 'bpassmore@powersfrost.com'
--SET @emailaddress = 'owenss@kcic.com'

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.


    DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE cpi.ClaimantPersonalInfoID = '
        + CONVERT(NVARCHAR(20), @ClaimantPersonalInfoID)


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    DECLARE @JoinString NVARCHAR(MAX)
    IF ( ( RIGHT(@emailaddress, 11) = 'kcicllc.com' )
		   OR ( RIGHT(@emailaddress, 8) = 'kcic.com' )	
           OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        BEGIN
            SET @JoinString = ''
            SET @WhereString = @WhereString + ' AND cl.IsPrimaryForClaim = 1'
        END
    ELSE 
        BEGIN
            SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
            SET @WhereString = @WhereString
                + ' AND (dj.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ' OR l.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ')'
        END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
    DECLARE @QueryString NVARCHAR(MAX)
    SET @QueryString = 'SELECT DISTINCT
        cpi.ClaimantPersonalInfoID, 
		c.ClaimID, 
		CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-'' + RIGHT(SocialSecurityNumber, 4) END AS SSN,
	    cls.ClaimLawsuitStatus, 
	    hddis.HighLevelDisease AS Disease, 
	    Matter,
        CASE WHEN IsMDL = 0 THEN ''No'' WHEN IsMDL = 1 THEN ''Yes'' ELSE ''Unknown'' END AS IsMDL, 
        CASE WHEN IsThirdPartyComplaint = 0 THEN ''No'' WHEN IsThirdPartyComplaint = 1 THEN ''Yes'' ELSE ''Unknown'' END AS IsThirdPartyComplaint, 
        claimsett.SettlementGroupID,
        SettlementGroup, 
        SettlementDueDate,
        ClaimLawsuitStatus as ResolutionStatus,
        CASE WHEN ClaimLawsuitStatusGroupID IN(1, 2, 6) THEN StatusDate 
														ELSE NULL END AS ResolutionDate,
		clst.processeddate,
		ClaimSequence,
		c.ClaimSequenceID,
		SettlementGroupType
	 FROM tblClaimantPersonalInfo cpi 
		INNER JOIN tblClaim c						ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl				ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l						ON cl.LawsuitID = l.LawsuitID '
        + @JoinString
        + ' 
		LEFT JOIN tblMatter m						ON m.MatterID = c.MatterID
		LEFT JOIN (SELECT * 
				   FROM tblClaimDisease 
				   WHERE PrimaryDisease = 1) cd		ON cd.ClaimID = c.ClaimID
		LEFT JOIN tblDisease ddis					ON ddis.DiseaseID = cd.DiseaseID
		LEFT JOIN tblHighLevelDisease hddis			ON hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID
		LEFT JOIN (SELECT * 
				   FROM tblClaimLawsuitStatusTracking 
				   WHERE IsPrimaryStatus = 1) clst  ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls			ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN tblClaimantSettlement claimsett	ON claimsett.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblSettlePaymentDocumentation spd	ON spd.ClaimLawsuitID = cl.ClaimLawsuitID 
		LEFT JOIN tblReleaseType rt					ON rt.ReleaseTypeID = spd.ReleaseTypeID
		LEFT JOIN tblSettlementGroup settg			ON settg.SettlementGroupID = claimsett.SettlementGroupID
		LEFT JOIN tblClaimSequence csq				ON c.ClaimSequenceID = csq.ClaimSequenceID
		LEFT JOIN tblSettlementGroupType sgt		ON settg.SettlementGroupTypeID = sgt.SettlementGroupTypeID
		'
        + @WhereString + '
	 OPTION (RECOMPILE)'
    PRINT @querystring
--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryAllClaims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryAllClaims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryAllClaims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryAllClaims] TO [power_user]
GO
