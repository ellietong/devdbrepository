SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmBroker]
As

SET NOCOUNT ON

SELECT BrokerID, Broker
FROM tblBroker
ORDER BY Broker

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBroker] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmBroker] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBroker] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmBroker] TO [power_user]
GO
