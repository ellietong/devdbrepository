SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_DISMISSAL_qryDismissalDocs]
(@CaseID int = 0)
As
Set NoCount On

DECLARE @Bucket nvarchar(max) = (SELECT MAX(Bucket) FROM [KGA-ClaimsAdmin].dbo.tblCase WHERE CaseID = @CaseID)

DECLARE @ConnectionString nvarchar(max)
SET @ConnectionString = '[' + (SELECT MAX(ConnectionStringKey) FROM tblCase WHERE CaseID = @CaseID) + '].[DBO].'

DECLARE @DocTypeID nvarchar(50) = 'ClaimantDocumentTypeID'
IF @CaseID < 35 BEGIN SET @DocTypeID = 'DetailDocumentTypeID' END

DECLARE @DivisorString nvarchar(5) = '10'
IF @CaseID = 35 OR @CaseID = 36 BEGIN SET @DivisorString = '1' END

DECLARE @DisableString nvarchar(max) = ''
DECLARE @EnableString nvarchar(max) = ''
IF @CaseID = 29
BEGIN
	SET @DisableString	= 'ALTER TABLE ' + @ConnectionString + 'tblDocuments DISABLE TRIGGER TRIG_tblDocuments_Upd_Type'
	SET @EnableString	= 'ALTER TABLE ' + @ConnectionString + 'tblDocuments ENABLE TRIGGER TRIG_tblDocuments_Upd_Type'
END

DECLARE @QueryString nvarchar(max)
SET @QueryString = '

DECLARE @Temp TABLE (DocumentID int, ForQC bit, TempID int Primary Key Identity)
INSERT INTO @Temp (DocumentID, ForQC)
SELECT d.DocumentID, NULL as ForQC
FROM ' + @ConnectionString + 'tblDocuments d
	inner join ' + @ConnectionString + 'tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
	inner join ' + @ConnectionString + 'tblClaim c on c.ClaimantPersonalInfoID=mcd.ClaimantPersonalInfoID
	inner join ' + @ConnectionString + 'tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
	inner join ' + @ConnectionString + 'tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID

WHERE
	(clst.ClaimLawsuitStatusID is not null or clst.ProcessedDate is not null) 
	AND (d.QCDate IS NULL) AND (d.' + @DocTypeID + ' = 16) and d.CreatedBy not like ''%Historical%'' 
	AND d.DateCreated > ''6/11/2012'' --When new Dismissal Upload was rolled out to Dana, IUNA, MSA
	AND d.ForQC IS NULL
GROUP BY
	d.DocumentID, d.ForQC, comments, d.' + @DocTypeID + ', DocumentURL,
	DocumentTitle, d.QCDate, d.QCBy,  d.CreatedBy, AmazonKey


DECLARE @RowsForQC int
IF (SELECT COUNT(*) from @Temp) >= ' + @DivisorString + ' BEGIN SET @RowsForQC = (SELECT COUNT(*)/' + @DivisorString + ' from @Temp) END
								ELSE BEGIN SET @RowsForQC = 0 END

UPDATE @Temp
SET ForQC = CASE WHEN TempID <= @RowsForQC THEN 1
				 ELSE 0 END

IF @RowsForQC > 0
BEGIN
	' + @DisableString + '
	UPDATE ' + @ConnectionString + 'tblDocuments
			SET ForQC = s.ForQC
		FROM (
			SELECT	d.DocumentID,
					CASE WHEN d.ForQC = 1 THEN 1
						 WHEN t.TempID > @RowsForQC * ' + @DivisorString + ' THEN NULL
						 WHEN t.ForQC IS NULL THEN 0
					ELSE t.ForQC END as ForQC
			FROM ' + @ConnectionString + 'tblDocuments d
			LEFT JOIN @Temp t ON d.DocumentID = t.DocumentID) s
		WHERE ' + @ConnectionString + 'tblDocuments.DocumentID = s.DocumentID
END
' + @EnableString + '

SELECT	d.DocumentID,
		d.AmazonKey,
		d.Comments,
		d.CreatedBy,
		d.DocumentTitle,
		CASE	WHEN Issues IS NULL or Issues = 0 THEN ''No''
				WHEN Issues = 1 THEN ''Yes'' END
		as Issues,
		@BucketParam as Bucket,
		MAX(COALESCE(LastName + '', '' + FirstName + '' '' + MiddleName, LastName + '', '' + FirstName, LastName)) as ClaimantName
FROM ' + @ConnectionString + 'tblDocuments d
	LEFT JOIN ' + @ConnectionString + 'tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
	LEFT JOIN ' + @ConnectionString + 'tblClaimantPersonalInfo cpi on mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE ForQC = 1 AND d.QCDate IS NULL AND d.QCBy IS NULL
GROUP BY d.DocumentID, d.AmazonKey, d.Comments, d.CreatedBy, d.DocumentTitle, Issues
'

Print @QueryString
EXEC sp_executesql @QueryString, N'@BucketParam nvarchar(max)',
@BucketParam = @Bucket

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_qryDismissalDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_qryDismissalDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_qryDismissalDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_qryDismissalDocs] TO [power_user]
GO
