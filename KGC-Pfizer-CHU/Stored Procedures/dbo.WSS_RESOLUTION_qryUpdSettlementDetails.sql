SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_RESOLUTION_qryUpdSettlementDetails]
(@ClaimantSettlementID bigint, @DemandDate datetime, @DemandAmount money)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE tblClaimantSettlement
	SET DemandDate = NULLIF(@DemandDate, ''),
		DemandAmount = NULLIF(@DemandAmount, '')
	WHERE tblClaimantSettlement.ClaimantSettlementID=@ClaimantSettlementID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryUpdSettlementDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryUpdSettlementDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryUpdSettlementDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryUpdSettlementDetails] TO [power_user]
GO
