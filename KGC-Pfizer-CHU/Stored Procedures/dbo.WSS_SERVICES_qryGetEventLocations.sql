SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[WSS_SERVICES_qryGetEventLocations]
(@PrefixText nvarchar(200))
AS
BEGIN
SELECT EventLocationID, 
(COALESCE(LocationName + ': ', '') + COALESCE(Address1 + ' ', '') + COALESCE(Address2 + ' ', '') + COALESCE(City, '') + ', '+ COALESCE(State + ' ', '')  + COALESCE(Zip, '')) AS EventLocation
FROM tblEventLocation el
	INNER JOIN tblState s on el.StateID = s.StateID
WHERE (COALESCE(LocationName, '') + COALESCE(Address1, '') + COALESCE(Address2, '') + COALESCE(City, '') + COALESCE(State, '')  + ', ' + COALESCE(Zip, '')) LIKE '%' + @PrefixText + '%'

END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetEventLocations] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetEventLocations] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetEventLocations] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetEventLocations] TO [power_user]
GO
