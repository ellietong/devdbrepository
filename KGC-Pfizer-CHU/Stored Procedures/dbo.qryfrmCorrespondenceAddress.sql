SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmCorrespondenceAddress]
(@CorrespondenceID int)
AS
SET NOCOUNT ON

SELECT ca.CorrespondenceID, AddressID, mRecipientTypeID
FROM tblCorrespondenceAddress ca
WHERE CorrespondenceID = @CorrespondenceID

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorrespondenceAddress] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorrespondenceAddress] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorrespondenceAddress] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorrespondenceAddress] TO [power_user]
GO
