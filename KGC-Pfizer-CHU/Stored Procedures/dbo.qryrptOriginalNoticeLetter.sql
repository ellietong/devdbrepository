SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[qryrptOriginalNoticeLetter]
(@CorrespondenceID int)
AS
SET NOCOUNT ON

SELECT   c.CorrespondenceID,r.NoticeResponseID
FROM     tblCorrespondence c LEFT OUTER JOIN
tblResponse r ON c.CorrespondenceID = r.CorrespondenceID

WHERE @CorrespondenceID = c.CorrespondenceID
ORDER BY c.CorrespondenceID, r.NoticeResponseID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOriginalNoticeLetter] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOriginalNoticeLetter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOriginalNoticeLetter] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOriginalNoticeLetter] TO [power_user]
GO
