SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryMultiClaimantSearchMultiIDs]
	-- Add the parameters for the stored procedure here
(
@ClaimantNames					nvarchar_list_tbltype READONLY, 
@SSNs							nvarchar_list_tbltype READONLY, 
@DocketNumbers					nvarchar_list_tbltype READONLY, 
@StateIDs						integer_list_tbltype READONLY, 
@PlaintiffCounselIDs			integer_list_tbltype READONLY, 
@ClaimLawsuitStatusGroupIDs		integer_list_tbltype READONLY, 
@SettlementGroupTypeIDs			integer_list_tbltype READONLY,
@ClaimantIDs					integer_list_tbltype READONLY,
@MatterIDs						integer_list_tbltype READONLY, 
@ClaimSequenceIDs				integer_list_tbltype READONLY,
@ClaimTypeIDs					integer_list_tbltype READONLY,
@InjuredPartyIDs					bigint_list_tbltype READONLY,
@emailaddress					nvarchar(250), 
@OrderByString					nvarchar(MAX),
@RowStart						bigint,
@RowCount						bigint
)
AS
BEGIN
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE
--@ClaimantNames			nvarchar_list_tbltype, 
--@SSNs						nvarchar_list_tbltype, 
--@DocketNumbers			nvarchar_list_tbltype , 
--@StateIDs					integer_list_tbltype , 
--@PlaintiffCounselIDs		integer_list_tbltype , 
--@ClaimLawsuitStatusGroupIDs	integer_list_tbltype , 
--@SettlementGroupTypeIDs	integer_list_tbltype ,
--@ClaimantIDs				integer_list_tbltype ,
--@MatterIDs				integer_list_tbltype , 
--@ClaimSequenceIDs			integer_list_tbltype ,
--@ClaimTypeIDs				integer_list_tbltype ,
--@InjuredPartyIDs				bigint_list_tbltype ,
--@emailaddress				nvarchar(250) = 'owenss@kcic.com',
--@OrderByString			nvarchar(MAX) = 'FullName',--'cpi.ClaimantPersonalInfoID',
--@RowStart				bigint = 1,
--@RowCount				bigint = 15

--INSERT INTO @StateIDs 
--SELECT 30--52
--INSERT INTO @ClaimLawsuitStatusGroupIDs
--SELECT 3
--INSERT INTO @SettlementGroupTypeIDs
--SELECT 5
--INSERT INTO @ClaimSequenceIDs
--SELECT 1
--INSERT INTO @ClaimTypeIDs
--SELECT 1

--INSERT INTO @ClaimantNames 
--SELECT 1, 'Smith'
--INSERT INTO @DocketNumbers
--SELECT 1, '99906127'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
	
IF object_id('tempdb..#ClaimantNames') IS NOT NULL
BEGIN
   DROP TABLE #ClaimantNames
END
CREATE TABLE #ClaimantNames (id int NOT NULL IDENTITY(1,1) PRIMARY KEY, cn nvarchar(max))
INSERT INTO #ClaimantNames
	SELECT n FROM @ClaimantNames	
	
IF object_id('tempdb..#SSNs') IS NOT NULL
BEGIN
   DROP TABLE #SSNS
END
CREATE TABLE #SSNS (id int NOT NULL IDENTITY(1,1) PRIMARY KEY, ssn nvarchar(20))
INSERT INTO #SSNS
	SELECT n FROM @SSNs

IF object_id('tempdb..#StateIDs') IS NOT NULL
BEGIN
   DROP TABLE #StateIDs
END
CREATE TABLE #StateIDs (s int)
INSERT INTO #StateIDs
	SELECT DISTINCT n FROM @StateIDs
	
IF object_id('tempdb..#PlaintiffCounselIDs') IS NOT NULL
BEGIN
   DROP TABLE #PlaintiffCounselIDs
END
CREATE TABLE #PlaintiffCounselIDs (pca int)
INSERT INTO #PlaintiffCounselIDs
	SELECT n FROM @PlaintiffCounselIDs
	
IF object_id('tempdb..#InjPartyIDs') IS NOT NULL
BEGIN
   DROP TABLE #InjPartyIDs
END
CREATE TABLE #InjPartyIDs (nl bigint)
INSERT INTO #InjPartyIDs
	SELECT n FROM @InjuredPartyIDs
	
IF object_id('tempdb..#MatterIDs') IS NOT NULL
BEGIN
   DROP TABLE #MatterIDs
END
CREATE TABLE #MatterIDs (m int)
INSERT INTO #MatterIDs
	SELECT n FROM @MatterIDs

IF object_id('tempdb..#ClaimSequenceIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimSequenceIDs
END
CREATE TABLE #ClaimSequenceIDs (csq int)
INSERT INTO #ClaimSequenceIDs
	SELECT n FROM @ClaimSequenceIDs

IF object_id('tempdb..#ClaimTypeIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimTypeIDs
END
CREATE TABLE #ClaimTypeIDs (ct int)
INSERT INTO #ClaimTypeIDs
	SELECT n FROM @ClaimTypeIDs

IF object_id('tempdb..#SettlementGroupTypeIDs') IS NOT NULL
BEGIN
   DROP TABLE #SettlementGroupTypeIDs
END
CREATE TABLE #SettlementGroupTypeIDs (sgt int)
INSERT INTO #SettlementGroupTypeIDs
	SELECT n FROM @SettlementGroupTypeIDs
	
--IF object_id('tempdb..#IsMDL') IS NOT NULL
--BEGIN
--   DROP TABLE #IsMDL
--END
--CREATE TABLE #IsMDL (mdl int)
--INSERT INTO #IsMDL
--	SELECT n FROM @IsMDL	
	
IF object_id('tempdb..#DocketNumbers') IS NOT NULL
BEGIN
   DROP TABLE #DocketNumbers
END
CREATE TABLE #DocketNumbers (id int NOT NULL IDENTITY(1,1) PRIMARY KEY, dn nvarchar(250))
INSERT INTO #DocketNumbers
	SELECT n FROM @DocketNumbers
	
IF object_id('tempdb..#ClaimLawsuitStatusGroupIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimLawsuitStatusGroupIDs
END
CREATE TABLE #ClaimLawsuitStatusGroupIDs (cls int)
INSERT INTO #ClaimLawsuitStatusGroupIDs
	SELECT n FROM @ClaimLawsuitStatusGroupIDs

IF object_id('tempdb..#ClaimantIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimantIDs
END
CREATE TABLE #ClaimantIDs (cid int)
INSERT INTO #ClaimantIDs
	SELECT n FROM @ClaimantIDs		
	
	
		
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE 1=1'
IF (SELECT COUNT(cn) FROM #ClaimantNames) <> 0 
BEGIN 
				SET @WhereString = @WhereString + ' AND ('


			DECLARE curs_ClaimantName Cursor --- We need to name It!
			FOR --Rows we want to loop through
			SELECT id, cn FROM #ClaimantNames 
			--added a row number, too, since my temp table doesn't come with an autonumber field like nvarchar_tbl_type


			Open curs_ClaimantName
			--declare variables to hold data from each column of the current row we're looping through
			DECLARE @ClaimantName nvarchar(MAX)
			DECLARE @RowNum int
			--populate vars with data from first row
			Fetch NEXT FROM curs_ClaimantName INTO @rowNum, @ClaimantName
			While (@@FETCH_STATUS <> -1) --Loop until there is no data left!
			BEGIN
				--check our "auto number" field and see if there should be an OR in front of the statement we're about to add
				IF(@RowNum > 1)
					SET @WhereString = @WhereString + ' OR '
				SET @WhereString = @WhereString + 'CONTAINS(FullName, ''"' + @ClaimantName + '"'')'

				--Get data from the next row in the table
				FETCH NEXT FROM curs_ClaimantName INTO @rowNUm, @claimantname
			END
			--this stuff is important to free up memory after you're finished with the cursor
			CLOSE curs_ClaimantName
			DEALLOCATE curs_ClaimantName
			--add the closing paren
			SET @WhereString = @WhereString + ')' 
END

IF (SELECT COUNT(ssn) FROM #SSNS) <> 0 
BEGIN 
				SET @WhereString = @WhereString + ' AND ('


			DECLARE curs_SSN Cursor --- We need to name It!
			FOR --Rows we want to loop through
			SELECT id, ssn FROM #SSNS 
			--added a row number, too, since my temp table doesn't come with an autonumber field like nvarchar_tbl_type


			Open curs_SSN
			--declare variables to hold data from each column of the current row we're looping through
			DECLARE @SSN nvarchar(MAX)
			DECLARE @SSNid int
			--populate vars with data from first row
			Fetch NEXT FROM curs_SSN INTO @SSNid, @SSN
			While (@@FETCH_STATUS <> -1) --Loop until there is no data left!
			BEGIN
				--check our "auto number" field and see if there should be an OR in front of the statement we're about to add
				IF(@SSNid > 1)
					SET @WhereString = @WhereString + ' OR '
				SET @WhereString = @WhereString + ' socialsecurityNumber LIKE ''' + @SSN + '''' --'CONTAINS(socialsecurityNumber, ''"' + @SSN + '"'')'

				--Get data from the next row in the table
				FETCH NEXT FROM curs_SSN INTO @SSNid, @SSN
			END
			--this stuff is important to free up memory after you're finished with the cursor
			CLOSE curs_SSN
			DEALLOCATE curs_SSN
			--add the closing paren
			SET @WhereString = @WhereString + ')' 
END

IF (SELECT COUNT(dn) FROM #DocketNumbers) <> 0 
BEGIN 
				SET @WhereString = @WhereString + ' AND ('


			DECLARE curs_DocketNumber Cursor --- We need to name It!
			FOR --Rows we want to loop through
			SELECT id, dn FROM #DocketNumbers 
			--added a row number, too, since my temp table doesn't come with an autonumber field like nvarchar_tbl_type


			Open curs_DocketNumber
			--declare variables to hold data from each column of the current row we're looping through
			DECLARE @DN nvarchar(MAX)
			DECLARE @DNid int
			--populate vars with data from first row
			Fetch NEXT FROM curs_DocketNumber INTO @DNid, @DN
			While (@@FETCH_STATUS <> -1) --Loop until there is no data left!
			BEGIN
				--check our "auto number" field and see if there should be an OR in front of the statement we're about to add
				IF(@DNid > 1)
					SET @WhereString = @WhereString + ' OR '
				SET @WhereString = @WhereString + ' DocketNumber LIKE ''' + @DN + '''' --'CONTAINS(DocketNumber, ''"' + @DN + '"'')'

				--Get data from the next row in the table
				FETCH NEXT FROM curs_DocketNumber INTO @DNid, @DN
			END
			--this stuff is important to free up memory after you're finished with the cursor
			CLOSE curs_DocketNumber
			DEALLOCATE curs_DocketNumber
			--add the closing paren
			SET @WhereString = @WhereString + ')' 
END
IF (SELECT COUNT(nl) FROM #InjPartyIDs) <> 0 BEGIN SET @WhereString = @WhereString + ' AND COALESCE(cpi.INJURED_PARTY_ID,''0'') IN(SELECT nl FROM #InjPartyIDs)' END
--IF (SELECT COUNT(mdl) FROM #IsMDL) <> 0 BEGIN SET @WhereString = @WhereString + ' AND COALESCE(IsMDL, 0) IN(SELECT mdl FROM #IsMDL)' END
IF (SELECT COUNT(cid) FROM #ClaimantIDs) <> 0 BEGIN SET @WhereString = @WhereString + ' AND cpi.ClaimantPersonalInfoID IN(SELECT cid FROM #ClaimantIDs)' END
--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID 
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

IF (SELECT COUNT(s) FROM #StateIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #StateIDs si ON l.StateID = si.s' END
IF (SELECT COUNT(pca) FROM #PlaintiffCounselIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #PlaintiffCounselIDs pc ON pca.PlaintiffCounselID = pc.pca' END
IF (SELECT COUNT(m) FROM #MatterIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #MatterIDs mid ON c.MatterID = mid.m' END
IF (SELECT COUNT(cls) FROM #ClaimLawsuitStatusGroupIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #ClaimLawsuitStatusGroupIDs clsid ON cls.ClaimLawsuitStatusGroupID = clsid.cls' END
IF (SELECT COUNT(sgt) FROM #SettlementGroupTypeIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #SettlementGroupTypeIDs sgti ON sg.SettlementGroupTypeID = sgti.sgt ' END
IF (SELECT COUNT(csq) FROM #ClaimSequenceIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #ClaimSequenceIDs csqi ON c.ClaimSequenceID = csqi.csq' END
IF (SELECT COUNT(ct) FROM #ClaimTypeIDs) <> 0 BEGIN SET @JoinString = @JoinString + ' INNER JOIN #ClaimTypeIDs cti ON ct.ClaimTypeValueID = cti.ct' END



--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'DECLARE @totalRows bigint
	SELECT @totalRows = COUNT(DISTINCT cpi.ClaimantpersonalinfoID) 
	FROM dbo.tblClaimantPersonalInfo cpi
		INNER JOIN dbo.tblClaim c				 ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl			 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l					 ON cl.LawsuitID = l.LawsuitID ' + 
		'LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls		 ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN tblClaimLawsuitStatusGroup               clsg ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID
		--LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		LEFT JOIN tblPlaintiffCounselAddress pca		ON pca.PlaintiffCounselAddressID = l.PrimaryNationalPlaintiffCounselID
		LEFT JOIN tblState s					 ON l.StateID = s.StateID
		LEFT JOIN tblClaimSequence							csq ON c.ClaimSequenceID = csq.ClaimSequenceID
		LEFT JOIN tblClaimantSettlement						cs	ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		LEFT JOIN tblSettlementGroup						sg	ON cs.SettlementGroupID = sg.SettlementGroupID
		LEFT JOIN tblSettlementGroupType					sgt ON sg.SettlementGroupTypeID = sgt.SettlementGroupTypeID	
		LEFT JOIN tblMatter									m	ON c.MatterID = m.MatterID		
		LEFT JOIN tblClaimType								ct	ON c.ClaimID = ct.ClaimID
		LEFT JOIN (SELECT DISTINCT c.ClaimID,
					(SELECT  DISTINCT '''' + ClaimTypeValue + '''' + ''| '' AS [text()] 
					FROM    tblClaimType CT
						INNER JOIN tblClaimTypeValue CTV ON CT.ClaimTypeValueID = CTV.ClaimTypeValueID	
					WHERE ct.ClaimID = c.ClaimID 
					FOR XML PATH('''')) AS ClaimTypeValue
					FROM tblClaim c) ctv ON c.ClaimID = ctv.ClaimID'+
				@JoinString+
	 @WhereString + '
	SET ROWCOUNT ' + CASE WHEN (@RowCount < 0) THEN CONVERT(NVARCHAR(100), 0) ELSE CONVERT(nvarchar, @RowCount) END+'
	 SELECT DISTINCT ClaimantPersonalInfoID, 
			FullName, 
			SocialSecurityNumber, 
			DocketNumber, 
			s.State, 
			SettlementGroupType,
			PlaintiffCounsel AS ''Plaintiff Counsel'', 
			Matter, 
			ClaimSequence,
			REPLACE(ClaimTypeValue, ''|'', ''<br/>''), 
			ClaimLawsuitStatus, 

			@totalrows as TotalRows FROM (
	
		SELECT ROW_NUMBER() OVER (ORDER BY ' + @OrderByString + ') AS RowNum,
		CPI.ClaimantPersonalInfoID, 
		REPLACE(FullName, ''"'', ''\"'') AS FullName, 
		CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SocialSecurityNumber, 
		Coalesce(PlaintiffCounsel, ''Unknown'') /*+ '' ('' + Coalesce(pca.PlaintiffCounselCity, ''Unknown'') + '')''*/ As PlaintiffCounsel,
		INJURED_PARTY_ID AS NavInjPartyID, 
		s.[State], 
		IsMDL, 
		ClaimLawsuitStatus, 
		DocketNumber,
		--cl.ClaimLawsuitID,
		cpi.LastName,
		pca.PlaintiffCounselID,
		ClaimSequence,
		SettlementGroupType,
		Matter,
		ClaimTypeValue
	FROM dbo.tblClaimantPersonalInfo cpi
		INNER JOIN dbo.tblClaim c				 ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl			 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l					 ON cl.LawsuitID = l.LawsuitID ' + 
		'LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls		 ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN tblClaimLawsuitStatusGroup               clsg ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID
		--LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		LEFT JOIN tblPlaintiffCounselAddress pca		ON pca.PlaintiffCounselAddressID = l.PrimaryNationalPlaintiffCounselID
		LEFT JOIN tblState s					 ON l.StateID = s.StateID
		LEFT JOIN tblClaimSequence							csq ON c.ClaimSequenceID = csq.ClaimSequenceID
		LEFT JOIN tblClaimantSettlement						cs	ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		LEFT JOIN tblSettlementGroup						sg	ON cs.SettlementGroupID = sg.SettlementGroupID
		LEFT JOIN tblSettlementGroupType					sgt ON sg.SettlementGroupTypeID = sgt.SettlementGroupTypeID	
		LEFT JOIN tblMatter									m	ON c.MatterID = m.MatterID		
		LEFT JOIN tblClaimType								ct	ON c.ClaimID = ct.ClaimID
		LEFT JOIN (SELECT DISTINCT c.ClaimID,
					(SELECT  DISTINCT '''' + ClaimTypeValue + '''' + ''| '' AS [text()] 
					FROM    tblClaimType CT
						INNER JOIN tblClaimTypeValue CTV ON CT.ClaimTypeValueID = CTV.ClaimTypeValueID	
					WHERE ct.ClaimID = c.ClaimID 
					FOR XML PATH('''')) AS ClaimTypeValue
					FROM tblClaim c) ctv ON c.ClaimID = ctv.ClaimID		'+
		@JoinString +
	 @WhereString + ') cpi
	 INNER JOIN tblState s ON cpi.State = s.State'
	 IF @RowCount > 0 BEGIN
SET	 @Querystring = @QueryString + '
	 WHERE RowNum BETWEEN ' + CONVERT(nvarchar, @RowStart) + ' AND ' + CONVERT(nvarchar, @RowStart + @RowCount ) 
	 END
SET	 @Querystring= @Querystring +
	 ' ORDER BY ' + @OrderByString + ' 
	 OPTION (RECOMPILE)
	 SET ROWCOUNT 0
	'
Print @QueryString
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString--, N'@ClaimantNameParam nvarchar(max), @SSNParam nvarchar(max), @DocketNumberParam nvarchar(max)', @ClaimantNameParam = @ClaimantNames, @SSNParam = @SSNs, @DocketNumberParam = @DocketNumbers

--select @QueryString
--select @WhereString
--select @JoinString
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearchMultiIDs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearchMultiIDs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearchMultiIDs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearchMultiIDs] TO [power_user]
GO
