SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_DISMISSAL_updDismissalInfo]
(@ClaimLawsuitStatusID int, @StatusDate nvarchar(max), @StatusNotes nvarchar(max), @IsPrimary bit, @ClaimLawsuitStatusTrackingID int, @CaseID int)
AS

--DECLARE @ClaimLawsuitStatusID nvarchar(max), @StatusDate nvarchar(max), @StatusNotes nvarchar(max), @IsPrimary nvarchar(8), @ProcessedDate nvarchar(max), @ClaimLawsuitStatusTrackingID int
--SET @ClaimLawsuitStatusID = 15
--SET @StatusDate = '11/11/2011 12:00:00 AM'
--SET @StatusNotes = 'I changed this'--'Updated by upload of documentID 115 by owenss@kcic.com'
--SET @IsPrimary = 'True'
--SET @ProcessedDate = '7/9/2013 12:00:00 AM'  
--SET @ClaimLawsuitStatusTrackingID = 542624

DECLARE @ConnectionString nvarchar(max)
SET @ConnectionString = '[' + (SELECT MAX(ConnectionStringKey) FROM tblCase WHERE CaseID = @CaseID) + '].[DBO].'

DECLARE @QueryString nvarchar(max)
SET @QueryString = '
UPDATE ' + @ConnectionString + 'tblClaimLawsuitStatusTracking
SET StatusDate				= CASE WHEN @StatusDateParam = '''' THEN NULL ELSE CONVERT(datetime, @StatusDateParam) END,
	StatusNotes				= CASE WHEN @StatusNotesParam = '''' THEN NULL ELSE @StatusNotesParam END,
	IsPrimaryStatus			= @IsPrimaryParam,
	ClaimLawsuitStatusID	= CASE WHEN @ClaimLawsuitStatusIDParam = 0 THEN NULL ELSE @ClaimLawsuitStatusIDParam END
WHERE ClaimLawsuitStatusTrackingID = CONVERT(bigint, @ClaimLawsuitStatusTrackingIDParam)'

Print @QueryString
EXEC sp_executesql @QueryString, N'@ClaimLawsuitStatusIDParam int, @StatusDateParam nvarchar(max), @StatusNotesParam nvarchar(max), @IsPrimaryParam bit, @ClaimLawsuitStatusTrackingIDParam int, @ConnectionStringParam nvarchar(max)',
@ClaimLawsuitStatusIDParam = @ClaimLawsuitStatusID, @StatusDateParam = @StatusDate, @StatusNotesParam = @StatusNotes, @IsPrimaryParam = @IsPrimary, @ClaimLawsuitStatusTrackingIDParam = @ClaimLawsuitStatusTrackingID, @ConnectionStringParam = @ConnectionString
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_updDismissalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_updDismissalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_updDismissalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_updDismissalInfo] TO [power_user]
GO
