SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_DEADLINE_qryAttendanceModeList]
AS 
    SET NOCOUNT ON

    SELECT  AttendanceModeID, AttendanceMode
    FROM    tblAttendanceMode
	UNION
	SELECT NULL, ''
	ORDER BY AttendanceMode

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryAttendanceModeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryAttendanceModeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryAttendanceModeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryAttendanceModeList] TO [power_user]
GO
