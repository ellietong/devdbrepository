SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryApproveForPayment]
(@ClaimantSettlementID bigint, @isApproved bit, @Emailaddress nvarchar(250))
AS
SET NOCOUNT ON

UPDATE tblClaimantSettlement
SET PaymentApprovedDate = CASE WHEN @isApproved = 1 THEN GETDATE() ELSE NULL END,
	PaymentApprovedBy = CASE WHEN @isApproved = 1 THEN @Emailaddress ELSE NULL END
WHERE ClaimantSettlementID = COALESCE(@ClaimantSettlementID, 0)

--EXEC [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals_OnClick]
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryApproveForPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryApproveForPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryApproveForPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryApproveForPayment] TO [power_user]
GO
