SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryUpdPrimaryClaimType]
AS
BEGIN

--SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
SET NOCOUNT ON

IF object_id('tempdb..#PrimaryClaimType') IS NOT NULL
BEGIN
	DROP TABLE #PrimaryClaimType
END
CREATE TABLE #PrimaryClaimType (ClaimID       BIGINT, 
								ClaimTypeRank INT)
INSERT INTO #PrimaryClaimType
	SELECT ct.ClaimID, 
		MIN(PrimaryClaimTypeRank)
	FROM tblClaimType ct
		INNER JOIN tblClaimTypeValue ctv on ct.ClaimTypeValueID = ctv.ClaimTypeValueID
	GROUP BY ct.ClaimID

UPDATE ct
	SET IsPrimaryClaimType = 1
	FROM tblClaimType ct
		INNER JOIN #PrimaryClaimType pct ON ct.ClaimID = pct.ClaimID
		INNER JOIN tblClaimTypeValue ctv ON pct.ClaimTypeRank =  ctv.PrimaryClaimTypeRank AND ct.ClaimTypeValueID =  ctv.ClaimTypeValueID
	WHERE COALESCE(IsPrimaryClaimType,0) <> 1

UPDATE ct
	SET IsPrimaryClaimType = 0
	FROM tblClaimType ct
		INNER JOIN #PrimaryClaimType pct ON ct.ClaimID = pct.ClaimID
		INNER JOIN tblClaimTypeValue ctv ON pct.ClaimTypeRank =  ctv.PrimaryClaimTypeRank AND ct.ClaimTypeValueID <>  ctv.ClaimTypeValueID
	WHERE COALESCE(IsPrimaryClaimType,0) <> 0 OR IsPrimaryClaimType IS NULL

END
GO
