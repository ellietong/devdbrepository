SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DEADLINE_qryGetClaimLawsuitsByCaseDeadlineID]

@CaseDeadlineID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT DISTINCT CONVERT(NVARCHAR(20), cl.ClaimLawsuitID) AS ClaimLawsuitID,
	CONVERT(NVARCHAR(20), cpi.ClaimantPersonalInfoID) AS ClaimantID,
	cpi.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName
FROM tblClaimantPersonalInfo cpi 
	INNER JOIN tblClaim c									 ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl							 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l	ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitCaseDeadline clcd ON cl.ClaimLawsuitID = clcd.ClaimLawsuitID
WHERE CaseDeadlineID = @CaseDeadlineID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryGetClaimLawsuitsByCaseDeadlineID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryGetClaimLawsuitsByCaseDeadlineID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryGetClaimLawsuitsByCaseDeadlineID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryGetClaimLawsuitsByCaseDeadlineID] TO [power_user]
GO
