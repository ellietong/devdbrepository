SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SERVICES_GetClaimantsByTrialGroupID] 

@TrialGroupID int,
@CaseDeadlineID bigint = NULL,
@EmailAddress nvarchar(250)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @UserDefenseCounselAddressID INT
    SET @UserDefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @Emailaddress
                                   )

	IF object_id('tempdb..#ClaimantLists') IS NOT NULL
	BEGIN
		DROP TABLE #ClaimantLists
	END
	CREATE TABLE #ClaimantLists (CaseDeadlineID bigint, ClaimantList nvarchar(MAX))
	BEGIN

		IF ( (RIGHT(@Emailaddress, 11) = 'kcicllc.com' OR RIGHT(@Emailaddress, 8) = 'kcic.com')
				OR ( ( SELECT  NoFilters
					FROM    vtblPerson
					WHERE   Email = @Emailaddress
					) = 1 )
			) 

			BEGIN
				INSERT INTO #ClaimantLists
				SELECT cd.TrialGroupID,
					'<ul style="list-style-type: none; padding: 0px; margin: 0px;" id="sgid' + CONVERT(nvarchar(10), cd.CaseDeadlineID) + '">' + CONVERT(nvarchar(MAX), (	SELECT 
								--'a/@OnClick' = 'getTrialGroupClaimants('+ CONVERT(nvarchar(10), cpi.ClaimantPersonalInfoID) + ' , ' + CONVERT(nvarchar(10), MAX(SettlementAuthorityID)) + ' , ' + 'event' + ');',
								'a/@href' = '/clients/pfizer/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, cpi.ClaimantPersonalInfoID),
								'a/@target' = '_blank',
								'a/@class' = 'anchorStyle',
								'a' = cpi.LastName + ', ' + cpi.FirstName					
					FROM tblTrialGroup tgroup
						INNER JOIN tblClaimLawsuit cl ON tgroup.TrialGroupID = cl.TrialGroupID
						INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
						INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
					WHERE tgroup.TrialGroupID = cd.TrialGroupID
						GROUP by cpi.ClaimantPersonalInfoID, LastName, FirstName
						FOR XML PATH('li')
					)) + '</ul>'

			FROM tblCaseDeadline cd
			WHERE cd.TrialGroupID =  @TrialGroupID
				AND CaseDeadlineID = @CaseDeadlineID
			GROUP BY cd.CaseDeadlineID, cd.TrialGroupID
			END
		ELSE 
			BEGIN
				INSERT INTO #ClaimantLists
				SELECT cd.TrialGroupID,
					'<ul style="list-style-type: none; padding: 0px; margin: 0px;" id="sgid' + CONVERT(nvarchar(10), cd.CaseDeadlineID) + '">' + CONVERT(nvarchar(MAX), (	SELECT 
								--'a/@OnClick' = 'getTrialGroupClaimants('+ CONVERT(nvarchar(10), cpi.ClaimantPersonalInfoID) + ' , ' + CONVERT(nvarchar(10), MAX(SettlementAuthorityID)) + ' , ' + 'event' + ');',
								'a/@href' = '/clients/pfizer/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, cpi.ClaimantPersonalInfoID),
								'a/@target' = '_blank',
								'a/@class' = 'anchorStyle',
								'a' = cpi.LastName + ', ' + cpi.FirstName					
					FROM tblTrialGroup tgroup
						INNER JOIN tblClaimLawsuit cl ON tgroup.TrialGroupID = cl.TrialGroupID
						INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
						INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
						INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID
					WHERE tgroup.TrialGroupID = cd.TrialGroupID
						AND (dj.DefenseCounselAddressID = @UserDefenseCounselAddressID OR l.DefenseCounselAddressID = @UserDefenseCounselAddressID)
						GROUP by cpi.ClaimantPersonalInfoID, LastName, FirstName
						FOR XML PATH('li')
					)) + '</ul>'

			FROM tblCaseDeadline cd
			WHERE cd.TrialGroupID =  @TrialGroupID
				AND CaseDeadlineID = @CaseDeadlineID		
			GROUP BY cd.CaseDeadlineID, cd.TrialGroupID
			END
	END

	SELECT *
	FROM #ClaimantLists

END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_GetClaimantsByTrialGroupID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_GetClaimantsByTrialGroupID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_GetClaimantsByTrialGroupID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_GetClaimantsByTrialGroupID] TO [power_user]
GO
