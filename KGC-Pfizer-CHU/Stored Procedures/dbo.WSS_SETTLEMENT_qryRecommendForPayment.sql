SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryRecommendForPayment]
(@ClaimantSettlementID bigint, @IsRecommended bit, @Emailaddress nvarchar(250))
AS
SET NOCOUNT ON

UPDATE tblClaimantSettlement
SET PaymentRecommendedDate = CASE WHEN @IsRecommended = 1 THEN GETDATE() ELSE NULL END,
	PaymentRecommendedBy = CASE WHEN @IsRecommended = 1 THEN @Emailaddress ELSE NULL END
WHERE ClaimantSettlementID = COALESCE(@ClaimantSettlementID, 0)

--EXEC [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewRecommendations_OnClick] 
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryRecommendForPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryRecommendForPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryRecommendForPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryRecommendForPayment] TO [power_user]
GO
