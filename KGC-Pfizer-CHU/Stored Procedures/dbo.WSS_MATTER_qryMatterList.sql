SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_MATTER_qryMatterList]
AS 
    SET NOCOUNT ON

    SELECT  MatterID ,
            Matter
    FROM    tblMatter
    UNION
    SELECT  NULL ,
            ''
    ORDER BY Matter
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_MATTER_qryMatterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_MATTER_qryMatterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_MATTER_qryMatterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_MATTER_qryMatterList] TO [power_user]
GO
