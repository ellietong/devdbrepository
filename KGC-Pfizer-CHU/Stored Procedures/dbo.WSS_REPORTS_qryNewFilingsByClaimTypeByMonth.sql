SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByClaimTypeByMonth] 
	-- Add the parameters for the stored procedure here
	(@MonthID int, @YearID int)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @month int
--Set @month= 11
--Declare @year int
--Set @Year=2010

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

select Material as ClaimType, 
		COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblAllegation a on a.ClaimLawsuitID=cl.ClaimLawsuitID
inner join tblMaterial m on m.MaterialID = a.MaterialID
where	month(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))=@Month 
		and year(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))=@Year 
		
group by Material
order by Material

END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimTypeByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimTypeByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimTypeByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimTypeByMonth] TO [power_user]
GO
