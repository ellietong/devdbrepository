SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] ( @DocId INT )
AS --DECLARE @SPDocId nvarchar(50)
--SET @SPDocId = 'c43ef74a-898e-40bb-a64f-a09ae1b9ed25'



--SELECT d.CreatedBy,
--	COALESCE(d.Accepted, 0) AS Accepted,
--	d.AcceptedBy,
--	d.DateAccepted,
--	--d.FirmID,
--	cdt.ClaimantDocumentGroupID,
--	DocumentURL,
--	d.ClaimantDocumentTypeID--,
--	--d.StatusID
--FROM tblDocuments d
--	INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
--WHERE DocumentID = @DocId

    SELECT  d.CreatedBy ,
            COALESCE(d.Accepted, 0) AS Accepted ,
            d.AcceptedBy ,
            d.DateAccepted ,
            DefenseCounselID AS FirmID ,
            cdt.ClaimantDocumentGroupID ,
            AmazonKey AS DocumentURL ,
            d.ClaimantDocumentTypeID ,
            AmazonKey ,
            StatusID
    FROM    tblDocuments d
            INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
            LEFT JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
            INNER JOIN tblClaim c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
            INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
            INNER JOIN tblLawsuit l ON cl.LawsuitID = l.lawsuitid
            INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
            INNER JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
            LEFT JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID
    WHERE   d.DocumentID = @DocId
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [power_user]
GO
