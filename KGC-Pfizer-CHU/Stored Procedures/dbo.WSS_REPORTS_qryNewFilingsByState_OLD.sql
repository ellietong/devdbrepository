SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByState_OLD] 
	-- Add the parameters for the stored procedure here
	(@StartDate datetime, @EndDate datetime, @ClaimTypeValueID int, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @MaxClaimTypeValueID int
SET @MaxClaimTypeValueID = (SELECT MAX(ClaimTypeValueID) FROM tblClaimTypeValue)

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole =0--(SELECT distinct InsurerRole from vtblPerson WHERE Email = @emailaddress)

--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@EmailAddress, 11) = 'kcicllc.com' OR RIGHT(@EmailAddress, 8) = 'kcic.com' OR @NoFilters = 1)
BEGIN

SELECT FullNameState AS State, NULL AS Combination, COUNT(distinct(cl.ClaimLawsuitID)) As Count, COUNT(distinct(cl.ClaimLawsuitID)) As StateCount
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
LEFT JOIN (SELECT * from tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
INNER JOIN tblAllegation a on cl.ClaimLawsuitID = a.ClaimLawsuitID
WHERE ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
and((c.MatterID =1 and a.servicedate between @StartDate and @EndDate and DocketNumber <>'UNFILED' ) 
	or (cl.ClaimLawsuitID  in (3617371,3930921,4104422,4150163,3727290,3746595,3740572,3781433,3906063,3920744,3937709,4087312,4978702) and a.ServiceDate between @StartDate and @EndDate))	
	GROUP BY FullNameState
--ORDER BY COUNT(distinct(cl.ClaimLawsuitID)) desc
UNION
SELECT s.FullNameState, Jurisdiction AS Combination, COUNT(distinct(cl.ClaimLawsuitID)) as Count, 0 AS JurisdictionCount
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
LEFT JOIN (SELECT * from tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
INNER JOIN tblAllegation a on cl.ClaimLawsuitID = a.ClaimLawsuitID
INNER JOIN vREPORTS_StateJurisdictionCombinations sjc on l.JurisdictionID = sjc.JurisdictionID
WHERE ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
and((c.MatterID =1 and a.servicedate between @StartDate and @EndDate and DocketNumber <>'UNFILED' ) 
	or (cl.ClaimLawsuitID  in (3617371,3930921,4104422,4150163,3727290,3746595,3740572,3781433,3906063,3920744,3937709,4087312,4978702) and a.ServiceDate between @StartDate and @EndDate))	
	group by s.FullNameState, Jurisdiction
order by s.FullNameState, Combination

END
ELSE IF(@InsurerRole = 1) --Insurers
BEGIN

SELECT FullNameState AS State, NULL AS Combination, COUNT(distinct(cl.ClaimLawsuitID)) As Count, COUNT(distinct(cl.ClaimLawsuitID)) As StateCount
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
LEFT JOIN (SELECT * from tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
INNER JOIN tblAllegation a on cl.ClaimLawsuitID = a.ClaimLawsuitID
WHERE (c.MatterID =1 and a.servicedate between @StartDate and @EndDate and DocketNumber <>'UNFILED' ) 
	or (cl.ClaimLawsuitID  in (3617371,3930921,4104422,4150163,3727290,3746595,3740572,3781433,3906063,3920744,3937709,4087312,4978702) and a.ServiceDate between @StartDate and @EndDate)
and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID) and c.ClaimSequenceID in (4,5) 
GROUP BY FullNameState
UNION
SELECT s.FullNameState, Jurisdiction AS Combination, COUNT(distinct(cl.ClaimLawsuitID)) as Count, 0 AS JurisdictionCount
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
LEFT JOIN (SELECT * from tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
INNER JOIN tblAllegation a on cl.ClaimLawsuitID = a.ClaimLawsuitID
INNER JOIN vREPORTS_StateJurisdictionCombinations sjc on l.JurisdictionID = sjc.JurisdictionID
WHERE ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
and((c.MatterID =1 and a.servicedate between @StartDate and @EndDate and DocketNumber <>'UNFILED' ) 
	or (cl.ClaimLawsuitID  in (3617371,3930921,4104422,4150163,3727290,3746595,3740572,3781433,3906063,3920744,3937709,4087312,4978702) and a.ServiceDate between @StartDate and @EndDate))	
	group by s.FullNameState, Jurisdiction
order by s.FullNameState, Combination


END
ELSE 
BEGIN

SELECT FullNameState AS State, NULL AS Combination, COUNT(distinct(cl.ClaimLawsuitID)) As Count, COUNT(distinct(cl.ClaimLawsuitID)) As StateCount
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
LEFT JOIN (SELECT * from tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
INNER JOIN tblAllegation a on cl.ClaimLawsuitID = a.ClaimLawsuitID
WHERE ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
and((c.MatterID =1 and a.servicedate between @StartDate and @EndDate and DocketNumber <>'UNFILED' ) 
	or (cl.ClaimLawsuitID  in (3617371,3930921,4104422,4150163,3727290,3746595,3740572,3781433,3906063,3920744,3937709,4087312,4978702) and a.ServiceDate between @StartDate and @EndDate))	
AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID) and c.ClaimSequenceID in (4,5) 
GROUP BY FullNameState
--ORDER BY COUNT(distinct(cl.ClaimLawsuitID)) desc
UNION
SELECT s.FullNameState, Jurisdiction AS Combination, COUNT(distinct(cl.ClaimLawsuitID)) as Count, 0 AS JurisdictionCount
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
LEFT JOIN (SELECT * from tblClaimLawsuitStatusTracking WHERE ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
INNER JOIN tblAllegation a on cl.ClaimLawsuitID = a.ClaimLawsuitID
INNER JOIN vREPORTS_StateJurisdictionCombinations sjc on l.JurisdictionID = sjc.JurisdictionID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
WHERE ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
and((c.MatterID =1 and a.servicedate between @StartDate and @EndDate and DocketNumber <>'UNFILED' ) 
	or (cl.ClaimLawsuitID  in (3617371,3930921,4104422,4150163,3727290,3746595,3740572,3781433,3906063,3920744,3937709,4087312,4978702) and a.ServiceDate between @StartDate and @EndDate))	
AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID) and c.ClaimSequenceID in (4,5) 
group by s.FullNameState, Jurisdiction
order by s.FullNameState, Combination

END


END



GO
