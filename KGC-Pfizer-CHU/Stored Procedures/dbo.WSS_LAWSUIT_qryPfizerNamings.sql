SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryPfizerNamings]
	-- Add the parameters for the stored procedure here
@LawsuitID int =NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT UPPER(DefendantCompany) AS DefendantCompany, a.DefendantCompanyID, a.ServiceDate, a.PfizerFileDate
	FROM tblAllegation a
		INNER JOIN tblClaimLawsuit cl on a.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblDefendantCompany dc on a.DefendantCompanyID = dc.DefendantCompanyID
	WHERE LawsuitID = @LawsuitID
	GROUP BY DefendantCompany, a.DefendantCompanyID, a.ServiceDate, a.PfizerFileDate
	ORDER BY DefendantCompany

		--SELECT '' AS DefendantCompany, '' AS DefendantCompanyID, '' AS ServiceDate
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryPfizerNamings] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryPfizerNamings] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryPfizerNamings] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryPfizerNamings] TO [power_user]
GO
