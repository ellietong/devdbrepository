SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryUpdClaimantNotes]
    (
      @ClaimantPersonalInfoID BIGINT ,
	  @ClaimantNotes NVARCHAR(MAX),
      @Username NVARCHAR(250) 
    )
AS 
    SET NOCOUNT ON

    UPDATE  tblClaimantPersonalInfo
    SET     ClaimantNotes = @ClaimantNotes,
            SPUsername = @Username 
    WHERE   ClaimantPersonalInfoID = @ClaimantPersonalInfoID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantNotes] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantNotes] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantNotes] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantNotes] TO [power_user]
GO
