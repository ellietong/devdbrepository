SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  Procedure [dbo].[qryrptBillReport]
(@BillID int = NULL)
As

SET NOCOUNT ON

--DECLARE @BillID INT
--SET @BillID = 0

SELECT BillID, BillDate, BillGroupID, PreviousBalance, SUM(Credit) AS TotalCredits, SUM(Debit) AS TotalDebits, 
	SUM(CASE WHEN LedgerTypeID = 1 THEN Debit-Credit ELSE 0 END) AS TotalExhaustion, SUM(CASE WHEN LedgerTypeID = 2 THEN Debit-Credit ELSE 0 END) AS TotalPayments,
	SUM(CASE WHEN LedgerTypeID = 3 THEN Debit-Credit ELSE 0 END) AS TotalInterest, SUM(CASE WHEN LedgerTypeID = 4 THEN Debit-Credit ELSE 0 END) AS TotalAdjustments, 
	NewBalance, FinalizedBy, PaymentDueDate
FROM vBilling
WHERE BillID LIKE COALESCE(@BillID, 0)
GROUP BY BillID, BillDate, BillGroupID, PreviousBalance, NewBalance, FinalizedBy, PaymentDueDate

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillReport] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBillReport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillReport] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBillReport] TO [power_user]
GO
