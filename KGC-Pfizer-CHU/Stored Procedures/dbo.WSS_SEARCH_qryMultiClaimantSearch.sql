SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SEARCH_qryMultiClaimantSearch]
    (
      @ClaimantName NVARCHAR(MAX) ,
      @SSN NVARCHAR(20) ,
      @StateID INT ,
      @PlaintiffCounselID INT ,
      @DefendantCompanyID BIGINT = NULL,
      @InjPartyID BIGINT = NULL,
      @emailaddress NVARCHAR(250) ,
      @DocketNumber NVARCHAR(250) ,
      @ClaimLawsuitStatusGroupID INT ,
      @TrialGroupID INT = NULL,
	  @SettlementGroupTypeID INT,
	  @ClaimSequenceID INT,
	  @MatterID	INT,
	  @ClaimTypeValueID INT,
      @ClaimantIDs integer_list_tbltype READONLY
    )
AS 
    SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantName              nvarchar(max)
--DECLARE @SSN                       nvarchar(20)
--DECLARE @StateID                   int
--DECLARE @PlaintiffCounselID		   int
--DECLARE @DefendantCompanyID		   bigint
--DECLARE @InjPartyID                bigint
--DECLARE @MatterID                  int
--DECLARE @ClaimTypeValueID				 int
--DECLARE @IsMDL                     int
--DECLARE @emailaddress              nvarchar(250)
--DECLARE @DocketNumber              nvarchar(250)
--DECLARE @ClaimLawsuitStatusGroupID int
--DECLARE @TrialGroupID			   int
--DECLARE @SettlementGroupTypeID	   int
--DECLARE @ClaimSequenceID		   int
--DECLARE @ClaimantIDs               TABLE (n int) 

--SET @ClaimantName              = NULL
--SET @SSN                       = NULL
--SET @StateID                   = 52
--SET @PlaintiffCounselID		   = NULL
--SET @DefendantCompanyID		   = NULL
--SET @InjPartyID                = NULL
--SET @MatterID                  = NULL
--SET @ClaimTypeValueID				 = NULL
--SET @IsMDL                     = NULL
--SET @emailaddress              = 'owenss@kcic.com'
--SET @DocketNumber              = NULL
--SET @ClaimLawsuitStatusGroupID = 3
--SET @TrialGroupID			   = NULL
--SET @SettlementGroupTypeID	   = NULL
--SET @ClaimSequenceID		   = NULL

    DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
    IF OBJECT_ID('tempdb..#ClaimantIDs') IS NOT NULL 
        BEGIN
            DROP TABLE #ClaimantIDs
        END
    CREATE TABLE #ClaimantIDs ( n INT )
    INSERT  INTO #ClaimantIDs
            SELECT  n
            FROM    @ClaimantIDs
	
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE 1=1'
    IF @ClaimantName IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND ((CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' end) like ''%'' + @ClaimantNameParam + ''%'')'
        END
    IF @SSN IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND COALESCE(socialsecurityNumber, '''') like ''%'' + @SSNParam + ''%'''
        END
    IF @StateID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND l.StateID = '
                + CONVERT(NVARCHAR(20), @StateID)
        END
    IF @PlaintiffCounselID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND COALESCE(l.PlaintiffCounselAddressID,0) IN((SELECT PlaintiffCounselAddressID FROM tblPlaintiffCounselAddress WHERE PlaintiffCounselID ='
                + CONVERT(NVARCHAR(20), @PlaintiffCounselID) +'))'
        END
    IF @DefendantCompanyID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND COALESCE(a.DefendantCompanyID,0) = '
                + CONVERT(NVARCHAR(20), @DefendantCompanyID)
        END

	IF @InjPartyID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND COALESCE(cpi.INJURED_PARTY_ID,''0'') = '
                + CONVERT(NVARCHAR(20), @InjPartyID)
        END

    IF @DocketNumber IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND DocketNumber LIKE ''%'' + @DocketNumberParam + ''%'''
        END
    IF @ClaimLawsuitStatusGroupID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND cls.ClaimLawsuitStatusGroupID = '
                + CONVERT(NVARCHAR(20), @ClaimLawsuitStatusGroupID)
        END
    IF @TrialGroupID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND cl.TrialGroupID = '
                + CONVERT(NVARCHAR(20), @TrialGroupID)
        END
    IF @SettlementGroupTypeID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND sg.SettlementGroupTypeID = '
                + CONVERT(NVARCHAR(20), @SettlementGroupTypeID)
        END
    IF @ClaimSequenceID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND c.ClaimSequenceID = '
                + CONVERT(NVARCHAR(20), @ClaimSequenceID)
        END
    IF @MatterID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND c.MatterID = '
                + CONVERT(NVARCHAR(20), @MatterID)
        END
    IF @ClaimTypeValueID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND ct.ClaimTypeID = '
                + CONVERT(NVARCHAR(20), @ClaimTypeValueID)
        END
    IF ( SELECT COUNT(n)
         FROM   #ClaimantIDs
       ) <> 0 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)'
        END

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    DECLARE @JoinString NVARCHAR(MAX)
    IF ( (RIGHT(@emailaddress, 11) = 'kcicllc.com' OR RIGHT(@emailaddress, 8) = 'kcic.com')
         OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @emailaddress
              ) = 1 )
       ) 
        SET @JoinString = ''
    ELSE 
        BEGIN
            SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
            SET @WhereString = @WhereString
                + ' AND (dj.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ' OR l.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ')'
        END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
    DECLARE @QueryString NVARCHAR(MAX)
    SET @QueryString = 'SELECT DISTINCT CPI.ClaimantPersonalInfoID AS ClaimantID, 
		CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '' '' ELSE MiddleName + '' '' END AS ClaimantName, 
		CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
		BirthDate, Coalesce(PlaintiffCounsel, ''Unknown'') + '' ('' + Coalesce(pca.PlaintiffCounselCity, ''Unknown'') + '')'' As PlaintiffCounsel,
		INJURED_PARTY_ID AS NavInjPartyID, 
		s.[State], 
		IsMDL, 
		ClaimLawsuitStatus, 
		DocketNumber,
		COALESCE(DefendantCompanies, '''') AS DefendantCompany,
		--cl.ClaimLawsuitID,
		cpi.LastName,
		pca.PlaintiffCounselID,
		TrialGroup,
		ClaimSequence,
		SettlementGroupType,
		Matter,
		dbo.fnClaimTypesByClaimID(c.ClaimID) AS ClaimType
	 FROM dbo.tblClaimantPersonalInfo cpi
		INNER JOIN dbo.tblClaim                            c    ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit                          cl   ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit                               l    ON cl.LawsuitID = l.LawsuitID '
        + @JoinString + '
		LEFT JOIN tblPlaintiffCounselAddress               pca  ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		LEFT JOIN tblState                                 s    ON l.StateID = s.StateID
		LEFT JOIN (SELECT * 
				   FROM tblClaimLawsuitStatusTracking 
				   WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus                    cls  ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN tblAllegation                            a    ON a.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatusGroup               clsg ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID
		LEFT JOIN tblTrialGroup								tg  ON cl.TrialGroupID = tg.TrialGroupID
		LEFT JOIN tblClaimSequence							csq ON c.ClaimSequenceID = csq.ClaimSequenceID
		LEFT JOIN tblClaimantSettlement						cs	ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		LEFT JOIN tblSettlementGroup						sg	ON cs.SettlementGroupID = sg.SettlementGroupID
		LEFT JOIN tblSettlementGroupType					sgt ON sg.SettlementGroupTypeID = sgt.SettlementGroupTypeID	
		LEFT JOIN tblMatter									m	ON c.MatterID = m.MatterID		
		LEFT JOIN tblClaimType								ct	ON c.ClaimID = ct.ClaimID
		CROSS APPLY (SELECT DefendantCompany + ''</br> ''
					 FROM tblAllegation a 
						LEFT JOIN tbldefendantCompany dc ON a.DefendantCompanyID = dc.DefendantCompanyID
					 WHERE a.ClaimLawsuitID = cl.ClaimLawsuitID
					 FOR XML PATH('''') ) Q ( DefendantCompanies )
		'
        + @WhereString + '
	 ORDER BY ClaimantName 
	 OPTION (RECOMPILE)'

    PRINT @querystring
--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString,
        N'@ClaimantNameParam nvarchar(max), @SSNParam nvarchar(max), @DocketNumberParam nvarchar(max)',
        @ClaimantNameParam = @ClaimantName, @SSNParam = @SSN,
        @DocketNumberParam = @DocketNumber


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryMultiClaimantSearch] TO [power_user]
GO
