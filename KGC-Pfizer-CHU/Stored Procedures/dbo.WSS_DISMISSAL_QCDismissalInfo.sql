SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_DISMISSAL_QCDismissalInfo]
(@DocumentID bigint, @EmailAddress nvarchar(50), @CaseID int)
AS

--DECLARE @DocumentID int, @EmailAddress nvarchar(50)
--SET @DocumentID = 19
--SET @EmailAddress = 'owenss@kcic.com'

DECLARE @ConnectionString nvarchar(max)
SET @ConnectionString = '[' + (SELECT MAX(ConnectionStringKey) FROM tblCase WHERE CaseID = @CaseID) + '].[DBO].'

DECLARE @QueryString nvarchar(max)
SET @QueryString = '
UPDATE ' + @ConnectionString + 'tblDocuments
SET QCBy = @EmailAddressParam,
	QCDate = CURRENT_TIMESTAMP,
	Issues = 0
WHERE DocumentID = @DocumentIDParam'

Print @QueryString
EXEC sp_executesql @QueryString, N'@DocumentIDParam bigint, @EmailAddressParam nvarchar(50)',
@DocumentIDParam = @DocumentID, @EmailAddressParam = @EmailAddress
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_QCDismissalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_QCDismissalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DISMISSAL_QCDismissalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DISMISSAL_QCDismissalInfo] TO [power_user]
GO
