SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryGetSavedSearchTitleByUser]
	-- Add the parameters for the stored procedure here
    @EmailAddress NVARCHAR(500)
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        SELECT  SavedSearchTitle ,
                SavedSearchID
        FROM    tblSavedSearch
        WHERE   SPUserName = @Emailaddress

    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryGetSavedSearchTitleByUser] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryGetSavedSearchTitleByUser] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryGetSavedSearchTitleByUser] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryGetSavedSearchTitleByUser] TO [power_user]
GO
