SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryUpdOccupationInfo]
    (
      @ClaimantEmploymentID INT ,
      @OccupationID INT ,
      @EmpStartDate DATETIME ,
      @EmpEndDate DATETIME ,
      @Username NVARCHAR(250) ,
      @Jobsite NVARCHAR(4000) ,
      @JobsiteCity NVARCHAR(250) ,
      @JobsiteStateID INT
    )
AS 
    SET NOCOUNT ON

    UPDATE  tblClaimantEmployment
    SET     OccupationID = @OccupationID ,
            EmpStartDate = @EmpStartDate ,
            EmpEndDate = @EmpEndDate ,
            SPUsername = @Username ,
            Jobsite = UPPER(@Jobsite) ,
            JobsiteCity = UPPER(@JobsiteCity) ,
            JobsiteStateID = @JobsiteStateID
    WHERE   ClaimantEmploymentID = @ClaimantEmploymentID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryUpdOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryUpdOccupationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryUpdOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryUpdOccupationInfo] TO [power_user]
GO
