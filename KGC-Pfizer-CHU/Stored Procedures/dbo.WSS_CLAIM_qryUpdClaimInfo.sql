SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[WSS_CLAIM_qryUpdClaimInfo]
    (
      @ClaimID INT ,
	  @ClaimantPersonalInfoID INT,
      @ClaimSequenceID INT,
      @ClaimTypeID INT,
      @Username NVARCHAR(250)
	)
AS 

    UPDATE  tblClaim
    SET     SPUsername = @Username,
			ClaimTypeID = @ClaimTypeID,
			ClaimSequenceID = @ClaimSequenceID	
    WHERE   ClaimID = @ClaimID
			AND ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
	


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaimInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaimInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaimInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaimInfo] TO [power_user]
GO
