SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qrySettlementsByState_OLD] 
	-- Add the parameters for the stored procedure here
	(@StartDate datetime, @EndDate datetime, @ClaimTypeValueID int, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--declare @StartDate datetime = '1/1/2010'
--, @EndDate datetime = '7/6/2014'
--, @ClaimTypeValueID int, @EmailAddress nvarchar(250) = 'owenss@kcic.com'
	
DECLARE @MaxClaimTypeValueID int
SET @MaxClaimTypeValueID = (SELECT MAX(ClaimTypeValueID) FROM tblClaimTypeValue)

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
DECLARE @InsurerRole bit
SET @InsurerRole = 0--(select distinct InsurerRole from vtblPerson where Email = @emailaddress)

--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@EmailAddress, 11) = 'kcicllc.com' OR RIGHT(@EmailAddress, 8) = 'kcic.com' OR @NoFilters = 1)
BEGIN
select FullNameState, 
NULL AS Combination, 
COUNT(distinct(cl.ClaimLawsuitID)) as Count, 
sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity,
COUNT(distinct(cl.ClaimLawsuitID)) as StateCount, 
sum(COALESCE(SettlementAmount, 0)) as StateIndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as StateAverageIndemnity
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
INNER JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct on c.ClaimID = ct.ClaimID
where	coalesce(StatusDate, ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and clst.ClaimLawsuitStatusID = 23
		and  IsPrimaryStatus=1 and c.ClaimSequenceID in (4,5) 
		and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
		and claimantsettlementid in (select distinct claimantsettlementid from tblclaimantsettlement cs INNER JOIN tblsettlementgroup sg on cs.SettlementGroupID = sg.SettlementGroupID where SettlementGroupTypeID = 5)
group by FullNameState
UNION
select s.FullNameState, 
Jurisdiction AS Combination, 
COUNT(distinct(cl.ClaimLawsuitID)) as Count, 
sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity, 0,0,0
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
INNER JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct on c.ClaimID = ct.ClaimID
INNER JOIN vREPORTS_StateJurisdictionCombinations sjc on l.JurisdictionID = sjc.JurisdictionID
where	coalesce(StatusDate, ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and clst.ClaimLawsuitStatusID = 23
		and  IsPrimaryStatus=1 and c.ClaimSequenceID in (4,5) 
		and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
		and claimantsettlementid in (select distinct claimantsettlementid from tblclaimantsettlement cs INNER JOIN tblsettlementgroup sg on cs.SettlementGroupID = sg.SettlementGroupID where SettlementGroupTypeID = 5)
group by s.FullNameState, sjc.Jurisdiction
--order by COUNT(distinct(cl.ClaimLawsuitID)) desc
END
ELSE --Defense Counsel
BEGIN
select FullNameState, 
NULL AS Combination, 
COUNT(distinct(cl.ClaimLawsuitID)) as Count, 
sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity,
COUNT(distinct(cl.ClaimLawsuitID)) as StateCount, 
sum(COALESCE(SettlementAmount, 0)) as StateIndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as StateAverageIndemnity
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
INNER JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
LEFT JOIN tblClaimType ct on c.ClaimID = ct.ClaimID
where	coalesce(StatusDate, ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and clst.ClaimLawsuitStatusID = 23 and c.ClaimSequenceID in (4,5) 
		and  IsPrimaryStatus=1
		AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
		and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
		and claimantsettlementid in (select distinct claimantsettlementid from tblclaimantsettlement cs INNER JOIN tblsettlementgroup sg on cs.SettlementGroupID = sg.SettlementGroupID where SettlementGroupTypeID = 5)
group by FullNameState
--order by COUNT(distinct(cl.ClaimLawsuitID)) desc
UNION 
select s.FullNameState, 
Jurisdiction AS Combination, 
COUNT(distinct(cl.ClaimLawsuitID)) as Count, 
sum(COALESCE(SettlementAmount, 0)) as IndemnityPaid, sum(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity, 0,0,0
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
INNER JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct on c.ClaimID = ct.ClaimID
INNER JOIN vREPORTS_StateJurisdictionCombinations sjc on l.JurisdictionID = sjc.JurisdictionID
where	coalesce(StatusDate, ProcessedDate, '1/1/1900') BETWEEN @StartDate AND @EndDate
		and clst.ClaimLawsuitStatusID = 23
		and  IsPrimaryStatus=1 and c.ClaimSequenceID in (4,5) 
		and ct.ClaimTypeValueID >= coalesce(@ClaimTypeValueID, 0) and ct.ClaimTypeValueID <= coalesce(@ClaimTypeValueID, @maxClaimTypeValueID)
		and claimantsettlementid in (select distinct claimantsettlementid from tblclaimantsettlement cs INNER JOIN tblsettlementgroup sg on cs.SettlementGroupID = sg.SettlementGroupID where SettlementGroupTypeID = 5)
group by s.FullNameState, sjc.Jurisdiction
END

END



GO
