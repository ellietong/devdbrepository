SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_DEADLINE_qryAddClaimLawsuitCaseDeadline]
(
@ClaimLawsuitID bigint, 
@DeadlineID bigint 
)
AS
SET NOCOUNT ON


INSERT INTO tblClaimLawsuitCaseDeadline (CaseDeadlineID, ClaimLawsuitID)
	VALUES(@DeadlineID, @ClaimLawsuitID)



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryAddClaimLawsuitCaseDeadline] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryAddClaimLawsuitCaseDeadline] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryAddClaimLawsuitCaseDeadline] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryAddClaimLawsuitCaseDeadline] TO [power_user]
GO
