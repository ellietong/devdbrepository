SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[WSS_SERVICES_qryGetEventTypes]
(@PrefixText nvarchar(200), @Emailaddress nvarchar(250) = null, @DeadlineTypeID int )
AS
BEGIN
	SELECT EventTypeID, EventType --+ ' (' + CONVERT(nvarchar, TrialGroupID) + ')'
	FROM tblEventType
	WHERE EventType LIKE '%' + @PrefixText + '%'
	AND DeadlineTypeID = @DeadlineTypeID
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetEventTypes] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetEventTypes] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetEventTypes] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetEventTypes] TO [power_user]
GO
