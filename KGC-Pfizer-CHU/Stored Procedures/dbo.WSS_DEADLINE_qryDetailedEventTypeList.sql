SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_DEADLINE_qryDetailedEventTypeList]
AS 
    SET NOCOUNT ON

    SELECT  DetailedEventTypeID ,
			UPPER(DetailedEventType) AS DetailedEventType
    FROM    tblDetailedEventType
	UNION
	SELECT NULL, ''
	ORDER BY DetailedEventType

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryDetailedEventTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryDetailedEventTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryDetailedEventTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryDetailedEventTypeList] TO [power_user]
GO
