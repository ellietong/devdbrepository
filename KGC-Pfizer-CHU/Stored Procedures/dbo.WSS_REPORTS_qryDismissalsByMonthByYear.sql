SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryDismissalsByMonthByYear] 
	-- Add the parameters for the stored procedure here
	(@YearID int, @MonthID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @YearID int
--Set @YearId=2010

DECLARE @year int
SET @year = @YearID

DECLARE @Month int
set @Month = @MonthID

    -- Insert statements for procedure here
select DATENAME("mm", ProcessedDate) AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS Count, MONTH(ProcessedDate) As MonthID
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
left join tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct					  ON c.ClaimID = ct.ClaimID
where	year(ProcessedDate)=@Year 
		and MONTH(ProcessedDate) <=@Month
		and ClaimLawsuitStatusGroupID =2
		and  IsPrimaryStatus=1
group by DATENAME("mm", ProcessedDate), MONTH(ProcessedDate)
order by MONTH(ProcessedDate)
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByMonthByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByMonthByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByMonthByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByMonthByYear] TO [power_user]
GO
