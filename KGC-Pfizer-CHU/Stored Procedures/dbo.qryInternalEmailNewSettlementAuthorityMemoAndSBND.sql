SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[qryInternalEmailNewSettlementAuthorityMemoAndSBND]
AS 
BEGIN

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @LogoHeader nvarchar (2000)
DECLARE @Today4PM datetime
DECLARE @Yesterday4PM datetime
SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')
SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')

SET @Subject = 'Pfizer Claims: New Settlement Authority Memo Uploaded/New SBND Claims ' + CONVERT(varchar, GETDATE(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--Get claims that have had a Settlement Authority Memo uploaded in the last 24 hours
DECLARE @NewSettlementAuthorityMemosTable TABLE 
								(
								 DocID bigint,
								 ClaimantPersonalInfoID bigint,
								 ClaimantName nvarchar(250),
								 DocumentLink nvarchar(1000),
								 Status nvarchar(250),
								 SettlementDate nvarchar(50),
								 SettlementAmount nvarchar(50)
								)
INSERT INTO @NewSettlementAuthorityMemosTable
SELECT DISTINCT
	d.DocumentID,
	cpi.ClaimantPersonalInfoID,
	'<a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                    + CONVERT(NVARCHAR(50), cpi.ClaimantPersonalInfoID) + '">'
                    + cpi.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END + COALESCE(' ('
                                            + CONVERT(NVARCHAR(50), cpi.ClaimantPersonalInfoID)
                                            + ')', '') + '</a>',
	'<a href="https://www.kcicextranet.com/pfizer/cp/pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
	ClaimLawsuitStatus,
	CONVERT(nvarchar(250), SettlementDate, 101),
		'$' + CONVERT(nvarchar(50), CAST(SettlementAmount AS money), 1)
FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID 
	INNER JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	INNER JOIN tblClaimantSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatus	cls on	clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
	AND d.ClaimantDocumentTypeID = 311
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	AND c.MatterID = 1
	AND IsPrimaryStatus = 1
	AND AcceptedBy <> 'Historical'
GROUP BY d.documentID,
			LastName,
			FirstName,
			MiddleName,
			d.DocumentTitle,
			cpi.ClaimantPersonalInfoID,
			cl.ClaimLawsuitID,
			ClaimLawsuitStatus,
			SettlementDate,
			SettlementAmount

---- Get all claims that have been changed to SBND in the last 24 hours
DECLARE @NewSBNDClaimsTable TABLE 
								(
								 ClaimantPersonalInfoID bigint,
								 ClaimantName nvarchar(250),
								 Status nvarchar(250),
								 SettlementDate nvarchar(50),
								 SettlementAmount nvarchar(50)
								)
INSERT INTO @NewSBNDClaimsTable		
SELECT DISTINCT
	cpi.ClaimantPersonalInfoID,
	'<a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                    + CONVERT(NVARCHAR(50), cpi.ClaimantPersonalInfoID) + '">'
                    + cpi.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END + COALESCE(' ('
                    + CONVERT(NVARCHAR(50), cpi.ClaimantPersonalInfoID)
                    + ')', '') + '</a>',
	ClaimLawsuitStatus,
	CONVERT(nvarchar(250), SettlementDate, 101),
		'$' + CONVERT(nvarchar(50), CAST(SettlementAmount AS money), 1)
FROM tblClaimantPersonalInfo cpi 
	INNER JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	INNER JOIN tblClaimantSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatus	cls on	clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	LEFT JOIN tblSettlementGroup sg on cs.SettlementGroupID = sg.SettlementGroupID
WHERE ProcessedDate BETWEEN @Yesterday4PM AND @Today4PM
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	AND c.MatterID = 1
	AND IsPrimaryStatus = 1
	AND clst.ClaimLawsuitStatusID = 25
	AND SettlementGroupTypeID = 5
GROUP BY LastName,
			FirstName,
			MiddleName,
			cpi.ClaimantPersonalInfoID,
			cl.ClaimLawsuitID,
			ClaimLawsuitStatus,
			SettlementDate,
			SettlementAmount

BEGIN
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
		<!--
		TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
		TD	
		{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
			
		}
		TH	
		{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
		UL {margin:0; 
			
		padding:0;}	
 
 
		--->
		</STYLE>'

IF (SELECT
	COUNT(ClaimantPersonalInfoID)
FROM @NewSettlementAuthorityMemosTable) > 0
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>The following settlement authority memos have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th>
	  <th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Name (ClaimantID) </th>
	  <th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Lawsuit Status</th>
	  <th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Settlement Date</th>
	  <th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Settlement Amount</th>' +
CAST((SELECT
	'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + DocumentLink + ' </td>',
	'',
	'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + ClaimantName + ' </td>',
	'',
	'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + Status + ' </td>',
	'',
	'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + SettlementDate + ' </td>',
	'',
	'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + SettlementAmount + ' </td>',
	''
FROM @NewSettlementAuthorityMemosTable nsam

FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'
END
ELSE
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>No Settlement Authority Memos have been uploaded in the last 24 hours. </H4>'
END

IF (SELECT
	COUNT(ClaimantPersonalInfoID)
FROM @NewSBNDClaimsTable) > 0
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>The following claims have been changed to Settled/Not Documented in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr>
									<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Name (ClaimantID)</th>
									<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Lawsuit Status</th>
									<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Settlement Date</th>
									<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Settlement Amount</th>' +
CAST((SELECT
	'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + ClaimantName + ' </td>',
	'',
	'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + Status + ' </td>',
	'',
	'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + SettlementDate + ' </td>',
	'',
	'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + SettlementAmount + ' </td>',
	''
FROM @NewSBNDClaimsTable sbnd

FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'
END
ELSE
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>No claims have been changed to Settled/Not Documented in the last 24 hours. </H4>'
END

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--SELECT @BodyMessage

IF ((Select COUNT(ClaimantPersonalInfoID) from @NewSBNDClaimsTable) >0) OR ((Select COUNT(ClaimantPersonalInfoID) from @NewSettlementAuthorityMemosTable) >0)
BEGIN
--Execute stored procedure to send the email 
EXEC msdb.dbo.sp_send_dbmail 
@profile_name = 'Claims',
@recipients = 'Pfizer@kcic.com',
@copy_recipients = '',
@blind_copy_recipients = 'monahanc@kcic.com; kanga@kcic.com',
@subject = @subject, 
@body = @BodyMessage, 
@body_format = 'html'
END

END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailNewSettlementAuthorityMemoAndSBND] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailNewSettlementAuthorityMemoAndSBND] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailNewSettlementAuthorityMemoAndSBND] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailNewSettlementAuthorityMemoAndSBND] TO [power_user]
GO
