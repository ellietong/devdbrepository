SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SEARCH_qryDocumentSearch]
    (
      @ClaimantName NVARCHAR(MAX) ,
      @DocumentTitle NVARCHAR(MAX) ,
	  @DocumentGroupID INT,
      @DocumentTypeID INT ,
      @CreatedBy NVARCHAR(250) ,
      @DateCreatedStart DATETIME ,
      @DateCreatedEnd DATETIME ,
	  @DocumentDateStart DATETIME,
	  @DocumentDateEnd DATETIME,
      --@IncludeHistorical BIT ,
      @ClaimantIDs integer_list_tbltype READONLY ,
      @emailaddress NVARCHAR(250)
    )
AS 
    SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantName      nvarchar(max)
--DECLARE @DocumentTitle     nvarchar(max)
--DECLARE @DocumentTypeID    int
--DECLARE @CreatedBy         nvarchar(250)
--DECLARE @DateCreatedStart  datetime
--DECLARE @DateCreatedEnd    datetime
--DECLARE @DocumentDateStart datetime
--DECLARE @DocumentDateEnd datetime
--DECLARE @DocumentGroupID int
--DECLARE @IncludeHistorical bit
--DECLARE @ClaimantIDs       TABLE (n int) 
--DECLARE @emailaddress      nvarchar(250)
--SET @ClaimantName      = NULL
--SET @DocumentTitle     = NULL
--SET @DocumentTypeID    = NULL
--SET @CreatedBy         = NULL
--SET @DateCreatedStart  = NULL
--SET @DateCreatedEnd    = NULL
--SET @DocumentDateStart = '1/1/2013'
--SET @DocumentDateEnd	 = NULL
--SET @DocumentGroupID = NULL
--SET @IncludeHistorical = 0
----SET @ClaimantIDs     = NULL
--SET @emailaddress      = 'owenss@kcic.com'

    DECLARE @DefenseCounselAddressID INT
    SET @DefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @emailaddress
                                   )

    IF OBJECT_ID('tempdb..#AccessibleClaimants') IS NOT NULL 
        DROP TABLE #AccessibleClaimants
    CREATE TABLE #AccessibleClaimants
        (
          DocumentID INT ,
          ClaimantName NVARCHAR(MAX)
        )

    IF OBJECT_ID('tempdb..#DocsToConsider') IS NOT NULL 
        DROP TABLE #DocsToConsider
    CREATE TABLE #DocsToConsider
        (
          DocumentID INT ,
          DocumentTitle NVARCHAR(4000) ,
          ClaimantDocumentType NVARCHAR(100) ,
          CreatedBy NVARCHAR(250) ,
          DateCreated DATETIME ,
          AcceptedBy NVARCHAR(250) ,
          DateAccepted DATETIME ,
          AmazonKey NVARCHAR(MAX) ,
          IsHistorical BIT,
		  DocumentDate DATETIME
        )

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
    IF OBJECT_ID('tempdb..#ClaimantIDs') IS NOT NULL 
        DROP TABLE #ClaimantIDs 
    CREATE TABLE #ClaimantIDs ( n INT )
    INSERT  INTO #ClaimantIDs
            SELECT  n
            FROM    @ClaimantIDs
	
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
    DECLARE @SelectStringAccessibleClaimants NVARCHAR(MAX)
    DECLARE @GroupByAccessibleClaimants NVARCHAR(MAX)
    DECLARE @SelectStringDocsToConsider NVARCHAR(MAX)
    DECLARE @JoinString NVARCHAR(MAX)
    DECLARE @WhereStringClaimants NVARCHAR(MAX)
    DECLARE @WhereStringClaimantName NVARCHAR(MAX)
    DECLARE @WhereStringDocuments NVARCHAR(MAX)
    DECLARE @QueryString NVARCHAR(MAX)

    SET @SelectStringAccessibleClaimants = 'INSERT INTO #AccessibleClaimants
											SELECT mcd.DocumentID, 
												''<a target="_blank" href="/clients/Pfizer/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + ''">'' + (COALESCE(LastName, '''') + '', '' + COALESCE(FirstName, '''') + '' ('' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + '')'') + ''</a>''
											FROM tblMultiClaimantDocument mcd
												INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID '

    SET @GroupByAccessibleClaimants = 'GROUP BY mcd.DocumentID, 
											''<a target="_blank" href="/clients/Pfizer/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + ''">'' + (COALESCE(LastName, '''') + '', '' + COALESCE(FirstName, '''') + '' ('' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + '')'') + ''</a>'',
											LastName, FirstName	
										ORDER BY LastName, FirstName '

    SET @SelectStringDocsToConsider = 'INSERT INTO #DocsToConsider
											SELECT DISTINCT d.DocumentID,
												DocumentTitle,
												ClaimantDocumentType,
												CreatedBy,
												DateCreated,
												AcceptedBy,
												DateAccepted,
												AmazonKey,
												IsHistorical,
												DocumentDate
											FROM tblDocuments d 
												INNER JOIN tblClaimantDocumentType cdt  ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID'

    SET @JoinString = ' '
    SET @WhereStringClaimantName = ' '
    SET @WhereStringClaimants = ' WHERE 1=1 '
    SET @WhereStringDocuments = ' WHERE 1=1 AND COALESCE(IsHistorical, 0) = 0 ' 

    IF @ClaimantName IS NOT NULL 
        BEGIN
            SET @WhereStringClaimantName = @WhereStringClaimantName
                + ' AND ClaimantNames like ''%'' + @ClaimantNameParam + ''%'' '
        END
    IF ( SELECT COUNT(n)
         FROM   #ClaimantIDs
       ) <> 0 
        BEGIN
            SET @WhereStringClaimants = @WhereStringClaimants
                + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)'
        END

    IF @DocumentTitle IS NOT NULL 
        BEGIN
            SET @WhereStringDocuments = @WhereStringDocuments
                + ' AND DocumentTitle LIKE ''%'' + @DocumentTitleParam + ''%'''
        END
    IF @DocumentGroupID IS NOT NULL 
        BEGIN
            SET @WhereStringDocuments = @WhereStringDocuments
                + ' AND cdt.ClaimantDocumentGroupID = '
                + CONVERT(NVARCHAR(20), @DocumentGroupID)
        END
    IF @DocumentTypeID IS NOT NULL 
        BEGIN
            SET @WhereStringDocuments = @WhereStringDocuments
                + ' AND d.ClaimantDocumentTypeID = '
                + CONVERT(NVARCHAR(20), @DocumentTypeID)
        END
    IF @CreatedBy IS NOT NULL 
        BEGIN
            SET @WhereStringDocuments = @WhereStringDocuments
                + ' AND d.CreatedBy LIKE ''%'' + @CreatedByParam + ''%'''
        END
    IF @DateCreatedStart IS NOT NULL
        OR @DateCreatedEnd IS NOT NULL 
        BEGIN
            SET @WhereStringDocuments = @WhereStringDocuments
                + ' AND COALESCE(CONVERT(date,d.DateCreated), ''1/1/1900'') >= '''
                + CONVERT(NVARCHAR, COALESCE(@DateCreatedStart, '1/1/1900'))
                + ''' AND COALESCE(CONVERT(date,d.DateCreated), CONVERT(date, GETDATE())) <= '''
                + CONVERT(NVARCHAR, COALESCE(@DateCreatedEnd,
                                             CONVERT(DATE, GETDATE()))) + ''''
        END

    IF @DocumentDateStart IS NOT NULL
        OR @DocumentDateEnd IS NOT NULL 
        BEGIN
            SET @WhereStringDocuments = @WhereStringDocuments
                + ' AND COALESCE(CONVERT(date,DocumentDate), ''1/1/1900'') >= '''
                + CONVERT(NVARCHAR, COALESCE(@DocumentDateStart, '1/1/1900'))
                + ''' AND COALESCE(CONVERT(date,DocumentDate), CONVERT(date, GETDATE())) <= '''
                + CONVERT(NVARCHAR, COALESCE(@DocumentDateEnd,
                                             CONVERT(DATE, GETDATE()))) + ''''
        END

    --IF @IncludeHistorical IS NOT NULL 
    --    BEGIN
    --        SET @WhereStringDocuments = @WhereStringDocuments
    --            + ' AND CASE WHEN COALESCE(IsHistorical,0) <= 0 THEN 0 ELSE 1 END <= '
    --            + CONVERT(NVARCHAR(20), @IncludeHistorical)
    --    END

    IF ( ( RIGHT(@emailaddress, 11) <> 'kcicllc.com' )
		 AND ( RIGHT(COALESCE(@emailaddress, ''), 8) <> 'kcic.com')
         AND ( ( SELECT NoFilters
                 FROM   vtblPerson
                 WHERE  Email = @emailaddress
               ) <> 1 )
       ) 
        BEGIN
            SET @WhereStringClaimants = @WhereStringClaimants
                + ' AND (dj.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ' OR l.DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@DefenseCounselAddressID, 0))
                + ')'
            SET @JoinString = ' INNER JOIN tblClaim c                  ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						INNER JOIN tblClaimLawsuit cl          ON c.ClaimID = cl.ClaimID
						INNER JOIN tblLawsuit l                ON cl.LawsuitID = l.LawsuitID
						INNER JOIN tblDefenseJurisdiction dj   ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID '
            SET @QueryString = @SelectStringAccessibleClaimants + @JoinString
                + @WhereStringClaimants + @GroupByAccessibleClaimants
                + @SelectStringDocsToConsider + @WhereStringDocuments
                + ' AND d.DocumentID IN(SELECT ac.DocumentID FROM #AccessibleClaimants ac) '			   
        END
    ELSE 
        BEGIN
            SET @QueryString = @SelectStringDocsToConsider
                + @WhereStringDocuments + @SelectStringAccessibleClaimants
                + @WhereStringClaimants
                + ' AND DocumentID IN(SELECT dtc.DocumentID FROM #DocsToConsider dtc) '
                + @GroupByAccessibleClaimants			   	
        END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.

		SET @QueryString = @QueryString
			+ ' 
		SELECT DISTINCT d.DocumentID,
			AmazonKey,
			''/clients/Pfizer/cp/Pages/DocumentProfile.aspx?ItemId='' + CONVERT(nvarchar(100), d.DocumentID) AS DocumentURL,
			DocumentTitle,
			ClaimantDocumentType AS DocumentType,
			COALESCE(ClaimantNames, '''') AS Claimants,
			DateCreated,
			CreatedBy  + ''</br>'' + ''('' + CONVERT(nvarchar,CONVERT(DATE, DateCreated))  + '')'' AS Created,
			AcceptedBy + ''</br>'' + ''('' + CONVERT(nvarchar,CONVERT(DATE, DateAccepted)) + '')'' AS Accepted,
			IsHistorical,
			DocumentDate

		FROM #DocsToConsider d
			CROSS APPLY (SELECT ClaimantName + ''</br> ''
						 FROM #AccessibleClaimants ac
						 WHERE ac.DocumentID = d.DocumentID
						 FOR XML PATH('''') ) Q ( ClaimantNames )
		WHERE d.DocumentID IN(SELECT ac.DocumentID FROM #AccessibleClaimants ac) '
			+ @WhereStringClaimantName + '
		ORDER BY DateCreated DESC
		OPTION (RECOMPILE)'

PRINT @Querystring

--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString,
        N'@ClaimantNameParam nvarchar(max), @DocumentTitleParam nvarchar(max), @CreatedByParam nvarchar(250)',
        @ClaimantNameParam = @ClaimantName,
        @DocumentTitleParam = @DocumentTitle, @CreatedByParam = @CreatedBy


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [power_user]
GO
