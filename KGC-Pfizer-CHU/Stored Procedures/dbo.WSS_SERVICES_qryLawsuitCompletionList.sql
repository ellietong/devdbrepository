SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SERVICES_qryLawsuitCompletionList]
    (
      @PrefixText NVARCHAR(100) ,
      @Emailaddress NVARCHAR(100)
    )
AS 
    BEGIN

--DECLARE @emailaddress nvarchar(250) = 'crookc@kcicllc.com'
--DECLARE @PrefixText nvarchar(100) = 'CV'

        SET NOCOUNT ON

        DECLARE @FirmAddressID INT
        SET @FirmAddressID = ( SELECT   FirmAddressID
                               FROM     vtblPerson
                               WHERE    Email = @emailaddress
                             )

        DECLARE @NoFilters BIT
        SET @NoFilters = ( SELECT   NoFilters
                           FROM     vtblPerson
                           WHERE    Email = @emailaddress
                         )

        IF ( RIGHT(@Emailaddress, 11) = 'kcicllc.com'
			 OR RIGHT(@EmailAddress, 8) = 'kcic.com'
             OR @NoFilters = 1
           ) 
            BEGIN
                SELECT DISTINCT
                        l.LawsuitID ,
                        DocketNumber + ' (' + COALESCE(CaseCaption, 'UNKNOWN') + ')' AS CaseCaption
                FROM    tblClaim c
                        LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
                        LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
                        LEFT JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimID
                        LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
                        --LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                WHERE   (DocketNumber LIKE COALESCE('%' + @PrefixText, '')
                        + '%') --AND clst.ClaimLawsuitStatusID IN (select ClaimLawsuitStatusID from tblclaimlawsuitstatus where ClaimLawsuitStatusID NOT IN(15,16,23,24,25))
                ORDER BY DocketNumber + ' (' + COALESCE(CaseCaption, 'UNKNOWN') + ')'

            END
        ELSE 
            BEGIN
                SELECT DISTINCT
                        l.LawsuitID ,
                        DocketNumber + ' (' + COALESCE(CaseCaption, 'UNKNOWN') + ')' AS CaseCaption
                FROM    tblClaim c
                        LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
                        LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
                        LEFT JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimID
                        LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
                        INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID
                                                              AND l.JurisdictionID = dj.JurisdictionID --AND c.MatterID = dj.matterID
						--LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                WHERE   (DocketNumber LIKE COALESCE('%' + @PrefixText, '')
                        + '%') --AND clst.ClaimLawsuitStatusID IN (select ClaimLawsuitStatusID from tblclaimlawsuitstatus where ClaimLawsuitStatusID NOT IN(15,16,23,24,25))
                        AND dj.DefenseCounselAddressID = @FirmAddressID
                ORDER BY DocketNumber + ' (' + COALESCE(CaseCaption, 'UNKNOWN') + ')'


            END
    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [power_user]
GO
