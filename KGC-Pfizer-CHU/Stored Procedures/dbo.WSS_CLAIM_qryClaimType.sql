SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryClaimType] 
	-- Add the parameters for the stored procedure here
    @ClaimID BIGINT = NULL
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        SELECT  ct.ClaimID,
				ct.ClaimTypeID, 
				CT.ClaimTypeValueID,
				ClaimTypeValue,
				--CASE WHEN NotifyCounsel =1 THEN 1 ELSE 0 END AS NotifyCounsel,
				NotifyCounsel
        FROM    tblClaimType CT
			INNER JOIN tblClaimTypeValue CTV ON CT.ClaimTypeValueID = CTV.ClaimTypeValueID
			LEFT JOIN tblClaim c on c.claimid = ct.claimid
        WHERE   ct.ClaimID = @ClaimID
		ORDER BY ClaimTypeValue
    END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryClaimType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryClaimType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryClaimType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryClaimType] TO [power_user]
GO
