SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_DEADLINE_qryDeadlineStatusList]
AS 
    SET NOCOUNT ON

    SELECT  DeadlineStatusID ,
            UPPER(DeadlineStatus) AS DeadlineStatus
    FROM    tblDeadlineStatus
	UNION
	SELECT NULL, ''
	ORDER BY DeadlineStatus

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryDeadlineStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryDeadlineStatusList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryDeadlineStatusList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryDeadlineStatusList] TO [power_user]
GO
