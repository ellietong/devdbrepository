SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryExternalEmailDeadlines_TwoWeeks]
AS 
BEGIN
        DECLARE @Subject NVARCHAR(100)
        DECLARE @LogoHeader NVARCHAR(2000)

        SET @Subject = 'Pfizer Upcoming Deadlines and Depos'          

        SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

DECLARE @TodaysDate date = (SELECT DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))
DECLARE @2WeeksFuture date = (SELECT DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))+14)
DECLARE @tdStyleW nvarchar(MAX) = 'font-family: verdana,arial,helvetica,sans-serif; font-size: 8pt; text-align: left; border: none; padding: .1em; vertical-align: left; background-color: transparent;'

    IF OBJECT_ID('tempdb..#CaseDeadlineClaimants') IS NOT NULL 
        DROP TABLE #CaseDeadlineClaimants
    CREATE TABLE #CaseDeadlineClaimants
        (
			CaseDeadlineID int,
			ClaimantName nvarchar(max)
        )
INSERT INTO #CaseDeadlineClaimants
SELECT DISTINCT cci.casedeadlineID,
		COALESCE((SELECT N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0; width:200px;">' + 	
		CAST((SELECT @tdStyleW AS 'td/@style', (SELECT COALESCE(	'<a target="_blank" href="http://admin.kcicextranetdev.com/clients/Pfizer/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + COALESCE(CONVERT(nvarchar(10), cpi.ClaimantPersonalInfoID), '') + '">' + (COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '')) + '</a>' --+ ' (' + COALESCE(CONVERT(nvarchar(10), cpi.ClaimantPersonalInfoID), '') + ')') + '</a>'
, '')) as td, ''
 
FROM tblClaimLawsuitCaseDeadline clcd
INNER JOIN tblClaimLawsuit cl ON clcd.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID						 
		WHERE cci.caseDeadlineid = clcd.caseDeadlineid 
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) )) +  N'</table>' +  N'<br/>'	, 'UNKNOWN') AS Missing
FROM tblCaseDeadline cci
where TrialGroupID is null

IF object_id('tempdb..#TrialGroupClaimantLists') IS NOT NULL
BEGIN
	DROP TABLE #TrialGroupClaimantLists
END
CREATE TABLE #TrialGroupClaimantLists (TrialGroupID int, ClaimantList NVARCHAR(MAX), ClaimLawsuitID bigint)
BEGIN
	INSERT INTO #TrialGroupClaimantLists
	SELECT DISTINCT cd.TrialGroupID,
		'<ul>' + CONVERT(NVARCHAR(MAX), (	SELECT 
					cpi.LastName + ', ' + cpi.FirstName	
			FROM tblTrialGroup tgroup
				INNER JOIN tblClaimLawsuit cl ON tgroup.TrialGroupID = cl.TrialGroupID
				INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
				INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
			WHERE tgroup.TrialGroupID = cd.TrialGroupID
			GROUP by cpi.ClaimantPersonalInfoID, LastName, FirstName
			FOR XML PATH('li')
		)) + '</ul>', (select MAX(ClaimLawsuitID) from tblClaimLawsuit where TrialGroupID = cd.TrialGroupID)

	FROM tblCaseDeadline cd
	WHERE cd.TrialGroupID IS NOT NULL
	GROUP BY cd.TrialGroupID
END

IF OBJECT_ID('tempdb..#CaseNames') IS NOT NULL 
    DROP TABLE #CaseNames
CREATE TABLE #CaseNames
    (
		CaseDeadlineID bigint,
		CaseName nvarchar(max),
		Jurisdiction nvarchar(250),
		JurisdictionID int,
		DefenseCounsel nvarchar(250),
		DefenseCounselAddressID int,
		PlaintiffCounsel nvarchar(250),
		PlaintiffCounselID int,
		State nvarchar(25),
		StateID int,
		ClaimType nvarchar(max),
		ClaimID bigint			
    )
INSERT INTO #CaseNames
SELECT DISTINCT  clcd.CaseDeadlineID, 
		ClaimantName,
		Jurisdiction, 
		l.JurisdictionID,
		DefenseCounsel, 
		l.DefenseCounselAddressID,
		PlaintiffCounsel,
		PrimaryNationalPlaintiffCounselID,
		s.state, 
		l.StateID,
		dbo.fnClaimTypesByClaimID(c.ClaimID) AS ClaimType ,
		c.ClaimID
FROM tblClaimantPersonalInfo cpi
	INNER JOIN dbo.tblClaim                  c    ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit               cl   ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit                    l    ON cl.LawsuitID = l.LawsuitID 
	LEFT JOIN tblPlaintiffCounselAddress     pca  ON l.PrimaryNationalPlaintiffCounselID = pca.PlaintiffCounselAddressID
	LEFT JOIN tblDefenseCounselAddress				dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN tblJurisdiction 						j	ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblState                                 s    ON l.StateID = s.StateID
	INNER JOIN (SELECT CaseDeadlineID, MAX(ClaimLawsuitID) AS ClaimLawsuitID 
				FROM tblClaimLawsuitCaseDeadline 
				GROUP BY CaseDeadlineID) clcd ON cl.ClaimLawsuitID = clcd.ClaimLawsuitID
	LEFT JOIN #CaseDeadlineClaimants cdc ON clcd.CaseDeadlineID = cdc.CaseDeadlineID 
UNION
SELECT  CaseDeadlineID,
		ClaimantList, 
		Jurisdiction, 
		l.JurisdictionID,
		DefenseCounsel, 
		l.DefenseCounselAddressID,
		PlaintiffCounsel,
		PlaintiffCounselID,
		s.state, 
		l.StateID,
		dbo.fnClaimTypesByClaimID(cl.ClaimID) AS ClaimType,
		cl.ClaimID
FROM tblClaimLawsuit cl              
	INNER JOIN tblLawsuit							 l    ON cl.LawsuitID = l.LawsuitID 
	LEFT JOIN tblPlaintiffCounselAddress     pca  ON l.PrimaryNationalPlaintiffCounselID = pca.PlaintiffCounselAddressID
	LEFT JOIN tblDefenseCounselAddress				dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN tblJurisdiction 						j	ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblState                                 s    ON l.StateID = s.StateID
	INNER JOIN tblCaseDeadline cd ON cl.TrialGroupID = cd.TrialGroupID
	INNER JOIN tblTrialGroup tg on cd.TrialGroupID = tg.TrialGroupID
	INNER JOIN #TrialGroupClaimantLists tgcl ON cd.TrialGroupID = tgcl.TrialGroupID AND tgcl.ClaimLawsuitID = cl.ClaimLawsuitID
WHERE cd.TrialGroupID IS NOT NULL

DECLARE @CaseDeadlines TABLE 
(
	DeadlineID BIGINT,
    CaseName NVARCHAR(MAX) ,
    State NVARCHAR(250) ,
    PlaintiffCounsel NVARCHAR(250) ,
	CreatedBy NVARCHAR(250) ,
    TrialGroup NVARCHAR(250) ,
	DetailedEventType NVARCHAR(4000),
	ClaimTypeValue NVARCHAR(MAX),
    DeadlineDate NVARCHAR(250) ,
	DeadlineType NVARCHAR(250) ,
	EventType NVARCHAR(250) ,
	Jurisdiction NVARCHAR(250) ,
	DeadlineStatus NVARCHAR(250) ,
	DefenseCounsel NVARCHAR(250),
	EventLocation NVARCHAR(MAX),
	Comments NVARCHAR(MAX),
	Date DATE,
	ClaimID BIGINT,
	AttendanceMode NVARCHAR(50),
	Deponent NVARCHAR(2000),
	Attendees NVARCHAR(2000),
	DeadlineTypeID INT,
	DeadlineTime TIME
)
INSERT INTO @CaseDeadlines
SELECT DISTINCT
cd.CaseDeadlineID, 
CASE WHEN cd.TrialGroupID IS NOT NULL THEN TrialGroup ELSE CaseName END AS CaseName,
	UPPER(COALESCE(cn.State, '')),
	UPPER(COALESCE(PlaintiffCounsel, '')),
	COALESCE(CreatedBy, ''),
	UPPER(COALESCE(TrialGroup, '')),
	COALESCE(UPPER(DetailedEventType), ''),
	COALESCE(UPPER(ClaimType), '') AS ClaimType,
	CASE WHEN DeadlineDate IS NOT NULL 
		 THEN CONVERT(NVARCHAR(15), DeadlineDate, 101) + ' ' ELSE 'TBD' END +  
	CASE WHEN DeadlineTime IS NOT NULL 
		THEN + ' @ ' + 
			CASE WHEN DATEPART(hour,DeadlineTime) in (10, 11, 12, 22, 23, 24) 
			THEN CONVERT(varchar(5),CAST(DeadlineTime AS TIME),109)
			ELSE CONVERT(varchar(4),CAST(DeadlineTime AS TIME),109) END ELSE '' END 
		+ 
	CASE WHEN DeadlineTime IS NOT NULL AND TimePeriod IS NOT NULL 
		THEN + ' ' + TimePeriod ELSE '' END + 
	CASE WHEN DeadlineTime IS NOT NULL AND Abbreviation IS NOT NULL 
		THEN  + ' ' + Abbreviation ELSE '' END AS FullDeadlineDate,
	UPPER(DeadlineType),
	UPPER(EventType),
	UPPER(COALESCE(Jurisdiction, '')),
	UPPER(COALESCE(DeadlineStatus, 'UNKNOWN')),
	UPPER(COALESCE(DefenseCounsel, '')),
	UPPER(
	CASE WHEN LocationName IS NULL THEN '' ELSE LocationName END +
	CASE WHEN LocationName IS NOT NULL AND (Address1 IS NULL AND Address2 IS NULL AND City IS NULL AND s.State IS NULL AND Zip IS NULL) THEN ''
		 WHEN LocationName IS NULL AND Address1 IS NULL AND Address2 IS NULL AND City IS NULL AND s.State IS NULL AND Zip IS NULL THEN ''
			WHEN LocationName IS NULL AND (Address1 IS NOT NULL OR Address2 IS NOT NULL OR City IS NOT NULL OR s.State IS NOT NULL OR Zip IS NOT NULL) THEN '' 
			ELSE ': ' END +
	+ COALESCE(Address1 + ' ', '') 
	+ COALESCE(Address2 + ' ', '') 
	+ COALESCE(City, '') 
	+ CASE WHEN City IS NULL OR (s.State IS NULL AND Zip IS NULL) THEN '' ELSE ', ' END 
	+ COALESCE(s.State + ' ', '')  + COALESCE(Zip, '')
	) AS EventLocation,
	UPPER(COALESCE(Comments, '')),
	DeadlineDate,
	ClaimID,
	UPPER(COALESCE(AttendanceMode, '')),
	UPPER(COALESCE(Attendees, '')),
	UPPER(COALESCE(Deponent, '')),
	et.DeadlineTypeID,
	DeadlineTime
FROM tblCaseDeadline cd
INNER JOIN tblEventType et ON cd.EventTypeID = et.EventTypeID
LEFT JOIN tblDetailedEventType det ON cd.DetailedEventTypeID = det.DetailedEventTypeID
INNER JOIN tblDeadlineType dt on et.DeadlineTypeID = dt.DeadlineTypeID
LEFT JOIN tblTimeZone tz ON cd.TimeZoneID = tz.TimeZoneID
LEFT JOIN tblTimePeriod tp ON cd.TimePeriodID = tp.TimePeriodID
LEFT JOIN tblEventLocation el ON cd.EventLocationID = el.EventLocationID
LEFT JOIN tblAttendanceMode am ON cd.AttendanceModeID = am.AttendanceModeID
LEFT JOIN tblState s ON el.StateID = s.StateID
LEFT JOIN tblDeadlineStatus ds on cd.DeadlineStatusID = ds.DeadlineStatusID
LEFT JOIN tblTrialGroup tg ON cd.TrialGroupID = tg.TrialGroupID
LEFT JOIN #CaseNames cn ON cd.CaseDeadlineID = cn.CaseDeadlineID
WHERE ((DeadlineDate BETWEEN @TodaysDate AND @2WeeksFuture) OR DeadlineDate IS NULL)
ORDER BY DeadlineDate, DeadlineTime


--SELECT *
--FROM @CaseDeadlines

        DECLARE @ClaimantPerDeadline TABLE
            (
              DeadlineID INT ,
              ClaimantList NVARCHAR(MAX)
            )
        DECLARE @DeadlineID BIGINT ,
            @LastDeadlineID INT ,
            @Claimant NVARCHAR(MAX) ,
            @ClaimantID BIGINT ,
            @ClaimantList NVARCHAR(MAX)

        DECLARE Cur_Deadlines CURSOR Local Fast_Forward
        FOR
            SELECT  CaseDeadlineID ,
                    COALESCE(Lastname, '') + ', ' + COALESCE(Firstname, '') ,
                    cpi.claimantpersonalInfoID
            FROM tblClaimLawsuitCaseDeadline clcd
				INNER JOIN tblClaimLawsuit cl ON clcd.ClaimLawsuitID = cl.ClaimLawsuitID
				INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
				INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
			ORDER BY CaseDeadlineID
        OPEN Cur_Deadlines
        FETCH NEXT FROM Cur_Deadlines INTO @DeadlineID, @Claimant, @ClaimantID
        WHILE @@FETCH_STATUS = 0 
            BEGIN


                IF ( LEN(@ClaimantList) = 0
                     OR @ClaimantList IS NULL
                   ) 
                    BEGIN
	
                        SET @ClaimantList = '<ul style="margin:0; padding:0;"> <a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                            + CONVERT(NVARCHAR(50), @claimantID) + '">'
                            + @Claimant 
							+ '</a></ul>'
                    END
                ELSE 
                    BEGIN
	
                        SET @ClaimantList = COALESCE(@ClaimantList, '')
                            + '<ul style="margin:0; padding:0;">  <a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                            + CONVERT(NVARCHAR(50), @claimantID) + '">'
                            + COALESCE(@Claimant, '') 
                            + '</a></ul>'

                    END
                SET @LastDeadlineID = @DeadlineID
                FETCH NEXT FROM Cur_Deadlines INTO @DeadlineID, @Claimant,
                    @ClaimantID
                PRINT @@FETCH_STATUS
                IF @DeadlineID <> @LastDeadlineID
                    OR @@FETCH_STATUS <> 0 
                    BEGIN
	
                        INSERT  INTO @ClaimantPerDeadline
                                SELECT  @LastDeadlineID ,
                                        @ClaimantList
	
                        SET @ClaimantList = NULL
                    END
            END
        CLOSE Cur_Deadlines
        DEALLOCATE Cur_Deadlines
	
DECLARE @BodyMessage NVARCHAR(MAX)

SET @BodyMessage = @LogoHeader
    + '<STYLE TYPE="text/css">
		<!--
		TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
		TD	
		{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
		}
		TH	
		{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
		UL {margin:0; 
	
		padding:0;}	


		--->
		</STYLE>'

        DECLARE @LastLawsuitID NVARCHAR(250) = ''

        IF ( SELECT COUNT(DeadlineType)
             FROM   @CaseDeadlines
           ) > 0 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<H4>Please see the upcoming events/deadlines for the next two weeks: </H4>' --+ '<H4>Please see below for the upcoming Pfizer deadlines and depos for the next two weeks.</H4>'
                SET @BodyMessage = @BodyMessage
                    + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' 
				SET @BodyMessage = @BodyMessage 
					+ N'<tr>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">DEADLINE TYPE</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">DEADLINE DATE</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">CASE NAME</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">PLAINTIFF COUNSEL</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">DEFENSE COUNSEL</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">STATE</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">COUNTY</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">EVENT TYPE</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">EVENT LOCATION</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">CASE TYPE</th>
						<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">DEADLINE STATUS</th>
						'
	
                DECLARE 
					@CaseName NVARCHAR(MAX) ,
					@State NVARCHAR(250) ,
					@PlaintiffCounsel NVARCHAR(250) ,
					@CreatedBy NVARCHAR(250) ,
					@TrialGroup NVARCHAR(250) ,
					@DetailedEventType NVARCHAR(4000),
					@GetClaimTypes NVARCHAR(250),
					@ClaimID BIGINT,
					@DeadlineDate NVARCHAR(250) ,
					@DeadlineType NVARCHAR(250) ,
					@EventType NVARCHAR(250) ,
					@Jurisdiction NVARCHAR(250) ,
					@DeadlineStatus NVARCHAR(250) ,
					@DefenseCounsel NVARCHAR(250),
					@EventLocation NVARCHAR(MAX),
					@Comments NVARCHAR(MAX),
					@AttendanceMode NVARCHAR(50),
					@Deponent NVARCHAR(2000),
					@Attendees NVARCHAR(2000),
					@DeadlineTypeID INT
	
                DECLARE Deadline_List CURSOR
                FOR
                    SELECT  CASE WHEN ClaimantList IS NULL THEN TrialGroup ELSE ClaimantList END AS CaseName,
							State,
							PlaintiffCounsel,
							CreatedBy,
							TrialGroup,
							DetailedEventType,
							ClaimID,
							DeadlineDate,
							DeadlineType,
							EventType,
							Jurisdiction,
							DeadlineStatus,
							DefenseCounsel,
							EventLocation,
							Comments,
							AttendanceMode,
							Attendees, 
							Deponent,
							DeadlineTypeID
                    FROM    @CaseDeadlines C
					 LEFT JOIN @ClaimantPerDeadline cl ON c.DeadlineID = cl.DeadlineID
					ORDER BY Date, DeadlineTime

                OPEN Deadline_List
                FETCH NEXT FROM Deadline_List INTO	@CaseName,
												@State,
												@PlaintiffCounsel,
												@CreatedBy,
												@TrialGroup,
												@DetailedEventType,
												@ClaimID,
												@DeadlineDate,
												@DeadlineType,
												@EventType,
												@Jurisdiction,
												@DeadlineStatus,
												@DefenseCounsel,
												@EventLocation,
												@Comments,
												@AttendanceMode,
												@Attendees, 
												@Deponent,
												@DeadlineTypeID

                WHILE @@FETCH_STATUS = 0 
                    BEGIN
	
		
                        SET @BodyMessage = @BodyMessage + '<tr>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @DeadlineType + ' </td>'
                       SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @DeadlineDate + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top; width:150px;"> '
                            + @CaseName + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @PlaintiffCounsel + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @DefenseCounsel + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @State + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @Jurisdiction + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @EventType + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top; width: 130px;"> '
                            + @EventLocation + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top">'

		
                        DECLARE @ClaimTypes TABLE
                            (
                              ID INT NOT NULL
                                     IDENTITY(1, 1)
                                     PRIMARY KEY ,
                              ClaimID BIGINT ,
                              ClaimTypeValue NVARCHAR(500) ,
                              ClaimTypeValueID INT
                            )
                        INSERT  INTO @ClaimTypes
                                SELECT  c.ClaimID,										
										UPPER(ClaimTypeValue),
										ct.ClaimTypeValueID
                                FROM    tblClaim c
                                        INNER JOIN tblClaimType ct		ON c.ClaimID = ct.ClaimID
                                        INNER JOIN tblClaimTypeValue ctv	ON ct.ClaimTypeValueID = ctv.ClaimTypeValueID
                                WHERE   c.ClaimID = @ClaimID
                                GROUP BY c.ClaimID,
										ct.ClaimTypeValueID,
										ClaimTypeValue
                                ORDER BY ClaimTypeValue
		
                        DECLARE ClaimTypes_curs CURSOR
                        FOR
                            SELECT DISTINCT
                                    ClaimTypeValue
                            FROM    @ClaimTypes
                            WHERE   ClaimID = @ClaimID
			

                        OPEN ClaimTypes_curs
                        FETCH NEXT FROM ClaimTypes_curs INTO @GetClaimTypes
			
                        WHILE @@FETCH_STATUS = 0 
                            BEGIN		
				
                                SET @BodyMessage = @BodyMessage + @GetClaimTypes
                                    + ' <br/>'
				
                                FETCH NEXT FROM ClaimTypes_curs INTO @GetClaimTypes
			
                            END
                        CLOSE ClaimTypes_curs
                        DEALLOCATE ClaimTypes_curs
						SET @BodyMessage = @BodyMessage + '</td>'

                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @DeadlineStatus + ' </td>'

                        FETCH NEXT FROM Deadline_List INTO @CaseName,
												@State,
												@PlaintiffCounsel,
												@CreatedBy,
												@TrialGroup,
												@DetailedEventType,
												@ClaimID,
												@DeadlineDate,
												@DeadlineType,
												@EventType,
												@Jurisdiction,
												@DeadlineStatus,
												@DefenseCounsel,
												@EventLocation,
												@Comments,
												@AttendanceMode,
												@Attendees, 
												@Deponent,
												@DeadlineTypeID

                    END
                CLOSE Deadline_List
                DEALLOCATE Deadline_List
	
	
                SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'
	

            
			END
        ELSE 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<H4>There are no upcoming deadlines.</H4>'
            END

        SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;',
                                   '>')


SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

DECLARE @RecipientList nvarchar(MAX) = ' '
SET @RecipientList = 'Christopher.Campbell@dlapiper.com; William.Hoffman@kayescholer.com; HaydenColeman@quinnemanuel.com; Angela.Vicari@kayescholer.com; Amy.Chung@dlapiper.com; JHealy@tuckerellis.com; JGoldwood@tuckerellis.com; KRoss@tuckerellis.com; WBerglund@tuckerellis.com; MMonroe@tuckerellis.com; DLittler@tuckerellis.com'

--SET @BodyMessage = @BodyMessage + '<br/> @RecipientList = ' + @RecipientList

--SELECT @BodyMessage

--Execute stored procedure to send the email to that specific users
EXEC msdb.dbo.sp_send_dbmail	
@profile_name = 'Claims',
@recipients = @RecipientList,
@copy_recipients = '',
@blind_copy_recipients = 'potterm@kcic.com; monahanc@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailDeadlines_TwoWeeks] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailDeadlines_TwoWeeks] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailDeadlines_TwoWeeks] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailDeadlines_TwoWeeks] TO [power_user]
GO
