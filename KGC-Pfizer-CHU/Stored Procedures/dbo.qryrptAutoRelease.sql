SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptAutoRelease]
(@ClaimantPersonalInfoID bigint, @ClaimLawsuitID bigint)
As
SET NOCOUNT ON

--DECLARE @Claimantpersonalinfoid bigint
--DECLARE @ClaimLawsuitID bigint
--set @Claimantpersonalinfoid = 973519
------set @Claimantpersonalinfoid = 21417 --two claims for one claimant
--Set @ClaimLawsuitID = 2316984

-- Find Spouse for Claimant
Declare @Spouse nvarchar (100)
Declare @SpouseCapacity nvarchar (100)
Declare @SpouseInfo table (Spouse nvarchar (100), SpouseCapacity nvarchar (100))
INSERT INTO @SpouseInfo
	SELECT DISTINCT
		crp.FirstName + ' ' + COALESCE(crp.MiddleName + ' ', '') + crp.LastName AS Spouse,
		COALESCE(lc.LegalCapacity, '[CAPACITY]') AS SpouceCapacity
	FROM tblClaimantPersonalInfo cpi
	INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
	LEFT JOIN tblClaimantRelatedParty crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblLegalCapacity lc ON lc.LegalCapacityID = crp.LegalCapacityID
	LEFT JOIN tblRelationship r ON r.RelationshipID = crp.RelationshipID
	WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND cl.ClaimLawsuitID = @ClaimLawsuitID
	AND (lc.LegalCapacityID IN (19, 24) OR crp.RelationshipID= 9)

SET @Spouse = (SELECT
	Spouse
FROM @SpouseInfo)
SET @SpouseCapacity = (SELECT
	SpouseCapacity
FROM @SpouseInfo)



-- Find Executors for Claimant
DECLARE Executor_Curs CURSOR FOR SELECT DISTINCT
	crp.FirstName + ' ' + COALESCE(crp.MiddleName + ' ', '') + crp.LastName AS RPName,
	COALESCE(lc.LegalCapacity, '[CAPACITY]') AS ExecutorCapacity
FROM tblClaimantPersonalInfo cpi
INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
LEFT JOIN tblClaimDisease cd ON cd.ClaimID = c.ClaimID
LEFT JOIN tblDisease d ON d.DiseaseID = cd.DiseaseID
LEFT JOIN tblHighLevelDisease dhld ON dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
LEFT JOIN tblClaimantSettlement cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblAllegation a ON a.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblDefendantCompany dc ON dc.DefendantCompanyID = a.DefendantCompanyID
LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
LEFT JOIN tblState laws ON laws.StateID = l.StateID
LEFT JOIN tblClaimantRelatedParty crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblLegalCapacity lc ON lc.LegalCapacityID = crp.LegalCapacityID
LEFT JOIN tblRelationship r ON r.RelationshipID = crp.RelationshipID
LEFT JOIN tblClaimantEmployment ce ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblPlaintiffCounselAddress pc ON pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
LEFT JOIN tblCourtType ct ON ct.CourtTypeID = l.CourtTypeID
LEFT JOIN tblJurisdiction j ON j.JurisdictionID = l.jurisdictionID
LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID


WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
AND cl.ClaimLawsuitID = @ClaimLawsuitID
AND lc.LegalCapacityID NOT IN (19, 24)

DECLARE @ExecutorName nvarchar (100)
DECLARE @ExecutorCapacity nvarchar (100)
Declare @AllExecutorNames nvarchar (200)
Declare @AllExecutorCapacities nvarchar (100)

----Open Cursor to loop through executors
OPEN Executor_Curs
FETCH NEXT FROM Executor_Curs INTO @ExecutorName, @ExecutorCapacity
WHILE @@fetch_status = 0
BEGIN

If @AllExecutorNames is null
Begin
SET @AllExecutorNames = @ExecutorName
End
Else
Begin
SET @AllExecutorNames = @AllExecutorNames + ', ' + @ExecutorName
End

If @AllExecutorCapacities is null
Begin
SET @AllExecutorCapacities = @ExecutorCapacity
End
Else
Begin
If @AllExecutorCapacities = @ExecutorCapacity
Begin
SET @AllExecutorCapacities = @AllExecutorCapacities
End
Else
Begin
SET @AllExecutorCapacities = '[MULTIPLE CAPACITIES FOR EXECUTORS]'
End
End

FETCH NEXT FROM Executor_Curs INTO @ExecutorName, @ExecutorCapacity
END
CLOSE Executor_Curs
DEALLOCATE Executor_Curs

--Select Information about Claimant for Release
SELECT DISTINCT
	--LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4) AS SSN,
	'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) AS SSN,
	cpi.FirstName,
	COALESCE(cpi.MiddleName + ' ', '') AS MiddleName,
	cpi.LastName,
	COALESCE(Suffix, '') AS Suffix,
	--laws.State,
	'TX' as State,
	pc.PlaintiffCounsel,
	pc.State AS PCState,
	laws.StateID,
	COALESCE(@AllExecutorNames, '[EXECUTOR]') AS Executors,
	COALESCE(@AllExecutorCapacities, '[CAPACITY]') AS ExecutorCapacity,
	COALESCE(@Spouse, '[SPOUSE]') AS Spouse,
	COALESCE(@SpouseCapacity, '[CAPACITY]') AS SpouseCapacity,
	CourtType,
	Jurisdiction,
	CaseCaption,
	DocketNumber,
	cl.ClaimLawsuitID,
	ReleaseTemplateTypeID,
	c.MatterID,
	SettlementGroup,
	cpi.INJURED_PARTY_ID as InjuredPartyID,
	DATENAME(month,cs.SettlementDate) +' '+DATENAME(DAY, cs.SettlementDate)+', '+DATENAME(year, cs.SettlementDate) AS SettlementDate,
	DATENAME(month,GETDATE()) +' '+DATENAME(DAY, GETDATE())+', '+DATENAME(year, GETDATE()) AS CurrentDate,
	cs.SettlementAmount,
	UPPER([KGA-ClaimsAdmin].dbo.Num_ToWords(SettlementAmount)) as WordAmount,
	CASE WHEN l.PlaintiffCounselAddressID = 109 THEN 'BRYAN O. BLEVINS, JR., ESQ.' END AS Attorney
	



FROM tblClaimantPersonalInfo cpi
INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
LEFT JOIN tblClaimDisease cd ON cd.ClaimID = c.ClaimID
LEFT JOIN tblDisease d ON d.DiseaseID = cd.DiseaseID
LEFT JOIN tblHighLevelDisease dhld ON dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
LEFT JOIN tblClaimantSettlement cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblAllegation a ON a.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblDefendantCompany dc ON dc.DefendantCompanyID = a.DefendantCompanyID
LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
LEFT JOIN tblState laws ON laws.StateID = l.StateID
LEFT JOIN tblClaimantRelatedParty crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblLegalCapacity lc ON lc.LegalCapacityID = crp.LegalCapacityID
LEFT JOIN tblRelationship r ON r.RelationshipID = crp.RelationshipID
LEFT JOIN tblClaimantEmployment ce ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblPlaintiffCounselAddress pc ON pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
LEFT JOIN tblCourtType ct ON ct.CourtTypeID = l.CourtTypeID
LEFT JOIN tblJurisdiction j ON j.JurisdictionID = l.jurisdictionID
LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID
LEFT JOIN tblSettlementGroup sg ON sg.SettlementGroupID = cs.SettlementGroupID
LEFT JOIN tblClaimantSuffix csuf ON csuf.SuffixID = cpi.SuffixID


WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
AND cl.ClaimLawsuitID = @ClaimLawsuitID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoRelease] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAutoRelease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoRelease] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptAutoRelease] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoRelease] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAutoRelease] TO [power_user]
GO
