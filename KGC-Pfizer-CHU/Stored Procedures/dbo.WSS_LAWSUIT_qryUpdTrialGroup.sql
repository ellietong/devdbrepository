SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[WSS_LAWSUIT_qryUpdTrialGroup]
(@TrialGroupID int, @TrialGroupName nvarchar(max), @SPUsername nvarchar(250))
AS
BEGIN
	
	UPDATE tblTrialGroup
	SET TrialGroup = @TrialGroupName,
		spusername = @SPUsername
	WHERE TrialGroupID = @TrialGroupID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryUpdTrialGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryUpdTrialGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryUpdTrialGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryUpdTrialGroup] TO [power_user]
GO
