SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryPendingMattersActiveByStateAndJurisdiction_OLD]
	-- Add the parameters for the stored procedure here
(@ClaimTypeValueID int = null, @EmailAddress nvarchar(250))		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
DECLARE @MaxClaimTypeValueID int
SET @MaxClaimTypeValueID = (SELECT MAX(ClaimTypeValueID) FROM tblClaimTypeValue)

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT (dca.NoFilters | vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
					
--DECLARE @InsurerRole bit
--SET @InsurerRole = (select distinct InsurerRole from vtblPerson WHERE Email = @emailaddress)

--KCIC & 'NoFilters' people/firms		
IF(RIGHT(@EmailAddress, 11) = 'kcicllc.com' OR RIGHT(@EmailAddress, 8) = 'kcic.com' OR @NoFilters = 1)
BEGIN
    
select FullNameState As State, NULL AS Combination,  COUNT(distinct(cl.ClaimLawsuitID)) AS Count, COUNT(distinct(cl.ClaimLawsuitID)) AS StateCount
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls on clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
	WHERE 
	 (IsPrimaryStatus = 1 AND cls.ClaimLawsuitStatusGroupID = 3) AND c.ClaimSequenceID in (4,5)
	AND ClaimTypeValueID >= COALESCE(@ClaimTypeValueID, 0) AND ClaimTypeValueID <= COALESCE(@ClaimTypeValueID, @MaxClaimTypeValueID)
group by FullNameState
--order by COUNT(distinct(cl.ClaimLawsuitID)) desc
UNION
select UPPER(s.FullNameState), Jurisdiction AS Combination , COUNT(distinct(cl.ClaimLawsuitID)) AS Count, 0
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls on clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
INNER JOIN vREPORTS_StateJurisdictionCombinations sjc on l.JurisdictionID = sjc.JurisdictionID
	WHERE 
	 (IsPrimaryStatus = 1 AND cls.ClaimLawsuitStatusGroupID = 3) AND c.ClaimSequenceID in (4,5)
	AND sjc.StateID IN (17, 31, 37, 45)
group by s.FullNameState, Jurisdiction
order by s.FullNameState, Combination

END

ELSE --Defense Counsel
BEGIN


select FullNameState As State, NULL AS Combination,  COUNT(distinct(cl.ClaimLawsuitID)) AS Count, COUNT(distinct(cl.ClaimLawsuitID)) AS StateCount
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls on clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
	WHERE 
	 (IsPrimaryStatus = 1 AND cls.ClaimLawsuitStatusGroupID = 3) AND c.ClaimSequenceID in (4,5)
	 AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
	AND ClaimTypeValueID >= COALESCE(@ClaimTypeValueID, 0) AND ClaimTypeValueID <= COALESCE(@ClaimTypeValueID, @MaxClaimTypeValueID)
group by FullNameState
--order by COUNT(distinct(cl.ClaimLawsuitID)) desc
UNION
select s.FullNameState, Jurisdiction AS Combination , COUNT(distinct(cl.ClaimLawsuitID)) AS Count, 0
from tblClaim c 
INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls on clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
LEFT JOIN tblState s on l.StateID=s.StateID
LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
INNER JOIN vREPORTS_StateJurisdictionCombinations sjc on l.JurisdictionID = sjc.JurisdictionID
INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
	WHERE 
	 (IsPrimaryStatus = 1 AND cls.ClaimLawsuitStatusGroupID = 3) AND c.ClaimSequenceID in (4,5)
	 AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
	AND sjc.StateID IN (17, 31, 37, 45)
group by s.FullNameState, Jurisdiction
order by s.FullNameState, Combination



--select FullNameState As State, COUNT(distinct(cl.ClaimLawsuitID)) AS Count
--from tblClaim c 
--INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
--INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
--INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
--INNER JOIN tblClaimLawsuitStatus cls on clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
--LEFT JOIN tblState s on l.StateID=s.StateID
--INNER JOIN tblDefenseJurisdiction dj ON l.JurisdictionID = dj.JurisdictionID AND l.StateID = dj.StateID
--LEFT JOIN tblClaimType ct ON c.ClaimID = ct.ClaimID
--	WHERE 
--	 (IsPrimaryStatus = 1 AND cls.ClaimLawsuitStatusGroupID = 3) AND c.ClaimSequenceID in (4,5)
--	AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID)
--	AND ct.ClaimTypeValueID >= COALESCE(@ClaimTypeValueID, 0) AND ct.ClaimTypeValueID <= COALESCE(@ClaimTypeValueID, @MaxClaimTypeValueID)
--group by FullNameState
--order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END

END





GO
