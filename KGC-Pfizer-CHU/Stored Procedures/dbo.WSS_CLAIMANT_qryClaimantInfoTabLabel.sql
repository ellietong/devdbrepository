SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryClaimantInfoTabLabel]
    (
      @ClaimantPersonalInfoID INT = NULL ,
      @emailaddress NVARCHAR(1000) = NULL
    )
AS 
    SET NOCOUNT ON 

--DECLARE @ClaimantPersonalInfoID int
--DECLARE @emailaddress nvarchar(1000)
--SET @ClaimantPersonalInfoID = 6940
--SET @emailaddress           = 'owenss@kcic.com'

    DECLARE @AllResponsibleDefenseCounsel TABLE
        (
          DefenseCounselAddressID INT ,
          DefenseCounsel NVARCHAR(100) ,
          ResponsibleDefenseCounselAddressID INT ,
          StateID INT ,
          ResponsibleDefenseCounsel NVARCHAR(100) ,
          IsActiveCounsel BIT
        )
											 
    IF ( RIGHT(@emailaddress, 11) = 'kcicllc.com'
         OR LEFT(@emailaddress, 4) = 'kcic'
		 OR ( RIGHT(@emailaddress, 8) = 'kcic.com' )
       ) 
        BEGIN
            INSERT  INTO @AllResponsibleDefenseCounsel
                    SELECT  dj.DefenseCounselAddressID ,
                            d.DefenseCounsel AS DefenseCounsel ,
                            dj.DefenseCounselAddressID ,
                            StateID AS StateID ,
                            d.DefenseCounsel AS ResponsibleDefenseCOunsel ,
                            IsActive
                    FROM    tblDefenseJurisdiction dj
                            INNER JOIN tblDefenseCounselAddress da ON da.DefenseCounselAddressID = dj.DefenseCounselAddressID
                            INNER JOIN tblDefenseCounsel d ON d.defensecounselID = da.defensecounselID
                    GROUP BY d.DefenseCounsel ,
                            d.DefenseCounsel ,
                            dj.DefenseCounselAddressID ,
                            IsActive ,
                            StateID
        END
    ELSE 
        BEGIN
            INSERT  INTO @AllResponsibleDefenseCounsel
                    SELECT  dj.DefenseCounselAddressID ,
                            d.DefenseCounsel AS DefenseCounsel ,
                            dj.DefenseCounselAddressID ,
                            StateID AS StateID ,
                            d.DefenseCounsel AS ResponsibleDefenseCOunsel ,
                            IsActive
                    FROM    tblDefenseJurisdiction dj
                            INNER JOIN tblDefenseCounselAddress da ON da.DefenseCounselAddressID = dj.DefenseCounselAddressID
                            INNER JOIN tblDefenseCounsel d ON d.defensecounselID = da.defensecounselID
                    WHERE   d.DefenseCounselID = ( SELECT   DefenseCounselID
                                                   FROM     tblDefenseCounsel dc
                                                            INNER JOIN vtblPerson p ON p.FirmID = dc.DefenseCounselID
                                                   WHERE    ( email = @emailaddress )
                                                 )
                    GROUP BY d.DefenseCounsel ,
                            d.DefenseCounsel ,
                            dj.DefenseCounselAddressID ,
                            IsActive ,
                            StateID
        END

    IF @ClaimantPersonalInfoID IS NULL 
        BEGIN
            SELECT  CASE WHEN m.LawsuitID = 50513 THEN ' CLAIMANT NOT FOUND'
                         ELSE cpi.LastName + ', ' + cpi.FirstName
                    END AS Claimant ,
                    cpi.ClaimantPersonalInfoID ,
                    FirstName ,
                    LastName
            FROM    dbo.tblLawsuit m
                    INNER JOIN @AllResponsibleDefenseCounsel dc ON m.StateID = dc.StateID
                                                              AND m.DefenseCounselAddressID = dc.DefenseCounselAddressID
                    INNER JOIN tblClaimLawsuit cl ON m.LawsuitID = cl.LawsuitID
                    INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
                    INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
            GROUP BY CASE WHEN m.LawsuitID = 50513 THEN ' CLAIMANT NOT FOUND'
                          ELSE cpi.LastName + ', ' + cpi.FirstName
                     END ,
                    cpi.ClaimantPersonalInfoID ,
                    FirstName ,
                    LastName
            ORDER BY Claimant
        END
    ELSE 
        BEGIN
            SELECT  CASE WHEN m.LawsuitID = 50513 THEN ' CLAIMANT NOT FOUND'
                         ELSE cpi.LastName + ', ' + cpi.FirstName
                    END AS Claimant ,
                    cpi.ClaimantPersonalInfoID ,
                    FirstName ,
                    LastName
            FROM    dbo.tblLawsuit m
                    LEFT JOIN tblDefenseCounselState s ON m.DefenseCounselAddressID = s.DefenseCounselAddressID
                    INNER JOIN tblClaimLawsuit cl ON m.LawsuitID = cl.LawsuitID
                    INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
                    INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
            WHERE   cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
            GROUP BY CASE WHEN m.LawsuitID = 50513 THEN ' CLAIMANT NOT FOUND'
                          ELSE cpi.LastName + ', ' + cpi.FirstName
                     END ,
                    cpi.ClaimantPersonalInfoID ,
                    FirstName ,
                    LastName
            ORDER BY Claimant
        END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantInfoTabLabel] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantInfoTabLabel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantInfoTabLabel] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantInfoTabLabel] TO [power_user]
GO
