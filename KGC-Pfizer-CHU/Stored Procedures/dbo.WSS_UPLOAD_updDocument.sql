SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_updDocument]
    (
      @ClaimantDocumentTypeID INT ,
      @Comments NVARCHAR(4000) = NULL ,			--comments are optional		
      @DocumentURL NVARCHAR(4000) ,
      @DocumentTitle NVARCHAR(4000) ,
      @DocID INT ,
	  @DocumentDate DATETIME,
	
	--These are null unless the document is an Invoice document
      @InvoiceNumber NVARCHAR(50) = NULL ,
      @Period NVARCHAR(50) = NULL ,
      @DatePayable DATETIME = NULL ,
      @FeeAmount MONEY = NULL ,
      @DisbursementAmount MONEY = NULL ,
      @ModifiedBy NVARCHAR(250) ,
      @DateModified DATETIME ,
      @Approved BIT = NULL ,
      @ApprovedBy NVARCHAR(50) = NULL ,
      @DateApproved DATETIME = NULL ,
      @DocumentSourceID INT ,
      @PrivateComments NVARCHAR(4000)
    )
AS 
    UPDATE  tblDocuments
    SET     ClaimantDocumentTypeID = @ClaimantDocumentTypeID ,
            Comments = @Comments ,
            DocumentURL = @DocumentURL ,
            DocumentTitle = @DocumentTitle ,
            ModifiedBy = @ModifiedBy ,
            DateModified = @DateModified ,
            DocumentSourceID = @DocumentSourceID,
			DocumentDate = @DocumentDate
    WHERE   DocumentID = @DocID

    UPDATE  tblDefenseInvoice
    SET     InvoiceNumber = @InvoiceNumber ,
            Period = @Period ,
            DatePayable = @DatePayable
    WHERE   DocumentID = @DocID

    SELECT  DocumentID
    FROM    tblDocuments
    WHERE   DocumentID = @DocID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocument] TO [power_user]
GO
