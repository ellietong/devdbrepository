SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryHighLevelDisease]
AS

SELECT NULL AS HighLevelDiseaseID, NULL AS HighLevelDisease
UNION
SELECT HighLevelDiseaseID, HighLevelDisease
FROM tblHighLevelDisease
ORDER BY HighLevelDisease
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryHighLevelDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryHighLevelDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryHighLevelDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryHighLevelDisease] TO [power_user]
GO
