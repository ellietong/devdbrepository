SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_REPORT_qryCaseDeadlineReport]
    (
      @ClaimantName NVARCHAR(MAX) ,
      @StateID INT ,
      @PlaintiffCounselID INT ,
	  @CreatedBy NVARCHAR(250) ,
      @Emailaddress NVARCHAR(250) ,
      @TrialGroupID INT = NULL,
	  @DetailedEventType NVARCHAR(4000),
	  --@ClaimTypeValueID INT,
	  @ClaimTypeValue NVARCHAR(250),
      @DeadlineCreatedStart DATETIME ,
      @DeadlineCreatedEnd DATETIME ,
	  @DeadlineDateStart DATETIME,
	  @DeadlineDateEnd DATETIME,
	  @DeadlineTypeID INT,
	  @EventTypeID INT,
	  @JurisdictionID INT,
	  @DeadlineStatusID INT,
	  @DefenseCounselAddressID INT
    )
AS 
    SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantName              NVARCHAR(max)
--DECLARE @SSN                       NVARCHAR(20)
--DECLARE @StateID                   int
--DECLARE @PlaintiffCounselID		 int
--DECLARE @DefendantCompanyID		 bigint
--DECLARE @InjPartyID                bigint
--DECLARE @MatterID                  int
--DECLARE @ClaimTypeValueID			 int
--DECLARE @IsMDL                     int
--DECLARE @Emailaddress              NVARCHAR(250)
--DECLARE @DocketNumber              NVARCHAR(250)
--DECLARE @ClaimLawsuitStatusGroupID int
--DECLARE @TrialGroupID			     int
--DECLARE @SettlementGroupTypeID	 int
--DECLARE @ClaimSequenceID		     int
--DECLARE @ClaimantIDs               TABLE (n int) 

--SET @ClaimantName              = NULL
--SET @SSN                       = NULL
--SET @StateID                   = 52
--SET @PlaintiffCounselID		 = NULL
--SET @DefendantCompanyID		 = NULL
--SET @InjPartyID                = NULL
--SET @MatterID                  = NULL
--SET @ClaimTypeValueID			 = NULL
--SET @IsMDL                     = NULL
--SET @Emailaddress              = 'owenss@kcic.com'
--SET @DocketNumber              = NULL
--SET @ClaimLawsuitStatusGroupID = 3
--SET @TrialGroupID				 = NULL
--SET @SettlementGroupTypeID	 = NULL
--SET @ClaimSequenceID		     = NULL
----INSERT INTO @ClaimantIDs
----SELECT 296938	

    DECLARE @UserDefenseCounselAddressID INT
    SET @UserDefenseCounselAddressID = ( SELECT FirmAddressID
                                     FROM   vtblPerson
                                     WHERE  email = @Emailaddress
                                   )
DECLARE @tdStyleW NVARCHAR(MAX) = 'font-family: verdana,arial,helvetica,sans-serif; font-size: 8pt; text-align: left; border: none; padding: .1em; vertical-align: left; background-color: transparent;'
DECLARE @tdStyleB NVARCHAR(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
    IF OBJECT_ID('tempdb..#CaseDeadlineClaimants') IS NOT NULL 
        DROP TABLE #CaseDeadlineClaimants
    CREATE TABLE #CaseDeadlineClaimants
        (
			CaseDeadlineID int,
			ClaimantName NVARCHAR(max)
        )
INSERT INTO #CaseDeadlineClaimants
SELECT DISTINCT cci.casedeadlineID,--cci.ClaimantPersonalInfoID,
		COALESCE((SELECT N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0; width:200px;">' + 	
		CAST((SELECT @tdStyleW AS 'td/@style', (SELECT COALESCE(	'<a target="_blank" href="/clients/Pfizer/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + COALESCE(CONVERT(NVARCHAR(10), cpi.ClaimantPersonalInfoID), '') + '">' + (COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '')) + '</a>' --+ ' (' + COALESCE(CONVERT(NVARCHAR(10), cpi.ClaimantPersonalInfoID), '') + ')') + '</a>'
, '')) as td, ''
 
FROM tblClaimLawsuitCaseDeadline clcd
INNER JOIN tblClaimLawsuit cl ON clcd.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID						 
		WHERE cci.caseDeadlineid = clcd.caseDeadlineid 
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) )) +  N'</table>' +  N'<br/>'	, 'UNKNOWN') AS Missing
FROM tblCaseDeadline cci
where TrialGroupID is null

IF object_id('tempdb..#TrialGroupClaimantLists') IS NOT NULL
BEGIN
	DROP TABLE #TrialGroupClaimantLists
END
CREATE TABLE #TrialGroupClaimantLists (TrialGroupID int, ClaimantList NVARCHAR(MAX), ClaimLawsuitID bigint)
BEGIN
	INSERT INTO #TrialGroupClaimantLists
	SELECT DISTINCT cd.TrialGroupID,
		'<ul>' + CONVERT(NVARCHAR(MAX), (	SELECT 
					--'a/@class' = 'anchorStyle',
					--'a' = 
					cpi.LastName + ', ' + cpi.FirstName	
			FROM tblTrialGroup tgroup
				INNER JOIN tblClaimLawsuit cl ON tgroup.TrialGroupID = cl.TrialGroupID
				INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
				INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
			WHERE tgroup.TrialGroupID = cd.TrialGroupID
			GROUP by cpi.ClaimantPersonalInfoID, LastName, FirstName
			FOR XML PATH('li')
		)) + '</ul>', (select MAX(ClaimLawsuitID) AS ClaimLawsuitID from tblClaimLawsuit where TrialGroupID = cd.TrialGroupID)

	FROM tblCaseDeadline cd
	WHERE cd.TrialGroupID IS NOT NULL --on tg.TrialGroupID = cd.TrialGroupID
	GROUP BY cd.TrialGroupID
END

IF OBJECT_ID('tempdb..#CaseNames') IS NOT NULL 
    DROP TABLE #CaseNames
CREATE TABLE #CaseNames
    (
		CaseDeadlineID bigint,
		CaseName NVARCHAR(max),
		Jurisdiction NVARCHAR(250),
		JurisdictionID int,
		DefenseCounsel NVARCHAR(250),
		DefenseCounselAddressID int,
		PlaintiffCounsel NVARCHAR(250),
		PlaintiffCounselID int,
		State NVARCHAR(25),
		StateID int,
		ClaimType NVARCHAR(max)
			
    )
INSERT INTO #CaseNames
SELECT DISTINCT  clcd.CaseDeadlineID, 
		--LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, 
		ClaimantName,
		Jurisdiction, 
		l.JurisdictionID,
		DefenseCounsel, 
		l.DefenseCounselAddressID,
		PlaintiffCounsel,
		PlaintiffCounselID,
		s.state, 
		l.StateID,
		dbo.fnClaimTypesByClaimID(c.ClaimID) AS ClaimType 
FROM tblClaimantPersonalInfo cpi
	INNER JOIN dbo.tblClaim                  c    ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit               cl   ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit                    l    ON cl.LawsuitID = l.LawsuitID 
	LEFT JOIN tblPlaintiffCounselAddress     pca  ON l.PrimaryNationalPlaintiffCounselID = pca.PlaintiffCounselAddressID
	LEFT JOIN tblDefenseCounselAddress				dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN tblJurisdiction 						j	ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblState                                 s    ON l.StateID = s.StateID
	INNER JOIN (SELECT CaseDeadlineID, MAX(ClaimLawsuitID) AS ClaimLawsuitID 
				FROM tblClaimLawsuitCaseDeadline 
				GROUP BY CaseDeadlineID) clcd ON cl.ClaimLawsuitID = clcd.ClaimLawsuitID
	LEFT JOIN #CaseDeadlineClaimants cdc ON clcd.CaseDeadlineID = cdc.CaseDeadlineID 
UNION
SELECT  CaseDeadlineID,
		ClaimantList, 
		Jurisdiction, 
		l.JurisdictionID,
		DefenseCounsel, 
		l.DefenseCounselAddressID,
		PlaintiffCounsel,
		PlaintiffCounselID,
		s.state, 
		l.StateID,
		dbo.fnClaimTypesByClaimID(cl.ClaimID) AS ClaimType 
FROM tblClaimLawsuit cl              
	INNER JOIN tblLawsuit							 l    ON cl.LawsuitID = l.LawsuitID 
	LEFT JOIN tblPlaintiffCounselAddress     pca  ON l.PrimaryNationalPlaintiffCounselID = pca.PlaintiffCounselAddressID
	LEFT JOIN tblDefenseCounselAddress				dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	LEFT JOIN tblJurisdiction 						j	ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblState                                 s    ON l.StateID = s.StateID
	INNER JOIN tblCaseDeadline cd ON cl.TrialGroupID = cd.TrialGroupID
	INNER JOIN tblTrialGroup tg on cd.TrialGroupID = tg.TrialGroupID
	INNER JOIN #TrialGroupClaimantLists tgcl ON cd.TrialGroupID = tgcl.TrialGroupID AND tgcl.ClaimLawsuitID = cl.ClaimLawsuitID
WHERE cd.TrialGroupID IS NOT NULL
	
DECLARE @TodaysDate date = (SELECT DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))
DECLARE @2WeeksFuture date = (SELECT DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))+14)


--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE 1=1 '

IF ((SELECT SUM(COALESCE(@StateID, 
						@PlaintiffCounselID,						
						@TrialGroupID,
						CONVERT(INT, CONVERT(VARCHAR(10), @DeadlineCreatedStart, 112)),
						CONVERT(INT, CONVERT(VARCHAR(10), @DeadlineCreatedEnd, 112)),
						CONVERT(INT, CONVERT(VARCHAR(10), @DeadlineDateStart, 112)),
						CONVERT(INT, CONVERT(VARCHAR(10), @DeadlineDateEnd, 112)),
						@DeadlineTypeID,
						@EventTypeID,
						@JurisdictionID,
						@DeadlineStatusID,
						@DefenseCounselAddressID, 0))) = 0 
						AND @CreatedBy IS NULL AND @ClaimTypeValue IS NULL AND @ClaimantName IS NULL AND @DetailedEventType IS NULL)
SET @WhereString = @WhereString + 
	+ ' AND ((DeadlineDate BETWEEN @TodaysDateParam AND @2WeeksFutureParam) OR DeadlineDate IS NULL)'
                

    IF @ClaimantName IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND (CaseName like ''%'' + @ClaimantNameParam + ''%'')'
        END
    IF @DefenseCounselAddressID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), @DefenseCounselAddressID)
        END
    IF @StateID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND cn.StateID = '
                + CONVERT(NVARCHAR(20), @StateID)
        END
    IF @PlaintiffCounselID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND PlaintiffCounselID ='
                + CONVERT(NVARCHAR(20), @PlaintiffCounselID)
        END

    IF @TrialGroupID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND cd.TrialGroupID = '
                + CONVERT(NVARCHAR(20), @TrialGroupID)
        END

    IF @ClaimTypeValue IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + 
			+ ' AND ClaimType LIKE ''%'' + @ClaimTypeParam + ''%'''
        END

    IF @DeadlineTypeID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND dt.DeadlineTypeID = '
                + CONVERT(NVARCHAR(20), @DeadlineTypeID)
        END

    IF @EventTypeID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND cd.EventTypeID = '
                + CONVERT(NVARCHAR(20), @EventTypeID)
        END

    IF @DetailedEventType IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND DetailedEventType LIKE ''%'' + @DetailedEventTypeParam + ''%'''
        END

    IF @JurisdictionID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND JurisdictionID = '
                + CONVERT(NVARCHAR(20), @JurisdictionID)
        END

    IF @DeadlineStatusID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND cd.DeadlineStatusID = '
                + CONVERT(NVARCHAR(20), @DeadlineStatusID)
        END

    IF @CreatedBy IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND cd.CreatedBy LIKE ''%'' + @CreatedByParam + ''%'''
        END

    IF @DeadlineCreatedStart IS NOT NULL
        OR @DeadlineCreatedEnd IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND COALESCE(CONVERT(date,cd.DateCreated), ''1/1/1900'') >= '''
                + CONVERT(NVARCHAR, COALESCE(@DeadlineCreatedStart, '1/1/1900'))
                + ''' AND COALESCE(CONVERT(date,cd.DateCreated), CONVERT(date, GETDATE())) <= '''
                + CONVERT(NVARCHAR, COALESCE(@DeadlineCreatedEnd,
                                             CONVERT(DATE, GETDATE()))) + ''''
        END

    IF @DeadlineDateStart IS NOT NULL
        OR @DeadlineDateEnd IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString
                + ' AND COALESCE(CONVERT(date,cd.DeadlineDate), ''1/1/1900'') >= '''
                + CONVERT(NVARCHAR, COALESCE(@DeadlineDateStart, '1/1/1900'))
                + ''' AND COALESCE(CONVERT(date,cd.DeadLineDate), CONVERT(date, GETDATE())) <= '''
                + CONVERT(NVARCHAR, COALESCE(@DeadlineDateEnd,
                                             CONVERT(DATE, GETDATE()))) + ''''
        END

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
    DECLARE @JoinString NVARCHAR(MAX)
    IF ( (RIGHT(@Emailaddress, 11) = 'kcicllc.com' OR RIGHT(@Emailaddress, 8) = 'kcic.com')
         OR ( ( SELECT  NoFilters
                FROM    vtblPerson
                WHERE   Email = @Emailaddress
              ) = 1 )
       ) 
        SET @JoinString = ''
    ELSE 
        BEGIN
            SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
            SET @WhereString = @WhereString
                + ' AND (DefenseCounselAddressID = '
                + CONVERT(NVARCHAR(20), COALESCE(@UserDefenseCounselAddressID, 0))
                + ')'
        END


--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
    DECLARE @QueryString NVARCHAR(MAX)
    SET @QueryString = '
SELECT DISTINCT --TOP 10
	cd.CaseDeadlineID,
	CASE WHEN cd.TrialGroupID is null then CaseName END AS CaseName,
	dt.DeadlineTypeID,
	UPPER(DeadlineType) AS DeadlineType,
	cd.EventTypeID,
	UPPER(EventType) AS EventType,
	cd.DetailedEventTypeID,
	UPPER(DetailedEventType) AS DetailedEventType,
	CASE WHEN DeadlineDate IS NOT NULL 
		 THEN CONVERT(NVARCHAR(15), DeadlineDate, 101) + '' '' ELSE ''TBD'' END +  
	CASE WHEN DeadlineTime IS NOT NULL 
		THEN + '' @ '' + 		
	CASE WHEN DATEPART(hour,DeadlineTime) in (10, 11, 12, 22, 23, 24) 
		THEN CONVERT(varchar(5),CAST(DeadlineTime AS TIME),109)
	ELSE CONVERT(varchar(4),CAST(DeadlineTime AS TIME),109) END
		 ELSE '''' END + 
	CASE WHEN DeadlineTime IS NOT NULL AND TimePeriod IS NOT NULL 
		THEN + '' '' + TimePeriod ELSE '''' END + 
	CASE WHEN DeadlineTime IS NOT NULL AND Abbreviation IS NOT NULL 
		THEN  + '' '' + Abbreviation ELSE '''' END AS FullDeadlineDate,
	DeadlineDate,
	LEFT(DeadlineTime, 5) AS DeadlineTime,
	cd.TimePeriodID,
	TimePeriod,
	cd.TimeZoneID,
	TimeZone,
	UPPER(PlaintiffCounsel) AS PlaintiffCounsel,
	PlaintiffCounselID,
	UPPER(DefenseCounsel) AS DefenseCounsel,
	DefenseCounselAddressID,
	cn.State, 
	UPPER(Jurisdiction) AS County,
	cd.EventLocationID,
	CASE WHEN cd.EventLocationID IS NOT NULL THEN 
		UPPER(
		CASE WHEN LocationName IS NULL THEN '''' ELSE LocationName END +
		CASE WHEN LocationName IS NOT NULL AND (Address1 IS NULL AND Address2 IS NULL AND City IS NULL AND s.State IS NULL AND Zip IS NULL) THEN ''''
			 WHEN LocationName IS NULL AND (Address1 IS NOT NULL OR Address2 IS NOT NULL OR City IS NOT NULL OR s.State IS NOT NULL OR Zip IS NOT NULL) THEN '''' 
			 ELSE '': '' END +
		+ COALESCE(Address1 + '' '', '''') 
		+ COALESCE(Address2 + '' '', '''') 
		+ COALESCE(City, '''') 
		+ CASE WHEN City IS NULL OR (s.State IS NULL AND Zip IS NULL) THEN '''' ELSE '', '' END 
		+ COALESCE(s.State + '' '', '''')  + COALESCE(Zip, '''')
		) END AS EventLocation,	 
	--(COALESCE(LocationName+'':'', '''') + ''<br/>'' + COALESCE(Address1, '''') + '' '' + COALESCE(Address2, '''') + ''<br/>'' +  COALESCE(City, '''') + '', '' + COALESCE(s.State, '''')  + '' '' + COALESCE(Zip, '''')) END AS EventLocation,
	LocationName,
	Deponent,
	Attendees,
	cd.AttendanceModeID,
	AttendanceMode,
	CreatedBy,
	DateCreated,
	Comments,
	UPPER(ClaimType) AS ClaimType,
	cd.DeadlineStatusID,
	UPPER(DeadlineStatus) AS DeadlineStatus,
	cd.TrialGroupID,
	CASE WHEN cd.TrialGroupID IS NOT NULL 
		 THEN 	''<a target="_blank" OnClick="getTrialGroupClaimants('' + CONVERT(NVARCHAR, cd.CaseDeadlineID) + '','' + CONVERT(NVARCHAR, cd.TrialGroupID) + '', event );" class="anchorStyle">'' + (COALESCE(UPPER(TrialGroup), '''')) + ''</a>''
	END AS TrialGroup
FROM tblCaseDeadline cd
INNER JOIN tblEventType et ON cd.EventTypeID = et.EventTypeID
LEFT JOIN tblDetailedEventType det ON cd.DetailedEventTypeID = det.DetailedEventTypeID
INNER JOIN tblDeadlineType dt on et.DeadlineTypeID = dt.DeadlineTypeID
LEFT JOIN tblTimeZone tz ON cd.TimeZoneID = tz.TimeZoneID
LEFT JOIN tblTimePeriod tp ON cd.TimePeriodID = tp.TimePeriodID
LEFT JOIN tblEventLocation el ON cd.EventLocationID = el.EventLocationID
LEFT JOIN tblAttendanceMode am ON cd.AttendanceModeID = am.AttendanceModeID
LEFT JOIN tblState s ON el.StateID = s.StateID
LEFT JOIN tblDeadlineStatus ds on cd.DeadlineStatusID = ds.DeadlineStatusID
LEFT JOIN tblTrialGroup tg ON cd.TrialGroupID = tg.TrialGroupID
INNER JOIN #CaseNames cn ON cd.CaseDeadlineID = cn.CaseDeadlineID
' + @WhereString + '
ORDER BY 
DeadlineDate, DeadlineTime
OPTION (RECOMPILE)'

    PRINT @querystring
--Finally, the query created above is executed.
    EXEC sp_executesql @QueryString,
	    N'@ClaimantNameParam NVARCHAR(max), @ClaimTypeParam NVARCHAR(max), @CreatedByParam NVARCHAR(250), @DetailedEventTypeParam NVARCHAR(4000), @TodaysDateParam date, @2WeeksFutureParam date',
        @ClaimantNameParam = @ClaimantName,
        @ClaimTypeParam = @ClaimTypeValue, 
		@CreatedByParam = @CreatedBy,
		@DetailedEventTypeParam = @DetailedEventType,
		@TodaysDateParam = @TodaysDate,
		@2WeeksFutureParam = @2WeeksFuture




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORT_qryCaseDeadlineReport] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORT_qryCaseDeadlineReport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORT_qryCaseDeadlineReport] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORT_qryCaseDeadlineReport] TO [power_user]
GO
