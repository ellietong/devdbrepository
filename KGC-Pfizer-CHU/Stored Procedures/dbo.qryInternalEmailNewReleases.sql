SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[qryInternalEmailNewReleases]
AS 
BEGIN

--Declare and set email inputs.
DECLARE @Subject NVARCHAR(100)
DECLARE @LogoHeader NVARCHAR(2000)

--DECLARE @Today4PM DATETIME
--DECLARE @Yesterday4PM DATETIME
--SET @Yesterday4PM = CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(DATE, GETDATE()
--    - 1)) + ' 16:00:00.000')
--SET @Today4PM = CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(DATE, GETDATE()))
--    + ' 16:00:00.000')



SET @Subject = 'Pfizer Claims: New Releases Uploaded '
    + CONVERT(VARCHAR, GETDATE(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	

        SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

DECLARE @NewReleasesTable TABLE
    (
		DocID bigint,
		ClaimantPersonalInfoID bigint,
		Lname nvarchar(150),
		Fname nvarchar(150),
		ClaimantName nvarchar(250),
		DocumentLink nvarchar(1000),
		SettlementDate nvarchar(50),
		SettlementAmount nvarchar(250),
		MissingClaimantInfo nvarchar(max),
		MissingDocumentation nvarchar(max),
		ClaimLawsuitID bigint
    )
INSERT  INTO @NewReleasesTable
SELECT 
	COALESCE(d.DocumentID, ' '),
	COALESCE(cpi.ClaimantPersonalInfoID, ' '),
	COALESCE(LastName, ' '),
	COALESCE(FirstName, ' '),
	COALESCE('<a href="https://www.kcicextranet.com/clients/Pfizer/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID='
                    + CONVERT(NVARCHAR(50), cpi.ClaimantPersonalInfoID) + '">'
                    + cpi.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END + COALESCE(' ('
                                            + CONVERT(NVARCHAR(50), cpi.ClaimantPersonalInfoID)
                                            + ')', '') + '</a>', 'Unknown'),
	COALESCE('<a href="https://www.kcicextranet.com/pfizer/cp/pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>', ' ') AS DocumentTitle,
	COALESCE(CONVERT(nvarchar(250), SettlementDate, 101), ' '),
	COALESCE('$' + CONVERT(nvarchar(50), CAST(SettlementAmount AS money), 1), ' '),
	COALESCE(dbo.fnMissingClaimantInformation(cl.ClaimLawsuitID), ' '),-- 'All required claimant information submitted.'),
	COALESCE(dbo.fnMissingSettlementDocumentation(cl.ClaimLawsuitID), ' '),
	COALESCE(cl.ClaimLawsuitID, ' ')-- 'All required documents submitted.')
FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID 
	INNER JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	INNER JOIN tblClaimantSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID
WHERE d.DateCreated BETWEEN CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(DATE, GETDATE()
    - 1)) + ' 16:00:00.000') AND CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(DATE, GETDATE()))
    + ' 16:00:00.000')
	AND d.ClaimantDocumentTypeID = 31
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	AND c.MatterID = 1
	AND ClaimSequenceID = 4


--SELECT *
--from @NewReleasesTable
DECLARE @Product table (ClaimantPersonalInfoID int, ProductEntered int)
DECLARE @Employment table (ClaimantPersonalInfoID int, EmploymentDates int)

DECLARE @CurrentClaimantInfo TABLE  (
									 ClaimantName bit,
									 SSN bit, 
									 BirthDate bit, 
									 GenderID bit,
									 Disease bit, 
									 DiagnosisDate bit,  
									 ProductID bit,
									 Employment bit
									)

DECLARE @MissingClaimantInfo  TABLE  (MissingClaimantInformation nvarchar(500), Test nvarchar(10))


--Declare and populate the body of the email for individual firms
        DECLARE @BodyMessage NVARCHAR(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
        SET @BodyMessage = @LogoHeader
            + '<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'

        DECLARE @LastLawsuitID NVARCHAR(250) = ''

--Set table column headings and populate the cells 

        IF ( SELECT COUNT(DocID)
             FROM   @NewReleasesTable
           ) > 0 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<H4>The following releases have been uploaded in the last 24 hours: </H4>'
                SET @BodyMessage = @BodyMessage
                    + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' 
                SET @BodyMessage = @BodyMessage
                    + N'<tr><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Claimant</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Settlement Date</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Settlement Amount</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Missing Claimant Information</th>
							<th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Missing Settlement Documentation</th>
					   '
	
                DECLARE @ClaimantName NVARCHAR(500) ,
						@SettlementDate NVARCHAR(300) ,
						@SettlementAmount NVARCHAR(250) ,
						@GetMissingClaimantInfo NVARCHAR(max) ,
						@GetMissingDocumentation NVARCHAR(max),
						@ClaimLawsuitID	BIGINT 
	
                DECLARE Doc_List CURSOR
                FOR
                    SELECT  ClaimantName,
                            SettlementDate,
							SettlementAmount,
							ClaimLawsuitID
                    FROM    @NewReleasesTable nrt
                    GROUP BY ClaimantName,
                             SettlementDate,
							 SettlementAmount,
							 ClaimLawsuitID
                    ORDER BY ClaimantName	

                OPEN Doc_List
                FETCH NEXT FROM Doc_List INTO @ClaimantName,
											  @SettlementDate, 
											  @SettlementAmount, 
											  @ClaimLawsuitID

                WHILE @@FETCH_STATUS = 0 
                    BEGIN
	
		
                        SET @BodyMessage = @BodyMessage + '<tr>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @ClaimantName + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @SettlementDate + ' </td>'
                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> '
                            + @SettlementAmount + ' </td>'	

                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top">'

		
								INSERT INTO @Product 
								SELECT ce.ClaimantPersonalInfoID, 
									   CASE WHEN MAX(ProductID) IS NULL  THEN 0  ELSE 1  END AS ProductEntered
								FROM tblClaimantEmployment ce 
									INNER JOIN tblClaim c on ce.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
									INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
									INNER JOIN tblClaimantProduct cp on ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
								WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
								GROUP BY ce.ClaimantPersonalInfoID 

								INSERT INTO @Employment
								SELECT ce.ClaimantPersonalInfoID,
									   CASE WHEN max(ClaimantEmploymentID) IS NULL THEN 0  ELSE 1  END
								FROM tblClaimantEmployment ce
									INNER JOIN tblClaim c on ce.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
									INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
								WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
									AND EmpEndDate IS NOT NULL
									AND EmpStartDate IS NOT NULL
								GROUP BY ce.ClaimantPersonalInfoID

								INSERT INTO @CurrentClaimantInfo
								SELECT DISTINCT CASE WHEN cpi.LastName IS NULL THEN 0 WHEN cpi.FirstName IS NULL THEN 0 ELSE 1 END AS HasClaimantName,
												CASE WHEN SocialSecurityNumber is NULL THEN 0 ELSE 1 END AS HasSSN, 
												CASE WHEN BirthDate is NULL THEN 0 ELSE 1 END AS HasBirthDate,
												CASE WHEN COALESCE(GenderID, 0) = 0 THEN 0 WHEN COALESCE(GenderID, 0) = 4 THEN 0 ELSE 1 END AS HasGenderID,
												CASE WHEN DiseaseID IS NULL THEN 0 ELSE 1 END AS HasDiseaseID,
												CASE WHEN CONVERT(date, DiseaseDiagnosisDate) IS NULL THEN 0 ELSE 1 END AS HasDxDate, 
												CASE WHEN COALESCE(p.ProductEntered, 0) = 0 THEN 0  ELSE 1 END AS HasProductEntered,
												CASE WHEN COALESCE(e.EmploymentDates, 0) = 0 THEN 0  ELSE 1 END AS HasValidEmployment  															  			
								FROM tblClaim c
									INNER JOIN tblClaimantPersonalInfo cpi	 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
									INNER JOIN tblClaimLawsuit cl	 ON c.ClaimID = cl.ClaimID
									LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
									LEFT JOIN @Employment e ON cpi.ClaimantPersonalInfoID = e.ClaimantPersonalInfoID
									LEFT JOIN @Product p	 ON p.claimantpersonalinfoid =cpi.ClaimantPersonalInfoID 
									LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
									LEFT JOIN tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
									LEFT JOIN (SELECT * FROM tblClaimDisease WHERE PrimaryDisease = 1) cdi	 ON cdi.ClaimID = c.ClaimID
									INNER JOIN tblClaimLawsuitStatusTracking cls on cls.ClaimLawsuitID =cl.ClaimLawsuitID 
									LEFT JOIN (SELECT * FROM tblClaimantRelatedParty WHERE LegalCapacityID in (1,2,3,4,5,6,7,8,15,17,30,31))crp	 ON crp.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
								WHERE cl.ClaimLawsuitID = @ClaimLawsuitID


								INSERT INTO @MissingClaimantInfo
								SELECT COALESCE('Claimant Name', NULL), NULL
								FROM @CurrentClaimantInfo
								WHERE ClaimantName = 0
								UNION
								SELECT 'Social Security Number', NULL
								FROM @CurrentClaimantInfo
								WHERE SSN = 0
								UNION
								SELECT 'Birth Date', NULL
								FROM @CurrentClaimantInfo
								WHERE BirthDate = 0
								UNION
								SELECT 'Gender', NULL
								FROM @CurrentClaimantInfo
								WHERE GenderID = 0
								UNION
								SELECT 'Disease Diagnosis', NULL
								FROM @CurrentClaimantInfo
								WHERE Disease = 0
								UNION
								SELECT 'Diagnosis Date', NULL
								FROM @CurrentClaimantInfo
								WHERE DiagnosisDate = 0
								UNION
								SELECT 'Product ID', NULL
								FROM @CurrentClaimantInfo
								WHERE ProductID = 0
								UNION
								SELECT 'Employment Dates', NULL
								FROM @CurrentClaimantInfo
								WHERE Employment = 0

								IF(SELECT COUNT(MissingClaimantInformation) FROM @MissingClaimantInfo) = 0
								BEGIN
								INSERT INTO @MissingClaimantInfo
								SELECT 'All required claimant information submitted.', NULL
								END
		
                        DECLARE MissingClaimantInfo_curs CURSOR
                        FOR
                            SELECT DISTINCT
                                    COALESCE(MissingClaimantInformation, ' ')
                            FROM    @MissingClaimantInfo

							--SELECT *
							--FROM @MissingClaimantInfo
			

                        OPEN MissingClaimantInfo_curs
                        FETCH NEXT FROM MissingClaimantInfo_curs INTO @GetMissingClaimantInfo
			
                        WHILE @@FETCH_STATUS = 0 
                            BEGIN		
				
                                SET @BodyMessage = @BodyMessage + @GetMissingClaimantInfo -- + '_' + CONVERT(NVARCHAR(10), @ClaimLawsuitID)
                                    + ' <br/>'
				
                                FETCH NEXT FROM MissingClaimantInfo_curs INTO @GetMissingClaimantInfo
			
                            END

							DELETE FROM @Product
							DELETE FROM @Employment
							DELETE FROM @CurrentClaimantInfo
							DELETE FROM @MissingClaimantInfo

                        CLOSE MissingClaimantInfo_curs
                        DEALLOCATE MissingClaimantInfo_curs
						
 
                        SET @BodyMessage = @BodyMessage + '</td>'

                        SET @BodyMessage = @BodyMessage
                            + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top">'

		
						DECLARE @CurrentDocumentation TABLE  (DxMedicals bit, 
															  DODCert int, 
															  EstatePaper int)

						INSERT INTO @CurrentDocumentation
						SELECT DISTINCT COALESCE(sd.DxMedicals, 0) AS DxMedicals,
										CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE(sd.DODCert, 0) END AS DODCert, 
										CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE(sd.EstatePapers, 0) END AS Estatepaper
						FROM tblClaim c
							INNER JOIN tblClaimantPersonalInfo cpi	 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
							INNER JOIN tblClaimLawsuit cl	 ON c.ClaimID = cl.ClaimID
							LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
							LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
							LEFT JOIN tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
						WHERE cl.ClaimLawsuitID = @ClaimLawsuitID

						--SELECT * FROM @CurrentDocumentation

						DECLARE @MissingDocumentation TABLE  (ID int NOT NULL IDENTITY(1,1), MissingDocumentation nvarchar(500), Remainder int)
						INSERT INTO @MissingDocumentation
						SELECT 'Diagnosing Medicals', NULL
						FROM @CurrentDocumentation
						WHERE DxMedicals = 0
						UNION
						SELECT 'Death Cerificate', NULL
						FROM @CurrentDocumentation
						WHERE DODCert = 0
						UNION
						SELECT 'Estate Papers', NULL
						FROM @CurrentDocumentation
						WHERE EstatePaper = 0

						IF(SELECT COUNT(MissingDocumentation) FROM @MissingDocumentation) = 0
							BEGIN
								INSERT INTO @MissingDocumentation
								SELECT 'All required documents submitted.', NULL
							END
		
                        DECLARE MissingSettlementDoc_curs CURSOR
                        FOR
                            SELECT DISTINCT
                                    COALESCE(MissingDocumentation, ' ')
                            FROM    @MissingDocumentation

							--SELECT *
							--FROM @MissingDocumentation
			

                        OPEN MissingSettlementDoc_curs
                        FETCH NEXT FROM MissingSettlementDoc_curs INTO @GetMissingDocumentation
			
                        WHILE @@FETCH_STATUS = 0 
                            BEGIN		
				
                                SET @BodyMessage = @BodyMessage + @GetMissingDocumentation --+ '_' + CONVERT(NVARCHAR(10), @ClaimLawsuitID)
                                    + ' <br/>'
				
                                FETCH NEXT FROM MissingSettlementDoc_curs INTO @GetMissingDocumentation
			
                            END
						DELETE FROM @CurrentDocumentation													
						DELETE FROM @MissingDocumentation

                        CLOSE MissingSettlementDoc_curs
                        DEALLOCATE MissingSettlementDoc_curs
						
 
                        SET @BodyMessage = @BodyMessage + '</td></tr>'
																	

					FETCH NEXT FROM Doc_List INTO  
												  @ClaimantName,
												  @SettlementDate, 
												  @SettlementAmount, 
												  @ClaimLawsuitID

                    END
                CLOSE Doc_List
                DEALLOCATE Doc_List
	
	
                SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'
	

            END
        ELSE 
            BEGIN
                SET @BodyMessage = @BodyMessage
                    + '<H4>No releases have been uploaded in the last 24 hours. </H4>'
            END



--Undo the HTML stripping from the xml path function above so embedded tags work
        SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;',
                                   '>')

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--SELECT @BodyMessage

IF (Select COUNT(DocID) from @NewReleasesTable) >0 
begin
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail 
@profile_name = 'Claims',
@recipients = 'Pfizer@kcic.com',
@copy_recipients = '',
@blind_copy_recipients = 'monahanc@kcic.com; kanga@kcic.com',
@subject = @subject, 
@body = @BodyMessage, 
@body_format = 'html'
END
END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailNewReleases] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailNewReleases] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailNewReleases] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailNewReleases] TO [power_user]
GO
