SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIMTYPE_qryClaimTypeList]
AS 
    SET NOCOUNT ON

    SELECT  ClaimTypeID ,
            ClaimType
    FROM    tblClaimType
    UNION
    SELECT  NULL ,
            ''
    ORDER BY ClaimType

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMTYPE_qryClaimTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMTYPE_qryClaimTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMTYPE_qryClaimTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMTYPE_qryClaimTypeList] TO [power_user]
GO
