SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DEADLINE_qryUpdCaseDeadline] 
	-- Add the parameters for the stored procedure here
	  @CaseDeadlineID BIGINT,
	  @DeadlineTypeID INT, 	
	  @EventTypeID INT,
	  @DetailedEventType NVARCHAR(MAX),
      @Emailaddress NVARCHAR(250) ,
      @DeadlineDate DATE ,
	  @DeadlineTime TIME,
	  @TimeZoneID INT,
	  @TimePeriodID INT,
	  @EventLocationID INT = NULL,
	  @Comments NVARCHAR(MAX),	
	  @Deponent NVARCHAR(4000),
	  @Attendee NVARCHAR(4000),
	  @AttendanceModeID INT,
	  @DeadlineStatusID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @DetailedEventTypeID INT


IF (@DetailedEventType IS NOT NULL)
BEGIN
	IF (SELECT COUNT(DetailedEventTypeID)
		FROM tblDetailedEventType
		WHERE DetailedEventType LIKE '%'+ RTRIM(LTRIM(@DetailedEventType)) +'%'
		) = 0
		BEGIN
			INSERT INTO tblDetailedEventType (DetailedEventType, DeadlineTypeID)
			VALUES (@DetailedEventType, @DeadlineTypeID)
			SET @DetailedEventTypeID = (SELECT MAX(DetailedEventTypeID) FROM tblDetailedEventType)
		END
	ELSE 
		BEGIN
			SET @DetailedEventTypeID = (SELECT TOP 1 DetailedEventTypeID
											FROM tblDetailedEventType
											WHERE DetailedEventType LIKE '%'+ RTRIM(LTRIM(@DetailedEventType)) +'%')
		END
END

IF @CaseDeadlineID IS NOT NULL
BEGIN
	UPDATE tblCaseDeadline
	SET EventTypeID = @EventTypeID,
		DetailedEventTypeID = @DetailedEventTypeID,
		DeadlineDate = @DeadlineDate,
		DeadlineTime = @DeadlineTime,
		TimePeriodID = @TimePeriodID,
		TimeZoneID = @TimeZoneID,
		EventLocationID = @EventLocationID,
		Comments = @Comments,
		Deponent = @Deponent,
		Attendees = @Attendee,
		AttendanceModeID = @AttendanceModeID,
		DeadlineStatusID = @DeadlineStatusID
	WHERE CaseDeadlineID = @CaseDeadlineID
END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryUpdCaseDeadline] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryUpdCaseDeadline] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryUpdCaseDeadline] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryUpdCaseDeadline] TO [power_user]
GO
