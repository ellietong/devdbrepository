SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptDismissalQCRange](@start datetime, @End datetime)
AS 
SET NOCOUNT ON



Select 
	d.DocumentID, d.DocumentTitle, StatusDate, --d.DismissalDate as DocDismissalDate, d.QCBy, 
	cpi.ClaimantPersonalInfoID, cpi.LastName + ', ' + cpi.FirstName + COALESCE(' ' + cpi.MiddleName,'') AS ClaimantName,
	IndividualDocketNumber,l.CaseCaption, l.DocketNumber, cls.ClaimLawsuitStatus, clst.StatusDate as DismissalDate, clst.IsPrimaryStatus, COUNT(distinct mcd.claimantpersonalinfoid) as CountClaimants

from tblDocuments d

	inner join tblMultiClaimantDocument mcd ON mcd.DocumentID = d.DocumentID
	inner join tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	inner join tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	inner join tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
	inner join tblLawsuit l ON l.LawsuitID = cl.LawsuitID
	inner join tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	inner join tblClaimLawsuitStatus CLS on clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID

where 

   (d.QCDate < @End and d.QCDate > @start ) and (ClaimantDocumentTypeID = 16)
   
group by 
	d.DocumentID, d.DocumentTitle, StatusDate, --d.DismissalDate,  d.QCBy, 
	cpi.ClaimantPersonalInfoID, cpi.LastName + ', ' + cpi.FirstName + COALESCE(' ' + cpi.MiddleName,''), IndividualDocketNumber,
	l.CaseCaption, l.DocketNumber, cls.ClaimLawsuitStatus, clst.StatusDate, IsPrimaryStatus
		   
UNION


Select d.DocumentID, d.DocumentTitle, CLST2.StatusDate, --d.DismissalDate as DocDismissalDate,  d.QCBy, 
	   cpi.ClaimantPersonalInfoID, cpi.LastName + ', ' + cpi.FirstName + COALESCE(' ' + cpi.MiddleName,'') AS ClaimantName, IndividualDocketNumber,
	   l.CaseCaption, l.DocketNumber, cls.ClaimLawsuitStatus, clst.StatusDate as DismissalDate, clst2.IsPrimaryStatus, COUNT(distinct mcd.claimantpersonalinfoid) as CountClaimants
from tblDocuments d

	inner join tblMultiClaimantDocument mcd ON mcd.DocumentID = d.DocumentID
	inner join tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	inner join tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	inner join tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
	inner join tblLawsuit l ON l.LawsuitID = cl.LawsuitID
	inner join tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	inner join (select * from tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst2 ON clst2.ClaimLawsuitID = cl.ClaimLawsuitID
	inner join tblClaimLawsuitStatus CLS on clst2.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID

where 
	(d.QCDate < @End and d.QCDate > @start ) and (ClaimantDocumentTypeID = 16)

group by 
	d.DocumentID, d.DocumentTitle, CLST2.StatusDate, --d.DismissalDate, d.QCBy, 
	cpi.ClaimantPersonalInfoID, cpi.LastName + ', ' + cpi.FirstName + COALESCE(' ' + cpi.MiddleName,''), IndividualDocketNumber,
	l.CaseCaption, l.DocketNumber, cls.ClaimLawsuitStatus, clst.StatusDate, clst2.IsPrimaryStatus

ORDER BY 
	DocumentID, ClaimantName, ClaimantPersonalInfoID


RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissalQCRange] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDismissalQCRange] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissalQCRange] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDismissalQCRange] TO [power_user]
GO
