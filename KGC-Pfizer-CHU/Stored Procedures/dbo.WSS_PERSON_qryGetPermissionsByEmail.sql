SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_PERSON_qryGetPermissionsByEmail]
    (
      @ApproverTypeID INT,
	  @StateID INT = NULL
    )
AS 
    DECLARE @PersonID INT


IF @ApproverTypeID = 1 --Document Profile Doc Acceptance
BEGIN
	IF @StateID <> 47
	BEGIN
		SELECT DISTINCT CONVERT(nvarchar(10),PersonID) AS PersonID
		FROM vtblPerson 
		WHERE PersonID IN (5087, 5098, 5099) --K. Ross, D. Littler, M. Hughes
	END
	ELSE
	BEGIN
		SELECT DISTINCT CONVERT(nvarchar(10),PersonID) AS PersonID
		FROM vtblPerson 
		WHERE PersonID IN (5087, 5099)	--K. Ross, M. Hughes; D.Littler is required to accept documents of Ohio	
	END
END
ELSE IF @ApproverTypeID = 2 --Adding/editing/deleting claimant documents
BEGIN
	SELECT DISTINCT CONVERT(nvarchar(10),PersonID) AS PersonID
	FROM vtblPerson 
	WHERE
	FirmAddressID IN (622, -- Pfizer
					  624, -- Dickstein Shapiro
					  602, -- Tucker Ellis
					  620, -- DLA Piper
					  621, -- Kaye Scholer LLP
					  623, -- Quinn Emanuel Urguhart & Sullivan, LLP
					  682  -- Dechert LLP
					  )
END
ELSE
BEGIN
	RETURN
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetPermissionsByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetPermissionsByEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetPermissionsByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetPermissionsByEmail] TO [power_user]
GO
