SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimLawsuitInfoQC] (@ClaimantPersonalInfoID int)
as

Select --Claim Info
	ClaimantPersonalInfoID,
	IsCurrentClaim,
	ClaimSequence,
	Matter,
	c.ClaimID, 
	cl.ClaimLawsuitID,
	cl.LawsuitID,
	IndividualDocketNumber,
	IsPrimaryForClaim

From tblClaim c
	Left Join tblClaimLawsuit cl on c.claimID = cl.claimID
	Left Join tblClaimSequence cs on cs.claimsequenceID = c.claimsequenceID
	Left Join tblMatter m on c.MatterID = m.MatterID
Where c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By IsPrimaryForClaim

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [power_user]
GO
