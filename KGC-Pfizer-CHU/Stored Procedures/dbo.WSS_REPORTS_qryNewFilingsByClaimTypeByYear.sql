SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByClaimTypeByYear] 
	-- Add the parameters for the stored procedure here
	(@YearID int, @MonthID int)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     -- Insert statements for procedure here
    
--Declare @year int
--Set @Year=2010

DECLARE @year int
SET @year =@YearID

DECLARE @Month int
set @Month = @MonthID

select Material as ClaimType, 
		COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
left join (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblAllegation a on a.ClaimLawsuitID=cl.ClaimLawsuitID
inner join tblMaterial m on m.MaterialID = a.MaterialID
where	year(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate))= @Year 
		and MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,processeddate,Statusdate)) <=@Month

group by Material
order by Material


END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimTypeByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimTypeByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimTypeByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimTypeByYear] TO [power_user]
GO
