SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmAllegationInfoEntry] 
as
 
Select *
From tblAllegation
Order By DefendantCompanyID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllegationInfoEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllegationInfoEntry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllegationInfoEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllegationInfoEntry] TO [power_user]
GO
