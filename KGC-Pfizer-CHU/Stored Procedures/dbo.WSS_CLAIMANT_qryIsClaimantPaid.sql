SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryIsClaimantPaid]
    (
      @ClaimantPersonalInfoID INT
    )
AS 
    SET NOCOUNT ON

IF (SELECT COUNT(cl.ClaimLawsuitID)
	FROM tblClaimLawsuitStatusTracking clst 
		INNER JOIN tblClaimLawsuit cl on clst.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaim c on cl.ClaimID = c.ClaimID	
		INNER JOIN tblClaimLawsuitStatus cls on clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND cls.ClaimLawsuitStatusGroupID = 3
	GROUP BY c.ClaimantPersonalInfoID
) > 0
BEGIN
	SELECT TOP 0 PaymentSettlementID
	FROM tblPaymentSettlement
END
ELSE IF (SELECT COALESCE(COUNT(DISTINCT paymentSettlementID), 0)
		 FROM tblPaymentSettlement ps
			 LEFT JOIN tblClaimantSettlement cs ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
			 LEFT JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
			 LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		 WHERE   ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		 GROUP BY c.ClaimantPersonalInfoID
) < 1
BEGIN
	SELECT TOP 0 PaymentSettlementID
	FROM tblPaymentSettlement
END
ELSE IF (SELECT COALESCE(COUNT(DISTINCT paymentSettlementID), 0)
		 FROM tblPaymentSettlement ps
			 LEFT JOIN tblClaimantSettlement cs ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
			 LEFT JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
			 LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		 WHERE   ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		 GROUP BY c.ClaimantPersonalInfoID
) > 0
BEGIN
	SELECT 3
END
ELSE
BEGIN
	SELECT TOP 0 PaymentSettlementID
	FROM tblPaymentSettlement
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [power_user]
GO
