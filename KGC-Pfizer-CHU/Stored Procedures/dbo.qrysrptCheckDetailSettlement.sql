SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qrysrptCheckDetailSettlement] (@CheckID int)

AS
Set NoCount On
/*
CAS - 10/4/11 - procedure altered to show the details of claimants paid on the check.

*/

--declare @checkid int
--set @checkid=12728


Select CheckID,cpi.ClaimantPersonalInfoID as ClaimantID, cpi.LastName +', '+cpi.FirstName as ClaimantName, 'XXX-XX-'+right(cpi.SocialSecurityNumber, 4) as SSN,
convert(date,cs.SettlementDate,101) as SettleDate, SettlementAmount,
ps.PaymentAmount, case when ps.IsFullyPaid = 1 then 'Yes' else 'No' end as FullyPaid
from tblPaymentSettlement ps
Inner Join tblClaimantSettlement cs on cs.ClaimantSettlementID = ps.ClaimantSettlementID
Inner Join tblSettlementGroup sg on sg.SettlementGroupID = cs.SettlementGroupID
left join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
left join tblClaim c on c.ClaimID = cl.ClaimID
left join tblClaimantPersonalInfo cpi	 on cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
Where CheckID = @CheckID



Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [power_user]
GO
