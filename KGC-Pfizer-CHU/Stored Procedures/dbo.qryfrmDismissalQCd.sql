SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmDismissalQCd]
(@DocumentID bigint, @QCBy nvarchar(255))

As
Set NoCount On

update tblDocuments
set  QCBy = @QCBy, QCDate = GETDATE()
where DocumentID = @DocumentID

if coalesce((select Accepted from tblDocuments where DocumentID = @DocumentID), 0) = 0 
begin
update tblDocuments
set Accepted = 1, 
AcceptedBy = @QCBy + '@kcic.com',
DateAccepted = GETDATE()
where DocumentID = @DocumentID
end


RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalQCd] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalQCd] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalQCd] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalQCd] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalQCd] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalQCd] TO [power_user]
GO
