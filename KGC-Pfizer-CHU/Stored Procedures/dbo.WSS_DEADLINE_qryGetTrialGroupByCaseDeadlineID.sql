SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DEADLINE_qryGetTrialGroupByCaseDeadlineID]

@CaseDeadlineID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT DISTINCT cd.TrialGroupID
FROM tblCaseDeadline cd 
WHERE cd.CaseDeadlineID = @CaseDeadlineID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryGetTrialGroupByCaseDeadlineID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryGetTrialGroupByCaseDeadlineID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DEADLINE_qryGetTrialGroupByCaseDeadlineID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DEADLINE_qryGetTrialGroupByCaseDeadlineID] TO [power_user]
GO
