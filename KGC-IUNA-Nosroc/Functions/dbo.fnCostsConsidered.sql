SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Function [dbo].[fnCostsConsidered]
--Converted to SQL by SHM 01/09/2006
--edited 5/25/02 by EAH to consider only the indemnity cost fields, costs 3-12
(@AllocationID int, @OccurrenceID int) 
Returns money
AS 
BEGIN

DECLARE @Costs money

SET @Costs = 0

If (SELECT Cost3 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost3 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost3Discount FROM tblAllocation WHERE AllocationID = @AllocationID)
If (SELECT Cost4 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost4 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost4Discount FROM tblAllocation WHERE AllocationID = @AllocationID)
If (SELECT Cost5 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost5 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost5Discount FROM tblAllocation WHERE AllocationID = @AllocationID)
If (SELECT Cost6 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost6 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost6Discount FROM tblAllocation WHERE AllocationID = @AllocationID)
If (SELECT Cost7 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost7 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost7Discount FROM tblAllocation WHERE AllocationID = @AllocationID)
If (SELECT Cost8 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost8 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost8Discount FROM tblAllocation WHERE AllocationID = @AllocationID)
If (SELECT Cost9 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost9 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost9Discount FROM tblAllocation WHERE AllocationID = @AllocationID)
If (SELECT Cost10 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost10 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost10Discount FROM tblAllocation WHERE AllocationID = @AllocationID)
If (SELECT Cost11 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost11 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost11Discount FROM tblAllocation WHERE AllocationID = @AllocationID)
If (SELECT Cost12 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost12 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost12Discount FROM tblAllocation WHERE AllocationID = @AllocationID)

RETURN @Costs

END
GO
GRANT EXECUTE ON  [dbo].[fnCostsConsidered] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnCostsConsidered] TO [power_user]
GO
