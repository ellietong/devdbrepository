SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		JKC
-- Create date:1-27-2010
-- Description:Returns minimum triggered layer in each column
-- =============================================
CREATE FUNCTION [dbo].[fnMinSigPolicyPerColumn]
(@ColumnID int, @allocationid int, @limittypeID int)
RETURNS int
AS
BEGIN
--@tmp orders the triggered layer numbers
DECLARE @tmp TABLE (Layer int)
INSERT INTO @tmp
	SELECT MIN(Layer)
	FROM ztblSigLevel l 
	INNER JOIN ztblPolicyAllocation a ON l.PolicyID = a.PolicyID
	INNER JOIN tblPolicyLimits pl ON a.PolicyID = pl.PolicyID
	WHERE allocationID = @allocationID 
	AND limittypeID = @limittypeID
	AND ColumnID = @ColumnID
	AND OccurrenceRemaining > .01

	ORDER BY MIN(Layer)
	

	
RETURN (SELECT  TOP 1 Layer FROM @tmp)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fnMinSigPolicyPerColumn] TO [Allocation_User]
GRANT EXECUTE ON  [dbo].[fnMinSigPolicyPerColumn] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[fnMinSigPolicyPerColumn] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnMinSigPolicyPerColumn] TO [power_user]
GO
