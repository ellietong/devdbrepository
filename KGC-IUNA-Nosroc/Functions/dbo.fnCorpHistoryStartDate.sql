SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Function [dbo].[fnCorpHistoryStartDate]
--Calculate the Start Date by Corporate History ID and Insurance Program ID
(@InsuranceProgramID int, @CorporateHistoryID int) 
Returns datetime
AS 
BEGIN

DECLARE @CorpHistStartDate datetime

Set @CorpHistStartDate = (Coalesce((Select [From]
From tblCorpHistInsProgram 
Where InsuranceProgramID = @InsuranceProgramID and CorporateHistoryID = @CorporateHistoryID
Group By [From]),'2/1/1900'))

RETURN @CorpHistStartDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCorpHistoryStartDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnCorpHistoryStartDate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnCorpHistoryStartDate] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnCorpHistoryStartDate] TO [power_user]
GO
