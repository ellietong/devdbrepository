SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnTeens]
(@Teens int)
RETURNS nvarchar(15)
AS
Begin

Declare @TeensStr nvarchar (15)

SET @TeensStr = 
	CASE 
	 When @Teens = 10 Then 'ten' 
	 When @Teens = 11 Then 'eleven' 
	 When @Teens = 12 Then 'twelve' 
	 When @Teens = 13 Then 'thirteen'
	 When @Teens = 14 Then 'fourteen'
	 When @Teens = 15 Then 'fifteen'
	 When @Teens = 16 Then 'sixteen'
	 When @Teens = 17 Then 'seventeen'
	 When @Teens = 18 Then 'eighteen'
	 When @Teens = 19 Then 'nineteen'End 

Return @TeensStr
End
GO
