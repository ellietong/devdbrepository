SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create function [dbo].[fnCheckWriting] (@Amount money)

RETURNS nvarchar(500)
AS
Begin
DECLARE @String nvarchar (500)

--DECLARE @Amount money
--SET @amount = 150273.84

Declare @Digits int
Set @Digits = LEN(@amount)

Declare @AmtStr nvarchar (20)
Set @AmtStr =  CONVERT (nvarchar(20), @Amount)

declare @cents bit
declare @hundreds bit
declare @thousands bit

Set @hundreds = CASE WHEN @digits >=6 then 1 END
Set @thousands = CASE WHEN @digits >=7 then 1 END

--Create the cents string
Declare @cent1 int
Declare @cent2 int
Declare @Single int
Declare @Ten int
Declare @Hund int
Declare @Thous int
Declare @TenThous int
Declare @HundThous int

Set @cent1 = RIGHT (@AmtStr, 1)
Set @cent2 = SUBSTRING (@AmtStr, @digits-1,1)
Set @Single = SUBSTRING (@AmtStr, @digits-3,1)
Set @Ten = SUBSTRING (@AmtStr, @digits-4,1)
Set @Hund = SUBSTRING (@AmtStr, @digits-5,1)
Set @Thous = SUBSTRING (@AmtStr, @digits-6,1)
Set @TenThous = SUBSTRING (@AmtStr, @digits-7,1)
Set @HundThous = SUBSTRING (@AmtStr, @digits-8,1)

set @String =  dbo.fnnumbers(@HundThous)+ ' Hundred '+dbo.fntens(@TenThous)+' '+
case when @Thous <> '0' then dbo.fnnumbers(@thous)else '' End + 'Thousand'+ ' '+
dbo.fnnumbers(@hund) + ' Hundred '+dbo.fntens(@Ten)+' '+ 
case when @single <> '0' then dbo.fnnumbers(@single)else '' End + ' Dollars and '+

RIGHT(@amount, 2)+ '/100 '

--print @String
Return @String
End
--select dbo.fnteens (16)

--CREATE FUNCTION [dbo].[fnTeens]
--(@Teens int)
--RETURNS nvarchar(15)
--AS
--Begin

--Declare @TeensStr nvarchar (15)

--SET @TeensStr = 
--	CASE 
--	 When @Teens = 10 Then 'ten' 
--	 When @Teens = 11 Then 'eleven' 
--	 When @Teens = 12 Then 'twelve' 
--	 When @Teens = 13 Then 'thirteen'
--	 When @Teens = 14 Then 'fourteen'
--	 When @Teens = 15 Then 'fifteen'
--	 When @Teens = 16 Then 'sixteen'
--	 When @Teens = 17 Then 'seventeen'
--	 When @Teens = 18 Then 'eighteen'
--	 When @Teens = 19 Then 'nineteen'End 

--Return @TeensStr
--End

GO
GRANT EXECUTE ON  [dbo].[fnCheckWriting] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnCheckWriting] TO [power_user]
GO
