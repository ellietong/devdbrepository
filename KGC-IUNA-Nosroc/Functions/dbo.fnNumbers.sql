SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnNumbers]
(@Numbers int)
RETURNS nvarchar(15)
AS
Begin

Declare @NumStr nvarchar (15)

SET @NumStr = 
	CASE 
	 When @Numbers = 1 Then 'one' 
	 When @Numbers = 2 Then 'two' 
	 When @Numbers = 3 Then 'three'
	 When @Numbers = 4 Then 'four'
	 When @Numbers = 5 Then 'five'
	 When @Numbers = 6 Then 'six'
	 When @Numbers = 7 Then 'seven'
	 When @Numbers = 8 Then 'eight'
	 When @Numbers = 9 Then 'nine'End 

Return @NumStr
End
GO
