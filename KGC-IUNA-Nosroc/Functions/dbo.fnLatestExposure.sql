SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE FUNCTION [dbo].[fnLatestExposure]
(@ClaimantPersonalInfoID int)

RETURNS datetime
AS
BEGIN

--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 6

DECLARE @LatestExposure Date

/**SECTION OF CODE TO USE IF YOU DO WANT TO CONSIDER PRODUCTS IN THE EXPOSURE CALCULATION. IUNA AND NSI FOR EXAMPLE**/

IF (SELECT COUNT(ClaimantProductID)	From tblclaimantemployment ce inner join tblclaimantproduct cp on cp.claimantemploymentid = ce.claimantemploymentID where ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID) = 0
BEGIN 

set @LatestExposure =(select  MAX(EmpEndDate) as LatestExposureDate
from tblClaimantEmployment
where ClaimantPersonalInfoID=@ClaimantPersonalInfoID)

END
ELSE 
BEGIN

set @LatestExposure =(select MAX(LatestExposureDate) as LatestExposureDate
From
(SELECT case when coalesce(max(cp.EndDate), '1/1/2050') > ce.EmpEndDate then ce.EmpEndDate else MAX(cp.EndDate) end as LatestExposureDate
From tblclaimantemployment ce 
inner join tblclaimantproduct cp on cp.claimantemploymentid = ce.claimantemploymentID 
where ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
group by ce.EmpEndDate) tblEmploymentExposure)

end


/**SECTION OF CODE TO USE IF YOU DO NOT WANT TO CONSIDER PRODUCTS IN THE EXPOSURE CALCULATION. DANA FOR EXAMPLE**/

--set @LatestExposure =( select MAX(EmpEndDate) as LatestExposureDate
--from tblClaimantEmployment
--where ClaimantPersonalInfoID=@ClaimantPersonalInfoID)

Return convert(nvarchar(10),@LatestExposure,101)

End

GO
GRANT VIEW DEFINITION ON  [dbo].[fnLatestExposure] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnLatestExposure] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnLatestExposure] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnLatestExposure] TO [power_user]
GO
