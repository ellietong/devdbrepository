SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnTens]
(@Tens int)
RETURNS nvarchar(15)
AS
Begin

Declare @TensStr nvarchar (15)

SET @TensStr = 
	CASE When @Tens = 2 Then 'Twenty' 
	 When @Tens = 3 Then 'Thirty'
	 When @Tens = 4 Then 'Forty'
	 When @Tens = 5 Then 'Fifty'
	 When @Tens = 6 Then 'Sixty'
	 When @Tens = 7 Then 'Seventy'
	 When @Tens = 8 Then 'Eighty'
	 When @Tens = 9 Then 'Ninty'End 

Return @TensStr
End
GO
