SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Function [dbo].[fnCorpHistoryEndDate]
--Calculate the End Date by Corporate History ID and Insurance Program ID
(@InsuranceProgramID int, @CorporateHistoryID int) 
Returns datetime
AS 
BEGIN

DECLARE @CorpHistEndDate datetime

Set @CorpHistEndDate = (coalesce((Select [Until]
From tblCorpHistInsProgram 
Where InsuranceProgramID = @InsuranceProgramID and CorporateHistoryID = @CorporateHistoryID
Group By [Until]),'1/1/2079'))

RETURN @CorpHistEndDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCorpHistoryEndDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnCorpHistoryEndDate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnCorpHistoryEndDate] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnCorpHistoryEndDate] TO [power_user]
GO
