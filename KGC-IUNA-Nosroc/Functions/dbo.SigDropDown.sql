SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Function [dbo].[SigDropDown]
(@ColumnID int, @PolicyID int, @LimitTypeID int, @Layer int, @allocationID int) 

Returns money

AS 

/*For Testing
DECLARE @ColumnID int
DECLARE @PolicyID int
DECLARE @LimitTypeID int
DECLARE  @Layer int

SET @ColumnID = 116783
SET @PolicyID =1375
SET @LimitTypeID =2
SET  @Layer =5
*/ 

BEGIN
--JKC 1/13/2009 calculates the lowest attachment point a signatory can "drop-down" to
-- during the signatory only portion of Wellington
-- declare temporary integer variable for looping
-- drop down over sig coverage and insolvent

DECLARE @tmpInt int
SET @tmpInt = 1

DECLARE @tempTable TABLE (ColumnID int, AttachmentPoint money)

WHILE @tmpInt < @Layer AND ((SELECT MAX(ProvisionValueID) FROM ztblpolicycolumn PC 
		INNER JOIN ztblPolicyAllocation z ON pc.policydateID = z.PolicydateID
		INNER JOIN tblpolicylimits L on L.policyid=z.policyid
		WHERE (Layer = (@Layer - @tmpInt)) AND allocationID = @AllocationID AND ColumnID = @ColumnID  ) = 182  
		AND (SELECT MAX(ProvisionValueID) FROM ztblpolicycolumn PC 
		INNER JOIN ztblPolicyAllocation z ON pc.policydateID = z.PolicydateID
		INNER JOIN tblpolicylimits L on L.policyid=z.policyid
		WHERE Layer = @Layer AND ColumnID = @ColumnID AND AllocationID = @AllocationID) = 182  ) OR (
		--Or insolvent coverage
		(SELECT TOP 1 CarrierSolvency FROM ztblpolicycolumn PC 
		INNER JOIN ztblPolicyAllocation z ON pc.policydateID = z.PolicyDateID
		INNER JOIN tblpolicylimits L on L.policyid=z.policyid
		INNER JOIN tblPolicy P ON z.PolicyID = P.PolicyID
		INNER JOIN tblCarrier C ON P.CarrierID = C.CarrierID
		WHERE (Layer = (@Layer - @tmpInt)) AND ColumnID = @ColumnID AND allocationID =@allocationID
		GROUP BY CarrierSolvency ORDER BY CarrierSolvency ) = 'Insolvent'  
		AND (SELECT MAX(ProvisionValueID) FROM ztblpolicycolumn PC 
		INNER JOIN ztblPolicyAllocation z ON pc.PolicyDateID = z.PolicyDateID
		INNER JOIN tblpolicylimits L on L.policyid=z.policyid
		WHERE AllocationID =@AllocationID AND Layer = @Layer AND ColumnID = @ColumnID ) = 182 )

BEGIN
	
	
	INSERT INTO @tempTable (ColumnID, AttachmentPoint)
	SELECT ColumnID, min(pc.AttachmentPoint )
	FROM ztblpolicycolumn PC INNER JOIN ztblPolicyAllocation z ON pc.policydateid = z.policydateid
	INNER JOIN tblpolicylimits L on L.policyid=z.policyid
	WHERE Layer = @Layer - @tmpInt AND ColumnID = @ColumnID  AND allocationID = @allocationID
	GROUP BY columnid, pc.attachmentpoint

	SELECT @tmpInt = @tmpInt + 1
		
END
	
	
RETURN (SELECT  Min(AttachmentPoint) FROM @tempTable)


END


GO
GRANT VIEW DEFINITION ON  [dbo].[SigDropDown] TO [Allocation_User]
GRANT EXECUTE ON  [dbo].[SigDropDown] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[SigDropDown] TO [power_user]
GRANT EXECUTE ON  [dbo].[SigDropDown] TO [power_user]
GO
