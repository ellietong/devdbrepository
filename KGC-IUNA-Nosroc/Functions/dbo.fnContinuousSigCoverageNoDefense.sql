SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Function [dbo].[fnContinuousSigCoverageNoDefense]
(@ColumnID int, @Layer int, @allocationid int, @limittypeID int) 

Returns money

AS 

--For Testing
--DECLARE @ColumnID int
--DECLARE @PolicyID int
--DECLARE @LimitTypeID int
--DECLARE  @Layer int
--DECLARE @Allocationid int

--SET @ColumnID = 19860
--SET @PolicyID =189
--SET @LimitTypeID =2
--SET  @Layer =1
--SET @allocationID = 219


BEGIN
--JKC 1/13/2009 calculates the lowest attachment point a signatory can "drop-down" to
-- during the signatory only portion of Wellington
-- declare temporary integer variable for looping
-- drop down over sig coverage and insolvent

DECLARE @tmpInt int
SET @tmpInt = @layer 

DECLARE @tempTable TABLE (ColumnID int, ContinuousSigPerOccRemaining money)
--while the tmpint is greater than the layer of the policy you are testing
--1. test that the layer above has a sig
--2. test that the current layer has a sig


WHILE @tmpInt >= @Layer AND ((SELECT MAX(ProvisionValueID) FROM ztblpolicycolumn PC 
		INNER JOIN ztblPolicyAllocation z on z.policydateid=PC.policydateid
		INNER JOIN tblPolicyLimits L ON l.policyid = z.policyid
		WHERE (Layer = (@tmpInt)AND LimitTypeID = @LimitTypeID) AND ColumnID = @ColumnID  ) = 182  
		AND (SELECT MAX(ProvisionValueID) FROM ztblpolicycolumn PC 
		INNER JOIN ztblPolicyAllocation z ON PC.policydateID = z.PolicyDateID
		INNER JOIN tblpolicylimits L on L.policyid=z.policyid
		WHERE Layer = @Layer AND ColumnID = @ColumnID ) = 182  ) 
	

BEGIN
	
	
	INSERT INTO @tempTable (ColumnID, ContinuousSigPerOccRemaining)
	SELECT ColumnID, SUM((OccurrenceRemaining-ColumnAllocation) * (ColumnDays/PolicyDaysInColumn)) --prorate occ remaining based upon dates in column
	FROM ztblpolicycolumn PC 
	INNER JOIN ztblPolicyAllocation z ON z.policydateid = pc.policydateID
	INNER JOIN tblpolicylimits L on L.policyid=z.policyid
	WHERE Layer = @tmpInt AND ColumnID = @ColumnID AND LimitTypeID = @LimitTypeID
	AND AllocationID = @allocationID
	AND Provision1 <>4
	GROUP BY columnid

	SELECT @tmpInt = @tmpInt + 1
		
END
	
	
RETURN (SELECT  SUM(ContinuousSigPerOccRemaining) FROM @tempTable)


END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnContinuousSigCoverageNoDefense] TO [Allocation_User]
GRANT EXECUTE ON  [dbo].[fnContinuousSigCoverageNoDefense] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[fnContinuousSigCoverageNoDefense] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnContinuousSigCoverageNoDefense] TO [power_user]
GO
