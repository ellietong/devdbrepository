SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Function [dbo].[fnDefenseConsidered]
--Converted to SQL by SHM 03/20/2006
(@AllocationID int, @OccurrenceID int) 
Returns money
AS 
BEGIN

DECLARE @Costs money

SET @Costs = 0

If (SELECT Cost1 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost1 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost1Discount FROM tblAllocation WHERE AllocationID = @AllocationID)
If (SELECT Cost2 FROM tblAllocation WHERE AllocationID =@AllocationID) = 1
    SET @Costs = @Costs + (SELECT Cost2 FROM tblOccurrence WHERE OccurrenceID = @OccurrenceID)
	* (SELECT Cost2Discount FROM tblAllocation WHERE AllocationID = @AllocationID)

RETURN @Costs

END
GO
GRANT EXECUTE ON  [dbo].[fnDefenseConsidered] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnDefenseConsidered] TO [power_user]
GO
