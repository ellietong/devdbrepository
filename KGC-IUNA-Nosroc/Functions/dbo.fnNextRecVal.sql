SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE   FUNCTION [dbo].[fnNextRecVal]
(@pStartDate datetime, @insuranceProgramID int, @allocationID int)
RETURNS datetime
AS
BEGIN
-- declare temporary table
DECLARE @zPStartDates table (RowIndex int IDENTITY, 
	InsuranceProgramID int, PStartDate datetime)
-- declare temporary integer variable
DECLARE @tmpInt int
-- insert policy start dates, grouped by insurance program id into temporary table
INSERT INTO @zPStartDates (InsuranceProgramID, PStartDate)
SELECT InsuranceProgramID, PStartDate
FROM ztblPolicyAllocation
WHERE InsuranceProgramID = @insuranceProgramID AND 
	AllocationID = @allocationID
GROUP BY InsuranceProgramID, PStartDate
UNION
SELECT InsuranceProgramID, PEndDate
FROM ztblPolicyAllocation
WHERE InsuranceProgramID = @insuranceProgramID AND
	AllocationID = @allocationID
GROUP BY InsuranceProgramID, PEndDate
ORDER BY PStartDate ASC
-- Find the row index corresponding to @pStartDate
SET @tmpInt = 0
SET @tmpInt = (
	SELECT RowIndex
	FROM @zPStartDates
	WHERE PStartDate = @pStartDate
)
-- Increase the row index by 1
SET @tmpInt = @tmpInt + 1
-- Return the PStartDate corresponding to the increased row index
RETURN (SELECT PStartDate FROM @zPStartDates WHERE RowIndex = @tmpInt)
END
GO
GRANT EXECUTE ON  [dbo].[fnNextRecVal] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnNextRecVal] TO [power_user]
GO
