SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByCarrier]
(@allocationID int = NULL, @CarrierGroupID int = NULL)
As

SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 83
--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = 0

DECLARE @maxAllocationID int
DECLARE @maxCarrierGroupID int

SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
IF @CarrierGroupID = 0 SET @CarrierGroupID = NULL

DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
CarrierID int, CarrierName nvarchar(250))
INSERT INTO @Policy

SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName
FROM vPolicy
GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName

SELECT ar.DateToPresentValue, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' (' + pinfo.CarrierGroupName + ')' END AS 'CarrierGroupInfo',
	pinfo.CarrierGroupID, pinfo.CarrierID, MAX(ar.DateTime) AS [DateTime], pinfo.CarrierGroupName, pinfo.CarrierName, CONVERT(money, SUM(ar.PV)) AS PV, 
	SUM(ar.Allocation) AS Allocation, SUM(ar.SIRAllocated) AS SIRAllocated, a.AllocationID, a.AllocationDescription, YieldCurveDate as PVRateDate
FROM vAllocationResults ar 
	LEFT JOIN @Policy pinfo ON pinfo.PolicyDateID = ar.PolicyDateID
	LEFT JOIN vAllocation a ON a.AllocationID = ar.AllocationID
	LEFT JOIN tblYieldCurve yc on yc.YieldCurveID = ar.YieldCurveID 
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationid) AND
	(coalesce(pinfo.CarrierGroupID,0) >= COALESCE(@CarrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID,0) <= COALESCE(@CarrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID,0) = @carrierGroupID)
GROUP BY ar.DateToPresentValue, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' (' + pinfo.CarrierGroupName + ')' END, 
	pinfo.CarrierGroupID, pinfo.CarrierID, pinfo.CarrierGroupName, pinfo.CarrierName, a.AllocationID, a.AllocationDescription, YieldCurveDate 
ORDER BY pinfo.CarrierName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByCarrier] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByCarrier] TO [power_user]
GO
