SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryrptNewFilingsByState] (@StateID int, @StartDate datetime, @EndDate datetime, @Matter int)

AS
SET NOCOUNT ON
--Declare @stateid int
--set @stateid = null
--declare @startdate datetime
--set @startdate = '7/1/2011'
--declare @enddate datetime
--set @enddate = '9/30/2011'
--declare @matter int
--set @matter = 2


--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState

Declare @DiseaseCount Table (ClaimID int, Matter nvarchar(100), MatterID int, 
FullNameState nvarchar(250), YearFiled int, Disease nvarchar (100), lastname nvarchar (100), firstname nvarchar(100), socialsecuritynumber nvarchar (250),filedate datetime, servicedate datetime,
localplaintiffcounsel nvarchar (200), jurisdiction nvarchar (200), docketnumber nvarchar (100), ClaimSequence nvarchar (200))
Insert into @DiseaseCount

Select C.ClaimID, Matter, C.MatterID, S.FullNameState,
COALESCE(YEAR(ServiceDate), YEAR(FileDate)) as YearFiled, coalesce(HighLevelDiseaseID,5) as Disease, CPI.LastName, CPI.FirstName, COALESCE(RIGHT(CPI.SocialSecurityNumber,4),''), L.FileDate,
L.ServiceDate, PCA.PlaintiffCounsel, j.Jurisdiction, DocketNumber, ClaimSequence

From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID

LEFT Join (select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD where  PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
Left Join tblDisease D on CD.DiseaseID=D.DiseaseID

Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblPlaintiffCounselAddress PCA on PCA.PlaintiffCounselAddressID=L.PlaintiffCounselAddressID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
--Left Join tblDefenseCounselState DCS on L.PrimaryDefenseCounselID=DCS.DefenseCounselID AND L.StateID=DCS.StateID
Left Join tblState S on L.StateID=S.StateID
left join tblClaimSequence csq on csq.ClaimSequenceID = c.ClaimSequenceID
left join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
	
Where  s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) 
	AND	ProcessedDate between @StartDate and @EndDate and IsPrimaryForClaim = 1 
	AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
	AND COALESCE(cpi.jobsiteinfo, 0) = 0 and clst.ClaimLawsuitStatusID = 22
	 
	group by  s.State, C.ClaimID, Matter, C.MatterID, S.FullNameState,
COALESCE(YEAR(ServiceDate), YEAR(FileDate))  , coalesce(HighLevelDiseaseID,5)  , CPI.LastName, CPI.FirstName, COALESCE(RIGHT(CPI.SocialSecurityNumber,4),''), L.FileDate,
L.ServiceDate, PCA.PlaintiffCounsel, j.Jurisdiction, DocketNumber, ClaimSequence
Order By S.State, Year(ServiceDate), Jurisdiction, PCA.PlaintiffCounsel


Select Matter, MatterID, FullNameState, YearFiled AS 'Year',hd.HighLevelDisease, LastName, FirstName, SocialSecurityNumber, coalesce(FileDate, '1/1/1900') as 'filedate', coalesce(ServiceDate,'1/1/1900') as 'servicedate', localplaintiffcounsel, Jurisdiction, DocketNumber
,ClaimSequence,@startDate as StartDate, @EndDate as EndDate
from @DiseaseCount DC
left join tblHighLevelDisease hd on hd.HighLevelDiseaseID = DC.Disease
ORDER BY Matter, FullNameState, COALESCE(ServiceDate, FileDate), lastname, firstname

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByState] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByState] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByState] TO [power_user]
GO
