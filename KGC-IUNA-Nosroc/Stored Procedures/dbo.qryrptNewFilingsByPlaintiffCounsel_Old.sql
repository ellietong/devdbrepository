SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptNewFilingsByPlaintiffCounsel_Old]
(@BeginDate datetime, @EndDate datetime, @matterid int, @StateID int)
As 
Set NoCount On


-- Declare @BeginDate datetime
-- Declare @EndDate datetime

-- Set @BeginDate = '4/1/2010'
-- Set @EndDate = '6/30/2010'
-- Declare @StateID int
----Set @StateID = 47
--Declare @matter int
--set @matter = 1

DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState

Declare @AllResponsibleDefenseCounsel table (DefenseCounselID int, 
				DefenseCounsel nvarchar(100), 
				ResponsibleDefenseCounselID int,
				StateID int, 
				ResponsibleDefenseCounsel nvarchar(100),
				IsActiveCounsel bit)
Insert Into @AllResponsibleDefenseCounsel
Select dcs.DefenseCOunselID,
	d.DefenseCounsel as DefenseCounsel,
	dcs.responsibledefensecounselID,
	StateID as StateID,
	r.DefenseCOunsel as ResponsibleDefenseCOunsel,
	IsActiveCounsel
From tblDefenseCounselState dcs Inner Join tblDefenseCounsel d on d.defensecounselID = dcs.defensecounselID
	Inner JOin tblDefenseCounsel r on r.defensecounselID = dcs.responsibledefensecounselID
Group By d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCOunselID, dcs.responsibledefensecounselID, IsActiveCounsel, StateID



Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int,  PrimaryLocalPlaintiffCounsel nvarchar(1000),FilingCount int)
Insert into @DiseaseCount

Select Matter, C.MatterID,PrimaryLocalPlaintiffCounsel, 1


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
--Meso
Left Join tblDisease DDefenseMeso on C.DefenseDiseaseID=DDefenseMeso.DiseaseID
Left Join tblDisease DPlaintiffMeso on C.PlaintiffDiseaseID=DPlaintiffMeso.DiseaseID
--Lung Cancer
Left Join tblDisease DDefenseLC on C.DefenseDiseaseID=DDefenseLC.DiseaseID
Left Join tblDisease DPlaintiffLC on C.PlaintiffDiseaseID=DPlaintiffLC.DiseaseID
--Other Cancer
Left Join tblDisease DDefenseOC on C.DefenseDiseaseID=DDefenseOC.DiseaseID
Left Join tblDisease DPlaintiffOC on C.PlaintiffDiseaseID=DPlaintiffOC.DiseaseID
--Non-Malig
Left Join tblDisease DDefenseNM on C.DefenseDiseaseID=DDefenseNM.DiseaseID
Left Join tblDisease DPlaintiffNM on C.PlaintiffDiseaseID=DPlaintiffNM.DiseaseID
--Unknown
Left Join tblDisease DDefenseUnk on C.DefenseDiseaseID=DDefenseUnk.DiseaseID
Left Join tblDisease DPlaintiffUnk on C.PlaintiffDiseaseID=DPlaintiffUnk.DiseaseID


Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
Left Join tblDefenseCounselState DCS on L.PrimaryDefenseCounselID=DCS.DefenseCounselID AND L.StateID=DCS.StateID
Left Join tblState S on L.StateID=S.StateID
Left Join @AllResponsibleDefenseCounsel r on r.DefenseCounselID = l.PrimaryDefenseCounselID and r.StateID = l.StateID

Where  coalesce(ServiceDate,FileDate) between @BeginDate and @EndDate  and s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) 
	and IsPrimaryForClaim = 1
Order By PrimaryLocalPlaintiffCounsel


Select Matter, MatterID, PrimaryLocalPlaintiffCounsel, Sum(FilingCount) as NewFilingCount, @BeginDate as StartDate, @EndDate as EndDate
from @DiseaseCount
Group By Matter, MatterID, PrimaryLocalPlaintiffCounsel
ORDER BY Matter, PrimaryLocalPlaintiffCounsel

Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByPlaintiffCounsel_Old] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByPlaintiffCounsel_Old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByPlaintiffCounsel_Old] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByPlaintiffCounsel_Old] TO [power_user]
GO
