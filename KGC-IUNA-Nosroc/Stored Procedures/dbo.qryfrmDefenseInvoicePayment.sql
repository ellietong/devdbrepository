SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmDefenseInvoicePayment] (@DefenseInvoiceID int)
as 

Select SourceDate, 
	SourceNumber,
	PaymentAmount,
	DefenseInvoiceID
From tblCheck c
left join tblPaymentDefense pd on pd.CheckID = c.CheckID
Where DefenseInvoiceID = @DefenseInvoiceID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefenseInvoicePayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDefenseInvoicePayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefenseInvoicePayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDefenseInvoicePayment] TO [power_user]
GO
