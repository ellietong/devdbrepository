SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyWithSubscriberOptions]
(@SubscriberID int = NULL, @Solvency varchar(50) = NULL, @limitTypeID int = null, @InsuranceProgramID int = NULL)
As

SET NOCOUNT ON

DECLARE @maxSubscriberID int
DECLARE @maxInsuranceProgramID int
DECLARE @maxLimitTypeID int

SELECT @maxSubscriberID = MAX(SubscriberID) FROM tblSubscriberCompany
SELECT @maxInsuranceProgramID  = MAX (InsuranceProgramID) FROM tblInsuranceProgram
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM tblPolicyLimitType

SELECT MIN(p.PStartDate) AS MinOfPStartDate, MAX(p.PEndDate) AS MaxOfPEndDate, MIN(p.AnnualPeriod) AS MinOfAnnualPeriod, p.InsuranceProgram, 
	scomp.SubscriptionCompany, scomp.SubscriptionCompanySolvency, scov.Percentage, (scov.Percentage * p.PerOccLimit) AS SubscriberPerOcc, p.CarrierID, 
	p.InsuranceProgramID, AttachmentPoint, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, Layer, p.PolicyNum, p.PolicyID, scov.SubscriberID, 
	 scomp.AgencyName, scov.UWReference, scomp.PolicyType,
	scov.SubscriptionNotes,  LimitType, p.LimitTypeID, scov.SubscriptionCoverageID
FROM vPolicy p
	INNER JOIN tblSubscriptionCoverage scov ON p.PolicyID = scov.PolicyID
	INNER JOIN vSubscription scomp ON scomp.SubscriberID = scov.SubscriberID
WHERE p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND
	p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) AND
	p.LimitTypeID >= COALESCE(@limitTypeID, 0) AND p.limitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	scomp.SubscriberID >= COALESCE(@SubscriberID, 0) AND scomp.SubscriberID <= COALESCE(NULLIF(@SubscriberID, 0), @maxSubscriberID) AND
	scomp.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%')
	And DoNotReport <>1
GROUP BY p.InsuranceProgram, scomp.SubscriptionCompany, scomp.SubscriptionCompanySolvency, scov.Percentage, (scov.Percentage * p.PerOccLimit), p.CarrierID, p.InsuranceProgramID, 
	AttachmentPoint, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, Layer, PolicyNum, p.PolicyID, scov.SubscriberID, 
	 scomp.AgencyName, scov.UWReference, scomp.PolicyType, scov.SubscriptionNotes,LimitType, p.LimitTypeID, scov.SubscriptionCoverageID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyWithSubscriberOptions] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyWithSubscriberOptions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyWithSubscriberOptions] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyWithSubscriberOptions] TO [power_user]
GO
