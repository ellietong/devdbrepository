SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--Input Parameters
--@MatterID=forms!frmMasterReports!MatterID


CREATE Proc [dbo].[qryrptOutstandingPayableSettlements_New_Pre2009] (@MatterID int, @SettlementstartDate datetime, @settlementEnddate datetime)

As

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 1

--DECLARE @SettlementstartDate datetime
--set @SettlementstartDate = '1/1/2007'
--declare @settlementenddate datetime
--SET @SettlementendDate = '12/31/2007'

DECLARE @MaxMatterID int
SELECT @MaxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @SettlementGroupPaidUnpaid TABLE (SettlementGroupID int, Paid money, Unpaid money, Total money)
INSERT INTO @SettlementGroupPaidUnpaid
	SELECT SettlementGroupID, 
	SUM( CASE WHEN PaidDate IS NULL THEN 0 ELSE SettlementAmount END) As Paid,
	SUM( CASE WHEN PaidDate IS NULL THEN SettlementAmount ELSE 0 END) as Unpaid, 
	SUM(SettlementAmount) as Total
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupCount TABLE (SettlementGroupID int, ClaimCount nvarchar(50))
INSERT INTO @SettlementGroupCount	
	SELECT SettlementGroupID, COUNT(SettlementDate) as ClaimCount
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupPaidCount TABLE (SettlementGroupID int, PaidCount nvarchar(50))
INSERT INTO @SettlementGroupPaidCount	
	SELECT SettlementGroupID, COUNT(paiddate) as PaidCount
	FROM tblClaimantSettlement
	WHERE SettlementGroupID IN	(SELECT SettlementGroupID
								 FROM tblClaimantSettlement
								 WHERE paiddate IS NOT NULL 
								 GROUP BY SettlementGroupID)
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupMaxDate TABLE (SettlementGroupID int, MaxSettlementDate datetime)
INSERT INTO @SettlementGroupMaxDate
	SELECT SettlementGroupID, MAX(SettlementDate) as MaxSettlementDate
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY MaxSettlementDate


SELECT pca.PlaintiffCounsel , cs.SettlementDate, SettlementDueDate as DatePayable, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, CheckNumber, spd.ReleaseSignedDate, FullyDocumented, c.MatterID, Matter, cl.LawsuitID, PaidDate,
	@SettlementstartDate AS SettlementDateAsOf, sgp.Paid, sgp.Unpaid, DocketNumbers, COALESCE(sgpc.PaidCount,0) AS PaidCount, sgc.ClaimCount, MaxSettlementDate, c.claimid,
 Jurisdiction, st.State
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m						 ON c.MatterID = m.MatterID		
	INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs			 ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblLawsuit l						 oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaimLawsuitStatus cls		 ON cl.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID	
	LEFT  JOIN tblSettlementGroup s				 ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblClaimantDocketNumbers cd		 ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT  JOIN @SettlementGroupPaidUnpaid sgp	 ON s.SettlementGroupID = sgp.SettlementGroupID
	LEFT  JOIN @SettlementGroupCount sgc		 ON s.SettlementGroupID= sgc.SettlementGroupID
	LEFT  JOIN @SettlementGroupPaidCount sgpc	 ON s.SettlementGroupID = sgpc.SettlementGroupID
	LEFT  JOIN @SettlementGroupMaxDate sgm		 ON s.SettlementGroupID = sgm.SettlementGroupID
	LEFT  JOIN tblReleasetype crr				 ON crr.ReleaseTypeID =spd.ReleaseTypeID
	LEFT  JOIN tblJurisdiction j				 ON l.JurisdictionID = j.JurisdictionID
	LEFT  JOIN tblPlaintiffCounselAddress pca	 ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	LEFT  JOIN tblState st 					     ON st.StateID = l.StateID
	LEFT  JOIN tblclaimsequence sq on sq.ClaimSequenceID = c.ClaimSequenceID
WHERE COALESCE(IsCurrentClaim,0) = 1 
	AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID) 
	AND PaidDate is null 
	AND COALESCE(PaidCount,0) <> COALESCE(ClaimCount,0)
	AND SettlementDate >=@SettlementstartDate and SettlementDate <=@settlementenddate
	AND sq.ClaimSequenceID not in (4,2,3,11)
	AND COALESCE(cpi.jobsiteinfo, 0) = 0
GROUP BY cs.SettlementDate, SettlementDueDate, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, CheckNumber, spd.ReleaseSignedDate, FullyDocumented, c.MatterID, Matter, cl.LawsuitID,paiddate,l.PrimaryLocalPlaintiffCounsel ,
	sgp.Paid, sgp.Unpaid, DocketNumbers, sgpc.PaidCount, sgc.ClaimCount, MaxSettlementDate,spd.ReleaseRcvd, GETDATE() - SettlementDueDate, c.ClaimID,
 Jurisdiction, PlaintiffCounsel, st.State
ORDER BY pca.PlaintiffCounsel, MaxSettlementDate, SettlementGroup, SettlementDate, LastName
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingPayableSettlements_New_Pre2009] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingPayableSettlements_New_Pre2009] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingPayableSettlements_New_Pre2009] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingPayableSettlements_New_Pre2009] TO [power_user]
GO
