SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[REVERT_SP_MOSS_qryInvoiceTypeFilterList] 

AS
SET NOCOUNT ON
--DECLARE @invoiceFlag bit
--SET @invoiceFlag = 0
--DECLARE @settlementFlag bit
--SET @settlementFlag = 0

	BEGIN
		Select 
			ClaimantDocumentTypeID, 
			ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID =2 AND ClaimantDocumentTypeID <> 41
		UNION
		SELECT null, '<< All Invoice Types >>', null
		ORDER BY ClaimantDocumentType
	END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryInvoiceTypeFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryInvoiceTypeFilterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryInvoiceTypeFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryInvoiceTypeFilterList] TO [power_user]
GO
