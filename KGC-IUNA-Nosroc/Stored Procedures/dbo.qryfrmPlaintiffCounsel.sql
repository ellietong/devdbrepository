SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmPlaintiffCounsel]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PlaintiffCounselID, PlaintiffCounsel, PlaintiffCounselTaxIDNum FROM [KGA-ClaimsAdmin].dbo.tblPlaintiffCounsel
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPlaintiffCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPlaintiffCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPlaintiffCounsel] TO [power_user]
GO
