SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[REVERT_SP_MOSS_qryDocuments]
( @DocID int)
AS
--DECLARE @DocID int
--SET @DocID = 2



--GET LIST OF CLAIMANTS FOR DOCUMENT

 
DECLARE @Claimants nvarchar(max)
SET @Claimants= ''  
DECLARE @SettleDocs nvarchar(max)
SET @SettleDocs= ''  

DECLARE @addClaimant nvarchar(max)
DECLARE @addClaimantID int

DECLARE @DocCat nvarchar(max)
DECLARE @PageNum nvarchar(max)

DECLARE Cur_Claimants CURSOR Local Fast_Forward for
SELECT mcd.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID) + ')'--,
--ClaimLawsuitStatus, DismissalDate, DismissalProcessDate
FROM tblMultiClaimantDocument mcd
INNER JOIN tblDocuments d ON d.DocumentID = mcd.DocumentID
LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblClaimLawsuitStatus cls ON d.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
WHERE mcd.DocumentID = @DocID
UNION
SELECT d.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), d.ClaimantPersonalInfoID) + ')'--,
--ClaimLawsuitStatus, DismissalDate, DismissalProcessDate
FROM tblDocuments d
LEFT JOIN tblClaimantPersonalInfo cpi ON d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblClaimLawsuitStatus cls ON d.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
WHERE d.DocumentID = @DocID AND d.ClaimantPersonalInfoID IS NOT NULL


--Open Docket Cursor
OPEN Cur_Claimants

--Fetch Data
FETCH NEXT FROM Cur_Claimants INTO 

@addClaimantID, @addClaimant

WHILE @@Fetch_Status = 0
BEGIN
      --assemble the string
      if LEN(@Claimants) = 0 SET @Claimants = '<a target="_blank" href="/clients/iuna_nosroc/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + COALESCE(CONVERT(nvarchar(10), @addClaimantID), '') + '">' + COALESCE(@addClaimant, '') + '</a>'
      else SET @Claimants =  @Claimants  + '<br />' +  '<a target="_blank" href="/clients/iuna_nosroc/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + COALESCE(CONVERT(nvarchar(10), @addClaimantID), '') + '">' + COALESCE(@addClaimant, '') + '</a>'
      PRINT @Claimants


      FETCH NEXT FROM Cur_Claimants INTO @addClaimantID, @addClaimant
END

CLOSE Cur_Claimants 
DEALLOCATE Cur_Claimants


-----------------------------------------------------------------


DECLARE Cur_SettleDoc CURSOR Local Fast_Forward for
SELECT DocCategory, PageNums
--ClaimLawsuitStatus, DismissalDate, DismissalProcessDate
FROM tblSettleDocs sd 
LEFT JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
WHERE DocumentID = @DocID

--Open Docket Cursor
OPEN Cur_SettleDoc

--Fetch Data
FETCH NEXT FROM Cur_SettleDoc INTO @DocCat, @PageNum

WHILE @@Fetch_Status = 0
BEGIN
      --assemble the string
      if LEN(@SettleDocs) = 0 SET @SettleDocs = COALESCE(@DocCat, '') + '&nbsp;&nbsp;pg(s): ' + COALESCE(@PageNum, '')
      else SET @SettleDocs =  @SettleDocs  + '<br />' +  COALESCE(@DocCat, '') + '&nbsp;&nbsp;pg(s): ' + COALESCE(@PageNum, '')
      PRINT @SettleDocs


      FETCH NEXT FROM Cur_SettleDoc INTO @DocCat, @PageNum
      
END


CLOSE Cur_SettleDoc 
DEALLOCATE Cur_SettleDoc

------------------------------------------------------------------------------------------------------------------




SELECT 
	cast(d.DocumentID as varchar(50)) as DocumentId,
	dt.ClaimantDocumentType, 
	dg.DocumentGroup, 		
	d.Comments,
	d.DocumentURL,
	d.DocumentTitle,
	d.SPDocId,
	d.CreatedBy,
	d.DateCreated,
	d.ModifiedBy,
	d.DateModified,
	d.Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	d.ApprovedBy,
	d.DateApproved,
	f.Firm,
	COALESCE(@Claimants, '') AS Claimants,
	CASE WHEN LEN(@SettleDocs)=0 THEN 'N/A' ELSE COALESCE(@SettleDocs, 'N/A') END AS SettleDocs,
	d.DismissalDate,
	DismissalProcessDate,
	ClaimLawsuitStatus AS DismissalType,
	DocumentSource,
	d.DocumentSourceID,
	
	--Settlement Info Only
	NULL as SettlementGroupID,
	NULL as SettlementDate,
	NULL as SettlementAmount,

	--Settlements and Claimants
	NULL as Claimant,
	NULL as SocialSecurityNumber,
	NULL as BirthDate,
	NULL as disease, 	
	NULL as claimlawsuitstatus, 
	NULL as IsCurrentClaim, 
	
	--not used for invoices
	NULL as docketnumber, 
	NULL as CaseCaption,
	NULL as stateabb, 
	
	--lawsuits only
	NULL as jurisdiction,
	NULL as LeadPlaintiff,	
	NULL as filedate, 
	NULL as servicedate, 
	NULL as primarylocalplaintiffcounsel,

	--Invoices Only
	d.InvoiceNumber,
	d.Period,
	d.DatePayable,
	d.FeeAmount,
	d.DisbursementAmount,
	d.Adjustment,
	d.AdjustmentNotes,
	dis.Status,	
	di.SubmitDate,
	PrivateComments,
			

	
	--Court Documents Only
	NULL as DateOfFiling,
	NULL as DateOfDeath,
	DocketNumbers
	

	
FROM tblDocuments d
	JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
	JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
	INNER JOIN tblDocumentSource ds ON d.DocumentSourceID = ds.DocumentSourceID
	--invoices only
	LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
	LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
	LEFT OUTER JOIN tblClaimLawsuitStatus cls ON cls.ClaimLawsuitStatusID = d.ClaimLawsuitStatusID
	
WHERE (d.DetailDocumentTypeID = 0 OR dg.DocumentGroupID = 2)
	AND d.DocumentID = @DocID


--STEP TWO: Get Settlements and Claimants
UNION

SELECT 
	cast(d.DocumentID as varchar(50)) as DocumentId,
	dt.ClaimantDocumentType, 
	dg.DocumentGroup, 		
	d.Comments,
	d.DocumentURL,
	d.DocumentTitle,
	d.SPDocId,
	d.CreatedBy,
	d.DateCreated,
	d.ModifiedBy,
	d.DateModified,
	d.Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	d.ApprovedBy,
	d.DateApproved,
	f.Firm,
	@Claimants AS Claimants,
	CASE WHEN LEN(@SettleDocs)=0 THEN 'N/A' ELSE COALESCE(@SettleDocs, 'N/A') END AS SettleDocs,
	d.DismissalDate,
	DismissalProcessDate,
	cls1.ClaimLawsuitStatus AS DismissalType,
	DocumentSource,
	d.DocumentSourceID,
	
	--Settlement Info Only
	d.SettlementGroupID,
	d.SettlementDate,
	d.SettlementAmount,

	--Settlements and Claimants
	COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, '') AS 'Claimant',
	CASE WHEN cpi.SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(cpi.SocialSecurityNumber,4) END AS SocialSecurityNumber,
	cpi.BirthDate,
	di2.disease, 	
	cls.claimlawsuitstatus, 
	c.IsCurrentClaim, 	
	
	--these values used for settlements and claimants, different joins used for lawsuits
	l.docketnumber, 
	l.CaseCaption,
	st.[State], 
	j.jurisdiction,
	
	--lawsuits only	
	NULL as LeadPlaintiff,	
	NULL as filedate, 
	NULL as servicedate, 
	NULL as primarylocalplaintiffcounsel,

	--Invoices Only
	NULL as InvoiceNumber,
	NULL as Period,
	NULL as DatePayable,
	NULL as FeeAmount,
	NULL as DisbursementAmount,
	NULL as Adjustment,
	NULL as AdjustmentNotes,
	NULL as Status,		
	NULL as SubmitDate, 		
	PrivateComments,
	
	--Court Documents Only
	NULL as DateOfFiling,
	NULL as DateOfDeath,
	DocketNumbers	
	
FROM tblDocuments d
	JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
	JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
	INNER JOIN tblDocumentSource ds ON d.DocumentSourceID = ds.DocumentSourceID
	--settlements and claimants only
	LEFT OUTER JOIN tblClaimantPersonalInfo cpi ON d.claimantpersonalinfoid = cpi.claimantpersonalinfoid 
	LEFT OUTER JOIN tblClaim c ON cpi.claimantpersonalinfoid = c.claimantpersonalinfoid
	LEFT OUTER JOIN (SELECT * FROM tblClaimDisease CD WHERE PrimaryDisease = 1) CD ON CD.ClaimID = C.ClaimID
	LEFT OUTER JOIN tblDisease di2 ON CD.DiseaseID = di2.diseaseid
	LEFT OUTER JOIN tblClaimLawsuit cl ON c.claimid = cl.claimid and cl.IsPrimaryForClaim = 1
	LEFT OUTER JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
	LEFT OUTER JOIN tblState st ON l.stateid = st.stateid
	LEFT OUTER JOIN tblJurisdiction j ON l.jurisdictionid = j.jurisdictionid
	LEFT OUTER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT OUTER JOIN tblClaimLawsuitStatus cls ON clst.claimlawsuitstatusid = cls.claimlawsuitstatusid
	LEFT OUTER JOIN tblClaimantSettlement cs ON cl.claimlawsuitid = cs.claimlawsuitid
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
	LEFT OUTER JOIN tblClaimLawsuitStatus cls1 ON cls1.ClaimLawsuitStatusID = d.ClaimLawsuitStatusID
WHERE dg.DocumentGroupId in(1,4,5)
		AND d.DocumentID = @DocID
	--AND IsCurrentClaim = 1

-- STEP THREE: Get Lawsuits
UNION

SELECT 
	cast(d.DocumentID as varchar(50)) as DocumentId,
	dt.ClaimantDocumentType, 
	dg.DocumentGroup, 		
	d.Comments,
	d.DocumentURL,
	d.DocumentTitle,
	d.SPDocId,
	d.CreatedBy,
	d.DateCreated,
	d.ModifiedBy,
	d.DateModified,
	d.Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	d.ApprovedBy,
	d.DateApproved,
	f.Firm,
    @Claimants AS Claimants,
    CASE WHEN LEN(@SettleDocs)=0 THEN 'N/A' ELSE COALESCE(@SettleDocs, 'N/A') END AS SettleDocs,
    d.DismissalDate,
	DismissalProcessDate,
	cls1.ClaimLawsuitStatus AS DismissalType,
	DocumentSource,
	d.DocumentSourceID,
	
	--Settlement Info Only
	NULL as SettlementGroupID,
	NULL as SettlementDate,
	NULL as SettlementAmount,

	--Settlements and Claimants
	COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, '') AS 'Claimant',
	NULL as SocialSecurityNumber,
	NULL as BirthDate,
	NULL as disease, 	
	NULL as claimlawsuitstatus, 
	NULL as IsCurrentClaim, 	
	
	--settlements and claimants, values through different joins for lawsuits
	l.docketnumber, 
	l.CaseCaption,
	s.[State], 
	
	--lawsuits only
	j.jurisdiction,
	l.leadplaintifflastname+', '+l.leadplaintifffirstname AS LeadPlaintiff,	
	l.filedate, 
	l.servicedate, 
	pca.PlaintiffCounsel AS primarylocalplaintiffcounsel,

	--Invoices Only
	NULL as InvoiceNumber,
	NULL as Period,
	NULL as DatePayable,
	NULL as FeeAmount,
	NULL as DisbursementAmount,
	NULL as Adjustment,
	NULL as AdjustmentNotes,
	NULL as Status,		
	NULL as SubmitDate, 	
	PrivateComments,
	
	--Court Documents Only
	NULL as DateOfFiling,
	NULL as DateOfDeath,
	DocketNumbers
	

FROM tblDocuments d
	LEFT OUTER JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
	LEFT OUTER JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
	INNER JOIN tblDocumentSource ds ON d.DocumentSourceID = ds.DocumentSourceID
	--lawsuits only
	LEFT OUTER JOIN tblLawsuit l ON d.lawsuitid = l.lawsuitid
	LEFT OUTER JOIN tblJurisdiction j ON l.jurisdictionid = j.jurisdictionid
	LEFT OUTER JOIN tblState s ON l.stateid = s.stateid
	LEFT OUTER JOIN tblDefenseCounselState sid ON l.DefenseCounselAddressID = sid.defensecounseladdressid AND l.stateid = sid.stateid
	LEFT OUTER JOIN tblDefenseCounselAddress dca ON sid.responsibledefensecounseladdressid = dca.defensecounseladdressid 
	LEFT OUTER JOIN tblDefenseCounsel dc on dc.DefenseCounselID=dca.defensecounselID
	LEFT OUTER JOIN tblClaimLawsuit cl ON l.lawsuitid = cl.lawsuitid
	LEFT OUTER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	LEFT OUTER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
	LEFT OUTER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	LEFT OUTER JOIN tblClaimLawsuitStatus cls1 ON cls1.ClaimLawsuitStatusID = d.ClaimLawsuitStatusID
WHERE dg.documentgroupid = 3
		AND d.DocumentID = @DocID
GROUP BY cast(d.DocumentID as varchar(50)), dt.ClaimantDocumentType, dg.DocumentGroup, d.Comments, d.DocumentURL, d.DocumentTitle, d.SPDocId,
	d.CreatedBy, d.DateCreated, d.ModifiedBy, d.DateModified, d.Accepted, d.AcceptedBy, d.DateAccepted, d.ApprovedBy, d.DateApproved,
	--settlements and claimants, values through different joins for lawsuits
	l.docketnumber, l.CaseCaption, s.[State], 
	--lawsuits only
	j.jurisdiction, l.leadplaintifflastname+', '+l.leadplaintifffirstname,	l.filedate, l.servicedate, pca.PlaintiffCounsel, f.Firm, COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, ''), DocketNumbers,
	PrivateComments, DismissalDate,	DismissalProcessDate,	cls1.ClaimLawsuitStatus, DocumentSource, d.DocumentSourceID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDocuments] TO [power_user]
GO
