SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmClaimantName-SelectLetter]
as 

Set NoCount ON

select LEFT(LastName, 1) as Letter
from tblClaimantPersonalInfo cpi
WHERE ClaimantPersonalInfoID NOT IN (91146,91147)
group by LEFT(LastName, 1)
order by LEFT(LastName, 1)

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantName-SelectLetter] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantName-SelectLetter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantName-SelectLetter] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantName-SelectLetter] TO [power_user]
GO
