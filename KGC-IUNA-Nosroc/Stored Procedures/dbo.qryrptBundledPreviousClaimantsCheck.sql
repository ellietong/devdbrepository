SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptBundledPreviousClaimantsCheck] (@ClaimantPersonalInfoID bigint)

--(@FirstName nvarchar(150), @LastName nvarchar(150), @MiddleName nvarchar(150), @SSN nvarchar(150), @BirthDate datetime)



AS
SET NOCOUNT ON


Declare @FirstName as nvarchar(150)
Declare @LastName as nVarchar(150)
Declare @MiddleName as nvarchar(150)
Declare @SSN as nvarchar(150)
--Declare @BirthDate as datetime

Set @FirstName = 'select firstname from tblClaimantPersonalInfo where ClaimantPersonalInfoID = @ClaimantPersonalInfoID'
set @LastName = 'select lastname from tblClaimantPersonalInfo where ClaimantPersonalInfoID = @ClaimantPersonalInfoID'
set @middlename = 'select middlename from tblClaimantPersonalInfo where ClaimantPersonalInfoID = @ClaimantPersonalInfoID'
set @SSN = 'select socialsecuritynumber from tblClaimantPersonalInfo where ClaimantPersonalInfoID = @ClaimantPersonalInfoID'
--Set @birthdate = convert(datetime, 'select BirthDate from tblClaimantPersonalInfo where ClaimantPersonalInfoID = @ClaimantPersonalInfoID')


SELECT ip.InjuredPartyID, ip.FirstName as FirstName, ip.MiddleName as MiddleName, ip.LastName as LastName, ip.SSN as SSN, 
	ip.BirthDate as DateOfBirth, ip.DeathDate, c.ClaimSequenceNum, ClaimStatusCode, ClaimStatusDescription, 
	c.DiagnosisDescription, FirstDiagnosisDate, FirstExposureDate, LastExposureDate, c.settlementAmount, c.settlementDate, 
	c.releasetype, ReleaseReceivedDate, l.docketnum, l.statefiled, l.jurisdictiondescription, l.filedate, l.datereceived,
	LocalPlaintiffName, NationalPlaintiffName,
CASE WHEN FirstName LIKE (@FirstName) THEN 1 
WHEN FirstName LIKE (@MiddleName) THEN 1
ELSE 0 END AS FlagFirstName,
CASE WHEN MiddleName LIKE (@FirstName) THEN 1 
WHEN MiddleName LIKE (@MiddleName) THEN 1
ELSE 0 END AS FlagMiddleName,
CASE WHEN LastName LIKE (@LastName) 
THEN 1 ELSE 0 END AS FlagLastName,
CASE WHEN SSN LIKE (@SSN) 
THEN 1 ELSE 0 END AS FlagSocialSecurityNumber,
CASE WHEN BirthDate LIKE (select BirthDate from tblClaimantPersonalInfo where ClaimantPersonalInfoID = @ClaimantPersonalInfoID)
THEN 1 ELSE 0 END AS FlagBirthDate


FROM mtblBundledInjuredParty ip
Left join mtblBundledClaim C on c.injuredpartyid = ip.injuredpartyid
Left join mtblBundledLawsuit L on l.injuredpartyid = c.injuredpartyid and l.ClaimSequenceNum = c.ClaimSequenceNum

WHERE (((((LEFT(FirstName,3) LIKE (LEFT(@FirstName,3)) OR
LEFT(MiddleName,3) LIKE (LEFT(@FirstName,3))) OR
(LEFT (FirstName,3) LIKE (LEFT(@MiddleName,3)))))
AND LastName LIKE (@LastName)) OR (LastName LIKE (@LastName)) OR
(SSN LIKE (@SSN)) OR
(BirthDate = (select BirthDate from tblClaimantPersonalInfo where ClaimantPersonalInfoID = @ClaimantPersonalInfoID)))
and leadlawsuitflag = 'Y'


Union

SELECT fip.InjuredPartyID, fip.FirstName as FirstName, fip.MiddleName as MiddleName, fip.LastName as LastName, fip.SSN as SSN, 
	fip.BirthDate as DateOfBirth, fip.DeathDate, fc.ClaimSequenceNum, ClaimStatusCode, ClaimStatusDescription, 
	fc.diagnosisdescription, FirstDiagnosisDate, FirstExposureDate, LastExposureDate, fc.settlementAmount, fc.settlementDate, 
	fc.releasetype, ReleasedReceivedDate, fl.docketnum, fl.statefiled, fl.jurisdictiondescription, fl.filedate, fl.datereceived,
	LocalPlaintiffName, NationalPlaintiffName,
CASE WHEN FirstName LIKE (@FirstName) THEN 1 
WHEN FirstName LIKE (@MiddleName) THEN 1
ELSE 0 END AS FlagFirstName,
CASE WHEN MiddleName LIKE (@FirstName) THEN 1 
WHEN MiddleName LIKE (@MiddleName) THEN 1
ELSE 0 END AS FlagMiddleName,
CASE WHEN LastName LIKE (@LastName) 
THEN 1 ELSE 0 END AS FlagLastName,
CASE WHEN SSN LIKE (@SSN) 
THEN 1 ELSE 0 END AS FlagSocialSecurityNumber,
CASE WHEN BirthDate LIKE (select BirthDate from tblClaimantPersonalInfo where ClaimantPersonalInfoID = @ClaimantPersonalInfoID) 
THEN 1 ELSE 0 END AS FlagBirthDate
FROM mtblBundledFInjuredParty fip
Left join mtblBundledFClaim fC on fc.injuredpartyid = fip.injuredpartyid
Left join mtblBundledFLawsuit fL on fl.injuredpartyid = fc.injuredpartyid and fl.ClaimSequenceNum = fc.ClaimSequenceNum
WHERE (((((LEFT(FirstName,3) LIKE (LEFT(@FirstName,3)) OR
LEFT(MiddleName,3) LIKE (LEFT(@FirstName,3))) OR
(LEFT (FirstName,3) LIKE (LEFT(@MiddleName,3)))))
AND LastName LIKE (@LastName)) OR (LastName LIKE (@LastName)) OR
(SSN LIKE (@SSN)) OR
(BirthDate = (select BirthDate from tblClaimantPersonalInfo where ClaimantPersonalInfoID = @ClaimantPersonalInfoID)))
and leadlawsuitflag = 'Y'


ORDER BY flaglastname desc, flagfirstname desc, flagsocialsecuritynumber desc, flagbirthdate desc

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBundledPreviousClaimantsCheck] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBundledPreviousClaimantsCheck] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBundledPreviousClaimantsCheck] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBundledPreviousClaimantsCheck] TO [power_user]
GO
