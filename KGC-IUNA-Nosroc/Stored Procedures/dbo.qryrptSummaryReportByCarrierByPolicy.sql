SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptSummaryReportByCarrierByPolicy] (@ExhaustionDateID int, @Matter int, @AllocationID int, @carriergroupid int)
As
Set Nocount on

 --Declare @ExhaustionDateID int
 --Declare @Matter int
 --Declare @AllocationID int
 --DECLARE @CarrierGroupID int 
 
 --Set @ExhaustionDateID = 206
 --Set @Matter = 1
 --Set @AllocationID = 24
 --set @CarrierGroupID=1
 
 DECLARE @policyDateID table (PolicyDateID int)
 INSERT INTO @policyDateID 
 SELECT PolicyDateID 
 FROM tblExhaustion E
 WHERE ExhaustionDateID = @ExhaustionDateID

 
 IF @AllocationID = 24
 BEGIN
	DELETE FROM @policyDateID WHERE PolicyDateID in (93,95,96)
 END
 
  IF @matter = 2
 BEGIN
	DELETE FROM @policyDateID WHERE PolicyDateID in (65,71,78,79,164,165,166,167,168,561,562,563,564,565,566,567,568,569)
 END

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--Set Exhaustion Type
DECLARE @ExhaustionTypeID int
SET @ExhaustionTypeID = (Select ExhaustionTypeID From tblExhaustionDate Where ExhaustionDateID=@ExhaustionDateID)

--Set Insurance Program
DECLARE @InsuranceProgramID int
SET @InsuranceProgramID = (Case When @Matter=1 then '15' When @Matter=2 then '16' END)

--Set Policy Date Type
DECLARE @PolicyDateTypeID int
SET @PolicyDateTypeID = (Case When @ExhaustionTypeID=6 Then '2' Else '1' END)

--Set Limit Type
DECLARE @LimitTypeID int
SET @LimitTypeID = (Case When @ExhaustionTypeID=6 Then '4' Else '3' END)


--CURRENT SHORTFALL
DECLARE @Shortfall TABLE (PolicyDateID int, CurrentIUNAShortfall money, CurrentNOSROCShortfall money, CurrentCombinedShortfall money)
INSERT INTO @Shortfall
SELECT PolicyDateID, 
SUM(CASE WHEN MatterID = 1 THEN IndemnityAllocation+DefenseAllocation ELSE 0 END) AS IUNAShortfall,
SUM(CASE WHEN MatterID = 2 THEN IndemnityAllocation+DefenseAllocation ELSE 0 END) AS NOSROCShortfall,
SUM(IndemnityAllocation+DefenseAllocation) AS CombinedShortfall
FROM tblExhaustion e
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O ON E.OccurrenceID = O.OccurrenceID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID=@ExhaustionDateID and OccurrenceTypeID IN (22,23)
Group By PolicyDateID

--PREVIOUS SHORTFALL
DECLARE @PreviousShortfall TABLE (PolicyDateID int, PreviousIUNAShortfall money, PreviousNOSROCShortfall money, PreviousCombinedShortfall money)
INSERT INTO @PreviousShortfall
SELECT PolicyDateID, 
SUM(CASE WHEN MatterID = 1 THEN IndemnityAllocation+DefenseAllocation ELSE 0 END) AS IUNAShortfall,
SUM(CASE WHEN MatterID = 2 THEN IndemnityAllocation+DefenseAllocation ELSE 0 END) AS NOSROCShortfall,
SUM(IndemnityAllocation+DefenseAllocation) AS CombinedShortfall
FROM tblExhaustion e
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O ON E.OccurrenceID = O.OccurrenceID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<@ExhaustionDateID and OccurrenceTypeID IN (22,23)
Group By PolicyDateID

--Outside Consumption For Other Insurance Program
Declare @OutsideConsumptionOtherInsPro Table (PolicyDateID int, CurrentOutsideConsumptionOtherInsPro money)
Insert Into @OutsideConsumptionOtherInsPro
Select PolicyDateID, SUM(IndemnityAllocation+DefenseAllocation) as OutsideConsumptionOtherInsPro
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O on E.OccurrenceID=O.OccurrenceID
Inner Join tblOccurrenceType OT on O.OccurrenceTypeID=OT.OccurrenceTypeID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID=@ExhaustionDateID and o.MatterID<>@Matter and o.OccurrenceTypeID NOT IN (22,23)
Group By PolicyDateID
 
--Other Outside Consumption For Aetna
Declare @OutsideConsumptionAetna Table (PolicyDateID int, CurrentOutsideConsumptionAetna money)
Insert Into @OutsideConsumptionAetna
Select e.PolicyDateID, SUM(OutsideConsumption) as OutsideConsumptionAetna
From tblExhaustion E
Inner Join tblPolicyDates pd on E.PolicyDateID = pd.PolicyDateID
Inner Join tblPolicy p ON pd.PolicyID = p.PolicyID
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O on E.OccurrenceID=O.OccurrenceID
Inner Join tblOccurrenceType OT on O.OccurrenceTypeID=OT.OccurrenceTypeID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID=@ExhaustionDateID and o.MatterID=@Matter And CarrierID = 2
And PolicyDateTypeID = @PolicyDateTypeID
Group By e.PolicyDateID
 
--Other Outside Consumption Not Aetna
Declare @OutsideConsumptionOther Table (PolicyDateID int, CurrentOutsideConsumptionOther money)
Insert Into @OutsideConsumptionOther
Select PolicyDateID, SUM(OutsideConsumption) as OutsideConsumptionOther
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O on E.OccurrenceID=O.OccurrenceID
Inner Join tblOccurrenceType OT on O.OccurrenceTypeID=OT.OccurrenceTypeID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID=@ExhaustionDateID
Group By PolicyDateID
 
 
--Previous Exhaustion
Declare @Exhaustion Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	PreviousIndemnityAllocation money, PreviousDefenseAllocation money, PreviousDefenseOutsideLimitsAllocation money, PreviousSIRIndemnityAllocation money,
	PreviousSIRDefenseAllocation money, PreviousOutsideConsumption money, LedgerID bigint)
Insert Into @Exhaustion
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, PolicyDateID, IndemnityAllocation,
	DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation, OutsideConsumption, 
	 LedgerID
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Left Join tblOccurrence O on E.OccurrenceID=O.OccurrenceID
--Inner Join tblOccurrenceType OT on O.OccurrenceTypeID=OT.OccurrenceTypeID
Where ExhaustionTypeID = @ExhaustionTypeID and ED.ExhaustionDateID < @ExhaustionDateID --and o.OccurrenceTypeID NOT IN (22,23)


Declare @PreviousExhaustionTotals table (ExhaustionTypeID int, PolicyDateID int, PreviousIndemnity money, PreviousDefense money, PreviousOutsideLimits money)
Insert Into @PreviousExhaustionTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(PreviousIndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(PreviousDefenseAllocation), 0) as Defense,
	coalesce(Sum(PreviousOutsideConsumption), 0) as OutsideConsumption
From @Exhaustion
Group By ExhaustionTypeID, PolicyDateID


--Current Exhaustion
Declare @CurrentExhaustion Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	CurrentIndemnityAllocation money, CurrentDefenseAllocation money, CurrentDefenseOutsideLimitsAllocation money, CurrentSIRIndemnityAllocation money,
	CurrentSIRDefenseAllocation money, CurrentOutsideConsumption money, LedgerID bigint)
Insert Into @CurrentExhaustion
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, PolicyDateID, IndemnityAllocation,
	DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation, OutsideConsumption, 
	 LedgerID
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O on E.OccurrenceID=O.OccurrenceID
Inner Join tblOccurrenceType OT on O.OccurrenceTypeID=OT.OccurrenceTypeID
Where ExhaustionTypeID = @ExhaustionTypeID and ED.ExhaustionDateID = @ExhaustionDateID
AND MatterID = @Matter AND mAllocationID = @AllocationID


Declare @CurrentExhaustionTotals table (ExhaustionTypeID int, PolicyDateID int, CurrentIndemnity money, CurrentDefense money, CurrentDefenseOutsideLimits money, 
CurrentOutsideLimits money)
Insert Into @CurrentExhaustionTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(CurrentIndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(CurrentDefenseAllocation), 0) as Defense,
	coalesce(Sum(CurrentDefenseOutsideLimitsAllocation),0) AS DefenseOutsideLimits,
	coalesce(Sum(CurrentOutsideConsumption), 0) as OutsideConsumption
From @CurrentExhaustion
Group By ExhaustionTypeID, PolicyDateID


----Header Summary
Declare @HeaderSummary Table (ExhaustionType nvarchar(100), Description nvarchar(100), DefenseForm nvarchar(10))
Insert into @HeaderSummary
Select ExhaustionType, Description, 'H'
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, Description, 'I'
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, Description, 'G'
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, Description, ''
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID


--Query Results
Declare @Results Table(OrderID int, OrderName nvarchar(50),policyid bigint, policydateid bigint, carriername nvarchar(100), layer int, policynum nvarchar(100), pstartdate datetime, penddate datetime, aggregatelimit money,
	DefenseForm nvarchar(100), Indemnity money, Defense money, PreviousExhaustion money, OutsideConsumption money, DefenseOutsideLimits money)
Insert Into @Results

--LONDON
select '2' as OrderID, 'London', p.policyid, pd.policydateid, CASE WHEN SubscriptionCompanySolvency LIKE 'Insolvent' THEN 'London Insolvent' ELSE 'London Solvent' END AS CarrierName, 
pl.layer, p.policynum, pd.pstartdate, pd.penddate, pl.aggregatelimit,
CASE When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	SUM(coalesce(CurrentIndemnity,0)*Percentage) as Indemnity, 
	SUM(coalesce(CurrentDefense,0)*Percentage) as Defense, 
	SUM((coalesce(PreviousIndemnity,0) + coalesce(PreviousDefense,0) + coalesce(PreviousOutsideLimits,0) + coalesce(PreviousCombinedShortfall,0))*Percentage) AS PreviousExhaustion,
	SUM(CASE WHEN @AllocationID IN (8,9,10) AND @Matter = 1 THEN coalesce(CurrentNosrocShortfall,0)*Percentage
		WHEN @AllocationID IN (8,9,10) AND @Matter = 2 THEN coalesce(CurrentIUNAShortfall,0)*Percentage
		WHEN @AllocationID IN (3,4,5,24) THEN ((coalesce(CurrentNosrocShortfall,0) + coalesce(CurrentIUNAShortfall,0) + coalesce(CurrentOutsideConsumptionOtherInsPro,0) 
			+ coalesce(CurrentOutsideConsumptionAetna,0) + coalesce(CurrentOutsideConsumptionOther,0)))*Percentage END) AS OtherConsumption,		
	SUM(coalesce(CurrentDefenseOutsideLimits,0)*Percentage) as DefenseOutsideLimits
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	LEFT join tblSubscriptionCoverage sc ON p.PolicyID = sc.PolicyID
	LEFT join tblSubscriberCompany scomp on sc.SubscriberID = scomp.SubscriberID
	Left Join @CurrentExhaustionTotals E on PD.PolicyDateID=E.PolicyDateID
	Left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	Left Join @Shortfall S on pd.PolicyDateID=s.PolicyDateID
	Left Join @PreviousShortfall sf on pd.PolicyDateID=sf.PolicyDateID
	Left Join @OutsideConsumptionOther O on pd.PolicyDateID=O.PolicyDateID
	Left Join @OutsideConsumptionOtherInsPro OO on pd.PolicyDateID=OO.PolicyDateID
	Left Join @OutsideConsumptionAetna A on pd.PolicyDateID=A.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where   limittypeid = @LimitTypeID and provisionlabelID=1 and PolicyDateTypeID=@PolicyDateTypeID 
		and (InsuranceProgramID=14 or InsuranceProgramID = @InsuranceProgramID)and c.carrierid = 20
GROUP BY p.policyid, pd.policydateid, CASE WHEN SubscriptionCompanySolvency LIKE 'Insolvent' THEN 'London Insolvent' ELSE 'London Solvent' END, pl.layer, p.policynum, pd.pstartdate, pd.penddate, pl.aggregatelimit,
CASE When provisionvalueID=2 then 'H' when provisionvalueID=3 then 'G' when provisionvalueID=4 then 'I' end

UNION

--Most Carriers
select '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, Case when p.CarrierID in (152) then 'Employers Insurance of Wausau, A Mutual Company' else c.carriername end as CarrierName, 
pl.layer, p.policynum, pd.pstartdate, pd.penddate, pl.aggregatelimit,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	coalesce(CurrentIndemnity,0) as Indemnity, 
	coalesce(CurrentDefense,0) as Defense, 
	coalesce(PreviousIndemnity,0) + coalesce(PreviousDefense,0) + coalesce(PreviousOutsideLimits,0) + coalesce(PreviousCombinedShortfall,0) AS PreviousExhaustion,
	CASE WHEN @AllocationID IN (8,9,10) AND @Matter = 1 THEN coalesce(CurrentNosrocShortfall,0) 
		WHEN @AllocationID IN (8,9,10) AND @Matter = 2 THEN coalesce(CurrentIUNAShortfall,0)
		WHEN @AllocationID IN (3,4,5,24) THEN (coalesce(CurrentNosrocShortfall,0) + coalesce(CurrentIUNAShortfall,0) + coalesce(CurrentOutsideConsumptionOtherInsPro,0) 
			+ coalesce(CurrentOutsideConsumptionAetna,0) + coalesce(CurrentOutsideConsumptionOther,0)) END AS OtherConsumption,		
	coalesce(CurrentDefenseOutsideLimits,0) as DefenseOutsideLimits
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	Left Join @CurrentExhaustionTotals E on PD.PolicyDateID=E.PolicyDateID
	Left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	Left Join @Shortfall S on pd.PolicyDateID=s.PolicyDateID
	Left Join @PreviousShortfall sf on pd.PolicyDateID=sf.PolicyDateID
	Left Join @OutsideConsumptionOther O on pd.PolicyDateID=O.PolicyDateID
	Left Join @OutsideConsumptionOtherInsPro OO on pd.PolicyDateID=OO.PolicyDateID
	Left Join @OutsideConsumptionAetna A on pd.PolicyDateID=A.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where   limittypeid = @LimitTypeID and provisionlabelID=1 and PolicyDateTypeID=@PolicyDateTypeID 
		and (InsuranceProgramID=14 or InsuranceProgramID = @InsuranceProgramID)and c.carrierid not in (10,20,20001,20002,20003) and c.CarrierGroupID =@CarrierGroupID

UNION

--IUNA/Nosroc Producer
--select '2' as OrderID, 'Producer', p.policyiD, pd.policydateid, c.carriername, null as layer, 'Producer' as policynum, null as pstartdate, null as penddate,
--	null as aggregatelimit, '' as DefenseForm,
--	coalesce(CurrentIndemnity,0) as Indemnity, 
--	coalesce(CurrentDefense,0) as Defense, 
--	coalesce(PreviousIndemnity,0) + coalesce(PreviousDefense,0) + coalesce(PreviousOutsideLimits,0) + coalesce(PreviousCombinedShortfall,0) AS PreviousExhaustion,
--	CASE WHEN @AllocationID IN (8,9,10) AND @Matter = 1 THEN coalesce(CurrentNosrocShortfall,0) 
--		WHEN @AllocationID IN (8,9,10) AND @Matter = 2 THEN coalesce(CurrentIUNAShortfall,0)
--		WHEN @AllocationID IN (3,4,5,24) THEN (coalesce(CurrentNosrocShortfall,0) + coalesce(CurrentIUNAShortfall,0) + coalesce(CurrentOutsideConsumptionOtherInsPro,0) 
--			+ coalesce(CurrentOutsideConsumptionAetna,0) + coalesce(CurrentOutsideConsumptionOther,0)) END AS OtherConsumption,		
--	coalesce(CurrentDefenseOutsideLimits,0) as DefenseOutsideLimits
--from tblpolicy p 
--	left join tblpolicydates pd on p.policyid = pd.policyid
--	Left Join @CurrentExhaustionTotals E on PD.PolicyDateID=E.PolicyDateID
--	Left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
--	Left Join @Shortfall S on pd.PolicyDateID=s.PolicyDateID
--	Left Join @PreviousShortfall sf on pd.PolicyDateID=sf.PolicyDateID
--	Left Join @OutsideConsumptionOther O on pd.PolicyDateID=O.PolicyDateID
--	Left Join @OutsideConsumptionOtherInsPro OO on pd.PolicyDateID=OO.PolicyDateID
--	Left Join @OutsideConsumptionAetna A on pd.PolicyDateID=A.PolicyDateID
--	inner join tblcarrier c on c.carrierid = p.carrierid
--Where PolicyDateTypeID=@PolicyDateTypeID
--and (InsuranceProgramID=14 or InsuranceProgramID=@InsuranceProgramID)and ((@InsuranceProgramID = 15 AND c.carrierid = 20002) OR (@InsuranceProgramID = 16 AND c.carrierid = 20003))
--and c.CarrierID NOT IN (10,20,20001)

--UNION

--ACF Refund
select '2' as OrderID, 'ACF Refund', p.policyid, pd.policydateid, c.carriername, pl.layer, 'ACF Refund' as policynum, Null as pstartdate, Null as penddate,
	null as aggregatelimit,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	coalesce(CurrentIndemnity,0) as Indemnity, 
	coalesce(CurrentDefense,0) as Defense, 
	coalesce(PreviousIndemnity,0) + coalesce(PreviousDefense,0) + coalesce(PreviousOutsideLimits,0) + coalesce(PreviousCombinedShortfall,0) AS PreviousExhaustion,
	CASE WHEN @AllocationID IN (8,9,10) AND @Matter = 1 THEN coalesce(CurrentNosrocShortfall,0) 
		WHEN @AllocationID IN (8,9,10) AND @Matter = 2 THEN coalesce(CurrentIUNAShortfall,0)
		WHEN @AllocationID IN (3,4,5,24) THEN (coalesce(CurrentNosrocShortfall,0) + coalesce(CurrentIUNAShortfall,0) + coalesce(CurrentOutsideConsumptionOtherInsPro,0) 
			+ coalesce(CurrentOutsideConsumptionAetna,0) + coalesce(CurrentOutsideConsumptionOther,0)) END AS OtherConsumption,		
	coalesce(CurrentDefenseOutsideLimits,0) as DefenseOutsideLimits
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	Left Join @CurrentExhaustionTotals E on PD.PolicyDateID=E.PolicyDateID
	Left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	Left Join @Shortfall S on pd.PolicyDateID=s.PolicyDateID
	Left Join @PreviousShortfall sf on pd.PolicyDateID=sf.PolicyDateID
	Left Join @OutsideConsumptionOther O on pd.PolicyDateID=O.PolicyDateID
	Left Join @OutsideConsumptionOtherInsPro OO on pd.PolicyDateID=OO.PolicyDateID
	Left Join @OutsideConsumptionAetna A on pd.PolicyDateID=A.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where  provisionlabelID=1 and PolicyDateTypeID=@PolicyDateTypeID 
and (InsuranceProgramID=14 or InsuranceProgramID=@InsuranceProgramID)and c.carrierid in (20001)
and c.CarrierID NOT IN (10,20)


Select R.*, ExhaustionType, [Description], 
CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
From @Results R
Inner Join @HeaderSummary HS on R.DefenseForm=HS.DefenseForm
WHERE Indemnity+Defense+OutsideConsumption+DefenseOutsideLimits <> 0
and policydateid in (Select policydateid from @policyDateID)
Order by Matter, OrderID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GO
