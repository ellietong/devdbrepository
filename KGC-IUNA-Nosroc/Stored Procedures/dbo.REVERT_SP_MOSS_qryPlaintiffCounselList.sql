SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryPlaintiffCounselList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
    -- Insert statements for procedure here
	SELECT PlaintiffCounselID, PlaintiffCounsel
	From tblPlaintiffCounselAddress b 
	WHERE PlaintiffCounsel IS NOT NULL AND PlaintiffCounsel <> ''
	GROUP BY PlaintiffCounselID, PlaintiffCounsel
	UNION
	SELECT NULL, NULL
	ORDER BY PlaintiffCounsel


    -- Insert statements for procedure here
	--SELECT PlaintiffCounselAddressID, PlaintiffCounsel + ' (' + case when PlaintiffCounselCity IS Null then 'UNKNOWN' else PlaintiffCounselCity end + ')' AS PlaintiffCounsel 
	--From tblPlaintiffCounselAddress b 
	--WHERE PlaintiffCounsel IS NOT NULL AND PlaintiffCounsel <> ''
	--UNION
	--SELECT NULL, NULL
	--ORDER BY PlaintiffCounsel + ' (' + case when PlaintiffCounselCity IS Null then 'UNKNOWN' else PlaintiffCounselCity end + ')', PlaintiffCounselAddressID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryPlaintiffCounselList] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryPlaintiffCounselList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryPlaintiffCounselList] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryPlaintiffCounselList] TO [power_user]
GO
