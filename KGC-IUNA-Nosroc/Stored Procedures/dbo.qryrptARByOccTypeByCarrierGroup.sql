SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByOccTypeByCarrierGroup]
(@allocationID int = null, @CarrierID INT = NULL, @Solvency varchar (50)= NULL, @CarrierGroupID int = NULL, @InsuranceProgramID INT = NULL)
As
SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 83
--DECLARE @CarrierID int
--SET @CarrierID = 0
--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = 0
--DECLARE @Solvency varchar (50)
--SET @Solvency = null
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID = 0
--DECLARE @OccurrenceTypeIDs nvarchar(100)
--SET @OccurrenceTypeIDs = null

--DECLARE Variables
DECLARE @maxAllocationID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @maxInsuranceProgramID int
--SET Variables
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxInsuranceProgramID = MAX (InsuranceProgramID) FROM vPolicy
IF @CarrierGroupID = 0 SET @CarrierGroupID = NULL


DECLARE @Policy TABLE (PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
CarrierID int, CarrierName nvarchar(250), CarrierSolvency nvarchar(25), PolicyNum nvarchar(100), 
InsuranceProgramID int, InsuranceProgram nvarchar(50))
INSERT INTO @Policy

SELECT PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, CarrierSolvency,
PolicyNum, InsuranceProgramID, InsuranceProgram
FROM vPolicy p
GROUP BY PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, CarrierSolvency,
PolicyNum, InsuranceProgramID, InsuranceProgram


SELECT pinfo.InsuranceProgram, o.OccurrenceType, o.OccurrenceTypeID, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' ('+ pinfo.CarrierGroupName + ')' END AS 'CarrierInfo', pinfo.CarrierID, pinfo.CarrierGroupID, pinfo.CarrierName, 
	pinfo.CarrierGroupName, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END AS 'CarrierGroupInfo', a.AllocationID, a.AllocationDescription, 
	MAX(ar.DateTime) AS MaxOfDateTime, SUM(ar.Allocation) AS SumOfAllocation, CONVERT(money, SUM(ar.PV)) AS SumOfPV, SUM(ar.SIRAllocated) AS SumOfSIRAllocated, 
	ar.DateToPresentValue, YieldCurveDate as PVRateDate
FROM vAllocationResults ar
	INNER JOIN @Policy pinfo ON pinfo.PolicyDateID = ar.PolicyDateID
	INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID 
	INNER JOIN vOccurrence o ON ar.OccurrenceID = o.OccurrenceID
	LEFT JOIN tblYieldCurve yc on yc.YieldCurveID = ar.YieldCurveID 
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) AND
	pinfo.CarrierID >= COALESCE(@CarrierID, 0) AND pinfo.CarrierID <= COALESCE(NULLIF(@CarrierID, 0), @maxCarrierID) AND
	pinfo.CarrierSolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%') AND 
	(coalesce(pinfo.CarrierGroupID,0) >= COALESCE(@CarrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID,0) <= COALESCE(@CarrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID,0) = @CarrierGroupID) AND
	pinfo.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) 
GROUP BY pinfo.InsuranceProgram, o.OccurrenceType, o.OccurrenceTypeID, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' ('+ pinfo.CarrierGroupName + ')' END, pinfo.CarrierID, pinfo.CarrierGroupID, pinfo.CarrierName, 
	pinfo.CarrierGroupName, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END, a.AllocationID, a.AllocationDescription, 
	ar.DateToPresentValue, YieldCurveDate 
	ORDER BY InsuranceProgram

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccTypeByCarrierGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccTypeByCarrierGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccTypeByCarrierGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccTypeByCarrierGroup] TO [power_user]
GO
