SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_NosrocDocuments]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--NOSROC
select cpi.ClaimantPersonalInfoID, NavInjPartyID, LastName, FirstName,DocumentURL, DocumentTitle, DocumentID
from tbldocuments d
left join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
left join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID=d.ClaimantPersonalInfoID
left join tblClaim c on c.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
where NavInjPartyID in (5003184878,
5004198546,5002098804,3010084538,5003170110,8706906459,3010392750,
2001428874,5001020622,3010064884,5002090495,5003185363,2000269127,
5002094247,5003175946,3010308948,5002028821,2000269172,5002028875,
2000268924,2000269137,2001427494,2000293904,2001428362,2001428452,
2000269022,5002029131,5002028973,5002029073,3010288895,5004208133
,9807165976,2000269052,2001427326) and c.matterid=2
order by NavInjPartyID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_NosrocDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_NosrocDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_NosrocDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_NosrocDocuments] TO [power_user]
GO
