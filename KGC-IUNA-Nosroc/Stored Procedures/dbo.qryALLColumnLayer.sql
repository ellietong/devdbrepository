SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create PROCEDURE [dbo].[qryALLColumnLayer]
(@strColumns nvarchar(4000), @allocationid nvarchar(5), @limittypeID nvarchar(5))
AS
/*  DECLARE @AllocationID nvarchar(5)
DECLARE @strColumns nvarchar(4000)
DECLARE @limittypeID nvarchar(5)


 SET @allocationId = '219'
 SET @strColumns = 'ColumnID = 20185 OR ColumnID =  20192 OR ColumnID =  20193 OR ColumnID =  20194 OR ColumnID =  20195 OR ColumnID =  20196 OR ColumnID =  20197 OR ColumnID =  20198 OR ColumnID =  20199 OR ColumnID =  20200 OR ColumnID =  20201 OR ColumnID =  20202 OR ColumnID =  20203 OR ColumnID =  20204 OR ColumnID =  20205 OR ColumnID =  20206 OR ColumnID =  20207 OR ColumnID =  20208 OR ColumnID =  20209 OR ColumnID =  20210 OR ColumnID =  20211 OR ColumnID =  20212 OR ColumnID =  20213 OR ColumnID =  20214 OR ColumnID =  20215 OR ColumnID =  20216 OR ColumnID =  20217 OR ColumnID =  20218 OR ColumnID =  20219 OR ColumnID =  20220 OR ColumnID =  20221 OR ColumnID =  20222 OR ColumnID =  20225 OR ColumnID =  20227 OR ColumnID =  20228 OR ColumnID =  20229 OR ColumnID =  20230'
 SET @limittypeID = '4'

 */

	
DECLARE @strsql nvarchar(4000)

SET @strsql = ' SET NOCOUNT ON
DELETE FROM ztblColumnLayer
INSERT INTO ztblColumnLayer (SigLevelColumnID, Layer)
SELECT ColumnID, dbo.fnMinSigPolicyPerColumn(ColumnID, ' + @allocationid + ',' + 
@limittypeID +') As Layer
FROM ztblSigLevel
WHERE ' + @strColumns

--PRINT @strsql
EXECUTE sp_executesql @strSQL

RETURN 





GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLColumnLayer] TO [Allocation_User]
GRANT EXECUTE ON  [dbo].[qryALLColumnLayer] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLColumnLayer] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLColumnLayer] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLColumnLayer] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLColumnLayer] TO [power_user]
GO
