SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryUpdClaimantPersonalInfo]
(@ClaimantPersonalInfoID bigint, @Address1 nvarchar(250), @Address2 nvarchar(250), @StateID int, @City nvarchar(250), @Zip nvarchar(5), @Mailzip nvarchar(4), 
@BirthDate datetime, @DeceasedDate datetime, @GenderID int, @MaritalStatusID int,
@Username nvarchar(250), @FullSSN nvarchar(150), @HICN nvarchar(50))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE tblClaimantPersonalInfo
    SET Address1 = UPPER(@Address1),
		Address2 = UPPER(@Address2),
		StateID = @StateID,
		City = UPPER(@City),
		Zip = @Zip,
		MailZip = @Mailzip,
		BirthDate = @BirthDate,
		DeceasedDate = @DeceasedDate,
		GenderID = @GenderID,
		MaritalStatusID = @MaritalStatusID,
		SPUsername = @Username,
		SocialSecurityNumber = REPLACE(REPLACE(@FullSSN, '-', ''), ' ', ''),
		HealthInsuranceClaimNumber = @HICN
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdClaimantPersonalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdClaimantPersonalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdClaimantPersonalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdClaimantPersonalInfo] TO [power_user]
GO
