SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[qryfrmCorrespondence]
AS
SET NOCOUNT ON

SELECT *
FROM tblCorrespondence
	
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorrespondence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorrespondence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorrespondence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorrespondence] TO [power_user]
GO
