SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimLawsuitInfoQC_Old] (@ClaimantPersonalInfoID int)
as

Select --Claim Info
	ClaimantPersonalInfoID,
	IsCurrentClaim,
	CLaimSequence,
	Matter,
	--ClaimLawsuit Info
	c.ClaimID, 
	cl.ClaimLawsuitID,
	cl.LawsuitID,
	IndividualDocketNumber,
	IsPrimaryForClaim,
	ClaimLawsuitStatus,
	StatusDate,
	StatusNotes,
	p.Disease as PlaintiffDisease,
	PlaintiffDiseaseDiagnosisDate,
	d.Disease as DefenseDisease,
	DefenseDiseaseDiagnosisDate,
CASE WHEN do.DoctorMiddleName is NULL THEN do.DoctorLastName + ', ' + do.DoctorFirstName ELSE do.DoctorLastName + ', ' + do.DoctorFirstName+ ' ' + do.DoctorMiddleName END as DoctorName

From tblClaim c Left Join tblClaimLawsuit cl on c.claimID = cl.claimID
	Left Join tblClaimSequence cs on cs.claimsequenceID = c.claimsequenceID
	Left Join tblClaimLawsuitStatus cls on cls.claimlawsuitstatusid = cl.claimlawsuitstatusID
	Left Join tblDisease p on p.DiseaseID = c.plaintiffdiseaseID
	Left Join tblDisease d on d.DiseaseID = c.defensediseaseID
	Left Join tblDoctor do on do.DoctorID = c.DoctorID
	Left Join tblMatter m on c.MatterID = m.MatterID
Where c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By IsPrimaryForClaim

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitInfoQC_Old] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitInfoQC_Old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitInfoQC_Old] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitInfoQC_Old] TO [power_user]
GO
