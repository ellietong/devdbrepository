SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qrysrptCheckDetailSettlement] (@CheckID int)

AS
Set NoCount On


Declare @SGTotals table (SettlementGroupID int, CountOfTotalSettlements int)
Insert Into @SGTotals
Select SettlementGroupID, COUNT(*)
From tblClaimantSettlement cs
Group By SettlementGroupID

Select CheckID, cs.SettlementGroupID as SettlementGroup, COUNT(*) as CountOfSettlements, CountOfTotalSettlements, SUM(SettlementAmount) as SettlementAmount, Sum(PaymentAmount) as PaymentAmount
from tblPaymentSettlement ps
Inner Join tblClaimantSettlement cs on cs.ClaimantSettlementID = ps.ClaimantSettlementID
Inner Join tblSettlementGroup sg on sg.SettlementGroupID = cs.SettlementGroupID
Inner Join @SGTotals sgt on sgt.SettlementGroupID=cs.SettlementGroupID
Where CheckID = @CheckID
Group By CheckID, cs.SettlementGroupID, CountOfTotalSettlements


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [power_user]
GO
