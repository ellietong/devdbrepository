SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[REVERT_SP_MOSS_qryClaimantPersonalInfo]
@ClaimantPersonalInfoID bigint=null
AS
BEGIN

--DECLARE @ClaimantpersonalinfoID bigint
--SET @ClaimantPersonalInfoID = null
--DECLARE @Emailaddress nvarchar(250)
--SET @emailaddress = 'enewfiles@heylroyster.com'

--Declare @UserFirmState table (DefenseCounselID bigint, StateID int)
--Insert into @UserFirmState

--Select dca.DefenseCounselID, StateID
--From dbo.tblDefenseCounselState AS dcs 
--left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
--LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid --and dcar.stateID = dcs.StateID 
--LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
--left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
--Where (ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) OR
--dca.DefenseCounselID in (SELECT f.FirmID FROM vtblPerson p inner join vtblFirm f on p.FirmID=f.FirmID WHERE Email = @emailaddress))


--IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress) IN (132, 133) OR ((Select PersonID FROM vtblPerson WHERE Email = @emailaddress) IN (1376,1377,1378,1383,1384,1389,1392))
--BEGIN

				SELECT 
                      CPI.ClaimantPersonalInfoID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, Gender, COALESCE(cpi.GenderID, 4) AS GenderID, COALESCE(cpi.MaritalStatusID, 1) AS MaritalStatusID,
                      MaritalStatus, coalesce(nullif(coalesce(Address1 + CHAR(13) + CHAR(10), '') + coalesce(Address2 + CHAR(13) + CHAR(10),'') + 
						coalesce(City + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,Zip),'') + Coalesce('-'+convert(varchar,mailzip),''),''),'') as CompleteAddress, BirthDate, DeceasedDate, City,
					  s.StateID, Zip, MailZip, Address1, Address2, IsSmoker, SmokerStart, SmokerEnd, PacksPerDay, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS FullSSN,
					  HealthInsuranceClaimNumber AS HICN, 'XXXXXX' + RIGHT(RTRIM(HealthInsuranceClaimNumber), 4) AS MaskedHICN
                     
                        
					FROM         dbo.tblClaimantPersonalInfo AS CPI 
					 LEFT JOIN tblGender as G on G.GenderID = CPI.GenderID
                     LEFT JOIN tblMaritalStatus as MS on MS.MaritalStatusID=CPI.MaritalStatusID
                     LEFT JOIN tblState s ON cpi.StateID = s.StateID
				Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID

--END
--ELSE
--BEGIN

--				SELECT DISTINCT
--                      CPI.ClaimantPersonalInfoID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
--                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
--                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, Gender, COALESCE(cpi.GenderID, 4) AS GenderID, COALESCE(cpi.MaritalStatusID, 1) AS MaritalStatusID,
--                      MaritalStatus, coalesce(nullif(coalesce(Address1 + CHAR(13) + CHAR(10), '') + coalesce(Address2 + CHAR(13) + CHAR(10),'') + 
--						coalesce(City + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,Zip),'') + Coalesce('-'+convert(varchar,mailzip),''),''),'') as CompleteAddress, BirthDate, DeceasedDate, City,
--					  s.StateID, Zip, MailZip, Address1, Address2, IsSmoker, SmokerStart, SmokerEnd, PacksPerDay, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS FullSSN
                     
                        
--					FROM         dbo.tblClaimantPersonalInfo AS CPI 
--					 LEFT JOIN tblGender as G on G.GenderID = CPI.GenderID
--                     LEFT JOIN tblMaritalStatus as MS on MS.MaritalStatusID=CPI.MaritalStatusID
--                     LEFT JOIN tblState s ON cpi.StateID = s.StateID
--                     LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
--					LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
--					LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
--					INNER JOIN tblDefenseCounselState dcs on l.DefenseCounselAddressID=dcs.DefenseCounselAddressID and l.StateID=dcs.StateID
--					INNER JOIN tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCounselAddressID=dca.DefenseCounselAddressID
--					INNER JOIN @UserFirmState u on dca.DefenseCounselID = u.DefenseCounselID and l.StateID = u.StateID
	
--				Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
--				--AND DefenseCounselAddressID in (SELECT   DISTINCT  dcs.DefenseCounseladdressID
--    --                        FROM          dbo.tblDefenseCounselState AS dcs INNER JOIN
--				--							dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
--				--							LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
--				--							LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID INNER JOIN
--    --                                               dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
--    --                        WHERE ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress))
--END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantPersonalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantPersonalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantPersonalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantPersonalInfo] TO [power_user]
GO
