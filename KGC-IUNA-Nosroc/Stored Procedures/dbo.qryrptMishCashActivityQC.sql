SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[qryrptMishCashActivityQC] (@MiscCashActivityID int)
As
Set NoCount On

Select CashActivityAmount, CashActivityDate, CashActivityNotes, CashActivityType, ApprovedDate, ApprovedBy, Matter
From tblMiscCashActivity mca
Left Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
Left Join tblMatter m on m.MatterID = mca.MatterID
Left Join tblMiscCashActivityApproval mcaa on mcaa.MiscCashActivityID = mca.MiscCashActivityID
Where mca.MiscCashActivityID = @MiscCashActivityID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptMishCashActivityQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptMishCashActivityQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptMishCashActivityQC] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryrptMishCashActivityQC] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptMishCashActivityQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptMishCashActivityQC] TO [power_user]
GO
