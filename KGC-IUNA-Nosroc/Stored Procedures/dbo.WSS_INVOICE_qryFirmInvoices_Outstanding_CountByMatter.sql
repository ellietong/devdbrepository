SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCedure [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding_CountByMatter] (@emailaddress nvarchar(max))
AS
--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'j.heenan@envirosafeservices.com'
DECLARE @firmid int
SET @firmid = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

IF ((@emailaddress = 'cjs@wlbdeflaw.com') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com')) 
BEGIN
	SELECT COALESCE(m.Matter, '') + ' (' + CONVERT(nvarchar(10), COUNT(COALESCE(d.DocumentID, 0))) + ')' AS Matter, m.MatterID
	FROM tblDocuments d
	LEFT OUTER JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
	LEFT OUTER JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
	LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 	
	LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
	LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
	LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	WHERE dt.DocumentGroupID = 2 AND d.StatusID IS NULL 
	GROUP BY m.Matter, m.MatterID
	ORDER BY m.Matter
END
ELSE IF (@emailaddress = 'j.heenan@envirosafeservices.com')
BEGIN
	SELECT COALESCE(m.Matter, '') + ' (' + CONVERT(nvarchar(10), COUNT(COALESCE(d.DocumentID, 0))) + ')' AS Matter, m.MatterID
	FROM tblDocuments d
	LEFT OUTER JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
	LEFT OUTER JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
	LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 	
	LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
	LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
	LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	WHERE dt.DocumentGroupID = 2 AND d.StatusID IS NULL AND d.FirmID = 38
	GROUP BY m.Matter, m.MatterID
	ORDER BY m.Matter
END
ELSE
BEGIN
	SELECT COALESCE(m.Matter, '') + ' (' + CONVERT(nvarchar(10), COUNT(COALESCE(d.DocumentID, 0))) + ')' AS Matter, m.MatterID
	FROM tblDocuments d
	LEFT OUTER JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
	LEFT OUTER JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
	LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 	
	LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
	LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
	LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	WHERE dt.DocumentGroupID = 2 AND d.StatusID IS NULL AND d.FirmID = @firmID
	GROUP BY m.Matter, m.MatterID
	ORDER BY m.Matter
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding_CountByMatter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding_CountByMatter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding_CountByMatter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding_CountByMatter] TO [power_user]
GO
