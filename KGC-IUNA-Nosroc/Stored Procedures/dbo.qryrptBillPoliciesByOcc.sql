SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  Procedure [dbo].[qryrptBillPoliciesByOcc]
(@LimitTypeID int = null, @billGroupID int = NULL, @BillDate Datetime = null)
As

SET NOCOUNT ON

--DECLARE @BillGroupID int
--SET @BillGroupID = 2
--DECLARE @LimitTypeID INT
--SET @LimitTypeID = 2
--DECLARE @BillDate datetime
--SET @BillDate = '2/23/2010'

DECLARE @maxLimitTypeID int
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
DECLARE @maxBillGroupID int
SELECT @maxBillGroupID = MAX(Billgroupid) FROM vBilling

DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierName nvarchar(255), PolicyNum nvarchar(100), 
PStartDate datetime, PEndDate datetime, AnnualPeriod int, Layer int, SIR money, AttachmentPoint money, 
PerOccLimit money, AggregateLimit money, LayerPerOccLimit money)
INSERT INTO @Policy

SELECT PolicyID, PolicyDateID, CarrierName, PolicyNum, PStartDate, PEndDate, AnnualPeriod, Layer, SIR, 
AttachmentPoint, PerOccLimit, AggregateLimit, LayerPerOccLimit
FROM vPolicy p
WHERE p.LimitTypeID >= COALESCE(@LimitTypeID, 0) AND p.LimitTypeID <= COALESCE(NULLIF(@LimitTypeID, 0), @maxLimitTypeID )
GROUP BY PolicyID, PolicyDateID, CarrierName, PolicyNum, PStartDate, PEndDate, AnnualPeriod, Layer, SIR, 
AttachmentPoint, PerOccLimit, AggregateLimit, LayerPerOccLimit
	

SELECT BillDate, a.AllocationID, BillGroupID, o.ExpectedPayDate, --bil.NewBalance, 
	bil.PaymentDueDate, pinfo.PolicyID, CarrierName, PolicyNum, PStartDate, PEndDate, AnnualPeriod, Layer, SIR, AttachmentPoint, 
	PerOccLimit, AggregateLimit, LayerPerOccLimit, OccurrenceName, 
	(CASE WHEN a.TriggerStartID <> 0 THEN 
		CASE
			WHEN TriggerStartID = 1 THEN TriggerStart1
			WHEN TriggerStartID = 2 THEN TriggerStart2
			WHEN TriggerStartID = 3 THEN TriggerStart3
			WHEN TriggerStartID = 4 THEN TriggerStart4
		END
	ELSE a.TriggerStartDate END) AS TriggerStart, 
	(CASE WHEN a.TriggerStartID <> 0 THEN
		CASE
			WHEN TriggerEndID = 1 THEN TriggerEnd1
			WHEN TriggerEndID = 2 THEN TriggerEnd2
			WHEN TriggerEndID = 3 THEN TriggerEnd3
			WHEN TriggerEndID = 4 THEN TriggerEnd4
		END
	ELSE a.TriggerEndDate END) AS TriggerEnd, dbo.fnCostsConsidered(e.AllocationID, e.OccurrenceID) AS IndemnityOcc, IndemnityAllocation AS Indemnity, DefenseAllocation AS Defense, 
	dbo.fnDefenseConsidered(e.AllocationID, e.OccurrenceID) AS DefenseOcc, DefenseOutsideLimitsAllocation AS DefenseOutside, SIRIndemnityAllocation AS SIRIndemnity, 
	SIRDefenseAllocation AS SIRDefense
FROM vBilling bil
	INNER JOIN vExhaustion e ON bil.ExhaustionDateID = e.ExhaustionDateID
	INNER JOIN vAllocation a ON a.AllocationID = e.AllocationID 
	INNER JOIN vOccurrence o ON e.OccurrenceID = o.OccurrenceID
	INNER JOIN @Policy pinfo ON e.PolicyDateID = pinfo.PolicyDateID
where BillGroupID >= COALESCE(@BillGroupID, 0) AND BillGroupID <= COALESCE(NULLIF(@BillGroupID, 0), @maxBillGroupID )
and BillDate like coalesce(@BillDate, '%')

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillPoliciesByOcc] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBillPoliciesByOcc] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillPoliciesByOcc] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBillPoliciesByOcc] TO [power_user]
GO
