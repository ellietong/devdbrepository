SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create proc [dbo].[qryrptExternalContactsCarrier]
(@carriergroupid int = null)
as
set nocount on
select CarrierName, CarrierGroupID , carrierid



from tblCarrier where CarrierID in (4,6,9,74,120,121,97,47,460,433) 
and carriergroupid = @carriergroupid
order by CarrierName


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExternalContactsCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExternalContactsCarrier] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExternalContactsCarrier] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptExternalContactsCarrier] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExternalContactsCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExternalContactsCarrier] TO [power_user]
GO
