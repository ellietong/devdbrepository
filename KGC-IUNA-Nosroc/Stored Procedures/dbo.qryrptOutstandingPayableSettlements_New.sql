SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Input Parameters
--@MatterID=forms!frmMasterReports!MatterID


CREATE Proc [dbo].[qryrptOutstandingPayableSettlements_New]  (@MatterID int, @settlementdate datetime)

As

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 1
--DECLARE @SettlementDate datetime
--SET @SettlementDate = '1/1/2009'

DECLARE @MaxMatterID int
SELECT @MaxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @SettlementGroupPaidUnpaid TABLE (SettlementGroupID int, Paid money, Unpaid money, Total money)
INSERT INTO @SettlementGroupPaidUnpaid
	SELECT SettlementGroupID, 
	SUM( CASE WHEN PaidDate IS NULL THEN 0 ELSE SettlementAmount END) As Paid,
	SUM( CASE WHEN PaidDate IS NULL THEN SettlementAmount ELSE 0 END) as Unpaid, 
	SUM(SettlementAmount) as Total
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupCount TABLE (SettlementGroupID int, ClaimCount nvarchar(50))
INSERT INTO @SettlementGroupCount	
	SELECT SettlementGroupID, COUNT(SettlementDate) as ClaimCount
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupPaidCount TABLE (SettlementGroupID int, PaidCount nvarchar(50))
INSERT INTO @SettlementGroupPaidCount	
	SELECT SettlementGroupID, COUNT(paiddate) as PaidCount
	FROM tblClaimantSettlement
	WHERE SettlementGroupID IN	(SELECT SettlementGroupID
								 FROM tblClaimantSettlement
								 WHERE paiddate IS NOT NULL 
								 GROUP BY SettlementGroupID)
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupMaxDate TABLE (SettlementGroupID int, MaxSettlementDate datetime)
INSERT INTO @SettlementGroupMaxDate
	SELECT SettlementGroupID, MAX(SettlementDate) as MaxSettlementDate
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY MaxSettlementDate


SELECT pca.PlaintiffCounsel , cs.SettlementDate, SettlementDueDate as DatePayable, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, CheckNumber, spd.ReleaseSignedDate, FullyDocumented, c.MatterID, Matter, cl.LawsuitID, PaidDate,
	@SettlementDate AS SettlementDateAsOf, sgp.Paid, sgp.Unpaid, DocketNumbers, COALESCE(sgpc.PaidCount,0) AS PaidCount, sgc.ClaimCount, MaxSettlementDate, c.claimid,
	CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
		 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END AS SupportingDocumentation,
	CASE WHEN spd.ReleaseRcvd = 1
		 THEN coalesce( crr.ReleaseType,'Executed' )
		 ELSE 'Release Missing'  END AS ReleaseExecution, convert(datetime,GETDATE() - 7) as WeeklyStatusDate, Jurisdiction, st.State
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m						 ON c.MatterID = m.MatterID		
	INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs			 ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblLawsuit l						 oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaimLawsuitStatus cls		 ON cl.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID	
	LEFT  JOIN tblSettlementGroup s				 ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblClaimantDocketNumbers cd		 ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT  JOIN @SettlementGroupPaidUnpaid sgp	 ON s.SettlementGroupID = sgp.SettlementGroupID
	LEFT  JOIN @SettlementGroupCount sgc		 ON s.SettlementGroupID= sgc.SettlementGroupID
	LEFT  JOIN @SettlementGroupPaidCount sgpc	 ON s.SettlementGroupID = sgpc.SettlementGroupID
	LEFT  JOIN @SettlementGroupMaxDate sgm		 ON s.SettlementGroupID = sgm.SettlementGroupID
	LEFT  JOIN tblReleasetype crr				 ON crr.ReleaseTypeID =spd.ReleaseTypeID
	LEFT  JOIN tblJurisdiction j				 ON l.JurisdictionID = j.JurisdictionID
	LEFT  JOIN tblPlaintiffCounselAddress pca	 ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	LEFT  JOIN tblState st 					     ON st.StateID = l.StateID
	LEFT  JOIN tblClaimSequence csq				 ON c.ClaimSequenceID = csq.ClaimSequenceID
WHERE COALESCE(IsCurrentClaim,0) = 1
	AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID) and PaidDate is null
	AND COALESCE(PaidCount,0) <> COALESCE(ClaimCount,0)  and SettlementDate >=@SettlementDate --and SettlementDueDate is not null
	AND csq.ClaimSequenceID not in (4,2,3,11)
	AND COALESCE(cpi.jobsiteinfo, 0) = 0
GROUP BY cs.SettlementDate, SettlementDueDate, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, CheckNumber, spd.ReleaseSignedDate, FullyDocumented, c.MatterID, Matter, cl.LawsuitID,paiddate,l.PrimaryLocalPlaintiffCounsel ,
	sgp.Paid, sgp.Unpaid, DocketNumbers, sgpc.PaidCount, sgc.ClaimCount, MaxSettlementDate,spd.ReleaseRcvd, GETDATE() - SettlementDueDate, c.ClaimID,

CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
			 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END,
	CASE WHEN spd.ReleaseRcvd = 1
		 THEN coalesce( crr.ReleaseType,'Executed' ) 
		 ELSE 'Release Missing'  END, Jurisdiction, PlaintiffCounsel, st.State
ORDER BY pca.PlaintiffCounsel, MaxSettlementDate, SettlementGroup, SettlementDate, LastName

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingPayableSettlements_New] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingPayableSettlements_New] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingPayableSettlements_New] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingPayableSettlements_New] TO [power_user]
GO
