SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptByPolicyFirstTriggeredOccurrence] (@allocationID int)
AS
Set NoCount On

--Declare @AllocationID int
--Set @AllocationID = 234

Declare @NonSigID int
Set @NonSigID = (Select NonSigAllocationID From tblAllocation Where AllocationID = @AllocationID)
Declare @LimitTypeID int
Set @LimitTypeID = (Select LimittypeID from tblAllocation where AllocationID = @AllocationID)

Declare @one table (RankID int, OccurrenceID int, PolicyDateID int, ExpectedPayDate datetime)
Insert Into @one
Select RANK () OVER (partition by policydateID order by allocationresultID), ar.OccurrenceID, PolicyDateID, ExpectedPayDate
From tblAllocationResults ar
Inner Join tblOccurrence o on ar.OccurrenceID = o.OccurrenceID
Where AllocationID = @NonSigID

Declare @NonSig table (OccurrenceID int, PolicyDateID int, ExpectedPayDate datetime)
Insert Into @NonSig
Select OccurrenceID, PolicyDateID, ExpectedPayDate
From @one
Where RankID = 1

Declare @two table (RankID int, OriginalOccurrenceID int, PolicyDateID int, ExpectedPayDate datetime)
Insert Into @two
Select RANK () OVER (partition by policydateID order by allocationresultID), OriginalOccurrenceID, PolicyDateID, ExpectedPayDate
From tblAllocationResults ar
Inner Join tblOccurrence o on ar.OccurrenceID = o.OccurrenceID
Where AllocationID = @AllocationID

Declare @SigRun table (OriginalOccurrenceID int, PolicyDateID int, ExpectedPayDate datetime)
Insert Into @SigRun
Select OriginalOccurrenceID, PolicyDateID, ExpectedPayDate
From @two
Where RankID = 1 

Declare @RankOfOccurrence table (AllocationresultID bigint, OccurrenceID bigint)
Insert Into @RankOfOccurrence
Select Min(AllocationResultID), OccurrenceID
From tblAllocationResults ar
Where AllocationID = @NonSigID
Group By OccurrenceID

Declare @NonSigOccurrenceRank table (RankID bigint, OccurrenceID bigint)
Insert Into @NonSigOccurrenceRank
Select RANK () Over (order by allocationresultID), OccurrenceID
From @RankOfOccurrence

/*  The Next section finds the lowest continuous sig policy date ID   */

Declare @Temp table (SigAttachment money, Attachmentpoint money, PolicyID int, ColumnID int)
Insert Into @Temp
select Coalesce(dbo.SigDropDown(columnid, za.PolicyID, @limittypeID, Layer, @allocationID), za.AttachmentPoint) as SigAttachment, za.AttachmentPoint, za.PolicyID, ColumnID
from ztblPolicyAllocation za
	inner join ztblpolicycolumn zc on zc.policydateid = za.policydateid
	inner join tblPolicyLimits pl on pl.PolicyID = za.PolicyID
Where AllocationID = @allocationID and LimitTypeID = @LimitTypeID

Declare @Temp1 table (SigAttachment money, Attachmentpoint money, PolicyID int, ColumnID int, RankID int, Rank2 int)
Insert Into @Temp1
Select SigAttachment, Attachmentpoint, PolicyID, ColumnID, Dense_RANK () Over (Order By SigAttachment, ColumnID) as RankID,
	RANK () Over (Partition By SigAttachment, ColumnID Order By AttachmentPoint) as Rank2
From @Temp

Declare @Final table (PolicyDateID int)
Insert Into @Final
Select PolicyDateID
From @Temp1 t
Inner Join tblPolicyDates pd on pd.PolicyID=t.PolicyID
Where Rank2 = 1
Group By PolicyDateID

/*  End Lowest Sig Policy Section  */

Select s.PolicyDateID, @allocationID as SigAllocationID, OriginalOccurrenceID as SigOriginalOccurrenceID, sor.RankID as SigRankID,
	s.ExpectedPayDate as SigExpectedPayDate, @NonSigID as NonSigAllocationID, ns.OccurrenceID as NonSigOccurrenceID, nsor.RankID as NonSigRankID,
	ns.ExpectedPayDate as NonSigExpectedPayDate,
	Case When sor.RankID = 1 and Coalesce(nsor.RankID,999999) = 1 Then 1  --Initially Trggered
	     When s.policyDateID in (Select PolicyDateID From @Final) and sor.RankID < Coalesce(nsor.RankID,0) Then 2 --Lowest Lying Sig Policy
		 When sor.RankID < Coalesce(nsor.RankID,999999) Then 3 --Triggered Continuous Sig Coverage
		 When sor.RankID = Coalesce(nsor.RankID,999999) Then 4 --Triggered On a Non Sig Run
		 When sor.RankID > Coalesce(nsor.RankID,999999) Then 5 --Triggered After Non Sig Run
		 End as Code
From @SigRun s
Left Join @NonSig ns on s.PolicyDateID=ns.PolicyDateID
Left Join @NonSigOccurrenceRank nsor on ns.OccurrenceID=nsor.OccurrenceID
Left Join @NonSigOccurrenceRank sor on s.OriginalOccurrenceID=sor.OccurrenceID
Order By sor.RankID

Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptByPolicyFirstTriggeredOccurrence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptByPolicyFirstTriggeredOccurrence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptByPolicyFirstTriggeredOccurrence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptByPolicyFirstTriggeredOccurrence] TO [power_user]
GO
