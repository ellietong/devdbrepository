SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qrysrptNoticeDocumentLink]
(@CorrespondenceID int)
AS
SET NOCOUNT ON


Select sd.CorrespondenceID, SupportingDocumentType, Description + '#' + ExternalLink as ExternalLink
From tblSupportingDocument sd
Left Join tblSupportingDocumentType sdt on sd.SupportingDocumentTypeID=sdt.SupportingDocumentTypeID
Where sd.CorrespondenceID = @CorrespondenceID 
or sd.CorrespondenceID in (select r.NoticeResponseID from tblResponse r where r.CorrespondenceID=@CorrespondenceID)
Group By sd.SupportingDocumentTypeID, sd.CorrespondenceID, SupportingDocumentType, Description, ExternalLink
Order By sd.SupportingDocumentTypeID, sd.CorrespondenceID, SupportingDocumentType, Description, ExternalLink

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptNoticeDocumentLink] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptNoticeDocumentLink] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptNoticeDocumentLink] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptNoticeDocumentLink] TO [power_user]
GO
