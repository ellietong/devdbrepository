SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryUpdateEmailSentDate]
As

SET NOCOUNT ON

--Procedure will update the ExternalEmail date for each document being sent in the daily email.

Declare @Today4PM datetime
Declare @Yesterday4PM datetime
SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')
SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')


--Create a temp table for Complaints which where emailed out.
DECLARE @NewComplaintsTable TABLE (DocID int, FirmAddressID int, TrackingID int, DateAccepted datetime)

INSERT INTO @NewComplaintsTable
	SELECT DISTINCT
		d.DocumentID,
		l.DefenseCOunselAddressID,
		ctl.ComplaintTrackingID,
		DateAccepted
	FROM tblDocuments d
	JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
	JOIN tblComplaintTrackingLawsuit ctl ON ctl.LawsuitID = cl.LawsuitID
	WHERE D.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
	AND detailDocumentTypeID IN (8, 4, 33, 25, 28)
	AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	AND FirmID IS NOT NULL
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
--AND DateAccepted is NULL-- DO POPULATE FOR ALREADY ACCEPTED -- Don't Populate Email Sent Date for Complaints or Documents that have already been accepted. 


--select * from @NewComplaintsTable
-- Update the complaint tracking table with the date the email is sent

--Begin tran
UPDATE ct
SET ct.ExternalEmailDate = GETDATE()
FROM [kga-ClaimsAdmin].dbo.tblComplaintTracking ct
JOIN @NewComplaintsTable nct
	ON ct.ComplaintID = nct.TrackingID
WHERE ct.complaintID IN (SELECT
	trackingID
FROM @NewComplaintsTable)
AND ct.ExternalEmailDate IS NULL

--rollback


Return




GO
GRANT VIEW DEFINITION ON  [dbo].[qryUpdateEmailSentDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryUpdateEmailSentDate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryUpdateEmailSentDate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryUpdateEmailSentDate] TO [power_user]
GO
