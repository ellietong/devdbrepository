SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCedure [dbo].[qrysrptPolicyLimitsQC]
(@policyid int)
as
set nocount on

--DECLARE @PolicyID INT
--SET @policyid = 2

select pl.*, plt.LimitType
from tblPolicyLimits pl
LEFT JOIN tblPolicyLimitType plt ON pl.LimitTypeID = plt.LimitTypeID
where policyid = @policyid

order by policyid, pl.limittypeid

return

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptPolicyLimitsQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptPolicyLimitsQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptPolicyLimitsQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptPolicyLimitsQC] TO [power_user]
GO
