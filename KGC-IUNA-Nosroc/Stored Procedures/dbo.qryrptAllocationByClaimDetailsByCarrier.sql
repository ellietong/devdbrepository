SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAllocationByClaimDetailsByCarrier] (@ExhaustionDateID int, @Matter int, @carriergroupid int)
As
Set NoCount on


 --Declare @Matter int
 --Declare @ExhaustionDateID int
 --Declare @carrriergroupid int
 --Set @Matter = 2
 --Set @ExhaustionDateID = 181
 --set @carrriergroupid = 2
 
  

DECLARE @SignatoryStatus TABLE (PolicyID int, SignatoryStatus nvarchar(25))
INSERT INTO @SignatoryStatus
SELECT PolicyID, CASE When ProvisionValueID=182 Then 'Sig' When ProvisionValueID=183 Then 'NonSig' else 'N/A' End as SigNonSig
FROM tblProvision pv
WHERE pv.ProvisionLabelID = 35
GROUP BY PolicyID, CASE When ProvisionValueID=182 Then 'Sig' When ProvisionValueID=183 Then 'NonSig' else 'N/A' End

DECLARE @Defense TABLE (PolicyID int, DefenseForm nvarchar(25))
INSERT INTO @Defense
SELECT PolicyID, CASE When pv.provisionvalueID=2 then 'H' when pv.provisionvalueID=3 then 'G' when pv.provisionvalueID=4 then 'I' else 'N/A' end as DefenseForm
FROM tblProvision pv
WHERE pv.ProvisionLabelID = 1
GROUP BY PolicyID, CASE When pv.provisionvalueID=2 then 'H' when pv.provisionvalueID=3 then 'G' when pv.provisionvalueID=4 then 'I' else 'N/A' end



--OTHER CARRIERS
Select triggerstart1 as EarliestExposureDate, triggerend1 as DiagnosisDate, OccurrenceName, 
 sum(O.Cost1)  as CostAllocated, 
	PolicyNum, DefenseForm, SignatoryStatus, CarrierName, Min(PStartDate)as MinStartDate, Max(PEndDate)as MaxEndDate,
	SUM(DefenseAllocation) as AllocationDefense, 
	SUM(SIRIndemnityAllocation+SIRDefenseAllocation) as SIRAllocated, 
	0 as Adjustment, 
	SUM(DefenseOutsideLimitsAllocation) as AllocationDefenseOutsideLimits,
	CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
From tblExhaustion e
	Inner Join tblOccurrence O on e.OccurrenceID=O.OccurrenceID 
	inner join tblPolicyDates pd on pd.PolicyDateID =e.PolicyDateID 
	inner join tblPolicy p on p.PolicyID =pd.PolicyID 
	inner join tblCarrier c on c.CarrierID =p.CarrierID 
	inner Join @Defense d ON p.PolicyID = d.[PolicyID]
	inner JOIN @SignatoryStatus ss ON p.PolicyID = ss.PolicyID	
Where e.ExhaustionDateID = @ExhaustionDateID
AND matterid=@Matter and e.ExhaustionDateID=@ExhaustionDateID
and carriergroupid=@carriergroupid and Cost1 >0
GROUP BY  triggerstart1, triggerend1,  OccurrenceName,
	 PolicyNum, DefenseForm, SignatoryStatus, CarrierName
	
	
RETURN




GO
GRANT ALTER ON  [dbo].[qryrptAllocationByClaimDetailsByCarrier] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetailsByCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetailsByCarrier] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptAllocationByClaimDetailsByCarrier] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetailsByCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetailsByCarrier] TO [power_user]
GO
