SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Proc [dbo].[qryfrmPlaintiffCounselID]

as
Select plaintiffcounselid, plaintiffcounsel
From tblplaintiffcounsel
Order By plaintiffcounsel

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPlaintiffCounselID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPlaintiffCounselID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPlaintiffCounselID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPlaintiffCounselID] TO [power_user]
GO
