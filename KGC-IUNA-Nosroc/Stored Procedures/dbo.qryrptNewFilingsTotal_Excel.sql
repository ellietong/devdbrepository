SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptNewFilingsTotal_Excel] (@StartDate datetime, 
										        @EndDate   datetime,
										        @Matter    int)
AS
SET NOCOUNT ON

----Variables used for testing.
--DECLARE @startdate datetime
--DECLARE @enddate   datetime
--DECLARE @matter    int
--SET @startdate = '1/1/1900'
--SET @enddate   = '3/1/2012'
--SET @matter    = 1

----Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @DiseaseCount TABLE (ClaimID          int, 
							 Matter           nvarchar(100), 
							 MatterID         int, 
							 YearFiled        nvarchar(50), 
							 MoYearFiled      nvarchar(50), 
							 MonthID          int, 
							 MesoCount        int, 
							 LungCancerCount  int, 
							 OtherCancerCount int, 
							 NonMaligCount    int, 
							 SilicosisCount   int, 
							 UnknownCount     int, 
							 unknowncount1    int)
INSERT INTO @DiseaseCount
	SELECT c.ClaimID, 
		Matter, 
		c.MatterID, 
		COALESCE(YEAR(servicedate), YEAR(fileDate), YEAR(statusdate)) AS YearFiled, 
		COALESCE(DATENAME(MONTH,servicedate), DATENAME(MONTH,filedate), DATENAME(MONTH,statusdate)) AS MonthFiled,
		COALESCE(MONTH(servicedate), MONTH(filedate), MONTH(statusdate)) AS MonthID,
		CASE WHEN dmeso.HighLevelDiseaseID = 1      THEN 1 ELSE 0 END AS MesoCount,        --Meso
		CASE WHEN   dlc.HighLevelDiseaseID = 2      THEN 1 ELSE 0 END AS LungCancerCount,  --Lung Cancer
		CASE WHEN   doc.HighLevelDiseaseID = 3      THEN 1 ELSE 0 END AS OtherCancerCount, --Other Cancer
		CASE WHEN   dnm.HighLevelDiseaseID = 4      THEN 1 ELSE 0 END AS NonMaligCount,    --NonMalig
		CASE WHEN   dnm.HighLevelDiseaseID = 7      THEN 1 ELSE 0 END AS Silicosis,        --Silicosis
		CASE WHEN  dunk.HighLevelDiseaseID IN (5,6) THEN 1 ELSE 0 END AS UnknownCount,     --Unknown
		CASE WHEN  dunk.HighLevelDiseaseID IS NULL  THEN 1 ELSE 0 END AS UnknownCount1     --Null
	FROM tblClaimantPersonalInfo cpi
		LEFT JOIN tblClaim c                         ON CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
		LEFT JOIN (SELECT cd.ClaimDiseaseID,
				       cd.ClaimID, 
				       cd.DiseaseDiagnosisDate, 
				       cd.DiseaseID, 
				       cd.DoctorID, 
				       cd.PrimaryDisease 
				   FROM tblClaimDisease cd 
				   WHERE PrimaryDisease = 1) cd      ON CD.ClaimID=C.ClaimID
		LEFT JOIN tblMatter m                        ON c.MatterID = M.MatterID
		LEFT JOIN tblDisease dmeso                   ON CD.DiseaseID=DMeso.DiseaseID --Meso
		LEFT JOIN tblDisease dlc                     ON CD.DiseaseID=DLC.DiseaseID --Lung Cancer
		LEFT JOIN tblDisease doc                     ON CD.DiseaseID=DOC.DiseaseID --Other Cancer
		LEFT JOIN tblDisease dnm                     ON CD.DiseaseID=DNM.DiseaseID --Non-Malig
		LEFT JOIN tblDisease dunk                    ON CD.DiseaseID=DUnk.DiseaseID --Unknown
		LEFT JOIN tblClaimLawsuit cl                 ON C.ClaimID = CL.ClaimID
		LEFT JOIN tblLawsuit l                       ON CL.LawsuitID = L.LawsuitID
		LEFT JOIN tblJurisdiction j                  ON L.JurisdictionID = J.JurisdictionID
		LEFT JOIN tblClaimSequence csq               ON c.ClaimSequenceID = csq.ClaimSequenceID
		LEFT JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = CL.ClaimLawsuitID
	WHERE IsPrimaryForClaim = 1 
		AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) 
		AND ProcessedDate between @StartDate AND @EndDate 
		AND COALESCE(cpi.jobsiteinfo, 0) = 0  
		AND ClaimLawsuitStatusID = 22
	ORDER BY COALESCE(YEAR(serviceDate), YEAR(fileDate), YEAR(StatusDate))

SELECT YEAR(YearFiled) AS 'Service Year', 
	MoYearFiled AS 'Month',
	SUM(MesoCount)                      AS 'Mesothelioma', 
	SUM(LungCancerCount)                AS 'Lung Cancer', 
	SUM(OtherCancerCount)               AS 'Other Cancer',
	SUM(NonMaligCount + SilicosisCount) AS 'Non-Malignant', 
	SUM(UnknownCount + unknowncount1)   AS 'Unknown',
	SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + SilicosisCount + UnknownCount + unknowncount1) AS 'Total'
FROM @DiseaseCount DC
GROUP BY Matter, MatterID, YearFiled, MoYearFiled, MonthID
ORDER BY Matter, YearFiled, MonthID

SELECT DISTINCT YEAR(COALESCE(YearFiled, 0)) AS ApplicableYears
FROM @DiseaseCount
ORDER BY YEAR(COALESCE(YearFiled, 0))


RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsTotal_Excel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsTotal_Excel] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptNewFilingsTotal_Excel] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsTotal_Excel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsTotal_Excel] TO [power_user]
GO
