SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_qryInternalEmailDailyActivity] 
AS
BEGIN
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)

SET @Subject = 	'IUNA/Nosroc: Daily Activity Summary ' + CONVERT(varchar, getdate(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>
<H4>The following activity has in the last 24 hours: </H4>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (docTitle nvarchar(50), FirmID int, lName nvarchar(50), 
	fName nvarchar(50))
INSERT INTO @NewComplaintsTable
SELECT d.DocumentTitle, FirmID, cpi.LastName, cpi.FirstName
	
FROM tblDocuments d
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
WHERE datecreated BETWEEN GETDATE()-1 AND GETDATE() 
	AND detailDocumentTypeID IN (8,4)
	AND RIGHT(CreatedBy, 11) = 'kcicllc.com'
	AND FirmID IS NOT NULL
GROUP BY d.DocumentTitle,
	 FirmID, LastName, FirstName
--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

DECLARE @firm nvarchar(500), @numCreated int, @numAccepted int, @numApproved int, @numModified int

DECLARE @uploaded TABLE (firmID int, firm nvarchar(500), NumCreated int, [User] nvarchar(500))
DECLARE @accepted TABLE (firmID int, firm nvarchar(500), NumAccepted int, [User] nvarchar(500))
DECLARE @approved TABLE (firmID int, firm nvarchar(500), NumApproved int, [User] nvarchar(500))
DECLARE @modified TABLE (firmID int, firm nvarchar(500), NumModified int, [User] nvarchar(500))
DECLARE @firmActivity TABLE (firm nvarchar(500), numCreated int, numAccepted int, numApproved int, numModified int)




INSERT INTO @uploaded
--Number of documents UPLOADED in last day by firm
SELECT COALESCE(f.FirmID, 0), COALESCE(Firm, 'KCIC') AS Firm, COUNT(DocumentID), NULL AS CreatedBy--, cdt.ClaimantDocumentType
 FROM tblDocuments d
INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
LEFT JOIN vtblPerson p ON p.Email = d.CreatedBy
LEFT JOIN vtblFirm f ON p.FirmID = f.FirmID
WHERE (DateCreated BETWEEN GETDATE()-1 AND GETDATE())
GROUP BY  Firm, f.FirmID--, d.CreatedBy--, ClaimantDocumentType
ORDER BY f.Firm

INSERT INTO @accepted
SELECT COALESCE(f.FirmID, 0), COALESCE(Firm, 'KCIC') AS Firm, COUNT(DocumentID), NULL AS AcceptedBy--, cdt.ClaimantDocumentType
 FROM tblDocuments d
INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
LEFT JOIN vtblPerson p ON p.Email = d.AcceptedBy
LEFT JOIN vtblFirm f ON p.FirmID = f.FirmID
WHERE (DateAccepted BETWEEN GETDATE()-1 AND GETDATE())
GROUP BY  Firm, f.FirmID--, d.CreatedBy--, ClaimantDocumentType
ORDER BY f.Firm

INSERT INTO @approved
SELECT COALESCE(f.FirmID, 0), COALESCE(Firm, 'KCIC') AS Firm, COUNT(DocumentID), NULL AS ApprovedBy--, cdt.ClaimantDocumentType
 FROM tblDocuments d
INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
LEFT JOIN vtblPerson p ON p.Email = d.ApprovedBy
LEFT JOIN vtblFirm f ON p.FirmID = f.FirmID
WHERE (DateApproved BETWEEN GETDATE()-1 AND GETDATE())
GROUP BY  Firm, f.FirmID--, d.CreatedBy--, ClaimantDocumentType
ORDER BY f.Firm

INSERT INTO @modified
SELECT COALESCE(f.FirmID, 0), COALESCE(Firm, 'KCIC') AS Firm, COUNT(DocumentID), NULL AS ModifiedBy--, cdt.ClaimantDocumentType
 FROM tblDocuments d
INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
LEFT JOIN vtblPerson p ON p.Email = d.ModifiedBy
LEFT JOIN vtblFirm f ON p.FirmID = f.FirmID
WHERE (DateModified BETWEEN GETDATE()-1 AND GETDATE())
AND DocumentID NOT IN (SELECT DocumentID
	FROM tblDocuments d
	WHERE (DateCreated = DateModified))
GROUP BY  Firm, f.FirmID--, d.CreatedBy--, ClaimantDocumentType
ORDER BY f.Firm

--DECLARE firm_Curs CURSOR FOR
INSERT INTO @firmActivity
SELECT u.firm AS Firm, COALESCE(NumCreated, 0) AS NumCreated, COALESCE(NumModified, 0) AS NumModified, COALESCE(NumAccepted, 0) AS NumAccepted, COALESCE(NumApproved, 0) AS NumApproved 
FROM @uploaded u
LEFT JOIN @accepted ac ON u.firmID = ac.firmID
LEFT JOIN @approved ap ON u.firmID = ap.firmID
LEFT JOIN @modified m ON u.firmID = m.firmID

--OPEN firm_curs
--FETCH NEXT FROM firm_curs INTO @firm, @numCreated, @numModified, @numAccepted, @numApproved
--WHILE @@FETCH_STATUS = 0
--BEGIN

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	border: 1px solid #38160C;
	color: #F6ECF0;}
TD	{font-family: Goudy Old Style; 
	font-size: 10pt; 
	text-align: center;
	border: 1px solid rgb(0,121,193);
	padding: .3em;
	color: #000000}
TH	{border: 1px solid #ffffff;
	font-size: 10pt; 
	padding: .3em;
	color: #F6ECF0;
	background: rgb(0,121,193);}
--->
</STYLE>'

--Declare a cursor and variables to be used in the cursor to create a separate table of complaints for each matter
--DECLARE @NumberPerMatter int
--DECLARE @Matter nVarChar(20)

--DECLARE matterDocs_Curs CURSOR FOR
		
--SELECT COUNT(DISTINCT docTitle), Matter
--FROM @NewComplaintsTable 
--WHERE FirmID = (SELECT @FirmID)
--GROUP BY Matter
--ORDER BY Matter

--OPEN matterDocs_curs 

--FETCH NEXT FROM matterDocs_curs INTO @NumberPerMatter, @Matter

--WHILE @@fetch_status = 0
--BEGIN
----Header line for each matter's table
--SET @BodyMessage = @BodyMessage + '<br>'+ convert(nvarchar(5), (select @NumberPerMatter))+ ' New ' + (select @Matter) + ' Complaint(s):'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @BodyMessage + N'<table>' + 
		N'<tr><th>Firm</th><th>Documents Uploaded</th><th>Documents Accepted</th><th>Documents Approved</th><th>Documents Modified</th>' +
		
		CAST ( (
			SELECT  td = firm, '', td = numCreated, '', td = numAccepted, '', td = numApproved, '', td = numModified, '' 
			FROM @firmActivity
			ORDER BY firm
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
		N'</table>' +  N'<br/>'		

----Repeat for next matter
--FETCH NEXT FROM matterDocs_curs INTO @NumberPerMatter, @Matter
--END			
--CLOSE matterDocs_curs
--DEALLOCATE matterDocs_curs

--Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Activity Summary</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

  
DECLARE @RecipientList nvarchar(500)
SET @RecipientList=''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
--DECLARE @Contact nvarchar(500)
--DECLARE contact_curs CURSOR FOR

--SELECT email 
--FROM vtblPerson p
--WHERE FirmID = @FirmID
--	AND complaintContact = 1
	
--OPEN contact_curs
--FETCH NEXT FROM contact_curs INTO @contact
--WHILE @@FETCH_STATUS=0
--BEGIN 

--SET @RecipientList = @RecipientList + @Contact + '; '

--FETCH NEXT FROM contact_curs INTO @contact
--END
--CLOSE contact_curs
--DEALLOCATE contact_curs

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The NSIProfile sends from NSI@kcicllc.com
@profile_name='IUNA/NosrocProfile',
@recipients = 'crookc@kcicllc.com',
@blind_copy_recipients = '',--'iuna-nosroc@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

----Move to the next firm to create a new email
--FETCH NEXT FROM firm_curs INTO @FirmID, @firmurl
--END
--CLOSE firm_curs
--DEALLOCATE firm_curs

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalEmailDailyActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalEmailDailyActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalEmailDailyActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalEmailDailyActivity] TO [power_user]
GO
