SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID]
	@DocId int
AS
--DECLARE @SPDocId nvarchar(50)
--SET @SPDocId = 'c43ef74a-898e-40bb-a64f-a09ae1b9ed25'
BEGIN
	SELECT
	DocumentID,
	DocumentTitle,
	DocumentURL,
	
	DetailDocumentTypeID As ClaimantDocumentTypeID,
	DocumentSourceID,
	NULL AS AutoAccept,
	ClaimLawsuitStatusID,
	DismissalDate,
	d.CreatedBy,
	COALESCE(d.Accepted, 0) AS Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	COALESCE(d.FirmID, 0) AS FirmID,
	cdt.DocumentGroupID,
	DetailDocumentTypeID,
	AmazonKey
FROM tblDocuments d
INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
WHERE DocumentID = @DocId
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [power_user]
GO
