SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptSubscriptionCoverageByCompanyBySolvency]	
(@limitTypeID int = Null)
As

SET NOCOUNT ON

DECLARE @maxLimitTypeID int
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy

DECLARE @qrySubscriptionCoverageByCompany TABLE (
	[insuranceprogram] varchar (256),
	[subscriptionCompany] varchar(256),
	[subscriptionCompanySolvency] varchar(256),	
	[SubscriberPerOcc] money,
	[CarrierID] int, 
	[InsuranceProgramID] int, 
	[SubscriberID] int)	

INSERT INTO @qrySubscriptionCoverageByCompany 

SELECT InsuranceProgram, scomp.SubscriptionCompany, scomp.SubscriptionCompanySolvency, SUM(scov.Percentage * PerOccLimit) AS SubscriberPerOcc, CarrierID, InsuranceProgramID, 
	scov.SubscriberID
FROM vPolicy p 
	INNER JOIN tblSubscriptionCoverage scov ON p.PolicyID = scov.PolicyID
	INNER JOIN vSubscription scomp ON scomp.SubscriberID = scov.SubscriberID
WHERE p.limitTypeID >= COALESCE(@limitTypeID, 0) AND p.limitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)
	and DoNotReport <>1
GROUP BY InsuranceProgram, scomp.SubscriptionCompany, scomp.SubscriptionCompanySolvency, CarrierID, InsuranceProgramID, scov.SubscriberID

SELECT CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END AS 'SubscriberSolvencyGroup', SUM(SubscriberPerOcc) AS SumOfSubscriberPerOcc
FROM @qrySubscriptionCoverageByCompany
GROUP BY CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByCompanyBySolvency] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByCompanyBySolvency] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByCompanyBySolvency] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByCompanyBySolvency] TO [power_user]
GO
