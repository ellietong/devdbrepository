SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptNewFilingsByPlaintiffCounsel]
(@BeginDate datetime, @EndDate datetime, @matterid int, @StateID int)
As 
Set NoCount On


-- Declare @BeginDate datetime
-- Declare @EndDate datetime

-- Set @BeginDate = '4/1/2011'
-- Set @EndDate = '6/30/2011'
-- Declare @StateID int
--Set @StateID = 0
--Declare @matter int
--set @matter = 1

DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState


Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int,  PrimaryLocalPlaintiffCounsel nvarchar(1000),FilingCount int)
Insert into @DiseaseCount

Select Matter, C.MatterID, PCA.PlaintiffCounsel, 1
From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblPlaintiffCounselAddress PCA on PCA.PlaintiffCounselAddressID=L.PlaintiffCounselAddressID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
left join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=CL.ClaimLawsuitID

Left Join tblState S on L.StateID=S.StateID
LEFT JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID

Where ProcessedDate between @BeginDate and @EndDate  and s.StateID >= COALESCE(@StateID, 0) 
	AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID)
	AND IsPrimaryForClaim = 1 AND ClaimLawsuitStatusID = 22
	AND COALESCE(cpi.jobsiteinfo, 0) = 0
	AND m.MatterID = @matterid
Order By PCA.PlaintiffCounsel

 


Select Matter, MatterID, PrimaryLocalPlaintiffCounsel, Sum(FilingCount) as NewFilingCount, @BeginDate as StartDate, @EndDate as EndDate
from @DiseaseCount
Group By Matter, MatterID, PrimaryLocalPlaintiffCounsel
ORDER BY Matter, PrimaryLocalPlaintiffCounsel

Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByPlaintiffCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByPlaintiffCounsel] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByPlaintiffCounsel] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByPlaintiffCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByPlaintiffCounsel] TO [power_user]
GO
