SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryClaimantDocuments]
(@ClaimantPersonalInfoID bigint = null)
AS

BEGIN

DECLARE @ClaimantPersonalInfoIDlocal bigint
SET @ClaimantPersonalInfoIDlocal = @claimantpersonalinfoid

DECLARE @locked TABLE (DocumentID bigint, Locked bit)

INSERT INTO @locked
SELECT mcd.DocumentID, 
	   CASE WHEN (COUNT(c.ClaimID)+ COUNT(ps.PaymentSettlementID)) > 0 THEN 1 ELSE 0 END  
FROM tblClaim c	
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	LEFT JOIN tblPaymentSettlement ps ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
	INNER JOIN tblLawsuit l ON cl.LawsuitID	 = l.LawsuitID
	INNER JOIN tblMultiClaimantDocument mcd ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	WHERE COALESCE(c.IsCurrentClaim,0) = 1 
	AND  COALESCE(IsPrimaryForClaim, 0) = 1 
	AND  ClaimLawsuitStatusID IN (15,16,17)
	AND c.ClaimantPersonalInfoID = @ClaimantPersonalInfoIDlocal
	GROUP BY mcd.DocumentID

SELECT  DISTINCT  d.DocumentID, 
				  d.SPDocId, 
				  d.DocumentTitle, 
				  '/clients/iuna_nosroc/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/clients/iuna_nosroc/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), @ClaimantPersonalInfoIDlocal) AS DocumentURL, 
				  dg.DocumentGroup, 
				  cdt.ClaimantDocumentType, 
                  f.Firm, 
                  d.AcceptedBy, 
                  d.DateAccepted, 
                  d.DateCreated, 
                  mcd.ClaimantPersonalInfoID,  
                  Accepted, 
                  CreatedBy, 
                  COALESCE(Locked, 0) AS Locked, 
				  AmazonKey,
				  CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical                                   
FROM tblMultiClaimantDocument mcd 
	INNER JOIN dbo.tblDocuments AS d ON mcd.DocumentID = d.documentID
	INNER JOIN dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID 
	INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
	INNER JOIN dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID
	LEFT JOIN  dbo.vtblFirm AS f ON f.FirmID = d.FirmID
	LEFT JOIN @locked l ON mcd.DocumentID = l.DocumentID            
WHERE mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoIDlocal
	AND dg.DocumentGroupID <> 2
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantDocuments] TO [power_user]
GO
