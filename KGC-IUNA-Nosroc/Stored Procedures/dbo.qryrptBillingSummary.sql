SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptBillingSummary]
(@ExhaustionDateID int, @Matter int, @AllocationID int)
As
Set Nocount on

--Declare @Matter int
--Declare @ExhaustionDateID int
--Declare @AllocationID int
--Set @Matter = 2
--Set @ExhaustionDateID = 19
--Set @AllocationID = 3

Declare @Header nvarchar(100)
Declare @ExhaustionTypeID int
Set @Header = (select exhaustiontype + ' - ' + description from tblexhaustiondate ed inner join tblexhaustiontype et on et.exhaustiontypeid = ed.exhaustiontypeid where exhaustiondateid = @exhaustiondateid)
Set @ExhaustionTypeID = (Select exhaustiontypeid from tblExhaustionDate where ExhaustionDateID = @ExhaustionDateID group by ExhaustionTypeID)

--Set Insurance Program
DECLARE @InsuranceProgramID int
SET @InsuranceProgramID = (Case When @Matter=1 then '15' When @Matter=2 then '16' END)

--Set Policy Date Type
DECLARE @PolicyDateTypeID int
SET @PolicyDateTypeID = (Case When @ExhaustionTypeID=6 Then '2' Else '1' END)

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

------------------------------------------------------------------------------------------------------------
/*Non-Shortfall Sig only*/
------------------------------------------------------------------------------------------------------------


Declare @CurrentAllocation Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, OutsideConsumption money, LedgerID bigint, MatterID int)
Insert Into @CurrentAllocation
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, e.PolicyDateID, 
CASE WHEN @AllocationID IN (3,4,5) THEN IndemnityAllocation ELSE ShortfallIndemnityAllocationResults END AS IndemnityAllocation, 
CASE WHEN @AllocationID IN (3,4,5) THEN DefenseAllocation ELSE ShortfallDefenseAllocationResults END AS DefenseAllocation, 
CASE WHEN @AllocationID IN (3,4,5) THEN DefenseOUtsideLimitsAllocation ELSE ShortfallDefenseOutsideLimitsAllocationResults END AS DefenseOUtsideLimitsAllocation, 
SIRIndemnityAllocation, SIRDefenseAllocation, OutsideConsumption, LedgerID, MatterID
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Left Join tblOccurrence O on E.OccurrenceID = O.OccurrenceID
Where ExhaustionTypeID = @ExhaustionTypeID and ED.ExhaustionDateID = @ExhaustionDateID and mAllocationID = @AllocationID


Declare @CurrentAllocationTotals table (ExhaustionTypeID int, PolicyDateID int, Indemnity money, Defense money,
	DefenseOutsideLimits money, TotalDefense money, SIRIndemnityAllocation money, SIRDefenseAllocation money, OutsideConsumption money, MatterID int)
Insert Into @CurrentAllocationTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(IndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(DefenseAllocation), 0) as Defense,
	coalesce(Sum(DefenseOutsideLimitsAllocation),0) as DefenseOutsideLimits,
	coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
	coalesce(Sum(SIRIndemnityAllocation), 0) as SIRIndemnity,
	coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense,
	coalesce(Sum(OutsideConsumption),0) as OutsideConsumption, MatterID
From @CurrentAllocation
Group By ExhaustionTypeID, PolicyDateID, MatterID

--Query Results
Declare @Results Table(OrderID int, OrderName nvarchar(100), policyid bigint, policydateid bigint, carriername nvarchar(100), policynum nvarchar(100), 
	pstartdate datetime, penddate datetime, Solvency nvarchar(100), TotalPercentage float, Indemnity money, Defense money, OutsideConsumption money, 
	DefenseOutsideLimits money, TotalDefense money, MatterID int)
Insert Into @Results

--London
select  '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, c.carriername, p.policynum, pd.pstartdate, pd.penddate,
CASE When subscriptionCompanySolvency like 'Solvent' or  subscriptionCompanySolvency like 'unknown' Then 'Solvent'
	When subscriptionCompanySolvency like 'insolvent' Then 'Insolvent' End As Solvency,
	Round(Sum(Percentage), 5) as TotalPercentage,
	coalesce(Indemnity,0)*Round(Sum(Percentage), 5) as Indemnity, 
	coalesce(Defense,0)*Round(Sum(Percentage), 5) as Defense, 
	coalesce(OutsideConsumption,0)*Round(Sum(Percentage), 5) as OutsideConsumption,
	coalesce(DefenseOutsideLimits,0)*Round(Sum(Percentage), 5) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0)*Round(Sum(Percentage), 5) as TotalDefense, MatterID 
from tblpolicy p 
	Left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblSubscriptionCoverage sc on p.PolicyID=sc.PolicyID
	left join tblSubscriberCompany subc on sc.SubscriberID=Subc.subscriberID
Where C.CarrierID=20 AND pd.PolicyDateTypeID = @PolicyDateTypeID and (InsuranceProgramID=14 or InsuranceProgramID=@InsuranceProgramID)
group by p.policyid, pd.policydateid, c.carriername, p.policynum, indemnity, defense, defenseoutsidelimits, pd.pstartdate, pd.penddate, totaldefense, 
	subc.SubscriptionCompanySolvency, OutsideConsumption, MatterID 
	
UNION

--Most Carriers
select '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, 
CASE WHEN p.CarrierID = 152 THEN 'Employers Insurance of Wausau, A Mutual Company' ELSE CarrierName END AS CarrierName, 
p.policynum, pd.pstartdate, pd.penddate,
	'' As Solvency, '' as TotalPercentage,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(OutsideConsumption,0) as OutsideConsumption,
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, MatterID
 from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
Where c.carrierid not in (10,20,20001,20002,20003)
 AND pd.PolicyDateTypeID = @PolicyDateTypeID and (InsuranceProgramID=14 or InsuranceProgramID=@InsuranceProgramID)

UNION

--IUNA Producer
select '2' as OrderID, CASE WHEN p.CarrierID = 20002 THEN 'IUNA' WHEN p.CarrierID = 20003 THEN 'NOSROC' ELSE 'ACF Refund' END AS OrderName, 
p.policyiD, pd.policydateid, c.carriername, 
CASE WHEN p.CarrierID IN (20002,20003) THEN 'Producer' ELSE 'ACF Refumd' END as policynum, 
null as pstartdate, null as penddate, '' As Solvency, '' as TotalPercentage,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(OutsideConsumption,0) as OutsideConsumption,
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, MatterID
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
Where c.carrierid IN (20001,20002,20003)
AND pd.PolicyDateTypeID = @PolicyDateTypeID and (InsuranceProgramID=14 or InsuranceProgramID=@InsuranceProgramID)

select OrderID, OrderName, policyid, policydateid, carriername, policynum, pstartdate, penddate, Solvency, TotalPercentage, SUM(Indemnity) AS Indemnity, SUM(Defense) AS Defense, SUM(OutsideConsumption) AS OutsideConsumption, 
SUM(DefenseOutsideLimits) AS DefenseOutsideLimits, SUM(TotalDefense) AS TotalDefense, @header as header1, 
CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
From @Results r
Left Join tblMatter m on r.MatterID = m.MatterID
Where r.MatterID >= COALESCE(@Matter,0) AND r.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
GROUP BY OrderID, OrderName, policyid, policydateid, carriername, policynum, pstartdate, penddate, Solvency, TotalPercentage
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [power_user]
GO
