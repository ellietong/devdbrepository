SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAllocationByClaimDetails] (@ExhaustionDateID int, @Matter int)
As
Set NoCount on


 --Declare @Matter int
 --Declare @ExhaustionDateID int
 --Set @Matter = 1
 --Set @ExhaustionDateID = 157
 
  
 
 --Set AllocationID
DECLARE @AllocationID int
SET @AllocationID = (Select mAllocationID From tblExhaustion e INNER JOIN tblOccurrence o ON e.OccurrenceID = o.OccurrenceID 
Where ExhaustionDateID = @ExhaustionDateID AND OccurrenceTypeID NOT IN (22,23) AND mAllocationID IS NOT NULL GROUP BY mAllocationID)
 
 DECLARE @policyDateID table (PolicyDateID int)
 INSERT INTO @policyDateID 
 SELECT PolicyDateID 
 FROM tblExhaustion E
 WHERE ExhaustionDateID = @ExhaustionDateID
 
 IF @AllocationID = 24
 BEGIN
	DELETE FROM @policyDateID WHERE PolicyDateID in (93,95,96)
 END

 
--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @LimitTypeID int
Set @LimitTypeID=(Select LimitTypeID from tblAllocation Where AllocationID=@AllocationID)

DECLARE @PolicyDateTypeID int
SET @PolicyDateTypeID = (SELECT PolicyDateTypeID FROM tblAllocation WHERE AllocationID = @AllocationID)

DECLARE @SignatoryStatus TABLE (PolicyID int, SignatoryStatus nvarchar(25))
INSERT INTO @SignatoryStatus
SELECT PolicyID, CASE When ProvisionValueID=182 Then 'Sig' When ProvisionValueID=183 Then 'NonSig' else 'N/A' End as SigNonSig
FROM tblProvision pv
WHERE pv.ProvisionLabelID = 35
GROUP BY PolicyID, CASE When ProvisionValueID=182 Then 'Sig' When ProvisionValueID=183 Then 'NonSig' else 'N/A' End

DECLARE @Defense TABLE (PolicyID int, DefenseForm nvarchar(25))
INSERT INTO @Defense
SELECT PolicyID, CASE When pv.provisionvalueID=2 then 'H' when pv.provisionvalueID=3 then 'G' when pv.provisionvalueID=4 then 'I' else 'N/A' end as DefenseForm
FROM tblProvision pv
WHERE pv.ProvisionLabelID = 1
GROUP BY PolicyID, CASE When pv.provisionvalueID=2 then 'H' when pv.provisionvalueID=3 then 'G' when pv.provisionvalueID=4 then 'I' else 'N/A' end

DECLARE @Subscription TABLE (PolicyID int, Percentage float, Solvency nvarchar(25))
INSERT INTO @Subscription
SELECT PolicyID, SUM(Percentage), SubscriptionCompanySolvency
FROM tblSubscriptionCoverage sc
INNER JOIN tblSubscriberCompany c ON sc.SubscriberID = c.SubscriberID
GROUP BY PolicyID, SubscriptionCompanySolvency

--LONDON
Select AllocationDescription, ClaimID, triggerstart1 as EarliestExposureDate, triggerend1 as DiagnosisDate, o.OccurrenceID, OccurrenceGroup, Segment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
CASE When Solvency like 'Solvent' or  Solvency like 'unknown' Then 'Solvent' When Solvency like 'insolvent' Then 'Insolvent' End As Solvency, SUM(Percentage) AS TotalPercentage, O.Cost1 + O.Cost3 as CostAllocated, 
	PolicyNum, DefenseForm, SignatoryStatus, CarrierName, Min(PStartDate)as MinStartDate, Max(PEndDate)as MaxEndDate, Layer, 
	SUM(IndemnityAllocation*Percentage) as AllocationIndemnity, 
	SUM(DefenseAllocation*Percentage) as AllocationDefense, 
	SUM((SIRIndemnityAllocation*Percentage)+(SIRDefenseAllocation*Percentage)) as SIRAllocated, 
	0 as Adjustment, 
	SUM(DefenseOutsideLimitsAllocation*Percentage) as AllocationDefenseOutsideLimits,
	CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
From tblExhaustion e
	Inner Join tblAllocation A on A.AllocationID=e.mAllocationID
	INNER JOIN vPolicy p ON e.PolicyDateID = p.PolicyDateID
	INNER JOIN @Subscription s ON p.PolicyID = s.PolicyID
	Left Join @Defense d ON p.PolicyID = d.[PolicyID]
	LEFT JOIN @SignatoryStatus ss ON p.PolicyID = ss.PolicyID	
	Inner Join tblOccurrence O on e.OccurrenceID=O.OccurrenceID
	Inner Join tblOccurrenceGroup OG on OG.OccurrenceGroupID=O.OccurrenceGroupID
	Left Join tblOccurrenceType ot on o.OccurrenceTypeID = ot.OccurrenceTypeID
Where e.mAllocationID=@AllocationID and p.LimitTypeID = @LimitTypeID
and CarrierID = 20 AND p.PolicyDateTypeID = @PolicyDateTypeID AND e.ExhaustionDateID = @ExhaustionDateID
AND O.MatterID >= COALESCE(@Matter,0) AND O.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)and e.policydateid in (Select policydateid from @policyDateID)
GROUP BY AllocationDescription, ClaimID, triggerstart1, triggerend1, o.OccurrenceID, OccurrenceGroup, Segment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
CASE When Solvency like 'Solvent' or  Solvency like 'unknown' Then 'Solvent' When Solvency like 'insolvent' Then 'Insolvent' End, 
	O.Cost1 + O.Cost3, PolicyNum, DefenseForm, SignatoryStatus, CarrierName, Layer

UNION

--OTHER CARRIERS
Select AllocationDescription, ClaimID, triggerstart1 as EarliestExposureDate, triggerend1 as DiagnosisDate, o.OccurrenceID, OccurrenceGroup, Segment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
NULL As Solvency, NULL AS TotalPercentage, O.Cost1 + O.Cost3 as CostAllocated, 
	PolicyNum, DefenseForm, SignatoryStatus, CarrierName, Min(PStartDate)as MinStartDate, Max(PEndDate)as MaxEndDate, Layer, 
	SUM(IndemnityAllocation) as AllocationIndemnity, 
	SUM(DefenseAllocation) as AllocationDefense, 
	SUM(SIRIndemnityAllocation+SIRDefenseAllocation) as SIRAllocated, 
	0 as Adjustment, 
	SUM(DefenseOutsideLimitsAllocation) as AllocationDefenseOutsideLimits,
	CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
From tblExhaustion e
	Inner Join tblAllocation A on A.AllocationID=e.mAllocationID
	INNER JOIN vPolicy p ON e.PolicyDateID = p.PolicyDateID
	Left Join @Defense d ON p.PolicyID = d.[PolicyID]
	LEFT JOIN @SignatoryStatus ss ON p.PolicyID = ss.PolicyID	
	Inner Join tblOccurrence O on e.OccurrenceID=O.OccurrenceID
	Inner Join tblOccurrenceGroup OG on OG.OccurrenceGroupID=O.OccurrenceGroupID
	Left Join tblOccurrenceType ot on o.OccurrenceTypeID = ot.OccurrenceTypeID
Where e.mAllocationID=@AllocationID and p.LimitTypeID = @LimitTypeID
and CarrierID <> 20 AND CarrierID NOT IN (20001,20002,20003) AND p.PolicyDateTypeID = @PolicyDateTypeID AND e.ExhaustionDateID = @ExhaustionDateID
AND O.MatterID >= COALESCE(@Matter,0) AND O.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) and e.policydateid in (Select policydateid from @policyDateID)
GROUP BY AllocationDescription, ClaimID, triggerstart1, triggerend1, o.OccurrenceID, OccurrenceGroup, Segment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
	O.Cost1 + O.Cost3, PolicyNum, DefenseForm, SignatoryStatus, CarrierName, Layer
	
UNION

--PRODUCER
Select AllocationDescription, ClaimID, triggerstart1 as EarliestExposureDate, triggerend1 as DiagnosisDate, o.OccurrenceID, OccurrenceGroup, Segment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
NULL As Solvency, NULL AS TotalPercentage, O.Cost1 + O.Cost3 as CostAllocated, 
	PolicyNum, DefenseForm, SignatoryStatus, CarrierName, Min(PStartDate)as MinStartDate, Max(PEndDate)as MaxEndDate, Layer, 
	SUM(IndemnityAllocation) as AllocationIndemnity, 
	SUM(DefenseAllocation) as AllocationDefense, 
	SUM(SIRIndemnityAllocation+SIRDefenseAllocation) as SIRAllocated, 
	0 as Adjustment, 
	SUM(O.Cost1 + O.Cost3) as AllocationDefenseOutsideLimits,
	CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
From tblExhaustion e
	Inner Join tblAllocation A on A.AllocationID=e.mAllocationID
	INNER JOIN vPolicy p ON e.PolicyDateID = p.PolicyDateID
	Left Join @Defense d ON p.PolicyID = d.[PolicyID]
	LEFT JOIN @SignatoryStatus ss ON p.PolicyID = ss.PolicyID	
	Inner Join tblOccurrence O on e.OccurrenceID=O.OccurrenceID
	Inner Join tblOccurrenceGroup OG on OG.OccurrenceGroupID=O.OccurrenceGroupID
	Left Join tblOccurrenceType ot on o.OccurrenceTypeID = ot.OccurrenceTypeID
Where e.mAllocationID=@AllocationID AND p.PolicyDateTypeID = @PolicyDateTypeID
AND CarrierID IN (20001,20002,20003) AND e.ExhaustionDateID = @ExhaustionDateID AND  p.LimitTypeID = @LimitTypeID
AND O.MatterID >= COALESCE(@Matter,0) AND O.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) and e.policydateid in (Select policydateid from @policyDateID)
GROUP BY AllocationDescription, ClaimID, triggerstart1, triggerend1, o.OccurrenceID, OccurrenceGroup, Segment, OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
	O.Cost1 + O.Cost3, PolicyNum, DefenseForm, SignatoryStatus, CarrierName, Layer
	
RETURN




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetails] TO [power_user]
GO
