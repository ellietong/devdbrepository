SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptExhaustedLimitsSummary]
(@ExhaustionDateID int,  @Matter int)
As
Set Nocount on


 ----Declare @LimitTypeID int
 --Declare @ExhaustionDateID int
 ----Declare @ExhaustionTypeID int
 ----Declare @InsuranceProgramID int
 --Declare @Matter int
 ----Declare @PolicyDateTypeID int
 ----Set @LimitTypeID = 3
 --Set @ExhaustionDateID = 24
 ----Set @ExhaustionTypeID = 6
 ----Set @InsuranceProgramID = 16
 --Set @Matter = 2
 ----Set @PolicyDateTypeID = 1

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--Set Exhaustion Type
DECLARE @ExhaustionTypeID int
SET @ExhaustionTypeID = (Select ExhaustionTypeID From tblExhaustionDate Where ExhaustionDateID=@ExhaustionDateID)

--Set Insurance Program
DECLARE @InsuranceProgramID int
SET @InsuranceProgramID = (Case When @Matter=1 then '15' When @Matter=2 then '16' END)

--Set Policy Date Type
DECLARE @PolicyDateTypeID int
SET @PolicyDateTypeID = (Case When @ExhaustionTypeID=6 Then '2' Else '1' END)

--Set Limit Type
DECLARE @LimitTypeID int
SET @LimitTypeID = (Case When @ExhaustionTypeID=6 Then '4' Else '3' END)


--Actual Policy Dates
Declare @SigPolicyDates table (SigPolicyDateID int, PolicyID int)
Insert Into @SigPolicyDates
Select PolicyDateID, PolicyID
From tblPolicyDates 
Where PolicyDateTypeID=2

Declare @NonSigPolicyDates table (NonSigPolicyDateID int, PolicyID int, PstartDate date, PEndDate date)
Insert Into @NonSigPolicyDates
Select PolicyDateID, PolicyID, PStartDate, PEndDate
From tblPolicyDates 
Where PolicyDateTypeID=1

Declare @ActualPolicyDates table (NonSigPolicyDateID int, SigPolicyDateID int, PolicyID int, PstartDate date, PEndDate date)
Insert Into @ActualPolicyDates
Select NonSigPolicyDateID, SigPolicyDateID, n.PolicyID, PstartDate, PEndDate
From @NonSigPolicyDates n
Left Join @SigPolicyDates s on n.PolicyID=s.PolicyID

--Outside Consumption From Shortfall Allocation
Declare @OutsideConsumptionShortfall Table (PolicyDateID int, OutsideConsumptionShortfall money)
Insert Into @OutsideConsumptionShortfall
Select PolicyDateID, SUM(IndemnityAllocation+DefenseAllocation+DefenseOutsideLimitsAllocation) AS OutsideConsumptionShortfall
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O ON E.OccurrenceID = O.OccurrenceID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<=@ExhaustionDateID and OccurrenceTypeID IN (22,23)
Group By PolicyDateID


--Outside Consumption For Other Insurance Program
Declare @OutsideConsumptionOtherInsPro Table (PolicyDateID int, OutsideConsumptionOtherInsPro money)
Insert Into @OutsideConsumptionOtherInsPro
Select PolicyDateID, SUM(IndemnityAllocation+DefenseAllocation) as OutsideConsumptionOtherInsPro
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O on E.OccurrenceID=O.OccurrenceID
Inner Join tblOccurrenceType OT on O.OccurrenceTypeID=OT.OccurrenceTypeID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<=@ExhaustionDateID and o.MatterID!=@Matter and o.OccurrenceTypeID NOT IN (22,23)
Group By PolicyDateID
 

--Other Outside Consumption For Aetna
Declare @OutsideConsumptionAetna Table (PolicyDateID int, OutsideConsumptionAetna money)
Insert Into @OutsideConsumptionAetna
Select PolicyDateID, SUM(OutsideConsumption) as OutsideConsumptionAetna
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O on E.OccurrenceID=O.OccurrenceID
Inner Join tblOccurrenceType OT on O.OccurrenceTypeID=OT.OccurrenceTypeID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<=@ExhaustionDateID and o.MatterID=@Matter
Group By PolicyDateID
 
 
--Other Outside Consumption Not Aetna
Declare @OutsideConsumptionOther Table (PolicyDateID int, OutsideConsumptionOther money)
Insert Into @OutsideConsumptionOther
Select PolicyDateID, SUM(OutsideConsumption) as OutsideConsumptionOther
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O on E.OccurrenceID=O.OccurrenceID
Inner Join tblOccurrenceType OT on O.OccurrenceTypeID=OT.OccurrenceTypeID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<=@ExhaustionDateID and o.OccurrenceID=55
Group By PolicyDateID
 
--Previous Exhaustion
Declare @Exhaustion Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint,  Matter nvarchar(20))
Insert Into @Exhaustion
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, PolicyDateID, IndemnityAllocation,
	DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation, --OutsideConsumption, 
	 LedgerID, Matter
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Inner Join tblOccurrence O on E.OccurrenceID=O.OccurrenceID
Inner Join tblOccurrenceType OT on O.OccurrenceTypeID=OT.OccurrenceTypeID
Inner Join tblMatter M on O.MatterID=M.MatterID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<=@ExhaustionDateID and 
o.MatterID >= COALESCE(@Matter,0) AND o.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
 and o.OccurrenceTypeID NOT IN (22,23)

Declare @PreviousExhaustionTotals table (ExhaustionTypeID int, PolicyDateID int, Indemnity money, Defense money,
	DefenseOutsideLimits money, TotalDefense money, PreviousAmtExcludingDefOutsideLimits money, PreviousAllocationAmt money,
	SIRIndemnityAllocation money, SIRDefenseAllocation money,  matter nvarchar(20))
Insert Into @PreviousExhaustionTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(IndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(DefenseAllocation), 0) as Defense,
	coalesce(Sum(DefenseOutsideLimitsAllocation),0) as DefenseOutsideLimits,
	coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAmtExcludingDefOutsideLimits,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAllocationAmt,
	coalesce(Sum(SIRIndemnityAllocation), 0) as SIRIndemnity,
	coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense,
	--coalesce(Sum(OutsideConsumption), 0) as OutsideConsumption,
	matter
From @Exhaustion
Group By ExhaustionTypeID, PolicyDateID, Matter

----Header Summary
Declare @HeaderSummary Table (ExhaustionType nvarchar(100), Description nvarchar(100), DefenseForm nvarchar(10))
Insert into @HeaderSummary
Select ExhaustionType, Description, 'H'
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, Description, 'I'
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, Description, 'G'
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, Description, ''
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID



--Query Results
Declare @Results Table(OrderID int, OrderName nvarchar(50),policyid bigint, policydateid bigint, carriername nvarchar(100), layer int, policynum nvarchar(100), pstartdate datetime, penddate datetime, 
attachmentpoint money, perocclimit money, aggregatelimit money, matter nvarchar(20), DefenseForm nvarchar(100), Indemnity money, Defense money, OutsideConsumption money, DefenseOutsideLimits money, 
TotalDefense money, PreviousAllocationAmt money, PreviousAmtExcludingDefOutsideLimits money, Balance money)
Insert Into @Results


--Most Carriers
select '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, Case when p.CarrierID in (152) then 'Employers Insurance of Wausau, A Mutual Company' else c.carriername end as CarrierName, 
pl.layer, p.policynum, apd.pstartdate, apd.penddate,
	pl.attachmentpoint, pl.perocclimit, pl.aggregatelimit, matter,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(OutsideConsumptionShortfall,0) + coalesce(OutsideConsumptionAetna,0) + coalesce(OutsideConsumptionOther,0) + coalesce(OutsideConsumptionOtherInsPro,0) as OutsideConsumption,
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
	coalesce(PreviousAmtExcludingDefOutsideLimits,0) as PreviousAmtExcludingDefOutsideLimits,
  	(coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0) - (coalesce(OutsideConsumptionShortfall,0) + coalesce(OutsideConsumptionAetna,0)+ coalesce(OutsideConsumptionOther,0)+ coalesce(OutsideConsumptionOtherInsPro,0))) as Balance
from tblpolicy p 
	Left Join tblPolicyDates pd on p.PolicyID=pd.PolicyID
	Left join @ActualPolicyDates apd on p.PolicyID=apd.PolicyID
	Left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	Left Join @OutsideConsumptionShortfall S on pd.PolicyDateID=s.PolicyDateID
	Left Join @OutsideConsumptionOther O on pd.PolicyDateID=O.PolicyDateID
	Left Join @OutsideConsumptionOtherInsPro OO on pd.PolicyDateID=OO.PolicyDateID
	Left Join @OutsideConsumptionAetna A on pd.PolicyDateID=A.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where   limittypeid = @LimitTypeID and provisionlabelID=1 and PolicyDateTypeID=@PolicyDateTypeID 
		and (InsuranceProgramID=14 or InsuranceProgramID=@InsuranceProgramID)and c.carrierid not in (10,20001,20002,20003)
 
Union
--NSI Producer
select '2' as OrderID, 'Producer', p.policyiD, pd.policydateid, c.carriername, null as layer, 'Producer' as policynum, null as pstartdate, null as penddate,
	null as attachmentpoint, null as perocclimit, null as aggregatelimit, matter, '' as DefenseForm,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(OutsideConsumptionAetna,0) + coalesce(OutsideConsumptionOther,0) + coalesce(OutsideConsumptionOtherInsPro,0) as OutsideConsumption,
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
	coalesce(PreviousAmtExcludingDefOutsideLimits,0) as PreviousAmtExcludingDefOutsideLimits,
 	null as Balance
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	Left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	Left Join @OutsideConsumptionOther O on pd.PolicyDateID=O.PolicyDateID
	Left Join @OutsideConsumptionOtherInsPro OO on pd.PolicyDateID=OO.PolicyDateID
	Left Join @OutsideConsumptionAetna A on pd.PolicyDateID=A.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
Where (InsuranceProgramID=14 or InsuranceProgramID=@InsuranceProgramID) and ((@insuranceprogramID = 15 and c.carrierid in (20002) or (@InsuranceProgramID = 16 and c.CarrierID = (20003))))
	and PolicyDateTypeID=@PolicyDateTypeID
	
Union
--ACF Refund
select '2' as OrderID, 'ACF Refund', p.policyid, pd.policydateid, c.carriername, pl.layer, 'ACF Refund' as policynum, Null as pstartdate, Null as penddate,
	pl.attachmentpoint, pl.perocclimit, null as aggregatelimit, matter,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	'' as OutsideConsumption,
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
	coalesce(PreviousAmtExcludingDefOutsideLimits,0) as PreviousAmtExcludingDefOutsideLimits,
  	null  as Balance
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	Left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	Left Join @OutsideConsumptionOther O on pd.PolicyDateID=O.PolicyDateID
	Left Join @OutsideConsumptionOtherInsPro OO on pd.PolicyDateID=OO.PolicyDateID
	Left Join @OutsideConsumptionAetna A on pd.PolicyDateID=A.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where (InsuranceProgramID=14 or InsuranceProgramID=@InsuranceProgramID)and c.carrierid in (20001) and PolicyDateTypeID=@PolicyDateTypeID


Select OrderID, OrderName, policyid, policydateid, carriername, layer, policynum, pstartdate, penddate, attachmentpoint, perocclimit, aggregatelimit,
	r.DefenseForm, SUM(Indemnity) AS 'Indemnity', SUM(Defense) AS Defense,SUM(OutsideConsumption) AS OutsideConsumption, SUM(DefenseOutsideLimits) AS DefenseOutsideLimits, 
	SUM(TotalDefense) AS TotalDefense, SUM(PreviousAllocationAmt) AS PreviousAllocationAmt,
	SUM(PreviousAmtExcludingDefOutsideLimits) AS PreviousAmtExcludingDefOutsideLimits, SUM(Balance) AS Balance, ExhaustionType, [Description], 
CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
From @Results R
Inner Join @HeaderSummary HS on R.DefenseForm=HS.DefenseForm
GROUP BY OrderID,OrderName,policyid, policydateid, carriername, layer, policynum, pstartdate, penddate, attachmentpoint, perocclimit, aggregatelimit,
	r.DefenseForm, ExhaustionType, [Description]
Order by Matter, OrderID, PstartDate, attachmentpoint, perocclimit

Return





GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExhaustedLimitsSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExhaustedLimitsSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExhaustedLimitsSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExhaustedLimitsSummary] TO [power_user]
GO
