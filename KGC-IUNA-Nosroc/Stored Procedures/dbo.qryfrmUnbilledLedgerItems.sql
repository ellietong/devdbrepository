SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmUnbilledLedgerItems]
(@BillGroupID varchar(3) = NULL	)
As

SET NOCOUNT ON

SELECT LedgerID, LedgerType, LedgerDate, CheckNumber, Credit, Debit, LedgerNote
FROM tblLedger l INNER JOIN tblLedgerType lt ON l.LedgerTypeID = lt.LedgerTypeID
WHERE BillID IS NULL AND BillGroupID LIKE COALESCE (@BillGroupID, '%')
ORDER BY LedgerDate, Debit DESC, Credit, LedgerID
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnbilledLedgerItems] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnbilledLedgerItems] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnbilledLedgerItems] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnbilledLedgerItems] TO [power_user]
GO
