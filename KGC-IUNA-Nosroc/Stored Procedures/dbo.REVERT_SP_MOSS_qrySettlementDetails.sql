SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qrySettlementDetails]
@ClaimID bigint=null
AS
BEGIN
--DECLARE @ClaimID int
--SET @ClaimID = 1
SELECT     C.ClaimID, hddis.HighLevelDisease AS Disease, S.State, J.Jurisdiction, cs.ClaimSequence, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, L.DocketNumber, L.CaseCaption, 
                      L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  pca.PlaintiffCounsel AS PrimaryLocalPlaintiffCounsel, 
                      r_1.DefenseCounsel AS PrimaryDefenseCounsel, CPI.ClaimantPersonalInfoID, ReleaseSignedDate, rt.ReleaseType, 
                      CLst.StatusNotes, ExecutedReleaseDate as ReleaseReceivedDate, SettlementDueDate, PaidDate, CheckNumber, JOBSITE, ClaimDiseaseID, cl.LawsuitID, ActiveTrialDate,
                      StatusDate, SettlementDate, cd.DiseaseID,
                       doc.DoctorLastName + ', ' + COALESCE(doc.DoctorFirstName, '') + COALESCE(' ' + doc.DoctorMiddleName, '') AS Doctor, SettlementAmount, SettlementGroupID,
                       FullyDocumented, CauseOfInjuryID,
                       IsThirdPartyComplaint, IsMDL, ClaimantSettlementID
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                          (SELECT     dcs.DefenseCounseladdressID, d.DefenseCounsel, dcs.ResponsibleDefenseCounseladdressID, dcs.StateID, 
                                                   r.DefenseCounsel AS ResponsibleDefenseCOunsel, dcs.IsActiveCounsel
                            FROM          dbo.tblDefenseCounselState AS dcs INNER JOIN
											dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
											inner join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
											inner join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID INNER JOIN
                                                   dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
                            GROUP BY d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounseladdressID, dcs.ResponsibleDefenseCounseladdressID, dcs.IsActiveCounsel, dcs.StateID) 
                      AS r_1 ON r_1.DefenseCounseladdressID = L.DefenseCounselAddressID AND r_1.StateID = L.StateID
                      left join (SELECT * from tblClaimDisease where PrimaryDisease = 1) as CD on CD.ClaimID = c.ClaimID
					left join tblDisease ddis on ddis.DiseaseID = cd.DiseaseID
					left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID 
					 LEFT JOIN (SELECT * FROM tblClaimantEmployment WHERE IsPrimary = 1) CE ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID   
                      Left Join (Select * From dbo.tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                      LEFT JOIN dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
					  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
                      left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
                      LEFT JOIN tblDoctor doc ON cd.DoctorID = doc.DoctorID 
                      LEFT JOIN tblDoctor ILODoc ON ILODoctorID=ILODoc.DoctorID
                      LEFT JOIN tblDoctor PFTDoc ON PFTDoctorID=PFTDoc.DoctorID
                      
            
WHERE C.ClaimID = @ClaimID AND cl.IsPrimaryForClaim = 1
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qrySettlementDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qrySettlementDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qrySettlementDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qrySettlementDetails] TO [power_user]
GO
