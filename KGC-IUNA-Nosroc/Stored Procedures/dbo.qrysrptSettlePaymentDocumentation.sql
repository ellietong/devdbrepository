SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--USE [KGC-IUNA-Nosroc]
----GO
--/****** Object:  StoredProcedure [dbo].[qrysrptSettlePaymentDocumentation]    Script Date: 11/08/2012 14:23:23 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
CREATE PROC [dbo].[qrysrptSettlePaymentDocumentation]
(@ClaimantPersonalInfoID bigint)

As
Set NoCount On


--Declare @ClaimantPersonalInfoID bigint
--Set @ClaimantPersonalInfoID = 48625


SELECT sd.SettlementConfirmation, sd.ClaimantIDed, sd.DxMedicals, sd.DODCert, sd.EstatePapers, sd.ProductID, sd.EmployExpoHist, ReleaseRqstDate, 
ExecutedReleaseDate, ReleaseRcvd, MedicareFullyDocumented, MedicareReported, ReleaseToPC, cpi.ClaimantPersonalInfoID, cl.ClaimLawsuitID,
spd.MedicareAddendumRcvd, medicareformsent, MedicareFormRcvd, MedicareAddendumSent 
FROM tblClaimantPersonalInfo cpi
LEFT JOIN tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
LEFT JOIN tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblSettlementDocumentation sd on spd.SetPymtID = sd.SetPymtID
WHERE cpi.ClaimantPersonalInfoID=@ClaimantPersonalInfoID




GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptSettlePaymentDocumentation] TO [power_user]
GO
