SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmLedger]
(@carrierID int = null, @matterID int = null)
AS
SET NOCOUNT ON
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

SELECT *
FROM tblLedger
WHERE CarrierID = @carrierID and matterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
order by BillDate


RETURN 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger] TO [power_user]
GO
