SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmPlaintiffCounselAddress] (@PlaintiffCounselID int)
AS
BEGIN
	IF @PlaintiffCounselID = 0
	BEGIN
		
		SELECT PlaintiffCounselAddressID, PlaintiffCounselID, 
				PlaintiffCounselAddress1, PlaintiffCounselAddress2, 
				PlaintiffCounselCity, PlaintiffCounselStateID, 
				PlaintiffCounselZip, PlaintiffCounselMailZip, 
				PlaintiffCounselPhone, PlaintiffCounselExtension
		FROM [KGA-ClaimsAdmin].dbo.tblPlaintiffCounselAddress
		
	END
	ELSE
	BEGIN
		SELECT PlaintiffCounselAddressID, PlaintiffCounselID, 
				PlaintiffCounselAddress1, PlaintiffCounselAddress2, 
				PlaintiffCounselCity, PlaintiffCounselStateID, 
				PlaintiffCounselZip, PlaintiffCounselMailZip, 
				PlaintiffCounselPhone, PlaintiffCounselExtension
		FROM [KGA-ClaimsAdmin].dbo.tblPlaintiffCounselAddress
		WHERE PlaintiffCounselID = @PlaintiffCounselID
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPlaintiffCounselAddress] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPlaintiffCounselAddress] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPlaintiffCounselAddress] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPlaintiffCounselAddress] TO [power_user]
GO
