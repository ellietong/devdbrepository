SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryDismissalTypeList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimLawsuitStatusID, ClaimLawsuitStatus FROM tblClaimLawsuitStatus WHERE ClaimLawsuitStatusID IN (15, 16)
	UNION
	SELECT NULL, '-- Dismissal Type --'
	ORDER BY ClaimLawsuitStatus
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDismissalTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDismissalTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDismissalTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDismissalTypeList] TO [power_user]
GO
