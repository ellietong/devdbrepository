SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qrySettlementPaymentDetails]
@ClaimantSettlementID bigint=null
AS
BEGIN

Select	cs.ClaimantSettlementID, 
		ps.PaymentAmount, 
		c.SourceNumber as CheckNumber, 
		c.SourceDate as PaymentDate, 
		Case When IsFullyPaid = 1 
					then 'Settlement Fully Paid' 
			 When IsFullyPaid = 0 and SUM(PaymentAmount) > 0 
					then 'Settlement Not Fully Paid'
			Else 'No Payments Made' End as SettlementPaymentStatus
From tblClaimantSettlement cs 
	Left Join tblPaymentSettlement ps on cs.ClaimantSettlementID=ps.ClaimantSettlementID
	Left Join tblCheck c on ps.CheckID=c.CheckID
Where cs.ClaimantSettlementID = @ClaimantSettlementID
Group By cs.ClaimantSettlementID, 
		ps.PaymentAmount, 
		c.SourceNumber, 
		c.SourceDate,
		IsFullyPaid
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qrySettlementPaymentDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qrySettlementPaymentDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qrySettlementPaymentDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qrySettlementPaymentDetails] TO [power_user]
GO
