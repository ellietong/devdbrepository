SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryInternalNewDocumentEmail]
As
Begin

/*Query run daily to send an internal email listing any new documents. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (MAX)

SET @Subject = 'IUNA/NOSROC: New Documents Uploaded as of ' + CONVERT(varchar, GETDATE(), 107)
--Set body font, insert KCIC logo/banner at the top of the email  
--KCIC logo is being pulled from the KCIC site.  Contact D. Snyder with questions or requests.   
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>
<H4>Below are the new uploaded documents for review.</H4>'

SET @BodyMessage =
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	border: 1px solid #38160C;
	color: #F6ECF0;}
TD	{font-family: Goudy Old Style; 
	font-size: 10pt; 
	text-align: center;
	border: 1px solid rgb(0,121,193);
	padding: .3em;
	color: #000000}
TH	{border: 1px solid #ffffff;
	font-size: 10pt; 
	padding: .3em;
	color: #F6ECF0;
	background: rgb(0,121,193);}
--->
</STYLE>'


--Create temp table to hold all new documents
DECLARE @NewDocuments TABLE (docID nvarchar (200), FirmID int, cpiID bigint, DocType nvarchar (200), CreatedBy nvarchar (200), ReleaseRqstDate date, ReleaseToPC date, documentID bigint)
INSERT INTO @NewDocuments
	SELECT
		spdocid,
		FirmID,
		mcd.ClaimantPersonalInfoID,
		ClaimantDocumentType,
		CreatedBy,
		ReleaseRqstDate,
		ReleaseToPC,
		d.DocumentID
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblSettlePaymentDocumentation spd ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
	WHERE datecreated BETWEEN GETDATE() - 1 AND GETDATE()
	--Do not include any documents uploaded by us.
	AND (RIGHT(CreatedBy, 11) != 'kcicllc.com' OR RIGHT(CreatedBy, 8) != 'kcic.com')
	AND detailDocumentTypeID NOT IN (0, 40, 41)
	AND COALESCE(IsPrimaryForClaim, 0) = 1
	AND COALESCE(IsCurrentClaim, 0) = 1
	GROUP BY	SPDocId,
				FirmID,
				mcd.ClaimantPersonalInfoID,
				ClaimantDocumentType,
				CreatedBy,
				ReleaseRqstDate,
				ReleaseToPC,
				d.DocumentID



--Create cursor for each firm
DECLARE @DocCount int, @firm nvarchar (200), @firmURL nvarchar (200)

DECLARE Document_Curs CURSOR FOR SELECT
	COUNT(DISTINCT docid),
	dc.DefenseCounsel,
	'https://admin.kcicllc.com/cp/iuna_nosroccp/Defense%20Counsel%20Documents/Forms/AllItems.aspx'
FROM @newdocuments d
INNER JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
INNER JOIN vtblFirmCase fc ON dc.DefenseCounselID = fc.FirmID
WHERE CaseID = 22
GROUP BY dc.DefenseCounsel
ORDER BY dc.DefenseCounsel
--Open
OPEN Document_Curs
--Fetch
FETCH NEXT FROM Document_Curs INTO @DocCount, @firm, @firmURL
WHILE @@fetch_status = 0
BEGIN
--List which firm, how many documents, and the firm url
SET @BodyMessage = @BodyMessage + CONVERT(nvarchar(10), (SELECT
	@DocCount)
) + ' New Document(s) in the <A HREF ='
+ @firmURL + '>' + @firm + '</A> Document Library<br><br>'

FETCH NEXT FROM Document_Curs INTO @DocCount, @firm, @firmURL
END
CLOSE Document_Curs
DEALLOCATE Document_Curs

IF ((SELECT
	COUNT(ClaimantPersonalInfoID)
FROM tblClaimLawsuit cl
INNER JOIN tblClaim c ON c.ClaimID = cl.ClaimID
INNER JOIN @NewDocuments nd ON nd.cpiID = c.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
WHERE clst.ClaimLawsuitStatusID IN (23, 28)) > 0)
BEGIN
SET @BodyMessage = @bodymessage + '<H4>The following settled claimants have new documents:</H4>'

SET @BodyMessage = @BodyMessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr><style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Last Name</th><style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">First Name</th><style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th><style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Uploaded By</th><style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">RRF Date</th><style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Release To PC</th></tr>'



DECLARE @firstName nvarchar (500), @lastName nvarchar (500), @ClaimantDocumentType nvarchar (500), @CreatedBy nvarchar (200), @ReleaseRqstDate date, @ReleaseToPC date, @DocID bigint
DECLARE Claimant_Curs CURSOR FOR SELECT DISTINCT
	FirstName,
	LastName,
	DocType,
	CreatedBy,
	ReleaseRqstDate,
	ReleaseToPC,
	nd.DocumentID
FROM tblClaimantPersonalInfo cpi
INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
INNER JOIN @NewDocuments nd ON nd.cpiID = cpi.ClaimantPersonalInfoID
INNER JOIN (SELECT
	ClaimantPersonalInfoID
FROM tblClaimLawsuit cl
INNER JOIN tblClaim c ON c.ClaimID = cl.ClaimID
INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
WHERE clst.ClaimLawsuitStatusID IN (23, 28))
s ON cpi.ClaimantPersonalInfoID = s.ClaimantPersonalInfoID
GROUP BY	FirstName,
			LastName,
			DocType,
			CreatedBy,
			ReleaseRqstDate,
			ReleaseToPC,
			nd.DocumentID
ORDER BY LastName, FirstName

--Open
OPEN Claimant_Curs
--Fetch
FETCH NEXT FROM Claimant_Curs INTO @firstname, @lastname, @ClaimantDocumentType, @CreatedBy, @ReleaseRqstDate, @ReleaseToPC, @DocID
WHILE @@fetch_status = 0
BEGIN


SET @BodyMessage = @BodyMessage + N'<tr><td>' + (SELECT
	COALESCE(@lastName, ''))
+ '</td><td>' + (SELECT
	COALESCE(@firstName, ''))
+ '</td><td><a href="https://admin.kcicextranet.com/clients/iuna_nosroc/cp/Pages/DocumentProfile.aspx?ItemId=' + (SELECT
	COALESCE(CONVERT(nvarchar, @DocID), ''))
+ '">' + (SELECT
	@ClaimantDocumentType)
+ '</a></td><td>' + (SELECT
	@CreatedBy)
+ '</td><td>' + (SELECT
	COALESCE(CONVERT(nvarchar, @ReleaseRqstDate, 101), ''))
+ '</td><td>' + (SELECT
	COALESCE(CONVERT(nvarchar, @ReleaseToPC, 101), ''))
+ '</td></tr>'


FETCH NEXT FROM Claimant_Curs INTO @firstname, @lastname, @ClaimantDocumentType, @CreatedBy, @ReleaseRqstDate, @ReleaseToPC, @DocID
END
CLOSE Claimant_Curs
DEALLOCATE Claimant_Curs

SET @BodyMessage = @BodyMessage + N'</table>'
END
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


--Check for new documents
IF (SELECT
	COUNT(DISTINCT docid)
FROM @newdocuments) < > 0
BEGIN
--Send email
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'iuna_nosroc@kcic.com',
								--@recipients = 'amanp@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_Format = 'html'
END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalNewDocumentEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalNewDocumentEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalNewDocumentEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalNewDocumentEmail] TO [power_user]
GO
