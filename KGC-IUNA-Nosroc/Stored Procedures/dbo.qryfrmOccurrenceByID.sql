SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmOccurrenceByID]
As

SET NOCOUNT ON

SELECT OccurrenceID, OccurrenceName
FROM tblOccurrence
order by OccurrenceName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceByID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceByID] TO [power_user]
GO
