SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysrptClaimantRelatedParty]
(@ClaimantPersonalInfoID bigint)

As
Set NoCount On

--Declare @ClaimantPersonalInfoID bigint
--Set @ClaimantPersonalInfoID = 5185

Select FirstName, LastName, middleName, Relationship, lc.LegalCapacity,
RPAddress1, RPAddress2, RPCity, st.[State], RPZip, RPSocialSecurityNumber, RPTIN,
CASE WHEN RPExtension IS NULL OR RPExtension LIKE '' THEN '(' + CONVERT(nvarchar(3),LEFT(RPPhone,3)) + ') ' + CONVERT(nvarchar(3),LEFT(RIGHT(RPPhone,7),3)) + '-' + CONVERT(nvarchar(4),RIGHT(RPPhone,4))
ELSE '(' + CONVERT(nvarchar(3),LEFT(RPPhone,3)) + ') ' + CONVERT(nvarchar(3),LEFT(RIGHT(RPPhone,7),3)) + '-' + CONVERT(nvarchar(4),RIGHT(RPPhone,4)) + ' x' + CONVERT(nvarchar(10),RPExtension) END AS Phone,
RPExtension 
From tblClaimantRelatedParty CRP
LEFT Join tblRelationship R on CRP.RelationshipID=R.RelationshipID
LEFT JOIN tblLegalCapacity lc ON CRP.LegalCapacityID = lc.LegalCapacityID
LEFT JOIN tblState st ON CRP.StateID = st.StateID
Where ClaimantPersonalInfoID=@ClaimantPersonalInfoID
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantRelatedParty] TO [power_user]
GO
