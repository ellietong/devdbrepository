SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptSettledCaseSummaryByState] (@StateID int, 
					@SettlementYearStart nvarchar(250),
					@SettlementYEarEnd nvarchar(250), @Matter int) 

as

Set NoCount On

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

 --Declare @SettlementYearStart nvarchar(250)
 --Declare @SettlementYearEnd nvarchar(250)
 --Declare @StateID int
 --Set @SettlementYearStart = '2000'
 --Set @SettlementYearEnd = '2008'
 --Set @StateID = 0


DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState


Declare @SettledSummary table (Matter nvarchar(100), MatterID int, ClaimNumber int, 
				StateID int,
				State nvarchar(50),
				SettlementYear nvarchar(250),
				SettlementAmount money,
				SettlementAverage money,
				SettlementDate datetime, 
				Meso_Count int,
				Meso_SettlementAmount money,
				Meso_Avg money,
				LungCancer_Count int,
				LungCancer_SettlementAmount money,
				LungCancer_Avg money,
				OtherCancer_Count int,
				OtherCancer_SettlementAmount money,
				OtherCancer_Avg money,
				NonMalig_Count int,
				NonMalig_SettlementAmount money,
				NonMalig_Avg money,
				Other_Count int,
				Other_SettlementAmount money,
				Other_Avg money,
				NoDisease_Count int,
				NoDisease_SettlementAmount money,
				NoDisease_Avg money)

Insert Into @SettledSummary
Select Matter, c.MatterID, count(c.ClaimID) as ClaimantNumber, st.StateID, FullNameState,
	year(settlementdate) as SettlementYear,
	sum(SettlementAmount) as SettlementAmount,
	sum(SettlementAmount)/count(c.ClaimID) as SettlementAverage, 
	convert(datetime,(convert(nvarchar,month(SettlementDate))+'/1/'+ convert(nvarchar,year(SettlementDate)))) as SettlementDate,
	sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 1 then 1 else 0 end) as Meso_Count,
	sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 1 then SettlementAmount else 0 end) as Meso_SettlementAmount,
	Case When sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 1 then 1 else 0 end) = 0 then 0 
		else sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) in (1) then SettlementAmount else 0 end)/ 
			sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 1 then 1 else 0 end) end  as Meso_Avg,
	sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 2 then 1 else 0 end) as LungCancer_Count,
	sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 2 then SettlementAmount else 0 end) as LungCancer_SettlementAmount,
	Case When sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) in (2) then 1 else 0 end) = 0 then 0 
		else sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) in (2) then SettlementAmount else 0 end)/ 
			sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 2 then 1 else 0 end) end  as LungCancer_Avg,
	sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 3 then 1 else 0 end) as OtherCancer_Count,
	sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 3 then SettlementAmount else 0 end) as OtherCancer_SettlementAmount,
	Case When sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 3 then 1 else 0 end) = 0 then 0 
		else sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) =3 then SettlementAmount else 0 end)/ 
			sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 3 then 1 else 0 end) end  as OtherCancer_Avg,
	sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 4 then 1 else 0 end) as NonMalig_Count,
	sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 4 then SettlementAmount else 0 end) as NonMalig_SettlementAmount,
	Case When sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 4 then 1 else 0 end) = 0 then 0 
		else sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 4 then SettlementAmount else 0 end)/ 
			sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 4 then 1 else 0 end) end  as NonMalig_Avg,
	Sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 5 then 1 else 0 end) as Other_Count,
	Sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 5 then SettlementAmount else 0 end) as Other_SettlementAmount,
	Case When sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 5 then 1 else 0 end) = 0 then 0 
		else sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) in (5) then SettlementAmount else 0 end)/ 
			sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 5 then 1 else 0 end) end  as Other_Avg,
	Sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 6 then 1 else 0 end) as NoDisease_Count,
	Sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 6 then SettlementAmount else 0 end) as NoDisease_SettlementAmount,	
	Case When sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 6 then 1 else 0 end) = 0 then 0 
		else sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 6 then SettlementAmount else 0 end)/ 
			sum(Case When coalesce(HLDDef.HighLevelDiseaseID, HLDPl.HighLevelDiseaseID) = 6 then 1 else 0 end) end  as NoDisease_Avg
From tblClaimantPersonalInfo i 
	Left Join tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN tblMatter M on c.MatterID = m.MatterID
	Left Join tblClaimLawsuit l on l.claimID = c.claimID
	Left Join tblLawsuit law on l.lawsuitid = law.lawsuitid
	Left Join tblState st on law.StateID = st.StateID
	Left Join tblCLaimantSettlement s on s.claimlawsuitID = l.claimlawsuitID
	Left Join tblDisease plDisease on c.PlaintiffDiseaseID = plDisease.DiseaseID
	left join tblHighLevelDisease HLDPl on HLDPl.HighLevelDiseaseID=plDisease.HighLevelDiseaseID
	left join tblDisease defDisease on c.DefenseDiseaseID = defDisease.DiseaseID
	Left Join tblHighLevelDisease HLDDef on HLDDef.HighLevelDiseaseID = defDisease.HighLevelDiseaseID
Where IsPrimaryForClaim = 1 AND SettlementAmount > 0
	AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
Group By Matter, c.MatterID, st.StateID, FullNameState, convert(datetime,(convert(nvarchar,month(SettlementDate))+'/1/'+ convert(nvarchar,year(SettlementDate)))),
	year(settlementdate)

Select Matter, MatterID, State, coalesce(ClaimNumber,0) as ClaimNumber, 
	Year(SettlementDate) as SettlementYear,
	coalesce(SettlementAmount,0) as SettlementAmount,
	coalesce(SettlementAverage,0) as SettlementAverage,
	SettlementDate as SettlementDate, 
	coalesce(Meso_Count,0) as Meso_Count,
	coalesce(Meso_SettlementAmount,0) as Meso_SettlementAmount,
	coalesce(Meso_Avg,0) as Meso_Avg,
	coalesce(LungCancer_Count,0) as LungCancer_Count,
	coalesce(LungCancer_SettlementAmount,0) as LungCancer_SettlementAmount,
	coalesce(LungCancer_Avg,0) as LungCancer_Avg,
	coalesce(OtherCancer_Count,0) as OtherCancer_Count,
	coalesce(OtherCancer_SettlementAmount,0) as OtherCancer_SettlementAmount,
	coalesce(OtherCancer_Avg,0) as OtherCancer_Avg,
	coalesce(NonMalig_Count,0)as NonMalig_Count,
	coalesce(NonMalig_SettlementAmount,0)as NonMalig_SettlementAmount,
	coalesce(NonMalig_Avg,0)as NonMalig_Avg,
	coalesce(Other_Count,0)as Other_Count,
	coalesce(Other_SettlementAmount,0)as Other_SettlementAmount,
	coalesce(Other_Avg,0)as Other_Avg,
	coalesce(NoDisease_Count,0) as NoDisease_Count,
	coalesce(NoDisease_SettlementAmount,0) as NoDisease_SettlementAmount,
	coalesce(NoDisease_Avg,0) as NoDisease_Avg
From @SettledSummary 
Where (year(SettlementDate) between @SettlementYearStart and @SettlementYearEnd) 
AND StateID >= COALESCE(@StateID, 0) AND StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID)
Order By SettlementDate

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSettledCaseSummaryByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSettledCaseSummaryByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSettledCaseSummaryByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSettledCaseSummaryByState] TO [power_user]
GO
