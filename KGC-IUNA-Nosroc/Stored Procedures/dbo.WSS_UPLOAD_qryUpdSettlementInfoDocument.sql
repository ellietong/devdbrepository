SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryUpdSettlementInfoDocument]
(@SettleDocID bigint, @DocCategoryID int, @PageNums nvarchar(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblSettleDocs
	SET DocCategoryID = @DocCategoryID,
		PageNums = @PageNums
	WHERE SettleDocID = @SettleDocID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdSettlementInfoDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdSettlementInfoDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdSettlementInfoDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdSettlementInfoDocument] TO [power_user]
GO
