SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryrptPendingClaimsCountByJurisdiction] (@StateID int, @StartDate date, @EndDate date, @Matter int)
AS

Set NoCount ON

----Declare @ResponsibleDefenseCounselID int
--Declare @StateID int
--Declare @startdate date
--Declare @endDate Date
--Declare @matter int
----Set @ResponsibleDefenseCounselID = 18
--Set @StateID = 0
--Set @startdate = '7/1/2010' 
--Set @endDate = '9/30/2010'
--set @matter = 1


-- Find Max ID Values
--Declare @MaxResponsibleDefenseCounselID int
Declare @MaxStateID int

--Define Max State ID
Select @MaxStateID = MAX(stateid) from tblState
--Select @MaxResponsibleDefenseCounselID = MAX(ResponsibleDefenseCounselID) From @AllResponsibleDefenseCounsel
--If @ResponsibleDefenseCounselID = 0 Set @ResponsibleDefenseCounselID = Null

Declare @AllResponsibleDefenseCounsel table (DefenseCounselID int, 
				DefenseCounsel nvarchar(100), 
				ResponsibleDefenseCounselID int,
				StateID int, 
				ResponsibleDefenseCounsel nvarchar(100),
				IsActiveCounsel bit)
Insert Into @AllResponsibleDefenseCounsel
Select dcs.DefenseCOunselID,
	d.DefenseCounsel as DefenseCounsel,
	dcs.responsibledefensecounselID,
	StateID as StateID,
	r.DefenseCOunsel as ResponsibleDefenseCOunsel,
	IsActiveCounsel
From tblDefenseCounselState dcs Inner Join tblDefenseCounsel d on d.defensecounselID = dcs.defensecounselID
	Inner JOin tblDefenseCounsel r on r.defensecounselID = dcs.responsibledefensecounselID
Group By d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCOunselID, dcs.responsibledefensecounselID, IsActiveCounsel, StateID


-- Find Max ID Values
Declare @MaxResponsibleDefenseCounselID int


--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int, State nvarchar(150), Jurisdiction nvarchar(250), 
PrimaryLocalPlaintiffCounsel nvarchar(1000), YearFiled datetime, MesoCount int, LungCancerCount int, OtherCancerCount int, 
NonMaligCount int, UnknownCount int)
Insert into @DiseaseCount

Select Matter, C.MatterID, FullNameState as state, Jurisdiction, PrimaryLocalPlaintiffCounsel, 
convert(datetime,('1/1/'+convert(nvarchar,Year(ServiceDate)))) as YearFiled,
--Meso
Case	when(Coalesce(DDefenseMeso.HighLevelDiseaseID,DPlaintiffMeso.HighLevelDiseaseID))=1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when(Coalesce(DDefenseLC.HighLevelDiseaseID,DPlaintiffLC.HighLevelDiseaseID))=2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when(Coalesce(DDefenseOC.HighLevelDiseaseID,DPlaintiffOC.HighLevelDiseaseID))=3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when(Coalesce(DDefenseNM.HighLevelDiseaseID,DPlaintiffNM.HighLevelDiseaseID))=4 Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when(Coalesce(DDefenseUnk.HighLevelDiseaseID,DPlaintiffUnk.HighLevelDiseaseID))=5 Then 1
Else 0 End AS UnknownCount

From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
--Meso
Left Join tblDisease DDefenseMeso on C.DefenseDiseaseID=DDefenseMeso.DiseaseID
Left Join tblDisease DPlaintiffMeso on C.PlaintiffDiseaseID=DPlaintiffMeso.DiseaseID
--Lung Cancer
Left Join tblDisease DDefenseLC on C.DefenseDiseaseID=DDefenseLC.DiseaseID
Left Join tblDisease DPlaintiffLC on C.PlaintiffDiseaseID=DPlaintiffLC.DiseaseID
--Other Cancer
Left Join tblDisease DDefenseOC on C.DefenseDiseaseID=DDefenseOC.DiseaseID
Left Join tblDisease DPlaintiffOC on C.PlaintiffDiseaseID=DPlaintiffOC.DiseaseID
--Non-Malig
Left Join tblDisease DDefenseNM on C.DefenseDiseaseID=DDefenseNM.DiseaseID
Left Join tblDisease DPlaintiffNM on C.PlaintiffDiseaseID=DPlaintiffNM.DiseaseID
--Unknown
Left Join tblDisease DDefenseUnk on C.DefenseDiseaseID=DDefenseUnk.DiseaseID
Left Join tblDisease DPlaintiffUnk on C.PlaintiffDiseaseID=DPlaintiffUnk.DiseaseID


Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
--Left Join tblDefenseCounselState DCS on L.PrimaryDefenseCounselID=DCS.DefenseCounselID AND L.StateID=DCS.StateID
Left Join tblState S on L.StateID=S.StateID
Left Join @AllResponsibleDefenseCounsel r on r.DefenseCounselID = l.PrimaryDefenseCounselID and r.StateID = l.StateID

Where  s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) and 
 ServiceDate between @StartDate and @EndDate and IsPrimaryForClaim = 1
--	AND ((r.ResponsibleDefenseCounselID >= COALESCE(@ResponsibleDefenseCounselID, 0) AND r.ResponsibleDefenseCounselID <= COALESCE(@ResponsibleDefenseCounselID, @MaxResponsibleDefenseCounselID)))
	AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
	AND ClaimLawsuitStatusID = 22
Order By State, Year(ServiceDate), Jurisdiction, PrimaryLocalPlaintiffCounsel


Select Matter, MatterID, YEAR(@EndDate) AS CloseYear, State, Jurisdiction, PrimaryLocalPlaintiffCounsel, Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, Sum(OtherCancerCount) as OtherCancerCount,
	Sum(NonMaligCount) as NonMaligCount, Sum(UnknownCount) as UnknownCount
from @DiseaseCount
Group By Matter, MatterID, State, Jurisdiction, PrimaryLocalPlaintiffCounsel
ORDER BY Matter, State, Jurisdiction, PrimaryLocalPlaintiffCounsel

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdiction] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdiction] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdiction] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdiction] TO [power_user]
GO
