SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptCashFlowRecentActivity]
(@StartDate date, @Matter int)
As

SET NOCOUNT ON

--Declare @StartDate date
--Declare @matter int
--Set @StartDate = '4/1/2010'
--Set @Matter = NULL

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblCashFlow

DECLARE @minStartDate datetime
SELECT @minStartDate = MIN([Date]) FROM tblCashFlow

SELECT      CashFlowID, TransactionType, Matter, CF.MatterID, [Date], Indemnity, DefenseCOunsel, NonBillable_DefenseCOunsel, MiscFees,
      Shortfall, Bundled, CashReceipts,
      Indemnity+DefenseCounsel+Shortfall+MiscFees+Bundled+CashReceipts as TransferTotal,
      Total, Comment, CheckNumber, CheckStatus, Year([Date]) as Year, @StartDate as MinDate
FROM  tblCashFlownew CF
Left Join tblTransactionType TT on CF.TransactionTypeID=TT.TransactionTypeID
Left Join tblMatter M on CF.MatterID=M.MatterID
Where Date>=COALESCE(@startdate,'1/1/1900')
and CF.MatterID >= COALESCE(@Matter,0) AND CF.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
Order By [Date]

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity] TO [power_user]
GO
