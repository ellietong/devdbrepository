SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_PROFILE_qryDelDocument] (@DocumentID int, 
															 @Emailaddress nvarchar(250))
AS

SET NOCOUNT ON
	
DECLARE @AmazonKey nvarchar(4000) = (SELECT AmazonKey FROM tblDocuments WHERE DocumentID = @DocumentID)	

--If there is only 1 instance of the AmazonKey in tblDocuments, select the AmazonKey to be deleted from Amazon docs
IF(SELECT COUNT(AmazonKey) FROM tblDocuments WHERE AmazonKey = @AmazonKey) = 1
BEGIN
	--Select the AmazonKey
	SELECT AmazonKey FROM tblDocuments WHERE DocumentID = @DocumentID
END	

--Update the SPUsername field in tblMultiClaimantDocument prior to delete for audit log purposes
UPDATE tblMultiClaimantDocument
	SET SPUsername = @emailaddress
	WHERE DocumentID = @DocumentID

--Update the ModifiedBy field in tblDocuments prior to delete for audit log purposes
UPDATE tblDocuments
	SET ModifiedBy = @emailaddress
	WHERE DocumentID = @DocumentID

--This is used on ViewDoc to delete **ALL** tblDocument and tblMultiClaimantDocument records for a given DocumentID
DELETE FROM tblMultiClaimantDocument WHERE DocumentID = @DocumentID
DELETE FROM tblDocuments WHERE DocumentID = @DocumentID

--This is used to delete the document-related records from the settlement and invoice tables
DELETE FROM tblSettleDocs WHERE DocumentID = @DocumentID
DELETE FROM tblDefenseInvoice WHERE DocumentID = @DocumentID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDelDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDelDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDelDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDelDocument] TO [power_user]
GO
