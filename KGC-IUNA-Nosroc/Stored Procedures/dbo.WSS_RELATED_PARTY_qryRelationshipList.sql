SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_RELATED_PARTY_qryRelationshipList]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT RelationshipID, Relationship FROM tblRelationship
	UNION
	SELECT NULL,''
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryRelationshipList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryRelationshipList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryRelationshipList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryRelationshipList] TO [power_user]
GO
