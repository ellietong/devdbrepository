SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptNewFilingsByState_Old] (@StateID int, @StartDate datetime, @EndDate datetime, @Matter int)


AS
SET NOCOUNT ON

--Declare @StateID int
--Declare @StartDate date
--Declare @EndDate date
--Declare @Matter int
--Set @StateID = 0
--Set @StartDate='3/31/2010'
--Set @EndDate='6/30/2010'
--Set @Matter = 1

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState

Declare @DiseaseCount Table (ClaimID int, Matter nvarchar(100), MatterID int, 
FullNameState nvarchar(250), YearFiled int, Disease nvarchar (100), lastname nvarchar (100), firstname nvarchar(100), socialsecuritynumber nvarchar (250),filedate datetime, servicedate datetime,
localplaintiffcounsel nvarchar (200), jurisdiction nvarchar (200), docketnumber nvarchar (100), ClaimSequence nvarchar (200))
Insert into @DiseaseCount

Select C.ClaimID, Matter, C.MatterID, FullNameState,
COALESCE(YEAR(ServiceDate), YEAR(FileDate)) as YearFiled,
--Meso
Coalesce(D.HighLevelDiseaseID,P.HighLevelDiseaseid) as Disease,
 CPI.LastName, CPI.FirstName, COALESCE(RIGHT(CPI.SocialSecurityNumber,4),''), L.FileDate, L.ServiceDate, L.PrimaryLocalPlaintiffCounsel, j.Jurisdiction, DocketNumber,
ClaimSequence


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
--Meso
Left Join tblDisease D on C.DefenseDiseaseID=D.DiseaseID
Left Join tblDisease p on C.PlaintiffDiseaseID=p.DiseaseID
--left join tblHighLevelDisease hd on hd.HighLevelDiseaseID = d.HighLevelDiseaseID --and hd.HighLevelDiseaseID = p.HighLevelDiseaseID

Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
Left Join tblDefenseCounselState DCS on L.PrimaryDefenseCounselID=DCS.DefenseCounselID AND L.StateID=DCS.StateID
Left Join tblState S on L.StateID=S.StateID
left join tblClaimSequence css on css.ClaimSequenceID = c.ClaimSequenceID

Where  s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) and 
ServiceDate between @StartDate and @EndDate and IsPrimaryForClaim = 1 
	AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
Order By State, Year(ServiceDate), Jurisdiction, PrimaryLocalPlaintiffCounsel


Select Matter, MatterID, FullNameState, YearFiled AS 'Year', hd.HighLevelDisease, LastName, FirstName, SocialSecurityNumber, FileDate, ServiceDate, localplaintiffcounsel, Jurisdiction, DocketNumber
,ClaimSequence,@startDate as StartDate, @EndDate as EndDate
from @DiseaseCount DC
left join tblHighLevelDisease hd on hd.HighLevelDiseaseID = DC.Disease
Group By Matter, MatterID, FullNameState, hd.HighLevelDisease, YearFiled, LastName, FirstName, SocialSecurityNumber, FileDate, ServiceDate, localplaintiffcounsel, Jurisdiction, DocketNumber,ClaimSequence
ORDER BY  Matter, FullNameState, COALESCE(ServiceDate, FileDate), lastname, firstname

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByState_Old] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByState_Old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByState_Old] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByState_Old] TO [power_user]
GO
