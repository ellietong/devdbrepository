SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qrysfrmClaimsForDismissal2]
(@DocumentID bigint)

As
SET NOCOUNT ON

SELECT
	LastName + COALESCE(', ' + FirstName + ' ' + MiddleName, ', ' + FirstName, ', ' + MiddleName, '') AS ClaimantName,
	SocialSecurityNumber,
	IndividualDocketNumber,
	CaseCaption,
	DocketNumber,
	clst.StatusNotes,
	State AS FullNameState,
	cl.ClaimLawsuitID,
	clst.ClaimLawsuitStatusTrackingID,
	ProcessedDate,
	StatusDate,
	ClaimLawsuitStatus,
	clst.ClaimLawsuitStatusID,
	IsPrimaryStatus

FROM tblClaimantPersonalInfo cpi
INNER JOIN tblMultiClaimantDocument mcd	ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblClaim c	ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl	ON cl.ClaimID = c.ClaimID AND mcd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN (SELECT	* FROM tblClaimLawsuitStatusTracking WHERE IsPrimaryStatus = 1)clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblClaimLawsuitStatus cls	ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
INNER JOIN tblLawsuit l	ON l.LawsuitID = cl.LawsuitID
INNER JOIN tblState s   ON s.StateID = l.StateID

WHERE DocumentID = @DocumentID

GROUP BY	DocketNumber,
			IndividualDocketNumber,
			LastName,
			MiddleName,
			FirstName,
			cpi.ClaimantPersonalInfoID,
			CaseCaption,
			DocketNumber,
			clst.StatusNotes,
			IsPrimaryStatus,
			State,
			cl.ClaimLawsuitID,
			SocialSecurityNumber,
			clst.ClaimLawsuitStatusTrackingID,
			ProcessedDate,
			ClaimLawsuitStatus,
			StatusDate,
			clst.ClaimLawsuitStatusID
--Order By ClaimantName, cpi.ClaimantPersonalInfoID, DocketNumber

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForDismissal2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForDismissal2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForDismissal2] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForDismissal2] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForDismissal2] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForDismissal2] TO [power_user]
GO
