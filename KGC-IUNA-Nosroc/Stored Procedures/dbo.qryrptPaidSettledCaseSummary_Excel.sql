SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptPaidSettledCaseSummary_Excel] (@paidDateStart nvarchar(250),
														@paidDateEnd nvarchar(250), 
														@Matter int) 
AS
SET NOCOUNT ON

----Variables used for testing.
--DECLARE @paidDateStart datetime
--DECLARE @paidDateEnd   datetime
--DECLARE @matter        int
--SET @paidDateStart = '1/1/1900'
--SET @paidDateEnd   = '3/1/2012'
--SET @matter        = 2
 
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @ResolvedSummary TABLE (ClaimNumber                  int, 			
								SettlementAmount             money,
								SettlementAverage            money,
								ResolvedDate                 datetime,
								Meso_Count                   int,
								Meso_SettlementAmount        money,
								Meso_Avg                     money,
								LungCancer_Count             int,
								LungCancer_SettlementAmount  money,
								LungCancer_Avg               money,
								OtherCancer_Count            int,
								OtherCancer_SettlementAmount money,
								OtherCancer_Avg              money,
								NonMalig_Count               int,
								NonMalig_SettlementAmount    money,
								NonMalig_Avg                 money,
								Other_Count                  int,
								Other_SettlementAmount       money,
								NoDisease_Count              int,
								NoDisease_SettlementAmount   money,
								Null_Count                   int,
								Null_SettlementAmount        money, 
								Matter                       nvarchar(100),
								MatterID                     int)
INSERT INTO @ResolvedSummary 
SELECT count(c.ClaimID) as ClaimantNumber,
	coalesce(sum(SettlementAmount),0) as SettlementAmount,
	coalesce(sum(SettlementAmount),0)/count(c.ClaimID) as SettlementAverage, 
	convert(datetime,(convert(nvarchar,month(ck.sourcedate))+'/1/'+ convert(nvarchar,year(sourcedate)))) as resolvedDate,
	sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) as Meso_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end) as Meso_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) = 0 then 0 
		 else sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end)/ 
			  sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) end  as Meso_Avg,
	sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) as LungCancer_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end) as LungCancer_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) = 0 then 0 
		 else sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end)/ 
			  sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) end  as LungCancer_Avg,
	sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) as OtherCancer_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end) as OtherCancer_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) = 0 then 0 
		 else sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end)/ 
			  sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) end  as OtherCancer_Avg,
	sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) as NonMalig_Count,	
	sum(Case When HLD.HighLevelDiseaseID in (4,7) then SettlementAmount else 0 end) as NonMalig_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) = 0 then 0 
		 else sum(Case When HLD.HighLevelDiseaseID in (4,7)then SettlementAmount else 0 end)/ 
			  sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) end  as NonMalig_Avg,
	Sum(Case When HLD.HighLevelDiseaseID = 5 then 1 else 0 end) as Other_Count,	
	Sum(Case When HLD.HighLevelDiseaseID = 5 then SettlementAmount else 0 end) as Other_SettlementAmount,
	Sum(Case When HLD.HighLevelDiseaseID = 6 then 1 else 0 end) as NoDisease_Count,	
	Sum(Case When HLD.HighLevelDiseaseID = 6 then SettlementAmount else 0 end) as NoDisease_SettlementAmount,
	Sum(Case When HLD.HighLevelDiseaseID is null then 1 else 0 end) as Null_Count,				
	Sum(Case When HLD.HighLevelDiseaseID  is null then SettlementAmount else 0 end) as Null_SettlementAmount,
	Matter, 
	M.MatterID
FROM tblClaimantPersonalInfo i 
	LEFT  JOIN tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT  JOIN (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
	LEFT  JOIN tblClaimLawsuit l on l.claimID = c.claimID
	LEFT  JOIN tblLawsuit law on l.lawsuitid = law.lawsuitid
	LEFT  JOIN tblCLaimantSettlement s on s.claimlawsuitID = l.claimlawsuitID
	LEFT  JOIN tblDisease D on CD.DiseaseID = D.DiseaseID
	LEFT  JOIN tblHighLevelDisease HLD on HLD.HighLevelDiseaseID=D.HighLevelDiseaseID
	LEFT  JOIN tblMatter m on m.MatterID = c.MatterID
	LEFT  JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID
	LEFT  JOIN tblPaymentSettlement ps on ps.ClaimantSettlementID = s.ClaimantSettlementID
	LEFT  JOIN tblCheck ck on ck.CheckID= ps.CheckID
	INNER JOIN tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = l.ClaimLawsuitID
WHERE IsPrimaryForClaim = 1 
	AND convert(datetime,(convert(nvarchar,month(SourceDate))+'/1/'+ convert(nvarchar,year(SourceDate)))) >= @paidDateStart 
	AND convert(datetime,(convert(nvarchar,month(sourcedate))+'/1/'+ convert(nvarchar,year(sourcedate)))) <= @paidDateEnd
	AND ClaimLawsuitStatusID IN (23) 
	AND cst.IsPrimaryStatus = 1
	AND C.MatterID  >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
 	AND COALESCE(i.jobsiteinfo, 0) = 0
GROUP BY convert(datetime,(convert(nvarchar,month(SourceDate))+'/1/'+ convert(nvarchar,year(SourceDate)))), Matter, m.MatterID

SELECT COALESCE(ClaimNumber,0) as ClaimNumber, 
	COALESCE(resolveddate, 0) as resolvedDate,
	YEAR(COALESCE(resolveddate, 0)) as resolvedYear,
	CASE WHEN COALESCE(MONTH(ResolvedDate), 0) =  1 THEN 'January'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  2 THEN 'February'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  3 THEN 'March'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  4 THEN 'April'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  5 THEN 'May'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  6 THEN 'June'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  7 THEN 'July'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  8 THEN 'August'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  9 THEN 'September'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) = 10 THEN 'October'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) = 11 THEN 'November'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) = 12 THEN 'December' END AS ResolvedMonth, 
	COALESCE(SettlementAmount,0) as SettlementAmount,
	COALESCE(SettlementAverage,0) as SettlementAverage,
	COALESCE(Meso_Count,0) as Meso_Count,
	COALESCE(Meso_SettlementAmount,0) as Meso_SettlementAmount,
	COALESCE(Meso_Avg,0) as Meso_Avg,
	COALESCE(LungCancer_Count,0) as LungCancer_Count,
	COALESCE(LungCancer_SettlementAmount,0) as LungCancer_SettlementAmount,
	COALESCE(LungCancer_Avg,0) as LungCancer_Avg,
	COALESCE(OtherCancer_Count,0) as OtherCancer_Count,
	COALESCE(OtherCancer_SettlementAmount,0) as OtherCancer_SettlementAmount,
	COALESCE(OtherCancer_Avg,0) as OtherCancer_Avg,
	COALESCE(NonMalig_Count,0) as NonMalig_Count,
	COALESCE(NonMalig_SettlementAmount,0) as NonMalig_SettlementAmount,
	COALESCE(NonMalig_Avg,0) as NonMalig_Avg,
	COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)                                       AS Other_Count,
	COALESCE(Other_SettlementAmount,0) + COALESCE(NoDisease_SettlementAmount,0) + COALESCE(Null_SettlementAmount,0)      AS Other_SettlementAmount,
	(COALESCE(Other_SettlementAmount,0) + COALESCE(NoDisease_SettlementAmount,0) + COALESCE(Null_SettlementAmount,0)) / 
		CASE WHEN (COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)) = 0 THEN 1 
			 ELSE (COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)) END                   AS Other_Avg,
	COALESCE(Meso_Count,0) + COALESCE(LungCancer_Count,0) + COALESCE(OtherCancer_Count,0) + COALESCE(NonMalig_Count,0) + 
		COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)                                   AS Total_Count,
	COALESCE(Meso_SettlementAmount,0) + COALESCE(LungCancer_SettlementAmount,0) + COALESCE(OtherCancer_SettlementAmount,0) + 
		COALESCE(NonMalig_SettlementAmount,0) + COALESCE(Other_SettlementAmount,0) + 
		COALESCE(NoDisease_SettlementAmount,0) + COALESCE(Null_SettlementAmount,0)                                       AS Total_SettlementAmount,
	(COALESCE(Meso_SettlementAmount,0) + COALESCE(LungCancer_SettlementAmount,0) + COALESCE(OtherCancer_SettlementAmount,0) + 
		COALESCE(NonMalig_SettlementAmount,0) + COALESCE(Other_SettlementAmount,0) + 
		COALESCE(NoDisease_SettlementAmount,0) + COALESCE(Null_SettlementAmount,0)) /
		CASE WHEN (COALESCE(Meso_Count,0) + COALESCE(LungCancer_Count,0) + COALESCE(OtherCancer_Count,0) + COALESCE(NonMalig_Count,0) + 
				   COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)) = 0 THEN 1 
			 ELSE (COALESCE(Meso_Count,0) + COALESCE(LungCancer_Count,0) + COALESCE(OtherCancer_Count,0) + COALESCE(NonMalig_Count,0) + 
			       COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)) END                   AS Total_Avg,
 	@paidDatestart as startdate, 
 	@paidDateEnd as Enddate, 
 	Matter, 
 	MatterID
FROM @resolvedSummary 
ORDER BY COALESCE(ResolvedDate,0)

SELECT DISTINCT YEAR(COALESCE(ResolvedDate, 0)) AS ApplicableYears
FROM @resolvedSummary
ORDER BY YEAR(COALESCE(ResolvedDate, 0))

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPaidSettledCaseSummary_Excel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPaidSettledCaseSummary_Excel] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptPaidSettledCaseSummary_Excel] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPaidSettledCaseSummary_Excel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPaidSettledCaseSummary_Excel] TO [power_user]
GO
