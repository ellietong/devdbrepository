SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Old] (@StateID int, @StartDate datetime, @EndDate datetime, @Matter int)
AS

Set NoCount ON


--Declare @StateID int
--Declare @startdate date
--Declare @endDate Date
----Set @StateID = 44
--Set @startdate = '1/1/1900'
--Set @endDate = '6/30/2010'
--Declare @matter int
--set @matter = 1


-- Find Max ID Values

Declare @MaxStateID int

--Define Max State ID
Select @MaxStateID = MAX(stateid) from tblState


-- Find Max ID Values
Declare @MaxResponsibleDefenseCounselID int


--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int, State nvarchar(150), YearFiled datetime,statusdate datetime, MesoCount int, LungCancerCount int, OtherCancerCount int, 
NonMaligCount int,  UnknownCount int, UnknownCount1 int, SilicosisCount int, NoDiseaseCount int)
Insert into @DiseaseCount

Select Matter, C.MatterID, FullNameState as state,  
case when serviceDate is null then  '1/1/1900' else convert(datetime,('1/1/'+convert(nvarchar,Year(serviceDate))))end as YearFiled,
coalesce(settlementdate,statusdate,'1/1/1900'),
--Meso
Case	when(Coalesce(DDefenseMeso.HighLevelDiseaseID,DPlaintiffMeso.HighLevelDiseaseID))=1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when(Coalesce(DDefenseLC.HighLevelDiseaseID,DPlaintiffLC.HighLevelDiseaseID))=2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when(Coalesce(DDefenseOC.HighLevelDiseaseID,DPlaintiffOC.HighLevelDiseaseID))=3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when(Coalesce(DDefenseNM.HighLevelDiseaseID,DPlaintiffNM.HighLevelDiseaseID))=4 Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when(Coalesce(DDefenseUnk.HighLevelDiseaseID,DPlaintiffUnk.HighLevelDiseaseID))=5 Then 1
Else 0 End AS UnknownCount,
--No Disease
Case	when(Coalesce(DDefenseND.HighLevelDiseaseID,DPlaintiffND.HighLevelDiseaseID))=6 Then 1
Else 0 End AS NoDiseaseCount,
--Silicosis
Case	when(Coalesce(DDefenseSC.HighLevelDiseaseID,DPlaintiffSC.HighLevelDiseaseID))=7 Then 1
Else 0 End AS SilicosisCount,
--Null
Case when (Coalesce(DDefenseUnk.HighLevelDiseaseID,DPlaintiffUnk.HighLevelDiseaseID))is NULL Then 1
Else 0 End AS UnknownCount1


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
--Meso
Left Join tblDisease DDefenseMeso on C.DefenseDiseaseID=DDefenseMeso.DiseaseID
Left Join tblDisease DPlaintiffMeso on C.PlaintiffDiseaseID=DPlaintiffMeso.DiseaseID
--Lung Cancer
Left Join tblDisease DDefenseLC on C.DefenseDiseaseID=DDefenseLC.DiseaseID
Left Join tblDisease DPlaintiffLC on C.PlaintiffDiseaseID=DPlaintiffLC.DiseaseID
--Other Cancer
Left Join tblDisease DDefenseOC on C.DefenseDiseaseID=DDefenseOC.DiseaseID
Left Join tblDisease DPlaintiffOC on C.PlaintiffDiseaseID=DPlaintiffOC.DiseaseID
--Non-Malig
Left Join tblDisease DDefenseNM on C.DefenseDiseaseID=DDefenseNM.DiseaseID
Left Join tblDisease DPlaintiffNM on C.PlaintiffDiseaseID=DPlaintiffNM.DiseaseID
--Silicosis
Left Join tblDisease DDefenseSC on C.DefenseDiseaseID=DDefenseSC.DiseaseID
Left Join tblDisease DPlaintiffSC on C.PlaintiffDiseaseID=DPlaintiffSC.DiseaseID
--No Disease
Left Join tblDisease DDefenseND on C.DefenseDiseaseID=DDefenseND.DiseaseID
Left Join tblDisease DPlaintiffND on C.PlaintiffDiseaseID=DPlaintiffND.DiseaseID
--Unknown
Left Join tblDisease DDefenseUnk on C.DefenseDiseaseID=DDefenseUnk.DiseaseID
Left Join tblDisease DPlaintiffUnk on C.PlaintiffDiseaseID=DPlaintiffUnk.DiseaseID

Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblState S on L.StateID=S.StateID
left join tblClaimantSettlement cs on cs.ClaimLawsuitID = CL.ClaimLawsuitID


Where  s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) 

and IsPrimaryForClaim = 1
	AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
	AND ((ClaimLawsuitStatusID = 22 and coalesce(servicedate, filedate, '1/1/1900')<=@endDate) OR (ClaimLawsuitStatusID <> 22 and coalesce(settlementdate,statusdate,'1/1/1900') > @EndDate))
	
Order By State, Year(serviceDate)

Select Matter, MatterID, YEAR(@EndDate) AS CloseYear, State,  Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, Sum(OtherCancerCount) as OtherCancerCount,yearfiled,@StartDate as StartDate, @EndDate as EndDate,
	Sum(NonMaligCount+SilicosisCount) as NonMaligCount, Sum(UnknownCount+UnknownCount1+NoDiseaseCount) as UnknownCount
from @DiseaseCount
where yearfiled between @StartDate and @EndDate  
Group By Matter, MatterID, State, yearfiled
ORDER BY Matter, State, YearFiled


 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Old] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Old] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Old] TO [power_user]
GO
