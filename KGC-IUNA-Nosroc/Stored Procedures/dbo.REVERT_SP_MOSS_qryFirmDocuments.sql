SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[REVERT_SP_MOSS_qryFirmDocuments](@emailaddress nvarchar (1000)=null)
AS
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'dmccoy@harveykruse.com'
--Declare and set the temporary field used to check the user's firm based on their login email address
DECLARE @firmid int
SET @firmid = (SELECT DISTINCT FirmID FROM  vtblPerson WHERE Email = @emailaddress)

IF (@firmid IN (38, 79) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11) = 'kcicllc.com'))
BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, f.Firm, Accepted
	FROM dbo.tblDocuments AS d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		INNER JOIN tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		INNER JOIN vtblFirm AS f ON d.FirmID = f.FirmID
		LEFT JOIN tblClaim As c ON c.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40)
	ORDER BY Accepted ASC, DateCreated DESC
END ELSE BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, f.Firm, Accepted
	FROM dbo.tblDocuments AS d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		INNER JOIN tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		INNER JOIN vtblFirm AS f ON d.FirmID = f.FirmID
		INNER JOIN tblClaim As c ON c.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40) AND cdt.DocumentGroupID <> 4
		AND @firmid = d.FirmID
	ORDER BY Accepted ASC, DateCreated DESC
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryFirmDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryFirmDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryFirmDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryFirmDocuments] TO [power_user]
GO
