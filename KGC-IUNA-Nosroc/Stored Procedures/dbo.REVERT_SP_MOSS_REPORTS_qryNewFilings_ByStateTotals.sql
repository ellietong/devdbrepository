SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilings_ByStateTotals] (@StartDate datetime, @EndDate datetime,@MatterID int)

AS
SET NOCOUNT ON

--Declare @MatterID int
--set @MatterID = null
--declare @startdate datetime
--set @startdate = '1/1/2010'
--declare @enddate datetime
--set @enddate = '9/30/2010'


----Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @DiseaseCount Table (ClaimID int, Matter nvarchar(100), MatterID int, YearFiled nvarchar(50), MoYearFiled nvarchar(50), MonthID int, MesoCount int, LungCancerCount int, 
OtherCancerCount int, NonMaligCount int,  UnknownCount int, unknowncount1 int, [state] nvarchar(100), stateid int)
Insert into @DiseaseCount

Select C.ClaimID, Matter, C.MatterID, Coalesce(year(servicedate), year(fileDate), year(statusdate)) as YearFiled, 
coalesce(DateName(month,servicedate), DateName(month,filedate), DateName(month,statusdate)) as MonthFiled,
coalesce(month(servicedate), month(filedate), month(statusdate)) as MonthID,
--Meso
Case	when COALESCE(DMeso.HighLevelDiseaseID, 0) = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when COALESCE(DLC.HighLevelDiseaseID, 0) = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when COALESCE(DOC.HighLevelDiseaseID, 0) = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when COALESCE(DNM.HighLevelDiseaseID, 0) IN (4, 7) Then 1
Else 0 End as NonMaligCount,
--Silicosis
--Case	when COALESCE(DNM.HighLevelDiseaseID, 0) = 7 Then 1
--Else 0 End as Silicosis,
--Unknown
Case	when COALESCE(DUnk.HighLevelDiseaseID, 0) in (5,6) Then 1
Else 0 End AS UnknownCount,
Case	when COALESCE(DUnk.HighLevelDiseaseID, 0) is NULL Then 1
Else 0 End AS UnknownCount1, FullNameState,
l.StateID


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (Select CD.ClaimDiseaseID,CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
LEFT JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID
--Meso
Left Join tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
Left Join tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
Left Join tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
Left Join tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Unknown
Left Join tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID

Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
left JOIN tblState s ON l.StateID = s.StateID
left join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = cl.ClaimLawsuitID

Where   IsPrimaryForClaim = 1 and C.MatterID >= COALESCE(@MatterID,0) AND C.MatterID <= COALESCE(NULLIF(@MatterID,0),@maxMatterID) and 
	processeddate between COALESCE(@StartDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) and COALESCE(@EndDate, GETDATE())  AND ClaimLawsuitStatusID = 22
	
	AND COALESCE(cpi.jobsiteinfo, 0) = 0
Order By COALESCE(YEAR(serviceDate), YEAR(fileDate), YEAR(StatusDate))


Select Matter, MatterID, COUNT(ClaimID) AS NewFilings, @StartDate as StartDate, @EndDate as EndDate,
Sum(MesoCount) as MesoFilings, Sum(LungCancerCount) as LCFilings, Sum(OtherCancerCount) as OCFilings,
Sum(NonMaligCount) as NMFILINGS,  Sum(UnknownCount+unknowncount1) as UnknownFilings, [state], stateid,
SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + Unknowncount1) AS TotalCount
from @DiseaseCount DC
Group By Matter, MatterID, [state], stateid
ORDER BY Matter

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilings_ByStateTotals] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilings_ByStateTotals] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilings_ByStateTotals] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilings_ByStateTotals] TO [power_user]
GO
