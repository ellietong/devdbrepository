SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[qryrptARBySubscriptionCompanyByPolicy] 
(@LimitTypeID int = NULL, @SubscriberCompanyID INT = NULL, @Solvency varchar(50) = NULL, @InsuranceProgramID INT = NULL, @AllocationID INT = NULL)
As

SET NOCOUNT ON

--DECLARE @SubscriberCompanyID INT
--SET @SubscriberCompanyID = 0
--DECLARE @Solvency varchar(50)
--SET @Solvency = null
--DECLARE @AllocationID INT
--SET @AllocationID = 150
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID = 0
--DECLARE @LimitTypeID INT
--SET @LimitTypeID = 3

DECLARE @maxInsuranceProgramID int
DECLARE @maxSubscriberCompanyID int
DECLARE @maxAllocationID int
DECLARE @maxLimitTypeID int

SELECT @maxInsuranceProgramID = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxSubscriberCompanyID = MAX(SubscriberID) FROM vSubscription
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy

SELECT subs.SubscriberID, subs.SubscriptionCompany, subs.Percentage, pinfo.PolicyHolder, MAX(ar.DateTime) AS [DateTime], pinfo.CarrierGroupName, pinfo.CarrierName, pinfo.InsuranceProgram, 
	SUM(ar.PV * subs.Percentage) AS pvAmt, SUM(ar.Allocation * subs.Percentage) AS AllocationAmt, SUM(ar.SIRAllocated * subs.Percentage) AS SIRAMt, a.AllocationID, a.AllocationDescription, 
	pinfo.PolicyNum, pinfo.AttachmentPoint, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit, ar.PStartSplit, ar.PEndSplit, ar.AnnualPeriodSplit, pinfo.PolicyID, pinfo.Layer, 
	subs.SubscriptionCompanySolvency, pinfo.InsuranceProgramID
FROM vPolicy pinfo INNER JOIN vSubscription subs ON pinfo.PolicyID = subs.PolicyID
	INNER JOIN vAllocationResults ar ON pinfo.PolicydateID = ar.PolicydateID
	LEFT JOIN vAllocation a ON a.AllocationID = ar.AllocationID
WHERE pinfo.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) AND
	subs.SubscriberID >= COALESCE(@SubscriberCompanyID, 0) AND subs.SubscriberID <= COALESCE(NULLIF(@SubscriberCompanyID, 0), @maxSubscriberCompanyID) AND
	ar.AllocationID >= COALESCE(@AllocationID, 0) AND ar.AllocationID <= COALESCE(NULLIF(@AllocationID, 0), @maxAllocationID) AND 
	subs.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%')
	AND pinfo.LimitTypeID >= COALESCE(@LimitTypeID, 0) AND pinfo.LimitTypeID <= COALESCE(NULLIF(@LimitTypeID, 0), @maxLimitTypeID)
GROUP BY subs.SubscriberID, subs.SubscriptionCompany, subs.Percentage, pinfo.PolicyHolder,  pinfo.CarrierGroupName, pinfo.CarrierName, pinfo.InsuranceProgram, a.AllocationID, a.AllocationDescription, pinfo.PolicyNum, 
	pinfo.AttachmentPoint, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit, ar.PStartSplit, ar.PEndSplit, ar.AnnualPeriodSplit, pinfo.PolicyID, pinfo.Layer, subs.SubscriptionCompanySolvency, 
	pinfo.InsuranceProgramID
ORDER BY pinfo.CarrierName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriptionCompanyByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriptionCompanyByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriptionCompanyByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriptionCompanyByPolicy] TO [power_user]
GO
