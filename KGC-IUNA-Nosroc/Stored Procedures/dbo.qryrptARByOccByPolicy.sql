SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByOccByPolicy]
(@allocationID int, @CarrierID INT, @Solvency varchar (50)= NULL, @CarrierGroupID int, @InsuranceProgramID INT , @OccurrenceTypeIDs nvarchar(100) = NULL, @limittypeid int)
AS
SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 150
--DECLARE @CarrierID int
--SET @CarrierID = 0
--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = 0
--DECLARE @Solvency varchar (50)
--SET @Solvency = null
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID = 0
--DECLARE @OccurrenceTypeIDs nvarchar(100)
--SET @OccurrenceTypeIDs = null
--DECLARE @limittypeid int
--SET @limittypeid = 3

---------------------------------------------------------------------------------------
-- 	DECLARE ALL VARIABLES
---------------------------------------------------------------------------------------
DECLARE @maxAllocationID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @maxInsuranceProgramID int
DECLARE @strSQL nvarchar (4000)
DECLARE @prmDef nvarchar (1000)
DECLARE @tmpSolvency nvarchar (50)
DECLARE @strOccurrences nvarchar (4000)
DECLARE @carrierGroupInfo nvarchar (50)
DECLARE @carrierInfo nvarchar (50)
DECLARE @maxOccurrenceTypeID int
DECLARE @index int
---------------------------------------------------------------------------------------
--	Set Variables to initial values
---------------------------------------------------------------------------------------
SET @carrierGroupInfo = ''
SET @strSQL = ''
SET @strOccurrences = ''
SET @tmpSolvency = ''
SET @prmDef = ''
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxInsuranceProgramID = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxOccurrenceTypeID = MAX(OccurrenceTypeID) FROM vOccurrence
---------------------------------------------------------------------------------------
--	Update Null Strings Set above that aren't resolved in
--		WHERE clause (1)solvency, 2)Occurrence Types
---------------------------------------------------------------------------------------
--	1	Update Solvency Info
IF @Solvency = '<<ALL>>' OR @Solvency IS NULL OR @Solvency = ''
	SET @tmpSolvency = '%'
ELSE 
	SET @tmpSolvency = @Solvency
----------------------------------
--	2	Setup the Occurrence String.
--		Test if @OccurrenceTypeIDs is null, if so, build a string of ALL occurrence types
IF (LTRIM(RTRIM(@OccurrenceTypeIDs)) <> '' AND @OccurrenceTypeIDs IS NOT NULL)  
	SET @strOccurrences = @OccurrenceTypeIDs  
ELSE 
BEGIN
	SET @strOccurrences = '1, '	
	SET @index = 2
	
	WHILE @index <= @maxOccurrenceTypeID
	BEGIN
		SET @strOccurrences = @strOccurrences + CONVERT(nvarchar, @index) + ', '
		SET @index = @index + 1
	END
	SET @strOccurrences = LEFT(@strOccurrences, LEN(@strOccurrences) - 1)
END
---------------------------------------------------------------------------------------
-- 	Build the zOccurrencesConsideredInAllocation Table 
---------------------------------------------------------------------------------------
SELECT OccurrenceID, SUM(Cost1+Cost2) AS 'DefenseConsidered', SUM(Cost3+Cost4+Cost5+Cost6+Cost7+Cost8+Cost9+Cost10+Cost11+Cost12) AS 'CostsConsidered', @allocationid AS AllocationID
INTO #tmpOccurrencesConsideredinAllocation
FROM vOccurrence
WHERE OccurrenceID NOT IN
	(SELECT aoe.OccurrenceID 
	FROM vAllocation a INNER JOIN tblAllocationOccurrenceExclude aoe ON a.AllocationID = aoe.AllocationID 
	WHERE a.AllocationID =@allocationid) AND (OccurrenceTypeID IN 
		(SELECT aoti.OccurrenceTypeID 
		FROM vAllocation a INNER JOIN tblAllocationOccurrenceTypeInclude aoti ON a.AllocationID = aoti.AllocationID 
		WHERE a.AllocationID = @allocationid
		)
	)
GROUP BY OccurrenceID
---------------------------------------------------------------------------------------
-- 	Build the SQL String
---------------------------------------------------------------------------------------
SET @StrSQL = N'SELECT pinfo.PolicyID, pinfo.PolicyNum, pinfo.InsuranceProgram, pinfo.InsuranceProgramID, o.OccurrenceTypeID, o.OccurrenceName, o.OccurrenceID, o.OccurrenceGroupID, o.TriggerStart1, 
	o.OccurrenceGroup, o.ExpectedPayDate, pinfo.CarrierID, pinfo.CarrierSolvency, pinfo.CarrierName, pinfo.CarrierGroupID, pinfo.CarrierGroupName, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END AS CarrierGroupInfo,
	CASE WHEN pinfo.CarrierName IS NULL THEN pinfo.CarrierGroupName ELSE pinfo.CarrierName END AS CarrierInfo, ar.TriggerStartDateUsed, ar.DateTime, ar.Allocation, 
	CONVERT(money, ar.PV) AS PV, ar.SIRAllocated, ar.PStartSplit, ar.PVRate, ar.DateToPresentValue, ar.PEndSplit, ar.AnnualPeriodSplit, ar.TriggerEndDateUsed, a.AllocationID, a.AllocationDescription, 
	tmp.CostsConsidered, tmp.DefenseConsidered, pinfo.AttachmentPoint, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit, pinfo.Layer, YieldCurveDate as PVRateDate
FROM vPolicy pinfo INNER JOIN vAllocationResults ar ON pinfo.PolicydateID = ar.PolicydateID
	INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID
	INNER JOIN vOccurrence o ON o.OccurrenceID = ar.OccurrenceID
	INNER JOIN #tmpOccurrencesConsideredinAllocation tmp ON (a.AllocationID = tmp.AllocationID AND o.OccurrenceID = tmp.OccurrenceID) 
	LEFT JOIN tblYieldCurve yc on yc.YieldCurveID = ar.YieldCurveID 
WHERE a.AllocationID >= COALESCE('+ CONVERT (nvarchar, @allocationID) +', 0) AND 
	a.AllocationID <= COALESCE(NULLIF(' + CONVERT (nvarchar, @allocationID) + ', 0), ' + CONVERT (nvarchar, @maxAllocationID) + ' ) AND
	pinfo.CarrierID >= COALESCE('+ CONVERT (nvarchar, @CarrierID) +', 0) AND
	pinfo.CarrierID <= COALESCE(NULLIF(' + CONVERT (nvarchar, @CarrierID) + ', 0), ' + CONVERT (nvarchar, @maxCarrierID) + ' ) AND
	pinfo.CarrierSolvency LIKE @SolvencyPrm AND '
IF @CarrierGroupID <> 0
	SET @strSQL = @strSQL + 'pinfo.CarrierGroupID = ' + CONVERT(nvarchar, @CarrierGroupID) + ' AND '
SET @strSQL = @strSQL + '
	pinfo.InsuranceProgramID >= COALESCE('+ CONVERT (nvarchar, @InsuranceProgramID) +', 0) AND
	pinfo.InsuranceProgramID <= COALESCE(NULLIF(' + CONVERT (nvarchar, @InsuranceProgramID) + ', 0), ' + CONVERT (nvarchar, @maxInsuranceProgramID) + ' ) AND
	o.OccurrenceTypeID IN (' + CONVERT (nvarchar (4000),@strOccurrences) + ') AND 
	pinfo.limittypeid = ' + CONVERT (nvarchar, @limittypeid) + '
GROUP BY pinfo.PolicyID, pinfo.PolicyNum, pinfo.InsuranceProgram, pinfo.InsuranceProgramID, o.ExpectedPayDate, o.OccurrenceTypeID, o.OccurrenceName, o.OccurrenceID, o.OccurrenceGroupID, o.TriggerStart1, 
	o.OccurrenceGroup, pinfo.CarrierID, pinfo.CarrierSolvency, pinfo.CarrierName, pinfo.CarrierGroupID, pinfo.CarrierGroupName, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END,
	CASE WHEN pinfo.CarrierName IS NULL then pinfo.CarrierGroupName ELSE pinfo.CarrierName END, ar.TriggerStartDateUsed, ar.DateTime, ar.Allocation, CONVERT(money, ar.PV), ar.SIRAllocated, 
	ar.PStartSplit, ar.PVRate, ar.Datetopresentvalue, ar.PEndSplit, ar.AnnualPeriodSplit, ar.TriggerEndDateUsed, a.AllocationID, a.AllocationDescription, tmp.CostsConsidered, tmp.DefenseConsidered,
	pinfo.AttachmentPoint, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit, pinfo.Layer, YieldCurveDate'
---------------------------------------------------------------------------------------
--	Declare the Parameters that will be passed to the created SQL string.
---------------------------------------------------------------------------------------
SET @prmDef = N'@SolvencyPrm nvarchar (50)'
---------------------------------------------------------------------------------------
--	Run/Execute the SQL Statement Created
---------------------------------------------------------------------------------------
EXEC sp_executeSQL @strSQL, @prmDef, @SolvencyPrm = @tmpSolvency
DROP TABLE #tmpOccurrencesConsideredinAllocation
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccByPolicy] TO [power_user]
GO
