SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryLawsuitCompletionList] 
(@PrefixText nvarchar(100), @Emailaddress nvarchar(100))
AS
BEGIN

--8/9/12 - CAS added no filters criteria to the query.
--DECLARE @emailaddress nvarchar(250) = 'dburmeister@harveykruse.com'
--DECLARE @PrefixText nvarchar(100) = 'CV'

SET NOCOUNT ON

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)


DECLARE @NoFilters bit
SET @NoFilters = (SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress)

IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') or @NoFilters = 1 
BEGIN
	SELECT DISTINCT l.LawsuitID, DocketNumber + ' (' + CaseCaption + ')' AS CaseCaption
	FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimDisease cd ON c.ClaimID	= cd.ClaimID
	INNER JOIN tblMatter m ON c.MatterID = m.MatterID
	WHERE DocketNumber LIKE COALESCE('%' + @PrefixText, '') + '%'
	ORDER BY DocketNumber + ' (' + CaseCaption + ')' 

END
ELSE
BEGIN
	SELECT DISTINCT l.LawsuitID, DocketNumber + ' (' + CaseCaption + ')' AS CaseCaption
	FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimDisease cd ON c.ClaimID	= cd.ClaimID
	INNER JOIN tblMatter m ON c.MatterID = m.MatterID
	INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID 
	WHERE DocketNumber LIKE COALESCE('%' + @PrefixText, '') + '%'
	AND dj.DefenseCounselAddressID = @FirmAddressID
	ORDER BY DocketNumber + ' (' + CaseCaption + ')'


END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [power_user]
GO
