SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryrptClaimantMedicareQC]
(@ClaimantPersonalInfoID bigint)
As
Set NoCount On

--DECLARE @Claimantpersonalinfoid bigint
--set @Claimantpersonalinfoid = 739


SELECT  Distinct cpi.ClaimantPersonalInfoID, HealthInsuranceClaimNumber, SocialSecurityNumber,cpi.LastName,cpi.FirstName,cpi.MiddleName, GenderID, BirthDate, EarliestExposureDate, LatestExposureDate,laws.State,

	PlaintiffCounselAddress1,
	d.ICD9,dhld.highleveldisease,	pc.PlaintiffCounsel,PrimaryLocalPlaintiffCounsel,PlaintiffCounselTaxIDNum, 
	PlaintiffCounselAddress2,PlaintiffCounselCity,pc.State,PlaintiffCounselZip,PlaintiffCounselPhone, PlaintiffCounselExtension, 
	SettlementDate, SettlementAmount,MedicareRelationshipID, crp.ClaimantRelatedPartyID, RPSocialSecurityNumber,RPTIN,crp.LastName, crp.FirstName, crp.MiddleName,	
	RPAddress1, Address1,RPAddress2, Address2,
	RPCity, cpi.City,RPZip, Zip, RPMailZip, MailZip, RPPhone, Phone, RPExtension
	
	FROM tblClaimantPersonalInfo cpi
	inner join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
	left join tblClaimDisease cd on cd.ClaimID = c.ClaimID
	left join tblDisease d on d.DiseaseID = cd.DiseaseID
	left join tblHighLevelDisease dhld on dhld.HighLevelDiseaseID = d.HighLevelDiseaseID

	left join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblDefendantCompany dc on dc.DefendantCompanyID = a.DefendantCompanyID
	left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	left join tblState laws on laws.StateID = l.StateID
	left join tblClaimantRelatedParty crp on crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	
	left join tblLegalCapacity lc on lc.LegalCapacityID = crp.LegalCapacityID
	left join tblRelationship r on r.RelationshipID = crp.RelationshipID
	left join tblClaimantEmployment ce on ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblClaimantProduct cp on cp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblPlaintiffCounselAddress pc on pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID

	
	Where
	cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantMedicareQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantMedicareQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantMedicareQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantMedicareQC] TO [power_user]
GO
