SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[qryrptExternalContactsNoticeQC_2]
--(@carrierid int = null)
as
set nocount on

DECLARE @CarrierID int
SET @CarrierID=0

Declare @MAXcarrierid int
SELECT @MAXcarrierid = MAX(carrierid) from tblCarrier

SELECT  case when carriergroupname = 'American International Group, Inc.' then 'Chartis Insurance' 
else
carriergroupname end as carriergroupname, cg.CarrierGroupID, cc.CarrierID, car.CarrierName, c.*, ct.ContactType, FullNameState as 'State', CurrentContact, DateAdded 
FROM dbo.tblCarrierContact cc 
inner join tblCarrier car on car.CarrierID = cc.CarrierID
inner join tblCarrierGroup cg on cg.CarrierGroupID = car.CarrierGroupID
INNER JOIN dbo.tblContact c ON cc.ContactID = c.ContactID 
LEFT JOIN dbo.tblContactType ct ON ct.ContactTypeID = c.ContactTypeID
left join tblState s on s.StateID = c.StateID

Where car.carrierid >= COALESCE(@carrierID, 0) AND car.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID)and 
InternalContact<>1 and CurrentContact = 1 
group by carriergroupname, c.AddressLine1, c.AddressLine2, c.AddressLine3, c.BadAddress, c.CareOf, c.City, c.Company, c.ContactClaimTypeID, c.ContactID,
      c.ContactTypeID, c.Email, c.Fax, c.FirstName, c.LastName, c.MiddleName, c.Phone, c.Position, c.StateID, c.Title, c.Zip, 
      ct.ContactType, FullNameState, cg.CarrierGroupID, cc.CarrierID, car.CarrierName,CurrentContact, DateAdded, c.InternalContact
order by ContactID


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExternalContactsNoticeQC_2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExternalContactsNoticeQC_2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExternalContactsNoticeQC_2] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExternalContactsNoticeQC_2] TO [power_user]
GO
