SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Procedure [dbo].[qryrptByPolicyByOriginalOccurrence] (@allocationID int)
As
Set NoCount On

Select OriginalOccurrenceID, PolicyDateID, Sum(Cost1), Sum(Cost3), Sum(AllocationDefense), Sum(AllocationIndemnity)
From tblAllocationResults ar
Inner Join tblOccurrence o on ar.OccurrenceID = o.OccurrenceID
Where AllocationID = @allocationID
Group By OriginalOccurrenceID, PolicyDateID
Having Sum(AllocationDefense) <0 or Sum(AllocationIndemnity) < 0 

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptByPolicyByOriginalOccurrence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptByPolicyByOriginalOccurrence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptByPolicyByOriginalOccurrence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptByPolicyByOriginalOccurrence] TO [power_user]
GO
