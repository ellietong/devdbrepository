SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptExpenseClaimListing] (@TotalAllocatedExpense money, @serviceDate datetime, @header nvarchar(100), @Matter int, @ExpenseCurveID int)

AS
Set NoCount On

-- Declare @TotalAllocatedExpense money
-- Set @TotalAllocatedExpense = 5000
 
-- Declare @ServiceDate datetime
-- Set @ServiceDate = '9/30/2010'
--Declare @header nvarchar(100)
--Set @header = 'test'
--Declare @Matter int
--Set @Matter = 1
--DECLARE @ExpenseCurveID int
--SET @ExpenseCurveID = 1
--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

SELECT  FirstExposureDate, FirstDiagnosisDate, ecl.ExpensePercentage, @TotalAllocatedExpense AS TotalAllocatedExpense, (@TotalAllocatedExpense*ExpensePercentage) AS AllocatedExpense,  @header as [header], m.MatterID, Matter, @serviceDate AS [Servicedate]
FROM tblExpenseClaimsListing ecl
INNER JOIN tblExpenseClaimsCurve ecc ON ecl.ExpenseCurveID = ecc.ExpenseCurveID
INNER JOIN tblMatter m ON ecl.MatterID = m.MatterID
WHERE m.MatterID >= COALESCE(@Matter,0) AND m.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
	AND ecc.ExpenseCurveID = COALESCE(@ExpenseCurveID, 1)
ORDER BY FirstExposureDate DESC
--Return

--GO
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExpenseClaimListing] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExpenseClaimListing] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExpenseClaimListing] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExpenseClaimListing] TO [power_user]
GO
