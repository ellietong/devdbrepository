SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RELATED_PARTY_qryClaimantRelatedPartyInfo]
@ClaimantPersonalInfoID bigint=null, @emailaddress nvarchar(250)
AS
BEGIN
 
 
Declare @UserFirmState table (DefenseCounselID bigint, StateID int)
Insert into @UserFirmState
 
Select dca.DefenseCounselID, StateID
From dbo.tblDefenseCounselState AS dcs 
left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid --and dcar.stateID = dcs.StateID 
LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
Where (ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) OR
dca.DefenseCounselID in (SELECT f.FirmID FROM vtblPerson p inner join vtblFirm f on p.FirmID=f.FirmID WHERE Email = @emailaddress))
 
--IF RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress) IN (132, 133) OR ((Select PersonID FROM vtblPerson WHERE Email = @emailaddress) IN (1376,1377,1378,1383,1384,1389,1392))
--BEGIN
Select ClaimantRelatedPartyID, 
	ClaimantPersonalInfoID,  
	COALESCE(LastName + ', ', '') + CASE WHEN FirstName IS NULL 
    THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
	LastName, FirstName, MiddleName,
	Relationship, 
	LC.LegalCapacity,
	Case When crp.RPSocialSecurityNumber IS NULL Then '' ELSE 'XXX-XX-' + RIGHT(crp.RPSocialSecurityNumber, 4) END AS SSN,
	COALESCE(crp.RPSocialSecurityNumber, '') AS FullSSN, CASE WHEN crp.RelationshipID = 0 THEN NULL ELSE crp.RelationshipID END AS RelationshipID, CASE WHEN crp.LegalCapacityID = 0 THEN NULL ELSE crp.LegalCapacityID END AS LegalCapacityID,
	coalesce(nullif(coalesce(RPAddress1 + CHAR(13) + CHAR(10), '') + coalesce(RPAddress2 + CHAR(13) + CHAR(10),'') + 
						coalesce(RPCity + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,RPZip),'') + Coalesce('-'+convert(varchar,RPmailzip),''),''),'') as CompleteAddress, RPCity,
					  s.StateID, RPZip, RPMailZip, RPAddress1, RPAddress2, RPPhone, RPExtension
From tblClaimantRelatedParty crp 
LEFT JOIN tblRelationship r on r.RelationshipID=crp.RelationshipID
LEFT JOIN tblLegalCapacity LC ON LC.LegalCapacityID = crp.LegalCapacityID
LEFT JOIN tblState s ON crp.StateID = s.StateID
WHERE ClaimantPersonalInfoID=@ClaimantPersonalInfoID
--END
--ELSE
--BEGIN
--Select DISTINCT ClaimantRelatedPartyID, 
--	crp.ClaimantPersonalInfoID,  
--	LastName + ', ' + CASE WHEN FirstName IS NULL 
--    THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
--	LastName, FirstName, MiddleName,
--	Relationship, 
--	LC.LegalCapacity,
--	Case When crp.RPSocialSecurityNumber IS NULL Then '' ELSE 'XXX-XX-' + RIGHT(crp.RPSocialSecurityNumber, 4) END AS SSN,
--	Case When crp.RPSocialSecurityNumber IS NULL Then '' ELSE crp.RPSocialSecurityNumber END AS FullSSN, crp.RelationshipID, crp.LegalCapacityID
--From tblClaimantRelatedParty crp 
--LEFT JOIN tblRelationship r on r.RelationshipID=crp.RelationshipID
--LEFT JOIN tblLegalCapacity LC ON LC.LegalCapacityID = crp.LegalCapacityID
--LEFT JOIN tblClaim c ON crp.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
--                      LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
--                      LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
--INNER JOIN tblDefenseCounselState dcs on l.DefenseCounselAddressID=dcs.DefenseCounselAddressID and l.StateID=dcs.StateID
--INNER JOIN tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCounselAddressID=dca.DefenseCounselAddressID
--INNER JOIN @UserFirmState u on dca.DefenseCounselID = u.DefenseCounselID and l.StateID = u.StateID
--WHERE crp.ClaimantPersonalInfoID=@ClaimantPersonalInfoID
 
--END
 
 
 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryClaimantRelatedPartyInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryClaimantRelatedPartyInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryClaimantRelatedPartyInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryClaimantRelatedPartyInfo] TO [power_user]
GO
