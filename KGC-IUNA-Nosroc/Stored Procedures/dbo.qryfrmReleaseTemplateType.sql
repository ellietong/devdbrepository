SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[qryfrmReleaseTemplateType] 
as

Set NoCount On

Select ReleaseTemplateTypeID, ReleaseTemplateType
From tblReleaseTemplateType
Order By ReleaseTemplateType

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmReleaseTemplateType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmReleaseTemplateType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmReleaseTemplateType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmReleaseTemplateType] TO [power_user]
GO
