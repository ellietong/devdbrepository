SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryDelSettlementInfoDocument]
(@SettleDocID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM tblSettleDocs
	WHERE SettleDocID = @SettleDocID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDelSettlementInfoDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDelSettlementInfoDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDelSettlementInfoDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDelSettlementInfoDocument] TO [power_user]
GO
