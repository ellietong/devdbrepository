SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  Procedure [dbo].[qryrptOccurrenceWGroups]
(@CorporateProfileID INT = NULL, @InClaim bit) 
As

SET NOCOUNT ON

--DECLARE @CorporateProfileID int
--SET @CorporateProfileID = 0
--DECLARE @InClaim bit
--SET @InClaim = 1

DECLARE @maxCorporateProfileID int
SELECT @maxCorporateProfileID = MAX(CorporateHistoryID) FROM vOccurrence

IF @InClaim IS NULL
BEGIN
	SELECT @InClaim = 0

	SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, 
		CorporateHistoryID, OccurrenceTypeID, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, 
		TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, 
		Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceGroup, OccurrenceType, CorporateHistoryProfile 
	FROM vOccurrence
	WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
		InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), 1)
END 
ELSE
	SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, 
		CorporateHistoryID, OccurrenceTypeID, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, 
		TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, 
		Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceGroup, OccurrenceType, CorporateHistoryProfile 
	FROM vOccurrence
	WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
		InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), @InClaim)

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrenceWGroups] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOccurrenceWGroups] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrenceWGroups] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOccurrenceWGroups] TO [power_user]
GO
