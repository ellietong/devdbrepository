SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_qryIsDocumentLocked]
(@DocumentID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--Test vars
	--DECLARE @DocumentID int = 4228753
	
	DECLARE @retVal int = null

	--Check for settlements
	SET @retVal = (SELECT COUNT(paymentSettlementID) FROM tblPaymentSettlement ps
	LEFT JOIN tblClaimantSettlement cs ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
	LEFT JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblMultiClaimantDocument mcd ON c.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	WHERE
	mcd.DocumentID = @DocumentID)
	
	
	--Add in dismissals
	SET @retVal = COALESCE(@retVal, 0) + (SELECT COUNT(c.ClaimID) FROM tblClaim c
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID	 = l.LawsuitID
	INNER JOIN tblMultiClaimantDocument mcd ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	WHERE COALESCE(c.IsCurrentClaim,0) = 1 AND
			COALESCE(IsPrimaryForClaim, 0) = 1 AND
			ClaimLawsuitStatusID IN (15,16,17) AND
			mcd.DocumentID = @DocumentID)
			
	--return the total # found
	SELECT @retVal
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [power_user]
GO
