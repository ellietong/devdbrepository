SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryClaimantSearch] 
(@ClaimantName nvarchar(max), @SSN nvarchar(20), @StateID int, @PlaintiffCounselAddressID int, @ClaimantID int,
  @MatterID int, @DocketNumber nvarchar(250)=null, @ClaimLawsuitStatusID int)
AS
SET NOCOUNT ON
--DECLARE @ClaimantName nvarchar(max)
--SET @ClaimantName = null
--DECLARE @MatterID int
--SET @MatterID = null
--DECLARE @SSN nvarchar(20)
--SET @SSN = null
--DECLARE @StateID int
--SET @StateID =null
--DECLARE @PlaintiffCounselAddressID int
--SET @PlaintiffCounselAddressID = null
--DECLARE @ClaimantID bigint
--SET @ClaimantID = null
--DECLARE @NavLegacyID bigint
--SET @NavLegacyID = null
--DECLARE @IsMDL int
--SET @IsMDL = null
--DECLARE @DocketNumber nvarchar(250)
--SET @DocketNumber = 'DVP'


DECLARE @MaxState int
SET @MaxState = (SELECT MAX(StateID) FROM tblState)
DECLARE @MaxPC int
SET @MaxPC = (SELECT MAX(PlaintiffCounselAddressID) FROM tblPlaintiffCounselAddress)
DECLARE @MaxClaimantID bigint
SET @MaxClaimantID = (SELECT MAX(ClaimantPersonalInfoID) FROM tblClaimantPersonalInfo)
DECLARE @MaxMatter int
SET @MaxMatter = (SELECT MAX(MatterID) FROM tblMatter)
--DECLARE @MaxNavLegacyID bigint
--SET @MaxNavLegacyID = (SELECT MAX(NavLegacyID) FROM tblClaimantPersonalInfo)
DECLARE @MaxMDL int
SET @MaxMDL = 1
DECLARE @MaxClaimLawsuitStatusID bigint
SET @MaxClaimLawsuitStatusID = (SELECT MAX(ClaimLawsuitStatusID) FROM tblClaimLawsuitStatus)

--If user is Dana or KCIC
BEGIN
	SELECT DISTINCT
						  CPI.ClaimantPersonalInfoID AS ClaimantID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
						  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, 
	                      
						  CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
						  COALESCE(CONVERT(VARCHAR(10), CPI.BirthDate, 101), '') AS BirthDate, Matter, PlaintiffCounsel + ' (' + pca.PlaintiffCounselCity + ')' As PlaintiffCounsel,
						  s.[State], ClaimLawsuitStatus, DocketNumber
	                      
						  --ROW_NUMBER() OVER (ORDER BY cpi.LastName, cpi.Firstname) AS ResultSetRowNumber
	                        
	FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
						  dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
						  --LEFT JOIN tblClaimantDocketNumbers cdn ON CPI.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
						  LEFT JOIN tblClaimLawsuit CL ON c.ClaimID = cl.ClaimID
						  LEFT JOIN tblLawsuit L ON cl.LawsuitID = L.LawsuitID
						  LEFT JOIN tblMatter M ON c.MatterID = M.MatterID
						  LEFT JOIN tblPlaintiffCounselAddress PCA ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
						  LEFT JOIN tblState s ON l.StateID = s.StateID
						  LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
						  LEFT JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	WHERE l.StateID BETWEEN COALESCE(@StateID, 0) AND COALESCE(@StateID, @MaxState)
			AND  l.PlaintiffCounselAddressID >= COALESCE(@PlaintiffCounselAddressID, 0) AND l.PlaintiffCounselAddressID <= COALESCE(@PlaintiffCounselAddressID, @MaxPC)
			AND cpi.ClaimantPersonalInfoID >= COALESCE(@ClaimantID, 0) AND cpi.ClaimantPersonalInfoID <= COALESCE(@ClaimantID, @MaxClaimantID)
			AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(@MatterID, @MaxMatter)
			AND ((CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
				THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' end) like '%'+coalesce(@ClaimantName, '')+'%')
			AND Coalesce(socialsecurityNumber, '') like '%'+coalesce(@SSN, '')+'%'
			--AND cpi.NavLegacyID <= COALESCE(@NavLegacyID, @MaxNavLegacyID) AND cpi.NavLegacyID >= COALESCE(@NavLegacyID, 0)
			AND DocketNumber LIKE '%' + COALESCE(@DocketNumber, '') + '%'
			AND c.IsCurrentClaim = 1
			AND clst.ClaimLawsuitStatusID >= COALESCE(@ClaimLawsuitStatusID, 0) AND clst.ClaimLawsuitStatusID <= COALESCE(@ClaimLawsuitStatusID, @MaxClaimLawsuitStatusID)
	ORDER BY CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
						  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantSearch] TO [power_user]
GO
