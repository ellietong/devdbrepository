SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimLawsuitInfoQC] (@ClaimantPersonalInfoID int)
as

--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 91861

Select --Claim Info
	ClaimantPersonalInfoID,
	IsCurrentClaim,
	CLaimSequence,
	Matter,
	--ClaimLawsuit Info
	c.ClaimID, 
	cl.ClaimLawsuitID,
	cl.LawsuitID,
	IndividualDocketNumber,
	IsPrimaryForClaim,
	ClaimLawsuitStatus,
	clst.StatusDate,
	clst.StatusNotes,
	ActiveTrialDate

From tblClaim c
	Left Join tblClaimLawsuit cl on c.claimID = cl.claimID
	Left Join tblClaimSequence cs on cs.claimsequenceID = c.claimsequenceID
	left join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
	Left Join tblClaimLawsuitStatus cls on cls.claimlawsuitstatusid = clst.claimlawsuitstatusID
	Left Join tblMatter m on c.MatterID = m.MatterID
Where c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and coalesce(clst.IsPrimaryStatus,0) = 1
Order By IsPrimaryForClaim

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitInfoQC] TO [power_user]
GO
