SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCheckGeneration]

( @Matter int, @FirmID int, @CheckNo int)
--@FundsRequestStartDate datetime, @FundsRequestEndDate datetime,
As

SET NOCOUNT ON

--DECLARE @CheckNo nvarchar(50)
--SET @CheckNo = 2029

--Declare @FundsRequestStartDate datetime
--Declare @FundsRequestEndDate datetime
--DECLARE @Matter int
--DECLARE @FirmID int
--SET @Matter = 1
--Set @FirmID = 0
--SET @FundsRequestStartDate = '12/1/2009'
--SET @FundsRequestEndDate = '12/1/2009'

DECLARE @maxMatterID int
DECLARE @maxFirmID int
Declare @MaxCheckNo int
SELECT @maxMatterID = MAX(matterid) from tblMatter
SELECT @maxFirmID = MAX(firmID) from tblDefenseInvoiceFirm
SELECT @MaxCheckNo = MAX(CheckNo) from tblChecks


SELECT Matter, m.MatterID,
C.FirmID,Firm, PayTo, Period, SubmitDate, InvoiceTotal, FirmAddress, InvoiceNumber, FundsRequestDate,
C.CheckNo, C.AmountDesc, C.checkAmount, C.CheckDate,c.CommentLine
FROM	tblDefenseInvoice DI
 JOIN	tblDefenseInvoiceFirm DIF ON DI.FirmID = DIF.FirmID
JOIN	tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
Join tblChecks C on C.CheckNo = DI.CheckNumber and c.MatterID = DI.MatterID
JOIN  tblMatter M on C.MatterID=M.MatterID

Where 
--FundsRequestDate Between @FundsRequestStartDate and @FundsRequestEndDate
c.MatterID >= COALESCE(@Matter,0) AND c.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
AND DI.FirmID >= COALESCE(@FirmID,0) AND DI.FirmID <= COALESCE(NULLIF(@FirmID,0),@MaxFirmID)
--AND di.CheckNumber>= COALESCE(@checkno,0) AND di.CheckNumber <= COALESCE(NULLIF(@CheckNo,0),@MaxCheckNo)
AND c.CheckNo>= COALESCE(@checkno,0) AND C.CheckNo <= COALESCE(NULLIF(@CheckNo,0),@MaxCheckNo)

ORDER BY SubmitDate

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGeneration] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckGeneration] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptCheckGeneration] TO [CheckWriter_User]
GRANT CONTROL ON  [dbo].[qryrptCheckGeneration] TO [CheckWriter_User]
GRANT TAKE OWNERSHIP ON  [dbo].[qryrptCheckGeneration] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGeneration] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckGeneration] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGeneration] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckGeneration] TO [power_user]
GO
