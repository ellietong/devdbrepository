SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptDismissedCasesByJurisdiction_Old]
(@Matter int,@BeginDate datetime, @EndDate datetime)

As
Set NoCount On



 --Declare @matter int
 --Set @matter = 2
 --Declare @BeginDate datetime
 --Declare @EndDate datetime

 --Set @BeginDate = '1/1/1900'
 --Set @EndDate = '6/30/2010'




--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int, claimlawsuitstatus nvarchar (500),
	MoYearDismissed datetime, YearDismissed datetime,MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int, unknowncount1 int)
Insert into @DiseaseCount

Select Matter, C.MatterID,  claimlawsuitstatus,
 convert(datetime,(convert(nvarchar,Month(statusDate))+'/1/'+ convert(nvarchar,Year(StatusDate)))) as MoYearDismissed,
  convert(nvarchar,Year(StatusDate)) as YearDismissed,
--Meso
Case	when(Coalesce(DDefenseMeso.HighLevelDiseaseID,DPlaintiffMeso.HighLevelDiseaseID))=1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when(Coalesce(DDefenseLC.HighLevelDiseaseID,DPlaintiffLC.HighLevelDiseaseID))=2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when(Coalesce(DDefenseOC.HighLevelDiseaseID,DPlaintiffOC.HighLevelDiseaseID))=3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when(Coalesce(DDefenseNM.HighLevelDiseaseID,DPlaintiffNM.HighLevelDiseaseID))in (4,7) Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when(Coalesce(DDefenseUnk.HighLevelDiseaseID,DPlaintiffUnk.HighLevelDiseaseID))in (5,6) Then 1
Else 0 End AS UnknownCount,
Case when (Coalesce(DDefenseUnk.HighLevelDiseaseID,DPlaintiffUnk.HighLevelDiseaseID))is NULL Then 1
Else 0 End AS UnknownCount1


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
--Meso
Left Join tblDisease DDefenseMeso on C.DefenseDiseaseID=DDefenseMeso.DiseaseID
Left Join tblDisease DPlaintiffMeso on C.PlaintiffDiseaseID=DPlaintiffMeso.DiseaseID
--Lung Cancer
Left Join tblDisease DDefenseLC on C.DefenseDiseaseID=DDefenseLC.DiseaseID
Left Join tblDisease DPlaintiffLC on C.PlaintiffDiseaseID=DPlaintiffLC.DiseaseID
--Other Cancer
Left Join tblDisease DDefenseOC on C.DefenseDiseaseID=DDefenseOC.DiseaseID
Left Join tblDisease DPlaintiffOC on C.PlaintiffDiseaseID=DPlaintiffOC.DiseaseID
--Non-Malig
Left Join tblDisease DDefenseNM on C.DefenseDiseaseID=DDefenseNM.DiseaseID
Left Join tblDisease DPlaintiffNM on C.PlaintiffDiseaseID=DPlaintiffNM.DiseaseID
--Unknown
Left Join tblDisease DDefenseUnk on C.DefenseDiseaseID=DDefenseUnk.DiseaseID
Left Join tblDisease DPlaintiffUnk on C.PlaintiffDiseaseID=DPlaintiffUnk.DiseaseID

Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
left join tblClaimantSettlement cs on cs.ClaimLawsuitID = CL.ClaimLawsuitID
inner join tblClaimLawsuitStatus css on css.ClaimLawsuitStatusID = CL.ClaimLawsuitStatusID


Where  isprimaryforclaim=1   
and css.ClaimLawsuitStatusID in (15,16,27) 
AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) and statusDate between @BeginDate and @EndDate 

Order By  Year(StatusDate)


Select Matter, MatterID,  Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, Sum(OtherCancerCount) as OtherCancerCount,
	Sum(NonMaligCount) as NonMaligCount, Sum(UnknownCount+unknowncount1) as UnknownCount, @BeginDate as StartDate, @EndDate as EndDate,
MoYearDismissed, 		YearDismissed 
from @DiseaseCount
Group By Matter, MatterID,  YearDismissed,MoYearDismissed



Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissedCasesByJurisdiction_Old] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDismissedCasesByJurisdiction_Old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissedCasesByJurisdiction_Old] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDismissedCasesByJurisdiction_Old] TO [power_user]
GO
