SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptSubscriptionCoverageByPolicyByCompany]
(@InsuranceProgramID int = null, @SubscriberID int = null, @Solvency varchar(50) = null, @limitTypeID int = Null)
As
SET NOCOUNT ON

--DECLARE @InsuranceProgramID int 
--DECLARE @Solvency varchar(50) 
--DECLARE @SubscriberID int 
--DECLARE @limitTypeID int
--
--SET @InsuranceProgramID = 0
--SET @Solvency = null
--SET @SubscriberID = 0
--SET @limitTypeID = 0

DECLARE @maxInsuranceProgramID int
DECLARE @maxSubscriberID int
DECLARE @maxLimitTypeID int
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxInsuranceProgramID = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxSubscriberID = MAX(SubscriberID) FROM vSubscription

SELECT MIN(pinfo.PStartDate) AS MinOfPStartDate, MAX(pinfo.PEndDate) AS MaxOfPEndDate, MIN(pinfo.AnnualPeriod) AS MinOfAnnualPeriod, pinfo.InsuranceProgram, subs.SubscriptionCompany, 
	subs.SubscriptionCompanySolvency, subs.Percentage, (subs.Percentage * pinfo.PerOccLimit) AS SubscriberPerOcc, pinfo.CarrierID, pinfo.InsuranceProgramID, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
	SIR, AggregateLimit, Layer, pinfo.PolicyNum, pinfo.PolicyID, subs.SubscriberID, subs.SubscriptionCoverageID, AgencyName, UWReference, PolicyType, AcctNum
FROM vPolicy pinfo 
	INNER JOIN vSubscription subs ON pinfo.PolicyID = subs.PolicyID
WHERE pinfo.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) AND
	pinfo.limitTypeID >= COALESCE(@limitTypeID, 0) AND pinfo.limitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND 
	subs.SubscriberID >= COALESCE(@SubscriberID, 0) AND subs.SubscriberID <= COALESCE(NULLIF(@SubscriberID, 0), @maxSubscriberID) AND
	subs.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%') 
	and DoNotReport <>1
GROUP BY pinfo.InsuranceProgram, subs.SubscriptionCompany, subs.SubscriptionCompanySolvency, subs.Percentage, (subs.Percentage * pinfo.PerOccLimit), pinfo.CarrierID, pinfo.InsuranceProgramID, 
	AttachmentPoint, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, Layer, PolicyNum, pinfo.PolicyID, subs.SubscriberID, subs.SubscriptionCoverageID, AgencyName, UWReference, PolicyType, AcctNum

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompany] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompany] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompany] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompany] TO [power_user]
GO
