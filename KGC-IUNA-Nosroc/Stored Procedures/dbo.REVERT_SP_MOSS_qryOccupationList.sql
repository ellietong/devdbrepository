SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryOccupationList]
AS
BEGIN
	SELECT OccupationID, Occupation FROM tblOccupation
	UNION
	SELECT NULL, ''
	ORDER BY Occupation
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryOccupationList] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryOccupationList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryOccupationList] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryOccupationList] TO [power_user]
GO
