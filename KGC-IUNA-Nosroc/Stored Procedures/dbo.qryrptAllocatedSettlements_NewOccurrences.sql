SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptAllocatedSettlements_NewOccurrences] (@SettlementDateBegin datetime, @SettlementDateEnd datetime, @header nvarchar(100), @Matter int)

As
SET NOCOUNT ON

-- Declare @SettlementDateBegin datetime
-- Declare @SettlementDateEnd datetime
-- Set @SettlementDateBegin='3/1/2010'
--Set @SettlementDateEnd='12/31/2010'
--Declare @header nvarchar(100)
--set @header = '4th Quarter Billing 2010'
--Declare @Matter int
--Set @Matter = null

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter



declare @ClaimsAlreadyBilled table (claimid bigint)
insert into @ClaimsAlreadyBilled
select claimid
from tblOccurrence
group by ClaimID


Select 	C.ClaimID, LastName, FirstName, Firstname+' '+LastName as ClaimantName, 'XXX-XX-'+ Right([SocialSecurityNumber],4) as SocialSecurityNumber, State,
	HighLevelDisease, FileDate, PaidDate, SettlementDate, EarliestExposureDate,
	DiseaseDiagnosisDate as DiagnosisDate, SettlementAmount,
	Coalesce(@SettlementDateBegin,'1/1/1900') as PaidDateBegin,
	Coalesce(@SettlementDateEnd,convert(varchar(10),getdate(),101)) as PaidDateEnd,
	@header as header, CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
From tblClaimantPersonalInfo CPI
Left Join tblClaimantEmployment CE on CPI.ClaimantPersonalInfoID=CE.ClaimantPersonalInfoID
Left Join tblClaim C on CPI.ClaimantPersonalInfoID=C.ClaimantPersonalInfoID
Left Join (select * from tblClaimDisease where PrimaryDisease = 1) cd on cd.ClaimID = c.ClaimID
Left Join tblDisease D on cd.DiseaseID=D.DiseaseID
Left Join tblHighLevelDisease HLD on D.HighLevelDiseaseID=HLD.HighLevelDiseaseID
Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Inner Join tblClaimantSettlement CS on CL.ClaimLawsuitID=CS.CLaimLawsuitID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblState S on L.StateID=S.StateID
Left Join tblMatter M on C.MatterID = m.MatterID
left JOIN @ClaimsAlreadyBilled cab on cab.claimid = c.ClaimID
Where IsPrimary=1 
and SettlementDate>= Coalesce(@SettlementDateBegin,'1/1/1900') 
AND SettlementDate<= Coalesce(@SettlementDateEnd,convert(varchar(10),getdate(),101))
and c.MatterID >= COALESCE(@Matter,0) AND c.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
and SettlementAmount <> 0
AND cab.claimid is null
AND COALESCE(JobSiteInfo, 0) <> 1
GROUP BY C.ClaimID, LastName, FirstName, Firstname+' '+LastName, 'XXX-XX-'+ Right([SocialSecurityNumber],4), State, HighLevelDisease,
	FileDate, PaidDate, SettlementDate, EarliestExposureDate,
	DiseaseDiagnosisDate, 
	SettlementAmount
Order By Matter, PaidDate

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocatedSettlements_NewOccurrences] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocatedSettlements_NewOccurrences] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocatedSettlements_NewOccurrences] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocatedSettlements_NewOccurrences] TO [power_user]
GO
