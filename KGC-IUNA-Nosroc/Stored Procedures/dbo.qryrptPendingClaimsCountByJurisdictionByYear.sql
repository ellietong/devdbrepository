SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryrptPendingClaimsCountByJurisdictionByYear] (@StateID int, @StartDate datetime, @EndDate datetime, @Matter int)
AS

Set NoCount ON


--DECLARE @StartDate datetime
--DECLARE @EndDate datetime
--DECLARE @StateID int
--DECLARE @Matter int

--SET @StartDate = '1/1/1900'
--SET @EndDate = '6/30/2011'
--SET @StateID = null
--SET @Matter = 1


-- Find Max ID Values

Declare @MaxStateID int

--Define Max State ID
Select @MaxStateID = MAX(stateid) from tblState

-- Find Max ID Values
Declare @MaxResponsibleDefenseCounselID int

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int, State nvarchar(150), YearFiled datetime,statusdate datetime, MesoCount int, LungCancerCount int, OtherCancerCount int, 
NonMaligCount int,  UnknownCount int, UnknownCount1 int, SilicosisCount int, NoDiseaseCount int)
Insert into @DiseaseCount

Select Matter, C.MatterID, FullNameState as state,  
case when serviceDate is null then  '1/1/1900' else convert(datetime,('1/1/'+convert(nvarchar,Year(serviceDate))))end as YearFiled,
coalesce(settlementdate,statusdate,'1/1/1900'),
--Meso
Case	when DMeso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when DLC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when DOC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when DNM.HighLevelDiseaseID = 4 Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when DUnk.HighLevelDiseaseID = 5 Then 1
Else 0 End AS UnknownCount,
--No Disease
Case	when DND.HighLevelDiseaseID = 6 Then 1
Else 0 End AS NoDiseaseCount,
--Silicosis
Case	when DSC.HighLevelDiseaseID = 7 Then 1
Else 0 End AS SilicosisCount,
--Null
Case	when DUnk.HighLevelDiseaseID is NULL Then 1
Else 0 End AS UnknownCount1


From tblClaimantPersonalInfo CPI
LEFT JOIN tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
--Meso
LEFT JOIN tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
LEFT JOIN tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
LEFT JOIN tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
LEFT JOIN tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Silicosis
LEFT JOIN tblDisease DSC on CD.DiseaseID=DSC.DiseaseID

--No Disease
LEFT JOIN tblDisease DND on CD.DiseaseID=DND.DiseaseID

--Unknown
LEFT JOIN tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID


LEFT JOIN tblClaimLawsuit CL		on C.ClaimID=CL.ClaimID
LEFT JOIN tblLawsuit L				on CL.LawsuitID=L.LawsuitID
LEFT JOIN tblState S				on L.StateID=S.StateID
LEFT JOIN tblClaimantSettlement cs	on cs.ClaimLawsuitID = CL.ClaimLawsuitID
LEFT JOIN tblClaimSequence csq		on c.ClaimSequenceID = csq.ClaimSequenceID
LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=CL.ClaimLawsuitID
LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
left join tblClaimLawsuitStatusGroup clsg on clsg.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID

Where  s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) and IsPrimaryForClaim = 1
	AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
	
	AND coalesce(clst.isprimarystatus,0)=1
	AND ((clsg.StatusGroupID=1 and coalesce(servicedate,filedate,'1/1/1900')<=@EndDate) or (StatusGroupID in (2,3) and ProcessedDate>@EndDate))
	AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
	AND COALESCE(cpi.jobsiteinfo, 0) = 0
	
Order By State, Year(serviceDate)

Select Matter, MatterID, YEAR(@EndDate) AS CloseYear, State,  Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, Sum(OtherCancerCount) as OtherCancerCount,yearfiled,@StartDate as StartDate, @EndDate as EndDate,
	Sum(NonMaligCount+SilicosisCount) as NonMaligCount, Sum(UnknownCount+UnknownCount1+NoDiseaseCount) as UnknownCount
from @DiseaseCount
where yearfiled between @StartDate and @EndDate  
Group By Matter, MatterID, State, yearfiled
ORDER BY Matter, State, YearFiled


 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear] TO [power_user]
GO
