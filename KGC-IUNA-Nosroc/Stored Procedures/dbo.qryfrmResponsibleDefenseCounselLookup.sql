SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmResponsibleDefenseCounselLookup]
as


Select 0 as ResponsibleDefenseCounselID, '<<All Defense Counsels>>' as ResponsibleDefenseCounsel
Union All
Select ResponsibleDefenseCounselID, DefenseCounsel as ResponsibleDefenseCounsel
From tblDefenseCounselState dcs Inner Join tblDefenseCounsel d on dcs.ResponsibleDefenseCOunselID = d.DefenseCounselID
Where IsActiveCounsel = 1
Group By ResponsibleDefenseCounselID, DefenseCounsel 
Order By ResponsibleDefenseCounsel

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmResponsibleDefenseCounselLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmResponsibleDefenseCounselLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmResponsibleDefenseCounselLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmResponsibleDefenseCounselLookup] TO [power_user]
GO
