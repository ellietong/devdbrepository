SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryClaimantProduct]
(@ClaimantEmploymentID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimantEmploymentID, StartDate, EndDate, cp.ProductID, ProductBrand, ProductManufacturer, pn.ProductName, pt.ProductType, ClaimantProductID,
	pt.ProductType + ' - ' + pm.ProductManufacturer + ' - ' + ProductBrand + ' - ' + pn.ProductName  AS Product
	
	FROM tblClaimantProduct cp
	Inner Join tblProduct p on p.ProductID = cp.ProductID
	Inner Join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
	Inner Join tblProductManufacturer pm on pm.ProductManufacturerID = p.ProductManufacturerID
	Inner Join tblProductName pn on pn.ProductNameID = p.ProductNameID
	INner Join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
	WHERE ClaimantEmploymentID = @ClaimantEmploymentID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantProduct] TO [power_user]
GO
