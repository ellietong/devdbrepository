SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptOutstandingSettlements]  (@MatterID int, @SettlementDate datetime)

As

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 1
--DECLARE @SettlementDate datetime
--SET @SettlementDate = '1/1/2009'
DECLARE @MaxMatterID int
--SELECT @MaxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @SettlementGroupPaidUnpaid TABLE (SettlementGroupID int, Paid money, Total money)
INSERT INTO @SettlementGroupPaidUnpaid
SELECT SettlementGroupID, 	SUM( CASE WHEN ps.CheckID IS NULL THEN 0 ELSE SettlementAmount END) As Paid,
	SUM(SettlementAmount) as Total
	FROM tblClaimantSettlement cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	left join tblCheck k on k.CheckID =  ps.CheckID
	
WHERE COALESCE(c.IsCurrentClaim,0) = 1 AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
and (FundingSourceID is null or FundingSourceID in (1,2))	
GROUP BY SettlementGroupID, COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupCount TABLE (SettlementGroupID int, ClaimCount nvarchar(50))
INSERT INTO @SettlementGroupCount	
SELECT SettlementGroupID, COUNT(SettlementDate) as ClaimCount
	FROM tblClaimantSettlement 	
cs inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
left join tblCheck k on k.CheckID =  ps.CheckID
	where  COALESCE(c.IsCurrentClaim,0) = 1 AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
and (FundingSourceID is null or FundingSourceID in (1,2))	
GROUP BY SettlementGroupID,  COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID

DECLARE @SettlementGroupPaidCount TABLE (SettlementGroupID int, PaidCount nvarchar(50))
INSERT INTO @SettlementGroupPaidCount	
	SELECT SettlementGroupID, COUNT(ps.CheckID) as PaidCount
	FROM tblClaimantSettlement cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
left join tblCheck k on k.CheckID =  ps.CheckID
	WHERE SettlementGroupID IN	
(SELECT SettlementGroupID 
	FROM tblClaimantSettlement cs
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID 
WHERE ps.CheckID IS NOT NULL GROUP BY SettlementGroupID)
	and (FundingSourceID is null or FundingSourceID in (1,2))and COALESCE(c.IsCurrentClaim,0) = 1
	 AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
GROUP BY SettlementGroupID, COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID
DECLARE @SettlementGroupMaxDate TABLE (SettlementGroupID int, MaxSettlementDate datetime)
INSERT INTO @SettlementGroupMaxDate
	SELECT SettlementGroupID, MAX(SettlementDate) as MaxSettlementDate
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID 
	ORDER BY MaxSettlementDate
SELECT pl.PlaintiffCounsel, cs.SettlementDate, COALESCE(CONVERT(nvarchar,SettlementDueDate,101), 'N/A') as DatePayable, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, ps.CheckID, spd.ReleaseSignedDate, FullyDocumented, c.MatterID, Matter, cl.LawsuitID, CONVERT(nvarchar,k.SourceDate,101) as 'paiddate',
	@SettlementDate AS SettlementDateAsOf,  DocketNumbers, COALESCE(sgpc.PaidCount,0) AS PaidCount, sgc.ClaimCount, MaxSettlementDate,
	CASE WHEN (sd.SettlementConfirmation>0  and sd.ClaimantIDed>0 and sd.DxMedicals>0 and sd.DODCert>0 and sd.EstatePapers> 0 and sd.ProductID>0 
	and sd.EmployExpoHist>0 and spd.MedicareFormRcvd>0)
	THEN 'Complete'
WHEN DeceasedDate IS NULL AND sd.SettlementConfirmation>0  and sd.ClaimantIDed>0 and sd.DxMedicals>0 and sd.ProductID>0 and sd.EmployExpoHist>0 and spd.MedicareFormRcvd>0
THEN 'Complete'
when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) <= '12/5/1980' and DeceasedDate IS NULL  AND sd.SettlementConfirmation>0  
and sd.ClaimantIDed>0 and sd.DxMedicals>0 and sd.ProductID>0 and sd.EmployExpoHist>0  
THEN 'Complete'
	  WHEN  dbo.fnLatestExposure(cpi.claimantpersonalinfoid) <= '12/5/1980' and sd.SettlementConfirmation>0  and sd.ClaimantIDed>0 
	  and sd.DxMedicals>0 and sd.DODCert>0 and sd.EstatePapers> 0 and sd.ProductID>0 
	and sd.EmployExpoHist>0  
	THEN 'Complete'
ELSE 'Documentation Needed' END AS SupportingDocumentation,
CASE WHEN spd.ReleaseRcvd = 1
	THEN coalesce( crr.ReleaseType,'Executed' )ELSE 'Release Missing'  END AS ReleaseExecution, Case when ps.CheckID is not null then cs.SettlementAmount else 0 end as Paid,
	CASE WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 1 THEN 'Unknown' WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 2 THEN 'Yes - ' + COALESCE(HealthInsuranceClaimNumber, 'HICN Not Provided')WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 3 THEN 'Non-Beneficiary' END AS HICN,
CASE WHEN spd.MedicareFormRcvd IS not null THEN 'Yes' ELSE 'No' END As MedicareForm, case when MedicareFullyDocumented = 1 then 'Yes' Else 'No' end as MedicareFullyDocumented,
	CONVERT(nvarchar(max), COALESCE(dbo.fnEarliestExposure(cpi.ClaimantPersonalInfoID),'N/A'),101) AS EarliestExposureDate,
	CASE WHEN COALESCE(DiseaseDiagnosisDate,'1/1/2053') <= COALESCE(FileDate,'1/1/2051') and COALESCE(DiseaseDiagnosisDate,'1/1/2053') <= COALESCE(DeceasedDate,'1/1/2052')
		 THEN CONVERT(nvarchar(max),DiseaseDiagnosisDate,101)
		 WHEN COALESCE(FileDate,'1/1/2053') <= COALESCE(DiseaseDiagnosisDate,'1/1/2051') and COALESCE(FileDate,'1/1/2053') <= COALESCE(DeceasedDate,'1/1/2052')
		 THEN CONVERT(nvarchar(max),FileDate,101)
		 WHEN COALESCE(DeceasedDate,'1/1/2053') <= COALESCE(DiseaseDiagnosisDate,'1/1/2051') and COALESCE(DeceasedDate,'1/1/2053') <= COALESCE(FileDate,'1/1/2052')
		 THEN CONVERT(nvarchar(max),DeceasedDate,101)
		 ELSE 'N/A' END AS ContinuousExposureDate,
CONVERT(nvarchar(max), COALESCE(dbo.fnLatestExposure(cpi.ClaimantPersonalInfoID),'N/A'),101) AS LastProductDate

FROM tblClaim c
	
	 JOIN tblClaimantPersonalInfo cpi	ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
	iNNER JOIN tblClaimLawsuit cl	ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INner JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pl	ON l.PlaintiffCounselAddressID = pl.PlaintiffCounselAddressID
LEFT join tblClaimLawsuitStatusTracking st on st.ClaimLawsuitID =cl.ClaimLawsuitID 	
INNER JOIN tblClaimLawsuitStatus cls	
	ON st.ClaimLawsuitstatusID = cls.ClaimLawsuitstatusID	
left join tblclaimlawsuitstatusgroup csg on csg.claimlawsuitstatusid = st.ClaimLawsuitStatusID
	LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblClaimantDocketNumbers cd	ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
LEFT join tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
LEFT  JOIN @SettlementGroupPaidUnpaid sgp	ON s.SettlementGroupID = sgp.SettlementGroupID
	LEFT  JOIN @SettlementGroupCount sgc	ON s.SettlementGroupID= sgc.SettlementGroupID
	LEFT  JOIN @SettlementGroupPaidCount sgpc	ON s.SettlementGroupID = sgpc.SettlementGroupID
	LEFT  JOIN @SettlementGroupMaxDate sgm	ON s.SettlementGroupID = sgm.SettlementGroupID
	LEFT JOIN  tblReleasetype crr	ON crr.ReleaseTypeID =spd.ReleaseTypeID
	LEFT JOIN tblClaimSequence csq	ON c.ClaimSequenceID = csq.ClaimSequenceID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	left join tblCheck k on k.CheckID =  ps.CheckID
	LEFT JOIN tblClaimDisease cdi				 ON cdi.ClaimID = c.ClaimID
WHERE MaxSettlementDate >= @SettlementDate
	AND COALESCE(IsCurrentClaim,0) = 1 and coalesce(st.isprimarystatus,0) = 1
	AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	AND COALESCE(PaidCount,0) <> COALESCE(ClaimCount,0)
	AND statusgroupid=2 
	AND COALESCE(cpi.jobsiteinfo, 0) = 0  and (FundingSourceID is null or FundingSourceID in (1,2))
	and SettlementAmount > 0
	and PrimaryDisease = 1
GROUP BY cs.SettlementDate, COALESCE(CONVERT(nvarchar,SettlementDueDate,101), 'N/A'), LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, ps.Checkid, spd.ReleaseSignedDate, FullyDocumented, c.MatterID, Matter, cl.LawsuitID,CONVERT(nvarchar,k.SourceDate,101),pl.PlaintiffCounsel,
	  DocketNumbers, sgpc.PaidCount, sgc.ClaimCount, MaxSettlementDate,spd.ReleaseRcvd,spd.MedicareFormRcvd,
CASE WHEN (sd.SettlementConfirmation>0  and sd.ClaimantIDed>0 and sd.DxMedicals>0 and sd.DODCert>0 and sd.EstatePapers> 0 and sd.ProductID>0 
	and sd.EmployExpoHist>0 and spd.MedicareFormRcvd>0)
	THEN 'Complete'
WHEN DeceasedDate IS NULL AND sd.SettlementConfirmation>0  and sd.ClaimantIDed>0 and sd.DxMedicals>0 and sd.ProductID>0 and sd.EmployExpoHist>0 and spd.MedicareFormRcvd>0
THEN 'Complete'
when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) <= '12/5/1980' and DeceasedDate IS NULL  AND sd.SettlementConfirmation>0  and sd.ClaimantIDed>0 and sd.DxMedicals>0 and sd.ProductID>0 and sd.EmployExpoHist>0  
THEN 'Complete'
	  WHEN  dbo.fnLatestExposure(cpi.claimantpersonalinfoid) <= '12/5/1980' and sd.SettlementConfirmation>0  and sd.ClaimantIDed>0 and sd.DxMedicals>0 and sd.DODCert>0 and sd.EstatePapers> 0 and sd.ProductID>0 
	and sd.EmployExpoHist>0  
	THEN 'Complete'
ELSE 'Documentation Needed' END,
	CASE WHEN spd.ReleaseRcvd = 1
THEN coalesce( crr.ReleaseType,'Executed' ) 
	ELSE 'Release Missing'  END
, CASE WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 1 THEN 'Unknown' WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 2 THEN 'Yes - ' + COALESCE(HealthInsuranceClaimNumber, 'HICN Not Provided')WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 3 THEN 'Non-Beneficiary' END,
CASE WHEN spd.MedicareFormRcvd IS not null THEN 'Yes' ELSE 'No' END, case when MedicareFullyDocumented = 1 then 'Yes' Else 'No' end,
	CONVERT(nvarchar(max),COALESCE(dbo.fnEarliestExposure(cpi.ClaimantPersonalInfoID),'N/A'),101), 
	CASE WHEN COALESCE(DiseaseDiagnosisDate,'1/1/2053') <= COALESCE(FileDate,'1/1/2051') and COALESCE(DiseaseDiagnosisDate,'1/1/2053') <= COALESCE(DeceasedDate,'1/1/2052')
		 THEN CONVERT(nvarchar(max),DiseaseDiagnosisDate,101)
		 WHEN COALESCE(FileDate,'1/1/2053') <= COALESCE(DiseaseDiagnosisDate,'1/1/2051') and COALESCE(FileDate,'1/1/2053') <= COALESCE(DeceasedDate,'1/1/2052')
		 THEN CONVERT(nvarchar(max),FileDate,101)
		 WHEN COALESCE(DeceasedDate,'1/1/2053') <= COALESCE(DiseaseDiagnosisDate,'1/1/2051') and COALESCE(DeceasedDate,'1/1/2053') <= COALESCE(FileDate,'1/1/2052')
		 THEN CONVERT(nvarchar(max),DeceasedDate,101)
		 ELSE 'N/A' END,
CONVERT(nvarchar(max), COALESCE(dbo.fnLatestExposure(cpi.ClaimantPersonalInfoID),'N/A'),101)
ORDER BY --MaxSettlementDate, SettlementGroup, SettlementDate, 
LastName
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlements] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlements] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlements] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlements] TO [power_user]
GO
