SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[qryrptCheckLog] 
As
Set NoCount On

Select c.SourceNumber, SourceDate, SourceAmount, Matter, ApprovedBy, ApprovedDate, IsCheckCleared, IsApproved, FundingSource, FundingType
From tblCheck C
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where ApprovedDate >= (GETDATE()-7)
Order by Matter, ApprovedDate, SourceAmount desc

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckLog] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckLog] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckLog] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckLog] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckLog] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckLog] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckLog] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckLog] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckLog] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckLog] TO [power_user]
GO
