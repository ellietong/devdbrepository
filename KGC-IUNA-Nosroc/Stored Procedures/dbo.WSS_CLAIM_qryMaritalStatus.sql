SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_CLAIM_qryMaritalStatus]
as

Select MaritalStatusID, MaritalStatus
From tblMaritalStatus
UNION
SELECT NULL, ''

UNION
SELECT 1, ''
Order By MaritalStatus
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryMaritalStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryMaritalStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryMaritalStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryMaritalStatus] TO [power_user]
GO
