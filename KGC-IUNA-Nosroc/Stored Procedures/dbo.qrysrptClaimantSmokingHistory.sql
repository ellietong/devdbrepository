SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysrptClaimantSmokingHistory]
(@ClaimantPersonalInfoID bigint)

As
Set NoCount On


Select IsSmoker, SmokerEnd as SmokingEnd, SmokerStart as SmokingStart, PacksPerDay, ClaimantPersonalInfoID
From tblClaimantPersonalInfo
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantSmokingHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantSmokingHistory] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantSmokingHistory] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantSmokingHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantSmokingHistory] TO [power_user]
GO
