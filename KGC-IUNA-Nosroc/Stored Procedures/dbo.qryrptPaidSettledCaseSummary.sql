SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 CREATE Proc [dbo].[qryrptPaidSettledCaseSummary] ( @paidDateStart  nvarchar(250),	@paidDateEnd  nvarchar(250), @Matter int) 

as

Set NoCount On

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


 --Declare @paidDateStart datetime
 --Declare @paidDateEnd datetime
 -- Set  @paidDateStart  = '1/1/1900'
 --Set @paidDateEnd = '9/30/2011'
 --declare @matter int
 --set @matter = 2
 


Declare @ResolvedSummary table (ClaimNumber int, 
							
				SettlementAmount money,
				SettlementAverage money,
				ResolvedDate datetime,
				Meso_Count int,
				Meso_SettlementAmount money,
				Meso_Avg money,
				LungCancer_Count int,
				LungCancer_SettlementAmount money,
				LungCancer_Avg money,
				OtherCancer_Count int,
				OtherCancer_SettlementAmount money,
				OtherCancer_Avg money,
				NonMalig_Count int,
				NonMalig_SettlementAmount money,
				NonMalig_Avg money,
				Other_Count int,
				Other_SettlementAmount money,

				NoDisease_Count int,
				NoDisease_SettlementAmount money,

				Null_Count int,
				Null_SettlementAmount money, 
				Matter nvarchar(100),
				MatterID int)


Insert Into @ResolvedSummary 
Select   count(c.ClaimID) as ClaimantNumber,
		coalesce(sum(SettlementAmount),0) as SettlementAmount,
	coalesce(sum(SettlementAmount),0)/count(c.ClaimID) as SettlementAverage, 
	convert(datetime,(convert(nvarchar,month(ck.sourcedate))+'/1/'+ convert(nvarchar,year(sourcedate)))) as resolvedDate,
	
sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) as Meso_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end) as Meso_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = (1) then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) end  as Meso_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) as LungCancer_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end) as LungCancer_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = (2) then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = (2) then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) end  as LungCancer_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) as OtherCancer_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end) as OtherCancer_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) end  as OtherCancer_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) as NonMalig_Count,	
	sum(Case When HLD.HighLevelDiseaseID in (4,7) then SettlementAmount else 0 end) as NonMalig_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID in (4,7)then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) end  as NonMalig_Avg,
			
	Sum(Case When HLD.HighLevelDiseaseID = 5 then 1 else 0 end) as Other_Count,	
	Sum(Case When HLD.HighLevelDiseaseID = 5 then SettlementAmount else 0 end) as Other_SettlementAmount,
			
	Sum(Case When HLD.HighLevelDiseaseID = 6 then 1 else 0 end) as NoDisease_Count,	
	Sum(Case When HLD.HighLevelDiseaseID = 6 then SettlementAmount else 0 end) as NoDisease_SettlementAmount,
			
	Sum(Case When HLD.HighLevelDiseaseID is null then 1 else 0 end) as Null_Count,				
	Sum(Case When HLD.HighLevelDiseaseID  is null then SettlementAmount else 0 end) as Null_SettlementAmount,
	Matter, M.MatterID
	
From tblClaimantPersonalInfo i 
	Left Join tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
	Left Join tblClaimLawsuit l on l.claimID = c.claimID
	Left Join tblLawsuit law on l.lawsuitid = law.lawsuitid
	Left Join tblCLaimantSettlement s on s.claimlawsuitID = l.claimlawsuitID
	Left Join tblDisease D on CD.DiseaseID = D.DiseaseID
	left join tblHighLevelDisease HLD on HLD.HighLevelDiseaseID=D.HighLevelDiseaseID
	left join tblMatter m on m.MatterID = c.MatterID
	LEFT JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID
	left join tblPaymentSettlement ps on ps.ClaimantSettlementID = s.ClaimantSettlementID
	left join tblCheck ck on ck.CheckID= ps.CheckID
	inner join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = l.ClaimLawsuitID
Where IsPrimaryForClaim = 1 and convert(datetime,(convert(nvarchar,month(SourceDate))+'/1/'+ convert(nvarchar,year(SourceDate))))>= 
@paidDateStart and convert(datetime,(convert(nvarchar,month(sourcedate))+'/1/'+ convert(nvarchar,year(sourcedate)))) <= @paidDateEnd
	AND ClaimLawsuitStatusID IN (23) and cst.IsPrimaryStatus = 1
	AND C.MatterID  >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
 

	AND COALESCE(i.jobsiteinfo, 0) = 0
Group By convert(datetime,(convert(nvarchar,month(SourceDate))+'/1/'+ convert(nvarchar,year(SourceDate)))), Matter, m.MatterID


Select coalesce(ClaimNumber,0) as ClaimNumber, 
	Coalesce(resolveddate, 0) as resolvedDate,
	YEAR(Coalesce(resolveddate, 0)) as resolvedYear,
	MONTH(Coalesce(resolveddate, 0)) as resolvedMonth,
	coalesce(SettlementAmount,0) as SettlementAmount,
	coalesce(SettlementAverage,0) as SettlementAverage,
	coalesce(Meso_Count,0) as Meso_Count,
	coalesce(Meso_SettlementAmount,0) as Meso_SettlementAmount,
	coalesce(Meso_Avg,0) as Meso_Avg,
	coalesce(LungCancer_Count,0) as LungCancer_Count,
	coalesce(LungCancer_SettlementAmount,0) as LungCancer_SettlementAmount,
	coalesce(LungCancer_Avg,0) as LungCancer_Avg,
	coalesce(OtherCancer_Count,0) as OtherCancer_Count,
	coalesce(OtherCancer_SettlementAmount,0) as OtherCancer_SettlementAmount,
	coalesce(OtherCancer_Avg,0) as OtherCancer_Avg,
	coalesce(NonMalig_Count,0)as NonMalig_Count,
	coalesce(NonMalig_SettlementAmount,0)as NonMalig_SettlementAmount,
	coalesce(NonMalig_Avg,0)as NonMalig_Avg,
	coalesce(Other_Count,0)+ coalesce(NoDisease_Count,0) + coalesce(Null_Count,0) as Other_Count,
	coalesce(Other_SettlementAmount,0)+ coalesce(NoDisease_SettlementAmount,0)+coalesce(Null_SettlementAmount,0) as Other_SettlementAmount,
 	@paidDatestart as startdate, @paidDateEnd as Enddate, Matter, MatterID
From @resolvedSummary 
Order By Coalesce(ResolvedDate,0)

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPaidSettledCaseSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPaidSettledCaseSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPaidSettledCaseSummary] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptPaidSettledCaseSummary] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPaidSettledCaseSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPaidSettledCaseSummary] TO [power_user]
GO
