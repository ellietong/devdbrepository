SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_DOCUMENT_PROFILE_qryGetFirmByEmail]
	-- Add the parameters for the stored procedure here
	@emailaddress nvarchar(1000)
AS
BEGIN
	
DECLARE @firmid int
SET @firmid = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)
	--DECLARE @emaildomain nvarchar(1000)
	--SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress), LEN(@emailaddress)-CHARINDEX('@', @emailaddress)+1)
	IF @firmid IN (79) OR RIGHT(@emailaddress, 11) = 'kcicllc.com' -- Removed FirmID 38 from IN statement to fix issues with document acceptance
	BEGIN
		SELECT DISTINCT FirmID, DefenseCounsel as Firm 
		FROM vtblPerson INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
		WHERE FirmID NOT IN (79)
	END
	ELSE
	BEGIN
		SELECT DISTINCT FirmID, DefenseCounsel as Firm FROM vtblPerson 
		INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
		WHERE @emailaddress = vtblPerson.Email
	END
		
	 
END

GO
