SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[REVERT_SP_MOSS_qryUpdSettleDocs] (@SettleDocID int, @DocumentID int, @DocCategoryID int, @PageNums nvarchar(100), @EmailAddress nvarchar(250))
as

--DECLARE @SettlePaymentID int
--SET @SettlePaymentID = 23
--DECLARE @EmailAddress nvarchar(100)
--set @EmailAddress = 'hubbardb@kcicllc.com' 
SET NOCOUNT ON
UPDATE tblSettleDocs
SET DocumentID = @DocumentID, DocCategoryID = @DocCategoryID, PageNums=@PageNums, SPUsername = @EmailAddress
WHERE SettleDocID = @SettleDocID
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdSettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdSettleDocs] TO [power_user]
GO
