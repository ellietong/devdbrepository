SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmPolicyLanguage]
(@policyID int = null)
AS

SET NOCOUNT ON

SELECT pl.*, dbo.fnProvisionOrder(ProvisionLabelID) as ORDERBY
FROM tblProvisionLanguage pl 
--INNER JOIN tblProvisionLabel plab ON pl.ProvisionLabelID = plab.ProvisionLabelID
WHERE PolicyID = @policyID
ORDER BY ORDERBY

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLanguage] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLanguage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLanguage] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLanguage] TO [power_user]
GO
