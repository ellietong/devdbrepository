SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  Proc [dbo].[qryrptLawsuitEntryQCSheet]
(@LawsuitID bigint)

As
Set NoCount On

--DECLARE @LawsuitID int
--SET @LawsuitID = 21151


Select LawsuitID, CaseCaption, DocketNumber, FileDate, Jurisdiction, s.State, CourtType, ServiceDate,
	ReceivedDate, DefenseCounsel as PrimaryDefenseCounsel, d.DefenseCounselCity, d.State as DefenseCounselState,PlaintiffCounsel as PrimaryLocalPlaintiffCounsel, PrimaryNationalPlaintiffCounsel,
	LeadPlaintiffFirstName, LeadPlaintiffLastName, PlaintiffCounselCity, PlaintiffCounselTaxIDNum
From tblLawsuit L
Left Join tblJurisdiction J on J.JurisdictionID=L.JurisdictionID
Left Join tblState S on S.StateID=L.StateID
Left Join tblCourtType CT on CT.CourtTypeID=L.CourtTypeID
Left Join tblDefenseCounseladdress d on d.DefenseCounselAddressID = l.DefenseCounselAddressID
left join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID 
Where LawsuitID = @LawsuitID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptLawsuitEntryQCSheet] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptLawsuitEntryQCSheet] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptLawsuitEntryQCSheet] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptLawsuitEntryQCSheet] TO [power_user]
GO
