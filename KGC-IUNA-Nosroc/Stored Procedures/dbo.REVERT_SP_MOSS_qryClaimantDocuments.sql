SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryClaimantDocuments]
(@ClaimantPersonalInfoID bigint = null)
AS

BEGIN

DECLARE @ClaimantPersonalInfoIDlocal bigint
SET @ClaimantPersonalInfoIDlocal = @claimantpersonalinfoid
SELECT  DISTINCT   d.DocumentID, d.SPDocId, d.DocumentTitle, 
CASE WHEN d.FirmID IS NULL THEN DocumentURL ELSE '/clients/iuna_nosroc/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/clients/iuna_nosroc/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), @ClaimantPersonalInfoIDlocal) END AS DocumentURL, dg.DocumentGroup, cdt.ClaimantDocumentType, 
                      f.Firm, d.AcceptedBy, d.DateAccepted, d.DateCreated, mcd.ClaimantPersonalInfoID,  DocumentURL as DirectDocumentURL, Accepted, CreatedBy
FROM tblMultiClaimantDocument mcd INNER JOIN         
	dbo.tblDocuments AS d ON mcd.DocumentID = d.documentID INNER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID INNER JOIN
                      dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID LEFT JOIN
                      dbo.vtblFirm AS f ON f.FirmID = d.FirmID
WHERE mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoIDlocal
AND dg.DocumentGroupID <> 2
ORDER BY DateCreated DESC
--The following will change for Dana since there are historical documents linked to multiple claimants (tblHistoricalMultiClaimantDocument will 
----be used
--UNION

--SELECT DISTINCT cd.ClaimantDocumentID, SPDocID, DocumentTitle, DocumentURL, dg.DocumentGroup, ClaimantDocumentType,
--				'Historical Document' as Firm, 'Historical' as AcceptedBy, '4/1/2010' as DateAccepted, '4/1/2010' as DateCreated, cd.ClaimantPersonalInfoID, 
--				DocumentURL as DirectDocumentURL, 1 AS Accepted
--FROM tblClaimantDocuments cd
--                      INNER JOIN dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID 
--                      INNER JOIN dbo.tblClaimantDocumentType AS cdt ON cd.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
--                      INNER JOIN dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID 
--WHERE cd.ClaimantPersonalInfoID = @ClaimantPersonalInfoIDlocal
--ORDER BY Accepted ASC, DateCreated DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantDocuments] TO [power_user]
GO
