SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAccountsReceivableActivity] (@BillDate datetime, @Matter int)
As
Set NoCount On

----+++++++++++++++++++++++++
--Declare @Billdate Datetime
--Declare @Matter int
--Set @Billdate = '11/15/2012'
--Set @Matter = NULL
----+++++++++++++++++++++++++

--Find Max Matter ID

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--As of DATE Accounts Receivable

Declare @Ledger1 Table (LedgerID int, CarrierID int, Allocation money, MatterID int)
Insert into @Ledger1
Select LedgerID, CarrierID, Allocation, MatterID
From tblLedger
Where BillDate <= @Billdate

Declare @Payment1 Table (LedgerID int, Payment money)
Insert into @Payment1
Select LedgerID, Sum(coalesce(Payment,0))
From tblPayment
Where PaymentDate <= @Billdate
Group By LedgerID

Declare @AR table (CarrierID int, AccountReceivable money, MatterID int)
Insert Into @AR
Select CarrierID, Sum(Coalesce(L.Allocation,0)-coalesce(P.payment,0)) AS AccountReceivable, MatterID
From @Ledger1 L
Left Join @Payment1 P on L.LedgerID=P.LedgerID
where MatterID >= COALESCE(@Matter,0) AND MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) 
Group By CarrierID, MatterID
HAVING Sum(Coalesce(L.Allocation,0)-coalesce(P.payment,0)) <> 0

--New Activity Accounts Receivable

Declare @Ledger2 Table (LedgerID int, CarrierID int, SubscriberID int, MatterID int, NewBillings money)
Insert into @Ledger2
Select LedgerID, CarrierID, SubscriberID, MatterID, Sum(coalesce(Allocation,0))
From tblLedger
Where BillDate > @Billdate
Group BY LedgerID, CarrierID, SubscriberID, MatterID

Declare @Payment2 Table (LedgerID int, NewPayments money)
Insert into @Payment2
Select LedgerID, Sum(coalesce(Payment,0))
From tblPayment P
Where PaymentDate > @BillDate
Group By LedgerID

Declare @PaymentDate Table (CarrierID int, PaymentDate datetime)
Insert into @PaymentDate
Select CarrierID, MAX(PaymentDate) as PaymentDate
From tblPayment P
Inner Join tblLedger L on L.LedgerID=P.LedgerID
where MatterID >= COALESCE(@Matter,0) AND MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) 
Group by CarrierID

Declare @NewARTemp table (CarrierID int, NewBillings money, NewPayments money, MatterID int, PaymentDate datetime)
Insert Into @NewARTemp

Select L.CarrierID, coalesce(NewBillings,0), coalesce(NewPayments,0), MatterID, PaymentDate
From @Ledger2 L
Left Join @Payment2 P on L.LedgerID=P.LedgerID
Left Join @PaymentDate PD on PD.CarrierID=L.CarrierID
where MatterID >= COALESCE(@Matter,0) AND MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) 
Group By L.CarrierID, MatterID, PaymentDate, NewBillings, NewPayments

UNION

Select L.CarrierID, 0, coalesce(NewPayments,0), MatterID, PaymentDate
From @Ledger1 L
Left Join @Payment2 P on L.LedgerID=P.LedgerID
Left Join @PaymentDate PD on PD.CarrierID=L.CarrierID
where MatterID >= COALESCE(@Matter,0) AND MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) 
Group By L.CarrierID, MatterID, PaymentDate, NewPayments

Declare @NewAR table (CarrierID int, NewBillings money, NewPayments money, MatterID int, PaymentDate datetime)
Insert Into @NewAR
SELECT CarrierID, SUM(coalesce(NewBillings,0)), SUM(coalesce(NewPayments,0)), MatterID, PaymentDate
FROM @NewARTemp
GROUP BY CarrierID, MatterID, PaymentDate


--Report Data

Declare @Final table (OrderID int, CarrierGrouping nvarchar(50), CarrierName nvarchar(2000), carrierid int, DatedAccountsReceivable money, NewBilledAmounts money, NewPayments money, MatterID int, PaymentDate datetime)
Insert Into @Final

Select 1 as OrderID, 'CarrierGrouping', CarrierName, ar.CarrierID, AccountReceivable, 
NewBillings,NewPayments, AR.MatterID,
CASE
WHEN ar.CarrierID = 4 THEN (CASE WHEN @Matter=1 THEN '3/11/2008' ELSE PaymentDate  END)
WHEN ar.CarrierID = 125 THEN (CASE WHEN @Matter is null THEN '11/04/2008' ELSE '11/04/2008' END)
WHEN ar.CarrierID = 9 THEN '2/1/1999'
WHEN ar.CarrierID = 20 THEN '11/30/2004'
WHEN ar.CarrierID = 97 THEN Null
WHEN ar.CarrierID = 47 THEN Null
WHEN PaymentDate is NULL THEN Null
ELSE PaymentDate END as PaymentDate
--ELSE MAX(Cast(MONTH(PaymentDate) as varchar(20)) +'/' +Cast(DAY(PaymentDate) as varchar(20))+'/'+Cast(YEAR(PaymentDate) as varchar(20))) END as PaymentDate
From tblCarrier c
left join (Select CarrierID, AccountReceivable, MatterID From @AR Where MatterID = 1) A on c.CarrierID = a.CarrierID
left Join (Select CarrierID, NewBillings, NewPayments, MatterID, PaymentDate From @NewAR Where MatterID = 1) AR ON c.CarrierID = AR.CarrierID 
Where c.CarrierID not in (10,20001,20002,20003,2)--20
GROUP BY CarrierName, AccountReceivable, 
NewBillings,NewPayments, AR.MatterID, PaymentDate, AR.CarrierID
--Having (SUM(NewPayments) <>0 OR SUM(NewBillings) <>0 or AccountReceivable<>0) 

UNION

Select 1 as OrderID, 'CarrierGrouping', CarrierName, AR.CarrierID, AccountReceivable, 
NewBillings,NewPayments, AR.MatterID,
CASE
WHEN ar.CarrierID = 4 THEN (CASE WHEN @Matter=1 THEN '3/11/2008' ELSE PaymentDate  END)
WHEN ar.CarrierID = 125 THEN (CASE WHEN @Matter is null THEN '8/19/2008' ELSE '11/04/2008' END)
WHEN ar.CarrierID = 9 THEN '2/1/1999'
WHEN ar.CarrierID = 20 THEN '11/30/2004'
WHEN ar.CarrierID = 97 THEN Null
WHEN ar.CarrierID = 47 THEN Null
WHEN PaymentDate is NULL THEN Null
ELSE PaymentDate END as PaymentDate
--ELSE MAX(Cast(MONTH(PaymentDate) as varchar(20)) +'/' +Cast(DAY(PaymentDate) as varchar(20))+'/'+Cast(YEAR(PaymentDate) as varchar(20))) END as PaymentDate
From tblCarrier c
left join (Select CarrierID, AccountReceivable, MatterID From @AR Where MatterID = 2) A on c.CarrierID = a.CarrierID
left Join (Select CarrierID, NewBillings, NewPayments, MatterID, PaymentDate From @NewAR Where MatterID = 2) AR ON c.CarrierID = AR.CarrierID 
Where c.CarrierID not in (10,20001,20002,20003,2)--20
GROUP BY CarrierName, AccountReceivable, 
NewBillings,NewPayments, AR.MatterID, PaymentDate, AR.CarrierID


--Having (SUM(NewPayments) <>0 OR SUM(NewBillings) <>0 or AccountReceivable<>0) 

--Union

--Select 2 as OrderID, CASE WHEN SubscriptionCompanySolvency LIKE 'Solvent' THEN 'London Solvent' WHEN SubscriptionCompanySolvency LIKE 'Insolvent' THEN 'London Insolvent' ELSE 'London Solvent' END,
--SubscriptionCompany, AccountReceivable as DatedAccountsReceivable, NewBillings as NewBilledAmounts,
-- COALESCE(NewPayments,0) as NewPayments, A.MatterID, Matter
--From @AR A 
--Left Join @NewAR AR on A.CarrierID = AR.CarrierID AND A.MatterID = AR.MatterID AND A.SubscriberID = AR.SubscriberID
--Left Join tblSubscriberCompany SC on A.SubscriberID=SC.SubscriberID
--Left Join tblMatter M on A.MatterID = M.MatterID
--Where a.CarrierID = 20

--Union

--Select 3 as OrderID, 'Self Insured', CarrierName, AccountReceivable as DatedAccountsReceivable, NewBillings as NewBilledAmounts,
-- NewPayments as NewPayments, AR.MatterID, Matter
--From @AR A
--Left Join @NewAR AR ON A.CarrierID = AR.CarrierID AND A.MatterID = AR.MatterID
--Left Join tblCarrier c on a.CarrierID = c.CarrierID
--Left Join tblCarrierGroup cg on c.CarrierGroupID=cg.CarrierGroupID
--Left Join tblMatter M on A.MatterID = M.MatterID
--Where A.CarrierID IN (20002,20003)

--FINAL SELECT

Select @BillDate as BillDate, OrderID, CarrierGrouping, CarrierName, carrierid, sum(coalesce(DatedAccountsReceivable,0)) as DatedAccountsReceivable,
Sum(Coalesce(NewBilledAmounts,0)) as NewBilledAmounts, Sum(Coalesce(NewPayments,0)) as NewPayments, 
CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter,
DATEADD(DAY,30,@BillDate) as CurrentDate, CONVERT(VARCHAR(10), MAX(PaymentDate),101) as PaymentDate
From @Final
Group By OrderID, CarrierGrouping, CarrierName, carrierid
Having Sum(coalesce(DatedAccountsReceivable,0)) <> 0 OR Sum(Coalesce(NewBilledAmounts,0)) > 0 OR Sum(Coalesce(NewPayments,0)) > 0
Order By Matter, OrderID, CarrierName

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAccountsReceivableActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAccountsReceivableActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAccountsReceivableActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAccountsReceivableActivity] TO [power_user]
GO
