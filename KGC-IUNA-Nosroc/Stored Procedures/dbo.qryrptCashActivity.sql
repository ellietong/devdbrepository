SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCashActivity] (@MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime)

As
Set NoCount On

--Declare @MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime
--Set @MatterID = 2
--Set @BalanceStartDate = '1/1/11'
--Set @BalanceEndDate = '6/7/11'

Declare @MinBalanceStartDate datetime
Set @MinBalanceStartDate = (Case when @BalanceStartDate < '1/1/2011' then '1/1/2011' Else @BalanceStartDate END)

Declare @AsOf2011 money
Set @AsOf2011 = (Case when @MatterID = 1 then 764549.45 When @MatterID = 2 then 97393.05 End)

Declare @StartingBalance money
Set @StartingBalance = (
Coalesce((Select SUM(CashActivityAmount) From tblMiscCashActivity mca 
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID 
where CashActivityDate >= '1/1/2011' and CashActivityDate < @MinBalanceStartDate and MatterID = @MatterID),0)
+
Coalesce((Select SUM(SourceAmount)*-1 From tblCheck c
Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
Where SourceDate >= '1/1/2011' and SourceDate < @MinBalanceStartDate and MatterID = @MatterID),0)
+ 
@AsOf2011
)

Declare @Matter nvarchar(250)
Set @Matter = (Select Matter from tblMatter where MatterID = @MatterID)

Declare @Detail table (Matter nvarchar(250), FundingType nvarchar(250), FundingSource nvarchar(250), SourceDate datetime, SourceNumber nvarchar(250), Indemnity money, Defense money, MiscFees money, CashReceipts money, Comment nvarchar(Max), CheckStatus bit, CheckNotes nvarchar(max))
Insert Into @Detail

--Defense
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, 0, SourceAmount*-1 as SourceAmount, 0, 0, DefenseCounsel, IsCheckCleared, c.CheckNotes
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentDefense pd on pd.CheckID = c.CheckID
	Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
	Inner Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @MinBalanceStartDate and SourceDate <= @BalanceEndDate) AND c.MatterID = @MatterID AND c.FundingTypeID = 1
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, DefenseCounsel, IsCheckCleared, c.CheckNotes

Union ALL

--Settlement
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1 as SourceAmount, 0, 0, 0, PlaintiffCounsel, IsCheckCleared, c.CheckNotes
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
	Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @MinBalanceStartDate and SourceDate <= @BalanceEndDate) AND c.MatterID = @MatterID AND c.FundingTypeID = 2
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, PlaintiffCounsel, IsCheckCleared, c.CheckNotes

Union ALL
--misc fees
Select Matter, CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, CashActivityAmount, 0, CashActivityNotes, Null, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @MinBalanceStartDate and CashActivityDate <= @BalanceEndDate) AND mca.MatterID = @MatterID AND mca.CashActivityTypeID in (2,3)

Union ALL
--cash receipts
Select Matter, CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, 0, CashActivityAmount, CashActivityNotes, Null, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @MinBalanceStartDate and CashActivityDate <= @BalanceEndDate) AND mca.MatterID = @MatterID AND mca.CashActivityTypeID in (4,5,6,7)

Union ALL

Select @Matter, 'Starting Balance', 'N/A', @BalanceStartDate , 'N/A', 0, 0, 0, Coalesce(@StartingBalance,0), 'Starting Balance', Null, Null

Select *, @MinBalanceStartDate as BalanceStartDate, @BalanceEndDate as BalanceEndDate from @Detail Order By SourceDate
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [power_user]
GO
