SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptAllocationByPolicyByOccGroup]
(@allocationid int, @insuranceProgramID int, @limitTypeID int, @exhaustiondateID int)
AS

SET NOCOUNT ON

 --Declare @allocationid int
 --Declare @insuranceProgramID int
 --Declare @limitTypeID int 
 --Declare @exhaustiondateID int
 
 --set @allocationid = 3
 --set @insuranceprogramid = 4
 --set @limittypeid = 2
 --set @exhaustiondateid = 1282



Declare @Allocations table(PolicyID int, PolicyDateID int, OccurrenceGroupID int, SiteName nvarchar (200),
Allocation money, AllocationIndemnity money, AllocationDefense money, AllocationDefenseOutsideLimits money, 
SIRAllocated money, SIRAllocatedIndemnity money, SIRAllocateDefense money, AllocationID int, AllocationDescription 
nvarchar(255))

Insert Into @Allocations 
Select p.PolicyID, p.PolicyDateID, occ.OccurrenceGroupID, SiteName, SUM(Allocation) AS AllocationTotal, 
SUM(AllocationIndemnity) AS SumIndem, SUM(AllocationDefense) AS SumDef, SUM(AllocationDefenseOutsideLimits) AS SumDefOutside, 
SUM(SIRAllocated) AS SumSIRAlloc, SUM(SIRAllocatedIndemnity) AS SumSIRIndem, SUM(SIRAllocatedDefense) AS SumSIRDef,
ar.AllocationID, ar.AllocationDescription

FROM vAllocationResults ar
     Inner Join vPolicy p ON p.PolicydateID = ar.PolicydateID 
     Inner Join tblOccurrence occ ON occ.OccurrenceID = ar.OccurrenceID 
     Inner Join tblOccurrenceGroup occg ON occ.OccurrenceGroupID = occg.OccurrenceGroupID 
    left Join tblEnviroSitesDetail esd ON occg.SiteID = esd.SiteID
     Inner Join vAllocation a ON a.allocationID = ar.allocationID
WHERE ar.allocationid = @allocationid AND p.LimitTypeID = @LimitTypeID
GROUP BY p.PolicyID, p.PolicyDateID, occ.OccurrenceGroupID, occg.OccurrenceGroup, esd.SiteName, ar.allocationid, 
ar.AllocationDescription
ORDER BY p.PolicyID, p.PolicyDateID, occ.OccurrenceGroupID

Declare @Exhaustion table (ExhaustionDateID int, PolicyDateID int, IndemnityAllocation money)

Insert Into @Exhaustion
Select ExhaustionDateID, PolicyDateID, IndemnityAllocation
From vExhaustion
WHERE ExhaustionDateID = @ExhaustionDateID
Group By ExhaustionDateID, PolicyDateID, IndemnityAllocation

SELECT p.PolicyID, pd.PolicyDateID, c.CarrierName, p.PolicyNum, pd.PStartDate, pd.PEndDate, 
pl.AttachmentPoint, pl.PerOccLimit, pl.AggregateLimit, InsuranceProgram,
 OccurrenceGroupID, SiteName, AllocationIndemnity, AllocationDefense, AllocationDefenseOutsideLimits, 
SIRAllocatedIndemnity, SIRAllocateDefense, AllocationID, AllocationDescription, COALESCE(IndemnityAllocation, 0) as IndemnityAllocation
FROM tblPolicy p
	INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID 
	INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID
	INNER JOIN @allocations a ON pd.PolicyDateID = a.PolicyDateID
	INNER JOIN tblPolicyLimits pl ON p.PolicyID = pl.PolicyID 
	INNER JOIN tblInsuranceProgram i ON p.insuranceprogramID = i.insuranceprogramID
	LEFT JOIN @exhaustion ex ON pd.PolicyDateID = ex.PolicyDateID

WHERE pl.LimitTypeID = @limitTypeID and i.insuranceProgramID = @insuranceProgramID 
GROUP BY p.PolicyID, pd.PolicyDateID, c.CarrierName, p.PolicyNum, pd.PStartDate, pd.PEndDate, 
pl.AttachmentPoint, pl.PerOccLimit, pl.AggregateLimit, InsuranceProgram,
 OccurrenceGroupID, SiteName, AllocationIndemnity, AllocationDefense, AllocationDefenseOutsideLimits, 
SIRAllocatedIndemnity, SIRAllocateDefense, AllocationID, AllocationDescription, IndemnityAllocation
ORDER BY pd.PStartDate, pl.AttachmentPoint

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByPolicyByOccGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByPolicyByOccGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByPolicyByOccGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByPolicyByOccGroup] TO [power_user]
GO
