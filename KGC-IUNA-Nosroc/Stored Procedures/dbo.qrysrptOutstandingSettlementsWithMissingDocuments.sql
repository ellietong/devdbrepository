SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--USE [KGC-IUNA-Nosroc]
--GO
--/****** Object:  StoredProcedure [dbo].[SP_MOSS_REPORTS_qryOutstandingSettlementsWithMissingDocuments]    Script Date: 07/01/2011 15:42:53 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE Proc [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] (@ClaimantPersonalInfoID int, @LawsuitID int, @MatterID int)

As

SET NOCOUNT ON

--DECLARE @ClaimantPersonalInfoID int
--DECLARE @LawsuitID int
--DECLARE @MatterID int
--SET @ClaimantPersonalInfoID = 9685
--SET @LawsuitID = 13463
--SET @MatterID = 1

DECLARE @ClaimantDocs TABLE(ClaimantIDed int, DxMedicals int, DODCert int, EstatePapers int, ProductID int, EmployExpoHist int, settlementconfirmation int, medicareform int)
DECLARE @MissingDocuments TABLE(MissingDocName nvarchar(100))

INSERT INTO @ClaimantDocs
	SELECT  sd.ClaimantIDed, sd.DxMedicals, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE  sd.DODCert END AS DODCert, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE  sd.EstatePapers END AS EstatePapers, 
		 sd.ProductID,  sd.EmployExpoHist,  sd.settlementconfirmation, 
		 CASE WHEN MedicareFormRcvd IS NOT NULL 
		 THEN 1   when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) <= '12/5/1980' then 1 else 0 END 
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID		
		INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		inner join tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND LawsuitID = @LawsuitID
		AND MatterID = @MatterID
		
INSERT INTO @MissingDocuments
	SELECT 'Fully Identified - SSN, DOB, DOD' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ClaimantIDed IS NULL Or ClaimantIDed = 0
INSERT INTO @MissingDocuments
	SELECT 'Diagnosing Medicals' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DxMedicals IS NULL Or DxMedicals = 0
INSERT INTO @MissingDocuments
	SELECT 'Death Certificate' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DODCert IS NULL Or DODCert = 0
INSERT INTO @MissingDocuments
	SELECT 'Estate Paperwork' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EstatePapers IS NULL Or EstatePapers = 0
INSERT INTO @MissingDocuments
	SELECT 'Product ID' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ProductID IS NULL Or ProductID = 0
INSERT INTO @MissingDocuments
	SELECT 'Employment/Exposure History' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EmployExpoHist IS NULL Or EmployExpoHist = 0
INSERT INTO @MissingDocuments
	SELECT 'Settlement Confirmation Letter' AS MissingDocName
	FROM @ClaimantDocs
	WHERE settlementconfirmation IS NULL Or settlementconfirmation = 0
INSERT INTO @MissingDocuments
	SELECT 'Medicare Form' AS MissingDocName
	FROM @ClaimantDocs
	WHERE medicareform IS NULL Or medicareform = 0

SELECT MissingDocName FROM @MissingDocuments



GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptOutstandingSettlementsWithMissingDocuments] TO [power_user]
GO
