SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[REVERT_SP_MOSS_qryGender]
as

Select GenderID, Gender
From tblGender
Order By Gender

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryGender] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryGender] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryGender] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryGender] TO [power_user]
GO
