SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency]
(@SubscriberCompanyID int = NULL, @Solvency varchar(50) = NULL, @InsuranceProgramID int = NULL,
@limitTypeID int = Null)

AS 
SET NOCOUNT ON

DECLARE @maxInsuranceProgramID int
DECLARE @maxSubscriberCompanyID int
DECLARE @maxLimitTypeID int
SELECT @maxInsuranceProgramID = MAX(InsuranceProgramID) FROM tblInsuranceProgram
SELECT @maxSubscriberCompanyID  = MAX(SubscriberID) FROM tblSubscriptionCoverage
SELECT @maxSubscriberCompanyID  = MAX(SubscriberID) FROM vSubscription

DECLARE @qrySubscriptionCoverageByPolicyByCompany table (
	[minOfPStartDate] smalldatetime, 
	[maxOfPEndDate] smalldatetime,
	[minOfAnnualPeriod] int,
	[insuranceprogram] varchar (256),
	[subscriptionCompany] varchar(256),
	[subscriptionCompanySolvency] varchar(256),
	[Percentage] decimal,	
	[SubscriberPerOcc] money,
	[CarrierID] int, 
	[InsuranceProgramID] int, 
	[AttachmentPoint] money, 
	[PerOccLimit] money,
	[LayerPerOccLimit] money, 
	[SIR] money, 
	[AggregateLimit] money, 
	[Layer] int, 
	[PolicyNum] varchar (256), 
	[PolicyID] int, 
	[SubscriberID] int
)

INSERT INTO @qrySubscriptionCoverageByPolicyByCompany
SELECT MIN(p.PStartDate) AS MinOfPStartDate, MAX(p.PEndDate) AS MaxOfPEndDate, MIN(p.AnnualPeriod) AS MinOfAnnualPeriod, p.InsuranceProgram, scomp.SubscriptionCompany, 
	scomp.SubscriptionCompanySolvency, scov.Percentage, (scov.Percentage * p.PerOccLimit) AS SubscriberPerOcc, p.CarrierID, p.InsuranceProgramID, p.AttachmentPoint, p.PerOccLimit, 
	p.LayerPerOccLimit, p.SIR, p.AggregateLimit, p.Layer, p.PolicyNum, p.PolicyID, scov.SubscriberID
FROM vPolicy p 
	INNER JOIN tblSubscriptionCoverage scov ON p.PolicyID = scov.PolicyID 
	INNER JOIN vSubscription scomp ON scomp.SubscriberID = scov.SubscriberID 
WHERE p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) AND
	scomp.SubscriberID >= COALESCE(@SubscriberCompanyID, 0) AND scomp.SubscriberID <= COALESCE(NULLIF(@SubscriberCompanyID, 0), @maxSubscriberCompanyID) AND
	scomp.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%')
	AND p.limitTypeID >= COALESCE(@limitTypeID, 0) AND p.limitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)
	AND DoNotReport <>1
GROUP BY p.InsuranceProgram, scomp.SubscriptionCompany, scomp.SubscriptionCompanySolvency, scov.Percentage, (scov.Percentage * p.PerOccLimit), p.CarrierID, p.InsuranceProgramID, 
	p.AttachmentPoint, p.PerOccLimit, p.LayerPerOccLimit, p.SIR, p.AggregateLimit, p.Layer, p.PolicyNum, p.PolicyID, scov.SubscriberID

SELECT SubscriptionCompany, InsuranceProgramID, InsuranceProgram, CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END AS 'SubscriberSolvencyGroup',
	SUM(SubscriberPerOcc) AS SumOfSubscriberPerOcc
FROM @qrySubscriptionCoverageByPolicyByCompany
GROUP BY SubscriptionCompany, InsuranceProgramID, InsuranceProgram, CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyBySolvency] TO [power_user]
GO
