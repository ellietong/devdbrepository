SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryALLSigLevel]
(@allocationID int, @defensecosts float, @costfields float, @InsuranceProgramID nvarchar(250),
@TriggerStart datetime, @TriggerEnd datetime, @limitTypeID int, @DefenseType bit, @CorporateHistoryID int, @PolicyDateTypeID int)

AS
--01-27-2010: JKC revised query to; anytime OccurrenceRemaining is used, replace with (OccurrenceRemaining -CumAllocationIndemnity - CumAllocationDefense) to account for timing
-- of Occurrence Remaining update


--DECLARE @DefenseCosts  money
--DECLARE	@CostFields money
--DECLARE @AllocationID int
--DECLARE @InsuranceProgramID nvarchar(250)
--DECLARE @triggerstart datetime
--DECLARE @triggerend datetime
--DECLARE @limittypeID int
-- SET @DefenseCosts = 525633.9588 
-- SET @CostFields = 0 
-- SET @allocationId = 232
-- SET @InsuranceProgramID = '1'
-- SET @triggerstart = '1/1/1971'
-- SET @triggerend = '5/31/1985'
-- SET @limittypeid = 4
 
--FOR IUNA ONLY ADDED INSURANCE PROGRAM 14 to ALL INSURANCE PROGRAM REQUIREMENTS 20100514 JTM

DECLARE @strSQL nvarchar(max)

SET NOCOUNT ON 

Declare @ExhaustionTypeID nvarchar(10)
Set @ExhaustionTypeID = convert(nvarchar(10),(Select ExhaustionTypeID From tblAllocation Where AllocationID = @AllocationID)) 

Declare @Exhaustion table (PolicyDateID int, Exhaustion money)
INSERT INTO @Exhaustion
Select PolicyDateID, sum(Coalesce(IndemnityAllocation,0) + Coalesce(DefenseAllocation,0) + Coalesce(OutsideConsumption,0)) as Exhaustion
From tblExhaustion e Inner Join tblExhaustionDate ed on e.exhaustionDateID = ed.ExhaustionDateID
Where ExhaustionTypeID = @ExhaustionTypeID
Group By PolicyDateID


Declare @AllocationPreviousOccurrences table (PolicyDateID int, AllocationPrevOcc money)
Insert INTO @AllocationPreviousOccurrences 
Select PolicyDateID, SUM(ar.AllocationIndemnity+ar.AllocationDefense)
From tblAllocationResults ar 
Where AllocationID = @AllocationID
Group By PolicyDateID 

Declare @AllocationCurrentOccurrence table (PolicyDateID int, AllocationCurrentOcc money)
Insert Into @AllocationCurrentOccurrence 
Select PolicyDateID, SUM(ar.CumAllocationIndemnity+ar.CumAllocationDefense)
From ztblPolicyAllocation ar 
Where AllocationID = @AllocationID
Group By PolicyDateID 

-- Update for IUNA for the moving coverage block JTM 5/21/2010

Declare @CorporateHistory table (InsuranceProgramID int, PolicyStart datetime, PolicyEnd datetime)
Insert Into @CorporateHistory
Select InsuranceProgramID, Coalesce([From], '1/1/1900') as [From], Coalesce([Until], '12/31/2010')
From tblCorpHistInsProgram
Where CorporateHistoryID = @CorporateHistoryID

Declare @ApplicablePolicies table (PolicyID int)
Insert Into @ApplicablePolicies
Select p.PolicyID From tblPolicy p
Inner join tblPolicyDates pd on p.PolicyID = pd.PolicyID
Inner Join @CorporateHistory ch on ch.InsuranceProgramID = p.InsuranceProgramID
Where PEndDate > PolicyStart and PStartDate < PolicyEnd and PolicyDateTypeID = @PolicyDateTypeID

-- End Update Table Linked in final to limit record set considered


IF @DefenseType = 0 --Indemnity 

Begin

Delete FROM ztblSigLevel 

Declare @temp table (columnid int,columnstartdate datetime, columnenddate datetime,
 minattach money)
insert into @temp
select zc.columnid,columnstartdate, columnenddate, min(zc.attachmentpoint)
from (Select * from ztblPolicyAllocation where InsuranceProgramID in (14,@InsuranceProgramID) AND (OccurrenceRemaining) > .1 AND allocationID = @allocationID ) za
inner join ztblpolicycolumn zc on zc.policydateid=za.policydateid
inner join (SELECT * FROM tblAllocationresultscolumn WHERE InsuranceProgramID in (14,@InsuranceProgramID) and allocationid = @allocationID ) arc on zc.columnid = arc.columnid
inner join
(select policyid from ztblpolicycolumn z INNER JOIN ztblPolicyAllocation a ON z.PolicyDateID = a.PolicyDateID where provisionvalueid=182)q on q.policyid=za.policyid where (OccurrenceRemaining) > .1
	and za.allocationID = @allocationid 

group by zc.columnid, columnstartdate,columnenddate
order by columnstartdate




Declare @SigLevelInfo table (ColumnID int, PolicyID int, Provision1 int, 
UpperLimit money, SigDropDownAttachmentPoint money, AttachmentPoint money, 
NextAttachmentPoint money, PolicyRemainingAmt money, ColumnPercent float)
Insert Into @SigLevelInfo
Select t.columnid, 
	a.policyid, 
	a.Provision1, 
	(a.AttachmentPoint+coalesce(e.Exhaustion,0) +coalesce(apo.AllocationPrevOcc,0) +coalesce(aco.AllocationCurrentOcc,0))as UpperLimit, 
	CASE WHEN dbo.sigdropdown(c.columnid, a.policyid, @limitTypeID, l.layer, @allocationID) IS NULL 
		THEN a.attachmentpoint
		ELSE dbo.sigdropdown(c.columnid, a.policyid, @limitTypeID, l.layer,@allocationID)  END as SigDropDownAttachmentPoint,
	a.AttachmentPoint,
	Coalesce(dbo.fnNextLowestAttachmentPoint(@AllocationID, @InsuranceProgramID, @triggerstart, 
				@triggerend, 
				@limittypeID, 
				(a.attachmentpoint+coalesce(e.Exhaustion,0) +coalesce(apo.AllocationPrevOcc,0) +coalesce(aco.AllocationCurrentOcc,0))),1000000000) as NextAttachmentPoint,
	(occurrenceremaining) as PolicyRemainingAmt,
	(ColumnDays/PolicyDaysInColumn) as ColumnPercent			
from ztblpolicycolumn c
	inner join @temp t on t.columnid=c.columnid
	inner join ztblpolicyallocation a on a.policydateid=c.policydateid AND t.minattach=a.attachmentpoint
	INNER JOIN tblPolicyLimits l ON a.PolicyID = l.PolicyID
	Left Join @AllocationCurrentOccurrence aco on aco.PolicyDateID = c.PolicyDateID 
	Left Join @AllocationPreviousOccurrences apo on apo.PolicyDateID = c.PolicyDateID 
	Left Join @Exhaustion e on e.PolicyDateID = c.PolicyDateID 
where (occurrenceremaining) > 0.01
	and allocationid= @allocationid 
	AND  InsuranceProgramID in (14,@InsuranceProgramID)
	AND ColumnStartDate < @TriggerEnd
	AND ColumnEndDate > @TriggerStart
	AND l.limittypeID = @limitTypeID


----For Testing Purposes	
--Select * From @SigLevelInfo 
--Order By UpperLimit, PolicyRemainingAmt 	
--removed s.columnPercent from smallest indemnity/defense to prevent double pro-ration
INSERT INTO ztblSigLevel (ColumnID, PolicyID, UpperLimit, SmallestIndemnity, SmallestDefense, AttachmentPoint)
Select s.ColumnID, 
	s.PolicyID, 
	s.UpperLimit, 
	(Case When s.UpperLimit + s.PolicyRemainingAmt > s.NextAttachmentPoint Then s.NextAttachmentPoint - s.UpperLimit Else s.PolicyRemainingAmt End)  as SmallestIndemnity ,
	0 as SmallestDefense,
	s.SigDropDownAttachmentPoint as AttachmentPoint
From @SigLevelInfo s
Inner Join @ApplicablePolicies ap on ap.PolicyID = s.PolicyID
Order By UpperLimit, (Case When s.UpperLimit + s.PolicyRemainingAmt > s.NextAttachmentPoint Then s.NextAttachmentPoint - s.UpperLimit Else s.PolicyRemainingAmt End) 

End

IF @DefenseType = 1 -- defense, exclude policies that do not pay defense (note that smallest indemnity should always be zero)

Begin

Delete FROM ztblSigLevel 

Declare @tempDefense table (columnid int,columnstartdate datetime, columnenddate datetime, minattach money)
insert into @tempDefense
select zc.columnid,columnstartdate, columnenddate, min(zc.attachmentpoint)
from (Select * from ztblPolicyAllocation where InsuranceProgramID in (14,@InsuranceProgramID) AND (OccurrenceRemaining) > .1 AND allocationID =  @allocationID) za
	inner join ztblpolicycolumn zc on zc.policydateid=za.policydateid
	inner join (SELECT * FROM tblAllocationresultscolumn WHERE InsuranceProgramID in (14,@InsuranceProgramID) and allocationid = @allocationID ) arc on zc.columnid = arc.columnid
	inner join
		(select policyid from ztblpolicycolumn z INNER JOIN ztblPolicyAllocation a ON z.PolicyDateID = a.PolicyDateID where provisionvalueid=182)q on q.policyid=za.policyid where (OccurrenceRemaining) > .1
		and za.allocationID = @allocationid
group by zc.columnid, columnstartdate,columnenddate
order by columnstartdate


Declare @SigLevelInfoDefense table (ColumnID int, PolicyID int, Provision1 int, UpperLimit money, SigDropDownAttachmentPoint money, AttachmentPoint money, NextAttachmentPoint money, PolicyRemainingAmt money, ColumnPercent float)
Insert Into @SigLevelInfoDefense
Select t.columnid, 
	a.policyid, 
	a.Provision1, 
	(a.AttachmentPoint+coalesce(e.Exhaustion,0) +coalesce(apo.AllocationPrevOcc,0) +coalesce(aco.AllocationCurrentOcc,0))as UpperLimit, 
	CASE WHEN dbo.sigdropdown(c.columnid, a.policyid, @limitTypeID, l.layer, @allocationID) IS NULL 
		THEN a.attachmentpoint
		ELSE dbo.sigdropdown(c.columnid, a.policyid, @limitTypeID, l.layer,@allocationID)  END as SigDropDownAttachmentPoint,
	a.AttachmentPoint,
	Coalesce(dbo.fnNextLowestAttachmentPoint(@AllocationID, @InsuranceProgramID, @triggerstart, 
				@triggerend, 
				@limittypeID, 
				(a.attachmentpoint+coalesce(e.Exhaustion,0) +coalesce(apo.AllocationPrevOcc,0) +coalesce(aco.AllocationCurrentOcc,0))),1000000000) as NextAttachmentPoint,
	(occurrenceremaining) as PolicyRemainingAmt,
	(ColumnDays/PolicyDaysInColumn) as ColumnPercent			
from ztblpolicycolumn c
	inner join @tempDefense t on t.columnid=c.columnid
	inner join ztblpolicyallocation a on a.policydateid=c.policydateid AND t.minattach=a.attachmentpoint
	INNER JOIN tblPolicyLimits l ON a.PolicyID = l.PolicyID
	Left Join @AllocationCurrentOccurrence aco on aco.PolicyDateID = c.PolicyDateID 
	Left Join @AllocationPreviousOccurrences apo on apo.PolicyDateID = c.PolicyDateID 
	Left Join @Exhaustion e on e.PolicyDateID = c.PolicyDateID 
where (occurrenceremaining) > 0.01
	and allocationid= @allocationid 
	AND  InsuranceProgramID in (14,@InsuranceProgramID)
	AND ColumnStartDate < @TriggerEnd
	AND ColumnEndDate > @TriggerStart
	AND l.limittypeID = @limitTypeID


----For Testing Purposes	
--Select * From @SigLevelInfoDefense 
--Order By UpperLimit, PolicyRemainingAmt 	
		
INSERT INTO ztblSigLevel (ColumnID, PolicyID, UpperLimit, SmallestIndemnity, SmallestDefense, AttachmentPoint)
Select s.ColumnID, 
	s.PolicyID, 
	s.UpperLimit, 
	0 as SmallestIndemnity,
	(Case When Provision1 in (1,2) Then (Case When s.UpperLimit + s.PolicyRemainingAmt > s.NextAttachmentPoint 
										Then s.NextAttachmentPoint - s.UpperLimit 
										Else s.PolicyRemainingAmt End) 
		When Provision1 = 3 Then @DefenseCosts 
		When Provision1 = 4 Then 0 End ) as SmallestDefense,
	s.SigDropDownAttachmentPoint as AttachmentPoint
From @SigLevelInfoDefense s
Inner Join @ApplicablePolicies ap on ap.PolicyID = s.PolicyID
Order By UpperLimit, (Case When Provision1 in (1,2) Then (Case When s.UpperLimit + s.PolicyRemainingAmt > s.NextAttachmentPoint 
										Then s.NextAttachmentPoint - s.UpperLimit 
										Else s.PolicyRemainingAmt End) 
		When Provision1 = 3 Then @DefenseCosts 
		When Provision1 = 4 Then 0 End )


		
	
End 

RETURN




GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLSigLevel] TO [Allocation_User]
GRANT EXECUTE ON  [dbo].[qryALLSigLevel] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLSigLevel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLSigLevel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLSigLevel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLSigLevel] TO [power_user]
GO
