SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qrysfrmSettleDocs] (@SetPymtID int)

as

SELECT *
FROM tblSettleDocs
WHERE SetPymtID = @SetPymtID


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmSettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmSettleDocs] TO [power_user]
GO
