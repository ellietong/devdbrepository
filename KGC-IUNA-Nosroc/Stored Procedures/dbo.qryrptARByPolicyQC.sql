SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByPolicyQC]
(@allocationID int = null, @limittypeid int = NULL)
As
	
SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 3
--DECLARE @LimitTypeID int
--SET @LimitTypeID = 2

SELECT ar.DateToPresentValue, MIN(ar.DateTime) AS [DateTime], SUM(ar.Allocation) AS Allocation, Sum(ar.AllocationIndemnity) AS AllocationIndemnity, 
	SUM(ar.AllocationDefense) AS AllocationDefense, SUM(ar.AllocationDefenseOutsideLimits) AS AllocationDefenseOutsideLimits, SUM(ar.PV) AS PV, 
	SUM(ar.SIRAllocated) AS SIRAllocated, 
	ar.PStartSplit, ar.PEndSplit, ar.AnnualPeriodSplit, pinfo.CarrierID, pinfo.CarrierSolvency, pinfo.CarrierName, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END AS 'CarrierSort', pinfo.CarrierGroupID, 
	pinfo.CarrierGroupName, pinfo.PolicyID, pr.Provisionid, 
	pr.ProvisionValue, pinfo.PolicyNum, pinfo.AttachmentPoint, pinfo.PreviouslyExhaustedAggregate, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, 
	pinfo.SIR, pinfo.AggregateLimit, pinfo.Layer, a.AllocationID, a.AllocationDescription, 
	a.AllocationSchemeID, a.TriggerStartID, a.TriggerStartDate, a.TriggerEndID, a.TriggerEndDate, a.OccurrenceDefId, a.UnCoveredYearstoPH, a.Cost1, a.Cost2, a.Cost3, a.Cost4, a.Cost5, a.Cost6, 
	a.Cost7, a.Cost8, a.Cost9, a.SIRID, a.SIRToPay, a.LessThanAttachmentPoint, a.LessThanLayer, a.OccurrencesInClaim, pinfo.InsuranceProgram, pinfo.InsuranceProgramID
FROM vPolicy pinfo INNER JOIN vProvisions pr on pr.PolicyID = pinfo.PolicyID
	INNER JOIN vAllocationResults ar ON pinfo.PolicyDateID = ar.PolicyDateID
	INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID
WHERE pr.ProvisionLabelID = 1 AND a.AllocationID = COALESCE(@allocationID, 0) AND pinfo.LimitTypeID = @limittypeid

GROUP BY ar.DateToPresentValue, pinfo.CarrierID, pr.ProvisionID, pr.ProvisionValue, pinfo.CarrierSolvency, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END,
	pinfo.PolicyID, pinfo.InsuranceProgram, ar.PStartSplit, ar.PEndSplit, ar.AnnualPeriodSplit, pinfo.CarrierName, pinfo.PolicyNum, pinfo.AttachmentPoint, pinfo.PreviouslyExhaustedAggregate, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, 
	pinfo.SIR, pinfo.AggregateLimit, pinfo.Layer, a.AllocationID, a.AllocationDescription, a.AllocationSchemeID, a.TriggerStartID, a.TriggerStartDate, a.TriggerEndID, a.TriggerEndDate, a.OccurrenceDefId, 
	a.UnCoveredYearstoPH, a.Cost1, a.Cost2, a.Cost3, a.Cost4, a.Cost5, a.Cost6, a.Cost7, a.Cost8, a.Cost9, a.SIRID, a.SIRToPay, a.LessThanAttachmentPoint, a.LessThanLayer, a.OccurrencesInClaim, 
	pinfo.CarrierGroupID, pinfo.CarrierGroupName, pinfo.InsuranceProgramID, pinfo.AttachmentPoint
	ORDER BY PolicyNum, PStartSplit

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByPolicyQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByPolicyQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByPolicyQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByPolicyQC] TO [power_user]
GO
