SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2]
(@AllocationID int = NULL)
As

SET NOCOUNT ON

DECLARE @qryARBySubscriberCompanyByInsProg TABLE (AllocationAmt money, AllocationID int, SubscriptionCompanySolvency varchar(256) )

DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM tblAllocation

DECLARE @Policy TABLE (PolicyID int)
INSERT INTO @Policy

SELECT PolicyID
FROM vPolicy
GROUP BY PolicyID

INSERT INTO @qryARBySubscriberCompanyByInsProg
SELECT SUM(ar.Allocation * scov.Percentage) AS AllocationAmt, a.AllocationID, scomp.SubscriptionCompanySolvency	
FROM (((@Policy p INNER JOIN tblSubscriptionCoverage scov ON p.PolicyID = scov.PolicyID) INNER JOIN tblSubscriberCompany scomp ON scomp.SubscriberID = scov.SubscriberID)
	INNER JOIN tblAllocationResults ar ON p.PolicyID = ar.PolicyID) INNER JOIN tblAllocation a ON a.AllocationID = ar.AllocationID 
GROUP BY a.AllocationID, scomp.SubscriptionCompanySolvency

SELECT AllocationID, CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END AS 'SubscriberSolvencyGroup', SUM(AllocationAmt) as SumOfAllocation
FROM @qryARBySubscriberCompanyByInsProg
WHERE AllocationID >= COALESCE(@allocationID, 0) AND AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID)
GROUP BY AllocationID, CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END	

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2] TO [power_user]
GO
