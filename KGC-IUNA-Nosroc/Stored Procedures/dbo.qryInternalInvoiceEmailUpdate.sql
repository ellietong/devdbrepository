SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCedure [dbo].[qryInternalInvoiceEmailUpdate]
AS
Begin
/*Query run daily to send an internal email listing any invoices which have been approved or rejected
and are ready to be QC'd. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (2000)

SET @Subject = 'IUNA/NOSROC: New Defense Invoices Approved/Rejected as of ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>
<H4>Below are any newly approved/rejected invoices for review.</H4>'

--Declare firm counter
DECLARE @FirmCount int
SET @FirmCount = 0

--Create temp table for invoices with new status changes
DECLARE @NewInvoicesTable TABLE (spdocid nvarchar (100), FirmID nvarchar (50))
INSERT INTO @NewInvoicesTable
	SELECT
		SPDocID,
		FirmID
	FROM tblDocuments d
	INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	WHERE DateApproved BETWEEN GETDATE() - 1 AND GETDATE()
	AND cdt.DocumentGroupID = 2
	GROUP BY	SPDocID,
				FirmID

--Declare a cursor to create a separate email for each firm
DECLARE @invoicecount int, @firmid nvarchar (20), @Firm nvarchar (200), @firmURL nvarchar (100)
DECLARE firm_Curs CURSOR FOR SELECT
	COUNT(spdocid),
	f.FirmID,
	Firm,
	'https://admin.kcicllc.com/cp/iuna_nosroccp/Defense%20Counsel%20Invoices/Forms/AllItems.aspx'
FROM @NewInvoicesTable i
INNER JOIN vtblFirm f ON i.FirmID = f.FirmID
INNER JOIN vtblFirmCase fc ON f.FirmID = fc.FirmID
WHERE CaseID = 22
GROUP BY	f.FirmID,
			Firm

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @invoicecount, @firmid, @Firm, @firmurl

WHILE @@FETCH_STATUS = 0 BEGIN
SET @FirmCount = @FirmCount + 1
SET @BodyMessage = @BodyMessage + CONVERT(nvarchar(10), (SELECT
	@invoicecount)
) + ' New Invoices Were Approved or Rejected in the <A HREF=' + (SELECT
	@firmurl)
+ '>' + @Firm + '</a> Invoice Library.<br><br>'
FETCH NEXT FROM firm_curs INTO @invoicecount, @firmid, @Firm, @firmurl
END

CLOSE firm_curs
DEALLOCATE firm_curs

SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


--Only send email if there are new status updates.
IF (@FirmCount > 0)
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'iuna_nosroc@kcic.com',
								--@recipients = 'amanp@kcic.com.test-google-a.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'

End


GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalInvoiceEmailUpdate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalInvoiceEmailUpdate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalInvoiceEmailUpdate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalInvoiceEmailUpdate] TO [power_user]
GO
