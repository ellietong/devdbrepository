SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[qrysrptApplicableCarriers]
(@correspondenceid int)
as
set nocount on
select correspondenceid, carriername
from tblapplicablecarrier ac inner join tblcarrier c on c.CarrierID = ac.CarrierID
where correspondenceid = @correspondenceid
return
grant execute on dbo.qrysrptApplicableCarriers to power_user, base_user

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptApplicableCarriers] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptApplicableCarriers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptApplicableCarriers] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptApplicableCarriers] TO [power_user]
GO
