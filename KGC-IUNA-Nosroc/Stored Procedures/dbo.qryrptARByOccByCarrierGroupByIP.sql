SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByOccByCarrierGroupByIP]
(@allocationID int = null, @CarrierID INT = NULL, @Solvency varchar (50)= NULL, @CarrierGroupID int = NULL, @InsuranceProgramID INT = NULL, @OccurrenceTypeIDs nvarchar(100) = NULL)

As

SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 83
--DECLARE @CarrierID int
--SET @CarrierID = 0
--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = 0
--DECLARE @Solvency varchar (50)
--SET @Solvency = null
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID = 0
--DECLARE @OccurrenceTypeIDs nvarchar(100)
--SET @OccurrenceTypeIDs = null

DECLARE @maxAllocationID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @maxInsuranceProgramID int
DECLARE @strSQL nvarchar (4000)
DECLARE @prmDef nvarchar (1000)
DECLARE @tmpSolvency nvarchar (50)
DECLARE @strOccurrences nvarchar (4000)
DECLARE @carrierGroupInfo nvarchar (50)
DECLARE @maxOccurrenceTypeID int
DECLARE @index int

--SET Variables to initial values
SET @carrierGroupInfo = ''
SET @strSQL = ''
SET @strOccurrences = ''
SET @tmpSolvency = ''
SET @prmDef = ''
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxInsuranceProgramID  = MAX (InsuranceProgramID) FROM vPolicy
SELECT @maxOccurrenceTypeID = MAX(OccurrenceTypeID) FROM vOccurrence

--Update Null Strings Set above that aren't resolved in WHERE clause (1)solvency, 2)Occurrence Types, 3)Carrier Group Info)
--	1	Update Solvency Info
IF @Solvency = '<<ALL>>' OR @Solvency IS NULL OR @Solvency = ''
	SET @tmpSolvency = '%'
ELSE
	SET @tmpSolvency = @Solvency

---------------------------------------------------------------------------------------
--	2	Setup the Occurrence String.
-- STILL NEED TO UPDATE AND test if @OccurrenceTypeIDs is null, if so, 
--update @strOccurrences to work with that.
--Test if Null, if null, then build a string of all occurrence types
IF (LTRIM(RTRIM(@OccurrenceTypeIDs)) <> '' AND @OccurrenceTypeIDs IS NOT NULL)  
	SET @strOccurrences = @OccurrenceTypeIDs  
ELSE 
	BEGIN
		SET @strOccurrences = '1, '
		SET @index = 2
		
		WHILE @index <= @maxOccurrenceTypeID
		BEGIN
			SET @strOccurrences = @strOccurrences + CONVERT(nvarchar, @index) + ', '
			SET @index = @index + 1
		END
		SET @strOccurrences = LEFT(@strOccurrences, LEN(@strOccurrences) - 1)
	END

---------------------------------------------------------------------------------------
--	3	Update Carrier Group Info.
SELECT @carrierGroupInfo = CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END 
FROM vPolicy pinfo

--Build the SQL String

SET @strSQL = N'
SELECT o.OccurrenceTypeID, o.TriggerStart1, o.OccurrenceName, pinfo.InsuranceProgramID, pinfo.InsuranceProgram, a.AllocationID, a.AllocationDescription, pinfo.CarrierSolvency, pinfo.CarrierID, 
	pinfo.CarrierName, pinfo.CarrierGroupID, pinfo.CarrierGroupName, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END AS CarrierGroupInfo,
	ar.DateToPresentValue, ar.OccurrenceID, ar.TriggerStartDateUsed, ar.DateTime, SUM(ar.Allocation) AS SumOfAllocation, CONVERT(money, SUM(ar.PV)) AS SumOfPV,
	SUM(ar.SIRAllocated) AS SumOfSIRAllocated, ar.TriggerEndDateUsed, YieldCurveDate as PVRateDate
FROM (
SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, InsuranceProgramID, InsuranceProgram,
CarrierSolvency
FROM vPolicy
GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, InsuranceProgramID, InsuranceProgram,
CarrierSolvency) pinfo 
	INNER JOIN vAllocationResults ar ON pinfo.PolicyDateID = ar.PolicyDateID 
	INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID 
	INNER JOIN vOccurrence o ON ar.OccurrenceID = o.OccurrenceID
	LEFT JOIN tblYieldCurve yc on yc.YieldCurveID = ar.YieldCurveID 
WHERE a.AllocationID >= COALESCE('+ CONVERT (nvarchar, @allocationID) +', 0) AND 
	a.AllocationID <= COALESCE(NULLIF(' + CONVERT (nvarchar, @allocationID) + ', 0), ' + CONVERT (nvarchar, @MAXallocationid) + ' ) AND
	pinfo.CarrierID >= COALESCE('+ CONVERT (nvarchar, @CarrierID) +', 0) AND 
	pinfo.CarrierID <= COALESCE(NULLIF(' + CONVERT (nvarchar, @CarrierID) + ', 0), ' + CONVERT (nvarchar, @MAXCarrierID) + ' ) AND
	pinfo.CarrierSolvency LIKE @SolvencyPrm AND '
IF @CarrierGroupID <> 0
	SET @strSQL = @strSQL + 'pinfo.CarrierGroupID = ' + CONVERT(nvarchar, @CarrierGroupID) + ' AND '

SET @strSQL = @strSQL + 'pinfo.InsuranceProgramID >= COALESCE('+ CONVERT (nvarchar, @InsuranceProgramID) +', 0) AND 
	pinfo.InsuranceProgramID <= COALESCE(NULLIF(' + CONVERT (nvarchar, @InsuranceProgramID) + ', 0), ' + CONVERT (nvarchar, @maxInsuranceProgramID) + ' ) AND
	o.OccurrenceTypeID IN (' + CONVERT (nvarchar (4000),@strOccurrences) + ')
GROUP BY o.OccurrenceTypeID, o.TriggerStart1, o.OccurrenceName, pinfo.InsuranceProgramID, pinfo.InsuranceProgram, a.AllocationID, a.AllocationDescription, pinfo.CarrierSolvency, pinfo.CarrierID, pinfo.CarrierName, 
	pinfo.CarrierGroupID, pinfo.CarrierGroupName, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END, ar.DateToPresentValue, ar.OccurrenceID,
	ar.TriggerStartDateUsed, ar.DateTime, ar.TriggerEndDateUsed, YieldCurveDate'

--Declare the Parameters that will be passed to the created SQL string.
SET @prmDef = N'@SolvencyPrm nvarchar (50)'

--Run/Execute the SQL Statement Created
EXEC sp_executeSQL @strSQL, @prmDef, @SolvencyPrm = @tmpSolvency

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccByCarrierGroupByIP] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccByCarrierGroupByIP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccByCarrierGroupByIP] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccByCarrierGroupByIP] TO [power_user]
GO
