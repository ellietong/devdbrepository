SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create Proc [dbo].[qryfrmSelectClaimant] (@LastNameInitial nvarchar(5), @MatterID int)
AS 
Set nocount on


--Declare @LastNameInitial nvarchar(5)
--Set @LastNameInitial = 'B'

--Declare @MatterID int
--Set @MatterID = 2

Declare @MaxMatterID int
Set @MaxMatterID = (select MAX(matterid) from tblMatter)


Select cpi.ClaimantPersonalInfoID,
      Case when LastName is null then '[LastName]' else LastName end  +', '+
      Case when FirstName is null then '[FirstName]' else FirstName end +
      Case when MiddleName is null then '' else ' ' + MiddleName end +
      Case when Suffix is null then '' else ', ' + Suffix end +
      Case when SocialSecurityNumber is null then (Case when DocketNumber is null then '' else '  -  ' + DocketNumber end) else '  -  ' + Left(SocialSecurityNumber,3) + '-'+ Right(Left(SocialSecurityNumber,5),2) + '-'+ Right(SocialSecurityNumber,4) end +
      Case when Matter is null then '' else '  (' + Matter + ')' end as Claimant
From tblClaimantPersonalInfo cpi
left join tblClaimantSuffix cs on cs.SuffixID = cpi.SuffixID
left join tblclaim c on cpi.claimantpersonalinfoid = c.claimantpersonalinfoid
left join tblclaimlawsuit cl on c.claimid = cl.claimid
left join tbllawsuit l on cl.lawsuitid = l.lawsuitid
left join tblMatter m on m.matterid = c.matterid
where M.matterid >= COALESCE(@matterID, 0) AND m.matterid <= COALESCE(NULLIF(@MatterID, 0), @maxMAtterid)
and LastName like coalesce(@LastNameInitial + '%', '%')
group by cpi.ClaimantPersonalInfoID,
       Case when LastName is null then '[LastName]' else LastName end  +', '+
      Case when FirstName is null then '[FirstName]' else FirstName end +
      Case when MiddleName is null then '' else ' ' + MiddleName end +
      Case when Suffix is null then '' else ', ' + Suffix end +
      Case when SocialSecurityNumber is null then (Case when DocketNumber is null then '' else '  -  ' + DocketNumber end) else '  -  ' + Left(SocialSecurityNumber,3) + '-'+ Right(Left(SocialSecurityNumber,5),2) + '-'+ Right(SocialSecurityNumber,4) end +
      Case when Matter is null then '' else '  (' + Matter + ')' end
Order by 
      Case when LastName is null then '[LastName]' else LastName end  +', '+
      Case when FirstName is null then '[FirstName]' else FirstName end +
      Case when MiddleName is null then '' else ' ' + MiddleName end +
      Case when Suffix is null then '' else ', ' + Suffix end +
      Case when SocialSecurityNumber is null then (Case when DocketNumber is null then '' else '  -  ' + DocketNumber end) else '  -  ' + Left(SocialSecurityNumber,3) + '-'+ Right(Left(SocialSecurityNumber,5),2) + '-'+ Right(SocialSecurityNumber,4) end +
      Case when Matter is null then '' else '  (' + Matter + ')' end



return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSelectClaimant] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSelectClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSelectClaimant] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSelectClaimant] TO [power_user]
GO
