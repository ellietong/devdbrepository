SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding]
AS
--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'vicki.stringham@danacollc.com'
--DECLARE @firmid int
--SET @firmid = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

SELECT CAST(d.DocumentID as varchar(50)) as DocumentId,
dt.ClaimantDocumentType, 
dg.DocumentGroup, 	
d.Comments,
d.DocumentURL,
d.DocumentTitle,
d.SPDocId,
d.CreatedBy,
d.DateCreated,
d.ModifiedBy,
d.DateModified,
d.Accepted,
d.AcceptedBy,
d.DateAccepted,
d.ApprovedBy,
d.DateApproved,
f.Firm,
d.InvoiceNumber,
d.Period,
d.DatePayable,
d.FeeAmount,
d.DisbursementAmount,
d.Adjustment,
d.AdjustmentNotes,
dis.Status,	
dif.Firm DefenseInvoiceFirm, 	
m.Matter,
di.DatePaid, 
di.CheckNumber,
d.FirmID,
(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal
FROM tblDocuments d
JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 	
LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
WHERE dt.DocumentGroupID = 2 AND d.StatusID IS NULL
ORDER BY Accepted ASC, DateCreated DESC
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [power_user]
GO
