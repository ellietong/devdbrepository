SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_updDocumentByNCC]
(
	@DetailDocumentTypeID int,					--aka ClaimantDocumentTypeID
	@ClaimantPersonalInfoID bigint = null,	
	@Comments nvarchar(4000) = null,			--comments are optional		
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@SPDocId nvarchar(50),	
	@MatterID int=null,					--this is the guid assigned by SharePoint
	--@FirmID int,
	
	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,
    @Adjustment money = null,
    @AdjustmentNotes nvarchar(4000) = null,
    @StatusID int = null,

	@ModifiedBy nvarchar(250),
	@DateModified datetime,
	@Approved bit = null,
	@ApprovedBy nvarchar(50) = null,
	@DateApproved	datetime = null,
	@DocID int=null,
	@DocumentSourceID int,
	@FirmID int
)

AS
Update tblDocuments 
SET
	DetailDocumentTypeID	= @DetailDocumentTypeID,	
	ClaimantPersonalInfoID	= @ClaimantPersonalInfoID,
	Comments				= @Comments,	
	DocumentURL				= @DocumentURL,	
	DocumentTitle			= @DocumentTitle,	
	InvoiceNumber			= @InvoiceNumber,
	Period					= @Period,
	DatePayable				= @DatePayable,
	FeeAmount				= @FeeAmount,
	DisbursementAmount		= @DisbursementAmount,
    Adjustment				= @Adjustment,
    AdjustmentNotes			= @AdjustmentNotes,
    StatusID				= @StatusID,
	ModifiedBy				= @ModifiedBy,
	DateModified			= @DateModified,
	ApprovedBy				= @ApprovedBy,
	DateApproved			= @DateApproved,
	DocumentSourceID		= @DocumentSourceID,
	MatterID				= @MatterID,
	FirmID					= @FirmID
	
WHERE DocumentID = @DocID

Select DocumentID From tblDocuments Where DocumentID = @DocID
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_updDocumentByNCC] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_updDocumentByNCC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_updDocumentByNCC] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_updDocumentByNCC] TO [power_user]
GO
