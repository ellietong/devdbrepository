SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptDismissedCasesByJurisdiction_Excel] (@Matter    int,
															 @BeginDate datetime, 
															 @EndDate   datetime)
AS
SET NOCOUNT ON

--DECLARE @beginDate datetime
--DECLARE @EndDate   datetime
--DECLARE @Matter    int
--SET @beginDate =  '1/1/1900'
--SET @EndDate   = '2/28/2012'
--SET @Matter    = 1

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @DiseaseCount TABLE (Matter             nvarchar(100), 
							 MatterID           int, 
							 ClaimLawsuitStatus nvarchar(500),
							 MoYearDismissed    datetime, 
							 YearDismissed      datetime,
							 MesoCount          int, 
							 LungCancerCount    int, 
							 OtherCancerCount   int, 
							 NonMaligCount      int, 
							 UnknownCount       int, 
							 UnknownCount1      int)
INSERT INTO @DiseaseCount
	SELECT Matter, 
		C.MatterID, 
		ClaimLawsuitStatus,
		CONVERT(datetime,(CONVERT(nvarchar,MONTH(cst.statusDate))+'/1/'+ CONVERT(nvarchar,YEAR(cst.StatusDate)))) AS MoYearDismissed,
		CONVERT(nvarchar,YEAR(cst.StatusDate)) AS YearDismissed,
		CASE WHEN DMeso.HighLevelDiseaseID  = 1    THEN 1 ELSE 0 END AS MesoCount,
		CASE WHEN DLC.HighLevelDiseaseID    = 2    THEN 1 ELSE 0 END AS LungCancerCount,
		CASE WHEN DOC.HighLevelDiseaseID    = 3    THEN 1 ELSE 0 END AS OtherCancerCount,
		CASE WHEN DNM.HighLevelDiseaseID  IN (4,7) THEN 1 ELSE 0 END AS NonMaligCount,
		CASE WHEN DUnk.HighLevelDiseaseID IN (5,6) THEN 1 ELSE 0 END AS UnknownCount,
		CASE WHEN DUnk.HighLevelDiseaseID IS NULL  THEN 1 ELSE 0 END AS UnknownCount1
	FROM tblClaimantPersonalInfo cpi
		LEFT JOIN tblClaim c                        ON cpi.ClaimantPersonalINfoID = c.ClaimantPersonalInfoID
		LEFT JOIN (SELECT cd.ClaimDiseaseID, 
				       cd.ClaimID, 
				       cd.DiseaseDiagnosisDate, 
				       cd.DiseaseID, 
				       cd.DoctorID, 
				       cd.PrimaryDisease 
				   FROM tblClaimDisease cd 
				   WHERE PrimaryDisease = 1) cd     ON cd.ClaimID = c.ClaimID
		LEFT JOIN tblMatter m                       ON c.MatterID = m.MatterID
		LEFT JOIN tblClaimSequence csq              ON c.ClaimSequenceID = csq.ClaimSequenceID
		LEFT JOIN tblDisease dmeso                  ON cd.DiseaseID = dmeso.DiseaseID --Meso
		LEFT JOIN tblDisease dlc                    ON cd.DiseaseID = dlc.DiseaseID   --Lung Cancer
		LEFT JOIN tblDisease doc                    ON cd.DiseaseID = doc.DiseaseID   --Other Cancer
		LEFT JOIN tblDisease dnm                    ON cd.DiseaseID = dnm.DiseaseID   --Non-Malig
		LEFT JOIN tblDisease dunk                   ON cd.DiseaseID = dunk.DiseaseID  --Unknown
		LEFT JOIN tblClaimLawsuit cl                ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit L                      ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN tblClaimantSettlement cs          ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatusTracking cst ON cst.ClaimLawsuitID = cl.claimlawsuitid
		LEFT JOIN tblClaimLawsuitStatus css         ON css.ClaimLawsuitStatusID = cst.ClaimLawsuitStatusID
		LEFT JOIN tblClaimLawsuitStatusGroup csg    ON csg.ClaimLawsuitStatusID = cst.ClaimLawsuitStatusID
	WHERE COALESCE(IsPrimaryForClaim, 0) = 1   
		AND COALESCE(IsPrimaryStatus, 0) = 1
		AND csg.StatusGroupID = 3
		AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
		AND ProcessedDate BETWEEN COALESCE(@BeginDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) AND COALESCE(@EndDate, GETDATE())
		AND COALESCE(cpi.jobsiteinfo, 0) = 0
	ORDER BY YEAR(cst.StatusDate)

SELECT Matter,
	MatterID,
	YEAR(YearDismissed) AS 'Year Dismissed',
	CASE WHEN MONTH(MoYearDismissed) =  1 THEN 'January'
		 WHEN MONTH(MoYearDismissed) =  2 THEN 'February'
		 WHEN MONTH(MoYearDismissed) =  3 THEN 'March'
		 WHEN MONTH(MoYearDismissed) =  4 THEN 'April'
		 WHEN MONTH(MoYearDismissed) =  5 THEN 'May'
		 WHEN MONTH(MoYearDismissed) =  6 THEN 'June'
		 WHEN MONTH(MoYearDismissed) =  7 THEN 'July'
		 WHEN MONTH(MoYearDismissed) =  8 THEN 'August'
		 WHEN MONTH(MoYearDismissed) =  9 THEN 'September'
		 WHEN MONTH(MoYearDismissed) = 10 THEN 'October'
		 WHEN MONTH(MoYearDismissed) = 11 THEN 'November'
		 WHEN MONTH(MoYearDismissed) = 12 THEN 'December' END AS 'Month Dismissed', 
	SUM(MesoCount)                                                                                     AS 'Mesothelioma',
	SUM(LungCancerCount)                                                                               AS 'Lung Cancer', 
	SUM(OtherCancerCount)                                                                              AS 'Other Cancer',
	SUM(NonMaligCount)                                                                                 AS 'Non-Malignant', 
	SUM(UnknownCount + UnknownCount1)                                                                  AS 'Unknown',
	SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + UnknownCount1) AS 'Total',
	@BeginDate AS StartDate, 
	@EndDate AS EndDate	
FROM @DiseaseCount
GROUP BY Matter, MatterID, YearDismissed, MoYearDismissed

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissedCasesByJurisdiction_Excel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDismissedCasesByJurisdiction_Excel] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptDismissedCasesByJurisdiction_Excel] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissedCasesByJurisdiction_Excel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDismissedCasesByJurisdiction_Excel] TO [power_user]
GO
