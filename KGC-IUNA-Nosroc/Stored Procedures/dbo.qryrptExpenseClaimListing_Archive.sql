SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptExpenseClaimListing_Archive] (@TotalAllocatedExpense money, @serviceDate datetime, @header nvarchar(100), @Matter int)

AS
Set NoCount On

-- Declare @TotalAllocatedExpense money
-- Set @TotalAllocatedExpense = 500000.25
 
-- Declare @ServiceDate datetime
-- Set @ServiceDate = '10/1/2008'
--Declare @header nvarchar(100)
--Set @header = '3rd Quarter 2008 Billing'
--Delcare @Matter int
--Set @Matter = 1

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


Declare @DefenseExposure Table (ClaimLawsuitID bigint, FirstExposureDate datetime, FirstDiagnosisDate datetime, TotalAllocatedExpense money,
MatterID int, Matter nvarchar(250))
Insert Into @DefenseExposure
Select CL.ClaimLawsuitID, 
Case	When EarliestExposureDate <= '12/31/1968' And Coalesce(DefenseDiseaseDiagnosisDate,PlaintiffDiseaseDiagnosisDate)>='1/1/1968' Then '1/1/1968'
	When EarliestExposureDate between '1/1/1969'and '12/31/1969' Then '1/1/1969'
	When EarliestExposureDate between '1/1/1970'and '12/31/1970' Then '1/1/1970'
	When EarliestExposureDate between '1/1/1971'and '12/31/1971' Then '1/1/1971'
	When EarliestExposureDate between '1/1/1972'and '12/31/1972' Then '1/1/1972'
	When EarliestExposureDate between '1/1/1973'and '12/31/1973' Then '1/1/1973'
	When EarliestExposureDate between '1/1/1974'and '12/31/1974' Then '1/1/1974'
	When EarliestExposureDate between '1/1/1975'and '12/31/1975' Then '1/1/1975'
	When EarliestExposureDate between '1/1/1976'and '12/31/1976' Then '1/1/1976'
	When EarliestExposureDate between '1/1/1977'and '12/31/1977' Then '1/1/1977'
	When EarliestExposureDate between '1/1/1978'and '12/31/1978' Then '1/1/1978'
	When EarliestExposureDate between '1/1/1979'and '12/31/1979' Then '1/1/1979'
	When EarliestExposureDate between '1/1/1980'and '12/31/1980' Then '1/1/1980'
	When EarliestExposureDate between '1/1/1981'and '12/31/1981' Then '1/1/1981'
	When EarliestExposureDate between '1/1/1982'and '12/31/1982' Then '1/1/1982'
	When EarliestExposureDate >= '1/1/1983' And Coalesce(DefenseDiseaseDiagnosisDate,PlaintiffDiseaseDiagnosisDate)>'8/31/1982' Then '1/1/1983'
	End As FirstExposureDate,
Case	When EarliestExposureDate >= '1/1/1983' And Coalesce(DefenseDiseaseDiagnosisDate,PlaintiffDiseaseDiagnosisDate)>'8/31/1982' Then '7/1/2007'
	Else '9/1/1982' End As FirstDiagnosisDate,
@TotalAllocatedExpense as TotalAllocatedExpense,
c.MatterID, Matter
From tblClaimantPersonalInfo CPI
Left Join tblClaimantEmployment CE on CPI.ClaimantPersonalInfoID=CE.ClaimantPersonalInfoID
Left Join tblClaim C on CPI.ClaimantPersonalInfoID=C.ClaimantPersonalInfoID
Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblClaimantSettlement CS on CL.ClaimLawsuitID=CS.ClaimLawsuitID
Left Join tblMatter M on c.MatterID = m.MatterID
Where isPrimary=1 and isprimaryforclaim=1  and earliestexposuredate is not null and
(EarliestExposureDate >= '1/1/1968' or Coalesce(DefenseDiseaseDiagnosisDate,PlaintiffDiseaseDiagnosisDate) >= '1/1/1968') and
((L.ServiceDate<=@ServiceDate and claimlawsuitstatusID=22) or ((CL.StatusDate>@ServiceDate or CS.SettlementDate>@ServiceDate)and(L.ServiceDate<=@ServiceDate)))
Group By CL.ClaimLawsuitID, earliestExposureDate, DefenseDiseaseDiagnosisDate, PlaintiffDiseaseDiagnosisDate, c.MatterID, Matter
Having EarliestExposureDate < Coalesce(DefenseDiseaseDiagnosisDate,PlaintiffDiseaseDiagnosisDate)
Order By EarliestExposureDate

Declare @Counts Table (TotalAllocatedExpense float, TotalCount bigint, MatterID int)
Insert into @Counts
Select  @TotalAllocatedExpense as TotalAllocatedExpense, Count(ClaimLawsuitID) as TotalCount, MatterID
From @DefenseExposure
Group By MatterID

Select Count(ClaimLawsuitID) as CountOfClaims, TotalCount, FirstExposureDate,
	FirstDiagnosisDate, DE.TotalAllocatedExpense, @header as header, de.MatterID, Matter
From @DefenseExposure DE
Inner Join @Counts counts on Counts.TotalAllocatedExpense=DE.TotalAllocatedExpense and DE.MatterID = counts.MatterID
WHERE DE.MatterID >= COALESCE(@Matter,0) AND DE.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
Group By FirstExposureDate, FirstDiagnosisDate, DE.TotalAllocatedExpense, TotalCount, de.MatterID, Matter
Order By Matter, FirstExposuredate Desc

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExpenseClaimListing_Archive] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExpenseClaimListing_Archive] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExpenseClaimListing_Archive] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExpenseClaimListing_Archive] TO [power_user]
GO
