SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrpt1099Data] (@TaxYear int, @matterid int)
as
--This query is used to populate the 1099 forms for each tax year.
--Declare @TaxYear int
--Set @TaxYear = 2011

--Declare @matterid int
--Set @matterid = 1

Set nocount on


SELECT 'V'+cast(DI.FirmID as NCHAR) as PayeeID, DefenseCounsel as Payee, f.taxid as taxID, f.firmAddress1 as Address1, f.firmAddress2 as Address2, f.firmCity as City, f.firmZip as ZipCode, s.State, 0 as Box14, sum(PaymentAmount) as Box7,
@matterid as MatterID
FROM tblCheck C
INNER JOIN tblPaymentDefense PD ON PD.CheckID = C.CheckID
INNER JOIN tblDefenseInvoice DI ON DI.DefenseInvoiceID = PD.DefenseInvoiceID
LEFT JOIN tblDefenseCounselAddress DCA ON DCA.DefenseCounselAddressID = DI.DefenseCounselAddressID
left join sharesql.fbsecurity.dbo.tblFirmAddress f on dca.DefenseCounselID = f.FirmID
left join tblState s on s.StateID = f.firmstateID
WHERE  year(SourceDate)=  @TaxYear AND FundingTypeID = 1 and f.payeeaddress = 1 and di.MatterID = @matterid
GROUP BY DI.FirmID,DefenseCounsel, f.firmAddress1, f.taxid, f.firmAddress2 , firmCity,firmZip, s.State
 
 
Union

SELECT 'P'+cast(pca.PlaintiffCounselID as NCHAR), PCA.PlaintiffCounsel,PCA.PlaintiffCounselTaxIDNum, pca.PlaintiffCounselAddress1, pca.PlaintiffCounselAddress2, 
pca.PlaintiffCounselCity,pca.PlaintiffCounselZip, State, sum(ps.PaymentAmount) as Box14, 0 as Box7, @matterid
FROM tblCheck C
INNER JOIN tblPaymentSettlement PS ON PS.CheckID = C.CheckID
INNER JOIN tblClaimantSettlement CS ON CS.ClaimantSettlementID = PS.ClaimantSettlementID
INNER JOIN tblClaimLawsuit CL ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
INNER JOIN tblLawsuit L ON L.LawsuitID = CL.LawsuitID
inner join tblClaim m on m.ClaimID = cl.ClaimID
LEFT JOIN tblPlaintiffCounselAddress PCA ON PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
WHERE  year(SourceDate)=   @TaxYear  AND pca.payeeAddress = 1   and m.MatterID = @matterid
GROUP BY pca.PlaintiffCounselID, PCA.PlaintiffCounsel, PCA.PlaintiffCounselTaxIDNum, pca.PlaintiffCounselAddress1, pca.PlaintiffCounselAddress2, 
pca.PlaintiffCounselCity,pca.PlaintiffCounselZip, State
Order by Payee
 
return
rollback

 
 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrpt1099Data] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrpt1099Data] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrpt1099Data] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrpt1099Data] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrpt1099Data] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrpt1099Data] TO [power_user]
GO
