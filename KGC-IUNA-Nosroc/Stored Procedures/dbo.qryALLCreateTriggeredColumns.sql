SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryALLCreateTriggeredColumns] (@CorporateHistoryID nvarchar(20), @TriggerStart nvarchar(20), 
	@TriggerEnd nvarchar(20), 
	@AllocationID nvarchar(10), 
	@strInsuranceProfile nvarchar(1000), 
	@strVerticalCollapse nvarchar(200))
	
as 
SET NOCOUNT ON

/* --Test Information
Declare @CorporateHistoryID nvarchar(20)
Declare @TriggerStart nvarchar(20)
Declare @TriggerEnd nvarchar(20)
Declare @AllocationID nvarchar(10)
Declare @strInsuranceProfile nvarchar(1000)
Declare @strVerticalCollapse nvarchar(200)

Set @CorporateHistoryID = '13'
Set @TriggerStart = '1/1/1951'
Set @TriggerEnd = '7/25/2008'
Set @AllocationID = '3'
Set @strInsuranceProfile = '( InsuranceProgramID = 15) OR ( InsuranceProgramID = 14 AND (PStartDate < ''3/1/1985'') AND PEndDate > ''6/1/1974'')'
Set @strVerticalCollapse = ' AND ColumnPerOcc > 0.1'*/

Declare @strInsuranceProfileNew nvarchar(1000)
Set @strInsuranceProfileNew = Replace(Replace(@strInsuranceProfile, 'penddate', 'columnenddate'), 'pstartdate', 'columnstartdate')



Declare @strsql nvarchar(4000)
Set @strsql = 



'SELECT (CONVERT(int, (CASE WHEN ColumnEndDate < (Case When ''' + @TriggerEnd + ''' < dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+')  Then '''+ @TriggerEnd+''' Else dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+') End)
				THEN ColumnEndDate 
				ELSE (Case When ''' + @TriggerEnd + ''' < dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+')  Then '''+ @TriggerEnd+''' Else dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+') End) END) - 
				(CASE WHEN ColumnStartDate < (Case When '''+ @TriggerStart +''' < dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Then dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Else ''' + @TriggerStart + ''' End) 
				THEN (Case When '''+ @TriggerStart +''' < dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Then dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Else ''' + @TriggerStart + ''' End) 
				ELSE ColumnStartDate END))) AS TriggeredDays,
				(CASE WHEN ColumnStartDate < (Case When '''+ @TriggerStart +''' < dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Then dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Else ''' + @TriggerStart + ''' End) 
				THEN (Case When '''+ @TriggerStart +''' < dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Then dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Else ''' + @TriggerStart + ''' End) 
				ELSE ColumnStartDate END) as ColumnStartDate, 
				(CASE WHEN ColumnEndDate < (Case When ''' + @TriggerEnd + ''' < dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+')  Then '''+ @TriggerEnd+''' Else dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+') End)
				THEN ColumnEndDate 
				ELSE (Case When ''' + @TriggerEnd + ''' < dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+')  Then '''+ @TriggerEnd+''' Else dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+') End) END) as ColumnEndDate, InsuranceProgramID, ColumnPerOcc, ColumnCumAllocation, ColumnID 
FROM tblAllocationResultsColumn ac
WHERE AllocationID = ' + @AllocationID + ' 
AND (ColumnStartDate < ''' + @TriggerEnd + ''') 
AND (ColumnEndDate > ''' + @TriggerStart + ''') 
AND ('+ @strInsuranceProfileNew + ')' 
+ @strVerticalCollapse
+ ' Order By ColumnStartDate'

EXEC sp_executesql @strsql



RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLCreateTriggeredColumns] TO [Allocation_User]
GRANT EXECUTE ON  [dbo].[qryALLCreateTriggeredColumns] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLCreateTriggeredColumns] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLCreateTriggeredColumns] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLCreateTriggeredColumns] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLCreateTriggeredColumns] TO [power_user]
GO
