SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_PROFILE_qryUpdateAcceptedBy]
	@DocId int,
	@AcceptedBy NVarChar(250)
AS
BEGIN
	Update tblDocuments
	Set Accepted = 1,
	AcceptedBy = @AcceptedBy,
	DateAccepted = GetDate()
WHERE DocumentID = @DocId
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryUpdateAcceptedBy] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryUpdateAcceptedBy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryUpdateAcceptedBy] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryUpdateAcceptedBy] TO [power_user]
GO
