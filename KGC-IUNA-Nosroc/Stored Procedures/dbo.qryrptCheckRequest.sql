SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptCheckRequest]

(@FundsRequestStartDate datetime, @FundsRequestEndDate datetime, @Matter int)

As

SET NOCOUNT ON

--DECLARE @Matter int
--SET @Matter = 1

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblCashFlow


SELECT Matter, Firm, PayTo, Period, SubmitDate, InvoiceTotal, FirmAddress, InvoiceNumber, FundsRequestDate
FROM	tblDefenseInvoice DI
INNER JOIN	tblDefenseInvoiceFirm DIF ON DI.FirmID = DIF.FirmID
INNER JOIN	tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
LEFT JOIN   tblMatter M on DI.MatterID=M.MatterID
 
Where FundsRequestDate Between @FundsRequestStartDate and @FundsRequestEndDate
AND DI.MatterID >= COALESCE(@Matter,0) AND DI.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckRequest] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckRequest] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckRequest] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckRequest] TO [power_user]
GO
