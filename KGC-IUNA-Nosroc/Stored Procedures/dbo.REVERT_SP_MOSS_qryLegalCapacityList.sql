SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[REVERT_SP_MOSS_qryLegalCapacityList]
as
Select LegalCapacityID, LegalCapacity
From tblLegalCapacity
UNION
SELECT NULL, ''
Order By LegalCapacity
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryLegalCapacityList] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryLegalCapacityList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryLegalCapacityList] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryLegalCapacityList] TO [power_user]
GO
