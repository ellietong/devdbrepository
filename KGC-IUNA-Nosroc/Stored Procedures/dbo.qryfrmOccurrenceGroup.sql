SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmOccurrenceGroup]
As

SET NOCOUNT ON

SELECT OccurrenceGroupID, OccurrenceGroup, OccGrpTriggerStart, OccGrpDiscount
FROM tblOccurrenceGroup
ORDER BY OccurrenceGroupID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceGroup] TO [power_user]
GO
