SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  Procedure [dbo].[qryrptSubscriptionCoverageByCompany]
(@SubscriberCompanyID int = NULL, @Solvency varchar(50) = NULL, @InsuranceProgramID int = NULL, @limitTypeID int = Null)
AS 
SET NOCOUNT ON

--DECLARE @SubscriberCompanyID int 
--DECLARE @Solvency varchar(50) 
--DECLARE @InsuranceProgramID int 
--DECLARE @limitTypeID int
--
--SET @SubscriberCompanyID = 0
--SET @Solvency = null
--SET @InsuranceProgramID = 0
--SET @limitTypeID = 0

DECLARE @maxInsuranceProgramID int
DECLARE @maxSubscriberCompanyID int
DECLARE @maxLimitTypeID int

SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxInsuranceProgramID  = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxSubscriberCompanyID  = MAX(SubscriberID) FROM vSubscription

SELECT pinfo.InsuranceProgram, subs.SubscriptionCompany, subs.SubscriptionCompanySolvency, SUM(subs.Percentage *pinfo.PerOccLimit) AS SubscriberPerOcc, pinfo.CarrierID, pinfo.InsuranceProgramID, 
	subs.SubscriberID
FROM vPolicy pinfo INNER JOIN vSubscription subs ON pinfo.PolicyID = subs.PolicyID
WHERE pinfo.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) AND
	pinfo.limitTypeID >= COALESCE(@limitTypeID, 0) AND pinfo.limitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	subs.SubscriberID >= COALESCE(@SubscriberCompanyID, 0) AND subs.SubscriberID <= COALESCE(NULLIF(@SubscriberCompanyID, 0), @maxSubscriberCompanyID) AND
	subs.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%')
	and DoNotReport <>1
GROUP BY pinfo.InsuranceProgram, subs.SubscriptionCompany, subs.SubscriptionCompanySolvency, pinfo.CarrierID, pinfo.InsuranceProgramID, subs.SubscriberID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByCompany] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByCompany] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByCompany] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByCompany] TO [power_user]
GO
