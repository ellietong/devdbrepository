SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryALLTriggeredSig] (@AllocationID int, @NonSigAllocationID int, @LimitTypeID int)

As

Set NoCount On
	
--Declare @AllocationID int
--Set @AllocationID = 231
--Declare @NonSigAllocationID int
--Set @NonSigAllocationID = 228
--Declare @LimitTypeID int
--Set @LimitTypeID = 4

Declare @Temp table (SigAttachment money, Attachmentpoint money, PolicyID int, ColumnID int)
Insert Into @Temp
select Coalesce(dbo.SigDropDown(columnid, za.PolicyID, @limittypeID, Layer, @allocationID), za.AttachmentPoint) as SigAttachment, za.AttachmentPoint, za.PolicyID, ColumnID
from ztblPolicyAllocation za
	inner join ztblpolicycolumn zc on zc.policydateid = za.policydateid
	inner join tblPolicyLimits pl on pl.PolicyID = za.PolicyID
Where AllocationID = @allocationID and LimitTypeID = @LimitTypeID and OccurrenceRemaining > 0

Declare @Temp1 table (SigAttachment money, Attachmentpoint money, PolicyID int, ColumnID int, RankID int, Rank2 int)
Insert Into @Temp1
Select SigAttachment, Attachmentpoint, PolicyID, ColumnID, Dense_RANK () Over (Order By SigAttachment, ColumnID) as RankID,
	RANK () Over (Partition By SigAttachment, ColumnID Order By AttachmentPoint) as Rank2
From @Temp

Declare @Temp2 table (SigAttachment money, Attachmentpoint money, PolicyID int, ColumnID int, RankID int)
Insert Into @Temp2
Select SigAttachment, Attachmentpoint, PolicyID, ColumnID, Dense_RANK () Over (Order By SigAttachment, ColumnID) as RankID
From @Temp

Declare @Final table (FirstPolicyID int, SigAttachment money, Attachmentpoint money, PolicyID int, ColumnID int)
Insert @Final 
Select t.PolicyID, t2.SigAttachment, t2.Attachmentpoint, t2.PolicyID, t2.ColumnID
From @Temp1 t
Inner Join @Temp2 t2 on t.RankID= t2.RankID
Where Rank2 = 1

--Select FirstPolicyID, SigAttachment, Attachmentpoint, PolicyID, ColumnID
--From @Final Order BY FirstPolicyID, Attachmentpoint


Declare @ByPolicyDateIDOccFirst table (RankID2 int, PolicyID int, OriginalOccurrenceID int, ColumnID int)
Insert Into @ByPolicyDateIDOccFirst
	Select rank() over (partition by ar.PolicyID order by allocationresultID) as RankID,
		ar.PolicyID, ar.OccurrenceID, Null
	From tblAllocationResults ar		
	Where AllocationID = @NonSigAllocationID

Insert Into ztblPolicyDateIDTriggeredSig (PolicyID, AllocationID, OriginalOccurrenceID, ColumnID, BlockTriggered)
	Select f.PolicyID, @AllocationID, OriginalOccurrenceID, f.ColumnID, 0 as BlockTriggered
	From @ByPolicyDateIDOccFirst a
	Inner Join @Final f on f.FirstPolicyID = a.PolicyID 
	Where RankID2 = 1
	Order By ColumnID, SigAttachment, Attachmentpoint
	
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLTriggeredSig] TO [Allocation_User]
GRANT EXECUTE ON  [dbo].[qryALLTriggeredSig] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLTriggeredSig] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLTriggeredSig] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLTriggeredSig] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLTriggeredSig] TO [power_user]
GO
