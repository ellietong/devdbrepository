SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[WSS_CLAIMANT_qryGender]
as

Select GenderID, Gender
From tblGender
Order By Gender

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryGender] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryGender] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryGender] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryGender] TO [power_user]
GO
