SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_qryExternalEmailInvoiceSummary_NOSROC] 
	
AS
BEGIN
/*External Email to Marie Shipley which runs once a week.  The email includes two tables.
The first table is for invoices which have been uploaded within the past week.
The second table is for invoices which are over a week old but she has not yet approved or rejected. - BEH*/

--Declare and set sp_send_dbmail input variables
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(max)

SET @Subject = 	'Nosroc: Weekly Invoice Summary ' + CONVERT(varchar, GETDATE()-7, 107) + ' - ' + CONVERT(varchar, GETDATE(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top and the header for table 1
--KCIC logo is temporarily being pulled from D. Snyder's Google site	
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create temp table for invoices which have been uploaded in the past week
DECLARE @NewWeekInvoicesTable TABLE (spDocID nvarchar(100), [Status] nvarchar(30), invoiceNum nvarchar(100), 
		Period nvarchar(100), Fees  money, Disbursement money, Adjustment money, SubmitDate datetime, DatePayable datetime, Firm nvarchar(100))
INSERT INTO @NewWeekInvoicesTable

SELECT SPDocID, [Status], InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, DateCreated, DatePayable, DefenseCounsel
FROM tblDocuments d 
INNER JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
LEFT JOIN tblDefenseInvoiceStatus s ON d.StatusID=s.StatusID
INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
WHERE Datecreated BETWEEN GETDATE()-7 AND GETDATE() 
	AND cdt.DocumentGroupID = 2
	AND MatterID = 2
	AND FirmID <> 38
GROUP BY SPDocID, [Status], InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, DateCreated, DatePayable, DefenseCounsel

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
Set @BodyMessage = @BodyMessage + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	border: 1px solid #38160C;
	color: #F6ECF0;}
TD	{font-family: Goudy Old Style; 
	font-size: 10pt; 
	text-align: center;
	border: 1px solid rgb(0,121,193);
	padding: .3em;
	color: #000000}
TH {font-family: Goudy Old Style; 
	font-size: 10pt; 
	border: 1px solid #ffffff;
	padding: .3em;
	color: #F6ECF0;
	background: rgb(0,121,193);}
--->
</STYLE>'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @bodymessage + COALESCE('<H4>The following Nosroc invoices have been uploaded since ' + CONVERT(varchar, GETDATE()-7, 107) + ':</H4>' + N'<table>' + 
    N'<tr><th>Firm</th><th>Invoice Number</th><th>Period</th><th>Fees</th>' + 
	'<th>Expenses</th><th>Total Billed</th><th>Submit Date</th><th>Date Payable</th>' +
    CAST ( (
		SELECT  td = firm, '', td = invoicenum, '', td = period, '', 
				td = '$'+convert(nvarchar(30), coalesce(fees, 0), 1), '', 
				td = '$'+convert(nvarchar(30), coalesce(disbursement, 0), 1), '', 
				td = '$'+convert(nvarchar(30),(coalesce(fees, 0)+coalesce(disbursement, 0)), 1), '', 
				td = convert(nvarchar(100), submitdate, 101), '', 
				td = convert(nvarchar(100), datepayable, 101), ''
		FROM @NewWeekInvoicesTable i
			ORDER BY firm, submitdate
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	N'</table>' +  N'<br/>', '<H4>No Nosroc invoices have been uploaded since ' + CONVERT(varchar, GETDATE()-7, 107) + '.</H4>')

--Add header for table 2
--SET @BodyMessage = @BodyMessage + 

--Create temp table for invoices over a week old which have not yet been approved or rejected
DECLARE @OlderThanWeekInvoicesTable TABLE (spDocID nvarchar(100), [Status] nvarchar(30), invoiceNum nvarchar(100), 
		Period nvarchar(100), Fees  money, Disbursement money, Adjustment money, SubmitDate datetime, DatePayable datetime, Firm nvarchar(100))
INSERT INTO @OlderThanWeekInvoicesTable
SELECT SPDocID, [Status], InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, DateCreated, DatePayable, DefenseCounsel
FROM tblDocuments d 
INNER JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
LEFT JOIN tblDefenseInvoiceStatus s ON d.StatusID=s.StatusID
INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
WHERE Datecreated < GETDATE()-7 
	AND cdt.DocumentGroupID = 2
	AND d.StatusID IS NULL
	AND MatterID = 2
	AND FirmID <> 38
GROUP BY SPDocID, [Status], InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, DateCreated, DatePayable, DefenseCounsel


--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @BodyMessage + COALESCE('<H4>The following Nosroc invoices were uploaded before ' + CONVERT(varchar, GETDATE()-7, 107) + ' and remain unapproved.</H4>' + N'<table>' + 
    N'<tr><th>Firm</th><th>Invoice Number</th><th>Period</th><th>Fees</th>' + 
		'<th>Expenses</th><th>Total Billed</th><th>Submit Date</th><th>Date Payable</th>' +
    CAST ( (
		SELECT  td = firm, '', td = invoicenum, '', td = period, '', 
				td = '$'+convert(nvarchar(30), coalesce(fees, 0), 1), '', 
				td = '$'+convert(nvarchar(30), coalesce(disbursement, 0), 1), '', 
				td = '$'+convert(nvarchar(30),(coalesce(fees, 0)+coalesce(disbursement, 0)), 1), '', 
				td = convert(nvarchar(100), submitdate, 101), '', 
				td = convert(nvarchar(100), datepayable, 101), ''
		FROM @OlderThanWeekInvoicesTable i
		ORDER BY firm, submitdate
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
    N'</table>' +  N'<br/>',  '<H4>No unapproved Nosroc invoices before ' + CONVERT(varchar, GETDATE()-7, 107) + '.</H4>')

--Include link to invoice roll up site at the bottom
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Invoices.aspx">here</A> to access the invoice(s).</b><br><br>'

--Include KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC <br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

--Execute email-sending stored procedure
EXEC msdb.dbo.sp_send_dbmail

@profile_name = 'IUNA/NOSROCProfile',
@recipients = 'cjs@wlbdeflaw.com',
@copy_recipients = 'iuna_nosroc@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailInvoiceSummary_NOSROC] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailInvoiceSummary_NOSROC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailInvoiceSummary_NOSROC] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailInvoiceSummary_NOSROC] TO [power_user]
GO
