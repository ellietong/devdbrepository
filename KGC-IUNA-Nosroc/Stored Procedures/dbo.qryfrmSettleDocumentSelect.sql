SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmSettleDocumentSelect]
(@ClaimantPersonalInfoID int)

AS

--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 47000

SELECT DocumentID, ClaimantDocumentType + ': ' + DocumentTitle + ' (' + convert(nvarchar,month(DateCreated))+'/'+convert(nvarchar,day(DateCreated))+
	'/'+convert(nvarchar,Year(DateCreated)) + ')'
FROM tblDocuments d
LEFT JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
ORDER BY ClaimantDocumentType + ': ' + DocumentTitle + ' (' + convert(nvarchar,month(DateCreated))+'/'+convert(nvarchar,day(DateCreated))+
	'/'+convert(nvarchar,Year(DateCreated)) + ')'

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettleDocumentSelect] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettleDocumentSelect] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettleDocumentSelect] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettleDocumentSelect] TO [power_user]
GO
