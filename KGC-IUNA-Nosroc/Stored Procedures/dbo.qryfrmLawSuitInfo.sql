SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmLawSuitInfo] (@LawsuitID int)
as

Select LawsuitID,
                CaseCaption, 
                DocketNumber, 
                FileDate, 
                JurisdictionID, 
                StateID, 
                CourtTypeID, 
                ServiceDate, 
                ReceivedDate,
                PrimaryLocalPlaintiffCounsel, 
                PrimaryNationalPlaintiffCounsel, 
                LeadPlaintiffFirstName, 
                LeadPlaintiffLastName,
                PlaintiffCounselID,
                PlaintiffCounselAddressID,
                DefenseCounselAddressID
From tblLawsuit l 
Where LawsuitID = @LawsuitID
Order By FileDate desc, DocketNumber, CaseCaption

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawSuitInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawSuitInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawSuitInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawSuitInfo] TO [power_user]
GO
