SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryrptByPolicyByExpectedPayYearAllocationTriggers] (@allocationID int)
As
Set NoCount On

--Declare @AllocationID int
--Set @AllocationID = 231


Declare @NewTable TABLE (PolicyDateID int, ExpectedPayDate DateTime, TriggerStartYear nvarchar(2000))
DECLARE @ExpectedPayDateIDCursor DateTime, @TriggerStarts nvarchar(2000), @PolicyDateIDCursor int

DECLARE Cur CURSOR FOR
SELECT DISTINCT PolicyDateID, ExpectedPayDate
FROM tblAllocationResults ar
Inner Join tbloccurrence o on o.occurrenceID=ar.occurrenceID Where allocationID = @allocationID
OPEN Cur
FETCH NEXT FROM Cur INTO @PolicyDateIDCursor, @ExpectedPayDateIDCursor
WHILE @@FETCH_STATUS = 0 BEGIN
 SELECT @TriggerStarts = ISNULL(@TriggerStarts + ', ','') + Convert(Nvarchar(4),YEAR(TriggerStart1))
 From tblAllocationResults ar
 Inner Join tbloccurrence o on o.occurrenceID=ar.occurrenceID
 Where allocationID = @allocationID and ExpectedPayDate = @ExpectedPayDateIDCursor and PolicyDateID = @PolicyDateIDCursor
 Group By TriggerStart1
 Order By TriggerStart1
 INSERT @NewTable 
 VALUES(@PolicyDateIDCursor, @ExpectedPayDateIDCursor, @TriggerStarts)
 SET @TriggerStarts = NULL
 FETCH NEXT FROM Cur INTO @PolicyDateIDCursor, @ExpectedPayDateIDCursor
END
CLOSE Cur
DEALLOCATE Cur

Declare @AllocationResults table (PolicyDateID int , ExpectedPayDate datetime, AllocationIndemnity money, AllocationDefense money, AllocationDefenseOutsideLimits money)
Insert Into @AllocationResults
Select PolicyDateID, ExpectedPayDate, Sum(AllocationIndemnity), SUM(AllocationDefense), SUM(AllocationDefenseOutsideLimits)
From tblAllocationResults ar
 Inner Join tbloccurrence o on o.occurrenceID=ar.occurrenceID
Where AllocationID = @AllocationID
group By PolicyDateID, ExpectedPayDate


Select @AllocationID as AllocationID, Carriername, PolicyNum, AttachmentPoint, nt.PolicyDateID, PStartDate, PEndDate, nt.ExpectedPayDate, TriggerStartYear, AllocationIndemnity, AllocationDefense, AllocationDefenseOutsideLimits, PerOccLimit
From @NewTable NT
Left Join tblPolicyDates pd on pd.PolicyDateID=NT.PolicyDateID
Left Join tblPolicy p on p.PolicyID=pd.PolicyID
Left Join tblCarrier c on c.CarrierID=p.CarrierID
Left Join tblPolicyLimits pl on pl.PolicyID = p.PolicyID
Left Join @AllocationResults ar on ar.PolicyDateID =NT.PolicyDateID and ar.ExpectedPayDate=NT.ExpectedPayDate
Where LimitTypeID = 4


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptByPolicyByExpectedPayYearAllocationTriggers] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptByPolicyByExpectedPayYearAllocationTriggers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptByPolicyByExpectedPayYearAllocationTriggers] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptByPolicyByExpectedPayYearAllocationTriggers] TO [power_user]
GO
