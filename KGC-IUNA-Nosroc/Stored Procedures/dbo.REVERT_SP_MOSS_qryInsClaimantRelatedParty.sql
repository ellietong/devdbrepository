SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryInsClaimantRelatedParty]
(@FirstName nvarchar(100), @MiddleName nvarchar(100), @LastName nvarchar(100), 
@RelationshipID int, @LegalCapacityID int, @Address1 nvarchar(250), @Address2 nvarchar(250), 
@City nvarchar(250), @StateID int, @Zip nvarchar(100), @MailZip nvarchar(100), @Phone nvarchar(100), @Extension nvarchar(100),
@SSN nvarchar(50), @ClaimantPersonalInfoID bigint, @emailaddress nvarchar(100))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblClaimantRelatedParty
	SELECT @ClaimantPersonalInfoID, UPPER(@FirstName), UPPER(@MiddleName), UPPER(@LastName), @RelationshipID, NULL AS LegalCapacity, NULL AS NavInjPartyKey, NULL AS NavRelationship,
		   @LegalCapacityID, UPPER(@Address1), UPPER(@Address2), UPPER(@City), @StateID,
		   @Zip, @MailZip, @Phone, @Extension, @SSN, NULL, @Emailaddress
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryInsClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryInsClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryInsClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryInsClaimantRelatedParty] TO [power_user]
GO
