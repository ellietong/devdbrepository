SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailUnaccepted4DaysComplaints]
AS
BEGIN
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

Declare @4daysold datetime
SET @4daysold = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 4)) + ' 16:00:00.000')

SET @Subject = 'IUNA/Nosroc: Unaccepted Complaints/Documents Over 4 Days Old'
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (docTitle nvarchar (50), state nvarchar (50), FirmID int, Name nvarchar (250),
dockNum nvarchar (50), serviceDate datetime, Matter nvarchar (100))
INSERT INTO @NewComplaintsTable
	SELECT
		d.DocumentTitle,
		[state],
		FirmID,
		cpi.LastName + ', ' + cpi.FirstName + ' (' + CONVERT(varchar(50), cpi.ClaimantPersonalInfoID) + ')',
		DocketNumber,
		ServiceDate,
		Matter
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.lawsuitid
	INNER JOIN tblState s ON s.StateID = l.stateid
	INNER JOIN tblMatter m ON c.MatterID = m.MatterID
	WHERE d.DateCreated < @4daysold
	AND detailDocumentTypeID IN (8, 4, 33)
	AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	AND FirmID IS NOT NULL
	AND COALESCE(Accepted, 0) <> 1
	AND cl.IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	GROUP BY	d.DocumentTitle,
				[state],
				FirmID,
				cpi.LastName + ', ' + cpi.FirstName + ' (' + CONVERT(varchar(50), cpi.ClaimantPersonalInfoID) + ')',
				DocketNumber,
				ServiceDate,
				Matter


DECLARE @NewDocuments TABLE (docTitle nvarchar (100), FirmID int, Name nvarchar (250),
Matter nvarchar (100), DocType nvarchar (150))
INSERT INTO @NewDocuments
	SELECT
		d.DocumentTitle,
		FirmID,
		cpi.LastName + ', ' + cpi.FirstName + ' (' + CONVERT(varchar(50), cpi.ClaimantPersonalInfoID) + ')',
		Matter,
		ClaimantDocumentType
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblMatter m ON c.MatterID = m.MatterID
	LEFT JOIN tblClaimantDocumentType ct ON ct.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	WHERE datecreated < GETDATE() - 4
	AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	AND detailDocumentTypeID NOT IN (40, 41, 8, 4)
	AND FirmID IS NOT NULL
	AND COALESCE(Accepted, 0) <> 1
	AND cl.IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1
	GROUP BY	d.DocumentTitle,
				FirmID,
				cpi.LastName + ', ' + cpi.FirstName + ' (' + CONVERT(varchar(50), cpi.ClaimantPersonalInfoID) + ')',
				Matter,
				ClaimantDocumentType


--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

DECLARE @firmid nvarchar (10), @firmURL nvarchar (100)

DECLARE firm_Curs CURSOR FOR SELECT
	nct.FirmID,
	'https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Documents.aspx'
FROM @NewComplaintsTable nct
GROUP BY nct.FirmID UNION SELECT
	doc.firmid,
	'https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Documents.aspx'
FROM @NewDocuments doc
GROUP BY doc.firmid


OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @firmid, @firmURL
WHILE @@FETCH_STATUS = 0
BEGIN

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH    {border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
--->
</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'


--Declare a cursor and variables to be used in the cursor to create a separate table of complaints for each matter
DECLARE @NumberPerMatter int
DECLARE @Matter nVarChar (20)
DECLARE @Type nVarChar (20)

DECLARE matterDocs_Curs CURSOR FOR SELECT
	COUNT(docTitle),
	Matter,
	'Complaint(s): '
FROM @NewComplaintsTable
WHERE FirmID = (SELECT
	@FirmID)
GROUP BY Matter UNION SELECT
	COUNT(docTitle),
	Matter,
	'Document(s): '
FROM @NewDocuments
WHERE FirmID = (SELECT
	@FirmID)
GROUP BY Matter
ORDER BY Matter

OPEN matterDocs_curs

FETCH NEXT FROM matterDocs_curs INTO @NumberPerMatter, @Matter, @Type

WHILE @@fetch_status = 0
BEGIN

IF (SELECT
	COUNT(docTitle)
FROM @NewComplaintsTable
WHERE FirmID = @firmid) = 0
begin
SET @BodyMessage = @BodyMessage + '<H4>There are currently no outstanding complaints. </H4>'
end


--Header line for each matter's table
SET @BodyMessage = @BodyMessage + '<br>' + CONVERT(nvarchar(5), (SELECT
	@NumberPerMatter)
) + ' ' + (SELECT
	@Matter)
+ ' ' + @Type

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
if @Type = 'Complaint(s): '
begin
SET @BodyMessage = @BodyMessage + '<H4>The following complaints were uploaded more than 4 days ago and have not yet been accepted:  </H4>'

SET @BodyMessage = @BodyMessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0">' +
N'<tr><th style = "border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style = "border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Name</th><th style = "border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Civil Action Number</th>' +
'<th style = "border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Service Date</th>' + '<th style = "border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">State</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		docTitle)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		Name)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		dockNum)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(varchar, servicedate, 101))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		[state])
	AS td,
	''
FROM @NewComplaintsTable C
WHERE c.Matter = (SELECT
	@Matter)
AND c.FirmID = (SELECT
	@firmid)
ORDER BY serviceDate, [state], Name
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'


IF (SELECT
	COUNT(docTitle)
FROM @NewDocuments
WHERE FirmID = @firmid) = 0
begin
SET @BodyMessage = @BodyMessage + '<H4>There are currently no other documents awaiting acceptance. </H4>'
end
end
else
begin


SET @BodyMessage = @BodyMessage + '<H4>The following documents were uploaded more than 4 days ago and have not yet been accepted:  </H4>'

SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Name</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		docTitle)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		Name)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		DocType)
	AS td,
	''
FROM @NewDocuments C
WHERE c.Matter = (SELECT
	@Matter)
AND c.FirmID = (SELECT
	@firmid)
ORDER BY DocType, Name
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'
end


--Repeat for next matter
FETCH NEXT FROM matterDocs_curs INTO @NumberPerMatter, @Matter, @Type
END
CLOSE matterDocs_curs
DEALLOCATE matterDocs_curs

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT
	@firmURL)
+ '>here</A> to access the document(s).</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC <br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


DECLARE @RecipientList nvarchar (500)
SET @RecipientList = ''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar (500)
DECLARE contact_curs CURSOR FOR SELECT
	email
FROM vtblPerson p
WHERE FirmID = @FirmID
AND complaintContact = 1

OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS = 0
BEGIN

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								--@recipients = 'scottc@kcic.com',
								@recipients = @RecipientList,
								@blind_copy_recipients = 'iuna_nosroc@kcic.com',
								--@recipients = 'snyderd@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'

--Move to the next firm to create a new email
FETCH NEXT FROM firm_curs INTO @FirmID, @firmurl
END
CLOSE firm_curs
DEALLOCATE firm_curs

END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GO
