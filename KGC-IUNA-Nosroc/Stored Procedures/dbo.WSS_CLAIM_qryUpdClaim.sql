SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryUpdClaim]
	(@ClaimDiseaseID bigint, @DiseaseID int=null, @ActiveTrialDate datetime=null, @LawsuitID bigint,
	@Username nvarchar(250),
    @ClaimID bigint, @DiseaseDiagnosisDate  datetime= null
	
	)
AS
BEGIN
	UPDATE tblClaimDisease
	SET DiseaseID = @DiseaseID,
		DiseaseDiagnosisDate = @DiseaseDiagnosisDate,
	    SPUsername = @Username
	WHERE ClaimDiseaseID = @ClaimDiseaseID
	
	UPDATE tblLawsuit
	SET SPUsername = @Username
	WHERE LawsuitID = @LawsuitID
	
	UPDATE tblClaimLawsuit
	SET ActiveTrialDate = @ActiveTrialDate,
		SPUsername = @Username
	WHERE LawsuitID = @LawsuitID AND ClaimID = @ClaimID
	
	--UPDATE tblClaim
	--SET ILODoctorID = @ILODoctorID,
 --     ILOProfusion = @ILOProfusion,
 --     ILODate = @ILODate,
 --     PFTDoctorID = @PFTDoctorID,
 --     PFTDate = @PFTDate,
 --     PFTTypeID = @PFTTypeID,
 --     PFTFVC_Actual = @PFTFVC_Actual,
 --     PFTFVC_Predicted = @PFTFVC_Predicted,
 --     PFTFEV1_Actual = @PFTFEV1_Actual,
 --     PFTFEV1_Predicted = @PFTFEV1_Predicted,
 --     [PFTFEV1andFVC_Actual] = @PFTFEV1FVC_Actual,
 --     [PFTFEV1andFVC_Predicted] = @PFTFEV1FVC_Predicted,
 --     PFTTLC_Actual = @PFTTLC_Actual,
 --     PFTTLC_Predicted = @PFTTLC_Predicted,
 --     PFTDLCO_Actual = @PFTDLCO_Actual,
 --     PFTDLCO_Predicted = @PFTDLCO_Predicted,
 --     PFTTV = COALESCE(@PFTTV,PFTTV),
 --     SPUsername = @Username
 --     WHERE ClaimID = @ClaimID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GO
