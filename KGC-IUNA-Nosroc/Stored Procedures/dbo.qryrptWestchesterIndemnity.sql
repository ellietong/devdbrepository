SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptWestchesterIndemnity] (@SettlementDateBegin datetime, @SettlementDateEnd datetime,@ExecutedReleaseBegin datetime, @ExecutedReleaseEnd datetime, @header nvarchar(100), @Matter int)

As
SET NOCOUNT ON

-- Declare @SettlementDateBegin datetime
-- Declare @SettlementDateEnd datetime
-- Set @SettlementDateBegin='9/1/2010'
--Set @SettlementDateEnd='2/1/2011'
-- Declare @ExecutedReleaseBegin datetime
-- Declare @ExecutedReleaseEnd datetime
-- Set @ExecutedReleaseBegin='1/1/2011'
--Set @ExecutedReleaseEnd='2/1/2011'
--Declare @header nvarchar(100)
--set @header = '3rd Quarter 2008 Billing'
--Declare @Matter int
--Set @Matter = 1

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Select 	C.ClaimID, LastName, FirstName, Lastname+', '+FirstName as ClaimantName, 'XXX-XX-'+ Right([SocialSecurityNumber],4) as SocialSecurityNumber, State,
	HighLevelDisease, FileDate, ExecutedReleaseDate, SettlementDate, dbo.fnEarliestExposure(cpi.claimantpersonalinfoid) as  EarliestExposureDate,
	DiseaseDiagnosisDate as DiagnosisDate, SettlementAmount,
	Coalesce(@SettlementDateBegin,'1/1/1900') as PaidDateBegin,
	Coalesce(@SettlementDateEnd,convert(varchar(10),getdate(),101)) as SettlementDateEnd, BirthDate, DeceasedDate, Address1 + ' '+ coalesce(Address2, '') + ' ' + City + ', '+ State + ' '+ Zip as ClaimantAddress, DocketNumber, Occupation,
	Coalesce(HealthInsuranceClaimNumber, 'N/A') as HealthInsuranceClaimNumber, 
	@header as header, CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
	, CASE WHEN GenderID = 2 THEN 'F' WHEN GenderID = 3 THEN 'M' ELSE 'U' END AS Gender,
	 Jurisdiction + ' ' + CourtType As Court, dbo.fnlatestexposure(cpi.claimantpersonalinfoid) as LatestExposureDate, SettlementGroupID, Disease + '-'+ ICD9 as Disease
	
From tblClaimantPersonalInfo CPI
Left Join tblClaimantEmployment CE on CPI.ClaimantPersonalInfoID=CE.ClaimantPersonalInfoID
Left Join tblClaim C on CPI.ClaimantPersonalInfoID=C.ClaimantPersonalInfoID
Left Join (select * from tblClaimDisease where PrimaryDisease = 1) cd on cd.ClaimID = c.ClaimID
Left Join tblDisease D on cd.DiseaseID=D.DiseaseID
Left Join tblHighLevelDisease HLD on D.HighLevelDiseaseID=HLD.HighLevelDiseaseID
Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Inner Join tblClaimantSettlement CS on CL.ClaimLawsuitID=CS.CLaimLawsuitID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblState S on L.StateID=S.StateID
Left Join tblMatter M on C.MatterID = m.MatterID
left join tblCourtType ct on ct.CourtTypeID = L.CourtTypeID
left join tblJurisdiction j on j.JurisdictionID = L.JurisdictionID
left join tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID =CL.ClaimLawsuitID


Where IsPrimary=1 
and (SettlementDate>= Coalesce(@SettlementDateBegin,'1/1/1900') 
AND SettlementDate<= Coalesce(@SettlementDateEnd,convert(varchar(10),getdate(),101)))
AND ExecutedReleaseDate Between @ExecutedReleaseBegin AND @ExecutedReleaseEnd
and c.MatterID >= COALESCE(@Matter,0) AND c.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
and SettlementAmount <> 0
GROUP BY C.ClaimID, Lastname+', '+FirstName, 'XXX-XX-'+ Right([SocialSecurityNumber],4), State, HighLevelDisease,
	FileDate, ExecutedReleaseDate, SettlementDate, CPI.ClaimantPersonalInfoID,
	DiseaseDiagnosisDate, LastName, FirstName,
	SettlementAmount,  BirthDate, DeceasedDate, Address1 + ' '+ coalesce(Address2, '') + ' ' + City + ', '+ State + ' '+ Zip, DocketNumber, Occupation,
	Coalesce(HealthInsuranceClaimNumber, 'N/A'), GenderID, CourtType, Jurisdiction, State, SettlementGroupID, Disease + '-'+ICD9
Order By Lastname+', '+FirstName, LastName, FirstName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptWestchesterIndemnity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptWestchesterIndemnity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptWestchesterIndemnity] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptWestchesterIndemnity] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptWestchesterIndemnity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptWestchesterIndemnity] TO [power_user]
GO
