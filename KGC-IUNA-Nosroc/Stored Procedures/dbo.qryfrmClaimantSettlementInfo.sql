SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantSettlementInfo] (@ClaimLawsuitID int)
as

Select *
From tblClaimantSettlement cs
Where ClaimLawsuitID = @ClaimLawsuitID
Order By SettlementDate desc

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementInfo] TO [power_user]
GO
