SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmSettleDocsCategoryEntry]

as

SELECT DocCategoryID, DocCategory
FROM tblDocumentCategory
ORDER BY DocCategory


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettleDocsCategoryEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettleDocsCategoryEntry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettleDocsCategoryEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettleDocsCategoryEntry] TO [power_user]
GO
