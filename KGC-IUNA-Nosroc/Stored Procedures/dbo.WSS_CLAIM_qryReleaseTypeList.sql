SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryReleaseTypeList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ReleaseTypeID, ReleaseType
	FROM tblReleaseType
	WHERE ReleaseTypeID IN (2, 4)
	UNION
	SELECT NULL, '-- Release Type --'
	ORDER BY ReleaseType
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryReleaseTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryReleaseTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryReleaseTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryReleaseTypeList] TO [power_user]
GO
