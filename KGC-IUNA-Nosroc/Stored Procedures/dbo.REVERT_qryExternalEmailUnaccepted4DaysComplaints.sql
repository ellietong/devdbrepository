SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] 
AS
BEGIN
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)

Declare @4daysold datetime
SET @4daysold = convert(datetime,convert(nvarchar,convert(date, GETDATE()-4))+' 16:00:00.000')

SET @Subject =    'IUNA/Nosroc: Unaccepted Complaints/Documents Over 4 Days Old'
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (docTitle nvarchar(50), state nvarchar(50), FirmID int, Name nvarchar(250), 
        dockNum nvarchar(50), serviceDate datetime, Matter nvarchar(100))
INSERT INTO @NewComplaintsTable
SELECT d.DocumentTitle,
      [state], FirmID, cpi.LastName + ', '+ cpi.FirstName+' ('+convert(varchar(50),cpi.ClaimantPersonalInfoID)+')', DocketNumber, ServiceDate, Matter
FROM tblDocuments d
	  INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
      INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
      INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
      INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
      INNER JOIN tblLawsuit l ON cl.LawsuitID = l.lawsuitid
      INNER JOIN tblState s ON s.StateID = l.stateid
      INNER JOIN tblMatter m ON c.MatterID = m.MatterID
WHERE mcd.QCDate < @4daysold
      AND detailDocumentTypeID IN (8,4,33)
      AND RIGHT(CreatedBy, 11) = 'kcicllc.com'
      AND FirmID IS NOT NULL
      AND coalesce(Accepted,0) <> 1
      AND cl.IsPrimaryForClaim = 1
      and IsCurrentClaim = 1
GROUP BY d.DocumentTitle,
      [state], FirmID, cpi.LastName + ', '+ cpi.FirstName+' ('+convert(varchar(50),cpi.ClaimantPersonalInfoID)+')', DocketNumber, ServiceDate, Matter
      
      
DECLARE @NewDocuments TABLE (docTitle nvarchar(100), FirmID int, Name nvarchar(250), 
           Matter nvarchar(100), DocType nvarchar(150))
INSERT INTO @NewDocuments
SELECT d.DocumentTitle, FirmID, cpi.LastName + ', '+ cpi.FirstName+' ('+convert(varchar(50),cpi.ClaimantPersonalInfoID)+')', 
 Matter, ClaimantDocumentType
FROM tblDocuments d
	  INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
      INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
      INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
      INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
      INNER JOIN tblMatter m ON c.MatterID = m.MatterID
      left join tblClaimantDocumentType ct on ct.ClaimantDocumentTypeID = d.DetailDocumentTypeID
WHERE datecreated < GETDATE()-4
	and RIGHT(CreatedBy, 11) = 'kcicllc.com'
	AND detailDocumentTypeID NOT IN (40,41, 8, 4)
	AND FirmID IS NOT NULL
	AND coalesce(Accepted,0) <> 1
        AND cl.IsPrimaryForClaim = 1
      and IsCurrentClaim=1
GROUP BY d.DocumentTitle, FirmID, cpi.LastName + ', '+ cpi.FirstName+' ('+convert(varchar(50),cpi.ClaimantPersonalInfoID)+')', 
 Matter, ClaimantDocumentType   
      
      
--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

DECLARE @firmid nvarchar(10), @firmURL nvarchar(100)

DECLARE firm_Curs CURSOR FOR

SELECT nct.FirmID, 
'https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Documents.aspx'
FROM @NewComplaintsTable nct
GROUP BY nct.FirmID 
Union
Select doc.firmid, 
'https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Documents.aspx'
from @NewDocuments doc
group by doc.firmid


OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @firmid, @firmURL
WHILE @@FETCH_STATUS = 0
BEGIN

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH    {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

--Declare a cursor and variables to be used in the cursor to create a separate table of complaints for each matter
DECLARE @NumberPerMatter int
DECLARE @Matter nVarChar(20)
DECLARE @Type nVarChar(20)

DECLARE matterDocs_Curs CURSOR FOR
            
SELECT COUNT(docTitle), Matter, 'Complaint(s): '
FROM @NewComplaintsTable 
WHERE FirmID = (SELECT @FirmID)
GROUP BY Matter
Union
SELECT COUNT(docTitle), Matter, 'Document(s): '
FROM @NewDocuments 
WHERE FirmID = (SELECT @FirmID)
GROUP BY Matter
ORDER BY Matter

OPEN matterDocs_curs 

FETCH NEXT FROM matterDocs_curs INTO @NumberPerMatter, @Matter, @Type

WHILE @@fetch_status = 0
BEGIN

IF (Select COUNT(docTitle) from @NewComplaintsTable where FirmID = @firmid) =0
begin
Set @BodyMessage = @BodyMessage + '<H4>There are currently no outstanding complaints. </H4>'
end


--Header line for each matter's table
SET @BodyMessage = @BodyMessage + '<br>'+ convert(nvarchar(5), (select @NumberPerMatter))+ ' ' + (select @Matter) + ' ' + @Type
 
--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
if @Type = 'Complaint(s): '
begin 
set @BodyMessage = @BodyMessage+ '<H4>The following complaints were uploaded more than 4 days ago and have not yet been accepted:  </H4>'

SET @BodyMessage = @BodyMessage + N'<table>' + 
            N'<tr><th>Document Title</th><th>Claimant Name</th><th>Civil Action Number</th>' + 
            '<th>Service Date</th>' + '<th>State</th>' +
            CAST ( (
                  SELECT  td = docTitle, '', td = Name, '', 
                              td = dockNum, '', 
                              td = CONVERT(varchar, servicedate, 101), '',
                              td = [state], ''
                  FROM @NewComplaintsTable C
                  WHERE c.Matter = (SELECT @Matter)
                        AND c.FirmID = (select @firmid)
                  ORDER BY serviceDate, [state], Name
            for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
            N'</table>' +  N'<br/>'      
            
            
	IF (Select COUNT(docTitle) from @NewDocuments where FirmID = @firmid) =0
	begin
	Set @BodyMessage = @BodyMessage + '<H4>There are currently no other documents awaiting acceptance. </H4>'
	end 
end
else
begin


set @BodyMessage = @BodyMessage+ '<H4>The following documents were uploaded more than 4 days ago and have not yet been accepted:  </H4>'

SET @BodyMessage = @BodyMessage + N'<table>' + 
            N'<tr><th>Document Title</th><th>Claimant Name</th><th>Document Type</th>' + 
            CAST ( (
                  SELECT  td = docTitle, '', td = Name, '', 
                              td = DocType, ''
                  FROM @NewDocuments C
                  WHERE c.Matter = (SELECT @Matter)
                        AND c.FirmID = (select @firmid)
                  ORDER BY DocType, Name
            for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
            N'</table>' +  N'<br/>' 
end


--Repeat for next matter
FETCH NEXT FROM matterDocs_curs INTO @NumberPerMatter, @Matter, @Type
END               
CLOSE matterDocs_curs
DEALLOCATE matterDocs_curs

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC <br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

  
DECLARE @RecipientList nvarchar(500)
SET @RecipientList=''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar(500)
DECLARE contact_curs CURSOR FOR

SELECT email 
FROM vtblPerson p
WHERE FirmID = @FirmID
      AND complaintContact = 1
      
OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS=0
BEGIN 

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail

@profile_name='IUNA/NosrocProfile',
--@recipients = 'scottc@kcicllc.com',
@recipients = @RecipientList,
@copy_recipients = 'iuna_nosroc@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

--Move to the next firm to create a new email
FETCH NEXT FROM firm_curs INTO @FirmID, @firmurl
END
CLOSE firm_curs
DEALLOCATE firm_curs

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GO
