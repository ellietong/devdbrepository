SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryrptWestchesterHighLevel] (@SettlementDateBegin datetime, @SettlementDateEnd datetime,@ExecutedReleaseBegin datetime, @ExecutedReleaseEnd datetime, @header nvarchar(100), @Matter int)

As

SET NOCOUNT ON

-- Declare @SettlementDateBegin datetime
-- Declare @SettlementDateEnd datetime
-- Set @SettlementDateBegin='10/1/2010'
--Set @SettlementDateEnd='6/30/2012'
-- Declare @ExecutedReleaseBegin datetime
-- Declare @ExecutedReleaseEnd datetime
-- Set @ExecutedReleaseBegin='6/1/2012'
--Set @ExecutedReleaseEnd='6/30/2012'
--Declare @header nvarchar(100)
--set @header = 'March 2011'
--Declare @Matter int
--Set @Matter = 1

--7/25/12 - AG - Update query to account for capped WLB fees at $325k.

Declare @InvoiceDateStart datetime
Set @InvoiceDateStart=@SettlementDateBegin
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblDefenseInvoice

Declare @defense table (firmid int, defensetotal money)
insert into @defense
Select di.firmid,
case when max(di.firmID)=21 and @Matter=1 Then sum(325000.00+disbursementamount)
when max(di.FirmID)=21 and @Matter=2 then (0+sum(DisbursementAmount)) Else sum(FeeAmount+DisbursementAmount-Adjustment)
	END as DefenseTotal 
from tblDefenseInvoice di inner join tblDefenseInvoiceFirm DIF ON DI.FirmID = DIF.FirmID
left JOIN tblPaymentDefense PD ON PD.DefenseInvoiceID = DI.DefenseInvoiceID
left JOIN tblCheck CK ON CK.CheckID = PD.CheckID
WHERE	((CK.SourceDate between @ExecutedReleaseBegin and @ExecutedReleaseEnd and ck.fundingsourceid <> 3)
OR (DI.FirmID = 28 AND di.datepaid between @ExecutedReleaseBegin and @ExecutedReleaseEnd)
AND (DI.StatusID IN (1, 2)) AND (di.SubmitDate IS NOT NULL))
AND DI.MatterID = @Matter
group by di.FirmID, firm



Select coalesce(sum(SettlementAmount),0) as IndemnityTotal, (Select Matter from tblMatter Where MatterID= @Matter) As Matter,
coalesce((select sum(defensetotal) from @defense),0) As DefenseTotal
 
	
from tblClaimantSettlement CS 
left join tblClaimLawsuit cl on cl.ClaimLawsuitID = CS.ClaimLawsuitID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
left join tblClaim c on c.ClaimID = cl.ClaimID
left join tblMatter m on m.MatterID= c.MatterID
left join tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID
Where  (SettlementDate>= @SettlementDateBegin
AND SettlementDate<= Coalesce(@SettlementDateEnd,convert(varchar(10),getdate(),101)) )
AND ExecutedReleaseDate Between @ExecutedReleaseBegin AND @ExecutedReleaseEnd
and c.MatterID >= COALESCE(@Matter,0) AND c.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
and SettlementAmount <> 0


RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptWestchesterHighLevel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptWestchesterHighLevel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptWestchesterHighLevel] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptWestchesterHighLevel] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptWestchesterHighLevel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptWestchesterHighLevel] TO [power_user]
GO
