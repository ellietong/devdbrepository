SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  Procedure [dbo].[qryrptPolicyByProvision]
(@insuranceProgramID int = NULL, @limitTypeID int = NULL, @carrierID int = NULL, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL, @namedInsuredID int = NULL, 
@lowLayer int = NULL, @highLayer int = NULL, @lowAttach nvarchar(50) = NULL, @highAttach nvarchar(50) = NULL, @provisionLabelID int = NULL, @provisionValueID int = NULL, @startDate int = NULL, @endDate int = NULL)
As

SET NOCOUNT ON
SET ANSI_WARNINGS OFF

--DECLARE @insuranceProgramID int 
--DECLARE @limitTypeID int
--DECLARE @carrierID int 
--DECLARE @carrierGroupID int 
--DECLARE @carrierSolvency varchar(50) 
--DECLARE @namedInsuredID int 
--DECLARE @lowLayer int 
--DECLARE @highLayer int 
--DECLARE @lowAttach nvarchar(50) 
--DECLARE @highAttach nvarchar(50) 
--DECLARE @provisionLabelID int 
--DECLARE @provisionValueID int 
--DECLARE @startdate int
--DECLARE @enddate int

--SET @insuranceProgramID = 0
--SET @limitTypeID = 0
--SET @carrierID = 0
--SET @carrierGroupID = 0
--SET @carrierSolvency = null
--SET @namedInsuredID = 0
--SET @lowLayer = 0
--SET @highLayer = 0
--SET @lowAttach = null
--SET @highAttach = null
--SET @provisionLabelID = 0
--SET @provisionValueID = 0
--SET @startdate = 1970
--SET @enddate = 1980
-- Variables hold maximum values
DECLARE @lowAttachNum money
DECLARE @highAttachNum money
DECLARE @maxInsuranceProgram int
DECLARE @maxLimitTypeID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)
DECLARE @maxLayer int
DECLARE @maxAttach int
DECLARE @maxNamedInsuredID int
DECLARE @maxProvisionValueID int
DECLARE @maxProvisionLabelID int
DECLARE @minProvisionValueID int
DECLARE @maxEndDate int

-- Find max ID values
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxLayer = MAX(Layer) FROM vPolicy
SELECT @maxAttach = MAX(AttachmentPoint) FROM vPolicy
SELECT @maxNamedInsuredID = MAX(NamedInsuredID) FROM vPolicy
SELECT @maxProvisionLabelID = MAX(ProvisionLabelID) FROM vProvisions
SELECT @maxEndDate = MAX(annualperiod) FROM vPolicy
IF @carrierGroupID = 0 SET @carrierGroupID = NULL

IF @provisionLabelID = 0
	BEGIN 
		SELECT @maxProvisionValueID = MAX(ProvisionValueID) FROM vProvisions
		SELECT @minProvisionValueID = MIN(ProvisionValueID) FROM vProvisions
	END
ELSE 
	BEGIN
		SELECT @maxProvisionValueID = MAX(ProvisionValueID) FROM vProvisions WHERE ProvisionLabelID = @provisionLabelID
		SELECT @minProvisionValueID = MIN(ProvisionValueID) FROM vProvisions WHERE ProvisionLabelID = @provisionLabelID
	END

-- Eliminate Null Values
IF @highAttach IS NULL
	SET @highAttachNum = @maxAttach
ELSE 
	SET @highAttachNum = CONVERT(money, @highAttach)
IF @lowAttach IS NULL
	SET @lowAttachNum = 0
ELSE 
	SET @lowAttachNum =  CONVERT(money, @lowAttach)

SELECT pr.Provision, pr.ProvisionValue, pinfo.InsuranceProgram, pinfo.InsuranceProgramID, pinfo.NamedInsuredID, pinfo.NamedInsured, pinfo.MinPStartDate, pinfo.PolicyID, pinfo.PolicyNum, pinfo.PolicyNotes, 
	pinfo.AnnualPeriod AS SplitAnnualPeriod, pinfo.ActualPStartDate AS SplitStart, pinfo.ActualPEndDate AS SplitEnd, pinfo.CarrierID, pinfo.CarrierName, pinfo.CarrierSolvency, pinfo.CarrierGroupID, pinfo.CarrierGroupName,
	pinfo.LimitTypeID, pinfo.Layer, pinfo.AttachmentPoint, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit 
FROM vPolicy pinfo INNER JOIN vProvisions pr ON pinfo.PolicyID = pr.PolicyID
WHERE pinfo.InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND 
	pinfo.LimitTypeID >= COALESCE(@limitTypeID, 0) AND pinfo.LimitTypeID  <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	pinfo.CarrierID >= COALESCE(@carrierID, 0) AND pinfo.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
	(coalesce(pinfo.CarrierGroupID, 0) >= COALESCE(@carrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID, 0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID, 0) = @carrierGroupID) AND
	pinfo.NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND pinfo.NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsuredID) AND

	pinfo.CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND pinfo.Layer BETWEEN COALESCE(@lowLayer,0) AND COALESCE(NullIF(@highLayer,0),@maxLayer) AND
	pinfo.AttachmentPoint BETWEEN @lowAttachNum AND @highAttachNum AND
	pr.ProvisionValueID >= COALESCE(NULLIF(@provisionValueID, 0), @minProvisionValueID) AND pr.ProvisionValueID <= COALESCE(NULLIF(@provisionValueID, 0), @maxProvisionValueID) AND
	pr.ProvisionLabelID >= COALESCE(@provisionLabelID, 0) AND pr.ProvisionLabelID <= COALESCE(NULLIF(@provisionLabelID, 0), @maxProvisionLabelID) 
	and DoNotReport <> 1
	AND AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyByProvision] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyByProvision] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyByProvision] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyByProvision] TO [power_user]
GO
