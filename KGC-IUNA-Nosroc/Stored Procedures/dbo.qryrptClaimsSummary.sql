SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimsSummary]( 
                            @Start nvarchar(250),
                            @End nvarchar(250), @Matter int) 

as

Set NoCount On

------------Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

-- Declare @Start nvarchar(250)
-- Declare @End nvarchar(250)
-- declare @matter int
-- Set @Start = '1/1/1900'
-- Set @End = '6/30/2011'
-- set @matter = 2
 

Declare @newFilings table (Matter nvarchar(100), MatterID int,filedate nvarchar(250), claimnumber int)
insert into @newFilings
select Matter, c.MatterID, YEAR(coalesce(servicedate,filedate,statusdate)) as filedate, count(c.ClaimID) as ClaimNumber
From tblClaimantPersonalInfo i 
      Left Join tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
      LEFT JOIN tblMatter M on c.MatterID = m.MatterID
      Left Join tblClaimLawsuit l on l.claimID = c.claimID
      Left Join tblLawsuit law on l.lawsuitid = law.lawsuitid
      Left Join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=l.ClaimLawsuitID
 Where  IsPrimaryForClaim = 1 and C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) and 
	processeddate between COALESCE(@Start, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) and COALESCE(@End, GETDATE())
	AND ClaimLawsuitStatusID = 22
	AND COALESCE(i.jobsiteinfo, 0) = 0
 
Group By Matter, c.MatterID, YEAR(coalesce(servicedate,filedate,statusdate))
order by  YEAR(coalesce(servicedate,filedate,statusdate))

Declare @ClaimSummary table (Matter nvarchar(100), MatterID int, Statusdate nvarchar(250),
SBNDAmount money,SettlementAmount money, Inactive_Count int,DBnd_Count int,Dismissed_Count int,SBND_Count int,Settled_Count int)

Insert Into @ClaimSummary
Select Matter, c.MatterID,
      year(coalesce(settlementDate,statusdate)) as StatusDate,
      sum(case when clst.ClaimLawsuitStatusID = 25 then coalesce(SettlementAmount,0)else 0 end) as SBNDAmount,
      SUM(case when clst.ClaimLawsuitStatusID = 23 then coalesce(SettlementAmount,0) else 0 end) as SettlementAmount,
      sum(Case When clst.ClaimLawsuitStatusID = 29 then 1 else 0 end) as Inactive_Count,
sum(Case When clst.ClaimLawsuitStatusID = 17 then 1 else 0 end) as DBnd_Count,
sum(Case When clst.ClaimLawsuitStatusID IN (15,16,27,18,19,20,24,30,26,21) then 1 else 0 end) as Dismissed_Count,
sum(Case When clst.ClaimLawsuitStatusID = 25 then 1 else 0 end) as SBND_Count,
sum(Case When clst.ClaimLawsuitStatusID in (23,28) then 1 else 0 end) as Settled_Count

From tblClaimantPersonalInfo i 
      Left Join tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
      LEFT JOIN tblMatter M on c.MatterID = m.MatterID
      Left Join tblClaimLawsuit l on l.claimID = c.claimID
      Left Join tblLawsuit law on l.lawsuitid = law.lawsuitid
      Left Join tblCLaimantSettlement s on s.claimlawsuitID = l.claimlawsuitID
      Left Join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=l.ClaimLawsuitID
	left join tblClaimLawsuitStatusGroup csg on csg.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
    
Where IsPrimaryForClaim = 1 and coalesce(IsPrimaryStatus,0) = 1
      AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) 
      AND ProcessedDate >= @Start 
      AND ProcessedDate <= @End
      AND COALESCE(i.jobsiteinfo, 0) = 0
Group By Matter, c.MatterID, year(coalesce(settlementDate,statusdate))
order by    year(coalesce(settlementDate,statusdate))

Select f.Matter, f.MatterID, 
      year(coalesce(filedate,statusdate))as date1,
coalesce(SettlementAmount,0) as SettlementAmount,
coalesce(SbndAmount,0) as SbndAmount,
coalesce(claimnumber, 0) as File_Count,
coalesce(Inactive_Count,0) as Inactive_Count,
coalesce(DBnd_Count,0) as DBnd_Count,
coalesce(Dismissed_Count,0) as Dismissed_Count,
coalesce(SBND_Count,0) as SBND_Count,
coalesce(Settled_Count,0) as Settled_Count, @Start as StartDate, @End as EndDate
From @newFilings f
left join @claimSummary s on s.statusdate = f.filedate and f.Matterid = s.Matterid

Order By    year(coalesce(filedate,statusdate))

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimsSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimsSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimsSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimsSummary] TO [power_user]
GO
