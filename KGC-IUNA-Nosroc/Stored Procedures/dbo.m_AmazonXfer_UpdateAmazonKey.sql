SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_AmazonXfer_UpdateAmazonKey]
	-- Add the parameters for the stored procedure here
(@DocumentID int, @Key nvarchar(MAX))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblDocuments
	SET [AmazonKey] = @Key
	WHERE DocumentID = @DocumentID
	
	--UPDATE tblDocuments
	--SET AmazonKey = @Key
	--WHERE DocumentURL = (SELECT DocumentURL FROM tblDocuments WHERE DocumentID = @DocumentID)
	--AND AmazonKey IS NULL
END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonXfer_UpdateAmazonKey] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_AmazonXfer_UpdateAmazonKey] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonXfer_UpdateAmazonKey] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_AmazonXfer_UpdateAmazonKey] TO [power_user]
GO
