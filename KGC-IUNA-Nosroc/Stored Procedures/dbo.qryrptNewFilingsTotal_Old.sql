SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptNewFilingsTotal_Old]
( @StartDate datetime, @EndDate datetime,@Matter int)

AS
SET NOCOUNT ON

--Declare @EndDate DateTime
--Set @EndDate = '6/30/2010'
--Declare @StartDate DateTime
--Set @StartDate = '1/1/1900'
--Declare @matter int
--Set @Matter = 1

----Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


Declare @DiseaseCount Table (ClaimID int, Matter nvarchar(100), MatterID int, YearFiled nvarchar(50), MoYearFiled nvarchar(50), MonthID int, MesoCount int, LungCancerCount int, 
OtherCancerCount int, NonMaligCount int, SilicosisCount int, UnknownCount int, unknowncount1 int)
Insert into @DiseaseCount

Select C.ClaimID, Matter, C.MatterID, Coalesce(year(servicedate), year(fileDate), year(statusdate)) as YearFiled, 
coalesce(DateName(month,servicedate), DateName(month,filedate), DateName(month,statusdate)) as MonthFiled,
coalesce(month(servicedate), month(filedate), month(statusdate)) as MonthID,

--Meso
Case	when(Coalesce(DDefenseMeso.HighLevelDiseaseID,DPlaintiffMeso.HighLevelDiseaseID))=1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when(Coalesce(DDefenseLC.HighLevelDiseaseID,DPlaintiffLC.HighLevelDiseaseID))=2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when(Coalesce(DDefenseOC.HighLevelDiseaseID,DPlaintiffOC.HighLevelDiseaseID))=3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when(Coalesce(DDefenseNM.HighLevelDiseaseID,DPlaintiffNM.HighLevelDiseaseID))=4 Then 1
Else 0 End as NonMaligCount,
--Silicosis
Case	when(Coalesce(DDefenseNM.HighLevelDiseaseID,DPlaintiffNM.HighLevelDiseaseID))=7 Then 1
Else 0 End as SilicosisCount,
--Unknown
Case	when(Coalesce(DDefenseUnk.HighLevelDiseaseID,DPlaintiffUnk.HighLevelDiseaseID))in (5,6) Then 1
Else 0 End AS UnknownCount,
Case when (Coalesce(DDefenseUnk.HighLevelDiseaseID,DPlaintiffUnk.HighLevelDiseaseID))is NULL Then 1
Else 0 End AS UnknownCount1


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
--Meso
Left Join tblDisease DDefenseMeso on C.DefenseDiseaseID=DDefenseMeso.DiseaseID
Left Join tblDisease DPlaintiffMeso on C.PlaintiffDiseaseID=DPlaintiffMeso.DiseaseID
--Lung Cancer
Left Join tblDisease DDefenseLC on C.DefenseDiseaseID=DDefenseLC.DiseaseID
Left Join tblDisease DPlaintiffLC on C.PlaintiffDiseaseID=DPlaintiffLC.DiseaseID
--Other Cancer
Left Join tblDisease DDefenseOC on C.DefenseDiseaseID=DDefenseOC.DiseaseID
Left Join tblDisease DPlaintiffOC on C.PlaintiffDiseaseID=DPlaintiffOC.DiseaseID
--Non-Malig
Left Join tblDisease DDefenseNM on C.DefenseDiseaseID=DDefenseNM.DiseaseID
Left Join tblDisease DPlaintiffNM on C.PlaintiffDiseaseID=DPlaintiffNM.DiseaseID
--Unknown
Left Join tblDisease DDefenseUnk on C.DefenseDiseaseID=DDefenseUnk.DiseaseID
Left Join tblDisease DPlaintiffUnk on C.PlaintiffDiseaseID=DPlaintiffUnk.DiseaseID


Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
Left Join tblDefenseCounselState DCS on L.PrimaryDefenseCounselID=DCS.DefenseCounselID AND L.StateID=DCS.StateID

Where   IsPrimaryForClaim = 1 and C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) and 
	coalesce(servicedate,FileDate, StatusDate) between @StartDate and @EndDate 
Order By  COALESCE(YEAR(serviceDate), YEAR(fileDate), YEAR(StatusDate))


Select Matter, MatterID, YearFiled AS 'Year', MoYearFiled as 'Month', MonthID, COUNT(ClaimID) AS NewFilings,@StartDate as StartDate, @EndDate as EndDate,
Sum(MesoCount) as MesoFilings, Sum(LungCancerCount) as LCFilings, Sum(OtherCancerCount) as OCFilings,
Sum(NonMaligCount) as NMFILINGS, SUM(SilicosisCount) AS SilicosisCount, Sum(UnknownCount+unknowncount1) as UnknownFilings
from @DiseaseCount DC
Group By Matter, MatterID, YearFiled, MoYearFiled, MonthID
ORDER BY Matter, YearFiled, MonthID

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsTotal_Old] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsTotal_Old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsTotal_Old] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsTotal_Old] TO [power_user]
GO
