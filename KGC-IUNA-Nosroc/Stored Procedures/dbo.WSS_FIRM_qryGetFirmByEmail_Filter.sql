SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_FIRM_qryGetFirmByEmail_Filter]
	-- Add the parameters for the stored procedure here
	@emailaddress nvarchar(1000)
AS
DECLARE @firmID int
SET @firmID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)
BEGIN
	DECLARE @emaildomain nvarchar(1000)
	SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress), LEN(@emailaddress)-CHARINDEX('@', @emailaddress)+1)
	IF @emaildomain = '@kcicllc.com' OR @emailDomain='@kcic.com' OR @firmID = 38
	BEGIN
		SELECT DISTINCT FirmID, DefenseCounsel as Firm 
		FROM vtblPerson INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
		WHERE FirmID NOT IN ( 79)
		UNION 
		SELECT NULL, '<< All Firms >>'
		ORDER BY DefenseCounsel
	END
	ELSE IF @firmID = 79
	BEGIN
		SELECT DISTINCT FirmID, DefenseCounsel as Firm FROM vtblPerson 
		INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
		WHERE FirmID = 38
		UNION 
		SELECT NULL, '<< All Firms >>'
		ORDER BY DefenseCounsel
	END
	ELSE
	BEGIN
		SELECT DISTINCT FirmID, DefenseCounsel as Firm FROM vtblPerson 
		INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
		WHERE @emailaddress = vtblPerson.Email
		UNION 
		SELECT NULL, '<< All Firms >>'
		ORDER BY DefenseCounsel
	END
		
	 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmByEmail_Filter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmByEmail_Filter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmByEmail_Filter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmByEmail_Filter] TO [power_user]
GO
