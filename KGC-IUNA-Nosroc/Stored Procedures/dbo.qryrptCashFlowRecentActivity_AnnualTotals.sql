SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptCashFlowRecentActivity_AnnualTotals]
(@StartDate date, @Matter int)
As

SET NOCOUNT ON

--Declare @StartDate date
--Declare @matter int
--Set @StartDate = '4/1/2010'
--Set @Matter = NULL

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblCashFlow

DECLARE @minStartDate datetime
SELECT @minStartDate = MIN([Date]) FROM tblCashFlow

SELECT   SUM(Total) AS YearTotal, Year([Date]) as Year
FROM  tblCashFlownew CF
Left Join tblTransactionType TT on CF.TransactionTypeID=TT.TransactionTypeID
Left Join tblMatter M on CF.MatterID=M.MatterID
Where  YEAR([Date]) < YEAR(GETDATE())
		AND CF.MatterID >= COALESCE(@Matter,0) AND CF.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
--Order By [Date]
GROUP BY  Year([Date])
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity_AnnualTotals] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity_AnnualTotals] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowRecentActivity_AnnualTotals] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowRecentActivity_AnnualTotals] TO [power_user]
GO
