SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptClaimantPersonalInfoEntryQC] (@ClaimantPersonalInfoID int)

as

--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 5185

Select cpi.ClaimantPersonalInfoID,
      cpi.FirstName as ClaimantFirstName, 
      cpi.MiddleName as ClaimantMiddleName, 
      cpi.LastName as ClaimantLastName, 
      Suffix as ClaimantSuffix,
      SocialSecurityNumber, 
      DeceasedDate, 
      MaritalStatus, 
      Gender, 
      CompleteAddress, 
            Address1,
      Address2,
      City,
      [State],
      Zip,
      CASE WHEN Extension IS NULL OR Extension LIKE '' THEN '(' + CONVERT(nvarchar(3),LEFT(Phone,3)) + ') ' + CONVERT(nvarchar(3),LEFT(RIGHT(Phone,7),3)) + '-' + CONVERT(nvarchar(4),RIGHT(Phone,4))
      ELSE '(' + CONVERT(nvarchar(3),LEFT(Phone,3)) + ') ' + CONVERT(nvarchar(3),LEFT(RIGHT(Phone,7),3)) + '-' + CONVERT(nvarchar(4),RIGHT(Phone,4)) + ' x' + CONVERT(nvarchar(10),Extension) END AS Phone,
      BirthDate,
      DPlaintiff.Disease as PlaintiffDisease,
      c.PlaintiffDiseaseDiagnosisDate,
      DDefense.Disease as DefenseDisease,
      c.DefenseDiseaseDiagnosisDate,
      HealthInsuranceClaimNumber,
      Extension,
      MedicareBeneficiarystatusid,
      CMSQueryDate
From tblClaimantPersonalInfo cpi 
      Left Join tblGender g on g.GenderID = cpi.GenderID
      Left Join tblClaimantSuffix cs on cs.SuffixID = cpi.SuffixID
      Left Join tblMaritalStatus ms on ms.MaritalStatusID = cpi.MaritalStatusID
      Left Join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
      Left Join tblDisease DPlaintiff on DPlaintiff.DiseaseID = C.PlaintiffDiseaseID
      Left Join tblDisease DDefense on DDefense.DiseaseID = C.DefenseDiseaseID
      Left Join tblState st ON cpi.StateID = st.StateID
Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND c.IsCurrentClaim = 1
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantPersonalInfoEntryQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantPersonalInfoEntryQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantPersonalInfoEntryQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantPersonalInfoEntryQC] TO [power_user]
GO
