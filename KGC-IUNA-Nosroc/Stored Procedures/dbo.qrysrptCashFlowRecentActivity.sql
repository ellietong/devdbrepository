SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysrptCashFlowRecentActivity]
(@Matter int)
As

SET NOCOUNT ON

--DECLARE @Matter INT
--SET @Matter = 1

SELECT	Sum(Indemnity) as Indemnity, Sum(DefenseCounsel) as DefenseCounsel, 
Sum(Shortfall) as Shortfall, Sum(MiscFees) as MiscFees, Sum(Bundled) as Bundled, 
Sum(CashReceipts) as CashReceipts, Sum(Indemnity)+Sum(DefenseCOunsel)+Sum(Shortfall)+Sum(MiscFees)
+Sum(Bundled)+Sum(CashReceipts) as TransferTotal,
Sum(Total) as Total, MIN([date]) as mindate, MAX([date]) as maxdate
FROM	tblCashFlownew CF
Left Join tblTransactionType TT on CF.TransactionTypeID=TT.TransactionTypeID
Left Join tblMatter M on CF.MatterID=M.MatterID
Where CF.MatterID = @Matter

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCashFlowRecentActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCashFlowRecentActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCashFlowRecentActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCashFlowRecentActivity] TO [power_user]
GO
