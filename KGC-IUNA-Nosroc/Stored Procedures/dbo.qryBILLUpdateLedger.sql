SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryBILLUpdateLedger]
(@BillDate datetime, @exhaustionDateID int, @exhaustionTypeID int, @BillGroupID int)

As

--Set NoCount On
 --Declare @exhaustionDateID int
 --Declare @exhaustionTypeID int
 --Declare @BillGroupID int
 --Declare @BillDate datetime
 --Set @exhaustionDateID = 1261 --6/30/08
 --Set @exhaustionTypeID = 6 --Sig only
 --Set @BillGroupID = 1 --signatory only
 --Set @BillDate= '10/31/2000'


Declare @Exhaustion Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
      IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
      SIRDefenseAllocation money, LedgerID bigint, occurrencetypeid int, MatterID int)
Insert Into @Exhaustion
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, o.OccurrenceID, PolicyDateID, IndemnityAllocation,
      DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation,
      LedgerID, occurrencetypeid, e.MatterID
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
inner join tblOccurrence o on o.OccurrenceID = E.OccurrenceID
Where ExhaustionTypeID = @ExhaustionTypeID and ED.ExhaustionDateID = @ExhaustionDateID

Declare @Ledger table (BillGroupID int, CarrierID int, SubscriberID int, Allocation money, ExhaustionDateID int, BillDate datetime, 
PaymentDueDate datetime, MatterID int, occurrencetypeid int)
Insert into @Ledger
Select @BillGroupID, P.CarrierID, null as SubscriberID, Coalesce(SUM(IndemnityAllocation) + SUM(DefenseAllocation) + SUM(DefenseOutsideLimitsAllocation),0) as Allocation,
      ExhaustionDateID, @BillDate, @BillDate+15 as PaymentDueDate, MatterID,occurrencetypeid = 
    case occurrencetypeid when (18)then 1 
    when  20 then 1
    when 19 then 2
    when 21 then 2
    else 'Null'
    END         
    From @Exhaustion E
      INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
      INNER JOIN tblPolicy p ON pd.PolicyID = p.PolicyID
      INNER JOIN tblBillGroupCarriers bgc ON p.CarrierID = bgc.CarrierID
Where P.CarrierID in (Select CarrierID from tblBillGroupCarriers where billgroupid=@BillGroupID) and P.CarrierID<>20
Group By p.CarrierID, exhaustionDateID, MatterID, occurrencetypeid
Union
Select @BillGroupID, P.CarrierID, SubscriberID as SubscriberID, Coalesce(SUM(IndemnityAllocation*percentage) + SUM(DefenseAllocation*percentage) + SUM(DefenseOutsideLimitsAllocation*percentage),0) as Allocation,
      ExhaustionDateID, @BillDate, @BillDate+15 as PaymentDueDate, MatterID, 
          occurrencetypeid = 
    case occurrencetypeid when (18)then 1 
    when  20 then 1
    when 19 then 2
    when 21 then 2
    else 'Null'
    END   
From @Exhaustion E
      INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
      INNER JOIN tblPolicy p ON pd.PolicyID = p.PolicyID
      Inner Join tblSubscriptionCoverage SC on p.policyid=sc.policyID
      INNER JOIN tblBillGroupCarriers bgc ON p.CarrierID = bgc.CarrierID
Where P.CarrierID in (Select CarrierID from tblBillGroupCarriers where billgroupid=@BillGroupID) and P.CarrierID=20
Group By P.CarrierID, SubscriberID, ExhaustionDateID, MatterID, occurrencetypeid


Declare @Final Table (BillGroupID int, CarrierID int, SubscriberID int, Allocation money, ExhaustionDateID int, BillDate datetime, PaymentDueDate datetime, MatterID int,
billtypeid int)
Insert Into @Final
Select BillGroupID, CarrierID, Case When SubscriberID in (13,34,38,35,30) then '20000' else SubscriberID end as subscriberID, Sum(Allocation) as Allocation, ExhaustionDateID, BillDate, PaymentDueDate,
MatterID, occurrencetypeid 
From @Ledger
Group By BillGroupID, CarrierID, SubscriberID, ExhaustionDateID, BillDate, PaymentDueDate, MatterID, occurrencetypeid

Insert Into tblLedger (BillGroupID, CarrierID, SubscriberID, Allocation, ExhaustionDateID, BillDate, PaymentDueDate, MatterID, BillTypeID)
Select BillGroupID, CarrierID, SubscriberID , Sum(Allocation) as Allocation, ExhaustionDateID, BillDate, PaymentDueDate, MatterID, BillTypeID
From @Final
Group By BillGroupID, CarrierID, SubscriberID, ExhaustionDateID, BillDate, PaymentDueDate, MatterID, BillTypeID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryBILLUpdateLedger] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryBILLUpdateLedger] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryBILLUpdateLedger] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryBILLUpdateLedger] TO [power_user]
GO
