SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[REVERT_SP_MOSS_REPORTS_qryClaimsReports]
(@ClaimLawsuitStatusGroupID int, @HighLevelDiseaseID int, @StateID int, @PlaintiffCounselID bigint, @JurisdictionID int, @MatterID int,
@StartDate datetime, @EndDate datetime)
AS
BEGIN

--DECLARE @ClaimLawsuitStatusID int
--SET @ClaimLawsuitStatusID = 1
--DECLARE @HighLevelDiseaseID int
--SET @HighLevelDiseaseID = null
--DECLARE @StateID int
--SET @StateID = null
--DECLARE @PlaintiffCounselID bigint
--SET @PlaintiffCounselID = null
----DECLARE @plaintiffCounselAddressID int
----SET @DefenseCounselAddressID = null
--DECLARE @JurisdictionID int
--SET @JurisdictionID = null

--DECLARE @StartDate datetime
--SET @StartDate = '1/1/2009'

--DECLARE @EndDate datetime
--SET @EndDate = GETDATE()
----DECLARE @Paid bit
----SET @Paid = null
--DECLARE @MatterID int
--SET @MatterID = 1
--DECLARE @ClaimLawsuitStatusGroupID int
--SET @ClaimLawsuitStatusGroupID = null
DECLARE @MaxHighLevelDiseaseID int
SET @MaxHighLevelDiseaseID = (SELECT MAX(HighLevelDiseaseID) FROM tblHighLevelDisease)
DECLARE @MaxStateID int
SET @MaxStateID = (SELECT MAX(StateID) FROM tblState)
DECLARE @MaxPlaintiffCounselID int
SET @MaxPlaintiffCounselID = (SELECT MAX(PlaintiffCounselID) FROM tblPlaintiffCounselAddress)
DECLARE @MaxDefenseCounselAddressID int
SET @MaxDefenseCounselAddressID = (SELECT MAX(DefenseCounselAddressID) FROM tblDefenseCounselAddress)
DECLARE @MaxJurisdictionID int
SET @MaxJurisdictionID = (SELECT MAX(JurisdictionID) FROM tblJurisdiction)
DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)
DECLARE @MaxClaimLawsuitStatusGroupID int
SET @MaxClaimLawsuitStatusGroupID = (SELECT MAX(StatusGroupID) FROM tblStatusGroup)


DECLARE @HLD TABLE (HLDiseaseID int)
IF(@HighLevelDiseaseID IS NULL)
BEGIN
	INSERT INTO @HLD
	SELECT HighLevelDiseaseID FROM tblHighLevelDisease
END
ELSE IF (@HighLevelDiseaseID = 4)
BEGIN
	INSERT INTO @HLD
	SELECT HighLevelDiseaseID FROM tblHighLevelDisease
	WHERE HighLevelDiseaseID IN (4, 7)
END
ELSE IF (@HighLevelDiseaseID = 5)
BEGIN
	INSERT INTO @HLD
	SELECT HighLevelDiseaseID FROM tblHighLevelDisease
	WHERE HighLevelDiseaseID IN (5, 6) OR HighLevelDiseaseID IS NULL
END
ELSE
BEGIN
	INSERT INTO @HLD
	SELECT @HighLevelDiseaseID
END


	SELECT     DISTINCT C.ClaimID, c.ClaimantPersonalInfoID, DDisease.Disease  + COALESCE(' - ' + ICD9, '') AS Disease, S.State, J.Jurisdiction, cs.ClaimSequence, 
						  CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
						  THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
						  CONVERT(VARCHAR(10), CPI.BirthDate, 101) AS BirthDate, CPI.DeceasedDate, CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, 
						  DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) 
						  = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, 
						  BirthDate, DeceasedDate) END END AS DeathAge, cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) 
						  AS FileDate, L.DocketNumber, L.CaseCaption, L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName, 
						  pca.PlaintiffCounsel, dca1.DefenseCounsel AS PrimaryDefenseCounsel, m.Matter, ActiveTrialDate, StatusDate, SettlementAmount
	FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
						  dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
						  dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
						  dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID 
						  LEFT join tblplaintiffcounselAddress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid OR pca.PlaintiffCounselID = l.PlaintiffCounselID INNER JOIN
						  dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
						  dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
						  dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID 
						  LEFT OUTER JOIN tblDefenseCounselAddress dca1 ON l.DefenseCounselAddressID = dca1.DefenseCounselAddressID OR PrimaryDefenseCounselID = dca1.DefenseCounselID 
						  --LEFT OUTER JOIN tblDefenseCounselAddress dca2 ON l.DefenseCounselAddressID = dca2
						  LEFT OUTER JOIN (select * from dbo.tblClaimDisease where PrimaryDisease = 1) as cd on cd.ClaimID = c.ClaimID LEFT OUTER JOIN
						  dbo.tblDisease AS DDisease ON Cd.DiseaseID = DDisease.DiseaseID LEFT OUTER JOIN
						  dbo.tblHighLevelDisease AS DHLD ON DHLD.HighLevelDiseaseID = DDisease.HighLevelDiseaseID INNER JOIN
						  (SELECT * FROM tblClaimLawsuitStatusTracking WHERE IsPrimaryStatus = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID INNER JOIN
						  dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
						  LEFT OUTER JOIN tblClaimantSettlement csett ON cl.ClaimLawsuitID = csett.ClaimLawsuitID
						  LEFT JOIN tblMatter M ON c.MatterID = m.MatterID
						  INNER JOIN tblClaimLawsuitStatusGroup clsg ON cls.ClaimLawsuitStatusID = clsg.ClaimLawsuitStatusID
	WHERE     (COALESCE(CL.IsPrimaryForClaim, 0) = 1) 
				AND DDisease.HighLevelDiseaseID IN (SELECT * FROM @HLD)
				--AND ((ddisease.HighLevelDiseaseID >= COALESCE(@HighLevelDiseaseID, 0) AND ddisease.HighLevelDiseaseID <= COALESCE(@HighLevelDiseaseID, @MaxHighLevelDiseaseID)) OR DDisease.HighLevelDiseaseID IS NULL )
				AND l.StateID >= COALESCE(@StateID, 0) AND l.StateID <= COALESCE(@StateID, @MaxStateID)
				AND pca.PlaintiffCounselID >= COALESCE(@PlaintiffCounselID, 0) AND pca.PlaintiffCounselID <= COALESCE(@PlaintiffCounselID, @MaxPlaintiffCounselID)
				AND l.JurisdictionID  >= COALESCE(@JurisdictionID, 0) AND l.JurisdictionID <= COALESCE(@JurisdictionID, @MaxJurisdictionID)
				AND clsg.StatusGroupID >= COALESCE(@ClaimLawsuitStatusGroupID, 0) AND clsg.StatusGroupID <= COALESCE(@ClaimLawsuitStatusGroupID, @MaxClaimLawsuitStatusGroupID)
				AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(@MatterID, @MaxMatterID)
				AND ProcessedDate BETWEEN COALESCE(@StartDate, '1/1/1900') AND COALESCE(@EndDate, GETDATE())
	ORDER BY CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
						  THEN '' ELSE MiddleName + ' ' END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryClaimsReports] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryClaimsReports] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryClaimsReports] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryClaimsReports] TO [power_user]
GO
