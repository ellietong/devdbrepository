SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptSettledCaseSummary_Excel] (@SettlementDateStart nvarchar(250),
											       @SettlementDateEnd   nvarchar(250), 
											       @Matter              int) 
AS
SET NOCOUNT ON

----Variables used for testing.
--DECLARE @SettlementDateStart datetime
--DECLARE @SettlementDateEnd datetime
--DECLARE @matter int
--SET @SettlementDateStart  = '1/1/1900'
--SET @SettlementDateEnd = '3/1/2012'
--SET @matter = 1

----Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @SettlementSummary TABLE (Matter                       nvarchar(100), 
								  MatterID                     int, 
								  ClaimNumber                  int, 
								  SettlementAmount             money,
								  SettlementAverage            money,
								  ResolvedDate                 datetime,
								  Meso_Count                   int,
								  Meso_SettlementAmount        money,
								  Meso_Avg                     money,
								  LungCancer_Count             int,
								  LungCancer_SettlementAmount  money,
								  LungCancer_Avg               money,
								  OtherCancer_Count            int,
								  OtherCancer_SettlementAmount money,
								  OtherCancer_Avg              money,
								  NonMalig_Count               int,
								  NonMalig_SettlementAmount    money,
								  NonMalig_Avg                 money,
								  Other_Count                  int,
								  Other_SettlementAmount       money,
								  NoDisease_Count              int,
								  NoDisease_SettlementAmount   money,
								  Null_Count                   int,
								  Null_SettlementAmount        money)
INSERT INTO @SettlementSummary 
	Select Matter, 
		c.MatterID, 
		count(c.ClaimID) as ClaimantNumber,
		coalesce(sum(SettlementAmount),0) as SettlementAmount,
		coalesce(sum(SettlementAmount),0)/count(c.ClaimID) as SettlementAverage, 
		convert(datetime,(convert(nvarchar,month(coalesce(SettlementDate,StatusDate)))+'/1/'+ convert(nvarchar,year(coalesce(SettlementDate,StatusDate, '1/1/1900'))))) as ResolvedDate,
		sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) as Meso_Count,	
		sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end) as Meso_SettlementAmount,	
		Case When sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) = 0 then 0 
			 else sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end)/ 
				  sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) end  as Meso_Avg,		
		sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) as LungCancer_Count,	
		sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end) as LungCancer_SettlementAmount,	
		Case When sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) = 0 then 0 
			 else sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end)/ 
				  sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) end  as LungCancer_Avg,	
		sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) as OtherCancer_Count,	
		sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end) as OtherCancer_SettlementAmount,	
		Case When sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) = 0 then 0 
			 else sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end)/ 
				  sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) end  as OtherCancer_Avg,
		sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) as NonMalig_Count,	
		sum(Case When HLD.HighLevelDiseaseID in (4,7) then SettlementAmount else 0 end) as NonMalig_SettlementAmount,	
		Case When sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) = 0 then 0 
			 else sum(Case When HLD.HighLevelDiseaseID in (4,7) then SettlementAmount else 0 end)/ 
				  sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) end  as NonMalig_Avg,
		Sum(Case When HLD.HighLevelDiseaseID = 5 then 1 else 0 end) as Other_Count,	
		Sum(Case When HLD.HighLevelDiseaseID = 5 then SettlementAmount else 0 end) as Other_SettlementAmount,
		Sum(Case When HLD.HighLevelDiseaseID = 6 then 1 else 0 end) as NoDisease_Count,	
		Sum(Case When HLD.HighLevelDiseaseID = 6 then SettlementAmount else 0 end) as NoDisease_SettlementAmount,
		Sum(Case When HLD.HighLevelDiseaseID is null then 1 else 0 end) as Null_Count,				
		Sum(Case When HLD.HighLevelDiseaseID is null then SettlementAmount else 0 end) as Null_SettlementAmount
	FROM tblClaimantPersonalInfo i 
		LEFT JOIN tblClaim c                         ON i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN (SELECT cd.ClaimDiseaseID, 
				       cd.ClaimID, 
				       cd.DiseaseDiagnosisDate, 
				       cd.DiseaseID, 
				       cd.DoctorID, 
				       cd.PrimaryDisease 
				   FROM tblClaimDisease cd 
				   WHERE PrimaryDisease = 1) cd      ON cd.ClaimID = c.ClaimID
		LEFT JOIN tblMatter m                        ON c.MatterID = m.MatterID
		LEFT JOIN tblClaimLawsuit l                  ON l.claimID = c.claimID
		LEFT JOIN tblLawsuit law                     ON l.lawsuitid = law.lawsuitid
		LEFT JOIN tblCLaimantSettlement s            ON s.claimlawsuitID = l.claimlawsuitID
		LEFT JOIN tblDisease d                       ON cd.DiseaseID = d.DiseaseID
		LEFT JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID
		LEFT JOIN tblClaimSequence csq               ON c.ClaimSequenceID = csq.ClaimSequenceID
		LEFT JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = l.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls          ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		LEFT JOIN tblClaimLawsuitStatusGroup csg     ON csg.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	WHERE IsPrimaryForClaim = 1 
		and ProcessedDate >= COALESCE(@SettlementDateStart, '1/1/'+CONVERT(nvarchar(4), YEAR(GETDATE()))) and ProcessedDate <= COALESCE(@SettlementDateEnd, GETDATE())
		AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) 
		AND StatusGroupID = 2
		AND COALESCE(i.jobsiteinfo, 0) = 0
	GROUP BY Matter, c.MatterID, CONVERT(datetime,(CONVERT(nvarchar,MONTH(COALESCE(SettlementDate,StatusDate)))+'/1/'+ 
		CONVERT(nvarchar,YEAR(COALESCE(SettlementDate,StatusDate, '1/1/1900')))))

Select Matter, 
	MatterID, 
	COALESCE(ClaimNumber,0)                  AS ClaimNumber, 
	COALESCE(ResolvedDate, 0)                AS ResolvedDate,
	YEAR(COALESCE(ResolvedDate, 0))          AS ResolvedYear,
	CASE WHEN COALESCE(MONTH(ResolvedDate), 0) =  1 THEN 'January'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  2 THEN 'February'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  3 THEN 'March'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  4 THEN 'April'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  5 THEN 'May'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  6 THEN 'June'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  7 THEN 'July'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  8 THEN 'August'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) =  9 THEN 'September'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) = 10 THEN 'October'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) = 11 THEN 'November'
		 WHEN COALESCE(MONTH(ResolvedDate), 0) = 12 THEN 'December' END AS ResolvedMonth, 
	COALESCE(SettlementAmount,0)             AS SettlementAmount,
	COALESCE(SettlementAverage,0)            AS SettlementAverage,
	COALESCE(Meso_Count,0)                   AS Meso_Count,
	COALESCE(Meso_SettlementAmount,0)        AS Meso_SettlementAmount,
	COALESCE(Meso_Avg,0)                     AS Meso_Avg,
	COALESCE(LungCancer_Count,0)             AS LungCancer_Count,
	COALESCE(LungCancer_SettlementAmount,0)  AS LungCancer_SettlementAmount,
	COALESCE(LungCancer_Avg,0)               AS LungCancer_Avg,
	COALESCE(OtherCancer_Count,0)            AS OtherCancer_Count,
	COALESCE(OtherCancer_SettlementAmount,0) AS OtherCancer_SettlementAmount,
	COALESCE(OtherCancer_Avg,0)              AS OtherCancer_Avg,
	COALESCE(NonMalig_Count,0)               AS NonMalig_Count,
	COALESCE(NonMalig_SettlementAmount,0)    AS NonMalig_SettlementAmount,
	COALESCE(NonMalig_Avg,0)                 AS NonMalig_Avg,
	COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)                                       AS Other_Count,
	COALESCE(Other_SettlementAmount,0) + COALESCE(NoDisease_SettlementAmount,0) + COALESCE(Null_SettlementAmount,0)      AS Other_SettlementAmount,
	(COALESCE(Other_SettlementAmount,0) + COALESCE(NoDisease_SettlementAmount,0) + COALESCE(Null_SettlementAmount,0)) / 
		CASE WHEN (COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)) = 0 THEN 1 
			 ELSE (COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)) END                   AS Other_Avg,
	COALESCE(Meso_Count,0) + COALESCE(LungCancer_Count,0) + COALESCE(OtherCancer_Count,0) + COALESCE(NonMalig_Count,0) + 
		COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)                                   AS Total_Count,
	COALESCE(Meso_SettlementAmount,0) + COALESCE(LungCancer_SettlementAmount,0) + COALESCE(OtherCancer_SettlementAmount,0) + 
		COALESCE(NonMalig_SettlementAmount,0) + COALESCE(Other_SettlementAmount,0) + 
		COALESCE(NoDisease_SettlementAmount,0) + COALESCE(Null_SettlementAmount,0)                                       AS Total_SettlementAmount,
	(COALESCE(Meso_SettlementAmount,0) + COALESCE(LungCancer_SettlementAmount,0) + COALESCE(OtherCancer_SettlementAmount,0) + 
		COALESCE(NonMalig_SettlementAmount,0) + COALESCE(Other_SettlementAmount,0) + 
		COALESCE(NoDisease_SettlementAmount,0) + COALESCE(Null_SettlementAmount,0)) /
		CASE WHEN (COALESCE(Meso_Count,0) + COALESCE(LungCancer_Count,0) + COALESCE(OtherCancer_Count,0) + COALESCE(NonMalig_Count,0) + 
				   COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)) = 0 THEN 1 
			 ELSE (COALESCE(Meso_Count,0) + COALESCE(LungCancer_Count,0) + COALESCE(OtherCancer_Count,0) + COALESCE(NonMalig_Count,0) + 
			       COALESCE(Other_Count,0) + COALESCE(NoDisease_Count,0) + COALESCE(Null_Count,0)) END                   AS Total_Avg,
 	@SettlementDateStart AS StartDate, 
 	@SettlementDateEnd AS EndDate
FROM @SettlementSummary 
ORDER BY COALESCE(ResolvedDate,0)

SELECT DISTINCT YEAR(COALESCE(ResolvedDate, 0)) AS ApplicableYears
FROM @SettlementSummary
ORDER BY YEAR(COALESCE(ResolvedDate, 0))

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSettledCaseSummary_Excel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSettledCaseSummary_Excel] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptSettledCaseSummary_Excel] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSettledCaseSummary_Excel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSettledCaseSummary_Excel] TO [power_user]
GO
