SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Demo] (@StateID int, @StartDate datetime, @EndDate datetime, @Matter int)
AS

Set NoCount ON

--DECLARE @StartDate datetime
--DECLARE @EndDate datetime
--DECLARE @StateID int
--DECLARE @Matter int

--SET @StartDate = '1/1/1900'
--SET @EndDate = '9/30/2010'
--SET @StateID = null
--SET @Matter = null

-- Find Max ID Values

Declare @MaxStateID int

--Define Max State ID
Select @MaxStateID = MAX(stateid) from tblState

-- Find Max ID Values
Declare @MaxResponsibleDefenseCounselID int

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int, State nvarchar(150),TrialDate datetime, YearFiled datetime, monthFiledID int, monthFiledName nvarchar(100), statusdate datetime, MesoCount int, LungCancerCount int, OtherCancerCount int, 
NonMaligCount int,  UnknownCount int, UnknownCount1 int, SilicosisCount int, NoDiseaseCount int)


Insert into @DiseaseCount

Select Matter, C.MatterID, FullNameState as state, L.activeTrialDate, 
case when serviceDate is null then  '1/1/1900' else convert(datetime,('1/1/'+convert(nvarchar,Year(serviceDate))))end as YearFiled, MONTH(COALESCE(serviceDate, '1/1/1900')), Datename(month, COALESCE(serviceDate, '1/1/1900')),
coalesce(settlementdate,statusdate,'1/1/1900'),
--Meso
Case	when DMeso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when DLC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when DOC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when DNM.HighLevelDiseaseID = 4 Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when DUnk.HighLevelDiseaseID = 5 Then 1
Else 0 End AS UnknownCount,
--No Disease
Case	when DND.HighLevelDiseaseID = 6 Then 1
Else 0 End AS NoDiseaseCount,
--Silicosis
Case	when DSC.HighLevelDiseaseID = 7 Then 1
Else 0 End AS SilicosisCount,
--Null
Case	when DUnk.HighLevelDiseaseID is NULL Then 1
Else 0 End AS UnknownCount1

From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
LEFT JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID
--Meso
Left Join tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
Left Join tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
Left Join tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
Left Join tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Silicosis
Left Join tblDisease DSC on CD.DiseaseID=DSC.DiseaseID

--No Disease
Left Join tblDisease DND on CD.DiseaseID=DND.DiseaseID

--Unknown
Left Join tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID


Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblState S on L.StateID=S.StateID
left join tblClaimantSettlement cs on cs.ClaimLawsuitID = CL.ClaimLawsuitID


Where  s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) and IsPrimaryForClaim = 1
	AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
	AND ((ClaimLawsuitStatusID = 22 and coalesce(servicedate, filedate, '1/1/1900')<=@endDate) OR (ClaimLawsuitStatusID <> 22 and coalesce(settlementdate,statusdate,'1/1/1900') > @EndDate))
	AND csq.ClaimSequenceID NOT IN (4,2,3,11)
	AND COALESCE(cpi.jobsiteinfo, 0) = 0
Order By State, Year(serviceDate)

Select Matter, MatterID, TrialDate, YEAR(@EndDate) AS CloseYear, State,  Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, Sum(OtherCancerCount) as OtherCancerCount ,@StartDate as StartDate, @EndDate as EndDate,
	Sum(NonMaligCount+SilicosisCount) as NonMaligCount, Sum(UnknownCount+UnknownCount1+NoDiseaseCount) as UnknownCount
from @DiseaseCount
where yearfiled between @StartDate and @EndDate  
Group By Matter, MatterID, State, TrialDate
ORDER BY Matter, State 

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Demo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Demo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Demo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Demo] TO [power_user]
GO
