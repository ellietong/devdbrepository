SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAllocationAllRelatedTables]
(@AllocationID int)
As
SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 3

DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation


SELECT a.AllocationID, a.AllocationDescription, a.SaveResults, a.LimitTypeID, a.AllocationSchemeID, 
a.TriggerStartID, a.TriggerStartDate, a.TriggerEndID, a.TriggerEndDate, a.OccurrenceDefId, 
a.UnCoveredYearstoPH, a.VerticallyUnCollapse, a.Cost1, a.Cost2, a.Cost3, a.Cost4, a.Cost5, a.Cost6, a.Cost7, 
a.Cost8, a.Cost9, a.Cost10, a.Cost11, a.Cost12, a.SIRID, a.SIRToPay, a.ConsiderPreviousExhaustion, 
a.ConsiderExhaustionCarterWallace, a.ExhaustionTypeID, a.LessThanAttachmentPoint, a.LessThanLayer, 
a.OccurrencesInClaim, a.Cost1Discount, a.Cost2Discount, a.Cost3Discount, a.Cost4Discount, a.Cost5Discount, 
a.Cost6Discount, a.Cost7Discount, a.Cost8Discount, a.Cost9Discount, a.Cost10Discount, a.Cost11Discount, 
a.Cost12Discount, a.AllocationNotes, a.ConsiderPreviousAllocation, a.PreviousAllocation, 
a.AllocationScheme, asir.SIRTreatment, od.OccurrenceDef, 
tl.TriggerDescription AS TriggerStart, tblTriggerLabel_1.TriggerDescription AS TriggerEnd, 
a.LimitType, a.ExhaustionType AS 'Description', UnevenQSType, PolicyDateType, ConsiderNegatives
FROM vAllocation a 
      LEFT JOIN tblOccurrenceDef od ON a.OccurrenceDefId = od.OccurrenceDefID
      LEFT JOIN tblAllocationSIR asir ON a.SIRID = asir.SIRID
      LEFT JOIN tblTriggerLabel tl ON a.TriggerStartID = tl.TriggerID
      LEFT JOIN tblTriggerLabel AS tblTriggerLabel_1 ON a.TriggerEndID = tblTriggerLabel_1.TriggerID
      LEFT JOIN tblAllocation al on al.AllocationID=a.AllocationID
		LEFT JOIN tblAllocationUnevenQS aqs on aqs.UnevenQSID=al.UnevenQSID
		LEFT JOIN tblPolicyDateType pdt on pdt.PolicyDateTypeID=al.PolicyDateTypeID
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) 
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationAllRelatedTables] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationAllRelatedTables] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationAllRelatedTables] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationAllRelatedTables] TO [power_user]
GO
