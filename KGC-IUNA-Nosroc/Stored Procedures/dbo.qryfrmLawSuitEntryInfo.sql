SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmLawSuitEntryInfo] 
as

Select LawsuitID, 
                CaseCaption, 
                DocketNumber, 
                FileDate, 
                JurisdictionID, 
                StateID, 
                CourtTypeID, 
                ServiceDate, 
                ReceivedDate, 
                PrimaryDefenseCounselID, 
                PrimaryLocalPlaintiffCounsel, 
                PrimaryNationalPlaintiffCounsel, 
                LeadPlaintiffFirstName, 
                LeadPlaintiffLastName,
                plaintiffcounselid,
                PlaintiffCounselAddressID,
                DefenseCounselAddressID
From tblLawsuit
Order By FileDate desc, DocketNumber, CaseCaption

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawSuitEntryInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawSuitEntryInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawSuitEntryInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawSuitEntryInfo] TO [power_user]
GO
