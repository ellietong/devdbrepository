SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryPaymentTermsList]
AS
BEGIN
	SET NOCOUNT ON;
	
    SELECT PaymentTermsID, PaymentTerms FROM
    tblPaymentTerms
    WHERE PaymentTermsID = 5
    ORDER BY PaymentTerms
    
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPaymentTermsList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPaymentTermsList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPaymentTermsList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPaymentTermsList] TO [power_user]
GO
