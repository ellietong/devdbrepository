SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmPolicyDateType]
AS
SET NOCOUNT ON
SELECT *
FROM tblPolicyDateType
ORDER BY PolicyDateType

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyDateType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyDateType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyDateType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyDateType] TO [power_user]
GO
