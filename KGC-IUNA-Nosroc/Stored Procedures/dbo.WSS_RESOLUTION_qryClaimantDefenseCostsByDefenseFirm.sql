SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] 
	-- Add the parameters for the stored procedure here
	(@ClaimantPersonalInfoID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 9494

	

--Declare @InvoiceTotal table (ClaimantPersonalInfoID int, DefenseCounselAddressID int, DefenseTotal money)
--Insert into @InvoiceTotal

----Invoices linked on NavLegacyIDs - Pre-KCIC transfer
--Select  cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID as DefenseCounselID, Sum(a.AdjustedTotal + a.AppealTotal) as DefenseTotal 
--From tblClaimantPersonalInfo cpi
--Inner Join m_tblLegalBillInvoices a on a.Navlegacyid=cpi.NavLegacyID
--Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and cpi.ClaimantPersonalInfoID is null
--Group By cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID

--Union

----Invoices linked on KCIC IDs
--Select  cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID as DefenseCounselID,  Sum(a.AdjustedTotal + a.AppealTotal) as DefenseTotal 
--From tblClaimantPersonalInfo cpi
--Inner Join m_tblLegalBillInvoices a on a.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
--Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID --and a.DefenseCounselAddressID = @DefenseCounselID
--Group By cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID


--Select	ClaimantPersonalInfoID, DefenseCounsel, i.DefenseCounselAddressID as DefenseCounselID, Sum(DefenseTotal) as DefenseTotal 
--From @InvoiceTotal i 
--Inner Join tblDefenseCounselAddress d on i.DefenseCounselAddressID = d.DefenseCounselAddressID
--Group By ClaimantPersonalInfoID, DefenseCounsel, i.DefenseCounselAddressID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GO
