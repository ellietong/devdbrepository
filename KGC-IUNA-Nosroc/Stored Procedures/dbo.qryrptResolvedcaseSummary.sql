SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptResolvedcaseSummary]
( @ResolvedDateStart  nvarchar(250), @ResolvedDateEnd nvarchar(250), @Matter int) 

as

Set NoCount On



--Declare @Matter int
--set @Matter = 1
--declare @resolveddatestart datetime
--set @resolveddatestart = '1/1/2011'
--declare @resolveddateend datetime
--set @resolveddateend = '12/31/2011'


------Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


Declare @resolvedSummary table (Matter nvarchar(100), MatterID int, ClaimNumber int, 
							
				SettlementAmount money,
				SettlementAverage money,
				ResolvedDate datetime,
				Meso_Count int,
				Meso_SettlementAmount money,
				Meso_Avg money,
				LungCancer_Count int,
				LungCancer_SettlementAmount money,
				LungCancer_Avg money,
				OtherCancer_Count int,
				OtherCancer_SettlementAmount money,
				OtherCancer_Avg money,
				NonMalig_Count int,
				NonMalig_SettlementAmount money,
				NonMalig_Avg money,
				Other_Count int,
				Other_SettlementAmount money,
				Other_Avg money,
				Null_Count int,
				Null_SettlementAmount money,
				Null_Avg money)

Insert Into @resolvedSummary 
Select Matter, c.MatterID, count(c.ClaimID) as ClaimantNumber,
		coalesce(sum(SettlementAmount),0) as SettlementAmount,
	coalesce(sum(SettlementAmount),0)/count(c.ClaimID) as SettlementAverage, 
	convert(datetime,(convert(nvarchar,month(coalesce(SettlementDate,statusdate)))+'/1/'+ convert(nvarchar,year(coalesce(SettlementDate,statusdate))))) as ResolvedDate,

sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) as Meso_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end) as Meso_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = (1) then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) end  as Meso_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) as LungCancer_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end) as LungCancer_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = (2) then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = (2) then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) end  as LungCancer_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) as OtherCancer_Count,	
	sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end) as OtherCancer_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) end  as OtherCancer_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) as NonMalig_Count,	
	sum(Case When HLD.HighLevelDiseaseID in (4,7) then SettlementAmount else 0 end) as NonMalig_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID in (4,7)then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID in (4,7) then 1 else 0 end) end  as NonMalig_Avg,
			
	Sum(Case When HLD.HighLevelDiseaseID IN (5, 6) then 1 else 0 end) as Other_Count,	
	Sum(Case When HLD.HighLevelDiseaseID IN (5, 6) then SettlementAmount else 0 end) as Other_SettlementAmount,			
	 	Case When sum(Case When HLD.HighLevelDiseaseID in (5,6) then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID in (5,6)then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID in (5,6) then 1 else 0 end) end  as Other_Avg,

	Sum(Case When HLD.HighLevelDiseaseID is null then 1 else 0 end) as Null_Count,				
	Sum(Case When HLD.HighLevelDiseaseID  is null then SettlementAmount else 0 end) as Null_SettlementAmount,
		Case When sum(Case When HLD.HighLevelDiseaseID is null then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID is null then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID is null then 1 else 0 end) end  as Null_Avg
	
From tblClaimantPersonalInfo i 
	Left Join tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
	LEFT JOIN tblMatter M on c.MatterID = m.MatterID
	Left Join tblClaimLawsuit l on l.claimID = c.claimID
	Left Join tblLawsuit law on l.lawsuitid = law.lawsuitid
	left Join tblCLaimantSettlement s on s.claimlawsuitID = l.claimlawsuitID
	Left Join tblDisease D on CD.DiseaseID = D.DiseaseID
	left join tblHighLevelDisease HLD on HLD.HighLevelDiseaseID=D.HighLevelDiseaseID
	left join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = l.ClaimLawsuitID
left join tblclaimlawsuitstatusgroup csg on csg.claimlawsuitstatusid = cst.ClaimLawsuitStatusID

Where IsPrimaryForClaim = 1 and ProcessedDate >= COALESCE(@resolveddatestart, '1/1/'+CONVERT(nvarchar(4), YEAR(GETDATE()))) and ProcessedDate <= COALESCE(@resolvedDateEnd, GETDATE())
	AND C.Matterid >= COALESCE(@Matter,0) AND C.Matterid <= COALESCE(NULLIF(@Matter,0),@maxMatterID) and statusgroupID in (2,3)
	 and coalesce(cst.IsPrimaryStatus,0) = 1
	AND COALESCE(i.jobsiteinfo, 0) = 0
Group By Matter, c.MatterID, 	convert(datetime,(convert(nvarchar,month(coalesce(SettlementDate,statusdate)))+'/1/'+ convert(nvarchar,year(coalesce(SettlementDate,statusdate))))) 



Select Matter, MatterID, 
coalesce(ClaimNumber,0) as ClaimNumber, 
	Coalesce(resolveddate, 0) as resolvedDate,
	YEAR(Coalesce(resolveddate, 0)) as resolvedYear,
	MONTH(Coalesce(resolveddate, 0)) as resolvedMonth,
	coalesce(SettlementAmount,0) as SettlementAmount,
	coalesce(SettlementAverage,0) as SettlementAverage,
	coalesce(Meso_Count,0) as Meso_Count,
	coalesce(Meso_SettlementAmount,0) as Meso_SettlementAmount,
	coalesce(Meso_Avg,0) as Meso_Avg,
	coalesce(LungCancer_Count,0) as LungCancer_Count,
	coalesce(LungCancer_SettlementAmount,0) as LungCancer_SettlementAmount,
	coalesce(LungCancer_Avg,0) as LungCancer_Avg,
	coalesce(OtherCancer_Count,0) as OtherCancer_Count,
	coalesce(OtherCancer_SettlementAmount,0) as OtherCancer_SettlementAmount,
	coalesce(OtherCancer_Avg,0) as OtherCancer_Avg,
	coalesce(NonMalig_Count,0)as NonMalig_Count,
	coalesce(NonMalig_SettlementAmount,0)as NonMalig_SettlementAmount,
	coalesce(NonMalig_Avg,0)as NonMalig_Avg,
	coalesce(Other_Count,0) + coalesce(Null_Count,0) as Other_Count,
	coalesce(Other_SettlementAmount,0)+ coalesce(Null_SettlementAmount,0) as Other_SettlementAmount,
	coalesce(Other_Avg,0)+coalesce(Null_Avg,0)  as Other_Avg,
 	@resolvedDateStart as startdate, @resolvedDateEnd as Enddate
From @resolvedSummary 
Order By Coalesce(ResolvedDate,0)

RETURN

 

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedcaseSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptResolvedcaseSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedcaseSummary] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qryrptResolvedcaseSummary] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedcaseSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptResolvedcaseSummary] TO [power_user]
GO
