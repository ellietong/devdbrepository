SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup]
AS
SET NOCOUNT ON

SELECT statusid, status
FROM tblDefenseInvoiceStatus

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [power_user]
GO
