SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptAllocationByPolicyByExpectedPayYear]
(@allocationID int = null, @insuranceProgramID int = NULL, 
@carriergroupid int = NULL, @CarrierID int = null, @limittypeid int, 
@carrierSolvency varchar(50) = NULL)

AS

SET NOCOUNT ON

 
 --DECLARE @allocationid int
 --DECLARE @insuranceprogramid int
 --DECLARE @carriergroupid int
 --DECLARE @carrierid int
 --DECLARE @limittypeid int
 --DECLARE @carrierSolvency varchar(50)
 
 --SET @allocationid = 95
 --SET @insuranceprogramid = 0
 --SET @carriergroupid = 0
 --SET @CarrierID = 0
 --SET @limittypeid = 3
 --SET @carrierSolvency = null
 
 
DECLARE @solvency varchar(50)
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @maxInsuranceProgramID int
--SET Variables
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxInsuranceProgramID = MAX (InsuranceProgramID) FROM vPolicy
IF @CarrierGroupID = 0 SET @CarrierGroupID = NULL



DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation

Declare @ExhaustionTypeID int
Set @exhaustiontypeid = (Select ExhaustionTypeID From tblAllocation Where AllocationID = @allocationID)


/* Policy Information */
------------------------
DECLARE @Policies table (PolicyID int, CGID int, PolicyDateid int, CarrierGroupName nvarchar(101),
CarrierName nvarchar(101), PolicyNum nvarchar(101), Pstartdate datetime, Penddate datetime,
Attachmentpoint money, PerOccLimit money, insuranceprogramid int, insuranceprogram nvarchar(101),
LayerPercent decimal(18,15), LayerPerOccLimit money, Exhaustion money)

DECLARE @exh table (ExhaustionTypeID int, PolicyDateID int, PreviouslyExhaustedAggregate money)
INSERT INTO @exh (ExhaustionTypeID, PolicyDateID, PreviouslyExhaustedAggregate)
SELECT ExhaustionTypeID, e.PolicyDateID, SUM(IndemnityAllocation)
FROM tblExhaustion e INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID
	INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
WHERE ExhaustionTypeID = @exhaustionTypeID
GROUP BY ExhaustionTypeID, e.PolicyDateID


INSERT INTO @Policies (PolicyID, CGID, PolicyDateID, CarrierGroupName, CarrierName, PolicyNum, Pstartdate,Penddate,
AttachmentPoint, PerOccLimit, insuranceprogramid, insuranceprogram, LayerPercent, LayerPerOccLimit, Exhaustion)

SELECT p.PolicyID,
CASE WHEN cg.CarrierGroupID is NULL THEN c.CarrierID ELSE cg.CarrierGroupID END as CGID,
 pd.PolicyDateID, cg.CarrierGroupName, CarrierName, PolicyNum, Pstartdate, Penddate, attachmentpoint,
perocclimit, p.insuranceprogramid, i.insuranceprogram, PerOccLimit/LayerPerOccLimit AS LayerPercent, LayerPerOccLimit,
e.PreviouslyExhaustedAggregate AS Exhaustion 
FROM tblPolicy p
INNER JOIN tblPolicyDates pd ON p.policyid = pd.policyid
INNER JOIN tblPolicyLimits pl ON p.policyid = pl.policyid
INNER JOIN tblCarrier c on p.carrierid = c.carrierid
LEFT JOIN tblCarrierGroup cg ON c.carriergroupid = cg.carriergroupid
INNER JOIN tblInsuranceProgram i ON p.insuranceprogramid = i.insuranceprogramid
LEFT JOIN @exh e ON pd.PolicyDateID = e.PolicyDateID
WHERE (c.CarrierGroupID >= COALESCE(@carrierGroupID, 0) AND c.CarrierGroupID <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR c.CarrierGroupID = @carrierGroupID) 
 AND limittypeid = @limittypeID 
 AND CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%')
 and p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
 AND p.CarrierID >= COALESCE(@carrierID, 0) AND p.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID)
 and LayerPerOccLimit >0

/* Expected Pay Years */
------------------------
DECLARE @CostsConsidered table (AllocationID int, OccurrenceID bigint, ExpectedPayDate datetime)

INSERT INTO @CostsConsidered (AllocationID, OccurrenceID, ExpectedPayDate)
SELECT @AllocationID, OccurrenceID, ExpectedPayDate
FROM tblOccurrence o
WHERE OccurrenceID NOT IN
	(SELECT aoe.OccurrenceID 
	FROM tblAllocation a 
		INNER JOIN tblAllocationOccurrenceExclude aoe ON a.AllocationID = aoe.AllocationID 
	WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID)
	) AND 
OccurrenceTypeID IN
	(SELECT aote.OccurrenceTypeID 
	FROM tblAllocation a INNER JOIN tblAllocationOccurrenceTypeInclude aote ON a.AllocationID = aote.AllocationID 
	WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID)
	) AND OccurrenceID is not null
GROUP BY OccurrenceID, ExpectedPayDate

/* Allocation */
----------------


DECLARE @AllocationTotals table (AllocationID int,InsuranceProgramID int, OccurrenceID bigint,
NominalAllocation money, PV money, PolicyDateID int, DateToPresentValue datetime, PVRateDate datetime, AllocationDescription nvarchar (500),
IndemnityAllocation money,DefenseAllocation money,DefenseOutsideAllocation money)

INSERT INTO @AllocationTotals (AllocationID, InsuranceProgramID, OccurrenceID, NominalAllocation, PV, PolicyDateID, DateToPresentValue, PVRateDate, AllocationDescription,IndemnityAllocation, DefenseAllocation,DefenseOutsideAllocation )

SELECT @allocationID, @InsuranceProgramID, ar.OccurrenceID, Sum(Allocation), Sum(PV), p.PolicyDateID, DateToPresentValue, YieldCurveDate as PVRateDate, A.AllocationDescription, sum(ar.AllocationIndemnity), sum(ar.AllocationDefense), sum(ar.AllocationDefenseOutsideLimits)
FROM tblAllocationResults ar 
INNER JOIN vPolicy p ON p.policydateid = ar.policydateid 
	INNER JOIN tblAllocation a ON a.AllocationID = ar.AllocationID
	left JOIN tblYieldCurve yc on yc.YieldCurveID = ar.yieldcurveid
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) 
AND (p.CarrierGroupID >= COALESCE(@carrierGroupID, 0) AND p.CarrierGroupID <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR p.CarrierGroupID = @carrierGroupID) 
 AND p.limittypeid = @limittypeID 
 AND CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%')
 and p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
 AND p.CarrierID >= COALESCE(@carrierID, 0) AND p.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID)
GROUP BY a.AllocationID, ar.OccurrenceID, InsuranceProgramID, p.policydateid, DateToPresentValue, YieldCurveDate, A.AllocationDescription



/* Summarize Results */
-----------------------
SELECT p.PolicyID, @AllocationID AS AllocationID, CarrierGroupName, CarrierName, PolicyNum, AllocationDescription,
CONVERT(nvarchar,PstartDate,101) as startdate, 
CONVERT(nvarchar,Penddate,101) as penddate, AttachmentPoint, 
PerOccLimit, LayerPercent, LayerPerOccLimit, COALESCE(Exhaustion,0) as Exhaustion, 
Year(ExpectedPayDate) as ExpectedPayDate, COALESCE(Sum(NominalAllocation),0) AS NominalAllocation, COALESCE(Sum(IndemnityAllocation),0) AS IndemnityAllocation, 
COALESCE(Sum(DefenseAllocation),0) AS DefenseAllocation,COALESCE(Sum(DefenseOutsideAllocation),0) AS DefenseOutsideAllocation,
COALESCE(Sum(PV),0) as PV, pstartdate, DateToPresentValue, PVRateDate
FROM @CostsConsidered c
LEFT JOIN @AllocationTotals a ON c.OccurrenceID = a.OccurrenceID
LEFT JOIN @Policies p ON a.PolicydateID = p.PolicydateID
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) 
AND a.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND a.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
GROUP BY p.PolicyID, a.AllocationID, CarrierGroupName, CarrierName, PolicyNum, PstartDate, Penddate, AttachmentPoint,
PerOccLimit, Year(ExpectedPayDate), LayerPercent, LayerPerOccLimit, Exhaustion, DateToPresentValue, PVRateDate, AllocationDescription
ORDER BY CarrierName, PStartDate, AttachmentPoint, Year(ExpectedPayDate)

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByPolicyByExpectedPayYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByPolicyByExpectedPayYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByPolicyByExpectedPayYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByPolicyByExpectedPayYear] TO [power_user]
GO
