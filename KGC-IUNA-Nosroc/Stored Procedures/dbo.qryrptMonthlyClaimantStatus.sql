SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptMonthlyClaimantStatus] (@StatusDateBegin datetime, @StatusDateEnd datetime, @groupid int, @header nvarchar(100), @Matter int)

As
SET NOCOUNT ON
 


-- Declare @StatusDateBegin datetime
-- Declare @StatusDateend datetime
-- Set @StatusDateBegin='6/8/2011'
--Set @StatusDateend='6/10/2011'
--Declare @header nvarchar(100)
--set @header = 'Settled Claimants'
--Declare @Matter int
--Set @Matter = 1
--declare @groupid int
--set @groupid = 1

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


Select 	C.ClaimID, LastName + ', '+ coalesce([FirstName], '') + ' '+ coalesce([MiddleName], '') as ClaimantName,
CASE WHEN GenderID = 2 THEN 'F' WHEN GenderID = 3 THEN 'M' ELSE 'U' END AS Gender, cpi.BirthDate, DeceasedDate, 
case when hld.highleveldiseaseid = 5 then 'Unknown' else Disease + '-'+ ICD9 end as Disease, Jurisdiction ,DocketNumber,pca.PlaintiffCounsel,    
dca.DefenseCounsel, case when statusgroupid = 1 then servicedate
	 when statusgroupid = 2 then cs.SettlementDate  when statusgroupid =3 then cls.statusdate end as statusdate,		 
	@header as header, CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
	, @StatusDateBegin as StatusDateBegin,
	Coalesce(@StatusDateEnd,convert(varchar(10),getdate(),101)) as StatusDateEnd,
	 ClaimLawsuitStatus ,
	   case when SettlementAmount is null then 'N/A' else convert(nvarchar, SettlementAmount) end as SettlementAmount, s.State, l.FileDate
	  
From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalInfoID=C.ClaimantPersonalInfoID
Left Join (select * from tblClaimDisease where PrimaryDisease = 1) cd on cd.ClaimID = c.ClaimID
Left Join tblDisease D on cd.DiseaseID=D.DiseaseID
Left Join tblHighLevelDisease HLD on D.HighLevelDiseaseID=HLD.HighLevelDiseaseID
Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
left Join tblClaimantSettlement CS on CL.ClaimLawsuitID=CS.CLaimLawsuitID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblState S on L.StateID=S.StateID
Left Join tblMatter M on C.MatterID = m.MatterID
left join tblCourtType ct on ct.CourtTypeID = L.CourtTypeID
left join tblJurisdiction j on j.JurisdictionID = L.JurisdictionID
inner join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = l.DefenseCounselAddressID
inner join tblClaimLawsuitStatusTracking cls on cls.ClaimLawsuitID = cl.ClaimLawsuitID
inner join tblClaimLawsuitStatus css on css.claimlawsuitstatusid = cls.ClaimLawsuitStatusID
left join tblclaimlawsuitstatusgroup clg on clg.claimlawsuitstatusid = css.ClaimLawsuitStatusID
left join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
 

Where  cls.IsPrimaryStatus = 1 and clg.statusgroupid = @groupid
and processeddate between @statusdatebegin and @statusdateend

and c.MatterID >= COALESCE(@Matter,0) AND c.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) and ClaimSequenceID not in (2,3,4,11) and coalesce(cpi.JobSiteInfo,0) = 0

GROUP BY 	C.ClaimID, LastName + ', '+ coalesce([FirstName], '') + ' '+ coalesce([MiddleName], ''),  cpi.BirthDate,
   case when SettlementAmount is null then 'N/A' else convert(nvarchar, SettlementAmount)end, case when statusgroupid = 1 then servicedate
	 when statusgroupid = 2 then cs.SettlementDate  when statusgroupid =3 then cls.statusdate end,  DeceasedDate, DocketNumber,  
	 CASE WHEN GenderID = 2 THEN 'F' WHEN GenderID = 3 THEN 'M' ELSE 'U' END ,
	 Jurisdiction,   case when hld.highleveldiseaseid = 5 then 'Unknown' else Disease + '-'+ ICD9 end , dca.DefenseCounsel, ClaimLawsuitStatus, pca.PlaintiffCounsel,
	 s.State, l.FileDate
	
 
Order By    case when statusgroupid = 1 then servicedate
	 when statusgroupid = 2 then cs.SettlementDate  when statusgroupid =3 then cls.statusdate end ,
LastName + ', '+ coalesce([FirstName], '') + ' '+ coalesce([MiddleName], '')

RETURN

 




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptMonthlyClaimantStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptMonthlyClaimantStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptMonthlyClaimantStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptMonthlyClaimantStatus] TO [power_user]
GO
