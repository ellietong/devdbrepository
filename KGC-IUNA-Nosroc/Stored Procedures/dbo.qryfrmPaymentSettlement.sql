SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[qryfrmPaymentSettlement] (@PlaintiffCounselAddressID int, @MatterID int)
AS
SET NOCOUNT ON


Select cs.ClaimantSettlementID, PlaintiffCounselAddressID, cs.SettlementGroupID, LastName + ', ' + Coalesce(FirstName,'') + ' ' + Coalesce(MiddleName, '') as Claimant, DocketNumber, SettlementAmount, SettlementAmount-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount, c.MatterID
From tblClaim c
INNER JOIN tblClaimSequence csq ON c.ClaimSequenceID = csq.ClaimSequenceID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID 
LEFT JOIN tblSettlementGroup s ON cs.SettlementGroupID = s.SettlementGroupID
LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID

LEFT JOIN (Select ClaimantSettlementID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentSettlement ps on ca.CheckID = ps.CheckID)
a on a.ClaimantSettlementID = cs.ClaimantSettlementID

Where IsPrimaryStatus = 1 AND
COALESCE(IsCurrentClaim,0) = 1
AND c.MatterID = @MatterID
AND l.PlaintiffCounselAddressID = @PlaintiffCounselAddressID
AND SettlementDueDate is not null

Group By cs.ClaimantSettlementID, PlaintiffCounselAddressID, LastName + ', ' + Coalesce(FirstName,'') + ' ' + Coalesce(MiddleName, ''), DocketNumber, SettlementAmount, MatterID, cs.SettlementGroupID
Having SettlementAmount-SUM(Coalesce(PaymentAmount,0)) <> 0
Order By cs.SettlementGroupID, LastName + ', ' + Coalesce(FirstName,'') + ' ' + Coalesce(MiddleName, '')

RETURN


 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [power_user]
GO
