SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryDocList] (@ClaimantPersonalInfoID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT d.DocumentID as DocumentID, COALESCE(ClaimantDocumentType +': ','') + DOCUMENTTITLE as DocumentTitle
    FROM tblDocuments D
    LEFT JOIN tblClaimantDocumentType CDT ON CDT.ClaimantDocumentTypeID = D.DetailDocumentTypeID
    inner join tblMultiClaimantDocument mcd on mcd.DocumentID=d.DocumentID
    where mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
    UNION
    SELECT NULL as DocumentID, '-- Select a Document --'  as DocumentTitle
    ORDER BY 2
    
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDocList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDocList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDocList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDocList] TO [power_user]
GO
