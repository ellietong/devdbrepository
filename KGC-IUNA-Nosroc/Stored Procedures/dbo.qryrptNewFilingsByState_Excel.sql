SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptNewFilingsByState_Excel] (@StateID   int, 
												  @StartDate datetime, 
												  @EndDate   datetime, 
												  @Matter    int)
AS
SET NOCOUNT ON

----Variables used for testing.
--DECLARE @stateid   int
--DECLARE @startdate datetime
--DECLARE @enddate   datetime
--DECLARE @matter    int
--SET @stateid   = NULL
--SET @startdate = '1/1/1900'
--SET @enddate   = '3/1/2012'
--SET @matter    = 1

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState

DECLARE @DiseaseCount TABLE (ClaimID               int, 
							 Matter                nvarchar(100), 
							 MatterID              int, 
							 FullNameState         nvarchar(250), 
							 YearFiled             int, 
							 Disease               nvarchar(100), 
							 lastname              nvarchar(100), 
							 firstname             nvarchar(100), 
							 socialsecuritynumber  nvarchar(250),
							 filedate              datetime, 
							 servicedate           datetime,
							 localplaintiffcounsel nvarchar(200), 
							 jurisdiction          nvarchar(200), 
							 docketnumber          nvarchar(100), 
							 ClaimSequence         nvarchar(200))
INSERT INTO @DiseaseCount
	SELECT c.ClaimID, 
		Matter, 
		c.MatterID, 
		s.FullNameState,
		COALESCE(YEAR(ServiceDate), YEAR(FileDate)) AS YearFiled, 
		COALESCE(HighLevelDiseaseID,5) AS Disease, 
		cpi.LastName, 
		cpi.FirstName, 
		COALESCE(RIGHT(CPI.SocialSecurityNumber,4),''), l.FileDate,
		l.ServiceDate, 
		pca.PlaintiffCounsel, 
		j.Jurisdiction, 
		DocketNumber, 
		ClaimSequence
	FROM tblClaimantPersonalInfo cpi
		LEFT JOIN tblClaim c                         ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblMatter m                        ON c.MatterID = m.MatterID
		LEFT JOIN (SELECT cd.ClaimDiseaseID, 
				       cd.ClaimID, 
				       cd.DiseaseDiagnosisDate, 
				       cd.DiseaseID, 
				       cd.DoctorID, 
				       cd.PrimaryDisease 
				   FROM tblClaimDisease CD 
				   WHERE PrimaryDisease = 1) cd      ON cd.ClaimID = c.ClaimID
		LEFT JOIN tblDisease d                       ON cd.DiseaseID = d.DiseaseID
		LEFT JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN tblPlaintiffCounselAddress pca     ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		LEFT JOIN tblJurisdiction j                  ON l.JurisdictionID = j.JurisdictionID
		LEFT JOIN tblState s                         ON l.StateID = s.StateID
		LEFT JOIN tblClaimSequence csq               ON csq.ClaimSequenceID = c.ClaimSequenceID
		LEFT JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	WHERE s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) 
		AND	ProcessedDate BETWEEN @StartDate AND @EndDate 
		AND IsPrimaryForClaim = 1 
		AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0), @maxMatterID)
		AND COALESCE(cpi.jobsiteinfo, 0) = 0 
		AND clst.ClaimLawsuitStatusID = 22
	GROUP BY s.State, c.ClaimID, Matter, c.MatterID, s.FullNameState, COALESCE(YEAR(ServiceDate), YEAR(FileDate)), 
		COALESCE(HighLevelDiseaseID,5), cpi.LastName, cpi.FirstName, COALESCE(RIGHT(cpi.SocialSecurityNumber,4),''), 
		l.FileDate, l.ServiceDate, pca.PlaintiffCounsel, j.Jurisdiction, DocketNumber, ClaimSequence
	ORDER BY s.State, YEAR(ServiceDate), Jurisdiction, pca.PlaintiffCounsel

SELECT Matter, 
	MatterID, 
	FullNameState, 
	YearFiled AS 'Year',
	hd.HighLevelDisease, 
	LastName, 
	FirstName, 
	SocialSecurityNumber, 
	coalesce(FileDate, '1/1/1900') AS 'filedate', 
	coalesce(ServiceDate,'1/1/1900') AS 'servicedate', 
	localplaintiffcounsel, 
	Jurisdiction, 
	DocketNumber,
	ClaimSequence,
	@startDate AS StartDate, 
	@EndDate AS EndDate
FROM @DiseaseCount DC
	LEFT JOIN tblHighLevelDisease hd on hd.HighLevelDiseaseID = DC.Disease
ORDER BY Matter, FullNameState, COALESCE(ServiceDate, FileDate), lastname, firstname

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByState_Excel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByState_Excel] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptNewFilingsByState_Excel] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByState_Excel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByState_Excel] TO [power_user]
GO
