SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryInternalNewDocumentPast4DayAcceptanceEmail]
As
Begin

/*Query run daily to send an internal email listing any documents 4 days old and unreviewed. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (2000)

SET @Subject = 'IUNA/NOSROC: Unaccepted Documents Over 4 Days Old ' + CONVERT(varchar, GETDATE(), 107)
--Set body font, insert KCIC logo/banner at the top of the email
--KCIC logo is being pulled from the KCIC site.  Contact D. Snyder with questions or requests.   
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>Below are documents which are over 4 days old and have not yet been accepted.</H4>'

--Create temp table to hold all new documents
DECLARE @NewDocuments TABLE (docID nvarchar (200), firm nvarchar (100), firmURL nvarchar (200))
INSERT INTO @NewDocuments
	SELECT
		spdocid,
		dc.DefenseCounsel,
		'https://admin.kcicllc.com/cp/iuna_nosroccp/Defense%20Counsel%20Documents/Forms/AllItems.aspx'
	FROM tblDocuments d
	INNER JOIN vtblPerson p ON p.Email = d.CreatedBy
	INNER JOIN tblDefenseCounsel dc ON dc.DefenseCounselID = p.firmid
	INNER JOIN vtblFirmCase fc ON fc.FirmID = d.FirmID
	WHERE datecreated < GETDATE() - 4
	AND (RIGHT(CreatedBy, 11) != 'kcicllc.com' OR RIGHT(CreatedBy, 8) != 'kcic.com')
	AND detailDocumentTypeID NOT IN (40, 41)
	AND COALESCE(Accepted, 0) <> 1
	GROUP BY	SPDocId,
				dc.DefenseCounsel,
				DocumentURL


--Create cursor for each firm
DECLARE @DocCount int, @firm nvarchar (200), @firmURL nvarchar (200)

DECLARE Document_Curs CURSOR FOR SELECT
	COUNT(DISTINCT docid),
	firm,
	FirmURL
FROM @newdocuments
GROUP BY	firm,
			firmurl
ORDER BY firm
--Open
OPEN Document_Curs
--Fetch
FETCH NEXT FROM Document_Curs INTO @DocCount, @firm, @firmURL

WHILE @@fetch_status = 0
BEGIN
--List which firm, how many documents, and the firm url
SET @BodyMessage = @BodyMessage + CONVERT(nvarchar(10), (SELECT
	@DocCount)
) + ' Document(s) in the <A HREF ="'
+ @firmURL + '">' + @firm + '</A> Document Library<br><br>'
FETCH NEXT FROM Document_Curs INTO @DocCount, @firm, @firmURL
END
CLOSE Document_Curs
DEALLOCATE Document_Curs


SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


--Check for new documents
IF (SELECT
	COUNT(DISTINCT docid)
FROM @newdocuments) < > 0
BEGIN
--Send email
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'iuna_nosroc@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_Format = 'html'
END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalNewDocumentPast4DayAcceptanceEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalNewDocumentPast4DayAcceptanceEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalNewDocumentPast4DayAcceptanceEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalNewDocumentPast4DayAcceptanceEmail] TO [power_user]
GO
