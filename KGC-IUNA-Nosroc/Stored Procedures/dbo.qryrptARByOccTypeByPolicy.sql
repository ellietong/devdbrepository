SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByOccTypeByPolicy]
(@LimitTypeID int = NULL, @allocationID int = null, @CarrierID INT = NULL, @Solvency varchar (50)= NULL, @CarrierGroupID int = NULL, @InsuranceProgramID INT = NULL)
As
SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 302
--DECLARE @CarrierID int
--SET @CarrierID = 0
--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = 0
--DECLARE @Solvency varchar (50)
--SET @Solvency = null
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID = 0

--DECLARE Variables
DECLARE @maxAllocationID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @maxInsuranceProgramID int
DECLARE @maxLimitTypeID int
--SET Variables
SELECT @maxAllocationID = MAX(AllocationID) from vAllocation
SELECT @maxCarrierID = MAX(CarrierID) from vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) from vPolicy
SELECT @maxInsuranceProgramID = MAX (InsuranceProgramID) from vPolicy
IF @CarrierGroupID = 0 SET @CarrierGroupID = NULL
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy

DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
CarrierID int, CarrierName nvarchar(250), CarrierSolvency nvarchar(25), PolicyNum nvarchar(100), 
AttachmentPoint money, PreviouslyExhaustedAggregate money, PerOccLimit money, LayerPerOccLimit money, 
SIR money, AggregateLimit money, Layer tinyint, InsuranceProgramID int, InsuranceProgram nvarchar(50),
LimitTypeID int)
INSERT INTO @Policy

SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, CarrierSolvency,
PolicyNum, AttachmentPoint, PreviouslyExhaustedAggregate, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, 
Layer, InsuranceProgramID, InsuranceProgram, LimitTypeID
FROM vPolicy p
WHERE LimitTypeID >= COALESCE(@LimitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@LimitTypeID, 0), @maxLimitTypeID )
GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, CarrierSolvency,
PolicyNum, AttachmentPoint, PreviouslyExhaustedAggregate, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, 
Layer, InsuranceProgramID, InsuranceProgram, LimitTypeID


SELECT pinfo.InsuranceProgram, o.OccurrenceType, o.OccurrenceTypeID, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' ('+ pinfo.CarrierGroupName + ')' END AS 'CarrierInfo', pinfo.PolicyNum, pinfo.PolicyID, pinfo.CarrierID, pinfo.CarrierGroupID, 
	pinfo.CarrierName, pinfo.CarrierGroupName, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END AS 'CarrierGroupInfo', a.AllocationID, a.AllocationDescription, 
	MAX(ar.DateTime) AS MaxOfDateTime, SUM(ar.Allocation) AS SumOfAllocation, CONVERT(money, SUM(ar.PV)) AS SumOfPV, SUM(ar.SIRAllocated) AS SumOfSIRAllocated, ar.DateToPresentValue, 
	ar.PVRate, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.PreviouslyExhaustedAggregate, pinfo.AggregateLimit, pinfo.AttachmentPoint, pinfo.PerOccLimit, ar.PStartSplit, ar.PEndSplit, ar.AnnualPeriodSplit,  
	CONVERT(varchar, pinfo.PolicyID) + CONVERT(varchar, ar.PStartSplit,103) AS PolicyIDPStartSplit, pr.ProvisionID, pr.ProvisionValueID, pr.ProvisionValue, YieldCurveDate as PVRateDate
FROM @Policy pinfo 
	INNER JOIN vProvisions pr ON pr.policyID = pinfo.policyID
	INNER JOIN vAllocationResults ar ON pinfo.PolicydateID = ar.PolicydateID
	INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID 
	INNER JOIN vOccurrence o ON ar.OccurrenceID = o.OccurrenceID
	LEFT JOIN tblYieldCurve yc on yc.YieldCurveID = ar.YieldCurveID 
WHERE pr.ProvisionLabelID = 1 AND a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) AND
	pinfo.CarrierID >= COALESCE(@CarrierID, 0) AND pinfo.CarrierID <= COALESCE(NULLIF(@CarrierID, 0), @maxCarrierID) AND 
	pinfo.CarrierSolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%') AND
	(coalesce(pinfo.CarrierGroupID,0) >= COALESCE(@CarrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID,0) <= COALESCE(@CarrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID,0) = @CarrierGroupID) AND 
	pinfo.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) 
GROUP BY pinfo.InsuranceProgram, o.OccurrenceType, o.OccurrenceTypeID, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' ('+ pinfo.CarrierGroupName + ')' END, 
	pinfo.PolicyNum, pinfo.PolicyID, pinfo.CarrierID, pinfo.CarrierGroupID, pinfo.CarrierName, pinfo.CarrierGroupName, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END,
	a.AllocationID, a.AllocationDescription, ar.DateToPresentValue, ar.PVRate, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.PreviouslyExhaustedAggregate, pinfo.AggregateLimit, pinfo.AttachmentPoint, pinfo.PerOccLimit, 
	ar.PStartSplit, ar.PEndSplit, ar.AnnualPeriodSplit, pr.ProvisionID, pr.ProvisionValueID, pr.ProvisionValue, YieldCurveDate

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccTypeByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccTypeByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccTypeByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccTypeByPolicy] TO [power_user]
GO
