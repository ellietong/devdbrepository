SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  Procedure [dbo].[qryrptOccurrenceCostByOccType] 
(@CorporateProfileID INT = NULL,@InClaim bit) 
As

SET NOCOUNT ON

--DECLARE @CorporateProfileID int
--SET @CorporateProfileID = 0
--DECLARE @InClaim bit
--SET @InClaim = 1

DECLARE @maxCorporateProfileID int
SELECT @maxCorporateProfileID  = MAX(CorporateHistoryID) FROM vOccurrence

IF @InClaim IS NULL
BEGIN
	SELECT @InClaim = 0

	SELECT InClaim, OccurrenceTypeID, OccurrenceType, CorporateHistoryID, CorporateHistoryProfile, SUM(Cost1) AS SumOfCost1, Sum(Cost2) AS SumOfCost2, 
		SUM(Cost3) AS SumOfCost3, SUM(Cost4) AS SumOfCost4, SUM(Cost5) AS SumOfCost5, SUM(Cost6) AS SumOfCost6, SUM(Cost7) AS SumOfCost7, 
		SUM(Cost8) AS SumOfCost8, SUM(Cost9) AS SumOfCost9, SUM(Cost10) AS SumOfCost10, SUM(Cost11) AS SumOfCost11, SUM(Cost12) AS SumOfCost12
	FROM vOccurrence
	WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
		InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), 1)
	GROUP BY InClaim, OccurrenceTypeID, CorporateHistoryID, OccurrenceType, CorporateHistoryProfile
END 
ELSE
	SELECT InClaim, OccurrenceTypeID, OccurrenceType, CorporateHistoryID, CorporateHistoryProfile, SUM(Cost1) AS SumOfCost1, SUM(Cost2) AS SumOfCost2, 
		SUM(Cost3) AS SumOfCost3, SUM(Cost4) AS SumOfCost4, SUM(Cost5) AS SumOfCost5, SUM(Cost6) AS SumOfCost6, SUM(Cost7) AS SumOfCost7, 
		SUM(Cost8) AS SumOfCost8, SUM(Cost9) AS SumOfCost9, SUM(Cost10) AS SumOfCost10, SUM(Cost11) AS SumOfCost11, SUM(Cost12) AS SumOfCost12
	FROM vOccurrence
	WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
		InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), @InClaim)
	GROUP BY InClaim, OccurrenceTypeID, CorporateHistoryID, OccurrenceType, CorporateHistoryProfile

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrenceCostByOccType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOccurrenceCostByOccType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrenceCostByOccType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOccurrenceCostByOccType] TO [power_user]
GO
