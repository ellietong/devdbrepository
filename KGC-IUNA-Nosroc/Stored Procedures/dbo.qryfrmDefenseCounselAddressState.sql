SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmDefenseCounselAddressState]
(@DefCounselID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @MaxDCID int
	DECLARE @MinDCID int
	SET @MaxDCID = (SELECT MAX(DefenseCounselID) FROM tblDefenseCounselAddress)
    SET @MinDCID = (SELECT MIN(DefenseCounselID) FROM tblDefenseCounselAddress)
    
    IF (@DefCounselID = 0)
		SET @DefCounselID = NULL
    
    SELECT DefenseCounselID, DefenseCounsel
    FROM tblDefenseCounselAddress
    WHERE DefenseCounselID >= COALESCE(@DefCounselID, @MinDCID) AND DefenseCounselID <= COALESCE(@DefCounselID, @MaxDCID)
    GROUP BY DefenseCounselID, DefenseCounsel
	ORDER BY DefenseCounsel
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefenseCounselAddressState] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDefenseCounselAddressState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefenseCounselAddressState] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDefenseCounselAddressState] TO [power_user]
GO
