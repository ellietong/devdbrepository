SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_qryExternalWilbrahamInvoiceLoaded] 
	
AS
BEGIN
/*External email to alert that Wilbraham Lawler has uploaded an invoice. - BEH*/

--Declare and set sp_send_dbmail input variables
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(max)

SET @Subject = 	'IUNA/Nosroc: Wilbraham, Lawler & Buba Invoice(s) Uploaded ' + CONVERT(varchar, GETDATE(), 107) 
--Set the font for the entire email, and include the KCIC logo/banner on the top and the header for table 1
--KCIC logo is being pulled from the KCIC site.  Contact D. Snyder with questions or requests.	
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>
<H4>Wilbraham Lawler & Buba uploaded the following invoice(s) on ' + CONVERT(varchar, GETDATE(), 107) + ':</H4>'

--Create temp table for invoices which have been uploaded by WLB in the past day.
DECLARE @NewInvoicesTable TABLE (spDocID nvarchar(100), FirmID int,[status] nvarchar(30), invoiceNum nvarchar(100), 
		period nvarchar(100), fees  money, disbursement money, adjustment money, submitDate datetime, datePayable datetime, Matter nvarchar(50))
INSERT INTO @NewInvoicesTable
SELECT SPDocID, FirmID, [Status], InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, DateCreated, DatePayable, Matter
FROM tblDocuments d LEFT JOIN tblDefenseInvoiceStatus s ON d.StatusID=s.StatusID
INNER Join tblMatter m ON m.MatterID = d.MatterID
WHERE Datecreated BETWEEN GETDATE()-1 AND GETDATE() 
	AND detailDocumentTypeID IN (0,40,41)
	--This email only includes invoices uploaded by Wilbraham Lawler
	AND FirmID = 38
GROUP BY DocumentURL, SPDocId, FirmID, [Status], InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, DateCreated, DatePayable, Matter


--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
Set @BodyMessage = @BodyMessage + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	border: 1px solid #38160C;
	color: #F6ECF0;}
TD	{font-family: Goudy Old Style; 
	font-size: 10pt; 
	text-align: center;
	border: 1px solid rgb(0,121,193);
	padding: .3em;
	color: #000000}
TH {font-family: Goudy Old Style; 
	font-size: 10pt; 
	border: 1px solid #ffffff;
	padding: .3em;
	color: #F6ECF0;
	background: rgb(0,121,193);}
--->
</STYLE>'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @bodymessage + N'<table>' + 
    N'<tr><th>Firm</th><th>Invoice Number</th><th>Period</th><th>Fees</th>' + 
	'<th>Expenses</th><th>Total Billed</th><th>Submit Date</th><th>Date Payable</th><th>Matter</th>' +
    CAST ( (
		SELECT  td = dc.DefenseCounsel, '', td = invoicenum, '', td = period, '', 
				td = '$'+convert(nvarchar(30), coalesce(fees, 0), 1), '', 
				td = '$'+convert(nvarchar(30), coalesce(disbursement, 0), 1), '', 
				td = '$'+convert(nvarchar(30),(coalesce(fees, 0)+coalesce(disbursement, 0)), 1), '', 
				td = convert(nvarchar(100), submitdate, 101), '', 
				td = convert(nvarchar(100), datepayable, 101), '',
				td = Matter, ''
		FROM @NewInvoicesTable i
			INNER JOIN tblDefenseCounsel dc ON dc.DefenseCounselID = i.firmID
			ORDER BY dc.DefenseCounsel, submitdate, Matter
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	N'</table>' +  N'<br/>'



--Include link to the King and Spalding Invoice library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Invoices.aspx">here</A> to access the invoice(s).</b><br><br>'

--Include KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC <br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'
IF(SELECT COUNT(DISTINCT spdocid)
FROM @NewInvoicesTable) <> 0	
BEGIN
--Execute email-sending stored procedure
EXEC msdb.dbo.sp_send_dbmail
@profile_name='IUNA/NosrocProfile',
@recipients = 'j.heenan@envirosafeservices.com',
@copy_recipients = 'iuna_nosroc@kcicllc.com',
@subject= @subject,
@body= @BodyMessage,
@body_format = 'html'
END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalWilbrahamInvoiceLoaded] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalWilbrahamInvoiceLoaded] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalWilbrahamInvoiceLoaded] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalWilbrahamInvoiceLoaded] TO [power_user]
GO
