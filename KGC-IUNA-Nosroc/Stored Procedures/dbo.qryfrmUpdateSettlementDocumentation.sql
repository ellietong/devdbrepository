SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmUpdateSettlementDocumentation]
(@ClaimLawsuitID int, @DocumentID int, @DocCategoryID int, @PageNums varchar)

As
Set NoCount On

--DECLARE @ClaimLawsuitID int
--declare @DocumentID int
--declare @DocCategoryID int
--declare @PageNums varchar

--set @ClaimLawsuitID = 52237
--set @DocumentID = 973
--set @DocCategoryID = 4 
--set @PageNums = '1'



DECLARE @NewSettlePaymentDocumentationID INT


--if there is no record for this claim in tblSettlePaymentDocumentation then create one
if (select COUNT(setpymtid) from tblSettlePaymentDocumentation where ClaimLawsuitID = @ClaimLawsuitID) = 0 
begin
Insert into tblSettlePaymentDocumentation (ClaimLawsuitID)
select @ClaimLawsuitID
set @NewSettlePaymentDocumentationID = (select MAX(SetPymtID) from tblSettlePaymentDocumentation)
end
ELSE
begin
set @NewSettlePaymentDocumentationID = (Select setpymtID from tblSettlePaymentDocumentation where ClaimLawsuitID = @ClaimLawsuitID)
end

--if there is no current settlementdocumentation record for this document then update it
if (Select count(SetPymtID) from tblSettleDocs where DocumentID = @DocumentID and DocCategoryID = @DocCategoryID and PageNums = @PageNums and SetPymtID is not null) =0
begin
update tblSettleDocs set SetPymtID = @NewSettlePaymentDocumentationID 
where DocumentID = @DocumentID and DocCategoryID = @DocCategoryID and PageNums = @PageNums
end
else
--otherwise insert new record for this document
begin
insert into tblSettleDocs (DocumentID, DocCategoryID, PageNums, SetPymtID)
select @DocumentID, @DocCategoryID, @PageNums, @NewSettlePaymentDocumentationID
end
--update the settlementdocumentation record to include this document
--1	Claimant Identified
--2	Diagnosing Medicals
--3	Death Certificate
--4	Estate Paperwork
--5	Product Identification
--6	Emloyment/Exposure History
--7	MMSEA Settlement Form (Not in IUNA)
--8	Medicare Representation of Plaintiff (Not in IUNA)
--9	Medicare Representation of Plaintiff Counsel (Not in IUNA)
if @DocCategoryID = 1 
begin
update tblSettlePaymentDocumentation set ClaimantIDed = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 2 
begin
update tblSettlePaymentDocumentation set DxMedicals = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 3
begin
update tblSettlePaymentDocumentation set DODCert = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 4
begin
update tblSettlePaymentDocumentation set EstatePapers = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 5
begin
update tblSettlePaymentDocumentation set ProductID = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 6
begin
update tblSettlePaymentDocumentation set EmployExpoHist = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
--if @DocCategoryID = 7
--begin
--update tblSettlePaymentDocumentation set MMSEA = 1 
--where SetPymtID = @NewSettlePaymentDocumentationID
--end
--if @DocCategoryID = 8
--begin
--update tblSettlePaymentDocumentation set MedicarePlaintiff = 1 
--where SetPymtID = @NewSettlePaymentDocumentationID
--end
--if @DocCategoryID = 9
--begin
--update tblSettlePaymentDocumentation set MedicarePlaintiffCounsel = 1 
--where SetPymtID = @NewSettlePaymentDocumentationID
--end

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateSettlementDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateSettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateSettlementDocumentation] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateSettlementDocumentation] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateSettlementDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateSettlementDocumentation] TO [power_user]
GO
