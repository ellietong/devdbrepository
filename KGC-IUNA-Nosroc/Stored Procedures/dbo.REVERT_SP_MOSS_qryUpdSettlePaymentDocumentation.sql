SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[REVERT_SP_MOSS_qryUpdSettlePaymentDocumentation] (@SetPymtID int, @SettlePaymentComments nvarchar(max), @ReleaseTypeID int,
 @ExecutedReleaseDate Date, @MedicareFullyDocumented bit, @MedicareReported bit, @MedicareFormRcvd datetime, @ReleaseToPC datetime,
 @MedicareAddendumSent datetime, @MedicareAddendumRcvd datetime)
as

--DECLARE @ClaimID int
--SET @ClaimID = 1

update tblSettlePaymentDocumentation
set SettlePaymentComments = @SettlePaymentComments, 
ReleaseTypeID = CASE WHEN @ReleaseTypeID = 0 THEN NULL ELSE @ReleaseTypeID END, 
ExecutedReleaseDate = nullif(@ExecutedReleaseDate,''), 
	MedicareFullyDocumented = @MedicareFullyDocumented, 
	MedicareReported = @MedicareReported,
	MedicareFormRcvd = nullif(@MedicareFormRcvd, ''), 
	ReleaseToPC = @ReleaseToPC, 
	MedicareAddendumSent = nullif(@MedicareAddendumSent, ''), 
	MedicareAddendumRcvd = nullif(@MedicareAddendumRcvd, '')
where SetPymtID = @SetPymtID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdSettlePaymentDocumentation] TO [power_user]
GO
