SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_INVOICE_qryInvoiceStatusFilterList]
AS
SET NOCOUNT ON

SELECT statusid, status
FROM tblDefenseInvoiceStatus
WHERE StatusID <> 3
UNION
SELECT NULL, '<< All Statuses >>'
ORDER BY Status
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryInvoiceStatusFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryInvoiceStatusFilterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryInvoiceStatusFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryInvoiceStatusFilterList] TO [power_user]
GO
