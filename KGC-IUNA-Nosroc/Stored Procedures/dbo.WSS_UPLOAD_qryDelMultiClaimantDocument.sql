SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryDelMultiClaimantDocument]
(@MultiClaimantDocumentID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM tblMultiClaimantDocument
	WHERE MultiClaimantDocumentID = @MultiClaimantDocumentID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDelMultiClaimantDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDelMultiClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDelMultiClaimantDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDelMultiClaimantDocument] TO [power_user]
GO
