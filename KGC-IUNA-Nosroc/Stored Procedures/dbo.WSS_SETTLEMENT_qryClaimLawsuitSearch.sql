SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] 
(@ClaimantName nvarchar(500) = null, @StateID int = null, 
@SSN nvarchar(15) = null, @MatterID int = null, @DocketNumber nvarchar(500) = null,
@CaseCaption nvarchar(500) = null, @HighLevelDiseaseID int = null, @PlaintiffCounselAddressID int = null, @ClaimantIDs integer_list_tbltype READONLY)
AS
BEGIN

--DECLARE @ClaimLawsuitID bigint = 226053
--DECLARE @ClaimantName nvarchar(500) = 'smith'
--DECLARE @StateID int = 17
--DECLARE @SSN nvarchar(15) = ''
--DECLARE @MatterID int = null
--DECLARE @DocketNumber nvarchar(500) = null
--DECLARE @CaseCaption nvarchar(500) = null
--DECLARE @HighLevelDiseaseID int = null
--DECLARE @PlaintiffCounselAddressID int = null

SET NOCOUNT ON

IF object_id('tempdb..#ClaimantIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimantIDs
END
CREATE TABLE #ClaimantIDs (n int)
INSERT INTO #ClaimantIDs
	SELECT n FROM @ClaimantIDs

DECLARE @WhereString nvarchar(MAX) = ''

--Construct Where String
IF(@ClaimantName IS NOT NULL)
	SET @WhereString = @WhereString + ' AND LastName + '', '' + FirstName + COALESCE('' '' + MiddleName, '''') LIKE ''%' + @ClaimantName + '%'''
IF(@StateID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND cpi.StateID = ' + CONVERT(nvarchar, @StateID)
IF(@SSN IS NOT NULL)
	SET @WhereString = @WhereString + ' AND cpi.SocialSecurityNumber LIKE ''%'+ @SSN + '%'''
IF(@MatterID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar, @MatterID)
IF(@DocketNumber IS NOT NULL)
	SET @WhereString = @WhereString + ' AND l.DocketNumber LIKE ''%' + @DocketNumber + '%'''
IF(@CaseCaption IS NOT NULL)
	SET @WhereString = @WhereString + ' AND l.CaseCaption LIKE ''%' + @CaseCaption + '%'''
IF(@HighLevelDiseaseID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND d.HighLevelDiseaseID = ' + CONVERT(nvarchar, @HighLevelDiseaseID)
IF(@PlaintiffCounselAddressID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND l.PlaintiffCounselAddressID = ' + CONVERT(nvarchar, @PlaintiffCounselAddressID)
IF (SELECT COUNT(n) FROM #ClaimantIDs) <> 0 
    SET @WhereString = @WhereString + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)'	


DECLARE @QueryString nvarchar(MAX) = 
'
SELECT 
		c.ClaimantPersonalInfoID,
		cl.ClaimLawsuitID, 
		LastName + '', '' + FirstName + COALESCE('' '' + MiddleName, '''')  as ClaimantName,
		DocketNumber,
		HighLevelDisease,
		Matter,
		CaseCaption,
		[State],
		ClaimLawsuitStatus,
		'''' AS AuthorityStatus,
		DefenseCounselAddressID
FROM tblClaimLawsuit cl 
LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
LEFT JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimID
LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
LEFT JOIN tblDisease d ON cd.DiseaseID = d.DiseaseID
LEFT JOIN tblHighLevelDisease hld ON d.HighLevelDiseaseID = hld.HighLevelDiseaseID
LEFT JOIN tblState s ON l.StateID = s.StateID
INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
INNER JOIN tblClaimLawsuitStatusGroup clsg ON cls.ClaimLawsuitStatusID = clsg.ClaimLawsuitStatusID
WHERE	PrimaryDisease = 1 
		AND IsCurrentClaim = 1
		AND IsPrimaryForClaim = 1
		AND IsPrimaryStatus = 1
		AND clsg.StatusGroupID in (1, 3)
		' + @WhereString + '
ORDER BY ClaimantName'
	
PRINT @QueryString
		
EXEC sp_executesql @QueryString

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] TO [power_user]
GO
