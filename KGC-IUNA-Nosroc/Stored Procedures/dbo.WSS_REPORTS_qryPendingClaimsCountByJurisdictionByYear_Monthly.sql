SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_REPORTS_qryPendingClaimsCountByJurisdictionByYear_Monthly] (@StartDate datetime, @EndDate datetime, @MatterID int)
AS

Set NoCount ON

--DECLARE @StartDate datetime
--DECLARE @EndDate datetime
DECLARE @StateID int
--DECLARE @Matter int

SET @StartDate = '1/1/1900'
--SET @EndDate = '9/30/2010'
SET @StateID = null
--SET @Matter = null

-- Find Max ID Values

Declare @MaxStateID int

--Define Max State ID
Select @MaxStateID = MAX(stateid) from tblState

-- Find Max ID Values
Declare @MaxResponsibleDefenseCounselID int

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int, State nvarchar(150), YearFiled datetime, monthFiledID int, monthFiledName nvarchar(100), statusdate datetime, MesoCount int, LungCancerCount int, OtherCancerCount int, 
NonMaligCount int,  UnknownCount int, UnknownCount1 int)


Insert into @DiseaseCount

Select Matter, C.MatterID, FullNameState as state,  
case when serviceDate is null then  '1/1/1900' else convert(datetime,('1/1/'+convert(nvarchar,Year(serviceDate))))end as YearFiled, MONTH(COALESCE(serviceDate, '1/1/1900')), Datename(month, COALESCE(serviceDate, '1/1/1900')),
coalesce(settlementdate,statusdate,'1/1/1900'),
--Meso
Case	when DMeso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when DLC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when DOC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig & silicosis
Case	when DNM.HighLevelDiseaseID IN (4, 7) Then 1
Else 0 End as NonMaligCount,
--Unknown & no disease
Case	when DUnk.HighLevelDiseaseID IN (5, 6) Then 1
Else 0 End AS UnknownCount,
--NULLs
Case	when DUnk.HighLevelDiseaseID is NULL Then 1
Else 0 End AS UnknownCount1


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
LEFT JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID
--Meso
Left Join tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
Left Join tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
Left Join tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
Left Join tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Silicosis
Left Join tblDisease DSC on CD.DiseaseID=DSC.DiseaseID

--No Disease
Left Join tblDisease DND on CD.DiseaseID=DND.DiseaseID

--Unknown
Left Join tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID


Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblState S on L.StateID=S.StateID
left join tblClaimantSettlement cs on cs.ClaimLawsuitID = CL.ClaimLawsuitID
left join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblClaimLawsuitStatusgroup csg on csg.claimlawsuitstatusid = cst.ClaimLawsuitStatusID

Where  s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) and IsPrimaryForClaim = 1 and coalesce(cst.IsPrimaryStatus,0) = 1
	AND C.MatterID >= COALESCE(@MatterID,0) AND C.MatterID <= COALESCE(NULLIF(@MatterID,0),@maxMatterID)
	AND ((csg.statusgroupid = 1 and coalesce(servicedate, filedate, '1/1/1900')<=@endDate)  OR (statusgroupid IN (2,3) and ProcessedDate > @EndDate) )
	AND COALESCE(cpi.jobsiteinfo, 0) = 0


 Select Matter, MatterID, YEAR(@EndDate) AS CloseYear, State,  Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, Sum(OtherCancerCount) as OtherCancerCount ,@StartDate as StartDate, @EndDate as EndDate,
	Sum(NonMaligCount) as NonMaligCount, Sum(UnknownCount+UnknownCount1) as UnknownCount, Sum(UnknownCount+UnknownCount1+MesoCount+LungCancerCount+OtherCancerCount+NonMaligCount) AS Total
from @DiseaseCount
where yearfiled between COALESCE(@StartDate, '1/1/1900') and COALESCE(@EndDate, GETDATE())
Group By Matter, MatterID, State
ORDER BY Matter, State
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [power_user]
GO
