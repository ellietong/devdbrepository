SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryDefenseCounselAddressList]
AS 
    SET NOCOUNT ON

    SELECT  DefenseCounselAddressID ,
            DefenseCounsel + ' ('
            + CASE WHEN DefenseCounselCity IS NULL THEN 'UNKNOWN'
                   ELSE DefenseCounselCity
              END + ')' AS DefenseCounsel
    FROM    tblDefenseCounselAddress dca
    WHERE   DefenseCounsel IS NOT NULL
            AND DefenseCounsel <> ''
    UNION
    SELECT  NULL ,
            NULL
    ORDER BY DefenseCounsel + ' ('
            + CASE WHEN DefenseCounselCity IS NULL THEN 'UNKNOWN'
                   ELSE DefenseCounselCity
              END + ')' ,
            DefenseCounselAddressID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryDefenseCounselAddressList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryDefenseCounselAddressList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryDefenseCounselAddressList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryDefenseCounselAddressList] TO [power_user]
GO
