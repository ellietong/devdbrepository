SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptClaimantSummaryByDefenseCounsel] ( @ResponsibleDefenseCounselID int,
							@ClaimLawsuitStatusID int,
							@BeginDate datetime,
							@EndDate datetime, @Matter int)

AS
Set NoCount ON


 --Declare @ResponsibleDefenseCounselID int
 --Declare @ClaimLawsuitStatusID int
 --Declare @BeginDate datetime
 --Declare @EndDate datetime
 --Declare @Matter int
 --Set @ResponsibleDefenseCounselID = 0
 --Set @ClaimLawsuitStatusID = 22
 --Set @BeginDate = '1/1/1950'
 --Set @EndDate = '8/31/2008'
 --Set @Matter = 2


Declare @AllResponsibleDefenseCounsel table (DefenseCounselID int, 
				DefenseCounsel nvarchar(100), 
				ResponsibleDefenseCounselID int,
				StateID int, 
				ResponsibleDefenseCounsel nvarchar(100),
				IsActiveCounsel bit)
Insert Into @AllResponsibleDefenseCounsel
Select dcs.DefenseCOunselID,
	d.DefenseCounsel as DefenseCounsel,
	dcs.responsibledefensecounselID,
	StateID as StateID,
	r.DefenseCOunsel as ResponsibleDefenseCOunsel,
	IsActiveCounsel
From tblDefenseCounselState dcs Inner Join tblDefenseCounsel d on d.defensecounselID = dcs.defensecounselID
	Inner JOin tblDefenseCounsel r on r.defensecounselID = dcs.responsibledefensecounselID
Group By d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCOunselID, dcs.responsibledefensecounselID, IsActiveCounsel, StateID


-- Find Max ID Values
Declare @MaxResponsibleDefenseCounselID int
Declare @MaxClaimLawsuitStatusID int

--Define Max State ID
Select @MaxResponsibleDefenseCounselID = MAX(ResponsibleDefenseCounselID) From @AllResponsibleDefenseCounsel
If @ResponsibleDefenseCounselID = 0 Set @ResponsibleDefenseCounselID = Null

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


--Define Max Resolution Type ID
Select @MaxClaimLawsuitStatusID = MAX(ClaimLawsuitStatusID) From tblClaimLawsuitStatus
If @ClaimLawsuitStatusID = 0 Set @ClaimLawsuitStatusID = Null


Select 'National Service Industries' as ClientName, 
	s.StateID, State, 
	Jurisdiction, 
	ClaimSequence, 
	LastName, 
	FirstName,
	MiddleName, 
	'XXXX-XX-'+right(convert(nvarchar(9), SocialSecurityNumber), 4) AS SocialSecurityNumber, 
	coalesce(dhld.HighLevelDisease, hld.highleveldisease) AS HighLevelDisease,
	Matter, C.MatterID, 
	ClaimLawsuitStatus, 
	ServiceDate, 
	FileDate, 
	DocketNumber,
	CaseCaption, 
	LeadPlaintiffFirstName, 
	LeadPlaintiffLastName,
	PrimaryLocalPlaintiffCounsel,
	DefenseCounsel as PrimaryDefenseCounsel 
	

From tblClaimantPersonalInfo CPI
	Left Join tblClaim C on CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
	LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
	Left Join tblClaimLawsuit CL on C.ClaimID = CL.ClaimID
	Left Join tblLawsuit L on CL.LawsuitID = L.LawsuitID
	Left Join tblState S on S.StateID = L.StateID
	Left Join tblJurisdiction J on J.JurisdictionID = L.JurisdictionID
	Left Join tblClaimSequence cs on cs.ClaimSequenceID = C.ClaimSequenceID
	Left Join tblDisease D on c.PlaintiffDiseaseID = D.DiseaseID
	Left Join tblHighLevelDisease HLD on D.HighLevelDiseaseID = HLD.HighLevelDiseaseID
	Left Join tblDisease DD on c.DefenseDiseaseID= DD.DiseaseID
	Left Join tblHighLevelDisease DHLD on DD.HighLevelDiseaseID=DHLD.HighLevelDiseaseID
	Left Join tblClaimLawsuitStatus cls on CL.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	INNER Join @AllResponsibleDefenseCounsel r on r.DefenseCounselID = l.PrimaryDefenseCounselID and r.StateID = l.StateID

Where	
	IsPrimaryForClaim = 1 
	and IsActiveCounsel = 1
	AND ((CL.ClaimLawsuitStatusID >= COALESCE(@ClaimLawsuitStatusID, 0) AND CL.ClaimLawsuitStatusID <= COALESCE(@ClaimLawsuitStatusID, @MaxClaimLawsuitStatusID)))
	AND ((ResponsibleDefenseCounselID >= COALESCE(@ResponsibleDefenseCounselID, 0) AND ResponsibleDefenseCounselID <= COALESCE(@ResponsibleDefenseCounselID, @MaxResponsibleDefenseCounselID)))
	AND (FileDate >= @BeginDate OR FileDate IS NULL)
	AND (FileDate <= @EndDate OR FileDate IS NULL)
	AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
Order By Matter, J.Jurisdiction, LastName

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantSummaryByDefenseCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantSummaryByDefenseCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantSummaryByDefenseCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantSummaryByDefenseCounsel] TO [power_user]
GO
