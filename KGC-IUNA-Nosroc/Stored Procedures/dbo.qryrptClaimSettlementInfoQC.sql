SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimSettlementInfoQC] (@CLaimLawsuitID int)
as

--Declare @CLaimLawsuitID int
--Set @CLaimLawsuitID = 17075

--SELECT cs.ClaimantSettlementID, 
--      cs.ClaimantSettlementID as SettlementHeader,
--      ClaimLawsuitID, 
--      SettlementGroup, 
--      SettlementDate, 
--      SettlementAmount, 
--      PayToCompany, 
--      CheckNumber, 
--      PaidDate,
--      SettlementNotes,
--      PaymentTerms,
--      SettlementDueDate,
--      ReleaseType
--FROM tblCLaimantSettlement cs
--LEFT JOIN tblSettlementGroup sg on sg.SettlementGroupID = cs.SettlementGroupID
--LEFT JOIN tblPaymentTerms pt  on pt.PaymentTermsID = sg.PaymentTermsID
--LEFT JOIN tblClaimantRelease cr on cr.ClaimantSettlementID = cs.ClaimantSettlementID
--LEFT JOIN tblReleaseType rt         on rt.ReleaseTypeID = cr.ReleaseTypeID
--WHERE ClaimLawsuitID = @ClaimLawsuitID
--ORDER BY SettlementDate

SELECT cs.ClaimantSettlementID, 
      cs.ClaimantSettlementID as SettlementHeader,
      cs.ClaimLawsuitID, 
      SettlementGroup, 
      SettlementDate, 
      SettlementAmount, 
      PayToCompany, 
      CheckNumber, 
      PaidDate,
      SettlementNotes,
      PaymentTerms,
      SettlementDueDate,
      ReleaseType
FROM tblCLaimantSettlement cs
LEFT JOIN tblSettlementGroup sg on sg.SettlementGroupID = cs.SettlementGroupID
LEFT JOIN tblPaymentTerms pt  on pt.PaymentTermsID = sg.PaymentTermsID
LEFT JOIN tblSettlePaymentDocumentation spd ON spd.ClaimLawsuitID = cs.ClaimLawsuitID
LEFT JOIN tblReleaseType rt         on rt.ReleaseTypeID = spd.ReleaseTypeID
WHERE cs.ClaimLawsuitID = @ClaimLawsuitID
ORDER BY SettlementDate
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimSettlementInfoQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimSettlementInfoQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimSettlementInfoQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimSettlementInfoQC] TO [power_user]
GO
