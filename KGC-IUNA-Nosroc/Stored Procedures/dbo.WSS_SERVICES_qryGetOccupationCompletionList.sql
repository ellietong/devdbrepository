SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetOccupationCompletionList]
(@prefix nvarchar(MAX), @count int = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SET ROWCOUNT @count
	SELECT DISTINCT Occupation FROM tblClaimantEmployment WHERE Occupation LIKE @prefix + '%'
	SET ROWCOUNT 0
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [power_user]
GO
