SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qryfrmPaymentDefense] (@DefenseCounselAddressID int, @MatterID int)
AS
SET NOCOUNT ON
--M. Price added statusID to the where stmt to only pull back "approved" or "adjusted and approved" invoices. 12/9/11

select di.DefenseInvoiceID, DefenseCounselAddressID, InvoiceNumber, Period, InvoiceTotal, InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount, status
from tblDefenseInvoice di
left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca 
Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID) a on a.DefenseInvoiceID = di.DefenseInvoiceID
INNER JOIN tblDefenseInvoiceStatus dis on di.StatusID=dis.StatusID
where  di.StatusID in (1,2) and DefenseCounselAddressID = @DefenseCounselAddressID and MatterID = @MatterID
Group By di.DefenseInvoiceID, DefenseCounselAddressID, InvoiceNumber, Period, InvoiceTotal, status
Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [power_user]
GO
