SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryDoctorList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DoctorID, UPPER(DoctorLastName + ', ' + COALESCE(DoctorFirstName, '') + ' ' + COALESCE(DoctorMiddleName, '')) AS Doctor FROM tblDoctor
	UNION
	SELECT NULL AS DoctorID, '' AS Doctor FROM tblDoctor
	ORDER BY Doctor
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDoctorList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDoctorList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDoctorList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDoctorList] TO [power_user]
GO
