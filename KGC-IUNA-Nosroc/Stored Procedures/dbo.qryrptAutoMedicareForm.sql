SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptAutoMedicareForm]
(@ClaimantPersonalInfoID bigint, @ClaimLawsuitID bigint)
As
Set NoCount On

--DECLARE @Claimantpersonalinfoid bigint
--DECLARE @ClaimLawsuitID bigint
--set @Claimantpersonalinfoid = 50168
----set @Claimantpersonalinfoid = 21417 --two claims for one claimant
--Set @ClaimLawsuitID = 51987

--Find Single Related Party for Deceased Claimant
Declare @RelatedParty table (ClaimantPersonalInfoID int, RPFirstName nvarchar(255), RPMiddleName nvarchar(255), RPLastName nvarchar(255),
RPAddress1 nvarchar(255), RPAddress2 nvarchar(255), RPTaxIDNum nvarchar(255), 
RPCity nvarchar(255), RPPhone nvarchar(255), RPState nvarchar(255), RPZip nvarchar(255),
RPExtension nvarchar(255), RPLegalCapacity int, RPSSN nvarchar(255))
Insert Into @RelatedParty 
SELECT Distinct cpi.ClaimantPersonalInfoID, crp.FirstName, crp.MiddleName, crp.LastName, crp.RPAddress1, crp.RPAddress2,
crp.RPTIN, crp.RPCity, crp.RPPhone, rpstate.FullNameState, crp.RPZip, crp.RPExtension, lc.LegalCapacityID, crp.RPSocialSecurityNumber

FROM tblClaimantPersonalInfo cpi
inner join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
left join tblClaimDisease cd on cd.ClaimID = c.ClaimID
left join tblDisease d on d.DiseaseID = cd.DiseaseID
left join tblHighLevelDisease dhld on dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
left join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblDefendantCompany dc on dc.DefendantCompanyID = a.DefendantCompanyID
left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
left join tblState laws on laws.StateID = l.StateID
left join tblClaimantRelatedParty crp on crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT join tblState rpstate ON rpstate.StateID = crp.StateID
left join tblLegalCapacity lc on lc.LegalCapacityID = crp.LegalCapacityID
left join tblRelationship r on r.RelationshipID = crp.RelationshipID
left join tblClaimantEmployment ce on ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblClaimantProduct cp on cp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblPlaintiffCounselAddress pc on pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
left join tblCourtType ct on ct.CourtTypeID = l.CourtTypeID
left join tblJurisdiction j on j.JurisdictionID = l.jurisdictionID 
left join tblSettlePaymentDocumentation spd on cl.ClaimLawsuitID = spd.ClaimLawsuitID
Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
and cl.ClaimLawsuitID = @ClaimLawsuitID 
and lc.LegalCapacityID in (1,8,3,5,6,7,9,15,16,17,22)

--Select Information about Claimant for Medicare Form
SELECT Distinct 
cpi.MedicareBeneficiaryStatusID,
cpi.FirstName, 
Coalesce(cpi.MiddleName,'')as MiddleName, 
cpi.LastName, 
Coalesce(Suffix,'') as Suffix,
laws.State,
LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4)as SSN, 
Coalesce(cpi.HealthInsuranceClaimNumber,'') as HealthInsuranceClaimNumber,
CONVERT(nvarchar,MONTH(cpi.BirthDate))+'/'+CONVERT(nvarchar,DAY(cpi.BirthDate))+'/'+CONVERT(nvarchar,YEAR(cpi.BirthDate)) as BirthDate,
cpi.GenderID,
CASE WHEN cpi.DeceasedDate IS NULL THEN 0 ELSE 1 END AS DeceasedID,
CONVERT(nvarchar,MONTH(dbo.fnEarliestExposure(@ClaimantPersonalInfoID)))+'/'+CONVERT(nvarchar,DAY(dbo.fnEarliestExposure(@ClaimantPersonalInfoID)))+'/'+CONVERT(nvarchar,YEAR(dbo.fnEarliestExposure(@ClaimantPersonalInfoID))) as FirstExposure,
CONVERT(nvarchar,MONTH(dbo.fnLatestExposure(@ClaimantPersonalInfoID)))+'/'+CONVERT(nvarchar,DAY(dbo.fnLatestExposure(@ClaimantPersonalInfoID)))+'/'+CONVERT(nvarchar,YEAR(dbo.fnLatestExposure(@ClaimantPersonalInfoID))) as LastExposure,
CONVERT(nvarchar,MONTH(cs.SettlementDate))+'/'+CONVERT(nvarchar,DAY(cs.SettlementDate))+'/'+CONVERT(nvarchar,YEAR(cs.SettlementDate)) as SettlementDate,
CONVERT(nvarchar,MONTH(cs.SettlementDueDate))+'/'+CONVERT(nvarchar,DAY(cs.SettlementDueDate))+'/'+CONVERT(nvarchar,YEAR(cs.SettlementDueDate)) as SettlementDueDate,
'$' + LTRIM(CONVERT(CHAR(17),CAST(cs.SettlementAmount as money),1)) AS SettlementAmount,
dcomp.DefendantCompany as Matter,
m.Matter as MatterFolder,
d.Disease+' - '+d.ICD9 As Disease,
COALESCE(mat.Material,'') AS Material,
pc.PlaintiffCounsel,
pc.FullNameState AS PlaintiffCounselState,
pc.PlaintiffCounselAddress1,
Coalesce(pc.PlaintiffCounselAddress2,'') As PlaintiffCounselAddress2,
pc.PlaintiffCounselCity,
Case when claimsaddress.PlaintiffCounselMailZip is null then CONVERT(nvarchar,pc.PlaintiffCounselZip) else
CONVERT(nvarchar,pc.PlaintiffCounselZip) + '-' + CONVERT(nvarchar,claimsaddress.PlaintiffCounselMailZip) end as PlaintiffCounselZip,
Coalesce(CONVERT(nvarchar,pc.PlaintiffCounselExtension), '') As PlaintiffCounselExtension,
'(' + LEFT(CONVERT(nvarchar,pc.PlaintiffCounselPhone), 3) + ') ' + SUBSTRING(CONVERT(nvarchar,pc.PlaintiffCounselPhone), 4, 3) + '-' + RIGHT(CONVERT(nvarchar,pc.PlaintiffCounselPhone), 4)as PlaintiffCounselPhone,
Coalesce(pc.PlaintiffCounselTaxIDNum,'') as PlaintiffCounselTaxIDNum,
rp.RPFirstName, 
Coalesce(rp.RPMiddleName,'') as RPMiddleName, 
rp.RPLastName,
Coalesce(rp.RPAddress1,'') as RPAddress1,
Coalesce(rp.RPAddress2,'') as RPAddress2, 
Coalesce(rp.RPTaxIDNum,'') as RPTaxIDNum, 
Coalesce(rp.RPCity,'') as RPCity,
Coalesce('(' + LEFT(CONVERT(nvarchar,rp.RPPhone), 3) + ') ' + SUBSTRING(CONVERT(nvarchar,rp.RPPhone), 4, 3) + '-' + RIGHT(CONVERT(nvarchar,rp.RPPhone), 4),'') as RPPhone, 
Coalesce(rp.RPState,'') as RPState,
Coalesce(rp.RPZip,'') as RPZip,
Coalesce(rp.RPExtension,'') as RPExtension,
DocketNumber,
rp.RPLegalCapacity,
Coalesce(LEFT(rp.RPSSN, 3) + '-' + SUBSTRING(rp.RPSSN, 4, 2) + '-' + RIGHT(rp.RPSSN, 4),'') as RPSSN,
CONVERT(nvarchar,sg.SettlementGroupID) AS SettlementGroupID,
SettlementGroup


FROM tblClaimantPersonalInfo cpi
inner join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
left join tblClaimDisease cd on cd.ClaimID = c.ClaimID
left join tblDisease d on d.DiseaseID = cd.DiseaseID
left join tblHighLevelDisease dhld on dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
left join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT join tblMaterial mat ON mat.MaterialID = a.MaterialID
left join tblDefendantCompany dc on dc.DefendantCompanyID = a.DefendantCompanyID
left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
LEFT join tblMatter m ON m.MatterID = c.MatterID
left join tblState laws on laws.StateID = l.StateID
left join tblClaimantRelatedParty crp on crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblLegalCapacity lc on lc.LegalCapacityID = crp.LegalCapacityID
left join tblRelationship r on r.RelationshipID = crp.RelationshipID
left join tblClaimantEmployment ce on ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblClaimantProduct cp on cp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblPlaintiffCounselAddress pc on pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
left join tblCourtType ct on ct.CourtTypeID = l.CourtTypeID
left join tblJurisdiction j on j.JurisdictionID = l.jurisdictionID 
left join tblSettlePaymentDocumentation spd on cl.ClaimLawsuitID = spd.ClaimLawsuitID
left join tblSettlementGroup sg on sg.SettlementGroupID = cs.SettlementGroupID
left join tblClaimantSuffix csuf on csuf.SuffixID = cpi.SuffixID
left join @RelatedParty rp ON rp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join [KGA-ClaimsAdmin].[dbo].[tblPlaintiffCounselAddress] claimsaddress ON pc.PlaintiffCounselAddressID = claimsaddress.PlaintiffCounselAddressID
left join tblDefendantCompany dcomp ON dcomp.DefendantCompanyID = a.DefendantCompanyID

Where
cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
and cl.ClaimLawsuitID = @ClaimLawsuitID
and PrimaryDisease = 1


RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoMedicareForm] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAutoMedicareForm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoMedicareForm] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptAutoMedicareForm] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoMedicareForm] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAutoMedicareForm] TO [power_user]
GO
