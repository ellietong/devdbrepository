SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[qryfrmCheckSettlementGroup] (@PlaintiffCounselAddressID int)
As
Set NoCount On


Select sg.SettlementGroupID, sg.SettlementGroup
From tblSettlementGroup sg
Inner Join tblclaimantsettlement cs on sg.settlementgroupID = cs.SettlementGroupID
Inner JOin tblclaimlawsuit cl on cl.claimlawsuitid=cs.claimlawsuitid
Inner Join tbllawsuit l on l.LawsuitID = cl.LawsuitID
Where PlaintiffCounselAddressID = @PlaintiffCounselAddressID
Group By Sg.SettlementGroupID, sg.SettlementGroup
Order By SettlementGroup

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckSettlementGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckSettlementGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckSettlementGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckSettlementGroup] TO [power_user]
GO
