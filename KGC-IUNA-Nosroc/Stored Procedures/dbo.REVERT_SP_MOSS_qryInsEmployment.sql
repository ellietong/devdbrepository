SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryInsEmployment]
(@ClaimantPersonalInfoID bigint, @Jobsite nvarchar(250), @City nvarchar(100), @StateID int, @Occupation nvarchar(250), @StartDate datetime, @EndDate datetime, @Emailaddress nvarchar(100))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblClaimantEmployment (ClaimantPersonalInfoID, Jobsite, Occupation, JobsiteCity, JobsiteStateID, EmpStartDate, EmpEndDate, SPUsername)
	VALUES (@ClaimantPersonalInfoID, UPPER(@Jobsite), UPPER(@Occupation), UPPER(@City), @StateID, @StartDate, @EndDate, @Emailaddress)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryInsEmployment] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryInsEmployment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryInsEmployment] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryInsEmployment] TO [power_user]
GO
