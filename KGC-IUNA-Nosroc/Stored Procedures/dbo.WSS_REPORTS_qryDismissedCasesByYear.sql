SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_REPORTS_qryDismissedCasesByYear]
(@MatterID int,@BeginDate datetime, @EndDate datetime)

As
Set NoCount On


--DECLARE @matterID int = 1
--DECLARE @BeginDate datetime = '1/1/1900'
--DECLARE @EndDate datetime = '1/1/2012'

DECLARE @tblYears TABLE ([Year] int)
DECLARE @StartYear int = YEAR(@BeginDate)
DECLARE @EndYear int = YEAR(@EndDate)

WHILE (@StartYear < @EndYear)
BEGIN
   INSERT INTO @tblYears
   SELECT @StartYear
   
   SET @StartYear = @StartYear + 1

END


--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int, claimlawsuitstatus nvarchar (500),
	MoYearDismissed datetime, YearDismissed datetime,MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int, unknowncount1 int)
Insert into @DiseaseCount

Select Matter, C.MatterID,  css.claimlawsuitstatus,
 convert(datetime,(convert(nvarchar,Month(cst.statusDate))+'/1/'+ convert(nvarchar,Year(cst.StatusDate)))) as MoYearDismissed,
  convert(nvarchar,Year(cst.StatusDate)) as YearDismissed,
--Meso
Case	when DMeso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when DLC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when DOC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when DNM.HighLevelDiseaseID in (4,7) Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when DUnk.HighLevelDiseaseID in (5,6) Then 1
Else 0 End AS UnknownCount,
Case	when DUnk.HighLevelDiseaseID is NULL Then 1
Else 0 End AS UnknownCount1


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (Select CD.ClaimDiseaseID,CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
LEFT JOIN tblClaimSequence csq ON c.ClaimSequenceID = csq.ClaimSequenceID
--Meso
Left Join tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
Left Join tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
Left Join tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
Left Join tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Unknown
Left Join tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID


Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
left join tblClaimantSettlement cs on cs.ClaimLawsuitID = CL.ClaimLawsuitID
left join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblClaimLawsuitStatus css on css.ClaimLawsuitStatusID = cst.ClaimLawsuitStatusID
left join tblclaimlawsuitstatusgroup csg on csg.claimlawsuitstatusid = cst.ClaimLawsuitStatusID

Where  COALESCE(isprimaryforclaim, 0) = 1   
AND COALESCE(IsPrimaryStatus, 0) = 1
and csg.statusgroupid = 3
AND C.MatterID >= COALESCE(@MatterID,0) AND C.MatterID <= COALESCE(NULLIF(@MatterID,0),@maxMatterID) and ProcessedDate between COALESCE(@BeginDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) and COALESCE(@EndDate, GETDATE())

AND COALESCE(cpi.jobsiteinfo, 0) = 0
 
	 Order By  Year(cst.StatusDate)


Select Matter, MatterID,  Sum(COALESCE(MesoCount, 0)) as MesoCount,
	Sum(COALESCE(LungCancerCount, 0)) as LungCancerCount, Sum(COALESCE(OtherCancerCount, 0)) as OtherCancerCount,
	Sum(COALESCE(NonMaligCount, 0)) as NonMaligCount, Sum(COALESCE(UnknownCount, 0)+COALESCE(unknowncount1, 0)) as UnknownCount,
	[Year] AS MonthYear,
	-- @BeginDate as StartDate, @EndDate as EndDate,
--MoYearDismissed, 		YearDismissed, DATENAME(month, MoYearDismissed) AS Month, DATENAME(year, MoYearDismissed) As Year,
SUM(COALESCE(MesoCount, 0) + COALESCE(LungCancerCount, 0) + COALESCE(OtherCancerCount, 0) + COALESCE(NonMaligCount, 0) + COALESCE(UnknownCount, 0) + COALESCE(unknowncount1, 0)) AS MonthTotal-- , DATENAME(month, MoYearDismissed) + ' ' + DATENAME(year, MoYearDismissed) AS MonthYear
from @tblYears y
LEFT JOIN @DiseaseCount dc ON Year(MoYearDismissed) = y.Year  
Group By Matter, MatterID, Year



Return
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissedCasesByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissedCasesByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissedCasesByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissedCasesByYear] TO [power_user]
GO
