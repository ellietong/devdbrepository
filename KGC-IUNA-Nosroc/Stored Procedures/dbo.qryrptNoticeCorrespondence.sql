SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptNoticeCorrespondence]

AS
SET NOCOUNT ON

SELECT  cor.CorrespondenceDate, 
	ct.CorrespondenceType, 
	cm.CorrespondenceMode, 
	cor.Notes, 
	cor.Attachments, 
	cor.confirmreceipt,
	cor.CorrespondenceID, 
	c.CarrierName, 
	p.PolicyNum, 
	p.policyID, 
	min(pd.PStartDate) AS StartDate, 
	max(pd.PEndDate) AS EndDate
            
FROM     tblCorrespondence cor LEFT OUTER JOIN
         tblCorrespondenceMode cm ON cor.CorrespondenceModeID = cm.CorrespondenceModeID LEFT OUTER JOIN
         tblCorrespondenceType ct ON cor.CorrespondenceTypeID = ct.CorrespondenceTypeID LEFT JOIN
	 tblPolicyNoticed pn ON cor.CorrespondenceID = pn.CorrespondenceID LEFT JOIN
         tblPolicy p ON pn.PolicyID = p.PolicyID LEFT JOIN
         tblCarrier c ON p.CarrierID = c.CarrierID LEFT JOIN
	 tblPolicyDates pd ON p.PolicyID = pd.PolicyID
GROUP BY cor.CorrespondenceID, c.CarrierName, p.PolicyNum, p.policyid, cor.CorrespondenceDate, 
	ct.CorrespondenceType, cm.CorrespondenceMode, cor.Notes, cor.Attachments, cor.confirmreceipt
ORDER BY cor.CorrespondenceID,  c.CarrierName, p.PolicyNum,  StartDate, p.policyid, cor.CorrespondenceDate 
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNoticeCorrespondence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNoticeCorrespondence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNoticeCorrespondence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNoticeCorrespondence] TO [power_user]
GO
