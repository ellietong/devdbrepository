SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAccountsReceivableSummary] (@BillDate datetime, @Matter int)

AS
Set NoCount On

--DECLARE @Matter int
--SET @Matter = NULL
--DECLARE @BillDate datetime
--SET @BillDate = '3/23/2010'

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @today datetime
Set @today = CONVERT(datetime, CONVERT(varchar, GETDATE(), 101), 101)

--Ledger Table
Declare @Ledger Table (LedgerID int, BillGroupID int, CarrierID int, SubscriberID int, Allocation money, ExhaustionDateID int, BillDate datetime,
                PaymentDueDate datetime, MatterID int)
Insert into @Ledger
Select LedgerID, BillGroupID, CarrierID, SubscriberID, Allocation, ExhaustionDateID, BillDate, PaymentDueDate, MatterID
From tblLedger
Where BillDate <= @BillDate

--Payment Table
Declare @Payment Table (PaymentID int, LedgerID int, Payment money, PaymentDate Datetime)
Insert Into @Payment
Select PaymentID, LedgerID, Payment, PaymentDate
From tblPayment
Where PaymentDate <= @BillDate

--Most Carriers
Declare @AccountsReceivable Table (orderID int, InsurerGroup nvarchar(250), Insurer nvarchar(250), BilledAmount money, PaidAmount money, AccountReceivable money,
MatterID int)
Insert into @AccountsReceivable
Select '1' as OrderID, COALESCE(CASE WHEN L.CarrierID = 152 THEN 'Nationwide Group' ELSE CarrierGroupName END, CarrierName) AS InsurerGroup, 
CASE WHEN L.CarrierID = 152 THEN 'Employers Insurance of Wausau, A Mutual Company' ELSE CarrierName END AS Insurer, 
Sum(coalesce(Allocation,0)) as BilledAmount, Sum(Coalesce(Payment,0)) as PaidAmount, Sum(coalesce(Allocation,0))-Sum(Coalesce(Payment,0)) as AccountReceivable,
MatterID
From @Ledger L
Inner Join tblCarrier C On L.CarrierID=C.CarrierID
Left Join tblCarrierGroup CG on C.CarrierGroupID=CG.CarrierGroupID
Left Join @Payment P on L.LedgerID=P.LedgerID
Where c.CarrierID NOT IN (10,20001,20002,20003)--20
Group By COALESCE(CASE WHEN L.CarrierID = 152 THEN 'Nationwide Group' ELSE CarrierGroupName END, CarrierName), 
CASE WHEN L.CarrierID = 152 THEN 'Employers Insurance of Wausau, A Mutual Company' ELSE CarrierName END, MatterID
Having Sum(coalesce(Allocation,0))-Sum(Coalesce(Payment,0)) <> 0


--UNION

----London Solvent
--Select 2 as OrderID, 'London Solvent' as InsurerGroup, SubscriptionCompany as Insurer,
--                Sum(coalesce(Allocation,0)) as BilledAmount, Sum(Coalesce(Payment,0)) as PaidAmount,
--                Sum(coalesce(Allocation,0))-Sum(Coalesce(Payment,0)) as AccountReceivable, MatterID
--From @Ledger L
--Inner Join tblCarrier C On L.CarrierID=C.CarrierID
--Left Join tblSubscriberCompany sc on sc.SubscriberID=l.SubscriberID
--Left Join @Payment P on L.LedgerID=P.LedgerID
--Where c.carrierID = 20 AND SubscriptionCompanySolvency LIKE 'Solvent'
--Group By SubscriptionCompany, sc.subscriberID, MatterID
--Having Sum(coalesce(Allocation,0))-Sum(Coalesce(Payment,0)) <> 0

--UNION

----London Solvent
--Select 2 as OrderID, 'London Insolvent' as InsurerGroup, SubscriptionCompany as Insurer,
--                Sum(coalesce(Allocation,0)) as BilledAmount, Sum(Coalesce(Payment,0)) as PaidAmount,
--                Sum(coalesce(Allocation,0))-Sum(Coalesce(Payment,0)) as AccountReceivable, MatterID
--From @Ledger L
--Inner Join tblCarrier C On L.CarrierID=C.CarrierID
--Left Join tblSubscriberCompany sc on sc.SubscriberID=l.SubscriberID
--Left Join @Payment P on L.LedgerID=P.LedgerID
--Where c.carrierID = 20 AND SubscriptionCompanySolvency LIKE 'Insolvent'
--Group By SubscriptionCompany, sc.subscriberID, MatterID
--Having Sum(coalesce(Allocation,0))-Sum(Coalesce(Payment,0)) <> 0

UNION

--IUNA Producer
Select '2' as OrderID, CASE WHEN L.CarrierID = 20002 THEN 'Self Insured' WHEN L.CarrierID = 20003 THEN 'Self Insured' ELSE 'ACF Refund' END AS InsurerGroup, 
CarrierName AS Insurer, 
Sum(coalesce(Allocation,0)) as BilledAmount, Sum(Coalesce(Payment,0)) as PaidAmount, Sum(coalesce(Allocation,0))-Sum(Coalesce(Payment,0)) as AccountReceivable,
MatterID
From @Ledger L
Inner Join tblCarrier C On L.CarrierID=C.CarrierID
Left Join tblCarrierGroup CG on C.CarrierGroupID=CG.CarrierGroupID
Left Join @Payment P on L.LedgerID=P.LedgerID
WHERE L.CarrierID IN (20002,20003) AND L.CarrierID NOT IN (10,20)
Group By CASE WHEN L.CarrierID = 20002 THEN 'Self Insured' WHEN L.CarrierID = 20003 THEN 'Self Insured' ELSE 'ACF Refund' END, CarrierName, MatterID
Having Sum(coalesce(Allocation,0))-Sum(Coalesce(Payment,0)) <> 0

Select @today AS Today, OrderID, InsurerGroup, Insurer, Sum(BilledAmount) as BilledAmount, Sum(PaidAmount) as PaidAmount, Sum(AccountReceivable) as AccountReceivable,
CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
From @AccountsReceivable ar
Left Join tblMatter m on ar.MatterID = m.MatterID
Where ar.MatterID >= COALESCE(@Matter,0) AND ar.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
Group By OrderID, InsurerGroup, Insurer
Order BY Insurer

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAccountsReceivableSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAccountsReceivableSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAccountsReceivableSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAccountsReceivableSummary] TO [power_user]
GO
