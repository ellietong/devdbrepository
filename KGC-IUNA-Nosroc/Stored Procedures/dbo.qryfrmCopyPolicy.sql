SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[qryfrmCopyPolicy]
(@policyID int)
AS

SET NOCOUNT ON

--DECLARE @PolicyID int
--SET @policyID = 1

DECLARE @newPolicyID int

--Copy Policy Record 
INSERT INTO tblPolicy (CarrierID, DoNotReport, InitialDataEntry, InitialDataEntryDate, InsuranceProgramID, MinPStartDate, NamedInsuredID, PolicyNotes, PolicyNum, PolicyPDF, PolicyReview, PolicyReviewDate, PolicyReviewed, PolPicPDFLink)
SELECT CarrierID, DoNotReport, InitialDataEntry, InitialDataEntryDate, InsuranceProgramID, MinPStartDate, NamedInsuredID, PolicyNotes, PolicyNum, PolicyPDF, PolicyReview, PolicyReviewDate, PolicyReviewed, PolPicPDFLink
FROM tblpolicy
WHERE PolicyID = @policyID

--Determine new policy id
SET @newPolicyID = (SELECT MAX(PolicyID) FROM tblPolicy)

--Copy Policy Dates
INSERT INTO tblPolicyDates (ActualPStartDate, ActualPEndDate, AnnualPeriod, PEndDate, PolicyID, PStartDate)
SELECT ActualPStartDate, ActualPEndDate, AnnualPeriod, PEndDate, @newPolicyID, PStartDate
FROM tblPolicyDates
WHERE PolicyID = @policyID

--Copy Broker Policy 
INSERT INTO tblBrokerPolicy (BrokerID, BrokerRefID, BrokerTypeID, PolicyBrokerNotes, PolicyID)
SELECT BrokerID, BrokerRefID, BrokerTypeID, PolicyBrokerNotes, @newPolicyID
FROM tblBrokerPolicy
WHERE PolicyID = @policyID

--Copy Policy Limits
INSERT INTO tblPolicyLimits (AggregateLimit, AttachmentPoint, Layer, LayerPerOccLimit, LimitNote, LimitTypeID, PerOccLimit, PolicyID, PreviouslyExhaustedAggregate, SIR, SIRAggregateLimit)
SELECT AggregateLimit, AttachmentPoint, Layer, LayerPerOccLimit, LimitNote, LimitTypeID, PerOccLimit, @newPolicyID, PreviouslyExhaustedAggregate, SIR, SIRAggregateLimit
FROM tblPolicyLimits
WHERE PolicyID = @policyID

--Copy Policy Additional Named Insured
INSERT INTO tblAdditionalNamedInsured (AdditionalNamedInsTypeID, Bates, EffectiveDate, NamedInsuredID, PolicyID)
SELECT AdditionalNamedInsTypeID, Bates, EffectiveDate, NamedInsuredID, @newPolicyID
FROM tblAdditionalNamedInsured
WHERE PolicyID = @policyID

--Copy Policy Provisions
INSERT INTO tblProvision (Bates, LanguageID, PolicyID, ProvisionLabelID, ProvisionMemo, ProvisionNotes, ProvisionValueID)
SELECT Bates, LanguageID, @newPolicyID, ProvisionLabelID, ProvisionMemo, ProvisionNotes, ProvisionValueID
FROM tblProvision
WHERE PolicyID = @policyID

--Copy Policy Provision Language
INSERT INTO tblProvisionLanguage (BatesEnd, BatesStart, DoNotShow, EffectiveDate, EndorsementNum, ExactLanguage, FormNumber, LangTypeID, LanguageID, LocationID, PolicyID, ProvisionLabelID, ProvisionNotes, Sort)
SELECT BatesEnd, BatesStart, DoNotShow, EffectiveDate, EndorsementNum, ExactLanguage, FormNumber, LangTypeID, LanguageID, LocationID, @newPolicyID, ProvisionLabelID, ProvisionNotes, Sort
FROM tblProvisionLanguage
WHERE PolicyID = @policyID

--Copy Policy Subscription Coverage
INSERT INTO tblSubscriptionCoverage (AcctNum, AgencyID, BrokerNotes, DomesticBroker, DomesticBrokerReference, LondonBroker, LondonBrokerReference, Percentage, PolicyID, PolicyTypeID, SubscriberID, SubscriptionNotes, UWReference)
SELECT AcctNum, AgencyID, BrokerNotes, DomesticBroker, DomesticBrokerReference, LondonBroker, LondonBrokerReference, Percentage, @newPolicyID, PolicyTypeID, SubscriberID, SubscriptionNotes, UWReference
FROM tblSubscriptionCoverage
WHERE PolicyID = @policyID

--Copy Policy Issues
INSERT INTO tblIssues (IssueDate, IssueDescription, IssueTypeID, MngrApproval, MngrReviewDate, MngrReviewNeeded, PolicyID, Resolution, Resolved, ResolvedBy)
SELECT IssueDate, IssueDescription, IssueTypeID, MngrApproval, MngrReviewDate, MngrReviewNeeded, @newPolicyID, Resolution, Resolved, ResolvedBy
FROM tblIssues
WHERE PolicyID = @policyID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyPolicy] TO [power_user]
GO
