SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[WSS_UPLOAD_qryDocumentType] (@invoiceFlag bit=0, @settlementFlag bit=0)

AS
SET NOCOUNT ON
--DECLARE @invoiceFlag bit
--SET @invoiceFlag = 0
--DECLARE @settlementFlag bit
--SET @settlementFlag = 0
IF @invoiceFlag = 0
BEGIN
	if @settlementFlag = 1
	BEGIN
		Select 
		ClaimantDocumentTypeID, 
		ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID IN (1, 3, 4)
		AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <> 16
		ORDER BY ClaimantDocumentType
	END
	ELSE
	BEGIN
		Select 
			ClaimantDocumentTypeID, 
			ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID IN (1, 3)
		AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <> 16
		ORDER BY ClaimantDocumentType
	END
END
ELSE
BEGIN
		Select 
			ClaimantDocumentTypeID, 
			ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID = 2 AND ClaimantDocumentTypeID <> 41
		ORDER BY ClaimantDocumentType
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [power_user]
GO
