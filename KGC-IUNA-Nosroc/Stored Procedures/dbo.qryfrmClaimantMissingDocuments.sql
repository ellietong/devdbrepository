SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmClaimantMissingDocuments] (@ClaimantPersonalInfoID int)
as

Set NoCount On


--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 1


DECLARE @ClaimantDocs TABLE(ClaimantIDed int, DxMedicals int, DODCert int, EstatePapers int, ProductID int, EmployExpoHist int, settlementconfirmation int)
DECLARE @MissingDocuments TABLE(MissingDocName nvarchar(100))

--Determine which documents a claimant needs in order to be fully documented
INSERT INTO @ClaimantDocs
	SELECT sd.ClaimantIDed, sd.DxMedicals, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE sd.DODCert END AS DODCert, --If Claimant is dead, they do not need DODCert
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE sd.EstatePapers END AS EstatePapers, --If Claimant is dead, they do not need Estate Papers
		sd.ProductID, sd.EmployExpoHist, sd.settlementconfirmation
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID		
		INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		inner join tblSettlementDocumentation sd on sd.SetPymtID = spd.SetPymtID
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
INSERT INTO @MissingDocuments
	SELECT 'Fully Identified - SSN, DOB, DOD' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ClaimantIDed IS NULL Or ClaimantIDed = 0
INSERT INTO @MissingDocuments
	SELECT 'Diagnosing Medicals' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DxMedicals IS NULL Or DxMedicals = 0
INSERT INTO @MissingDocuments
	SELECT 'Death Certificate' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DODCert IS NULL Or DODCert = 0
INSERT INTO @MissingDocuments
	SELECT 'Estate Paperwork' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EstatePapers IS NULL Or EstatePapers = 0
INSERT INTO @MissingDocuments
	SELECT 'Product ID' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ProductID IS NULL Or ProductID = 0
INSERT INTO @MissingDocuments
	SELECT 'Employment/Exposure History' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EmployExpoHist IS NULL Or EmployExpoHist = 0
	INSERT INTO @MissingDocuments
	SELECT 'Settlement Confirmation Letter' AS MissingDocName
	FROM @ClaimantDocs
	WHERE settlementconfirmation IS NULL Or settlementconfirmation = 0

SELECT MissingDocName FROM @MissingDocuments

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantMissingDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantMissingDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantMissingDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantMissingDocuments] TO [power_user]
GO
