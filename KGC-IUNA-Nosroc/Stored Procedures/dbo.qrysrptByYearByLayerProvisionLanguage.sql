SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qrysrptByYearByLayerProvisionLanguage]
(@policyid int,@provisionlabelid int)
AS
SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

DECLARE @MaxPolicyID INT
SELECT @MaxPolicyID = Max(PolicyID) from tblPolicy

--DECLARE @PolicyID int
--DECLARE @ProvisionLabelID int
--SET @PolicyID = 64
--SET @provisionlabelid = 25

----------------------------------------------------------------------------
DECLARE @SRPTTable TABLE(PolicyID INT, ProvisionLabelID INT, Provision varchar(75), Provisionvalue varchar (300), LanguageID INT, 
	BatesStart varchar(50), BatesEnd varchar(50), ExactLanguage varchar(8000), ProvisionNotes varchar(8000), Location varchar(50), 
	langtype varchar (100),EndorsementNum varchar(50),  m_Sort INT, LocationID INT)

	INSERT INTO @SRPTTable
	SELECT PRVL.PolicyID, 
	PL.ProvisionLabelID, 
	PL.Provision, 
	PVV.Provisionvalue,
	PRVL.LanguageID, 
	PRVL.BatesStart, 
	PRVL.BatesEnd, 
	PRVL.ExactLanguage, 
	PRVL.ProvisionNotes, 
	LOC.Location, 
	LangType,
	PRVL.EndorsementNum, 
	PRVL.Sort, 
	LOC.LocationID
	FROM  dbo.tblProvisionLanguage PRVL 
LEFT JOIN dbo.tblProvisionLabel PL ON PRVL.ProvisionLabelID = PL.ProvisionLabelID
INNER JOIN tblprovision psn on psn.policyid = prvl.policyid 
INNER JOIN tblprovisionvalue pvv on pvv.provisionlabelid = pl.provisionlabelid and pvv.provisionvalueid = psn.provisionvalueid
LEFT JOIN dbo.tblLocation LOC ON PRVL.LocationID = LOC.LocationID
LEFT join tbllangtype mlt on mlt.langtypeid = prvl.langtypeid
		WHERE PRVL.policyid >= COALESCE(@policyid, 0) AND 
PRVL.policyid <= COALESCE(NULLIF(@policyid, 0), @MaxPolicyID)
		AND pl.ProvisionLabelID = @provisionlabelid
	and isnull(donotshow, 0)<>1
--the above line was not working because donotshow was null for most records, which cannot be compared
--to an int. Now NULLs default to 0
	
SELECT PolicyID, m_sort, ProvisionNotes,location,batesstart,batesend, langtype,endorsementnum,Provisionlabelid,provisionvalue,
CASE 

	WHEN  ExactLanguage <> 'No Policy Language Found.' 
		AND ExactLanguage <> 'No Relevant Policy Language Found.' 
		AND ExactLanguage <> 'Policy Language Not Analyzed.'
		THEN ExactLanguage --+ ' [' + location + ', effective ' + expr1 + ', at ABC-' + right(BatesStart,6) + ']' + '.'
	WHEN ExactLanguage = 'No Policy Language Found.' or ExactLanguage = 'No Relevant Policy Language Found.'
		or ExactLanguage = 'Policy Language Not Analyzed.'
		THEN ExactLanguage 
	WHEN rtrim(ltrim(ExactLanguage)) is null THEN 'Policy Language Not Analyzed.'

END AS Language
From @SRPTTable
where ProvisionLabelID is not null

Group by PolicyID, m_sort, ProvisionNotes,location,batesstart,batesend,langtype,endorsementnum, provisionlabelid,provisionvalue,
CASE 

	WHEN  ExactLanguage <> 'No Policy Language Found.' 
		AND ExactLanguage <> 'No Relevant Policy Language Found.' 
		AND ExactLanguage <> 'Policy Language Not Analyzed.'
		THEN ExactLanguage-- + ' [' + location + ', effective ' + expr1 + ', at ABC-' + right(BatesStart,6) + ']' + '.'
	WHEN ExactLanguage = 'No Policy Language Found.' or ExactLanguage = 'No Relevant Policy Language Found.'
		or ExactLanguage = 'Policy Language Not Analyzed.'
		THEN ExactLanguage 
	WHEN rtrim(ltrim(ExactLanguage)) is null THEN 'Policy Language Not Analyzed.'
--'No Policy Language Found.'
END



--grant execute on dbo.qrysrptByYearByLayerProvisionLanguage to power_user, base_user
SET CONCAT_NULL_YIELDS_NULL ON
RETURN

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptByYearByLayerProvisionLanguage] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptByYearByLayerProvisionLanguage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptByYearByLayerProvisionLanguage] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptByYearByLayerProvisionLanguage] TO [power_user]
GO
