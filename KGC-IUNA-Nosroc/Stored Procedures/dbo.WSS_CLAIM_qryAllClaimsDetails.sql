SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryAllClaimsDetails]
@ClaimID bigint=null
AS
BEGIN
----DECLARE @ClaimID int
----SET @ClaimID = 5130

SELECT DISTINCT
	C.ClaimID,
	hddis.HighLevelDisease AS Disease,
	S.State,
	J.Jurisdiction,
	cs.ClaimSequence,
	CPI.LastName + ', ' + CASE
		WHEN FirstName IS NULL THEN '' ELSE FirstName + ' '
	END + CASE
		WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' '
	END AS ClaimantName,
	CASE
		WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4)
	END AS SSN,
	CONVERT(date, CPI.BirthDate) AS BirthDate,
	CPI.DeceasedDate,
	CASE
		WHEN MONTH(BirthDate) < MONTH(DeceasedDate) THEN DATEDIFF(yy, BirthDate, DeceasedDate)
		WHEN MONTH(BirthDate) > MONTH(DeceasedDate) THEN DATEDIFF(yy, BirthDate, DeceasedDate) - 1
		WHEN MONTH(BirthDate) = MONTH(DeceasedDate) THEN CASE
			WHEN DAY(BirthDate)
			> DAY(DeceasedDate) THEN DATEDIFF(yy, BirthDate, DeceasedDate) - 1 ELSE DATEDIFF(yy, BirthDate, DeceasedDate)
		END
	END AS DeathAge,
	cls.ClaimLawsuitStatus,
	CONVERT(date, L.ServiceDate) AS ServiceDate,
	CONVERT(date, L.FileDate) AS FileDate,
	L.DocketNumber,
	L.CaseCaption,
	L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,
	pca.PlaintiffCounsel AS PrimaryLocalPlaintiffCounsel,
	r_1.DefenseCounsel AS PrimaryDefenseCounsel,
	CPI.ClaimantPersonalInfoID,
	ReleaseSignedDate,
	rt.ReleaseType,
	CLst.StatusNotes,
	ExecutedReleaseDate AS ReleaseReceivedDate,
	SettlementDueDate,
	PaidDate,
	CheckNumber,
	--JOBSITE,
	ClaimDiseaseID,
	cl.LawsuitID,
	ActiveTrialDate,
	StatusDate,
	SettlementDate,
	cd.DiseaseID,
	individualdocketnumber,
	doc.DoctorLastName + ', ' + COALESCE(doc.DoctorFirstName, '') + COALESCE(' ' + doc.DoctorMiddleName, '') AS Doctor,
	SettlementAmount,
	SettlementGroupID,
	FullyDocumented,
	CauseOfInjuryID,
	CASE
		WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + ', ' + COALESCE(ilodoc.DoctorFirstName, '') + COALESCE(' ' + ilodoc.DoctorMiddleName, '')
	END AS ILODoctor,
	CASE
		WHEN PFTDoctorID IS NULL THEN NULL ELSE pftdoc.DoctorLastName + ', ' + COALESCE(pftdoc.DoctorFirstName, '') + COALESCE(' ' + pftdoc.DoctorMiddleName, '')
	END AS PFTDoctor,
	ILODoctorID,
	ILOProfusion,
	ILODate,
	PFTDoctorID,
	PFTDate,
	PFTTypeID,
	PFTFVC_Actual,
	PFTFVC_Predicted,
	CASE
		WHEN PFTFVC_Predicted <> 0 THEN (PFTFVC_Actual / PFTFVC_Predicted) ELSE 0
	END AS PFTFVC_Pct,

	PFTFEV1_Actual,
	PFTFEV1_Predicted,
	CASE
		WHEN PFTFEV1_Predicted <> 0 THEN (PFTFEV1_Actual / PFTFEV1_Predicted) ELSE 0
	END AS PFTFEV1_Pct,
	[PFTFEV1andFVC_Actual] AS PFTFEV1FVC_Actual,
	[PFTFEV1andFVC_Predicted] AS PFTFEV1FVC_Predicted,
	CASE
		WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN ([PFTFEV1andFVC_Actual] / [PFTFEV1andFVC_Predicted]) ELSE 0
	END AS PFTFEV1FVC_Pct,

	PFTTLC_Actual,
	PFTTLC_Predicted,
	CASE
		WHEN PFTTLC_Predicted <> 0 THEN (PFTTLC_Actual / PFTTLC_Predicted) ELSE 0
	END AS PFTTLC_Pct,
	PFTDLCO_Actual,
	PFTDLCO_Predicted,
	CASE
		WHEN PFTDLCO_Predicted <> 0 THEN (PFTDLCO_Actual / PFTDLCO_Predicted) ELSE 0
	END AS PFTDLCO_Pct,
	PFTTV,
	IsThirdPartyComplaint,
	IsMDL,
	AllegationType + ' - ' + Material AS Allegation,
	CASE
		WHEN IsPrimaryForClaim = 1 THEN 'YES' ELSE 'NO'
	END AS PrimaryLawsuit,
	cd.DiseaseDiagnosisDate
FROM dbo.tblClaimantPersonalInfo AS CPI
INNER JOIN dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
INNER JOIN dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID
INNER JOIN dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID
INNER JOIN dbo.tblState AS S ON S.StateID = L.StateID
INNER JOIN dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID
INNER JOIN dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID
LEFT OUTER JOIN (SELECT
	dcs.DefenseCounseladdressID,
	d.DefenseCounsel,
	dcs.ResponsibleDefenseCounseladdressID,
	dcs.StateID,
	r.DefenseCounsel AS ResponsibleDefenseCOunsel,
	dcs.IsActiveCounsel
FROM dbo.tblDefenseCounselState AS dcs
INNER JOIN dbo.tblDefenseCounselAddress dca ON dca.defensecounseladdressid = dcs.defensecounseladdressid
INNER JOIN dbo.tbldefensecounseladdress dcar ON dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
INNER JOIN dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
INNER JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
GROUP BY	d.DefenseCounsel,
			r.DefenseCounsel,
			dcs.DefenseCounseladdressID,
			dcs.ResponsibleDefenseCounseladdressID,
			dcs.IsActiveCounsel,
			dcs.StateID) AS r_1 ON r_1.DefenseCounseladdressID = L.DefenseCounselAddressID AND r_1.StateID = L.StateID
LEFT JOIN (SELECT
	*
FROM tblClaimDisease
WHERE PrimaryDisease = 1) AS CD ON CD.ClaimID = c.ClaimID
LEFT JOIN tblDisease ddis ON ddis.DiseaseID = cd.DiseaseID
LEFT JOIN tblHighLevelDisease hddis ON hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID
LEFT JOIN (SELECT
	*
FROM tblClaimantEmployment
WHERE IsPrimary = 1) CE ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
LEFT JOIN (SELECT
	*
FROM dbo.tblClaimLawsuitStatusTracking
WHERE IsPrimaryStatus = 1) clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
LEFT OUTER JOIN dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID
LEFT OUTER JOIN dbo.tblSettlePaymentDocumentation spd ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT OUTER JOIN dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
LEFT JOIN tblplaintiffcounseladdress pca ON pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
LEFT JOIN tblDoctor doc ON cd.DoctorID = doc.DoctorID
LEFT JOIN tblDoctor ILODoc ON ILODoctorID = ILODoc.DoctorID
LEFT JOIN tblDoctor PFTDoc ON PFTDoctorID = PFTDoc.DoctorID
LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
LEFT JOIN tblAllegationType at ON a.AllegationTypeID = at.AllegationTypeID
LEFT JOIN tblMaterial mat ON a.MaterialID = mat.MaterialID
WHERE C.ClaimID = @ClaimID AND cl.IsPrimaryForClaim = 1
GROUP BY
C.ClaimID,
	hddis.HighLevelDisease ,
	S.State,
	J.Jurisdiction,
	cs.ClaimSequence,
	CPI.LastName + ', ' + CASE
		WHEN FirstName IS NULL THEN '' ELSE FirstName + ' '
	END + CASE
		WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' '
	END ,
	CASE
		WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4)
	END ,
	CONVERT(date, CPI.BirthDate) ,
	CPI.DeceasedDate,
	CASE
		WHEN MONTH(BirthDate) < MONTH(DeceasedDate) THEN DATEDIFF(yy, BirthDate, DeceasedDate)
		WHEN MONTH(BirthDate) > MONTH(DeceasedDate) THEN DATEDIFF(yy, BirthDate, DeceasedDate) - 1
		WHEN MONTH(BirthDate) = MONTH(DeceasedDate) THEN CASE
			WHEN DAY(BirthDate)
			> DAY(DeceasedDate) THEN DATEDIFF(yy, BirthDate, DeceasedDate) - 1 ELSE DATEDIFF(yy, BirthDate, DeceasedDate)
		END
	END ,
	cls.ClaimLawsuitStatus,
	CONVERT(date, L.ServiceDate) ,
	CONVERT(date, L.FileDate) ,
	L.DocketNumber,
	L.CaseCaption,
	L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName ,
	pca.PlaintiffCounsel ,
	r_1.DefenseCounsel ,
	CPI.ClaimantPersonalInfoID,
	ReleaseSignedDate,
	rt.ReleaseType,
	CLst.StatusNotes,
	ExecutedReleaseDate,
	SettlementDueDate,
	PaidDate,
	CheckNumber,
	--JOBSITE,
	ClaimDiseaseID,
	cl.LawsuitID,
	ActiveTrialDate,
	StatusDate,
	SettlementDate,
	cd.DiseaseID,
	individualdocketnumber,
	doc.DoctorLastName + ', ' + COALESCE(doc.DoctorFirstName, '') + COALESCE(' ' + doc.DoctorMiddleName, '') ,
	SettlementAmount,
	SettlementGroupID,
	FullyDocumented,
	CauseOfInjuryID,
	CASE
		WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + ', ' + COALESCE(ilodoc.DoctorFirstName, '') + COALESCE(' ' + ilodoc.DoctorMiddleName, '')
	END ,
	CASE
		WHEN PFTDoctorID IS NULL THEN NULL ELSE pftdoc.DoctorLastName + ', ' + COALESCE(pftdoc.DoctorFirstName, '') + COALESCE(' ' + pftdoc.DoctorMiddleName, '')
	END ,
	ILODoctorID,
	ILOProfusion,
	ILODate,
	PFTDoctorID,
	PFTDate,
	PFTTypeID,
	PFTFVC_Actual,
	PFTFVC_Predicted,
	CASE
		WHEN PFTFVC_Predicted <> 0 THEN (PFTFVC_Actual / PFTFVC_Predicted) ELSE 0
	END ,

	PFTFEV1_Actual,
	PFTFEV1_Predicted,
	CASE
		WHEN PFTFEV1_Predicted <> 0 THEN (PFTFEV1_Actual / PFTFEV1_Predicted) ELSE 0
	END ,
	[PFTFEV1andFVC_Actual] ,
	[PFTFEV1andFVC_Predicted] ,
	CASE
		WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN ([PFTFEV1andFVC_Actual] / [PFTFEV1andFVC_Predicted]) ELSE 0
	END ,

	PFTTLC_Actual,
	PFTTLC_Predicted,
	CASE
		WHEN PFTTLC_Predicted <> 0 THEN (PFTTLC_Actual / PFTTLC_Predicted) ELSE 0
	END ,
	PFTDLCO_Actual,
	PFTDLCO_Predicted,
	CASE
		WHEN PFTDLCO_Predicted <> 0 THEN (PFTDLCO_Actual / PFTDLCO_Predicted) ELSE 0
	END ,
	PFTTV,
	IsThirdPartyComplaint,
	IsMDL,
	AllegationType + ' - ' + Material ,
	CASE
		WHEN IsPrimaryForClaim = 1 THEN 'YES' ELSE 'NO'
	END ,
	cd.DiseaseDiagnosisDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [power_user]
GO
