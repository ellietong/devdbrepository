SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Proc [dbo].[qrysrptClaimLawsuitStatus]
(@ClaimlawsuitID bigint)

As
Set NoCount On
Select Claimlawsuitstatustrackingid, StatusDate, ClaimLawsuitStatus, StatusNotes, ProcessedDate, IsPrimaryStatus
From tblClaimLawsuitStatusTracking cts
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = cts.ClaimLawsuitStatusID
Where ClaimlawsuitID = @ClaimlawsuitID
Order By ProcessedDate

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimLawsuitStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimLawsuitStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimLawsuitStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimLawsuitStatus] TO [power_user]
GO
