SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[qryALLColumnTotalDates_SigOnly] (@CorporateHistoryID nvarchar(20), @TriggerStart nvarchar(20), @TriggerEnd nvarchar(20), 
	@AllocationID nvarchar(10), @strInsuranceProfile nvarchar(1000), @strVerticalCollapse nvarchar(200),@strColumns nvarchar(1000))
	
as 
SET NOCOUNT ON

Declare @strInsuranceProfileNew nvarchar(1000)
Set @strInsuranceProfileNew = Replace(Replace(@strInsuranceProfile, 'penddate', 'columnenddate'), 'pstartdate', 'columnstartdate')



Declare @strsql nvarchar(4000)
Set @strsql = 



'SELECT SUM(CONVERT(int, (CASE WHEN ColumnEndDate < (Case When ''' + @TriggerEnd + ''' < dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+')  Then '''+ @TriggerEnd+''' Else dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+') End)
				THEN ColumnEndDate 
				ELSE (Case When ''' + @TriggerEnd + ''' < dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+')  Then '''+ @TriggerEnd+''' Else dbo.fnCorpHistoryEndDate(InsuranceProgramID, '+ @CorporateHistoryID+') End) END) - 
				(CASE WHEN ColumnStartDate < (Case When '''+ @TriggerStart +''' < dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Then dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Else ''' + @TriggerStart + ''' End) 
				THEN (Case When '''+ @TriggerStart +''' < dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Then dbo.fnCorpHistoryStartDate(InsuranceProgramID, '+ @CorporateHistoryID+') Else ''' + @TriggerStart + ''' End) 
				ELSE ColumnStartDate END))) AS TriggeredDays 
FROM tblAllocationResultsColumn ac
WHERE AllocationID = ' + @AllocationID + ' 
AND (ColumnStartDate < ''' + @TriggerEnd + ''') 
AND (ColumnEndDate > ''' + @TriggerStart + ''') 
AND ('+ @strInsuranceProfileNew + ')' 
+ @strVerticalCollapse + ' AND ('+@strColumns+')'

EXEC sp_executesql @strsql

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLColumnTotalDates_SigOnly] TO [Allocation_User]
GRANT EXECUTE ON  [dbo].[qryALLColumnTotalDates_SigOnly] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLColumnTotalDates_SigOnly] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLColumnTotalDates_SigOnly] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLColumnTotalDates_SigOnly] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLColumnTotalDates_SigOnly] TO [power_user]
GO
