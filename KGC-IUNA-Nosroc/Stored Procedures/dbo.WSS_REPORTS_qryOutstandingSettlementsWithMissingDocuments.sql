SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_REPORTS_qryOutstandingSettlementsWithMissingDocuments] (@ClaimantPersonalInfoID int, @LawsuitID int, @MatterID int)

As

SET NOCOUNT ON

--DECLARE @ClaimantPersonalInfoID int
--DECLARE @LawsuitID int
--DECLARE @MatterID int
--SET @ClaimantPersonalInfoID = 18446
--SET @LawsuitID = 14510
--SET @MatterID = 1
--select * from tblClaimLawsuit where claimid=21774

DECLARE @ClaimantDocs TABLE(ClaimantIDed int, DxMedicals int, DODCert int, EstatePapers int, ProductID int, EmployExpoHist int, settlementconfirmation int, medicareform int, productid2 int, startdate datetime, enddate datetime,
birthdate datetime, socialsecuritynumber int, diseasediagnosisdate datetime, diseaseid int)
DECLARE @MissingDocuments TABLE(MissingDocName nvarchar(100))

INSERT INTO @ClaimantDocs
	SELECT sp.ClaimantIDed, sp.DxMedicals, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE sp.DODCert END AS DODCert, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE sp.EstatePapers END AS EstatePapers, 
		sp.ProductID, sp.EmployExpoHist, sp.settlementconfirmation, 
		 CASE WHEN MedicareFormRcvd IS NOT NULL 
		 THEN 1   when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) <= '12/5/1980' then 1 else 0
	 END, cp.productid, cp.startdate, cp.enddate, cpi.BirthDate, cpi.SocialSecurityNumber , DiseaseDiagnosisDate, DiseaseID
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID		
		INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		inner join tblSettlementDocumentation sp on sp.SetPymtID = spd.SetPymtID
		left join tblClaimantEmployment ce on ce.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID 
		left join tblClaimantProduct cp on cp.ClaimantEmploymentID =ce.ClaimantEmploymentID 
		left join tblclaimdisease cd on cd.ClaimID =c.ClaimID 
	WHERE c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND LawsuitID = @LawsuitID
		AND MatterID = @MatterID
		
INSERT INTO @MissingDocuments
	SELECT 'Fully Identified - SSN, DOB, DOD' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ClaimantIDed IS NULL Or ClaimantIDed = 0 or birthdate is null or socialsecuritynumber  is null
INSERT INTO @MissingDocuments
	SELECT 'Diagnosing Medicals' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DxMedicals IS NULL Or DxMedicals = 0 or diseasediagnosisdate is null or diseaseid is null
INSERT INTO @MissingDocuments
	SELECT 'Death Certificate' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DODCert IS NULL Or DODCert = 0
INSERT INTO @MissingDocuments
	SELECT 'Estate Paperwork' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EstatePapers IS NULL Or EstatePapers = 0 
INSERT INTO @MissingDocuments
	SELECT 'Product ID' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ProductID IS NULL Or ProductID = 0 or ProductID2 is null or StartDate is null or EndDate is null
INSERT INTO @MissingDocuments
	SELECT 'Employment/Exposure History' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EmployExpoHist IS NULL Or EmployExpoHist = 0
INSERT INTO @MissingDocuments
	SELECT 'Settlement Confirmation Letter' AS MissingDocName
	FROM @ClaimantDocs
	WHERE settlementconfirmation IS NULL Or settlementconfirmation = 0

SELECT MissingDocName FROM @MissingDocuments
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementsWithMissingDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementsWithMissingDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementsWithMissingDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingSettlementsWithMissingDocuments] TO [power_user]
GO
