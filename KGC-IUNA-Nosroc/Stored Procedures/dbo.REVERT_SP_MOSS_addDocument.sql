SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_addDocument]
(
	@DetailDocumentTypeID int,					--aka ClaimantDocumentTypeID
	@ClaimantPersonalInfoID bigint = null,
	@CreatedBy nvarchar(250),
	@DateCreated datetime,
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@SPDocId nvarchar(50),	
	@Comments nvarchar(4000) = null,			--comments are optional	
	@FirmID int,
	@DocumentSourceID int,

	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,
    @Adjustment money = null,
    @AdjustmentNotes nvarchar(4000) = null,
    @StatusID int = null,
    @MatterID int = null,
    @Approved bit = null,
    @ApprovedBy nvarchar(250) = null,
    @DateApproved datetime = null
)

AS
BEGIN
DECLARE @MaxDocID int
SET @MaxDocID = (SELECT IDENT_CURRENT('tblDocuments')+IDENT_INCR('tblDocuments'))

INSERT INTO tblDocuments (
	DetailDocumentTypeID,	
	ClaimantPersonalInfoID,
	Comments,
	DocumentURL,
	DocumentTitle,
	SPDocId,
	CreatedBy,
	DateCreated,
	InvoiceNumber,
	Period,
	DatePayable,
	FeeAmount,
	DisbursementAmount,
    Adjustment,
    AdjustmentNotes,
    StatusID,	
	ModifiedBy,
	DateModified,
	FirmID,
	DocumentSourceID,
	MatterID,
	ApprovedBy,
	DateApproved
)
VALUES (	
	@DetailDocumentTypeID,
	@ClaimantPersonalInfoID,
	@Comments,
	REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
	@DocumentTitle,
	@SPDocId,
	@CreatedBy,
	@DateCreated,
	@InvoiceNumber,
	@Period,
	@DatePayable,
	@FeeAmount,
	@DisbursementAmount,
    @Adjustment,
    @AdjustmentNotes,
    @StatusID,	
	@CreatedBy,
	@DateCreated,	
	@FirmID,
	@DocumentSourceID,
	@MatterID,
	@ApprovedBy,
	@DateApproved
)


--Test to see if we should auto-accept this document **NEEDS UPDATE: see if claimant was paid already or if release recv'd -- if so, we can auto accept
IF (@DetailDocumentTypeID IN (1,2,6,7,12,13,14,15,17,18,19,20,21,22,24,26,30,35,37,38,42) AND RIGHT(@CreatedBy, 11) <> 'kcicllc.com'
	AND @ClaimantPersonalInfoID NOT IN (
		SELECT ClaimantPersonalInfoID 
		FROM tblClaim c 
		WHERE c.ClaimID IN (SELECT cl.ClaimID FROM tblClaimLawsuitStatusTracking clst 
							INNER JOIN tblClaimLawsuit cl ON clst.ClaimLawsuitID = cl.ClaimLawsuitID 
							INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
							WHERE ClaimLawsuitStatusID IN (23, 24, 25, 28))))
BEGIN
UPDATE tblDocuments
SET AcceptedBy = 'demouser@kcicllc.com',
	Accepted = 1,
	DateAccepted = GETDATE()
WHERE DocumentID = @MaxDocID
END

SELECT @MaxDocID


--SELECT * FROM tblClaim WHERE ClaimantPersonalInfoID = 7

--SELECT ClaimID FROM tblClaimLawsuitStatusTracking clst INNER JOIN tblClaimLawsuit cl ON clst.ClaimLawsuitID = cl.ClaimLawsuitID WHERE ClaimLawsuitStatusID IN (23, 24, 25, 28)

--SELECT * FROm tblClaimantDocumentType WHERE ClaimantDocumentTypeID IN (1,2,6,7,12,13,14,15,17,18,19,20,21,22,24,26,30,35,37,38,42)

END

--SELECT * FROM tblClaim WHERE IsCurrentClaim = 1
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_addDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_addDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_addDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_addDocument] TO [power_user]
GO
