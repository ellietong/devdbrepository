SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryDisease]
AS
BEGIN
	SELECT DiseaseID, COALESCE(Disease, 'Unknown') + ' (ICD9: ' + COALESCE(CONVERT(nvarchar(10),ICD9),'N/A') + '), ' + COALESCE(HighLevelDisease, 'Unknown') AS Disease
	FROM tblDisease dn
	LEFT JOIN tblHighLevelDisease hld ON dn.HighLevelDiseaseID = hld.HighLevelDiseaseID --WHERE ICD9 IS NOT NULL or dn.diseaseid = 20
	UNION
	SELECT NULL, ''
	ORDER BY Disease
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDisease] TO [power_user]
GO
