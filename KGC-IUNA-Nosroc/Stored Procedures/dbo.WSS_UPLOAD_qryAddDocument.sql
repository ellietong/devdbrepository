SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryAddDocument]
(
	@DetailDocumentTypeID int,					--aka ClaimantDocumentTypeID
	@ClaimantPersonalInfoID bigint = null,
	@CreatedBy nvarchar(250),
	@DateCreated datetime,
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@Comments nvarchar(4000) = null,			--comments are optional	
	@FirmID int,
	@DocumentSourceID int,

	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,
    @Adjustment money = null,
    @AdjustmentNotes nvarchar(4000) = null,
    @StatusID int = null,
    @MatterID int = null,
    @Approved bit = null,
    @ApprovedBy nvarchar(250) = null,
    @DateApproved datetime = null
)

AS
BEGIN
DECLARE @MaxDocID int
SET @MaxDocID = (SELECT IDENT_CURRENT('tblDocuments')+IDENT_INCR('tblDocuments'))

IF(RIGHT(@CreatedBy, 11) = 'kcicllc.com') OR (@DetailDocumentTypeID IN (40, 41))
BEGIN
INSERT INTO tblDocuments (
	DetailDocumentTypeID,	
	ClaimantPersonalInfoID,
	Comments,
	DocumentURL,
	DocumentTitle,
	CreatedBy,
	DateCreated,
	InvoiceNumber,
	Period,
	DatePayable,
	FeeAmount,
	DisbursementAmount,
    Adjustment,
    AdjustmentNotes,
    StatusID,	
	ModifiedBy,
	DateModified,
	FirmID,
	DocumentSourceID,
	MatterID,
	ApprovedBy,
	DateApproved
)
VALUES (	
	@DetailDocumentTypeID,
	@ClaimantPersonalInfoID,
	@Comments,
	REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
	@DocumentTitle,
	@CreatedBy,
	@DateCreated,
	@InvoiceNumber,
	@Period,
	@DatePayable,
	@FeeAmount,
	@DisbursementAmount,
    @Adjustment,
    @AdjustmentNotes,
    @StatusID,	
	@CreatedBy,
	@DateCreated,	
	@FirmID,
	@DocumentSourceID,
	@MatterID,
	@ApprovedBy,
	@DateApproved
)
SELECT @MaxDocID
END
ELSE
BEGIN
INSERT INTO tblDocuments (
	DetailDocumentTypeID,	
	ClaimantPersonalInfoID,
	Comments,
	DocumentURL,
	DocumentTitle,
	CreatedBy,
	DateCreated,
	InvoiceNumber,
	Period,
	DatePayable,
	FeeAmount,
	DisbursementAmount,
    Adjustment,
    AdjustmentNotes,
    StatusID,	
	ModifiedBy,
	DateModified,
	FirmID,
	DocumentSourceID,
	MatterID,
	ApprovedBy,
	DateApproved,
	Accepted,
    AcceptedBy,
    DateAccepted
)
VALUES (	
	@DetailDocumentTypeID,
	@ClaimantPersonalInfoID,
	@Comments,
	REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
	@DocumentTitle,
	@CreatedBy,
	@DateCreated,
	@InvoiceNumber,
	@Period,
	@DatePayable,
	@FeeAmount,
	@DisbursementAmount,
    @Adjustment,
    @AdjustmentNotes,
    @StatusID,	
	@CreatedBy,
	@DateCreated,	
	@FirmID,
	@DocumentSourceID,
	@MatterID,
	@ApprovedBy,
	@DateApproved,
	1,
	'iuna_nosroc@kcicllc.com',
	GETDATE()
)
SELECT @MaxDocID

END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [power_user]
GO
