SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalWilbrahamInvoiceLoaded]

AS
BEGIN
/*External email to alert that Wilbraham Lawler has uploaded an invoice. - BEH*/

--Declare and set sp_send_dbmail input variables
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (max)

SET @Subject = 'IUNA/Nosroc: Wilbraham, Lawler & Buba Invoice(s) Uploaded ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top and the header for table 1
--KCIC logo is being pulled from the KCIC site.  Contact D. Snyder with questions or requests.	
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>
<H4>Wilbraham Lawler & Buba uploaded the following invoice(s) on ' + CONVERT(varchar, GETDATE(), 107) + ':</H4>'

--Create temp table for invoices which have been uploaded by WLB in the past day.
DECLARE @NewInvoicesTable TABLE (spDocID nvarchar (100), FirmID int, [status] nvarchar (30), invoiceNum nvarchar (100),
period nvarchar (100), fees money, disbursement money, adjustment money, submitDate datetime, datePayable datetime, Matter nvarchar (50))
INSERT INTO @NewInvoicesTable
	SELECT
		SPDocID,
		FirmID,
		[Status],
		InvoiceNumber,
		Period,
		FeeAmount,
		DisbursementAmount,
		Adjustment,
		DateCreated,
		DatePayable,
		Matter
	FROM tblDocuments d
	LEFT JOIN tblDefenseInvoiceStatus s ON d.StatusID = s.StatusID
	INNER JOIN tblMatter m ON m.MatterID = d.MatterID
	WHERE Datecreated BETWEEN GETDATE() - 1 AND GETDATE()
	AND detailDocumentTypeID IN (0, 40, 41)
	--This email only includes invoices uploaded by Wilbraham Lawler
	AND FirmID = 38
	GROUP BY	DocumentURL,
				SPDocId,
				FirmID,
				[Status],
				InvoiceNumber,
				Period,
				FeeAmount,
				DisbursementAmount,
				Adjustment,
				DateCreated,
				DatePayable,
				Matter


--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @BodyMessage +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	border: 1px solid #38160C;
	color: #F6ECF0;}
TD	{font-family: Goudy Old Style; 
	font-size: 10pt; 
	text-align: center;
	border: 1px solid rgb(0,121,193);
	padding: .3em;
	color: #000000}
TH {font-family: Goudy Old Style; 
	font-size: 10pt; 
	border: 1px solid #ffffff;
	padding: .3em;
	color: #F6ECF0;
	background: rgb(0,121,193);}
--->
</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @bodymessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #0A0A0A;">' +
N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Firm</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Number</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Period</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Fees</th>' +
'<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Expenses</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Total Billed</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Submit Date</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Payable</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		dc.DefenseCounsel)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		invoicenum)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		period)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), COALESCE(fees, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), COALESCE(disbursement, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), (COALESCE(fees, 0) + COALESCE(disbursement, 0)), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(100), submitdate, 101))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(100), datepayable, 101))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		Matter)
	AS td,
	''

FROM @NewInvoicesTable i
INNER JOIN tblDefenseCounsel dc ON dc.DefenseCounselID = i.firmID
ORDER BY dc.DefenseCounsel, submitdate, Matter
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'



--Include link to the King and Spalding Invoice library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Invoices.aspx">here</A> to access the invoice(s).</b><br><br>'

--Include KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC <br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


IF (SELECT
	COUNT(DISTINCT spdocid)
FROM @NewInvoicesTable) < > 0
BEGIN
--Execute email-sending stored procedure
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'j.heenan@envirosafeservices.com',
								@blind_copy_recipients = 'iuna_nosroc@kcic.com',
								--@recipients = 'amanp@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'
END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalWilbrahamInvoiceLoaded] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalWilbrahamInvoiceLoaded] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalWilbrahamInvoiceLoaded] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalWilbrahamInvoiceLoaded] TO [power_user]
GO
