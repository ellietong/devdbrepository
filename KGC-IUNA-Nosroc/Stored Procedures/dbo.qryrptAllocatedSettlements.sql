SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptAllocatedSettlements] (@SettlementDateBegin datetime, @SettlementDateEnd datetime,@ExecutedReleaseBegin datetime, @ExecutedReleaseEnd datetime, @header nvarchar(100), @Matter int, @carrierid int, @occurrencegroupid int)

As
SET NOCOUNT ON

-- Declare @SettlementDateBegin datetime
-- Declare @SettlementDateEnd datetime
-- Set @SettlementDateBegin='1/1/2010'
--Set @SettlementDateEnd='12/31/2013'
-- Declare @ExecutedReleaseBegin datetime
-- Declare @ExecutedReleaseEnd datetime
-- Set @ExecutedReleaseBegin='10/1/2013'
--Set @ExecutedReleaseEnd='12/31/2013'
--Declare @header nvarchar(100)
--set @header = '2nd Quarter 2013'
--Declare @Matter int
--Set @Matter = 1
--Declare @carrierid int
--set @carrierid = 9
--Declare @occurrencegroupid int
--set @occurrencegroupid = 170


--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Select 	C.ClaimID, LastName, FirstName, Firstname+' '+LastName as ClaimantName, 'XXX-XX-'+ Right([SocialSecurityNumber],4) as SocialSecurityNumber, State,
	HighLevelDisease, FileDate, PaidDate, SettlementDate,dbo.fnEarliestExposure(cpi.Claimantpersonalinfoid) as earliestexposuredate,
	DiseaseDiagnosisDate as DiagnosisDate, SettlementAmount, case when sum(e.indemnityallocation) is null then '0' else sum(e.IndemnityAllocation) end as AllocatedAmount,
	Coalesce(@SettlementDateBegin,'1/1/1900') as PaidDateBegin,
	Coalesce(@SettlementDateEnd,convert(varchar(10),getdate(),101)) as PaidDateEnd, CPI.DeceasedDate AS dateofdeath,
	@header as header, CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
	
From tblClaimantPersonalInfo CPI
Left Join tblClaimantEmployment CE on CPI.ClaimantPersonalInfoID=CE.ClaimantPersonalInfoID
Left Join tblClaim C on CPI.ClaimantPersonalInfoID=C.ClaimantPersonalInfoID
Left Join (select * from tblClaimDisease where PrimaryDisease = 1) cd on cd.ClaimID = c.ClaimID
Left Join tblDisease D on cd.DiseaseID=D.DiseaseID
Left Join tblHighLevelDisease HLD on D.HighLevelDiseaseID=HLD.HighLevelDiseaseID
Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left  Join tblClaimantSettlement CS on CL.ClaimLawsuitID=CS.CLaimLawsuitID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblState S on L.StateID=S.StateID
Left Join tblMatter M on C.MatterID = m.MatterID
left join tblCourtType ct on ct.CourtTypeID = L.CourtTypeID
left join tblJurisdiction j on j.JurisdictionID = L.JurisdictionID
left join tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID =CL.ClaimLawsuitID
Left join tblOccurrence o on o.ClaimID = c.ClaimID
Left join tblOccurrenceGroup og on og.OccurrenceGroupID = o.OccurrenceGroupID
Left join (select indemnityallocation, occurrenceid,ExhaustionID from tblExhaustion e left join tblpolicydates pd on pd.PolicyDateID =e.PolicyDateID 
left join tblpolicy p on p.PolicyID =pd.PolicyID where p.carrierid = @carrierid ) e on e.OccurrenceID = o.OccurrenceID
--Left join tblPolicyDates pd on pd.PolicyDateID = e.PolicyDateID
--Left join tblPolicy p on p.PolicyID = pd.PolicyID


Where (IsPrimary=1 
and (SettlementDate>= Coalesce(@SettlementDateBegin,'1/1/1900') 
AND SettlementDate<= Coalesce(@SettlementDateEnd,convert(varchar(10),getdate(),101)))
AND ExecutedReleaseDate Between @ExecutedReleaseBegin AND @ExecutedReleaseEnd
and c.MatterID =1
and SettlementAmount <> 0  
and 
og.OccurrenceGroupID = @occurrencegroupid)

GROUP BY C.ClaimID, LastName, FirstName, Firstname+' '+LastName, 'XXX-XX-'+ Right([SocialSecurityNumber],4), State, HighLevelDisease,
	FileDate, PaidDate, SettlementDate, dbo.fnEarliestExposure(cpi.Claimantpersonalinfoid),DeceasedDate,
	DiseaseDiagnosisDate,  
	SettlementAmount
Order By SettlementDate

RETURN




 rollback
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocatedSettlements] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocatedSettlements] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocatedSettlements] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocatedSettlements] TO [power_user]
GO
