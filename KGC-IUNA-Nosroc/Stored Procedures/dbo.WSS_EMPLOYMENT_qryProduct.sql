SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryProduct]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SELECT ProductID,  ProductType + ' - ' + ProductManufacturer + ' - ' + ProductBrand + ' - ' + ProductName  AS Product, ProductType, ProductManufacturer, ProductBrand, ProductName
	FROM
	tblProduct p
	Inner Join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
	Inner Join tblProductManufacturer pm on pm.ProductManufacturerID = p.ProductManufacturerID
	Inner Join tblProductName pn on pn.ProductNameID = p.ProductNameID
	INner Join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
	UNION 
	SELECT NULL, '' AS Product, null, null, null, null
	ORDER BY ProductType, ProductManufacturer, ProductBrand, ProductName
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryProduct] TO [power_user]
GO
