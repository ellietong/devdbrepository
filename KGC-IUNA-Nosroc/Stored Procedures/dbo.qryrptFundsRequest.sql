SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC  [dbo].[qryrptFundsRequest] (@Matter int)

As

SET NOCOUNT ON

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblDefenseInvoice

SELECT Matter, DI.MatterID, Firm, InvoiceTotal
FROM tblDefenseInvoice DI
Inner Join tblDefenseInvoiceFirm DIF on DI.FirmID=DIF.FirmID
LEFT JOIN tblMatter m ON DI.MatterID = m.MatterID

Where StatusID in (1,2) and DatePayable <= 
(SELECT CONVERT(datetime, CONVERT(varchar, GETDATE(), 101), 101) + 30 AS OneMonthOut)
AND FundsRequestDate is Null
AND DI.MatterID >= COALESCE(@Matter,0) AND DI.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequest] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequest] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequest] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequest] TO [power_user]
GO
