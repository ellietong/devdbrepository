SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryfrmCopyDocumentToDefenseInvoice]
(@DocumentID int, @defensecounselAddressID int)
AS

--declare @documentID int
--declare @defensecounselAddressID int

--set @documentID = 2668598
--set @defensecounselAddressID = 43


--select * from tblDefenseCounselAddress
--select * from tblDefenseInvoiceFirm
--select * from tblDocuments where QCBy is null and DetailDocumentTypeID = 40

SET NOCOUNT ON

/*
Query updated:
CAS - 10/14/11 - updated to pull data out of tbldocuments and write to tbldefenseInvoice.  Previously was pulling information off a form and not
updating data in the tables.  As a result, incorrect data was transfering between tables.

Tables should link as follows: 
tblDefenseInvoiceFirm.FirmID = tblDefenseInvoice.FirmID
tblDefenseInvoice.DefenseCounselAddressID = tblDefenseInvoiceFirm.DefenseCounselAddressID = tblDefenseCounselAddress.DefenseCounselAddressID
tblDefenseCounselAddress.DefenseCounselID = tblDefenseCounsel.DefenseCounselID
*/
--Change the firmID to match the firm ID needed to mail defense invoices from the tbldefenseInvoice table.
DECLARE @FirmID INT

Set @FirmID = (select firmID from tblDefenseInvoiceFirm where DefenseCounselAddressID = @defensecounselAddressID)


--Copy Document Record to tblDefenseInvoice

INSERT INTO tblDefenseInvoice (StatusID, FirmID, InvoiceNumber, Period, 
FeeAmount, DisbursementAmount, Adjustment, InvoiceTotal, SubmitDate, DatePayable, CheckNumber,
FundsRequestDate, DatePaid, DocumentID, AdjustmentNotes, InvoiceLink, CheckLink, DefenseCounselAddressID, MatterID, InvoiceTypeID)

SELECT StatusID, 
@FirmID as Firmid, 
InvoiceNumber AS InvoiceNumber, 
Period AS Period, 
FeeAmount, 
DisbursementAmount, 
Adjustment, 
(FeeAmount + DisbursementAmount - Adjustment) AS InvoiceTotal,
DateCreated AS SubmitDate, 
DatePayable AS DatePayable,
NULL AS CheckNumber,
NULL AS FundsRequestDate,
NULL AS DatePaid,
DocumentID,
AdjustmentNotes,
AmazonKey AS InvoiceLink,
'' AS CheckLink, 
@DefenseCounselAddressID as DefenseCounselAddressID, 
MatterID as MatterID, 
DetailDocumentTypeID as InvoiceTypeID
FROM tblDocuments 
WHERE DocumentID = @DocumentID


--Update Document Record in tblDocuments
UPDATE tblDocuments
SET QCDate = GETDATE()
WHERE DocumentID = @DocumentID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [power_user]
GO
