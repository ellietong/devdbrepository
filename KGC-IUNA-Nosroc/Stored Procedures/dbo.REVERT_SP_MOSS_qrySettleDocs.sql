SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[REVERT_SP_MOSS_qrySettleDocs] (@SettlePaymentID int)
as

--DECLARE @ClaimID int
--SET @ClaimID = 1


Select DocumentURL + CASE WHEN ISNUMERIC(PageNums)=1 AND CHARINDEX(',', PageNums)=0 THEN '#page='+PageNums WHEN CHARINDEX(',', PageNums)-1 > 0 THEN '#page='+LEFT(PageNums, CHARINDEX(',', PageNums)-1)
WHEN CHARINDEX('-', PageNums)-1 > 0 THEN '#page='+LEFT(PageNums, CHARINDEX('-', PageNums)-1)
  ELSE '' END AS DocumentURL, DocumentTitle, PageNums, DocCategory, SettleDocID, SetPymtID, sd.DocumentID, sd.DocCategoryID
From tblSettleDocs sd 
INNER JOIN tblDocuments d ON sd.DocumentID = d.DocumentID
INNER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
WHERE DetailDocumentTypeID <> 40 --invoices
AND SetPymtID = @SettlePaymentID
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qrySettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qrySettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qrySettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qrySettleDocs] TO [power_user]
GO
