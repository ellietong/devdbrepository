SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCedure [dbo].[WSS_DOCUMENT_qryFirmDocuments_Filtered](@emailaddress nvarchar (1000)=null, @FilterDocumentTypeID int, @FilterFirmID int)
AS
 
 
--DECLARE @Emailaddress nvarchar(250)
--SET @emailaddress = 'dmccoy@harveykruse.com'
--DECLARE @FilterDocumentTypeID int
--DECLARE @FilterFirmID int
--SET @FilterDocumentTypeID = null
--SET @FilterFirmID = null
 
--Declare and set the temporary field used to check the user's firm based on their login email address
DECLARE @firmid int
SET @firmid = (SELECT DISTINCT FirmID FROM  vtblPerson WHERE Email = @emailaddress)
 
DECLARE @maxDocType int
SET @maxDocType = (SELECT MAX(ClaimantDocumentTypeID) FROM tblClaimantDocumentType)
 
DECLARE @MaxFirmID int
SET @MaxFirmID = (SELECT MAX(DefensecounselID) FROM tblDefenseCounsel)
 
 
IF( @FilterDocumentTypeID IS NULL AND @FilterFirmID IS NULL)
BEGIN
IF (@firmid IN (38, 79) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'))
BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, f.Firm, Accepted
	FROM dbo.tblDocuments AS d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		INNER JOIN tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		INNER JOIN vtblFirm AS f ON d.FirmID = f.FirmID
		INNER JOIN tblClaim As c ON c.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40) 
 
	ORDER BY Accepted ASC, DateCreated DESC
END ELSE BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, f.Firm, Accepted
	FROM dbo.tblDocuments AS d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		INNER JOIN tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		INNER JOIN vtblFirm AS f ON d.FirmID = f.FirmID
		INNER JOIN tblClaim As c ON c.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40) AND cdt.DocumentGroupID <> 4
		AND @firmid = d.FirmID
	ORDER BY Accepted ASC, DateCreated DESC
END
 
END
ELSE
BEGIN
	IF (@firmid IN (38, 79) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'))
	BEGIN
		SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL, d.SPDocId, d.DateCreated, 
			d.AcceptedBy, d.DateAccepted, f.Firm, Accepted
		FROM dbo.tblDocuments AS d
			INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
			INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
			INNER JOIN tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
			INNER JOIN vtblFirm AS f ON d.FirmID = f.FirmID
			INNER JOIN tblClaim As c ON c.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40) 
		AND ClaimantDocumentTypeID >= COALESCE(@FilterDocumentTypeID, 0) AND ClaimantDocumentTypeID <= COALESCE(@FilterDocumentTypeID, @maxDocType)
		AND d.FirmID >= COALESCE(@FilterFirmID, 0) AND d.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
		ORDER BY Accepted ASC, DateCreated DESC
	END 
	ELSE BEGIN
		SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL, d.SPDocId, d.DateCreated, 
			d.AcceptedBy, d.DateAccepted, f.Firm, Accepted
		FROM dbo.tblDocuments AS d
			INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
			INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
			INNER JOIN tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
			INNER JOIN vtblFirm AS f ON d.FirmID = f.FirmID
			INNER JOIN tblClaim As c ON c.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40) AND cdt.DocumentGroupID <> 4
			AND @firmid = d.FirmID
			AND ClaimantDocumentTypeID >= COALESCE(@FilterDocumentTypeID, 0) AND ClaimantDocumentTypeID <= COALESCE(@FilterDocumentTypeID, @maxDocType)
			AND d.FirmID >= COALESCE(@FilterFirmID, 0) AND d.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
		ORDER BY Accepted ASC, DateCreated DESC
	END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments_Filtered] TO [power_user]
GO
