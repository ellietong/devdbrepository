SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryBILLAddPayment]
(@CarrierID int, @SubscriberID int, @BillNotes nvarchar(500), @Payment money, @PaymentDate Date, @PaymentNotes nvarchar(500))
AS
-- Procedure adds a blank Ledger record and a Payment associated with that record
SET NOCOUNT ON

DECLARE @LedgerID INT

-- Insert data to tblLedger
INSERT INTO tblLedger (CarrierID, SubscriberID, Allocation, BillNotes)
VALUES (@CarrierID, @SubscriberID, 0, @BillNotes)
-- Capture Ledger ID
SET @LedgerID = @@IDENTITY

-- Insert data to tblPayment with associated ledgerID
INSERT INTO tblPayment (LedgerID, Payment, PaymentDate, PaymentNote)
VALUES (@LedgerID, @Payment, @PaymentDate, @PaymentNotes)


RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryBILLAddPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryBILLAddPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryBILLAddPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryBILLAddPayment] TO [power_user]
GO
