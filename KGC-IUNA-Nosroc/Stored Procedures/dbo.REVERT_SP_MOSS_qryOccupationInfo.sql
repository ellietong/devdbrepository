SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryOccupationInfo]
(@ClaimantPersonalInfoID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare @UserFirmState table (DefenseCounselID bigint, StateID int)
Insert into @UserFirmState

Select dca.DefenseCounselID, StateID
From dbo.tblDefenseCounselState AS dcs 
left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid --and dcar.stateID = dcs.StateID 
LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID


BEGIN
    -- Insert statements for procedure here
	SELECT DISTINCT ce.ClaimantEmploymentID, Occupation, Jobsite, EmpStartDate, EmpEndDate, cpi.FirstName + ' ' + cpi.LastName AS Claimant,
			JobsiteCity, JobsiteStateID, s.State AS JobsiteState, IsPrimary
	FROM tblClaimantEmployment ce 
	INNER JOIN tblClaimantPersonalInfo cpi ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblState s ON ce.JobsiteStateID = s.StateID
	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	ORDER BY IsPrimary DESC
END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryOccupationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryOccupationInfo] TO [power_user]
GO
