SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailInvoiceSummary_IUNA]

AS
BEGIN
/*External Email to Marie Shipley which runs once a week.  The email includes two tables.
The first table is for invoices which have been uploaded within the past week.
The second table is for invoices which are over a week old but she has not yet approved or rejected. - BEH*/

--Declare and set sp_send_dbmail input variables
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (max)

SET @Subject = 'IUNA: Weekly Invoice Summary ' + CONVERT(varchar, GETDATE() - 7, 107) + ' - ' + CONVERT(varchar, GETDATE(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top and the header for table 1
--KCIC logo is temporarily being pulled from D. Snyder's Google site	
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create temp table for invoices which have been uploaded in the past week
DECLARE @NewWeekInvoicesTable TABLE (spDocID nvarchar (100), [Status] nvarchar (30), invoiceNum nvarchar (100),
Period nvarchar (100), Fees money, Disbursement money, Adjustment money, SubmitDate datetime, DatePayable datetime, Firm nvarchar (100))
INSERT INTO @NewWeekInvoicesTable

	SELECT
		SPDocID,
		[Status],
		InvoiceNumber,
		Period,
		FeeAmount,
		DisbursementAmount,
		Adjustment,
		DateCreated,
		DatePayable,
		DefenseCounsel
	FROM tblDocuments d
	INNER JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
	LEFT JOIN tblDefenseInvoiceStatus s ON d.StatusID = s.StatusID
	INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	WHERE Datecreated BETWEEN GETDATE() - 7 AND GETDATE()
	AND cdt.DocumentGroupID = 2
	AND MatterID = 1
	AND FirmID <> 38
	GROUP BY	SPDocID,
				[Status],
				InvoiceNumber,
				Period,
				FeeAmount,
				DisbursementAmount,
				Adjustment,
				DateCreated,
				DatePayable,
				DefenseCounsel

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
--Set @BodyMessage = @BodyMessage 
--'<STYLE TYPE="text/css">
--<!--
--TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
--TD	{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000}
--TH {font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
----->
--</STYLE>'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

SET @BodyMessage = @bodymessage + COALESCE('<H4>The following IUNA invoices have been uploaded since ' + CONVERT(varchar, GETDATE() - 7, 107) + ':</H4>' + N'<table style=" padding: .3em; border-collapse: collapse; border: 1px solid #0A0A0A;" >' +
N'<tr><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Firm</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Number</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Period</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Fees</th>' +
'<th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Expenses</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Total Billed</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Submit Date</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Payable</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		firm)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		invoicenum)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		period)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), COALESCE(fees, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), COALESCE(disbursement, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), (COALESCE(fees, 0) + COALESCE(disbursement, 0)), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(100), submitdate, 101))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(100), datepayable, 101))
	AS td,
	''

FROM @NewWeekInvoicesTable i
ORDER BY firm, submitdate
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>', '<H4>No IUNA invoices have been uploaded since ' + CONVERT(varchar, GETDATE() - 7, 107) + '.</H4>')

--Add header for table 2
--SET @BodyMessage = @BodyMessage + 

--Create temp table for invoices over a week old which have not yet been approved or rejected
DECLARE @OlderThanWeekInvoicesTable TABLE (spDocID nvarchar (100), [Status] nvarchar (30), invoiceNum nvarchar (100),
Period nvarchar (100), Fees money, Disbursement money, Adjustment money, SubmitDate datetime, DatePayable datetime, Firm nvarchar (100))
INSERT INTO @OlderThanWeekInvoicesTable
	SELECT
		SPDocID,
		[Status],
		InvoiceNumber,
		Period,
		FeeAmount,
		DisbursementAmount,
		Adjustment,
		DateCreated,
		DatePayable,
		DefenseCounsel
	FROM tblDocuments d
	INNER JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
	LEFT JOIN tblDefenseInvoiceStatus s ON d.StatusID = s.StatusID
	INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	WHERE Datecreated < GETDATE() - 7
	AND cdt.DocumentGroupID = 2
	AND d.StatusID IS NULL
	AND MatterID = 1
	AND FirmID <> 38
	GROUP BY	SPDocID,
				[Status],
				InvoiceNumber,
				Period,
				FeeAmount,
				DisbursementAmount,
				Adjustment,
				DateCreated,
				DatePayable,
				DefenseCounsel


--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @bodymessage + COALESCE('<H4>The following IUNA invoices have been uploaded since ' + CONVERT(varchar, GETDATE() - 7, 107) + ':</H4>' + N'<table style=" padding: .3em; border-collapse: collapse; border: 1px solid #0A0A0A;" >' +
N'<tr><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Firm</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Number</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Period</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Fees</th>' +
'<th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Expenses</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Total Billed</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Submit Date</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Payable</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		firm)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		invoicenum)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		period)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), COALESCE(fees, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), COALESCE(disbursement, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), (COALESCE(fees, 0) + COALESCE(disbursement, 0)), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(100), submitdate, 101))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(100), datepayable, 101))
	AS td,
	''

FROM @OlderThanWeekInvoicesTable i
ORDER BY firm, submitdate
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>', '<H4>No unapproved IUNA invoices before ' + CONVERT(varchar, GETDATE() - 7, 107) + '.</H4>')

--Include link to invoice roll up site at the bottom
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Invoices.aspx">here</A> to access the invoice(s).</b><br><br>'

--Include KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--Execute email-sending stored procedure
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'cjs@wlbdeflaw.com',
								@blind_copy_recipients = 'iuna_nosroc@kcic.com',
								--@recipients = 'amanp@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'

END


GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailInvoiceSummary_IUNA] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailInvoiceSummary_IUNA] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailInvoiceSummary_IUNA] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailInvoiceSummary_IUNA] TO [power_user]
GO
