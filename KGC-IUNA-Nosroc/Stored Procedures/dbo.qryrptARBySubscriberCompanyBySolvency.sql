SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryrptARBySubscriberCompanyBySolvency]
(@AllocationID int = null)
As

SET NOCOUNT ON

DECLARE @qryARByAllocationID TABLE (AllocationID int, AllocationAmt money, SubscriptionCompanySolvency varchar(256) )
DECLARE @maxAllocationID int

SELECT @maxAllocationID = MAX(AllocationID) FROM tblAllocation

DECLARE @Policy TABLE (PolicyID int)
INSERT INTO @Policy

SELECT PolicyID
FROM vPolicy
GROUP BY PolicyID

INSERT INTO @qryARByAllocationID
SELECT a.AllocationID, SUM(ar.Allocation * scov.Percentage) AS AllocationAmt, scomp.SubscriptionCompanySolvency 
FROM (((@Policy p INNER JOIN tblSubscriptionCoverage scov ON p.PolicyID = scov.PolicyID) INNER JOIN tblSubscriberCompany scomp ON scomp.SubscriberID = scov.SubscriberID)
	INNER JOIN tblAllocationResults ar ON p.PolicyID = ar.PolicyID) LEFT JOIN tblAllocation a ON a.AllocationID = ar.AllocationID
GROUP BY a.AllocationID, scomp.SubscriptionCompanySolvency
ORDER BY a.AllocationID

SELECT AllocationID, CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END AS 'SubscriberSolvencyGroup', SUM(AllocationAmt) AS SumOfAllocationAmt
FROM @qryARByAllocationID
WHERE AllocationID >= COALESCE(@allocationID, 0) AND AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID)
GROUP BY AllocationID, CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyBySolvency] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyBySolvency] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyBySolvency] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyBySolvency] TO [power_user]
GO
