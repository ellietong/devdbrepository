SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptWestchesterDefenseCosts] (@Matter int, @MonthYear nvarchar, @paiddatestart datetime, @paiddateend datetime)

As

SET NOCOUNT ON

--DECLARE @Matter int
--SET @Matter = 1
--declare @paiddatestart datetime
--declare @paiddateend datetime
--set @paiddatestart = '3/1/2014'
--set @paiddateend = '3/31/2014'


DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblDefenseInvoice

SELECT di.firmid, Matter, DI.MatterID, Firm, PayTo, InvoiceNumber, Period, InvoiceTotal,SubmitDate,DatePayable, case when di.FirmID=21 and @Matter=1 Then 325000.00 when di.FirmID=21 and @Matter=2 then 0 else FeeAmount end as feeamount, 
DisbursementAmount, 
Case When di.FirmID=21 then 0 else Adjustment end as 'Adjustment',
DI.StatusID, Status,
CASE When (FeeAmount is null AND DisbursementAmount is null) or (FeeAmount+DisbursementAmount=0) then InvoiceTotal 
when di.FirmID=21 and @Matter=1 Then (325000.00+disbursementamount) 
when di.FirmID=21 and @Matter=2 then (0+DisbursementAmount) 
Else FeeAmount+DisbursementAmount 
End as Subtotal,
CASE When (FeeAmount is null AND DisbursementAmount is null) or (FeeAmount+DisbursementAmount=0) Then InvoiceTotal
when di.FirmID=21 and @Matter=1 Then (325000.00+disbursementamount) 
when di.FirmID=21 and @Matter=2 then (0+DisbursementAmount) 
Else FeeAmount+DisbursementAmount-Adjustment
End as AmountPayable
FROM tblDefenseInvoice DI
INNER JOIN tblDefenseInvoiceFirm DIF ON DI.FirmID = DIF.FirmID
INNER JOIN tblMatter M ON DI.MatterID = M.MatterID
INNER JOIN tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
left join tblPaymentDefense pd on pd.DefenseInvoiceID = di.DefenseInvoiceID
left join tblCheck ck on ck.CheckID = pd.CheckID
WHERE ( ck.SourceDate between @paiddatestart and @paiddateend 
AND DIS.StatusID IN (1, 2) AND (DI.SubmitDate IS NOT NULL)aND DI.MatterID >= COALESCE(@Matter,0) AND DI.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) and FundingSourceID in (1,2)) 
or ((di.FirmID = 28 and di.DatePaid between @paiddatestart and @paiddateend)
AND DI.MatterID >= COALESCE(@Matter,0) AND DI.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID))
group by Matter, DI.MatterID, Firm, PayTo, InvoiceNumber, Period, InvoiceTotal,SubmitDate,DatePayable, FeeAmount,
DisbursementAmount, Adjustment, DI.StatusID, Status, DI.FirmID 
ORDER BY Firm, DatePayable, invoicenumber, Status

Return




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptWestchesterDefenseCosts] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptWestchesterDefenseCosts] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptWestchesterDefenseCosts] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptWestchesterDefenseCosts] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptWestchesterDefenseCosts] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptWestchesterDefenseCosts] TO [power_user]
GO
