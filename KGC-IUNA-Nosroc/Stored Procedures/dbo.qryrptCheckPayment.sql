SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptCheckPayment] (@CheckID int, @CheckAmount money)

As

SET NOCOUNT ON

--Declare @CheckID int
--Declare @CheckAmount money
--Set @CheckAmount = 500
--Set @CheckID = 531

Declare @FundingTypeID int
Set @FundingTypeID = (select FundingTypeID from tblCheck where CheckID = @checkID)

Declare @CheckApproved bit
Set @CheckApproved = (Select Case when ca.CheckID is null then 0 Else 1 End From tblCheck c Left join tblCheckApproval ca on c.CheckID = ca.CheckID where c.CheckID = @CheckID)

If @FundingTypeID = 1 Begin

Select c.CheckID, DefenseCounsel as Firm, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip) as FirmAddress, DefenseCounsel as PayTo, SourceNumber as CheckNo, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)) as AmountDesc,
Coalesce(SourceAmount,@CheckAmount) as CheckAmount, SourceDate as CheckDate, Null as PlaintiffCounselName, Null as PCounselAddress, FundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
Where c.CheckID = @CheckID
Group By c.CheckID, DefenseCounsel, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip), DefenseCounsel, SourceNumber, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)),
Coalesce(SourceAmount,@CheckAmount), SourceDate, FundingTypeID, Matter

End

If @FundingTypeID = 2 Begin

Select c.CheckID, Null as Firm, Null as FirmAddress, Null As PayTo, SourceNumber as CheckNo, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)) as AmountDesc,
Coalesce(SourceAmount,@CheckAmount) as CheckAmount, SourceDate as CheckDate, PlaintiffCounsel as PlaintiffCounselName, PlaintiffCounselAddress1 + Case When PLaintiffCOunselAddress2 is null then '' Else ' 
' End + Coalesce(PlaintiffCounselAddress2,'
') + '
' + PlaintiffCounselCity + ', ' +pca.State + ' ' +Convert(nvarchar(10),PlaintiffCounselZip) as PCounselAddress, FundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentSettlement ps on c.CheckID = ps.CheckID
Inner Join tblClaimantSettlement cs on cs.ClaimantSettlementID = ps.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
Where c.CheckID = @CheckID
Group By c.CheckID, SourceNumber, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)), Coalesce(SourceAmount,@CheckAmount),
SourceDate, PlaintiffCounsel, PlaintiffCounselAddress1 + Case When PLaintiffCOunselAddress2 is null then '' Else ' 
' End + Coalesce(PlaintiffCounselAddress2,'
') + '
' + PlaintiffCounselCity + ', ' +pca.State + ' ' +Convert(nvarchar(10),PlaintiffCounselZip), FundingTypeID, Matter

End

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPayment] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckPayment] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPayment] TO [power_user]
GO
