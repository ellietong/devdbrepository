SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[REVERT_SP_MOSS_qryFirmInvoices](@emailaddress nvarchar (1000)=null)
AS
--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'crookc@kcicllc.com'
DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT DISTINCT FirmAddressID FROM vtblPerson p WHERE p.Email = @emailaddress)

IF ((@emailaddress = 'cjs@wlbdeflaw.com') OR (RIGHT(@emailaddress,11) = 'kcicllc.com')) 
BEGIN
	SELECT CAST(d.DocumentID as varchar(50)) as DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm, 				
		m.Matter,
		di.DatePaid, 
		di.CheckNumber,
		d.FirmID,
		d.DetailDocumentTypeID,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal,
		SourceDate
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
		LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
	WHERE dg.DocumentGroupID = 2
	ORDER BY Accepted ASC, DateCreated DESC
END 

ELSE BEGIN
	SELECT 
		cast(d.DocumentID as varchar(50)) as DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		dca.DefenseCounsel as Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm, 				
		m.Matter,
		di.DatePaid, 
		di.CheckNumber,
		d.FirmID,
		d.DetailDocumentTypeID,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal,
		SourceDate
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
		LEFT OUTER JOIN tblDefenseCounselAddress dca ON d.FirmID = dca.DefenseCounselID
		LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
	WHERE dg.DocumentGroupID = 2
		AND dca.DefenseCOunselAddressID = @FirmAddressID
	ORDER BY Accepted ASC, DateCreated DESC
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices] TO [power_user]
GO
