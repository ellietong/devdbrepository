SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantEmploymentInfo] (@ClaimantPersonalInfoID int)
as

Select ClaimantEmploymentID, 
	ClaimantPersonalInfoID, 
	Occupation, 
	Jobsite, 
	Comments, 
	EmpStartDate, 
	EmpEndDate, 
	EarliestExposureDate, 
	LatestExposureDate, 
	IsPrimary,
	IsSecondaryExposure
From tblClaimantEmployment
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By EmpStartDate, Occupation, Jobsite

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [power_user]
GO
