SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[WSS_INVOICE_qryInvoiceTypeFilterList] 

AS
SET NOCOUNT ON
--DECLARE @invoiceFlag bit
--SET @invoiceFlag = 0
--DECLARE @settlementFlag bit
--SET @settlementFlag = 0

	BEGIN
		Select 
			ClaimantDocumentTypeID, 
			ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID =2 AND ClaimantDocumentTypeID <> 41
		UNION
		SELECT null, '<< All Invoice Types >>', null
		ORDER BY ClaimantDocumentType
	END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryInvoiceTypeFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryInvoiceTypeFilterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryInvoiceTypeFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryInvoiceTypeFilterList] TO [power_user]
GO
