SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryClaimantInfo](@ClaimantPersonalInfoID int = NULL)
AS 
SET NOCOUNT ON 

--SELECT *
--FROM vtblPerson

--Declare @emailaddress nvarchar(1000)
--SET @emailaddress = 'crookc@kcicllc.com'
------Set @emailaddress = 'jwatson@wlbdeflaw.com'
----Set @emailaddress = 'mpietrykowski@gordonrees.com'
--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 6940

Declare @AllResponsibleDefenseCounsel table (DefenseCounselAddressID int, 
				DefenseCounsel nvarchar(100), 
				ResponsibleDefenseCounselAddressID int,
				StateID int, 
				ResponsibleDefenseCounsel nvarchar(100),
				IsActiveCounsel bit)

Insert Into @AllResponsibleDefenseCounsel
Select dcs.DefenseCounselAddressID,
	d.DefenseCounsel as DefenseCounsel,
	dcs.responsibledefensecounselAddressID,
	StateID as StateID,
	r.DefenseCOunsel as ResponsibleDefenseCOunsel,
	IsActiveCounsel
From tblDefenseCounselState dcs 
	Inner join tblDefenseCounselAddress da on da.DefenseCounselAddressID = dcs.defensecounselAddressID
	Inner Join tblDefenseCounsel d on d.defensecounselID = da.defensecounselID
	INNER JOIN tblDefenseCounselAddress dar on dar.DefenseCounselAddressID = dcs.responsibledefensecounseladdressid
	Inner Join tblDefenseCounsel r on r.defensecounselID = dar.defensecounselID
Group By d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCOunseladdressID, dcs.responsibledefensecounseladdressID, IsActiveCounsel, StateID

IF @ClaimantPersonalInfoID IS NULL
BEGIN
	SELECT CASE WHEN m.LawsuitID = 50513 THEN ' CLAIMANT NOT FOUND'
		ELSE cpi.LastName + ', ' + cpi.FirstName END AS Claimant,
		cpi.ClaimantPersonalInfoID, FirstName, LastName
	FROM dbo.tblLawsuit m
		INNER JOIN @AllResponsibleDefenseCounsel dc ON m.StateID = dc.StateID AND m.DefenseCounselAddressID = dc.DefenseCounselAddressID		
		INNER JOIN tblClaimLawsuit cl ON m.LawsuitID = cl.LawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	GROUP BY CASE WHEN m.LawsuitID = 50513 THEN ' CLAIMANT NOT FOUND'
		ELSE cpi.LastName + ', ' + cpi.FirstName END,
		cpi.ClaimantPersonalInfoID, FirstName, LastName
	ORDER BY Claimant
END

ELSE BEGIN
	SELECT CASE WHEN m.LawsuitID = 50513 THEN ' CLAIMANT NOT FOUND'
		ELSE cpi.LastName + ', ' + cpi.FirstName END AS Claimant,
		cpi.ClaimantPersonalInfoID, FirstName, LastName
	FROM dbo.tblLawsuit m
		LEFT JOIN dbo.tblDefenseCounselState s ON m.DefenseCounselAddressID = s.DefenseCounselAddressID 
		INNER JOIN tblClaimLawsuit cl ON m.LawsuitID = cl.LawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	GROUP BY CASE WHEN m.LawsuitID = 50513 THEN ' CLAIMANT NOT FOUND'
		ELSE cpi.LastName + ', ' + cpi.FirstName END,
		cpi.ClaimantPersonalInfoID, FirstName, LastName
	ORDER BY Claimant
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantInfo] TO [power_user]
GO
