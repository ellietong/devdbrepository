SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryDelClaimantRelatedParty]
(@ClaimantRelatedPartyID int, @Emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE tblClaimantRelatedParty
	SET SPUserName = @emailaddress
	WHERE ClaimantRelatedPartyID = @ClaimantRelatedPartyID
    -- Insert statements for procedure here
	DELETE FROM tblClaimantRelatedParty
	WHERE ClaimantRelatedPartyID = @ClaimantRelatedPartyID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDelClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDelClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDelClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDelClaimantRelatedParty] TO [power_user]
GO
