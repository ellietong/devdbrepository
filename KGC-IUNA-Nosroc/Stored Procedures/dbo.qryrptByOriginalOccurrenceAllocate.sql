SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create Procedure [dbo].[qryrptByOriginalOccurrenceAllocate] (@allocationID int)
As
Set NoCount On

Select ar.OccurrenceID, Cost1, Cost3, Sum(AllocationDefense), Sum(AllocationIndemnity), TriggerStart1, TriggerEnd1
From tblAllocationResults ar
Inner Join tblOccurrence o on ar.OccurrenceID = o.OccurrenceID
Where AllocationID = 231
Group By ar.OccurrenceID, Cost1, Cost3, TriggerStart1, TriggerEnd1
Having Round(Sum(AllocationDefense+AllocationDefenseOutsideLimits),0) <> Round(Cost1,0) or Round(Sum(AllocationIndemnity),0) <> Round(Cost3,0)

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptByOriginalOccurrenceAllocate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptByOriginalOccurrenceAllocate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptByOriginalOccurrenceAllocate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptByOriginalOccurrenceAllocate] TO [power_user]
GO
