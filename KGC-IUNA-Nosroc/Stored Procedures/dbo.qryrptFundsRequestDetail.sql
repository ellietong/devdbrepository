SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptFundsRequestDetail] (@Matter int, @InvoiceTypeID int)

As

SET NOCOUNT ON

--DECLARE @InvoiceTypeID int
--SET @InvoiceTypeID = NULL

--DECLARE @Matter int
--SET @Matter = 1

DECLARE @maxInvoiceTypeID int
SELECT @maxInvoiceTypeID = MAX(InvoiceTypeID) FROM tblDefenseInvoice
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblDefenseInvoice

Declare @PaidInvoices table (DefenseInvoiceID bigint)
insert INto @PaidInvoices
Select DefenseInvoiceID
From tblPaymentDefense pd
Inner Join tblCheckApproval ca on pd.CheckID = ca.CheckID
Where IsFullyPaid = 1

SELECT	Matter, DI.MatterID, Firm, PayTo, InvoiceNumber, Period, InvoiceTotal,SubmitDate,DatePayable, FeeAmount,
	DisbursementAmount, Adjustment, DI.StatusID, Status,
CASE	When (FeeAmount is null AND DisbursementAmount is null) or (FeeAmount+DisbursementAmount=0) Then InvoiceTotal
	Else FeeAmount+DisbursementAmount
	End as Subtotal,
CASE	When (FeeAmount is null AND DisbursementAmount is null) or (FeeAmount+DisbursementAmount=0) Then InvoiceTotal
	Else FeeAmount+DisbursementAmount-Adjustment
	End as AmountPayable,
	InvoiceType
FROM	tblDefenseInvoice DI
INNER JOIN	tblDefenseInvoiceFirm DIF ON DI.FirmID = DIF.FirmID
INNER JOIN tblMatter M ON DI.MatterID = M.MatterID
INNER JOIN	tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
LEFT JOIN tblDefenseInvoiceType DIT ON DI.InvoiceTypeID = DIT.InvoiceTypeID
WHERE	((DI.DefenseInvoiceID not IN (Select DefenseInvoiceID From @PaidInvoices)) AND DI.DatePayable <= (SELECT CONVERT(datetime, CONVERT(varchar, GETDATE(), 101), 101) + 30 AS OneMonthOut)
	AND (DIS.StatusID IN (1, 2)) AND (DI.SubmitDate IS NOT NULL))
	AND DI.MatterID >= COALESCE(@Matter,0) AND DI.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
	AND DI.InvoiceTypeID >= COALESCE(@InvoiceTypeID,0) AND DI.InvoiceTypeID <= COALESCE(NULLIF(@InvoiceTypeID,0),@maxInvoiceTypeID)
ORDER BY Firm, Status, DatePayable

/* 
-- RUN THIS AFTER SENDING DEFENSE INVOICE
BEGIN TRAN

Update mBtblDefenseInvoice
Set FundsRequestDate=CONVERT(datetime, CONVERT(varchar, GETDATE(), 101))
WHERE	(CheckNumber IS NULL) AND DatePayable <= (SELECT CONVERT(datetime, CONVERT(varchar, GETDATE(), 101), 101) + 30 AS OneMonthOut)
	AND (StatusID IN (1, 2)) AND (SubmitDate IS NOT NULL) and FundsRequestDate is null
ROLLBACK TRAN
COMMIT TRAN
*/

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequestDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequestDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequestDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequestDetail] TO [power_user]
GO
