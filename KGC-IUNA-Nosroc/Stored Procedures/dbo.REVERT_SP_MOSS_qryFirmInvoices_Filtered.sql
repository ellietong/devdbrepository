SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[REVERT_SP_MOSS_qryFirmInvoices_Filtered](@emailaddress nvarchar (1000)=null, @FilterStatusID int, @FilterMatterID int, @FilterFirmID int)
AS
--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'crookc@kcicllc.com'
--DECLARE @FilterStatusID int
--DECLARE @FilterDocumentTypeID int
--DECLARE @FilterFirmID int

--SET @FilterStatusID = null
--SET @FilterDocumentTypeID = null
--SET @FilterFirmID = null

DECLARE @firmid int
SET @firmid = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

DECLARE @MaxStatusID int
SET @MaxStatusID = (SELECT MAX(StatusID) FROM tblDefenseInvoiceStatus)

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @MaxFirmID int
SET @MaxFirmID = (SELECT MAX(DefenseCounselID) FROM tblDefenseCounsel)


IF ((@emailaddress = 'cjs@wlbdeflaw.com') OR (RIGHT(@emailaddress,11) = 'kcicllc.com')) 
BEGIN
	SELECT CAST(d.DocumentID as varchar(50)) as DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm, 				
		m.Matter,
		di.DatePaid, 
		di.CheckNumber,
		d.FirmID,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal,
		SourceDate
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
		LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
	WHERE dt.DocumentGroupID = 2
	AND d.MatterID >= COALESCE(@FilterMatterID, 0) AND d.MatterID <= COALESCE(@FilterMatterID, @maxMatterID)
		AND d.FirmID >= COALESCE(@FilterFirmID, 0) AND d.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
		AND COALESCE(d.StatusID, 0) >= COALESCE(@FilterStatusID, 0) AND COALESCE(d.StatusID, 0) <= COALESCE(@FilterStatusID, @MaxStatusID)
	ORDER BY Accepted ASC, DateCreated DESC
END 
ELSE IF (@emailaddress = 'j.heenan@envirosafeservices.com')
BEGIN
	SELECT 
		cast(d.DocumentID as varchar(50)) as DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm, 				
		m.Matter,
		di.DatePaid, 
		di.CheckNumber,
		d.FirmID,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal,
		SourceDate
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
		LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
	WHERE dg.DocumentGroupID = 2
		AND d.FirmID = 38
			AND d.MatterID >= COALESCE(@FilterMatterID, 0) AND d.MatterID <= COALESCE(@FilterMatterID, @maxMatterID)
		AND d.FirmID >= COALESCE(@FilterFirmID, 0) AND d.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
		AND COALESCE(d.StatusID, 0) >= COALESCE(@FilterStatusID, 0) AND COALESCE(d.StatusID, 0) <= COALESCE(@FilterStatusID, @MaxStatusID)
	ORDER BY Accepted ASC, DateCreated DESC
END 
ELSE BEGIN
	SELECT 
		cast(d.DocumentID as varchar(50)) as DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm, 				
		m.Matter,
		di.DatePaid, 
		di.CheckNumber,
		d.FirmID,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal,
		SourceDate
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
		LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
	WHERE dg.DocumentGroupID = 2
		AND f.FirmID = @firmid
			AND d.MatterID >= COALESCE(@FilterMatterID, 0) AND d.MatterID <= COALESCE(@FilterMatterID, @maxMatterID)
		AND d.FirmID >= COALESCE(@FilterFirmID, 0) AND d.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
		AND COALESCE(d.StatusID, 0) >= COALESCE(@FilterStatusID, 0) AND COALESCE(d.StatusID, 0) <= COALESCE(@FilterStatusID, @MaxStatusID)
	ORDER BY Accepted ASC, DateCreated DESC
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices_Filtered] TO [power_user]
GO
