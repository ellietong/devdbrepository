SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qrysfrmDefenseCounselAddressState]
(@DefCounselID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @MaxDCID int
	DECLARE @MinDCID int
	SET @MaxDCID = (SELECT MAX(DefenseCounselID) FROM tblDefenseCounselAddress)
    SET @MinDCID = (SELECT MIN(DefenseCounselID) FROM tblDefenseCounselAddress)
    
    SELECT DefenseCounselStateID, DefenseCounselAddressID, ResponsibleDefenseCounselAddressID, dcs.StateID
    FROM tblDefenseCounselState dcs
    INNER JOIN tblState s ON dcs.StateID = s.StateID
    WHERE DefenseCounselID >= COALESCE(@DefCounselID, @MinDCID) AND DefenseCounselID <= COALESCE(@DefCounselID, @MaxDCID)
	ORDER BY s.[State]
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDefenseCounselAddressState] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmDefenseCounselAddressState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDefenseCounselAddressState] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmDefenseCounselAddressState] TO [power_user]
GO
