SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantPersonalInfoFiltered] (@ClaimantID bigint)
as
set nocount on

--Declare @ClaimantID bigint
--set @ClaimantID = 69398


SELECT *
FROM tblClaimantPersonalInfo
Where ClaimantPersonalInfoID = @ClaimantID







GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantPersonalInfoFiltered] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantPersonalInfoFiltered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantPersonalInfoFiltered] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantPersonalInfoFiltered] TO [power_user]
GO
