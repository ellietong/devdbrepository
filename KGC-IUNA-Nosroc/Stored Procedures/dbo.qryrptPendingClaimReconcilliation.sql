SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryrptPendingClaimReconcilliation]
(@StartDate datetime, @EndDate datetime, @MatterID int)
AS
BEGIN

SET NOCOUNT ON
--DECLARE @MatterID int
--SET @MatterID = 0
--DECLARE @StartDate datetime
--SET @StartDate = '9/1/2010'
--DECLARE @EndDate datetime
--SET @EndDate = '9/30/2010'

DECLARE @OpenClaimsPendingBeginningCount table (openPending int, matterID int)
DECLARE @ServedCount table (served int, matterID int)
DECLARE @DismissedCount table (dismissed int, matterID int)
DECLARE @SettledCount table (settled int, matterID int)

if @MatterID = 0 set @MatterID = null

DECLARE @MaxMatter int
DECLARE @MinMatter int
SET @MaxMatter = (SELECT MAX(MatterID) FROM tblMatter)
SET @MinMatter = (SELECT MIN(MatterID) FROM tblMatter)	


	
--Get beginning open claims pending balance
INSERT INTO @OpenClaimsPendingBeginningCount 
SELECT COALESCE(COUNT(DISTINCT cl.claimid), 0) AS Pending, c.matterID
FROM tblClaimLawsuit cl
Left Join tblLawsuit l on cl.LawsuitID = l.LawsuitID
Left Join tblClaimLawsuitStatus cls on cl.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
left join tblClaim c on c.ClaimID = cl.ClaimID
WHERE IsPrimaryForClaim = 1 and (cl.ClaimLawsuitStatusID  in  (22) and (coalesce(FileDate,ServiceDate,StatusDate) < @StartDate or ServiceDate is null))
and c.matterid <= COALESCE(@MatterID, @MaxMatter) AND c.MatterID >= COALESCE(@MatterID, @MinMatter)
GROUP BY c.MatterID



--Get count of claims served between start date and end date
INSERT INTO @ServedCount 
SELECT COALESCE(COUNT(DISTINCT c.claimid), 0) AS NewFilings, c.MatterID
from tblClaim c 	
Left JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID	
Left JOIN tblLawsuit l on cl.LawsuitID = l.LawsuitID	
where isprimaryforclaim = 1 and coalesce(servicedate, filedate, statusdate) > @StartDate  and coalesce(servicedate, filedate, statusdate)<= @EndDate
and matterid <= COALESCE(@MatterID, @MaxMatter) AND MatterID >= COALESCE(@MatterID, @MinMatter)
GROUP BY c.MatterID



--Get count of claims dismissed between start and end date
INSERT INTO @DismissedCount
SELECT COALESCE(COUNT(DISTINCT cl.claimid), 0)* -1 AS Dismissed, c.MatterID
FROM tblClaimLawsuit cl	
Left Join tblLawsuit l on cl.LawsuitID = l.LawsuitID	
Left Join tblClaimLawsuitStatus cls on cl.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID	
Left Join tblClaim c on c.ClaimID = cl.ClaimID
WHERE cl.ClaimLawsuitStatusID  in  (15,16,27) and IsPrimaryForClaim = 1 and 	
	StatusDate >= @StartDate and StatusDate <= @EndDate
	and matterid <= COALESCE(@MatterID, @MaxMatter) AND MatterID >= COALESCE(@MatterID, @MinMatter)
GROUP BY c.MatterID



--Get count of claims settled between start and end date
INSERT INTO  @SettledCount
SELECT  COALESCE(COUNT(DISTINCT cl.claimid), 0)*-1 AS Settled, c.MatterID
FROM tblClaimLawsuit cl	
left join tblClaim c on c.ClaimID = cl.ClaimID
Left Join tblClaimantSettlement cs on cl.ClaimLawsuitID=cs.ClaimLawsuitID	
Left Join tblClaimLawsuitStatus cls on cl.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID	
WHERE cl.ClaimLawsuitStatusID  in  (23, 28) and IsPrimaryForClaim = 1
	and SettlementDate > = @StartDate
	and SettlementDate < = @EndDate
	and matterid <= COALESCE(@MatterID, @MaxMatter) AND MatterID >= COALESCE(@MatterID, @MinMatter)
GROUP BY c.MatterID




--Get all open claims up to end date
	
SELECT COALESCE(openPending, 0) AS OpenPending, COALESCE(served, 0) AS Served, COALESCE(dismissed, 0) AS Dismissed, COALESCE(settled, 0) AS Settled, 
COALESCE(openPending, 0)+COALESCE(served, 0)+COALESCE(dismissed, 0)+COALESCE(settled, 0) AS endPending, op.matterID, m.matter, @StartDate AS StartDate, @EndDate AS EndDate
FROM @OpenClaimsPendingBeginningCount op
LEFT JOIN @ServedCount s ON op.matterID = s.matterID
LEFT JOIN @DismissedCount d ON op.matterID = d.matterID
LEFT JOIN @SettledCount sc ON op.matterID = sc.matterID
LEFT JOIN tblMatter m ON op.matterID = m.MatterID
return
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimReconcilliation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimReconcilliation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimReconcilliation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimReconcilliation] TO [power_user]
GO
