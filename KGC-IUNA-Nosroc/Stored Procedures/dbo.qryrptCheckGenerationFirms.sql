SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryrptCheckGenerationFirms](@Matter int, @FirmID int, @CheckNo int)
--@FundsRequestStartDate datetime, @FundsRequestEndDate datetime,
As

SET NOCOUNT ON

--Declare @FundsRequestStartDate datetime
--Declare @FundsRequestEndDate datetime
--DECLARE @Matter int
--DECLARE @FirmID int
--Declare @CheckNo int
--SET @checkno = 0
--SET @Matter = 1
--Set @FirmID = 0
--SET @FundsRequestStartDate = '12/1/2009'
--SET @FundsRequestEndDate = '12/1/2009'

DECLARE @maxMatterID int
DECLARE @maxFirmID int
Declare @MaxCheckNo int
SELECT @maxMatterID = MAX(matterid) from tblMatter
SELECT @maxFirmID = MAX(firmID) from tblDefenseInvoiceFirm
SELECT @MaxCheckNo = MAX(CheckNo) from tblChecks

SELECT Matter, m.matterid,
c.FirmID,Firm, PayTo, FirmAddress,C.CheckNo, C.AmountDesc, C.checkAmount, C.CheckDate,c.CommentLine,
C.PlaintiffCounselName, C.PCounselAddress, c.FundTypeID, c.SettlementDetails
FROM	 tblChecks C 
Left JOIN tblDefenseInvoiceFirm DIF ON C.FirmID = DIF.FirmID
left Join tblDefenseInvoice DI on C.firmID = DI.FirmID and C.MatterID = DI.MatterID
LEFT JOIN tblDefenseInvoiceStatus DIS ON Di.StatusID = DIS.StatusID
LEFT JOIN tblMatter M on C.MatterID=M.MatterID

Where 
--FundsRequestDate Between @FundsRequestStartDate and @FundsRequestEndDate
c.MatterID >= COALESCE(@Matter,0) AND c.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
AND c.FirmID >= COALESCE(@FirmID,0) AND c.FirmID <= COALESCE(NULLIF(@FirmID,0),@MaxFirmID)
--AND di.CheckNumber>= COALESCE(@checkno,0) AND di.CheckNumber <= COALESCE(NULLIF(@CheckNo,0),@MaxCheckNo)
AND c.CheckNo>= COALESCE(@checkno,0) AND C.CheckNo <= COALESCE(NULLIF(@CheckNo,0),@MaxCheckNo)
Group by Matter,m.MatterID, 
c.FirmID,Firm, PayTo, FirmAddress,C.CheckNo, C.AmountDesc, C.checkAmount, C.CheckDate,c.CommentLine,
C.PlaintiffCounselName, C.PCounselAddress, c.FundTypeID, c.SettlementDetails
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGenerationFirms] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckGenerationFirms] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptCheckGenerationFirms] TO [CheckWriter_User]
GRANT CONTROL ON  [dbo].[qryrptCheckGenerationFirms] TO [CheckWriter_User]
GRANT TAKE OWNERSHIP ON  [dbo].[qryrptCheckGenerationFirms] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGenerationFirms] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckGenerationFirms] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGenerationFirms] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckGenerationFirms] TO [power_user]
GO
