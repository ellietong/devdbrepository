SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--USE [KGC-IUNA-Nosroc]
--GO
--/****** Object:  StoredProcedure [blairo].[qryrptOutstandingSettlements]    Script Date: 03/13/2012 10:01:55 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON GO
CREATE Proc [dbo].[qryrptOutstandingSettlements_1]  (@Settlementgroup int)

As

SET NOCOUNT ON

--DECLARE @Settlementgroup int
--SET @Settlementgroup =6857


SELECT claimlawsuitstatus, statusdate ,c.claimantpersonalinfoid as CPI,pl.PlaintiffCounsel, disease, MedicareFormRcvd, MedicareFormSent, 
 LastName, FirstName, MiddleName,socialsecuritynumber,birthdate,diseasediagnosisdate, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,cl.lawsuitid,
	cs.SettlementAmount, matter,cs.SettlementDate  AS SettlementDateAsOf,  DocketNumbers,  dbo.fnLatestExposure(cpi.claimantpersonalinfoid)ExposureEnd ,
	CASE WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 1 THEN 'Unknown' WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 2 THEN 'Yes - ' + COALESCE(HealthInsuranceClaimNumber, 'HICN Not Provided')WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 3 THEN 'Non-Beneficiary' END AS HICN
FROM tblClaim c
	
	 JOIN tblClaimantPersonalInfo cpi	ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
	iNNER JOIN tblClaimLawsuit cl	ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INner JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pl	ON l.PlaintiffCounselAddressID = pl.PlaintiffCounselAddressID
LEFT join tblClaimLawsuitStatusTracking st on st.ClaimLawsuitID =cl.ClaimLawsuitID 	
INNER JOIN tblClaimLawsuitStatus cls	
	ON st.ClaimLawsuitstatusID = cls.ClaimLawsuitstatusID	
left join tblclaimlawsuitstatusgroup csg on csg.claimlawsuitstatusid = st.ClaimLawsuitStatusID
	LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblClaimantDocketNumbers cd	ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
LEFT join tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
	LEFT JOIN tblClaimDisease cdi				 ON cdi.ClaimID = c.ClaimID
	LEFT JOIN tblDisease d on d.DiseaseID =cdi.DiseaseID
WHERE s.SettlementGroupID  =@Settlementgroup
	AND COALESCE(IsCurrentClaim,0) = 1 and coalesce(st.isprimarystatus,0) = 1
	AND statusgroupid=2 and PrimaryDisease =1
GROUP BY claimlawsuitstatus,MedicareFormRcvd, MedicareFormSent, statusdate,cs.SettlementDate,  LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, Matter,pl.PlaintiffCounsel,cpi.claimantpersonalinfoid,
	 cl.LawsuitID, DocketNumbers,  spd.MedicareFormRcvd,Disease, DiseaseDiagnosisDate,socialsecuritynumber,birthdate,
CASE WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 1 THEN 'Unknown' WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 2 THEN 'Yes - ' + COALESCE(HealthInsuranceClaimNumber, 'HICN Not Provided')WHEN COALESCE(cpi.MedicareBeneficiaryStatusID, 1) = 3 THEN 'Non-Beneficiary' END,
CASE WHEN spd.MedicareFormRcvd IS not null THEN 'Yes' ELSE 'No' END, case when MedicareFullyDocumented = 1 then 'Yes' Else 'No' end,
	CONVERT(nvarchar(max),COALESCE(dbo.fnEarliestExposure(cpi.ClaimantPersonalInfoID),'N/A'),101), 
	CASE WHEN COALESCE(DiseaseDiagnosisDate,'1/1/2053') <= COALESCE(FileDate,'1/1/2051') and COALESCE(DiseaseDiagnosisDate,'1/1/2053') <= COALESCE(DeceasedDate,'1/1/2052')
		 THEN CONVERT(nvarchar(max),DiseaseDiagnosisDate,101)
		 WHEN COALESCE(FileDate,'1/1/2053') <= COALESCE(DiseaseDiagnosisDate,'1/1/2051') and COALESCE(FileDate,'1/1/2053') <= COALESCE(DeceasedDate,'1/1/2052')
		 THEN CONVERT(nvarchar(max),FileDate,101)
		 WHEN COALESCE(DeceasedDate,'1/1/2053') <= COALESCE(DiseaseDiagnosisDate,'1/1/2051') and COALESCE(DeceasedDate,'1/1/2053') <= COALESCE(FileDate,'1/1/2052')
		 THEN CONVERT(nvarchar(max),DeceasedDate,101)
		 ELSE 'N/A' END,
CONVERT(nvarchar(max), COALESCE(dbo.fnLatestExposure(cpi.ClaimantPersonalInfoID),'N/A'),101)
ORDER BY --MaxSettlementDate, SettlementGroup, SettlementDate, 
LastName
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlements_1] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlements_1] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlements_1] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlements_1] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlements_1] TO [power_user]
GO
