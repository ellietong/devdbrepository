SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Excel] (@StateID int, 
																	   @StartDate datetime, 
																	   @EndDate datetime, 
																	   @Matter int)
AS
SET NOCOUNT ON

----Variables used for testing.
--DECLARE @StartDate datetime
--DECLARE @EndDate   datetime
--DECLARE @StateID   int
--DECLARE @Matter    int
--SET @StartDate = '1/1/1900'
--SET @EndDate   = '3/1/2012'
--SET @StateID   = null
--SET @Matter    = 1

Declare @MaxStateID int
Select @MaxStateID = MAX(stateid) from tblState

Declare @MaxResponsibleDefenseCounselID int

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @DiseaseCount TABLE (Matter           nvarchar(100), 
							 MatterID         int, 
							 State            nvarchar(150), 
							 YearFiled        datetime,
							 statusdate       datetime, 
							 MesoCount        int, 
							 LungCancerCount  int, 
							 OtherCancerCount int, 
							 NonMaligCount    int,  
							 UnknownCount     int, 
							 UnknownCount1    int, 
							 SilicosisCount   int, 
							 NoDiseaseCount   int)
INSERT INTO @DiseaseCount
	SELECT Matter, 
		C.MatterID, 
		FullNameState AS state,  
		case when serviceDate IS NULL THEN  '1/1/1900' ELSE CONVERT(datetime,('1/1/'+CONVERT(nvarchar,YEAR(serviceDate)))) END AS YearFiled,
		coalesce(settlementdate,statusdate,'1/1/1900'),
		CASE WHEN DMeso.HighLevelDiseaseID = 1     THEN 1 ELSE 0 END AS MesoCount,        --Meso
		CASE WHEN   DLC.HighLevelDiseaseID = 2     THEN 1 ELSE 0 END AS LungCancerCount,  --Lung Cancer
		CASE WHEN   DOC.HighLevelDiseaseID = 3     THEN 1 ELSE 0 END AS OtherCancerCount, --Other Cancer
		CASE WHEN   DNM.HighLevelDiseaseID = 4     THEN 1 ELSE 0 END AS NonMaligCount,    --NonMalig
		CASE WHEN  DUnk.HighLevelDiseaseID = 5     THEN 1 ELSE 0 END AS UnknownCount,     --Unknown
		CASE WHEN   DND.HighLevelDiseaseID = 6     THEN 1 ELSE 0 END AS NoDiseaseCount,   --No Disease
		CASE WHEN   DSC.HighLevelDiseaseID = 7     THEN 1 ELSE 0 END AS SilicosisCount,   --Silicosis
		CASE WHEN  DUnk.HighLevelDiseaseID IS NULL THEN 1 ELSE 0 End AS UnknownCount1     --Null
	FROM tblClaimantPersonalInfo CPI
		LEFT JOIN tblClaim C                         ON CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
		LEFT JOIN (Select CD.ClaimDiseaseID, 
					   CD.ClaimID, 
					   CD.DiseaseDiagnosisDate, 
					   CD.DiseaseID, 
					   CD.DoctorID, 
					   CD.PrimaryDisease 
				   From tblClaimDisease CD 
				   Where PrimaryDisease = 1) CD      ON CD.ClaimID=C.ClaimID
		LEFT JOIN tblMatter M                        ON c.MatterID = M.MatterID
		LEFT JOIN tblDisease DMeso                   ON CD.DiseaseID=DMeso.DiseaseID --Meso
		LEFT JOIN tblDisease DLC                     ON CD.DiseaseID=DLC.DiseaseID   --Lung Cancer
		LEFT JOIN tblDisease DOC                     ON CD.DiseaseID=DOC.DiseaseID   --Other Cancer
		LEFT JOIN tblDisease DNM                     ON CD.DiseaseID=DNM.DiseaseID   --Non-Malig
		LEFT JOIN tblDisease DSC                     ON CD.DiseaseID=DSC.DiseaseID   --Silicosis
		LEFT JOIN tblDisease DND                     ON CD.DiseaseID=DND.DiseaseID   --No Disease
		LEFT JOIN tblDisease DUnk                    ON CD.DiseaseID=DUnk.DiseaseID  --Unknown
		LEFT JOIN tblClaimLawsuit CL		         ON C.ClaimID=CL.ClaimID
		LEFT JOIN tblLawsuit L				         ON CL.LawsuitID=L.LawsuitID
		LEFT JOIN tblState S				         ON L.StateID=S.StateID
		LEFT JOIN tblClaimantSettlement cs	         ON cs.ClaimLawsuitID = CL.ClaimLawsuitID
		LEFT JOIN tblClaimSequence csq		         ON c.ClaimSequenceID = csq.ClaimSequenceID
		LEFT JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID=CL.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls          ON cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		LEFT JOIN tblClaimLawsuitStatusGroup clsg    ON clsg.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
	WHERE s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) 
		AND IsPrimaryForClaim = 1
		AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
		AND coalesce(clst.isprimarystatus,0)=1
		AND ((clsg.StatusGroupID=1 and coalesce(servicedate,filedate,'1/1/1900')<=@EndDate) or (StatusGroupID in (2,3) and ProcessedDate>@EndDate))
		AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
		AND COALESCE(cpi.jobsiteinfo, 0) = 0
	ORDER BY State, YEAR(serviceDate)

SELECT State,
	CASE WHEN YEAR(YearFiled) = 1900 THEN 'N/A' ELSE CONVERT(nvarchar(4),YEAR(YearFiled)) END AS 'Service Year',
	SUM(MesoCount)                                     AS 'Mesothelioma',
	SUM(LungCancerCount)                               AS 'Lung Cancer', 
	SUM(OtherCancerCount)                              AS 'Other Cancer',
	SUM(NonMaligCount + SilicosisCount)                AS 'Non-Malignant', 
	SUM(UnknownCount + UnknownCount1 + NoDiseaseCount) AS 'Unknown',
	SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + SilicosisCount + UnknownCount + UnknownCount1 + NoDiseaseCount) AS 'Total'
FROM @DiseaseCount
WHERE YearFiled BETWEEN @StartDate AND @EndDate  
GROUP BY Matter, MatterID, State, YearFiled
ORDER BY Matter, State, CASE WHEN YEAR(YearFiled) = 1900 THEN '0' ELSE CONVERT(nvarchar(4),YEAR(YearFiled)) END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Excel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Excel] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Excel] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Excel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPendingClaimsCountByJurisdictionByYear_Excel] TO [power_user]
GO
