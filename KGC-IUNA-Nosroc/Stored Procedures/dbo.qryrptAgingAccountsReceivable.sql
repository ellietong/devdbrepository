SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAgingAccountsReceivable] (@Matter int, @BillDate datetime)
AS
Set NoCount On



--Declare @Matter int
--Set @Matter = 2
--Declare @BillDate datetime
--Set @BillDate = '9/1/2009'

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @ledger1 table (LedgerID int, CarrierID int, SubscriberID int, Allocation money, MatterID int)
Insert Into @Ledger1
Select LedgerID, CarrierID, SubscriberID, coalesce(sum(Allocation),0), MatterID
From tblLedger 
Group By LedgerID, CarrierID, SubscriberID,  BillDate, MatterID
Having @BillDate-billdate<=30  and @BillDate-billdate >= 0

Declare @ledger2 table (LedgerID int, CarrierID int, SubscriberID int, Allocation money, MatterID int)
Insert Into @Ledger2
Select LedgerID, CarrierID, SubscriberID, coalesce(sum(Allocation),0), MatterID
From tblLedger
Group By LedgerID, CarrierID, SubscriberID,  BillDate, MatterID
Having @BillDate-billdate>30 and @BillDate-billdate<=60

Declare @ledger3 table (LedgerID int, CarrierID int, SubscriberID int, Allocation money, MatterID int)
Insert Into @Ledger3
Select LedgerID, CarrierID, SubscriberID,coalesce(sum(Allocation),0), MatterID
From tblLedger 
Group By LedgerID, CarrierID, SubscriberID, BillDate, MatterID
Having @BillDate-billdate>60 and @BillDate-billdate<=90

Declare @ledger4 table (LedgerID int, CarrierID int, SubscriberID int, Allocation money, MatterID int)
Insert Into @Ledger4
Select LedgerID, CarrierID, SubscriberID, coalesce(sum(Allocation),0), MatterID
From tblLedger 
Group By LedgerID, CarrierID, SubscriberID, BillDate, MatterID
Having @BillDate-billdate>90

Declare @payment table (LedgerID int, Payment money)
Insert Into @payment
Select LedgerID, coalesce(sum(payment),0)
From tblPayment 
Group By LedgerID,PaymentDate
having @BillDate-PaymentDate >= 0

Declare @AR1 Table (CarrierID int, SubscriberID int, MatterID int, AccountReceivable money)
Insert Into @AR1
Select CarrierID, SubscriberID, MatterID, Sum(Coalesce(L.Allocation,0)-coalesce(P.payment,0))
From @Ledger1 L 
Left Join @Payment P on L.LedgerID=P.LedgerID 
Group By CarrierID, SubscriberID, MatterID

Declare @AR2 Table (CarrierID int, SubscriberID int, MatterID int, AccountReceivable money)
Insert Into @AR2
Select CarrierID, SubscriberID, MatterID, Sum(Coalesce(L.Allocation,0)-coalesce(P.payment,0))
From @Ledger2 L
Left Join @Payment P on L.LedgerID=P.LedgerID 
Group By CarrierID, SubscriberID, MatterID

Declare @AR3 Table (CarrierID int, SubscriberID int, MatterID int, AccountReceivable money)
Insert Into @AR3
Select CarrierID, SubscriberID, MatterID, Sum(Coalesce(L.Allocation,0)-coalesce(P.payment,0))
From @Ledger3 L
Left Join @Payment P on L.LedgerID=P.LedgerID 
Group By CarrierID, SubscriberID, MatterID

Declare @AR4 Table (CarrierID int, SubscriberID int, MatterID int, AccountReceivable money)
Insert Into @AR4
Select CarrierID, SubscriberID, MatterID, Sum(Coalesce(L.Allocation,0)-coalesce(P.payment,0))
From @Ledger4 L
Left Join @Payment P on L.LedgerID=P.LedgerID 
Group By CarrierID, SubscriberID, MatterID

Declare @Final table (OrderID int, OrderName nvarchar(100), CarrierName nvarchar(2000), AccountReceivable1 money, AccountReceivable2 money, AccountReceivable3 money, AccountReceivable4 money,
MatterID int)
Insert Into @Final
Select 1 as OrderID, 'Domestic' as OrderName, CarrierName, AR1.AccountReceivable, AR2.AccountReceivable, AR3.AccountReceivable,
      AR4.AccountReceivable, COALESCE(AR1.MatterID, AR2.MatterID, AR3.MatterID, AR4.MatterID)
From tblCarrier C
Full Outer Join @AR1 AR1 on C.CarrierID=AR1.CarrierID
Full Outer Join @AR2 AR2 on C.CarrierID=AR2.CarrierID
Full Outer Join @AR3 AR3 on C.CarrierID=AR3.CarrierID
Full Outer Join @AR4 AR4 on C.CarrierID=AR4.CarrierID
Where (AR1.AccountReceivable <> 0 or AR2.AccountReceivable <> 0 or AR3.AccountReceivable <> 0 or
      AR4.AccountReceivable <> 0) and C.carrierID not in (10,20001,20002,20003)--20
      
--Union

--Select 2 as OrderID, 'London Solvent' as OrderName, SubscriptionCompany as CarrierName, AR1.AccountReceivable, AR2.AccountReceivable, AR3.AccountReceivable,
--      AR4.AccountReceivable, COALESCE(AR1.MatterID, AR2.MatterID, AR3.MatterID, AR4.MatterID)
--From tblPolicy p
--Inner Join tblSubscriptionCoverage s on p.PolicyID=s.PolicyID
--Inner Join tblSubscriberCompany sc on s.SubscriberID=sc.SubscriberID
--Full Outer Join @AR1 AR1 on sc.SubscriberID=AR1.SubscriberID
--Full Outer Join @AR2 AR2 on sC.SubscriberID=AR2.SubscriberID
--Full Outer Join @AR3 AR3 on sC.SubscriberID=AR3.SubscriberID
--Full Outer Join @AR4 AR4 on sC.SubscriberID=AR4.SubscriberID
--Where (AR1.AccountReceivable <> 0 or AR2.AccountReceivable <> 0 or AR3.AccountReceivable <> 0 or
--      AR4.AccountReceivable <> 0) and p.carrierID in (20) 
--      and sc.SubscriptionCompanySolvency LIKE 'Solvent'
      
--Union

--Select 3 as OrderID, 'London Insolvent' as OrderName, SubscriptionCompany as CarrierName, AR1.AccountReceivable, AR2.AccountReceivable, AR3.AccountReceivable,
--      AR4.AccountReceivable, COALESCE(AR1.MatterID, AR2.MatterID, AR3.MatterID, AR4.MatterID)
--From tblPolicy p
--Inner Join tblSubscriptionCoverage s on p.PolicyID=s.PolicyID
--Inner Join tblSubscriberCompany sc on s.SubscriberID=sc.SubscriberID
--Full Outer Join @AR1 AR1 on sc.SubscriberID=AR1.SubscriberID
--Full Outer Join @AR2 AR2 on sC.SubscriberID=AR2.SubscriberID
--Full Outer Join @AR3 AR3 on sC.SubscriberID=AR3.SubscriberID
--Full Outer Join @AR4 AR4 on sC.SubscriberID=AR4.SubscriberID
--Where (AR1.AccountReceivable <> 0 or AR2.AccountReceivable <> 0 or AR3.AccountReceivable <> 0 or
--      AR4.AccountReceivable <> 0) and p.carrierID in (20) 
--      and sc.SubscriptionCompanySolvency LIKE 'Insolvent'

Union

Select 4 as OrderID, 'Self Insured' as OrderName, CarrierName, AR1.AccountReceivable, AR2.AccountReceivable, AR3.AccountReceivable,
      AR4.AccountReceivable, COALESCE(AR1.MatterID, AR2.MatterID, AR3.MatterID, AR4.MatterID)
From tblCarrier C
Full Outer Join @AR1 AR1 on C.CarrierID=AR1.CarrierID
Full Outer Join @AR2 AR2 on C.CarrierID=AR2.CarrierID
Full Outer Join @AR3 AR3 on C.CarrierID=AR3.CarrierID
Full Outer Join @AR4 AR4 on C.CarrierID=AR4.CarrierID
Where (AR1.AccountReceivable <> 0 or AR2.AccountReceivable <> 0 or AR3.AccountReceivable <> 0 or
      AR4.AccountReceivable <> 0) and C.carrierID = 20002
      
Union

Select 4 as OrderID, 'Self Insured' as OrderName, CarrierName, AR1.AccountReceivable, AR2.AccountReceivable, AR3.AccountReceivable,
AR4.AccountReceivable, COALESCE(AR1.MatterID, AR2.MatterID, AR3.MatterID, AR4.MatterID)
From tblCarrier C
Full Outer Join @AR1 AR1 on C.CarrierID=AR1.CarrierID
Full Outer Join @AR2 AR2 on C.CarrierID=AR2.CarrierID
Full Outer Join @AR3 AR3 on C.CarrierID=AR3.CarrierID
Full Outer Join @AR4 AR4 on C.CarrierID=AR4.CarrierID
Where (AR1.AccountReceivable <> 0 or AR2.AccountReceivable <> 0 or AR3.AccountReceivable <> 0 or
      AR4.AccountReceivable <> 0) and C.carrierID = 20003
      


Select ORderID, OrderName, CarrierName, Sum(Coalesce(AccountReceivable1,0)) as AccountReceivable1, Sum(Coalesce(AccountReceivable2,0)) as AccountReceivable2, Sum(Coalesce(AccountReceivable3,0)) as AccountReceivable3, Sum(Coalesce(AccountReceivable4,0)) as AccountReceivable4,
CASE WHEN @Matter = 1 THEN 'IUNA' WHEN @Matter = 2 THEN 'Nosroc' ELSE 'IUNA/Nosroc' END AS Matter
From @Final F
LEFT JOIN tblMatter M ON F.MatterID = M.MatterID
Where F.MatterID >= COALESCE(@Matter,0) AND F.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID)
Group By OrderID, CarrierName, OrderName
Order By Matter, OrderID, OrderName, CarrierName

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAgingAccountsReceivable] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAgingAccountsReceivable] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAgingAccountsReceivable] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAgingAccountsReceivable] TO [power_user]
GO
