SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROC [dbo].[qrysrptCorporateHistorySiteTransfer] (@siteID int)
As 
set nocount on
SELECT e.SiteID, ch.CorpHistoryID, PurchaseType, AcqSold, AcquisitionDate, SaleDate,
AcquiringCompany, AcquiredCompany, PreviousOwner, NewOwner, Notes
FROM tblEnviroSitesDetail e INNER JOIN tblSiteTransfer st
	ON e.siteid = st.siteid
INNER JOIN tblCorpHistory ch 
	ON st.corphistoryid = ch.corphistoryid
INNER JOIN tblCorpHistoryPurchaseType pt
	ON ch.purchasetypeid = pt.purchasetypeid
WHERE e.siteid = @siteid
return

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCorporateHistorySiteTransfer] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCorporateHistorySiteTransfer] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCorporateHistorySiteTransfer] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCorporateHistorySiteTransfer] TO [power_user]
GO
