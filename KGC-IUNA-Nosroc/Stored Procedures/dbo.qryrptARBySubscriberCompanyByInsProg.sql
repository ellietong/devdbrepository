SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[qryrptARBySubscriberCompanyByInsProg] 
(@SubscriberCompanyID INT = NULL, @Solvency varchar(50) = NULL, @InsuranceProgramID INT = NULL, @AllocationID INT = NULL)
AS 
SET NOCOUNT ON

--DECLARE @SubscriberCompanyID INT
--SET @SubscriberCompanyID = 0
--DECLARE @Solvency varchar(50)
--SET @Solvency = null
--DECLARE @AllocationID INT
--SET @AllocationID = 150
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID = 0

DECLARE @maxInsuranceProgramID int
DECLARE @maxSubscriberCompanyID int
DECLARE @maxAllocationID int

SELECT @maxInsuranceProgramID = MAX (InsuranceProgramID) FROM vPolicy
SELECT @maxSubscriberCompanyID = MAX (SubscriberID) FROM vSubscription
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation

DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
CarrierID int, CarrierName nvarchar(250), InsuranceProgramID int, InsuranceProgram nvarchar(100))
INSERT INTO @Policy

SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, 
InsuranceProgramID, InsuranceProgram
FROM vPolicy
GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, 
InsuranceProgramID, InsuranceProgram

SELECT pinfo.InsuranceProgram, subs.SubscriberID, subs.SubscriptionCompany, MAX(ar.DateTime) AS [DateTime], pinfo.CarrierGroupName, pinfo.CarrierName, SUM(ar.PV * subs.Percentage) AS pvamt, 
	SUM(ar.Allocation * subs.Percentage) AS AllocationAmt, SUM(ar.SIRAllocated * subs.Percentage) AS SIRAMt, a.AllocationID, a.AllocationDescription, subs.SubscriptionCompanySolvency, 
	pinfo.CarrierID, pinfo.CarrierGroupID, pinfo.InsuranceProgramID
FROM @Policy pinfo 
	INNER JOIN vSubscription subs ON pinfo.PolicyID = subs.PolicyID  
	INNER JOIN vAllocationResults ar ON pinfo.PolicydateID = ar.PolicydateID
	INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID
WHERE pinfo.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) AND
	subs.SubscriberID >= COALESCE(@SubscriberCompanyID, 0) AND subs.SubscriberID <= COALESCE(NULLIF(@SubscriberCompanyID, 0), @maxSubscriberCompanyID) AND
	ar.AllocationID>=Coalesce(@AllocationID, 0) AND ar.AllocationID <= COALESCE(NULLIF(@AllocationID, 0), @maxAllocationID) AND 
	subs.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%')
GROUP BY pinfo.InsuranceProgram, subs.SubscriberID, subs.SubscriptionCompany, pinfo.CarrierGroupName, pinfo.CarrierName, a.AllocationID, a.AllocationDescription, subs.SubscriptionCompanySolvency, 
	pinfo.CarrierID, pinfo.CarrierGroupID, pinfo.InsuranceProgramID
ORDER BY pinfo.CarrierName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyByInsProg] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyByInsProg] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyByInsProg] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyByInsProg] TO [power_user]
GO
