SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryInsProduct]
(@ProductID bigint, @ClaimantEmploymentID bigint, @StartDate datetime, @EndDate datetime, @Emailaddress nvarchar(100))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblClaimantProduct (ProductID, ClaimantEmploymentID, StartDate, EndDate, SPUsername)
	VALUES (@ProductID, @ClaimantEmploymentID, @StartDate, @EndDate, @Emailaddress)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [power_user]
GO
