SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmSettleDocsDocEntry] (@ClaimLawsuitID int)

as

select DocumentID, DocumentTitle, L.ClaimLawsuitID, d.DetailDocumentTypeID
from tblDocuments D
Join tblclaim C on c.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID 
Join tblClaimLawsuit L on L.ClaimID = C.ClaimID
--Join tblDocumentType T on T.DocumentTypeID = d.DetailDocumentTypeID
where L.ClaimLawsuitID = @ClaimLawsuitID
ORDER BY DocumentTitle


Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettleDocsDocEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettleDocsDocEntry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettleDocsDocEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettleDocsDocEntry] TO [power_user]
GO
