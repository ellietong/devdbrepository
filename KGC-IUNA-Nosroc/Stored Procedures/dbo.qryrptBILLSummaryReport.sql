SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[qryrptBILLSummaryReport] (@BeginBill_ID int, @EndBill_ID int, @BillGroupID int)
AS

SET NOCOUNT ON
-- declare @BeginBill_id int
-- declare @EndBill_id int
-- declare @billgroupid int
-- set @BeginBill_id = 5
-- set @EndBill_id = 5
-- set @billgroupid =1

-- Past Amounts

DECLARE @PastAmounts TABLE
	(BillID int,
	PastBilled money,
	PastPaid money,
	PastBilledLessPaid money,
	PastAdjustments money,
	PastInterest money,
	PastInterestPaid money,
	PastInterestLessPaid money)

IF @BeginBill_ID = 1 
BEGIN
INSERT INTO @PastAmounts (BillID,
			PastBilled,
			PastPaid,
			PastBilledLessPaid,
			PastAdjustments,
			PastInterest,
			PastInterestPaid,
			PastInterestLessPaid)
VALUES (@EndBill_ID, 0, 0, 0, 0, 0, 0, 0)
END
ELSE 
BEGIN
INSERT INTO @PastAmounts
SELECT	@EndBill_ID as BillID,
	CASE WHEN LedgerTypeID = 1 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastBilled,
	CASE WHEN LedgerTypeID = 2 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastPaid,	
	CASE WHEN LedgerTypeID IN (1,2) THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastBilledLessPaid,
	CASE WHEN LedgerTYpeID = 3 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastAdjustments,
	CASE WHEN LedgerTypeID = 4 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastInterest,
	CASE WHEN LedgerTypeID = 5 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastInterestPaid,
	CASE WHEN LedgerTypeID IN (4,5) THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastInterestLessPaid
FROM vBilling b
WHERE BillID < @BeginBill_ID and BillGroupID = @BillGroupID
GROUP BY LedgerTypeID
END

-- Past Summed Amounts
DECLARE @PastSummedAmounts TABLE
	(BillID int,
	PastBilled money,
	PastPaid money,
	PastBilledLessPaid money,
	PastAdjustments money,
	PastInterest money,
	PastInterestPaid money,
	PastInterestLessPaid money)
INSERT INTO @PastSummedAmounts
SELECT	BillID,
	COALESCE(SUM(PastBilled),0) AS PastBilled,
	COALESCE(SUM(PastPaid),0) AS PastPaid,	
	COALESCE(SUM(PastBilledLessPaid),0) AS PastBilledLessPaid,
	COALESCE(SUM(PastAdjustments),0) AS PastAdjustments,
	COALESCE(SUM(PastInterest),0) AS PastInterest,
	COALESCE(SUM(PastInterestPaid),0) AS PastInterestPaid,
	COALESCE(SUM(PastInterestLessPaid),0) AS PastInterestLessPaid
FROM @PastAmounts
GROUP BY BillID

-- Current Amounts
DECLARE @CurrentAmounts TABLE
	(BillID int,
	CurrentBilled money,
	CurrentPaid money,
	CurrentBilledLessPaid money,
	CurrentAdjustments money,
	CurrentInterest money,
	CurrentInterestPaid money,
	CurrentInterestLessPaid money)
INSERT INTO @CurrentAmounts
SELECT	@EndBill_ID AS BillID,
	CASE WHEN LedgerTypeID = 1 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentBilled,
	CASE WHEN LedgerTypeID = 2 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentPaid,	
	CASE WHEN LedgerTypeID IN (1,2) THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentBilledLessPaid,
	CASE WHEN LedgerTYpeID = 3 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentAdjustments,
	CASE WHEN LedgerTypeID = 4 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentInterest,
	CASE WHEN LedgerTypeID = 5 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentInterestPaid,
	CASE WHEN LedgerTypeID IN (4,5) THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentInterestLessPaid
FROM vBilling b
WHERE (BillID BETWEEN @BeginBill_ID AND @EndBill_ID) and BillGroupID = @BillGroupID
GROUP BY LedgerTypeID, BillID

-- Current Amounts Summed
DECLARE @CurrentSummedAmounts TABLE
	(BillID int,
	CurrentBilled money,
	CurrentPaid money,
	CurrentBilledLessPaid money,
	CurrentAdjustments money,
	CurrentInterest money,
	CurrentInterestPaid money,
	CurrentInterestLessPaid money)
INSERT INTO @CurrentSummedAmounts
SELECT	BillID,
	SUM(CurrentBilled) AS CurrentBilled,
	SUM(CurrentPaid) AS CurrentPaid,	
	SUM(CurrentBilledLessPaid) AS CurrentBilledLessPaid,
	SUM(CurrentAdjustments) AS CurrentAdjustments,
	SUM(CurrentInterest) AS CurrentInterest,
	SUM(CurrentInterestPaid) AS CurrentInterestPaid,
	SUM(CurrentInterestLessPaid) AS CurrentInterestLessPaid
FROM @CurrentAmounts
GROUP BY BillID

--Results
SELECT	pa.BillID, BillDate, PaymentDueDate,
	SUM(CurrentBilled) AS CurrentBilled, 
	SUM(PastBilled) AS PastBilled, 
	SUM(CurrentPaid) AS CurrentPaid, 
	SUM(PastPaid) AS PastPaid, 
	SUM(CurrentBilledLessPaid) AS CurrentBilledLessPaid, 
	SUM(PastBilledLessPaid) AS PastBilledLessPaid,
	SUM(CurrentAdjustments) AS CurrentAdjustments,
	SUM(PastAdjustments) AS PastAdjustments,
	SUM(CurrentInterest) AS CurrentInterest,
	SUM(PastInterest) AS PastInterest,
	SUM(CurrentInterestPaid) AS CurrentInterestPaid,
	SUM(PastInterestPaid) AS PastInterestPaid,
	SUM(CurrentInterestLessPaid) AS CurrentInterestLessPaid,
	SUM(PastInterestLessPaid) AS PastInterestLessPaid
FROM @CurrentSummedAmounts ca INNER JOIN @PastSummedAmounts pa ON ca.BillID = pa.BillID
	INNER JOIN vBilling b ON ca.BillID = b.BillID
WHERE pa.BillID = @EndBill_ID and b.billgroupID = @BillgroupID
GROUP BY pa.BillID, BillDate, PaymentDueDate

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBILLSummaryReport] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBILLSummaryReport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBILLSummaryReport] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBILLSummaryReport] TO [power_user]
GO
