SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmViewSettlementDocuments] (@ClaimLawsuitID int)
as

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 17075

SELECT
	ClaimantDocumentID,
	UPPER(DocCategory) + ': PACE Document - ' + ClaimantDocumentType + COALESCE(' (' + pagenums + ')', '') AS DocumentTypeWithDate,
	CASE
		WHEN LEFT(DocumentURL, 2) NOT LIKE '/c' THEN DocumentURL ELSE 'https://admin.kcicllc.com' + DocumentURL
	END AS DocumentLink
FROM tblSettleDocs sd
INNER JOIN tblSettlePaymentDocumentation pd ON sd.SetPymtID = pd.SetPymtID
LEFT JOIN tblDocumentCategory cd ON sd.DocCategoryID = cd.DocCategoryID
LEFT JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblClaimantDocuments d ON sd.DocumentID = d.ClaimantDocumentID AND d.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
LEFT JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
WHERE pd.ClaimLawsuitID = @ClaimLawsuitID UNION SELECT
	sd.DocumentID,
	CASE
		WHEN DateCreated IS NULL THEN DocCategory ELSE UPPER(DocCategory) + ': ' + DocumentTitle + ' - ' + CONVERT(nvarchar, MONTH(DateCreated)) + '/' + CONVERT(nvarchar, DAY(DateCreated)) +
	'/' + CONVERT(nvarchar, YEAR(DateCreated))
	END + COALESCE(' (' + pagenums + ')', '') AS DocumentTypeWithDate,
	CASE
		WHEN LEFT(DocumentURL, 2) NOT LIKE '/c' THEN DocumentURL ELSE 'https://admin.kcicllc.com' + DocumentURL
	END AS DocumentLink
FROM tblSettleDocs sd
INNER JOIN tblSettlePaymentDocumentation pd ON sd.SetPymtID = pd.SetPymtID
LEFT JOIN tblDocumentCategory cd ON sd.DocCategoryID = cd.DocCategoryID
LEFT JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblDocuments d ON sd.DocumentID = d.DocumentID AND d.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
WHERE pd.ClaimLawsuitID = @ClaimLawsuitID
ORDER BY DocumentTypeWithDate

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewSettlementDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewSettlementDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewSettlementDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewSettlementDocuments] TO [power_user]
GO
