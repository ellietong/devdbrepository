SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_qrySettleDocs] (@SettlePaymentID int)
as

--DECLARE @ClaimID int
--SET @ClaimID = 1


SELECT AmazonKey,  
	   DocumentTitle, 
	   PageNums, 
	   DocCategory, 
	   SettleDocID, 
	   sd.SetPymtID, 
	   sd.DocumentID, 
	   sd.DocCategoryID,
	   '/clients/iuna_nosroc/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=/clients/iuna_nosroc/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), cpi.ClaimantPersonalInfoID) AS DocumentURL,
	   CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical	   
FROM tblSettleDocs sd 
	INNER JOIN tblDocuments d ON sd.DocumentID = d.DocumentID
	INNER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
	INNER JOIN (SELECT ClaimantPersonalInfoID, SetPymtID  
				FROM tblSettlePaymentDocumentation spd
				INNER JOIN  tblClaimLawsuit cl on spd.ClaimLawsuitID = cl.ClaimLawsuitID
				INNER JOIN  tblClaim c on cl.ClaimID = c.ClaimID) cpi ON sd.SetPymtID = cpi.SetPymtID		
WHERE DetailDocumentTypeID <> 40 --invoices
	AND sd.SetPymtID = @SettlePaymentID
RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [power_user]
GO
