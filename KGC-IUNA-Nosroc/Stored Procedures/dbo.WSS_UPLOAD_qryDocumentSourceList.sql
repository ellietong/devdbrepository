SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryDocumentSourceList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DocumentSourceID, DocumentSource FROM tblDocumentSource WHERE DocumentSourceID <> 6
	UNION
	SELECT NULL, '-- Select a Document Source --' 
	ORDER BY DocumentSource
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentSourceList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentSourceList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentSourceList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentSourceList] TO [power_user]
GO
