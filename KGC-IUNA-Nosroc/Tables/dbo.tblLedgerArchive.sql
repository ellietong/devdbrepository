CREATE TABLE [dbo].[tblLedgerArchive]
(
[LedgerID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupID] [int] NULL,
[CarrierID] [bigint] NOT NULL,
[SubscriberID] [int] NULL,
[Allocation] [money] NOT NULL,
[BillID] [int] NULL,
[BillDate] [datetime] NOT NULL,
[ExhaustionDateID] [int] NULL,
[PaymentDueDate] [datetime] NULL,
[BillNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [int] NULL,
[BillTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLedgerArchive] ADD CONSTRAINT [PK_tblLedger] PRIMARY KEY CLUSTERED  ([LedgerID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLedgerArchive] TO [base_user]
GRANT SELECT ON  [dbo].[tblLedgerArchive] TO [base_user]
GRANT SELECT ON  [dbo].[tblLedgerArchive] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblLedgerArchive] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblLedgerArchive] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblLedgerArchive] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblLedgerArchive] TO [power_user]
GRANT INSERT ON  [dbo].[tblLedgerArchive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLedgerArchive] TO [power_user]
GO
