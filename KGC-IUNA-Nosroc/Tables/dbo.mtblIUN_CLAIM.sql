CREATE TABLE [dbo].[mtblIUN_CLAIM]
(
[CLM_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MATTERID] [int] NULL,
[IS_CURRENT] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsCurrentClaim] [bit] NULL,
[CLM_SEQ] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLAINTIFF_DISEASE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLAINTIFF_DIAGNOSTIC_DATE] [datetime] NULL,
[DEFENSE_DISEASE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEFENSE_DIAGNOSTIC_DATE] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblIUN_CLAIM] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_CLAIM] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_CLAIM] TO [power_user]
GRANT INSERT ON  [dbo].[mtblIUN_CLAIM] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblIUN_CLAIM] TO [power_user]
GO
