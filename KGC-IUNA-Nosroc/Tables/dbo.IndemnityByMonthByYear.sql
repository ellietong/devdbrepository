CREATE TABLE [dbo].[IndemnityByMonthByYear]
(
[Month] [float] NULL,
[Year] [float] NULL,
[SumOfappliedindemnity] [money] NULL,
[Matterid] [float] NULL,
[ExpectedPayDate] [date] NULL
) ON [PRIMARY]
GO
