CREATE TABLE [dbo].[ExpenseByMonthByYear]
(
[Month] [float] NULL,
[Year] [float] NULL,
[Total Expense] [money] NULL,
[MatterID] [float] NULL,
[ExpectedPayDate] [date] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ExpenseByMonthByYear] TO [base_user]
GRANT SELECT ON  [dbo].[ExpenseByMonthByYear] TO [Claims_User]
GO
