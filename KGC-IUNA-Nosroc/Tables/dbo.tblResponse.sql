CREATE TABLE [dbo].[tblResponse]
(
[ResponseID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NULL,
[NoticeResponseID] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblResponse] ADD CONSTRAINT [PK_tblResponse] PRIMARY KEY CLUSTERED  ([ResponseID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblResponse] ADD CONSTRAINT [FK_tblResponse_tblCorrespondence] FOREIGN KEY ([CorrespondenceID]) REFERENCES [dbo].[tblCorrespondence] ([CorrespondenceID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblResponse] TO [base_user]
GRANT SELECT ON  [dbo].[tblResponse] TO [base_user]
GRANT SELECT ON  [dbo].[tblResponse] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblResponse] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblResponse] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblResponse] TO [Notice_User]
GRANT SELECT ON  [dbo].[tblResponse] TO [power_user]
GRANT INSERT ON  [dbo].[tblResponse] TO [power_user]
GRANT UPDATE ON  [dbo].[tblResponse] TO [power_user]
GO
