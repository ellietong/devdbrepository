CREATE TABLE [dbo].[SubBill$]
(
[LonBillID] [float] NULL,
[SubscriberID] [float] NULL,
[Defense (Excess] [money] NULL,
[Comment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Total] [money] NULL,
[F6] [money] NULL,
[F7] [money] NULL,
[F8] [money] NULL,
[F9] [money] NULL,
[F10] [money] NULL,
[F11] [money] NULL,
[F12] [money] NULL,
[F13] [money] NULL,
[F14] [money] NULL,
[F15] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F16] [money] NULL,
[F17] [money] NULL,
[F18] [money] NULL,
[F19] [money] NULL,
[F20] [money] NULL,
[F21] [money] NULL,
[F22] [money] NULL
) ON [PRIMARY]
GO
