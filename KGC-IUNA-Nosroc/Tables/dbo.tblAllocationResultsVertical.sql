CREATE TABLE [dbo].[tblAllocationResultsVertical]
(
[ARVerticalID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[OccurrenceID] [bigint] NULL,
[ColumnID] [int] NULL,
[uncollapsedVertical] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsVertical] ADD CONSTRAINT [PK_tblAllocationResultsVertical] PRIMARY KEY NONCLUSTERED  ([ARVerticalID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsVertical] ADD CONSTRAINT [FK_tblAllocationResultsVertical_tblOccurrence] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblAllocationResultsVertical] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationResultsVertical] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationResultsVertical] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationResultsVertical] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsVertical] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsVertical] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsVertical] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationResultsVertical] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationResultsVertical] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResultsVertical', 'COLUMN', N'OccurrenceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResultsVertical', 'COLUMN', N'OccurrenceID'
GO
