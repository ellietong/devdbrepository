CREATE TABLE [dbo].[tblClaimantEmployment]
(
[ClaimantEmploymentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[Occupation] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jobsite] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmpStartDate] [datetime] NULL,
[EmpEndDate] [datetime] NULL,
[EarliestExposureDate] [datetime] NULL,
[LatestExposureDate] [datetime] NULL,
[IsPrimary] [bit] NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteStateID] [int] NULL,
[IsSecondaryExposure] [bit] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantEmployment] ADD CONSTRAINT [PK__tblClaim__F25662D46932806F] PRIMARY KEY CLUSTERED  ([ClaimantEmploymentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantEmployment] ADD CONSTRAINT [FK_tblClaimantEmployment_NEW_tblClaimantPersonalInfo_NEW] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblClaimantEmployment] TO [kanga]
GRANT ALTER ON  [dbo].[tblClaimantEmployment] TO [monahanc]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantPersonalInfoID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantPersonalInfoID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantPersonalInfoID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=1125
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpEndDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpStartDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=4785
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'LatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'LatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'LatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Occupation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Occupation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Occupation'
GO
