CREATE TABLE [dbo].[tblClaimantSettlement]
(
[ClaimantSettlementID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[SettlementDate] [datetime] NULL,
[SettlementAmount] [money] NULL,
[SettlementDueDate] [datetime] NULL,
[SettlementNotificationDate] [datetime] NULL,
[SettlementFundsRequestDate] [datetime] NULL,
[PayToCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaidDate] [datetime] NULL,
[SettlementGroupID] [bigint] NULL,
[SettlementNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComplaintFlag] [bit] NULL CONSTRAINT [DF_tblClaimantSettlement_NEW_ComplaintFlag] DEFAULT ((0)),
[MedicalFlag] [bit] NULL CONSTRAINT [DF_tblClaimantSettlement_NEW_MedicalFlag] DEFAULT ((0)),
[WorkFlag] [bit] NULL CONSTRAINT [DF_tblClaimantSettlement_NEW_WorkFlag] DEFAULT ((0)),
[ProductFlag] [bit] NULL CONSTRAINT [DF_tblClaimantSettlement_NEW_ProductFlag] DEFAULT ((0)),
[ReleaseFlag] [bit] NULL CONSTRAINT [DF_tblClaimantSettlement_NEW_ReleaseFlag] DEFAULT ((0)),
[EstateFlag] [bit] NULL CONSTRAINT [DF_tblClaimantSettlement_NEW_EstateFlag] DEFAULT ((0)),
[SettConfirmLetterFlag] [bit] NULL CONSTRAINT [DF_tblClaimantSettlement_NEW_SettConfirmLetterFlag] DEFAULT ((0)),
[NavClaimResolutionKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavReleaseType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavSettlementGroup] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentApprovedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSettlement] ADD CONSTRAINT [PK_mBtblClaimantSettlement_NEW] PRIMARY KEY CLUSTERED  ([ClaimantSettlementID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimantSettlement_SettlementGrouPID] ON [dbo].[tblClaimantSettlement] ([SettlementGroupID]) INCLUDE ([ClaimLawsuitID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSettlement] ADD CONSTRAINT [FK_tblClaimantSettlement_NEW_tblClaimLawsuit_NEW] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantSettlement] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantSettlement] TO [power_user]
GO
