CREATE TABLE [dbo].[mtblBundledDependent]
(
[InjuredPartyID] [bigint] NULL,
[ClaimSequenceNum] [int] NULL,
[SequenceNum] [int] NULL,
[RelationshipCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RelationshipDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledDependent] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledDependent] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledDependent] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledDependent] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledDependent] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledDependent] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledDependent] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledDependent] TO [power_user]
GO
