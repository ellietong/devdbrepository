CREATE TABLE [dbo].[mtblIUN_PRODUCT]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISPLAY_VALUE] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BRAND] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESCRIPTION] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[START_DATE] [datetime] NULL,
[END_DATE] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblIUN_PRODUCT] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_PRODUCT] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_PRODUCT] TO [power_user]
GRANT INSERT ON  [dbo].[mtblIUN_PRODUCT] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblIUN_PRODUCT] TO [power_user]
GO
