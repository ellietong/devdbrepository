CREATE TABLE [dbo].[tblForecast_Puritan]
(
[ForecastYear] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForecastIndemnity] [money] NULL,
[ForecastDefenseConstant] [money] NULL,
[ForecastDefenseDecreasing] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblForecast_Puritan] TO [base_user]
GRANT SELECT ON  [dbo].[tblForecast_Puritan] TO [power_user]
GO
