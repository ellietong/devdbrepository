CREATE TABLE [dbo].[mtblSubscriberBilled]
(
[LonBillID] [float] NULL,
[SubscriberID] [float] NULL,
[TotBilled] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblSubscriberBilled] TO [base_user]
GRANT SELECT ON  [dbo].[mtblSubscriberBilled] TO [base_user]
GRANT ALTER ON  [dbo].[mtblSubscriberBilled] TO [power_user]
GRANT SELECT ON  [dbo].[mtblSubscriberBilled] TO [power_user]
GRANT INSERT ON  [dbo].[mtblSubscriberBilled] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblSubscriberBilled] TO [power_user]
GO
