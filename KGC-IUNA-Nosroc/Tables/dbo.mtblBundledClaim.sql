CREATE TABLE [dbo].[mtblBundledClaim]
(
[InjuredPartyID] [bigint] NULL,
[ClaimSequenceNum] [int] NULL,
[ClaimStatusCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimStatusDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstDiagnosisDate] [datetime] NULL,
[FirstExposureDate] [datetime] NULL,
[LastExposureDate] [datetime] NULL,
[DiagnosisCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiagnosisDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementAmount] [money] NULL,
[SettlementDate] [datetime] NULL,
[ReleaseReceivedDate] [datetime] NULL,
[ReleaseType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseTypeDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InputDate] [datetime] NULL,
[SettlementGroup] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimQCFlag] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DynamicCategoryCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DynamixCategoryDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageStatusNum] [int] NULL,
[ImageStatusDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BoxNum] [int] NULL,
[BoxDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RollNum] [int] NULL,
[FrameNum] [int] NULL,
[DISRInputDate] [datetime] NULL,
[ReleaseStatusCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PfizerTransferDate] [datetime] NULL,
[VoidLetterDate] [datetime] NULL,
[VoidDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledClaim] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledClaim] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledClaim] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledClaim] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledClaim] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledClaim] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledClaim] TO [power_user]
GO
