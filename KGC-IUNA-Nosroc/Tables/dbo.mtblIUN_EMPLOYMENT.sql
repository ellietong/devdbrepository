CREATE TABLE [dbo].[mtblIUN_EMPLOYMENT]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OCCUPATION] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JOBSITE] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_PRIMARY] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimary] [bit] NULL,
[COMMENTS] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[START_DATE] [datetime] NULL,
[END_DATE] [datetime] NULL,
[EARLIEST_EXPOSURE_DATE] [datetime] NULL,
[LATEST_EXPOSURE_DATE] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblIUN_EMPLOYMENT] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_EMPLOYMENT] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_EMPLOYMENT] TO [power_user]
GRANT INSERT ON  [dbo].[mtblIUN_EMPLOYMENT] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblIUN_EMPLOYMENT] TO [power_user]
GO
