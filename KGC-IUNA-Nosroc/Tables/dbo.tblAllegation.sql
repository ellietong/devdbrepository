CREATE TABLE [dbo].[tblAllegation]
(
[AllegationID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[DefendantCompanyID] [int] NULL,
[AllegationTypeID] [int] NULL,
[DamageID] [int] NULL,
[MaterialID] [int] NULL,
[NavClaimLawsuitKey] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDefendantCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavAllegationType] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDamage] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavMaterial] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegation] ADD CONSTRAINT [PK_tblAllegation] PRIMARY KEY CLUSTERED  ([AllegationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegation] ADD CONSTRAINT [FK_tblAllegation_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllegation] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllegation] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblAllegation] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllegation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllegation] TO [power_user]
GO
