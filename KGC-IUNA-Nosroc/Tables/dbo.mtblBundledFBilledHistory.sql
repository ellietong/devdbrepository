CREATE TABLE [dbo].[mtblBundledFBilledHistory]
(
[InjuredPartyID] [bigint] NULL,
[ClaimSequenceNum] [int] NULL,
[LatestBillDate] [datetime] NULL,
[AMCShare] [money] NULL,
[APGShare] [money] NULL,
[AWIShare] [money] NULL,
[CTDShare] [money] NULL,
[DNAShare] [money] NULL,
[FLEShare] [money] NULL,
[GAFShare] [money] NULL,
[IUNShare] [money] NULL,
[KENShare] [money] NULL,
[MARShare] [money] NULL,
[NATShare] [money] NULL,
[NBRShare] [money] NULL,
[NOSShare] [money] NULL,
[NUTShare] [money] NULL,
[PFZShare] [money] NULL,
[QUIShare] [money] NULL,
[SHOShare] [money] NULL,
[THUShare] [money] NULL,
[TNNShare] [money] NULL,
[UNCShare] [money] NULL,
[OriginalBillDate] [datetime] NULL,
[TotalAmount] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledFBilledHistory] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledFBilledHistory] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledFBilledHistory] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledFBilledHistory] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledFBilledHistory] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledFBilledHistory] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledFBilledHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledFBilledHistory] TO [power_user]
GO
