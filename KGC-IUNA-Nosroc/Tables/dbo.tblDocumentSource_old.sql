CREATE TABLE [dbo].[tblDocumentSource_old]
(
[DocumentSourceID] [int] NOT NULL IDENTITY(1, 1),
[DocumentSource] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDocumentSource_old] ADD CONSTRAINT [PK_tblDocumentSource] PRIMARY KEY CLUSTERED  ([DocumentSourceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDocumentSource_old] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocumentSource_old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDocumentSource_old] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDocumentSource_old] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDocumentSource_old] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDocumentSource_old] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDocumentSource_old] TO [power_user]
GRANT SELECT ON  [dbo].[tblDocumentSource_old] TO [power_user]
GRANT INSERT ON  [dbo].[tblDocumentSource_old] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDocumentSource_old] TO [power_user]
GO
