CREATE TABLE [dbo].[ExpenseIncurredByYear]
(
[Year] [int] NULL,
[Defense] [money] NULL,
[MatterID] [int] NULL,
[Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ExpenseIncurredByYear] TO [base_user]
GRANT SELECT ON  [dbo].[ExpenseIncurredByYear] TO [Claims_User]
GRANT INSERT ON  [dbo].[ExpenseIncurredByYear] TO [Claims_User]
GRANT DELETE ON  [dbo].[ExpenseIncurredByYear] TO [Claims_User]
GRANT UPDATE ON  [dbo].[ExpenseIncurredByYear] TO [Claims_User]
GO
