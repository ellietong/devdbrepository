CREATE TABLE [dbo].[mtblExhaustion_AIG_November2009]
(
[PolicyID] [float] NULL,
[PolicyDateID] [float] NULL,
[PolicyNum] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsuranceProgram] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PStartDate] [datetime] NULL,
[PEndDate] [datetime] NULL,
[AttachmentPoint] [money] NULL,
[PerOccLimit] [money] NULL,
[AggregateLimit] [money] NULL,
[ExhaustionTypeID] [float] NULL,
[ExhaustionDateID] [float] NULL,
[OccurrenceID] [float] NULL,
[OccurrenceTypeID] [float] NULL,
[IndemnityAllocation] [money] NULL,
[DefenseAllocation] [money] NULL,
[DefenseOutsideLimitsAllocation] [money] NULL,
[SIRIndemnityAllocation] [money] NULL,
[OutsideConsumption] [money] NULL,
[OtherConsumption] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblExhaustion_AIG_November2009] TO [base_user]
GRANT SELECT ON  [dbo].[mtblExhaustion_AIG_November2009] TO [base_user]
GRANT SELECT ON  [dbo].[mtblExhaustion_AIG_November2009] TO [Exhaustion_User]
GRANT INSERT ON  [dbo].[mtblExhaustion_AIG_November2009] TO [Exhaustion_User]
GRANT DELETE ON  [dbo].[mtblExhaustion_AIG_November2009] TO [Exhaustion_User]
GRANT UPDATE ON  [dbo].[mtblExhaustion_AIG_November2009] TO [Exhaustion_User]
GRANT SELECT ON  [dbo].[mtblExhaustion_AIG_November2009] TO [power_user]
GRANT INSERT ON  [dbo].[mtblExhaustion_AIG_November2009] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblExhaustion_AIG_November2009] TO [power_user]
GO
