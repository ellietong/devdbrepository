CREATE TABLE [dbo].[mtblBundledInjuredParty]
(
[InjuredPartyID] [bigint] NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [datetime] NULL,
[DeathDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledInjuredParty] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledInjuredParty] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledInjuredParty] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledInjuredParty] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledInjuredParty] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledInjuredParty] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledInjuredParty] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledInjuredParty] TO [power_user]
GO
