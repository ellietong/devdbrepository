CREATE TABLE [dbo].[tblStatusGroup]
(
[StatusGroupID] [int] NOT NULL IDENTITY(1, 1),
[StatusGroup] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblStatusGroup] ADD CONSTRAINT [PK_tblStatusGroup_1] PRIMARY KEY CLUSTERED  ([StatusGroupID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblStatusGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblStatusGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblStatusGroup] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblStatusGroup] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblStatusGroup] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblStatusGroup] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblStatusGroup] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblStatusGroup] TO [power_user]
GRANT SELECT ON  [dbo].[tblStatusGroup] TO [power_user]
GO
