CREATE TABLE [dbo].[tblBillType]
(
[BillTypeID] [int] NOT NULL IDENTITY(1, 1),
[BillType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillType] ADD CONSTRAINT [PK_tblBillType] PRIMARY KEY CLUSTERED  ([BillTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBillType] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillType] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillType] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblBillType] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblBillType] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblBillType] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblBillType] TO [power_user]
GRANT INSERT ON  [dbo].[tblBillType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBillType] TO [power_user]
GO
