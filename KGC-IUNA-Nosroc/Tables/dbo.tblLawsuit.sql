CREATE TABLE [dbo].[tblLawsuit]
(
[LawsuitID] [bigint] NOT NULL IDENTITY(1, 1),
[CaseCaption] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[JurisdictionID_OLD] [int] NULL,
[StateID] [int] NULL,
[CourtTypeID] [int] NULL,
[ServiceDate] [datetime] NULL,
[ReceivedDate] [datetime] NULL,
[PrimaryDefenseCounselID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[PrimaryLocalPlaintiffCounsel] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryNationalPlaintiffCounsel] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffFirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffLastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveTrialDate_old] [datetime] NULL,
[NavLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLawsuitID] [bigint] NULL,
[NavJurisdiction] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavState] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavCourtType] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDefenseCounsel] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselID] [int] NULL,
[PlaintiffCounselAddressID] [int] NULL,
[JurisdictionID] [int] NULL,
[IsThirdPartyComplaint] [bit] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLawsuit] ADD CONSTRAINT [PK__tblLawsuit_NEW1__5A1A5A11] PRIMARY KEY CLUSTERED  ([LawsuitID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblLawsuit] TO [monahanc]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [power_user]
GRANT INSERT ON  [dbo].[tblLawsuit] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLawsuit] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'JurisdictionID_OLD'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'JurisdictionID_OLD'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'StateID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'StateID'
GO
