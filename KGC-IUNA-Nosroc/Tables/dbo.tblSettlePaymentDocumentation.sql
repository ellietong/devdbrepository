CREATE TABLE [dbo].[tblSettlePaymentDocumentation]
(
[SetPymtID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [int] NULL,
[SettlementConfirmation] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_SettlementConfirmation] DEFAULT ((0)),
[ClaimantIDed] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_ClaimantIDed] DEFAULT ((0)),
[DxMedicals] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_DxMedicals] DEFAULT ((0)),
[DODCert] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_DODCert] DEFAULT ((0)),
[EstatePapers] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_EstatePapers] DEFAULT ((0)),
[ProductID] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_ProductID] DEFAULT ((0)),
[EmployExpoHist] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_EmployExpoHist] DEFAULT ((0)),
[ReleaseRqstDate] [datetime] NULL,
[ReleaseToPC] [datetime] NULL,
[ExecutedReleaseDate] [datetime] NULL,
[SettlePaymentComments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseRcvd] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_ReleaseRcvd] DEFAULT ((0)),
[MedicareFullyDocumented] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_MedicareFullyDocumented] DEFAULT ((0)),
[MedicareReported] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_MedicareReported] DEFAULT ((0)),
[ReleaseSignedDate] [datetime] NULL,
[ReleaseTypeID] [int] NULL,
[NavSettleReleaseType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseTemplateTypeID] [int] NULL,
[UpdateReleaseTemplate] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicareAddendumSent] [datetime] NULL,
[MedicareAddendumRcvd] [datetime] NULL,
[MedicareFormSent] [datetime] NULL,
[MedicareFormRcvd] [datetime] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlePaymentDocumentation] ADD CONSTRAINT [PK_tblSettlePaymentDocumentation] PRIMARY KEY CLUSTERED  ([SetPymtID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblSettlePaymentDocumentation_ClaimLawsuitID] ON [dbo].[tblSettlePaymentDocumentation] ([ClaimLawsuitID]) INCLUDE ([ClaimantIDed], [DODCert], [DxMedicals], [EmployExpoHist], [EstatePapers], [MedicareFormRcvd], [MedicareFullyDocumented], [ProductID], [ReleaseRcvd], [ReleaseSignedDate], [ReleaseTypeID], [SettlementConfirmation]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlePaymentDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlePaymentDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlePaymentDocumentation] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblSettlePaymentDocumentation] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblSettlePaymentDocumentation] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GO
