CREATE TABLE [dbo].[mtblNOS_SETTLEMENT]
(
[CLM_RSLTN_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLM_LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RSLTN_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SETTLEMENT_GROUP] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RSLTN_DATE] [datetime] NULL,
[SETTLE_AMT] [money] NULL,
[FIRST_PAYABLE_DATE] [datetime] NULL,
[PAY_TO] [nvarchar] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHECK_NBR] [int] NULL,
[PAID_DATE] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblNOS_SETTLEMENT] TO [base_user]
GRANT SELECT ON  [dbo].[mtblNOS_SETTLEMENT] TO [base_user]
GRANT SELECT ON  [dbo].[mtblNOS_SETTLEMENT] TO [power_user]
GRANT INSERT ON  [dbo].[mtblNOS_SETTLEMENT] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblNOS_SETTLEMENT] TO [power_user]
GO
