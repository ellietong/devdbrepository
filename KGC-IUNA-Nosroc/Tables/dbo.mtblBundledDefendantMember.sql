CREATE TABLE [dbo].[mtblBundledDefendantMember]
(
[InjuredPartyID] [bigint] NULL,
[ClaimSequenceNum] [int] NULL,
[CompanyNum] [int] NULL,
[CompanyName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentCompanyNum] [int] NULL,
[ParentCompanyName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyTypeCode] [int] NULL,
[Column 7] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 8] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 9] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 10] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 11] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 12] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledDefendantMember] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledDefendantMember] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledDefendantMember] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledDefendantMember] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledDefendantMember] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledDefendantMember] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledDefendantMember] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledDefendantMember] TO [power_user]
GO
