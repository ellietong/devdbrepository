CREATE TABLE [dbo].[tblExpenseClaimsCurve]
(
[ExpenseCurveID] [int] NOT NULL,
[ExpenseCurve] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExpenseClaimsCurve] TO [base_user]
GRANT SELECT ON  [dbo].[tblExpenseClaimsCurve] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblExpenseClaimsCurve] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblExpenseClaimsCurve] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblExpenseClaimsCurve] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblExpenseClaimsCurve] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblExpenseClaimsCurve] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblExpenseClaimsCurve] TO [power_user]
GRANT INSERT ON  [dbo].[tblExpenseClaimsCurve] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExpenseClaimsCurve] TO [power_user]
GO
