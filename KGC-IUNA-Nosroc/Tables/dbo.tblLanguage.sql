CREATE TABLE [dbo].[tblLanguage]
(
[LanguageID] [int] NOT NULL IDENTITY(1, 1),
[PolicyLanguage] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProvisionLabelID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLanguage] ADD CONSTRAINT [PK_tblLanguage] PRIMARY KEY CLUSTERED  ([LanguageID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblLanguage] TO [Language_User]
GRANT INSERT ON  [dbo].[tblLanguage] TO [Language_User]
GRANT DELETE ON  [dbo].[tblLanguage] TO [Language_User]
GRANT UPDATE ON  [dbo].[tblLanguage] TO [Language_User]
GRANT SELECT ON  [dbo].[tblLanguage] TO [power_user]
GRANT INSERT ON  [dbo].[tblLanguage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLanguage] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderByOn', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2147483647
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_ColumnHidden', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', 'COLUMN', N'LanguageID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', 'COLUMN', N'LanguageID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', 'COLUMN', N'LanguageID'
GO
EXEC sp_addextendedproperty N'MS_ColumnHidden', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', 'COLUMN', N'PolicyLanguage'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', 'COLUMN', N'PolicyLanguage'
GO
DECLARE @xp int
SELECT @xp=5355
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblLanguage', 'COLUMN', N'PolicyLanguage'
GO
