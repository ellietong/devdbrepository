CREATE TABLE [dbo].[tblProvisionLanguage]
(
[ProvisionLanguageID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NULL,
[LanguageID] [int] NULL,
[ExactLanguage] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatesStart] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatesEnd] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProvisionNotes] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationID] [int] NULL,
[EndorsementNum] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EffectiveDate] [datetime] NULL,
[ProvisionLabelID] [int] NULL,
[LangTypeID] [int] NULL,
[Sort] [int] NULL,
[DoNotShow] [bit] NULL,
[FormNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionLanguage] ADD CONSTRAINT [PK_tblProvisionLanguage] PRIMARY KEY CLUSTERED  ([ProvisionLanguageID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionLanguage] ADD CONSTRAINT [FK_tblProvisionLanguage_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy] ([PolicyID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionLanguage] TO [Language_User]
GRANT INSERT ON  [dbo].[tblProvisionLanguage] TO [Language_User]
GRANT DELETE ON  [dbo].[tblProvisionLanguage] TO [Language_User]
GRANT UPDATE ON  [dbo].[tblProvisionLanguage] TO [Language_User]
GRANT SELECT ON  [dbo].[tblProvisionLanguage] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvisionLanguage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvisionLanguage] TO [power_user]
GO
