CREATE TABLE [dbo].[tblForecast_OldRe_20130521]
(
[ForecastYear] [datetime] NULL,
[MatterID] [float] NULL,
[ForecastIndemnity] [money] NULL,
[ForecastDefense] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblForecast_OldRe_20130521] TO [base_user]
GRANT SELECT ON  [dbo].[tblForecast_OldRe_20130521] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblForecast_OldRe_20130521] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblForecast_OldRe_20130521] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblForecast_OldRe_20130521] TO [Claims_User]
GO
