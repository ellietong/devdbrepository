CREATE TABLE [dbo].[mtblBundledPresettlement]
(
[InjuredPartyID] [bigint] NULL,
[ClaimSequenceNum] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyNum] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementAmount] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledPresettlement] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledPresettlement] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledPresettlement] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledPresettlement] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledPresettlement] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledPresettlement] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledPresettlement] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledPresettlement] TO [power_user]
GO
