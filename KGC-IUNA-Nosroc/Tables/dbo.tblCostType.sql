CREATE TABLE [dbo].[tblCostType]
(
[CostTypeID] [int] NOT NULL,
[CostType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcludeCostType] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCostType] ADD CONSTRAINT [PK_mEtblCostType] PRIMARY KEY CLUSTERED  ([CostTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCostType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCostType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCostType] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblCostType] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblCostType] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblCostType] TO [Enviro_User]
GRANT SELECT ON  [dbo].[tblCostType] TO [power_user]
GRANT INSERT ON  [dbo].[tblCostType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCostType] TO [power_user]
GO
