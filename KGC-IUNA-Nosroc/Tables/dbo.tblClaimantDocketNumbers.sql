CREATE TABLE [dbo].[tblClaimantDocketNumbers]
(
[ClaimantDocketNumberID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NOT NULL,
[DocketNumbers] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimantDocketNumbers_ClaimantPersonalinfoID] ON [dbo].[tblClaimantDocketNumbers] ([ClaimantPersonalInfoID]) INCLUDE ([DocketNumbers]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantDocketNumbers] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantDocketNumbers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantDocketNumbers] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantDocketNumbers] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantDocketNumbers] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantDocketNumbers] TO [power_user]
GO
