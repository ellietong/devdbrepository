CREATE TABLE [dbo].[StatDefense$]
(
[FirstExposureDate] [datetime] NULL,
[FirstDiagnosisDate] [datetime] NULL,
[Percentage] [float] NULL,
[TotalAllocatedExpense] [money] NULL,
[PercentageAllocatedExpense] [money] NULL,
[MatterID] [float] NULL
) ON [PRIMARY]
GO
