CREATE TABLE [dbo].[tblProvision1]
(
[Provision1ID] [int] NOT NULL,
[Provision1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Provision1Discount] [real] NULL
) ON [PRIMARY]
GO
