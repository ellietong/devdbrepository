CREATE TABLE [dbo].[tblForecast_20130305]
(
[ForecastYear] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForecastIndemnity] [money] NULL,
[ForecastDefense] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblForecast_20130305] TO [base_user]
GRANT SELECT ON  [dbo].[tblForecast_20130305] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblForecast_20130305] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblForecast_20130305] TO [Claims_User]
GO
