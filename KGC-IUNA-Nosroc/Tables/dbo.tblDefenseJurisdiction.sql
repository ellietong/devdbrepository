CREATE TABLE [dbo].[tblDefenseJurisdiction]
(
[DefenseJurisdictionID] [int] NOT NULL IDENTITY(1, 1),
[StateID] [int] NULL,
[JurisdictionID] [int] NULL,
[CourtTypeID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[IsActive] [bit] NULL CONSTRAINT [DF_tblDefenseJurisdiction_IsActive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseJurisdiction] ADD CONSTRAINT [PK_tblDefenseJurisdiction] PRIMARY KEY CLUSTERED  ([DefenseJurisdictionID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseJurisdiction] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GO
