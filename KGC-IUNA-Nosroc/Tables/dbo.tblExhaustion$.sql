CREATE TABLE [dbo].[tblExhaustion$]
(
[ExhaustionDateID] [float] NULL,
[OccurrenceID] [float] NULL,
[PolicyDateID] [float] NULL,
[IndemnityAllocation] [float] NULL,
[ShortfallIndemnityAllocationResults] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseAllocation] [float] NULL,
[ShortfallDefenseAllocationResults] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseOutsideLimitsAllocation] [float] NULL,
[ShortfallDefenseOutsideLimitsAllocationResults] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SIRIndemnityAllocation] [float] NULL,
[SIRDefenseAllocation] [float] NULL,
[LedgerID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OutsideConsumption] [float] NULL,
[mAllocationResultID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mAllocationID] [float] NULL,
[mPStartSplit] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mPEndSplit] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mAnnualPeriodSplit] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mTriggerStartDateUsed] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mTriggerEndDateUsed] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mDateTime] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
