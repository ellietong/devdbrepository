CREATE TABLE [dbo].[tblInternalCompany]
(
[InternalCompanyID] [int] NOT NULL IDENTITY(1, 1),
[InternalCompany] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblInternalCompany] ADD CONSTRAINT [PK_tblInternalCompany] PRIMARY KEY CLUSTERED  ([InternalCompanyID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblInternalCompany] TO [base_user]
GRANT SELECT ON  [dbo].[tblInternalCompany] TO [base_user]
GRANT SELECT ON  [dbo].[tblInternalCompany] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblInternalCompany] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblInternalCompany] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblInternalCompany] TO [Notice_User]
GRANT SELECT ON  [dbo].[tblInternalCompany] TO [power_user]
GRANT INSERT ON  [dbo].[tblInternalCompany] TO [power_user]
GRANT UPDATE ON  [dbo].[tblInternalCompany] TO [power_user]
GO
