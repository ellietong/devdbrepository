CREATE TABLE [dbo].[mtblNOS_DOCTOR]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoctorID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblNOS_DOCTOR] TO [base_user]
GRANT SELECT ON  [dbo].[mtblNOS_DOCTOR] TO [base_user]
GRANT SELECT ON  [dbo].[mtblNOS_DOCTOR] TO [power_user]
GRANT INSERT ON  [dbo].[mtblNOS_DOCTOR] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblNOS_DOCTOR] TO [power_user]
GO
