CREATE TABLE [dbo].[tblTmpActiveTrial]
(
[DocketNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveTrialDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTmpActiveTrial] TO [base_user]
GRANT SELECT ON  [dbo].[tblTmpActiveTrial] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTmpActiveTrial] TO [power_user]
GRANT SELECT ON  [dbo].[tblTmpActiveTrial] TO [power_user]
GRANT INSERT ON  [dbo].[tblTmpActiveTrial] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTmpActiveTrial] TO [power_user]
GO
