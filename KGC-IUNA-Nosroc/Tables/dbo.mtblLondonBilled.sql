CREATE TABLE [dbo].[mtblLondonBilled]
(
[LonBillID] [float] NULL,
[F2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [float] NULL,
[PolID] [float] NULL,
[NavBillID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Month] [float] NULL,
[Year] [float] NULL,
[Indemnity] [money] NULL,
[Defense] [money] NULL,
[Defense (Excess] [money] NULL,
[Total] [money] NULL,
[Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblLondonBilled] TO [base_user]
GRANT SELECT ON  [dbo].[mtblLondonBilled] TO [base_user]
GRANT ALTER ON  [dbo].[mtblLondonBilled] TO [power_user]
GRANT SELECT ON  [dbo].[mtblLondonBilled] TO [power_user]
GRANT INSERT ON  [dbo].[mtblLondonBilled] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblLondonBilled] TO [power_user]
GO
