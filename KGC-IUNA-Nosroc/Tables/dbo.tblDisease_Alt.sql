CREATE TABLE [dbo].[tblDisease_Alt]
(
[DiseaseID_Alt] [int] NULL,
[ICD9] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICD9Desc] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighLevelDiseaseID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDisease_Alt] TO [base_user]
GRANT SELECT ON  [dbo].[tblDisease_Alt] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDisease_Alt] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDisease_Alt] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDisease_Alt] TO [power_user]
GRANT SELECT ON  [dbo].[tblDisease_Alt] TO [power_user]
GRANT INSERT ON  [dbo].[tblDisease_Alt] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDisease_Alt] TO [power_user]
GO
