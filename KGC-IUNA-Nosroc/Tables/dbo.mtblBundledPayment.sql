CREATE TABLE [dbo].[mtblBundledPayment]
(
[InjuredPartyID] [bigint] NULL,
[ClaimSequenceNum] [int] NULL,
[PaymentID] [int] NULL,
[AllocationAmount] [money] NULL,
[RefundAmount] [money] NULL,
[PaidDate] [datetime] NULL,
[CheckNum] [int] NULL,
[PaymentStatusCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledPayment] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledPayment] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledPayment] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledPayment] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledPayment] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledPayment] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledPayment] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledPayment] TO [power_user]
GO
