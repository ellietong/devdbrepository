CREATE TABLE [dbo].[mtblIUN_RELEASE]
(
[CLM_RSLTN_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELEASE_SIGNED_DATE] [datetime] NULL,
[RELEASE_RCVD_DATE] [datetime] NULL,
[RELEASE_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblIUN_RELEASE] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_RELEASE] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_RELEASE] TO [power_user]
GRANT INSERT ON  [dbo].[mtblIUN_RELEASE] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblIUN_RELEASE] TO [power_user]
GO
