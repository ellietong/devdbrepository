CREATE TABLE [dbo].[Sheet1$]
(
[BillGroupID] [float] NULL,
[CarrierID] [float] NULL,
[Allocation] [money] NULL,
[BilDate] [datetime] NULL,
[ExhaustionDateID] [float] NULL,
[PaymentDueDate] [datetime] NULL,
[BillNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [float] NULL,
[BillTypeID] [float] NULL
) ON [PRIMARY]
GO
