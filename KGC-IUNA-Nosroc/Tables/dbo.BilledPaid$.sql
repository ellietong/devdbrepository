CREATE TABLE [dbo].[BilledPaid$]
(
[MatterID] [float] NULL,
[SubscriptionCompany] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubscriptionCompanySolvency] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavBillID] [float] NULL,
[Month] [float] NULL,
[Year] [float] NULL,
[Billed] [money] NULL,
[Paid] [money] NULL,
[PaidDate] [datetime] NULL,
[F10] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F11] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F12] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F13] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F14] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
