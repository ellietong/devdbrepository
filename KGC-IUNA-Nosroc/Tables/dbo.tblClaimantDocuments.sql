CREATE TABLE [dbo].[tblClaimantDocuments]
(
[ClaimantDocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[DocumentNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantDocumentTypeID] [int] NULL,
[Folder] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentLink] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTimeEntered] [datetime] NULL CONSTRAINT [DF_tblClaimantDocuments_DateTimeEntered] DEFAULT (getdate()),
[NavInjPartyKey] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavReferenceID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDocumentType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTitle] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPDocId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantDocuments] ADD CONSTRAINT [PK_tblLegalDocs] PRIMARY KEY CLUSTERED  ([ClaimantDocumentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantDocuments] ADD CONSTRAINT [FK_tblClaimantDocuments_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantDocuments] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantDocuments] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantDocuments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantDocuments] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimantDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantDocuments] TO [power_user]
GO
