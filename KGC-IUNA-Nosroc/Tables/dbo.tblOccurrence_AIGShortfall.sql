CREATE TABLE [dbo].[tblOccurrence_AIGShortfall]
(
[OccurrenceID] [bigint] NOT NULL IDENTITY(1, 1),
[AggregateApplies] [bit] NOT NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_AggregateApplies] DEFAULT ((0)),
[ExpectedPayDate] [datetime] NULL,
[InClaim] [bit] NOT NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_InClaim] DEFAULT ((0)),
[OccurrenceName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceAddress] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceStateID] [int] NULL,
[OccurrenceGroupID] [int] NULL,
[CorporateHistoryID] [int] NULL,
[OccurrenceTypeID] [int] NULL,
[MatterID] [int] NULL,
[ClaimID] [bigint] NULL,
[Segment] [int] NULL,
[TriggerStart1] [datetime] NULL,
[TriggerStart1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriggerEnd1] [datetime] NULL,
[TriggerEnd1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriggerStart2] [datetime] NULL,
[TriggerEnd2] [datetime] NULL,
[TriggerStart3] [datetime] NULL,
[TriggerEnd3] [datetime] NULL,
[TriggerStart4] [datetime] NULL,
[TriggerEnd4] [datetime] NULL,
[Cost1] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost1] DEFAULT ((0)),
[Cost1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost2] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost2] DEFAULT ((0)),
[Cost2Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost3] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost3] DEFAULT ((0)),
[Cost3Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost4] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost4] DEFAULT ((0)),
[Cost5] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost5] DEFAULT ((0)),
[Cost6] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost6] DEFAULT ((0)),
[Cost7] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost7] DEFAULT ((0)),
[Cost8] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost8] DEFAULT ((0)),
[Cost9] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost9] DEFAULT ((0)),
[Cost10] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost10] DEFAULT ((0)),
[Cost11] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost11] DEFAULT ((0)),
[Cost12] [money] NULL CONSTRAINT [DF_tblOccurrence_AIGShortfall_Cost12] DEFAULT ((0)),
[OccurrenceNote] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccurrence_AIGShortfall] ADD CONSTRAINT [PK_tblOccurrence_AIGShortfall] PRIMARY KEY CLUSTERED  ([OccurrenceID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrence_AIGShortfall] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrence_AIGShortfall] TO [base_user]
GRANT CONTROL ON  [dbo].[tblOccurrence_AIGShortfall] TO [Occurrence_User]
GRANT SELECT ON  [dbo].[tblOccurrence_AIGShortfall] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[tblOccurrence_AIGShortfall] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[tblOccurrence_AIGShortfall] TO [Occurrence_User]
GRANT SELECT ON  [dbo].[tblOccurrence_AIGShortfall] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccurrence_AIGShortfall] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccurrence_AIGShortfall] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'AggregateApplies'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'AggregateApplies'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'AggregateApplies'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'AggregateApplies'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'AggregateApplies'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'ClaimID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'ClaimID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'ClaimID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'ClaimID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'ClaimID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'CorporateHistoryID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'CorporateHistoryID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'CorporateHistoryID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'CorporateHistoryID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'CorporateHistoryID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost1'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost1'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost1'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost1'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost1'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost10'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost10'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost10'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost10'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost10'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost11'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost11'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost11'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost11'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost11'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost12'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost12'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost12'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost12'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost12'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost1Explanation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost1Explanation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost1Explanation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost1Explanation'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost1Explanation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost2'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost2'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost2'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost2'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost2'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost2Explanation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost2Explanation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost2Explanation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost2Explanation'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost2Explanation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost3'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost3'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost3'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost3'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost3'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost3Explanation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost3Explanation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost3Explanation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost3Explanation'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost3Explanation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost4'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost4'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost4'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost4'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost4'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost5'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost5'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost5'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost5'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost5'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost6'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost6'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost6'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost6'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost6'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost7'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost7'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost7'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost7'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost7'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost8'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost8'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost8'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost8'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost8'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost9'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost9'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost9'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost9'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Cost9'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'ExpectedPayDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'ExpectedPayDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'ExpectedPayDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'ExpectedPayDate'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'ExpectedPayDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'InClaim'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'InClaim'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'InClaim'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'InClaim'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'InClaim'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'MatterID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'MatterID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'MatterID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'MatterID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'MatterID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceAddress'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceAddress'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceAddress'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceAddress'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceAddress'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceCity'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceCity'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceCity'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceCity'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceCity'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceGroupID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceGroupID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceGroupID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceGroupID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceGroupID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceName'
GO
DECLARE @xp int
SELECT @xp=4867
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceName'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceNote'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceNote'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceNote'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceNote'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceNote'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceStateID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceStateID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceStateID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceStateID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceStateID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Segment'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Segment'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Segment'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Segment'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'Segment'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd1'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd1'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd1'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd1'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd1'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd1Explanation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd1Explanation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd1Explanation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd1Explanation'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd1Explanation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd2'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd2'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd2'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd2'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd2'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd3'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd3'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd3'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd3'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd3'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd4'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd4'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd4'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd4'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerEnd4'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart1'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart1'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart1'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart1'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart1'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart1Explanation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart1Explanation'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart1Explanation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart1Explanation'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart1Explanation'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart2'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart2'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart2'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart2'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart2'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart3'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart3'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart3'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart3'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart3'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart4'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart4'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart4'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart4'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrence_AIGShortfall', 'COLUMN', N'TriggerStart4'
GO
