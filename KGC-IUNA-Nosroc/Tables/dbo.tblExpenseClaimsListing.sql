CREATE TABLE [dbo].[tblExpenseClaimsListing]
(
[ExpenseClaimsListingID] [int] NOT NULL IDENTITY(1, 1),
[ExpenseCurveID] [int] NOT NULL,
[FirstExposureDate] [datetime] NULL,
[FirstDiagnosisDate] [datetime] NULL,
[ExpensePercentage] [float] NULL,
[MatterID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExpenseClaimsListing] ADD CONSTRAINT [PK_tblExpenseClaimsListing] PRIMARY KEY CLUSTERED  ([ExpenseClaimsListingID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExpenseClaimsListing] TO [base_user]
GRANT SELECT ON  [dbo].[tblExpenseClaimsListing] TO [base_user]
GRANT SELECT ON  [dbo].[tblExpenseClaimsListing] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblExpenseClaimsListing] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblExpenseClaimsListing] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblExpenseClaimsListing] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblExpenseClaimsListing] TO [power_user]
GRANT INSERT ON  [dbo].[tblExpenseClaimsListing] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExpenseClaimsListing] TO [power_user]
GO
