CREATE TABLE [dbo].[mtblBundledExposure]
(
[InjuredPartyID] [bigint] NULL,
[ClaimSequenceNum] [int] NULL,
[SequenceNum] [int] NULL,
[PrimaryFlag] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccupationNum] [int] NULL,
[OccupationDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccupationCategoryNum] [int] NULL,
[OccupationCategoryDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteCategoryNum] [int] NULL,
[JobsiteCategoryDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExposureFromDate] [datetime] NULL,
[ExposureToDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledExposure] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledExposure] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledExposure] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledExposure] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledExposure] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledExposure] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledExposure] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledExposure] TO [power_user]
GO
