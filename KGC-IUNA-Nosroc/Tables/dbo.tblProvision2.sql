CREATE TABLE [dbo].[tblProvision2]
(
[Provision2ID] [int] NOT NULL,
[Provision2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Provision2Discount] [real] NULL
) ON [PRIMARY]
GO
