CREATE TABLE [dbo].[tblClaim]
(
[ClaimID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[IsCurrentClaim] [bit] NOT NULL CONSTRAINT [DF_tblClaim_NEW_IsCurrentClaim] DEFAULT ((0)),
[ClaimSequenceID] [int] NULL,
[PlaintiffDiseaseID] [int] NULL,
[PlaintiffDiseaseDiagnosisDate] [datetime] NULL,
[DoctorID] [int] NULL,
[DefenseDiseaseID] [int] NULL,
[DefenseDiseaseDiagnosisDate] [datetime] NULL,
[MatterID] [int] NOT NULL,
[NavClaimKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimSequence] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPlaintiffDisease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDefenseDisease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullyDocumented] [bit] NULL,
[CauseOfInjuryID] [int] NULL,
[ILODoctorID] [int] NULL,
[ILOProfusion] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILODate] [datetime] NULL,
[PFTDoctorID] [int] NULL,
[PFTDate] [datetime] NULL,
[PFTTypeID] [int] NULL,
[PFTFVC_Actual] [float] NULL,
[PFTFVC_Predicted] [float] NULL,
[PFTFEV1_Actual] [float] NULL,
[PFTFEV1_Predicted] [float] NULL,
[PFTFEV1andFVC_Actual] [float] NULL,
[PFTFEV1andFVC_Predicted] [float] NULL,
[PFTTLC_Actual] [float] NULL,
[PFTTLC_Predicted] [float] NULL,
[PFTDLCO_Actual] [float] NULL,
[PFTDLCO_Predicted] [float] NULL,
[PFTTV] [float] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [PK_mBtblClaim_NEW] PRIMARY KEY CLUSTERED  ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>] ON [dbo].[tblClaim] ([MatterID]) INCLUDE ([ClaimantPersonalInfoID], [ClaimID], [ClaimSequenceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaim_MatterID] ON [dbo].[tblClaim] ([MatterID]) INCLUDE ([ClaimID], [IsCurrentClaim]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [FK_tblClaim_NEW_tblClaimantPersonalInfo_NEW] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaim] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblClaim] TO [monahanc]
GRANT SELECT ON  [dbo].[tblClaim] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaim] TO [power_user]
GO
