CREATE TABLE [dbo].[tblCarrier_ALT]
(
[CarrierID] [int] NOT NULL IDENTITY(1, 1),
[CarrierName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CarrierNickname] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierCurrentName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierSolvency] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierSolvencyStatus] [int] NULL,
[Signatory] [bit] NULL,
[CarrierGroupID] [int] NULL,
[CarrierNote] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SolvencySource] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateUpdated] [smalldatetime] NULL,
[DateInsolvent] [smalldatetime] NULL,
[CarrierSolvencyDetail] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AMBestNo] [int] NULL,
[PrincipalPlaceBusinessCity] [int] NULL,
[PrincipalPlaceBusinessState] [int] NULL,
[PrincipalPlaceBusinessCountry] [int] NULL,
[PolicyHolder] [bit] NULL,
[GraceIgnore] [bit] NULL,
[Expr1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCarrier_ALT] ADD CONSTRAINT [PK_tblCarrier_1] PRIMARY KEY CLUSTERED  ([CarrierID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCarrier_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblCarrier_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblCarrier_ALT] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblCarrier_ALT] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblCarrier_ALT] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblCarrier_ALT] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblCarrier_ALT] TO [power_user]
GRANT INSERT ON  [dbo].[tblCarrier_ALT] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCarrier_ALT] TO [power_user]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'AMBestNo'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'AMBestNo'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'AMBestNo'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'AMBestNo'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'AMBestNo'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierCurrentName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierCurrentName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierCurrentName'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierCurrentName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierCurrentName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierGroupID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierGroupID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierGroupID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierGroupID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierGroupID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierName'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierName'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierName'
GO
DECLARE @xp smallint
SELECT @xp=2722
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierName'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierName'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierNickname'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierNickname'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierNickname'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierNickname'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierNickname'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierNote'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierNote'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierNote'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierNote'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierNote'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvency'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvency'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvency'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvency'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvency'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvencyDetail'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvencyDetail'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvencyDetail'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvencyDetail'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvencyDetail'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvencyStatus'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvencyStatus'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvencyStatus'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvencyStatus'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'CarrierSolvencyStatus'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'DateInsolvent'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'DateInsolvent'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'DateInsolvent'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'DateInsolvent'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'DateInsolvent'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'DateUpdated'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'DateUpdated'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'DateUpdated'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'DateUpdated'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'DateUpdated'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'Expr1'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'Expr1'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'Expr1'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'Expr1'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'Expr1'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'GraceIgnore'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'GraceIgnore'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'GraceIgnore'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'GraceIgnore'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'GraceIgnore'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PolicyHolder'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PolicyHolder'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PolicyHolder'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PolicyHolder'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PolicyHolder'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessCity'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessCity'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessCity'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessCity'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessCity'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessCountry'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessCountry'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessCountry'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessCountry'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessCountry'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessState'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessState'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessState'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessState'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'PrincipalPlaceBusinessState'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'Signatory'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'Signatory'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'Signatory'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'Signatory'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'Signatory'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'SolvencySource'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'SolvencySource'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'SolvencySource'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'SolvencySource'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrier_ALT', 'COLUMN', N'SolvencySource'
GO
