CREATE TABLE [dbo].[tblPlaintiffCounsel]
(
[PlaintiffCounselID] [int] NOT NULL IDENTITY(1, 1),
[PlaintiffCounsel] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCTaxIDNum] [bigint] NULL,
[PCAddress1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCAddress2] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[PCZip] [int] NULL,
[PCPhone] [bigint] NULL,
[PCExtension] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPlaintiffCounsel] ADD CONSTRAINT [PK_tblPlaintiffCounsel] PRIMARY KEY CLUSTERED  ([PlaintiffCounselID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblPlaintiffCounsel] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblPlaintiffCounsel] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel] TO [power_user]
GRANT INSERT ON  [dbo].[tblPlaintiffCounsel] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPlaintiffCounsel] TO [power_user]
GO
