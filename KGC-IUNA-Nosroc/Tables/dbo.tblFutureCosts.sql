CREATE TABLE [dbo].[tblFutureCosts]
(
[FutureCostID] [int] NOT NULL IDENTITY(1, 1),
[SiteID] [int] NULL,
[PayYear] [int] NULL,
[TotalCosts] [money] NULL,
[LiabPercent] [decimal] (18, 4) NULL,
[PVD] [money] NULL,
[NRD] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFutureCosts] ADD CONSTRAINT [PK_mEtblFutureCosts] PRIMARY KEY CLUSTERED  ([FutureCostID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFutureCosts] ADD CONSTRAINT [FK_tblFutureCosts_tblEnviroSitesDetail] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[tblEnviroSitesDetail] ([SiteID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblFutureCosts] TO [base_user]
GRANT SELECT ON  [dbo].[tblFutureCosts] TO [base_user]
GRANT SELECT ON  [dbo].[tblFutureCosts] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblFutureCosts] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblFutureCosts] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblFutureCosts] TO [Enviro_User]
GRANT SELECT ON  [dbo].[tblFutureCosts] TO [power_user]
GRANT INSERT ON  [dbo].[tblFutureCosts] TO [power_user]
GRANT UPDATE ON  [dbo].[tblFutureCosts] TO [power_user]
GO
