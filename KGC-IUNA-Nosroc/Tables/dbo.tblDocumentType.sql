CREATE TABLE [dbo].[tblDocumentType]
(
[DocumentTypeID] [int] NOT NULL IDENTITY(1, 1),
[DocumentType] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDocumentType] ADD CONSTRAINT [PK_mEtblDocumentType] PRIMARY KEY CLUSTERED  ([DocumentTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDocumentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocumentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocumentType] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDocumentType] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDocumentType] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDocumentType] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblDocumentType] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblDocumentType] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblDocumentType] TO [Enviro_User]
GRANT SELECT ON  [dbo].[tblDocumentType] TO [power_user]
GRANT INSERT ON  [dbo].[tblDocumentType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDocumentType] TO [power_user]
GO
