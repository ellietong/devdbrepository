CREATE TABLE [dbo].[tblCashFlowNew]
(
[CashFlowID] [bigint] NOT NULL IDENTITY(1, 1),
[BankID] [int] NULL,
[TransactionTypeID] [int] NULL,
[Date] [datetime] NULL,
[Indemnity] [money] NOT NULL,
[DefenseCounsel] [money] NOT NULL,
[Nonbillable_DefenseCounsel] [money] NULL,
[MiscFees] [money] NOT NULL,
[Shortfall] [money] NOT NULL,
[Bundled] [money] NOT NULL,
[CashReceipts] [money] NOT NULL,
[Total] [money] NOT NULL,
[Comment] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [int] NOT NULL,
[NavTransactionType] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCashFlowNew] ADD CONSTRAINT [PK_tblCashFlowNew] PRIMARY KEY CLUSTERED  ([CashFlowID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCashFlowNew] TO [base_user]
GRANT SELECT ON  [dbo].[tblCashFlowNew] TO [base_user]
GRANT SELECT ON  [dbo].[tblCashFlowNew] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblCashFlowNew] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblCashFlowNew] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblCashFlowNew] TO [power_user]
GRANT INSERT ON  [dbo].[tblCashFlowNew] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCashFlowNew] TO [power_user]
GO
