CREATE TABLE [dbo].[tblBatesRanges]
(
[BatesRangeID] [int] NOT NULL IDENTITY(1, 1),
[CostRecordID] [int] NOT NULL,
[BatesMin] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatesMax] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBatesRanges] ADD CONSTRAINT [PK_mEztblBatesRanges] PRIMARY KEY CLUSTERED  ([BatesRangeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBatesRanges] ADD CONSTRAINT [FK_ztblBatesRanges_tblSiteCosts] FOREIGN KEY ([CostRecordID]) REFERENCES [dbo].[tblSiteCosts] ([CostRecordID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBatesRanges] TO [base_user]
GRANT SELECT ON  [dbo].[tblBatesRanges] TO [base_user]
GRANT SELECT ON  [dbo].[tblBatesRanges] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblBatesRanges] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblBatesRanges] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblBatesRanges] TO [Enviro_User]
GRANT SELECT ON  [dbo].[tblBatesRanges] TO [power_user]
GRANT INSERT ON  [dbo].[tblBatesRanges] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBatesRanges] TO [power_user]
GO
