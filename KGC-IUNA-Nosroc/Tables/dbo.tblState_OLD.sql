CREATE TABLE [dbo].[tblState_OLD]
(
[StateID] [int] NOT NULL IDENTITY(1, 1),
[State] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullNameState] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateDiscount] [real] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblState_OLD] ADD CONSTRAINT [PK_tblState] PRIMARY KEY CLUSTERED  ([StateID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblState_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblState_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblState_OLD] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblState_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblState_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblState_OLD] TO [power_user]
GO
