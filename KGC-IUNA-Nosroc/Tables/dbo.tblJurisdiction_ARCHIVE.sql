CREATE TABLE [dbo].[tblJurisdiction_ARCHIVE]
(
[JurisdictionID_OLD] [int] NOT NULL IDENTITY(1, 1),
[Jurisdiction] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JurisdictionID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblJurisdiction_ARCHIVE] ADD CONSTRAINT [PK_mBtblJurisdiction] PRIMARY KEY CLUSTERED  ([JurisdictionID_OLD]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblJurisdiction_ARCHIVE] TO [base_user]
GRANT SELECT ON  [dbo].[tblJurisdiction_ARCHIVE] TO [base_user]
GRANT SELECT ON  [dbo].[tblJurisdiction_ARCHIVE] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblJurisdiction_ARCHIVE] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblJurisdiction_ARCHIVE] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblJurisdiction_ARCHIVE] TO [power_user]
GRANT INSERT ON  [dbo].[tblJurisdiction_ARCHIVE] TO [power_user]
GRANT UPDATE ON  [dbo].[tblJurisdiction_ARCHIVE] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblJurisdiction_ARCHIVE', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblJurisdiction_ARCHIVE', 'COLUMN', N'Jurisdiction'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblJurisdiction_ARCHIVE', 'COLUMN', N'Jurisdiction'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblJurisdiction_ARCHIVE', 'COLUMN', N'Jurisdiction'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblJurisdiction_ARCHIVE', 'COLUMN', N'JurisdictionID_OLD'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblJurisdiction_ARCHIVE', 'COLUMN', N'JurisdictionID_OLD'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblJurisdiction_ARCHIVE', 'COLUMN', N'JurisdictionID_OLD'
GO
