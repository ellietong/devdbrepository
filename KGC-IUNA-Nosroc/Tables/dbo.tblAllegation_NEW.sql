CREATE TABLE [dbo].[tblAllegation_NEW]
(
[AllegationID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[DefendantCompanyID] [int] NULL,
[AllegationTypeID] [int] NULL,
[DamageID] [int] NULL,
[MaterialID] [int] NULL,
[NavClaimLawsuitKey] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDefendantCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavAllegationType] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDamage] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavMaterial] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegation_NEW] ADD CONSTRAINT [PK__tblAllegation_NEW1__414EAC47] PRIMARY KEY CLUSTERED  ([AllegationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllegation_NEW] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllegation_NEW] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllegation_NEW] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblAllegation_NEW] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblAllegation_NEW] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblAllegation_NEW] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblAllegation_NEW] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllegation_NEW] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllegation_NEW] TO [power_user]
GO
