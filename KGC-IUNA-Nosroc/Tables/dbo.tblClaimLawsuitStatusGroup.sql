CREATE TABLE [dbo].[tblClaimLawsuitStatusGroup]
(
[ClaimLawsuitStatusGroupID] [int] NOT NULL IDENTITY(1, 1),
[StatusGroupID] [int] NULL,
[ClaimLawsuitStatusID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusGroup] ADD CONSTRAINT [PK_tblClaimLawsuitStatusGroup] PRIMARY KEY CLUSTERED  ([ClaimLawsuitStatusGroupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusGroup] ADD CONSTRAINT [FK_tblClaimLawsuitStatusGroup_tblClaimLawsuitStatus] FOREIGN KEY ([ClaimLawsuitStatusID]) REFERENCES [dbo].[tblClaimLawsuitStatus] ([ClaimLawsuitStatusID])
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusGroup] ADD CONSTRAINT [FK_tblClaimLawsuitStatusGroup_tblStatusGroup] FOREIGN KEY ([StatusGroupID]) REFERENCES [dbo].[tblStatusGroup] ([StatusGroupID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusGroup] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusGroup] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusGroup] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusGroup] TO [power_user]
GO
