CREATE TABLE [dbo].[mtblBundledFPayment]
(
[InjuredParty] [bigint] NULL,
[ClaimSequenceNum] [int] NULL,
[PaymentID] [int] NULL,
[AllocationAmount] [money] NULL,
[RefundAmount] [money] NULL,
[PaidDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledFPayment] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledFPayment] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledFPayment] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledFPayment] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledFPayment] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledFPayment] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledFPayment] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledFPayment] TO [power_user]
GO
