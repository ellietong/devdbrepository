CREATE TABLE [dbo].[ztblColumn]
(
[ColumnID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[ColumnStartDate] [datetime] NULL,
[ColumnEndDate] [datetime] NULL,
[ColumnDays] [int] NULL,
[ColumnPerOcc] [money] NULL,
[ColumnCumAllocation] [money] NULL,
[InsuranceProgramID] [int] NULL,
[VerticalUncollapse] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblColumn] ADD CONSTRAINT [PK_ztblColumn] PRIMARY KEY NONCLUSTERED  ([ColumnID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ztblColumn] TO [Allocation_User]
GRANT INSERT ON  [dbo].[ztblColumn] TO [Allocation_User]
GRANT DELETE ON  [dbo].[ztblColumn] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[ztblColumn] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[ztblColumn] TO [base_user]
GRANT SELECT ON  [dbo].[ztblColumn] TO [base_user]
GRANT SELECT ON  [dbo].[ztblColumn] TO [power_user]
GRANT INSERT ON  [dbo].[ztblColumn] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblColumn] TO [power_user]
GO
