CREATE TABLE [dbo].[tblForecast_RevisedPending]
(
[ForecastYear] [float] NULL,
[ForecastIndemnity] [money] NULL,
[ForecastDefense] [money] NULL
) ON [PRIMARY]
GO
