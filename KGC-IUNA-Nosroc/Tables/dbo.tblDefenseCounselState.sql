CREATE TABLE [dbo].[tblDefenseCounselState]
(
[DefenseCounselStateID] [int] NOT NULL IDENTITY(1, 1),
[DefenseCounselID] [int] NOT NULL,
[StateID] [int] NOT NULL,
[ResponsibleDefenseCounselID] [int] NOT NULL,
[IsActiveCounsel] [bit] NOT NULL,
[DefenseCounselAddressID] [int] NULL,
[ResponsibleDefenseCounselAddressID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseCounselState] ADD CONSTRAINT [PK_mBtblDefenseCounselState] PRIMARY KEY CLUSTERED  ([DefenseCounselStateID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselState] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselState] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselState] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseCounselState] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseCounselState] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefenseCounselState] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseCounselState] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseCounselState] TO [power_user]
GO
