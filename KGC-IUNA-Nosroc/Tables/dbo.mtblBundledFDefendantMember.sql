CREATE TABLE [dbo].[mtblBundledFDefendantMember]
(
[InjuredPartyID] [bigint] NULL,
[ClaimSequenceNum] [int] NULL,
[CompanyNum] [int] NULL,
[CompanyName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompanyTypeCode] [int] NULL,
[CompanyTypeDescription] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledFDefendantMember] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledFDefendantMember] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledFDefendantMember] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledFDefendantMember] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledFDefendantMember] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledFDefendantMember] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledFDefendantMember] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledFDefendantMember] TO [power_user]
GO
