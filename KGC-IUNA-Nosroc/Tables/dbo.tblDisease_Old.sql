CREATE TABLE [dbo].[tblDisease_Old]
(
[DiseaseID] [int] NOT NULL IDENTITY(1, 1),
[Disease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighLevelDiseaseID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDisease_Old] ADD CONSTRAINT [PK_mBtblDisease] PRIMARY KEY CLUSTERED  ([DiseaseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDisease_Old] TO [base_user]
GRANT SELECT ON  [dbo].[tblDisease_Old] TO [base_user]
GRANT SELECT ON  [dbo].[tblDisease_Old] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDisease_Old] TO [power_user]
GRANT INSERT ON  [dbo].[tblDisease_Old] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDisease_Old] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'Disease'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'Disease'
GO
DECLARE @xp int
SELECT @xp=4665
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'Disease'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'Disease'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'Disease'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'Disease'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'DiseaseID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'DiseaseID'
GO
DECLARE @xp int
SELECT @xp=1950
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'DiseaseID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'DiseaseID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'DiseaseID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'DiseaseID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'HighLevelDiseaseID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'HighLevelDiseaseID'
GO
DECLARE @xp int
SELECT @xp=1950
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'HighLevelDiseaseID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'HighLevelDiseaseID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'HighLevelDiseaseID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDisease_Old', 'COLUMN', N'HighLevelDiseaseID'
GO
