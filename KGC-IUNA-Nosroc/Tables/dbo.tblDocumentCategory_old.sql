CREATE TABLE [dbo].[tblDocumentCategory_old]
(
[DocCategoryID] [int] NOT NULL IDENTITY(1, 1),
[DocCategory] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDocumentCategory_old] ADD CONSTRAINT [PK_tblDocumentCategory] PRIMARY KEY CLUSTERED  ([DocCategoryID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDocumentCategory_old] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocumentCategory_old] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocumentCategory_old] TO [power_user]
GRANT INSERT ON  [dbo].[tblDocumentCategory_old] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDocumentCategory_old] TO [power_user]
GO
