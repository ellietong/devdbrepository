CREATE TABLE [dbo].[mtblForecastClaims_20120508]
(
[MatterID] [int] NOT NULL,
[claimantpersonalinfoID] [bigint] NOT NULL,
[claimID] [bigint] NOT NULL,
[iscurrentclaim] [bit] NOT NULL,
[claimsequenceID] [int] NULL,
[claimlawsuitID] [bigint] NOT NULL,
[isprimaryforClaim] [bit] NULL,
[HighLevelDisease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementAmount] [money] NULL,
[SettlementDate] [datetime] NULL,
[Jurisdiction] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[ServiceDate] [datetime] NULL,
[ReceivedDate] [datetime] NULL,
[ProcessedDate] [datetime] NULL,
[StatusDate] [datetime] NULL,
[ClaimLawsuitStatusID] [int] NULL,
[ClaimLawsuitStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusGroup] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UseClaimFlag] [bit] NULL,
[MinProcDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblForecastClaims_20120508] TO [base_user]
GRANT SELECT ON  [dbo].[mtblForecastClaims_20120508] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[mtblForecastClaims_20120508] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblForecastClaims_20120508] TO [Claims_User]
GO
