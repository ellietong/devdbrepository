CREATE TABLE [dbo].[tblProductBrand]
(
[ProductBrandID] [int] NOT NULL IDENTITY(1, 1),
[ProductBrand] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProductBrand] ADD CONSTRAINT [PK_tblClaimantProductBrand] PRIMARY KEY CLUSTERED  ([ProductBrandID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProductBrand] TO [base_user]
GRANT SELECT ON  [dbo].[tblProductBrand] TO [base_user]
GRANT SELECT ON  [dbo].[tblProductBrand] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblProductBrand] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblProductBrand] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblProductBrand] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblProductBrand] TO [power_user]
GRANT INSERT ON  [dbo].[tblProductBrand] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProductBrand] TO [power_user]
GO
