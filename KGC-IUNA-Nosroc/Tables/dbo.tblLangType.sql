CREATE TABLE [dbo].[tblLangType]
(
[LangTypeID] [int] NOT NULL IDENTITY(1, 1),
[LangType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLangType] ADD CONSTRAINT [PK_mtblLangType] PRIMARY KEY CLUSTERED  ([LangTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLangType] TO [base_user]
GRANT SELECT ON  [dbo].[tblLangType] TO [base_user]
GRANT SELECT ON  [dbo].[tblLangType] TO [Language_User]
GRANT INSERT ON  [dbo].[tblLangType] TO [Language_User]
GRANT DELETE ON  [dbo].[tblLangType] TO [Language_User]
GRANT UPDATE ON  [dbo].[tblLangType] TO [Language_User]
GRANT SELECT ON  [dbo].[tblLangType] TO [power_user]
GRANT INSERT ON  [dbo].[tblLangType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLangType] TO [power_user]
GO
