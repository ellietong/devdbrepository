CREATE TABLE [dbo].[tblOccurrenceGroup]
(
[OccurrenceGroupID] [int] NOT NULL IDENTITY(1, 1),
[OccurrenceGroup] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccGrpTriggerStart] [datetime] NULL,
[OccGrpDiscount] [real] NULL,
[SiteID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccurrenceGroup] ADD CONSTRAINT [PK_tblOccurrenceGroup] PRIMARY KEY CLUSTERED  ([OccurrenceGroupID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrenceGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrenceGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrenceGroup] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[tblOccurrenceGroup] TO [Occurrence_User]
GRANT DELETE ON  [dbo].[tblOccurrenceGroup] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[tblOccurrenceGroup] TO [Occurrence_User]
GRANT SELECT ON  [dbo].[tblOccurrenceGroup] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccurrenceGroup] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccurrenceGroup] TO [power_user]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccGrpDiscount'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccGrpDiscount'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccGrpDiscount'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccGrpDiscount'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccGrpDiscount'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccGrpTriggerStart'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccGrpTriggerStart'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccGrpTriggerStart'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccGrpTriggerStart'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccGrpTriggerStart'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccurrenceGroup'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccurrenceGroup'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccurrenceGroup'
GO
DECLARE @xp smallint
SELECT @xp=5370
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccurrenceGroup'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccurrenceGroup'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccurrenceGroupID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccurrenceGroupID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccurrenceGroupID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccurrenceGroupID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'OccurrenceGroupID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'SiteID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'SiteID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'SiteID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'SiteID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblOccurrenceGroup', 'COLUMN', N'SiteID'
GO
