CREATE TABLE [dbo].[mtblSubPayments]
(
[MatterID] [float] NULL,
[SubscriberID] [float] NULL,
[NavBillID] [float] NULL,
[PaymentDate] [datetime] NULL,
[TotalPaid] [money] NULL
) ON [PRIMARY]
GO
