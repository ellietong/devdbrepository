CREATE TABLE [dbo].[tblHighLevelDisease_Old]
(
[HighLevelDiseaseID] [int] NOT NULL IDENTITY(1, 1),
[HighLevelDisease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblHighLevelDisease_Old] ADD CONSTRAINT [PK__mBtblHighLevelDi__1BB31344] PRIMARY KEY CLUSTERED  ([HighLevelDiseaseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblHighLevelDisease_Old] TO [base_user]
GRANT SELECT ON  [dbo].[tblHighLevelDisease_Old] TO [base_user]
GRANT SELECT ON  [dbo].[tblHighLevelDisease_Old] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblHighLevelDisease_Old] TO [power_user]
GRANT INSERT ON  [dbo].[tblHighLevelDisease_Old] TO [power_user]
GRANT UPDATE ON  [dbo].[tblHighLevelDisease_Old] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1000000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', 'COLUMN', N'HighLevelDisease'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', 'COLUMN', N'HighLevelDisease'
GO
DECLARE @xp int
SELECT @xp=1710
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', 'COLUMN', N'HighLevelDisease'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', 'COLUMN', N'HighLevelDiseaseID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', 'COLUMN', N'HighLevelDiseaseID'
GO
DECLARE @xp int
SELECT @xp=1890
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblHighLevelDisease_Old', 'COLUMN', N'HighLevelDiseaseID'
GO
