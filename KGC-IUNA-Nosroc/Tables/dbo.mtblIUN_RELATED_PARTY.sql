CREATE TABLE [dbo].[mtblIUN_RELATED_PARTY]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELATIONSHIP] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEGAL_CAPACITY] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblIUN_RELATED_PARTY] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_RELATED_PARTY] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_RELATED_PARTY] TO [power_user]
GRANT INSERT ON  [dbo].[mtblIUN_RELATED_PARTY] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblIUN_RELATED_PARTY] TO [power_user]
GO
