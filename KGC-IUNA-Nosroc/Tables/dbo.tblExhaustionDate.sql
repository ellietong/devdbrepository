CREATE TABLE [dbo].[tblExhaustionDate]
(
[ExhaustionDateID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionTypeID] [int] NOT NULL,
[ExhaustionDate] [datetime] NOT NULL,
[AllocationID] [int] NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustionDate] ADD CONSTRAINT [PK_tblExhaustionDate] PRIMARY KEY NONCLUSTERED  ([ExhaustionDateID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustionDate] ADD CONSTRAINT [FK_tblExhaustionDate_tblExhaustionType] FOREIGN KEY ([ExhaustionTypeID]) REFERENCES [dbo].[tblExhaustionType] ([ExhaustionTypeID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustionDate] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustionDate] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustionDate] TO [Exhaustion_User]
GRANT INSERT ON  [dbo].[tblExhaustionDate] TO [Exhaustion_User]
GRANT DELETE ON  [dbo].[tblExhaustionDate] TO [Exhaustion_User]
GRANT UPDATE ON  [dbo].[tblExhaustionDate] TO [Exhaustion_User]
GRANT SELECT ON  [dbo].[tblExhaustionDate] TO [power_user]
GRANT INSERT ON  [dbo].[tblExhaustionDate] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExhaustionDate] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionDate', 'COLUMN', N'ExhaustionTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionDate', 'COLUMN', N'ExhaustionTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionDate', 'COLUMN', N'ExhaustionTypeID'
GO
