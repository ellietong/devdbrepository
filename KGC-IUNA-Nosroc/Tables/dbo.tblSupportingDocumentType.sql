CREATE TABLE [dbo].[tblSupportingDocumentType]
(
[SupportingDocumentTypeID] [int] NOT NULL IDENTITY(1, 1),
[SupportingDocumentType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSupportingDocumentType] ADD CONSTRAINT [PK_tblSupportingDocumentType] PRIMARY KEY CLUSTERED  ([SupportingDocumentTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSupportingDocumentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSupportingDocumentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSupportingDocumentType] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblSupportingDocumentType] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblSupportingDocumentType] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblSupportingDocumentType] TO [Notice_User]
GRANT SELECT ON  [dbo].[tblSupportingDocumentType] TO [power_user]
GRANT INSERT ON  [dbo].[tblSupportingDocumentType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSupportingDocumentType] TO [power_user]
GO
