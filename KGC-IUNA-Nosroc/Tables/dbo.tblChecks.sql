CREATE TABLE [dbo].[tblChecks]
(
[AutoID] [int] NOT NULL IDENTITY(1, 1),
[MatterID] [int] NOT NULL,
[FirmID] [int] NOT NULL,
[CheckNo] [int] NOT NULL,
[CheckDate] [date] NOT NULL,
[CheckAmount] [decimal] (18, 2) NOT NULL,
[AmountDesc] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentLine] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCounselAddress] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FundTypeID] [int] NULL,
[GeneralComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementDetails] [nvarchar] (800) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckPDF] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblChecks] ADD CONSTRAINT [PK_mtblChecks] PRIMARY KEY CLUSTERED  ([AutoID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblChecks] TO [CheckWriter_User]
GO
