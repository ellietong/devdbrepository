CREATE TABLE [dbo].[ExpenseIncurredByYear_2001]
(
[Year] [float] NULL,
[Defense] [money] NULL,
[MatterID] [float] NULL,
[Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ExpenseIncurredByYear_2001] TO [base_user]
GRANT SELECT ON  [dbo].[ExpenseIncurredByYear_2001] TO [Claims_User]
GRANT INSERT ON  [dbo].[ExpenseIncurredByYear_2001] TO [Claims_User]
GRANT DELETE ON  [dbo].[ExpenseIncurredByYear_2001] TO [Claims_User]
GRANT UPDATE ON  [dbo].[ExpenseIncurredByYear_2001] TO [Claims_User]
GO
