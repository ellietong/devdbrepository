CREATE TABLE [dbo].[tblBillGroup]
(
[BillGroupID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillGroup] ADD CONSTRAINT [PK_tblBillGroup] PRIMARY KEY CLUSTERED  ([BillGroupID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillGroup] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblBillGroup] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblBillGroup] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblBillGroup] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblBillGroup] TO [power_user]
GRANT INSERT ON  [dbo].[tblBillGroup] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBillGroup] TO [power_user]
GO
