CREATE TABLE [dbo].[tblDefenseInvoiceType]
(
[InvoiceTypeID] [int] NOT NULL IDENTITY(1, 1),
[InvoiceType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseInvoiceType] ADD CONSTRAINT [PK_tblDefenseInvoiceType] PRIMARY KEY CLUSTERED  ([InvoiceTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoiceType] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoiceType] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoiceType] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseInvoiceType] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseInvoiceType] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefenseInvoiceType] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseInvoiceType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseInvoiceType] TO [power_user]
GO
