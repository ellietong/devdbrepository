CREATE TABLE [dbo].[tblPaymentArchive]
(
[PaymentID] [bigint] NOT NULL IDENTITY(1, 1),
[LedgerID] [bigint] NOT NULL,
[Payment] [money] NULL,
[PaymentDate] [datetime] NULL,
[PaymentNote] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPaymentArchive] ADD CONSTRAINT [PK_tblPayment] PRIMARY KEY CLUSTERED  ([PaymentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentArchive] TO [base_user]
GRANT SELECT ON  [dbo].[tblPaymentArchive] TO [base_user]
GRANT SELECT ON  [dbo].[tblPaymentArchive] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblPaymentArchive] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblPaymentArchive] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblPaymentArchive] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblPaymentArchive] TO [power_user]
GRANT INSERT ON  [dbo].[tblPaymentArchive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPaymentArchive] TO [power_user]
GO
