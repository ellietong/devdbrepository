CREATE TABLE [dbo].[tblForms]
(
[FormID] [int] NOT NULL IDENTITY(1, 1),
[FormName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FormDescription] [nvarchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IPValues] [nvarchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblForms] ADD CONSTRAINT [PK_tblForms] PRIMARY KEY CLUSTERED  ([FormID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblForms] TO [base_user]
GRANT SELECT ON  [dbo].[tblForms] TO [base_user]
GRANT SELECT ON  [dbo].[tblForms] TO [power_user]
GRANT INSERT ON  [dbo].[tblForms] TO [power_user]
GRANT UPDATE ON  [dbo].[tblForms] TO [power_user]
GRANT SELECT ON  [dbo].[tblForms] TO [Reports_User]
GRANT INSERT ON  [dbo].[tblForms] TO [Reports_User]
GRANT DELETE ON  [dbo].[tblForms] TO [Reports_User]
GRANT UPDATE ON  [dbo].[tblForms] TO [Reports_User]
GO
