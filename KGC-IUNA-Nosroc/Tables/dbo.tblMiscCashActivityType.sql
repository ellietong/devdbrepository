CREATE TABLE [dbo].[tblMiscCashActivityType]
(
[CashActivityTypeID] [int] NOT NULL IDENTITY(1, 1),
[CashActivityType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMiscCashActivityType] ADD CONSTRAINT [PK_tblMiscCashActivityType] PRIMARY KEY CLUSTERED  ([CashActivityTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivityType] TO [base_user]
GRANT SELECT ON  [dbo].[tblMiscCashActivityType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivityType] TO [CheckApproval_User]
GRANT SELECT ON  [dbo].[tblMiscCashActivityType] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivityType] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[tblMiscCashActivityType] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivityType] TO [power_user]
GRANT SELECT ON  [dbo].[tblMiscCashActivityType] TO [power_user]
GO
