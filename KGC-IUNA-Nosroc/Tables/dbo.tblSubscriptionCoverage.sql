CREATE TABLE [dbo].[tblSubscriptionCoverage]
(
[SubscriptionCoverageID] [int] NOT NULL IDENTITY(1, 1),
[SubscriberID] [int] NOT NULL,
[PolicyID] [int] NOT NULL,
[Percentage] [float] NULL,
[AgencyID] [int] NULL,
[UWReference] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyTypeID] [int] NULL,
[SubscriptionNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcctNum] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LondonBroker] [int] NULL,
[LondonBrokerReference] [int] NULL,
[DomesticBroker] [int] NULL,
[DomesticBrokerReference] [int] NULL,
[BrokerNotes] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriptionCoverage] ADD CONSTRAINT [PK_tblSubscriptionCoverage] PRIMARY KEY NONCLUSTERED  ([SubscriptionCoverageID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriptionCoverage] ADD CONSTRAINT [FK_tblSubscriptionCoverage_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy] ([PolicyID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionCoverage] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriptionCoverage] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriptionCoverage] TO [power_user]
GRANT INSERT ON  [dbo].[tblSubscriptionCoverage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSubscriptionCoverage] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriptionCoverage] TO [Subscription_User]
GRANT INSERT ON  [dbo].[tblSubscriptionCoverage] TO [Subscription_User]
GRANT DELETE ON  [dbo].[tblSubscriptionCoverage] TO [Subscription_User]
GRANT UPDATE ON  [dbo].[tblSubscriptionCoverage] TO [Subscription_User]
GO
