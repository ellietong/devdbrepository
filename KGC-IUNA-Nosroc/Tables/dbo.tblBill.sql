CREATE TABLE [dbo].[tblBill]
(
[BillID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupID] [int] NOT NULL,
[BillDate] [datetime] NOT NULL,
[PreviousBalance] [money] NULL,
[NewBalance] [money] NULL,
[FinalizedBy] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PaymentDueDate] [datetime] NOT NULL,
[Contestable] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBill] ADD CONSTRAINT [PK_tblBill] PRIMARY KEY CLUSTERED  ([BillID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBill] TO [base_user]
GRANT SELECT ON  [dbo].[tblBill] TO [base_user]
GRANT SELECT ON  [dbo].[tblBill] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblBill] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblBill] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblBill] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblBill] TO [power_user]
GRANT INSERT ON  [dbo].[tblBill] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBill] TO [power_user]
GO
