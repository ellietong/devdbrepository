CREATE TABLE [dbo].[tblRecipientType]
(
[RecipientTypeID] [int] NOT NULL IDENTITY(1, 1),
[RecipientType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRecipientType] ADD CONSTRAINT [PK_tblRecipientType] PRIMARY KEY CLUSTERED  ([RecipientTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblRecipientType] TO [base_user]
GRANT SELECT ON  [dbo].[tblRecipientType] TO [base_user]
GRANT SELECT ON  [dbo].[tblRecipientType] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblRecipientType] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblRecipientType] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblRecipientType] TO [Notice_User]
GRANT SELECT ON  [dbo].[tblRecipientType] TO [power_user]
GRANT INSERT ON  [dbo].[tblRecipientType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblRecipientType] TO [power_user]
GO
