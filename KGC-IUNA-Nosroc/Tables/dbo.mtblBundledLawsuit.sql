CREATE TABLE [dbo].[mtblBundledLawsuit]
(
[InjuredPartyID] [bigint] NULL,
[ClaimSequenceNum] [int] NULL,
[LawsuitNum] [int] NULL,
[VersionNum] [int] NULL,
[DocketNum] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffFirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffLastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffMiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalPlaintiffNum] [int] NULL,
[LocalPlaintiffName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NationalPlaintiffNum] [int] NULL,
[NationalPlaintiffName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LiasonCounselNum] [int] NULL,
[LiasonCounselName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrialDate] [datetime] NULL,
[FileDate] [datetime] NULL,
[StateFiled] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JurisdictionCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JurisdictionDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourtType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourtDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourtLevelCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadLawsuitFlag] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffTypeCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffTypeDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawsuitVersionTypeCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawsuitVersionTypeDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawsuitQCFlag] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateInput] [datetime] NULL,
[DateReceived] [datetime] NULL,
[QCInputDate] [datetime] NULL,
[SurrogateDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblBundledLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[mtblBundledLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[mtblBundledLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[mtblBundledLawsuit] TO [Claims_User]
GRANT SELECT ON  [dbo].[mtblBundledLawsuit] TO [power_user]
GRANT INSERT ON  [dbo].[mtblBundledLawsuit] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblBundledLawsuit] TO [power_user]
GO
