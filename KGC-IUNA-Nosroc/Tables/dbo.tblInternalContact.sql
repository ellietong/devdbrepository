CREATE TABLE [dbo].[tblInternalContact]
(
[InternalContactID] [int] NOT NULL IDENTITY(1, 1),
[InternalCompanyID] [int] NOT NULL,
[ContactID] [int] NOT NULL,
[CurrentContact] [bit] NOT NULL CONSTRAINT [DF_tblInternalContact_CurrentContact] DEFAULT ((0)),
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblInternalContact] ADD CONSTRAINT [PK_tblInternalContact] PRIMARY KEY CLUSTERED  ([InternalContactID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblInternalContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblInternalContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblInternalContact] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblInternalContact] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblInternalContact] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblInternalContact] TO [Notice_User]
GRANT SELECT ON  [dbo].[tblInternalContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblInternalContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblInternalContact] TO [power_user]
GO
