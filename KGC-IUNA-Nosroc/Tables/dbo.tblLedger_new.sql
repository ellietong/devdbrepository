CREATE TABLE [dbo].[tblLedger_new]
(
[LedgerID] [int] NOT NULL,
[BillGroupID] [int] NULL,
[CarrierID] [bigint] NOT NULL,
[SubscriberID] [int] NULL,
[Allocation] [money] NOT NULL,
[BillID] [int] NOT NULL IDENTITY(1, 1),
[BillDate] [datetime] NOT NULL,
[ExhaustionDateID] [int] NULL,
[PaymentDueDate] [datetime] NULL,
[BillNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [int] NULL,
[BillTypeID] [int] NULL
) ON [PRIMARY]
GO
