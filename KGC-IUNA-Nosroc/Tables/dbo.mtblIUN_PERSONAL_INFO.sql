CREATE TABLE [dbo].[mtblIUN_PERSONAL_INFO]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REFERENCE_ID] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_ID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUFFIX] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOCIAL_SECURITY_NUMBER] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BIRTH_DATE] [datetime] NULL,
[DEATH_DATE] [datetime] NULL,
[MARITAL_STATUS] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GENDER] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPLETE_ADDRESS] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[mtblIUN_PERSONAL_INFO] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_PERSONAL_INFO] TO [base_user]
GRANT SELECT ON  [dbo].[mtblIUN_PERSONAL_INFO] TO [power_user]
GRANT INSERT ON  [dbo].[mtblIUN_PERSONAL_INFO] TO [power_user]
GRANT UPDATE ON  [dbo].[mtblIUN_PERSONAL_INFO] TO [power_user]
GO
