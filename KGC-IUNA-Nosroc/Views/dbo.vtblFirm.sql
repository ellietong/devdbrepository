SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vtblFirm]
AS
SELECT     FirmID, FirmDisplayName as Firm, FirmAdminDocLibURL, FirmExtranetDocLibURL, FirmDocAbb, FirmInvoiceAbb, 
	FirmAdminInvoiceURL, FirmExtranetInvoiceURL
FROM         SHARESQL.fbsecurity.dbo.tblfirm AS tblfirm_1
GO
GRANT VIEW DEFINITION ON  [dbo].[vtblFirm] TO [base_user]
GRANT SELECT ON  [dbo].[vtblFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vtblFirm] TO [power_user]
GRANT SELECT ON  [dbo].[vtblFirm] TO [power_user]
GO
