SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblDocumentSource]
AS
SELECT     DocumentSourceID, DocumentSource
FROM         [KGA-ClaimsAdmin].dbo.tblDocumentSource AS tblDocumentSource_1

GO
GRANT SELECT ON  [dbo].[tblDocumentSource] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocumentSource] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDocumentSource] TO [power_user]
GO
