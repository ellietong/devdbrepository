SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[tblSettlementDocumentation]
AS
SELECT     spd.SetPymtID, MAX(CASE WHEN DocCategoryID = 1 OR
                      spd.ExecutedReleaseDate IS NOT NULL OR
                      spd.ReleaseToPC IS NOT NULL OR
                      ps.IsFullyPaid = 1 THEN 1 ELSE 0 END) AS ClaimantIDed, MAX(CASE WHEN DocCategoryID = 2 OR
                      spd.ExecutedReleaseDate IS NOT NULL OR
                      spd.ReleaseToPC IS NOT NULL OR
                      ps.IsFullyPaid = 1 THEN 1 ELSE 0 END) AS DxMedicals, MAX(CASE WHEN cpi.DeceasedDate IS NOT NULL AND (DocCategoryID = 3 OR
                      spd.ExecutedReleaseDate IS NOT NULL OR
                      spd.ReleaseToPC IS NOT NULL OR
                      ps.IsFullyPaid = 1) THEN 1 ELSE 0 END) AS DODCert, MAX(CASE WHEN cpi.DeceasedDate IS NOT NULL AND (DocCategoryID = 4 OR
                      spd.ExecutedReleaseDate IS NOT NULL OR
                      spd.ReleaseToPC IS NOT NULL OR
                      ps.IsFullyPaid = 1) THEN 1 ELSE 0 END) AS EstatePapers, MAX(CASE WHEN DocCategoryID = 5 OR
                      spd.ExecutedReleaseDate IS NOT NULL OR
                      spd.ReleaseToPC IS NOT NULL OR
                      ps.IsFullyPaid = 1 THEN 1 ELSE 0 END) AS ProductID, MAX(CASE WHEN DocCategoryID = 6 OR
                      spd.ExecutedReleaseDate IS NOT NULL OR
                      spd.ReleaseToPC IS NOT NULL OR
                      ps.IsFullyPaid = 1 THEN 1 ELSE 0 END) AS EmployExpoHist, MAX(CASE WHEN DocCategoryID = 12 OR
                      spd.ExecutedReleaseDate IS NOT NULL OR
                      spd.ReleaseToPC IS NOT NULL OR
                      ps.IsFullyPaid = 1 THEN 1 ELSE 0 END) AS SettlementConfirmation
FROM         dbo.tblClaim AS m LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS cl ON m.ClaimID = cl.ClaimID LEFT OUTER JOIN
                      dbo.tblSettlePaymentDocumentation AS spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblSettleDocs AS sd ON sd.SetPymtID = spd.SetPymtID LEFT OUTER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = m.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblPaymentSettlement AS ps ON ps.ClaimantSettlementID = cs.ClaimantSettlementID LEFT OUTER JOIN
                      dbo.tblCheck AS k ON k.CheckID = ps.CheckID
WHERE     (m.IsCurrentClaim = 1) AND (cl.IsPrimaryForClaim = 1)
GROUP BY spd.SetPymtID
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementDocumentation] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementDocumentation] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "m"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 279
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "spd"
            Begin Extent = 
               Top = 6
               Left = 255
               Bottom = 125
               Right = 485
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sd"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 275
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 330
               Left = 38
               Bottom = 438
               Right = 278
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ps"
            Begin Extent = 
               Top = 438
               Left = 38
               Bottom = 546
               Right = 256
            End
            DisplayFlags = 280
            TopColumn = 0
         En', 'SCHEMA', N'dbo', 'VIEW', N'tblSettlementDocumentation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'd
         Begin Table = "k"
            Begin Extent = 
               Top = 126
               Left = 297
               Bottom = 234
               Right = 475
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'tblSettlementDocumentation', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'tblSettlementDocumentation', NULL, NULL
GO
