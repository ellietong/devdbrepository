SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDefenseJurisdiction]
AS
SELECT DJ.DefenseJurisdictionID,
       DJ.JurisdictionID,
       Jurisdiction,
       DJ.StateID,
       S.State,
	   DJ.CourtTypeID,
       CourtType,
       0 AS MatterID,--DJ.MatterID,  **Not Used In This Database
      'N/A' AS Matter,-- M. Matter,  **Not Used In This Database
	   0 AS MatterEntityID,--DJ.MatterEntityID, **Not Used In This Database
	   'N/A' AS MatterEntity,-- MatterEntity, **Not Used In This Database
	   DJ.DefenseCounselAddressID,
       DefenseCounsel,
       DJ.IsActive

FROM tblDefenseJurisdiction DJ
LEFT JOIN tblJurisdiction J ON J.JurisdictionID=DJ.JurisdictionID
LEFT JOIN tblDefenseCounselAddress DCA ON DCA.DefenseCounselAddressID=DJ.DefenseCounselAddressID
LEFT JOIN tblCourtType CT ON CT.CourtTypeID=DJ.CourtTypeID
--LEFT JOIN tblMatter M ON M.MatterID = DJ.MatterID
LEFT JOIN tblState S ON S.StateID = DJ.StateID
--LEFT JOIN tblMatterEntity ME ON ME.MatterEntityID = DJ.MatterEntityID **Not Used In This Database


GO
GRANT VIEW DEFINITION ON  [dbo].[vDefenseJurisdiction] TO [base_user]
GRANT SELECT ON  [dbo].[vDefenseJurisdiction] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDefenseJurisdiction] TO [power_user]
GRANT SELECT ON  [dbo].[vDefenseJurisdiction] TO [power_user]
GO
