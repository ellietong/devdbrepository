SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblLegalCapacity]
AS
SELECT     LegalCapacityID, LegalCapacity, MedicareRelationshipID
FROM         [KGA-ClaimsAdmin].dbo.tblLegalCapacity AS tblLegalCapacity_1

GO
GRANT VIEW DEFINITION ON  [dbo].[tblLegalCapacity] TO [base_user]
GRANT SELECT ON  [dbo].[tblLegalCapacity] TO [base_user]
GRANT SELECT ON  [dbo].[tblLegalCapacity] TO [power_user]
GO
