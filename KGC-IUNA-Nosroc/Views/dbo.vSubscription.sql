SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vSubscription]
AS
SELECT sc.SubscriberID, SubscriptionCompany, SubscriptionCompanySolvency, SubscriptionCompanySolvencyStatus, 
SubscriptionCompanyNote, SC.CarrierGroupID, CarrierGroupName, SolvencySource, DateUpdated, AMBestNo, 
WellingtonSignatory, SubscriptionCoverageID, PolicyID, Percentage, COV.AgencyID, SA.AgencyName, UWReference, 
COV.PolicyTypeID, PT.PolicyType, SubscriptionNotes, AcctNum
FROM tblSubscriberCompany SC
LEFT JOIN tblSubscriptionCoverage COV ON COV.SubscriberID = SC.SubscriberID
LEFT JOIN tblCarrierGroup CD ON SC.CarrierGroupID = CD.CarrierGroupID
LEFT JOIN tblSubscriberAgency SA ON COV.AgencyID = SA.AgencyID
LEFT JOIN tblSubscriberPolicyType PT ON COV.PolicyTypeID = PT.PolicyTypeID

GO
GRANT VIEW DEFINITION ON  [dbo].[vSubscription] TO [base_user]
GRANT SELECT ON  [dbo].[vSubscription] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSubscription] TO [power_user]
GRANT SELECT ON  [dbo].[vSubscription] TO [power_user]
GO
