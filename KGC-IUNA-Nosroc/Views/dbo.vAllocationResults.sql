SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vAllocationResults]
AS

SELECT AllocationResultID, ar.AllocationID, AllocationDescription, ar.PolicyID, PolicyDateID, PStartSplit, PEndSplit, AnnualPeriodSplit, Allocation, 
AllocationIndemnity, AllocationDefense, AllocationDefenseOutsideLimits, ar.OccurrenceID, OccurrenceName, ExpectedPayDate, Matter, O.MatterID, 
O.OccurrenceGroupID, OccurrenceGroup, O.OccurrenceTypeID, OT.OccurrenceType, TriggerStartDateUsed, TriggerEndDateUsed, SIRAllocated, SIRAllocatedIndemnity, 
SIRAllocatedDefense, SIRDiscount, [DateTime], PV, DateToPresentValue, PVRate, YieldCurveID
FROM tblAllocationResults AR
LEFT JOIN tblAllocation A ON AR.AllocationID = A.AllocationID
LEFT JOIN tblOccurrence O ON AR.OccurrenceID = O.OccurrenceID
LEFT JOIN tblMatter M ON O.MatterID = M.MatterID
LEFT JOIN tblOccurrenceGroup OG ON O.OccurrenceGroupID = OG.OccurrenceGroupID
LEFT JOIN tblOccurrenceType OT ON O.OccurrenceTypeID = OT.OccurrenceTypeID

GO
GRANT VIEW DEFINITION ON  [dbo].[vAllocationResults] TO [base_user]
GRANT SELECT ON  [dbo].[vAllocationResults] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vAllocationResults] TO [power_user]
GRANT SELECT ON  [dbo].[vAllocationResults] TO [power_user]
GO
