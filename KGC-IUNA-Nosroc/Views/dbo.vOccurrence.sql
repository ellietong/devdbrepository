SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vOccurrence]

AS

SELECT OccurrenceID, o.OccurrenceGroupID, OccurrenceGroup, OccGrpTriggerStart, OccGrpDiscount,
o.CorporateHistoryID, CorporateHistoryProfile, o.OccurrenceTypeID, OccurrenceType, OccurrenceTypeNotes, 
OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, State, FullNameState, StateDiscount,
AggregateApplies, InClaim, ClaimID, Segment, ExpectedPayDate, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, 
TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, 
Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, Cost6, Cost7, Cost8, 
Cost9, Cost10, Cost11, Cost12, OccurrenceNote, O.MatterID, Matter
FROM tblOccurrence O
LEFT JOIN tblMatter M ON O.MatterID = M.MatterID
LEFT JOIN tblState ST ON ST.StateID = O.OccurrenceStateID
LEFT JOIN tblOccurrenceGroup OG ON OG.OccurrenceGroupID = O.OccurrenceGroupID
LEFT JOIN tblCorporateHistoryProfile CHP ON CHP.CorporateHistoryID = O.CorporateHistoryID
LEFT JOIN tblOccurrenceType OT ON OT.OccurrenceTypeID = O.OccurrenceTypeID


GO
GRANT VIEW DEFINITION ON  [dbo].[vOccurrence] TO [base_user]
GRANT SELECT ON  [dbo].[vOccurrence] TO [base_user]
GRANT SELECT ON  [dbo].[vOccurrence] TO [power_user]
GO
