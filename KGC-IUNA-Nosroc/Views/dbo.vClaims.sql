SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE VIEW [dbo].[vClaims]
AS
SELECT     cpi.ClaimantPersonalInfoID, cpi.FirstName, cpi.MiddleName, cpi.LastName, csuf.Suffix, cpi.SocialSecurityNumber, cpi.DeceasedDate, ms.MaritalStatus, 
g.Gender, cpi.Address1, cpi.Address2, cpi.City, cst.State AS ClaimantState, cpi.Zip, cpi.MailZip, cpi.BirthDate,cpi.Phone, cpi.Extension, 
c.ClaimID, c.IsCurrentClaim, c.ClaimSequenceID, cseq.ClaimSequence, 
m.Matter, cd.DoctorID, d.DoctorLastName, d.DoctorFirstName, d.DoctorMiddleName, d.DirtyDoctorFlag, d.DoctorNote, Dx.Disease, Dx.ICD9, cd.DiseaseDiagnosisDate, 
cl.ClaimLawsuitID, cl.LawsuitID, cl.IndividualDocketNumber, cl.IsPrimaryForClaim, clst.ClaimLawsuitStatusID,cls.ClaimLawsuitStatus,
clst.StatusDate, clst.StatusNotes, l.CaseCaption, l.DocketNumber, l.FileDate, j.Jurisdiction,st.State, 
st.FullNameState, ct.CourtType, l.ServiceDate, l.ReceivedDate, dc.DefenseCounsel, pc.PlaintiffCounsel,pca.PlaintiffCounselAddressID,
 l.LeadPlaintiffFirstName, l.LeadPlaintiffLastName, l.ActiveTrialDate, clst.ProcessedDate
FROM         dbo.tblClaimantPersonalInfo AS cpi 
					  LEFT OUTER JOIN dbo.tblMaritalStatus AS ms ON cpi.MaritalStatusID = ms.MaritalStatusID 
                      LEFT OUTER JOIN dbo.tblGender AS g ON cpi.GenderID = g.GenderID 
                      LEFT OUTER JOIN dbo.tblClaimantSuffix AS csuf ON cpi.SuffixID = csuf.SuffixID 
                     
                      LEFT OUTER JOIN dbo.tblClaim AS c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID 
                      LEFT OUTER JOIN dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID 
                      left join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID = clst.ClaimLawsuitID
					  LEFT outer join tblClaimLawsuitStatus cls on clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
                      LEFT OUTER JOIN dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID 
                      
                      LEFT OUTER JOIN dbo.tblMatter AS m ON m.MatterID = c.MatterID 
                      LEFT OUTER JOIN dbo.tblClaimSequence AS cseq ON c.ClaimSequenceID = cseq.ClaimSequenceID 
                      LEFT OUTER JOIN dbo.tblClaimDisease AS cd ON cd.ClaimID = c.ClaimID 
                      LEFT OUTER JOIN dbo.tblDoctor AS d ON cd.DoctorID = d.DoctorID 
                     
                      
                      LEFT OUTER JOIN dbo.tblJurisdiction AS j ON l.JurisdictionID = j.JurisdictionID 
                      LEFT OUTER JOIN dbo.tblState AS st ON l.StateID = st.StateID 
                      LEFT OUTER JOIN dbo.tblState AS cst ON cpi.StateID = cst.StateID 
                      LEFT OUTER JOIN dbo.tblCourtType AS ct ON l.CourtTypeID = ct.CourtTypeID 
                      left outer join tblDisease Dx on Dx.DiseaseID = cd.DiseaseID
                      left outer join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = l.DefenseCounselAddressID
                      left outer join tblDefenseCounsel dc on dc.DefenseCounselID = dca.DefenseCounselID
                      Left outer join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
                      left outer join [KGA-ClaimsAdmin].dbo.tblPlaintiffCounsel pc on pc.PlaintiffCounselID = pca.PlaintiffCounselID
           






GO
GRANT VIEW DEFINITION ON  [dbo].[vClaims] TO [base_user]
GRANT SELECT ON  [dbo].[vClaims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vClaims] TO [power_user]
GRANT SELECT ON  [dbo].[vClaims] TO [power_user]
GO
