SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vSP_REPORTS_qryOpenClaims]
AS
SELECT     C.ClaimID, COALESCE (DHLD.HighLevelDisease, PHLD.HighLevelDisease) AS Disease, S.State, J.Jurisdiction, cs.ClaimSequence, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS SSN, 
                      CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, 
                      DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) 
                      = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, 
                      BirthDate, DeceasedDate) END END AS DeathAge, cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) 
                      AS FileDate, L.DocketNumber, L.CaseCaption, L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName, 
                      L.PrimaryLocalPlaintiffCounsel, r_1.DefenseCounsel AS PrimaryDefenseCounsel
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                          (SELECT     dcs.DefenseCounselID, d.DefenseCounsel, dcs.ResponsibleDefenseCounselID, dcs.StateID, 
                                                   r.DefenseCounsel AS ResponsibleDefenseCOunsel, dcs.IsActiveCounsel
                            FROM          dbo.tblDefenseCounselState AS dcs INNER JOIN
                                                   dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dcs.DefenseCounselID INNER JOIN
                                                   dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcs.ResponsibleDefenseCounselID
                            GROUP BY d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounselID, dcs.ResponsibleDefenseCounselID, dcs.IsActiveCounsel, dcs.StateID) 
                      AS r_1 ON r_1.DefenseCounselID = L.PrimaryDefenseCounselID AND r_1.StateID = L.StateID LEFT OUTER JOIN
                      dbo.tblDisease AS DDisease ON C.DefenseDiseaseID = DDisease.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS DHLD ON DHLD.HighLevelDiseaseID = DDisease.HighLevelDiseaseID LEFT OUTER JOIN
                      dbo.tblDisease AS PDisease ON C.PlaintiffDiseaseID = PDisease.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS PHLD ON PHLD.HighLevelDiseaseID = PDisease.HighLevelDiseaseID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatus AS cls ON CL.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
WHERE     (CL.IsPrimaryForClaim = 1) AND cl.ClaimLawsuitStatusID = 22

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CPI"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "C"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 263
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CL"
            Begin Extent = 
               Top = 6
               Left = 269
               Bottom = 114
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "L"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 267
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "S"
            Begin Extent = 
               Top = 114
               Left = 301
               Bottom = 222
               Right = 452
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "J"
            Begin Extent = 
               Top = 222
               Left = 305
               Bottom = 300
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 300
               Left = 305
               Bottom = 378
               Right = 468
            End
            DisplayFlags = 280
            TopColumn = 0
         E', 'SCHEMA', N'dbo', 'VIEW', N'vSP_REPORTS_qryOpenClaims', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'nd
         Begin Table = "r_1"
            Begin Extent = 
               Top = 330
               Left = 38
               Bottom = 438
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DDisease"
            Begin Extent = 
               Top = 378
               Left = 302
               Bottom = 471
               Right = 476
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DHLD"
            Begin Extent = 
               Top = 438
               Left = 38
               Bottom = 516
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PDisease"
            Begin Extent = 
               Top = 474
               Left = 250
               Bottom = 567
               Right = 424
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PHLD"
            Begin Extent = 
               Top = 516
               Left = 38
               Bottom = 594
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cls"
            Begin Extent = 
               Top = 570
               Left = 250
               Bottom = 648
               Right = 433
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSP_REPORTS_qryOpenClaims', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSP_REPORTS_qryOpenClaims', NULL, NULL
GO
