SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vDefenseCounsel]
AS
SELECT     d.defensecounselid, d.defensecounsel, da.defensecounseladdress1, da.defensecounseladdress2, da.defensecounselcity, 
da.state, da.defensecounselzip, da.defensecounselphone
FROM        dbo.Tbldefensecounsel d
left join dbo.tbldefensecounseladdress da on da.defensecounselID = d.defensecounselID



GO
