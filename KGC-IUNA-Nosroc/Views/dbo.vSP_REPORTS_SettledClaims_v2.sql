SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vSP_REPORTS_SettledClaims_v2]
AS
SELECT     C.ClaimID, S.State, J.Jurisdiction, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      HLDDefense.HighLevelDisease AS Disease, 
                      CASE WHEN CLst.ClaimLawsuitStatusID = 25 THEN 'SBND' WHEN CLst.ClaimLawsuitStatusID = 28 THEN 'Zero Dollar' ELSE ClaimLawsuitStatus END AS ClaimLawsuitStatus,
                       CS.SettlementAmount, CONVERT(date, CS.SettlementDate) AS SettlementDate, CASE WHEN MIN(PaidDate) IS NOT NULL 
                      THEN 'Yes' ELSE 'No' END AS Paid, CONVERT(date, MIN(CS.PaidDate)) AS MinPaidDate, CONVERT(date, CS.settlementduedate) AS settlementduedate, 
                      L.PrimaryLocalPlaintiffCounsel,
                      Case when PaidDate IS null then SettlementAmount else 0 end as UnpaidSettlement
FROM         dbo.tblClaimantPersonalInfo AS CPI LEFT OUTER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID LEFT OUTER JOIN
                      (SELECT * FROM tblClaimDisease WHERE PrimaryDisease=1) AS CD ON CD.ClaimID = C.ClaimID LEFT OUTER JOIN 
                      dbo.tblDisease AS DDefense ON CD.DiseaseID = DDefense.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS HLDDefense ON HLDDefense.HighLevelDiseaseID = DDefense.HighLevelDiseaseID LEFT OUTER JOIN
                      dbo.tblClaimSequence AS ClaimS ON ClaimS.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID 
                      Left Join (Select * From dbo.tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID INNER JOIN
                      dbo.tblClaimLawsuitStatus AS CLS ON CLST.ClaimLawsuitStatusID = CLS.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS CS ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
WHERE     (CLst.ClaimLawsuitStatusID IN (23, 25, 28)) AND (CL.IsPrimaryForClaim = 1) AND (CS.SettlementDate >= '1/1/2001') AND 
                      (HLDDefense.HighLevelDiseaseID IN (1, 2, 3, 4, 5, 6, 7))
GROUP BY C.ClaimID, HLDDefense.HighLevelDisease,  S.State, J.Jurisdiction, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END, CS.SettlementDate, CLS.ClaimLawsuitStatus, CS.SettlementAmount, CS.PaidDate, CS.SettlementDueDate, 
                      L.PrimaryLocalPlaintiffCounsel, CLst.ClaimLawsuitStatusID
GO
GRANT VIEW DEFINITION ON  [dbo].[vSP_REPORTS_SettledClaims_v2] TO [base_user]
GRANT SELECT ON  [dbo].[vSP_REPORTS_SettledClaims_v2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSP_REPORTS_SettledClaims_v2] TO [power_user]
GRANT SELECT ON  [dbo].[vSP_REPORTS_SettledClaims_v2] TO [power_user]
GO
