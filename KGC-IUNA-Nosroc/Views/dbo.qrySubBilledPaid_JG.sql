SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[qrySubBilledPaid_JG]
AS
SELECT     TOP (100) PERCENT dbo.qrySubBilled_IUNA_JG.SubscriptionCompany, dbo.qrySubBilled_IUNA_JG.SubscriptionCompanySolvency, 
                      dbo.qrySubBilled_IUNA_JG.NavBillID, SUM(dbo.qrySubBilled_IUNA_JG.[Total Billed]) AS Expr1, SUM(dbo.qrySubPymts_IUNA_JG.TotalPaid) 
                      AS [Total Paid]
FROM         dbo.qrySubBilled_IUNA_JG INNER JOIN
                      dbo.qrySubPymts_IUNA_JG ON dbo.qrySubBilled_IUNA_JG.NavBillID = dbo.qrySubPymts_IUNA_JG.NavBillID AND 
                      dbo.qrySubBilled_IUNA_JG.SubscriptionCompany = dbo.qrySubPymts_IUNA_JG.SubscriptionCompany
GROUP BY dbo.qrySubBilled_IUNA_JG.SubscriptionCompany, dbo.qrySubBilled_IUNA_JG.SubscriptionCompanySolvency, 
                      dbo.qrySubBilled_IUNA_JG.NavBillID
ORDER BY dbo.qrySubBilled_IUNA_JG.SubscriptionCompanySolvency DESC, dbo.qrySubBilled_IUNA_JG.SubscriptionCompany, 
                      dbo.qrySubBilled_IUNA_JG.NavBillID
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[33] 4[38] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "qrySubBilled_IUNA_JG"
            Begin Extent = 
               Top = 0
               Left = 40
               Bottom = 137
               Right = 378
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "qrySubPymts_IUNA_JG"
            Begin Extent = 
               Top = 18
               Left = 430
               Bottom = 137
               Right = 627
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1858
         Width = 1800
         Width = 1872
         Width = 1670
         Width = 1440
         Width = 1440
         Width = 1440
         Width = 1440
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 3298
         Alias = 907
         Table = 1166
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1354
         SortOrder = 1411
         GroupBy = 1350
         Filter = 1354
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'qrySubBilledPaid_JG', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'qrySubBilledPaid_JG', NULL, NULL
GO
