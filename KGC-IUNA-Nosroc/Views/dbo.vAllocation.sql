SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vAllocation]
AS

SELECT AllocationID, AllocationDescription, SaveResults, a.LimitTypeID, LimitType, a.AllocationSchemeID, AllocationScheme,
TriggerStartID, TriggerStartDate, TriggerEndID, TriggerEndDate, a.OccurrenceDefId, OccurrenceDef, UnCoveredYearstoPH, 
VerticallyUnCollapse, Cost1, Cost2, Cost3, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, 
Cost12, a.SIRID, SIRTreatment, SIRToPay, ConsiderPreviousExhaustion, ConsiderExhaustionCarterWallace, 
a.ExhaustionTypeID, ExhaustionType, LessThanAttachmentPoint, LessThanLayer, OccurrencesInClaim, 
Cost1Discount, Cost2Discount, Cost3Discount, Cost4Discount, Cost5Discount, Cost6Discount, Cost7Discount, 
Cost8Discount, Cost9Discount, Cost10Discount, Cost11Discount, Cost12Discount, AllocationNotes, 
ConsiderPreviousAllocation, PreviousAllocation
FROM tblAllocation a
LEFT JOIN tblExhaustionType et ON a.exhaustiontypeid = et.exhaustiontypeid
LEFT JOIN tblPolicyLimitType lt ON a.limittypeid = lt.limittypeid
LEFT JOIN tblAllocationScheme als ON a.allocationschemeid = als.allocationschemeid
LEFT JOIN tblOccurrenceDef od ON a.OccurrenceDefId = od.OccurrenceDefID
LEFT JOIN tblAllocationSIR sir ON a.SIRID = sir.SIRID


GO
GRANT VIEW DEFINITION ON  [dbo].[vAllocation] TO [base_user]
GRANT SELECT ON  [dbo].[vAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vAllocation] TO [power_user]
GRANT SELECT ON  [dbo].[vAllocation] TO [power_user]
GO
