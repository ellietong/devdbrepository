SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vBilling]
AS

SELECT L.LedgerID, L.BillGroupID, BillGroupName, CarrierID, SubscriberID, Allocation, L.ExhaustionDateID, ExhaustionDate, ED.ExhaustionTypeID,
ET.ExhaustionType, AllocationID, [Description] AS ExhaustionDateDescription, BillDate, PaymentDueDate, BillNotes
FROM tblLedger L
LEFT JOIN tblBillGroup BG ON BG.BillGroupID = L.BillGroupID
LEFT JOIN tblExhaustionDate ED ON L.ExhaustionDateID = ED.ExhaustionDateID
LEFT JOIN tblExhaustionType ET ON ED.ExhaustionTypeID = ET.ExhaustionTypeID
GROUP BY L.LedgerID, L.BillGroupID, BillGroupName, CarrierID, SubscriberID, Allocation, L.ExhaustionDateID, ExhaustionDate, ED.ExhaustionTypeID,
ET.ExhaustionType, AllocationID, [Description], BillDate, PaymentDueDate, BillNotes



GO
GRANT VIEW DEFINITION ON  [dbo].[vBilling] TO [base_user]
GRANT SELECT ON  [dbo].[vBilling] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vBilling] TO [power_user]
GRANT SELECT ON  [dbo].[vBilling] TO [power_user]
GO
