SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vSP_REPORTS_ClaimantPersonalInfo_v2]
AS
SELECT
                      CPI.ClaimantPersonalInfoID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, Gender,
                      MaritalStatus, coalesce(nullif(coalesce(Address1 + CHAR(13) + CHAR(10), '') + coalesce(Address2 + CHAR(13) + CHAR(10),'') + 
						coalesce(City + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,Zip),'') + Coalesce('-'+convert(varchar,mailzip),''),''),CompleteAddress) as CompleteAddress, 
						BirthDate, DeceasedDate
                        
FROM         dbo.tblClaimantPersonalInfo AS CPI LEFT JOIN
                     tblGender as G on G.GenderID = CPI.GenderID LEFT JOIN
                     tblMaritalStatus as MS on MS.MaritalStatusID=CPI.MaritalStatusID LEFT JOIN
                     tblState as s on s.StateID = CPI.StateID
GO
GRANT VIEW DEFINITION ON  [dbo].[vSP_REPORTS_ClaimantPersonalInfo_v2] TO [base_user]
GRANT SELECT ON  [dbo].[vSP_REPORTS_ClaimantPersonalInfo_v2] TO [base_user]
GRANT SELECT ON  [dbo].[vSP_REPORTS_ClaimantPersonalInfo_v2] TO [power_user]
GO
