SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create View [dbo].[vSP_REPORTS_ClaimCountDetailByYear]
AS
SELECT     CASE WHEN COALESCE (YEAR(ServiceDate), YEAR(FileDate)) IS NULL THEN '2001' ELSE COALESCE (YEAR(ServiceDate), YEAR(FileDate)) 
                      END AS Year, SUM(dc.MesoCount) AS MesoFilings, SUM(dc.LungCancerCount) AS LCFilings, SUM(dc.OtherCancerCount) AS OCFilings, 
                      SUM(dc.NonMaligCount) AS NMFILINGS, SUM(dc.SilicosisCount) AS SilicosisCount, SUM(dc.UnknownCount) AS UnknownFilings, COUNT(c.ClaimID) 
                      AS NewFilings
FROM         (SELECT     c.ClaimID, CASE WHEN (COALESCE (DDefenseMeso.HighLevelDiseaseID, DPlaintiffMeso.HighLevelDiseaseID)) 
                                              = 1 THEN 1 ELSE 0 END AS MesoCount, CASE WHEN (COALESCE (DDefenseLC.HighLevelDiseaseID, DPlaintiffLC.HighLevelDiseaseID)) 
                                              = 2 THEN 1 ELSE 0 END AS LungCancerCount, CASE WHEN (COALESCE (DDefenseOC.HighLevelDiseaseID, 
                                              DPlaintiffOC.HighLevelDiseaseID)) = 3 THEN 1 ELSE 0 END AS OtherCancerCount, 
                                              CASE WHEN (COALESCE (DDefenseNM.HighLevelDiseaseID, DPlaintiffNM.HighLevelDiseaseID)) = 4 THEN 1 ELSE 0 END AS NonMaligCount,
                                               CASE WHEN (COALESCE (DDefenseUnk.HighLevelDiseaseID, DPlaintiffUnk.HighLevelDiseaseID)) 
                                              = 5 THEN 1 WHEN (COALESCE (DDefenseUnk.HighLevelDiseaseID, DPlaintiffUnk.HighLevelDiseaseID)) IS NULL 
                                              THEN 1 ELSE 0 END AS UnknownCount, CASE WHEN (COALESCE (DDefenseSil.HighLevelDiseaseID, DPlaintiffSil.HighLevelDiseaseID)) 
                                              = 7 THEN 1 ELSE 0 END AS SilicosisCount
                       FROM          dbo.tblClaim AS c LEFT OUTER JOIN
                                              dbo.tblDisease AS DDefenseMeso ON c.DefenseDiseaseID = DDefenseMeso.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DPlaintiffMeso ON c.PlaintiffDiseaseID = DPlaintiffMeso.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DDefenseLC ON c.DefenseDiseaseID = DDefenseLC.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DPlaintiffLC ON c.PlaintiffDiseaseID = DPlaintiffLC.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DDefenseOC ON c.DefenseDiseaseID = DDefenseOC.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DPlaintiffOC ON c.PlaintiffDiseaseID = DPlaintiffOC.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DDefenseNM ON c.DefenseDiseaseID = DDefenseNM.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DPlaintiffNM ON c.PlaintiffDiseaseID = DPlaintiffNM.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DDefenseUnk ON c.DefenseDiseaseID = DDefenseUnk.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DPlaintiffUnk ON c.PlaintiffDiseaseID = DPlaintiffUnk.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DDefenseSil ON c.DefenseDiseaseID = DDefenseSil.DiseaseID LEFT OUTER JOIN
                                              dbo.tblDisease AS DPlaintiffSil ON c.PlaintiffDiseaseID = DPlaintiffSil.DiseaseID) AS dc LEFT OUTER JOIN
                      dbo.tblClaim AS c ON dc.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID LEFT OUTER JOIN
                      dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                      dbo.tblState AS s ON l.StateID = s.StateID
WHERE     (cl.IsPrimaryForClaim = 1) AND (cl.ClaimLawsuitID NOT IN (138061, 138224, 138227, 134926, 75142, 118319, 109901, 2688, 38257, 21087, 101398, 
                      99889, 97673, 95557)) AND (cl.ClaimLawsuitStatusID <> 31)
GROUP BY CASE WHEN COALESCE (YEAR(ServiceDate), YEAR(FileDate)) IS NULL THEN '2001' ELSE COALESCE (YEAR(ServiceDate), YEAR(FileDate)) END
GO
