SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vProvisions]
AS

SELECT PolicyID, ProvisionID, pr.ProvisionLabelID, Provision, pr.ProvisionValueID, ProvisionValue, pr.LanguageID, PolicyLanguage,
Bates, ProvisionNotes, ProvisionMemo
FROM tblProvision PR
LEFT JOIN tblProvisionLabel PL ON PL.ProvisionLabelID = PR.ProvisionLabelID
LEFT JOIN tblProvisionValue PV ON PV.ProvisionValueID = PR.ProvisionValueID
LEFT JOIN tblLanguage l ON pr.languageid = l.languageid
GO
GRANT VIEW DEFINITION ON  [dbo].[vProvisions] TO [base_user]
GRANT SELECT ON  [dbo].[vProvisions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vProvisions] TO [power_user]
GRANT SELECT ON  [dbo].[vProvisions] TO [power_user]
GO
