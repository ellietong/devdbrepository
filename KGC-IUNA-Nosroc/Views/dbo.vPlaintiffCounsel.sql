SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPlaintiffCounsel]
AS
SELECT     p.PlaintiffCounselID, p.PlaintiffCounsel, p.PCTaxIDNum, a.PCAddress1, a.PCAddress2, a.City, state, a.PCZip, a.PCPhone, a.PCExtension
FROM        [KGA-ClaimsAdmin].dbo.TblPLaintiffcounsel p
left join [KGA-ClaimsAdmin].dbo.tblplaintiffcounseladdress a on a.PlaintiffCounselID = p.PlaintiffCounselID
left join [KGA-ClaimsAdmin].dbo.tblstate s on s.stateid = a.StateID

GO
GRANT VIEW DEFINITION ON  [dbo].[vPlaintiffCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[vPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPlaintiffCounsel] TO [power_user]
GRANT SELECT ON  [dbo].[vPlaintiffCounsel] TO [power_user]
GO
