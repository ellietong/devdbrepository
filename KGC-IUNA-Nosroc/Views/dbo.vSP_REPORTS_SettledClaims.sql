SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE View [dbo].[vSP_REPORTS_SettledClaims]
AS
SELECT     C.ClaimID, S.State, J.Jurisdiction, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      COALESCE (HLDDefense.HighLevelDisease, HLDPlaintiff.HighLevelDisease) AS Disease, 
                      CASE WHEN CL.ClaimLawsuitStatusID = 25 THEN 'SBND' WHEN CL.ClaimLawsuitStatusID = 28 THEN 'Zero Dollar' ELSE ClaimLawsuitStatus END AS ClaimLawsuitStatus,
                       CS.SettlementAmount, CONVERT(date, CS.SettlementDate) AS SettlementDate, CASE WHEN MIN(PaidDate) IS NOT NULL 
                      THEN 'Yes' ELSE 'No' END AS Paid, CONVERT(date, MIN(CS.PaidDate)) AS MinPaidDate, CONVERT(date, CS.settlementduedate) AS settlementduedate, 
                      L.PrimaryLocalPlaintiffCounsel,
                      Case when PaidDate IS null then SettlementAmount else 0 end as UnpaidSettlement
FROM         dbo.tblClaimantPersonalInfo AS CPI LEFT OUTER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblDisease AS DDefense ON C.DefenseDiseaseID = DDefense.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS HLDDefense ON HLDDefense.HighLevelDiseaseID = DDefense.HighLevelDiseaseID LEFT OUTER JOIN
                      dbo.tblDisease AS DPlaintiff ON C.PlaintiffDiseaseID = DPlaintiff.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS HLDPlaintiff ON HLDPlaintiff.HighLevelDiseaseID = DPlaintiff.HighLevelDiseaseID INNER JOIN
                      dbo.tblClaimSequence AS ClaimS ON ClaimS.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblClaimLawsuitStatus AS CLS ON CL.ClaimLawsuitStatusID = CLS.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS CS ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
WHERE     (HLDPlaintiff.HighLevelDiseaseID IN (1, 2, 3, 4, 5, 6, 7)) AND (CL.ClaimLawsuitStatusID IN (23, 25, 28)) AND (CL.IsPrimaryForClaim = 1) AND 
                      (CS.SettlementDate >= '1/1/2007') OR
                      (CL.ClaimLawsuitStatusID IN (23, 25, 28)) AND (CL.IsPrimaryForClaim = 1) AND (CS.SettlementDate >= '1/1/2007') AND 
                      (HLDDefense.HighLevelDiseaseID IN (1, 2, 3, 4, 5, 6, 7))
GROUP BY C.ClaimID, HLDDefense.HighLevelDisease, HLDPlaintiff.HighLevelDisease, S.State, J.Jurisdiction, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END, CS.SettlementDate, CLS.ClaimLawsuitStatus, CS.SettlementAmount, CS.PaidDate, CS.SettlementDueDate, 
                      L.PrimaryLocalPlaintiffCounsel, CL.ClaimLawsuitStatusID
GO
