SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblCase]
AS
SELECT     CaseID, CaseName, CaseDisplayName, Active, ConnectionStringKey, Bucket, DocumentFolder, Email
FROM         SHARESQL.fbsecurity.dbo.tblcase AS tblcase_1
WHERE     (CaseID IN (3, 26, 22, 29, 35, 36, 37, 38,39,40,41))

GO
