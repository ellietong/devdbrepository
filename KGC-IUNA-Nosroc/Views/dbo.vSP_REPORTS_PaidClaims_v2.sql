SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*SET QUOTED_IDENTIFIER ON
GO*/
CREATE VIEW [dbo].[vSP_REPORTS_PaidClaims_v2]
AS
SELECT     C.ClaimID, S.State, J.Jurisdiction, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      HLDDefense.HighLevelDisease AS Disease, CS.SettlementAmount, CONVERT(date, CS.SettlementDate) 
                      AS SettlementDate, CONVERT(date, MIN(ck.SourceDate )) AS MinPaidDate, L.PrimaryLocalPlaintiffCounsel,ck.SourceNumber as 'CheckNumber', CS.SettlementGroupID, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS SSN, DocketNumber
FROM         dbo.tblClaimantPersonalInfo AS CPI LEFT OUTER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID LEFT OUTER JOIN
					  (SELECT * FROM tblClaimDisease WHERE PrimaryDisease = 1) AS CD ON CD.ClaimID = C.ClaimID LEFT OUTER JOIN
                      dbo.tblDisease AS DDefense ON CD.DiseaseID = DDefense.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS HLDDefense ON HLDDefense.HighLevelDiseaseID = DDefense.HighLevelDiseaseID LEFT OUTER JOIN
                      dbo.tblClaimSequence AS ClaimS ON ClaimS.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID
                      Left Join (Select * From dbo.tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                      INNER JOIN dbo.tblClaimLawsuitStatus AS CLS ON CLST.ClaimLawsuitStatusID = CLS.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS CS ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
                      left outer join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
                      left outer join tblCheck ck on ck.CheckID = ps.CheckID
WHERE     (CLst.ClaimLawsuitStatusID IN (23, 25, 28)) AND (CL.IsPrimaryForClaim = 1) AND (ck.SourceDate IS NOT NULL) OR
                      (CLst.ClaimLawsuitStatusID IN (23, 25, 28)) AND (CL.IsPrimaryForClaim = 1) AND (ck.SourceNumber IS NOT NULL)
GROUP BY C.ClaimID, HLDDefense.HighLevelDisease, S.State, J.Jurisdiction, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END, CS.SettlementDate, CLS.ClaimLawsuitStatus, CS.SettlementAmount,ck.SourceDate  , CS.SettlementDueDate, 
                      L.PrimaryLocalPlaintiffCounsel, clst.ClaimLawsuitStatusID, ck.SourceNumber  , CS.SettlementGroupID, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE SocialSecurityNumber END, DocketNumber
GO
GRANT VIEW DEFINITION ON  [dbo].[vSP_REPORTS_PaidClaims_v2] TO [base_user]
GRANT SELECT ON  [dbo].[vSP_REPORTS_PaidClaims_v2] TO [base_user]
GRANT SELECT ON  [dbo].[vSP_REPORTS_PaidClaims_v2] TO [power_user]
GRANT INSERT ON  [dbo].[vSP_REPORTS_PaidClaims_v2] TO [power_user]
GRANT UPDATE ON  [dbo].[vSP_REPORTS_PaidClaims_v2] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[22] 4[22] 2[37] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CPI"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "C"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 279
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DDefense"
            Begin Extent = 
               Top = 6
               Left = 285
               Bottom = 99
               Right = 475
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "HLDDefense"
            Begin Extent = 
               Top = 102
               Left = 285
               Bottom = 180
               Right = 475
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DPlaintiff"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 315
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "HLDPlaintiff"
            Begin Extent = 
               Top = 222
               Left = 266
               Bottom = 300
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ClaimS"
            Begin Extent = 
               Top = 300
               Left = 266
               Bottom = 378
               Right = 445
            End
            DisplayFlags = 280', 'SCHEMA', N'dbo', 'VIEW', N'vSP_REPORTS_PaidClaims_v2', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'
            TopColumn = 0
         End
         Begin Table = "CL"
            Begin Extent = 
               Top = 318
               Left = 38
               Bottom = 426
               Right = 250
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CLS"
            Begin Extent = 
               Top = 426
               Left = 38
               Bottom = 504
               Right = 237
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "L"
            Begin Extent = 
               Top = 504
               Left = 38
               Bottom = 612
               Right = 283
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "J"
            Begin Extent = 
               Top = 378
               Left = 288
               Bottom = 456
               Right = 455
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "S"
            Begin Extent = 
               Top = 612
               Left = 38
               Bottom = 720
               Right = 205
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CS"
            Begin Extent = 
               Top = 720
               Left = 38
               Bottom = 828
               Right = 278
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 6015
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSP_REPORTS_PaidClaims_v2', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSP_REPORTS_PaidClaims_v2', NULL, NULL
GO
