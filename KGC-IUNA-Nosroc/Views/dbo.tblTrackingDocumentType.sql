SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [dbo].[tblTrackingDocumentType] as
select * from [kga-claimsadmin].dbo.tblDocumentType
GO
GRANT SELECT ON  [dbo].[tblTrackingDocumentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblTrackingDocumentType] TO [power_user]
GO
