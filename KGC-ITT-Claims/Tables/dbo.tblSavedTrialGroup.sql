CREATE TABLE [dbo].[tblSavedTrialGroup]
(
[SavedTrialGroupID] [bigint] NOT NULL IDENTITY(1, 1),
[TrialGroupID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedTrialGroup] ADD CONSTRAINT [PK_tblSavedTrialGroup] PRIMARY KEY CLUSTERED  ([SavedTrialGroupID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedTrialGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedTrialGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedTrialGroup] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedTrialGroup] TO [power_user]
GO
