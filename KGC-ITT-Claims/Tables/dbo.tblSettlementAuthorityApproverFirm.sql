CREATE TABLE [dbo].[tblSettlementAuthorityApproverFirm]
(
[SettlementAuthorityApproverFirmID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementAuthorityID] [bigint] NOT NULL,
[SettlementApproverFirmID] [bigint] NOT NULL,
[ApprovalRequired] [bit] NOT NULL,
[ResponseWindowDays] [int] NOT NULL CONSTRAINT [DF_tblSettlementAuthorityApproverFirm_ResponseWindowDays] DEFAULT ((0)),
[HasResponded] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementAuthorityApproverFirm] ADD CONSTRAINT [PK_tblSettlementAuthorityApproverFirmID] PRIMARY KEY CLUSTERED  ([SettlementAuthorityApproverFirmID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementAuthorityApproverFirm] ADD CONSTRAINT [FK_tblSettlementAuthorityApproverFirmID_tblSettlementAuthorityApproverFirmID] FOREIGN KEY ([SettlementAuthorityApproverFirmID]) REFERENCES [dbo].[tblSettlementAuthorityApproverFirm] ([SettlementAuthorityApproverFirmID])
GO
ALTER TABLE [dbo].[tblSettlementAuthorityApproverFirm] ADD CONSTRAINT [FK_tblSettlementAuthorityApproverFirmID_tblSettlementAuthority] FOREIGN KEY ([SettlementAuthorityID]) REFERENCES [dbo].[tblSettlementAuthority] ([SettlementAuthorityID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityApproverFirm] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityApproverFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityApproverFirm] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityApproverFirm] TO [power_user]
GO
