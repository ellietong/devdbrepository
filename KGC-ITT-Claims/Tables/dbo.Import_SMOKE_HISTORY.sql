CREATE TABLE [dbo].[Import_SMOKE_HISTORY]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[START_DATE] [datetime] NULL,
[QUIT_DATE] [datetime] NULL,
[PACKS_PER_DAY] [decimal] (5, 2) NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_SMOKE_HISTORY] TO [base_user]
GRANT SELECT ON  [dbo].[Import_SMOKE_HISTORY] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_SMOKE_HISTORY] TO [power_user]
GRANT SELECT ON  [dbo].[Import_SMOKE_HISTORY] TO [power_user]
GO
