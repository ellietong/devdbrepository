CREATE TABLE [dbo].[tblCheckRecipient]
(
[CheckRecipientID] [int] NOT NULL IDENTITY(1, 1),
[DefenseCounselAddressID] [bigint] NULL,
[RecipientName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCheckRecipient] ADD CONSTRAINT [PK_tblCheckRecipient] PRIMARY KEY CLUSTERED  ([CheckRecipientID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCheckRecipient] TO [base_user]
GRANT SELECT ON  [dbo].[tblCheckRecipient] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckRecipient] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblCheckRecipient] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblCheckRecipient] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblCheckRecipient] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckRecipient] TO [power_user]
GRANT SELECT ON  [dbo].[tblCheckRecipient] TO [power_user]
GO
