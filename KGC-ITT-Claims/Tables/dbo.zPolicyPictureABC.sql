CREATE TABLE [dbo].[zPolicyPictureABC]
(
[ID] [int] NOT NULL,
[policy_no] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Carrier] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Incept] [datetime] NULL,
[term] [datetime] NULL,
[attach] [money] NULL,
[perocclim] [money] NULL,
[bquotashr] [int] NULL,
[quotashrlim] [money] NULL,
[code] [int] NULL,
[excl_alloc] [int] NULL,
[Consume] [int] NULL,
[TotCost] [int] NULL,
[PHolder] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Layer] [int] NULL,
[Agglim] [money] NULL,
[Separation] [int] NULL,
[Imgfile] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Totcost2] [money] NULL,
[Totcost3] [money] NULL,
[Totcost4] [money] NULL,
[Totcost5] [money] NULL,
[Costinadd] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Totcost6] [money] NULL,
[Totcost7] [money] NULL,
[Totcost8] [money] NULL,
[Totcost9] [money] NULL,
[Totcost10] [money] NULL,
[Totcost11] [money] NULL,
[Totcost12] [money] NULL,
[Totcost13] [money] NULL,
[Totcost14] [money] NULL,
[Totcost15] [money] NULL,
[Totcost16] [money] NULL,
[Totcost17] [money] NULL,
[Totcost18] [money] NULL,
[Totcost19] [money] NULL,
[Totcost20] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[zPolicyPictureABC] TO [Allocation_User]
GRANT INSERT ON  [dbo].[zPolicyPictureABC] TO [Allocation_User]
GRANT DELETE ON  [dbo].[zPolicyPictureABC] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[zPolicyPictureABC] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[zPolicyPictureABC] TO [base_user]
GRANT SELECT ON  [dbo].[zPolicyPictureABC] TO [base_user]
GRANT SELECT ON  [dbo].[zPolicyPictureABC] TO [PolPic_User]
GRANT INSERT ON  [dbo].[zPolicyPictureABC] TO [PolPic_User]
GRANT DELETE ON  [dbo].[zPolicyPictureABC] TO [PolPic_User]
GRANT UPDATE ON  [dbo].[zPolicyPictureABC] TO [PolPic_User]
GRANT VIEW DEFINITION ON  [dbo].[zPolicyPictureABC] TO [power_user]
GRANT SELECT ON  [dbo].[zPolicyPictureABC] TO [power_user]
GRANT INSERT ON  [dbo].[zPolicyPictureABC] TO [power_user]
GRANT UPDATE ON  [dbo].[zPolicyPictureABC] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Agglim'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Agglim'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Agglim'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Costinadd'
GO
EXEC sp_addextendedproperty N'MS_Format', NULL, 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Costinadd'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Costinadd'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Imgfile'
GO
EXEC sp_addextendedproperty N'MS_Format', NULL, 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Imgfile'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Imgfile'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Layer'
GO
EXEC sp_addextendedproperty N'MS_Format', NULL, 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Layer'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Layer'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'PHolder'
GO
EXEC sp_addextendedproperty N'MS_Format', NULL, 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'PHolder'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'PHolder'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Separation'
GO
EXEC sp_addextendedproperty N'MS_Format', NULL, 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Separation'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Separation'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost2'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost2'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost2'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost3'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost3'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost3'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost4'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost4'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost4'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost5'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost5'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost5'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost6'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost6'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost6'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost7'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost7'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'zPolicyPictureABC', 'COLUMN', N'Totcost7'
GO
