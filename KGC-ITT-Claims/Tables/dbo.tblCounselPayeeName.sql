CREATE TABLE [dbo].[tblCounselPayeeName]
(
[CounselPayeeNameID] [int] NOT NULL IDENTITY(1, 1),
[CounselAddressID] [int] NULL,
[CounselPayeeName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCounselPayeeName] TO [base_user]
GRANT SELECT ON  [dbo].[tblCounselPayeeName] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCounselPayeeName] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblCounselPayeeName] TO [CheckWriter_User]
GRANT INSERT ON  [dbo].[tblCounselPayeeName] TO [CheckWriter_User]
GRANT DELETE ON  [dbo].[tblCounselPayeeName] TO [CheckWriter_User]
GRANT UPDATE ON  [dbo].[tblCounselPayeeName] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCounselPayeeName] TO [power_user]
GRANT SELECT ON  [dbo].[tblCounselPayeeName] TO [power_user]
GRANT INSERT ON  [dbo].[tblCounselPayeeName] TO [power_user]
GRANT DELETE ON  [dbo].[tblCounselPayeeName] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCounselPayeeName] TO [power_user]
GO
