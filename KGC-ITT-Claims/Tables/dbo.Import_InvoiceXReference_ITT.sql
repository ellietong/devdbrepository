CREATE TABLE [dbo].[Import_InvoiceXReference_ITT]
(
[INVOICEIDENTIFIER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILENAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_InvoiceXReference_ITT] TO [base_user]
GRANT SELECT ON  [dbo].[Import_InvoiceXReference_ITT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_InvoiceXReference_ITT] TO [power_user]
GRANT SELECT ON  [dbo].[Import_InvoiceXReference_ITT] TO [power_user]
GO
