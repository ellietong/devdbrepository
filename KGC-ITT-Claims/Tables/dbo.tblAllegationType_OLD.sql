CREATE TABLE [dbo].[tblAllegationType_OLD]
(
[AllegationTypeID] [int] NOT NULL IDENTITY(1, 1),
[AllegationType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegationType_OLD] ADD CONSTRAINT [PK_mBtblAllegationLookup] PRIMARY KEY CLUSTERED  ([AllegationTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllegationType_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllegationType_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllegationType_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllegationType_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllegationType_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllegationType_OLD] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllegationType_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblAllegationType_OLD', 'COLUMN', N'AllegationType'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllegationType_OLD', 'COLUMN', N'AllegationType'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblAllegationType_OLD', 'COLUMN', N'AllegationType'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblAllegationType_OLD', 'COLUMN', N'AllegationTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllegationType_OLD', 'COLUMN', N'AllegationTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblAllegationType_OLD', 'COLUMN', N'AllegationTypeID'
GO
