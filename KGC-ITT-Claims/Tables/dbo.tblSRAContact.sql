CREATE TABLE [dbo].[tblSRAContact]
(
[SRAContactID] [int] NOT NULL IDENTITY(1, 1),
[StateID] [int] NOT NULL,
[PersonID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSRAContact] ADD CONSTRAINT [PK_tblSRAContact] PRIMARY KEY CLUSTERED  ([SRAContactID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSRAContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblSRAContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSRAContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblSRAContact] TO [power_user]
GO
