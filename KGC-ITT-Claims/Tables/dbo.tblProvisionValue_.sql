CREATE TABLE [dbo].[tblProvisionValue_]
(
[ProvisionValueID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionLabelID] [int] NOT NULL,
[ProvisionValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionValue_] ADD CONSTRAINT [PK__tblProvisionValu__24927208] PRIMARY KEY CLUSTERED  ([ProvisionValueID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionValue_] ADD CONSTRAINT [FK_tblProvisionValue_tblProvisionLabel] FOREIGN KEY ([ProvisionLabelID]) REFERENCES [dbo].[tblProvisionLabel_] ([ProvisionLabelID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionValue_] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionValue_] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionValue_] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionValue_] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvisionValue_] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvisionValue_] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionValue_] TO [Provision_User]
GRANT INSERT ON  [dbo].[tblProvisionValue_] TO [Provision_User]
GRANT DELETE ON  [dbo].[tblProvisionValue_] TO [Provision_User]
GRANT UPDATE ON  [dbo].[tblProvisionValue_] TO [Provision_User]
GO
