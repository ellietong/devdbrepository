CREATE TABLE [dbo].[tblSettlementAuthority]
(
[SettlementAuthorityID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementGroupID] [bigint] NOT NULL,
[AuthorityRequestedDate] [datetime] NOT NULL,
[AuthorityRequestedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DemandAmount] [money] NULL,
[AuthorityAmount] [money] NULL,
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthorityGranted] [bit] NOT NULL CONSTRAINT [DF_tblSettlementAuthority_EmailAlertSent] DEFAULT ((0)),
[DocumentID] [bigint] NULL,
[ResponseDueDate] [datetime] NULL,
[RequestApprovedDate] [datetime] NULL,
[RequestApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsRequestApproved] [bit] NULL CONSTRAINT [DF_tblSettlementAuthority_IsRequestApproved] DEFAULT ((0)),
[AuthorityDenied] [bit] NULL CONSTRAINT [DF_tblSettlementAuthority_AuthorityDenied] DEFAULT ((0)),
[ParentSettlementAuthorityID] [bigint] NULL,
[RequestNotificationDate] [datetime] NULL,
[ClaimLawsuitID] [bigint] NULL,
[InsurerNotificationDate] [datetime] NULL,
[FinalizedNotificationDate] [datetime] NULL,
[AdditionalRequestNotificationDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementAuthority] ADD CONSTRAINT [PK_tblSettlementAuthority] PRIMARY KEY CLUSTERED  ([SettlementAuthorityID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementAuthority] ADD CONSTRAINT [FK_tblSettlementAuthority_tblClaimLawsuit] FOREIGN KEY ([SettlementGroupID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthority] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthority] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthority] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthority] TO [power_user]
GO
