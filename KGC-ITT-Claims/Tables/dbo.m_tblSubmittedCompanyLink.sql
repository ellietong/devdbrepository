CREATE TABLE [dbo].[m_tblSubmittedCompanyLink]
(
[SubmittedBy] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblSubmittedCompanyLink] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblSubmittedCompanyLink] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblSubmittedCompanyLink] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblSubmittedCompanyLink] TO [power_user]
GO
