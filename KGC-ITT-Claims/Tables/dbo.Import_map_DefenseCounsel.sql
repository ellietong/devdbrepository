CREATE TABLE [dbo].[Import_map_DefenseCounsel]
(
[PACE_Defense_Counsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_map_DefenseCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[Import_map_DefenseCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_map_DefenseCounsel] TO [power_user]
GRANT SELECT ON  [dbo].[Import_map_DefenseCounsel] TO [power_user]
GO
