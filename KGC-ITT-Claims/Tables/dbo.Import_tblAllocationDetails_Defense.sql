CREATE TABLE [dbo].[Import_tblAllocationDetails_Defense]
(
[ID] [int] NULL,
[AllocationID] [int] NULL,
[ClaimID] [int] NULL,
[SegmentID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyID] [int] NULL,
[EffectiveDays] [int] NULL,
[PercentOfDays] [int] NULL,
[TotalIndemnity] [money] NULL,
[TotalDefense] [money] NULL,
[AmountType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScenarioID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_tblAllocationDetails_Defense] TO [base_user]
GRANT SELECT ON  [dbo].[Import_tblAllocationDetails_Defense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_tblAllocationDetails_Defense] TO [power_user]
GRANT SELECT ON  [dbo].[Import_tblAllocationDetails_Defense] TO [power_user]
GO
