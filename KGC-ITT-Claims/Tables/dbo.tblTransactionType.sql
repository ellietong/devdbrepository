CREATE TABLE [dbo].[tblTransactionType]
(
[TransactionTypeID] [int] NOT NULL IDENTITY(1, 1),
[TransactionType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTransactionType] ADD CONSTRAINT [PK_mBtblTransactionType] PRIMARY KEY CLUSTERED  ([TransactionTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTransactionType] TO [base_user]
GRANT SELECT ON  [dbo].[tblTransactionType] TO [base_user]
GRANT SELECT ON  [dbo].[tblTransactionType] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblTransactionType] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblTransactionType] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTransactionType] TO [power_user]
GRANT SELECT ON  [dbo].[tblTransactionType] TO [power_user]
GRANT INSERT ON  [dbo].[tblTransactionType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTransactionType] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1000000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionType'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionType'
GO
DECLARE @xp int
SELECT @xp=2955
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionType'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionType'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionType'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionType'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionTypeID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionTypeID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblTransactionType', 'COLUMN', N'TransactionTypeID'
GO
