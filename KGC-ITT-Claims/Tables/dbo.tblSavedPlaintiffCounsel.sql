CREATE TABLE [dbo].[tblSavedPlaintiffCounsel]
(
[SavedPlaintiffCounselID] [bigint] NOT NULL IDENTITY(1, 1),
[PlaintiffCounselID] [int] NULL,
[SavedSearchID] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedPlaintiffCounsel] ADD CONSTRAINT [PK_tblSavedPlaintiffCounsel] PRIMARY KEY CLUSTERED  ([SavedPlaintiffCounselID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedPlaintiffCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedPlaintiffCounsel] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedPlaintiffCounsel] TO [power_user]
GO
