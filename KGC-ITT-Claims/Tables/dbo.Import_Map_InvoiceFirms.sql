CREATE TABLE [dbo].[Import_Map_InvoiceFirms]
(
[SUBMITTED_FROM_COMPANY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselID] [float] NULL,
[defensecounseladdressid] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_Map_InvoiceFirms] TO [base_user]
GRANT SELECT ON  [dbo].[Import_Map_InvoiceFirms] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_Map_InvoiceFirms] TO [power_user]
GRANT SELECT ON  [dbo].[Import_Map_InvoiceFirms] TO [power_user]
GO
