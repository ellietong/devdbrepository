CREATE TABLE [dbo].[tblSubscriberAgency_ALT]
(
[AgencyID] [int] NOT NULL IDENTITY(1, 1),
[AgencyName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriberAgency_ALT] ADD CONSTRAINT [PK_tblSubscriberAgency] PRIMARY KEY CLUSTERED  ([AgencyID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriberAgency_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriberAgency_ALT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriberAgency_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriberAgency_ALT] TO [power_user]
GRANT INSERT ON  [dbo].[tblSubscriberAgency_ALT] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSubscriberAgency_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriberAgency_ALT] TO [Subscription_User]
GRANT INSERT ON  [dbo].[tblSubscriberAgency_ALT] TO [Subscription_User]
GRANT DELETE ON  [dbo].[tblSubscriberAgency_ALT] TO [Subscription_User]
GRANT UPDATE ON  [dbo].[tblSubscriberAgency_ALT] TO [Subscription_User]
GO
