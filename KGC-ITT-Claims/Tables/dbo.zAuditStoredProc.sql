CREATE TABLE [dbo].[zAuditStoredProc]
(
[AuditStoredProcID] [bigint] NOT NULL IDENTITY(1, 1),
[RunGUID] [uniqueidentifier] NULL,
[CodeBlockName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodeBlockStartTime] [datetime] NULL,
[CodeBlockEndTime] [datetime] NULL,
[CodeBlockRunTime] AS ([CodeBlockEndTime]-[CodeBlockStartTime]),
[EmailAddress] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zAuditStoredProc] ADD CONSTRAINT [PK_zAuditStoredProc] PRIMARY KEY CLUSTERED  ([AuditStoredProcID]) ON [PRIMARY]
GO
