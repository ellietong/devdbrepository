CREATE TABLE [dbo].[FINAL_DocXRef_ITT]
(
[REFERENCE_ID] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCUMENT_NUMBER] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCUMENT_TITLE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCUMENT_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILE_NAME] [decimal] (10, 0) NULL,
[FOLDER] [nvarchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MERGEDFILEPATH] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MERGEDFILEPATH_DOCUMENT_NUMBER] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[FINAL_DocXRef_ITT] TO [base_user]
GRANT SELECT ON  [dbo].[FINAL_DocXRef_ITT] TO [base_user]
GRANT SELECT ON  [dbo].[FINAL_DocXRef_ITT] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[FINAL_DocXRef_ITT] TO [power_user]
GRANT SELECT ON  [dbo].[FINAL_DocXRef_ITT] TO [power_user]
GO
