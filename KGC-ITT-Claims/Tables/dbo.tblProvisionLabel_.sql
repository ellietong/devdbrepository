CREATE TABLE [dbo].[tblProvisionLabel_]
(
[ProvisionLabelID] [int] NOT NULL IDENTITY(1, 1),
[Provision] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentProvisionID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionLabel_] ADD CONSTRAINT [PK_tblProvisionLabel] PRIMARY KEY CLUSTERED  ([ProvisionLabelID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionLabel_] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionLabel_] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionLabel_] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionLabel_] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvisionLabel_] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvisionLabel_] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionLabel_] TO [Provision_User]
GRANT INSERT ON  [dbo].[tblProvisionLabel_] TO [Provision_User]
GRANT DELETE ON  [dbo].[tblProvisionLabel_] TO [Provision_User]
GRANT UPDATE ON  [dbo].[tblProvisionLabel_] TO [Provision_User]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionLabel_', 'COLUMN', N'Provision'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionLabel_', 'COLUMN', N'Provision'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionLabel_', 'COLUMN', N'Provision'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionLabel_', 'COLUMN', N'ProvisionLabelID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionLabel_', 'COLUMN', N'ProvisionLabelID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionLabel_', 'COLUMN', N'ProvisionLabelID'
GO
