CREATE TABLE [dbo].[tblClaimantSettlementAuthority]
(
[ClaimantSettlementAuthorityID] [int] NULL,
[RequestedAmount] [money] NULL,
[AuthorizedAmount] [money] NULL,
[AuthorizedDate] [datetime] NULL,
[AuthorizedBy] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSettlementAuthority] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlementAuthority] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSettlementAuthority] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlementAuthority] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantSettlementAuthority] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantSettlementAuthority] TO [power_user]
GO
