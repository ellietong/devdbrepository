CREATE TABLE [dbo].[tblExactLanguage]
(
[ExactLanguageID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NULL,
[GenericLanguageID] [int] NULL,
[ExactLanguage] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatesStart] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatesEnd] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LanguageNotes] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationID] [int] NULL,
[EndorsementNum] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EffectiveDate] [datetime] NULL,
[DetailedLocationID] [int] NULL,
[Sort] [int] NULL,
[DoNotShow] [bit] NULL,
[FormID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExactLanguage] ADD CONSTRAINT [PK_tblProvisionLanguage] PRIMARY KEY CLUSTERED  ([ExactLanguageID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExactLanguage] ADD CONSTRAINT [FK_tblProvisionLanguage_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy_] ([PolicyID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExactLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblExactLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblExactLanguage] TO [Language_User]
GRANT INSERT ON  [dbo].[tblExactLanguage] TO [Language_User]
GRANT DELETE ON  [dbo].[tblExactLanguage] TO [Language_User]
GRANT UPDATE ON  [dbo].[tblExactLanguage] TO [Language_User]
GRANT VIEW DEFINITION ON  [dbo].[tblExactLanguage] TO [power_user]
GRANT SELECT ON  [dbo].[tblExactLanguage] TO [power_user]
GRANT INSERT ON  [dbo].[tblExactLanguage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExactLanguage] TO [power_user]
GO
