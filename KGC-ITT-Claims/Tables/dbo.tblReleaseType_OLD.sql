CREATE TABLE [dbo].[tblReleaseType_OLD]
(
[ReleaseTypeID] [int] NOT NULL IDENTITY(1, 1),
[ReleaseType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblReleaseType_OLD] ADD CONSTRAINT [PK_mBtblReleaseType] PRIMARY KEY CLUSTERED  ([ReleaseTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblReleaseType_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblReleaseType_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblReleaseType_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblReleaseType_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblReleaseType_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblReleaseType_OLD] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblReleaseType_OLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblReleaseType_OLD', 'COLUMN', N'ReleaseType'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblReleaseType_OLD', 'COLUMN', N'ReleaseType'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblReleaseType_OLD', 'COLUMN', N'ReleaseType'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblReleaseType_OLD', 'COLUMN', N'ReleaseTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblReleaseType_OLD', 'COLUMN', N'ReleaseTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblReleaseType_OLD', 'COLUMN', N'ReleaseTypeID'
GO
