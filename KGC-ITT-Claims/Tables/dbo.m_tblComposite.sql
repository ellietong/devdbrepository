CREATE TABLE [dbo].[m_tblComposite]
(
[CompositeID] [int] NOT NULL,
[CompositeTypeID] [int] NULL,
[ExposureDate] [date] NULL,
[DiagnosisDate] [date] NULL,
[ProfileShare] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblComposite] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblComposite] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblComposite] TO [Billing_User]
GRANT INSERT ON  [dbo].[m_tblComposite] TO [Billing_User]
GRANT DELETE ON  [dbo].[m_tblComposite] TO [Billing_User]
GRANT UPDATE ON  [dbo].[m_tblComposite] TO [Billing_User]
GRANT SELECT ON  [dbo].[m_tblComposite] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[m_tblComposite] TO [Occurrence_User]
GRANT DELETE ON  [dbo].[m_tblComposite] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[m_tblComposite] TO [Occurrence_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblComposite] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblComposite] TO [power_user]
GO
