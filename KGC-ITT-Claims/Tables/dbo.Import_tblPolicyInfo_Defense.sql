CREATE TABLE [dbo].[Import_tblPolicyInfo_Defense]
(
[ScenarioID] [int] NULL,
[PolicyID] [int] NULL,
[InsurerID] [int] NULL,
[InsurerName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[LimitAmount] [money] NULL,
[Layer] [int] NULL,
[TowerYear] [int] NULL,
[PolDefForm] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_tblPolicyInfo_Defense] TO [base_user]
GRANT SELECT ON  [dbo].[Import_tblPolicyInfo_Defense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_tblPolicyInfo_Defense] TO [power_user]
GRANT SELECT ON  [dbo].[Import_tblPolicyInfo_Defense] TO [power_user]
GO
