CREATE TABLE [dbo].[tblClaimantSettlement]
(
[ClaimantSettlementID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NOT NULL,
[SettlementDate] [datetime] NULL,
[SettlementAmount] [money] NULL,
[SettlementDueDate] [datetime] NULL,
[SettlementNotificationDate] [datetime] NULL,
[SettlementsFundsRequestDate] [datetime] NULL,
[PayToCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayToFirmAddressID] [int] NULL,
[CheckNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaidDate] [datetime] NULL,
[SettlementGroupID] [bigint] NULL,
[SettlementNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavResolutionKey] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantSettlementAuthorityID] [int] NULL,
[PaymentRecommendedBy] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentRecommendedDate] [datetime] NULL,
[PaymentApprovedBy] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentApprovedDate] [datetime] NULL,
[InsurerSettlementAmount] [money] NULL,
[BasisOfRecommendation] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSettlement] ADD CONSTRAINT [PK_mBtblClaimantSettlement] PRIMARY KEY CLUSTERED  ([ClaimantSettlementID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSettlement] ADD CONSTRAINT [FK_tblClaimantSettlement_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSettlement] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantSettlement] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantSettlement] TO [power_user]
GO
