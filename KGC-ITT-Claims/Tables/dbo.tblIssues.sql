CREATE TABLE [dbo].[tblIssues]
(
[IssueID] [int] NOT NULL IDENTITY(1, 1),
[IssueTypeID] [int] NULL,
[PolicyID] [int] NULL,
[IssueDescription] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IssueDate] [smalldatetime] NULL,
[Resolved] [bit] NOT NULL,
[ResolvedBy] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Resolution] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MngrReviewNeeded] [bit] NULL,
[MngrReviewDate] [datetime] NULL,
[MngrApproval] [nchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblIssues] ADD CONSTRAINT [PK_tblIssues] PRIMARY KEY NONCLUSTERED  ([IssueID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblIssues] ADD CONSTRAINT [FK_tblIssues_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy_] ([PolicyID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblIssues] TO [base_user]
GRANT SELECT ON  [dbo].[tblIssues] TO [base_user]
GRANT SELECT ON  [dbo].[tblIssues] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblIssues] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblIssues] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblIssues] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblIssues] TO [power_user]
GRANT SELECT ON  [dbo].[tblIssues] TO [power_user]
GRANT INSERT ON  [dbo].[tblIssues] TO [power_user]
GRANT UPDATE ON  [dbo].[tblIssues] TO [power_user]
GO
