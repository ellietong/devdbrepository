CREATE TABLE [dbo].[tblMedicareBeneficiaryStatus_OLD]
(
[MedicareBeneficiaryStatusID] [int] NOT NULL IDENTITY(1, 1),
[MedicareBeneficiaryStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMedicareBeneficiaryStatus_OLD] ADD CONSTRAINT [PK_tblMedicareBeneficiaryStatus] PRIMARY KEY CLUSTERED  ([MedicareBeneficiaryStatusID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMedicareBeneficiaryStatus_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblMedicareBeneficiaryStatus_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMedicareBeneficiaryStatus_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblMedicareBeneficiaryStatus_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblMedicareBeneficiaryStatus_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMedicareBeneficiaryStatus_OLD] TO [power_user]
GO
