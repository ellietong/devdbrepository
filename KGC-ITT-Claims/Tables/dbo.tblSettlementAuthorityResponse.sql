CREATE TABLE [dbo].[tblSettlementAuthorityResponse]
(
[SettlementAuthorityResponseID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementAuthorityStatusID] [int] NULL,
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentSettlementAuthorityResponseID] [bigint] NULL,
[CreatedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SettlementAuthorityID] [bigint] NOT NULL,
[DateCreated] [datetime] NULL,
[ResponseNotificationDate] [datetime] NULL,
[FirmID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementAuthorityResponse] ADD CONSTRAINT [PK_tblSettlementAuthorityResponse] PRIMARY KEY CLUSTERED  ([SettlementAuthorityResponseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_ParentSettlementAuthorityResponseID] ON [dbo].[tblSettlementAuthorityResponse] ([ParentSettlementAuthorityResponseID], [SettlementAuthorityID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementAuthorityResponse] ADD CONSTRAINT [FK_tblSettlementAuthorityResponse_tblSettlementAuthority] FOREIGN KEY ([SettlementAuthorityID]) REFERENCES [dbo].[tblSettlementAuthority] ([SettlementAuthorityID])
GO
ALTER TABLE [dbo].[tblSettlementAuthorityResponse] ADD CONSTRAINT [FK_tblSettlementAuthorityResponse_tblSettlementAuthorityStatus] FOREIGN KEY ([SettlementAuthorityStatusID]) REFERENCES [dbo].[tblSettlementAuthorityStatus] ([SettlementAuthorityStatusID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityResponse] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityResponse] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityResponse] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityResponse] TO [power_user]
GO
