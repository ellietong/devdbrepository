CREATE TABLE [dbo].[tblSettlementApproverFirm]
(
[SettlementApproverFirmID] [int] NOT NULL IDENTITY(1, 1),
[FirmID] [int] NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_tblSettlementApproverFirms_IsActive] DEFAULT ((1)),
[ApprovalRequired_Default] [bit] NOT NULL CONSTRAINT [DF_tblSettlementApproverFirms_ApprovalRequired] DEFAULT ((0)),
[AuthorityAmountThreshold] [money] NOT NULL CONSTRAINT [DF_tblSettlementApproverFirms_AuthorityAmountThreshold] DEFAULT ((0)),
[ResponseWindowDays_Default] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementApproverFirm] ADD CONSTRAINT [PK_tblSettlementApproverFirms] PRIMARY KEY CLUSTERED  ([SettlementApproverFirmID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementApproverFirm] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementApproverFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementApproverFirm] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementApproverFirm] TO [power_user]
GO
