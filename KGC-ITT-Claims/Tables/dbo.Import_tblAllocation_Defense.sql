CREATE TABLE [dbo].[Import_tblAllocation_Defense]
(
[ScenarioID] [int] NULL,
[AllocationID] [int] NULL,
[MonthlyBillID] [int] NULL,
[BillNumber] [bigint] NULL,
[Month] [int] NULL,
[Year] [int] NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_tblAllocation_Defense] TO [base_user]
GRANT SELECT ON  [dbo].[Import_tblAllocation_Defense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_tblAllocation_Defense] TO [power_user]
GRANT SELECT ON  [dbo].[Import_tblAllocation_Defense] TO [power_user]
GO
