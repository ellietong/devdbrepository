CREATE TABLE [dbo].[MidlandImport2014050]
(
[ClaimantID] [bigint] NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [bigint] NULL
) ON [PRIMARY]
GO
