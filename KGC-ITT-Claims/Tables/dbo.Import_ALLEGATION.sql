CREATE TABLE [dbo].[Import_ALLEGATION]
(
[CLM_LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALLEGATION] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DAMAGE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MATERIAL] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_ALLEGATION] TO [base_user]
GRANT SELECT ON  [dbo].[Import_ALLEGATION] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_ALLEGATION] TO [power_user]
GRANT SELECT ON  [dbo].[Import_ALLEGATION] TO [power_user]
GO
