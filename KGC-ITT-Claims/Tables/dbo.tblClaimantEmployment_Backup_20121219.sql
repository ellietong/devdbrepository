CREATE TABLE [dbo].[tblClaimantEmployment_Backup_20121219]
(
[ClaimantEmploymentID] [bigint] NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[OccupationID] [int] NULL,
[Jobsite] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmpStartDate] [datetime] NULL,
[EmpEndDate] [datetime] NULL,
[EarliestExposureDate] [datetime] NULL,
[LatestExposureDate] [datetime] NULL,
[IsPrimary] [bit] NULL,
[JobSiteCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobSiteStateID] [int] NULL,
[IsSecondaryExposure] [bit] NULL,
[SPUsername] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsApprovedExposure] [bit] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment_Backup_20121219] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment_Backup_20121219] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment_Backup_20121219] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment_Backup_20121219] TO [power_user]
GO
