CREATE TABLE [dbo].[tblPolicyLimitType_]
(
[LimitTypeID] [int] NOT NULL IDENTITY(1, 1),
[LimitType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyLimitType_] ADD CONSTRAINT [PK_tblPolicyLimitType] PRIMARY KEY CLUSTERED  ([LimitTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyLimitType_] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyLimitType_] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyLimitType_] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblPolicyLimitType_] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblPolicyLimitType_] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblPolicyLimitType_] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyLimitType_] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicyLimitType_] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicyLimitType_] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicyLimitType_] TO [power_user]
GO
