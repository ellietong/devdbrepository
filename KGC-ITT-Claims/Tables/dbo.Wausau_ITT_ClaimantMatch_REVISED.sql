CREATE TABLE [dbo].[Wausau_ITT_ClaimantMatch_REVISED]
(
[ID] [float] NULL,
[Wausau_Claimant] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITT_Claimant] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Wausau_ITT_ClaimantMatch_REVISED] TO [base_user]
GRANT SELECT ON  [dbo].[Wausau_ITT_ClaimantMatch_REVISED] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Wausau_ITT_ClaimantMatch_REVISED] TO [power_user]
GRANT SELECT ON  [dbo].[Wausau_ITT_ClaimantMatch_REVISED] TO [power_user]
GO
