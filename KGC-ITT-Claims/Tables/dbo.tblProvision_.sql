CREATE TABLE [dbo].[tblProvision_]
(
[ProvisionID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionLabelID] [int] NULL,
[PolicyID] [int] NULL,
[ProvisionValueID] [int] NULL,
[ProvisionNotes] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvision_] ADD CONSTRAINT [PK_dropTemptblProvision] PRIMARY KEY CLUSTERED  ([ProvisionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvision_] ADD CONSTRAINT [FK_tblProvision_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy_] ([PolicyID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProvision_] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvision_] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblProvision_] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvision_] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvision_] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvision_] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvision_] TO [Provision_User]
GRANT INSERT ON  [dbo].[tblProvision_] TO [Provision_User]
GRANT DELETE ON  [dbo].[tblProvision_] TO [Provision_User]
GRANT UPDATE ON  [dbo].[tblProvision_] TO [Provision_User]
GO
