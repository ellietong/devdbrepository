CREATE TABLE [dbo].[ITT_MissingInWausau_204]
(
[ID] [float] NULL,
[ITT_Claimant] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ITT_MissingInWausau_204] TO [base_user]
GRANT SELECT ON  [dbo].[ITT_MissingInWausau_204] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ITT_MissingInWausau_204] TO [power_user]
GRANT SELECT ON  [dbo].[ITT_MissingInWausau_204] TO [power_user]
GO
