CREATE TABLE [dbo].[tblDefendantCompanyInsProg]
(
[DefendantCompanyInsCoID] [int] NOT NULL IDENTITY(1, 1),
[DefendantCompanyID] [int] NULL,
[InsuranceProgramID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefendantCompanyInsProg] ADD CONSTRAINT [PK_tblDefendantCompanyInsCo] PRIMARY KEY CLUSTERED  ([DefendantCompanyInsCoID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompanyInsProg] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefendantCompanyInsProg] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompanyInsProg] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefendantCompanyInsProg] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefendantCompanyInsProg] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefendantCompanyInsProg] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefendantCompanyInsProg] TO [power_user]
GO
