CREATE TABLE [dbo].[LossRun_Wausau]
(
[EFF YEAR] [datetime] NULL,
[POLICY_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EVENT_NO#] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLAIMANT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LITIGANT_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOSS_PAID] [money] NULL,
[Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[LossRun_Wausau] TO [base_user]
GRANT SELECT ON  [dbo].[LossRun_Wausau] TO [base_user]
GRANT SELECT ON  [dbo].[LossRun_Wausau] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[LossRun_Wausau] TO [power_user]
GRANT SELECT ON  [dbo].[LossRun_Wausau] TO [power_user]
GO
