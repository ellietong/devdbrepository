CREATE TABLE [dbo].[tblClaimDisease]
(
[ClaimDiseaseID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID] [bigint] NULL,
[DiseaseID] [int] NULL,
[PrimaryDisease] [bit] NULL,
[DiseaseDiagnosisDate] [datetime] NULL,
[DoctorID] [int] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiseaseID_OLD] [int] NULL,
[IsDefenseDisease] [bit] NULL CONSTRAINT [DF_tblClaimDisease_IsDefenseDisease] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimDisease] ADD CONSTRAINT [PK_tblClaimDisease] PRIMARY KEY CLUSTERED  ([ClaimDiseaseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimDisease_ClaimID_PrimaryDisease] ON [dbo].[tblClaimDisease] ([ClaimID], [PrimaryDisease]) INCLUDE ([DiseaseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimDisease_PrimaryDisease] ON [dbo].[tblClaimDisease] ([PrimaryDisease]) INCLUDE ([ClaimID], [DiseaseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimDisease] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimDisease] TO [power_user]
GO
