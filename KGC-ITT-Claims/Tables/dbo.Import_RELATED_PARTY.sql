CREATE TABLE [dbo].[Import_RELATED_PARTY]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELATIONSHIP] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEGAL_CAPACITY] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOCIAL_SECURITY_NUMBER] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAX_ID] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESCRIPTION] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CODE] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP_CODE] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHONE_NUMBER] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_RELATED_PARTY] TO [base_user]
GRANT SELECT ON  [dbo].[Import_RELATED_PARTY] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_RELATED_PARTY] TO [power_user]
GRANT SELECT ON  [dbo].[Import_RELATED_PARTY] TO [power_user]
GO
