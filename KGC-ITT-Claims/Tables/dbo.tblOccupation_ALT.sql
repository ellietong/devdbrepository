CREATE TABLE [dbo].[tblOccupation_ALT]
(
[OccupationID] [int] NOT NULL IDENTITY(1, 1),
[Occupation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccupation_ALT] ADD CONSTRAINT [PK_tblOccupation_ALT] PRIMARY KEY CLUSTERED  ([OccupationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccupation_ALT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccupation_ALT] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccupation_ALT] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccupation_ALT] TO [power_user]
GO
