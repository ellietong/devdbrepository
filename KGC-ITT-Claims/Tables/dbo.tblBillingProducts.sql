CREATE TABLE [dbo].[tblBillingProducts]
(
[ProductID] [int] NULL,
[Product] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBillingProducts] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillingProducts] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBillingProducts] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblBillingProducts] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblBillingProducts] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblBillingProducts] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBillingProducts] TO [power_user]
GRANT SELECT ON  [dbo].[tblBillingProducts] TO [power_user]
GRANT INSERT ON  [dbo].[tblBillingProducts] TO [power_user]
GRANT DELETE ON  [dbo].[tblBillingProducts] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBillingProducts] TO [power_user]
GO
