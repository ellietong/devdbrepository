CREATE TABLE [dbo].[Import_tblPolicyBills_Defense]
(
[ScenarioID] [int] NULL,
[AllocationID] [int] NULL,
[PolicyID] [int] NULL,
[ConsumptionType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppliedIndemnity] [money] NULL,
[AppliedExpense] [money] NULL,
[NonAppliedExpense] [money] NULL,
[Comment] [nvarchar] (800) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_tblPolicyBills_Defense] TO [base_user]
GRANT SELECT ON  [dbo].[Import_tblPolicyBills_Defense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_tblPolicyBills_Defense] TO [power_user]
GRANT SELECT ON  [dbo].[Import_tblPolicyBills_Defense] TO [power_user]
GO
