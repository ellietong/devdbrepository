CREATE TABLE [dbo].[WausauExhaustion_304]
(
[ClaimID] [float] NULL,
[SumOfTotalIndemnity] [money] NULL,
[SumOfTotalExpense] [money] NULL,
[AllocationID] [float] NULL,
[Month Year] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonthlyBillID] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[WausauExhaustion_304] TO [base_user]
GRANT SELECT ON  [dbo].[WausauExhaustion_304] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WausauExhaustion_304] TO [power_user]
GRANT SELECT ON  [dbo].[WausauExhaustion_304] TO [power_user]
GO
