CREATE TABLE [dbo].[XReferenceReport$]
(
[REFERENCE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_KEY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCUMENT_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILENAME_IN_DATABASE] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCUMENT_TYPE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FOLDER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILENAME_IN_FOLDER] [float] NULL,
[Filenameinfolder1] [decimal] (18, 0) NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[XReferenceReport$] TO [base_user]
GRANT SELECT ON  [dbo].[XReferenceReport$] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[XReferenceReport$] TO [power_user]
GRANT SELECT ON  [dbo].[XReferenceReport$] TO [power_user]
GO
