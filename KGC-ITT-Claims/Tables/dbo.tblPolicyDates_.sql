CREATE TABLE [dbo].[tblPolicyDates_]
(
[PolicyDateID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NOT NULL,
[PStartDate] [datetime] NOT NULL,
[PEndDate] [datetime] NOT NULL,
[ActualPStartDate] [datetime] NOT NULL,
[ActualPEndDate] [datetime] NOT NULL,
[AnnualPeriod] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyDates_] ADD CONSTRAINT [PK_tblPolicyDates] PRIMARY KEY CLUSTERED  ([PolicyDateID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDates_] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyDates_] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyDates_] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblPolicyDates_] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblPolicyDates_] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblPolicyDates_] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDates_] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicyDates_] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicyDates_] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicyDates_] TO [power_user]
GO
