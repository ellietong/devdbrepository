CREATE TABLE [dbo].[Import_tblCumulativeClaims_Defense]
(
[INJ_PRTY_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOCIAL_SECURITY_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_EXPOSURE_DATE] [datetime] NULL,
[BILL_DIAGNOSIS_DATE] [datetime] NULL,
[SETTLE_AMT] [money] NULL,
[DISEASE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADJUSTMENT_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILE_DATE] [datetime] NULL,
[AMOUNT_TYPE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_PERIOD_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VENDOR_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESCRIPTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOCKET_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COVERAGE_LINE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUBMITTAL_DATE] [datetime] NULL,
[APPROVAL_DATE] [datetime] NULL,
[REFERENCE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PACE_REF_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [int] NULL,
[ClaimAmount] [money] NULL,
[AcePolicy1] [money] NULL,
[ITT] [money] NULL,
[WausauPolicy1] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_tblCumulativeClaims_Defense] TO [base_user]
GRANT SELECT ON  [dbo].[Import_tblCumulativeClaims_Defense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_tblCumulativeClaims_Defense] TO [chaudoinr]
GRANT SELECT ON  [dbo].[Import_tblCumulativeClaims_Defense] TO [chaudoinr]
GRANT INSERT ON  [dbo].[Import_tblCumulativeClaims_Defense] TO [chaudoinr]
GRANT DELETE ON  [dbo].[Import_tblCumulativeClaims_Defense] TO [chaudoinr]
GRANT UPDATE ON  [dbo].[Import_tblCumulativeClaims_Defense] TO [chaudoinr]
GRANT VIEW DEFINITION ON  [dbo].[Import_tblCumulativeClaims_Defense] TO [power_user]
GRANT SELECT ON  [dbo].[Import_tblCumulativeClaims_Defense] TO [power_user]
GO
