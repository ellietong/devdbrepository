CREATE TABLE [dbo].[tblMaritalStatus]
(
[MaritalStatusID] [int] NOT NULL,
[MaritalStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMaritalStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblMaritalStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMaritalStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblMaritalStatus] TO [power_user]
GO
