CREATE TABLE [dbo].[tblMaterial]
(
[MaterialID] [int] NOT NULL IDENTITY(1, 1),
[Material] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMaterial] ADD CONSTRAINT [PK_mBtblMaterial] PRIMARY KEY CLUSTERED  ([MaterialID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMaterial] TO [base_user]
GRANT SELECT ON  [dbo].[tblMaterial] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMaterial] TO [power_user]
GRANT SELECT ON  [dbo].[tblMaterial] TO [power_user]
GRANT INSERT ON  [dbo].[tblMaterial] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMaterial] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblMaterial', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblMaterial', 'COLUMN', N'Material'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblMaterial', 'COLUMN', N'Material'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblMaterial', 'COLUMN', N'Material'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblMaterial', 'COLUMN', N'MaterialID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblMaterial', 'COLUMN', N'MaterialID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblMaterial', 'COLUMN', N'MaterialID'
GO
