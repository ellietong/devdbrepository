CREATE TABLE [dbo].[tblAuditClaimants]
(
[AuditClaimantsID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAuditClaimants] ADD CONSTRAINT [PK_tblAuditClaimants] PRIMARY KEY CLUSTERED  ([AuditClaimantsID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAuditClaimants] TO [base_user]
GRANT SELECT ON  [dbo].[tblAuditClaimants] TO [base_user]
GO
