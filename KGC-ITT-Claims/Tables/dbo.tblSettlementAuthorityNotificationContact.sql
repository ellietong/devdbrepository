CREATE TABLE [dbo].[tblSettlementAuthorityNotificationContact]
(
[SettlementAuthorityNotificationContactID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementAuthorityID] [bigint] NOT NULL,
[SettlementNotificationContactID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementAuthorityNotificationContact] ADD CONSTRAINT [PK_tblSettlementAuthorityNotificationContact] PRIMARY KEY CLUSTERED  ([SettlementAuthorityNotificationContactID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityNotificationContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityNotificationContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAuthorityNotificationContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementAuthorityNotificationContact] TO [power_user]
GO
