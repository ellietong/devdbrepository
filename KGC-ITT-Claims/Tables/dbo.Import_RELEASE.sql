CREATE TABLE [dbo].[Import_RELEASE]
(
[CLM_RSLTN_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELEASE_SIGNED_DATE] [datetime] NULL,
[RELEASE_RCVD_DATE] [datetime] NULL,
[RELEASE_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELEASE_SENT_DATE] [datetime] NULL,
[RELEASE_REQUEST_RECEIVED] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_RELEASE] TO [base_user]
GRANT SELECT ON  [dbo].[Import_RELEASE] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_RELEASE] TO [power_user]
GRANT SELECT ON  [dbo].[Import_RELEASE] TO [power_user]
GO
