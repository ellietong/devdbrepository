CREATE TABLE [dbo].[tblSettlementShare]
(
[SettlementShareID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [int] NULL,
[InsuranceProgramID] [int] NULL,
[SettlementShare] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementShare] ADD CONSTRAINT [PK_tblSettlementShare] PRIMARY KEY CLUSTERED  ([SettlementShareID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementShare] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementShare] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementShare] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblSettlementShare] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblSettlementShare] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblSettlementShare] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblSettlementShare] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementShare] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblSettlementShare] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblSettlementShare] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblSettlementShare] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementShare] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementShare] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementShare] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementShare] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementShare] TO [power_user]
GO
