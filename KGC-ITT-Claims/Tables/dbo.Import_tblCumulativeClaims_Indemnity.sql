CREATE TABLE [dbo].[Import_tblCumulativeClaims_Indemnity]
(
[CountOfREFID] [int] NULL,
[MonthlyBillId] [int] NULL,
[REFID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [float] NULL,
[DISEASE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RESOLUTION_DATE] [datetime] NULL,
[RESOLUTION_AMT] [money] NULL,
[PRODUCTS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RESOLUTION_AMT_PER_PRODUCT_LINE] [money] NULL,
[First_Exposure_Discovery] [int] NULL,
[First_Exposure_Complaint] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACE] [money] NULL,
[Wausau] [money] NULL,
[ITT] [money] NULL,
[Bill_Period] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DONOTINCLUDE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claim_Sequence] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Release_Received_Date] [datetime] NULL,
[UniqueID] [int] NOT NULL,
[ClaimantPersonalInfoID] [bigint] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_tblCumulativeClaims_Indemnity] TO [base_user]
GRANT SELECT ON  [dbo].[Import_tblCumulativeClaims_Indemnity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_tblCumulativeClaims_Indemnity] TO [chaudoinr]
GRANT SELECT ON  [dbo].[Import_tblCumulativeClaims_Indemnity] TO [chaudoinr]
GRANT INSERT ON  [dbo].[Import_tblCumulativeClaims_Indemnity] TO [chaudoinr]
GRANT DELETE ON  [dbo].[Import_tblCumulativeClaims_Indemnity] TO [chaudoinr]
GRANT UPDATE ON  [dbo].[Import_tblCumulativeClaims_Indemnity] TO [chaudoinr]
GRANT VIEW DEFINITION ON  [dbo].[Import_tblCumulativeClaims_Indemnity] TO [power_user]
GRANT SELECT ON  [dbo].[Import_tblCumulativeClaims_Indemnity] TO [power_user]
GO
