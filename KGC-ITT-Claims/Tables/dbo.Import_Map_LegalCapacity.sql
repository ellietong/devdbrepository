CREATE TABLE [dbo].[Import_Map_LegalCapacity]
(
[PACESource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KCIC LegalCapacityID (Mapped)] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Import_Map_LegalCapacity] TO [base_user]
GRANT SELECT ON  [dbo].[Import_Map_LegalCapacity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Import_Map_LegalCapacity] TO [power_user]
GRANT SELECT ON  [dbo].[Import_Map_LegalCapacity] TO [power_user]
GO
