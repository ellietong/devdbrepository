SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE FUNCTION [dbo].[fnDeadlineDate]
(@InsurerNotificationDate datetime, @AuthorityApproverFirmID int,  @AuthorityAmount money)

RETURNS DATE
AS
BEGIN

--DECLARE @InsurerNotificationDate DATETIME = '2014-04-16 09:00:13.047'
--DECLARE @AuthorityApproverFirmID INT = 155
--DECLARE @AuthorityAmount MONEY = 2500000--120000000

DECLARE @DeadlineDate DATETIME

IF (@AuthorityApproverFirmID = 63)
	BEGIN
		IF DATENAME(DW , @InsurerNotificationDate) = 'Thursday' 
			SET @DeadlineDate = (@InsurerNotificationDate+4)
		ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Friday' 
			SET @DeadlineDate = (@InsurerNotificationDate+4)
		ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Saturday' 
			SET @DeadlineDate = (@InsurerNotificationDate+4)
		ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Sunday' 
			SET @DeadlineDate = (@InsurerNotificationDate+3)
		ELSE
			SET @DeadlineDate = (@InsurerNotificationDate+2)
	END
ELSE IF (@AuthorityApproverFirmID = 155)
	BEGIN
		IF (@AuthorityAmount <= 500000)
			BEGIN
				IF DATENAME(DW , @InsurerNotificationDate) = 'Wednesday' 
					SET @DeadlineDate = (@InsurerNotificationDate+5)
				ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Thursday' 
					SET @DeadlineDate = (@InsurerNotificationDate+5)
				ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Friday' 
					SET @DeadlineDate = (@InsurerNotificationDate+5)
				ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Saturday' 
					SET @DeadlineDate = (@InsurerNotificationDate+5)
				ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Sunday' 
					SET @DeadlineDate = (@InsurerNotificationDate+4)
				ELSE
					SET @DeadlineDate = (@InsurerNotificationDate+3)
			END
		ELSE
			BEGIN
				IF DATENAME(DW , @InsurerNotificationDate) = 'Monday' 
					SET @DeadlineDate = (@InsurerNotificationDate+9)
				ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Tuesday' 
					SET @DeadlineDate = (@InsurerNotificationDate+9)
				ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Wednesday' 
					SET @DeadlineDate = (@InsurerNotificationDate+12)
				ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Thursday' 
					SET @DeadlineDate = (@InsurerNotificationDate+11)
				ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Friday' 
					SET @DeadlineDate = (@InsurerNotificationDate+11)
				ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Saturday' 
					SET @DeadlineDate = (@InsurerNotificationDate+11)
				ELSE IF DATENAME(DW , @InsurerNotificationDate) = 'Sunday' 
					SET @DeadlineDate = (@InsurerNotificationDate+10)
			END

END

RETURN @DeadlineDate

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fnDeadlineDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnDeadlineDate] TO [base_user]
GO
