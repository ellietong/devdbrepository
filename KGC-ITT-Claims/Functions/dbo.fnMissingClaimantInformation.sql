SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE FUNCTION [dbo].[fnMissingClaimantInformation]
(@ClaimLawsuitID bigint)

RETURNS NVARCHAR(MAX)
AS
BEGIN

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 25776

DECLARE @Product table (claimantpersonalinfoid int, productentered int)
INSERT INTO @Product 
SELECT ce.ClaimantPersonalInfoID, CASE WHEN cp.ProductID is NULL or cp.StartDate is NULL or cp.EndDate is NULL  THEN 0  ELSE 1  END AS productentered
FROM tblClaimantEmployment ce 
LEFT JOIN tblClaimantProduct cp on cp.ClaimantEmploymentID =ce.ClaimantEmploymentID 
WHERE ce.IsPrimary = 1 AND cp.PrimaryProduct = 1
GROUP BY ce.ClaimantPersonalInfoID , CASE WHEN cp.ProductID is NULL or cp.StartDate is NULL or cp.EndDate is NULL  THEN 0  ELSE 1  end 
 

--SELECT * FROM @employment

--IF object_id('tempdb..@CurrentClaimantInfo') IS NOT NULL
--BEGIN
--   DROP TABLE @CurrentClaimantInfo
--END
DECLARE @CurrentClaimantInfo TABLE  (SSN bit, BirthDate bit, Disease bit, DiagnosisDate bit,  ProductInformation bit, LegalCapacity bit)--, SettlementDate bit, SettlementAmount bit)
INSERT INTO @CurrentClaimantInfo
SELECT distinct CASE WHEN socialsecuritynumber is NULL THEN 0 ELSE 1 END AS HasSSN, 
				CASE WHEN birthdate is NULL THEN 0 ELSE 1 END AS HasBirthDate,
				CASE WHEN DiseaseID IS NULL THEN 0 ELSE 1 END AS HasDiseaseID,
				CASE WHEN CONVERT(date, DiseaseDiagnosisDate) IS NULL THEN 0 ELSE 1 END AS HasDxDate, 
				COALESCE(p.productentered, 0) AS HasProductEntered,  
				CASE WHEN DeceasedDate is NOT NULL AND LegalCapacityid is NULL THEN 0  ELSE 1 END AS HasLegalCapacity
				--CASE WHEN SettlementDate IS NULL THEN 0 ELSE 1 END AS HasSettlementDate,
				--CASE WHEN SettlementAmount IS NULL THEN 0 ELSE 1 END AS HasSettlementAmount
				--CASE WHEN DeceasedDate is not NULL and LegalCapacityid is NULL THEN 0  ELSE 1 END AS LegalCapacity

FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi	 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl	 ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
	LEFT JOIN @Product p ON p.claimantpersonalinfoid =cpi.ClaimantPersonalInfoID 
	LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
	LEFT JOIN (SELECT cd1.* 
			   FROM tblClaimDisease cd1 
					INNER JOIN tbldisease d1 on d1.diseaseid = cd1.diseaseid 
			   WHERE PrimaryDisease = 1 
					AND highleveldiseaseid <> 5) cdi	 ON cdi.ClaimID = c.ClaimID
	INNER JOIN tblClaimLawsuitStatusTracking cls on cls.ClaimLawsuitID =cl.ClaimLawsuitID 
	LEFT JOIN (SELECT * 
				FROM tblClaimantRelatedParty 
				WHERE LegalCapacityID IN (1,2,3,4,5,6,7,8,15,17,30,31))crp	 ON crp.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID


--SELECT * FROM @CurrentClaimantInfo

--IF object_id('tempdb..#MissingClaimantInfo') IS NOT NULL
--BEGIN
--   DROP TABLE #MissingClaimantInfo
--END
DECLARE @MissingClaimantInfo  TABLE  (ID int NOT NULL IDENTITY(1,1), MissingClaimantInformation nvarchar(500), Remainder int)
INSERT INTO @MissingClaimantInfo
SELECT 'Social Security Number', NULL
FROM @CurrentClaimantInfo
WHERE SSN = 0
UNION
SELECT 'Birth Date', NULL
FROM @CurrentClaimantInfo
WHERE BirthDate = 0
UNION
SELECT 'Disease Diagnosis', NULL
FROM @CurrentClaimantInfo
WHERE Disease = 0
UNION
SELECT 'Diagnosis Date', NULL
FROM @CurrentClaimantInfo
WHERE DiagnosisDate = 0
UNION
SELECT 'Product Information', NULL
FROM @CurrentClaimantInfo
WHERE ProductInformation = 0
UNION
SELECT 'Legal Capacity', NULL
FROM @CurrentClaimantInfo
WHERE LegalCapacity = 0


	--Calculate the remainder for every-other row highlighting
	UPDATE @MissingClaimantInfo
	SET Remainder = fdp.Remainder 
	FROM (SELECT ID, (ID % 2) AS Remainder FROM @MissingClaimantInfo) fdp
	INNER JOIN @MissingClaimantInfo ofdp ON fdp.ID = ofdp.ID
	WHERE ofdp.ID = fdp.ID

--SELECT *
--FROM @MissingClaimantInfo

	DECLARE @Remainder TABLE (arID int, RowCounter int, Remainder int)
	INSERT INTO @Remainder
	SELECT ID,
		ROW_NUMBER() OVER (ORDER BY ID) AS Row_Counter,  ROW_NUMBER() OVER (ORDER BY ID)% 2 AS Remainder    
	FROM @MissingClaimantInfo

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'


DECLARE @MissingClaimantInfoTable nvarchar(max) = ''

SET @MissingClaimantInfoTable = @MissingClaimantInfoTable + N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0; width:200px;">' + 
	

N'<tr>
' +
	
	
CAST ( (
SELECT CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleB END AS 'td/@style', (SELECT COALESCE(MissingClaimantInformation, '')) as td, ''
	FROM @MissingClaimantInfo
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	
	--SELECT @MissingClaimantInfoTable
	RETURN @MissingClaimantInfoTable --AS MissingClaimantInfo



END



GO
GRANT VIEW DEFINITION ON  [dbo].[fnMissingClaimantInformation] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnMissingClaimantInformation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnMissingClaimantInformation] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnMissingClaimantInformation] TO [power_user]
GO
