SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fnPerson] 
(	

)
RETURNS TABLE 
AS
RETURN 
(
SELECT DISTINCT 
                    p.PersonID, p.FirmID, p.FirmAddressID, p.LName, p.FName, p.PositionID, p.Email, p.Status, p.Password, pe.InvoiceContact, pe.ComplaintContact, 
                    p.LastPasswordChangedDate, p.LastLoginDate, p.IsLockedOut, p.FailedPasswordAttemptCount, COALESCE (pe.NoFilters, 0) | COALESCE (fac.NoFilters, 0) 
                    AS NoFilters, p.PlaintiffCounselAddressID--, pe.CaseID
FROM            SHARESQL.fbsecurity.dbo.tblperson AS p INNER JOIN
                         SHARESQL.fbsecurity.dbo.tblpermissions AS pe ON p.PersonID = pe.PersonID INNER JOIN
                         SHARESQL.fbsecurity.dbo.tblFirmCase AS fc ON fc.FirmID = p.FirmID AND pe.CaseID = fc.CaseID LEFT OUTER JOIN
                         SHARESQL.fbsecurity.dbo.tblfirmaddresscase AS fac ON p.FirmAddressID = fac.FirmAddressID AND pe.CaseID = fac.CaseID
WHERE        (pe.CaseID = 35)



)
GO
