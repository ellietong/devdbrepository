SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE FUNCTION [dbo].[fnEarliestExposure]
(@ClaimantPersonalInfoID int)

RETURNS date
AS
BEGIN

--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 37174

DECLARE @EarliestExposure Date

--declare @MarleyProducts table (productID int)
--Insert into @MarleyProducts
--select p.ProductID
--from tblProduct p
--left join tblProductBrand B on b.ProductBrandID = p.ProductBrandID
--left join tblProductManufacturer M on M.ProductManufacturerID = p.ProductManufacturerID
--left join tblProductName N on N.ProductNameID = p.ProductNameID
--left join tblProductType T on t.ProductTypeID = p.ProductTypeID
--where ProductManufacturer like '%weil%' or ProductManufacturer like '%Marley%' or ProductManufacturer like '%Red Jacket%' or ProductManufacturer like '%hydromatic%' 
--or ProductBrand like '%weil%' or ProductBrand like '%Marley%' or ProductBrand like '%Red Jacket%' or ProductBrand like '%hydromatic%' 
--or ProductType like '%weil%' or ProductType like '%Marley%' or ProductType like '%Red Jacket%' or ProductType like '%hydromatic%' 
--119



/**SECTION OF CODE TO USE IF YOU DO WANT TO CONSIDER PRODUCTS IN THE EXPOSURE CALCULATION. IUNA AND NSI FOR EXAMPLE**/
--Updated for Marley to specifically look for Marley products since data not consolidated proir to importing. - CAS 10/5/11

IF (SELECT COUNT(ClaimantProductID)	From tblclaimantemployment ce 
inner join tblclaimantproduct cp on cp.claimantemploymentid = ce.claimantemploymentID 
--join @MarleyProducts MP on MP.productID = cp.ProductID
where ClaimantPersonalInfoID = @ClaimantPersonalInfoID ) = 0
BEGIN 
--print 'no marley product'
SET @EarliestExposure = (select Min(EmpStartDate) as EarliestExposureDate
from tblClaimantEmployment
where ClaimantPersonalInfoID=@ClaimantPersonalInfoID)

END
ELSE 
BEGIN
--print 'Marley product'
SET @EarliestExposure = (select MIN(EarliestExposureDate) as EarliestExposureDate
From
--3/9/12 EMS updated - products not always on primary jobsite, so take product date even if product start < emp start
--(SELECT case when coalesce(min(cp.StartDate), '1/1/1900') < ce.EmpStartDate then ce.EmpStartDate else MIN(cp.startdate) end as EarliestExposureDate
(SELECT coalesce(min(cp.StartDate), Min(EmpStartDate)) as EarliestExposureDate
From tblclaimantemployment ce 
inner join tblclaimantproduct cp on cp.claimantemploymentid = ce.claimantemploymentID 

where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
--group by ce.EmpStartDate) tblEmploymentExposure)
) tblEmploymentExposure)

end


/**SECTION OF CODE TO USE IF YOU DO NOT WANT TO CONSIDER PRODUCTS IN THE EXPOSURE CALCULATION. DANA FOR EXAMPLE**/

--SET @EarliestExposure = (select Min(EmpStartDate) as EarliestExposureDate
--from tblClaimantEmployment
--where ClaimantPersonalInfoID=@ClaimantPersonalInfoID)

Return @EarliestExposure
--print @EarliestExposure

END


----QC with Wahlgren claimant: primary jobsite starts 1/1/52, Marley product exposure starts 1/1/47 (should return 1/1/47)
--select dbo.fnEarliestExposure(42578)
GO
GRANT VIEW DEFINITION ON  [dbo].[fnEarliestExposure] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnEarliestExposure] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnEarliestExposure] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnEarliestExposure] TO [power_user]
GO
