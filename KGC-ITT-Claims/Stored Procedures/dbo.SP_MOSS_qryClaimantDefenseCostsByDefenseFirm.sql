SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MOSS_qryClaimantDefenseCostsByDefenseFirm] 
	-- Add the parameters for the stored procedure here
	(@ClaimantPersonalInfoID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 41635
Declare @InvoiceTotal table (ClaimantPersonalInfoID int, VendorFirmID int, DefenseCounselAddressID int, DefenseTotal money)
Insert into @InvoiceTotal

----Approved Expert and General Invoices with a ClaimantID link
Select CPI.ClaimantPersonalInfoID, di.VendorFirmID, Di.DefenseCounselAddressID, SUM(COALESCE(Costs, 0) + COALESCE(Fees, 0) + COALESCE(Adjustments, 0)) as DefenseTotal
From tblDefenseInvoice DI
LEFT JOIN tblDefInvClaimantDetail dicd ON di.InvoiceNumber = dicd.InvoiceNumber
LEFT Join tblClaimantPersonalInfo CPI on CPI.ClaimantPersonalInfoID=DIcd.ClaimantPersonalInfoID
Where DICD.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and DI.StatusID in (1,2)
Group by CPI.ClaimantPersonalInfoID, di.VendorFirmID, Di.DefenseCounselAddressID

Select	ClaimantPersonalInfoID, Coalesce(DefenseCounsel,VendorFirm) as DefenseCounsel, Coalesce(i.DefenseCounselAddressID, 0) as DefenseCounselID, i.VendorFirmID, Sum(DefenseTotal) as DefenseTotal 
From @InvoiceTotal i 
Left Join tblDefenseCounselAddress d on i.DefenseCounselAddressID = d.DefenseCounselAddressID
Left Join m_tblVendorFirm e on i.VendorFirmID = e.VendorFirmID
Group By ClaimantPersonalInfoID, DefenseCounsel, i.DefenseCounselAddressID, VendorFirm, i.VendorFirmID
HAVING Sum(DefenseTotal) <> 0

END

GO
