SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthority]
(
	@ClaimantName nvarchar(250) = NULL,
	@SettlementGroup nvarchar(250) = NULL,
	@StateID int = NULL,
	@PlaintiffCounselAddressID int = NULL,
	@RequestStartDate date = NULL,
	@RequestEndDate date = NULL,
	@Emailaddress nvarchar(250) = NULL, 
	@OrderByString nvarchar(MAX)=NULL,
	@AlreadyGrantedFirmID int = NULL,
	@AwaitingApprovalFirmID int = NULL
)

	

AS
BEGIN
	SET NOCOUNT ON;

	----test Vars
	--DECLARE @ClaimantName nvarchar(250) = NULL
	--DECLARE @SettlementGroup nvarchar(250) = NULL--'GOLD LAW FIRM (SAN FRANCISCO) /  THOMPSON'--'WEITZ & LUXENBERG PC (NEW YORK) BROWN, GARBUTT, MCCLOSKEY, TERRY, BODENSTEIN, EBANKS, FABIILLI, GOGOLSKI, HOLMES, JOZWIAK, KASCHERES, MAHONEY, MARANO, MCGUIRE, MCKAY, PETERSON, KOCIENCKI, ZIMMERMAN'
	--DECLARE @StateID int = NULL
	--DECLARE @PlaintiffCounselAddressID int = NULL
	--DECLARE @RequestStartDate date = NULL
	--DECLARE @RequestEndDate date = NULL
	--DECLARE @Emailaddress nvarchar(250) = 'owenss@kcic.com'--'srobles@prindlelaw.com'--='TestUser@Insurer.com'--'loree.shelko@itt.com'--
	--DECLARE @OrderByString nvarchar(MAX)=NULL
	--DECLARE @AlreadyGrantedFirmID int = NULL--264 -- 264 ITT
	--DECLARE @AwaitingApprovalFirmID int = NULL

	
	
	--Get FirmAddressID to see if user is insurer/NCC
	DECLARE @FirmAddressID bigint = (SELECT FirmAddressID FROM  vSETTLEMENT_AUTHORITY_Users WHERE Email = @Emailaddress)
	DECLARE @PersonID bigint = (SELECT TOP 1 PersonID FROM  vSETTLEMENT_AUTHORITY_Users WHERE Email = @Emailaddress)
	DECLARE @UserFirmID bigint = (SELECT FirmID FROM  vSETTLEMENT_AUTHORITY_Users WHERE Email = @Emailaddress)
	DECLARE @WhereString nvarchar(MAX)
	DECLARE @JoinString nvarchar(MAX) = ' LEFT JOIN  vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Unfiltered fra ON sa.SettlementAuthorityID = fra.SettlementAuthorityID '
	DECLARE @Field nvarchar(MAX)
	DECLARE @CanRespondString nvarchar(MAX)
	DECLARE @ClientFirmAddressID bigint = 264 --ITT
	DECLARE @NCCFirmAddressID bigint = 330 --McGuire Woods
	IF object_id('tempdb..#InsurerFirmAddresses') IS NOT NULL
	BEGIN
	   DROP TABLE #InsurerFirmAddresses
	END
	CREATE TABLE #InsurerFirmAddresses (InsurerFirmAddressID int)
	INSERT INTO #InsurerFirmAddresses
	SELECT 427 -- ACE
	UNION 
	SELECT 247 --Nationwide

	IF object_id('tempdb..#InsurerFirms') IS NOT NULL
	BEGIN
	   DROP TABLE #InsurerFirms
	END
	CREATE TABLE #InsurerFirms (InsurerFirmAddressID int)
	INSERT INTO #InsurerFirms
	SELECT 63 -- ACE
	UNION 
	SELECT 155 --Nationwide

--SELECT @PersonID
IF ( SELECT COUNT(PersonID)
		FROM    vSETTLEMENT_AUTHORITY_Users
		WHERE  Email = @emailaddress
			AND PersonID IN (3099, 5782, 5783)
	) > 0 
BEGIN
	SET @FirmAddressID = 427
	SET @UserFirmID = 63
END
	
DECLARE @NewGuid uniqueidentifier = NEWID();
--SELECT @NewGuid
DECLARE @AuditStoredProcID BIGINT
DECLARE @CodeBlockName NVARCHAR(250)

--SectionA - Initial setup of Order By String
	--Either accept the ORDER BY string parameter, or coalesce to most recent request date @ top
SET @CodeBlockName = 'SectionA'
INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime, EmailAddress)
SELECT @NewGuid, @CodeBlockName, GETDATE(), @Emailaddress
SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

	SET @OrderByString = COALESCE(@OrderByString, 'ORDER BY AuthorityRequestedDate DESC, COALESCE(ParentSettlementAuthorityID, sa.SettlementAuthorityID)')--COALESCE(@OrderByString, 'ORDER BY AuthorityRequestedDate DESC')

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName
	
	--******Filter appropriately for each user type ******--
--KCIC
--SectionB - Setup filters for each user type
	


	IF((@FirmAddressID IS NULL AND @Emailaddress IS NULL) OR @Emailaddress LIKE '%@kcicllc.com' OR @Emailaddress LIKE '%@kcic.com')
		BEGIN

SET @CodeBlockName = 'SectionB-KCIC'
INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
SELECT @NewGuid, @CodeBlockName, GETDATE()
SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

			IF(@ClaimantName IS NULL AND	
			   @SettlementGroup IS NULL AND 
			   @StateID IS NULL AND 
			   @PlaintiffCounselAddressID IS NULL AND 
			   @RequestStartDate IS NULL AND 
			   @RequestEndDate IS NULL AND 
			   @AlreadyGrantedFirmID IS NULL AND 
			   @AwaitingApprovalFirmID IS NULL
			  )
				BEGIN 
					SET @WhereString = 'WHERE 1=1 
											AND sa.AuthorityGranted <> 1
											AND sa.AuthorityDenied <> 1
											AND Status NOT LIKE ''Finalized'' '
				END
			ELSE
				BEGIN 
					SET @WhereString = 'WHERE 1=1 '			
			END

			SET @CanRespondString = '1'
			SET @Field = 'CASE WHEN (EXISTS (SELECT SettlementAuthorityResponseID 
											 FROM tblSettlementAuthorityResponse sar 
											 WHERE sar.SettlementAuthorityID = sa.SettlementAuthorityID)) 
							   THEN 1 
							   ELSE 0 END AS HasResponses, 
						  CASE WHEN Status = ''Finalized'' 
							   THEN ''<a target="_blank" href="/clients/ITT/ittcp/Pages/RequestAuthority.aspx?SettlementAuthorityID=''+ COALESCE(CONVERT(nvarchar(50), sa.SettlementAuthorityID), '''') + ''">'' + COALESCE(SettlementGroup, '''') + ''</a>'' 
							   ELSE SettlementGroup END AS SettlementGroup'
		
UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName	

		END

--NCC
	ELSE IF (@FirmAddressID = @NCCFirmAddressID)
		BEGIN

SET @CodeBlockName = 'SectionB-NCC'
INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
SELECT @NewGuid, @CodeBlockName, GETDATE()
SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

			IF(@ClaimantName IS NULL 
				AND @SettlementGroup IS NULL 
				AND @StateID IS NULL 
				AND @PlaintiffCounselAddressID IS NULL 
				AND @RequestStartDate IS NULL 
				AND @RequestEndDate IS NULL 
				AND @AlreadyGrantedFirmID IS NULL 
				AND @AwaitingApprovalFirmID IS NULL
			   )
				BEGIN 
					SET @WhereString = 'WHERE sa.AuthorityGranted <> 1 
											AND sa.AuthorityDenied <> 1
											AND Status NOT LIKE ''Finalized'' ' 
				END
			ELSE
				BEGIN
					SET @WhereString = 'WHERE 1=1 '
				END

			SET @CanRespondString = '0'
			SET @Field = 'CASE WHEN (EXISTS (SELECT SettlementAuthorityResponseID 
											 FROM tblSettlementAuthorityResponse sar 
											 WHERE sar.SettlementAuthorityID = sa.SettlementAuthorityID)) 
							   THEN 1 
							   ELSE 0 END AS HasResponses, 
						  CASE WHEN Status = ''Finalized'' 
							   THEN ''<a target="_blank" href="/clients/ITT/ittcp/Pages/RequestAuthority.aspx?SettlementAuthorityID=''+ COALESCE(CONVERT(nvarchar(50), sa.SettlementAuthorityID), '''') + ''">'' + COALESCE(SettlementGroup, '''') + ''</a>'' 
							   ELSE SettlementGroup END AS SettlementGroup'

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

		END

--Client
    ELSE IF (@FirmAddressID = @ClientFirmAddressID)
		BEGIN

SET @CodeBlockName = 'SectionB-Client'
INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
SELECT @NewGuid, @CodeBlockName, GETDATE()
SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

			IF(@ClaimantName IS NULL 
				AND @SettlementGroup IS NULL 
				AND @StateID IS NULL 
				AND @PlaintiffCounselAddressID IS NULL 
				AND @RequestStartDate IS NULL 
				AND @RequestEndDate IS NULL 
				AND @AlreadyGrantedFirmID IS NULL 
				AND @AwaitingApprovalFirmID IS NULL
			  )
				BEGIN 
					SET @WhereString = 'WHERE EXISTS ( SELECT saaf.SettlementApproverFirmID
													FROM tblSettlementAuthorityApproverFirm saaf
													INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
													INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON saf.FirmID = sau.FirmID
													WHERE saaf.SettlementAuthorityID = sa.SettlementAuthorityID
																	) 
												AND sa.AuthorityGranted <> 1
												AND sa.AuthorityDenied <> 1
												AND Status NOT LIKE ''Finalized'' '
				END
			ELSE
				BEGIN
					SET @WhereString = 'WHERE 1=1 '
				END

			SET @CanRespondString = '1'
			SET @Field = 'CASE WHEN (EXISTS (SELECT SettlementAuthorityResponseID 
											 FROM tblSettlementAuthorityResponse sar 
											 WHERE sar.SettlementAuthorityID = sa.SettlementAuthorityID)) THEN 1 
						 ELSE 0 END AS HasResponses, 
						 SettlementGroup'

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

		END

--Insurers
    ELSE IF (SELECT COUNT(InsurerFirmAddressID) FROM #InsurerFirmAddresses WHERE InsurerFirmAddressID = @FirmAddressID) > 0
		BEGIN

SET @CodeBlockName = 'SectionB-Insurers'
INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
SELECT @NewGuid, @CodeBlockName, GETDATE()
SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

			IF(@ClaimantName IS NULL 
				AND @SettlementGroup IS NULL 
				AND @StateID IS NULL 
				AND @PlaintiffCounselAddressID IS NULL 
				AND @RequestStartDate IS NULL 
				AND @RequestEndDate IS NULL 
				AND @AlreadyGrantedFirmID IS NULL 
				AND @AwaitingApprovalFirmID IS NULL
			  )
				  BEGIN
						SET @WhereString = 'WHERE EXISTS ( SELECT saaf.SettlementApproverFirmID
															FROM tblSettlementAuthorityApproverFirm saaf
															INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
															INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON saf.FirmID = sau.FirmID
															WHERE saaf.SettlementAuthorityID = sa.SettlementAuthorityID
															AND sau.FirmAddressID = ' + CONVERT(nvarchar, @FirmAddressID) + '
														 ) 
												AND sa.AuthorityGranted <> 1 
												AND sa.AuthorityDenied <> 1
												AND COALESCE(isRequestApproved, 0) = 1	
												AND RequestApprovedBy IS NOT NULL 
												AND RequestApprovedDate IS NOT NULL
												AND Status NOT LIKE ''Finalized''
											'
				 END
			 ELSE
				 BEGIN
						SET @WhereString = 'WHERE EXISTS ( SELECT saaf.SettlementApproverFirmID
																FROM tblSettlementAuthorityApproverFirm saaf
																INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
																INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON saf.FirmID = sau.FirmID
																WHERE saaf.SettlementAuthorityID = sa.SettlementAuthorityID
																AND sau.FirmAddressID = ' + CONVERT(nvarchar, @FirmAddressID) + '
															 ) 
												AND sa.AuthorityDenied <> 1
												AND COALESCE(isRequestApproved, 0) = 1		
											'
				 END 

			SET @CanRespondString = ' CASE WHEN Status = ''Authority Granted'' THEN 0 
										   WHEN EXISTS (SELECT SettlementAuthorityResponseID 
													   FROM tblSettlementAuthorityResponse sr 
													   WHERE sr.SettlementAuthorityID = sa.SettlementAuthorityID
															AND ParentSettlementAuthorityResponseID IS NULL
															AND CreatedBy = ''' + @Emailaddress + ''') OR 
										   NOT	EXISTS (SELECT SettlementAuthorityApproverFirmID
														FROM tblSettlementAuthorityApproverFirm saaf
															INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
															INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON saf.FirmID = sau.FirmID
														WHERE SettlementAuthorityID = sa.SettlementAuthorityID
															AND sau.Email	= ''' + @emailaddress + ''')
										   THEN 0 
										   ELSE 1 END'

			SET @Field = ' CASE WHEN (EXISTS (SELECT *
											  FROM tblSettlementAuthorityResponse sar
												LEFT JOIN  vSETTLEMENT_AUTHORITY_Users sau on sar.CreatedBy = sau.email
											  WHERE SettlementAuthorityID = sa.SettlementAuthorityID
												   AND ParentSettlementAuthorityResponseID IS NULL 
												   AND (sau.FirmID IS NULL OR sau.firmID NOT IN (SELECT FirmID
																							 FROM #InsurerFirmAddresses ifa
																								 INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON ifa.InsurerFirmAddressID = sau.FirmAddressID
																							 WHERE FirmAddressID <> ' + CONVERT(nvarchar, @FirmAddressID) + ')))) 
						 THEN 1 
						 ELSE 0 END AS HasResponses,
						 SettlementGroup
						 '
SET @JoinString = ' LEFT JOIN  vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Filtered fra ON sa.SettlementAuthorityID = fra.SettlementAuthorityID AND fra.FirmID = ' + CONVERT(nvarchar, @UserFirmID) 
UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName


		END
--NoFilters With Restrictions
	ELSE IF ((@FirmAddressID <> @NCCFirmAddressID) AND (@FirmAddressID <> @ClientFirmAddressID) 
	AND ( ( SELECT NoFilters
			FROM   vtblPerson
			WHERE  Email = @emailaddress
		) = 1 )  
	AND ( ( SELECT COUNT(PersonID)
		FROM    vSETTLEMENT_AUTHORITY_Users
		WHERE  Email = @emailaddress
			AND PersonID IN (3099, 5782, 5783)
	) > 0 ))
		BEGIN

SET @CodeBlockName = 'SectionB-Nofilters with Restrictions'
INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
SELECT @NewGuid, @CodeBlockName, GETDATE()
SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

			IF(@ClaimantName IS NULL 
				AND @SettlementGroup IS NULL 
				AND @StateID IS NULL 
				AND @PlaintiffCounselAddressID IS NULL 
				AND @RequestStartDate IS NULL 
				AND @RequestEndDate IS NULL 
				AND @AlreadyGrantedFirmID IS NULL 
				AND @AwaitingApprovalFirmID IS NULL
			  )
				  BEGIN
						SET @WhereString = 'WHERE EXISTS ( SELECT saaf.SettlementApproverFirmID
															FROM tblSettlementAuthorityApproverFirm saaf
															INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
															INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON saf.FirmID = sau.FirmID
															WHERE saaf.SettlementAuthorityID = sa.SettlementAuthorityID
															AND sau.FirmAddressID = ' + CONVERT(nvarchar, @FirmAddressID) + '
														 ) 
												AND sa.AuthorityGranted <> 1 
												AND sa.AuthorityDenied <> 1
												AND COALESCE(isRequestApproved, 0) = 1	
												AND RequestApprovedBy IS NOT NULL 
												AND RequestApprovedDate IS NOT NULL
												AND Status NOT LIKE ''Finalized''
											'
				 END
			 ELSE
				 BEGIN
						SET @WhereString = 'WHERE EXISTS ( SELECT saaf.SettlementApproverFirmID
																FROM tblSettlementAuthorityApproverFirm saaf
																INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
																INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON saf.FirmID = sau.FirmID
																WHERE saaf.SettlementAuthorityID = sa.SettlementAuthorityID
																AND sau.FirmAddressID = ' + CONVERT(nvarchar, @FirmAddressID) + '
															 ) 
												AND sa.AuthorityDenied <> 1
												AND COALESCE(isRequestApproved, 0) = 1		
											'
				 END 

			SET @CanRespondString = '0'
			SET @Field = ' CASE WHEN (EXISTS (SELECT *
											  FROM tblSettlementAuthorityResponse sar
												LEFT JOIN  vSETTLEMENT_AUTHORITY_Users sau on sar.CreatedBy = sau.email
											  WHERE SettlementAuthorityID = sa.SettlementAuthorityID
												   AND ParentSettlementAuthorityResponseID IS NULL 
												   AND (sau.FirmID IS NULL OR sau.firmID NOT IN (SELECT FirmID
																							 FROM #InsurerFirmAddresses ifa
																								 INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON ifa.InsurerFirmAddressID = sau.FirmAddressID
																							 WHERE FirmAddressID <> ' + CONVERT(nvarchar, @FirmAddressID) + ')))) 
						 THEN 1 
						 ELSE 0 END AS HasResponses,
						 SettlementGroup
						 '

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

		END
--NoFilters Without Restrictions (Ex. SRA)	
	ELSE IF ((@FirmAddressID <> @NCCFirmAddressID) AND (@FirmAddressID <> @ClientFirmAddressID) 
	AND ( ( SELECT NoFilters
			FROM   vtblPerson
			WHERE  Email = @emailaddress
		) = 1 ))
		BEGIN
			IF(@ClaimantName IS NULL 
				AND @SettlementGroup IS NULL 
				AND @StateID IS NULL 
				AND @PlaintiffCounselAddressID IS NULL 
				AND @RequestStartDate IS NULL 
				AND @RequestEndDate IS NULL 
				AND @AlreadyGrantedFirmID IS NULL 
				AND @AwaitingApprovalFirmID IS NULL
			  )
				BEGIN 

SET @CodeBlockName = 'SectionB-Nofilters without Restrictions'
INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
SELECT @NewGuid, @CodeBlockName, GETDATE()
SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

					SET @WhereString = 'WHERE EXISTS ( SELECT saaf.SettlementApproverFirmID
													FROM tblSettlementAuthorityApproverFirm saaf
													INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
													INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON saf.FirmID = sau.FirmID
													WHERE saaf.SettlementAuthorityID = sa.SettlementAuthorityID
																	) 
												AND sa.AuthorityGranted <> 1
												AND sa.AuthorityDenied <> 1
												AND Status NOT LIKE ''Finalized'' '
				END
			ELSE
				BEGIN
					SET @WhereString = 'WHERE 1=1 '
				END

			SET @CanRespondString = '0'
			SET @Field = 'CASE WHEN (EXISTS (SELECT SettlementAuthorityResponseID 
											 FROM tblSettlementAuthorityResponse sar 
											 WHERE sar.SettlementAuthorityID = sa.SettlementAuthorityID)) THEN 1 
						 ELSE 0 END AS HasResponses, 
						 SettlementGroup'

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

		END
--LC
	ELSE
		BEGIN

SET @CodeBlockName = 'SectionB-LC'
INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
SELECT @NewGuid, @CodeBlockName, GETDATE()
SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

			IF(@ClaimantName IS NULL 
				AND @SettlementGroup IS NULL 
				AND @StateID IS NULL 
				AND @PlaintiffCounselAddressID IS NULL 
				AND @RequestStartDate IS NULL 
				AND @RequestEndDate IS NULL 
				AND @AlreadyGrantedFirmID IS NULL 
				AND @AwaitingApprovalFirmID IS NULL
			  )
				BEGIN 
					SET @WhereString = 'WHERE sa.SettlementAuthorityID IN (SELECT sa.SettlementAuthorityID
											FROM tblSettlementAuthority sa
											INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau on sa.AuthorityRequestedBy = sau.Email
											AND sau.FirmAddressID = ' + CONVERT(nvarchar, @FirmAddressID) + '
										)
										AND RequestApprovedBy IS NOT NULL AND RequestApprovedDate IS NOT NULL
										AND Status NOT LIKE ''Finalized'' '
				END
			ELSE
				BEGIN
		SET @WhereString = 'WHERE sa.SettlementAuthorityID IN (SELECT sa.SettlementAuthorityID
											FROM tblSettlementAuthority sa
											INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau on sa.AuthorityRequestedBy = sau.Email
											AND sau.FirmAddressID = ' + CONVERT(nvarchar, @FirmAddressID) + '
							)
							AND RequestApprovedBy IS NOT NULL AND RequestApprovedDate IS NOT NULL
					'
				END

			SET @CanRespondString = '0'				

			SET @Field = 'CASE WHEN (EXISTS (SELECT SettlementAuthorityResponseID FROM tblSettlementAuthorityResponse sar WHERE sar.SettlementAuthorityID = sa.SettlementAuthorityID)) THEN 1 ELSE 0 END AS HasResponses, SettlementGroup'
		
UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName
		
		END



	--********** Filter based on filter/search parameters *********--
	
	--CLAIMANT NAME

	IF(@ClaimantName IS NOT NULL AND @ClaimantName <> '')
	BEGIN
		SET @CodeBlockName = 'SectionC-ClaimantName'
		INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
		SELECT @NewGuid, @CodeBlockName, GETDATE()
		SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

		SET @WhereString = @WhereString + ' 
			AND (cpi.LastName + '', '' + cpi.FirstName) LIKE ''%' + @ClaimantName + '%'''

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName
	END
	
	--SETTLEMENT GROUP


	IF(@SettlementGroup IS NOT NULL AND @SettlementGroup <> '')
	BEGIN

		SET @CodeBlockName = 'SectionC-SettlementGoup'
		INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
		SELECT @NewGuid, @CodeBlockName, GETDATE()
		SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

		SET @WhereString = @WhereString + ' 
			AND SettlementGroup LIKE ''%' + @SettlementGroup + '%'''

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

	END
	
	--STATEID
	IF(@StateID IS NOT NULL)
	BEGIN

		SET @CodeBlockName = 'SectionC-StateID'
		INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
		SELECT @NewGuid, @CodeBlockName, GETDATE()
		SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

		SET @WhereString = @WhereString + ' 
			AND l.StateID = ' + CONVERT(nvarchar, @StateID)

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

	END

	--PLAINTIFFCOUNSELADDRESSID
	IF(@PlaintiffCounselAddressID IS NOT NULL)
	BEGIN

		SET @CodeBlockName = 'SectionC-PlaintiffCounselAddressID'
		INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
		SELECT @NewGuid, @CodeBlockName, GETDATE()
		SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)
		
		SET @WhereString = @WhereString + ' 
			AND l.PlaintiffCounselAddressID = ' + CONVERT(nvarchar, @PlaintiffCounselAddressID)

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

	END
	
	--REQUEST DATE RANGE
	IF(@RequestStartDate IS NOT NULL)
	BEGIN

		SET @CodeBlockName = 'SectionC-RequestStartDate'
		INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
		SELECT @NewGuid, @CodeBlockName, GETDATE()
		SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

		SET @WhereString = @WhereString + ' 
			AND CONVERT(date, sa.AuthorityRequestedDate) >= ''' + CONVERT(nvarchar, @RequestStartDate) + ''''
UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

	END



	IF(@RequestEndDate IS NOT NULL)
	BEGIN

		SET @CodeBlockName = 'SectionC-RequestEndDate'
		INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
		SELECT @NewGuid, @CodeBlockName, GETDATE()
		SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

		SET @WhereString = @WhereString + ' 
			AND CONVERT(date, sa.AuthorityRequestedDate) <= ''' + CONVERT(nvarchar, @RequestEndDate) + ''''

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

	END
	
	--FIRMS HAVING ALREADY GRANTED APPROVAL


	IF(@AlreadyGrantedFirmID IS NOT NULL)
	BEGIN

		SET @CodeBlockName = 'SectionC-AlreadyGrantedFirmID'
		INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
		SELECT @NewGuid, @CodeBlockName, GETDATE()
		SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

		SET @WhereString = @WhereString + ' 
			AND EXISTS (SELECT SettlementAuthorityResponseID 
						FROM tblSettlementAuthorityResponse
						WHERE SettlementAuthorityStatusID IN (1, 2, 5)
						AND SettlementAuthorityID = sa.SettlementAuthorityID
						AND (SELECT FirmID FROM  vSETTLEMENT_AUTHORITY_Users sau
						WHERE Email = CreatedBy) = ' + CONVERT(nvarchar, @AlreadyGrantedFirmID) + ')'

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

	END
	
	--FIRMS AWAITING APPROVAL (no approval response exists AND a requirement to do so does exist
	IF(@AwaitingApprovalFirmID IS NOT NULL)
	BEGIN

		SET @CodeBlockName = 'SectionC-AwaitingApprovalFirmID'
		INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
		SELECT @NewGuid, @CodeBlockName, GETDATE()
		SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

		SET @WhereString = @WhereString + ' 
			AND NOT EXISTS (SELECT SettlementAuthorityResponseID 
						FROM tblSettlementAuthorityResponse sarAwaiting
						INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON sau.Email = CreatedBy
						WHERE SettlementAuthorityStatusID IN (1, 2, 5)
						AND sarAwaiting.SettlementAuthorityID = sa.SettlementAuthorityID
						AND sau.FirmID = ' + CONVERT(nvarchar, @AwaitingApprovalFirmID) + ')
						
						AND EXISTS (SELECT SettlementAuthorityApproverFirmID
						FROM tblSettlementAuthorityApproverFirm saaf
						INNER JOIN tblSettlementApproverFirm safAwaiting ON saaf.SettlementApproverFirmID = safAwaiting.SettlementApproverFirmID
						INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON safAwaiting.FirmID = sau.FirmID
						AND saaf.SettlementAuthorityID = sa.SettlementAuthorityID
						AND ApprovalRequired = 1
						AND sau.FirmID = ' + CONVERT(nvarchar, @AwaitingApprovalFirmID) + ')'

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

	END
				
SET @CodeBlockName = 'SectionG-DeclareQueryString'
INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
SELECT @NewGuid, @CodeBlockName, GETDATE()
SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

	DECLARE @QueryString nvarchar(MAX) = 
		'SELECT DISTINCT	
				sa.AuthorityRequestedBy,
				sa.AuthorityRequestedDate,
				sa.SettlementGroupID,
				sa.SettlementAuthorityID,
				sa.DemandAmount,
				sa.AuthorityAmount,
				REPLACE(sa.Comment, CHAR(13)+CHAR(10), ''<br/>'') AS Comment, --Replace newlines with <br/>s  
				SettlementGroup AS SettlementGroupDelete,
				' + @CanRespondString + ' AS CanRespond,
				 '''' AS AmazonKey,
				 ClaimantListWithEvent AS ClaimantList,
				 COALESCE(Status, ''Finalized'') AS Status,
				 ResponseDueDate AS ResponseDueDate,
				 COALESCE(ParentSettlementAuthorityID, sa.SettlementAuthorityID), 
				 ' + @Field + '
		FROM tblSettlementAuthority sa
			INNER JOIN tblClaimantSettlement cs ON sa.ClaimLawsuitID = cs.ClaimLawsuitID
			INNER JOIN tblSettlementGroup sg ON cs.SettlementGroupID = sg.SettlementGroupID
			INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
			INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
			INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
			INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
			INNER JOIN  vSETTLEMENT_AUTHORITY_ClaimantLists claimants ON cs.SettlementGroupID = claimants.SettlementGroupID
		' + @JoinString + '	
		' + @WhereString + ' 
		' + @OrderByString + '
		OPTION (RECOMPILE)'

UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

PRINT @QueryString

SET @CodeBlockName = 'SectionH-Execute QueryString'
INSERT INTO zAuditStoredProc (RunGUID, CodeBlockName, CodeBlockStartTime)
SELECT @NewGuid, @CodeBlockName, GETDATE()
SET @AuditStoredProcID = (SELECT MAX(AuditStoredProcID) FROM zAuditStoredProc)

EXEC sp_executesql @QueryString


UPDATE zAuditStoredProc
SET CodeBlockEndTime = GETDATE()
WHERE RunGUID = @NewGuid AND CodeBlockName = @CodeBlockName

END










GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthority] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthority] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthority] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthority] TO [power_user]
GO
