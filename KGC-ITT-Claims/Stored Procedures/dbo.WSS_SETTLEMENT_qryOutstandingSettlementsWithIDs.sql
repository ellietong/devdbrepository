SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs]  
(@emailaddress		nvarchar(250), 
 @SettlementGroupID int = NULL,
 @MSDCategory		nvarchar(50),
 @SDCategory		nvarchar(50), 
 @ClaimantName		nvarchar(max),
 @ClaimantIDs		integer_list_tbltype READONLY)

AS

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 4
--DECLARE @EmailAddress nvarchar(250) = 'randy@gorijulianlaw.com'
--DECLARE @SettlementGroupID int = NULL

DECLARE @MaxSettlementGroupID int = (SELECT MAX(SettlementGroupID) FROM tblSettlementGroup)

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

IF object_id('tempdb..#ClaimantIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimantIDs
END
CREATE TABLE #ClaimantIDs (n int)
INSERT INTO #ClaimantIDs
	SELECT n FROM @ClaimantIDs

DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE 1 = 1 '
--' WHERE IsPrimaryStatus =1 
--						AND ClaimLawsuitStatusID IN (25) 
--						AND PrimaryDisease = 1
--						AND Year(settlementdate) >= 2010'

IF @ClaimantName			  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND ((LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' end) like ''%'' + @ClaimantNameParam + ''%'')' END
IF (SELECT COUNT(n) FROM #ClaimantIDs) <> 0 BEGIN SET @WhereString = @WhereString + ' AND ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)' END
IF @MSDCategory				  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND ' + @MSDCategory + ' = 0' END
IF @SDCategory				  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND ' + @SDCategory + ' = 0' END
IF @SettlementGroupID		  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND SettlementGroupID = ' + CONVERT(nvarchar(20), @SettlementGroupID) END

--IF @PlaintiffCounselAddressID IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(l.PlaintiffCounselAddressID,0) = ' + CONVERT(nvarchar(20),@PlaintiffCounselAddressID) END
--IF @ReferenceID				  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND cpi.INJ_PRTY_ID LIKE ''%'' + @ReferenceIDParam + ''%''' END
--IF @TrialGroupID			  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND cl.TrialGroupID = ' + CONVERT(nvarchar(20), @TrialGroupID) END

--IF @IsMDL					  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(IsMDL, 0) = ' + CONVERT(nvarchar(20),@IsMDL) END
--IF @DocketNumber			  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND DocketNumber LIKE ''%'' + @DocketNumberParam + ''%''' END
--IF @ClaimLawsuitStatusID	  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND cls.ClaimLawsuitStatusID = ' + CONVERT(nvarchar(20),@ClaimLawsuitStatusID) END

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblLawsuit l ON msd.LawsuitID = l.LawsuitID
						INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END


DECLARE @QueryString NVARCHAR(MAX) = ''
SET @QueryString = '
SELECT DISTINCT ClaimantPersonalInfoID AS ClaimantID, 
				FirstName, 
				LastName, 
				ClaimantPersonalInfoID, 
				LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + 
					CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' END AS ClaimantName,
				DOB AS BirthDate, 
				DOD AS DeceasedDate,
				SettlementDate, 
				SettlementAmount, 
				SettlementGroup,
				SettlementGroupID,
				MissingClaimantInfo,
				MissingDocumentation
				--COALESCE(dbo.fnMissingClaimantInformation(ClaimLawsuitID), ''All required claimant information submitted.'') AS MissingClaimantInfo,
				--COALESCE(dbo.fnMissingDocumentation(ClaimLawsuitID), ''All required documents submitted.'') AS MissingDocumentation
FROM vMissingSettlementDocumentation msd

'
+ @JoinString +
@WhereString +
' ORDER BY ClaimantName 
  OPTION (RECOMPILE)'

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@ClaimantNameParam nvarchar(max)', @ClaimantNameParam = @ClaimantName


GO
