SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_AmazonDownload_JulySeptComplaints]
	
AS
BEGIN
Select DISTINCT d.DocumentID, AmazonKey, DATENAME(month, DateCreated) AS ClaimantDocumentType,
DocumentTitle
FROM tblDocuments d
WHERE ClaimantDocumentTypeID = 8 
AND DateCreated > '6/30/2013'
AND DateCreated < '10/1/2013'
--ORDER BY COALESCE(LastName, '') + '_' + COALESCE(FirstName, '') + '_' +  COALESCE(CONVERT(nvarchar, c.ClaimantPersonalInfoID), '')
END

GO
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonDownload_JulySeptComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_AmazonDownload_JulySeptComplaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonDownload_JulySeptComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_AmazonDownload_JulySeptComplaints] TO [power_user]
GO
