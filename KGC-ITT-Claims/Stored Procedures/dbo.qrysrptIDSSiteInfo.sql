SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qrysrptIDSSiteInfo] (@CorporateHistoryID int)
AS 
SET NOCOUNT ON 
--Updated: RLS 6/27/07 to include PVD and NRD
------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, PULL INCLUDED SITE PAST COSTS by site
------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @PastSiteCosts1 TABLE (siteid int, PrevBilledLegal money, PrevBilledOther money, 
AdtlAmt_Legal money, AdtlAmt_Other money)
INSERT INTO @PastSiteCosts1
SELECT sc.siteid, 
sum(CASE WHEN (sc.Billed = 1 AND sc.CostTypeID = 1) THEN sc.Amount ELSE 0 END) AS PrevBilledLegal, 
sum(CASE WHEN (sc.Billed = 1 AND sc.CostTypeID <> 1) THEN sc.Amount ELSE 0 END) AS PrevBilledOther, 
sum(CASE WHEN (sc.Billed <>1 AND sc.CostTypeID = 1) THEN sc.amount ELSE 0 END) as AdtlAmt_Legal,
sum(CASE WHEN (sc.Billed <>1 AND sc.CostTypeID <> 1) THEN sc.amount ELSE 0 END) as AdtlAmt_Other
FROM tblSiteCosts sc
LEFT JOIN tblEnviroSitesDetail d on sc.siteid = d.siteid
LEFT JOIN tblRecordSource rs ON sc.recordsourceid = rs.recordsourceid
LEFT JOIN tblCostType ct ON sc.CostTypeID = ct.CostTypeID
WHERE rs.ExcludeRecordSource <> 1 AND ct.ExcludeCostType <> 1 AND d.ExcludedSite <> 1 AND splitcovcost = 0
GROUP BY sc.siteid
order by sc.siteid
---------split out dual coverage site costs
DECLARE @PastCostCov TABLE (siteid int, PrevBilledLegal money, PrevBilledOther money, 
AdtlAmt_Legal money, AdtlAmt_Other money)
INSERT INTO @PastCostCov
SELECT CASE WHEN sc.siteid = 100001 THEN convert(nvarchar,100002) END as SiteIDs,
sum(
CASE WHEN sc.siteid = 100001 AND sc.Billed = 1 AND sc.CostTypeID = 1 THEN sc.Amount * .5 ELSE 0
END) AS PrevBilledLegal,
sum(
CASE WHEN sc.siteid = 100001 AND sc.Billed = 1 AND sc.CostTypeID <> 1 THEN sc.Amount * .5 ELSE 0
END) AS PrevBilledOther, 
0 as AdtlAmt_Legal,
0 as AdtlAmt_Other
FROM tblSiteCosts sc
LEFT JOIN tblEnviroSitesDetail d on sc.siteid = d.siteid
LEFT JOIN tblRecordSource rs ON sc.recordsourceid = rs.recordsourceid
LEFT JOIN tblCostType ct ON sc.CostTypeID = ct.CostTypeID
WHERE rs.ExcludeRecordSource <> 1 AND ct.ExcludeCostType <> 1 AND d.ExcludedSite <> 1 AND splitcovcost = 1
Group by sc.siteid
UNION
SELECT sc.siteid, 
sum(CASE WHEN sc.siteid = 100001 AND sc.Billed = 1 AND sc.CostTypeID = 1 THEN sc.Amount * .5 ELSE 0
END) AS PrevBilledLegal,
sum(
CASE WHEN sc.siteid = 100001 AND sc.Billed = 1 AND sc.CostTypeID <> 1 THEN sc.Amount * .5 ELSE 0
END) AS PrevBilledOther, 
0 as AdtlAmt_Legal,
0 as AdtlAmt_Other
FROM tblSiteCosts sc
LEFT JOIN tblEnviroSitesDetail d on sc.siteid = d.siteid
LEFT JOIN tblRecordSource rs ON sc.recordsourceid = rs.recordsourceid
LEFT JOIN tblCostType ct ON sc.CostTypeID = ct.CostTypeID
WHERE rs.ExcludeRecordSource <> 1 AND ct.ExcludeCostType <> 1 AND d.ExcludedSite <> 1 AND splitcovcost = 1
Group by sc.siteid
--union the two datasets.  
DECLARE @PastSiteCostsTemp TABLE (siteid int, PrevBilledLegal money, PrevBilledOther money, 
AdtlAmt_Legal money, AdtlAmt_Other money)
INSERT INTO @PastSiteCostsTemp
SELECT pc1.* 
FROM @PastSiteCosts1 pc1
UNION 
Select pcc.* 
FROM @PastCostCov pcc
--combine past cost data into single site id's
DECLARE @PastCosts TABLE (siteid int, PrevBilledLegal money, PrevBilledOther money, 
AdtlAmt_Legal money, AdtlAmt_Other money)
INSERT INTO @PastCosts
SELECT siteid, sum(PrevBilledLegal) AS 'PrevBilledLegal', sum(PrevBilledOther) AS 'PrevBilledOther', 
sum(AdtlAmt_Legal) AS 'AdtlAmt_Legal', sum(AdtlAmt_Other) AS 'AdtlAmt_Other'
FROM @PastSiteCostsTemp
GROUP BY siteid
------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, PULL INCLUDED SITE FUTURE COSTS by site
------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @FutureCostsTemp table (SiteID int, LiabPercent decimal(18,4), FutureCosts money, PVD money, NRD money)
INSERT INTO @FutureCostsTemp
SELECT fc.siteid, CASE WHEN fc.LiabPercent IS NULL THEN 0
WHEN fc.siteid = 125 THEN '.5'
ELSE fc.LiabPercent END AS LiabPercent,
sum(CASE 
	WHEN fc.siteid = 100002 THEN fc.TotalCosts * .5  
	ELSE fc.TotalCosts * fc.LiabPercent
END) AS FutureCosts,
SUM(fc.PVD * fc.LiabPercent) AS 'PVD',
SUM(fc.NRD * fc.LiabPercent) AS 'NRD'
FROM tblFutureCosts fc
LEFT JOIN tblEnviroSitesDetail e ON fc.siteid = e.siteid
WHERE fc.siteid not in (191, 193, 288, 289, 290)
GROUP BY fc.siteid, CASE WHEN fc.LiabPercent IS NULL THEN 0
WHEN fc.siteid = 125 THEN '.5'
ELSE fc.LiabPercent END
--create temp table for ID's with split coverage costs
DECLARE @SiteChange TABLE (Siteid int, AltSiteID int)
INSERT INTO @SiteChange
SELECT e.siteid, 
CASE WHEN e.siteid = 100001 THEN 100002 END as AltSiteID
FROM tblEnviroSitesDetail e 
--LEFT JOIN tblOccurrenceGroup og ON e.siteid = og.siteid
--create temp table with future costs for split IDs
DECLARE @FutSplitCosts TABLE (SiteID int, LiabPercent decimal(18,4), FutureCosts money, PVD money, NRD money)
INSERT INTO @FutSplitCosts
SELECT CASE WHEN fc.siteid = 100002 THEN convert(nvarchar,100001)
END AS 'SiteID',
CASE WHEN fc.LiabPercent IS NULL THEN 0 
WHEN fc.siteid = 100002 THEN '.5'  
ELSE fc.LiabPercent END AS LiabPercent,
sum(CASE WHEN fc.siteid = 100002 THEN fc.TotalCosts * .5 END) AS FutureCosts,
SUM(fc.PVD * fc.LiabPercent) AS 'PVD',
SUM(fc.NRD * fc.LiabPercent) AS 'NRD'
FROM tblFutureCosts fc
left join @SiteChange c on c.altsiteid = fc.siteid
GROUP BY CASE WHEN fc.siteid = 100002 THEN convert(nvarchar,100001) END,
CASE WHEN fc.LiabPercent IS NULL THEN 0 
WHEN fc.siteid = 100002 THEN '.5'  
ELSE fc.LiabPercent END
--Union Future Costs Together
DECLARE @FutureCostsUnion TABLE (SiteID int, LiabPercent decimal(18,4), FutureCosts money, PVD money, NRD money)
INSERT INTO @FutureCostsUnion
SELECT fct.*
FROM @FutureCostsTemp fct
UNION
SELECT fsc.*
FROM @FutSplitCosts fsc
--Combine Future Cost Data into single site ID's
DECLARE @FutureCosts TABLE (SiteID int, LiabPercent decimal(18,4), FutureCosts money, PVD money, NRD money)
INSERT INTO @FutureCosts
SELECT fc.siteid, fc.liabpercent, sum(fc.futurecosts) AS 'FutureCosts', sum(pvd) AS 'PVD', sum(NRD) AS 'NRD'
FROM @FutureCostsUnion fc
GROUP BY fc.siteid, fc.liabpercent
ORDER BY fc.siteid
------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, PULL INCLUDED SITE INFO by site
------------------------------------------------------------------------------------------------------------------------------------------------------
declare @Site table (SiteID int, SiteName nvarchar (150),  state nvarchar(10), country nvarchar(50))
insert into @Site
SELECT SiteID, SiteName, state, country
FROM tblEnvirositesDetail d
left join tblstate t on t.stateid = d.stateid
left join tblcountry c on c.countryid = d.countryid
GROUP BY SiteID, SiteName, state, country
------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, FLAG PREVIOUSLY BILLED by site
------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @Billed table (SiteId int, Flag bit)
Insert into @billed
select siteID, 1
From tblSiteCosts
where billed = 1
group by siteid
Order by siteid
------------------------------------------------------------------------------------------------------------------------------------------------------
--In this step, PULL DATA TOGETHER
------------------------------------------------------------------------------------------------------------------------------------------------------
select sd.CorporateHistoryID, ip.CorporateHistoryID, s.SiteID, s.SiteName, s.state, s.country,
CASE WHEN p.PrevBilledLegal is null then 0 else p.PrevBilledLegal end as 'PrevBilledLegal',
CASE WHEN p.PrevBilledOther is null then 0 else p.PrevBilledOther end as 'PrevBilledOther',
CASE WHEN p.AdtlAmt_Legal is null then 0 else p.AdtlAmt_Legal end as 'AdtlAmt_Legal', 
CASE WHEN p.AdtlAmt_Other is null then 0 else p.AdtlAmt_Other end as 'AdtlAmt_Other', 
CASE WHEN p.PrevBilledLegal is null then 0 else p.PrevBilledLegal end  +
CASE WHEN p.PrevBilledOther is null then 0 else p.PrevBilledOther end  +
CASE WHEN p.AdtlAmt_Legal is null then 0 else p.AdtlAmt_Legal end  +
CASE WHEN p.AdtlAmt_Other is null then 0 else p.AdtlAmt_Other end as PastCosts,
case when fc.futurecosts is null then 0 else fc.futurecosts END AS 'FutureCosts',
case when fc.PVD is null then 0 else fc.pvd end as 'PVD',
case when fc.nrd is null then 0 else fc.nrd end as 'NRD',
CASE WHEN p.PrevBilledLegal is null then 0 else p.PrevBilledLegal end +
CASE WHEN p.PrevBilledOther is null then 0 else p.PrevBilledOther end +
CASE WHEN p.AdtlAmt_Legal is null then 0 else p.AdtlAmt_Legal end +
CASE WHEN p.AdtlAmt_Other is null then 0 else p.AdtlAmt_Other end + 
CASE when fc.futurecosts is null then 0 else fc.futurecosts END +
case when fc.PVD is null then 0 else fc.pvd end +
case when fc.nrd is null then 0 else fc.nrd end as 'Total', 
sd.ExcludedSite,
case when b.Flag = 1 then 1 else 0 END as 'Billed',
fc.LiabPercent
from @site S
LEFT JOIN @PastCosts P on p.siteid = S.siteid
LEFT JOIN @billed B on B.siteid = S.siteid
LEFT JOIN @FutureCosts fc on fc.siteid = s.siteid
LEFT JOIN tblEnviroSitesDetail sd ON s.siteid = sd.siteid
LEFT JOIN tblCorporateHistoryProfile ip ON sd.CorporateHistoryID = ip.CorporateHistoryID
where sd.ExcludedSite <> 1 AND sd.CorporateHistoryID = @CorporateHistoryID AND
CASE WHEN p.PrevBilledLegal is null then 0 else p.PrevBilledLegal end +
CASE WHEN p.PrevBilledOther is null then 0 else p.PrevBilledOther end +
CASE WHEN p.AdtlAmt_Legal is null then 0 else p.AdtlAmt_Legal end +
CASE WHEN p.AdtlAmt_Other is null then 0 else p.AdtlAmt_Other end + 
CASE when fc.futurecosts is null then 0 else fc.futurecosts END +
case when fc.PVD is null then 0 else fc.pvd end +
case when fc.nrd is null then 0 else fc.nrd end >=0
order by s.sitename desc
RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptIDSSiteInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptIDSSiteInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptIDSSiteInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptIDSSiteInfo] TO [power_user]
GO
