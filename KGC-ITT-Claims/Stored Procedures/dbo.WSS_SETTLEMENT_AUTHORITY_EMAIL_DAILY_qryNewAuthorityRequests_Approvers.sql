SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewAuthorityRequests_Approvers] 
AS
BEGIN

--Executed in External ITT Settlement Authority 9AM, 12PM, 2PM, 4PM Alerts job

--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

SET @Subject = 'ITT Claims New Settlement Authority Requests: ' + CONVERT(varchar, GETDATE(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.	
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

DECLARE @NewAuthorityRequest TABLE (SettlementAuthorityID int,
									AuthorityRequesteDate datetime,
									SettlementGroup nvarchar (max),
									DemandAmount money,
									AuthorityAmount money,
									Comment nvarchar (max),
									ClaimantList nvarchar (max),
									ResponseDueDate datetime)
INSERT INTO @NewAuthorityRequest --used to populate the email table
	SELECT DISTINCT
		sa.SettlementAuthorityID,
		sa.AuthorityRequestedDate,
		SettlementGroup,	
		sa.DemandAmount,
		sa.AuthorityAmount,
		REPLACE(sa.Comment, CHAR(13) + CHAR(10), '<br/>') AS Comment, --Replace newlines with <br/>s  
		ClaimantList,
		ResponseDueDate AS ResponseDueDate 
	FROM tblSettlementAuthority                                            sa
		INNER JOIN tblSettlementGroup                                      sg        ON sa.SettlementGroupID     = sg.SettlementGroupID
		INNER JOIN tblClaimantSettlement                                   cs        ON sg.SettlementGroupID     = cs.SettlementGroupID
		INNER JOIN tblClaimLawsuit                                         cl        ON cs.ClaimLawsuitID        = cl.ClaimLawsuitID
		INNER JOIN tblClaim                                                c         ON cl.ClaimID               = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo                                 cpi       ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblLawsuit                                              l         ON cl.LawsuitID             = l.LawsuitID
		LEFT  JOIN tblDocuments                                            d         ON sa.DocumentID            = d.DocumentID
		INNER JOIN vSETTLEMENT_AUTHORITY_ClaimantLists                     claimants ON sa.SettlementGroupID     = claimants.SettlementGroupID
		LEFT  JOIN vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Unfiltered fra       ON sa.SettlementAuthorityID = fra.SettlementAuthorityID
	WHERE COALESCE(IsRequestApproved, 0) = 1    --Limits to only requests that have been marked as Approved (by ITT)
		AND ParentSettlementAuthorityID IS NULL --Limits to only initial requests, not followup requests for additional funds
		AND InsurerNotificationDate IS NULL     --Limits to only requests that have not yet been sent to insurers
	ORDER BY AuthorityRequestedDate DESC

DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar (max))

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar (MAX)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'

DECLARE @LastLawsuitID nvarchar (250) = ''

IF (SELECT COUNT(SettlementAuthorityID) FROM @NewAuthorityRequest) > 0
BEGIN
	SET @BodyMessage = @BodyMessage + '<H4>The following are new settlement authority requests: </H4>'
	SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">'
	SET @BodyMessage = @BodyMessage + N'<tr><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Settlement Group</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Linked Claimants</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Response<br/> Due Date</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Demand Amount</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Authority Amount</th>'

	DECLARE @SettlementAuthorityID int,
			@AuthorityRequesteDate datetime,
			@SettlementGroup nvarchar (max),
			@DemandAmount money,
			@AuthorityAmount money,
			@ClaimantList nvarchar (max),
			@ResponseDueDate datetime

	DECLARE Doc_List CURSOR FOR 
		SELECT DISTINCT AuthorityRequesteDate,
			SettlementGroup,
			DemandAmount,
			AuthorityAmount,
			ClaimantList,
			ResponseDueDate
		FROM @NewAuthorityRequest r
		ORDER BY ResponseDueDate

	OPEN Doc_List
	FETCH NEXT FROM Doc_List INTO @AuthorityRequesteDate, @SettlementGroup, @DemandAmount, @AuthorityAmount, @ClaimantList, @ResponseDueDate
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @BodyMessage = @BodyMessage + '<tr>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @SettlementGroup + ' </td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @ClaimantList + ' </td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + CONVERT(nvarchar(20), @ResponseDueDate, 101) + ' </td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + '$' + CONVERT(nvarchar(20), @DemandAmount, 1) + ' </td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + '$' + CONVERT(nvarchar(20), @AuthorityAmount, 1) + ' </td>'

		FETCH NEXT FROM Doc_List INTO @AuthorityRequesteDate, @SettlementGroup, @DemandAmount, @AuthorityAmount, @ClaimantList, @ResponseDueDate 
	END

	CLOSE Doc_List
	DEALLOCATE Doc_List

	SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'

	--Undo the HTML stripping from the xml path function above so embedded tags work
	SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

	--Link for document library
	SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/ITT/ittcp/Pages/SettlementAuthority.aspx">here</A> to view the Authority Requests.</b><br><br>'
END
ELSE
BEGIN
	SET @BodyMessage = @BodyMessage + '<H4>No new Settlement Authority Requests have been submitted. </H4>'
END

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + 
'
    <table style="width: 500px;" border="0" cellpadding="4" cellspacing="5" width="500">
        <tbody>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    <span style="color: #F06021; font-weight: bold;">KCIC Claims Administration</span><br />
                    </td>
            </tr>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    <img src="http://kcic.com/i/kcic-logo.png" width="103" height="93" />
                </td>
            </tr>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    733 10th Street, NW | Suite 3001 | Washington, DC 20001
                <br />
                    <a href="mailto:Claims@kcic.com" style="color: #737271;">Claims@kcic.com</a>
                    |<span style="font-family: Arial, Helvetica, sans-serif; color: #F06021"> </span>202.772.2300
                    | <a href="http://www.kcic.com" target="_new"
                        style="color: #FF6229">kcic.com</a>
                </td>
            </tr>
        </tbody>
    </table>
'

DECLARE @RecipientList nvarchar(MAX) = ' '
SET @RecipientList=(SELECT DISTINCT sau.Email + '; ' AS [text()]
					FROM tblSettlementAuthorityApproverFirm saaf
						INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
						INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON saf.FirmID = sau.FirmID
						INNER JOIN @NewAuthorityRequest nar ON saaf.SettlementAuthorityID = nar.SettlementAuthorityID
					WHERE sau.PersonID IN (4336, 4203, 4597) -- Jens Fog, Susan Eisenlohr, Paul Sliwicki
					For XML PATH (''))

DECLARE @CCList nvarchar(MAX) = ''

----QC Recipient List
--Set @Bodymessage = @BodyMessage + '<br/> Recipients: ' + @RecipientList

EXEC msdb.dbo.sp_send_dbmail	
	@profile_name = 'Claims',
	@recipients = @RecipientList,
	@copy_recipients = @CCList, 
	@blind_copy_recipients = 'itt@kcic.com',
	@subject = @subject,
	@body = @BodyMessage,
	@body_format = 'html'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewAuthorityRequests_Approvers] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewAuthorityRequests_Approvers] TO [base_user]
GO
