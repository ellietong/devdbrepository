SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByMonthByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null, @emailaddress nvarchar(200) = null) 
AS

--DECLARE 
--@MatterID int=1, 
--@StartDate datetime='1/1/2013',
--@EndDate datetime='12/31/2013'

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @YearID int
--Set @YearId=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

    -- Insert statements for procedure here
select DATENAME("mm", coalesce(SettlementDate,statusdate))+' '+convert(nvarchar,year(coalesce(SettlementDate,statusdate))) AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS Count, MONTH(coalesce(SettlementDate,statusdate)) As MonthID
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimantSettlement cs ON cs.ClaimLawsuitID=cl.ClaimLawsuitID
where	coalesce(SettlementDate,statusdate) between coalesce(@StartDate,'1/1/1900')
		and coalesce(@EndDate, '1/1/2100')
	and ClaimLawsuitStatusID in (23,25) 
		and  IsPrimaryStatus=1
group by DATENAME("mm", coalesce(SettlementDate,statusdate))+' '+convert(nvarchar,year(coalesce(SettlementDate,statusdate))), MONTH(coalesce(SettlementDate,statusdate)), year(coalesce(SettlementDate,statusdate))
order by year(coalesce(SettlementDate,statusdate)), MONTH(coalesce(SettlementDate,statusdate))
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByMonthByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByMonthByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByMonthByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByMonthByYear] TO [power_user]
GO
