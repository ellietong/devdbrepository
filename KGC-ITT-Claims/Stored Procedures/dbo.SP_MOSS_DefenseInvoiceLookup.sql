SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[SP_MOSS_DefenseInvoiceLookup]
AS
SET NOCOUNT ON

SELECT statusid, status
FROM tblDefenseInvoiceStatus
where StatusID <>5
UNION
	--SELECT null as StatusID, '<< All Status >>' As status
	--UNION
	SELECT null, '<<Any Status>>'
order by Status
RETURN
GO
