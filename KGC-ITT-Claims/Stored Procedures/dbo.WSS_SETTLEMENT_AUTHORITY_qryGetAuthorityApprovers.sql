SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetAuthorityApprovers]
(@emailaddress nvarchar(1000))
AS -- Declare the temporary fields used to track firm and email domain

DECLARE @firmID int
SET @firmID = (SELECT FirmID FROM  vSETTLEMENT_AUTHORITY_Users WHERE Email = @emailaddress)

-- Settlement Authority Approver Firms
BEGIN
	SELECT DISTINCT CONVERT(nvarchar(10),FirmID) AS FirmID
	FROM  vSETTLEMENT_AUTHORITY_Users 
	WHERE FirmID IN (63, 155) -- ACE, NationWide
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetAuthorityApprovers] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetAuthorityApprovers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetAuthorityApprovers] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetAuthorityApprovers] TO [power_user]
GO
