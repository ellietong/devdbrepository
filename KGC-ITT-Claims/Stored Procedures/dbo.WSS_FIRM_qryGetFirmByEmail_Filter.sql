SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_FIRM_qryGetFirmByEmail_Filter] (@emailaddress nvarchar(1000))
AS

DECLARE @firmID int
SET @firmID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)

IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
BEGIN
	SELECT DISTINCT DefenseCounselID AS FirmID, DefenseCounsel as Firm 
	FROM tblDefenseCounsel
	WHERE DefenseCounselID NOT IN (135, 136)
	UNION 
	SELECT NULL, '<< All Firms >>'
	ORDER BY DefenseCounsel
END
ELSE
BEGIN
	SELECT DefenseCounselID As FirmID, DefenseCounsel as Firm
	FROM tblDefenseCounselAddress
	WHERE DefenseCounselID = @FirmID
	UNION 
	SELECT NULL, '<< All Firms >>'
	ORDER BY DefenseCounsel
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmByEmail_Filter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmByEmail_Filter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirmByEmail_Filter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirmByEmail_Filter] TO [power_user]
GO
