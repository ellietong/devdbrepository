SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertResponse] (@SettlementAuthorityID         bigint, 
																	@SettlementAuthorityResponseID bigint = NULL,
																	@Comment                       nvarchar(MAX),
																	@SettlementAuthorityStatusID   int,
																	@Emailaddress                  nvarchar(250) = NULL)
AS
BEGIN

DECLARE @FirmID int
SET @FirmID = (SELECT TOP 1 FirmID FROM vSETTLEMENT_AUTHORITY_Users WHERE Email = @emailaddress)

INSERT INTO tblSettlementAuthorityResponse (Comment, CreatedBy, DateCreated, ParentSettlementAuthorityResponseID, SettlementAuthorityID, SettlementAuthorityStatusID, FirmID)
	SELECT @Comment, @Emailaddress, GETDATE(), @SettlementAuthorityResponseID, @SettlementAuthorityID, @SettlementAuthorityStatusID, @FirmID

IF (@FirmID = 277 AND @SettlementAuthorityStatusID = 2) --If ITT is responding and the status is Denied, mark the request as Denied
BEGIN
	UPDATE tblSettlementAuthority
		SET AuthorityDenied = 1
		WHERE SettlementAuthorityID = @SettlementAuthorityID
END
ELSE IF (@FirmID = 277 AND @SettlementAuthorityStatusID = 1)  -- If ITT is responding and the status is Approved...
BEGIN
	IF(SELECT COUNT(SettlementAuthorityID) 
		FROM tblSettlementAuthority 
		WHERE SettlementAuthorityID = @SettlementAuthorityID
			AND COALESCE(IsRequestApproved, 0) = 0) > 0       --If the settlement authority request is not currently marked as approved, mark the request as Approved
	BEGIN			
		UPDATE tblSettlementAuthority
			SET RequestApprovedBy = @Emailaddress,
				RequestApprovedDate = GETDATE(),
				IsRequestApproved = 1
			WHERE SettlementAuthorityID = @SettlementAuthorityID
	END
END

END





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertResponse] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertResponse] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertResponse] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryInsertResponse] TO [power_user]
GO
