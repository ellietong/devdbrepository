SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryInsVendorFirm]
(
	@VendorFirm nvarchar(250),
	@Address1 nvarchar(250),
	@Address2 nvarchar(250),
	@City nvarchar(250),
	@StateID int,
	@Zip nvarchar(10),
	@TaxID nvarchar(50),
	@SPUsername nvarchar(250)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO m_tblVendorFirm (VendorFirm, Address1, Address2, City, StateID, ZipCode, TaxID, SPUsername)
	SELECT @VendorFirm, @Address1, @Address2, @City, @StateID, @Zip, @TaxID, @SPUsername
		
	SELECT MAX(VendorFirmID) FROM m_tblVendorFirm
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInsVendorFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInsVendorFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInsVendorFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInsVendorFirm] TO [power_user]
GO
