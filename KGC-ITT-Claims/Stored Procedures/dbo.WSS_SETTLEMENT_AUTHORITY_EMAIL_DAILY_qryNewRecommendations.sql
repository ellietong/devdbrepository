SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewRecommendations] 
AS
SET NOCOUNT ON
 
BEGIN
 
 
/*
	TEST VARS
*/
--DECLARE @SettlementAuthorityID bigint = 1
 
--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)
	
SET @Subject = 	'ITT Payment Recommendation Summary ' + CONVERT(varchar, getdate(), 107) 
 
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	
 
 
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
 
 
 
 
--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)
 
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	
 
 
--->
</STYLE>'
 
 
 
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
--CSS
DECLARE @MainHeaderStyle nvarchar(MAX) = 'background-color: #0079C1; font-weight: bold; color: white; padding: 4px;'
DECLARE @tdStyle nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'
DECLARE @RecipientList nvarchar(500)
DECLARE @SendEmail bit=0
 
IF(SELECT COUNT(*)	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
		INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		INNER JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID	
		LEFT join tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
		LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
			AND clst.ClaimLawsuitStatusID = 23
			AND cs.PaymentRecommendedDate IS NOT NULL
			AND cs.PaymentApprovedDate IS NULL
			AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
			AND ExecutedReleaseDate IS NOT NULL
			) > 0
BEGIN
 
	--LINK TO SETTLEMENT AUTHORITY
	SET @BodyMessage = @BodyMessage + '<H4>There are settled claim(s) awaiting approval for payment.<br/><br/>' + 
	'To view additional information and approve these claims for settlement, please click ' + 
	'<a href="https://www.kcicextranet.com/clients/itt/ittcp/Pages/SettlementPaymentApproval.aspx?approval=">here</a>. </H4>'
	
	SET @SendEmail = 1
	
	-- CHECK IF THERE ARE ANY RECOMMENDED INVOICES AWAITING APPROVAL
	IF(SELECT COUNT(*) FROM tblDocuments d
	  LEFT JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
	  LEFT JOIN vtblDefenseInvoice dinv	  ON d.DocumentID = dinv.DocumentID 
	  INNER JOIN tblDefenseInvoiceStatus s ON COALESCE(di.StatusID, 3) = s.statusid
      LEFT JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
      LEFT JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalinfoID
      LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
      LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
      LEFT JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
      LEFT JOIN tblPaymentDefense pd on pd.DefenseInvoiceID = di.DefenseInvoiceID
	WHERE IsRecommended = 1 
	AND d.ClaimantDocumentTypeID IN (40, 41, 254) 
	AND pd.PaymentDefenseID IS NULL) > 0
      
	BEGIN
	--LINK TO INVOICE APPROVAL
	set @BodyMessage = @BodyMessage + '<br><H4>There are defense invoices awaiting approval for payment.<br/><br/>' + 
	'To view additional information and approve these invoices, please click ' + 
	'<a href="https://www.kcicextranet.com/clients/ITT/ittcp/Pages/Invoices.aspx">here</a>. </H4>'
	END
		
ELSE
 
-- ONLY CHECK IF THERE ARE ANY RECOMMENDED INVOICES AWAITING APPROVAL
	IF(SELECT COUNT(*) FROM tblDocuments d
	  LEFT JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
	  LEFT JOIN vtblDefenseInvoice dinv	  ON d.DocumentID = dinv.DocumentID 
	  INNER JOIN tblDefenseInvoiceStatus s ON COALESCE(di.StatusID, 3) = s.statusid
      LEFT JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
      LEFT JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalinfoID
      LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
      LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
      LEFT JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
      LEFT JOIN tblPaymentDefense pd on pd.DefenseInvoiceID = di.DefenseInvoiceID
	WHERE IsRecommended = 1 
	AND d.ClaimantDocumentTypeID IN (40, 41, 254) 
	AND pd.PaymentDefenseID IS NULL) > 0
      
	BEGIN
	--LINK TO INVOICE APPROVAL
	set @BodyMessage = @BodyMessage + '<br><H4>There are defense invoices awaiting approval for payment.<br/><br/>' + 
	'To view additional information and approve these invoices, please click ' + 
	'<a href="https://www.kcicextranet.com/clients/ITT/ittcp/Pages/Invoices.aspx">here</a>. </H4>'
	
	SET @SendEmail = 1
 
	
		
	----QC Recipient List
	--Set @Bodymessage = @BodyMessage + 'Recipients' + @RecipientList
	--PRINT @RecipientList
	--Execute stored procedure to send the email to that firm
	--CLOSE TABLE
	SET @BodyMessage = @BodyMessage + '</table>'
	---KCIC address and disclaimer
	SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
	733 10th Street, NW, Suite 3001<br>
	Washington, DC 20001<br>
	(t) 202-772-2300<br>
	(f) 202-772-2333'
	END
END
 
IF @SendEmail = 1
BEGIN
 
SET @RecipientList='loree.shelko@itt.com; pgoldstein@mcguirewoods.com;gmacsteel@mcguirewoods.com;scottc@kcicllc.com;shockleym@kcicllc.com;chaudoinr@kcic.com;itt@kcicllc.com' --ITT (Joanne), NCC, and KCIC users
EXEC msdb.dbo.sp_send_dbmail
--The Profile_Name sends from [client]@kcicllc.com email address
@profile_name='Claims',
--@recipients='prossc@kcicllc.com;hagep@kcicllc.com',
@recipients = @RecipientList,
@copy_recipients = '',
@blind_copy_recipients = 'itt@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
END
	
END
 



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewRecommendations] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewRecommendations] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewRecommendations] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewRecommendations] TO [power_user]
GO
