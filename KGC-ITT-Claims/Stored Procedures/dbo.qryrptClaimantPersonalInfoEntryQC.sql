SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimantPersonalInfoEntryQC] (@ClaimantPersonalInfoID int)

as

Select cpi.ClaimantPersonalInfoID,
cpi.FirstName as ClaimantFirstName, 
cpi.MiddleName as ClaimantMiddleName, 
cpi.LastName as ClaimantLastName, 
Suffix as ClaimantSuffix,
SocialSecurityNumber, 
DeceasedDate, 
MaritalStatus, 
Gender, 
Address1,
Address2,
City,
[State],
Zip,
CASE WHEN Extension IS NULL OR Extension LIKE '' THEN '(' + CONVERT(nvarchar(3),LEFT(Phone,3)) + ') ' + CONVERT(nvarchar(3),LEFT(RIGHT(Phone,7),3)) + '-' + CONVERT(nvarchar(4),RIGHT(Phone,4))
ELSE '(' + CONVERT(nvarchar(3),LEFT(Phone,3)) + ') ' + CONVERT(nvarchar(3),LEFT(RIGHT(Phone,7),3)) + '-' + CONVERT(nvarchar(4),RIGHT(Phone,4)) + ' x' + CONVERT(nvarchar(10),Extension) END AS Phone,
BirthDate,
HealthInsuranceClaimNumber,
Extension,
MedicareBeneficiaryStatus as medicarebeneficiary, IsSmoker, SmokerStart, SmokerEnd, PacksPerDay
From tblClaimantPersonalInfo cpi 
Left Join tblGender g on g.GenderID = cpi.GenderID
Left Join tblClaimantSuffix cs on cs.SuffixID = cpi.SuffixID
Left Join tblMaritalStatus ms on ms.MaritalStatusID = cpi.MaritalStatusID
Left Join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
Left Join tblState st ON cpi.StateID = st.StateID
left join tblMedicareBeneficiaryStatus mbs on mbs.MedicareBeneficiaryStatusID = cpi.MedicareBeneficiaryStatusID
Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND c.IsCurrentClaim = 1
Group By
cpi.ClaimantPersonalInfoID,
cpi.FirstName , 
cpi.MiddleName , 
cpi.LastName , 
Suffix,
SocialSecurityNumber, 
DeceasedDate, 
MaritalStatus, 
Gender, 
Address1,
Address2,
City,
[State],
Zip,
Extension, Phone,
BirthDate,
HealthInsuranceClaimNumber,
Extension,
MedicareBeneficiaryStatus, IsSmoker, SmokerStart, SmokerEnd, PacksPerDay
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantPersonalInfoEntryQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantPersonalInfoEntryQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantPersonalInfoEntryQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantPersonalInfoEntryQC] TO [power_user]
GO
