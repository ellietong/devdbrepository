SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityResponses_OLD] 
(
 @SettlementAuthorityID bigint, 
 @emailaddress nvarchar(250)
 )
AS
BEGIN
	--DECLARE @SettlementAuthorityID int = 337
	--DECLARE @EmailAddress nvarchar(250) = 'owenss@kcic.com'--'srobles@prindlelaw.com'--'Imhoff@VerisantLLC.com'--'Jens.Fog@brandywineholdings.com'--'scharal@nationwide.com'
	SET NOCOUNT ON;

	DECLARE @ClientFirmAddressID bigint = 264 --ITT
	DECLARE @NCCFirmAddressID bigint = 330 --McGuire Woods
	DECLARE @UserFirmAddressID bigint = (SELECT FirmAddressID FROM  vSETTLEMENT_AUTHORITY_Users WHERE Email = @emailaddress)
	DECLARE @CanRespondString nvarchar(MAX)
	IF object_id('tempdb..#InsurerFirmAddresses') IS NOT NULL
	BEGIN
	   DROP TABLE #InsurerFirmAddresses
	END
	CREATE TABLE #InsurerFirmAddresses (InsurerFirmAddressID int)
	INSERT INTO #InsurerFirmAddresses
	SELECT 427
	UNION 
	SELECT 247 

IF ( SELECT COUNT(PersonID)
		FROM    vSETTLEMENT_AUTHORITY_Users
		WHERE  Email = @emailaddress
			AND PersonID IN (3099, 5782, 5783)
	) > 0 
BEGIN
	SET @UserFirmAddressID = 427
END

	DECLARE @WhereString nvarchar(max) = '	WHERE sar.SettlementAuthorityID =' + CONVERT(nvarchar(50), @SettlementAuthorityID) +' AND ParentSettlementAuthorityResponseID IS NULL'
	DECLARE @Field nvarchar(500)
	DECLARE @JoinString nvarchar(max) = ' INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON sar.CreatedBy = sau.Email 
	INNER JOIN tblDefenseCounselAddress dca ON sau.FirmAddressID = dca.DefenseCounselAddressID '

	
	DECLARE @ResponsesWithChildren TABLE (SettlementAuthorityResponseID bigint)
	INSERT INTO @ResponsesWithChildren
	SELECT ParentSettlementAuthorityResponseID AS SettlementResponseID
	FROM tblSettlementAuthorityResponse
	GROUP BY ParentSettlementAuthorityResponseID

	IF object_id('tempdb..#ResponsesWithChildren') IS NOT NULL
	BEGIN
	   DROP TABLE #ResponsesWithChildren
	END
	CREATE TABLE #ResponsesWithChildren (SettlementAuthorityResponseID bigint)
	BEGIN
		INSERT INTO #ResponsesWithChildren
		SELECT ParentSettlementAuthorityResponseID AS SettlementResponseID
		FROM tblSettlementAuthorityResponse
		GROUP BY ParentSettlementAuthorityResponseID
	END

--KCIC & NCC & Client
	IF((@UserFirmAddressID IS NULL AND @Emailaddress IS NULL) 
		OR @Emailaddress LIKE '%@kcicllc.com'
		OR @Emailaddress LIKE '%@kcic.com'
		OR @UserFirmAddressID = @NCCFirmAddressID
		OR @UserFirmAddressID = @ClientFirmAddressID)
	BEGIN
		SET @CanRespondString = '1'

		SET @JoinString = ' LEFT JOIN  vSETTLEMENT_AUTHORITY_Users sau ON sar.CreatedBy = sau.Email
		LEFT JOIN tblDefenseCounselAddress dca ON sau.FirmAddressID = dca.DefenseCounselAddressID '
	END
--Insurers
    ELSE IF (SELECT COUNT(InsurerFirmAddressID) FROM #InsurerFirmAddresses WHERE InsurerFirmAddressID = @UserFirmAddressID) > 0
	BEGIN
		SET @WhereString = @WhereString + '  AND sau.FirmID <> 277
											 AND (sau.FirmID IS NULL OR  sau.firmID NOT IN (SELECT DISTINCT FirmID
																						FROM #InsurerFirmAddresses ifa
																							INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON ifa.InsurerFirmAddressID = sau.FirmAddressID
																						WHERE FirmAddressID <>'+ CONVERT(nvarchar(5), @UserFirmAddressID) +'))'

		SET @CanRespondString = '1'
	END
--LDC (Prindle)
	ELSE IF @UserFirmAddressID = 340	
    BEGIN
		SET @CanRespondString = '1'
    END
--Read only users
	ELSE 
    BEGIN
		SET @CanRespondString = '0'
    END


IF ( SELECT COUNT(PersonID)
		FROM    vSETTLEMENT_AUTHORITY_Users
		WHERE  Email = @emailaddress
			AND PersonID IN (3099, 5782, 5783)
	) > 0 
BEGIN
	SET @CanRespondString = '0'
END
	
DECLARE @QueryString nvarchar(max) = ''

SET @QueryString = @QueryString + '	
	SELECT DISTINCT	sar.CreatedBy,
			sar.DateCreated,
			sar.ParentSettlementAuthorityResponseID,
			sar.SettlementAuthorityID,
			sar.SettlementAuthorityResponseID,
			sar.SettlementAuthorityStatusID,
			REPLACE(sar.Comment, CHAR(13)+CHAR(10), ''<br/>'') AS Comment, --Replace newlines with <br/>s 
			SUM(CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END) OVER (PARTITION BY ParentSettlementAuthorityResponseID) - CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END AS Depth,
			CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END AS HasChildren,
			SettlementAuthorityStatus,
			CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END AS IsParent,
			SUM(CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END) OVER (PARTITION BY ParentSettlementAuthorityResponseID)*2 AS DepthEMs,
			CASE WHEN sau.Email LIKE ''%resolute-midwest.com'' THEN DefenseCounsel + '' - Midwest'' 
														WHEN sau.Email LIKE ''%resolute-ne.com'' THEN DefenseCounsel + '' - New England'' 
														ELSE COALESCE(DefenseCounsel, ''KCIC'') END AS Firm, 
			'
			+ @CanRespondString +
			' AS CanRespond
	FROM tblSettlementAuthorityResponse sar
	INNER JOIN tblSettlementAuthorityStatus sas ON sar.SettlementAuthorityStatusID = sas.SettlementAuthorityStatusID
	LEFT JOIN #ResponsesWithChildren children ON sar.SettlementAuthorityResponseID = children.SettlementAuthorityResponseID
	
	'
		+ @JoinString +
	'
	INNER JOIN tblSettlementAuthorityApproverFirm saaf ON saaf.SettlementAuthorityID = sar.SettlementAuthorityID
	INNER JOIN tblSettlementApproverFirm saf on saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
'
+ @WhereString

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityResponses_OLD] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityResponses_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityResponses_OLD] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityResponses_OLD] TO [power_user]
GO
