SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE PROC [dbo].[WSS_INVOICES_InvoiceSearch]
(@FirmID int, @InsuranceProgramID int, @StatusID bigint, @InvoiceNumber nvarchar(100), @PaidDateStart date, @PaidDateEnd date, @Period nvarchar(50),@emailaddress nvarchar(250))
AS
 
 
 
------VARIABLES FOR TESTING----
--DECLARE @FirmID int
--DECLARE @VendorFirmID int
--DECLARE @StatusID bigint
--DECLARE @PaidDateStart date
--DECLARE @PaidDateEnd date
--DECLARE @Period nvarchar(50)
--DECLARE @emailaddress nvarchar(250)
--declare @InsuranceProgramID int
--declare @InvoiceNumber nvarchar(100)
--SET @FirmID		   = 83
--SET @VendorFirmID  = null
--SET @StatusID	   = NULL
--SET @PaidDateStart = NULL
--SET @PaidDateEnd   = NULL
--SET @Period		   = NULL
--set @InsuranceProgramID = NULL
--set @InvoiceNumber  = '19497152'
--SET @emailaddress  = 'snyderd@kcicllc.com'
 
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE 1=1'
--SET @WhereString = ' WHERE dt.ClaimantDocumentGroupID = 2'
IF @FirmID				IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.FirmID = ' + CONVERT(nvarchar(20),@FirmID) END
IF @InsuranceProgramID	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND dinv.InsuranceProgramID = ' + CONVERT(nvarchar(20),@InsuranceProgramID) END
IF @StatusID			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND dis.StatusID = ' + CONVERT(nvarchar(20),@StatusID) END
IF @InvoiceNumber		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.InvoiceNumber LIKE ''%'' + @InvoiceNumberParam + ''%''' END
IF @PaidDateStart		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND c.Sourcedate >= ' + CONVERT(nvarchar(20),@PaidDateStart) END
IF @PaidDateEnd			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND c.Sourcedate <= ' + CONVERT(nvarchar(20),@PaidDateEnd) END
IF @Period				IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.Period LIKE ''%'' + @PeriodParam + ''%''' END
 
--These IF statements determine additional WHERE statements by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
 
DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)
 
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' '
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND di.FirmID = ' + CONVERT(nvarchar(20),@DefenseCounselID)
END
 
 
DECLARE @QueryString nvarchar(max)
SET @QueryString = '
	SELECT CAST(coalesce(d.DocumentID, 0) AS varchar(50)) AS DocumentId, 
		coalesce(dt.ClaimantDocumentType, '+'''Invoice - No Document'''+') AS ClaimantDocumentType, 
		dg.ClaimantDocumentGroup, 
		d.Comments,
		d.DocumentURL, 
		d.DocumentTitle, 
		d.CreatedBy, 
		d.DateCreated, 
		d.Accepted, 
		d.AcceptedBy, 
		d.DateAccepted, 
		coalesce(di.ApprovedBy, '+'''Historical Invoice'''+') AS ApprovedBy, 
		di.DateApproved, 
		di.DefenseCounsel as Firm,
		di.InvoiceNumber, 
		di.Period,	
		di.DatePayable, 
		di.fees as feeamount, 
		di.costs as disbursementamount, 
		di.adjustments as Adjustment,
		di.InvoiceNotes as AdjustmentNotes, 
		dis.Status, '''' AS DefenseInvoiceFirm, 
		c.SourceDate as PaidDate, 
		c.SourceNumber as CheckNumber, 
		di.FirmID, 
		COALESCE(di.Firm, '''') AS VendorFirm, 
		c.SourceDate, 
		di.InvoiceTotal AS InvoiceTotal,
		InsuranceProgram AS CoverageLine
	FROM  vtblDefenseInvoice di
		LEFT JOIN tblDocuments d 	  ON d.DocumentID = di.DocumentID 
		LEFT JOIN tblClaimantDocumentType dt  ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
		LEFT JOIN tblClaimantDocumentGroup dg ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
		LEFT JOIN tblDefenseInvoice dinv	  ON d.DocumentID = dinv.DocumentID 
		LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID 
		LEFT JOIN tblDefenseCounsel dc		  ON di.FirmID = dc.DefenseCounselID
		LEFT JOIN m_tblVendorFirm ex		  ON di.VendorFirmID = ex.VendorFirmID
		LEFT JOIN tblPaymentDefense pd		  ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN tblCheck c				  ON pd.CheckID = c.CheckID
		LEFT JOIN tblInsuranceProgram ip		ON dinv.InsuranceProgramID = ip.InsuranceProgramID ' +
	@WhereString + '
	ORDER BY DateCreated DESC, Accepted ASC 
	OPTION (RECOMPILE)'
	
Print @QueryString
 
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@PeriodParam nvarchar(max), @InvoiceNumberParam nvarchar(250)', @PeriodParam = @Period, @InvoiceNumberParam = @InvoiceNumber
 
 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_InvoiceSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_InvoiceSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_InvoiceSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_InvoiceSearch] TO [power_user]
GO
