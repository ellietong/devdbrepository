SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] (@SourceDate datetime)

As

SET NOCOUNT ON

--Declare @SourceDate datetime
--Set @SourceDate = '2/15/2013'

Declare @FundingInvoices table(CheckID bigint, DefenseCounselID bigint, Firm nvarchar(250), InvoiceNumber nvarchar(250), 
								InvoiceTotal money, Period date, SubmittedDate date)
Insert into @FundingInvoices
	select  c.CheckID,
			DefenseCounselID as DefenseCounselID,
			DefenseCounsel as Firm,
			di.InvoiceNumber,
			SUM((Fees + Costs) - Adjustments) as InvoiceTotal,
			MAX(DATENAME("mm", di.Period) + ' ' + DATENAME("yy", di.Period)) as Period,
			MAX(SubmitDate) as SubmittedDate
	from tblDefenseInvoice di
		inner join tblDefInvClaimantDetail dicd on di.DocumentID = dicd.DocumentID
		inner join tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		inner join tblCheck c on pd.CheckID = c.CheckID
		Left Join tblDefenseCounselAddress da on di.DefenseCounselAddressID=da.DefenseCounselAddressID
	Where c.SourceDate = @SourceDate
	Group by c.CheckID, DefenseCounselID, DefenseCounsel, di.InvoiceNumber

--MAYBE USED FOR GOULDS??
--Union

--select  c.CheckID,
--		DefenseCounselID as DefenseCounselID,
--		Coalesce(ExpertFirm, DefenseCounsel) as Firm,
--		di.InvoiceNumber as InvoiceNumber,
--		SUM(InvoiceTotal) as InvoiceTotal,
--		MAX(Period),
--		MAX(SubmitDate) as SubmittedDate
--from tblPaymentDefense pd
--inner join tbldefenseinvoice di on pd.DefenseInvoiceID=di.defenseInvoiceID
--inner join tblCheck c on pd.CheckID=c.CheckID
--left Join m_tblExpertFirm ef on ef.ExpertFirmID = di.ExpertFirmID
--Left Join tblDefenseCounselAddress da on di.DefenseCounselAddressID=da.DefenseCounselAddressID
--Where c.SourceDate = @SourceDate
--Group by c.CheckID, DefenseCounselID, Coalesce(ExpertFirm, DefenseCounsel), di.InvoiceNumber

Select f.CheckID, f.Firm, max(f.Period) as Period, max(f.SubmittedDate) as SubmittedDate, f.InvoiceNumber, 
	sum(f.InvoiceTotal) as InvoiceTotal
From @FundingInvoices f
Group BY f.CheckID, f.DefenseCounselID, f.Firm, f.InvoiceNumber
Order By f.CheckID, f.Firm, max(f.Period), f.InvoiceNumber
GO
GRANT ALTER ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT CONTROL ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GO
