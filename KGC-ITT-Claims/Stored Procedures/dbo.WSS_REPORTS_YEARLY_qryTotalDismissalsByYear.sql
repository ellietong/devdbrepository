SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_YEARLY_qryTotalDismissalsByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int,@StartDate datetime=null, @EndDate datetime=null, @emailaddress nvarchar(200) = null) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @YearID int
--Set @YearId=2010


DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)
    -- Insert statements for procedure here
select COUNT(cl.ClaimLawsuitID) as Count
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s ON l.StateID=s.StateID
where statusdate between coalesce(@StartDate,'1/1/1900') and coalesce(@EndDate, '1/1/2100') 
		and ClaimLawsuitStatusID in (15, 16, 17, 27, 28, 19, 34, 35, 37, 41)
		and  IsPrimaryStatus=1

order by COUNT(cl.ClaimLawsuitID) desc

END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryTotalDismissalsByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryTotalDismissalsByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryTotalDismissalsByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryTotalDismissalsByYear] TO [power_user]
GO
