SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryInsEmployment] (@ClaimantPersonalInfoID bigint, 
														 @Jobsite				 nvarchar(250), 
														 @City					 nvarchar(100), 
														 @StateID				 int, 
														 @OccupationID			 int, 
														 @StartDate				 datetime, 
														 @EndDate				 datetime,
														 @IsPrimaryJobsite       bit, 
														 @Emailaddress			 nvarchar(100))
AS

SET NOCOUNT ON

INSERT INTO tblClaimantEmployment (ClaimantPersonalInfoID, Jobsite, OccupationID, JobsiteCity, JobsiteStateID, EmpStartDate, EmpEndDate, IsPrimary, SPUsername)
VALUES (@ClaimantPersonalInfoID, UPPER(@Jobsite), @OccupationID, UPPER(@City), @StateID, @StartDate, @EndDate, @IsPrimaryJobsite, @Emailaddress)

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsEmployment] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsEmployment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsEmployment] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsEmployment] TO [power_user]
GO
