SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:	<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryClaimantDOFE]
-- Add the parameters for the stored procedure here
@ClaimantPersonalInfoID int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


SELECT COALESCE (CONVERT(nvarchar(50), (CASE WHEN CONVERT(nvarchar(50), COALESCE(MIN(empstartdate), MIN(startdate)), 101) IS NULL THEN 'UNKNOWN'
ELSE
(CASE WHEN MIN(startdate) is not null
  THEN CONVERT(nvarchar(50),MIN(startdate), 101) 
  ELSE CONVERT(nvarchar(50),MIN(empstartdate), 101)

  END)
END), 101), 'UNKNOWN') AS DOFE
FROM tblClaimantEmployment ce
LEFT JOIN tblClaimantProduct cp on ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryClaimantDOFE] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryClaimantDOFE] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryClaimantDOFE] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryClaimantDOFE] TO [power_user]
GO
