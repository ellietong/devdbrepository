SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryUpdateSettlementGroup]
	-- Add the parameters for the stored procedure here
(@SettlementGroupName nvarchar(500), @SettlementGroupID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE tblSettlementGroup
	SET SettlementGroup = @SettlementGroupName
	WHERE SettlementGroupID = @SettlementGroupID

	SELECT @SettlementGroupID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdateSettlementGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdateSettlementGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdateSettlementGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdateSettlementGroup] TO [power_user]
GO
