SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryUpdClaim] 
	(	@DiseaseID int, 
		@DoctorID bigint, 
		@DiseaseDiagnosisDate datetime, 
		@LawsuitID bigint, 
		@ActiveTrialDate datetime,
		@PrimaryLawsuit nvarchar(250), 
		@IsMDL nvarchar(250),
		@ClaimID bigint,
		--@ClaimDiseaseID bigint,
		@Username nvarchar(250),
		@TrialGroupID int--, 
		--@Specialty nvarchar(100)
	)

AS



	UPDATE tblLawsuit
	SET SPUsername = @Username
	WHERE LawsuitID = @LawsuitID
	
	--UPDATE tblDoctor
	--SET DoctorNote = @Specialty
	--WHERE DoctorID= @DoctorID	
	
	UPDATE tblClaimLawsuit
	SET ActiveTrialDate = @ActiveTrialDate,
		IsPrimaryForClaim = Case When @PrimaryLawsuit= 'YES' Then 1 else 0 END,
		IsMDL = Case When @IsMDL= 'YES' Then 1 else 0 END,
		TrialGroupID = @TrialGroupID,
		SPUsername = @Username
	WHERE LawsuitID = @LawsuitID AND ClaimID = @ClaimID

	--UPDATE tblClaim
	--SET ILODoctorID = COALESCE(@ILODoctorID, ILODoctorID),
	--	ILOProfusion = COALESCE(@ILOProfusion, ILOProfusion),
	--	ILODate = COALESCE(@ILODate,ILODate),
	--	PFTDoctorID = COALESCE(@PFTDoctorID,PFTDoctorID),
	--	PFTDate = COALESCE(@PFTDate,PFTDate),
	--	PFTTypeID = COALESCE(@PFTTypeID,PFTTypeID),
	--	PFTFVC_Actual = COALESCE(@PFTFVC_Actual,PFTFVC_Actual),
	--	PFTFVC_Predicted = COALESCE(@PFTFVC_Predicted,PFTFVC_Predicted),
	--	PFTFEV1_Actual = COALESCE(@PFTFEV1_Actual,PFTFEV1_Actual),
	--	PFTFEV1_Predicted = COALESCE(@PFTFEV1_Predicted,PFTFEV1_Predicted),
	--	[PFTFEV1andFVC_Actual] = COALESCE(@PFTFEV1FVC_Actual,[PFTFEV1andFVC_Actual]),
	--	[PFTFEV1andFVC_Predicted] = COALESCE(@PFTFEV1FVC_Predicted,[PFTFEV1andFVC_Predicted]),
	--	PFTTLC_Actual = COALESCE(@PFTTLC_Actual,PFTTLC_Actual),
	--	PFTTLC_Predicted = COALESCE(@PFTTLC_Predicted,PFTTLC_Predicted),
	--	PFTDLCO_Actual = COALESCE(@PFTDLCO_Actual,PFTDLCO_Actual),
	--	PFTDLCO_Predicted = COALESCE(@PFTDLCO_Predicted,PFTDLCO_Predicted),
	--	PFTTV = COALESCE(@PFTTV,PFTTV),
	--	SPUsername = @Username
	--WHERE ClaimID = @ClaimID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GO
