SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmClaimantPersonalInfo]
as

/*
Select 	cpi.ClaimantPersonalInfoID, 
	FirstName, 
	MiddleName, 
	LastName, 
	SocialSecurityNumber, 
	DeceasedDate, 
	MaritalStatusID, 
	GenderID, 
	CompleteAddress, 
	BirthDate,
	PrimaryDefenseDisease, 
	PrimaryDefenseDiagnosisDate, 
	PrimaryPlaintiffDisease, 
	PrimaryPlaintiffDiagnosisDate,
	PrimaryDefenseDiseaseID,
	PrimaryPlaintiffDiseaseID,
	cpi.SuffixID,
	Suffix
From mBtblClaimantPersonalInfo cpi 
left join mBtblClaimantSuffix cs on cs.SuffixID = cpi.SuffixID
Order By LastName, FirstName
*/

SELECT *
FROM tblClaimantPersonalInfo
Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantPersonalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantPersonalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantPersonalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantPersonalInfo] TO [power_user]
GO
