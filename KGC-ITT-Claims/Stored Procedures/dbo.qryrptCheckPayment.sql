SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptCheckPayment] (@CheckID int, @CheckAmount money)

As

SET NOCOUNT ON

--Declare @CheckID int
--Declare @CheckAmount money
--Set @CheckAmount = 21180.45
--Set @CheckID = 12726

Declare @FundingTypeID int
Set @FundingTypeID = (select FundingTypeID from tblCheck where CheckID = @checkID)

Declare @CheckApproved bit
Set @CheckApproved = (Select Case when ca.CheckID is null then 0 Else 1 End From tblCheck c Left join tblCheckApproval ca on c.CheckID = ca.CheckID where c.CheckID = @CheckID)

If @FundingTypeID = 1 Begin

	Select c.CheckID, DefenseCounsel as Firm, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
	' End + Coalesce(DefenseCounselAddress2,'
	') + '
	' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip) as FirmAddress, DefenseCounsel as PayTo, SourceNumber as CheckNo, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)) as AmountDesc,
	Coalesce(SourceAmount,@CheckAmount) as CheckAmount, SourceDate as CheckDate, Null as PlaintiffCounselName, Null as PCounselAddress, FundingTypeID, Matter,
	Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
	From tblCheck c
	Inner Join tblMatter m on m.MatterID = c.MatterID
	Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
	Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
	Inner Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
	Where c.CheckID = @CheckID
	Group By c.CheckID, DefenseCounsel, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
	' End + Coalesce(DefenseCounselAddress2,'
	') + '
	' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip), DefenseCounsel, SourceNumber, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)),
	Coalesce(SourceAmount,@CheckAmount), SourceDate, FundingTypeID, Matter

End

If @FundingTypeID = 2 Begin

	Select c.CheckID, 
		Null as Firm, 
		Null as FirmAddress, 
		null As PayTo, 
		SourceNumber as CheckNo, 
		dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)) as AmountDesc,
		Coalesce(SourceAmount,@CheckAmount) as CheckAmount, 
		SourceDate as CheckDate, 
		Case when L.PlaintiffCounselAddressID = 816 then 'Segal McCambridge Client Trust Account' else COALESCE(COALESCE(cpn2.CounselPayeeName, pca2.PlaintiffCounsel), COALESCE(cpn.CounselPayeeName, pca.PlaintiffCounsel)) end as PlaintiffCounselName, 
		CASE WHEN cs.PayToFirmAddressID IS NULL THEN
			pca.PlaintiffCounselAddress1 + Case When pca.PlaintiffCounselAddress2 is null then '' Else ' 
				' End + Coalesce(pca.PlaintiffCounselAddress2,'
				') + '
				' + pca.PlaintiffCounselCity + ', ' + pca.State + ' ' +Convert(nvarchar(10), pca.PlaintiffCounselZip)
			ELSE
			pca2.PlaintiffCounselAddress1 + Case When pca2.PlaintiffCounselAddress2 is null then '' Else ' 
				' End + Coalesce(pca2.PlaintiffCounselAddress2,'
				') + '
				' + pca2.PlaintiffCounselCity + ', ' + pca2.State + ' ' +Convert(nvarchar(10), pca2.PlaintiffCounselZip) end as PCounselAddress, 
		FundingTypeID, 
		Matter,
		Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
	From tblCheck c
		Inner Join tblMatter m                     on m.MatterID = c.MatterID
		Inner Join tblPaymentSettlement ps         on c.CheckID = ps.CheckID
		Inner Join tblClaimantSettlement cs        on cs.ClaimantSettlementID = ps.ClaimantSettlementID
		Inner Join tblClaimLawsuit cl              on cl.ClaimLawsuitID = cs.ClaimLawsuitID
		Inner Join tblLawsuit l                    on l.LawsuitID = cl.LawsuitID
		Inner Join tblPlaintiffCounselAddress pca  on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		Left  Join tblPlaintiffCounselAddress pca2 ON cs.PayToFirmAddressID = pca2.PlaintiffCounselAddressID
		Left  Join tblCounselPayeeName cpn         ON pca.PlaintiffCounselAddressID = cpn.CounselAddressID
		Left  Join tblCounselPayeeName cpn2        on pca2.PlaintiffCounselAddressID = cpn2.CounselAddressID
	Where c.CheckID = @CheckID
	Group By c.CheckID, L.PlaintiffCounselAddressID, SourceNumber, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)), Coalesce(SourceAmount,@CheckAmount),
	SourceDate, COALESCE(COALESCE(cpn2.CounselPayeeName, pca2.PlaintiffCounsel), COALESCE(cpn.CounselPayeeName, pca.PlaintiffCounsel)), CASE WHEN cs.PayToFirmAddressID IS NULL THEN
			pca.PlaintiffCounselAddress1 + Case When pca.PlaintiffCounselAddress2 is null then '' Else ' 
				' End + Coalesce(pca.PlaintiffCounselAddress2,'
				') + '
				' + pca.PlaintiffCounselCity + ', ' + pca.State + ' ' +Convert(nvarchar(10), pca.PlaintiffCounselZip)
			ELSE
			pca2.PlaintiffCounselAddress1 + Case When pca2.PlaintiffCounselAddress2 is null then '' Else ' 
				' End + Coalesce(pca2.PlaintiffCounselAddress2,'
				') + '
				' + pca2.PlaintiffCounselCity + ', ' + pca2.State + ' ' +Convert(nvarchar(10), pca2.PlaintiffCounselZip) end, FundingTypeID, Matter

	End

If @FundingTypeID = 3 Begin

	Select c.CheckID, VendorFirm as Firm, Address1 + Case When Address2 is null then '' Else ' 
	' End + Coalesce(Address2,'
	') + '
	' + City + ', ' +s.State + ' ' +Convert(nvarchar(10),ZipCode) as FirmAddress, VendorFirm as PayTo, SourceNumber as CheckNo, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)) as AmountDesc,
	Coalesce(SourceAmount,@CheckAmount) as CheckAmount, SourceDate as CheckDate, Null as PlaintiffCounselName, Null as PCounselAddress, FundingTypeID, Matter,
	Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
	From tblCheck c
	Inner Join tblMatter m on m.MatterID = c.MatterID
	Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
	Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
	Inner Join m_tblVendorFirm v on v.VendorFirmID=di.VendorFirmID
	left join tblState s on s.StateID=v.StateID
	Where c.CheckID = @CheckID
	Group By c.CheckID, Vendorfirm, Address1 + Case When Address2 is null then '' Else ' 
	' End + Coalesce(Address2,'
	') + '
	' + City + ', ' +s.State + ' ' +Convert(nvarchar(10),ZipCode), VendorFirm, SourceNumber, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)),
	Coalesce(SourceAmount,@CheckAmount), SourceDate, FundingTypeID, Matter

End

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPayment] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckPayment] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPayment] TO [power_user]
GO
