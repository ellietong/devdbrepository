SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryInsDoctor]
(
	@DoctorFName nvarchar(250),
	@DoctorMName nvarchar(250),
	@DoctorLName nvarchar(250),
	@DoctorSpecialty nvarchar(250), @SPUserName nvarchar(250)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblDoctor(DoctorFirstName, DoctorMiddleName, DoctorLastName, DoctorNote)
	SELECT @DoctorFName, @DoctorMName, @DoctorLName, @DoctorSpecialty
		
	SELECT MAX(DoctorID) FROM tblDoctor
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsDoctor] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsDoctor] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsDoctor] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsDoctor] TO [power_user]
GO
