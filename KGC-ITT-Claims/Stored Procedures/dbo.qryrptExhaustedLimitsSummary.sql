SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptExhaustedLimitsSummary]
(@ExhaustionDateID int, @LimitTypeID int, @ExhaustionTypeID int)
As
Set Nocount on

 --Declare @LimitTypeID int
 --Declare @ExhaustionDateID int
 --Declare @ExhaustionTypeID int
 --Set @LimitTypeID = 1
 --Set @ExhaustionDateID = 103
 --Set @ExhaustionTypeID = 1


--Previous Exhaustion
declare @otherexhaustion table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint, OtherExhaustion money)
	Insert Into @otherexhaustion
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation+DefenseAllocation+OutsideConsumption
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<=@ExhaustionDateID and ConsumptionType<>'b'	

Declare @OtherExhaustionTotals table (ExhaustionTypeID int, PolicyDateID int, TotalOtherExhaustion money)
Insert Into @OtherExhaustionTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(OtherExhaustion), 0)
From @otherexhaustion
Group By ExhaustionTypeID, PolicyDateID




Declare @Exhaustion Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint)
Insert Into @Exhaustion
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation,
	DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation,
	 LedgerID
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<=@ExhaustionDateID and ConsumptionType='b'

Declare @PreviousExhaustionTotals table (ExhaustionTypeID int, PolicyDateID int, Indemnity money, Defense money,
	DefenseOutsideLimits money, TotalDefense money, PreviousAmtExcludingDefOutsideLimits money, PreviousAllocationAmt money,
	SIRIndemnityAllocation money, SIRDefenseAllocation money)
Insert Into @PreviousExhaustionTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(IndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(DefenseAllocation), 0) as Defense,
	coalesce(Sum(DefenseOutsideLimitsAllocation),0) as DefenseOutsideLimits,
	coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAmtExcludingDefOutsideLimits,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAllocationAmt,
	coalesce(Sum(SIRIndemnityAllocation), 0) as SIRIndemnity,
	coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense
From @Exhaustion
Group By ExhaustionTypeID, PolicyDateID

--Header Summary
Declare @HeaderSummary Table (ExhaustionType nvarchar(100), Description nvarchar(100), DefenseForm nvarchar(10), HeaderDate nvarchar(50))
Insert into @HeaderSummary
Select ExhaustionType, Description, 'H', Datename(MONTH,exhaustiondate) +' ' + Datename(Year,exhaustiondate) as HeaderDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, Description, 'I', Datename(MONTH,exhaustiondate) +' ' + Datename(Year,exhaustiondate) as HeaderDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, Description, 'G', Datename(MONTH,exhaustiondate) +' ' + Datename(Year,exhaustiondate) as HeaderDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID
union
Select ExhaustionType, Description, '', Datename(MONTH,exhaustiondate) +' ' + Datename(Year,exhaustiondate) as HeaderDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@ExhaustionDateID

Declare @indemnityproducer money
select @indemnityproducer =Indemnity from @PreviousExhaustionTotals PET where  pet.PolicyDateID=128 

Declare @defenseproducer money
select @defenseproducer =Defense from @PreviousExhaustionTotals PET where  pet.PolicyDateID=128 

Declare @defenseoutsidelimitsproducer money
select @defenseoutsidelimitsproducer =DefenseOutsideLimits from @PreviousExhaustionTotals PET where  pet.PolicyDateID=128 

Declare @PreviousAllocationAmtProducer money
select @PreviousAllocationAmtProducer =PreviousAllocationAmt from @PreviousExhaustionTotals PET where  pet.PolicyDateID=128 

Declare @TotalOtherExhaustionProducer money
select @TotalOtherExhaustionProducer =TotalOtherExhaustion from @OtherExhaustionTotals oET where  oet.PolicyDateID=128 


----Query Results
Declare @Results Table(policyid bigint, policydateid bigint, carriername nvarchar(100), layer bigint, policynum nvarchar(100), pstartdate nvarchar(100), penddate nvarchar(100), attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), Indemnity money, Defense money, DefenseOutsideLimits money, TotalDefense money, PreviousAllocationAmt money,
	PreviousAmtExcludingDefOutsideLimits money, TotalOtherExhaustion money, Balance money)
Insert Into @Results

--Most Carriers
select  p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, convert(nvarchar(100), pstartdate, 101) as pstartdate, convert(nvarchar(100), PEndDate, 101)  as penddate,
	pl.attachmentpoint, pl.perocclimit, pl.aggregatelimit,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
	coalesce(PreviousAmtExcludingDefOutsideLimits,0) as PreviousAmtExcludingDefOutsideLimits,
	coalesce(TotalOtherExhaustion,0) as TotalOtherExhaustion,
 	(coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)-coalesce(totalotherexhaustion,0)) as Balance
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	Left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	left join @OtherExhaustionTotals OT on Ot.PolicyDateID=pd.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where limittypeid = @LimitTypeID and InsuranceProgramID = @ExhaustionTypeID and provisionlabelID=1 

Select r.*, @indemnityproducer as indemnityproducer, @defenseproducer as defenseproducer,@defenseoutsidelimitsproducer as defenseoutsidelimitsproducer,  @PreviousAllocationAmtProducer as previousallocationamtproducer, @TotalOtherExhaustionProducer as totalotherexhaustionproducer, ExhaustionType, [Description], HeaderDate
From @Results R
Inner Join @HeaderSummary HS on R.DefenseForm=HS.DefenseForm
where policydateid not in (33)
order by  r.carriername,   cast(r.pstartdate as date) , Cast(r.penddate as DATE),  r.policynum, attachmentpoint, perocclimit
Return




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExhaustedLimitsSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExhaustedLimitsSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExhaustedLimitsSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExhaustedLimitsSummary] TO [power_user]
GO
