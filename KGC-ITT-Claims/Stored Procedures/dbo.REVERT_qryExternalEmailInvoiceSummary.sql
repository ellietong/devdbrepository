SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_qryExternalEmailInvoiceSummary] 
      
AS
BEGIN
/*External Email to Marie Shipley which runs once a week.  The email includes two tables.
The first table is for invoices which have been uploaded within the past week.
The second table is for invoices which are over a week old but she has not yet approved or rejected. - BEH*/

--Declare and set sp_send_dbmail input variables
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(max)

SET @Subject =    'Marley-Wylain: Weekly Invoices Summary ' + CONVERT(varchar, GETDATE()-7, 107) + ' - ' + CONVERT(varchar, GETDATE(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top and the header for table 1
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_solid.PNG" alt="logo"/>'


--Create temp table for invoices which have been uploaded in the past week
DECLARE @NewWeekInvoicesTable TABLE (spDocID nvarchar(100), FirmAddressID int, [status] nvarchar(30), invoiceNum nvarchar(100), 
            period nvarchar(100), fees  money, disbursement money, adjustment money, submitDate datetime, datePayable datetime, DocumentType nvarchar(500))
INSERT INTO @NewWeekInvoicesTable
SELECT SPDocID, 
      --Retrieve the firm's invoice site abbreviation by parsing the documentURL
      --There are two different cases. One for those firms who are categorized as Defense Counsels and 
      --the other for firms who are categorized only as Firms.
      l.DefenseCounselAddressID AS FirmAddressID,
      [Status], d.InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, DateCreated, DatePayable, ClaimantDocumentType
FROM tblDocuments d
      INNER JOIN tblDefenseInvoiceStatus s ON COALESCE(d.StatusID, 3) = s.statusid
      LEFT JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
      LEFT JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalinfoID
      LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
      LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
      LEFT JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID 
WHERE Datecreated BETWEEN GETDATE()-7 AND GETDATE()
      AND cdt.DocumentGroupID = 2
      --This email excludes invoices uploaded by SPX
      AND DefenseCounselAddressID NOT IN (144)
	  AND COALESCE(IsPrimaryForClaim, 0) = 1
      AND COALESCE(IsCurrentClaim, 0) = 1

GROUP BY DocumentURL, SPDocId, [Status], d.InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, 
            DateCreated, DatePayable, l.DefenseCounselAddressID, ClaimantDocumentType

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
Set @BodyMessage = @BodyMessage + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {font-family: Goudy Old Style; 
      font-size: 10pt; 
      border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @bodymessage + COALESCE('<H4>The following invoices have been uploaded since ' + CONVERT(varchar, GETDATE()-7, 107) + '.</H4>'+ N'<table>' + 
    N'<tr><th>Firm</th><th>Invoice Type</th><th>Invoice Number</th><th>Period</th><th>Fees</th>' + 
      '<th>Expenses</th><th>Total Billed</th><th>Submit Date</th><th>Date Payable</th>' +
    CAST ( (
            SELECT  td = DefenseCounsel, '', td = DocumentType, '',td = invoicenum, '', td = period, '', 
                        td = '$'+convert(nvarchar(30), coalesce(fees, 0), 1), '', 
                        td = '$'+convert(nvarchar(30), coalesce(disbursement, 0), 1), '', 
                        td = '$'+convert(nvarchar(30),(coalesce(fees, 0)+coalesce(disbursement, 0)), 1), '', 
                        td = convert(nvarchar(100), submitdate, 101), '', 
                        td = convert(nvarchar(100), datepayable, 101), ''
            FROM @NewWeekInvoicesTable i
                  INNER JOIN tblDefenseCounselAddress f ON f.DefenseCounselAddressID = i.FirmAddressID
                  ORDER BY DefenseCounsel,  submitdate
            for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
      N'</table>' +  N'<br/>', '<H4>No invoices have been uploaded since ' + CONVERT(varchar, GETDATE()-7, 107) + '.</H4>')


--Create temp table for invoices over a week old which have not yet been approved or rejected
DECLARE @OlderThanWeekInvoicesTable TABLE (spdocid nvarchar(100), FirmAddressID int, invoicenum nvarchar(100), 
            period nvarchar(100), fees  money, disbursement money, adjustment money, submitdate datetime, datepayable datetime, documenttype nvarchar (500))
INSERT INTO @OlderThanWeekInvoicesTable
SELECT SPDocID, 
      --Retrieve the firm's invoice site abbreviation by parsing the documentURL
      --There are two different cases. One for those firms who are categorized as Defense Counsels and 
      --the other for firms who are categorized only as Firms.
      l.DefenseCounselAddressID as FirmAddressID,
      d.InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, DateCreated, DatePayable, claimantdocumenttype
FROM tblDocuments d
      INNER JOIN tblDefenseInvoiceStatus s ON COALESCE(d.StatusID, 3) = s.statusid
      LEFT JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
      LEFT JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalinfoID
      LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
      LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
      LEFT JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID 
WHERE Datecreated < GETDATE()-7 
      AND DocumentGroupID = 2
      AND d.StatusID = 3
      --This email excludes invoices uploaded by SPX
      AND DefenseCounselAddressID NOT IN (144)
	  AND COALESCE(IsPrimaryForClaim, 0) = 1
      AND COALESCE(IsCurrentClaim, 0) = 1
GROUP BY DocumentURL, SPDocId, d.InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, DateCreated, DatePayable, l.DefenseCounselAddressID, ClaimantDocumentType

if (select COUNT(spdocid) from @OlderThanWeekInvoicesTable) >0 
BEGIN
--Add header for table 2
--SET @BodyMessage = @BodyMessage+ '<H4>The following invoices were uploaded before ' + CONVERT(varchar, GETDATE()-7, 107) + ' and remain unapproved.</H4>'


--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @bodymessage + COALESCE('<H4>The following invoices were uploaded before ' + CONVERT(varchar, GETDATE()-7, 107) + ' and remain unapproved.</H4>'+ N'<table>' + 
    N'<tr><th>Firm</th><th>Invoice Type</th><th>Invoice Number</th><th>Period</th><th>Fees</th>' + 
            '<th>Expenses</th><th>Total Billed</th><th>Submit Date</th><th>Date Payable</th>' +
    CAST ( (
            SELECT  td = DefenseCounsel, '', td = documenttype, '', td = invoicenum, '', td = period, '', 
                        td = '$'+convert(nvarchar(30), coalesce(fees, 0), 1), '', 
                        td = '$'+convert(nvarchar(30), coalesce(disbursement, 0), 1), '', 
                        td = '$'+convert(nvarchar(30),(coalesce(fees, 0)+coalesce(disbursement, 0)), 1), '', 
                        td = convert(nvarchar(100), submitdate, 101), '', 
                        td = convert(nvarchar(100), datepayable, 101), ''
            FROM @OlderThanWeekInvoicesTable i
                  INNER JOIN tblDefenseCounselAddress f ON f.DefenseCounselAddressID = i.FirmAddressID
            ORDER BY DefenseCounsel, submitdate
            for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
    N'</table>' +  N'<br/>',  '<H4>No unapproved invoices before ' + CONVERT(varchar, GETDATE()-7, 107) + '.</H4>')

end
--Include link to invoice roll up site at the bottom
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/marleywylain/cp/Pages/Invoices.aspx"> here</A> to access these invoices.</b><br><br>'

--Include KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br><b>KCIC, LLC<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</b></font>'

--Execute email-sending stored procedure
EXEC msdb.dbo.sp_send_dbmail
--The Demo ClientProfile sends from Demo Client@kcicllc.com
@profile_name='MarleyProfile',
@recipients = 'gary.mckenna@spx.com',
@copy_recipients = 'mwc@kcicllc.com',
@subject= @subject,
@body= @BodyMessage,
@body_format = 'html'
      
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailInvoiceSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailInvoiceSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryExternalEmailInvoiceSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryExternalEmailInvoiceSummary] TO [power_user]
GO
