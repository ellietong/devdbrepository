SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_DOCUMENT_PROFILE_qryDocument]
    (
      @DocID INT ,
      @emailaddress NVARCHAR(250)
    )
AS 
----Variables used for testing.----
--DECLARE @DocID int
--DECLARE @emailaddress nvarchar(250)
--SET @DocID = 243372--185935
--SET @emailaddress = 'owenss@kcic.com'
 
--GET LIST OF CLAIMANTS FOR DOCUMENT
    DECLARE @FirmID INT
    SET @FirmID = ( SELECT  FirmID
                    FROM    vtblPerson
                    WHERE   email = @emailaddress
                  )
 
    DECLARE @Claimants NVARCHAR(MAX)
    SET @Claimants = '' 
 
    DECLARE @SettleDocs NVARCHAR(MAX)
    SET @SettleDocs = '' 
 
    DECLARE @addClaimant NVARCHAR(MAX)
    DECLARE @addClaimantID INT
 
 
    DECLARE @DocCat NVARCHAR(MAX)
    DECLARE @PageNum NVARCHAR(MAX)
 
    DECLARE @FirmAddressID INT
    SET @FirmAddressID = ( SELECT   FirmAddressID
                           FROM     vtblPerson
                           WHERE    Email = @emailaddress
                         )
 
    DECLARE @WhereString NVARCHAR(MAX)
    SET @WhereString = ' WHERE 1 = 1'
    IF @DocID IS NOT NULL 
        BEGIN
            SET @WhereString = @WhereString + ' AND d.DocumentID = '
                + CONVERT(NVARCHAR(20), @DocID)
        END	

	DECLARE @JoinString nvarchar(max)


DECLARE @DocGroupID int = (SELECT ClaimantDocumentGroupID 
						   FROM tblDocuments d
						   INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID  
						   WHERE DocumentID = @DocID)

IF @DocGroupID = 2
BEGIN 
	SET @JoinString = ' LEFT JOIN tblDefenseJurisdiction dj	ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.matterID'
END	
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.matterID'		
END

 
-----------------------------------------------------------------
 
    DECLARE Cur_SettleDoc CURSOR Local Fast_Forward
    FOR
        SELECT  DocCategory ,
                PageNums
        FROM    tblSettleDocs sd
                LEFT JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
        WHERE   DocumentID = @DocID
 
--Open Docket Cursor
    OPEN Cur_SettleDoc
 
--Fetch Data
    FETCH NEXT FROM Cur_SettleDoc 
	  INTO @DocCat, @PageNum
 
    WHILE @@Fetch_Status = 0 
        BEGIN
      --assemble the string
            IF LEN(@SettleDocs) = 0 
                SET @SettleDocs = COALESCE(@DocCat, '')
                    + '&nbsp;&nbsp;pg(s): ' + COALESCE(@PageNum, '')
            ELSE 
                SET @SettleDocs = @SettleDocs + '<br />' + COALESCE(@DocCat,
                                                              '')
                    + '&nbsp;&nbsp;pg(s): ' + COALESCE(@PageNum, '')
            PRINT @SettleDocs
 
 
            FETCH NEXT FROM Cur_SettleDoc 
      INTO @DocCat, @PageNum
      
        END
 
 
    CLOSE Cur_SettleDoc 
    DEALLOCATE Cur_SettleDoc
 
------------------------------------------------------------------------------------------------------------------
 
    BEGIN
        DECLARE @QueryString NVARCHAR(MAX)
        SET @QueryString = '
SELECT DISTINCT
	CAST(d.DocumentID as varchar(50)) as DocumentId,
	dt.ClaimantDocumentType, 
	dg.ClaimantDocumentGroup, 
	d.Comments,
	d.DocumentURL,
	d.DocumentTitle,
	--d.SPDocId,
	d.CreatedBy,
	d.DateCreated,
	d.ModifiedBy,
	d.DateModified,
	d.Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	ApprovedBy,
	DateApproved,
	f.Firm,
	(SELECT TOP 100 ''/clients/ITT/ittcp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='' + CONVERT(nvarchar, mcd.ClaimantPersonalINfoID) AS ''a/@href'', '''' + LastName + '', '' + FirstName + '''' as a, ''<br/>''
		FROM    tblMultiClaimantDocument mcd
				LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		WHERE   mcd.DocumentID = '
                + CONVERT(NVARCHAR(20), @DocID) + '
		GROUP BY mcd.Claimantpersonalinfoid ,
				LastName + '', '' + FirstName + '' (''
				+ CONVERT(NVARCHAR(10), mcd.ClaimantPersonalInfoID)
				+ '')'' ,
				LastName ,
				FirstName
		ORDER BY LastName
	FOR XML PATH('''')) AS Claimants,	
	CASE WHEN LEN(@SettleDocs)=0 THEN ''N/A'' ELSE COALESCE(@SettleDocs, ''N/A'') END AS SettleDocs,
	d.DocumentID,
	--d.DismissalDate,
	d.QCDate AS DismissalProcessDate,
	ClaimLawsuitStatus AS DismissalType,
	DocumentSource,
	d.DocumentSourceID,
	NULL as AutoAccept,
	di.InvoiceNumber,
	di.Period,
	di.DatePayable,
	di.fees as FeeAmount,
	di.costs as DisbursementAmount,
	di.adjustments as Adjustment,
	di.InvoiceNotes as AdjustmentNotes,
	dis.Status, 
	COALESCE(ef.VendorFirm, f.Firm) VendorFirm, 
	di.SubmitDate,
	'''' as PrivateComments,
	Coalesce(IsFullyPaid,0) as IsFullyPaid
FROM tblDocuments d
	LEFT OUTER JOIN tblClaimantDocumentType dt			ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
	LEFT OUTER JOIN tblClaimantDocumentGroup dg			ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
	LEFT OUTER JOIN tblMultiClaimantDocument mcd		ON d.DocumentID = mcd.DocumentID
	LEFT OUTER JOIN tblDefInvClaimantDetail dicd		ON d.DocumentID = dicd.DocumentID
	LEFT OUTER JOIN tblClaimantPersonalInfo cpi			ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID OR dicd.ClaimantPersonalInfOID = cpi.ClaimantpersonalInfoID
	LEFT OUTER JOIN tblClaim c							ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT OUTER JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
	LEFT OUTER JOIN tblLawsuit l						ON cl.lawsuitid = l.lawsuitid
	LEFT OUTER JOIN tblJurisdiction j					ON l.jurisdictionid = j.jurisdictionid' +
	@JoinString +'
	LEFT OUTER JOIN tblState s							ON l.stateid = s.stateid	
	LEFT OUTER JOIN tblDefenseCounselAddress dca		ON l.defensecounseladdressid = dca.defensecounseladdressid 
	LEFT OUTER JOIN vtblDefenseInvoice di				ON d.DocumentID = di.DocumentID 
	LEFT OUTER JOIN tblDefenseInvoiceStatus dis			ON di.StatusID = dis.StatusID 
	LEFT OUTER JOIN tblPaymentDefense pd				ON di.DefenseInvoiceID=pd.DefenseInvoiceID
	LEFT OUTER JOIN m_tblVendorFirm ef					ON dca.DefenseCounselID = ef.VendorFirmID 
	LEFT OUTER JOIN vtblFirm f							ON dca.DefenseCounselID = f.FirmID
	LEFT OUTER JOIN tblClaimantDocketNumbers cdn		ON mcd.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
	LEFT OUTER JOIN tblPlaintiffCounselAddress pca		ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	LEFT OUTER JOIN tblDocumentSource ds				ON d.DocumentSourceID = ds.DocumentSourceID
	LEFT OUTER JOIN tblClaimLawsuitStatusTracking clst	ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT OUTER JOIN tblClaimLawsuitStatus cls			ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID'
            + @WhereString + '
OPTION (RECOMPILE)'
 
    END
    --Print @querystring
    
    EXEC sp_executesql @QueryString,
        N'@Claimants nvarchar(max), @SettleDocs nvarchar(MAX)',
        @Claimants = @Claimants, @SettleDocs = @SettleDocs
    RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryDocument] TO [power_user]
GO
