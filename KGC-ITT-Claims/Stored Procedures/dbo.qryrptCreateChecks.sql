SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptCreateChecks]
    (
      @CheckID INT = NULL ,
      @SourceDate DATE = NULL
      
    )
AS 
    SET NOCOUNT ON

--Declare @CheckID int
--Declare @SourceDate DATETIME
--Set @SourceDate = '7/21/2014' 
--Set @CheckID = 3242
-- DECLARE @FundingType INT = 2

-- If you are only viewing a single check, bring back check data based on CheckID
IF @CheckID =0


    DECLARE @FundingTypeID INT
    SET @FundingTypeID = ( SELECT   FundingTypeID
                           FROM     tblCheck
                           WHERE    CheckID = @checkID
                         )

    DECLARE @CheckApproved BIT
    SET @CheckApproved = ( SELECT   CASE WHEN ca.CheckID IS NULL THEN 0
                                         ELSE 1
                                    END
                           FROM     tblCheck c
                                    LEFT JOIN tblCheckApproval ca ON c.CheckID = ca.CheckID
                           WHERE    c.CheckID = @CheckID
                         )
	-- Calculate CheckAmount rather than using another input parameter
	DECLARE @CheckAmount TABLE
    (
      CheckID INT ,
      CheckAmount MONEY
    )	INSERT  INTO @CheckAmount
        SELECT  CheckID ,
                SUM(PaymentAmount)
        FROM    tblPaymentDefense
        WHERE   CheckID =@CheckID
        GROUP BY CheckID
        
        UNION
        
        SELECT  CheckID ,
                SUM(PaymentAmount)
        FROM    tblPaymentSettlement
        WHERE   CheckID =@CheckID
        GROUP BY CheckID


    IF @FundingTypeID = 1 
        BEGIN

            SELECT  c.CheckID ,
                    DefenseCounsel AS Firm ,
                    DefenseCounselAddress1
                    + CASE WHEN DefenseCounselAddress2 IS NULL THEN ''
                           ELSE ' 
	'
                      END + COALESCE(DefenseCounselAddress2, '
	') + '
	' + DefenseCounselCity + ', ' + dca.State + ' '
                    + CONVERT(NVARCHAR(10), DefenseCounselZip) AS FirmAddress ,
                    DefenseCounsel AS PayTo ,
                    SourceNumber AS CheckNo ,
                    dbo.fnCurrencyToWords(SourceAmount) AS AmountDesc ,
                    SourceAmount AS CheckAmount ,
                    SourceDate AS CheckDate ,
                    NULL AS PlaintiffCounselName ,
                    NULL AS PCounselAddress ,
                    FundingTypeID ,
                    Matter ,
                    CASE WHEN @CheckApproved = 1 THEN ''
                         WHEN @CheckApproved = 0 THEN 'VOID'
                    END AS CheckStatus
            FROM    tblCheck c
                    INNER JOIN tblMatter m ON m.MatterID = c.MatterID
                    INNER JOIN tblPaymentDefense pd ON c.CheckID = pd.CheckID
                    INNER JOIN tblDefenseInvoice di ON di.DefenseInvoiceID = pd.DefenseInvoiceID
                    INNER JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselAddressID = di.DefenseCounselAddressID
            WHERE   c.CheckID = @CheckID
            GROUP BY c.CheckID ,
                    DefenseCounsel ,
                    DefenseCounselAddress1
                    + CASE WHEN DefenseCounselAddress2 IS NULL THEN ''
                           ELSE ' 
	'
                      END + COALESCE(DefenseCounselAddress2, '
	') + '
	' + DefenseCounselCity + ', ' + dca.State + ' '
                    + CONVERT(NVARCHAR(10), DefenseCounselZip) ,
                    DefenseCounsel ,
                    SourceNumber ,
                    dbo.fnCurrencyToWords(SourceAmount) ,
                    SourceAmount ,
                    SourceDate ,
                    FundingTypeID ,
                    Matter

        END

    IF @FundingTypeID = 2 
        BEGIN

            SELECT  c.CheckID ,
                    pca.PlaintiffCounsel AS Firm ,
                    pca.PlaintiffCounselAddress1 + Case When pca.PlaintiffCounselAddress2 is null then '' Else ' 
							' End + Coalesce(pca.PlaintiffCounselAddress2,'
							') + '
							' + pca.PlaintiffCounselCity + ', ' + pca.State + ' ' +Convert(nvarchar(10), pca.PlaintiffCounselZip)
					 AS FirmAddress ,
                     pca.PlaintiffCounsel +' TRUST ACCOUNT' AS PayTo ,
                    SourceNumber AS CheckNo ,
        dbo.fnCurrencyToWords(COALESCE(CheckAmount, 0)) AS AmountDesc ,
        COALESCE(CheckAmount, 0) AS CheckAmount ,
                    SourceDate AS CheckDate ,
                    pca.PlaintiffCounsel, 
					pca.PlaintiffCounselAddress1 + Case When pca.PlaintiffCounselAddress2 is null then '' Else ' 
							' End + Coalesce(pca.PlaintiffCounselAddress2,'
							') + '
							' + pca.PlaintiffCounselCity + ', ' + pca.State + ' ' +Convert(nvarchar(10), pca.PlaintiffCounselZip)
						 as PCounselAddress, 
                    FundingTypeID ,
                    Matter ,
                    CASE WHEN @CheckApproved = 1 THEN ''
                         WHEN @CheckApproved = 0 THEN 'VOID'
                    END AS CheckStatus
            FROM    tblCheck c
                    INNER JOIN tblMatter m ON m.MatterID = c.MatterID
                    INNER JOIN tblPaymentSettlement ps ON c.CheckID = ps.CheckID
                    INNER JOIN tblClaimantSettlement cs ON cs.ClaimantSettlementID = ps.ClaimantSettlementID
                    INNER JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
                    INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
                    INNER JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
                    LEFT  JOIN tblPlaintiffCounselAddress pca2 ON cs.PayToFirmAddressID = pca2.PlaintiffCounselAddressID
					Left  Join tblCounselPayeeName cpn         ON pca.PlaintiffCounselAddressID = cpn.CounselAddressID
					Left  Join tblCounselPayeeName cpn2        on pca2.PlaintiffCounselAddressID = cpn2.CounselAddressID
					LEFT JOIN @CheckAmount CA ON CA.CheckID = C.CheckID
            WHERE   c.CheckID = @CheckID
			Group By c.CheckID ,
                    SourceNumber,
                    dbo.fnCurrencyToWords(COALESCE(CheckAmount, 0)),
        COALESCE(CheckAmount, 0),
                    SourceDate,
                    pca.PlaintiffCounsel, 
					pca.PlaintiffCounselAddress1 + Case When pca.PlaintiffCounselAddress2 is null then '' Else ' 
							' End + Coalesce(pca.PlaintiffCounselAddress2,'
							') + '
							' + pca.PlaintiffCounselCity + ', ' + pca.State + ' ' +Convert(nvarchar(10), pca.PlaintiffCounselZip), 
                    FundingTypeID ,
                    Matter

        END

    IF @FundingTypeID = 3 
        BEGIN

            SELECT  c.CheckID ,
                    VendorFirm AS Firm ,
                    Address1 + CASE WHEN Address2 IS NULL THEN ''
                                    ELSE ' 
	'
                               END + COALESCE(Address2, '
	') + '
	' + City + ', ' + s.State + ' ' + CONVERT(NVARCHAR(10), ZipCode) AS FirmAddress ,
                    VendorFirm AS PayTo ,
                    SourceNumber AS CheckNo ,
                    dbo.fnCurrencyToWords(SourceAmount) AS AmountDesc ,
                    SourceAmount AS CheckAmount ,
                    SourceDate AS CheckDate ,
                    NULL AS PlaintiffCounselName ,
                    NULL AS PCounselAddress ,
                    FundingTypeID ,
                    Matter ,
                    CASE WHEN @CheckApproved = 1 THEN ''
                         WHEN @CheckApproved = 0 THEN 'VOID'
                    END AS CheckStatus
            FROM    tblCheck c
                    INNER JOIN tblMatter m ON m.MatterID = c.MatterID
                    INNER JOIN tblPaymentDefense pd ON c.CheckID = pd.CheckID
                    INNER JOIN tblDefenseInvoice di ON di.DefenseInvoiceID = pd.DefenseInvoiceID
                    INNER JOIN m_tblVendorFirm v ON v.VendorFirmID = di.VendorFirmID
                    LEFT JOIN tblState s ON s.StateID = v.StateID
            WHERE   c.CheckID = @CheckID
            GROUP BY c.CheckID ,
                    Vendorfirm ,
                    Address1 + CASE WHEN Address2 IS NULL THEN ''
                                    ELSE ' 
	'
                               END + COALESCE(Address2, '
	') + '
	' + City + ', ' + s.State + ' ' + CONVERT(NVARCHAR(10), ZipCode) ,
                    VendorFirm ,
                    SourceNumber ,
                    dbo.fnCurrencyToWords(SourceAmount) ,
                    SourceAmount ,
                    SourceDate ,
                    FundingTypeID ,
                    Matter

        END
        
        
--If trying to generate multiple checks, bring back data based on source date
ELSE IF @SourceDate IS NOT NULL

DECLARE @CheckAmountMulti TABLE
    (
      CheckID INT ,
      CheckAmount MONEY
    )
INSERT  INTO @CheckAmountMulti
        SELECT  CheckID ,
                SUM(PaymentAmount)
        FROM    tblPaymentDefense
        WHERE   CheckID IN ( SELECT CheckID
                             FROM   tblCheck
                             WHERE  SourceDate = @SourceDate )
        GROUP BY CheckID
        UNION
        SELECT  CheckID ,
                SUM(PaymentAmount)
        FROM    tblPaymentSettlement
        WHERE   CheckID IN ( SELECT CheckID
                             FROM   tblCheck
                             WHERE  SourceDate = @SourceDate )
        GROUP BY CheckID

DECLARE @CheckFundingLink TABLE
    (
      CheckID INT ,
      FundingTypeID INT ,
      CheckApproved BIT
    )
INSERT  INTO @CheckFundingLink
        SELECT  c.CheckID ,
                FundingTypeID ,
                CASE WHEN ca.CheckID IS NULL THEN 0
                     WHEN ca.CheckID IS NOT NULL THEN 1
                END
        FROM    tblCheck c
                LEFT JOIN tblCheckApproval ca ON c.CheckID = ca.CheckID
        WHERE   SourceDate = @SourceDate --AND FundingTypeID = @FundingType

SELECT  c.CheckID ,
        pca.PlaintiffCounsel AS Firm ,
        pca.PlaintiffCounselAddress1 + Case When pca.PlaintiffCounselAddress2 is null then '' Else ' 
							' End + Coalesce(pca.PlaintiffCounselAddress2,'
							') + '
							' + pca.PlaintiffCounselCity + ', ' + pca.State + ' ' +Convert(nvarchar(10), pca.PlaintiffCounselZip)
						AS FirmAddress ,
        SourceNumber AS CheckNo ,
        dbo.fnCurrencyToWords(COALESCE(CheckAmount, 0)) AS AmountDesc ,
        COALESCE(CheckAmount, 0) AS CheckAmount ,
        SourceDate AS CheckDate ,
        c.FundingTypeID ,
        Matter ,
        CASE WHEN CheckApproved = 1 THEN ''
             WHEN CheckApproved = 0 THEN 'VOID'
        END AS CheckStatus ,
        '' AS InvoiceDetail ,
        pca.PlaintiffCounsel +' TRUST ACCOUNT' AS PayTo
FROM    tblCheck c
        INNER JOIN @CheckFundingLink cfl ON c.CheckID = cfl.CheckID
        INNER JOIN tblMatter m ON m.MatterID = c.MatterID
        INNER JOIN tblPaymentSettlement ps ON c.CheckID = ps.CheckID
        INNER JOIN tblClaimantSettlement cs ON cs.ClaimantSettlementID = ps.ClaimantSettlementID
        INNER JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
        INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
        INNER JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		LEFT  JOIN tblPlaintiffCounselAddress pca2 ON cs.PayToFirmAddressID = pca2.PlaintiffCounselAddressID
		Left  Join tblCounselPayeeName cpn         ON pca.PlaintiffCounselAddressID = cpn.CounselAddressID
		Left  Join tblCounselPayeeName cpn2        on pca2.PlaintiffCounselAddressID = cpn2.CounselAddressID
        LEFT JOIN @CheckAmountMulti ca ON ca.CheckID = c.CheckID
WHERE   cfl.FundingTypeID = 2
GROUP BY c.CheckID ,
        SourceNumber ,
        dbo.fnCurrencyToWords(COALESCE(CheckAmount, 0)) ,
        COALESCE(CheckAmount, 0) ,
        SourceDate ,
        pca.PlaintiffCounsel,
        pca.PlaintiffCounselAddress1 + Case When pca.PlaintiffCounselAddress2 is null then '' Else ' 
							' End + Coalesce(pca.PlaintiffCounselAddress2,'
							') + '
							' + pca.PlaintiffCounselCity + ', ' + pca.State + ' ' +Convert(nvarchar(10), pca.PlaintiffCounselZip)
						,
        c.FundingTypeID ,
        Matter ,
        CASE WHEN CheckApproved = 1 THEN ''
             WHEN CheckApproved = 0 THEN 'VOID'
        END
UNION
SELECT  c.CheckID ,
        DefenseCounsel AS Firm ,
        DefenseCounselAddress1
        + CASE WHEN DefenseCounselAddress2 IS NULL THEN ''
               ELSE ' 
	'
          END + COALESCE(DefenseCounselAddress2, '
	') + '
	' + DefenseCounselCity + ', ' + dca.State + ' '
        + CONVERT(NVARCHAR(10), DefenseCounselZip) AS FirmAddress ,
        SourceNumber AS CheckNo ,
        dbo.fnCurrencyToWords(COALESCE(CheckAmount, 0)) AS AmountDesc ,
        COALESCE(CheckAmount, 0) AS CheckAmount ,
        SourceDate AS CheckDate ,
        c.FundingTypeID ,
        Matter ,
        CASE WHEN CheckApproved = 1 THEN ''
             WHEN CheckApproved = 0 THEN 'VOID'
        END AS CheckStatus , --convert(nvarchar(100), COUNT(pd.DefenseInvoiceID)) + 
        'Invoices Totaling ' AS InvoiceDetail ,
        DefenseCounsel AS PayTo
FROM    tblCheck c
        INNER JOIN @CheckFundingLink cfl ON c.CheckID = cfl.CheckID
        INNER JOIN tblMatter m ON m.MatterID = c.MatterID
        INNER JOIN tblPaymentDefense pd ON c.CheckID = pd.CheckID
        INNER JOIN tblDefenseInvoice di ON di.DefenseInvoiceID = pd.DefenseInvoiceID
        INNER JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselAddressID = di.DefenseCounselAddressID
        LEFT JOIN @CheckAmountMulti ca ON ca.CheckID = c.CheckID
WHERE   cfl.FundingTypeID = 1
GROUP BY c.CheckID ,
        DefenseCounsel ,
        DefenseCounselAddress1
        + CASE WHEN DefenseCounselAddress2 IS NULL THEN ''
               ELSE ' 
	'
          END + COALESCE(DefenseCounselAddress2, '
	') + '
	' + DefenseCounselCity + ', ' + dca.State + ' '
        + CONVERT(NVARCHAR(10), DefenseCounselZip) ,
        SourceNumber ,
        dbo.fnCurrencyToWords(COALESCE(CheckAmount, 0)) ,
        COALESCE(CheckAmount, 0) ,
        SourceDate ,
        c.FundingTypeID ,
        Matter ,
        CASE WHEN CheckApproved = 1 THEN ''
             WHEN CheckApproved = 0 THEN 'VOID'
        END


    RETURN

GO
GRANT ALTER ON  [dbo].[qryrptCreateChecks] TO [base_user]
GRANT CONTROL ON  [dbo].[qryrptCreateChecks] TO [base_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qryrptCreateChecks] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCreateChecks] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCreateChecks] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptCreateChecks] TO [power_user]
GRANT CONTROL ON  [dbo].[qryrptCreateChecks] TO [power_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qryrptCreateChecks] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCreateChecks] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCreateChecks] TO [power_user]
GO
