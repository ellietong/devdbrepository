SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryNotificationContactList]
AS
BEGIN
	
		SELECT NULL AS SettlementNotificationContactID, NULL as Contact
		UNION
		SELECT	SettlementNotificationContactID, 
				LName + COALESCE(', ' + FName, '') + ' - ' + Email + ' (' + dc.DefenseCounsel + ')' AS Contact
		FROM tblSettlementNotificationContact snc
		INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON snc.PersonID = sau.PersonID
		LEFT JOIN tblDefenseCounsel dc ON sau.FirmID = dc.DefenseCounselID
		ORDER BY Contact
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryNotificationContactList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryNotificationContactList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryNotificationContactList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryNotificationContactList] TO [power_user]
GO
