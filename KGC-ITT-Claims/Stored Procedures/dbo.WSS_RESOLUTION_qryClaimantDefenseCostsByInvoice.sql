SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice]



-- Add the parameters for the stored procedure here

(@ClaimantPersonalInfoID int, @DefenseCounselID nvarchar (250), @VendorFirmID int, @CurrentURL nvarchar (100))
AS
BEGIN


--DECLARE @ClaimantPersonalInfoID bigint
--DECLARE @DefenseCounselID int
--DECLARE @VendorFirmID int

--SET @ClaimantPersonalInfoID = 30661
--SET @DefenseCounselID = 138
--SET @VendorFirmID = 0
---- SET NOCOUNT ON added to prevent extra result sets from

-- interfering with SELECT statements.

SET NOCOUNT ON;


----Approved Expert and General Invoices with a ClaimantID link
SELECT
	CPI.ClaimantPersonalInfoID,
	di.VendorFirmID AS DefenseCounselID,
	Di.InvoiceNumber AS InvoiceNumber,
	SUM(COALESCE(Costs, 0) + COALESCE(Fees, 0) - COALESCE(Adjustments, 0)) AS DefenseTotal,
	CASE
		WHEN IsFullyPaid = 1 THEN 'Paid' ELSE Status
	END AS Status,
	SourceDate,
	@CurrentURL + '/clients/ITT/ittcp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), di.DocumentID) + '&source=' + '/clients/ITT/ittcp/Pages/AuditList.aspx' AS DocumentURL
FROM tblClaimantPersonalInfo cpi
LEFT JOIN tblDefInvClaimantDetail DICD ON cpi.ClaimantPersonalInfoId = DICD.ClaimantpersonalInfoID
LEFT JOIN tblDefenseInvoice di ON DICD.DocumentID = DI.DocumentID
LEFT JOIN tblDefenseInvoiceStatus ds ON di.StatusID = ds.StatusID
LEFT JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
LEFT JOIN tblCheck c ON pd.CheckID = c.CheckID
WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
AND DI.StatusID IN (1, 2, 6)
AND di.VendorFirmID = @VendorFirmID
AND DefenseCounselAddressID = @DefenseCounselID
GROUP BY	CPI.ClaimantPersonalInfoID,
			di.VendorFirmID,
			Di.InvoiceNumber,
			CASE
				WHEN IsFullyPaid = 1 THEN 'Paid' ELSE Status
			END,
			SourceDate,
			@CurrentURL + '/clients/ITT/ittcp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), di.DocumentID) + '&source=' + '/clients/ITT/ittcp/Pages/AuditList.aspx'
HAVING SUM(COALESCE(Costs, 0) + COALESCE(Fees, 0) - COALESCE(Adjustments, 0)) <> 0
ORDER BY SourceDate DESC
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [power_user]
GO
