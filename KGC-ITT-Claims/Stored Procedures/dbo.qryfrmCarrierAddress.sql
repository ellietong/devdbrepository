SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[qryfrmCarrierAddress]    Script Date: 08/04/2009 17:28:27 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
CREATE Procedure [dbo].[qryfrmCarrierAddress]
(@addressid int =null)

As
SET NOCOUNT ON

SELECT *
FROM dbo.tblcarrieraddress 
Where addressid = @addressid

RETURN












GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierAddress] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierAddress] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierAddress] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierAddress] TO [power_user]
GO
