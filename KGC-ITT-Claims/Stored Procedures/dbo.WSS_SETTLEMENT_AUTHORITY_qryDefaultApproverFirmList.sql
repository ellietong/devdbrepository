SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDefaultApproverFirmList]
AS
BEGIN
	SET NOCOUNT ON;
    SELECT	saf.*,
			DefenseCounsel AS Firm
    FROM tblSettlementApproverFirm saf
    INNER JOIN tblDefenseCounsel dc ON saf.FirmID = dc.DefenseCounselID
	WHERE IsActive = 1
		AND SettlementApproverFirmID <> 2
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDefaultApproverFirmList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDefaultApproverFirmList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDefaultApproverFirmList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDefaultApproverFirmList] TO [power_user]
GO
