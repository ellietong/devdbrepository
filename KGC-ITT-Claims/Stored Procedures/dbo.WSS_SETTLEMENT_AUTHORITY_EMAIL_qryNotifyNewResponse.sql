SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse] 
(@SettlementAuthorityResponseID bigint)
AS
SET NOCOUNT ON

BEGIN 

--DECLARE @SettlementAuthorityResponseID int = 34

DECLARE @SettlementAuthorityID bigint = (SELECT SettlementAuthorityID FROM tblSettlementAuthorityResponse WHERE SettlementAuthorityResponseID = @SettlementAuthorityResponseID)

--Declare and set email inputs.
Declare @Subject nvarchar(MAX)
Declare @LogoHeader nvarchar(MAX)


DECLARE @SettlementGroupID int = (SELECT sa.SettlementGroupID
										FROM tblSettlementAuthority sa
										WHERE sa.SettlementAuthorityID = @SettlementAuthorityID)

DECLARE @SettlementGroupInfo NVARCHAR(MAX) = (
SELECT FirstName + ' ' + LastName + ' - ' + PlaintiffCounsel + ' (' + COALESCE(jurisdiction, 'UNKNOWN') + ', '  + COALESCE(s.State, 'UNKNOWN') + ') ' AS Claimant
FROM tblSettlementAuthority sa 
	INNER JOIN tblClaimantSettlement cs ON sa.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblSettlementGroup sg ON cs.SettlementGroupID = sg.SettlementGroupID
	INNER JOIN tblClaimLawsuit cl on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblLawsuit l on cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaim c on cl.ClaimID = c.ClaimID
	INNER JOIN tblPlaintiffCounselAddress pca on l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	INNER JOIN tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblState s on l.StateID = s.StateID
	LEFT JOIN tblJurisdiction j on l.JurisdictionID = j.JurisdictionID
WHERE SettlementAuthorityID = @SettlementAuthorityID
)

--DECLARE @ParentFirmAddressID bigint = ( SELECT FirmAddressID 
--										FROM tblSettlementAuthorityResponse sar
--											INNER JOIN tblSettlementAuthorityResponse sarParent ON sar.ParentSettlementAuthorityResponseID = sarParent.SettlementAuthorityResponseID
--											INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON sarParent.CreatedBy = sau.Email
--										WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID)
										
SET @Subject = 	'ITT Settlement Authority Response ' + CONVERT(varchar, getdate(), 107) + ': ' + COALESCE(@SettlementGroupInfo,'') 

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'
DECLARE @Status  nvarchar(100) 
DECLARE @Comment nvarchar(MAX)

SELECT @Status = COALESCE(SettlementAuthorityStatus, 'Unknown Status'), @Comment = COALESCE(sar.Comment, 'No comment.')
FROM tblSettlementAuthorityResponse sar 
	INNER JOIN tblSettlementAuthorityStatus sas ON sar.SettlementAuthoritystatusID = sas.SettlementAuthorityStatusID 
WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID

--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
SET @BodyMessage = @BodyMessage + '<H4>' + (SELECT CASE WHEN sau.Email LIKE '%resolute-midwest.com' THEN DefenseCounsel + ' - Midwest' 
														WHEN sau.Email LIKE '%resolute-ne.com'		THEN DefenseCounsel + ' - New England' 
																									ELSE COALESCE(dca.DefenseCounsel, 'KCIC') END
											FROM tblSettlementAuthority sa
												LEFT JOIN tblSettlementAuthorityResponse sar  ON sa.SettlementAuthorityID = sar.SettlementAuthorityID
												LEFT JOIN  vSETTLEMENT_AUTHORITY_Users sau on sar.CreatedBy = sau.Email
												LEFT JOIN tblDefenseCounselAddress dca ON sau.FirmAddressID = dca.DefenseCounselAddressID
											WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID) +  
	' has posted the following response with status ' + @Status + ':<br/><blockquote>' + @Comment + '</blockquote>' +
	'To view additional information regarding this authority request click ' + 
	'<a href="https://www.kcicextranet.com/clients/ITT/ittcp/Pages/SettlementAuthority.aspx?SettlementAuthorityID=' + CONVERT(nvarchar, @SettlementAuthorityID) + '">here</a>. </H4>'

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + 
'
    <table style="width: 500px;" border="0" cellpadding="4" cellspacing="5" width="500">
        <tbody>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    <span style="color: #F06021; font-weight: bold;">KCIC Claims Administration</span><br />
                    </td>
            </tr>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    <img src="http://kcic.com/i/kcic-logo.png" width="103" height="93" />
                </td>
            </tr>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    733 10th Street, NW | Suite 3001 | Washington, DC 20001
                <br />
                    <a href="mailto:Claims@kcic.com" style="color: #737271;">Claims@kcic.com</a>
                    |<span style="font-family: Arial, Helvetica, sans-serif; color: #F06021"> </span>202.772.2300
                    | <a href="http://www.kcic.com" target="_new"
                        style="color: #FF6229">kcic.com</a>
                </td>
            </tr>
        </tbody>
    </table>
'

DECLARE @RecipientList nvarchar(500)
SET @RecipientList = COALESCE(( SELECT DISTINCT Email + '; ' AS [text()]
								FROM  ( SELECT COALESCE(sarParent.CreatedBy, sa.AuthorityRequestedBy) AS Email
										FROM tblSettlementAuthorityResponse sar
											LEFT  JOIN tblSettlementAuthorityResponse sarParent ON sar.ParentSettlementAuthorityResponseID = sarParent.SettlementAuthorityResponseID
											INNER JOIN tblSettlementAuthority sa ON sar.SettlementAuthorityID = sa.SettlementAuthorityID
										WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID
										UNION
										SELECT NULLIF(sa.AuthorityRequestedBy, sarParent.CreatedBy) AS Email
										FROM tblSettlementAuthorityResponse sar
											LEFT  JOIN tblSettlementAuthorityResponse sarParent ON sar.ParentSettlementAuthorityResponseID = sarParent.SettlementAuthorityResponseID
											INNER JOIN tblSettlementAuthority sa ON sar.SettlementAuthorityID = sa.SettlementAuthorityID
										WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID
										UNION
										SELECT CASE WHEN sarParent.CreatedBy = 'Jens.Fog@brandywineholdings.com' THEN 'Susan.Eisenlohr@brandywineholdings.com' END AS Email
										FROM tblSettlementAuthorityResponse sar
											LEFT  JOIN tblSettlementAuthorityResponse sarParent ON sar.ParentSettlementAuthorityResponseID = sarParent.SettlementAuthorityResponseID
											INNER JOIN tblSettlementAuthority sa ON sar.SettlementAuthorityID = sa.SettlementAuthorityID
										WHERE sar.SettlementAuthorityResponseID = @SettlementAuthorityResponseID
										UNION SELECT 'loree.shelko@itt.com'
										UNION SELECT 'Craig.Johnson@itt.com') cc
								For XML PATH ('')), ' ')

--DECLARE @CCList nvarchar(MAX) = ''

SET @RecipientList = @RecipientList + COALESCE((SELECT DISTINCT sau.Email + '; ' AS [text()]
												From tblSettlementAuthorityNotificationContact sanc
													INNER JOIN tblSettlementNotificationContact snc ON sanc.SettlementNotificationContactID = snc.SettlementNotificationContactID
													INNER JOIN vSETTLEMENT_AUTHORITY_Users sau ON snc.PersonID = sau.PersonID
												Where sanc.SettlementAuthorityID = @SettlementAuthorityID
												For XML PATH ('')), ' ')

----QC Recipient List
--Set @Bodymessage = @BodyMessage + '<h4>Recipients ' + @RecipientList + '</br>CC: ' + @CCList +'</h4>'
--SELECT @RecipientList
--SELECT @CCList	
--PRINT @Bodymessage
--SELECT @Subject

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
	@profile_name='Claims',
	--@recipients = 'youngc@kcic.com; owenss@kcic.com',
	@recipients = @RecipientList,
	@copy_recipients = '',
	@blind_copy_recipients = 'itt@kcic.com',
	@subject = @subject,
	@body = @BodyMessage,
	@body_format = 'html'

IF (SELECT COUNT(COALESCE(@SettlementAuthorityResponseID, 0))) > 0
BEGIN
	UPDATE tblSettlementAuthorityResponse
	SET ResponseNotificationDate = GETDATE()
	WHERE SettlementAuthorityResponseID = @SettlementAuthorityResponseID
END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse] TO [power_user]
GO
