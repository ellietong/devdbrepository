SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryUpdSettlementShare]
(@SettlementShareID int, @InsuranceProgramID bigint,  @SettlementShare decimal(5, 2))--, @ClaimLawsuitID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	  
  
UPDATE SS
SET SettlementShare = @SettlementShare,
	InsuranceProgramID = @InsuranceProgramID
FROM tblSettlementShare ss
WHERE SettlementShareID = @SettlementShareID
  	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlementShare] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlementShare] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlementShare] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlementShare] TO [power_user]
GO
