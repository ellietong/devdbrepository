SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByPlaintiffsCounselByYear] 
				(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null, @emailaddress nvarchar(200) = null) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @MatterID int =null
--Declare @StartDate datetime = null
--Declare @EndDate datetime = null

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


select coalesce(pca.PlaintiffCounsel, 'Unknown') as PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
--inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
Left join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
where	coalesce(FileDate,'1/1/1900')between coalesce(@StartDate,'1/1/1900')
		and coalesce(@EndDate, '1/1/2100')
group by pca.PlaintiffCounsel
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, coalesce(pca.PlaintiffCounsel, 'Unknown')-- as PlaintiffCounsel


END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByPlaintiffsCounselByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByPlaintiffsCounselByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByPlaintiffsCounselByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByPlaintiffsCounselByYear] TO [power_user]
GO
