SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverFirmList] (@emailaddress nvarchar(250))
AS
BEGIN
	SET NOCOUNT ON;

	IF object_id('tempdb..#InsurerFirm') IS NOT NULL
	BEGIN
	   DROP TABLE #InsurerFirm
	END
	CREATE TABLE #InsurerFirm (InsurerFirmID int)
	INSERT INTO #InsurerFirm
	SELECT 63
	UNION 
	SELECT 155 

	DECLARE @WhereString nvarchar(max) = ' WHERE IsActive = 1'	

	DECLARE @QueryString nvarchar(max)

	DECLARE @UserFirmID bigint = (SELECT FirmID FROM  vSETTLEMENT_AUTHORITY_Users WHERE Email = @emailaddress)

IF (SELECT COUNT(InsurerFirmID) FROM #InsurerFirm WHERE InsurerFirmID = @UserFirmID) > 0
	BEGIN
		SET @WhereString = @WhereString + ' AND saf.FirmID = ' + CONVERT(nvarchar, @UserFirmID)
	END

	SET @QueryString = 'SELECT NULL AS FirmID, NULL as Firm
						UNION
						SELECT DISTINCT dc.DefenseCounselID AS FirmID, dc.DefenseCounsel AS Firm
						FROM tblSettlementApproverFirm saf
						INNER JOIN tblDefenseCounsel dc ON saf.FirmID = dc.DefenseCounselID'
						+ @WhereString + '		
						ORDER BY Firm'

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverFirmList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverFirmList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverFirmList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverFirmList] TO [power_user]
GO
