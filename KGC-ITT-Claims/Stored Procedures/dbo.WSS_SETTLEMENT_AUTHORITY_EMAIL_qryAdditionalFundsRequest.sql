SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryAdditionalFundsRequest] 
(@SettlementAuthorityID bigint)
AS

SET NOCOUNT ON
---SET XACT_ABORT ON
BEGIN
--DECLARE @SettlementAuthorityID bigint = 109


/* Run once an request for additional funds is submitted.
*/
--SET ANSI_WARNINGS ONFF
--Declare and set email inputs.
Declare @Subject nvarchar(MAX)
Declare @LogoHeader nvarchar(MAX)

DECLARE @SettlementGroup NVARCHAR(MAX) = (SELECT SettlementGroup
										FROM tblSettlementAuthority sa
											INNER JOIN tblSettlementGroup sg on sa.SettlementGroupID = sg.SettlementGroupID
										WHERE sa.SettlementAuthorityID = @SettlementAuthorityID)

DECLARE @SettlementGroupID int = (SELECT sa.SettlementGroupID
										FROM tblSettlementAuthority sa
										WHERE sa.SettlementAuthorityID = @SettlementAuthorityID)

DECLARE @SettlementGroupInfo NVARCHAR(MAX) = (
SELECT FirstName + ' ' + LastName + ' - ' + PlaintiffCounsel + ' (' + COALESCE(jurisdiction, 'UNKNOWN') + ', '  + COALESCE(s.State, 'UNKNOWN') + ') ' AS Claimant
FROM tblSettlementAuthority sa 
	INNER JOIN tblClaimantSettlement cs ON sa.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblSettlementGroup sg ON cs.SettlementGroupID = sg.SettlementGroupID
	INNER JOIN tblClaimLawsuit cl on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblLawsuit l on cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaim c on cl.ClaimID = c.ClaimID
	INNER JOIN tblPlaintiffCounselAddress pca on l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	INNER JOIN tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblState s on l.StateID = s.StateID
	LEFT JOIN tblJurisdiction j on l.JurisdictionID = j.JurisdictionID
WHERE SettlementAuthorityID = @SettlementAuthorityID
)

DECLARE @ParentSettlementAuthorityID bigint = (SELECT ParentSettlementAuthorityID FROM tblSettlementAuthority WHERE SettlementAuthorityID = @SettlementAuthorityID)

SET @Subject = 	'ITT Settlement Authority Additional Funds Requested ' + CONVERT(varchar, getdate(), 107) + ': ' + COALESCE(@SettlementGroupInfo,'') 


--PRINT @Subject
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


DECLARE @newLine nvarchar(5) = CHAR(13) + CHAR(10)

DECLARE @MainHeaderStyle nvarchar(MAX) = 'background-color: #0079C1; font-weight: bold; color: white; padding: 4px;'
DECLARE @tdStyle nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'

DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar(max))
DECLARE @ClaimantList nvarchar(max)
--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)


--Print SettlementAuthority table first

--Recursively create response thread
DECLARE @out nvarchar(MAX)
EXEC	[dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryPrintAllResponses]
		@SettlementAuthorityID = @ParentSettlementAuthorityID,
		@out = @out OUTPUT

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
padding:0;}	
--->
</STYLE>'

DECLARE @NewAuthorityRequest TABLE (SettlementAuthorityID int,
AuthorityRequesteDate datetime,
SettlementGroup nvarchar (max),
DemandAmount money,
AuthorityAmount money,
Comment nvarchar (max),
ClaimantList nvarchar (max),
ResponseDueDate datetime
)
INSERT INTO @NewAuthorityRequest --columns to populate the email table
	SELECT DISTINCT
		sa.SettlementAuthorityID,
		sa.AuthorityRequestedDate,
		SettlementGroup,	
		sa.DemandAmount,
		sa.AuthorityAmount,
		REPLACE(sa.Comment, CHAR(13) + CHAR(10), '<br/>') AS Comment, --Replace newlines with <br/>s  
		ClaimantList,
		ResponseDueDate AS ResponseDueDate 

	FROM tblSettlementAuthority sa
	INNER JOIN tblClaimantSettlement cs ON sa.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblSettlementGroup sg ON cs.SettlementGroupID = sg.SettlementGroupID
	INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblDocuments d ON sa.DocumentID = d.DocumentID
	INNER JOIN  vSETTLEMENT_AUTHORITY_ClaimantLists claimants ON sa.SettlementGroupID = claimants.SettlementGroupID
	LEFT JOIN  vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Unfiltered fra ON sa.SettlementAuthorityID = fra.SettlementAuthorityID
	WHERE sa.SettlementAuthorityID = @SettlementAuthorityID --AND COALESCE(IsRequestApproved, 0) = 1
	ORDER BY AuthorityRequestedDate DESC

set @BodyMessage = @BodyMessage + '<H4>Additional funds have been requested for the following settlement group.</H4>'

DECLARE @NewSettlementAuthorityID int,
@AuthorityRequesteDate datetime,
@NewSettlementGroup nvarchar (max),
@DemandAmount money,
@AuthorityAmount money,
@NewClaimantList nvarchar (max),
@ResponseDueDate datetime

SET @BodyMessage = @BodyMessage + '<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">'
SET @BodyMessage = @BodyMessage + N'<tr><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Settlement Group</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Linked Claimants</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Response<br/> Due Date</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Additional Demand Amount</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Additional Authority Amount</th>'


DECLARE Doc_List CURSOR FOR SELECT DISTINCT
	AuthorityRequesteDate,
	SettlementGroup,
	DemandAmount,
	AuthorityAmount,
	ClaimantList,
	ResponseDueDate
FROM @NewAuthorityRequest r
ORDER BY ResponseDueDate

OPEN Doc_List
FETCH NEXT FROM Doc_List INTO @AuthorityRequesteDate, @SettlementGroup, @DemandAmount, @AuthorityAmount, @ClaimantList, @ResponseDueDate
While @@FETCH_STATUS = 0

BEGIN


SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @SettlementGroup + ' </td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @ClaimantList + ' </td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + CONVERT(nvarchar(20), @ResponseDueDate, 101) + ' </td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + '$' + CONVERT(nvarchar(20), @DemandAmount, 1) + ' </td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + '$' + CONVERT(nvarchar(20), @AuthorityAmount, 1) + ' </td>'

FETCH NEXT FROM Doc_List INTO @AuthorityRequesteDate, @SettlementGroup, @DemandAmount, @AuthorityAmount, @ClaimantList, @ResponseDueDate


END
CLOSE Doc_List
DEALLOCATE Doc_List


SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'



set @BodyMessage = @BodyMessage + '<H4>Original Settlement Authority Request:  </H4>'

SET @BodyMessage = @BodyMessage + '
	<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
		<tr>
			<th style="' + @MainHeaderStyle + '" Colspan="2">Settlement Authority</th>
		</tr>
		'
SET @BodyMessage = @BodyMessage + (
SELECT (select [td/@style] = @tdStyle, field as 'td' for xml path(''), type), (select [td/@style] = @tdStyle, Information as 'td' for xml path(''), type)
from (
select Field, Information
from (
		SELECT DISTINCT 
			    CONVERT(nvarchar(max),SettlementGroup) AS [Settlement Group],
			    CONVERT(nvarchar(max), sa.Comment) AS Comments,
			  '$ ' + CONVERT(nvarchar(max), DemandAmount, 1) AS [Demand Amount],
			  '$ ' + CONVERT(nvarchar(max), AuthorityAmount, 1) AS [Authority Amount],
			   CONVERT(nvarchar(max), [status]) AS [Status],
			   CONVERT(nvarchar(max), AuthorityRequestedDate, 101) AS [Authority Requested Date],
			   CONVERT(nvarchar(max), AuthorityRequestedBy) AS [Authority Requested By]
		FROM tblSettlementAuthority sa
		INNER JOIN tblClaimantSettlement cs ON sa.ClaimLawsuitID = cs.ClaimLawsuitID
		INNER JOIN tblSettlementGroup sg ON cs.SettlementGroupID = sg.SettlementGroupID
		LEFT JOIN  vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Unfiltered fra ON sa.SettlementAuthorityID = fra.SettlementAuthorityID
		WHERE sa.SettlementAuthorityID = @ParentSettlementAuthorityID
		GROUP BY	sg.SettlementGroup,
					sa.Comment, 
					CONVERT(nvarchar(max), DemandAmount, 1),
					CONVERT(nvarchar(max), AuthorityAmount, 1),
					[Status],
					CONVERT(nvarchar(max), AuthorityRequestedDate, 101),
					AuthorityRequestedBy
) ss
unpivot (Information FOR Field in ( [settlement group], comments, [demand amount], [Authority Amount], [Status], [Authority Requested Date], [Authority Requested By]
	   )) as unpv ) tbl FOR XML PATH('tr'))

SET @BodyMessage = @BodyMessage + '
	</tr>
</table>
<br/>
<br/>'

SET @BodyMessage = @BodyMessage + '
<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
	<tr>
		<td colspan="7">' + COALESCE(@out, '') + '
		</td>
	</tr>
</table>'
SET @out = ''

SET @BodyMessage = @BodyMessage + '<br/><H4>To view additional information regarding this authority request click ' + 
	'<a href="https://www.kcicextranet.com/clients/ITT/ittcp/Pages/SettlementAuthority.aspx?SettlementAuthorityID=' + CONVERT(nvarchar, @SettlementAuthorityID) + '">here</a>. </H4>'



--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')


--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + 
'
    <table style="width: 500px;" border="0" cellpadding="4" cellspacing="5" width="500">
        <tbody>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    <span style="color: #F06021; font-weight: bold;">KCIC Claims Administration</span><br />
                    </td>
            </tr>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    <img src="http://kcic.com/i/kcic-logo.png" width="103" height="93" />
                </td>
            </tr>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    733 10th Street, NW | Suite 3001 | Washington, DC 20001
                <br />
                    <a href="mailto:Claims@kcic.com" style="color: #737271;">Claims@kcic.com</a>
                    |<span style="font-family: Arial, Helvetica, sans-serif; color: #F06021"> </span>202.772.2300
                    | <a href="http://www.kcic.com" target="_new"
                        style="color: #FF6229">kcic.com</a>
                </td>
            </tr>
        </tbody>
    </table>
'
 
--Grab list of distinct email addresses of settlement approve contacts
DECLARE @RecipientList nvarchar(MAX)
SET @RecipientList=(
SELECT DISTINCT Email + '; ' AS [text()]
            FROM tblSettlementAuthorityApproverFirm saaf
				INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID				
				LEFT JOIN (
				SELECT DISTINCT sau.PersonID, Email, SettlementAuthorityID
				FROM tblSettlementAuthority sa
					INNER JOIN tblClaimantSettlement cs ON sa.ClaimLawsuitID = cs.ClaimLawsuitID
					INNER JOIN tblSettlementGroup sg ON cs.SettlementGroupID = sg.SettlementGroupID					
					INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
					INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID			 		
					INNER JOIN tblSRAContact sc on l.StateID = sc.StateID
					INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON sc.PersonID = sau.PersonID
				WHERE sa.SettlementAuthorityID = @SettlementAuthorityID) SRA ON saaf.SettlementAuthorityID = SRA.SettlementAuthorityID
            For XML PATH (''))
--SELECT @RecipientList
--Bring in notification contacts
SET @RecipientList = @RecipientList + COALESCE((SELECT DISTINCT sau.Email + '; ' AS [text()]
            From tblSettlementAuthorityNotificationContact sanc
            INNER JOIN tblSettlementNotificationContact snc ON sanc.SettlementNotificationContactID = snc.SettlementNotificationContactID
            INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON snc.PersonID = sau.PersonID
            Where sanc.SettlementAuthorityID = @SettlementAuthorityID
            For XML PATH ('')), ' ')
  --SELECT @RecipientList          

SET @RecipientList = @RecipientList + (SELECT AuthorityRequestedBy FROM tblSettlementAuthority WHERE SettlementAuthorityID = @SettlementAuthorityID)

DECLARE @CCList nvarchar(MAX) = 'loree.shelko@itt.com; Craig.Johnson@itt.com;'
--QC Recipient List
Set @Bodymessage = @BodyMessage + 'Recipients: ' + @RecipientList  + '</br>CC: ' + @CCList

--PRINT SUBSTRING(@BodyMessage, 0, 4000)
--PRINT SUBSTRING(@BodyMessage, 4000, 8000)

--SELECT @BodyMessage


----Execute stored procedure to send the email to that firm
--EXEC msdb.dbo.sp_send_dbmail
--@profile_name='Claims',
--@recipients = @RecipientList,
--@copy_recipients = @CCList,
--@blind_copy_recipients = 'itt@kcic.com',
--@subject = @subject,
--@body = @BodyMessage,
--@body_format = 'html'


--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
@profile_name='Claims',
@recipients = 'snyderd@kcic.com',
@copy_recipients = '',
@blind_copy_recipients = '',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'


	IF (SELECT COUNT(COALESCE(@SettlementAuthorityID, 0))) > 0
	BEGIN
		UPDATE tblSettlementAuthority
		SET RequestNotificationDate = GETDATE()
		WHERE SettlementAuthorityID = @SettlementAuthorityID
	END

SET ANSI_WARNINGS ON
--SET XACT_ABORT OFF
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryAdditionalFundsRequest] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryAdditionalFundsRequest] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryAdditionalFundsRequest] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryAdditionalFundsRequest] TO [power_user]
GO
