SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverFirmList]
AS
BEGIN
	
		--SELECT NULL AS FirmID, NULL as Firm
		--UNION
		--SELECT DISTINCT dc.DefenseCounselID AS FirmID, dc.DefenseCounsel AS Firm
		--FROM tblSettlementApproverFirm saf
		--INNER JOIN tblDefenseCounsel dc ON saf.FirmID = dc.DefenseCounselID
		----WHERE IsDefault = 1
		--ORDER BY Firm

		SELECT NULL AS SettlementApproverFirmID, NULL as SettlementApproverFirm
		UNION
		SELECT DISTINCT SettlementApproverFirmID, dc.DefenseCounsel AS SettlementApproverFirm
		FROM tblSettlementApproverFirm saf
		INNER JOIN tblDefenseCounsel dc ON saf.FirmID = dc.DefenseCounselID
		--WHERE IsDefault = 1
		ORDER BY SettlementApproverFirm
END	




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverFirmList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverFirmList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverFirmList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryApproverFirmList] TO [power_user]
GO
