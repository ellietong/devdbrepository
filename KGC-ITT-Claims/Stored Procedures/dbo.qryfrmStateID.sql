SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmStateID]

as
Select StateID, FullNameState
From tblState
Order By FullNameState

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmStateID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmStateID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmStateID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmStateID] TO [power_user]
GO
