SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryDocumentSourceList]
AS
SET NOCOUNT ON

SELECT DocumentSourceID, 
	DocumentSource 
FROM tblDocumentSource
UNION
SELECT NULL, '-- Select a Document Source --' 
ORDER BY DocumentSource

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentSourceList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentSourceList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentSourceList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentSourceList] TO [power_user]
GO
