SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_AmazonDownload]
	
AS
BEGIN
Select DISTINCT d.DocumentID, AmazonKey, COALESCE(LastName, '') + '_' + COALESCE(FirstName, '') + '_' +  COALESCE(CONVERT(nvarchar, c.ClaimantPersonalInfoID), '') + '_' + CONVERT(nvarchar, d.DocumentID) AS ClaimantDocumentType
FROM tblClaimLawsuit cl
INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblMultiClaimantDocument mcd ON c.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
INNER JOIN tblDocuments d ON mcd.DocumentID = d.DocumentID
INNER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE clst.ClaimLawsuitStatusID = 22 AND IsPrimaryStatus = 1
AND d.ClaimantDocumentTypeID = 8
AND a.DefendantCompanyID IN (422,467,468)
--ORDER BY COALESCE(LastName, '') + '_' + COALESCE(FirstName, '') + '_' +  COALESCE(CONVERT(nvarchar, c.ClaimantPersonalInfoID), '')
END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonDownload] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_AmazonDownload] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonDownload] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_AmazonDownload] TO [power_user]
GO
