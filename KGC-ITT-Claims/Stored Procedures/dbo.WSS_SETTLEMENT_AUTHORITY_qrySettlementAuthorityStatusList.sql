SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityStatusList] (@emailaddress nvarchar(250))
AS
BEGIN
	--DECLARE @EmailAddress nvarchar(250) = 'gmacsteel@mcguirewoods.com'--'loree.shelko@itt.com'--'owenss@kcicllc.com'--'Jens.Fog@brandywineholdings.com'--'scharal@nationwide.com'

	SET NOCOUNT ON;

	DECLARE @ClientFirmAddressID bigint = 264 --ITT
	DECLARE @NCCFirmAddressID bigint = 330 --McGuire Woods
	DECLARE @UserFirmAddressID bigint = (SELECT FirmAddressID FROM  vSETTLEMENT_AUTHORITY_Users WHERE Email = @emailaddress)
	DECLARE @UserFirmID bigint = (SELECT FirmID FROM  vSETTLEMENT_AUTHORITY_Users WHERE Email = @emailaddress)
	IF object_id('tempdb..#InsurerFirmAddresses') IS NOT NULL
	BEGIN
	   DROP TABLE #InsurerFirmAddresses
	END
	CREATE TABLE #InsurerFirmAddresses (InsurerFirmAddressID int)
	INSERT INTO #InsurerFirmAddresses
	SELECT 427
	UNION 
	SELECT 247 

	DECLARE @WhereString nvarchar(max) = ' WHERE 1=1'	

--KCIC & NCC & Client
	IF((@UserFirmAddressID IS NULL AND @Emailaddress IS NULL) 
		OR @Emailaddress LIKE '%@kcicllc.com'
		OR @Emailaddress LIKE '%@kcic.com'
		OR @UserFirmAddressID = @NCCFirmAddressID)
	BEGIN
		SET @WhereString = @WhereString + ' AND SettlementAuthorityStatusID IN (4)'
	END
	ELSE IF(@UserFirmAddressID = @ClientFirmAddressID)
	BEGIN
		SET @WhereString = @WhereString + ' AND SettlementAuthorityStatusID <> 5'
	END
--Insurers
    ELSE IF (SELECT COUNT(InsurerFirmAddressID) FROM #InsurerFirmAddresses WHERE InsurerFirmAddressID = @UserFirmAddressID) > 0
	BEGIN
		SET @WhereString = @WhereString + ' AND SettlementAuthorityStatusID <> 5'
	END
--LDC
	ELSE
    BEGIN
		SET @WhereString = @WhereString + ' AND SettlementAuthorityStatusID IN (4)'
    END
 
 DECLARE @QueryString nvarchar(max) --= ''

 SET @QueryString = '
 	SELECT * 
	FROM tblSettlementAuthorityStatus'
	+ @WhereString + '
	UNION
	SELECT NULL, NULL
	ORDER BY SettlementAuthorityStatus'

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityStatusList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityStatusList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityStatusList] TO [power_user]
GO
