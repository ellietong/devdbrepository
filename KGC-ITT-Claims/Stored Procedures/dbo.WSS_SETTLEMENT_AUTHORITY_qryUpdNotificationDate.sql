SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryUpdNotificationDate] 
AS
BEGIN

--Executed in External ITT Settlement Authority 9AM, 12PM, 2PM, 4PM Alerts job
--Fills in the Request Notification Date for the emails (notifications to counsel, contacts, ITT) that were sent out in the three previous steps of the job
--Note that if the counsel email step fails the entire job stops, preventing these dates from being erroneously filled in

DECLARE @NewAuthorityRequest TABLE (SettlementAuthorityID int,
									AuthorityRequesteDate datetime,
									SettlementGroup nvarchar (max),
									DemandAmount money,
									AuthorityAmount money,
									Comment nvarchar (max),
									ClaimantList nvarchar (max),
									ResponseDueDate datetime)
INSERT INTO @NewAuthorityRequest
	SELECT DISTINCT	sa.SettlementAuthorityID,
		sa.AuthorityRequestedDate,
		SettlementGroup,	
		sa.DemandAmount,
		sa.AuthorityAmount,
		REPLACE(sa.Comment, CHAR(13) + CHAR(10), '<br/>') AS Comment, --Replace newlines with <br/>s  
		ClaimantList,
		ResponseDueDate AS ResponseDueDate 
	FROM tblSettlementAuthority sa
		INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
		INNER JOIN tblClaimantSettlement cs ON sg.SettlementGroupID = cs.SettlementGroupID
		INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN tblDocuments d ON sa.DocumentID = d.DocumentID
		INNER JOIN  vSETTLEMENT_AUTHORITY_ClaimantLists claimants ON sa.SettlementGroupID = claimants.SettlementGroupID
		LEFT JOIN  vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Unfiltered fra ON sa.SettlementAuthorityID = fra.SettlementAuthorityID
	WHERE ParentSettlementAuthorityID IS NULL
		AND RequestNotificationDate IS NULL
	ORDER BY AuthorityRequestedDate DESC

UPDATE tblSettlementAuthority
	SET	RequestNotificationDate = GETDATE()
	WHERE SettlementAuthorityID IN (SELECT SettlementAuthorityID FROM @NewAuthorityRequest)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryUpdNotificationDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryUpdNotificationDate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryUpdNotificationDate] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryUpdNotificationDate] TO [power_user]
GO
