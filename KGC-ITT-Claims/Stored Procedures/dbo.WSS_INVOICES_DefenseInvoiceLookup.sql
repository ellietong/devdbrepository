SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_INVOICES_DefenseInvoiceLookup]
AS
SET NOCOUNT ON

SELECT statusid, status
FROM tblDefenseInvoiceStatus
where StatusID <>5
UNION
	--SELECT null as StatusID, '<< All Status >>' As status
	--UNION
	SELECT null, '<<Any Status>>'
order by Status
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_DefenseInvoiceLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_DefenseInvoiceLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_DefenseInvoiceLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_DefenseInvoiceLookup] TO [power_user]
GO
