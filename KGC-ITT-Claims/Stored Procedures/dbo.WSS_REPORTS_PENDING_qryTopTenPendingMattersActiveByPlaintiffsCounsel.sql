SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByPlaintiffsCounsel] (@MatterID int=null,@EndDate datetime=null, @emailaddress nvarchar(200) = null)
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--     --Insert statements for procedure here
--DECLARE @MatterID int
--SET @MatterID = null
DECLARE @maxMatterID int
SET @maxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

if (@MatterID = 0)
BEGIN
	select top 10 pca.PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) As [Count]
	from tblClaim c
	inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
	inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
	inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
	inner join tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID

	where	ClaimLawsuitStatusID in (22) 
			and  IsPrimaryStatus=1 
			--and ProcessedDate <= coalesce(@EndDate,GETDATE())
	group by pca.PlaintiffCounsel
	order by COUNT(distinct(cl.ClaimLawsuitID)) desc, pca.PlaintiffCounsel
END
ELSE
BEGIN
	select top 10 pca.PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) As [Count]
	from tblClaim c
	inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
	inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
	inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
	inner join tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID

	where	ClaimLawsuitStatusID in (22) 
			and  IsPrimaryStatus=1 
			--and ProcessedDate <= COALESCE(@EndDate,GETDATE())
	group by pca.PlaintiffCounsel
	order by COUNT(distinct(cl.ClaimLawsuitID)) desc, pca.PlaintiffCounsel
END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByPlaintiffsCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByPlaintiffsCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByPlaintiffsCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersActiveByPlaintiffsCounsel] TO [power_user]
GO
