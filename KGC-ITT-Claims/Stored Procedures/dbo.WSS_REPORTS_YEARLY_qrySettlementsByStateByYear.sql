SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByStateByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null, @emailaddress nvarchar(200) = null) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @YearID int
--Set @YearId=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

select FullNameState, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(SettlementAmount) as IndemnityPaid, sum(SettlementAmount)/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimantSettlement cs ON cs.ClaimLawsuitID=cl.ClaimLawsuitID
left join tblState s ON l.StateID=s.StateID
where	coalesce(SettlementDate,statusdate)between coalesce(@StartDate,'1/1/1900')
		and coalesce(@EndDate, '1/1/2100')
		and ClaimLawsuitStatusID in (23,25) 
		and  IsPrimaryStatus=1
group by FullNameState
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByStateByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByStateByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByStateByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByStateByYear] TO [power_user]
GO
