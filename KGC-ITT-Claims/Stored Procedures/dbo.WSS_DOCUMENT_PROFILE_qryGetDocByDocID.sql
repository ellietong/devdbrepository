SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] (@DocId int)
AS

--DECLARE @SPDocId nvarchar(50)
--SET @SPDocId = 'c43ef74a-898e-40bb-a64f-a09ae1b9ed25'



--SELECT d.CreatedBy,
--	COALESCE(d.Accepted, 0) AS Accepted,
--	d.AcceptedBy,
--	d.DateAccepted,
--	--d.FirmID,
--	cdt.ClaimantDocumentGroupID,
--	DocumentURL,
--	d.ClaimantDocumentTypeID--,
--	--d.StatusID
--FROM tblDocuments d
--	INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
--WHERE DocumentID = @DocId

SELECT d.CreatedBy,
	COALESCE(d.Accepted, 0) AS Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	DefenseCounselID as FirmID,
	cdt.ClaimantDocumentGroupID,
	AmazonKey AS DocumentURL,
	d.ClaimantDocumentTypeID,
	AmazonKey,
	StatusID
FROM tblDocuments d
	INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
	LEFT JOIN tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
	inner join tblClaim c on mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	inner join tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
	inner join tblLawsuit l on cl.LawsuitID = l.lawsuitid
	inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID = clst.ClaimLawsuitID
	inner join tblDefenseCounselAddress dca on l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	left join tblDefenseInvoice di on d.DocumentID = di.DocumentID
WHERE d.DocumentID = @DocId 
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID] TO [power_user]
GO
