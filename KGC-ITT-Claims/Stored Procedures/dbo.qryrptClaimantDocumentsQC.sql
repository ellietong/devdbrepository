SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimantDocumentsQC] (@ClaimantPersonalInfoID int)

as
--DECLARE @ClaimantPersonalInfOID bigint = 57510

Select d.DocumentID as ClaimantDocumentID,
mcd.ClaimantPersonalInfoID, 
ClaimantDocumentType,
Coalesce(AmazonKey,DocumentURL) as DocumentLink, 
COALESCE(DefenseCounsel, 'Internal/Historical') as DefenseCounsel,
DateAccepted,
ds.DocumentSource,
Comments
from tblDocuments d
INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
LEFT JOIN tblClaimantDocumentType dt on dt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID 
INNER JOIN tblClaim c on c.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
INNER JOIN tblLawsuit l on l.LawsuitID = cl.LawsuitID
LEFT JOIN tblDefenseCounselAddress dc on dc.DefenseCounselAddressID = l.DefenseCounselAddressID 
LEFT JOIN tblDocumentSource ds on ds.DocumentSourceID = d.DocumentSourceID 
where mcd.ClaimantPersonalInfoID = @claimantpersonalInfoID


Order By ClaimantDocumentType

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [power_user]
GO
