SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryDelSettlementShare] 
	-- Add the parameters for the stored procedure here
@SettlementShareID int, @ClaimLawsuitID int, @InsuranceProgramID int
AS
BEGIN

	SET NOCOUNT ON;

DELETE 
FROM tblSettlementShare
WHERE SettlementShareID = @SettlementShareID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryDelSettlementShare] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryDelSettlementShare] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryDelSettlementShare] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryDelSettlementShare] TO [power_user]
GO
