SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] (@emailaddress nvarchar(1000) = NULL)
AS

--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'hagep@kcicllc.com'

DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

DECLARE @WhereString nvarchar(max)
DECLARE @JoinString  nvarchar(max)

SET @WhereString = ' WHERE dt.ClaimantDocumentGroupID = 2 AND (di.StatusID IS NULL OR di.StatusID = 3)'
--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.

IF (@DefenseCounselID IN (277)) OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com')
	SET @JoinString = ' '
ELSE IF (@DefenseCounselID IN (63))
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND di.FirmID IN (340)'
END
ELSE IF (@DefenseCounselID IN (340))
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND di.FirmID <> 340'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT CAST(d.DocumentID as varchar(50)) as DocumentId,
		d.DocumentTitle,
		d.DocumentURL,				
		di.FirmID,
		f.Firm,		
		dt.ClaimantDocumentType, 
		di.Period,
		dinv.InvoiceTotal,
		di.DatePayable,
		dis.[Status],
		d.AcceptedBy,
		d.DateAccepted,
		d.DateCreated,
		v.VendorName as VendorFirm,
		InsuranceProgram AS CoverageLine,
		di.InvoiceNumber
	FROM tblDocuments d
		INNER JOIN tblClaimantDocumentType dt	ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
		INNER JOIN tblClaimantDocumentGroup dg	ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
		INNER JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
		LEFT JOIN vtblDefenseInvoice dinv		ON dinv.DocumentID = d.DocumentID
		LEFT JOIN  tblDefenseInvoiceStatus dis	ON di.StatusID = dis.StatusID 
		LEFT JOIN  vtblFirm f					ON di.FirmID = f.FirmID
		LEFT JOIN  tblPaymentDefense pd			ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN  tblCheck c					ON pd.CheckID = c.CheckID
		LEFT JOIN  tblVendor v  				ON v.VendorID = di.VendorFirmID
		LEFT JOIN tblInsuranceProgram ip		ON di.InsuranceProgramID = ip.InsuranceProgramID ' +
    @WhereString + '
	ORDER BY f.Firm ASC, DateCreated DESC
	OPTION (RECOMPILE)'

PRINT @Querystring


--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [power_user]
GO
