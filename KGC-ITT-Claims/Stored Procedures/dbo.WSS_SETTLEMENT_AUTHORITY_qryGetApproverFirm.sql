SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverFirm]
(@SettlementApproverFirmID int = NULL)
AS
BEGIN
	
		SELECT	saf.*,
			DefenseCounsel AS Firm
		FROM tblSettlementApproverFirm saf
		INNER JOIN tblDefenseCounsel dc ON saf.FirmID = dc.DefenseCounselID
		WHERE SettlementApproverFirmID = @SettlementApproverFirmID
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetApproverFirm] TO [power_user]
GO
