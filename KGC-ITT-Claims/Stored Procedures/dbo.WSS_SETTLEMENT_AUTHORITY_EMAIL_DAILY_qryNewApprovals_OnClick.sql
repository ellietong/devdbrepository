SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals_OnClick] 
AS
SET NOCOUNT ON
 
BEGIN
--INTERNAL ONLY
 
/*
	TEST VARS
*/
--DECLARE @SettlementAuthorityID bigint = 1
 
--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)
 
Declare @Today9AM datetime
Declare @Yesterday9AM datetime
SET @Yesterday9AM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-1))+' 9:00:00.000')
SET @Today9AM = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 9:00:00.000')
 
	
	SET @Subject = 	'Warren: New Payment Approval ' + CONVERT(varchar, getdate(), 107) 
 
	--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
	--KCIC logo is temporarily being pulled from D. Snyder's Google site	
 
 
	SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
 
 
 
 
	--Declare and populate the body of the email for individual firms
	DECLARE @BodyMessage nvarchar(MAX)
 
	--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
	--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
	SET @BodyMessage = @LogoHeader + 
	'<STYLE TYPE="text/css">
	<!--
	TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
	TD	
	{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
		
	}
	TH	
	{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
	UL {margin:0; 
		
	padding:0;}	
 
 
	--->
	</STYLE>'
 
 
 
	SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
	--CSS
 
 
IF(SELECT COUNT(*)
	FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
	INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID	
	LEFT join tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
	LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
			AND clst.ClaimLawsuitStatusID = 23
			AND cs.PaymentApprovedDate IS NOT NULL
			AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
			AND ExecutedReleaseDate IS NOT NULL) > 0
BEGIN
	
	DECLARE @MainHeaderStyle nvarchar(MAX) = 'background-color: #0079C1; font-weight: bold; color: white; padding: 4px;'
	DECLARE @tdStyle nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'
 
	--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
	set @BodyMessage = @BodyMessage + '<H4>The following settlement(s) have been approved for payment.<br/><br/>' + 
	'To view additional information please click ' + 
	'<a href="http://admin.kcicextranet.com/clients/wp/cp/Pages/SettlementAuthority.aspx?approval=">here</a>. </H4>'
 
	--PRINT TABLE HEADERS
	SET @BodyMessage = @BodyMessage + '
	<table>
		<thead>
			<tr>
				<th style="' + @MainHeaderStyle + '">Claimant</th>
				<th style="' + @MainHeaderStyle + '">Settlement Group</th>
				<th style="' + @MainHeaderStyle + '">Plaintiff Counsel</th>
				<th style="' + @MainHeaderStyle + '">Release Received</th>
				<th style="' + @MainHeaderStyle + '">Settlement Date</th>
				<th style="' + @MainHeaderStyle + '">Settlement Amount</th>
				<th style="' + @MainHeaderStyle + '">Approved By</th>
			</tr>
		</thead>
	'
	--PRINT TABLE OF CLAIMS
	SET @BodyMessage = @BodyMessage +
 
		COALESCE((CAST((SELECT 
			[td/@style] = @tdStyle,
			[td/a/@href] = 'http://admin.kcicextranet.com/clients/wp/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, c.ClaimantPersonalInfoID)+'&claimanttab=1&claimtab=2',
			[td/a] = cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''),
			[td] = '', '',
			[td/@style] = @tdStyle,
			td = COALESCE(UPPER(SettlementGroup), 'Unknown'), '',
			[td/@style] = @tdStyle,
			td = COALESCE(PlaintiffCounsel, 'Unknown'), '',
			[td/@style] = @tdStyle,
			td = CONVERT(nvarchar, ExecutedReleaseDate, 101), '',
			[td/@style] = @tdStyle,
			td = CONVERT(nvarchar, SettlementDate, 101), '',
			[td/@style] = @tdStyle,
			td = '$' + CONVERT(nvarchar, CONVERT(money, settlementamount)), '',
			[td/@style] = @tdStyle,
			td = cs.PaymentApprovedBy, ''
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
		INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		INNER JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID	
		LEFT join tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
		LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
			AND clst.ClaimLawsuitStatusID = 23
			AND cs.PaymentApprovedDate IS NOT NULL
			AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
			AND ExecutedReleaseDate IS NOT NULL
			--AND cs.SettlementGroupID >= COALESCE(@SettlementGroupID, 0) AND cs.SettlementGroupID <= COALESCE(@SettlementGroupID, @MaxSettlementGroupID)
			--TODO: add logic here to make sure the settlements have received authority
	GROUP BY	cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''), 
				c.ClaimantPersonalInfoID,
				PlaintiffCounsel,
				SettlementDate,
				SettlementGroup,
				cs.PaymentApprovedBy,
				cs.PaymentApprovedDate,
				ClaimantSettlementID,
				ExecutedReleaseDate,
				SettlementAmount
	FOR XML PATH('tr'), type) AS NVARCHAR(MAX))), '')
 
	--CLOSE TABLE
	SET @BodyMessage = @BodyMessage + '</table>'
 
	
 
 
 
--KCIC address and disclaimer
	SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
	733 10th Street, NW, Suite 3001<br>
	Washington, DC 20001<br>
	(t) 202-772-2300<br>
	(f) 202-772-2333'
 
	  PRINT @BodyMessage 
	DECLARE @RecipientList nvarchar(500)
	SET @RecipientList = 'hagep@kcicllc.com'--;monahanc@kcicllc.com;potterm@kcicllc.com' --KCIC internal email only
 
--QC Recipient List
--Set @Bodymessage = @BodyMessage + 'Recipients' + @RecipientList
--PRINT @RecipientList
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The Profile_Name sends from [client]@kcicllc.com email address
@profile_name='Claims',
--@recipients = 'scottc@kcicllc.com',
@recipients = @RecipientList,--@RecipientList,
@copy_recipients = '',--'crookc@kcicllc.com',
@blind_copy_recipients = 'itt@kcic.com',
--@recipients = 'amanp@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
 
END
ELSE
BEGIN
	SET @BodyMessage = @BodyMessage + '<h3>There were no new settlements approved for payment in the last 24 hours (9AM EST yesterday to 9AM EST today).</h3>'
 
END
 
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals_OnClick] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals_OnClick] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals_OnClick] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals_OnClick] TO [power_user]
GO
