SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_RESOLUTION_qrySettlementDetails] (@ClaimID bigint = NULL)
AS

--DECLARE @ClaimID int
--SET @ClaimID = 1

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

SELECT C.ClaimID, 
	rt.ReleaseType, 
	SettlementDueDate,
	SettlementDate,
	SettlementAmount,
	SettlementGroupID,
	ClaimantSettlementID
FROM tblClaim C								
	INNER JOIN tblClaimLawsuit CL						ON C.ClaimID = CL.ClaimID 
	LEFT  JOIN tblClaimantSettlement cs					ON cs.ClaimLawsuitID = CL.ClaimLawsuitID 
	LEFT  JOIN tblSettlePaymentDocumentation spd		ON spd.ClaimLawsuitID = cl.ClaimLawsuitID 
	LEFT  JOIN tblReleaseType rt						ON rt.ReleaseTypeID = spd.ReleaseTypeID
WHERE C.ClaimID = @ClaimID
	AND cl.IsPrimaryForClaim = 1


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qrySettlementDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qrySettlementDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qrySettlementDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qrySettlementDetails] TO [power_user]
GO
