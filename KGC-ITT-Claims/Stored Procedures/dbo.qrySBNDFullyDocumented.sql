SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create Proc [dbo].[qrySBNDFullyDocumented] 

As


SET NOCOUNT ON

--DECLARE @ClaimantPersonalInfoID int
--DECLARE @LawsuitID int


--SET @ClaimantPersonalInfoID = 57584
--SET @LawsuitID = 19211
--DECLARE @MatterID int
--SET @MatterID = 1

--select * from tblClaim where ClaimantPersonalInfoID = 57584
--select * from tblClaimLawsuit where ClaimID = 57648


DECLARE @Product table (claimantpersonalinfoid int, productentered int)
insert into @Product 
select ce.ClaimantPersonalInfoID, case when cp.ProductID is null or cp.StartDate is null or cp.EndDate is null  then 0  else 1  end as productentered
from tblClaimantEmployment ce 
left join tblClaimantProduct cp on cp.ClaimantEmploymentID =ce.ClaimantEmploymentID 
where ce.IsPrimary =1 
group by ce.ClaimantPersonalInfoID , case when cp.ProductID is null or cp.StartDate is null or cp.EndDate is null  then 0  else 1  end 



DECLARE @ClaimantDocs TABLE(ClaimantID int, FName nvarchar(50), LName nvarchar(200),
ClaimantIDed int, ClaimantSSN int, ClaimantBirthdate int, RPLegalcapacity int, RPrelationship int, RPaddress int, RPphone int, RPSSN int,  DxMedicals int,
Disease int, DiseaseDiagnosisdate datetime, DODCert int, EstatePapers int, productid int,enteredproductid int, EmployExpoHist int, settlementconfirmation int)
DECLARE @MissingDocuments TABLE(MissingDocName nvarchar(100))

INSERT INTO @ClaimantDocs
	select 
cpi.ClaimantPersonalInfoID, cpi.FirstName, cpi.LastName,
	sd.ClaimantIDed,case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980'and socialsecuritynumber is null then 0 else 1 end as SSN, 
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and birthdate is null then 0 else 1 end as birthdate,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and LegalCapacityID is null then 0 
	when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and LegalCapacityID=14 then 0 else 1 end as RPLegalCapacity,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RelationshipID is null then 0 
	when relationshipid=6 or RelationshipID is null and crp.firstname is not null then 0 else 1 end as RPRelationship,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RPAddress1 is null and MedicareBeneficiaryStatusID in (1,2) then 0 else 1 end as RPaddress,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RPPhone is null and MedicareBeneficiaryStatusID in (1,2)then 0 else 1 end as RPPhone,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RPSocialSecurityNumber is null and MedicareBeneficiaryStatusID in (1,2) then 0 else 1 end as RPSSN , 
	sd.DxMedicals, DiseaseID,
	DiseaseDiagnosisDate, 	
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE  sd.DODCert END AS DODCert, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE  sd.EstatePapers END AS Estatepaper,
		sd.productid,
		p.productentered,
  sd.EmployExpoHist,  sd.settlementconfirmation
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
		INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
		inner join @Product p						 ON p.claimantpersonalinfoid =cpi.ClaimantPersonalInfoID 
		INNER JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		inner join tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
		inner JOIN tblClaimDisease cdi				 ON cdi.ClaimID = c.ClaimID
		inner join tblClaimLawsuitStatusTracking cls on cls.ClaimLawsuitID =cl.ClaimLawsuitID 
		left join tblClaimantRelatedParty crp		ON crp.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
	WHERE IsPrimaryStatus =1 and ClaimLawsuitStatusID =25 and IsPrimaryForClaim =1


Select *
from @ClaimantDocs
where ClaimantIDed = 1 AND ClaimantSSN = 1 AND claimantBirthdate = 1 AND RPLegalcapacity = 1	AND
RPrelationship = 1 AND RPaddress = 1 AND RPphone = 1 AND RPSSN = 1 AND
DxMedicals <>0 AND coalesce(Disease,0) <>0 AND DiseaseDiagnosisdate is not null AND DODCert <>0 AND
EstatePapers <>0 AND ProductID <>0 AND EnteredProductID = 1 AND EmployExpoHist <>0 AND settlementconfirmation <>0


GO
GRANT VIEW DEFINITION ON  [dbo].[qrySBNDFullyDocumented] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrySBNDFullyDocumented] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrySBNDFullyDocumented] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrySBNDFullyDocumented] TO [power_user]
GO
