SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityApproverFirms]
	-- Add the parameters for the stored procedure here
@SettlementAuthorityID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT saaf.SettlementApproverFirmID, FirmID, IsActive, ApprovalRequired AS ApprovalRequired_Default, AuthorityAmountThreshold, ResponseWindowDays_Default, DefenseCounsel AS Firm
FROM tblSettlementAuthorityApproverFirm saaf
INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
INNER JOIN tblDefenseCounsel dc on saf.FirmID = dc.DefenseCounselID
WHERE SettlementAuthorityID = @SettlementAuthorityID
		AND IsActive = 1
END





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityApproverFirms] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityApproverFirms] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityApproverFirms] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityApproverFirms] TO [power_user]
GO
