SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryInsSettlementShare]
(@InsuranceProgramID bigint,  @SettlementShare decimal(5, 2), @ClaimID bigint = NULL)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @ClaimLawsuitID bigint
SET @ClaimLawsuitID = (SELECT ClaimLawsuitID FROM tblClaimLawsuit WHERE ClaimID = @ClaimID AND IsPrimaryForClaim = 1)

IF @ClaimLawsuitID IS NOT NULL
BEGIN
	INSERT INTO tblSettlementShare (ClaimLawsuitID, InsuranceProgramID, SettlementShare)
	VALUES (@ClaimLawsuitID, @InsuranceProgramID, @SettlementShare)
END
END
GO
