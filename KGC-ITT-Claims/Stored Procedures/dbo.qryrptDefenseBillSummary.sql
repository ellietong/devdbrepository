SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptDefenseBillSummary] (@MonthName NVARCHAR(15), @Month INT, @Year NVARCHAR(15))
AS
SET NOCOUNT ON

--DECLARE @MonthName NVARCHAR(15)
--DECLARE @Month INT
--DECLARE @Year NVARCHAR(15)

--SET @MonthName = 'February'
--SET @Month = 2
--SET @Year = 2013

DECLARE @ApproveDateBegin VARCHAR(25)
SET @ApproveDateBegin = 
	CASE WHEN @Month = 1 THEN '1/1/' + @Year
		 WHEN @Month = 2 THEN '2/1/' + @Year
		 WHEN @Month = 3 THEN '3/1/' + @Year
		 WHEN @Month = 4 THEN '4/1/' + @Year
		 WHEN @Month = 5 THEN '5/1/' + @Year
		 WHEN @Month = 6 THEN '6/1/' + @Year
		 WHEN @Month = 7 THEN '7/1/' + @Year
		 WHEN @Month = 8 THEN '8/1/' + @Year
		 WHEN @Month = 9 THEN '9/1/' + @Year
		 WHEN @Month = 10 THEN '10/1/' + @Year
		 WHEN @Month = 11 THEN '11/1/' + @Year
		 WHEN @Month = 12 THEN '12/1/' + @Year
	ELSE '1/1/2050' END

DECLARE @ApproveDateEnd VARCHAR(25)
SET @ApproveDateEnd =
	CASE WHEN @Month = 1 THEN '2/1/' + @Year
		 WHEN @Month = 2 THEN '3/1/' + @Year
		 WHEN @Month = 3 THEN '4/1/' + @Year
		 WHEN @Month = 4 THEN '5/1/' + @Year
		 WHEN @Month = 5 THEN '6/1/' + @Year
		 WHEN @Month = 6 THEN '7/1/' + @Year
		 WHEN @Month = 7 THEN '8/1/' + @Year
		 WHEN @Month = 8 THEN '9/1/' + @Year
		 WHEN @Month = 9 THEN '10/1/' + @Year
		 WHEN @Month = 10 THEN '11/1/' + @Year
		 WHEN @Month = 11 THEN '12/1/' + @Year
		 WHEN @Month = 12 THEN '1/1/' + CONVERT(NVARCHAR(25),(CONVERT(INT, @Year) + 1))
	ELSE '1/1/2050' END

DECLARE @CoverageLineShare TABLE (CoverageLine NVARCHAR(500), ACEShare FLOAT, WausauShare FLOAT, ITTShare FLOAT)
INSERT INTO @CoverageLineShare (CoverageLine, ACEShare, WausauShare, ITTShare)
SELECT 'Alfred Tevis GmbH', .5, 0, .5
UNION ALL
SELECT 'Allis Chalmers', .5, .5, 0
UNION ALL
SELECT 'Fabri-Valve', .5, 0, .5
UNION ALL
SELECT 'ITT', .5, .5, 0
UNION ALL
SELECT 'Kennedy Valve', .5, 0, .5
UNION ALL
SELECT 'Grinnell', .5, 0, .5
--SELECT * FROM tblInsuranceProgram

SELECT @MonthName AS MonthName, @Year AS Year,
DI.DefenseInvoiceID,
CASE WHEN DI.DefenseCounselAddressID IS NULL THEN DC.DefenseCounsel ELSE DCA.DefenseCounsel END AS DefenseCounsel, 
DI.InvoiceNumber, SUM((Fees + Costs - Adjustments)) AS ClaimantExpense, 
SUM((Fees + Costs - Adjustments)* ACEShare) AS ACEExpense, SUM((Fees + Costs - Adjustments) * WausauShare) AS WausauExpense, SUM((Fees + Costs - Adjustments) * ITTShare) AS ITTExpense
FROM tblDefInvClaimantDetail DICD 
LEFT JOIN tblClaimantPersonalInfo CPI on CPI.ClaimantPersonalInfoID = DICD.ClaimantPersonalInfoID
LEFT JOIN tblDefenseInvoice DI ON DICD.DocumentID = DI.DocumentID
LEFT JOIN tblDefenseCounselAddress DCA ON DCA.DefenseCounselAddressID = DI.DefenseCounselAddressID
LEFT JOIN tblDefenseCounsel DC ON DC.DefenseCounselID = DI.FirmID
LEFT JOIN (SELECT * FROM tblClaim WHERE IsCurrentClaim = 1)C ON C.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
LEFT JOIN (SELECT * FROM tblClaimLawsuit WHERE IsPrimaryForClaim = 1) CL ON CL.ClaimID = C.ClaimID
LEFT JOIN tblLawsuit L ON L.LawsuitID = CL.LawsuitID
LEFT JOIN tblState S ON S.StateID = L.StateID
LEFT JOIN tblJurisdiction J ON J.JurisdictionID = L.JurisdictionID
LEFT JOIN tblInsuranceProgram IP ON IP.InsuranceProgramID = DI.InsuranceProgramID
LEFT JOIN tblPaymentDefense PD ON PD.DefenseInvoiceID = DI.DefenseInvoiceID
LEFT JOIN tblCheck Ch ON Ch.CheckID = PD.CheckID
LEFT JOIN @CoverageLineShare CLS ON CLS.CoverageLine = IP.InsuranceProgram
WHERE (CONVERT(DATE, DateApproved) >= @ApproveDateBegin) AND (CONVERT(DATE, DateApproved) < @ApproveDateEnd)
AND StatusID <> 4
AND (DC.DefenseCounsel NOT LIKE '%McGuireWoods LLP%' OR DCA.DefenseCounsel NOT LIKE '%McGuireWoods LLP%')
--AND DICD.InvoiceNumber NOT LIKE '%9386%'
GROUP BY DC.DefenseCounsel, DCA.DefenseCounsel, DI.DefenseCounselAddressID,
DI.InvoiceNumber, DI.DefenseInvoiceID
ORDER BY DefenseCounsel, DI.InvoiceNumber

RETURN
GO
GRANT CONTROL ON  [dbo].[qryrptDefenseBillSummary] TO [base_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qryrptDefenseBillSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseBillSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseBillSummary] TO [base_user]
GRANT CONTROL ON  [dbo].[qryrptDefenseBillSummary] TO [Claims_User]
GRANT TAKE OWNERSHIP ON  [dbo].[qryrptDefenseBillSummary] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseBillSummary] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptDefenseBillSummary] TO [Claims_User]
GRANT CONTROL ON  [dbo].[qryrptDefenseBillSummary] TO [power_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qryrptDefenseBillSummary] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseBillSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseBillSummary] TO [power_user]
GO
