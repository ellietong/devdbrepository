SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmrptAllocationProvisionToExclude]
(@allocationID int = NULL)
AS
SET NOCOUNT ON
IF @allocationID IS NULL
	SELECT ProvisionValueID, AllocationID, ProvisionLabelID
	FROM tblAllocationProvisionToExclude
ELSE
	SELECT ProvisionValueID, AllocationID, ProvisionLabelID
	FROM tblAllocationProvisionToExclude
	WHERE AllocationID = @allocationID
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmrptAllocationProvisionToExclude] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmrptAllocationProvisionToExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmrptAllocationProvisionToExclude] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmrptAllocationProvisionToExclude] TO [power_user]
GO
