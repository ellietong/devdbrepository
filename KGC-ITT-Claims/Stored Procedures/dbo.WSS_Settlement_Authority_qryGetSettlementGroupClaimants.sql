SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_Settlement_Authority_qryGetSettlementGroupClaimants]
	-- Add the parameters for the stored procedure here
@SettlementGroupID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT cpi.ClaimantPersonalInfoID AS ClaimantID, cpi.LastName, cpi.FirstName, cs.ClaimLawsuitID, PlaintiffCounselAddressID, cpi.LastName + ', ' + cpi.FirstName AS ClaimantName, ComplaintDOFE, BasisOfRecommendation
	FROM tblSettlementGroup sginner
		INNER JOIN tblClaimantSettlement cs ON sginner.SettlementGroupID = cs.SettlementGroupID
		INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = L.LawsuitID
	WHERE sginner.SettlementGroupID = @SettlementGroupID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementGroupClaimants] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementGroupClaimants] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementGroupClaimants] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementGroupClaimants] TO [power_user]
GO
