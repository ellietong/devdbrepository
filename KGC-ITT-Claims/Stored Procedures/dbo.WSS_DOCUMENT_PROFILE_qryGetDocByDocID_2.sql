SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID_2] (@DocId int)

AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @DocID int
--SET @DocID = 243371

DECLARE @DocGroupID int = (SELECT ClaimantDocumentGroupID FROM tblDocuments d
								INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID  
						  WHERE DocumentID = @docid)

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
DECLARE @Field nvarchar(250)
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE d.DocumentID = @DocId '

IF @DocGroupID = 2
BEGIN 
	SET @JoinString = ' INNER JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
							 INNER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID
							 INNER JOIN tblDefenseCounselAddress dca ON di.DefenseCounselAddressID = dca.DefenseCounselAddressID'
	SET @Field = ', di.FirmID, StatusID'
END	
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
							 INNER JOIN tblClaim c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID 
							 INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
							 INNER JOIN tblLawsuit l on cl.LawsuitID = l.LawsuitID
							 INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID = clst.ClaimLawsuitID
							 INNER JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID'
	SET @Field = ', DefenseCounselID AS FirmID, NULL AS StatusID'
	SET @WhereString = @WhereString + '  AND cl.IsPrimaryForClaim = 1 AND c.IsCurrentClaim = 1 '			
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT d.CreatedBy,
		COALESCE(d.Accepted, 0) AS Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		cdt.ClaimantDocumentGroupID,
		AmazonKey AS DocumentURL,
		d.ClaimantDocumentTypeID,
		AmazonKey' + 
		@Field + '
	FROM tblDocuments d
		INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID' + 
		@JoinString + '
' + 
		@WhereString + '		
 '

print @QueryString
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@DocID int', @DocId = @DocId



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID_2] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID_2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID_2] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocByDocID_2] TO [power_user]
GO
