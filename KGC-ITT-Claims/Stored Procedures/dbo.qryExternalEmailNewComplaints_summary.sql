SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailNewComplaints_summary]
AS
BEGIN
	/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/


	--Declare and set email inputs.
	DECLARE @Subject nvarchar(100)
	DECLARE @LogoHeader nvarchar(2000)

	DECLARE @Today4PM datetime
	DECLARE @Yesterday4PM datetime
	SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')
	SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')



	SET @Subject =
	'ITT Claims: New Complaints Uploaded ' + CONVERT(varchar, GETDATE(), 107)
	--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
	--KCIC logo is temporarily being pulled from D. Snyder's Google site	


	SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


	--Create a temp table for Complaints which have been uploaded in the past day.
	--DocTitle and FirmAbb are retreived by parsing the documentURL.
	DECLARE @NewComplaintsTable TABLE (
		DocID int,
		docTitle nvarchar(500),
		FirmID int,
		lName nvarchar(50),
		fName nvarchar(50),
		ClaimantPersonalInfoID bigint,
		ServiceDate date,
		[State] nvarchar(5),
		PlaintiffCounsel nvarchar(500),
		DefenseCounsel nvarchar(400),
		LawsuitID int
	)
	INSERT INTO @NewComplaintsTable
		SELECT
			COALESCE(d.DocumentID, 0),
			COALESCE('<a href="https://www.kcicextranet.com/clients/ITT/ittcp/Pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>','') AS DocumentTitle,
			COALESCE(DefenseCounselID, 0),
			COALESCE(cpi.LastName, ' '),
			COALESCE(cpi.FirstName, ' '),
			COALESCE(cpi.ClaimantPersonalInfoID, 0),
			COALESCE(MAX(ServiceDate),''),
			COALESCE(s.State, ''),
			COALESCE(PlaintiffCounsel, ' '),
			COALESCE(DefenseCounsel, ''),
			COALESCE(cl.LawsuitID, 0)


		FROM tblDocuments d

		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID

		INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID

		LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID

		LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid

		LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid

		LEFT JOIN tblAllegation a ON a.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblState s ON s.StateID = l.StateID
		LEFT JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		LEFT JOIN tblDefendantCompany dc ON a.DefendantCompanyID = dc.DefendantCompanyID

		WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM

		AND d.ClaimantDocumentTypeID IN (8, 4, 33)

		AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')

		--AND (RIGHT(COALESCE(AcceptedBy, ''), 11) <> 'kcicllc.com' OR RIGHT(COALESCE(AcceptedBy, ''), 8) <> 'kcic.com')

		AND l.DefenseCounselAddressID IS NOT NULL

		AND (c.MatterID <> 4 OR a.AllegationTypeID IN (2, 3))

		AND IsPrimaryForClaim = 1
		AND IsCurrentClaim = 1

		GROUP BY	d.documentID,
					d.DocumentTitle,
					s.State,
					DefenseCounselID,
					LastName,
					FirstName,
					cpi.ClaimantPersonalInfoID,
					PlaintiffCounsel,
					DefenseCounsel,
					cl.LawsuitID




	DECLARE @ClaimantPerDoc TABLE (
		DocID int,
		ClaimantList nvarchar(max)
	)
	DECLARE	@DocID int,
			@LastDocID int,
			@Claimant nvarchar(max),
			@ClaimantID bigint,
			@ClaimantList nvarchar(max)

	DECLARE Cur_Complaints CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		COALESCE(DocID,0),
		COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
		COALESCE(claimantpersonalInfoID,0)
	FROM @NewComplaintsTable
	OPEN Cur_Complaints
	FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
	WHILE @@FETCH_STATUS = 0
	BEGIN


		IF (LEN(@ClaimantList) = 0 OR @ClaimantList IS NULL)
		BEGIN

			SET @ClaimantList = '<ul style="margin:0; padding:0;"> <a href="https://www.kcicextranet.com/clients/ITT/ittcp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + @Claimant + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
		END
		ELSE
		BEGIN

			SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul style="margin:0; padding:0;">  <a href="https://www.kcicextranet.com/clients/ITT/ittcp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + COALESCE(@Claimant, '') + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'

		END
		SET @LastDocID = @DocID
		FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
		PRINT @@FETCH_STATUS
		IF @DocID <> @LastDocID OR @@FETCH_STATUS <> 0
		BEGIN

			INSERT INTO @ClaimantPerDoc

				SELECT
					COALESCE(@LastDocID,0),
					COALESCE(@ClaimantList,'')

			SET @ClaimantList = NULL
		END
	END
	CLOSE Cur_Complaints
	DEALLOCATE Cur_Complaints



	--Declare and populate the body of the email for individual firms
	DECLARE @BodyMessage nvarchar(max)
	--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
	--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
	SET @BodyMessage = @LogoHeader +
	'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	
 
 
--->
</STYLE>'

	DECLARE @LastLawsuitID nvarchar(250) = ''

	--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

	IF (SELECT
			COUNT(docTitle)
		FROM @NewComplaintsTable)
		> 0
	BEGIN
		SET @BodyMessage = @BodyMessage + '<H4>The following complaints have been uploaded in the last 24 hours: </H4>'
		SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">'
		SET @BodyMessage = @BodyMessage + N'<tr><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Document Title</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Linked Claimants (ClaimantID)</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Date Served*</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">State</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Defense Counsel</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Plaintiff Counsel</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">ITT Namings</th>'

		DECLARE	@DocTitle nvarchar(250),
				@clClaimantList nvarchar(500),
				@ServiceDate nvarchar(300),
				@State nvarchar(250),
				@PlaintiffCounsel nvarchar(400),
				@DefenseCounsel nvarchar(400),
				@LawsuitID int,
				@Namings nvarchar(500),
				@NamingsRowID nvarchar(10)

		DECLARE Doc_List CURSOR FOR
		SELECT
			COALESCE(docTitle,''),
			COALESCE(cl.ClaimantList,''),
			CAST(MAX(ServiceDate) AS varchar(max)),
			COALESCE([state],''),
			COALESCE(PlaintiffCounsel,''),
			COALESCE(DefenseCounsel,''),
			COALESCE(LawsuitID,0)
		FROM @NewComplaintsTable C
		INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
		GROUP BY	cl.docid,
					docTitle,
					cl.ClaimantList,
					[state],
					PlaintiffCounsel,
					DefenseCounsel,
					LawsuitID
		ORDER BY cl.DocID

		OPEN Doc_List
		FETCH NEXT FROM Doc_List INTO @DocTitle, @clClaimantList, @ServiceDate, @State, @PlaintiffCounsel, @DefenseCounsel, @LawsuitID

		WHILE @@FETCH_STATUS = 0

		BEGIN


			SET @BodyMessage = @BodyMessage + '<tr>'
			SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @DocTitle + ' </td>'
			SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @clClaimantList + ' </td>'
			SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @ServiceDate + ' </td>'
			SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @State + ' </td>'
			SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @DefenseCounsel + ' </td>'
			SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @PlaintiffCounsel + ' </td>'
			SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top">'


			DECLARE @ITTNamings TABLE (
				ID int NOT NULL IDENTITY (1, 1) PRIMARY KEY,
				LawsuitID int,
				DefendantCompany nvarchar(500),
				DefendantCompanyID int
			)
			INSERT INTO @ITTNamings
				SELECT
					COALESCE(cl.LawsuitID, 0),
					COALESCE(DefendantCompany, ''),
					COALESCE(a.DefendantCompanyID, 0)
				FROM tblAllegation a
				INNER JOIN tblClaimLawsuit cl ON a.ClaimLawsuitID = cl.ClaimLawsuitID
				INNER JOIN tblDefendantCompany dc ON a.DefendantCompanyID = dc.DefendantCompanyID
				WHERE LawsuitID = @LawsuitID AND DefendantCompanyParentID = 1
				GROUP BY	cl.LawsuitID,
							DefendantCompany,
							a.DefendantCompanyID
				ORDER BY lawsuitid, DefendantCompany

			DECLARE ITTNamings_curs CURSOR FOR
			SELECT DISTINCT
				COALESCE(DefendantCompany, '')
			FROM @ITTNamings
			WHERE LawsuitID = @LawsuitID


			OPEN ITTNamings_curs
			FETCH NEXT FROM ITTNamings_curs INTO @Namings

			WHILE @@FETCH_STATUS = 0
			BEGIN

				SET @BodyMessage = @BodyMessage + @Namings + ' <br/>'

				FETCH NEXT FROM ITTNamings_curs INTO @Namings

			END
			CLOSE ITTNamings_curs
			DEALLOCATE ITTNamings_curs

			SET @BodyMessage = @BodyMessage + '</td></tr>'

			SET @LastLawsuitID = @LawsuitID

			FETCH NEXT FROM Doc_List INTO @DocTitle, @clClaimantList, @ServiceDate, @State, @PlaintiffCounsel, @DefenseCounsel, @LawsuitID

		END
		CLOSE Doc_List
		DEALLOCATE Doc_List


		SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'


	END
	ELSE
	BEGIN
		SET @BodyMessage = @BodyMessage + '<H4>No Complaints have been uploaded in the last 24 hours. </H4>'
	END



	--Undo the HTML stripping from the xml path function above so embedded tags work
	SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

	----Link for document library
	--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/ITT/ittcp/Pages/Documents.aspx">here</A> to access the document(s).</b><br><br>'

	--Service Date Assumption 

	IF (SELECT
			COUNT(docTitle)
		FROM @NewComplaintsTable)
		> 0
	BEGIN
		SET @BodyMessage = @BodyMessage + '<br>*Note: Date served is the latest service date for claimants'' lawsuits.<br><br>'
	END

	--KCIC address and disclaimer
	SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

PRINT @BodyMessage

	--IF (Select COUNT(docTitle) from @NewComplaintsTable) >0 
	--begin
	--Execute stored procedure to send the email to that firm
	EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
									--@recipients = 'hagep@kcic.com;onyiukeo@kcic.com;shockleym@kcic.com;monahanc@kcic.com',
									@recipients = 'mmeyer@mcguirewoods.com; pgoldstein@mcguirewoods.com; gmacsteel@mcguirewoods.com; Loree.Shelko@itt.com; William.feher@itt.com; uwilks@mcguirewoods.com',
									@copy_recipients = 'Craig.Johnson@itt.com',
									@blind_copy_recipients = 'scottc@kcic.com; itt@kcic.com; ',
									@subject = @subject,
									@body = @BodyMessage,
									@body_format = 'html'
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints_summary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints_summary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints_summary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints_summary] TO [power_user]
GO
