SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_REPORTS_qrySettlementDocCategoryList]
AS
SET NOCOUNT ON

SELECT *, CASE WHEN DocCategoryID = 1 THEN 'ClaimantIDed' 
					   WHEN DocCategoryID = 2 THEN 'DxMedicals'
					   WHEN DocCategoryID = 3 THEN 'DODCert' 
					   WHEN DocCategoryID = 4 THEN 'EstatePaper' 
					   WHEN DocCategoryID = 5 THEN 'ProductID' 
					   WHEN DocCategoryID = 6 THEN 'EmployExpoHist' 
					   WHEN DocCategoryID = 8 THEN 'SettlementConfirmation' 
					   END AS SDCategory
FROM tblDocumentCategory
WHERE DocCategoryID	<> 7
UNION
SELECT MAX(DocCategoryID) + 1, 'Executed Release', 'ReleaseReceived'
FROM tblDocumentCategory
UNION
SELECT NULL, '', ''
ORDER BY DocCategory



GO
