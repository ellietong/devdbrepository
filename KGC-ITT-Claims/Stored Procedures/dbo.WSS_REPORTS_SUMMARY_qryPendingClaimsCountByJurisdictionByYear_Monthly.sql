SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaimsCountByJurisdictionByYear_Monthly] 
(@StartDate datetime=null, @EndDate datetime=null, @emailaddress nvarchar(200) = null)
AS

Set NoCount ON

--DECLARE @StartDate datetime
--DECLARE @EndDate datetime

--SET @StartDate = '1/1/1900'
--SET @EndDate = '8/21/2013'



Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int, State nvarchar(150), YearFiled datetime, monthFiledID int, 
monthFiledName nvarchar(100), statusdate datetime,claimlawsuitid int,
MesoCount int, LungCancerCount int, OtherCancerCount int, 
NonMaligCount int,  UnknownCount int, UnknownCount1 int)


Insert into @DiseaseCount

Select Matter, C.MatterID, FullNameState as state,  
case when ProcessedDate is null then  '1/1/1900' else ProcessedDate end as YearFiled, 
MONTH(COALESCE(ProcessedDate, '1/1/1900')), Datename(month, COALESCE(ProcessedDate, '1/1/1900')),
coalesce(statusdate,'1/1/1900'), cl.claimlawsuitid,
--Meso
Case	when DMeso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when DLC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when DOC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig & silicosis & Pneumoconiosis
Case	when DNM.HighLevelDiseaseID IN (4, 7, 8) Then 1
Else 0 End as NonMaligCount,
--Unknown & no disease
Case	when DUnk.HighLevelDiseaseID IN (5, 6) Then 1
Else 0 End AS UnknownCount,
--NULLs
Case	when DUnk.HighLevelDiseaseID is NULL Then 1
Else 0 End AS UnknownCount1


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
LEFT JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID
--Meso
Left Join tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
Left Join tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
Left Join tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
Left Join tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Silicosis
Left Join tblDisease DSC on CD.DiseaseID=DSC.DiseaseID

--No Disease
Left Join tblDisease DND on CD.DiseaseID=DND.DiseaseID

--Unknown
Left Join tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID


Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblState S on L.StateID=S.StateID
left join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = cl.ClaimLawsuitID

Where -- s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID) and
 coalesce(cst.IsPrimaryStatus,1) = 1
		AND cst.ClaimLawsuitStatusID in (22) 


 Select  YEAR(@EndDate) AS CloseYear, State,  Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, Sum(OtherCancerCount) as OtherCancerCount ,@StartDate as StartDate, @EndDate as EndDate,
	Sum(NonMaligCount) as NonMaligCount, Sum(UnknownCount+UnknownCount1) as UnknownCount, Sum(UnknownCount+UnknownCount1+MesoCount+LungCancerCount+OtherCancerCount+NonMaligCount) AS Total
from @DiseaseCount
Group By  State
ORDER BY  State




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaimsCountByJurisdictionByYear_Monthly] TO [power_user]
GO
