SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_MONTHLY_qryTopTenDismissalsByPlaintiffsCounselByMonth] (@MatterID int = null,
                                                                                      @MonthID  int, 
                                                                                      @YearID   int) 
AS
SET NOCOUNT ON
	
----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

SELECT TOP 10 pca.PlaintiffCounsel, 
	COUNT(DISTINCT(cl.ClaimLawsuitID)) as 'Count'
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca     ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
WHERE  MONTH(ProcessedDate) = @month 
	AND YEAR(ProcessedDate) = @year 
	AND ClaimLawsuitStatusID IN (15,16,17,27,36,19) 
	AND IsPrimaryForClaim = 1 
	AND IsPrimaryStatus = 1
	AND MatterID >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
GROUP BY pca.PlaintiffCounsel
ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC, pca.PlaintiffCounsel


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenDismissalsByPlaintiffsCounselByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenDismissalsByPlaintiffsCounselByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenDismissalsByPlaintiffsCounselByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qryTopTenDismissalsByPlaintiffsCounselByMonth] TO [power_user]
GO
