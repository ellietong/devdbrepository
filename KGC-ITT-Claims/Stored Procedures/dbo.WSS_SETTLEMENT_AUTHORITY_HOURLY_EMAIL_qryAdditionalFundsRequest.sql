SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_HOURLY_EMAIL_qryAdditionalFundsRequest] 
AS

SET NOCOUNT ON
BEGIN

--Executed in External ITT Settlement Authority Hourly Alerts job
--Sends additional funds request details alongside original (ie parent) request details to the SRA contacts
--Note that this does NOT currently check for $50,000 being exceeded so may want to revisit (might be handled on entry of SA records, not sure)

DECLARE @SettlementAuthorityID BIGINT

DECLARE Discussion_Curs CURSOR Local Fast_Forward for
	SELECT DISTINCT SettlementAuthorityID
	FROM tblSettlementAuthority
	WHERE ParentSettlementAuthorityID IS NOT NULL
		AND RequestNotificationDate IS NULL
		AND COALESCE(AuthorityGranted,0) <> 1 
		AND COALESCE(AuthorityDenied,0) <> 1

OPEN Discussion_Curs
FETCH NEXT FROM Discussion_Curs INTO @SettlementAuthorityID

WHILE @@FETCH_STATUS = 0
BEGIN			
	EXEC dbo.WSS_SETTLEMENT_AUTHORITY_EMAIL_qryAdditionalFundsRequest @SettlementAuthorityID
	FETCH NEXT FROM Discussion_Curs INTO @SettlementAuthorityID
END

CLOSE Discussion_Curs
DEALLOCATE Discussion_Curs	

END
GO
