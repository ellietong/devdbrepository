SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID_Old]
(@ClaimID bigint)
AS
BEGIN

	SET NOCOUNT ON;

SELECT DISTINCT SettlementShareID, ss.ClaimLawsuitID, ss.InsuranceProgramID, ss.SettlementShare, InsuranceProgram,
COALESCE(SettlementAmount * (CASE WHEN  ss.SettlementShare IS NULL THEN COALESCE(ss.SettlementShare, 1) ELSE ss.SettlementShare END), 
		SettlementAmount*(ss.SettlementShare)) AS DollarShare 
from tblSettlementShare ss
inner join tblAllegation a on ss.ClaimLawsuitID = a.ClaimLawsuitID
inner join tblInsuranceProgram i on i.InsuranceProgramID = ss.InsuranceProgramID
inner join tblDefendantCompanyInsProg dip on dip.DefendantCompanyID = a.DefendantCompanyID
LEFT JOIN tblClaimantSettlement cs ON a.ClaimLawsuitID = cs.ClaimLawsuitID 
where a.ClaimLawsuitID in (select a.ClaimlawsuitID from tblAllegation a
INNER JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = a.ClaimLawsuitID
WHERE ClaimID = @ClaimID)
order by ClaimLawsuitID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID_Old] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID_Old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID_Old] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID_Old] TO [power_user]
GO
