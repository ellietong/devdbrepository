SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] (@DocumentID int, @Emailaddress nvarchar(1000),@IsApproved bit=null , @ApprovalNotes nvarchar(4000)=null)--@StatusID int = null
AS

BEGIN
	UPDATE tblDefenseInvoice
	SET 
		IsApproved = @IsApproved,		
		StatusID = 6,
		PaymentApprovedBy = @EmailAddress,
		DatePaymentApproved = GETDATE(),
		ApprovalNotes = @ApprovalNotes
	WHERE DocumentID = @DocumentID
END







GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [power_user]
GO
