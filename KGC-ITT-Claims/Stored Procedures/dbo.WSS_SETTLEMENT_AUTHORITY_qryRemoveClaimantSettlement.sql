SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveClaimantSettlement]
	-- Add the parameters for the stored procedure here
(@ClaimLawsuitID int, @SettlementGroupID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	UPDATE tblClaimantSettlement 
	SET SettlementGroupID = NULL
	WHERE ClaimLawsuitID = @ClaimLawsuitID
		AND SettlementGroupID = @SettlementGroupID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveClaimantSettlement] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveClaimantSettlement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveClaimantSettlement] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveClaimantSettlement] TO [power_user]
GO
