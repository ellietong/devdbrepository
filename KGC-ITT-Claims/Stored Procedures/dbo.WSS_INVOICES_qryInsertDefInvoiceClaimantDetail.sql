SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail]
(@FirmID int, @InvoiceNumber nchar(10), @InvoiceDate date, @ClaimantPersonalInfoID nvarchar(50), @Fees money, @Costs money, 
@Adjustments money, @Notes nvarchar(mAX), @PACEClaimID int,  @DocumentID int)
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE @CPID int = (SELECT TOP 1 cpi.ClaimantPersonalInfoID
					 FROM    tblClaimantPersonalInfo cpi
							  LEFT JOIN m_tblClaimantPersonalInfo_Lookup cpil ON cpi.ClaimantPersonalInfoID = cpil.ClaimantPersonalInfoID
					 WHERE   CONVERT(NVARCHAR(250),cpi.ClaimantPersonalInfoID) LIKE LTRIM(RTRIM(@ClaimantpersonalInfoID))
							  OR cpi.Reference_ID LIKE LTRIM(RTRIM(@ClaimantpersonalInfoID)))

	INSERT INTO tblDefInvClaimantDetail ([FirmID]
      ,[InvoiceNumber]
      ,[InvoiceDate]
      ,[ClaimantPersonalInfoID]
      ,[Fees]
      ,[Costs]
      ,[Adjustments]
      ,[Notes]
      ,[PACEClaimID]
      ,[DocumentID]) VALUES (@FirmID, @InvoiceNumber, @InvoiceDate, @CPID, @Fees, @Costs, @Adjustments, @Notes, @PACEClaimID, @DocumentID)

		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [power_user]
GO
