SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qrysrptSBNDWithMissingDocuments] (@ClaimantPersonalInfoID int, @LawsuitID int)

As

SET NOCOUNT ON

--DECLARE @ClaimantPersonalInfoID int
--DECLARE @LawsuitID int


--SET @ClaimantPersonalInfoID = 57584
--SET @LawsuitID = 19211
--DECLARE @MatterID int
--SET @MatterID = 1

--select * from tblClaim where ClaimantPersonalInfoID = 57584
--select * from tblClaimLawsuit where ClaimID = 57648


DECLARE @Product table (claimantpersonalinfoid int, productentered int)
insert into @Product 
select ce.ClaimantPersonalInfoID, case when cp.ProductID is null or cp.StartDate is null or cp.EndDate is null  then 0  else 1  end as productentered
from tblClaimantEmployment ce 
left join tblClaimantProduct cp on cp.ClaimantEmploymentID =ce.ClaimantEmploymentID 
where ce.IsPrimary =1 and ce.ClaimantPersonalInfoID =@ClaimantPersonalInfoID 
group by ce.ClaimantPersonalInfoID , case when cp.ProductID is null or cp.StartDate is null or cp.EndDate is null  then 0  else 1  end 



DECLARE @ClaimantDocs TABLE(ClaimantIDed int, ClaimantSSN int, ClaimantBirthdate int, RPLegalcapacity int, RPrelationship int, RPaddress int, RPphone int, RPSSN int,  DxMedicals int,
Disease int, DiseaseDiagnosisdate datetime, DODCert int, EstatePapers int, productid int,enteredproductid int, EmployExpoHist int, settlementconfirmation int)
DECLARE @MissingDocuments TABLE(MissingDocName nvarchar(100))

INSERT INTO @ClaimantDocs
	select sd.ClaimantIDed,case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980'and socialsecuritynumber is null then 0 else 1 end as SSN, 
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and birthdate is null then 0 else 1 end as birthdate,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and LegalCapacityID is null then 0 
	when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and LegalCapacityID=14 then 0 else 1 end as RPLegalCapacity,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RelationshipID is null then 0 
	when relationshipid=6 or RelationshipID is null and crp.firstname is not null then 0 else 1 end as RPRelationship,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RPAddress1 is null and MedicareBeneficiaryStatusID in (1,2) then 0 else 1 end as RPaddress,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RPPhone is null and MedicareBeneficiaryStatusID in (1,2)then 0 else 1 end as RPPhone,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RPSocialSecurityNumber is null and MedicareBeneficiaryStatusID in (1,2) then 0 else 1 end as RPSSN , 
	sd.DxMedicals, DiseaseID,
	DiseaseDiagnosisDate, 	
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE  sd.DODCert END AS DODCert, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE  sd.EstatePapers END AS Estatepaper,
		sd.productid,
		p.productentered,
  sd.EmployExpoHist,  sd.settlementconfirmation
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
		INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
		inner join @Product p						 ON p.claimantpersonalinfoid =cpi.ClaimantPersonalInfoID 
		INNER JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		inner join tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
		inner JOIN tblClaimDisease cdi				 ON cdi.ClaimID = c.ClaimID
		inner join tblClaimLawsuitStatusTracking cls on cls.ClaimLawsuitID =cl.ClaimLawsuitID 
		left join tblClaimantRelatedParty crp		ON crp.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
	WHERE c.ClaimantPersonalInfoID =@ClaimantPersonalInfoID 
		AND cl.LawsuitID = @LawsuitID and
		IsPrimaryStatus =1 and ClaimLawsuitStatusID =25 and IsPrimaryForClaim =1



INSERT INTO @MissingDocuments
	SELECT 'Fully Identified - SSN, DOB, DOD' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ClaimantIDed IS NULL Or ClaimantIDed = 0
INSERT INTO @MissingDocuments
	SELECT 'Fully Identified - SSN' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ClaimantSSN IS NULL Or ClaimantSSN = 0
INSERT INTO @MissingDocuments
	SELECT 'Fully Identified - DOB' AS MissingDocName
	FROM @ClaimantDocs
	WHERE claimantBirthdate IS NULL Or claimantBirthdate = 0		
INSERT INTO @MissingDocuments
	SELECT 'RP Legal Capacity' AS MissingDocName
	FROM @ClaimantDocs
	WHERE RPLegalcapacity = 0			
INSERT INTO @MissingDocuments
	SELECT 'RP Relationship' AS MissingDocName
	FROM @ClaimantDocs
	WHERE RPrelationship IS NULL Or RPrelationship = 0		
INSERT INTO @MissingDocuments
	SELECT 'RP Address' AS MissingDocName
	FROM @ClaimantDocs
	WHERE RPaddress IS NULL Or RPaddress = 0
INSERT INTO @MissingDocuments
	SELECT 'RP Phone' AS MissingDocName
	FROM @ClaimantDocs
	WHERE RPphone IS NULL Or RPphone = 0
INSERT INTO @MissingDocuments
	SELECT 'RP SSN' AS MissingDocName
	FROM @ClaimantDocs
	WHERE RPSSN IS NULL Or RPSSN = 0
INSERT INTO @MissingDocuments
	SELECT 'Diagnosing Medicals' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DxMedicals IS NULL Or DxMedicals = 0
INSERT INTO @MissingDocuments
	SELECT 'Disease' AS MissingDocName
	FROM @ClaimantDocs
	WHERE Disease IS NULL Or Disease = 0
INSERT INTO @MissingDocuments
	SELECT 'Disease Date' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DiseaseDiagnosisdate IS NULL Or DiseaseDiagnosisdate = 0
INSERT INTO @MissingDocuments
	SELECT 'Death Certificate' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DODCert IS NULL Or DODCert = 0
INSERT INTO @MissingDocuments
	SELECT 'Estate Paperwork' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EstatePapers IS NULL Or EstatePapers = 0
INSERT INTO @MissingDocuments
	SELECT 'Product ID Doc' AS MissingDocName
	FROM @ClaimantDocs
	WHERE ProductID IS NULL Or ProductID = 0 
INSERT INTO @MissingDocuments
	SELECT 'Product ID Linked' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EnteredProductID = 0
INSERT INTO @MissingDocuments
	SELECT 'Employment/Exposure History' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EmployExpoHist IS NULL Or EmployExpoHist = 0
INSERT INTO @MissingDocuments
	SELECT 'Settlement Confirmation Letter' AS MissingDocName
	FROM @ClaimantDocs
	WHERE settlementconfirmation IS NULL Or settlementconfirmation = 0


SELECT * FROM @MissingDocuments
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSBNDWithMissingDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptSBNDWithMissingDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSBNDWithMissingDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptSBNDWithMissingDocuments] TO [power_user]
GO
