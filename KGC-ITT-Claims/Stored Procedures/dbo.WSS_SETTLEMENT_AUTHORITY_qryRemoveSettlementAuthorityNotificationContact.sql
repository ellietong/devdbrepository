SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveSettlementAuthorityNotificationContact]
	-- Add the parameters for the stored procedure here
(@SettlementAuthorityID int, @SettlementNotificationContactID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE
	FROM tblSettlementAuthorityNotificationContact
	WHERE SettlementAuthorityID  = @SettlementAuthorityID
		AND SettlementNotificationContactID = @SettlementNotificationContactID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveSettlementAuthorityNotificationContact] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveSettlementAuthorityNotificationContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveSettlementAuthorityNotificationContact] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveSettlementAuthorityNotificationContact] TO [power_user]
GO
