SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_updDocumentByNCC]
(
	@ClaimantDocumentTypeID int,					--aka ClaimantDocumentTypeID
	--@ClaimantPersonalInfoID bigint = null,	
	@Comments nvarchar(4000) = null,			--comments are optional		
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@MatterID int=null,					--this is the guid assigned by SharePoint
	@FirmID int = null,
	
	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,
    @Adjustment money = null,
    @AdjustmentNotes nvarchar(4000) = null,
    @StatusID int = null,

	@ModifiedBy nvarchar(250),
	@DateModified datetime,
	--@Approved bit = null,
	@ApprovedBy nvarchar(50) = null,
	@DateApproved	datetime = null,
	@DocID int=null,
	@DocumentSourceID int,
	@PrivateComments nvarchar(4000)=null,
	@VendorFirmID int=null
)

AS
BEGIN
Update tblDocuments 
SET
	ClaimantDocumentTypeID	= @ClaimantDocumentTypeID,	
	
	Comments				= @Comments,	
	DocumentURL				= @DocumentURL,	
	DocumentTitle			= @DocumentTitle,
		
	----Fields no longer in tblDocuments, only tblDefenseInvoice - add update query for tblDefenseInvoice
	--InvoiceNumber			= @InvoiceNumber,
	--Period				= @Period,
	--DatePayable			= CASE WHEN @StatusID IN (1, 2) AND VendorFirmID IS NOT NULL AND @VendorFirmID IS NOT NULL THEN GETDATE()+1 ELSE @DatePayable END,
	--AdjustmentNotes		= @AdjustmentNotes,
	----Fields no longer in tblDocuments or tblDefenseInvoice (see vtblDefenseInvoice), so no update needed
	--FeeAmount				= @FeeAmount,
	--DisbursementAmount	= @DisbursementAmount,
	--Adjustment			= @Adjustment,

    
	ModifiedBy				= @ModifiedBy,
	DateModified			= @DateModified,
	
	DocumentSourceID		= @DocumentSourceID,
	--PrivateComments			= @PrivateComments,
	MatterID				= @MatterID
	
WHERE DocumentID = @DocID

----1/31/12 EMS added query to update tblDefenseInvoice
--Update tblDefenseInvoice
--SET 
--	InvoiceNumber			= @InvoiceNumber,
--	Period					= @Period,
--	DatePayable				= CASE WHEN @StatusID IN (1, 2) AND VendorFirmID IS NOT NULL AND @VendorFirmID IS NOT NULL THEN GETDATE()+1 ELSE @DatePayable END,
--    InvoiceNotes			= @AdjustmentNotes,
--    StatusID				= @StatusID,
--    DateApproved			= @DateApproved,
--    ApprovedBy				= @ApprovedBy    
    
--Where DocumentID=@DocID

Select @DocID
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [power_user]
GO
