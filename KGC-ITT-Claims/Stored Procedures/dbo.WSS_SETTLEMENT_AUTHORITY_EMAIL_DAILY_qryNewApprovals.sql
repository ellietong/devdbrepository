SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals] 
AS
SET NOCOUNT ON
 
BEGIN
 
 
/*
	TEST VARS
*/
--DECLARE @SettlementAuthorityID bigint = 1
 
--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)
 
Declare @Today10AM datetime
Declare @Yesterday10AM datetime
SET @Yesterday10AM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-1))+' 10:00:00.000')
SET @Today10AM = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 10:00:00.000')
DECLARE @SendEmail bit = 0
	
	SET @Subject = 	'ITT Payment Approval Summary ' + CONVERT(varchar, getdate(), 107) 
 
	--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
	--KCIC logo is temporarily being pulled from D. Snyder's Google site	
 
 
	SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
 
 
 
 
	--Declare and populate the body of the email for individual firms
	DECLARE @BodyMessage nvarchar(MAX)
 
	--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
	--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
	SET @BodyMessage = @LogoHeader + 
	'<STYLE TYPE="text/css">
	<!--
	TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
	TD	
	{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
		
	}
	TH	
	{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
	UL {margin:0; 
		
	padding:0;}	
 
 
	--->
	</STYLE>'
 
 
 
	SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
	--CSS
	DECLARE @MainHeaderStyle nvarchar(MAX) = 'background-color: #0079C1; font-weight: bold; color: white; padding: 4px;'
	DECLARE @tdStyle nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'
 
IF(SELECT COUNT(*)
	FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
	INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID	
	LEFT join tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
	LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
			AND clst.ClaimLawsuitStatusID = 23
			AND cs.PaymentApprovedDate BETWEEN @Yesterday10AM AND @Today10AM
			AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
			AND ExecutedReleaseDate IS NOT NULL
			) > 0
BEGIN
	SET @SendEmail = 1
	
 
	--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
	set @BodyMessage = @BodyMessage + '<H4>The following settlement(s) have been approved for payment.<br/><br/>' + 
	'To view additional information please click ' + 
	'<a href="https://www.kcicextranet.com/clients/itt/ittcp/Pages/SettlementPaymentApproval.aspx?approval=">here</a>. </H4>'
 
	--PRINT TABLE HEADERS
	SET @BodyMessage = @BodyMessage + '
	<table>
		<thead>
			<tr>
				<th style="' + @MainHeaderStyle + '">Claimant</th>
				<th style="' + @MainHeaderStyle + '">Settlement Group</th>
				<th style="' + @MainHeaderStyle + '">Plaintiff Counsel</th>
				<th style="' + @MainHeaderStyle + '">Release Received</th>
				<th style="' + @MainHeaderStyle + '">Settlement Date</th>
				<th style="' + @MainHeaderStyle + '">Settlement Amount</th>
				
			</tr>
		</thead>
	'
	--PRINT TABLE OF CLAIMS
	SET @BodyMessage = @BodyMessage +
 
		COALESCE((CAST((SELECT 
			[td/@style] = @tdStyle,
			[td/a/@href] = 'https://www.kcicextranet.com/clients/itt/ittcp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, c.ClaimantPersonalInfoID)+'&claimanttab=1&claimtab=2',
			[td/a] = cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''),
			[td] = '', '',
			[td/@style] = @tdStyle,
			td = COALESCE(UPPER(SettlementGroup), 'Unknown'), '',
			[td/@style] = @tdStyle,
			td = COALESCE(PlaintiffCounsel, 'Unknown'), '',
			[td/@style] = @tdStyle,
			td = CONVERT(nvarchar, ExecutedReleaseDate, 101), '',
			[td/@style] = @tdStyle,
			td = CONVERT(nvarchar, SettlementDate, 101), '',
			[td/@style] = @tdStyle,
			td = '$' + CONVERT(nvarchar, CONVERT(money, settlementamount)), ''
			
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
		INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		INNER JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID	
		LEFT join tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
		LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
			AND clst.ClaimLawsuitStatusID = 23
			AND cs.PaymentApprovedDate BETWEEN @Yesterday10AM AND @Today10AM
			AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
			--AND ExecutedReleaseDate IS NOT NULL
			--AND cs.SettlementGroupID >= COALESCE(@SettlementGroupID, 0) AND cs.SettlementGroupID <= COALESCE(@SettlementGroupID, @MaxSettlementGroupID)
			--TODO: add logic here to make sure the settlements have received authority
	GROUP BY	cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''), 
				c.ClaimantPersonalInfoID,
				PlaintiffCounsel,
				SettlementDate,
				SettlementGroup,
				cs.PaymentApprovedBy,
				cs.PaymentApprovedDate,
				ClaimantSettlementID,
				ExecutedReleaseDate,
				SettlementAmount
	FOR XML PATH('tr'), type) AS NVARCHAR(MAX))), '')
 
	--CLOSE TABLE
	SET @BodyMessage = @BodyMessage + '</table><br>'
 
	
-- SECTION FOR RECOMMENDATIONS
	IF( SELECT COUNT(*) FROM tblDocuments d
		  LEFT JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
		  LEFT JOIN vtblDefenseInvoice dinv	  ON d.DocumentID = dinv.DocumentID 
		  INNER JOIN tblDefenseInvoiceStatus s ON COALESCE(di.StatusID, 3) = s.statusid
		  LEFT JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
		  LEFT JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalinfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
		  LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		  LEFT JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
		  LEFT JOIN tblPaymentDefense pd on pd.DefenseInvoiceID = di.DefenseInvoiceID
		WHERE IsApproved = 1 
		AND d.ClaimantDocumentTypeID IN (40, 41, 254) 
		AND pd.PaymentDefenseID IS NULL
		AND di.DatePaymentApproved BETWEEN @Yesterday10AM AND @Today10AM)>0
		
 
		BEGIN
		set @BodyMessage = @BodyMessage + '<br><H4>The following defense invoices are approved for payment.<br/><br/>' + 
		'To view additional information, please click ' + 
		'<a href="https://www.kcicextranet.com/clients/ITT/ittcp/Pages/Invoices.aspx">here</a>. </H4>'
		
		--PRINT TABLE HEADERS
		SET @BodyMessage = @BodyMessage + '
		<table> <table style="width:98%;max-width:800px">
			<thead>
				<tr> 
					<th style="' + @MainHeaderStyle + '">Firm</th>
					<th style="' + @MainHeaderStyle + '">Invoice Number</th>
					<th style="' + @MainHeaderStyle + '">Period</th>
					<th style="' + @MainHeaderStyle + '">Submit Date</th>
					<th style="' + @MainHeaderStyle + '">Approved Date</th>
					<th style="' + @MainHeaderStyle + '">Invoice Amount</th>
				</tr>
			</thead>
		'
		----PRINT TABLE OF CLAIMS
		SET @BodyMessage = @BodyMessage +
 
			COALESCE((CAST((SELECT 
				[td/@style] = @tdStyle,
				td = COALESCE(UPPER(Firm), 'Unknown'), '',
				[td/@style] = @tdStyle,
				td = COALESCE(di.InvoiceNumber, 'Unknown'), '',
				[td/@style] = @tdStyle,
				td = CONVERT(nvarchar, di.Period, 101), '',
				[td/@style] = @tdStyle,
				td = CONVERT(nvarchar, di.SubmitDate, 101), '',
				[td/@style] = @tdStyle,
				td = CONVERT(nvarchar, di.DateApproved, 101),'',
				[td/@style] = @tdStyle,
				td = '$' + CONVERT(nvarchar, CONVERT(money, InvoiceTotal)), ''
		FROM tblDocuments d
		  LEFT JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
		  LEFT JOIN vtblDefenseInvoice dinv	  ON d.DocumentID = dinv.DocumentID 
		  INNER JOIN tblDefenseInvoiceStatus s ON COALESCE(di.StatusID, 3) = s.statusid
		  LEFT JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
		  LEFT JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalinfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
		  LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		  LEFT JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
		  LEFT JOIN tblPaymentDefense pd on pd.DefenseInvoiceID = di.DefenseInvoiceID
		WHERE IsApproved = 1 
		AND d.ClaimantDocumentTypeID IN (40, 41, 254) 
		AND pd.PaymentDefenseID IS NULL
		AND di.DatePaymentApproved BETWEEN @Yesterday10AM AND @Today10AM
				--AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
		GROUP BY	Firm, di.InvoiceNumber, di.Period,di.SubmitDate, di.DateApproved, InvoiceTotal
		
		
		FOR XML PATH('tr'), type) AS NVARCHAR(MAX))), '')
		
		END
END
ELSE 
BEGIN
		  IF( SELECT COUNT(*) FROM tblDocuments d
		  LEFT JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
		  LEFT JOIN vtblDefenseInvoice dinv	  ON d.DocumentID = dinv.DocumentID 
		  INNER JOIN tblDefenseInvoiceStatus s ON COALESCE(di.StatusID, 3) = s.statusid
		  LEFT JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
		  LEFT JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalinfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
		  LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		  LEFT JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
		  LEFT JOIN tblPaymentDefense pd on pd.DefenseInvoiceID = di.DefenseInvoiceID
		WHERE IsApproved = 1 
		AND d.ClaimantDocumentTypeID IN (40, 41, 254) 
		AND pd.PaymentDefenseID IS NULL
		AND di.DatePaymentApproved BETWEEN @Yesterday10AM AND @Today10AM)>0
		
		BEGIN
		SET @SendEmail = 1
		--PRINT TABLE HEADERS
		set @BodyMessage = @BodyMessage + '<br><H4>The following defense invoices are approved for payment.<br/><br/>' + 
		'To view additional information, please click ' + 
		'<a href="https://www.kcicextranet.com/clients/ITT/ittcp/Pages/Invoices.aspx">here</a>. </H4>'
		
		SET @BodyMessage = @BodyMessage + '
		<table><table style="width:98%;max-width:800px">
			<thead>
				<tr>
					<th style="' + @MainHeaderStyle + '">Firm</th>
					<th style="' + @MainHeaderStyle + '">Invoice Number</th>
					<th style="' + @MainHeaderStyle + '">Period</th>
					<th style="' + @MainHeaderStyle + '">Submit Date</th>
					<th style="' + @MainHeaderStyle + '">Approved Date</th>
					<th style="' + @MainHeaderStyle + '">Invoice Amount</th>
				</tr>
			</thead>
		'
		----PRINT TABLE OF CLAIMS
		SET @BodyMessage = @BodyMessage +
 
			COALESCE((CAST((SELECT 
				[td/@style] = @tdStyle,
				td = COALESCE(UPPER(Firm), 'Unknown'), '',
				[td/@style] = @tdStyle,
				td = COALESCE(di.InvoiceNumber, 'Unknown'), '',
				[td/@style] = @tdStyle,
				td = CONVERT(nvarchar, di.Period, 101), '',
				[td/@style] = @tdStyle,
				td = CONVERT(nvarchar, di.SubmitDate, 101), '',
				[td/@style] = @tdStyle,
				td = di.DateApproved,'',
				[td/@style] = @tdStyle,
				td = '$' + CONVERT(nvarchar, CONVERT(money, InvoiceTotal)), ''
		FROM tblDocuments d
		  LEFT JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
		  LEFT JOIN vtblDefenseInvoice dinv	  ON d.DocumentID = dinv.DocumentID 
		  INNER JOIN tblDefenseInvoiceStatus s ON COALESCE(di.StatusID, 3) = s.statusid
		  LEFT JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
		  LEFT JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalinfoID
		  LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
		  LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		  LEFT JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
		  LEFT JOIN tblPaymentDefense pd on pd.DefenseInvoiceID = di.DefenseInvoiceID
		WHERE IsApproved = 1 
		AND d.ClaimantDocumentTypeID IN (40, 41, 254) 
		AND pd.PaymentDefenseID IS NULL
		AND di.DatePaymentApproved BETWEEN @Yesterday10AM AND @Today10AM
				--AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
		GROUP BY	Firm, di.InvoiceNumber, di.Period,di.SubmitDate, di.DateApproved, InvoiceTotal
		
		
		FOR XML PATH('tr'), type) AS NVARCHAR(MAX))), '')
		END	
END
--KCIC address and disclaimer
	SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
	733 10th Street, NW, Suite 3001<br>
	Washington, DC 20001<br>
	(t) 202-772-2300<br>
	(f) 202-772-2333'
 
--PRINT SubString(@BodyMessage, 1, 4000)
--PRINT SubString(@BodyMessage, 4001, 4000)
--PRINT SubString(@BodyMessage, 8001, 4000) 
 
DECLARE @RecipientList nvarchar(500)
SET @RecipientList='loree.shelko@itt.com;pgoldstein@mcguirewoods.com;gmacsteel@mcguirewoods.com;scottc@kcicllc.com;shockleym@kcicllc.com;chaudoinr@kcic.com;itt@kcicllc.com' --ITT's NCC and KCIC users
 
----QC Recipient List
--Set @Bodymessage = @BodyMessage + 'Recipients' + @RecipientList
--PRINT @RecipientList
 
--PRINT @SendEmail
 
IF(@SendEmail = 1)
BEGIN
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The Profile_Name sends from [client]@kcicllc.com email address
@profile_name='Claims',
@recipients = @RecipientList,
--@recipients = 'hagep@kcicllc.com',--;prossc@kcicllc.com',
@copy_recipients = '',
@blind_copy_recipients = 'itt@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
 
 
 
END
END
 



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals] TO [power_user]
GO
