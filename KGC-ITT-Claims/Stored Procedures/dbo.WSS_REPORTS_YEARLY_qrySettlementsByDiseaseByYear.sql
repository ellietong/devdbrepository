SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null,@StartDate datetime=null, @EndDate datetime=null, @emailaddress nvarchar(200) = null) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @YearID int
--Set @YearId=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @Settlement table (HighLevelDisease nvarchar(25), Count int, Settlement money, AverageIndemnity money)
INSERT INTO @Settlement
select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end as HighLevelDisease, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(SettlementAmount) as IndemnityPaid, sum(SettlementAmount)/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join (select * from tblClaimDisease where primarydisease=1) cd ON c.ClaimID=cd.ClaimID
inner join tblClaimantSettlement cs ON cs.ClaimLawsuitID=cl.ClaimLawsuitID
left join tblDisease d ON d.DiseaseID=cd.DiseaseID
left join tblHighLevelDisease hld ON hld.HighLevelDiseaseID=d.HighLevelDiseaseID
where	coalesce(SettlementDate,statusdate)between coalesce(@StartDate,'1/1/1900')
		and coalesce(@EndDate, '1/1/2100')
		and ClaimLawsuitStatusID in (23,25) 
		and  IsPrimaryStatus=1
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end

select hld.HighLevelDisease, coalesce(Count,0) AS Count, coalesce(Settlement,0) AS IndemnityPaid, coalesce(AverageIndemnity,0) AS AverageIndemnity
from tblHighLevelDisease hld
left join @Settlement s ON hld.HighLevelDisease=s.HighLevelDisease
where HighLevelDiseaseID !=6
order by COUNT desc

END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [power_user]
GO
