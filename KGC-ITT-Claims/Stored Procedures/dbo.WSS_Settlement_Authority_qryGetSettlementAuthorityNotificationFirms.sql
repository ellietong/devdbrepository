SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityNotificationFirms]
	-- Add the parameters for the stored procedure here
@SettlementAuthorityID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT snc.*,
	LName + COALESCE(', ' + FName, '') + ' - ' + Email + ' (' + dc.DefenseCounsel + ')' AS Firm
	FROM tblSettlementNotificationFirm snc
	INNER JOIN tblSettlementAuthorityNotificationFirm sanc on snc.SettlementNotificationFirmID = sanc.SettlementNotificationFirmID
	INNER JOIN vtblperson p on snc.personid = p.personid
	INNER JOIN tblDefenseCounsel dc ON p.FirmID = dc.DefenseCounselID
	where SettlementAuthorityID = @SettlementAuthorityID
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityNotificationFirms] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityNotificationFirms] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityNotificationFirms] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityNotificationFirms] TO [power_user]
GO
