SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qrysrptSiteNoticed]
(@correspondenceid int)
as
set nocount on
select sn.sitenoticedid, sn.correspondenceid, sn.siteid, s.sitename, s.sitecity, st.state, s.sitename + ' (' 
+ s.sitecity + ', ' + st.state + ')' as sitenameinfo
from tblsitenoticed sn inner join tblEnviroSitesDetail s on sn.SiteID = s.siteid
inner join tblstate st on s.stateid = st.stateid
where correspondenceid = @correspondenceid
order by sitename
return
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSiteNoticed] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptSiteNoticed] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSiteNoticed] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptSiteNoticed] TO [power_user]
GO
