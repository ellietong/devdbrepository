SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptBillingSummary]
(@BillExhaustionDateID int,  @BillAllocationID int, @AdjustmentExhaustionDateID int, @AdjustmentAllocationID int)
As
Set Nocount on



--Declare @BillExhaustionDateID int
--Declare @BillAllocationID int
--Declare @AdjustmentExhaustionDateID int
--Declare @AdjustmentAllocationID int
--Set @BillExhaustionDateID = 131
--Set @BillAllocationID = 12
--Set @AdjustmentExhaustionDateID = 130
--Set @AdjustmentAllocationID = 9

Declare @Header nvarchar(100)
Declare @ExhaustionTypeID int
Set @Header = (select exhaustiontype + ' - ' + description from tblexhaustiondate ed inner join tblexhaustiontype et on et.exhaustiontypeid = ed.exhaustiontypeid where exhaustiondateid = @BillExhaustionDateID)
Set @ExhaustionTypeID = (Select exhaustiontypeid from tblExhaustionDate where ExhaustionDateID = @BillExhaustionDateID group by ExhaustionTypeID)

--Set Policy Date Type
DECLARE @PolicyDateTypeID int
SET @PolicyDateTypeID = (Case When @ExhaustionTypeID=6 Then '2' Else '1' END)


------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------

Declare @CurrentAllocation Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint, OutsideConsumption money)
Insert Into @CurrentAllocation
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, e.PolicyDateID, 
IndemnityAllocation,DefenseAllocation , DefenseOutsideLimitsAllocation, 
SIRIndemnityAllocation, SIRDefenseAllocation, LedgerID, OutsideConsumption
From tblExhaustion E 
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Left Join tblOccurrence O on E.OccurrenceID = O.OccurrenceID
Where ExhaustionTypeID = @ExhaustionTypeID and ED.ExhaustionDateID = @BillExhaustionDateID and AllocationID = @BillAllocationID
and ConsumptionType<>'C'


Declare @CurrentAllocationTotals table (ExhaustionTypeID int, PolicyDateID int, Indemnity money, Defense money,
	DefenseOutsideLimits money, TotalDefense money, SIRIndemnityAllocation money, SIRDefenseAllocation money, OutsideConsumption money)
Insert Into @CurrentAllocationTotals
Select ExhaustionTypeID, PolicyDateID,

	coalesce(Sum(IndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(DefenseAllocation), 0) as Defense,
	coalesce(Sum(DefenseOutsideLimitsAllocation),0) as DefenseOutsideLimits,
	coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
	coalesce(Sum(SIRIndemnityAllocation), 0) as SIRIndemnity,
	coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense,
	coalesce(Sum(OutsideConsumption),0) as OutsideConsumption
	

From @CurrentAllocation
Group By ExhaustionTypeID, PolicyDateID
--Confirmed that this pulls correct indemnity total (excludes credits)


Declare @CurrentAdjustment Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	TotalAdjustment money)
Insert Into @CurrentAdjustment
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, e.PolicyDateID, 
coalesce(Sum(IndemnityAllocation+DefenseAllocation), 0) as TotalAdjustment
From tblExhaustion E 
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
--Left Join tblOccurrence O on E.OccurrenceID = O.OccurrenceID
Where ExhaustionTypeID = @ExhaustionTypeID and ED.ExhaustionDateID = @AdjustmentExhaustionDateID and AllocationID = @AdjustmentAllocationID
Group By ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, e.PolicyDateID

UNION ALL

Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, e.PolicyDateID, 
coalesce(Sum(IndemnityAllocation+DefenseAllocation), 0) as TotalAdjustment
from tblExhaustion e
inner join tblExhaustionDate ed on ed.ExhaustionDateID=e.ExhaustionDateID
where ExhaustionTypeID=@ExhaustionTypeID and e.ExhaustionDateID = @BillExhaustionDateID and ConsumptionType='C'
group by ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, e.PolicyDateID


Declare @CurrentAdjustmentAllocationTotals table (ExhaustionTypeID int, PolicyDateID int, TotalAdjustment money)
Insert Into @CurrentAdjustmentAllocationTotals
Select ExhaustionTypeID, PolicyDateID,	coalesce(Sum(TotalAdjustment), 0) as TotalAdjustment
From @CurrentAdjustment
Group By ExhaustionTypeID, PolicyDateID
--CONFIRM THIS BRINGS BACK CORRECT ADJUSTMENTS (LONDON AND INTEGRITY/NAIC CREDITS)

--Query Results
Declare @Results Table(OrderID int, OrderName nvarchar(100), policyid bigint, policydateid bigint, carriername nvarchar(100), policynum nvarchar(100), 
	pstartdate nvarchar(100), penddate  nvarchar(100),  Indemnity money, Defense money, OutsideConsumption money, 
	DefenseOutsideLimits money, TotalDefense money)
Insert Into @Results
																																			
----London
--select  '1' as OrderID, 'All Insurers', p.policyid, pd.PolicyDateID, CarrierName, policynum,  convert(nvarchar(100), pstartdate, 101) as pstartdate, 
--convert(nvarchar(100), PEndDate, 101)  as penddate,  sum(IndemnityAllocation) as Indemnity, sum(defenseallocation) as Defense, sum(outsideconsumption) as outsideconsumption, 
--sum(defenseoutsidelimitsallocation) as defenseoutsidelimits, sum(DefenseAllocation+defenseoutsidelimitsallocation) as TotalDefense
--from tblPolicyDates pd
--inner join tblPolicy p on p.PolicyID=pd.PolicyID
--inner join tblcarrier c on c.carrierid=p.carrierid
--inner join tblexhaustion e on e.policydateid=pd.policydateid
--where c.CarrierID=20 and exhaustiondateid = @BillExhaustionDateID and ConsumptionType <>'C'
--group by pd.PolicyDateID,  carriername, policynum, pstartdate, penddate, p.policyid


--UNION

--Most Carriers
select '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, CarrierName, 
p.policynum, convert(nvarchar(100), pstartdate, 101) as pstartdate, convert(nvarchar(100), PEndDate, 101)  as penddate,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(OutsideConsumption,0) as OutsideConsumption,
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense
 from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
--Where c.carrierid not in (10,20, 100000) 
Where c.carrierid not in (10, 100000) 


UNION

--Marley Producer
select '2' as OrderID, 'Producer', p.policyid, pd.policydateid, 'Marley-Wylain' as CarrierName, 
'Producer' as policynum, ' ' as pstartdate, ' ' as penddate, 	
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	''as OutsideConsumption,
	'' as DefenseOutsideLimits, 
	coalesce(Defense,0) as TotalDefense
from tblPolicyDates pd
inner join tblPolicy p on p.PolicyID=pd.PolicyID
inner join tblcarrier c on c.carrierid=p.carrierid
--inner join tblexhaustion e on e.policydateid=pd.policydateid
inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
where  pd.PolicyDateID=128



--Adjustment Results
Declare @Adjustment Table(OrderID int, OrderName nvarchar(100), policyid bigint, policydateid bigint, carriername nvarchar(100), policynum nvarchar(100), 
	pstartdate datetime, penddate datetime,  TotalAdjustment money)
insert into @Adjustment

----London
--select '1' as OrderID, 'All Insurers',  p.policyid, pd.PolicyDateID, CarrierName, policynum, pstartdate, penddate, sum(DefenseAllocation+defenseoutsidelimitsallocation+Indemnityallocation) as TotalAdjustment
--from tblPolicyDates pd
--inner join tblPolicy p on p.PolicyID=pd.PolicyID
--inner join tblcarrier c on c.carrierid=p.carrierid
--inner join tblexhaustion e on e.policydateid=pd.policydateid
--where c.CarrierID=20 and exhaustiondateid= @AdjustmentExhaustionDateID 
--group by pd.PolicyDateID,  carriername, policynum, pstartdate, penddate, p.policyid


--UNION

--Most Carriers
 select '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, CarrierName, 
p.policynum, pd.pstartdate, pd.penddate,
	coalesce(TotalAdjustment,0) as TotalAdjustment
 from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAdjustmentAllocationTotals CAAT on PD.PolicyDateID=CAAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
--Where c.carrierid not in (10,20, 100000)
Where c.carrierid not in (10, 100000)

UNION

--Marley Producer

 select '2' as OrderID, 'Producer', p.policyid, pd.policydateid, CarrierName, 
p.policynum, pd.pstartdate, pd.penddate, 	SUM(TotalAdjustment) as TotalAdjustment
 from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAdjustmentAllocationTotals CAAT on PD.PolicyDateID=CAAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	where  pd.policydateid=128 
group by  p.policyid, pd.policydateid, CarrierName, 
p.policynum, pd.pstartdate, pd.penddate


select r.OrderID, r.OrderName, r.policyid, r.policydateid, r.carriername, r.policynum, r.pstartdate, r.penddate,   SUM(Indemnity) AS Indemnity, SUM(Defense) AS Defense, SUM(OutsideConsumption) AS OutsideConsumption, 
SUM(DefenseOutsideLimits) AS DefenseOutsideLimits, SUM(TotalDefense) AS TotalDefense, @header as header1, SUM(a.TotalAdjustment) as TotalAdjustment
From @Results r
left join @Adjustment a on r.policydateid=a.policydateid
GROUP BY r.OrderID, r.OrderName, r.policyid, r.policydateid, r.carriername, r.policynum, r.pstartdate, r.penddate 
order by carriername, cast(r.pstartdate as date) , Cast(r.penddate as DATE)

return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillingSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBillingSummary] TO [power_user]
GO
