SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[Dec2013DocDownload]
AS

select  d.documentid, d.DocumentTitle, convert(nvarchar,d.documentid)+'_'+cpi.lastname+'_'+ltrim(rtrim(docketnumber))+'_'+convert(nvarchar, cl.ClaimLawsuitID), amazonkey, 'COMPLAINT' AS ClaimantDocumentType, Claimantdocumenttypeid, documenturl
from tblclaimlawsuit cl
inner join tblclaim c on c.claimid = cl.claimid
inner join tblclaimantpersonalinfo cpi on cpi.claimantpersonalinfoid = c.claimantpersonalinfoid
inner join tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
inner join tbllawsuit l on l.lawsuitid = cl.LawsuitID
inner join (select * from tblDefendantCompany
			where DefendantCompany like '%hoffman%' and clientid = 35 and DefendantCompany not like 'xylem%') dc on dc.DefendantCompanyid = a.DefendantCompanyID
inner join tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
inner join (select * from tbldocuments where ClaimantDocumentTypeID = 8) d on d.documentid = mcd.DocumentID
where coalesce(ReceivedDate, '1/1/1900')<'11/1/2012'
and l.lawsuitid not in (15486, 8156)
group by d.documentid,l.lawsuitid, DocketNumber, CaseCaption,  d.DocumentTitle, convert(nvarchar,d.documentid)+'_'+cpi.lastname+'_'+ltrim(rtrim(docketnumber))+'_'+convert(nvarchar, cl.ClaimLawsuitID), amazonkey, Claimantdocumenttypeid, documenturl
GO
