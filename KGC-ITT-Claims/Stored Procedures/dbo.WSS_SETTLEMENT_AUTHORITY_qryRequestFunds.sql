SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestFunds]
(@Emailaddress nvarchar(250) = null, 
 @SettlementGroupID bigint,
 @DemandAmount money, 
 @AuthorityAmount money, 
 @Comment nvarchar(MAX) = NULL,
 @ResponseDueDate date,
 @SettlementAuthorityID bigint,
 @ClaimLawsuitID bigint)
AS
BEGIN
	

	--INSERT CHILD SETTLEMENT AUTHORITY RECORD
	INSERT INTO tblSettlementAuthority
	(
	 SettlementGroupID, 
	 AuthorityRequestedBy, 
	 AuthorityRequestedDate, 
	 DemandAmount, 
	 AuthorityAmount, 
	 Comment, 
	 ResponseDueDate, 
	 ParentSettlementAuthorityID, 
	 ClaimLawsuitID
	 )

	SELECT  @SettlementGroupID, 
			@Emailaddress, 
			GETDATE(), 
			@DemandAmount, 
			@AuthorityAmount, 
			@Comment, 
			@ResponseDueDate, 
			@SettlementAuthorityID, 
			@ClaimLawsuitID
	
	DECLARE @NewSettlementAuthorityID int
	SET @NewSettlementAuthorityID = (SELECT MAX(SettlementAuthorityID) FROM tblSettlementAuthority)

EXEC	[dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryAdditionalFundsRequest]
		@SettlementAuthorityID = @NewSettlementAuthorityID


	IF (@AuthorityAmount > 50000)
	BEGIN
		INSERT INTO tblSettlementAuthorityApproverFirm
		(	SettlementAuthorityID,
			SettlementApproverFirmID, 
			ResponseWindowDays, 
			ApprovalRequired
		)
		SELECT	@NewSettlementAuthorityID,
				2,
				30,
				1													
	END
	ELSE --Only approve and send notification to insurers if ITT's approval is not required
	BEGIN

		UPDATE tblSettlementAuthority
		SET RequestApprovedDate = GETDATE(),
			IsRequestApproved = 1,
			RequestApprovedBy = 'ITT@KCIC.COM'
		WHERE SettlementAuthorityID = COALESCE(@SettlementAuthorityID, 0)

		EXEC	[dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryAdditionalFundsRequest_Filter]
				@SettlementAuthorityID = @NewSettlementAuthorityID
	END

	SELECT @NewSettlementAuthorityID
END






GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestFunds] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestFunds] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestFunds] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestFunds] TO [power_user]
GO
