SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmCheckAddAll] (@MatterID int, @FundingTypeID int, @CheckID int, @ExpertFirmID int = NULL, @PlaintiffCounselAddressID int = NULL, @DefenseCounselAddressID int = NULL)
AS
Set NoCount On

--DECLARE @MatterID int=1,@FundingTypeID int=2, @CheckID int=3243, @ExpertFirmID int = NULL, @PlaintiffCounselAddressID int = 379, @DefenseCounselAddressID int = NULL


Declare @SettlementCheck int
Set @SettlementCheck = (Select COUNT(*) from tblPaymentSettlement where CheckID = @CheckID)

Declare @DefenseCheck int
Set @DefenseCheck = (Select COUNT(*) from tblPaymentDefense where CheckID = @CheckID)

--Declare @LegalBillCheck int
--Set @LegalBillCheck = (Select COUNT(*) from tblPaymentLegalBill where CheckID = @CheckID)

Declare @ApprovedCheck int
Set @ApprovedCheck = (Select Case when ca.CheckID is not null Then 1 Else 0 End
	From tblCheck c Left Join tblCheckApproval ca on c.CheckID = ca.CheckID where c.CheckID = @CheckID)



If @FundingTypeID = 1 Begin --Defense Invoices

Declare @CurrentFirmID1 int
Set @CurrentFirmID1 = (@DefenseCounselAddressID)
Declare @AcceptableFirmID1 int
Set @AcceptableFirmID1  = (Select Coalesce(DefenseCounselAddressID,@CurrentFirmID1) From tblPaymentDefense pd Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID Where CheckID = @CheckID Group By Coalesce(DefenseCounselAddressID,@CurrentFirmID1))

	If ((@AcceptableFirmID1 = @CurrentFirmID1 AND @ApprovedCheck = 0) Or (@AcceptableFirmID1 is Null)) AND @SettlementCheck = 0 --AND @LegalBillCheck = 0 
	Begin
		Insert Into tblPaymentDefense (DefenseInvoiceID, CheckID, PaymentAmount)
		select di.DefenseInvoiceID, @CheckID, InvoiceTotal
		from vtblDefenseInvoice di
		left join tblDefenseInvoice div on div.DefenseInvoiceID = di.DefenseInvoiceID
		left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
					 a on a.DefenseInvoiceID = di.DefenseInvoiceID
		where DefenseCounselAddressID = @DefenseCounselAddressID 
				--and MatterID = @MatterID 
				AND div.DatePaymentApproved IS NOT NULL --and FundsRequestDate is not null
				and di.StatusID=6
		Group By di.DefenseInvoiceID, DefenseCounselAddressID, di.InvoiceNumber, di.Period, InvoiceTotal
		Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0
	End
	
End

If @FundingTypeID = 2 Begin -- Settlements

Declare @CurrentFirmID2 int
Set @CurrentFirmID2 = @PlaintiffCounselAddressID
Declare @AcceptableFirmID2 int
Set @AcceptableFirmID2 = (Select Coalesce(PlaintiffCounselAddressID, @CurrentFirmID2) From tblPaymentSettlement ps 
	Inner Join tblClaimantSettlement cs on cs.ClaimantSettlementID = ps.ClaimantSettlementID
	Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID 
	Where CheckID = @CheckID
	Group By Coalesce(PlaintiffCounselAddressID, @CurrentFirmID2))
	
	If (@AcceptableFirmID2 = @CurrentFirmID2 AND @ApprovedCheck = 0 AND @DefenseCheck = 0)-- AND @LegalBillCheck = 0) 
		Or (@AcceptableFirmID2 is Null AND @DefenseCheck = 0) --AND @LegalBillCheck = 0) 
	Begin
		Insert Into tblPaymentSettlement (ClaimantSettlementID, CheckID, PaymentAmount)
		Select cs.ClaimantSettlementID, @CheckID, SettlementAmount-SUM(Coalesce(PaymentAmount,0))
		From tblClaim c
			INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
			INNER JOIN tblClaimantSettlement cs			 ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
			INNER JOIN tblLawsuit l						 ON l.LawsuitID = cl.LawsuitID
			LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID
			LEFT JOIN (Select ClaimantSettlementID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentSettlement ps on ca.CheckID = ps.CheckID)
				 a on a.ClaimantSettlementID = cs.ClaimantSettlementID
		Where COALESCE(IsCurrentClaim,0) = 1
			AND c.MatterID = @MatterID
			AND l.PlaintiffCounselAddressID = @PlaintiffCounselAddressID
			AND c.ClaimSequenceID not in (4,2,3,11)
			--AND ExecutedReleaseDate <= GETDATE()
			AND cs.PaymentApprovedDate IS NOT NULL
		Group By cs.ClaimantSettlementID, SettlementAmount
		Having SettlementAmount-SUM(Coalesce(PaymentAmount,0)) <> 0
	End

End


----MAYBE FOR GOULDS??

--If @FundingTypeID = 4 Begin --General Invoices


--Declare @CurrentFirmID1 int
--Set @CurrentFirmID1 = (@ExpertFirmID)
--Declare @AcceptableFirmID1 int
--Set @AcceptableFirmID1 = (Select Coalesce(ExpertFirmID,@CurrentFirmID1) From tblPaymentDefense pd Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID Where CheckID = @CheckID Group By Coalesce(ExpertFirmID,@CurrentFirmID1))

--	If ((@AcceptableFirmID1 = @CurrentFirmID1 AND @ApprovedCheck = 0) Or (@AcceptableFirmID1 is Null)) AND @SettlementCheck = 0 AND @LegalBillCheck = 0 Begin
--		Insert Into tblPaymentDefense (DefenseInvoiceID, CheckID, PaymentAmount)
--		Select di.DefenseInvoiceID, @CheckID, InvoiceTotal-SUM(Coalesce(PaymentAmount,0))
--		from tblDefenseInvoice di
--			left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
--						 a on a.DefenseInvoiceID = di.DefenseInvoiceID
--		where ExpertFirmID = @ExpertFirmID and MatterID = @MatterID and FundsRequestDate is not null
--		Group By di.DefenseInvoiceID, InvoiceTotal
--		Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0
--	End

--End


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddAll] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddAll] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddAll] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddAll] TO [power_user]
GO
