SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestAuthority]
(@Emailaddress nvarchar(250) = null, 
 @SettlementGroupID bigint,
 @DemandAmount money, 
 @AuthorityAmount money, 
 @Comment nvarchar(MAX) = NULL, 
 @ClaimLawsuitID bigint,
 @ResponseDueDate date)
AS
BEGIN
		
	--CREATE SETTLEMENT RECORD (DATE IS NULL SINCE SETTLEMENT HAS NOT BEEN APPROVED)
	INSERT INTO tblSettlementAuthority
	(
     SettlementGroupID, 
	 AuthorityRequestedBy, 
	 AuthorityRequestedDate, 
	 DemandAmount, 
	 AuthorityAmount, 
	 Comment, 
	 ResponseDueDate, 
	 ClaimLawsuitID
	 )
	SELECT  @SettlementGroupID, 
			@Emailaddress, 
			GETDATE(), 
			@DemandAmount, 
			@AuthorityAmount, 
			@Comment, 
			@ResponseDueDate, 
			@ClaimLawsuitID
	
	DECLARE @SettlementAuthorityID int
	SET @SettlementAuthorityID = (SELECT MAX(SettlementAuthorityID) FROM tblSettlementAuthority)


	IF (@AuthorityAmount > 50000)
	BEGIN
		INSERT INTO tblSettlementAuthorityApproverFirm
		(	SettlementAuthorityID,
			SettlementApproverFirmID, 
			ResponseWindowDays, 
			ApprovalRequired
		)
		SELECT	@SettlementAuthorityID,
				2,
				30,
				1													
	END
	ELSE
	BEGIN

			UPDATE tblSettlementAuthority
			SET RequestApprovedDate = GETDATE(),
				IsRequestApproved = 1,
				RequestApprovedBy = 'ITT@KCIC.COM'
			WHERE SettlementAuthorityID = COALESCE(@SettlementAuthorityID, 0)

	END

	SELECT @SettlementAuthorityID
END






GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestAuthority] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestAuthority] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestAuthority] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRequestAuthority] TO [power_user]
GO
