SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryInsExpertFirm] (@ExpertFirm nvarchar(250),
												@Address1 nvarchar(250),
												@Address2 nvarchar(250),
												@City nvarchar(250),
												@StateID int,
												@Zip nvarchar(10),
												@TaxID nvarchar(50),
												@SPUsername nvarchar(250))
AS
SET NOCOUNT ON

INSERT INTO m_tblExpertFirm
	SELECT @ExpertFirm, @Address1, @Address2, @City, @StateID, @Zip, @TaxID, @SPUsername
	
SELECT MAX(ExpertFirmID) 
FROM m_tblExpertFirm

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryInsExpertFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryInsExpertFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryInsExpertFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryInsExpertFirm] TO [power_user]
GO
