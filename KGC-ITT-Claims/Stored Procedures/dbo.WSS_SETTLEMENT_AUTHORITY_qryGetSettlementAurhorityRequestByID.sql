SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetSettlementAurhorityRequestByID]
	-- Add the parameters for the stored procedure here
@SettlementAuthorityID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT	
					sa.ClaimLawsuitID,
					cs.SettlementGroupID,
					sa.SettlementAuthorityID,
					'$' + CONVERT(nvarchar, CAST(sa.DemandAmount AS money), 1) AS DemandAmount,
					'$' + CONVERT(nvarchar, CAST(sa.AuthorityAmount AS money), 1) AS AuthorityAmount, 					 
					REPLACE(sa.Comment, CHAR(13)+CHAR(10), '<br/>') AS Comment, --Replace newlines with <br/>s  
					SettlementGroup,
					 d.AmazonKey,
					 d.DocumentID,
					 d.DocumentSourceID,
					 DocumentTitle,
					 '' AS TempHash,
					 ResponseDueDate AS ResponseDueDate,
					 l.PlaintiffCounselAddressID,
					 PlaintiffCounsel
	FROM tblSettlementAuthority sa
		INNER JOIN tblClaimantSettlement cs ON sa.ClaimLawsuitID = cs.ClaimLawsuitID
		INNER JOIN tblSettlementGroup sg ON cs.SettlementGroupID = sg.SettlementGroupID
		INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN tblDocuments d ON sa.DocumentID = d.DocumentID
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE SettlementAuthorityID = @SettlementAuthorityID
END





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetSettlementAurhorityRequestByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetSettlementAurhorityRequestByID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetSettlementAurhorityRequestByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetSettlementAurhorityRequestByID] TO [power_user]
GO
