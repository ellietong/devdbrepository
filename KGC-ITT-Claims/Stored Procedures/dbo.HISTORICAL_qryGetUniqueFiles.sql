SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HISTORICAL_qryGetUniqueFiles]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT DOCUMENT_TITLE AS DocumentTitle, 
			Document_Number AS DocumentNumber,
			COUNT(DISTINCT [FILE_NAME]) AS NumberOfPages
    FROM [FINAL_DocXRef_ITT]
    WHERE DOCUMENT_TITLE IN (	SELECT DOCUMENT_TITLE 
								FROM FINAL_DocXRef_ITT 
								GROUP BY DOCUMENT_TITLE 
								HAVING COUNT(Document_Number) > 1)
    GROUP BY DOCUMENT_TITLE, DOCUMENT_NUMBER
    ORDER BY DocumentTitle
    
    
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[HISTORICAL_qryGetUniqueFiles] TO [base_user]
GRANT EXECUTE ON  [dbo].[HISTORICAL_qryGetUniqueFiles] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[HISTORICAL_qryGetUniqueFiles] TO [power_user]
GRANT EXECUTE ON  [dbo].[HISTORICAL_qryGetUniqueFiles] TO [power_user]
GO
