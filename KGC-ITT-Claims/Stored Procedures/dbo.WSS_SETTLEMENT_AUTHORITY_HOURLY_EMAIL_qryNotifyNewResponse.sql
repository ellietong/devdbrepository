SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_HOURLY_EMAIL_qryNotifyNewResponse] 
AS

SET NOCOUNT ON
BEGIN 

--Executed in External ITT Settlement Authority Hourly Alerts job
--Sends response details to SA creator, parent response creator (if applicable), and contacts

DECLARE @SettlementAuthorityResponseID BIGINT

DECLARE Response_Curs CURSOR Local Fast_Forward for
	SELECT DISTINCT SettlementAuthorityResponseID
	FROM tblSettlementAuthorityResponse
	WHERE ResponseNotificationDate IS NULL

OPEN Response_Curs
FETCH NEXT FROM Response_Curs INTO @SettlementAuthorityResponseID

WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC dbo.WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyNewResponse @SettlementAuthorityResponseID
	FETCH NEXT FROM Response_Curs INTO @SettlementAuthorityResponseID
END

CLOSE Response_Curs
DEALLOCATE Response_Curs

END
GO
