SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryInvoiceYearsExtended]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @Year int
    SET @Year = 2003
    
    DECLARE @Years TABLE ([Year] int)
    WHILE @Year <= YEAR(GETDATE())
    BEGIN
		INSERT INTO @Years
		SELECT @Year
		SET @Year = @Year + 1
    
    END
    SELECT NULL AS Year
    UNION
    SELECT [YEAR] FROM @Years
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInvoiceYearsExtended] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInvoiceYearsExtended] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInvoiceYearsExtended] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInvoiceYearsExtended] TO [power_user]
GO
