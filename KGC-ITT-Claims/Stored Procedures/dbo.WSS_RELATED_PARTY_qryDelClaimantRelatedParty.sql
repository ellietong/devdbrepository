SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROCEDURE [dbo].[WSS_RELATED_PARTY_qryDelClaimantRelatedParty]   
(@ClaimantRelatedPartyID int, @Emailaddress nvarchar(250))
AS
SET NOCOUNT ON

UPDATE tblClaimantRelatedParty
SET SPUserName = @emailaddress
WHERE ClaimantRelatedPartyID = @ClaimantRelatedPartyID

DELETE FROM tblClaimantRelatedParty
WHERE ClaimantRelatedPartyID = @ClaimantRelatedPartyID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryDelClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryDelClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryDelClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryDelClaimantRelatedParty] TO [power_user]
GO
