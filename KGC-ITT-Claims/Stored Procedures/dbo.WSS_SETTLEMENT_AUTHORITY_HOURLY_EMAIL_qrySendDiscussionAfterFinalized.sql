SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_HOURLY_EMAIL_qrySendDiscussionAfterFinalized] 
AS
SET NOCOUNT ON

BEGIN

--Executed in External ITT Settlement Authority Hourly Alerts job
--Sends response details to SRA contacts in first email call, and approvers in second ("..._Filter") email call

DECLARE @SettlementAuthorityID BIGINT

DECLARE Discussion_Curs CURSOR Local Fast_Forward FOR
	SELECT DISTINCT sa.SettlementAuthorityID
	FROM tblSettlementAuthority sa 
		INNER JOIN (SELECT saaf.SettlementAuthorityID, 
						COUNT(DISTINCT saf.FirmID) AS RequiredApprovals, 
						COUNT(DISTINCT sar.FirmID) AS ApprovalsReceived
					FROM tblSettlementAuthorityApproverFirm saaf
						INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID
						LEFT  JOIN (SELECT DISTINCT SettlementAuthorityID, FirmID 
									FROM tblSettlementAuthorityResponse sar 
									WHERE SettlementAuthorityStatusID IN (1,2,5)
										AND FirmID <> 277) sar ON saaf.SettlementAuthorityID = sar.SettlementAuthorityID
					WHERE saf.FirmID <> 277
					GROUP BY saaf.SettlementAuthorityID) SettlementAuthorityApprovals ON SettlementAuthorityApprovals.SettlementAuthorityID = sa.SettlementAuthorityID
	WHERE RequiredApprovals = ApprovalsReceived
		AND COALESCE(AuthorityGranted,0) = 0
		AND COALESCE(AuthorityDenied, 0) = 0
		AND COALESCE(sa.IsRequestApproved,0) = 1
		AND sa.FinalizedNotificationDate IS NULL
		
OPEN Discussion_Curs
FETCH NEXT FROM Discussion_Curs INTO @SettlementAuthorityID
		
WHILE @@FETCH_STATUS = 0
BEGIN
	UPDATE tblSettlementAuthority
		SET AuthorityGranted = 1
		WHERE SettlementAuthorityID = @SettlementAuthorityID

	EXEC dbo.WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval @SettlementAuthorityID
	EXEC dbo.WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval_Filter @SettlementAuthorityID
	
	FETCH NEXT FROM Discussion_Curs INTO @SettlementAuthorityID
END

CLOSE Discussion_Curs
DEALLOCATE Discussion_Curs	

END
GO
