SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[WSS_INVOICES_qryAvailableFunds]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
Declare @MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime
Set @MatterID = 1
Set @BalanceStartDate = '8/1/2010'
Set @BalanceEndDate = GETDATE()

Declare @StartingBalance money
Set @StartingBalance = (
Coalesce((Select SUM(CashActivityAmount) From tblMiscCashActivity mca 
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID 
where CashActivityDate >= '1/1/2010' and CashActivityDate < @BalanceStartDate and MatterID = @MatterID),0)
+
Coalesce((Select SUM(SourceAmount)*-1 From tblCheck c
Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
Where SourceDate >= '1/1/2010' and SourceDate < @BalanceStartDate and MatterID = @MatterID),0)
)

Declare @Matter nvarchar(250)
Set @Matter = (Select Matter from tblMatter where MatterID = @MatterID)

Declare @Detail table (Indemnity money, Defense money, MiscFees money, CashReceipts money, OutstandingUnapprovedAmount money, ApprovedNotPaidAmount money)
Insert Into @Detail

--Defense
Select 0, SourceAmount*-1 as SourceAmount, 0, 0, 0, 0
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentDefense pd on pd.CheckID = c.CheckID
	Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
	Inner Join tblDefenseInvoiceFirm dif on dif.FirmID = di.FirmID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @BalanceStartDate and SourceDate <= @BalanceEndDate) AND c.MatterID = @MatterID AND c.FundingTypeID = 1
Group by SourceAmount*-1

Union ALL

--Settlement
Select SourceAmount*-1 as SourceAmount, 0, 0, 0, 0, 0
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
	Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @BalanceStartDate and SourceDate <= @BalanceEndDate) AND c.MatterID = @MatterID AND c.FundingTypeID = 2
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, PlaintiffCounsel, IsCheckCleared

Union ALL

Select  0, 0, CashActivityAmount, 0, 0, 0
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @BalanceStartDate and CashActivityDate <= @BalanceEndDate) AND mca.MatterID = @MatterID AND CashActivityAmount < 0

Union ALL

Select 0, 0, 0, CashActivityAmount, 0, 0
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @BalanceStartDate and CashActivityDate <= @BalanceEndDate) AND mca.MatterID = @MatterID AND CashActivityAmount > 0

Union ALL

Select  0, 0, 0, Coalesce(@StartingBalance,0), 0, 0

UNION ALL
 
--outstanding, unapproved invoices
SELECT 0, 0, 0, 0, COALESCE(invoicetotal, 0), 0
FROM tblDefInvClaimantDetail dicd
INNER JOIN vtblDefenseInvoice d ON dicd.DocumentID = d.DocumentID
-- FROM tblDefenseInvoice di
--WHERE CheckNumber IS NULL AND DatePaid IS NULL AND
WHERE COALESCE(d.StatusID, 0) NOT IN (1, 2)

UNION ALL

--approved but not yet paid
SELECT 0, 0, 0, 0, 0, COALESCE(invoicetotal, 0)
FROM vtblDefenseInvoice di
INNER JOIN tblDocuments d ON di.DocumentID = d.DocumentID
WHERE SourceNumber IS NULL AND SourceDate IS NULL

Select SUM(COALESCE(Indemnity, 0) + COALESCE(Defense, 0) + COALESCE(MiscFees, 0) + COALESCE(CashReceipts, 0)) AS AvailableFunds, SUM(OutstandingUnapprovedAmount) AS OutstandingUnapprovedAmount, SUM(ApprovedNotPaidAmount) AS ApprovedNotPaidAmount from @Detail
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryAvailableFunds] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryAvailableFunds] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryAvailableFunds] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryAvailableFunds] TO [power_user]
GO
