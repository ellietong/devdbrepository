SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_uHistoricalUpload]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimantDocumentID AS DocumentID, DocumentURL, DocumentTitle, d.ClaimantDocumentTypeID, ClaimantDocumentType,
	'' AS DefenseCounsel, '' as DefenseCounselID
	FROM tblClaimantDocuments d
	INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID

	WHERE DocumentURL <> DocumentLink
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_uHistoricalUpload] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_uHistoricalUpload] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_uHistoricalUpload] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_uHistoricalUpload] TO [power_user]
GO
