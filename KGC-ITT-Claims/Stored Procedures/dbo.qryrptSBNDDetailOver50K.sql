SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptSBNDDetailOver50K]

As


select PlaintiffCounsel, cpi.ClaimantPersonalInfoID, ClaimSequence, LastName, FirstName, SocialSecurityNumber, Disease, FileDate, DocketNumber, s.State, Jurisdiction, SettlementDate, SettlementAmount
from tblClaimantPersonalInfo cpi
left join tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
left join tblClaimDisease cd on cd.ClaimID=c.ClaimID
left join tblDisease d on d.DiseaseID=cd.DiseaseID
left join tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
left join tblLawsuit l on l.LawsuitID=cl.LawsuitID
left join tblState s on s.StateID = l.StateID
left join tblJurisdiction j on j.JurisdictionID=l.JurisdictionID
left join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
left join tblClaimSequence cseq on cseq.ClaimSequenceID=c.ClaimSequenceID
left join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=cl.ClaimLawsuitID
left join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
where ClaimLawsuitStatusID = 25 and coalesce(PrimaryDisease,1) = 1
and SettlementAmount > 50000
order by 1


Return




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSBNDDetailOver50K] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSBNDDetailOver50K] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSBNDDetailOver50K] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSBNDDetailOver50K] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSBNDDetailOver50K] TO [power_user]
GO
