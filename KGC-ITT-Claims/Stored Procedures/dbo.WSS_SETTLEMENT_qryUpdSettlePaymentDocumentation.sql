SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] (@SetPymtID int, 
																	@SettlePaymentComments nvarchar(max), 
																	@ReleaseTypeID int,
																	@ExecutedReleaseDate date, 
																	@MedicareReported bit ,
																	@MedicareFullyDocumented bit
																	--@ReleaseToPC datetime,
																	--@ReleaseRqstDate date)
																	)
AS

--DECLARE @ClaimID int
--SET @ClaimID = 1

UPDATE tblSettlePaymentDocumentation
	SET SettlePaymentComments = @SettlePaymentComments, 
		ReleaseTypeID = CASE WHEN @ReleaseTypeID = 0 THEN NULL ELSE @ReleaseTypeID END, 
		ExecutedReleaseDate = NULLIF(@ExecutedReleaseDate,''),
		--ReleaseRqstDate = NULLIF(@ReleaseRqstDate,'')
		MedicareReported = @MedicareReported,
		MedicareFullyDocumented = @MedicareFullyDocumented
		--ReleaseToPC = NULLIF(@ReleaseToPC, '')
	WHERE SetPymtID = @SetPymtID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GO
