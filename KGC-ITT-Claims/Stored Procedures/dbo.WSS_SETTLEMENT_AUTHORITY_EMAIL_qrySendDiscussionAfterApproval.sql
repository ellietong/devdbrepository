SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval] 
(@SettlementAuthorityID bigint)
AS

SET NOCOUNT ON
---SET XACT_ABORT ON
BEGIN
--DECLARE @SettlementAuthorityID bigint = 85


/* Query run once settlement authority is granted by all parties
*/
--SET ANSI_WARNINGS ONFF 
--Declare and set email inputs.
Declare @Subject nvarchar(MAX)
Declare @LogoHeader nvarchar(MAX)

DECLARE @SettlementGroupID int = (SELECT sa.SettlementGroupID
										FROM tblSettlementAuthority sa
										WHERE sa.SettlementAuthorityID = @SettlementAuthorityID)
DECLARE @SettlementGroupInfo NVARCHAR(MAX) = (
SELECT FirstName + ' ' + LastName + ' - ' + PlaintiffCounsel + ' (' + COALESCE(jurisdiction, 'UNKNOWN') + ', '  + COALESCE(s.State, 'UNKNOWN') + ') ' AS Claimant
FROM tblSettlementAuthority sa 
	INNER JOIN tblClaimantSettlement cs ON sa.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblSettlementGroup sg ON cs.SettlementGroupID = sg.SettlementGroupID
	INNER JOIN tblClaimLawsuit cl on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblLawsuit l on cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaim c on cl.ClaimID = c.ClaimID
	INNER JOIN tblPlaintiffCounselAddress pca on l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	INNER JOIN tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblState s on l.StateID = s.StateID
	LEFT JOIN tblJurisdiction j on l.JurisdictionID = j.JurisdictionID
WHERE SettlementAuthorityID = @SettlementAuthorityID
)

SET @Subject = 	'ITT Settlement Authority Finalized ' + CONVERT(varchar, getdate(), 107) + ': ' + COALESCE(@SettlementGroupInfo,'') 

--PRINT @Subject
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
padding:0;}	
--->
</STYLE>'
--SELECT @BodyMessage

--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
set @BodyMessage = @BodyMessage + '<H4>Settlement authority has been finalized for the following settlement group.  Authority correspondence is included below: </H4>'

--Print SettlementAuthority table first

--Recursively create response thread
DECLARE @out nvarchar(MAX)
EXEC	[dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryPrintAllResponses]
		@SettlementAuthorityID = @SettlementAuthorityID,
		@out = @out OUTPUT
--Print SettlementAuthority table row first...
DECLARE @MainHeaderStyle nvarchar(MAX) = 'background-color: #0079C1; font-weight: bold; color: white; padding: 4px;'
DECLARE @tdStyle nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'

DECLARE @newLine nvarchar(5) = CHAR(13) + CHAR(10)
	
SET @BodyMessage = @BodyMessage + '
	<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
		<tr>
			<th style="' + @MainHeaderStyle + '" Colspan="2">Settlement Authority</th>
		</tr>
		'
SET @BodyMessage = @BodyMessage + (
SELECT (select [td/@style] = @tdStyle, field as 'td' for xml path(''), type), (select [td/@style] = @tdStyle, Information as 'td' for xml path(''), type)
from (
select Field, Information
from (
		SELECT DISTINCT 
			    CONVERT(nvarchar(max),SettlementGroup) AS [Settlement Group],
			    CONVERT(nvarchar(max), sa.Comment) AS Comments,
			  '$ ' + CONVERT(nvarchar(max), DemandAmount, 1) AS [Demand Amount],
			  '$ ' + CONVERT(nvarchar(max), AuthorityAmount, 1) AS [Authority Amount],
			   CONVERT(nvarchar(max), [status]) AS [Status],
			   CONVERT(nvarchar(max), AuthorityRequestedDate, 101) AS [Authority Requested Date],
			   CONVERT(nvarchar(max), AuthorityRequestedBy) AS [Authority Requested By]
		FROM tblSettlementAuthority sa
		INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
		LEFT JOIN tblClaimantSettlement cs ON sg.SettlementGroupID = cs.SettlementGroupID
		LEFT JOIN  vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Unfiltered fra ON sa.SettlementAuthorityID = fra.SettlementAuthorityID
		WHERE sa.SettlementAuthorityID = @SettlementAuthorityID
		GROUP BY	sg.SettlementGroup,
					sa.Comment, 
					CONVERT(nvarchar(max), DemandAmount, 1),
					CONVERT(nvarchar(max), AuthorityAmount, 1),
					[Status],
					CONVERT(nvarchar(max), AuthorityRequestedDate, 101),
					AuthorityRequestedBy
) ss
unpivot (Information FOR Field in ( [settlement group], comments, [demand amount], [Authority Amount], [Status], [Authority Requested Date], [Authority Requested By]
	   )) as unpv ) tbl FOR XML PATH('tr'))

SET @BodyMessage = @BodyMessage + '
	</tr>
</table>
<br/>
<br/>'

SET @BodyMessage = @BodyMessage + '
<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
	<tr>
		<td colspan="7">' + COALESCE(@out, '') + '
		</td>
	</tr>
</table>'

SET @BodyMessage = @BodyMessage + '<br/><H4>To view additional information regarding this authority request click ' + 
	'<a href="https://www.kcicextranet.com/clients/ITT/ittcp/Pages/SettlementAuthority.aspx?SettlementAuthorityID=' + CONVERT(nvarchar, @SettlementAuthorityID) + '">here</a>. </H4>'

--SELECT SubString(@BodyMessage, 1, 4000)
--SELECT SubString(@BodyMessage, 4001, 4000)
--Create distinct semi-colon delimited list of people involved in discussion (or expected to
--in the case of insurers who do not respond)



--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + 
'
    <table style="width: 500px;" border="0" cellpadding="4" cellspacing="5" width="500">
        <tbody>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    <span style="color: #F06021; font-weight: bold;">KCIC Claims Administration</span><br />
                    </td>
            </tr>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    <img src="http://kcic.com/i/kcic-logo.png" width="103" height="93" />
                </td>
            </tr>
            <tr>
                <td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
                    color: #737271;">
                    733 10th Street, NW | Suite 3001 | Washington, DC 20001
                <br />
                    <a href="mailto:Claims@kcic.com" style="color: #737271;">Claims@kcic.com</a>
                    |<span style="font-family: Arial, Helvetica, sans-serif; color: #F06021"> </span>202.772.2300
                    | <a href="http://www.kcic.com" target="_new"
                        style="color: #FF6229">kcic.com</a>
                </td>
            </tr>
        </tbody>
    </table>
'

 
--Grab list of distinct email addresses of settlement approve contacts
DECLARE @RecipientList nvarchar(MAX)
SET @RecipientList=(
SELECT DISTINCT Email + '; ' AS [text()]
            FROM tblSettlementAuthorityApproverFirm saaf
				INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID				
				LEFT JOIN (
				SELECT DISTINCT sau.PersonID, Email, SettlementAuthorityID
				FROM tblSettlementAuthority sa
					INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
					LEFT JOIN tblClaimantSettlement cs ON sg.SettlementGroupID = cs.SettlementGroupID
					INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
					INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID			 		
					INNER JOIN tblSRAContact sc on l.StateID = sc.StateID
					INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON sc.PersonID = sau.PersonID
				WHERE sa.SettlementAuthorityID = @SettlementAuthorityID) SRA ON saaf.SettlementAuthorityID = SRA.SettlementAuthorityID
            For XML PATH (''))

SET @RecipientList = @RecipientList + COALESCE((SELECT DISTINCT sau.Email + '; ' AS [text()]
            From tblSettlementAuthorityNotificationContact sanc
            INNER JOIN tblSettlementNotificationContact snc ON sanc.SettlementNotificationContactID = snc.SettlementNotificationContactID
            INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON snc.PersonID = sau.PersonID
            Where sanc.SettlementAuthorityID = @SettlementAuthorityID
            For XML PATH ('')), ' ')

SET @RecipientList = @RecipientList + (SELECT AuthorityRequestedBy FROM tblSettlementAuthority WHERE SettlementAuthorityID = @SettlementAuthorityID)

DECLARE @CCList nvarchar(MAX) = 'loree.shelko@itt.com; Craig.Johnson@itt.com;'

--QC Recipient List
Set @Bodymessage = @BodyMessage + 'Recipients: ' + @RecipientList  + '</br>CC: ' + @CCList

----Execute stored procedure to send the email to that firm
--EXEC msdb.dbo.sp_send_dbmail
--@profile_name='Claims',
--@recipients = @RecipientList,
--@copy_recipients = @CCList,
--@blind_copy_recipients = 'itt@kcic.com',
--@subject = @subject,
--@body = @BodyMessage,
--@body_format = 'html'

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
@profile_name='Claims',
@recipients = 'snyderd@kcic.com',
@copy_recipients = '',
@blind_copy_recipients = '',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

SET ANSI_WARNINGS ON
--SET XACT_ABORT OFF

END






GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qrySendDiscussionAfterApproval] TO [power_user]
GO
