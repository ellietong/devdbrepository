SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_HOURLY_EMAIL_qryAdditionalFundsRequest_Filter] 
AS

SET NOCOUNT ON
BEGIN

--Executed in External ITT Settlement Authority Hourly Alerts job
--Sends additional funds request details alongside original (ie parent) request details to approving insurers
--Note that this does NOT currently check for $50,000 being exceeded so may want to revisit (might be handled on entry of SA records, not sure)

DECLARE @SettlementAuthorityID BIGINT

DECLARE FilteredDiscussion_Curs CURSOR Local Fast_Forward for
	SELECT DISTINCT SettlementAuthorityID
	FROM tblSettlementAuthority
	WHERE ParentSettlementAuthorityID IS NOT NULL
		AND COALESCE(IsRequestApproved, 0) = 1
		AND InsurerNotificationDate IS NULL
		AND COALESCE(AuthorityGranted,0) <> 1 
		AND COALESCE(AuthorityDenied,0) <> 1

OPEN FilteredDiscussion_Curs
FETCH NEXT FROM FilteredDiscussion_Curs INTO @SettlementAuthorityID

WHILE @@FETCH_STATUS = 0
BEGIN		
	EXEC dbo.WSS_SETTLEMENT_AUTHORITY_EMAIL_qryAdditionalFundsRequest_Filter @SettlementAuthorityID
	FETCH NEXT FROM FilteredDiscussion_Curs INTO @SettlementAuthorityID
END

CLOSE FilteredDiscussion_Curs
DEALLOCATE FilteredDiscussion_Curs

END
GO
