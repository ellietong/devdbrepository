SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICE_qryVendorInvoiceFirmList]
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
    
        SELECT  VendorFirmID ,
                UPPER(VendorFirm) AS VendorFirm
        FROM    m_tblVendorFirm
        UNION
        SELECT  DefenseCounselID ,
                COALESCE(UPPER(DefenseCounsel), 'Unknown')
        FROM    tblDefenseCounsel
        --UNION
        --SELECT  NULL , ''
        UNION
        SELECT  0 ,
                '<<No Vendor Firm>>'
        FROM    m_tblVendorFirm
        ORDER BY UPPER(VendorFirm)
	
    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryVendorInvoiceFirmList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryVendorInvoiceFirmList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryVendorInvoiceFirmList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryVendorInvoiceFirmList] TO [power_user]
GO
