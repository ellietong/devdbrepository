SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityApproverContacts]
	-- Add the parameters for the stored procedure here
@SettlementAuthorityID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sac.*,
	LName + COALESCE(', ' + FName, '') + ' - ' + Email + ' (' + dc.DefenseCounsel + ')' AS Contact
	FROM tblSettlementApproverContact sac
	INNER JOIN tblSettlementAuthorityApproverContact saac on sac.SettlementApproverContactID = saac.SettlementApproverContactID
	INNER JOIN vtblperson p on sac.personid = p.personid
	INNER JOIN tblDefenseCounsel dc ON p.FirmID = dc.DefenseCounselID
	WHERE SettlementAuthorityID = @SettlementAuthorityID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityApproverContacts] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityApproverContacts] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityApproverContacts] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityApproverContacts] TO [power_user]
GO
