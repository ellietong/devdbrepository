SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryAutomaticApproval] 
AS

BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

--NOTE: Executed in External ITT Settlement Authority 9AM, 12PM, 2PM, 4PM Alerts job

--Pull all settlement authority requests from firms other than ITT that are missing approval/rejection/deemed approval, where they are not yet granted authority and not overdue
--NOTE: The subquery for settlement authority response intentionally ignores questions/comments (StatusID 4)
DECLARE @AuthorityRequestsMissingResponses TABLE (SettlementAuthorityID int, 
												AuthorityRequestedDate datetime,
												AuthorityAmount money,
												FirmID int,
												InsurerNotificationDate datetime,
												ResponseDeadline datetime)

INSERT INTO @AuthorityRequestsMissingResponses
	SELECT SA.SettlementAuthorityID, AuthorityRequestedDate, AuthorityAmount, saf.FirmID, InsurerNotificationDate, dbo.fnDeadlineDate(InsurerNotificationDate, saf.FirmID, AuthorityAmount)
	FROM tblsettlementauthority sa
		INNER JOIN tblSettlementAuthorityApproverFirm saaf on sa.SettlementAuthorityID = saaf.SettlementAuthorityID
		INNER JOIN tblSettlementApproverFirm saf on saaf.SettlementApproverFirmID = saf.settlementapproverfirmid
		LEFT JOIN ( SELECT sar.SettlementAuthorityID, sau.FirmID 
					FROM tblSettlementAuthorityResponse sar
						INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau on sar.CreatedBy = sau.Email
					WHERE SettlementAuthorityStatusID IN (1, 2, 5)) sar on sa.SettlementAuthorityID = sar.SettlementAuthorityID and saf.FirmID = sar.FirmID
	WHERE sar.SettlementAuthorityID IS NULL  
		AND saaf.ApprovalRequired = 1
		AND saf.FirmID <> 277
		AND COALESCE(AuthorityGranted, 0) = 0
		AND InsurerNotificationDate < dbo.fnDeadlineDate(InsurerNotificationDate, saf.FirmID, AuthorityAmount)

DECLARE @SettlementAuthorityID int, @AuthorityRequestDate datetime, @AuthorityAmount money, @FirmID int, @InsurerNotificationDate datetime, @ResponseDeadline datetime
DECLARE deadline_curs CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY FOR
	SELECT * 
	FROM @AuthorityRequestsMissingResponses 
	WHERE ResponseDeadline IS NOT NULL

OPEN deadline_curs
FETCH NEXT FROM deadline_curs INTO @SettlementAuthorityID, @AuthorityRequestDate, @AuthorityAmount, @FirmID, @InsurerNotificationDate, @ResponseDeadline

WHILE @@FETCH_STATUS=0
BEGIN 
	IF @ResponseDeadline <= GETDATE() --NOTE: This should probably be moved to the initial temp table query, we should revisit this
	BEGIN
		INSERT INTO tblSettlementAuthorityResponse (Comment, CreatedBy, DateCreated, ParentSettlementAuthorityResponseID, SettlementAuthorityID, SettlementAuthorityStatusID)
			SELECT 'Deemed Approved'                        AS Comment,
				CASE WHEN @FirmID = 63 
					 THEN 'Jens.Fog@brandywineholdings.com' 
					 ELSE 'sliwicp@nationwide.com' END      AS CreatedBy, --If it's ACE use Jens otherwise use Paul (this will need to change when more insurers are involved) 
				GETDATE()                                   AS DateCreated, 
				NULL                                        AS ParentSettlementAuthorityResponseID,
				@SettlementAuthorityID                      AS SettlementAuthorityID, 
				5                                           AS SettlementAuthorityStatusID --Deemed approved
	END

	FETCH NEXT FROM deadline_curs INTO @SettlementAuthorityID, @AuthorityRequestDate, @AuthorityAmount, @FirmID, @InsurerNotificationDate, @ResponseDeadline
END

CLOSE deadline_curs
DEALLOCATE deadline_curs

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryAutomaticApproval] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryAutomaticApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryAutomaticApproval] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryAutomaticApproval] TO [power_user]
GO
