SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryPrintKids]
(@SettlementAuthorityResponseID bigint, @out nvarchar(MAX) OUTPUT)
AS
BEGIN
	DECLARE @Responses TABLE (Firm nvarchar(500),
							SettlementAuthorityResponseID bigint, 
							SettlementAuthorityStatus nvarchar(100), 
							Comment nvarchar(MAX), 
							ParentSettlementAuthorityResponseID bigint,
							CreatedBy nvarchar(250),
							DateCreated datetime)
	INSERT INTO @Responses (Firm, SettlementAuthorityResponseID, SettlementAuthorityStatus, Comment, ParentSettlementAuthorityResponseID, CreatedBy, DateCreated)
	SELECT DISTINCT DefenseCounsel, SettlementAuthorityResponseID, SettlementAuthorityStatus, Comment, ParentSettlementAuthorityResponseID, CreatedBy, DateCreated
	FROM tblSettlementAuthorityResponse sar
	LEFT JOIN tblSettlementAuthorityStatus sas ON sar.SettlementAuthorityStatusID = sas.SettlementAuthorityStatusID
	LEFT JOIN  vSETTLEMENT_AUTHORITY_Users sau ON sar.CreatedBy = sau.Email
	LEFT JOIN tblDefenseCounselAddress dca ON sau.FirmID = dca.DefenseCounselID
	WHERE ParentSettlementAuthorityResponseID = @SettlementAuthorityResponseID

	--Setup style
	DECLARE @SubHeaderStyle nvarchar(MAX) = 'style="background-color: #E36F1E; font-weight: bold; color: white; padding: 4px;'
	DECLARE @TDStyle nvarchar(500) = 'padding: 4px;border: 1px solid #e36f1e;'
	DECLARE @newLine nvarchar(5) = CHAR(13) + CHAR(10)
	
	--Use cursor to loop through each parent record and also print children
	DECLARE @CurrentSettlementAuthorityResponseID int
	DECLARE @newOut nvarchar(MAX) = ''
	DECLARE @Count int = 0
	DECLARE Cur_Complaints CURSOR Local Fast_Forward for
	SELECT DISTINCT SettlementAuthorityResponseID FROM @Responses
	OPEN Cur_Complaints
	FETCH NEXT FROM Cur_Complaints INTO @CurrentSettlementAuthorityResponseID--, @Claimant, @ClaimantID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT @CurrentSettlementAuthorityResponseID
	
		
		SET @newOut = @newOut
		IF(@Count = 0)
		BEGIN
			SET @newOut = @newOut + COALESCE(@out, '')
			--Start by opening a table and printing header row
			SET @newOut = @newOut + '<table cellspacing="0" cellpadding="0" style="width: 100%; border-collapse: collapse;">
				<thead>
					<tr>
						<th ' + @SubHeaderStyle + ' text-align: left;" >Firm</th>
						<th ' + @SubHeaderStyle + ' text-align: left;" >Status</th>
						<th ' + @SubHeaderStyle + ' text-align: left;" >Comments</th>
						<th ' + @SubHeaderStyle + ' text-align: left;" >Created By</th>
						<th ' + @SubHeaderStyle + ' text-align: right;" >Response Date</th>
					</tr>
				</thead>'	
		END
			
		SET @newOut = @newOut + (SELECT CAST ( (
		SELECT	 
				[td/@style] = @tdStyle,
				[td] = COALESCE(Firm, ' '), @newLine,

				[td/@style] = @tdStyle,
				[td] = COALESCE(SettlementAuthorityStatus, ' '), @newLine,
				
				[td/@style] = @tdStyle, 
				[td] = COALESCE(REPLACE(Comment, @newLine, (SELECT br='', '' FOR XML PATH(''))), ' '), @newLine,
				
				[td/@style] = @tdStyle,
				[td] = COALESCE(CreatedBy, ' '), @newLine,
				
				[td/@style] = @tdStyle,
				[td] = CONVERT(nvarchar, DateCreated, 101), @newLine
		FROM @Responses
			WHERE SettlementAuthorityResponseID = @CurrentSettlementAuthorityResponseID
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) ))

		--PRINT @newOut
		IF(EXISTS (SELECT SettlementAuthorityResponseID FROM tblSettlementAuthorityResponse WHERE ParentSettlementAuthorityResponseID = @CurrentSettlementAuthorityResponseID))
		BEGIN
			SET @newOut = @newOut + '<tr><td colspan="5" style="padding-left: 4em"><table style="border-spacing: 0; border-collapse: collapse;" cellspacing="0" cellpadding="0">' 
		--	SET @newOut = @newOut + '<table cellpadding="0" cellspacing="0">
		--<thead>
		--	<tr>
		--		<th ' + @SubHeaderStyle + ' text-align: left; padding: 4px;" >Status</th>
		--		<th ' + @SubHeaderStyle + ' text-align: left; padding: 4px;" >Comments</th>
		--		<th ' + @SubHeaderStyle + ' text-align: left; padding: 4px;" >Created By</th>
		--		<th ' + @SubHeaderStyle + ' text-align: right; padding: 4px;" >Response Date</th>
		--	</tr>
		--</thead>'
			EXEC WSS_SETTLEMENT_AUTHORITY_EMAIL_qryPrintKids @CurrentSettlementAuthorityResponseID, @newOut output
			SET @newOut = @newOut + '</table>' 
			
		END
		--SET @newOut = @newOut + '</td></tr>'
		SET @Count = @Count + 1
	FETCH NEXT FROM Cur_Complaints INTO @CurrentSettlementAuthorityResponseID--, @Claimant, @ClaimantID

	END
	SET @out = @newOut 
	--SELECT @out
	
	
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryPrintKids] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryPrintKids] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryPrintKids] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryPrintKids] TO [power_user]
GO
