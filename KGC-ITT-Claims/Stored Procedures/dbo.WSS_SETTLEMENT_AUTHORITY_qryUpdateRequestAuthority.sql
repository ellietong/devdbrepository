SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryUpdateRequestAuthority]
(@Emailaddress nvarchar(250) = null, @SettlementGroupID bigint,
 @DemandAmount money, @AuthorityAmount money, @Comment nvarchar(MAX) = NULL,
 @ResponseDueDate datetime, @SettlementAuthorityID int)
AS
BEGIN
	
	--UPDATE SETTLEMENT AUTHORITY RECORD
	UPDATE tblSettlementAuthority
	SET DemandAmount = @DemandAmount,
		AuthorityAmount =  @AuthorityAmount,
		Comment = @Comment,
		ResponseDueDate = @ResponseDueDate
	WHERE SettlementAuthorityID = @SettlementAuthorityID
END


























GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryUpdateRequestAuthority] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryUpdateRequestAuthority] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryUpdateRequestAuthority] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryUpdateRequestAuthority] TO [power_user]
GO
