SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveSettlementAuthorityApproverContact]
	-- Add the parameters for the stored procedure here
(@SettlementAuthorityID int, @SettlementApproverContactID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE
	FROM tblSettlementAuthorityApproverContact
	WHERE SettlementAuthorityID  = @SettlementAuthorityID
		AND SettlementApproverContactID = @SettlementApproverContactID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveSettlementAuthorityApproverContact] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveSettlementAuthorityApproverContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveSettlementAuthorityApproverContact] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryRemoveSettlementAuthorityApproverContact] TO [power_user]
GO
