SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityNotificationContacts]
	-- Add the parameters for the stored procedure here
@SettlementAuthorityID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT snc.*,
	LName + COALESCE(', ' + FName, '') + ' - ' + Email + ' (' + dc.DefenseCounsel + ')' AS Contact
	FROM tblSettlementNotificationContact snc
	INNER JOIN tblSettlementAuthorityNotificationContact sanc on snc.SettlementNotificationContactID = sanc.SettlementNotificationContactID
	INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau on snc.personid = sau.personid
	INNER JOIN tblDefenseCounsel dc ON sau.FirmID = dc.DefenseCounselID
	where SettlementAuthorityID = @SettlementAuthorityID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityNotificationContacts] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_Settlement_Authority_qryGetSettlementAuthorityNotificationContacts] TO [base_user]
GO
