SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDefaultNotificationContactList]
(@ClaimLawsuitID bigint = null)
AS
BEGIN
		DECLARE @WhereString nvarchar(MAX) 
		--IF(@ClaimLawsuitID IS NOT NULL)
			SET @WhereString = ' AND sncs.StateID IN (SELECT StateID 
							FROM tblLawsuit l 
							INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
							WHERE cl.ClaimLawsuitID = ' + CONVERT(nvarchar, COALESCE(@ClaimLawsuitID, 0)) + ')'
		--ELSE
		--	SET @WhereString = ''
			
		DECLARE @QueryString nvarchar(MAX) = '
		SELECT	snc.*, 
				LName + COALESCE('', '' + FName, '''') + '' - '' + Email + '' ('' + dca.DefenseCounsel + '', '' + dca.[State] + '')'' AS Contact
		FROM tblSettlementNotificationContact snc
		INNER JOIN tblSettlementNotificationContactState sncs ON snc.SettlementNotificationContactID = sncs.SettlementNotificationContactID
		INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON snc.PersonID = sau.PersonID
		INNER JOIN tblDefenseCounsel dc ON sau.FirmID = dc.DefenseCounselID
		INNER JOIN tblDefenseCounselAddress dca ON sau.FirmAddressID = DefenseCounselAddressID
		WHERE IsDefault = 1
		' + @WhereString + 
		'
		
		ORDER BY Contact'
		PRINT @QueryString
		EXEC sp_executesql @QueryString
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDefaultNotificationContactList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDefaultNotificationContactList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDefaultNotificationContactList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDefaultNotificationContactList] TO [power_user]
GO
