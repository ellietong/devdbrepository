SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID]
(@ClaimID bigint)
AS
BEGIN

	SET NOCOUNT ON;


SELECT DISTINCT SettlementShareID, ss.ClaimLawsuitID, ss.InsuranceProgramID, ss.SettlementShare, Product AS InsuranceProgram,
COALESCE(SettlementAmount * (CASE WHEN  ss.SettlementShare IS NULL THEN COALESCE(ss.SettlementShare, 1) ELSE ss.SettlementShare END), 
		SettlementAmount*(ss.SettlementShare)) AS DollarShare 
from dbo.tblSettlementShare ss
inner join dbo.tblBillingProducts bp on ss.InsuranceProgramID = bp.ProductID
LEFT JOIN tblClaimantSettlement cs ON ss.ClaimLawsuitID = cs.ClaimLawsuitID 
where ss.ClaimLawsuitID in (select cl.ClaimLawsuitID from tblClaimLawsuit cl
INNER JOIN tblClaimantsettlement cs ON cl.Claimlawsuitid = cs.claimlawsuitid
WHERE cl.ClaimID = @ClaimID)
order by ClaimLawsuitID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementShareByClaimID] TO [power_user]
GO
