SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmDismissalQCd]
(@DocumentID bigint, @QCBy nvarchar (255))

As
SET NOCOUNT ON

UPDATE tblDocuments
SET	QCBy = @QCBy,
	QCDate = GETDATE()
WHERE DocumentID = @DocumentID

if coalesce ((SELECT
	Accepted
FROM tblDocuments
WHERE DocumentID = @DocumentID), 0) = 0
begin
UPDATE tblDocuments
SET	Accepted = 1,
	AcceptedBy = @QCBy + '@kcic.com',
	DateAccepted = GETDATE()
WHERE DocumentID = @DocumentID
end


RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalQCd] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalQCd] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalQCd] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalQCd] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDismissalQCd] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDismissalQCd] TO [power_user]
GO
