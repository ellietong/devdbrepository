SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_REPORTS_qrySettlementGroupList]
@emailaddress nvarchar(250)
AS
SET NOCOUNT ON

--DECLARE @EmailAddress nvarchar(250) = 'hastinr@dmclaw.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE SettlementGroup IS NOT NULL
						AND clst.IsPrimaryStatus = 1 
						AND clst.ClaimLawsuitStatusID IN (25) 
						AND YEAR(cs.SettlementDate) >= 2010 '

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID					
						INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
						INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

DECLARE @QueryString NVARCHAR(MAX) = ''
SET @QueryString = '
SELECT DISTINCT cs.SettlementGroupID, SettlementGroup
FROM tblClaimantSettlement cs
	INNER JOIN tblSettlementGroup sg on cs.SettlementGroupID = sg.SettlementGroupID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cs.ClaimLawsuitID = clst.ClaimLawsuitID
'
+ @JoinString 
+ @WhereString +
'
UNION
SELECT NULL, ''''
ORDER BY SettlementGroup'

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
