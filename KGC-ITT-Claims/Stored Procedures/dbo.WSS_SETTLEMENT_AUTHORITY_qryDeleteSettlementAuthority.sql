SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDeleteSettlementAuthority]
(@SettlementAuthorityID bigint)
AS
BEGIN
	
	DELETE FROM tblSettlementAuthorityApproverFirm WHERE SettlementAuthorityID = @SettlementAuthorityID
	DELETE FROM tblSettlementAuthority WHERE SettlementAuthorityID = @SettlementAuthorityID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDeleteSettlementAuthority] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDeleteSettlementAuthority] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDeleteSettlementAuthority] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryDeleteSettlementAuthority] TO [power_user]
GO
