SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryDelOccupationInfo](@ClaimantEmploymentID int, @emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE tblClaimantEmployment
	SET SPUsername = @Emailaddress
	WHERE ClaimantEmploymentID = @ClaimantEmploymentID
	
	UPDATE tblClaimantProduct
	SET SPUsername = @emailaddress
	WHERE ClaimantEmploymentID = @ClaimantEmploymentID
    -- Insert statements for procedure here
	DELETE FROM tblClaimantEmployment WHERE ClaimantEmploymentID = @ClaimantEmploymentID
	DELETE FROM tblClaimantProduct WHERE ClaimantEmploymentID = @ClaimantEmploymentID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryDelOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryDelOccupationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryDelOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryDelOccupationInfo] TO [power_user]
GO
