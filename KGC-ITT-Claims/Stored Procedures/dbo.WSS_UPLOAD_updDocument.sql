SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_UPLOAD_updDocument]
(
	@ClaimantDocumentTypeID int,					
	
	@Comments nvarchar(4000) = null,			--comments are optional		
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@DocID int,
	
	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,

	@ModifiedBy nvarchar(250),
	@DateModified datetime,
	@Approved bit = null,
	@ApprovedBy nvarchar(50) = null,
	@DateApproved	datetime = null,
	@DocumentSourceID int
	
)

AS
Update tblDocuments 
SET
	ClaimantDocumentTypeID	= @ClaimantDocumentTypeID,	
	Comments				= @Comments,	
	DocumentURL				= @DocumentURL,	
	DocumentTitle			= @DocumentTitle,
	----These fields are now only in tblDefenseInvoice, add update query for tblDefenseInvoice
	--InvoiceNumber			= @InvoiceNumber,
	--Period				= @Period,
	--DatePayable			= @DatePayable,
	----These fields are now only pulled back in vtblDefenseInvoice as a sum of the individual claimant records in tblDefInvClaimantDetail
	----Do not need to be updated.
	--FeeAmount				= @FeeAmount,
	--DisbursementAmount	= @DisbursementAmount,
	ModifiedBy				= @ModifiedBy,
	DateModified			= @DateModified,
	DocumentSourceID		= @DocumentSourceID
	--PrivateComments         = @PrivateComments,

	
WHERE DocumentID = @DocID

Update tblDefenseInvoice
SET
	InvoiceNumber			= @InvoiceNumber,
	Period					= @Period,
	DatePayable				= @DatePayable
Where DocumentID = @DocID

Select DocumentID From tblDocuments Where DocumentID = @DocID


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocument] TO [power_user]
GO
