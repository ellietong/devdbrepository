SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits](@LawsuitID bigint = null, @TrialGroupID int = null)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

IF @TrialGroupID IS NULL
	BEGIN	
		SELECT cl.ClaimLawsuitID, COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') + COALESCE(' ' + MiddleName, '')AS ClaimantName, ClaimLawsuitStatus, CONVERT(nvarchar(250), (CASE WHEN ServiceDate IS NULL Then case when FileDate is null then Case when ReceivedDate is null then '1/1/1960' else ReceivedDate end else FileDate end else ServiceDate end), 101) as ServiceDate
		FROM tblClaimLawsuit cl
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		INNER JOIN tblLawsuit l on l.LawsuitID = cl.LawsuitID
		WHERE cl.LawsuitID = @LawsuitID
		AND IsPrimaryStatus=1
		ORDER BY LastName
	END
ELSE
	BEGIN
		SELECT cl.ClaimLawsuitID, COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') + COALESCE(' ' + MiddleName, '')AS ClaimantName, ClaimLawsuitStatus, CONVERT(nvarchar(250), (CASE WHEN ServiceDate IS NULL Then case when FileDate is null then Case when ReceivedDate is null then '1/1/1960' else ReceivedDate end else FileDate end else ServiceDate end), 101) as ServiceDate
		FROM tblClaimLawsuit cl
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		INNER JOIN tblLawsuit l on l.LawsuitID = cl.LawsuitID
		WHERE cl.TrialGroupID = @TrialGroupID
		AND IsPrimaryStatus=1
		ORDER BY LastName
	END
END	
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryGetClaimLawsuits] TO [power_user]
GO
