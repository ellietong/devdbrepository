SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptSummaryReportByCarrierByPolicy]
(@AdjustmentExhaustionDateID int,@BillExhaustionDateID int, @LimitTypeID int, @ExhaustionTypeID int)
As
Set Nocount on

-- Declare @LimitTypeID int
-- Declare @ExhaustionTypeID int 
--Declare @BillExhaustionDateID int
--Declare @AdjustmentExhaustionDateID int
 
-- Set @LimitTypeID = 1
-- Set @ExhaustionTypeID = 1
-- Set @BillExhaustionDateID = 103
-- Set @AdjustmentExhaustionDateID = 101

--Previous Exhaustion
Declare @Exhaustion Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint,OutsideConsumption money )
Insert Into @Exhaustion
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation,
	DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation,
	LedgerID, OutsideConsumption 
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<@BillExhaustionDateID and ed.ExhaustionDateID not in (@AdjustmentExhaustionDateID)

Declare @PreviousExhaustionTotals table (ExhaustionTypeID int, PolicyDateID int, PreviousAllocationAmt money, OutsideConsumption money)
Insert Into @PreviousExhaustionTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAllocationAmt,
	OutsideConsumption 
From @Exhaustion
Group By ExhaustionTypeID, PolicyDateID, OutsideConsumption

Declare @previousExhaustionLondon table (PolicyDateID int, PreviousAllocationAmt money)
Insert Into @previousExhaustionLondon 
select pd.PolicyDateID,  SUM(indemnityallocation+Defenseallocation+DefenseOutsideLimitsAllocation) as PreviousAllocationAmt
from tblPolicyDates pd
inner join tblexhaustion e on e.policydateid=pd.policydateid
inner join tblExhaustionDate ed on e.ExhaustionDateID=ed.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and E.ExhaustionDateID<@BillExhaustionDateID and e.ExhaustionDateID not in (@AdjustmentExhaustionDateID) and pd.PolicyDateID in (27,28,29,30)
group by pd.PolicyDateID


--Current Allocation
Declare @CurrentAllocation Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint, OutsideConsumption money)
Insert Into @CurrentAllocation
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation,
	DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation,
	LedgerID, OutsideConsumption 
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID=@BillExhaustionDateID

Declare @CurrentAllocationTotals table (ExhaustionTypeID int, PolicyDateID int, Indemnity money, Defense money,
	DefenseOutsideLimits money, TotalDefense money, SIRIndemnityAllocation money, SIRDefenseAllocation money)
Insert Into @CurrentAllocationTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(IndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(DefenseAllocation), 0) as Defense,
	coalesce(Sum(DefenseOutsideLimitsAllocation),0) as DefenseOutsideLimits,
	coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
	coalesce(Sum(SIRIndemnityAllocation), 0) as SIRIndemnity,
	coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense
From @CurrentAllocation
Group By ExhaustionTypeID, PolicyDateID

--Current Adjustment
Declare @CurrentAdjustment Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	TotalAdjustment money)
Insert Into @CurrentAdjustment
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID, Coalesce(SUM(Indemnityallocation+DefenseAllocation),0) as TotalAdjustment
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID=@AdjustmentExhaustionDateID
Group by ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID

Declare @CurrentAdjustmentTotals table (ExhaustionTypeID int, PolicyDateID int, TotalAdjustment money) 
Insert Into @CurrentAdjustmentTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(TotalAdjustment), 0) as TotalAdjustment
From @CurrentAdjustment
Group By ExhaustionTypeID, PolicyDateID


-- Header Summary
Declare @HeaderSummary Table (ExhaustionType nvarchar(100), Description nvarchar(100), DefenseForm nvarchar(10), footerdate nvarchar(50))
Insert into @HeaderSummary
Select ExhaustionType, [Description], 'H', Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@BillExhaustionDateID
union
Select ExhaustionType, [Description], 'I', Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@BillExhaustionDateID
union
Select ExhaustionType, [Description], 'G', Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@BillExhaustionDateID
union
Select ExhaustionType, [Description], '', Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@BillExhaustionDateID

--Query Results
Declare @Results Table(OrderID int, OrderName nvarchar(100),policyid bigint, policydateid bigint, carriername nvarchar(100), layer bigint, policynum nvarchar(100), pstartdate nvarchar(100), penddate nvarchar(100), attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), Indemnity money, Defense money, DefenseOutsideLimits money, TotalDefense money, PreviousAllocationAmt money,
	Balance money, OutsideConsumption money)
Insert Into @Results



--London

select '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum,  convert(nvarchar(100), pstartdate, 101) as pstartdate, convert(nvarchar(100), PEndDate, 101)  as penddate, attachmentpoint, perocclimit, aggregatelimit, 
	CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,  sum(IndemnityAllocation) as Indemnity, sum(defenseallocation) as Defense,  sum(defenseoutsidelimitsallocation) as defenseoutsidelimits, sum(DefenseAllocation+defenseoutsidelimitsallocation) as TotalDefense,
	(pel.PreviousAllocationAmt) as previousallocationAmt, 
	( Sum(aggregatelimit)-SUM(indemnityallocation)-SUM(defenseallocation)-SUM(PreviousAllocationAmt)) as Balance , sum(e.OutsideConsumption) as outsideconsumption
from tblPolicyDates pd
inner join tblPolicy p on p.PolicyID=pd.PolicyID
inner join tblPolicyLimits pl on pl.PolicyID=p.PolicyID
inner join tblcarrier c on c.carrierid=p.carrierid
inner join tblexhaustion e on e.policydateid=pd.policydateid
inner join tblProvision pro on pro.PolicyID=p.PolicyID
left Join @previousExhaustionLondon PEL on PD.PolicyDateID=PEL.PolicyDateID
where c.CarrierID=20 and exhaustiondateid= @BillExhaustionDateID
group by p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, pd.pstartdate, pd.penddate,
	pl.attachmentpoint, pl. perocclimit, aggregatelimit, ProvisionValueID,PreviousAllocationAmt
Union

--Most Carriers
select  '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum,  convert(nvarchar(100), pstartdate, 101) as pstartdate, convert(nvarchar(100), PEndDate, 101)  as penddate,
	pl.attachmentpoint, pl.perocclimit, pl.aggregatelimit,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
 	(coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)  - coalesce(PreviousAllocationAmt,0)) as Balance,
 	coalesce(OutsideConsumption,0) as OutsideConsumption
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where limittypeid = @LimitTypeID and CAT.ExhaustionTypeID = @ExhaustionTypeID and provisionlabelID=1 and c.carrierid not in (10,20, 100000)

Union

--Marley Producer
select  '2' as OrderID, 'Producer', p.policyid, pd.policydateid, 'Marley-Wylain'  as carriername,'100' as layer, 'Producer' as PolicyNum, ' ' as pstartdate, ' ' as penddate,
	'' as attachmentpoint, '-1000000000'  as perocclimit, '-1000000000'  as aggregatelimit, '' as defenseform,
 sum(Indemnity) as Indemnity, sum(defense) as Defense,  '' as defenseoutsidelimits, sum(Defense) as TotalDefense,
SUM(pet.PreviousAllocationAmt) as previousallocationAmt, 
'-1000000000' as Balance , 
'' as outsideconsumption
from tblPolicyDates pd
inner join tblPolicy p on p.PolicyID=pd.PolicyID
inner join tblcarrier c on c.carrierid=p.carrierid
--inner join tblexhaustion e on e.policydateid=pd.policydateid
left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
where  pd.PolicyDateID=128 
group by p.policyid, pd.policydateid, c.carriername,  p.policynum, pd.pstartdate, pd.penddate


--Adjustment Results
Declare @Adjustment Table(OrderID int, OrderName nvarchar(100), policyid bigint, policydateid bigint, carriername nvarchar(100), layer int, policynum nvarchar(100), pstartdate datetime, penddate datetime, attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), TotalAdjustment money)
Insert Into @Adjustment

--London
select  '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, pd.pstartdate, pd.penddate, attachmentpoint, perocclimit, aggregatelimit, 
	CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm, 
	(sum(IndemnityAllocation)+SUM(DefenseAllocation)+SUM(DefenseOutsideLimitsAllocation)) as TotalAdjustment
from tblPolicyDates pd
inner join tblPolicy p on p.PolicyID=pd.PolicyID
inner join tblPolicyLimits pl on pl.PolicyID=p.PolicyID
inner join tblcarrier c on c.carrierid=p.carrierid
inner join tblexhaustion e on e.policydateid=pd.policydateid
inner join tblProvision pro on pro.PolicyID=p.PolicyID
where c.CarrierID=20 and exhaustiondateid= @AdjustmentExhaustionDateID
group by p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, pd.pstartdate, pd.penddate,
	pl.attachmentpoint, pl. perocclimit, aggregatelimit, ProvisionValueID

Union
--Most Carriers

select  '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, pd.pstartdate, pd.penddate, attachmentpoint, perocclimit, aggregatelimit, 
	CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm, 
	coalesce(TotalAdjustment,0) as TotalAdjustment 
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAdjustmentTotals CAAT on PD.PolicyDateID=CAAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where limittypeid = @LimitTypeID and CAAT.ExhaustionTypeID = @ExhaustionTypeID and c.carrierid not in (10,20, 100000)

UNION

select  '2' as OrderID, 'Producer', p.policyid, pd.policydateid, ''  as carriername,'' as layer, 'Producer' as policynum, '' as pstartdate, '' as penddate,
	'' as attachmentpoint, '-1000000000'  as perocclimit, '-1000000000' as aggregatelimit, '' as defenseform,
	coalesce(TotalAdjustment,0) as TotalAdjustment 
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAdjustmentTotals CAAT on PD.PolicyDateID=CAAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
Where  CAAT.ExhaustionTypeID = @ExhaustionTypeID and pd.policydateid=128


Select R.*, ExhaustionType, [Description],sum(a.TotalAdjustment) as TotalAdjustment, hs.footerdate
From @Results R
Inner Join @HeaderSummary HS on R.DefenseForm=HS.DefenseForm
left join @Adjustment a on r.policydateid=a.policydateid
group by  r.policyid , r.OrderID, r.OrderName, r.policydateid , r.carriername , r.layer , r.policynum , r.pstartdate , r.penddate , r.attachmentpoint , r.perocclimit , r.aggregatelimit ,
	r.DefenseForm  , indemnity , Defense , DefenseOutsideLimits , TotalDefense , PreviousAllocationAmt ,
	Balance , OutsideConsumption , ExhaustionType, [Description], hs.footerdate
	order by carriername,   cast(r.pstartdate as date) , Cast(r.penddate as DATE),  policynum
Return




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GO
