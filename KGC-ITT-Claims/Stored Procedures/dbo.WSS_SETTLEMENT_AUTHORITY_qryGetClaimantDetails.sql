SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] 
	-- Add the parameters for the stored procedure here
(@ClaimantPersonalInfoID int, @SettlementAuthorityID int = null, @ClaimLawsuitID int = null)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----TEST VARIABLES:
	--DECLARE @ClaimantPersonalInfoID int = 7914 --2799--23115
	--DECLARE @SettlementAuthorityID int = 88--0--45
	--DECLARE @ClaimLawsuitID int = 51845--58508--0

    -- Insert statements for procedure here

--CLAIMANT INFORMATION
	SELECT DISTINCT TOP 1 cpi.ClaimantPersonalInfoID, 
		cpi.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + 
			CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
        CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN,
        Gender, 
        COALESCE(cpi.GenderID, 4) AS GenderID, 
        COALESCE(cpi.MaritalStatusID, 1) AS MaritalStatusID, 
        MaritalStatus, 
        COALESCE(nullif(COALESCE(Address1 + CHAR(13) + CHAR(10), '') + COALESCE(Address2 + CHAR(13) + CHAR(10),'') + 
			COALESCE(City + ', ','') + COALESCE(s.State + ' ','') + COALESCE(CONVERT(varchar,Zip),'') + 
			COALESCE('-'+CONVERT(varchar,mailzip),''),''),'') as CompleteAddress, 
        Convert(nvarchar(10), BirthDate, 101) AS BirthDate, 
        Convert(nvarchar(10), DeceasedDate, 101) AS DeceasedDate,
		City, 
		s.StateID, 
		Zip, 
		MailZip, 
		Address1, 
		Address2, 
		IsSmoker, 
		Reference_ID AS ReferenceID, 
		CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS FullSSN,
		HealthInsuranceClaimNumber,
		(SELECT COALESCE (CONVERT(nvarchar(50), (CASE WHEN CONVERT(nvarchar(50), COALESCE(MIN(empstartdate), MIN(startdate)), 101) IS NULL THEN 'UNKNOWN'
		ELSE
		(CASE WHEN MIN(startdate) is not null
			THEN CONVERT(nvarchar(50),MIN(startdate), 101) 
			ELSE CONVERT(nvarchar(50),MIN(empstartdate), 101)

			END)
		END), 101), 'UNKNOWN') AS DOFE
		FROM tblClaimantEmployment ce
		LEFT JOIN tblClaimantProduct cp on ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
		WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID) AS DiscoveryDOFE	
    FROM tblClaimantPersonalInfo cpi 
		 LEFT JOIN tblGender g ON g.GenderID = cpi.GenderID
         LEFT JOIN tblMaritalStatus ms ON ms.MaritalStatusID = cpi.MaritalStatusID
         LEFT JOIN tblState s ON cpi.StateID = s.StateID
         LEFT JOIN tblClaimantSmokingHistory csh ON cpi.ClaimantPersonalInfoID = csh.ClaimantPersonalInfoID  
		 INNER JOIN dbo.tblClaim c				 ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		 LEFT JOIN tblClaimLawsuit cl			 ON c.ClaimID = cl.ClaimID
		 LEFT JOIN tblLawsuit l					 ON cl.LawsuitID = l.LawsuitID 	
		 LEFT JOIN tblClaimantEmployment ce on cpi.ClaimantPersonalInfoID = ce.ClaimantPersonalInfoID
		 LEFT JOIN tblOccupation o on ce.OccupationID = o.OccupationID
		 LEFT JOIN tblClaimDisease cd on c.ClaimID = cd.ClaimID
		 LEFT JOIN tblDisease d on cd.DiseaseID = d.DiseaseID
	WHERE cpi.ClaimantPersonalInfoID =  @ClaimantPersonalInfoID

IF((@ClaimLawsuitID = NULL) OR (@ClaimLawsuitID = 0))
BEGIN
	--DECLARE @ClaimLawsuitID int
	SET @ClaimLawsuitID = 
	(

		SELECT cl.ClaimLawsuitID
		FROM tblClaimLawsuit cl
			INNER JOIN tblClaim c on cl.ClaimID = c.Claimid
			INNER JOIN tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
			INNER JOIN tblClaimantSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID	
			INNER JOIN tblSettlementAuthority sa on cs.ClaimLawsuitID = sa.ClaimLawsuitID
			--INNER JOIN tblSettlementAuthority sa on cs.SettlementGroupID = sa.SettlementGroupID	 
		WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID		 
			  AND sa.SettlementAuthorityID = @SettlementAuthorityID 
	)
END

--CLAIM INFORMATION
	SELECT DISTINCT TOP 1 c.ClaimID, 
		hddis.HighLevelDisease AS Disease, 
		CASE WHEN @SettlementAuthorityID = 0 THEN 'Pending Request Submission' ELSE Convert(nvarchar(10), ComplaintDOFE, 101) END AS ComplaintDOFE,
		S.State, 
        CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, 
        L.DocketNumber, 
		ClaimDiseaseID, 
		CauseOfInjuryID,
		PFTDoctorID,
		PFTDate,
		PFTTypeID,
		COALESCE(CONVERT(nvarchar(10), PFTFVC_Actual), '') AS PFTFVC_Actual,
		COALESCE(CONVERT(nvarchar(10), PFTFVC_Predicted), '') AS PFTFVC_Predicted,
		COALESCE(CONVERT(nvarchar(10), PFTFEV1_Actual), '') AS PFTFEV1_Actual,
		COALESCE(CONVERT(nvarchar(10), PFTFEV1_Predicted), '') AS PFTFEV1_Predicted,
		COALESCE(CONVERT(nvarchar(10), [PFTFEV1andFVC_Actual]), '') AS PFTFEV1FVC_Actual,
		COALESCE(CONVERT(nvarchar(10), [PFTFEV1andFVC_Predicted]), '') AS PFTFEV1FVC_Predicted,
		COALESCE(CONVERT(nvarchar(10), PFTTLC_Actual), '') AS PFTTLC_Actual,
		COALESCE(CONVERT(nvarchar(10), PFTTLC_Predicted), '') AS PFTTLC_Predicted,
		COALESCE(CONVERT(nvarchar(10), PFTDLCO_Actual), '') AS PFTDLCO_Actual,
		COALESCE(CONVERT(nvarchar(10), PFTDLCO_Predicted), '') AS PFTDLCO_Predicted,
		COALESCE(CONVERT(nvarchar(10), PFTTV), '') AS PFTTV,
		COALESCE(CONVERT(nvarchar(10), PFTDLCO_Predicted), '') AS PFTDLCO_Predicted,
		--CASE WHEN PFTFVC_Predicted <> 0 THEN CONVERT(nvarchar(10), CAST(((PFTFVC_Actual/PFTFVC_Predicted)*100) AS float)) + ' %' ELSE '' END AS PFTFVC_Pct,
		--CASE WHEN PFTFEV1_Predicted <> 0 THEN CONVERT(nvarchar(10),CAST(((PFTFEV1_Actual/PFTFEV1_Predicted)*100) AS float)) + ' %' ELSE '' END AS PFTFEV1_Pct,
		CASE WHEN PFTFVC_Predicted <> 0 THEN CONVERT(nvarchar(10), CAST(((PFTFVC_Actual/PFTFVC_Predicted)*100) AS numeric(5,2))) + ' %' ELSE '' END AS PFTFVC_Pct,
		CASE WHEN PFTFEV1_Predicted <> 0 THEN CONVERT(nvarchar(10),CAST(((PFTFEV1_Actual/PFTFEV1_Predicted)*100) AS numeric(5,2))) + ' %' ELSE '' END AS PFTFEV1_Pct,
		COALESCE([PFTFEV1andFVC_Actual], '') AS PFTFEV1FVC_Actual, 
		COALESCE([PFTFEV1andFVC_Predicted], '') AS PFTFEV1FVC_Predicted,
		--CASE WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN CONVERT(nvarchar(10),CAST((([PFTFEV1andFVC_Actual]/[PFTFEV1andFVC_Predicted])*100) AS float)) + ' %' ELSE '' END AS PFTFEV1FVC_Pct,
		--CASE WHEN PFTTLC_Predicted <> 0 THEN CONVERT(nvarchar(10),CAST(((PFTTLC_Actual/PFTTLC_Predicted)*100) AS float)) + ' %' ELSE '' END AS PFTTLC_Pct, 
		--CASE WHEN PFTDLCO_Predicted <> 0 THEN CONVERT(nvarchar(10),CAST(((PFTDLCO_Actual/PFTDLCO_Predicted)*100) AS float)) + ' %' ELSE '' END AS PFTDLCO_Pct, 
		--CASE WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN CONVERT(nvarchar(10),CAST((([PFTFEV1andFVC_Actual]/[PFTFEV1andFVC_Predicted])*100) AS numeric(5,2))) + ' %' ELSE '' END AS PFTFEV1FVC_Pct,
		'' AS PFTFEV1FVC_Pct,
		CASE WHEN PFTTLC_Predicted <> 0 THEN CONVERT(nvarchar(10),CAST(((PFTTLC_Actual/PFTTLC_Predicted)*100) AS numeric(5,2))) + ' %' ELSE '' END AS PFTTLC_Pct, 
		CASE WHEN PFTDLCO_Predicted <> 0 THEN CONVERT(nvarchar(10),CAST(((PFTDLCO_Actual/PFTDLCO_Predicted)*100) AS numeric(5,2))) + ' %' ELSE '' END AS PFTDLCO_Pct, 
		AllegationType + ' - ' + Material AS Allegation, 
		Convert(nvarchar(20), DiseaseDiagnosisDate, 101) AS DiseaseDiagnosisDate,
		cd.DiseaseID,
		CourtType,
		CASE WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + ', ' + COALESCE(ilodoc.DoctorFirstName, '') + COALESCE(' ' + ilodoc.DoctorMiddleName, '') END AS ILODoctor,
		ILOProfusion, 
		ILODate, 
		Jurisdiction + ', ' + s.State AS Jurisdiction,
		Convert(nvarchar(20), ActiveTrialDate, 101) AS ActiveTrialDate
     FROM tblClaimantPersonalInfo cpi 
		LEFT JOIN tblClaim c							ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l							ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblState s							ON s.StateID = l.StateID
		INNER JOIN tblJurisdiction j					ON j.JurisdictionID = l.JurisdictionID
		INNER JOIN tblClaimSequence csq					ON csq.ClaimSequenceID = c.ClaimSequenceID  
		LEFT JOIN  tblDefenseCounselAddress dca	ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID  
        LEFT JOIN  (SELECT * 
					FROM tblClaimDisease 
					WHERE PrimaryDisease = 1) cd		ON cd.ClaimID = c.ClaimID
		LEFT JOIN  tblDisease ddis						ON ddis.DiseaseID = cd.DiseaseID
		LEFT JOIN  tblDisease_ALT disa					ON disa.DiseaseID_Alt = cd.DiseaseID
		LEFT JOIN  tblHighLevelDisease hddis			ON hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
		LEFT JOIN  (SELECT * 
					FROM tblClaimLawsuitStatusTracking 
					WHERE IsPrimaryStatus = 1) clst		ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
        LEFT JOIN  tblClaimLawsuitStatus cls			ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
        LEFT JOIN  tblplaintiffcounseladdress pca		ON pca.plaintiffcounseladdressid = l.plaintiffcounseladdressid
        LEFT JOIN  tblDoctor doc						ON cd.DoctorID = doc.DoctorID 
        LEFT JOIN  tblDoctor ILODoc						ON ILODoctorID=ILODoc.DoctorID
        LEFT JOIN  tblDoctor PFTDoc						ON PFTDoctorID=PFTDoc.DoctorID
        LEFT JOIN  tblAllegation a						ON cl.ClaimLawsuitID = a.ClaimLawsuitID
        INNER JOIN tblAllegationType at					ON a.AllegationTypeID = at.AllegationTypeID
        INNER JOIN tblMaterial mat						ON a.MaterialID = mat.MaterialID
        LEFT JOIN tblTrialGroup tg				 ON cl.TrialGroupID = tg.TrialGroupID  
		LEFT join tblClaimantSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID
		LEFT JOIN tblCourtType ct on l.CourtTypeID = ct.CourtTypeID
	WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		  AND cl.ClaimLawsuitID = @ClaimLawsuitID

--EMPLOYMENT INFORMATION
	IF (SELECT Count(ClaimantEmploymentID) FROM tblclaimantemployment WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID) > 0
	BEGIN
		SELECT ce.ClaimantEmploymentID, 
			COALESCE(o.OccupationID, 1) AS OccupationID, 
			COALESCE(o.Occupation, ' ') AS Occupation,
			COALESCE(Jobsite, ' ') AS Jobsite,
			Convert(nvarchar(10), EmpStartDate, 101) AS EmpStartDate, 
			Convert(nvarchar(10), EmpEndDate, 101) AS EmpEndDate, 
			cpi.FirstName + ' ' + cpi.LastName AS Claimant,
			COALESCE(JobsiteCity, ' ') AS JobsiteCity,
			JobsiteStateID,
			s.State AS JobsiteState, 
			IsPrimary AS IsPrimaryJobsite,
			COALESCE(Comments, ' ')	AS Comments
		FROM tblClaimantEmployment ce 
			INNER JOIN tblClaimantPersonalInfo cpi	ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
			LEFT JOIN  tblOccupation o				ON o.OccupationID = ce.OccupationID
			LEFT JOIN  tblClaim c					ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
			LEFT JOIN  tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
			LEFT JOIN  tblLawsuit l					ON cl.LawsuitID = l.LawsuitID
			LEFT JOIN  tblState s					ON ce.JobsiteStateID = s.StateID
		WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		GROUP BY ce.ClaimantEmploymentID, 
			COALESCE(o.OccupationID, 1), 
			o.Occupation, 
			Jobsite, 
			EmpStartDate, 
			EmpEndDate, 
			cpi.FirstName + ' ' + cpi.LastName,
			JobsiteCity, 
			JobsiteStateID,
			s.State, 
			IsPrimary,
			Comments
		ORDER BY IsPrimary DESC

	END
	ELSE
	BEGIN
		SELECT 'No employment information available for this claimant.' AS EmptyEmployment
	END

--PRODUCT INFORMATION
	DECLARE @ClaimantEmploymentIDs TABLE (ceIDs int)
	INSERT INTO @ClaimantEmploymentIDs
	SELECT DISTINCT ClaimantEmploymentID
	FROM tblClaimantEmployment
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID

	IF (SELECT Count(ClaimantProductID) FROM tblClaimantProduct cp INNER JOIN @ClaimantEmploymentIDs ceid ON cp.ClaimantEmploymentID = ceid.ceIDs) > 0
	BEGIN

			SELECT  ClaimantEmploymentID ,
				COALESCE(Convert(nvarchar(10), StartDate, 101), ' ') AS StartDate,
				COALESCE(Convert(nvarchar(10), EndDate, 101), ' ') AS EndDate,
				cp.ProductID ,
				COALESCE(ProductBrand , 'UNKNOWN') AS ProductBrand,
				COALESCE(ProductManufacturer, 'UNKNOWN') AS ProductManufacturer,
				COALESCE(ProductName, 'UNSPECIFIED') AS ProductName ,
				COALESCE(ProductType , 'UNKNOWN') AS ProductType,
				ClaimantProductID ,
				Notes,
				PrimaryProduct as IsPrimaryProduct
		FROM    tblClaimantProduct cp
				INNER JOIN tblProduct p ON p.ProductID = cp.ProductID
				INNER JOIN tblProductBrand pb ON pb.ProductBrandID = p.ProductBrandID
				INNER JOIN tblProductManufacturer pm ON pm.ProductManufacturerID = p.ProductManufacturerID
				LEFT JOIN tblProductName pn ON pn.ProductNameID = p.ProductNameID
				INNER JOIN tblProductType pt ON pt.ProductTypeID = p.ProductTypeID
				INNER JOIN (
				SELECT DISTINCT ceIDs
				FROM @ClaimantEmploymentIDs
				) ceid ON cp.ClaimantEmploymentID = ceid.ceIDs
	END
	ELSE
	BEGIN
		SELECT 'No product information available for this claimant.' AS EmptyProduct
	END

--DIAGNOSIS INFORMATION
	DECLARE @ClaimIDs TABLE (claimIDs int)
	INSERT INTO @ClaimIDs
	SELECT DISTINCT ClaimID
	FROM tblClaim
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID

	IF (SELECT Count(ClaimDiseaseID) FROM tblClaimDisease cd INNER JOIN @ClaimIDs cids ON cd.ClaimID = cids.claimIDs) > 0
		BEGIN
			SELECT ClaimDiseaseID, 
				   CD.DiseaseID, 
				   COALESCE(Disease, ' ') AS Disease,
				   COALESCE(Convert(nvarchar(10), DiseaseDiagnosisDate, 101), ' ') AS DiagnosisDate,
				   CASE WHEN PrimaryDisease = 1 THEN 'YES' ELSE 'NO' END AS PrimaryDisease,
				   PrimaryDisease AS PrimaryDiseaseID, 
				   DOC.DoctorID, 
				   COALESCE(DoctorNote, ' ') AS Specialty, 
				   CASE WHEN DoctorLastName IS NULL THEN '' 
				   ELSE COALESCE(DoctorLastName, ' ') + ', ' + CASE WHEN DoctorFirstName IS NULL THEN '' ELSE DoctorFirstName + ' ' END + CASE WHEN DoctorMiddleName IS NULL THEN ' ' ELSE DoctorMiddleName + ' ' END END AS Doctor,
				   CASE WHEN IsDefenseDisease = 1 THEN 'YES' ELSE 'NO' END AS IsDefenseDisease,
				   IsDefenseDisease AS IsDefenseDiseaseID
			FROM tblClaimDisease CD
			INNER JOIN tblClaim C ON C.ClaimID = CD.ClaimID
			LEFT JOIN (select * from tblDisease union select * from tbldisease_alt) ddis on ddis.DiseaseID = CD.DiseaseID
			LEFT JOIN tblDoctor DOC on CD.DoctorID = DOC.DoctorID
			WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID		
		END
	ELSE
		BEGIN
			SELECT 'No diagnosis information available for this claimant.' AS EmptyDiagnosis
		END

--BASIS OF RECOMMENDATION
	IF (SELECT Count(ClaimantSettlementID) FROM tblClaimantSettlement WHERE ClaimLawsuitID = @ClaimLawsuitID) > 0
		BEGIN

		DECLARE @BasisOfRecommendations nvarchar(max)
		DECLARE @BasisOfRecommendationText TABLE (ClaimLawsuitID int, BasisOfRecommendationText nvarchar(max))
		DECLARE @BasisOfRecommendationID int = 0

		IF object_id('tempdb..#BasisOfRecommendationText') IS NOT NULL
		BEGIN
		   DROP TABLE #BasisOfRecommendationText
		END
		CREATE TABLE #BasisOfRecommendationText (BasisOfRecommendationID int, ClaimLawsuitID int, BasisOfRecommendationText nvarchar(max))

		DECLARE BasisOfRecommendation_Cursor CURSOR Local Fast_Forward FOR
				SELECT ClaimLawsuitID, CASE WHEN @SettlementAuthorityID = 0 THEN 'Pending Request Submission' ELSE COALESCE(BasisOfRecommendation, 'No basis of recommendation provided for this claimant.') END AS BasisOfRecommendation
				FROM tblClaimantSettlement CS
				WHERE ClaimLawsuitID = @ClaimLawsuitID

		OPEN BasisOfRecommendation_Cursor

		FETCH NEXT FROM BasisOfRecommendation_Cursor 
		INTO @ClaimLawsuitID, @BasisOfRecommendations

		WHILE @@FETCH_STATUS = 0
		BEGIN

			DECLARE @BasisOfRecommendation nvarchar(max) = (
				SELECT CASE WHEN @SettlementAuthorityID = 0 THEN 'Pending Request Submission' ELSE COALESCE(BasisOfRecommendation, 'No basis of recommendation provided for this claimant.') END--COALESCE(BasisOfRecommendation, 'No basis of recommendation provided for this claimant.')
				FROM tblClaimantSettlement
				WHERE ClaimLawsuitID = @ClaimLawsuitID
			)

			DECLARE @BasisOfRecommendationLength int 
			= (
				SELECT LEN(CASE WHEN @SettlementAuthorityID = 0 THEN 'Pending Request Submission' ELSE COALESCE(BasisOfRecommendation, 'No basis of recommendation provided for this claimant.') END)
				FROM tblClaimantSettlement
				WHERE ClaimLawsuitID = @ClaimLawsuitID
			)

			DECLARE @Index int = -1

			DECLARE @MoveNote nvarchar(max) = (
			SELECT RIGHT(@BasisOfRecommendation, (LEN(@BasisOfRecommendation)-@Index))
			)

			DECLARE	@Fragment100 nvarchar(150) 
			DECLARE @BasisOfRecommendationRight nvarchar(max)
			DECLARE @Remainder int 
			DECLARE @Count int = 0

			WHILE 1=1
				BEGIN
					SET @Count = @Count + 1
					SET @BasisOfRecommendationID = @BasisOfRecommendationID + 1
					SET @Fragment100 = (SELECT LEFT(@BasisOfRecommendation, 99)) 

					IF (LEN(@BasisOfRecommendation)-99)>= 1
						BEGIN

							SET @BasisOfRecommendationRight = (SELECT RIGHT(@BasisOfRecommendation, LEN(@BasisOfRecommendation)-99))
							SET @Remainder = (SELECT CHARINDEX(' ', @BasisOfRecommendationRight))
							SET @Fragment100 =  @Fragment100 + LEFT(@BasisOfRecommendationRight, @Remainder) + '<br/>'
							SET @BasisOfRecommendationRight = (SELECT RIGHT(@BasisOfRecommendation, LEN(@BasisOfRecommendation)-99- @Remainder))
							SET @BasisOfRecommendation = (RIGHT(@BasisOfRecommendationRight, LEN(@BasisOfRecommendationRIGHT)+1))

							INSERT INTO #BasisOfRecommendationText
								SELECT @BasisOfRecommendationID, @ClaimLawsuitID, LTRIM(@Fragment100)

						END
						ELSE
						BEGIN
							SET @Fragment100 = @BasisOfRecommendation
							INSERT INTO #BasisOfRecommendationText
								SELECT @BasisOfRecommendationID, @ClaimLawsuitID, LTRIM(@Fragment100) + '<br/>'
							BREAK
						END
				END

			FETCH NEXT FROM BasisOfRecommendation_Cursor
			INTO @ClaimLawsuitID, @BasisOfRecommendation

		END
		CLOSE BasisOfRecommendation_Cursor
		DEALLOCATE BasisOfRecommendation_Cursor

			--SELECT * FROM #BasisOfRecommendationText 


		DECLARE @FullBasisOfRecommendation nvarchar(max) = ''


		IF object_id('tempdb..#FullBasisOfRecommendationText') IS NOT NULL
		BEGIN
		   DROP TABLE #FullBasisOfRecommendationText
		END
		CREATE TABLE #FullBasisOfRecommendationText (ClaimLawsuitID int, BasisOfRecommendation nvarchar(max))


		DECLARE @FullBasisOfRecommendations nvarchar(1000)
		DECLARE @FullBasisOfRecommendationID int

		DECLARE FullBasisOfRecommendation_Cursor CURSOR Local Fast_Forward FOR
		SELECT DISTINCT max(BasisOfRecommendationID), ClaimLawsuitID
		FROM #BasisOfRecommendationText
		GROUP BY ClaimLawsuitID

		OPEN FullBasisOfRecommendation_Cursor

		FETCH NEXT FROM FullBasisOfRecommendation_Cursor 
		INTO @FullBasisOfRecommendationID, @ClaimLawsuitID

		WHILE @@FETCH_STATUS = 0
			BEGIN

			DECLARE @CountID int = 0
				SET @BasisOfRecommendationID = @FullBasisOfRecommendationID - (SELECT count(BasisOfRecommendationid) from #BasisOfRecommendationText WHERE ClaimLawsuitID = @ClaimLawsuitID) + 1 --@BasisOfRecommendationID + 1
			WHILE (SELECT MAX(BasisOfRecommendationID) FROM #BasisOfRecommendationText WHERE ClaimLawsuitID =  @ClaimLawsuitID)  = @FullBasisOfRecommendationID
				BEGIN
					SET @FullBasisOfRecommendation = @FullBasisOfRecommendation + (SELECT BasisOfRecommendationText FROM #BasisOfRecommendationText WHERE ClaimLawsuitID = @ClaimLawsuitID AND BasisOfRecommendationID = @BasisOfRecommendationID)
					IF @BasisOfRecommendationID = @FullBasisOfRecommendationID
						BEGIN
							IF (SELECT Count(ClaimLawsuitID) FROM #FullBasisOfRecommendationText WHERE ClaimLawsuitID = @ClaimLawsuitID) < 1
								BEGIN
									INSERT INTO #FullBasisOfRecommendationText
										SELECT @ClaimLawsuitID, @FullBasisOfRecommendation
									SET @FullBasisOfRecommendation = ''
								END
							IF @BasisOfRecommendationID = @FullBasisOfRecommendationID
								BEGIN	
									BREAK					
								END
						END		

						SET @BasisOfRecommendationID = @BasisOfRecommendationID + 1
				END

				FETCH NEXT FROM FullBasisOfRecommendation_Cursor
				INTO @FullBasisOfRecommendationID, @ClaimLawsuitID

			END
		CLOSE FullBasisOfRecommendation_Cursor
		DEALLOCATE FullBasisOfRecommendation_Cursor


		SELECT TOP 1 * FROM #FullBasisOfRecommendationText

			END
	ELSE
		BEGIN
			SELECT CASE WHEN @SettlementAuthorityID = 0 THEN 'Pending Request Submission' ELSE 'No basis of recommendation provided for this claimant.' END AS BasisOfRecommendation
		END

-- RELEASE DOCUMENT
	DECLARE @CountRelease int
	SET @CountRelease = (SELECT COUNT(d.DocumentID)
						 FROM tblDocuments d
							INNER JOIN tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
						 WHERE d.ClaimantDocumentTypeID = 31
							AND mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	)

	IF(SELECT @CountRelease) > 0
		BEGIN
			SELECT TOP 1 '<a OnClick="getFile('+ '''KCICITT''' + ', ''' + AmazonKey + ''');" Class="anchorStyle" >'  + DocumentTitle + '</a>' AS 'ViewRelease'
			FROM tblDocuments d
				INNER JOIN tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
				INNER JOIN tblClaimantPersonalInfo cpi on mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
			WHERE d.ClaimantDocumentTypeID = 31
				AND mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		END
	ELSE
		BEGIN
			SELECT '' AS 'ViewRelease'
		END

--SUPPORTING DOCUMENTS

	DECLARE @SDTable nvarchar(MAX) = '<table>' 
	DECLARE @tdStyle nvarchar(MAX) = ' '--'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'
	DECLARE @SetPymtID int = (SELECT MAX(SetPymtID) FROM tblSettlePaymentDocumentation WHERE ClaimLawsuitID = @ClaimLawsuitID)

	DECLARE @CountSettleDocs int
	SET @CountSettleDocs = (SELECT COUNT(SettleDocID) FROM tblSettleDocs WHERE SetPymtID = @SetPymtID)

	IF (SELECT @CountSettleDocs) > 0
	BEGIN 
		--SET @SDTable = @SDTable +  '<table>'
		SET @SDTable = @SDTable + (--COALESCE((CAST((
		SELECT (select [td/@style] = @tdStyle, field as 'td' for xml path(''), type), (select [td/@style] = @tdStyle, Information as 'td' for xml path(''), type)
		FROM (
		select Field, Information
		FROM (
		SELECT DISTINCT
 			CASE WHEN DocCategory = 'Settlement Confirmation' THEN '<a style="color:#0645AD;" OnClick="getFile('+ '''KCICITT''' + ', ''' + AmazonKey + ''');" Class="anchorStyle" >'  + DocumentTitle +  COALESCE(', Page '+ PageNums, '') + '</a>' END AS [Settlement Confirmation:   ],
				CASE WHEN DocCategory = 'Claimant Identified' THEN '<a style="color:#0645AD;" OnClick="getFile('+ '''KCICITT''' + ', ''' + AmazonKey + ''');" Class="anchorStyle" >'  + DocumentTitle + COALESCE(', Page '+ PageNums, '') + '</a>' END AS [Claimant Identified:   ],
				CASE WHEN DocCategory = 'Diagnosing Medicals' THEN '<a style="color:#0645AD;" OnClick="getFile('+ '''KCICITT''' + ', ''' + AmazonKey + ''');" Class="anchorStyle" >'  + DocumentTitle + COALESCE(', Page '+ PageNums, '') + '</a>' END AS [Diagnosing Medicals:   ],
				CASE WHEN DocCategory = 'Product Identification' THEN '<a style="color:#0645AD;" OnClick="getFile('+ '''KCICITT''' + ', ''' + AmazonKey + ''');" Class="anchorStyle" >'  + DocumentTitle + COALESCE(', Page '+ PageNums, '') + '</a>' END AS [Product ID:   ],
				CASE WHEN DocCategory = 'Employment/Exposure History' THEN '<a style="color:#0645AD;" OnClick="getFile('+ '''KCICITT''' + ', ''' + AmazonKey + ''');" Class="anchorStyle" >'  + DocumentTitle + COALESCE(', Page '+ PageNums, '') + '</a>' END AS [Employment/Exposure History:   ],
				CASE WHEN DocCategory = 'Estate Paperwork' THEN '<a style="color:#0645AD;" OnClick="getFile('+ '''KCICITT''' + ', ''' + AmazonKey + ''');" Class="anchorStyle" >'  + DocumentTitle + COALESCE(', Page '+ PageNums, '') + '</a>' END AS [Estate Papers:   ],
				CASE WHEN DocCategory = 'Death Certificate' THEN '<a style="color:#0645AD;" OnClick="getFile('+ '''KCICITT''' + ', ''' + AmazonKey + ''');" Class="anchorStyle" >'  + DocumentTitle + COALESCE(', Page '+ PageNums, '') + '</a>' END AS [Death Certificate:   ]
			FROM tblSettleDocs sd 
				INNER JOIN tblDocuments d         ON sd.DocumentID = d.DocumentID
				INNER JOIN (SELECT ClaimantPersonalInfoID, SetPymtID  
							FROM tblSettlePaymentDocumentation spd
							INNER JOIN  tblClaimLawsuit cl on spd.ClaimLawsuitID = cl.ClaimLawsuitID
							INNER JOIN  tblClaim c on cl.ClaimID = c.ClaimID) cpi ON sd.SetPymtID = cpi.SetPymtID
				INNER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID   WHERE DocumentURL IS NOT NULL AND AmazonKey IS NOT NULL AND d.ClaimantDocumentTypeID <> 40
								AND sd.SetPymtID = @SetPymtID
		) sd
		UNPIVOT (Information FOR Field in ( 
		[Settlement Confirmation:   ],
		[Claimant Identified:   ],
		[Diagnosing Medicals:   ],
		[Product ID:   ],
		[Employment/Exposure History:   ],
		[Estate Papers:   ],
		[Death Certificate:   ]
		)) as unpv 
		) tbl 
		ORDER BY Information
		FOR XML PATH('tr'))--, type) AS NVARCHAR(MAX))), '')
		SET @SDTable = @SDTable + '</table>'
		SELECT REPLACE(REPLACE(@SDTable,  '&lt;', '<'), '&gt;', '>') AS ViewSettleDocs
	END
	ELSE
	BEGIN
		SELECT '' AS 'ViewSettleDocs'
	END

--WORKSITE EXPOSURE CHART 
	DECLARE @CountWEC int
	SET @CountWEC = (SELECT COUNT(d.DocumentID)
						 FROM tblDocuments d
							INNER JOIN tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
						 WHERE d.ClaimantDocumentTypeID = 274
							AND mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	)	

	IF(SELECT @CountWEC) > 0
		BEGIN
			SELECT TOP 1 '<a style="color:#0645AD;" OnClick="getFile('+ '''KCICITT''' + ', ''' + AmazonKey + ''');" Class="anchorStyle" >'  + DocumentTitle + '</a>' AS ViewWEC
			FROM tblDocuments d
				INNER JOIN tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
				INNER JOIN tblClaimantPersonalInfo cpi on mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
			WHERE d.ClaimantDocumentTypeID = 274
				AND mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		END
	ELSE
		BEGIN
			SELECT '' AS ViewWEC
		END

--SMOKING HISTORY
DECLARE @SmokingInfo nvarchar(max) = ''
DECLARE @tdStyleON nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'

IF (SELECT Count(ClaimantSmokingHistoryID) FROM tblClaimantSmokingHistory WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID) > 0
	BEGIN

SET @SmokingInfo = @SmokingInfo + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
	

N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Packs Per Day</th>
<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Start Date</th>
<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">End Date</th>
<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Packs Per Year</th>' +
	
	
CAST ( (
SELECT	@tdStyleON AS 'td/@style', (SELECT COALESCE(CONVERT(decimal(10,1), (PacksPerDay)), 0)) as td, '',
		@tdStyleON AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(25), SmokingStart, 101), ' ')) as td, '',
		@tdStyleON AS 'td/@style', (SELECT COALESCE(CONVERT(nvarchar(25), SmokingEnd, 101), ' ')) as td, '',
		@tdStyleON AS 'td/@style', (SELECT COALESCE(Notes, ' ')) as td, ''
		FROM tblClaimantSmokingHistory
		WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	
			
	END
ELSE
	BEGIN
		SET @SmokingInfo = @SmokingInfo + 'There is no smoking history data for this claimant.<br/>'
	END
	SELECT @SmokingInfo AS SmokingInfo

END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetClaimantDetails] TO [power_user]
GO
