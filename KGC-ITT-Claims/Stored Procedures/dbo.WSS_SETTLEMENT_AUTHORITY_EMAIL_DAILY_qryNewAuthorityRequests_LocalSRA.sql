SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewAuthorityRequests_LocalSRA] 
AS
SET NOCOUNT ON

BEGIN

--Declare and set email inputs.
DECLARE @Subject      nvarchar(100)
DECLARE @LogoHeader   nvarchar(2000)


SET @Subject = 'ITT Claims New Settlement Authority Requests:  ' + CONVERT(varchar, getdate(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

DECLARE @NewAuthorityRequest TABLE (SettlementAuthorityID int,
AuthorityRequesteDate datetime,
SettlementGroup nvarchar (max),
DemandAmount money,
AuthorityAmount money,
Comment nvarchar (max),
ClaimantList nvarchar (max),
ResponseDueDate datetime,
StateID int
)
INSERT INTO @NewAuthorityRequest --columns to populate the email table
	SELECT DISTINCT
		sa.SettlementAuthorityID,
		sa.AuthorityRequestedDate,
		SettlementGroup,
		sa.DemandAmount,
		sa.AuthorityAmount,
		REPLACE(sa.Comment, CHAR(13) + CHAR(10), '<br/>') AS Comment, --Replace newlines with <br/>s  
		ClaimantList,
		ResponseDueDate AS ResponseDueDate,
		l.StateID
	FROM tblSettlementAuthority sa
	INNER JOIN tblSettlementGroup sg ON sa.SettlementGroupID = sg.SettlementGroupID
	INNER JOIN tblClaimantSettlement cs ON sg.SettlementGroupID = cs.SettlementGroupID
	INNER JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblDocuments d ON sa.DocumentID = d.DocumentID
	INNER JOIN  vSETTLEMENT_AUTHORITY_ClaimantLists claimants ON sa.SettlementGroupID = claimants.SettlementGroupID
	LEFT JOIN  vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Unfiltered fra ON sa.SettlementAuthorityID = fra.SettlementAuthorityID
	WHERE ParentSettlementAuthorityID IS NULL
		AND RequestNotificationDate IS NULL
	ORDER BY AuthorityRequestedDate DESC

	
DECLARE @ContactsByStateTable TABLE (PersonID int, StateID int)--, Email nvarchar(250))
INSERT INTO @ContactsByStateTable
SELECT PersonID, StateID
FROM tblSRAContact

DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar(max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar(max), @ClaimantID bigint--, @ClaimantList nvarchar(max)


--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.
DECLARE @PersonID nvarchar(10), @firmURL nvarchar(100)

DECLARE firm_Curs CURSOR FOR
	SELECT PersonID, 
		'https://www.kcicextranet.com/clients/ITT/ittcp/Pages/SettlementAuthority.aspx	'
	FROM @ContactsByStateTable cst
	GROUP BY PersonID

	
OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @PersonID, @firmURL
WHILE @@FETCH_STATUS = 0
BEGIN
	--Declare and populate the body of the email for individual firms
	DECLARE @BodyMessage nvarchar(MAX)
	--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
	--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
	SET @BodyMessage = @LogoHeader + 
		'<STYLE TYPE="text/css">
		<!--
		TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
		TD	
		{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
			
		}
		TH	
		{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
		UL {margin:0; 
			
		padding:0;}	


		--->
		</STYLE>'

	DECLARE @TDStyle nvarchar(max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

SET @BodyMessage = @BodyMessage + '<H4>The following are new settlement authority requests: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">'
SET @BodyMessage = @BodyMessage + N'<tr><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Settlement Group</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Linked Claimants</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Response<br/> Due Date</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Demand Amount</th><th style="font-family: Goudy Old Style;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Authority Amount</th>'


DECLARE @SettlementAuthorityID int,
@AuthorityRequesteDate datetime,
@SettlementGroup nvarchar (max),
@DemandAmount money,
@AuthorityAmount money,
@ClaimantList nvarchar (max),
@ResponseDueDate datetime

DECLARE Doc_List CURSOR FOR SELECT DISTINCT
	AuthorityRequesteDate,
	SettlementGroup,
	DemandAmount,
	AuthorityAmount,
	ClaimantList,
	ResponseDueDate
FROM @NewAuthorityRequest r
WHERE r.StateID IN (SELECT StateID FROM @ContactsByStateTable cst WHERE PersonID = @PersonID)
ORDER BY ResponseDueDate

OPEN Doc_List
FETCH NEXT FROM Doc_List INTO @AuthorityRequesteDate, @SettlementGroup, @DemandAmount, @AuthorityAmount, @ClaimantList, @ResponseDueDate
While @@FETCH_STATUS = 0

BEGIN


SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @SettlementGroup + ' </td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + @ClaimantList + ' </td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + CONVERT(nvarchar(20), @ResponseDueDate, 101) + ' </td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + '$' + CONVERT(nvarchar(20), @DemandAmount, 1) + ' </td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top"> ' + '$' + CONVERT(nvarchar(20), @AuthorityAmount, 1) + ' </td>'

--FETCH NEXT FROM Doc_List INTO @DocTitle, @clClaimantList, @ServiceDate, @State, @PlaintiffCounsel, @DefenseCounsel, @LawsuitID
FETCH NEXT FROM Doc_List INTO @AuthorityRequesteDate, @SettlementGroup, @DemandAmount, @AuthorityAmount, @ClaimantList, @ResponseDueDate --@DocTitle, @clClaimantList, @ServiceDate, @State, @PlaintiffCounsel, @DefenseCounsel, @LawsuitID


END
CLOSE Doc_List
DEALLOCATE Doc_List


SET @BodyMessage = @BodyMessage + N'</table>' + N'<br/>'
	
--Link to Settlement Authority Page
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/ITT/ittcp/Pages/SettlementAuthority.aspx">here</A> to view the Authority Requests.</b><br><br>'


	--Undo the HTML stripping from the xml path function above so embedded tags work
	SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

	--KCIC address and disclaimer
	SET @BodyMessage = @BodyMessage+ 
	'
		<table style="width: 500px;" border="0" cellpadding="4" cellspacing="5" width="500">
			<tbody>
				<tr>
					<td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
						color: #737271;">
						<span style="color: #F06021; font-weight: bold;">KCIC Claims Administration</span><br />
						</td>
				</tr>
				<tr>
					<td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
						color: #737271;">
						<img src="http://kcic.com/i/kcic-logo.png" width="103" height="93" />
					</td>
				</tr>
				<tr>
					<td style="font-size: 13px; font-family: Arial, Helvetica, sans-serif; line-height: 20px;
						color: #737271;">
						733 10th Street, NW | Suite 3001 | Washington, DC 20001
					<br />
						<a href="mailto:Claims@kcic.com" style="color: #737271;">Claims@kcic.com</a>
						|<span style="font-family: Arial, Helvetica, sans-serif; color: #F06021"> </span>202.772.2300
						| <a href="http://www.kcic.com" target="_new"
							style="color: #FF6229">kcic.com</a>
					</td>
				</tr>
			</tbody>
		</table>
	'
	  
    DECLARE @RecipientList NVARCHAR(500)
    SET @RecipientList = ''

	--Use the complaintContact field to determine the contact list.
	--Declare a cursor to create a recipient list for the email.
    DECLARE @Contact NVARCHAR(500)
    DECLARE contact_curs CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY
    FOR
        SELECT DISTINCT sau.Email
        FROM     @ContactsByStateTable cst
			INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau on cst.PersonID = sau.PersonID
        WHERE   sau.PersonID = @PersonID



    OPEN contact_curs
    FETCH NEXT FROM contact_curs INTO @contact
    WHILE @@FETCH_STATUS = 0 
        BEGIN 

            SET @RecipientList = @RecipientList + @Contact + '; '

            FETCH NEXT FROM contact_curs INTO @contact
        END
    CLOSE contact_curs
    DEALLOCATE contact_curs

	--PRINT @PersonID 

--QC Recipient List
Set @Bodymessage = @BodyMessage + '<br/> Recipients: ' + @RecipientList

IF (SELECT COUNT(StateID)
FROM @NewAuthorityRequest r
WHERE r.StateID IN (SELECT StateID FROM @ContactsByStateTable cst WHERE PersonID = @PersonID)) > 0
BEGIN

----Execute stored procedure to send the email to that firm
--EXEC msdb.dbo.sp_send_dbmail
--@profile_name='Claims',
--@recipients = @RecipientList,
--@copy_recipients = '',
--@blind_copy_recipients = 'itt@kcic.com',
--@subject = @subject,
--@body = @BodyMessage,
--@body_format = 'html'

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
@profile_name='Claims',
@recipients = 'snyderd@kcic.com',
@copy_recipients = '',
@blind_copy_recipients = '',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

END
	--Move to the next firm to create a new email
	FETCH NEXT FROM firm_curs INTO @PersonID, @firmurl
END
CLOSE firm_curs
DEALLOCATE firm_curs


END		




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewAuthorityRequests_LocalSRA] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewAuthorityRequests_LocalSRA] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewAuthorityRequests_LocalSRA] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewAuthorityRequests_LocalSRA] TO [power_user]
GO
