SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptAllocationByPolicyByExpectedPayYear]
(@allocationID int = null, @insuranceProgramID int = NULL, 
@carriergroupid int = NULL, @CarrierID int = null, 
@carrierSolvency varchar(50) = NULL)

AS

SET NOCOUNT ON

 
 --DECLARE @allocationid int
 --DECLARE @insuranceprogramid int
 --DECLARE @carriergroupid int
 --DECLARE @carrierid int
 --DECLARE @carrierSolvency varchar(50)
 
 --SET @allocationid = 56
 --SET @insuranceprogramid = 0
 --SET @carriergroupid = 0
 --SET @CarrierID = 0
 --SET @carrierSolvency = null
 
  DECLARE @limittypeid int
  SET @limittypeid = (SELECT LimitTypeID FROM tblAllocation WHERE AllocationID = @allocationID)

DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @maxInsuranceProgramID int
--SET Variables
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxInsuranceProgramID = MAX (InsuranceProgramID) FROM vPolicy

Declare @ExhaustionTypeID int
Set @exhaustiontypeid = (Select ExhaustionTypeID From tblAllocation Where AllocationID = @allocationID)

Declare @ExhaustionDateID int
set @ExhaustionDateID = (Select ExhaustionDateID from tblAllocation where AllocationID = @allocationid)

/* Total Allocation Information */
------------------------

DECLARE @Allocation TABLE (AllocationID int, TotalAllocation money, PVTotalAllocation money)
INSERT INTO @Allocation

SELECT AllocationID, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
WHERE AllocationID = @allocationID
GROUP BY AllocationID

/* Carrier Information */
------------------------
DECLARE @CarrierAllocation TABLE (AllocationID int, CarrierID int, CarrierTotalAllocation money, PVCarrierTotalAllocation money)
INSERT INTO @CarrierAllocation

SELECT AllocationID, CarrierID, SUM(Allocation) AS CarrierTotalAllocation, SUM(PV) AS PVCarrierTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @allocationID AND LimitTypeID = @limittypeid
GROUP BY AllocationID, CarrierID


/* Carrier Group Information */
------------------------
DECLARE @CarrierGroupAllocation TABLE (AllocationID int, CarrierGroupID int, CarrierGroupTotalAllocation money, PVCarrierGroupTotalAllocation money)
INSERT INTO @CarrierGroupAllocation

SELECT AllocationID, CarrierGroupID, SUM(Allocation) AS CarrierGroupTotalAllocation, SUM(PV) AS PVCarrierGroupTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @allocationID AND LimitTypeID = @limittypeid
GROUP BY AllocationID, CarrierGroupID



/* Exhaustion Information */
------------------------

DECLARE @exh table (ExhaustionTypeID int, PolicyDateID int, PreviouslyExhaustedAggregate money)
INSERT INTO @exh (ExhaustionTypeID, PolicyDateID, PreviouslyExhaustedAggregate)
SELECT ExhaustionTypeID, e.PolicyDateID, SUM(IndemnityAllocation + DefenseAllocation)
FROM tblExhaustion e 
	INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID
	INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
WHERE ExhaustionTypeID = @exhaustionTypeID
	AND ExhaustionDate < = (select ExhaustionDate from tblExhaustionDate where ExhaustionDateID = @ExhaustionDateID)
GROUP BY ExhaustionTypeID, e.PolicyDateID


/* Policy Information */
------------------------
DECLARE @Policies table (PolicyID int, CGID int, PolicyDateid int, CarrierGroupName nvarchar(101),
CarrierName nvarchar(101), PolicyNum nvarchar(101), Pstartdate datetime, Penddate datetime, ActualPStartDate datetime, ActualPEndDate datetime,
Attachmentpoint money, PerOccLimit money, insuranceprogramid int, insuranceprogram nvarchar(101),
LayerPercent decimal(18,15), LayerPerOccLimit money, Exhaustion money)

INSERT INTO @Policies (PolicyID, CGID, PolicyDateID, CarrierGroupName, CarrierName, PolicyNum, Pstartdate,Penddate,  ActualPStartDate, 
ActualPEndDate, AttachmentPoint, PerOccLimit, insuranceprogramid, insuranceprogram, LayerPercent, LayerPerOccLimit, Exhaustion)

SELECT p.PolicyID,
CASE WHEN cg.CarrierGroupID is NULL THEN c.CarrierID ELSE cg.CarrierGroupID END as CGID,
 pd.PolicyDateID, cg.CarrierGroupName, CarrierName, PolicyNum, Pstartdate, Penddate, ActualPStartDate, ActualPEndDate, attachmentpoint,
perocclimit, p.insuranceprogramid, i.insuranceprogram, PerOccLimit/LayerPerOccLimit AS LayerPercent, LayerPerOccLimit,
coalesce(e.PreviouslyExhaustedAggregate,0) AS Exhaustion 
FROM tblPolicy p
INNER JOIN tblPolicyDates pd ON p.policyid = pd.policyid
INNER JOIN tblPolicyLimits pl ON p.policyid = pl.policyid
INNER JOIN tblCarrier c on p.carrierid = c.carrierid
LEFT JOIN tblCarrierGroup cg ON c.carriergroupid = cg.carriergroupid
INNER JOIN tblInsuranceProgram i ON p.insuranceprogramid = i.insuranceprogramid
LEFT JOIN @exh e ON pd.PolicyDateID = e.PolicyDateID
WHERE (coalesce(c.CarrierGroupID,0) >= COALESCE(@carrierGroupID, 0) AND coalesce(c.CarrierGroupID,0) <= COALESCE(nullif(@carrierGroupID,0), @maxCarrierGroupID)) 
 AND limittypeid = @limittypeID 
 AND CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%')
 and p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
 AND p.CarrierID >= COALESCE(@carrierID, 0) AND p.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID)
 and LayerPerOccLimit >0



/* Expected Pay Years */
------------------------
DECLARE @CostsConsidered table (AllocationID int, OccurrenceID bigint, ExpectedPayDate datetime)

INSERT INTO @CostsConsidered (AllocationID, OccurrenceID, ExpectedPayDate)
SELECT @AllocationID, OccurrenceID, ExpectedPayDate
FROM tblOccurrence o
WHERE OccurrenceID NOT IN
	(SELECT OccurrenceID 
	FROM tblAllocationOccurrenceExclude
	WHERE AllocationID = @allocationID) 
	AND 
OccurrenceTypeID IN
	(SELECT OccurrenceTypeID 
	FROM  tblAllocationOccurrenceTypeInclude  
	WHERE AllocationID = @allocationID
	) AND OccurrenceID is not null
GROUP BY OccurrenceID, ExpectedPayDate

/* Allocation */
----------------


DECLARE @AllocationTotals table (AllocationID int,InsuranceProgramID int, OccurrenceID bigint,
NominalAllocation money, PV money, PolicyDateID int, DateToPresentValue datetime, PVRateDate datetime, CarrierGroupTotalAllocation money, 
PVCarrierGroupTotalAllocation money, TotalAllocation money, PVTotalAllocation money)
INSERT INTO @AllocationTotals (AllocationID, InsuranceProgramID, OccurrenceID, NominalAllocation, PV, PolicyDateID, DateToPresentValue, PVRateDate, CarrierGroupTotalAllocation,
PVCarrierGroupTotalAllocation, TotalAllocation, PVTotalAllocation)

SELECT @allocationID, @InsuranceProgramID, ar.OccurrenceID, Sum(Allocation), Sum(PV), p.PolicyDateID, DateToPresentValue, YieldCurveDate as PVRateDate,
COALESCE(CarrierGroupTotalAllocation, CarrierTotalAllocation) AS CarrierGroupTotalAllocation, COALESCE(PVCarrierGroupTotalAllocation, PVCarrierTotalAllocation) AS PVCarrierGroupTotalAllocation,
TotalAllocation, PVTotalAllocation
FROM tblAllocationResults ar 
INNER JOIN vPolicy p ON p.policydateid = ar.policydateid 
	INNER JOIN tblAllocation a ON a.AllocationID = ar.AllocationID
	INNER JOIN tblYieldCurve yc on yc.YieldCurveID = ar.yieldcurveid
	LEFT JOIN @Allocation al ON ar.AllocationID = al.AllocationID
	LEFT JOIN @CarrierAllocation cat ON ar.AllocationID = cat.AllocationID AND p.CarrierID = cat.CarrierID
	LEFT JOIN @CarrierGroupAllocation at ON ar.AllocationID = at.AllocationID AND p.CarrierGroupID = at.CarrierGroupID
WHERE a.AllocationID = @allocationID 
AND (coalesce(p.CarrierGroupID,0) >= COALESCE(@carrierGroupID, 0) AND coalesce(p.CarrierGroupID,0) <= COALESCE(nullif(@carrierGroupID,0), @maxCarrierGroupID)) 
 AND p.limittypeid = @limittypeID 
 AND CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%')
 and p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
 AND p.CarrierID >= COALESCE(@carrierID, 0) AND p.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID)
GROUP BY a.AllocationID, ar.OccurrenceID, InsuranceProgramID, p.policydateid, DateToPresentValue, YieldCurveDate, COALESCE(CarrierGroupTotalAllocation, CarrierTotalAllocation),
COALESCE(PVCarrierGroupTotalAllocation, PVCarrierTotalAllocation), TotalAllocation, PVTotalAllocation


/* Summarize Results */
-----------------------
SELECT p.PolicyID, @AllocationID AS AllocationID, COALESCE(CarrierGroupName, CarrierName) AS CarrierGroupName, CarrierName, PolicyNum, 
CONVERT(nvarchar,PstartDate,101) as startdate, 
CONVERT(nvarchar,Penddate,101) as penddate, ActualPStartDate, ActualPEndDate, p.policydateid, AttachmentPoint, 
PerOccLimit, LayerPercent, LayerPerOccLimit, COALESCE(Exhaustion,0) as Exhaustion, 
Year(ExpectedPayDate) as ExpectedPayDate, COALESCE(Sum(NominalAllocation),0) AS NominalAllocation, 
COALESCE(Sum(PV),0) as PV, pstartdate, DateToPresentValue, PVRateDate, CarrierGroupTotalAllocation, PVCarrierGroupTotalAllocation, TotalAllocation, PVTotalAllocation
FROM @CostsConsidered c
LEFT JOIN @AllocationTotals a ON c.OccurrenceID = a.OccurrenceID
LEFT JOIN @Policies p ON a.PolicydateID = p.PolicydateID
WHERE a.AllocationID = @allocationID 
AND a.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND a.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
GROUP BY p.PolicyID, a.AllocationID, COALESCE(CarrierGroupName, CarrierName), CarrierName, PolicyNum, PstartDate, Penddate, ActualPStartDate, ActualPEndDate, p.policydateid, AttachmentPoint,
PerOccLimit, Year(ExpectedPayDate), LayerPercent, LayerPerOccLimit, Exhaustion, DateToPresentValue, PVRateDate, CarrierGroupTotalAllocation, PVCarrierGroupTotalAllocation, TotalAllocation, 
PVTotalAllocation
ORDER BY CarrierName, PStartDate, AttachmentPoint, Year(ExpectedPayDate)

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByPolicyByExpectedPayYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByPolicyByExpectedPayYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByPolicyByExpectedPayYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByPolicyByExpectedPayYear] TO [power_user]
GO
