SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyClient] 
(@SettlementAuthorityID bigint)
AS
SET NOCOUNT ON

BEGIN


/*

*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)


DECLARE @ClaimantName nvarchar(500) = (SELECT LastName + ', ' + FirstName + COALESCE(' ' + MiddleName, '')
										FROM tblSettlementAuthority sa
										INNER JOIN tblClaimLawsuit			cl	ON sa.ClaimLawsuitID = cl.ClaimLawsuitID
										INNER JOIN tblClaim					c	ON cl.ClaimID = c.ClaimID
										INNER JOIN tblClaimantPersonalInfo	cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
										WHERE sa.SettlementAuthorityID = @SettlementAuthorityID)
										
SET @Subject = 	'ITT Settlement Authority Request ' + CONVERT(varchar, getdate(), 107) + ': ' + @ClaimantName 

PRINT @Subject
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'


--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
set @BodyMessage = @BodyMessage + '<H4>Settlement authority has been granted for the following claimant.  Authority process history is included below: </H4>'


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

  
DECLARE @RecipientList nvarchar(500)
SET @RecipientList=''


--QC Recipient List
--Set @Bodymessage = @BodyMessage + 'Recipients' + @RecipientList
--PRINT @RecipientList
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The Profile_Name sends from [client]@kcicllc.com email address
@profile_name='Admin',
--@recipients = 'scottc@kcicllc.com',
@recipients = 'owenss@kcicllc.com',--@RecipientList,
@copy_recipients = '',
--@recipients = 'amanp@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'



END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyClient] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyClient] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyClient] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_qryNotifyClient] TO [power_user]
GO
