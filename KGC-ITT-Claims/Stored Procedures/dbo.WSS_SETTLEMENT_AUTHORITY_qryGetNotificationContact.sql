SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetNotificationContact]
(@SettlementNotificationContactID int = NULL)
AS
BEGIN
	
		SELECT	snc.*,
			LName + COALESCE(', ' + FName, '') + ' - ' + Email + ' (' + dc.DefenseCounsel + ')' AS Contact
		FROM tblSettlementNotificationContact snc
		INNER JOIN  vSETTLEMENT_AUTHORITY_Users sau ON snc.PersonID = sau.PersonID
		INNER JOIN tblDefenseCounsel dc ON sau.FirmID = dc.DefenseCounselID
		WHERE SettlementNotificationContactID = @SettlementNotificationContactID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetNotificationContact] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetNotificationContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetNotificationContact] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qryGetNotificationContact] TO [power_user]
GO
