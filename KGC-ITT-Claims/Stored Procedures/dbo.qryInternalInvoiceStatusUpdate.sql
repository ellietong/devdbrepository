SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCedure [dbo].[qryInternalInvoiceStatusUpdate]
AS
Begin
/*Query run daily to send an internal email listing any invoices which have been approved or rejected
and are ready to be QC'd. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (2000)

SET @Subject = 'ITT: New Defense Invoices Approved/Rejected as of ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>Below are any newly approved/rejected invoices for review.</H4>'

--Declare firm counter
DECLARE @FirmCount int
SET @FirmCount = 0

--Create temp table for invoices with new status changes
DECLARE @NewInvoicesTable TABLE (docid nvarchar (100), FirmAddressID int)
INSERT INTO @NewInvoicesTable
	SELECT
		di.DocumentID,
		di.DefenseCounselAddressID
	FROM tblDocuments d
	LEFT JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID
	INNER JOIN tblDefenseInvoiceStatus s ON COALESCE(di.StatusID, 3) = s.statusid
	LEFT JOIN tblDefInvClaimantDetail dicd ON d.DocumentID = dicd.DocumentID
	LEFT JOIN tblClaim c ON dicd.ClaimantPersonalInfoID = c.ClaimantPersonalinfoID
	LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
	WHERE DateApproved BETWEEN GETDATE() - 1 AND GETDATE()
	AND d.ClaimantDocumentTypeID IN (40, 41, 254)
	GROUP BY	di.DefenseCounselAddressID,
				di.DocumentID

--Gather the new invoice information into a cursor object
DECLARE @invoicecount int
DECLARE @FirmAddressID int
DECLARE @firmName nvarchar (100)
DECLARE @firmInvoiceURL nvarchar (100)
SET @firmInvoiceURL = 'https://admin.kcicextranet.com/clients/ITT/ittcp/Pages/Invoices.aspx'
DECLARE Invoice_Curs CURSOR FOR SELECT
	COUNT(docid),
	dca.DefenseCounselAddressID,
	DefenseCounsel + ' (' + COALESCE(DefenseCounselCity, ' ') + ')'
FROM @NewInvoicesTable i
INNER JOIN tblDefenseCounselAddress dca ON i.FirmAddressID = dca.DefenseCounselAddressID
GROUP BY	dca.DefenseCounselAddressID,
			DefenseCounsel + ' (' + COALESCE(DefenseCounselCity, ' ') + ')'

--Open the invoice cursor
OPEN Invoice_Curs

--Fetch the first record from the cursor
FETCH NEXT FROM Invoice_Curs INTO @Invoicecount, @FirmAddressID, @firmName

WHILE @@FETCH_STATUS = 0 BEGIN
SET @FirmCount = @FirmCount + 1
SET @BodyMessage = @BodyMessage + CONVERT(nvarchar(10), (SELECT
	@invoicecount)
) + ' New Invoices Were Approved or Rejected in the <A HREF=' + (SELECT
	@firmInvoiceURL)
+ '>' + @firmName + '</a> Invoice Library.<br><br>'
FETCH NEXT FROM Invoice_Curs INTO @Invoicecount, @FirmAddressID, @firmName
END

CLOSE Invoice_Curs
DEALLOCATE Invoice_Curs

SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>
 
This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person respoDemo Clientble for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

Print @BodyMessage


--Only send email if there are new status updates.
IF (@FirmCount > 0)
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'itt@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'

End


GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalInvoiceStatusUpdate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalInvoiceStatusUpdate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalInvoiceStatusUpdate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalInvoiceStatusUpdate] TO [power_user]
GO
