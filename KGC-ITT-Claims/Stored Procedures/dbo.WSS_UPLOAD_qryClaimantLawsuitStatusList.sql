SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList]
AS
SET NOCOUNT ON

SELECT cs.ClaimLawsuitStatusID, 
	ClaimLawsuitStatus 
FROM tblClaimLawsuitStatus cs
inner join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitStatusID=cs.ClaimLawsuitStatusID
--WHERE ClaimLawsuitStatusID IN (15, 16, 21, 22, 23, 25, 17, 19, 27, 34, 35, 37)
UNION
SELECT NULL, ''
ORDER BY ClaimLawsuitStatus


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [power_user]
GO
