SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityChildResponses] (@SettlementAuthorityResponseID bigint, @emailaddress nvarchar(250))
AS
BEGIN
	--DECLARE @SettlementAuthorityID int = 1
	SET NOCOUNT ON;

	DECLARE @ResponsesWithChildren TABLE (SettlementAuthorityResponseID bigint)
	INSERT INTO @ResponsesWithChildren
	SELECT ParentSettlementAuthorityResponseID AS SettlementResponseID
	FROM tblSettlementAuthorityResponse
	GROUP BY ParentSettlementAuthorityResponseID


	DECLARE @ClientFirmAddressID bigint = 264 --ITT
	DECLARE @NCCFirmAddressID bigint = 330 --McGuire Woods
	DECLARE @UserFirmAddressID bigint = (SELECT FirmAddressID FROM  vSETTLEMENT_AUTHORITY_Users WHERE Email = @emailaddress)

	SELECT	sar.CreatedBy,
			sar.DateCreated,
			sar.ParentSettlementAuthorityResponseID,
			sar.SettlementAuthorityID,
			sar.SettlementAuthorityResponseID,
			sar.SettlementAuthorityStatusID,
			REPLACE(sar.Comment, CHAR(13)+CHAR(10), '<br/>') AS Comment, --Replace newlines with <br/>s 
			SUM(CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END) OVER (PARTITION BY ParentSettlementAuthorityResponseID) - CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END AS Depth,
			CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END AS HasChildren,
			SettlementAuthorityStatus,
			CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END AS IsParent,
			SUM(CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END) OVER (PARTITION BY ParentSettlementAuthorityResponseID)*2 AS DepthEMs,
			CASE WHEN sau.Email LIKE '%resolute-midwest.com' THEN DefenseCounsel + ' - Midwest' 
														WHEN sau.Email LIKE '%resolute-ne.com' THEN DefenseCounsel + ' - New England' 
														ELSE COALESCE(DefenseCounsel, 'KCIC') END AS Firm,
			1 AS CanRespond
	FROM tblSettlementAuthorityResponse sar
	LEFT JOIN tblSettlementAuthorityStatus sas ON sar.SettlementAuthorityStatusID = sas.SettlementAuthorityStatusID
	LEFT JOIN @ResponsesWithChildren children ON sar.SettlementAuthorityResponseID = children.SettlementAuthorityResponseID
	LEFT JOIN  vSETTLEMENT_AUTHORITY_Users sau ON sar.CreatedBy = sau.Email
	LEFT JOIN tblDefenseCounselAddress dca ON sau.FirmAddressID = dca.DefenseCounselAddressID
	WHERE ParentSettlementAuthorityResponseID = @SettlementAuthorityResponseID--ParentSettlementAuthorityResponseID IS NULL
			AND sar.SettlementAuthorityResponseID <> ParentSettlementAuthorityResponseID
			--AND CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END = 1
	ORDER BY	CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END DESC,
				SUM(CASE WHEN children.SettlementAuthorityResponseID IS NOT NULL THEN 1 ELSE 0 END) OVER (PARTITION BY ParentSettlementAuthorityResponseID) - CASE WHEN ParentSettlementAuthorityResponseID = sar.SettlementAuthorityResponseID THEN 1 ELSE 0 END ASC, 
				ParentSettlementAuthorityResponseID ASC
	
	


END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityChildResponses] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityChildResponses] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityChildResponses] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_AUTHORITY_qrySettlementAuthorityChildResponses] TO [power_user]
GO
