SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_AUDIT_qryClaimantList]

AS


SELECT
	cpi.ClaimantPersonalInfoID AS ClaimantID,
	LastName + ', ' + FirstName AS ClaimantName
FROM tblClaimantPersonalInfo cpi
WHERE cpi.ClaimantPersonalInfoID IN (57880,
1175,
3058,
3952,
6250,
2610,
3165,
4998,
3927,
5663,
57047,
7131,
7699,
1377,
3175,
3522,
3232,
2796,
3370,
5490,
3799,
2537,
7942,
7095,
3233,
2617,
1849,
2803,
1181,
45781,
2854,
2919,
5494,
9207,
7145,
2580,
9208,
57413,
2644,
1385,
3748,
2904,
4339,
2351,
6710,
57354,
58628,
3378,
1414,
2881,
3369,
4280,
5834,
2973,
2645,
801,
2841,
2242,
3172,
5496,
4252,
4337,
1855,
3513,
3960,
4365,
2446,
4891,
5555,
4922,
1154,
1277,
3191,
3411,
5835)
ORDER BY cpi.LastName
GO
GRANT EXECUTE ON  [dbo].[WSS_AUDIT_qryClaimantList] TO [power_user]
GO
