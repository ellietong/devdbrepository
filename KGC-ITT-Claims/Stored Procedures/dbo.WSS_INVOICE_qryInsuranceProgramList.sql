SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICE_qryInsuranceProgramList] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT InsuranceProgramID, InsuranceProgram
	FROM tblInsuranceProgram
	WHERE insuranceprogramid <>4
	ORDER BY InsuranceProgram


END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryInsuranceProgramList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryInsuranceProgramList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryInsuranceProgramList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryInsuranceProgramList] TO [power_user]
GO
