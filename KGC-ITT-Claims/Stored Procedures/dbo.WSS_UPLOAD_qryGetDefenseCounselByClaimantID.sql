SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryGetDefenseCounselByClaimantID] (@ClaimantPersonalInfoID bigint)
AS
SET NOCOUNT ON

SELECT DefenseCounselID, 
	dca.DefenseCounselAddressID
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l					ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblDefenseJurisdiction dj	ON l.DefenseCounselAddressID = dj.DefenseCounselAddressID AND l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID 
	INNER JOIN tblDefenseCounselAddress dca	ON dj.DefenseCounselAddressID = dca.DefenseCounselAddressID
WHERE COALESCE(c.IsCurrentClaim, 0) = 1 
	AND COALESCE(IsPrimaryForClaim, 0) = 1 
	AND c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetDefenseCounselByClaimantID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetDefenseCounselByClaimantID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetDefenseCounselByClaimantID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetDefenseCounselByClaimantID] TO [power_user]
GO
