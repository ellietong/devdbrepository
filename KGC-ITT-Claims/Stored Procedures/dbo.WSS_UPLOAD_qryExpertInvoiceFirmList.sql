SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryExpertInvoiceFirmList]
AS
SET NOCOUNT ON

SELECT ExpertFirmID, 
	ExpertFirm 
FROM m_tblExpertFirm
ORDER BY ExpertFirm

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryExpertInvoiceFirmList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryExpertInvoiceFirmList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryExpertInvoiceFirmList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryExpertInvoiceFirmList] TO [power_user]
GO
