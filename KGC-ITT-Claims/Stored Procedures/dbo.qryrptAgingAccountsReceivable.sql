SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAgingAccountsReceivable] (@BillDate datetime, @paymentdate datetime)
AS
Set NoCount On



-- Declare @Billdate Datetime
-- Set @Billdate = '9/1/2011'

--Declare @paymentdate Datetime
-- Set @paymentdate = '10/1/2011'


Declare @ledger1 table (LedgerID int, CarrierID int, Debit money)
Insert Into @Ledger1
Select Ledgerid, CarrierID, coalesce(sum(Debit),0)
From tblLedger 
where @BillDate-LedgerDate<=30  and @BillDate-LedgerDate >= 0
Group By CarrierID, Ledgerid


Declare @ledger2 table (LedgerID int, CarrierID int, Debit money)
Insert Into @Ledger2
Select LedgerID,CarrierID, coalesce(sum(Debit),0)
From tblLedger 
where @BillDate-LedgerDate>30 and @BillDate-LedgerDate<=60
Group By CarrierID,LedgerID

Declare @ledger3 table (LedgerID int, CarrierID int, Debit money)
Insert Into @Ledger3
Select LedgerID,CarrierID, coalesce(sum(Debit),0)
From tblLedger 
where @BillDate-LedgerDate>60 and @BillDate-LedgerDate<=90
Group By CarrierID,LedgerID

Declare @ledger4 table (LedgerID int, CarrierID int, Debit money)
Insert Into @Ledger4
Select LedgerID,CarrierID, coalesce(sum(Debit),0)
From tblLedger 
where @BillDate-LedgerDate>90
Group By CarrierID,LedgerID

Declare @payment1 table (ledgerid int, carrierid int, Payment money)
Insert Into @payment1
Select p.ledgerid, carrierid, coalesce(sum(payment),0)
From tblPayment p
inner join tblLedger l on l.LedgerID=p.LedgerID
where p.LedgerID in (select LedgerID from @ledger1) and paymentdate<@paymentdate
Group By CarrierID, p.ledgerid


Declare @payment2 table (ledgerid int, carrierid int, Payment money)
Insert Into @payment2
Select p.ledgerid, carrierid, coalesce(sum(payment),0)
From tblPayment p
inner join tblLedger l on l.LedgerID=p.LedgerID
where p.LedgerID in (select LedgerID from @ledger2) and paymentdate<@paymentdate
Group By CarrierID, p.ledgerid


Declare @payment3 table (ledgerid int, carrierid int, Payment money)
Insert Into @payment3
Select p.ledgerid, carrierid, coalesce(sum(payment),0)
From tblPayment p
inner join tblLedger l on l.LedgerID=p.LedgerID
where p.LedgerID in (select LedgerID from @ledger3) and paymentdate<@paymentdate
Group By CarrierID, p.ledgerid

Declare @payment4 table (ledgerid int, carrierid int, Payment money)
Insert Into @payment4
Select p.LedgerID, carrierid, coalesce(sum(payment),0)
From tblPayment p
inner join tblLedger l on l.LedgerID=p.LedgerID
where L.LedgerID in (select LedgerID from @ledger4) and paymentdate<@paymentdate
Group By CarrierID, p.LedgerID


Declare @AR1 Table (CarrierID int, AccountReceivable money)
Insert Into @AR1
Select l.CarrierID, Sum(Coalesce(L.Debit,0)-coalesce(P.payment,0)) AS AccountReceivable
From @ledger1 L 
Left Join @payment1 P on  L.ledgerID=P.ledgerid
Group By  L.CarrierID


Declare @AR2 Table (CarrierID int, AccountReceivable money)
Insert Into @AR2
Select l.CarrierID,  Sum(Coalesce(L.Debit,0))-sum(coalesce(P.payment,0)) AS AccountReceivable
From @ledger2 L 
Left Join @payment2 P on L.ledgerID=P.ledgerid
Group By  L.CarrierID

Declare @AR3 Table (CarrierID int, AccountReceivable money)
Insert Into @AR3
Select l.CarrierID,  Sum(Coalesce(L.Debit,0)-coalesce(P.payment,0)) AS AccountReceivable
From @ledger3 L 
Left Join @payment3 P on  L.ledgerID=P.ledgerid
Group By  L.CarrierID

Declare @AR4 Table (CarrierID int, AccountReceivable money)
Insert Into @AR4
Select l.CarrierID,  Sum(Coalesce(L.Debit,0)-coalesce(P.payment,0)) AS AccountReceivable
From @ledger4 L 
Left Join @payment4 P on L.ledgerID=P.ledgerid
Group By  L.CarrierID


Declare @Final table (CarrierName nvarchar(2000), AccountReceivable1 money, AccountReceivable2 money, AccountReceivable3 money, AccountReceivable4 money)
Insert Into @Final
Select CarrierName, AR1.AccountReceivable, AR2.AccountReceivable, AR3.AccountReceivable,
      AR4.AccountReceivable
From tblCarrier C
Full Outer Join @AR1 AR1 on C.CarrierID=AR1.CarrierID
Full Outer Join @AR2 AR2 on C.CarrierID=AR2.CarrierID
Full Outer Join @AR3 AR3 on C.CarrierID=AR3.CarrierID
Full Outer Join @AR4 AR4 on C.CarrierID=AR4.CarrierID
Where (AR1.AccountReceivable <> 0 or AR2.AccountReceivable <> 0 or AR3.AccountReceivable <> 0 or
      AR4.AccountReceivable <> 0) and C.carrierID not in (100000)
      

Union

Select  'Marley-Wylain - Producer' as CarrierName, '0.00' as AccountReceivable1, '0.00' as AccountReceivable2, '0.00' as AccountReceivable3,
      '0.00' as AccountReceivable4
From tblCarrier C
Full Outer Join @AR1 AR1 on C.CarrierID=AR1.CarrierID
Full Outer Join @AR2 AR2 on C.CarrierID=AR2.CarrierID
Full Outer Join @AR3 AR3 on C.CarrierID=AR3.CarrierID
Full Outer Join @AR4 AR4 on C.CarrierID=AR4.CarrierID
Where  C.carrierID = 100000
      

Select  CarrierName, Sum(Coalesce(AccountReceivable1,0)) as AccountReceivable1, Sum(Coalesce(AccountReceivable2,0)) as AccountReceivable2, Sum(Coalesce(AccountReceivable3,0)) as AccountReceivable3, Sum(Coalesce(AccountReceivable4,0)) as AccountReceivable4, coalesce(SUM(accountreceivable1+accountreceivable2+AccountReceivable3+AccountReceivable4),0) as TotalAccountReceivable, @paymentdate as paymentdate
From @Final F
Group By CarrierName
Order By CarrierName

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAgingAccountsReceivable] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAgingAccountsReceivable] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAgingAccountsReceivable] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAgingAccountsReceivable] TO [power_user]
GO
