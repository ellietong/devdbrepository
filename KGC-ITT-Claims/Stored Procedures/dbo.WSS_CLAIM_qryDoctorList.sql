SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIM_qryDoctorList]
AS

SET NOCOUNT ON

SELECT DoctorID, 
	UPPER(DoctorLastName + ', ' + COALESCE(DoctorFirstName, '') + ' ' + COALESCE(DoctorMiddleName, '')) AS Doctor 
FROM tblDoctor
UNION
SELECT NULL AS DoctorID, 
	'' AS Doctor 
FROM tblDoctor
ORDER BY Doctor

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDoctorList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDoctorList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDoctorList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDoctorList] TO [power_user]
GO
