SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROC [dbo].[qryInternalSBNDClaimsWithAllDocs]
AS
BEGIN
 
/*Query run daily to send an internal email listing any SBND claims that contain full documentation
and data for settled status. - CAS 12-5-12*/
 
--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(max)
 
 
Declare @Today datetime
SET @Today = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 16:00:00.000')
 
 
 
SET @Subject =    'ITT: SBND Claims with Full Data/Documentation'
--Set body font, insert KCIC logo/banner at the top of the email, and set table styles. 
--These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>The following claimants are currently SBND but are fully Documented: </H4>
<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'
 
DECLARE @TDStyle nvarchar(max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'
 
 
--Create temp table of Claimants who are SBND but are fully documented
DECLARE @Product table (claimantpersonalinfoid int, productentered int)
insert into @Product 
select ce.ClaimantPersonalInfoID, case when cp.ProductID is null or cp.StartDate is null or cp.EndDate is null  then 0  else 1  end as productentered
from tblClaimantEmployment ce 
left join tblClaimantProduct cp on cp.ClaimantEmploymentID =ce.ClaimantEmploymentID 
where ce.IsPrimary =1 
group by ce.ClaimantPersonalInfoID , case when cp.ProductID is null or cp.StartDate is null or cp.EndDate is null  then 0  else 1  end 
 
DECLARE @ClaimantDocs TABLE(ClaimantID int, FName nvarchar(50), LName nvarchar(200),
ClaimantIDed int, ClaimantSSN int, ClaimantBirthdate int, RPLegalcapacity int, RPrelationship int, RPaddress int, RPphone int, RPSSN int,  DxMedicals int,
Disease int, DiseaseDiagnosisdate datetime, DODCert int, EstatePapers int, productid int,enteredproductid int, EmployExpoHist int, settlementconfirmation int, Legalcapacity int,
releasereceived int)
 
INSERT INTO @ClaimantDocs
	select cpi.ClaimantPersonalInfoID, cpi.FirstName, cpi.LastName,
	sd.ClaimantIDed,case when socialsecuritynumber is null then 0 else 1 end as SSN, 
	case when birthdate is null then 0 else 1 end as birthdate,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and LegalCapacityID is null then 0 
	when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and LegalCapacityID=14 then 0 else 1 end as RPLegalCapacity,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RelationshipID is null then 0 
	when relationshipid=6 or RelationshipID is null and crp.firstname is not null then 0 else 1 end as RPRelationship,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RPAddress1 is null and MedicareBeneficiaryStatusID in (1,2) then 0 else 1 end as RPaddress,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RPPhone is null and MedicareBeneficiaryStatusID in (1,2)then 0 else 1 end as RPPhone,
	case when dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' and DeceasedDate is not null and RPSocialSecurityNumber is null and MedicareBeneficiaryStatusID in (1,2) then 0 else 1 end as RPSSN , 
	sd.DxMedicals, DiseaseID,
	DiseaseDiagnosisDate, 	
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE  sd.DODCert END AS DODCert, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE  sd.EstatePapers END AS Estatepaper,
		sd.productid,
		p.productentered,  sd.EmployExpoHist,  sd.settlementconfirmation,
		case when DeceasedDate is not null and LegalCapacityid is null then 0  else 1 end as LegalCapacity,
		--case when ExecutedReleaseDate IS null then 0 else 1 end as ReleaseReceived
		case when (d.ClaimantPersonalInfoID IS not null OR ExecutedReleaseDate IS not null) then 1 else 0 end as releasereceived
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
		INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
		inner join @Product p						 ON p.claimantpersonalinfoid =cpi.ClaimantPersonalInfoID 
		INNER JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		inner join tblSettlementDocumentation sd on sd.SetPymtID=spd.SetPymtID
		inner JOIN tblClaimDisease cdi				 ON cdi.ClaimID = c.ClaimID
		inner join tblClaimLawsuitStatusTracking cls on cls.ClaimLawsuitID =cl.ClaimLawsuitID 
		left join tblClaimantRelatedParty crp		ON crp.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
		left join (select claimantpersonalinfoid from tblMultiClaimantDocument mcd inner join tbldocuments d on d.DocumentID = mcd.DocumentID where ClaimantDocumentTypeID = 31 group by ClaimantPersonalInfoID) d on d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	WHERE IsPrimaryStatus =1 and ClaimLawsuitStatusID in (25) and IsPrimaryForClaim =1
 
DECLARE @ClaimantsDocumented TABLE(ClaimantID nvarchar(max), DocStatus nvarchar (50))
INSERT INTO @ClaimantsDocumented
Select Lname + ', '+Fname+' ('+'<a href="https://admin.kcicextranet.com/clients/ITT/ittcp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +coalesce(CONVERT(nvarchar(50),claimantID), '')+'">'+ coalesce(CONVERT(nvarchar(50), claimantID), '')+'</a>'+')', 
 'Claimant Fully Documented'
from @ClaimantDocs
where ClaimantIDed = 1 AND ClaimantSSN = 1 AND claimantBirthdate = 1 AND RPLegalcapacity = 1	AND
RPrelationship = 1 AND RPaddress = 1 AND RPphone = 1 AND RPSSN = 1 AND
DxMedicals <>0 AND coalesce(Disease,0) <>0 AND DiseaseDiagnosisdate is not null AND DODCert <>0 AND
EstatePapers <>0 AND ProductID <>0 AND EnteredProductID = 1 AND EmployExpoHist <>0 AND settlementconfirmation <>0 and Legalcapacity <>0
--and releasereceived <>0
group by  Lname + ', '+Fname+' ('+'<a href="https://admin.kcicextranet.com/clients/ITT/ittcp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +coalesce(CONVERT(nvarchar(50),claimantID), '')+'">'+ coalesce(CONVERT(nvarchar(50), claimantID), '')+'</a>'+')'
 
 
-------------------------------------------------------------------------
--Create email table of claimants
SET @BodyMessage = @bodymessage + 
    N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
    --Set column headings and populate cells from the cursor
    N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Documentation Status</th>' +
    CAST ( (
          SELECT	
			@TDStyle AS 'td/@style', (SELECT ClaimantID) as td, '',
			@TDStyle AS 'td/@style', (SELECT DocStatus) as td, ''
          FROM @ClaimantsDocumented C
          order by claimantID
          for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
    N'</table>' +  N'<br/>' 
    
--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')
 
 
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'
 
--Check if the email needs to be sent
IF (SELECT COUNT(DISTINCT claimantID)
FROM @ClaimantsDocumented)<>0 
 
BEGIN
      --Send email
      EXEC msdb.dbo.sp_send_dbmail
      @profile_name='Claims',
      --@recipients = 'scottc@kcic.com', 
      @recipients = 'itt@kcic.com',
      --@copy_recipients = 'prossc@kcic.com',
      @subject= @subject,
      @body= @BodyMessage,
      @body_format = 'html'
END
 
END
 

GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [power_user]
GO
