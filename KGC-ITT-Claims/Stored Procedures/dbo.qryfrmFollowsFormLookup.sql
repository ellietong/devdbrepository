SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmFollowsFormLookup]
As

SET NOCOUNT ON

SELECT p.PolicyID, CONVERT(varchar, p.PolicyID, 101) + ' - '  + CONVERT(varchar, AnnualPeriod, 101) + ' ' +CarrierName + ': ' + PolicyNum AS PolicyInfo
FROM (tblCarrier c INNER JOIN tblPolicy p ON c.CarrierID = p.CarrierID) INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmFollowsFormLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmFollowsFormLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmFollowsFormLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmFollowsFormLookup] TO [power_user]
GO
