SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_REPORTS_qryClaimantInfoCategoryList]
AS
SET NOCOUNT ON

SELECT *, CASE WHEN ClaimantInfoCategoryID = 1001 THEN 'SSN' 
			   WHEN ClaimantInfoCategoryID = 1002 THEN 'BirthDate'
			   WHEN ClaimantInfoCategoryID = 1003 THEN 'DiseaseID' 
			   WHEN ClaimantInfoCategoryID = 1004 THEN 'DiagnosisDate' 
			   WHEN ClaimantInfoCategoryID = 1005 THEN 'ProductEntered' 
			   WHEN ClaimantInfoCategoryID = 1006 THEN 'LegalCapacity' 
			   END AS MSDCategory
FROM tblClaimantInformationCategory
UNION
SELECT NULL, '', ''
ORDER BY ClaimantInfoCategory



GO
