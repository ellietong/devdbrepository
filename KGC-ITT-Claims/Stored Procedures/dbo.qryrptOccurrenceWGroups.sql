SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  Procedure [dbo].[qryrptOccurrenceWGroups]
(@CorporateProfileID INT = NULL, @InClaim int, @selectedOccTypes nvarchar(1000) = NULL, @selectedOccGroups nvarchar(1000) = NULL)  
As

SET NOCOUNT ON

--DECLARE @CorporateProfileID int
--SET @CorporateProfileID = 0
--DECLARE @InClaim bit
--SET @InClaim = 1

DECLARE @maxCorporateProfileID int
SELECT @maxCorporateProfileID = MAX(CorporateHistoryID) FROM vOccurrence

--Declare backup strings of the passed in lists to be used in the ultimate where clauses below
DECLARE @selectedProvisionsList nvarchar(1000)
SET @selectedProvisionsList = @selectedOccTypes
DECLARE @selectedProvisionValuesList nvarchar(1000)
SET @selectedProvisionValuesList = @selectedOccGroups

--Declare the tables to be used for the separated provision and provision value IDs
DECLARE @selectedOccTypesTable TABLE(OccTypeID int)
DECLARE @selectedOccGroupsTable TABLE(OccGroupID int)

--Prepare flags if there is only a zero in the table (and thus there should be no filter)
DECLARE @flagOccTypeFilter int
DECLARE @flagOccGroupFilter int
SET @flagOccTypeFilter = 0
SET @flagOccGroupFilter = 0
--add params for occType and occGroup
--add parsing of params into temp tables
-- add where for vars in temp tables
--Parse out occtypes	
WHILE LEN(@selectedOccTypes)>0 BEGIN
	IF CHARINDEX(',',@selectedOccTypes) = 0 BEGIN
		INSERT INTO @selectedOccTypesTable
			SELECT CONVERT(int, @selectedOcctypes)
		IF CONVERT(int,@selectedOccTypes) <> 0
			SET @flagOccTypeFilter = 1
		SET @selectedOcctypes = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedOccTypesTable
			SELECT CONVERT(int, LEFT(@selectedOccTypes, CHARINDEX(',',@selectedOccTypes)-1))
		SET @selectedOccTypes = RIGHT(@selectedOccTypes,LEN(@selectedOccTypes)-CHARINDEX(',',@selectedOccTypes))
		SET @flagOccTypeFilter = 1
	END
END
----Parse out occgroups	
WHILE LEN(@selectedOccGroups)>0 BEGIN
	IF CHARINDEX(',',@selectedOccGroups) = 0 BEGIN
		INSERT INTO @selectedOccGroupsTable
			SELECT CONVERT(int, @selectedOccGroups)
		IF CONVERT(int,@selectedOccGroups) <> 0
			SET @flagOccGroupFilter = 1
		SET @selectedOccGroups = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedoccgroupsTable
			SELECT CONVERT(int, LEFT(@selectedoccgroups, CHARINDEX(',',@selectedOccGroups)-1))
		SET @selectedOccGroups = RIGHT(@selectedOccGroups,LEN(@selectedOccGroups)-CHARINDEX(',',@selectedOccGroups))
		SET @flagOccGroupFilter = 1
	END
END


--   -1 = All Occurrences
IF @InClaim = -1
BEGIN
	SELECT @InClaim = 0
	
	IF @flagOccGroupFilter = 1 AND @flagOccTypeFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, 
			CorporateHistoryID, OccurrenceTypeID, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, 
			TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, 
			Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceGroup, OccurrenceType, CorporateHistoryProfile 
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
			AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
	END
	IF @flagOccGroupFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, 
			CorporateHistoryID, OccurrenceTypeID, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, 
			TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, 
			Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceGroup, OccurrenceType, CorporateHistoryProfile 
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
	END
	IF @flagOccTypeFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, 
			CorporateHistoryID, OccurrenceTypeID, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, 
			TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, 
			Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceGroup, OccurrenceType, CorporateHistoryProfile 
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
	END
	ELSE
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, 
			CorporateHistoryID, OccurrenceTypeID, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, 
			TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, 
			Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceGroup, OccurrenceType, CorporateHistoryProfile 
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
	END
END 
ELSE
BEGIN
	IF @flagOccGroupFilter = 1 AND @flagOccTypeFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, 
			CorporateHistoryID, OccurrenceTypeID, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, 
			TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, 
			Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceGroup, OccurrenceType, CorporateHistoryProfile 
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), @InClaim)
			AND OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
			AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
	END
	ELSE IF @flagOccTypeFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, 
			CorporateHistoryID, OccurrenceTypeID, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, 
			TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, 
			Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceGroup, OccurrenceType, CorporateHistoryProfile 
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), @InClaim)
			AND OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
	END
	IF @flagOccGroupFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, 
			CorporateHistoryID, OccurrenceTypeID, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, 
			TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, 
			Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceGroup, OccurrenceType, CorporateHistoryProfile 
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), @InClaim)
			AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
	END
	ELSE
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, 
			CorporateHistoryID, OccurrenceTypeID, TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, 
			TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, 
			Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceGroup, OccurrenceType, CorporateHistoryProfile 
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID) AND
			InClaim >= COALESCE(@InClaim, 0) AND InClaim <= COALESCE(NULLIF(@InClaim, 0), @InClaim)
	END
END
RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrenceWGroups] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOccurrenceWGroups] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrenceWGroups] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOccurrenceWGroups] TO [power_user]
GO
