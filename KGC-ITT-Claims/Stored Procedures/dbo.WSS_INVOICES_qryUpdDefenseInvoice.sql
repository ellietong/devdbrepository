SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_INVOICES_qryUpdDefenseInvoice]
(
	@DocID int,
	@InvoiceNumber nvarchar(250) = null,
	@Period nvarchar(50),-- = null,
	@DatePayable datetime = null,
	@InsuranceProgramID int	= null
)

AS

Update tblDefenseInvoice
SET
	InvoiceNumber			= @InvoiceNumber,
	Period					= @Period,
	DatePayable				= @DatePayable,
	InsuranceProgramID		= @InsuranceProgramID
Where DocumentID = @DocID

Select DocumentID From tblDocuments Where DocumentID = @DocID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdDefenseInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdDefenseInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdDefenseInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdDefenseInvoice] TO [power_user]
GO
