SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vMissingSettlementDocumentation]
AS
SELECT DISTINCT 
                      TOP (100) PERCENT cpi.ClaimantPersonalInfoID, cpi.FirstName, cpi.LastName, cpi.MiddleName, CONVERT(DATE, cpi.BirthDate) AS DOB, CONVERT(DATE, 
                      cpi.DeceasedDate) AS DOD, cs.SettlementDate, cs.SettlementAmount, sg.SettlementGroup, cs.SettlementGroupID, cl.ClaimLawsuitID, cl.LawsuitID, 
                      COALESCE (dbo.fnMissingClaimantInformation(cl.ClaimLawsuitID), 'All required claimant information submitted.') AS MissingClaimantInfo, 
                      COALESCE (dbo.fnMissingDocumentation(cl.ClaimLawsuitID), 'All required documents submitted.') AS MissingDocumentation, COALESCE (sd.ClaimantIDed, 0) 
                      AS ClaimantIDed, CASE WHEN socialsecuritynumber IS NULL THEN 0 ELSE 1 END AS SSN, CASE WHEN birthdate IS NULL THEN 0 ELSE 1 END AS BirthDate, 
                      COALESCE (sd.DxMedicals, 0) AS DxMedicals, CASE WHEN cdi.DiseaseID IS NULL THEN 0 ELSE 1 END AS DiseaseID, 
                      CASE WHEN cdi.DiseaseDiagnosisDate IS NULL THEN 0 ELSE 1 END AS DiagnosisDate, CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE (sd.DODCert, 
                      0) END AS DODCert, CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE (sd.EstatePapers, 0) END AS Estatepaper, COALESCE (sd.ProductID, 0) 
                      AS ProductID, COALESCE (p.productentered, 0) AS ProductEntered, COALESCE (sd.EmployExpoHist, 0) AS EmployExpoHist, COALESCE (sd.SettlementConfirmation, 0) 
                      AS SettlementConfirmation, CASE WHEN DeceasedDate IS NOT NULL AND LegalCapacityid IS NULL THEN 0 ELSE 1 END AS LegalCapacity, 
                      CASE WHEN (d_1.ClaimantPersonalInfoID IS NOT NULL OR
                      ExecutedReleaseDate IS NOT NULL) THEN 1 ELSE 0 END AS ReleaseReceived
FROM         dbo.tblClaim AS c INNER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                      dbo.tblClaimantSettlement AS cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID INNER JOIN
                      dbo.tblSettlementGroup AS sg ON cs.SettlementGroupID = sg.SettlementGroupID LEFT OUTER JOIN
                      dbo.tblSettlePaymentDocumentation AS spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblSettlementDocumentation AS sd ON sd.SetPymtID = spd.SetPymtID LEFT OUTER JOIN
                          (SELECT     cd1.ClaimDiseaseID, cd1.ClaimID, cd1.DiseaseID, cd1.PrimaryDisease, cd1.DiseaseDiagnosisDate, cd1.DoctorID, cd1.SPUsername, 
                                                   cd1.DiseaseID_OLD, cd1.IsDefenseDisease
                            FROM          dbo.tblClaimDisease AS cd1 INNER JOIN
                                                   dbo.tblDisease AS d1 ON d1.DiseaseID = cd1.DiseaseID
                            WHERE      (cd1.PrimaryDisease = 1) AND (d1.HighLevelDiseaseID <> 5)) AS cdi ON cdi.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatusTracking AS cls ON cls.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                          (SELECT     ClaimantRelatedPartyID, ClaimantPersonalInfoID, FirstName, MiddleName, LastName, RelationshipID, LegalCapacity, LegalCapacityID, RPAddress1, 
                                                   RPAddress2, RPCity, StateID, RPZip, RPMailZip, RPPhone, RPExtension, RPSocialSecurityNumber, RPTIN, SPUserName
                            FROM          dbo.tblClaimantRelatedParty
                            WHERE      (LegalCapacityID IN (1, 2, 3, 4, 5, 6, 7, 8, 15, 17, 30, 31))) AS crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                          (SELECT     mcd.ClaimantPersonalInfoID
                            FROM          dbo.tblMultiClaimantDocument AS mcd INNER JOIN
                                                   dbo.tblDocuments AS d ON d.DocumentID = mcd.DocumentID
                            WHERE      (d.ClaimantDocumentTypeID = 31)
                            GROUP BY mcd.ClaimantPersonalInfoID) AS d_1 ON d_1.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                          (SELECT     ce.ClaimantPersonalInfoID, CASE WHEN cp.ProductID IS NULL OR
                                                   cp.StartDate IS NULL OR
                                                   cp.EndDate IS NULL THEN 0 ELSE 1 END AS productentered
                            FROM          dbo.tblClaimantEmployment AS ce LEFT OUTER JOIN
                                                   dbo.tblClaimantProduct AS cp ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
                            WHERE      (ce.IsPrimary = 1) AND (cp.PrimaryProduct = 1)
                            GROUP BY ce.ClaimantPersonalInfoID, CASE WHEN cp.ProductID IS NULL OR
                                                   cp.StartDate IS NULL OR
                                                   cp.EndDate IS NULL THEN 0 ELSE 1 END) AS p ON cpi.ClaimantPersonalInfoID = p.ClaimantPersonalInfoID
WHERE     (cls.IsPrimaryStatus = 1) AND (cls.ClaimLawsuitStatusID IN (25)) AND (YEAR(cs.SettlementDate) >= 2010) AND (cdi.PrimaryDisease = 1)

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[27] 4[11] 2[44] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 7
               Left = 12
               Bottom = 115
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 139
               Left = 115
               Bottom = 247
               Right = 336
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 1
               Left = 512
               Bottom = 109
               Right = 708
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 6
               Left = 780
               Bottom = 114
               Right = 1009
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sg"
            Begin Extent = 
               Top = 114
               Left = 374
               Bottom = 222
               Right = 578
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "spd"
            Begin Extent = 
               Top = 6
               Left = 1047
               Bottom = 114
               Right = 1252
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sd"
            Begin Extent = 
               Top = 6
               Left = 1290
               Bottom = 114
               Right = 1483
            End
            DisplayFlags = 280
            TopColumn = 0
     ', 'SCHEMA', N'dbo', 'VIEW', N'vMissingSettlementDocumentation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'    End
         Begin Table = "cdi"
            Begin Extent = 
               Top = 6
               Left = 261
               Bottom = 114
               Right = 446
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cls"
            Begin Extent = 
               Top = 168
               Left = 781
               Bottom = 276
               Right = 1004
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crp"
            Begin Extent = 
               Top = 198
               Left = 780
               Bottom = 306
               Right = 976
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d_1"
            Begin Extent = 
               Top = 114
               Left = 1012
               Bottom = 177
               Right = 1205
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 114
               Left = 1243
               Bottom = 192
               Right = 1436
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vMissingSettlementDocumentation', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vMissingSettlementDocumentation', NULL, NULL
GO
