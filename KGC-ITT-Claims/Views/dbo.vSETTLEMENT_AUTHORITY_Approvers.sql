SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vSETTLEMENT_AUTHORITY_Approvers]
AS

SELECT DISTINCT P.PersonID, P.Email, FA.FirmID
FROM [Ligado-ExtranetSecurity].dbo.Person p 
		INNER JOIN [Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite pfcs on p.PersonID=pfcs.PersonID
		INNER JOIN [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite facs on pfcs.FirmAddressClientSiteID=facs.FirmAddressClientSiteID
		INNER JOIN [Ligado-ExtranetSecurity].dbo.FirmAddress fa on facs.FirmAddressID =fa.FirmAddressID
WHERE FA.FirmID IN (155,63,277) AND facs.ClientSiteID = 10

GO
