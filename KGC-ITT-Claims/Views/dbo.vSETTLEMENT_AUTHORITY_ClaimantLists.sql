SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vSETTLEMENT_AUTHORITY_ClaimantLists]
AS
SELECT        sg.SettlementGroupID, 
		'<ul style="display: none; list-style-type: none; padding: 0px; margin: 0px;" id="sgid' + CONVERT(nvarchar(10), SettlementGroupID) + '">' + CONVERT(nvarchar(MAX), (SELECT
			'a/@href' = '/clients/itt/ittcp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, c.ClaimantPersonalInfoID),
			'a' = cpi.LastName + ', ' + cpi.FirstName
		FROM tblSettlementGroup sginner
		INNER JOIN tblClaimantSettlement cs
			ON sginner.SettlementGroupID = cs.SettlementGroupID
		INNER JOIN tblClaimLawsuit cl
			ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaim c
			ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi
			ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblSettlementAuthority sa
			ON sginner.SettlementGroupID = sa.SettlementGroupID
		WHERE sginner.SettlementGroupID = sg.SettlementGroupID
		GROUP BY c.ClaimantPersonalInfoID, LastName, FirstName
		FOR xml PATH ('li'))
		) + '</ul>' AS ClaimantList,
			  '<ul style="display: none; list-style-type: none; padding: 0px; margin: 0px;" id="sgid' + CONVERT(nvarchar(10), sg.SettlementGroupID) + '">' + CONVERT(nvarchar(MAX),
                             (SELECT        'a/@OnClick' = 'getClaimantDetails(' + CONVERT(nvarchar(10), cpi.ClaimantPersonalInfoID) + ' , ' + CONVERT(nvarchar(10), MAX(SettlementAuthorityID)) + ' , ' + 'event' + ');', 
                                                         'a/@class' = 'anchorStyle', 'a' = cpi.LastName + ', ' + cpi.FirstName
                               FROM            tblSettlementGroup sginner INNER JOIN
                                                         tblClaimantSettlement cs ON sginner.SettlementGroupID = cs.SettlementGroupID INNER JOIN
                                                         tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID INNER JOIN
                                                         tblClaim c ON cl.ClaimID = c.ClaimID INNER JOIN
                                                         tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                                                         tblSettlementAuthority sa ON cs.ClaimLawsuitID = sa.ClaimLawsuitID
                               WHERE        sginner.SettlementGroupID = sg.SettlementGroupID
                               GROUP BY cpi.ClaimantPersonalInfoID, LastName, FirstName FOR XML PATH('li'))) + '</ul>' AS ClaimantListWithEvent

FROM            tblSettlementGroup sg
WHERE        sg.SettlementGroupID >= 32316
GROUP BY sg.SettlementGroupID

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSETTLEMENT_AUTHORITY_ClaimantLists', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSETTLEMENT_AUTHORITY_ClaimantLists', NULL, NULL
GO
