SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblDefenseCounselAddress]
AS
SELECT  F.FirmDisplayName AS DefenseCounsel, 
		FC.FirmAddressID AS DefenseCounselAddressID, 
		FC.FirmID AS DefenseCounselID, 
		FC.FirmAddress1 AS DefenseCounselAddress1, 
		FC.FirmAddress2 AS DefenseCounselAddress2, 
		FC.FirmCity AS DefenseCounselCity, 
		FC.FirmZip AS DefenseCounselZip, 
		FC.FirmMailZip AS DefenseCounselMailZip, 
		FC.FirmPhone AS DefenseCounselPhone, 
		s.FullNameState, 
		s.State,
		FC.TaxID,
		F.AcuityKey,
		FAC.PayeeAddress, 
        FAC.NoFilters, 
		FC.ACHRoutingNumber, 
		FC.ACHAccountNumber, 
		FC.ACHRemittanceEmail, 
		FAC.SettlementACHRoutingNumber, 
		FAC.SettlementACHAccountNumber, 
        FAC.SettlementACHRemittanceEmail
FROM [Ligado-ExtranetSecurity].dbo.FirmAddress AS FC 
		INNER JOIN [Ligado-ExtranetSecurity].dbo.Firm AS F ON FC.FirmID = F.FirmID 
		LEFT OUTER JOIN dbo.tblState AS s ON s.StateID = FC.FirmStateID 
		INNER JOIN  [Ligado-ExtranetSecurity].dbo.tblFirmAddressCase AS FAC ON FAC.FirmAddressID = FC.FirmAddressID
WHERE (FAC.CaseID = dbo.fn_GetCaseID())

GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselAddress] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselAddress] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselAddress] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselAddress] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[45] 4[16] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "FC"
            Begin Extent = 
               Top = 32
               Left = 10
               Bottom = 332
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "F"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 441
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 232
               Left = 237
               Bottom = 336
               Right = 397
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "q"
            Begin Extent = 
               Top = 141
               Left = 569
               Bottom = 260
               Right = 809
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
        ', 'SCHEMA', N'dbo', 'VIEW', N'tblDefenseCounselAddress', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'tblDefenseCounselAddress', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'tblDefenseCounselAddress', NULL, NULL
GO
