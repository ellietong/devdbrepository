SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPOLICY_AnnualPeriod]
AS
	SELECT DISTINCT AnnualPeriod
	FROM tblPolicy p
	INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID


GO
