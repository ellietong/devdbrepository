SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vOccurrence]
AS
SELECT     O.OccurrenceID, O.OccurrenceGroupID, OG.OccurrenceGroup, OG.OccGrpTriggerStart, OG.OccGrpDiscount, O.CorporateHistoryID, CHP.CorporateHistoryProfile, 
                      O.OccurrenceTypeID, OT.OccurrenceType, OT.OccurrenceTypeNotes, O.OccurrenceName, O.OccurrenceAddress, O.OccurrenceCity, O.OccurrenceStateID, ST.State, 
                      ST.FullNameState, O.AggregateApplies, O.InClaim, O.ClaimID, O.Segment, O.ExpectedPayDate, O.TriggerStart1, O.TriggerStart1Explanation, O.TriggerEnd1, 
                      O.TriggerEnd1Explanation, O.TriggerStart2, O.TriggerEnd2, O.TriggerStart3, O.TriggerEnd3, O.TriggerStart4, O.TriggerEnd4, O.Cost1, O.Cost1Explanation, O.Cost2, 
                      O.Cost2Explanation, O.Cost3, O.Cost3Explanation, O.Cost4, O.Cost5, O.Cost6, O.Cost7, O.Cost8, O.Cost9, O.Cost10, O.Cost11, O.Cost12, O.OccurrenceNote, 
                      OT.DefenseType
FROM         dbo.tblOccurrence AS O LEFT OUTER JOIN
                      dbo.tblState AS ST ON ST.StateID = O.OccurrenceStateID LEFT OUTER JOIN
                      dbo.tblOccurrenceGroup AS OG ON OG.OccurrenceGroupID = O.OccurrenceGroupID LEFT OUTER JOIN
                      dbo.tblCorporateHistoryProfile AS CHP ON CHP.CorporateHistoryID = O.CorporateHistoryID LEFT OUTER JOIN
                      dbo.tblOccurrenceType AS OT ON OT.OccurrenceTypeID = O.OccurrenceTypeID
GO
GRANT VIEW DEFINITION ON  [dbo].[vOccurrence] TO [base_user]
GRANT SELECT ON  [dbo].[vOccurrence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vOccurrence] TO [power_user]
GRANT SELECT ON  [dbo].[vOccurrence] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "O"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ST"
            Begin Extent = 
               Top = 6
               Left = 276
               Bottom = 114
               Right = 427
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "OG"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 213
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CHP"
            Begin Extent = 
               Top = 114
               Left = 251
               Bottom = 192
               Right = 444
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "OT"
            Begin Extent = 
               Top = 192
               Left = 251
               Bottom = 300
               Right = 438
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
', 'SCHEMA', N'dbo', 'VIEW', N'vOccurrence', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'End
', 'SCHEMA', N'dbo', 'VIEW', N'vOccurrence', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vOccurrence', NULL, NULL
GO
