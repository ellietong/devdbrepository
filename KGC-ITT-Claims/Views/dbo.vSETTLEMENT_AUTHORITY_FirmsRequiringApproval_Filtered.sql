SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Filtered]
AS
SELECT DISTINCT 
                sa.SettlementAuthorityID, 
				COALESCE (SAR.FirmID, SAF.FirmID) AS FirmID, 				
                CASE WHEN AuthorityDenied = 1 THEN 'Authority Denied' 
					 WHEN AuthorityGranted = 1 THEN 'Finalized' 
					 WHEN sar.SettlementAuthorityStatusID = 1 THEN 'Approved'
					 WHEN sar.SettlementAuthorityStatusID = 2 THEN 'Rejected' 
						  WHEN sar.SettlementAuthorityStatusID = 5 THEN 'Deemed Approved'
				ELSE  
						   
						   COALESCE ('Awaiting approval from: <br/>' + CONVERT(nvarchar(MAX),
                    (SELECT DISTINCT DefenseCounsel
                    FROM            tblSettlementAuthorityApproverFirm saaf 
					INNER JOIN tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID 
					INNER JOIN tblDefenseCounsel dc ON saf.FirmID = dc.DefenseCounselID
                    WHERE saf.FirmID <> sar.firmID 													
						AND saf.FirmID = COALESCE(SAR.FirmID, SAF.FirmID)
						AND saaf.SettlementAuthorityID = sa.SettlementAuthorityID 
						AND COALESCE (saaf.ApprovalRequired, 0) = 1 FOR XML PATH('div'))), 'Awaiting Approval') END  AS Status
FROM            tblSettlementAuthority sa INNER JOIN
                         tblSettlementAuthorityApproverFirm saaf ON sa.SettlementAuthorityID = saaf.SettlementAuthorityID INNER JOIN
                         tblSettlementApproverFirm saf ON saaf.SettlementApproverFirmID = saf.SettlementApproverFirmID LEFT JOIN
                         (SELECT MAX(SettlementAuthorityResponseID) AS SettlementAuthorityResponseID, FirmID, SettlementAuthorityID, MAX(SettlementAuthorityStatusID) AS SettlementAuthorityStatusID  FROM  tblSettlementAuthorityResponse GROUP BY FirmID, SettlementAuthorityID) sar ON sa.SettlementAuthorityID = sar.SettlementAuthorityID AND saf.FirmID = sar.FirmID 
WHERE  COALESCE (AuthorityGranted, 0) <> 1
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[28] 4[24] 2[24] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Filtered', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSETTLEMENT_AUTHORITY_FirmsRequiringApproval_Filtered', NULL, NULL
GO
