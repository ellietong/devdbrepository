SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByDisease] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null, @emailaddress nvarchar(200) = null)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     -- Insert statements for procedure here
    
----TEST VARS
--DECLARE @StartDate datetime = '1/1/2014'
--DECLARE @EndDate datetime = GETDATE()

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter



select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end as HighLevelDisease, 
		COUNT(distinct(cl.ClaimLawsuitID)) AS ClaimLawsuitCount
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
--inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join (select * from tblClaimDisease where PrimaryDisease =1) cd ON c.ClaimID=cd.ClaimID
left join tblDisease d ON d.DiseaseID=cd.DiseaseID
left join tblHighLevelDisease hld ON hld.HighLevelDiseaseID=d.HighLevelDiseaseID
where	coalesce(FileDate,'1/1/1900')between coalesce(@StartDate,'1/1/1900')
		and coalesce(@EndDate, '1/1/2100')
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end
order by COUNT(distinct(cl.ClaimLawsuitID)) desc


END





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByDisease] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByDisease] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByDisease] TO [power_user]
GO
