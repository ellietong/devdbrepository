SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptDefenseClaimList_ByCarrier_Marsh]
    (
      @ExhaustionDateID INT
    )
AS /*
Author: L. Carthy
Date: 02/27/2015
Purpose: To update stored procedure for billing reports
Example:
exec [dbo].[qryrptDefenseClaimList_ByCarrier_Marsh] 
@ExhaustionDateID = 310

*/
    SET NOCOUNT ON
    BEGIN
        BEGIN TRY

	--Declare @ExhaustionDateID int
	--Set @ExhaustionDateID = 310
	
	--select * from tblExhaustionDate
	
	
            CREATE TABLE #CarrierAllocation
                (
                  OccurrenceID INT ,
                  AllocatedAmount FLOAT ,
                  CarrierID INT ,
                  CarrierGroupID INT ,
                  CarrierGroupName NVARCHAR(100) ,
                  PolicyDateID INT
                )
            INSERT  INTO #CarrierAllocation
                    SELECT  OC.OccurrenceID ,
                            SUM(E.DefenseAllocation)
                            + SUM(E.DefenseOutsideLimitsAllocation) AS AllocatedAmount ,
                            C.CarrierID ,
                            cg.CarrierGroupID ,
                            CarrierGroupName ,
                            pd.PolicyDateID
                    FROM    tblExhaustion E
                            JOIN tblOccurrence OC ON OC.OccurrenceID = E.OccurrenceID
                            INNER JOIN tblPolicyDates pd ON pd.PolicyDateID = E.PolicyDateID
                            JOIN tblPolicy P ON P.PolicyID = pd.PolicyID
                            JOIN tblCarrier C ON C.CarrierID = P.CarrierID
                            LEFT JOIN tblCarrierGroup cg ON cg.CarrierGroupID = C.CarrierGroupID
                    WHERE   E.ExhaustionDateID = @ExhaustionDateID
                    GROUP BY OC.OccurrenceID ,
                            C.CarrierID ,
                            cg.CarrierGroupID ,
                            CarrierGroupName ,
                            pd.PolicyDateID
                    ORDER BY OC.OccurrenceID ,
                            C.CarrierID ,
                            cg.CarrierGroupID ,
                            CarrierGroupName ,
                            pd.PolicyDateID
	
	--Select * from @CarrierAllocation
	
            CREATE TABLE #Firm
                (
                  VendorFirmID BIGINT ,
                  FirmName VARCHAR(100)
                )
            INSERT  INTO #Firm
                    SELECT  VendorFirmID ,
                            VendorFirm
                    FROM    tblVendorFirm
                    UNION
                    SELECT  DefenseCounselID ,
                            DefenseCounsel
                    FROM    tblDefenseCounsel
	
	
            SELECT  di.DefenseInvoiceID ,
                    vf.FirmName AS Firm ,
                    dic.ClaimLawsuitID AS ClaimantLawsuitID ,
                    LastName + ', ' + FirstName AS ClaimantName ,
                    'XXX-XX-' + RIGHT(COALESCE(SocialSecurityNumber, ''), 4) AS socialsecuritynumber ,
                    CASE WHEN s.State = 'ZZ' THEN ''
                         ELSE s.State
                    END AS State ,
                    Jurisdiction ,
                    LTRIM(RTRIM(dic.InvoiceNumber)) AS invoicenumber ,
                    InvoiceDate ,
                    SUM(CASE WHEN CarrierGroupID = 13 THEN AllocatedAmount
                             ELSE 0
                        END) AS Hartford ,
                    SUM(CASE WHEN CarrierGroupID = 30 THEN AllocatedAmount
                             ELSE 0
                        END) AS AllState ,
                    SUM(CASE WHEN CarrierGroupID = 31 THEN AllocatedAmount
                             ELSE 0
                        END) AS ACE ,
                    SUM(CASE WHEN CarrierID = 10
                                  AND ca.PolicyDateID < 3107
                             THEN AllocatedAmount
                             ELSE 0
                        END) AS 'Marsh Pool' ,
                    COALESCE(AdjustedTotal,
                             COALESCE(Costs, 0) + COALESCE(Fees, 0)
                             - COALESCE(Adjustments, 0)) AS AdjustedTotal ,
                    o.TriggerStart1 AS DOFE ,
                    CASE WHEN cdt.ClaimantDocumentType = 'INVOICE'
                         THEN 'Invoice'
                         WHEN cdt.ClaimantDocumentType = 'VENDOR INVOICE'
                         THEN 'Expert Invoice'
                    END AS ClaimantDocumentType ,
                    Fees ,
                    Costs ,
                    COALESCE(Adjustments, 0) AS Adjustments ,
                    COALESCE(AdjustedFee, 0) AS AdjustedFee ,
                    COALESCE(AdjustedDisbursement, 0) AS AdjustedCost ,
                    ( dic.Fees + dic.Costs ) AS InvoiceTotal ,
                    Matter ,
                    AdjustmentNotes ,
                    OccurrenceGroup ,
                    vf.VendorFirmID AS FirmID
            FROM    tblDefenseInvoice di
                    INNER JOIN tblDefInvClaimantDetail dic ON dic.DefenseInvoiceID = di.DefenseInvoiceID
                    LEFT JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = dic.ClaimLawsuitID
                    LEFT JOIN tblClaim c ON c.ClaimID = cl.ClaimID
                    LEFT JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
                    LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
                    LEFT JOIN tblJurisdiction j ON j.JurisdictionID = l.JurisdictionID
                    LEFT JOIN tblState s ON s.StateID = l.StateID
                    LEFT JOIN #Firm vf ON vf.VendorFirmID = di.VendorFirmID
                    LEFT JOIN tblOccurrence o ON dic.InvoiceDetailID = o.OccurrenceNote
                    INNER JOIN tblOccurrenceGroup og ON og.OccurrenceGroupID = o.OccurrenceGroupID
                    INNER JOIN #CarrierAllocation ca ON ca.OccurrenceID = o.OccurrenceID
                    LEFT JOIN tblDocuments d ON d.DocumentID = di.DocumentID
                    LEFT JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
                    LEFT JOIN tblMatter m ON m.MatterID = c.MatterID
            GROUP BY di.DefenseInvoiceID ,
                    vf.FirmName ,
                    dic.ClaimLawsuitID ,
                    LastName + ', ' + FirstName ,
                    'XXX-XX-' + RIGHT(COALESCE(SocialSecurityNumber, ''), 4) ,
                    s.State ,
                    Jurisdiction ,
                    LTRIM(RTRIM(dic.InvoiceNumber)) ,
                    InvoiceDate ,
                    COALESCE(AdjustedTotal,
                             COALESCE(Costs, 0) + COALESCE(Fees, 0)
                             - COALESCE(Adjustments, 0)) ,
                    o.TriggerStart1 ,
                    CASE WHEN cdt.ClaimantDocumentType = 'INVOICE'
                         THEN 'Invoice'
                         WHEN cdt.ClaimantDocumentType = 'VENDOR INVOICE'
                         THEN 'Expert Invoice'
                    END ,
                    Fees ,
                    Costs ,
                    COALESCE(Adjustments, 0) ,
                    COALESCE(AdjustedFee, 0) ,
                    COALESCE(AdjustedDisbursement, 0) ,
                    ( dic.Fees + dic.Costs ) ,
                    Matter ,
                    AdjustmentNotes ,
                    OccurrenceGroup ,
                    vf.VendorFirmID
            ORDER BY di.DefenseInvoiceID ,
                    vf.FirmName ,
                    dic.ClaimLawsuitID ,
                    LastName + ', ' + FirstName ,
                    'XXX-XX-' + RIGHT(COALESCE(socialsecuritynumber, ''), 4) ,
                    s.State ,
                    Jurisdiction ,
                    LTRIM(RTRIM(dic.InvoiceNumber)) ,
                    InvoiceDate ,
                    COALESCE(AdjustedTotal,
                             COALESCE(Costs, 0) + COALESCE(Fees, 0)
                             - COALESCE(Adjustments, 0)) ,
                    o.TriggerStart1 ,
                    CASE WHEN cdt.ClaimantDocumentType = 'INVOICE'
                         THEN 'Invoice'
                         WHEN cdt.ClaimantDocumentType = 'VENDOR INVOICE'
                         THEN 'Expert Invoice'
                    END ,
                    Fees ,
                    Costs ,
                    COALESCE(Adjustments, 0) ,
                    COALESCE(AdjustedFee, 0) ,
                    COALESCE(AdjustedDisbursement, 0) ,
                    ( dic.Fees + dic.Costs ) ,
                    Matter ,
                    AdjustmentNotes ,
                    OccurrenceGroup ,
                    vf.VendorFirmID

        END TRY
        BEGIN CATCH
            SELECT  ERROR_NUMBER() ,
                    ERROR_LINE() ,
                    ERROR_MESSAGE() ,
                    ERROR_SEVERITY() ,
                    ERROR_STATE()	
        END CATCH
    END
GO
