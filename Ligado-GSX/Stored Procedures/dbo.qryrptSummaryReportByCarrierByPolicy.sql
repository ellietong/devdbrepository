SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryrptSummaryReportByCarrierByPolicy]
(--@AdjustmentExhaustionDateID int,
@ExhaustionDateID int, 
@LimitTypeID int, 
@ExhaustionTypeID int)
As
Set Nocount on

 --Declare @LimitTypeID int
 --Declare @ExhaustionTypeID int 
 --Declare @ExhaustionDateID int

 
 --Set @LimitTypeID = 1
 --Set @ExhaustionTypeID = 3
 --Set @ExhaustionDateID = 9


-----------------------------------------------------------------------------------------------------------------------------------------------



--Previous Exhaustion
Declare @Exhaustion Table (
			ExhaustionTypeID int, 
			ExhaustionID bigint, 
			ExhaustionDateID int, 
			OccurrenceID bigint, 
			PolicyDateID bigint,
			IndemnityAllocation money, 
			DefenseAllocation money, 
			DefenseOutsideLimitsAllocation money, 
			SIRIndemnityAllocation money,
			SIRDefenseAllocation money, 
			LedgerID bigint,
			OutsideConsumption money 
	)
Insert Into @Exhaustion
Select 
		ExhaustionTypeID, 
		ExhaustionID, 
		E.ExhaustionDateID, 
		OccurrenceID, 
		PolicyDateID, 
		IndemnityAllocation,
		DefenseAllocation, 
		DefenseOUtsideLimitsAllocation, 
		SIRIndemnityAllocation, 
		SIRDefenseAllocation,
		LedgerID, 
		OutsideConsumption 
From tblExhaustion E
		Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where 
		ExhaustionTypeID=@ExhaustionTypeID 
		and ED.ExhaustionDateID < @ExhaustionDateID -- If we want to make this simply less than the date, use this: ED.ExhaustionDate < (Select ExhaustionDate from tblExhaustionDate where ExhaustionDateID = @ExhaustionDateID)
		--and ed.ExhaustionDateID not in (@AdjustmentExhaustionDateID)
		and ConsumptionTypeID in (1)

Declare @PreviousExhaustionTotals table (
			ExhaustionTypeID int, 
			PolicyDateID int, 
			PreviousAllocationAmt money, 
			OutsideConsumption money)

Insert Into @PreviousExhaustionTotals
Select 
			ExhaustionTypeID, 
			PolicyDateID,
			coalesce(Sum(DefenseAllocation + IndemnityAllocation+SIRIndemnityAllocation+SIRDefenseAllocation),0) as PreviousAllocationAmt,
			OutsideConsumption 
From 
	@Exhaustion
Group By 
			ExhaustionTypeID, PolicyDateID, OutsideConsumption






--Current Allocation
Declare @CurrentAllocation Table (
			ExhaustionTypeID int, 
			ExhaustionID bigint, 
			ExhaustionDateID int, 
			OccurrenceID bigint, 
			PolicyDateID bigint,
			IndemnityAllocation money, 
			DefenseAllocation money, 
			DefenseOutsideLimitsAllocation money, 
			SIRIndemnityAllocation money,
			SIRDefenseAllocation money,
			 LedgerID bigint, 
			 OutsideConsumption money)
	
Insert Into @CurrentAllocation
Select 
			ExhaustionTypeID, 
			ExhaustionID, 
			E.ExhaustionDateID, 
			OccurrenceID, 
			PolicyDateID, 
			IndemnityAllocation,
			DefenseAllocation, 
			DefenseOUtsideLimitsAllocation, 
			SIRIndemnityAllocation, 
			SIRDefenseAllocation,
			LedgerID, 
			OutsideConsumption 
From tblExhaustion E
	Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where 
	ExhaustionTypeID=@ExhaustionTypeID 
	and ED.ExhaustionDateID=@ExhaustionDateID 
	and ConsumptionTypeID in (1)  --1 is Billed, 2 is Credit, 3 is Outside

Declare @CurrentAllocationTotals table (
				ExhaustionTypeID int, 
				PolicyDateID int, 
				Indemnity money, 
				Defense money,
				DefenseOutsideLimits money, 
				TotalDefense money, 
				SIRIndemnityAllocation money, 
				SIRDefenseAllocation money)
Insert Into @CurrentAllocationTotals
Select 
				ExhaustionTypeID, 
				PolicyDateID,
				coalesce(Sum(IndemnityAllocation), 0) as Indemnity,
				coalesce(Sum(DefenseAllocation), 0) as Defense,
				coalesce(Sum(DefenseOutsideLimitsAllocation),0) as DefenseOutsideLimits,
				coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
				coalesce(Sum(SIRIndemnityAllocation), 0) as SIRIndemnity,
				coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense
From @CurrentAllocation
Group By ExhaustionTypeID, PolicyDateID




--Current Adjustment
Declare @CurrentAdjustment Table (
				ExhaustionTypeID int, 
				ExhaustionID bigint, 
				ExhaustionDateID int, 
				OccurrenceID bigint, 
				PolicyDateID bigint,
				TotalAdjustment money)
Insert Into @CurrentAdjustment
Select 
				ExhaustionTypeID, 
				ExhaustionID, 
				E.ExhaustionDateID, 
				OccurrenceID, 
				PolicyDateID, 
				Coalesce(SUM(Indemnityallocation+DefenseAllocation),0) as TotalAdjustment
From tblExhaustion E
		Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where 
	ExhaustionTypeID=@ExhaustionTypeID 
	and ED.ExhaustionDateID = @ExhaustionDateID --Changed to Bill Exhaustion Date ID from adjustment (Now use ConsumptionTypeID)
	and ConsumptionTypeID in (2,3) --1 is Billed, 2 is Credit, 3 is Outside
Group by 
	ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID

Declare @CurrentAdjustmentTotals table (
				ExhaustionTypeID int, 
				PolicyDateID int, 
				TotalAdjustment money) 
Insert Into @CurrentAdjustmentTotals
Select 
				ExhaustionTypeID, 
				PolicyDateID,
				coalesce(Sum(TotalAdjustment), 0) as TotalAdjustment
From @CurrentAdjustment
Group By 
		ExhaustionTypeID, PolicyDateID




-- Header Summary
Declare @HeaderSummary Table (
				ExhaustionType nvarchar(100), 
				Description nvarchar(100), 
				DefenseForm nvarchar(30), 
				footerdate nvarchar(50))
Insert into @HeaderSummary
Select 
				ExhaustionType, 
				[Description], 
				'Within', 
				Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
		Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where 
		eD.ExhaustionTypeID=@ExhaustionTypeID 
		and ExhaustionDateID=@ExhaustionDateID

union
Select 
				ExhaustionType, 
				[Description], 
				'Excess', 
				Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
		Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where 
		ed.ExhaustionTypeID=@ExhaustionTypeID 
		and ExhaustionDateID=@ExhaustionDateID
union
Select 
				ExhaustionType, 
				[Description], 
				'', 
				Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
		Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where 
		ed.ExhaustionTypeID=@ExhaustionTypeID 
		and ExhaustionDateID=@ExhaustionDateID






--Query Results
Declare @Results Table(OrderID int, OrderName nvarchar(100),policyid bigint, policydateid bigint, carriername nvarchar(100), layer bigint, policynum nvarchar(100), pstartdate nvarchar(100), penddate nvarchar(100), attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), Indemnity money, Defense money, DefenseOutsideLimits money, TotalDefense money, PreviousAllocationAmt money,
	Balance money, OutsideConsumption money)
	
Insert Into @Results






--Carriers
select  
	'1' as OrderID, 
	'All Insurers', 
	p.policyid, 
	pd.policydateid, 
	c.carriername, 
	coalesce(pl.layer,0)+1 as Layer, 
	p.policynum,  
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
	else convert(nvarchar(100), PStartDate , 101) end as pstartdate, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end  as penddate,
	pl.attachmentpoint, 
	pl.perocclimit, 
	pl.aggregatelimit,
	CASE	When provisionvalueID=2 then 'Within'
			when provisionvalueID=3 then 'Excess'
		end as DefenseForm,
	coalesce(Indemnity ,0) + coalesce(SIRIndemnityAllocation ,0) as Indemnity, 
	coalesce(Defense,0) + coalesce(SIRDefenseAllocation ,0) as Defense, 
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense+SIRDefenseAllocation,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
 	(coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)  - coalesce(PreviousAllocationAmt,0)) as Balance,
 	coalesce(OutsideConsumption,0) as OutsideConsumption
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where 
	LimitTypeID = @LimitTypeID 
	and CAT.ExhaustionTypeID = @ExhaustionTypeID 
	and provisionlabelID=1 
	and c.CarrierID not in (10)

Union

--GSX Producer
select  
	'2' as OrderID, 
	'Producer', 
	p.policyid, 
	pd.policydateid, 
	'GSX'  as carriername,
	'100' as layer, 
	p.policynum as PolicyNum, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
	else convert(nvarchar(100), PStartDate , 101) end as pstartdate, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end  as penddate,
	'' as attachmentpoint, 
	'-1000000000'  as perocclimit, 
	'-1000000000'  as aggregatelimit, 
	'' as defenseform,
	sum(Indemnity+SIRIndemnityAllocation) as Indemnity, 
	sum(defense+SIRDefenseAllocation) as Defense,  
	'' as defenseoutsidelimits, 
	sum(Defense+SIRDefenseAllocation) as TotalDefense,
	Coalesce(SUM(pet.PreviousAllocationAmt),0) as previousallocationAmt, 
	'-1000000000' as Balance , 
	coalesce(OutsideConsumption,0) as OutsideConsumption
from tblPolicyDates pd
	inner join tblPolicy p on p.PolicyID=pd.PolicyID
	inner join tblcarrier c on c.carrierid=p.carrierid
	--inner join tblexhaustion e on e.policydateid=pd.policydateid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
where c.CarrierID in (10)
group by 
	p.policyid, pd.policydateid, c.carriername,  p.policynum, 		Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
	else convert(nvarchar(100), PStartDate , 101) end, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end,	coalesce(OutsideConsumption,0)







--Adjustment Results
Declare @Adjustment Table(OrderID int, OrderName nvarchar(100), policyid bigint, policydateid bigint, carriername nvarchar(100), layer int, policynum nvarchar(100), pstartdate datetime, penddate datetime, attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), TotalAdjustment money)
Insert Into @Adjustment



--Carriers

select  
	'1' as OrderID, 
	'All Insurers', 
	p.policyid, 
	pd.policydateid, 
	c.carriername, 
	coalesce(pl.layer,0)+1 as Layer, 
	p.policynum, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
	else convert(nvarchar(100), PStartDate , 101) end as pstartdate, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end  as penddate,
	attachmentpoint, 
	perocclimit, 
	aggregatelimit, 
	CASE	When provisionvalueID=2 then 'Within'
			when provisionvalueID=3 then 'Excess'
		end as DefenseForm, 
	coalesce(TotalAdjustment,0) as TotalAdjustment 
from 
	tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAdjustmentTotals CAAT on PD.PolicyDateID=CAAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where 
	LimitTypeID = @LimitTypeID 
	and CAAT.ExhaustionTypeID = @ExhaustionTypeID 
	and c.CarrierID not in (10)

UNION


--GSX Producer
select  
	'2' as OrderID, 
	'Producer', 
	p.policyid, 
	pd.policydateid, 
	''  as carriername,
	'' as layer, 
	p.PolicyNum as policynum, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
	else convert(nvarchar(100), PStartDate , 101) end as pstartdate, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end  as penddate,
	'' as attachmentpoint, 
	'-1000000000'  as perocclimit, 
	'-1000000000' as aggregatelimit, 
	'' as defenseform,
	coalesce(TotalAdjustment,0) as TotalAdjustment 
from 
	tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAdjustmentTotals CAAT on PD.PolicyDateID=CAAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
Where  
	CAAT.ExhaustionTypeID = @ExhaustionTypeID 
	and c.CarrierID in (10)


Select 
	R.*, ExhaustionType, 
	[Description],coalesce(sum(a.TotalAdjustment),0) as TotalAdjustment, 
	hs.footerdate
From 
	@Results R
	Inner Join @HeaderSummary HS on R.DefenseForm=HS.DefenseForm
	left join @Adjustment a on r.policydateid=a.policydateid
group by  r.policyid , r.OrderID, r.OrderName, r.policydateid , r.carriername , r.layer , r.policynum , r.pstartdate , r.penddate , r.attachmentpoint , r.perocclimit , r.aggregatelimit ,
	r.DefenseForm  , indemnity , Defense , DefenseOutsideLimits , TotalDefense , PreviousAllocationAmt ,
	Balance , OutsideConsumption , ExhaustionType, [Description], hs.footerdate
order by carriername, cast(r.pstartdate as date), Cast(r.penddate as DATE), policynum



Return







GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GO
