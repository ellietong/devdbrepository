SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmAllocationOccurrenceTypeInclude]
(@allocationID int)
As

SET NOCOUNT ON

SELECT AllocationID, OccurrenceTypeID
FROM tblAllocationOccurrenceTypeInclude
WHERE AllocationID = @allocationID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationOccurrenceTypeInclude] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationOccurrenceTypeInclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationOccurrenceTypeInclude] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationOccurrenceTypeInclude] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationOccurrenceTypeInclude] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationOccurrenceTypeInclude] TO [power_user]
GO
