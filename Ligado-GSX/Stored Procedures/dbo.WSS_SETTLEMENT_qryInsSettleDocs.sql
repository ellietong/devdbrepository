SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] (@ClaimLawsuitID INT, 
													@DocumentID int, 
													@DocCategoryID int, 
													@PageNums nvarchar(100), 
													@ClaimantPersonalInfoID bigint = NULL, 
													@Emailaddress nvarchar(250))
AS


INSERT INTO tblClaimSettlementDocuments (ClaimLawsuitID, DocumentID, DocCategoryID, PageNums, Spusername)
SELECT @ClaimLawsuitID, @DocumentID, @DocCategoryID, @PageNums, @Emailaddress
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [power_user]
GO
