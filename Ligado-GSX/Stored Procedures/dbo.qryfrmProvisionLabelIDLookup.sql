SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[qryfrmProvisionLabelIDLookup]
AS

SET NOCOUNT ON

SELECT ProvisionLabelID, Provision
FROM tblProvisionLabel
ORDER BY ProvisionLabelID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionLabelIDLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionLabelIDLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionLabelIDLookup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionLabelIDLookup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionLabelIDLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionLabelIDLookup] TO [power_user]
GO
