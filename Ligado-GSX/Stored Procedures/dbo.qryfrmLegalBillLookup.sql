SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmLegalBillLookup]
As
Set NoCount On

SELECT InvoiceID, Coalesce(LBInvoiceNumber,'') + ' - ' + Coalesce(convert(nvarchar(10),Period,1),'') +' - $'+ coalesce(convert(nvarchar(250),(Case when AppealFlag = 1 Then AppealTotal Else AdjustedTotal End)),'') as DefenseInvoice
FROM m_tblLegalBillInvoices ORDER BY LBInvoiceNumber, Period

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLegalBillLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLegalBillLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLegalBillLookup] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmLegalBillLookup] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLegalBillLookup] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmLegalBillLookup] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLegalBillLookup] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmLegalBillLookup] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLegalBillLookup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmLegalBillLookup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLegalBillLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLegalBillLookup] TO [power_user]
GO
