SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure] 
             @tableName NVARCHAR(450) 
         AS

         BEGIN 
             UPDATE dbo.AspNet_SqlCacheTablesForChangeNotification WITH (ROWLOCK) SET changeId = changeId + 1 
             WHERE tableName = @tableName
         END
   
GO
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure] TO [base_user]
GRANT EXECUTE ON  [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure] TO [power_user]
GRANT EXECUTE ON  [dbo].[AspNet_SqlCacheUpdateChangeIdStoredProcedure] TO [power_user]
GO
