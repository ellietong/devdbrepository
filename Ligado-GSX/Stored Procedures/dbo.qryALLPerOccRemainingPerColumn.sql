SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE Procedure [dbo].[qryALLPerOccRemainingPerColumn]
(@allocationID int)
AS
SET NOCOUNT ON

-- create a table variable to hold the results of the query to find the per occ remaining per column
DECLARE @tbltemp table (ColumnID int, PercentOcc float)
-- insert per occ remaining per column into the temporary table
INSERT INTO @tbltemp (ColumnID, PercentOcc)
-- find the per occ remaining per column
SELECT arc.ColumnID, 
	SUM(CASE WHEN PStartDate = ColumnStartDate
	   THEN CASE WHEN PEndDate > ColumnEndDate 
		  THEN (CONVERT(decimal, ColumnEndDate - PStartDate)) 
		  ELSE (CONVERT(decimal, PEndDate - PStartDate))
	   END
	   ELSE CASE WHEN PEndDate < ColumnEndDate 
		  THEN (CONVERT(decimal, PEndDate - ColumnStartDate))
		  ELSE (CONVERT(decimal, ColumnEndDate - ColumnStartDate))
	   END
	END / (CONVERT(decimal, PEndDate - PStartDate)) * OccurrenceRemaining) AS PercentOcc
FROM tblAllocationResultsColumn arc, ztblPolicyAllocation zpa
WHERE (zpa.PStartDate < ColumnEndDate) AND (zpa.PEndDate > ColumnStartDate) AND 
	(zpa.InsuranceProgramID = arc.InsuranceProgramID) AND arc.AllocationID = @allocationID
	AND zpa.AllocationID = @allocationID
GROUP BY arc.ColumnID

-- update the ColumnPerOcc field in ztblColumn with the updated per occ remaining per column
UPDATE tblAllocationResultsColumn
SET ColumnPerOcc = (SELECT PercentOcc FROM @tbltemp WHERE ColumnID = tblAllocationResultsColumn.ColumnID)
WHERE AllocationID = @allocationID

DELETE FROM @tbltemp

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLPerOccRemainingPerColumn] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLPerOccRemainingPerColumn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLPerOccRemainingPerColumn] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryALLPerOccRemainingPerColumn] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLPerOccRemainingPerColumn] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLPerOccRemainingPerColumn] TO [power_user]
GO
