SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROCEDURE [dbo].[qryALLSigDropDown]
(@allocationID nvarchar(5),@strInsuranceProfile nvarchar(250),
@TriggerStart nvarchar(30), @TriggerEnd nvarchar(30))
AS
/*  DECLARE @AllocationID nvarchar(5)
DECLARE @strInsuranceProfile nvarchar(250)
DECLARE @triggerstart nvarchar(30)
DECLARE @triggerend nvarchar(30)

 SET @allocationId = 8
 SET @strInsuranceProfile = 'InsuranceProgramID = 7'
 SET @triggerstart = '1/1/1970'
 SET @triggerend = '1/1/1973'
 */

	
DECLARE @strsql nvarchar(4000)

SET @strsql = ' SET NOCOUNT ON

select pc.columnid, pc.policyid, l.layer, (l.layer - 1) as layerbelow, PC.provisionvalueid,
a.Provisionvalueid as ProvisionValueBelow
from ztblpolicycolumn PC
inner join tblpolicylimits L on L.policyid=PC.policyid
inner join tblprovisionvalue PV on PV.provisionvalueid=PC.provisionvalueid
inner join (
	SELECT pc.columnid, pc.PolicyID, Layer, pc.ProvisionValueID, occurrenceremaining
	FROM ztblpolicycolumn PC
	inner join tblpolicylimits L on L.policyid=PC.policyid
	inner join tblprovisionvalue PV on PV.provisionvalueid=PC.provisionvalueid	
	inner join ztblpolicyallocation za on pc.policyid = za.policyid ) a
On PC.columnid = a.ColumnID
where pv.provisionlabelid=2
AND (a.Layer = l.layer - 1 OR l.layer =1)
AND l.Layer >1 
AND a.ProvisionValueID = 7
AND a.OccurrenceRemaining = 0
AND pc.provisionvalueid = 7
and allocationid= ' + @allocationid + '
AND ' + @strInsuranceProfile + '
AND ColumnStartDate <  ''' + @TriggerEnd + ' ''
AND ColumnEndDate > ''' + @TriggerStart + ''' 
GROUP BY pc.columnid, pc.policyid, l.layer, (l.layer - 1) , PC.provisionvalueid,
a.Provisionvalueid
order by pc.columnid, l.layer '

--PRINT @strsql
EXECUTE sp_executesql @strSQL

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLSigDropDown] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLSigDropDown] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLSigDropDown] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryALLSigDropDown] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLSigDropDown] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLSigDropDown] TO [power_user]
GO
