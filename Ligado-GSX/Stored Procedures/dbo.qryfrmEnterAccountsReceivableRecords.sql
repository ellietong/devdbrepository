SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[qryfrmEnterAccountsReceivableRecords] (@BillList NVARCHAR(250) = NULL, @PartialPayment MONEY = NULL)
AS

----Variables for testing----
--DECLARE @BillList NVARCHAR(250) = '160,161'
--Declare @PartialPayment money = null

DECLARE @NewPaymentID INT 
SELECT @NewPaymentID = MAX(PaymentID) + 1 FROM dbo.tblAccountsReceivablePayment

DECLARE @SQL NVARCHAR(MAX)

IF @PartialPayment IS NULL 
BEGIN
	SET @SQL = 'INSERT INTO tblAccountsReceivablePayment (ExhaustionID, PaymentID, Amount, IsHistoric)
					SELECT ExhaustionID, ' +
						cast(@NewPaymentID as varchar(100)) + ' AS PaymentID, 
						COALESCE(IndemnityAllocation,0) + COALESCE(SIRIndemnityAllocation,0) + COALESCE(DefenseAllocation,0) + COALESCE(DefenseOutsideLimitsAllocation,0) AS Amount,
						0 AS IsHistoric
					FROM tblExhaustion e
						INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID
						INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
						INNER JOIN tblPolicy p ON p.PolicyID = pd.PolicyID
						INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
					WHERE ed.ExhaustionDateID IN(' + @BillList + ')
						AND ExhaustionID NOT IN(SELECT ExhaustionID
												FROM tblAccountsReceivablePayment)
					Group By c.CarrierID,ExhaustionID, COALESCE(IndemnityAllocation,0) + COALESCE(SIRIndemnityAllocation,0) + COALESCE(DefenseAllocation,0) + COALESCE(DefenseOutsideLimitsAllocation,0)'
END
ELSE
BEGIN 
	SET @SQL = 'INSERT INTO tblAccountsReceivablePayment (ExhaustionID, PaymentID, Amount, IsHistoric)
					SELECT ExhaustionID, ' +
						cast(@NewPaymentID as varchar(100)) + ' AS PaymentID, 
						COALESCE(IndemnityAllocation,0) + COALESCE(SIRIndemnityAllocation,0) + COALESCE(DefenseAllocation,0) + COALESCE(DefenseOutsideLimitsAllocation,0) AS Amount,
						0 AS IsHistoric
					FROM tblExhaustion e
						INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID
						INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
						INNER JOIN tblPolicy p ON p.PolicyID = pd.PolicyID
						INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
					WHERE ed.ExhaustionDateID IN(' + @BillList + ')
						AND ExhaustionID NOT IN(SELECT ExhaustionID
												FROM tblAccountsReceivablePayment)
					Group By c.CarrierID,ExhaustionID, COALESCE(IndemnityAllocation,0) + COALESCE(SIRIndemnityAllocation,0) + COALESCE(DefenseAllocation,0) + COALESCE(DefenseOutsideLimitsAllocation,0)'
END

EXECUTE (@SQL)


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEnterAccountsReceivableRecords] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmEnterAccountsReceivableRecords] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEnterAccountsReceivableRecords] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmEnterAccountsReceivableRecords] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEnterAccountsReceivableRecords] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmEnterAccountsReceivableRecords] TO [power_user]
GO
