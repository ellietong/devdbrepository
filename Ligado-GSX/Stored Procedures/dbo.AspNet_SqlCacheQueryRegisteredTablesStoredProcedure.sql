SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] 
         AS
         SELECT tableName FROM dbo.AspNet_SqlCacheTablesForChangeNotification   
GO
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] TO [base_user]
GRANT EXECUTE ON  [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] TO [power_user]
GRANT EXECUTE ON  [dbo].[AspNet_SqlCacheQueryRegisteredTablesStoredProcedure] TO [power_user]
GO
