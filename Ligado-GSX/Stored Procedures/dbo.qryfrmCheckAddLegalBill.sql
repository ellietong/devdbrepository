SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmCheckAddLegalBill] (@InvoiceID int, @CheckID int, @PaymentAmount money)
As
Set NoCount On

--Declare @DefenseInvoiceID int
--Declare @CheckID int
--Declare @paymentAmount money
--Set @DefenseInvoiceID = 2
--Set @CheckID = 2
--Set @PaymentAmount = 10000

Declare @CurrentFirmID int
Set @CurrentFirmID = (select DefenseCounselAddressID from m_tblLegalBillInvoices where InvoiceID = @InvoiceID)
Declare @AcceptableFirmID int
Set @AcceptableFirmID = (Select Coalesce(DefenseCounselAddressID,@CurrentFirmID) From tblPaymentLegalBill pd Inner Join m_tblLegalBillInvoices di on di.InvoiceID = pd.InvoiceID Where CheckID = @CheckID Group By Coalesce(DefenseCounselAddressID,@CurrentFirmID))

Declare @SettlementCheck int
Set @SettlementCheck = (Select COUNT(*) from tblPaymentSettlement where CheckID = @CheckID)

Declare @DefenseCheck int
Set @DefenseCheck = (Select COUNT(*) from tblPaymentDefense where CheckID = @CheckID)

Declare @ApprovedCheck int
Set @ApprovedCheck = (Select Case when ca.CheckID is not null Then 1 Else 0 End From tblCheck c Left Join tblCheckApproval ca on c.CheckID = ca.CheckID where c.CheckID = @CheckID)

If (@AcceptableFirmID = @CurrentFirmID AND @ApprovedCheck = 0 AND @SettlementCheck = 0 AND @DefenseCheck = 0) Or (@AcceptableFirmID is Null AND @SettlementCheck = 0 AND @DefenseCheck = 0) Begin
	Insert Into tblPaymentLegalBill(InvoiceID, CheckID, PaymentAmount)
	Select @InvoiceID, @CheckID, @PaymentAmount
End

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddLegalBill] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddLegalBill] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddLegalBill] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddLegalBill] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddLegalBill] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddLegalBill] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddLegalBill] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddLegalBill] TO [power_user]
GO
