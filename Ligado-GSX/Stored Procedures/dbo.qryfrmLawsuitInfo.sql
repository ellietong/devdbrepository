SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryfrmLawsuitInfo] (@LawsuitID int)
as

Select LawsuitID,
	CaseCaption, 
	DocketNumber, 
	FileDate, 
	JurisdictionID, 
	StateID, 
	CourtTypeID, 
	ServiceDate, 
	ReceivedDate, 
	DefenseCounselAddressID,
	PlaintiffCounselAddressID,
	LeadPlaintiffFirstName, 
	LeadPlaintiffLastName,
	IsThirdPartyComplaint,
	NationalPlaintiffCounselID
From tblLawsuit
Where LawsuitID = @LawsuitID
Order By FileDate desc, DocketNumber, CaseCaption

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawsuitInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawsuitInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawsuitInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmLawsuitInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLawsuitInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLawsuitInfo] TO [power_user]
GO
