SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryBILLInsertAdjustment]
(@adjustmentDate datetime, @occurrenceID int, @indemnityAllocation money = 0, @defenseAllocation money = 0, 
@description nvarchar(100), @ledgerID int = NULL, @exhaustionDateID int OUT, @exhaustionTypeID int, @policyDateID int = NULL, @policyID int = NULL)
AS
-- Procedure takes adjustment date, occurrenceID, indemnity and defense allocation amounts, description, ledger ID, bill group ID, policy date ID, and policy ID
-- inputs and populates tblExhaustion and tblExhaustionDate, which memorialize and track the adjustment
-- If a billing module is included, tblLedger (and the bill) is also updated using the stored procedure UpdateLedger
SET NOCOUNT ON
-- Insert data to tblExhaustionDate
INSERT INTO tblExhaustionDate (ExhaustionTypeID, ExhaustionDate, Description)
VALUES (@exhaustionTypeID, @adjustmentDate, @description)
-- Capture exhaustion date ID
SET @exhaustionDateID = @@IDENTITY
DECLARE @tmp table (PolicyDateID int)
-- Insert the affected distinct policy date IDs into a temporary table
IF @policyID IS NOT NULL
	INSERT INTO @tmp
	SELECT DISTINCT e.PolicyDateID
	FROM (tblExhaustion e INNER JOIN tblExhaustionDate ed on e.ExhaustionDateID = ed.ExhaustionDateID) 
		INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
	WHERE PolicyID = @policyID AND ExhaustionDate <= @adjustmentDate
ELSE
	INSERT INTO @tmp
	VALUES(@PolicyDateID)

-- Insert data to tblExhaustion from tblAllocationResults
INSERT INTO tblExhaustion (ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation, DefenseAllocation, DefenseOutsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation, LedgerID)
SELECT @exhaustionDateID, @occurrenceID, PolicyDateID, @indemnityAllocation, @defenseAllocation, 0, 0, 0, @ledgerID
FROM @tmp
ORDER BY PolicyDateID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryBILLInsertAdjustment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryBILLInsertAdjustment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryBILLInsertAdjustment] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryBILLInsertAdjustment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryBILLInsertAdjustment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryBILLInsertAdjustment] TO [power_user]
GO
