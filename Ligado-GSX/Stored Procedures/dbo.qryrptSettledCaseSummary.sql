SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptSettledCaseSummary] (@SettlementYearStart nvarchar(250),
					@SettlementYEarEnd nvarchar(250)) 

as

Set NoCount On

 --Declare @SettlementYearStart nvarchar(250)
 --Declare @SettlementYearEnd nvarchar(250)
 --Set @SettlementYearStart = '2000'
 --Set @SettlementYearEnd = '2008'



Declare @SettledSummary table (ClaimNumber int, 
				SettlementYear nvarchar(250),
				SettlementAmount money,
				SettlementAverage money,
				SettlementDate datetime, 
				Meso_Count int,
				Meso_SettlementAmount money,
				Meso_Avg money,
				LungCancer_Count int,
				LungCancer_SettlementAmount money,
				LungCancer_Avg money,
				OtherCancer_Count int,
				OtherCancer_SettlementAmount money,
				OtherCancer_Avg money,
				NonMalig_Count int,
				NonMalig_SettlementAmount money,
				NonMalig_Avg money,
				Other_Count int,
				Other_SettlementAmount money,
				Other_Avg money,
				NoDisease_Count int,
				NoDisease_SettlementAmount money,
				NoDisease_Avg money)

Insert Into @SettledSummary
Select count(c.ClaimID) as ClaimantNumber,
	year(settlementdate) as SettlementYear,
	sum(SettlementAmount) as SettlementAmount,
	sum(SettlementAmount)/count(c.ClaimID) as SettlementAverage, 
	convert(datetime,(convert(nvarchar,month(SettlementDate))+'/1/'+ convert(nvarchar,year(SettlementDate)))) as SettlementDate,
	
	sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) as Meso_Count,
	sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end) as Meso_SettlementAmount,
	Case When sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) end  as Meso_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) as LungCancer_Count,
	sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end) as LungCancer_SettlementAmount,
	Case When sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) end  as LungCancer_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) as OtherCancer_Count,
	sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end) as OtherCancer_SettlementAmount,
	Case When sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) end  as OtherCancer_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 4 then 1 else 0 end) as NonMalig_Count,
	sum(Case When HLD.HighLevelDiseaseID = 4 then SettlementAmount else 0 end) as NonMalig_SettlementAmount,
	Case When sum(Case When HLD.HighLevelDiseaseID = 4 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 4 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 4 then 1 else 0 end) end  as NonMalig_Avg,
			
	Sum(Case When HLD.HighLevelDiseaseID = 5 then 1 else 0 end) as Other_Count,
	Sum(Case When HLD.HighLevelDiseaseID = 5 then SettlementAmount else 0 end) as Other_SettlementAmount,
	Case When sum(Case When HLD.HighLevelDiseaseID = 5 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 5 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 5 then 1 else 0 end) end  as Other_Avg,
			
	Sum(Case When HLD.HighLevelDiseaseID = 6 then 1 else 0 end) as NoDisease_Count,
	Sum(Case When HLD.HighLevelDiseaseID = 6 then SettlementAmount else 0 end) as NoDisease_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 6 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 6 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 6 then 1 else 0 end) end  as NoDisease_Avg		
		
From tblClaimantPersonalInfo i 
	Left Join tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
	Left Join tblClaimLawsuit cl on cl.claimID = c.claimID
	Left Join tblClaimantSettlement s on s.claimlawsuitID = cl.claimlawsuitID
	Left Join tblDisease D on CD.DiseaseID = D.DiseaseID
	Left Join tblHighLevelDisease HLD on HLD.HighLevelDiseaseID = D.HighLevelDiseaseID
Where IsPrimaryForClaim = 1 AND SettlementAmount > 0
Group By convert(datetime,(convert(nvarchar,month(SettlementDate))+'/1/'+ convert(nvarchar,year(SettlementDate)))),
	year(settlementdate)

Select coalesce(ClaimNumber,0) as ClaimNumber, 
	Year(SettlementDate) as SettlementYear,
	coalesce(SettlementAmount,0) as SettlementAmount,
	coalesce(SettlementAverage,0) as SettlementAverage,
	SettlementDate as SettlementDate, 
	coalesce(Meso_Count,0) as Meso_Count,
	coalesce(Meso_SettlementAmount,0) as Meso_SettlementAmount,
	coalesce(Meso_Avg,0) as Meso_Avg,
	coalesce(LungCancer_Count,0) as LungCancer_Count,
	coalesce(LungCancer_SettlementAmount,0) as LungCancer_SettlementAmount,
	coalesce(LungCancer_Avg,0) as LungCancer_Avg,
	coalesce(OtherCancer_Count,0) as OtherCancer_Count,
	coalesce(OtherCancer_SettlementAmount,0) as OtherCancer_SettlementAmount,
	coalesce(OtherCancer_Avg,0) as OtherCancer_Avg,
	coalesce(NonMalig_Count,0)as NonMalig_Count,
	coalesce(NonMalig_SettlementAmount,0)as NonMalig_SettlementAmount,
	coalesce(NonMalig_Avg,0)as NonMalig_Avg,
	coalesce(Other_Count,0)as Other_Count,
	coalesce(Other_SettlementAmount,0)as Other_SettlementAmount,
	coalesce(Other_Avg,0)as Other_Avg,
	coalesce(NoDisease_Count,0) as NoDisease_Count,
	coalesce(NoDisease_SettlementAmount,0) as NoDisease_SettlementAmount,
	coalesce(NoDisease_Avg,0) as NoDisease_Avg
From @SettledSummary s 
Where (year(SettlementDate) between @SettlementYearStart and @SettlementYearEnd)
Order By SettlementDate

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSettledCaseSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSettledCaseSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSettledCaseSummary] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSettledCaseSummary] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSettledCaseSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSettledCaseSummary] TO [power_user]
GO
