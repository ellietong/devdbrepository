SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryfrmBillsByCarrierGroup] (@CarrierGroupID INT, @CarrierList NVARCHAR(250) = NULL, @BillFilter NVARCHAR(250) = NULL)
AS

----Variables for testing----
--DECLARE @CarrierGroupID INT = 47
--DECLARE @CarrierList NVARCHAR(250) = NULL
--DECLARE @BillFilter NVARCHAR(250) = NULL

DECLARE @SQL NVARCHAR(2000)

SET @SQL = 'SELECT DISTINCT ed.ExhaustionDateID, [Description], ExhaustionDate
			FROM dbo.tblExhaustionDate ed
				INNER JOIN dbo.tblExhaustion e ON e.ExhaustionDateID = ed.ExhaustionDateID
				INNER JOIN dbo.tblPolicyDates pd ON pd.PolicyDateID = e.PolicyDateID
				INNER JOIN dbo.tblPolicy p ON p.PolicyID = pd.PolicyID
				INNER JOIN dbo.tblCarrier c ON c.CarrierID = p.CarrierID
			WHERE CarrierGroupID = ' + CONVERT(NVARCHAR(10),@CarrierGroupID)

IF @CarrierList IS NOT NULL AND @CarrierList <> '0'
	SET @SQL = @SQL + ' AND p.CarrierID IN (' + @CarrierList + ')'

IF @BillFilter IS NOT NULL AND @BillFilter <> ''
	SET @SQL = @SQL + ' AND [Description] LIKE ''%' + @BillFilter + '%'''

SET @SQL = @SQL + ' ORDER BY [ExhaustionDate] DESC, [Description] '
--SELECT @SQL
EXECUTE (@SQL)
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillsByCarrierGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmBillsByCarrierGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillsByCarrierGroup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmBillsByCarrierGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillsByCarrierGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmBillsByCarrierGroup] TO [power_user]
GO
