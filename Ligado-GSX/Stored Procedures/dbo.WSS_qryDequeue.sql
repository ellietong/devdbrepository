SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDequeue]
AS
BEGIN
	--DECLARE @ZipQueueID uniqueidentifier = 'CC4729FE-FDB9-463B-B95F-070FD9C9F543'
	DECLARE @ZipQueueID uniqueidentifier
	
	SET @ZipQueueID = (SELECT TOP 1 ZipQueueID FROM tblZipQueue WHERE (ZipKey IS NULL OR StartTime <= GETDATE()-.1) AND AddedToQueue >= GETDATE()-.1 ORDER BY AddedToQueue ASC)
		
		
	UPDATE tblZipQueue
	SET StartTime = GETDATE()
	WHERE ZipQueueID = @ZipQueueID

	SELECT @ZipQueueID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDequeue] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDequeue] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDequeue] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_qryDequeue] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDequeue] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDequeue] TO [power_user]
GO
