SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AspNet_SqlCachePollingStoredProcedure] AS
         SELECT tableName, changeId FROM dbo.AspNet_SqlCacheTablesForChangeNotification
         RETURN 0
GO
GRANT EXECUTE ON  [dbo].[AspNet_SqlCachePollingStoredProcedure] TO [aspnet_ChangeNotification_ReceiveNotificationsOnlyAccess]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCachePollingStoredProcedure] TO [base_user]
GRANT EXECUTE ON  [dbo].[AspNet_SqlCachePollingStoredProcedure] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCachePollingStoredProcedure] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[AspNet_SqlCachePollingStoredProcedure] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCachePollingStoredProcedure] TO [power_user]
GRANT EXECUTE ON  [dbo].[AspNet_SqlCachePollingStoredProcedure] TO [power_user]
GO
