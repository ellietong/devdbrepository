SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimantDocumentsQC] (@ClaimantPersonalInfoID int)

as

--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 251312

Select d.DocumentID as ClaimantDocumentID,
mcd.ClaimantPersonalInfoID, 
ClaimantDocumentType,
DocumentURL as DocumentLink, 
DefenseCounsel as DefenseCounsel,
DateAccepted, Comments, DocumentSource
from tblDocuments d
left join tblClaimantDocumentType dt on dt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID 
left join tblMultiClaimantDocument mcd on mcd.DocumentID = d.DocumentID
left join tblDocumentSource ds on ds.DocumentSourceID=d.DocumentSourceID
left join tblClaimLawsuit cl ON mcd.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = cl.DefenseCounselAddressID
where mcd.ClaimantPersonalInfoID = @claimantpersonalInfoID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [power_user]
GO
