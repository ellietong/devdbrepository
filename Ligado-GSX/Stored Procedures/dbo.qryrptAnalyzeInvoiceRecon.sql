SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE     PROC  [dbo].[qryrptAnalyzeInvoiceRecon] (@SiteID int)

AS 
SET NOCOUNT ON

 --DECLARE @Siteid INT
 --SET @Siteid = 0

DECLARE @MaxSiteID INT
SELECT @MaxSiteID = max(siteid) from tblEnviroSitesDetail


DECLARE @AMounts TABLE (vendorid int, SiteID INT, A_Bill money, A_Inv money, A_NoInv money)

INSERT INTO @Amounts
Select c.vendorid, c.siteid,
CASE WHEN  c.billed = 1   THEN   Sum(ReimbursableAmount) ELSE 0 END AS AmtBilled,
CASE WHEN   InvoiceNumber is not null  AND Billed = 0  THEN   Sum(ReimbursableAmount) ELSE 0 END AS AmtWInv,
CASE WHEN   InvoiceNumber is  null AND Billed = 0  THEN   Sum(ReimbursableAmount) ELSE 0 END AS AmtWoInv
FROM tblSiteCosts C
GROUP BY c.vendorid, c.billed, c.siteid, recordsourceid, InvoiceNumber


SELECT d.sitename, a.siteid, excludedsite, v.vendorname,  a.vendorid, SUM(a_bill) as Billed, SUM(A_Inv) as WInvoice, SUM(A_NoInv) as NoInvoice
FROM  @Amounts A
LEFT JOIN tblVendor V on v.vendorid = a.vendorid
LEFT JOIN tblEnviroSitesDetail D on a.siteid = d.siteid

WHERE coalesce(excludedsite,0) <>1
AND a.siteid BETWEEN COALESCE(@SiteID, 0) AND COALESCE(NullIF(@SiteID,0), @MaxSiteID)
GROUP BY  a.siteid, d.sitename, v.vendorname,  a.vendorid,  excludedsite
Order by vendorname, sitename

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAnalyzeInvoiceRecon] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAnalyzeInvoiceRecon] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAnalyzeInvoiceRecon] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptAnalyzeInvoiceRecon] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAnalyzeInvoiceRecon] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAnalyzeInvoiceRecon] TO [power_user]
GO
