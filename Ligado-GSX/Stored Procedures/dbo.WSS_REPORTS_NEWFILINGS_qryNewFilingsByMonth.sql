SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByMonth] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null, @emailaddress nvarchar(200) = null)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

----TEST VARS
--DECLARE @StartDate datetime = '1/1/2014'
--DECLARE @EndDate datetime = GETDATE()

select DATENAME("mm", coalesce(FileDate,'1/1/1900'))+' '+convert(nvarchar,year(coalesce(filedate,'1/1/1900'))) AS [Month], COUNT(distinct(cl.ClaimLawsuitID)) AS ClaimLawsuitCount, 
MONTH(coalesce(FileDate,'1/1/1900')) As MonthID
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
--inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s ON l.StateID=s.StateID
where	coalesce(FileDate,'1/1/1900') between coalesce(@StartDate,'1/1/1900')
		and coalesce(@EndDate, '1/1/2100')
group by  DATENAME("mm", coalesce(FileDate,'1/1/1900'))+' '+convert(nvarchar,year(coalesce(filedate,'1/1/1900'))),
MONTH(coalesce(FileDate,'1/1/1900')), year(coalesce(filedate,'1/1/1900'))
order by year(coalesce(filedate,'1/1/1900')),MONTH(coalesce(FileDate,'1/1/1900'))



END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByMonth] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByMonth] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByMonth] TO [power_user]
GO
