SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptClaimsSummary] (@Start nvarchar(250), @End nvarchar(250)) 
AS
SET NOCOUNT ON

--DECLARE @Start nvarchar(250)
--DECLARE @End nvarchar(250)
--SET @Start = '1/1/1900'
--SET @End = '6/30/2010'
 
DECLARE @newFilings TABLE(filedate nvarchar(250), claimnumber int)
INSERT INTO @newFilings
	SELECT YEAR(COALESCE(servicedate,filedate,statusdate)) AS filedate, count(c.ClaimID) AS ClaimNumber
	FROM tblClaimantPersonalInfo i 
		  LEFT JOIN tblClaim c ON i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit l ON l.claimID = c.claimID
		  LEFT JOIN tblLawsuit law ON l.lawsuitid = law.lawsuitid
	WHERE IsPrimaryForClaim = 1 
		  AND COALESCE(servicedate,filedate,statusdate) <= @End AND COALESCE(servicedate,filedate,statusdate)>= @start
	GROUP BY YEAR(COALESCE(servicedate,filedate,statusdate))
	ORDER BY YEAR(COALESCE(servicedate,filedate,statusdate))

DECLARE @ClaimSummary TABLE(Statusdate nvarchar(250), SBNDAmount money, SettlementAmount money, Inactive_Count int, DBnd_Count int, Dismissed_Count int, SBND_Count int, Settled_Count int)
INSERT INTO @ClaimSummary
	SELECT YEAR(COALESCE(settlementDate,statusdate)) AS StatusDate,
		SUM(CASE WHEN claimlawsuitstatusid = 25			THEN COALESCE(SettlementAmount,0) ELSE 0 END) AS SBNDAmount,
		SUM(CASE WHEN claimlawsuitstatusid = 23			THEN COALESCE(SettlementAmount,0) ELSE 0 END) AS SettlementAmount,
		SUM(CASE WHEN claimlawsuitstatusid = 21			THEN 1							  ELSE 0 END) AS Inactive_Count,
		SUM(CASE WHEN claimlawsuitstatusid = 17			THEN 1							  ELSE 0 END) AS DBnd_Count,
		SUM(CASE WHEN claimlawsuitstatusid IN(15,16,27) THEN 1							  ELSE 0 END) AS Dismissed_Count,
		SUM(CASE WHEN claimlawsuitstatusid = 25			THEN 1							  ELSE 0 END) AS SBND_Count,
		SUM(CASE WHEN claimlawsuitstatusid IN(23,28)    THEN 1							  ELSE 0 END) AS Settled_Count
	FROM tblClaimantPersonalInfo i 
		  LEFT JOIN tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit l on l.claimID = c.claimID
		  LEFT JOIN tblLawsuit law on l.lawsuitid = law.lawsuitid
		  LEFT JOIN tblCLaimantSettlement s on s.claimlawsuitID = l.claimlawsuitID
	WHERE IsPrimaryForClaim = 1
		  AND COALESCE(settlementDate,statusdate) >= @Start 
		  AND COALESCE(settlementDate,statusdate) <= @End
	GROUP BY YEAR(COALESCE(settlementDate,statusdate))
	ORDER BY YEAR(COALESCE(settlementDate,statusdate))

SELECT YEAR(COALESCE(filedate,statusdate))AS date1,
	COALESCE(SettlementAmount,0) AS SettlementAmount,
	COALESCE(SbndAmount,0)       AS SbndAmount,
	COALESCE(claimnumber, 0)     AS File_Count,
	COALESCE(Inactive_Count,0)   AS Inactive_Count,
	COALESCE(DBnd_Count,0)       AS DBnd_Count,
	COALESCE(Dismissed_Count,0)  AS Dismissed_Count,
	COALESCE(SBND_Count,0)       AS SBND_Count,
	COALESCE(Settled_Count,0)    AS Settled_Count, 
	@Start AS StartDate, 
	@End   AS EndDate
FROM @newFilings f
	LEFT JOIN @claimSummary s ON s.statusdate = f.filedate
ORDER BY YEAR(COALESCE(filedate,statusdate))

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimsSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimsSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimsSummary] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptClaimsSummary] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimsSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimsSummary] TO [power_user]
GO
