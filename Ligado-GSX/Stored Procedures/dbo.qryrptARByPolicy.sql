SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByPolicy]
(@allocationID int = null, @CarrierID INT = NULL, @Solvency varchar (50)= NULL, @CarrierGroupID int = NULL, @InsuranceProgramID INT = NULL)
As
        
SET NOCOUNT ON
 
--DECLARE @AllocationID int
--SET @AllocationID = 56
--DECLARE @CarrierID int
--SET @CarrierID = 0
--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = 0
--DECLARE @Solvency varchar (50)
--SET @Solvency = null
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID = 0
 
--DECLARE MAX VARIABLES
DECLARE @maxAllocationID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @maxInsuranceProgramID int
DECLARE @LimitTypeID int
 
--SET MAX VARIABLES
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxInsuranceProgramID = MAX(InsuranceProgramID) FROM vPolicy
IF @CarrierGroupID = 0 SET @CarrierGroupID = NULL
SELECT @LimitTypeID = (SELECT LimitTypeID FROM tblAllocation WHERE AllocationID = @allocationID)


DECLARE @Allocation TABLE (AllocationID int, TotalSIR money, TotalAllocation money, PVTotalAllocation money)
INSERT INTO @Allocation
SELECT AllocationID, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
WHERE AllocationID = @AllocationID
GROUP BY AllocationID


DECLARE @CarrierAllocation TABLE (AllocationID int, CarrierID int, CarrierTotalSIR money, CarrierTotalAllocation money, PVCarrierTotalAllocation money)
INSERT INTO @CarrierAllocation
SELECT AllocationID, CarrierID, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVCarrierTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, CarrierID


DECLARE @CarrierGroupAllocation TABLE (AllocationID int, CarrierGroupID int, CarrierGroupTotalSIR money, CarrierGroupTotalAllocation money, PVCarrierGroupTotalAllocation money)
INSERT INTO @CarrierGroupAllocation
SELECT AllocationID, CarrierGroupID, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVCarrierGroupTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, CarrierGroupID


 
DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
CarrierID int, CarrierName nvarchar(250), CarrierSolvency nvarchar(25), PolicyNum nvarchar(100), 
AttachmentPoint money, PreviouslyExhaustedAggregate money, PerOccLimit money, LayerPerOccLimit money, 
SIR money, AggregateLimit money, Layer tinyint, InsuranceProgramID int, InsuranceProgram nvarchar(50),
LimitTypeID int, PStartDate datetime, PEndDate datetime, ActualPStartDate datetime, ActualPEndDate datetime, AnnualPeriod int)
INSERT INTO @Policy
 
SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, CarrierSolvency,
PolicyNum, AttachmentPoint, PreviouslyExhaustedAggregate, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, 
Layer, InsuranceProgramID, InsuranceProgram, LimitTypeID, PStartDate, PEndDate, ActualPStartDate, ActualPEndDate, AnnualPeriod
FROM vPolicy p
WHERE LimitTypeID = @LimitTypeID
GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, CarrierSolvency,
PolicyNum, AttachmentPoint, PreviouslyExhaustedAggregate, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, 
Layer, InsuranceProgramID, InsuranceProgram, LimitTypeID, PStartDate, PEndDate, ActualPStartDate, ActualPEndDate, AnnualPeriod
 
SELECT ar.DateToPresentValue, MIN(ar.DateTime) AS [DateTime], SUM(ar.Allocation) AS Allocation, CONVERT(money, SUM(ar.PV)) AS PV, SUM(ar.SIRAllocated) AS SIRAllocated,  
  pinfo.CarrierID, pinfo.CarrierSolvency, pinfo.CarrierName, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END AS 'CarrierSort',  
        pinfo.CarrierGroupID, pinfo.CarrierGroupName, pinfo.PolicyID, pinfo.PolicyDateID, pinfo.PolicyNum, pinfo.AttachmentPoint, pinfo.PreviouslyExhaustedAggregate, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit, 
        pinfo.Layer, a.AllocationID, a.AllocationDescription, a.AllocationSchemeID, a.TriggerStartID, a.TriggerStartDate, a.TriggerEndID, a.TriggerEndDate, a.OccurrenceDefId, a.UnCoveredYearstoPH, 
        a.Cost1, a.Cost2, a.Cost3, a.Cost4, a.Cost5, a.Cost6, a.Cost7, a.Cost8, a.Cost9, a.SIRID, a.SIRToPay, a.LessThanAttachmentPoint, a.LessThanLayer, a.OccurrencesInClaim, pinfo.InsuranceProgram, 
        pinfo.InsuranceProgramID, YieldCurveDate as PVRateDate, PStartDate, PEndDate, ActualPStartDate, ActualPEndDate, AnnualPeriod, CarrierTotalAllocation, CarrierTotalSIR, TotalSIR, TotalAllocation,
        PVTotalAllocation, PVCarrierTotalAllocation, CarrierGroupTotalSIR, CarrierGroupTotalAllocation, PVCarrierGroupTotalAllocation
FROM vAllocationResults ar
        LEFT JOIN @Policy pinfo ON pinfo.PolicyDateID = ar.PolicyDateID
        LEFT JOIN vAllocation a ON a.AllocationID = ar.AllocationID 
        LEFT JOIN tblYieldCurve yc on yc.YieldCurveID = ar.YieldCurveID 
		LEFT JOIN @Allocation al ON ar.AllocationID = al.AllocationID
		LEFT JOIN @CarrierAllocation ca ON ar.AllocationID = ca.AllocationID AND ca.CarrierID = pinfo.CarrierID
			LEFT JOIN @CarrierGroupAllocation cat ON ar.AllocationID = ca.AllocationID AND cat.CarrierGroupID = pinfo.CarrierGroupID
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) AND
        pinfo.CarrierID >= COALESCE(@CarrierID, 0) AND pinfo.CarrierID <= COALESCE(NULLIF(@CarrierID, 0), @maxCarrierID) AND pinfo.CarrierSolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%') AND
        (coalesce(pinfo.CarrierGroupID,0) >= COALESCE(@CarrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID,0) <= COALESCE(@CarrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID,0) = @CarrierGroupID) AND
        pinfo.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) 
GROUP BY ar.DateToPresentValue, pinfo.CarrierID, pinfo.CarrierSolvency, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END, pinfo.PolicyID, pinfo.PolicyDateID, pinfo.InsuranceProgram,  
        ar.PStartSplit, ar.PEndSplit, ar.AnnualPeriodSplit, pinfo.CarrierName, pinfo.PolicyNum, pinfo.AttachmentPoint, pinfo.PreviouslyExhaustedAggregate, pinfo.PerOccLimit, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit, 
        pinfo.Layer, a.AllocationID, a.AllocationDescription, a.AllocationSchemeID, a.TriggerStartID, a.TriggerStartDate, a.TriggerEndID, a.TriggerEndDate, a.OccurrenceDefId, a.UnCoveredYearstoPH, 
        a.Cost1, a.Cost2, a.Cost3, a.Cost4, a.Cost5, a.Cost6, a.Cost7, a.Cost8, a.Cost9, a.SIRID, a.SIRToPay, a.LessThanAttachmentPoint, a.LessThanLayer, a.OccurrencesInClaim, pinfo.CarrierGroupID, 
        pinfo.CarrierGroupName, pinfo.InsuranceProgramID, YieldCurveDate, PStartDate, PEndDate, ActualPStartDate, ActualPEndDate, AnnualPeriod, CarrierTotalAllocation, CarrierTotalSIR, TotalSIR, TotalAllocation,
        PVTotalAllocation, PVCarrierTotalAllocation, CarrierGroupTotalSIR, CarrierGroupTotalAllocation, PVCarrierGroupTotalAllocation
 
RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByPolicy] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptARByPolicy] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByPolicy] TO [power_user]
GO
