SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmStateLookup] 
as

Select 0 as StateID, '<<All>>' as state, '<<All States>>' as fullNameState
Union All
Select StateID, state, fullNameState
From tblState
Where fullnamestate is not Null
Order By fullnamestate


Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmStateLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmStateLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmStateLookup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmStateLookup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmStateLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmStateLookup] TO [power_user]
GO
