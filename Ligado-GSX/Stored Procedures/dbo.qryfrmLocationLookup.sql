SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[qryfrmLocationLookup]
AS

SET NOCOUNT ON

SELECT * 
FROM tblLocation
ORDER BY Location

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLocationLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLocationLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLocationLookup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmLocationLookup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLocationLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLocationLookup] TO [power_user]
GO
