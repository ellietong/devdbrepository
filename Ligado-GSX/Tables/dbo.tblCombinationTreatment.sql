CREATE TABLE [dbo].[tblCombinationTreatment]
(
[CombinationTreatmentID] [int] NOT NULL IDENTITY(1, 1),
[ScenarioID] [int] NOT NULL,
[Combination] [int] NOT NULL,
[TreatmentID] [int] NULL,
[TreatmentNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCombinationTreatment] ADD CONSTRAINT [PK_tblCombinationTreatment] PRIMARY KEY CLUSTERED  ([CombinationTreatmentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCombinationTreatment] ADD CONSTRAINT [FK_tblCombinationTreatment_tblScenario] FOREIGN KEY ([ScenarioID]) REFERENCES [dbo].[tblScenario] ([ScenarioID])
GO
ALTER TABLE [dbo].[tblCombinationTreatment] ADD CONSTRAINT [FK_tblCombinationTreatment_tblTreatment] FOREIGN KEY ([TreatmentID]) REFERENCES [dbo].[tblTreatment] ([TreatmentID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCombinationTreatment] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblCombinationTreatment] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblCombinationTreatment] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblCombinationTreatment] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblCombinationTreatment] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCombinationTreatment] TO [base_user]
GRANT SELECT ON  [dbo].[tblCombinationTreatment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCombinationTreatment] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCombinationTreatment] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblCombinationTreatment] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblCombinationTreatment] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblCombinationTreatment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCombinationTreatment] TO [power_user]
GRANT SELECT ON  [dbo].[tblCombinationTreatment] TO [power_user]
GRANT INSERT ON  [dbo].[tblCombinationTreatment] TO [power_user]
GRANT DELETE ON  [dbo].[tblCombinationTreatment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCombinationTreatment] TO [power_user]
GO
