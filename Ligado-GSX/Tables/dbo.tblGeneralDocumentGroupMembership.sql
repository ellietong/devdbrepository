CREATE TABLE [dbo].[tblGeneralDocumentGroupMembership]
(
[GeneralDocumentGroupMembershipID] [bigint] NOT NULL IDENTITY(1, 1),
[GeneralDocumentGroupID] [int] NOT NULL,
[PersonID] [int] NULL,
[FirmAddressID] [int] NULL,
[FirmID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblGeneralDocumentGroupMembership] ADD CONSTRAINT [PK_tblGeneralDocumentGroupMembership] PRIMARY KEY CLUSTERED  ([GeneralDocumentGroupMembershipID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblGeneralDocumentGroupMembership] ADD CONSTRAINT [FK_tblGeneralDocumentGroupMembership_tblDefenseInvoiceFirm] FOREIGN KEY ([FirmID]) REFERENCES [dbo].[tblDefenseInvoiceFirm] ([FirmID])
GO
ALTER TABLE [dbo].[tblGeneralDocumentGroupMembership] ADD CONSTRAINT [FK_tblGeneralDocumentGroupMembership_tblGeneralDocumentGroup] FOREIGN KEY ([GeneralDocumentGroupID]) REFERENCES [dbo].[tblGeneralDocumentGroup] ([GeneralDocumentGroupID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblGeneralDocumentGroupMembership] TO [base_user]
GRANT SELECT ON  [dbo].[tblGeneralDocumentGroupMembership] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblGeneralDocumentGroupMembership] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblGeneralDocumentGroupMembership] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblGeneralDocumentGroupMembership] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblGeneralDocumentGroupMembership] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblGeneralDocumentGroupMembership] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblGeneralDocumentGroupMembership] TO [power_user]
GRANT SELECT ON  [dbo].[tblGeneralDocumentGroupMembership] TO [power_user]
GRANT INSERT ON  [dbo].[tblGeneralDocumentGroupMembership] TO [power_user]
GRANT DELETE ON  [dbo].[tblGeneralDocumentGroupMembership] TO [power_user]
GRANT UPDATE ON  [dbo].[tblGeneralDocumentGroupMembership] TO [power_user]
GO
