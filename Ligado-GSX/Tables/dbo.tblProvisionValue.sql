CREATE TABLE [dbo].[tblProvisionValue]
(
[ProvisionValueID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionLabelID] [int] NOT NULL,
[ProvisionValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionValue] ADD CONSTRAINT [PK__tblProvisionValu__24927208] PRIMARY KEY CLUSTERED  ([ProvisionValueID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblProvisionValue_tblProvisionLabel] ON [dbo].[tblProvisionValue] ([ProvisionLabelID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionValue] ADD CONSTRAINT [FK_tblProvisionValue_tblProvisionLabel] FOREIGN KEY ([ProvisionLabelID]) REFERENCES [dbo].[tblProvisionLabel] ([ProvisionLabelID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionValue] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionValue] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionValue] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblProvisionValue] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblProvisionValue] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblProvisionValue] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblProvisionValue] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionValue] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionValue] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvisionValue] TO [power_user]
GRANT DELETE ON  [dbo].[tblProvisionValue] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvisionValue] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionValue] TO [Provision_User]
GRANT INSERT ON  [dbo].[tblProvisionValue] TO [Provision_User]
GRANT DELETE ON  [dbo].[tblProvisionValue] TO [Provision_User]
GRANT UPDATE ON  [dbo].[tblProvisionValue] TO [Provision_User]
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AlternateBackThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetForeThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_DatasheetGridlinesThemeColorIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ThemeFontIndex', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionLabelID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionLabelID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionLabelID'
GO
DECLARE @xp smallint
SELECT @xp=1980
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionLabelID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionLabelID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionLabelID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValue'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValue'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValue'
GO
DECLARE @xp smallint
SELECT @xp=1830
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValue'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValue'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValue'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValueID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValueID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValueID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValueID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_CurrencyLCID', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValueID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblProvisionValue', 'COLUMN', N'ProvisionValueID'
GO
