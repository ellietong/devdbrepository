CREATE TABLE [dbo].[tblProvisionGenericLanguage]
(
[ProvisionGenericLanguageID] [int] NOT NULL IDENTITY(1, 1),
[GenericLanguageID] [int] NULL,
[ProvisionLabelID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionGenericLanguage] ADD CONSTRAINT [PK_tblProvisionGenericLanguage] PRIMARY KEY CLUSTERED  ([ProvisionGenericLanguageID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionGenericLanguage] ADD CONSTRAINT [FK_tblProvisionGenericLanguage_tblGenericLanguage] FOREIGN KEY ([GenericLanguageID]) REFERENCES [dbo].[tblGenericLanguage] ([GenericLanguageID])
GO
ALTER TABLE [dbo].[tblProvisionGenericLanguage] ADD CONSTRAINT [FK_tblProvisionGenericLanguage_tblProvisionLabel] FOREIGN KEY ([ProvisionLabelID]) REFERENCES [dbo].[tblProvisionLabel] ([ProvisionLabelID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionGenericLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionGenericLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionGenericLanguage] TO [Language_User]
GRANT INSERT ON  [dbo].[tblProvisionGenericLanguage] TO [Language_User]
GRANT DELETE ON  [dbo].[tblProvisionGenericLanguage] TO [Language_User]
GRANT UPDATE ON  [dbo].[tblProvisionGenericLanguage] TO [Language_User]
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionGenericLanguage] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblProvisionGenericLanguage] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblProvisionGenericLanguage] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblProvisionGenericLanguage] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblProvisionGenericLanguage] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionGenericLanguage] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionGenericLanguage] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvisionGenericLanguage] TO [power_user]
GRANT DELETE ON  [dbo].[tblProvisionGenericLanguage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvisionGenericLanguage] TO [power_user]
GO
