CREATE TABLE [dbo].[tblCorrInternalContact]
(
[CorrInternalContactID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NOT NULL,
[InternalContactID] [int] NOT NULL,
[RecipientTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorrInternalContact] ADD CONSTRAINT [PK_tblCorrInternalContact] PRIMARY KEY CLUSTERED  ([CorrInternalContactID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorrInternalContact] ADD CONSTRAINT [FK_tblCorrInternalContact_tblCorrespondence] FOREIGN KEY ([CorrespondenceID]) REFERENCES [dbo].[tblCorrespondence] ([CorrespondenceID])
GO
ALTER TABLE [dbo].[tblCorrInternalContact] ADD CONSTRAINT [FK_tblCorrInternalContact_tblInternalContact] FOREIGN KEY ([InternalContactID]) REFERENCES [dbo].[tblInternalContact] ([InternalContactID])
GO
ALTER TABLE [dbo].[tblCorrInternalContact] ADD CONSTRAINT [FK_tblCorrInternalContact_tblRecipientType] FOREIGN KEY ([RecipientTypeID]) REFERENCES [dbo].[tblRecipientType] ([RecipientTypeID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCorrInternalContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorrInternalContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCorrInternalContact] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCorrInternalContact] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblCorrInternalContact] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblCorrInternalContact] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblCorrInternalContact] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCorrInternalContact] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblCorrInternalContact] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblCorrInternalContact] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblCorrInternalContact] TO [Notice_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCorrInternalContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblCorrInternalContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblCorrInternalContact] TO [power_user]
GRANT DELETE ON  [dbo].[tblCorrInternalContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCorrInternalContact] TO [power_user]
GO
