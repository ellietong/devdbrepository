CREATE TABLE [dbo].[tblClaimLawsuitProduct]
(
[ClaimLawsuitProductID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NOT NULL,
[ProductID] [int] NULL,
[StartDate] [date] NULL,
[EndDate] [date] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitProduct] ADD CONSTRAINT [PK_tblClaimLawsuitProduct] PRIMARY KEY CLUSTERED  ([ClaimLawsuitProductID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitProduct] ADD CONSTRAINT [FK_tblClaimLawsuitProduct_tblProduct] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[tblProduct] ([ProductID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitProduct] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitProduct] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitProduct] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitProduct] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitProduct] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitProduct] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitProduct] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitProduct] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitProduct] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitProduct] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitProduct] TO [power_user]
GO
