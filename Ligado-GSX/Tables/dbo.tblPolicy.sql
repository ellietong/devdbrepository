CREATE TABLE [dbo].[tblPolicy]
(
[PolicyID] [int] NOT NULL IDENTITY(1, 1),
[MinPStartDate] [datetime] NULL,
[CarrierID] [int] NULL,
[NamedInsuredID] [int] NULL,
[InsuranceProgramID] [int] NULL,
[PolicyNum] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PolicyNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyReviewed] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyPDF] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitialDataEntry] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitialDataEntryDate] [datetime] NULL,
[PolicyReview] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyReviewDate] [datetime] NULL,
[PolPicPDFLink] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoNotReport] [bit] NULL CONSTRAINT [DF_tblPolicy_DoNotReport] DEFAULT ((0)),
[NavAIGPolicyID] [bigint] NULL,
[NavSigPolicyID] [bigint] NULL,
[NavNonSigPolicyID] [bigint] NULL,
[NavCarrier] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldPolicyID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicy] ADD CONSTRAINT [PK_tblPolicy] PRIMARY KEY CLUSTERED  ([PolicyID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicy] ADD CONSTRAINT [FK_tblPolicy_tblInsuranceProgram] FOREIGN KEY ([InsuranceProgramID]) REFERENCES [dbo].[tblInsuranceProgram] ([InsuranceProgramID])
GO
ALTER TABLE [dbo].[tblPolicy] ADD CONSTRAINT [FK_tblPolicy_tblNamedInsured] FOREIGN KEY ([NamedInsuredID]) REFERENCES [dbo].[tblNamedInsured] ([NamedInsuredID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicy] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicy] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPolicy] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPolicy] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPolicy] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPolicy] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPolicy] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblPolicy] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblPolicy] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblPolicy] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicy] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicy] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicy] TO [power_user]
GRANT DELETE ON  [dbo].[tblPolicy] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicy] TO [power_user]
GO
