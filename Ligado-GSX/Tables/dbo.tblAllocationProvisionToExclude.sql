CREATE TABLE [dbo].[tblAllocationProvisionToExclude]
(
[ProvisionExcludeID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[ProvisionLabelID] [int] NULL,
[ProvisionValueID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationProvisionToExclude] ADD CONSTRAINT [PK_tblAllocationProvisionToExclude] PRIMARY KEY NONCLUSTERED  ([ProvisionExcludeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblAllocationProvisionToExclude_tblAllocation] ON [dbo].[tblAllocationProvisionToExclude] ([AllocationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationProvisionToExclude] WITH NOCHECK ADD CONSTRAINT [FK_tblAllocationProvisionToExclude_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblAllocationProvisionToExclude] ADD CONSTRAINT [FK_tblAllocationProvisionToExclude_tblProvisionLabel] FOREIGN KEY ([ProvisionLabelID]) REFERENCES [dbo].[tblProvisionLabel] ([ProvisionLabelID])
GO
ALTER TABLE [dbo].[tblAllocationProvisionToExclude] ADD CONSTRAINT [FK_tblAllocationProvisionToExclude_tblProvisionValue] FOREIGN KEY ([ProvisionValueID]) REFERENCES [dbo].[tblProvisionValue] ([ProvisionValueID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationProvisionToExclude] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationProvisionToExclude] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationProvisionToExclude] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationProvisionToExclude] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationProvisionToExclude] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationProvisionToExclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationProvisionToExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationProvisionToExclude] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblAllocationProvisionToExclude] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblAllocationProvisionToExclude] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblAllocationProvisionToExclude] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblAllocationProvisionToExclude] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationProvisionToExclude] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationProvisionToExclude] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationProvisionToExclude] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationProvisionToExclude] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationProvisionToExclude] TO [power_user]
GO
