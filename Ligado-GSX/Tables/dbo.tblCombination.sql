CREATE TABLE [dbo].[tblCombination]
(
[ProvisionComboID] [int] NOT NULL IDENTITY(1, 1),
[Combination] [int] NOT NULL,
[ProvisionLabelID] [int] NOT NULL,
[ProvisionValueID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCombination] ADD CONSTRAINT [PK_tblCombination] PRIMARY KEY CLUSTERED  ([ProvisionComboID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCombination] ADD CONSTRAINT [FK_tblCombination_tblProvisionLabel] FOREIGN KEY ([ProvisionLabelID]) REFERENCES [dbo].[tblProvisionLabel] ([ProvisionLabelID])
GO
ALTER TABLE [dbo].[tblCombination] ADD CONSTRAINT [FK_tblCombination_tblProvisionValue] FOREIGN KEY ([ProvisionValueID]) REFERENCES [dbo].[tblProvisionValue] ([ProvisionValueID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCombination] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblCombination] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblCombination] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblCombination] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblCombination] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCombination] TO [base_user]
GRANT SELECT ON  [dbo].[tblCombination] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCombination] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCombination] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblCombination] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblCombination] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblCombination] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCombination] TO [power_user]
GRANT SELECT ON  [dbo].[tblCombination] TO [power_user]
GRANT INSERT ON  [dbo].[tblCombination] TO [power_user]
GRANT DELETE ON  [dbo].[tblCombination] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCombination] TO [power_user]
GO
