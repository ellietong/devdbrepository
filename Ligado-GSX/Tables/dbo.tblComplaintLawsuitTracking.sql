CREATE TABLE [dbo].[tblComplaintLawsuitTracking]
(
[ComplaintLawsuitTrackingID] [int] NOT NULL IDENTITY(1, 1),
[ComplaintID] [int] NULL,
[ComplaintLawsuitID] [int] NULL,
[LawsuitID] [int] NULL,
[ClaimLawsuitID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblComplaintLawsuitTracking] ADD CONSTRAINT [PK_tblComplaintTrackingLawsuit] PRIMARY KEY CLUSTERED  ([ComplaintLawsuitTrackingID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblComplaintLawsuitTracking] TO [base_user]
GRANT SELECT ON  [dbo].[tblComplaintLawsuitTracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblComplaintLawsuitTracking] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblComplaintLawsuitTracking] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblComplaintLawsuitTracking] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblComplaintLawsuitTracking] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblComplaintLawsuitTracking] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblComplaintLawsuitTracking] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblComplaintLawsuitTracking] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblComplaintLawsuitTracking] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblComplaintLawsuitTracking] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblComplaintLawsuitTracking] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblComplaintLawsuitTracking] TO [power_user]
GRANT SELECT ON  [dbo].[tblComplaintLawsuitTracking] TO [power_user]
GRANT INSERT ON  [dbo].[tblComplaintLawsuitTracking] TO [power_user]
GRANT DELETE ON  [dbo].[tblComplaintLawsuitTracking] TO [power_user]
GRANT UPDATE ON  [dbo].[tblComplaintLawsuitTracking] TO [power_user]
GO
