CREATE TABLE [dbo].[AspNet_ViewUpdateHistory]
(
[ViewName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastCheckSum] [int] NOT NULL,
[LastChecked] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNet_ViewUpdateHistory] ADD CONSTRAINT [PK_AspNet_ViewUpdateHistory] PRIMARY KEY CLUSTERED  ([ViewName]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[AspNet_ViewUpdateHistory] TO [base_user]
GRANT SELECT ON  [dbo].[AspNet_ViewUpdateHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_ViewUpdateHistory] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AspNet_ViewUpdateHistory] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AspNet_ViewUpdateHistory] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AspNet_ViewUpdateHistory] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AspNet_ViewUpdateHistory] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_ViewUpdateHistory] TO [power_user]
GRANT SELECT ON  [dbo].[AspNet_ViewUpdateHistory] TO [power_user]
GRANT INSERT ON  [dbo].[AspNet_ViewUpdateHistory] TO [power_user]
GRANT DELETE ON  [dbo].[AspNet_ViewUpdateHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[AspNet_ViewUpdateHistory] TO [power_user]
GO
