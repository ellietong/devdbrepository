CREATE TABLE [dbo].[tblBillGroupPolicyExclude]
(
[BillGroupPolicyExcludeID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupID] [int] NOT NULL,
[PolicyID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillGroupPolicyExclude] ADD CONSTRAINT [PK_tblBillGroupPolicyExclude] PRIMARY KEY CLUSTERED  ([BillGroupPolicyExcludeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillGroupPolicyExclude] ADD CONSTRAINT [FK_tblBillGroupPolicyExclude_tblBillGroup] FOREIGN KEY ([BillGroupID]) REFERENCES [dbo].[tblBillGroup] ([BillGroupID])
GO
ALTER TABLE [dbo].[tblBillGroupPolicyExclude] WITH NOCHECK ADD CONSTRAINT [FK_tblBillGroupPolicyExclude_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy] ([PolicyID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupPolicyExclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillGroupPolicyExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupPolicyExclude] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblBillGroupPolicyExclude] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblBillGroupPolicyExclude] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblBillGroupPolicyExclude] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblBillGroupPolicyExclude] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupPolicyExclude] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBillGroupPolicyExclude] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBillGroupPolicyExclude] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBillGroupPolicyExclude] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBillGroupPolicyExclude] TO [Ligado_User]
GRANT ALTER ON  [dbo].[tblBillGroupPolicyExclude] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupPolicyExclude] TO [power_user]
GRANT SELECT ON  [dbo].[tblBillGroupPolicyExclude] TO [power_user]
GRANT INSERT ON  [dbo].[tblBillGroupPolicyExclude] TO [power_user]
GRANT DELETE ON  [dbo].[tblBillGroupPolicyExclude] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBillGroupPolicyExclude] TO [power_user]
GO
