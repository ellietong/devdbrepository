CREATE TABLE [dbo].[IMPORT_ClaimsData_March_Supp]
(
[AutoNumber] [int] NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [date] NULL,
[DeceasedDate] [date] NULL,
[Gender] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaritalStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsConsortiumClaim] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HICN] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsMedicareEligible] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsConsortiumMedicareEligible] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportedtoMedicare] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PackYears] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPLastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPFirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPMiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Relationship] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LegalCapacity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPSSN] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPTaxID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPAddress1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPAddress2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPState] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPZip] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiseaseDiagnosisDate] [date] NULL,
[DocketNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndividualDocketNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawsuitState] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourtType] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [date] NULL,
[ServiceDate] [date] NULL,
[TrialDate] [date] NULL,
[DefenseCounsel] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefendantCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounsel] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffFirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffLastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InactiveDocket] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusDate] [date] NULL,
[SettlementDate] [date] NULL,
[SettlementAmount] [money] NULL,
[CheckNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaidDate] [date] NULL,
[PaidAmount] [money] NULL,
[ExecutedReleaseDate] [date] NULL,
[ReleaseRecdDate] [date] NULL,
[ReleaseType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffExpertILOProfusion] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Occupation] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jobsite] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOFE] [date] NULL,
[DOLE] [date] NULL,
[ProductType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductNotes] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TempCPI] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [base_user]
GRANT SELECT ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [Ligado_User]
GRANT SELECT ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [Ligado_User]
GRANT INSERT ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [Ligado_User]
GRANT DELETE ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [power_user]
GRANT SELECT ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [power_user]
GRANT INSERT ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [power_user]
GRANT DELETE ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [power_user]
GRANT UPDATE ON  [dbo].[IMPORT_ClaimsData_March_Supp] TO [power_user]
GO
