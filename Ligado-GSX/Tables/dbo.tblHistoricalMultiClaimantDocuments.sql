CREATE TABLE [dbo].[tblHistoricalMultiClaimantDocuments]
(
[ClaimantDocumentLinkID] [bigint] NOT NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[ClaimantDocumentID] [bigint] NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDocumentNumber] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDocumentNameInFolder] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblHistoricalMultiClaimantDocuments] ADD CONSTRAINT [PK_tblHistoricalMultiClaimantDocuments] PRIMARY KEY CLUSTERED  ([ClaimantDocumentLinkID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblHistoricalMultiClaimantDocuments_tblClaimantDocuments] ON [dbo].[tblHistoricalMultiClaimantDocuments] ([ClaimantDocumentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblHistoricalMultiClaimantDocuments_ClaimantPersonalInfoID] ON [dbo].[tblHistoricalMultiClaimantDocuments] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblHistoricalMultiClaimantDocuments_tblClaimantPersonalInfo] ON [dbo].[tblHistoricalMultiClaimantDocuments] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblHistoricalMultiClaimantDocuments] ADD CONSTRAINT [FK_tblHistoricalMultiClaimantDocuments_tblClaimantDocuments] FOREIGN KEY ([ClaimantDocumentID]) REFERENCES [dbo].[tblClaimantDocuments] ([ClaimantDocumentID])
GO
ALTER TABLE [dbo].[tblHistoricalMultiClaimantDocuments] WITH NOCHECK ADD CONSTRAINT [FK_tblHistoricalMultiClaimantDocuments_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [power_user]
GRANT DELETE ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [power_user]
GO
