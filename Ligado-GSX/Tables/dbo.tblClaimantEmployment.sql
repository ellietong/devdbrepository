CREATE TABLE [dbo].[tblClaimantEmployment]
(
[ClaimantEmploymentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[OccupationID] [int] NULL,
[Jobsite] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavEmpStartDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavEmpEndDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavEarliestExposureDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLatestExposureDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimary] [bit] NULL,
[IsSecondaryExposure] [bit] NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavOccupation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmpStartDate] [datetime] NULL,
[EmpEndDate] [datetime] NULL,
[EarliestExposureDate] [datetime] NULL,
[LatestExposureDate] [datetime] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteStateID] [int] NULL,
[Old_OccupationID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantEmployment] ADD CONSTRAINT [PK__mBtblCla__F25662D42665ABE1] PRIMARY KEY CLUSTERED  ([ClaimantEmploymentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimantEmployment_tblClaimantPersonalInfo] ON [dbo].[tblClaimantEmployment] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimantEmployment_CPID_IsPrimary] ON [dbo].[tblClaimantEmployment] ([ClaimantPersonalInfoID], [IsPrimary]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantEmployment] WITH NOCHECK ADD CONSTRAINT [FK_tblClaimantEmployment_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimantEmployment] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=1125
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=4785
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpEndDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpStartDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavLatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavLatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavLatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
