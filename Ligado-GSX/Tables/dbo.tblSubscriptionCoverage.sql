CREATE TABLE [dbo].[tblSubscriptionCoverage]
(
[SubscriptionCoverageID] [int] NOT NULL IDENTITY(1, 1),
[SubscriberID] [int] NOT NULL,
[SubscriptionSectionCoverageID] [int] NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[AgencyPercentOfSection] [float] NULL,
[GroupWithinAgencyPercent] [float] NULL,
[SubscriptionCompanyPercent] [float] NULL,
[AgencyID] [int] NULL,
[UWReference] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubscriptionNotes] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcctNum] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriptionCoverage] ADD CONSTRAINT [PK_tblSubscriptionCompanyCoverage] PRIMARY KEY CLUSTERED  ([SubscriptionCoverageID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriptionCoverage] ADD CONSTRAINT [FK_tblSubscriptionCoverage_tblSubscriberAgency_ALT] FOREIGN KEY ([AgencyID]) REFERENCES [dbo].[tblSubscriberAgency_ALT] ([AgencyID])
GO
ALTER TABLE [dbo].[tblSubscriptionCoverage] ADD CONSTRAINT [FK_tblSubscriptionCoverage_tblSubscriberCompany_ALT] FOREIGN KEY ([SubscriberID]) REFERENCES [dbo].[tblSubscriberCompany_ALT] ([SubscriberID])
GO
ALTER TABLE [dbo].[tblSubscriptionCoverage] ADD CONSTRAINT [FK_tblSubscriptionCoverage_tblSubscriptionSectionCoverage] FOREIGN KEY ([SubscriptionSectionCoverageID]) REFERENCES [dbo].[tblSubscriptionSectionCoverage] ([SubscriptionSectionCoverageID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionCoverage] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriptionCoverage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionCoverage] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSubscriptionCoverage] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSubscriptionCoverage] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSubscriptionCoverage] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSubscriptionCoverage] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionCoverage] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblSubscriptionCoverage] TO [Policy_User]
GRANT ALTER ON  [dbo].[tblSubscriptionCoverage] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionCoverage] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriptionCoverage] TO [power_user]
GRANT INSERT ON  [dbo].[tblSubscriptionCoverage] TO [power_user]
GRANT DELETE ON  [dbo].[tblSubscriptionCoverage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSubscriptionCoverage] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionCoverage] TO [Subscription_User]
GRANT SELECT ON  [dbo].[tblSubscriptionCoverage] TO [Subscription_User]
GRANT INSERT ON  [dbo].[tblSubscriptionCoverage] TO [Subscription_User]
GRANT DELETE ON  [dbo].[tblSubscriptionCoverage] TO [Subscription_User]
GRANT UPDATE ON  [dbo].[tblSubscriptionCoverage] TO [Subscription_User]
GO
