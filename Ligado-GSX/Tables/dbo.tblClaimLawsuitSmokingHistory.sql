CREATE TABLE [dbo].[tblClaimLawsuitSmokingHistory]
(
[ClaimLawsuitSmokingHistoryID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[SmokingStart] [date] NULL,
[SmokingEnd] [date] NULL,
[PacksPerDay] [float] NULL,
[Username] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitSmokingHistory] ADD CONSTRAINT [PK_tblClaimLawsuitSmokingHistory] PRIMARY KEY CLUSTERED  ([ClaimLawsuitSmokingHistoryID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitSmokingHistory] WITH NOCHECK ADD CONSTRAINT [FK_tblClaimLawsuitSmokingHistory_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [power_user]
GO
