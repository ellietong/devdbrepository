CREATE TABLE [dbo].[tblOccupation_ALT]
(
[OccupationID] [int] NOT NULL IDENTITY(100000, 1),
[Occupation] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccupation_ALT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation_ALT] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblOccupation_ALT] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblOccupation_ALT] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblOccupation_ALT] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblOccupation_ALT] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccupation_ALT] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccupation_ALT] TO [power_user]
GRANT DELETE ON  [dbo].[tblOccupation_ALT] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccupation_ALT] TO [power_user]
GO
