CREATE TABLE [dbo].[tblFollowUp]
(
[FollowupID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NULL,
[FollowUp] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaskCategoryID] [int] NULL,
[InternalContactID] [int] NULL,
[PartofSitePacket?] [bit] NULL,
[Completed] [bit] NOT NULL,
[DateCompleted] [smalldatetime] NULL,
[FollowUpNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompletedContactID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFollowUp] ADD CONSTRAINT [PK_tblFollowUp] PRIMARY KEY CLUSTERED  ([FollowupID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblFollowUp_tblCorrespondence] ON [dbo].[tblFollowUp] ([CorrespondenceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFollowUp] ADD CONSTRAINT [FK_tblFollowUp_tblCorrespondence] FOREIGN KEY ([CorrespondenceID]) REFERENCES [dbo].[tblCorrespondence] ([CorrespondenceID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblFollowUp] ADD CONSTRAINT [FK_tblFollowUp_tblInternalContact] FOREIGN KEY ([InternalContactID]) REFERENCES [dbo].[tblInternalContact] ([InternalContactID])
GO
ALTER TABLE [dbo].[tblFollowUp] ADD CONSTRAINT [FK_tblFollowUp_tblTaskCategoryLookup] FOREIGN KEY ([TaskCategoryID]) REFERENCES [dbo].[tblTaskCategoryLookup] ([TaskCategoryID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblFollowUp] TO [base_user]
GRANT SELECT ON  [dbo].[tblFollowUp] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblFollowUp] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblFollowUp] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblFollowUp] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblFollowUp] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblFollowUp] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblFollowUp] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblFollowUp] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblFollowUp] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblFollowUp] TO [Notice_User]
GRANT VIEW DEFINITION ON  [dbo].[tblFollowUp] TO [power_user]
GRANT SELECT ON  [dbo].[tblFollowUp] TO [power_user]
GRANT INSERT ON  [dbo].[tblFollowUp] TO [power_user]
GRANT DELETE ON  [dbo].[tblFollowUp] TO [power_user]
GRANT UPDATE ON  [dbo].[tblFollowUp] TO [power_user]
GO
