CREATE TABLE [dbo].[m_tblBundledPreSettlement]
(
[Injured_Party_ID] [bigint] NULL,
[Claim_Sequence_Nbr] [int] NULL,
[Company_Nbr] [int] NULL,
[Company_Name] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settlement_Date] [date] NULL,
[Settlement_Amt] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblBundledPreSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblBundledPreSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblBundledPreSettlement] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblBundledPreSettlement] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblBundledPreSettlement] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblBundledPreSettlement] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblBundledPreSettlement] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblBundledPreSettlement] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblBundledPreSettlement] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblBundledPreSettlement] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblBundledPreSettlement] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblBundledPreSettlement] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblBundledPreSettlement] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblBundledPreSettlement] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblBundledPreSettlement] TO [power_user]
GO
