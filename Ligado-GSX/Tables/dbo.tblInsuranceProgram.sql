CREATE TABLE [dbo].[tblInsuranceProgram]
(
[InsuranceProgramID] [int] NOT NULL IDENTITY(1, 1),
[InsuranceProgram] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblInsuranceProgram] ADD CONSTRAINT [PK_tblInsuranceProgram] PRIMARY KEY CLUSTERED  ([InsuranceProgramID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblInsuranceProgram] TO [base_user]
GRANT SELECT ON  [dbo].[tblInsuranceProgram] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblInsuranceProgram] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblInsuranceProgram] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblInsuranceProgram] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblInsuranceProgram] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblInsuranceProgram] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblInsuranceProgram] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblInsuranceProgram] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblInsuranceProgram] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblInsuranceProgram] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblInsuranceProgram] TO [power_user]
GRANT SELECT ON  [dbo].[tblInsuranceProgram] TO [power_user]
GRANT INSERT ON  [dbo].[tblInsuranceProgram] TO [power_user]
GRANT DELETE ON  [dbo].[tblInsuranceProgram] TO [power_user]
GRANT UPDATE ON  [dbo].[tblInsuranceProgram] TO [power_user]
GO
