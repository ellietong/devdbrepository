CREATE TABLE [dbo].[tblMatterAliases]
(
[MatterAlias] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MatterID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMatterAliases] ADD CONSTRAINT [PK_tblMatterAliases_1] PRIMARY KEY CLUSTERED  ([MatterAlias]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMatterAliases] ADD CONSTRAINT [FK_tblMatterAlias_tblMatter] FOREIGN KEY ([MatterID]) REFERENCES [dbo].[tblMatter] ([MatterID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMatterAliases] TO [base_user]
GRANT SELECT ON  [dbo].[tblMatterAliases] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMatterAliases] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblMatterAliases] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblMatterAliases] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblMatterAliases] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblMatterAliases] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMatterAliases] TO [power_user]
GRANT SELECT ON  [dbo].[tblMatterAliases] TO [power_user]
GRANT INSERT ON  [dbo].[tblMatterAliases] TO [power_user]
GRANT DELETE ON  [dbo].[tblMatterAliases] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMatterAliases] TO [power_user]
GO
