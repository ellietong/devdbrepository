CREATE TABLE [dbo].[ZZ_DiseaseUpdateImport$]
(
[Claimant_Last Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claimant_First Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Defendant Company] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_DiseaseUpdateImport$] TO [power_user]
GO
