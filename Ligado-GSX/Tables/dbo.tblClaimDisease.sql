CREATE TABLE [dbo].[tblClaimDisease]
(
[ClaimDiseaseID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID] [bigint] NULL,
[DiseaseID] [int] NULL,
[PrimaryDisease] [bit] NULL,
[DiseaseDiagnosisDate] [datetime] NULL,
[DoctorID] [int] NULL,
[NavDisease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimDiseaseNote] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EntryDate] [datetime] NULL CONSTRAINT [DF_tblClaimDisease_EntryDate] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Crook
-- Create date: 11/12/13
-- Description:	Trigger for making primary status the last one inserted
-- for Marley
-- =============================================
CREATE TRIGGER [dbo].[PrimaryDisease]
   ON  [dbo].[tblClaimDisease]
   AFTER INSERT
AS 
BEGIN

	SET NOCOUNT ON;

	--Set all the Disease records for the Claim to Primary = FALSE
	UPDATE tblClaimDisease
	SET PrimaryDisease = 0
	FROM tblClaimDisease cd
	WHERE ClaimID IN (SELECT ClaimID FROM inserted)

    --Update the last one from inserted as true
	UPDATE tblClaimDisease
	SET PrimaryDisease = 1
	WHERE ClaimDiseaseID IN (SELECT ClaimDiseaseID FROM inserted)

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Crook
-- Create date: 11/12/13
-- Description:	Trigger for making primary status the last one inserted
-- for Marley
-- =============================================
CREATE TRIGGER [dbo].[PrimaryDisease_Complaints]
   ON  [dbo].[tblClaimDisease]
   AFTER INSERT
AS 
BEGIN

	SET NOCOUNT ON;

	--Set all the Disease records for the Claim to Primary = FALSE
	UPDATE tblClaimDisease
	SET PrimaryDisease = 0
	FROM tblClaimDisease cd
	WHERE ClaimID IN (SELECT ClaimID FROM inserted)

END

GO
DISABLE TRIGGER [dbo].[PrimaryDisease_Complaints] ON [dbo].[tblClaimDisease]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Crook
-- Create date: 11/12/13
-- Description:	Trigger for making primary status the last one inserted
-- for Marley
-- =============================================
CREATE TRIGGER [dbo].[PrimaryDisease_DELETE]
   ON  [dbo].[tblClaimDisease]
   AFTER DELETE
AS 
BEGIN

	SET NOCOUNT ON;


	--Set all the...previous MAX ClaimDiseaseID for the Claim back to primary = TRUE?
	UPDATE tblClaimDisease
	SET PrimaryDisease = 1
	FROM tblClaimDisease cd
	WHERE cd.ClaimDiseaseID IN (SELECT MAX(cdmax.ClaimDiseaseID) AS LastClaimDiseaseID 
								FROM tblClaimDisease AS cdmax 
								INNER JOIN deleted ON cdmax.ClaimID = deleted.ClaimID
								WHERE cdmax.ClaimID = cd.ClaimID)
END
GO
ALTER TABLE [dbo].[tblClaimDisease] ADD CONSTRAINT [PK_tblClaimDisease] PRIMARY KEY CLUSTERED  ([ClaimDiseaseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_ClaimID_PrimaryDisease] ON [dbo].[tblClaimDisease] ([ClaimID], [PrimaryDisease]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_8258_8257_tblClaimDisease] ON [dbo].[tblClaimDisease] ([PrimaryDisease]) INCLUDE ([ClaimID], [DiseaseID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimDisease] WITH NOCHECK ADD CONSTRAINT [FK_tblClaimDisease_tblClaim] FOREIGN KEY ([ClaimID]) REFERENCES [dbo].[tblClaim] ([ClaimID])
GO
ALTER TABLE [dbo].[tblClaimDisease] ADD CONSTRAINT [FK_tblClaimDisease_tblDoctor] FOREIGN KEY ([DoctorID]) REFERENCES [dbo].[tblDoctor] ([DoctorID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimDisease] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimDisease] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimDisease] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimDisease] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimDisease] TO [power_user]
GO
