CREATE TABLE [dbo].[tblFormStoredProc]
(
[FormStoredProcID] [int] NOT NULL IDENTITY(1, 1),
[FormID] [int] NULL,
[StoredProcID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFormStoredProc] ADD CONSTRAINT [PK_tblFormStoredProc] PRIMARY KEY CLUSTERED  ([FormStoredProcID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFormStoredProc] ADD CONSTRAINT [FK_tblFormStoredProc_tblDBForms] FOREIGN KEY ([FormID]) REFERENCES [dbo].[tblDBForms] ([FormID])
GO
ALTER TABLE [dbo].[tblFormStoredProc] WITH NOCHECK ADD CONSTRAINT [FK_tblFormStoredProc_tblStoredProc] FOREIGN KEY ([StoredProcID]) REFERENCES [dbo].[tblStoredProc] ([StoredProcID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblFormStoredProc] TO [base_user]
GRANT SELECT ON  [dbo].[tblFormStoredProc] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblFormStoredProc] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblFormStoredProc] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblFormStoredProc] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblFormStoredProc] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblFormStoredProc] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblFormStoredProc] TO [power_user]
GRANT SELECT ON  [dbo].[tblFormStoredProc] TO [power_user]
GRANT INSERT ON  [dbo].[tblFormStoredProc] TO [power_user]
GRANT DELETE ON  [dbo].[tblFormStoredProc] TO [power_user]
GRANT UPDATE ON  [dbo].[tblFormStoredProc] TO [power_user]
GRANT SELECT ON  [dbo].[tblFormStoredProc] TO [Reports_User]
GRANT INSERT ON  [dbo].[tblFormStoredProc] TO [Reports_User]
GRANT DELETE ON  [dbo].[tblFormStoredProc] TO [Reports_User]
GRANT UPDATE ON  [dbo].[tblFormStoredProc] TO [Reports_User]
GO
