CREATE TABLE [dbo].[tblSupportingDocument]
(
[SupportingDocumentID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NOT NULL,
[SupportingDocumentTypeID] [int] NULL,
[Description] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InternalLink] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSupportingDocument] ADD CONSTRAINT [PK_tblSupportingDocument] PRIMARY KEY CLUSTERED  ([SupportingDocumentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSupportingDocument] ADD CONSTRAINT [FK_tblSupportingDocument_tblCorrespondence] FOREIGN KEY ([CorrespondenceID]) REFERENCES [dbo].[tblCorrespondence] ([CorrespondenceID])
GO
ALTER TABLE [dbo].[tblSupportingDocument] ADD CONSTRAINT [FK_tblSupportingDocument_tblSupportingDocumentType] FOREIGN KEY ([SupportingDocumentTypeID]) REFERENCES [dbo].[tblSupportingDocumentType] ([SupportingDocumentTypeID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSupportingDocument] TO [base_user]
GRANT SELECT ON  [dbo].[tblSupportingDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSupportingDocument] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSupportingDocument] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSupportingDocument] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSupportingDocument] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSupportingDocument] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSupportingDocument] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblSupportingDocument] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblSupportingDocument] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblSupportingDocument] TO [Notice_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSupportingDocument] TO [power_user]
GRANT SELECT ON  [dbo].[tblSupportingDocument] TO [power_user]
GRANT INSERT ON  [dbo].[tblSupportingDocument] TO [power_user]
GRANT DELETE ON  [dbo].[tblSupportingDocument] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSupportingDocument] TO [power_user]
GO
