CREATE TABLE [dbo].[AspNet_SqlCacheTablesForChangeNotification]
(
[tableName] [nvarchar] (450) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[notificationCreated] [datetime] NOT NULL CONSTRAINT [DF__AspNet_Sq__notif__1BFEC2B5] DEFAULT (getdate()),
[changeId] [int] NOT NULL CONSTRAINT [DF__AspNet_Sq__chang__1CF2E6EE] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNet_SqlCacheTablesForChangeNotification] ADD CONSTRAINT [PK__AspNet_S__93F7AC6963FCAA3A] PRIMARY KEY CLUSTERED  ([tableName]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [base_user]
GRANT SELECT ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [power_user]
GRANT SELECT ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [power_user]
GRANT INSERT ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [power_user]
GRANT DELETE ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [power_user]
GRANT UPDATE ON  [dbo].[AspNet_SqlCacheTablesForChangeNotification] TO [power_user]
GO
