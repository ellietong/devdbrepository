CREATE TABLE [dbo].[tblZipQueue]
(
[ZipQueueID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_tblZipQueue_ZipQueueID] DEFAULT (newid()),
[Bucket] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddedToQueue] [datetime] NULL,
[StartTime] [datetime] NULL,
[ZipKey] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblZipQueue] ADD CONSTRAINT [PK_tblZipQueue] PRIMARY KEY CLUSTERED  ([ZipQueueID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblZipQueue] TO [base_user]
GRANT SELECT ON  [dbo].[tblZipQueue] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblZipQueue] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblZipQueue] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblZipQueue] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblZipQueue] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblZipQueue] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblZipQueue] TO [power_user]
GRANT SELECT ON  [dbo].[tblZipQueue] TO [power_user]
GRANT INSERT ON  [dbo].[tblZipQueue] TO [power_user]
GRANT DELETE ON  [dbo].[tblZipQueue] TO [power_user]
GRANT UPDATE ON  [dbo].[tblZipQueue] TO [power_user]
GO
