CREATE TABLE [dbo].[ZZ_ComplaintMatchingImport$]
(
[ImportID] [int] NULL,
[File Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Matter] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [int] NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportMonth] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_ComplaintMatchingImport$] TO [power_user]
GO
