CREATE TABLE [dbo].[tblCorrCarrierContact]
(
[CorrCarrierContactID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NOT NULL,
[CarrierContactID] [int] NOT NULL,
[RecipientTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorrCarrierContact] ADD CONSTRAINT [PK_tblCorrCarrierContact] PRIMARY KEY CLUSTERED  ([CorrCarrierContactID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorrCarrierContact] ADD CONSTRAINT [FK_tblCorrCarrierContact_tblCarrierContact] FOREIGN KEY ([CarrierContactID]) REFERENCES [dbo].[tblCarrierContact] ([CarrierContactID])
GO
ALTER TABLE [dbo].[tblCorrCarrierContact] ADD CONSTRAINT [FK_tblCorrCarrierContact_tblCorrespondence] FOREIGN KEY ([CorrespondenceID]) REFERENCES [dbo].[tblCorrespondence] ([CorrespondenceID])
GO
ALTER TABLE [dbo].[tblCorrCarrierContact] ADD CONSTRAINT [FK_tblCorrCarrierContact_tblRecipientType] FOREIGN KEY ([RecipientTypeID]) REFERENCES [dbo].[tblRecipientType] ([RecipientTypeID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCorrCarrierContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorrCarrierContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCorrCarrierContact] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCorrCarrierContact] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblCorrCarrierContact] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblCorrCarrierContact] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblCorrCarrierContact] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCorrCarrierContact] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblCorrCarrierContact] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblCorrCarrierContact] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblCorrCarrierContact] TO [Notice_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCorrCarrierContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblCorrCarrierContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblCorrCarrierContact] TO [power_user]
GRANT DELETE ON  [dbo].[tblCorrCarrierContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCorrCarrierContact] TO [power_user]
GO
