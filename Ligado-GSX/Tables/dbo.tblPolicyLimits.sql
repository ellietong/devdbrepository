CREATE TABLE [dbo].[tblPolicyLimits]
(
[LimitID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NULL,
[LimitTypeID] [int] NULL,
[AttachmentPoint] [money] NULL,
[PerOccLimit] [money] NULL,
[LayerPerOccLimit] [money] NULL,
[SIR] [money] NULL,
[SIRAggregateLimit] [money] NULL,
[AggregateLimit] [money] NULL,
[PreviouslyExhaustedAggregate] [money] NULL,
[Layer] [tinyint] NULL,
[LimitNote] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldLimitID] [int] NULL,
[OldPolicyID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyLimits] ADD CONSTRAINT [PK_tblPolicyLimits] PRIMARY KEY CLUSTERED  ([LimitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblPolicyLimits_tblPolicy] ON [dbo].[tblPolicyLimits] ([PolicyID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyLimits] ADD CONSTRAINT [FK_tblPolicyLimits_tblPolicyLimitType] FOREIGN KEY ([LimitTypeID]) REFERENCES [dbo].[tblPolicyLimitType] ([LimitTypeID])
GO
ALTER TABLE [dbo].[tblPolicyLimits] ADD CONSTRAINT [FK_tblPolicyLimits_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy] ([PolicyID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyLimits] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyLimits] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyLimits] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPolicyLimits] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPolicyLimits] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPolicyLimits] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPolicyLimits] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPolicyLimits] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblPolicyLimits] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblPolicyLimits] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblPolicyLimits] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyLimits] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicyLimits] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicyLimits] TO [power_user]
GRANT DELETE ON  [dbo].[tblPolicyLimits] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicyLimits] TO [power_user]
GO
