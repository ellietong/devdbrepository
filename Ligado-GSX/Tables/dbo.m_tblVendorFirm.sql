CREATE TABLE [dbo].[m_tblVendorFirm]
(
[VendorFirmID] [int] NOT NULL,
[VendorFirm] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[ZipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblVendorFirm] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblVendorFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblVendorFirm] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblVendorFirm] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblVendorFirm] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblVendorFirm] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblVendorFirm] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblVendorFirm] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblVendorFirm] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblVendorFirm] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblVendorFirm] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblVendorFirm] TO [power_user]
GO
