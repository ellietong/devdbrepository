CREATE TABLE [dbo].[ZZ_PaymentCheckImport]
(
[AutoNumber] [int] NULL,
[CheckID] [int] NULL,
[CheckNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaidDate] [datetime] NULL,
[PaidAmount] [money] NULL,
[DefendantCompany] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountOfCheckNumber] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_PaymentCheckImport] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_PaymentCheckImport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_PaymentCheckImport] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ZZ_PaymentCheckImport] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ZZ_PaymentCheckImport] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ZZ_PaymentCheckImport] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ZZ_PaymentCheckImport] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_PaymentCheckImport] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_PaymentCheckImport] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_PaymentCheckImport] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_PaymentCheckImport] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_PaymentCheckImport] TO [power_user]
GO
