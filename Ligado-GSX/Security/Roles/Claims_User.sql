CREATE ROLE [Claims_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Claims_User', N'colek'
GO
EXEC sp_addrolemember N'Claims_User', N'hagep'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\carthyl'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\hashemik'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\jacobsb'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\marshc'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\ohaverk'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\pikeg'
GO
EXEC sp_addrolemember N'Claims_User', N'madlp'
GO
EXEC sp_addrolemember N'Claims_User', N'mollardj'
GO
EXEC sp_addrolemember N'Claims_User', N'owenss'
GO
EXEC sp_addrolemember N'Claims_User', N'zongc'
GO
