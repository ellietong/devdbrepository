SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnCombineName]
(
	@First nvarchar(100), @Middle nvarchar(100), @Last nvarchar(100), @Suffix nvarchar(50)
)
RETURNS nvarchar(500)
AS
BEGIN

	-- Return the result of the function
	RETURN COALESCE(@Last, '') + COALESCE(' ' + @Suffix + ', ', ', ') + COALESCE(@First, '') + COALESCE(' ' + @Middle, '')

END
GO
