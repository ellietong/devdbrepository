SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vDETAILS_MedicalScores]
AS
SELECT        c.ClaimID, c.ClaimantPersonalInfoID, c.ILODoctorID, c.ILODate, c.ILOProfusion, c.PFTDoctorID, c.PFTDate, c.PFTTypeID, c.PFTTV, c.PFTFVC_Actual, 
                         c.PFTFVC_Predicted, c.PFTFEV1_Actual, c.PFTFEV1_Predicted, c.PFTTLC_Actual, c.PFTTLC_Predicted, c.PFTDLCO_Actual, c.PFTDLCO_Predicted, 
                         c.PFTFEV1AndFVC_Actual, c.PFTFEV1AndFVC_Predicted, p.DoctorLastName + ', ' + p.DoctorFirstName AS PFTDoctor, 
                         i.DoctorLastName + ', ' + i.DoctorFirstName AS ILODoctor
FROM            dbo.tblClaim AS c LEFT OUTER JOIN
                         dbo.tblDoctor AS p ON c.PFTDoctorID = p.DoctorID LEFT OUTER JOIN
                         dbo.tblDoctor AS i ON c.ILODoctorID = i.DoctorID

GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_MedicalScores] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_MedicalScores] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_MedicalScores] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_MedicalScores] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_MedicalScores] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_MedicalScores] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_MedicalScores] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_MedicalScores] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_MedicalScores] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_MedicalScores] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_MedicalScores] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_MedicalScores] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 270
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 308
               Bottom = 136
               Right = 505
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "i"
            Begin Extent = 
               Top = 6
               Left = 543
               Bottom = 136
               Right = 740
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_MedicalScores', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_MedicalScores', NULL, NULL
GO
