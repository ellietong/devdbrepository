SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vSETTLEMENT_PaymentApproval]
AS

SELECT        cpi.LastName + ', ' + cpi.FirstName + COALESCE (' ' + cpi.MiddleName, '') AS ClaimantName, cl.ClaimLawsuitID, cs.ClaimSettlementID, c.ClaimantPersonalInfoID, pca.PlaintiffCounsel, cs.SettlementDate, 
                         cs.SettlementAmount, m.Matter, CASE WHEN PaymentApprovedBy IS NULL AND PaymentApprovedDate IS NULL THEN 0 ELSE 1 END AS IsApproved, CASE WHEN PaymentRecommendedBy IS NULL AND 
                         PaymentRecommendedDate IS NULL THEN 0 ELSE 1 END AS IsRecommended, COALESCE (SUM(ar.Amount), 0) AS TotalARAmount, o.ExpectedPayDate, NULL AS PrivateComment
FROM            dbo.tblClaim AS c INNER JOIN
                         dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                         dbo.tblMatter AS m ON c.MatterID = m.MatterID INNER JOIN
                         dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                         dbo.tblClaimLawsuitStatusTracking AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID INNER JOIN
                         dbo.tblLawsuit AS l ON l.LawsuitID = cl.LawsuitID LEFT OUTER JOIN
                         dbo.tblPlaintiffCounselAddress AS pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID INNER JOIN
                         dbo.tblClaimSettlement AS cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblPaymentSettlement AS ps ON cs.ClaimSettlementID = ps.ClaimSettlementID LEFT OUTER JOIN
                         dbo.tblOccurrence AS o ON o.ClaimID = cl.ClaimLawsuitID INNER JOIN
                         dbo.tblExhaustion AS e ON e.OccurrenceID = o.OccurrenceID LEFT JOIN
                         dbo.tblAccountsReceivablePayment AS ar ON ar.ExhaustionID = e.ExhaustionID
WHERE         (COALESCE (clst.IsPrimaryStatus, 0) = 1) AND (clst.ClaimLawsuitStatusID IN (23, 24, 25, 39)) AND (cs.SettlementAmount >= 0)
				AND PaymentApprovedBy IS NULL And occurrencetypeid = 1
GROUP BY cpi.LastName + ', ' + cpi.FirstName + COALESCE (' ' + cpi.MiddleName, ''), cl.ClaimLawsuitID, cs.ClaimSettlementID, c.ClaimantPersonalInfoID, cl.LawsuitID, pca.PlaintiffCounsel, cs.SettlementDate, 
                         cs.SettlementAmount, m.Matter, CASE WHEN PaymentApprovedBy IS NULL AND PaymentApprovedDate IS NULL THEN 0 ELSE 1 END, CASE WHEN PaymentRecommendedBy IS NULL AND 
                         PaymentRecommendedDate IS NULL THEN 0 ELSE 1 END, o.ExpectedPayDate
--HAVING        (COALESCE (SUM(ps.PaymentAmount), 0) < cs.SettlementAmount)






--GO




GO
GRANT VIEW DEFINITION ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [base_user]
GRANT SELECT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT SELECT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT INSERT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT DELETE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT UPDATE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[12] 4[31] 2[38] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 312
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 304
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 366
               Right = 224
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 496
               Right = 281
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clst"
            Begin Extent = 
               Top = 498
               Left = 38
               Bottom = 628
               Right = 303
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 630
               Left = 38
               Bottom = 760
               Right = 293
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pca"
            Begin Extent = 
               Top = 762
               Left = 38
               Bottom = 892
               Right = 283
            End
            DisplayFlags = 280
            TopColumn = 0
        ', 'SCHEMA', N'dbo', 'VIEW', N'vSETTLEMENT_PaymentApproval', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' End
         Begin Table = "cs"
            Begin Extent = 
               Top = 894
               Left = 38
               Bottom = 1024
               Right = 298
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ps"
            Begin Extent = 
               Top = 1026
               Left = 38
               Bottom = 1156
               Right = 278
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "o"
            Begin Extent = 
               Top = 1158
               Left = 38
               Bottom = 1288
               Right = 272
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 1290
               Left = 38
               Bottom = 1420
               Right = 312
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ar"
            Begin Extent = 
               Top = 1422
               Left = 38
               Bottom = 1552
               Right = 307
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 13
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 6255
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSETTLEMENT_PaymentApproval', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSETTLEMENT_PaymentApproval', NULL, NULL
GO
