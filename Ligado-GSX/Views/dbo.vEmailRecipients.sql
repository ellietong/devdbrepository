SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vEmailRecipients]
AS
SELECT DISTINCT 
                      [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite.FirmAddressID, [Ligado-ExtranetSecurity].dbo.Person.Email, [Ligado-ExtranetSecurity].dbo.Role.RoleName, 
                      [Ligado-ExtranetSecurity].dbo.RolePersonFirmAddressClientSite.RoleID, [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite.ClientSiteID
FROM         [Ligado-ExtranetSecurity].dbo.Person INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite ON 
                      [Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite.PersonID = [Ligado-ExtranetSecurity].dbo.Person.PersonID INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite ON 
                      [Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite.FirmAddressClientSiteID = [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite.FirmAddressClientSiteID INNER
                       JOIN
                      [Ligado-ExtranetSecurity].dbo.RolePersonFirmAddressClientSite ON 
                      [Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite.PersonFirmAddressClientSiteID = [Ligado-ExtranetSecurity].dbo.RolePersonFirmAddressClientSite.PersonFirmAddressClientSiteID
                       INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.Role ON [Ligado-ExtranetSecurity].dbo.RolePersonFirmAddressClientSite.RoleID = [Ligado-ExtranetSecurity].dbo.Role.RoleID
WHERE     ([Ligado-ExtranetSecurity].dbo.Role.IsNotification = 1) AND ([Ligado-ExtranetSecurity].dbo.FirmAddressClientSite.ClientSiteID = dbo.fn_GetClientSiteID())


GO
GRANT VIEW DEFINITION ON  [dbo].[vEmailRecipients] TO [base_user]
GRANT SELECT ON  [dbo].[vEmailRecipients] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vEmailRecipients] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vEmailRecipients] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vEmailRecipients] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vEmailRecipients] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vEmailRecipients] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vEmailRecipients] TO [power_user]
GRANT SELECT ON  [dbo].[vEmailRecipients] TO [power_user]
GRANT INSERT ON  [dbo].[vEmailRecipients] TO [power_user]
GRANT DELETE ON  [dbo].[vEmailRecipients] TO [power_user]
GRANT UPDATE ON  [dbo].[vEmailRecipients] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[41] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Person (Ligado-ExtranetSecurity.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 218
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PersonFirmAddressClientSite (Ligado-ExtranetSecurity.dbo)"
            Begin Extent = 
               Top = 6
               Left = 256
               Bottom = 114
               Right = 484
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FirmAddressClientSite (Ligado-ExtranetSecurity.dbo)"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RolePersonFirmAddressClientSite (Ligado-ExtranetSecurity.dbo)"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 287
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Role (Ligado-ExtranetSecurity.dbo)"
            Begin Extent = 
               Top = 114
               Left = 271
               Bottom = 222
               Right = 490
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1245
         Width = 2760
         Width = 3750
         Width = 1500
', 'SCHEMA', N'dbo', 'VIEW', N'vEmailRecipients', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vEmailRecipients', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vEmailRecipients', NULL, NULL
GO
