SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vDETAILS_SettlementAllocation]
AS
SELECT     sa.SettlementAllocationID, sa.ClaimSettlementID, cs.ClaimLawsuitID, sa.CarrierAddressID, ca.CarrierAddress, sa.AllocationAmount, sa.PaymentAmount, 
                      sa.PaymentDate
FROM         dbo.tblSettlementAllocation AS sa INNER JOIN
                      dbo.vCarrierAddress AS ca ON sa.CarrierAddressID = ca.CarrierFirmAddressID INNER JOIN
                      dbo.tblClaimSettlement AS cs ON sa.ClaimSettlementID = cs.ClaimSettlementID

GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementAllocation] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementAllocation] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementAllocation] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_SettlementAllocation] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_SettlementAllocation] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_SettlementAllocation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementAllocation] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementAllocation] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_SettlementAllocation] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_SettlementAllocation] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_SettlementAllocation] TO [power_user]
GO
