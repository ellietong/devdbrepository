SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblBrokerType]
AS
SELECT BrokerTypeID, BrokerType
FROM [KGA-Carrier].dbo.tblBrokerType

GO
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerType] TO [base_user]
GRANT SELECT ON  [dbo].[tblBrokerType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBrokerType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBrokerType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBrokerType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBrokerType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerType] TO [power_user]
GRANT SELECT ON  [dbo].[tblBrokerType] TO [power_user]
GRANT INSERT ON  [dbo].[tblBrokerType] TO [power_user]
GRANT DELETE ON  [dbo].[tblBrokerType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBrokerType] TO [power_user]
GO
