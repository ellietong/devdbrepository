SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vSEARCH_Invoice_old]
AS

SELECT 
                      d.DocumentID, 
					  COALESCE(ClaimantDocumentType, 'LOCAL COUNSEL INVOICE') AS ClaimantDocumentType, 
					  d.ClaimantDocumentTypeID,
					  d.Comments, 
					  d.DocumentTitle, 
					  d.CreatedBy, 
					  d.DateCreated, 
					  d.ModifiedBy, 
                      d.DateModified, 
					  d.Accepted, 
					  d.AcceptedBy, 
					  d.DateAccepted, 
					  di.ApprovedBy, 
					  di.DateApproved, 
					  UPPER(COALESCE(dca.DefenseCounsel, dc.defensecounsel,'')) AS Firm, 
                      di.InvoiceNumber, 
					  di.Period, 
					  di.DatePayable, 
					  feeamount, 
					  disbursementamount, 
                      Adjustment, 
					  di.InvoiceNotes AS AdjustmentNotes, 
					  dis.Status, 
					  '' AS DefenseInvoiceFirm, 
					  MAX(c.SourceDate) AS PaidDate, 
                      c.SourceNumber AS CheckNumber, 
					  di.FirmID AS FirmID,
					  CASE WHEN d.ClaimantDocumentTypeID = 41 THEN COALESCE(ex.FirmDisplayName, '') ELSE '' END AS VendorFirm, 
					  di.VendorFirmID, 
					  max(c.SourceDate) as SourceDate, 
                      InvoiceTotal, 
					  AccountsReceiveablePaymentTotal,
					  COALESCE(d.MatterID, 0) AS MatterID,
					  di.DefenseInvoiceID,
					  dis.StatusID,
					  FileSizeBytes, 
					  UPPER(COALESCE(m.Matter,M2.MATTER, 'Unknown')) AS Matter,
					  dj.DefenseCounselAddressID,
					  dj.PersonID
FROM      
		dbo.tblDefenseInvoice AS di
		 LEFT JOIN  (SELECT d.*, dt.ClaimantDocumentType 
			  FROM dbo.tblDocuments AS d 
					INNER JOIN dbo.tblClaimantDocumentType AS dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID 
					INNER JOIN dbo.tblClaimantDocumentTypeGroup AS cdtg ON dt.ClaimantDocumentTypeID = cdtg.ClaimantDocumentTypeID 
					INNER JOIN dbo.tblClaimantDocumentGroup AS cdg ON cdtg.ClaimantDocumentGroupID = cdtg.ClaimantDocumentGroupID
			  WHERE     (cdtg.ClaimantDocumentGroupID = 2) ) d ON d.documentid = di.DocumentID 
		INNER JOIN dbo.tblDefenseInvoiceStatus AS dis ON di.StatusID = dis.StatusID 
		INNER JOIN (Select DefenseInvoiceID, 
						  SUM(COALESCE(Fees,0) + COALESCE(Costs,0) - COALESCE(Adjustments,0)) AS InvoiceTotal,
						  SUM(COALESCE(Adjustments,0)) AS Adjustment,
						  SUM(COALESCE(Fees,0)) AS Feeamount,
						  SUM(COALESCE(Costs,0)) AS disbursementamount,
						  MAX(MATTERID) AS MATTERID
					FROM dbo.tblDefInvClaimantDetail
					GROUP BY DefenseInvoiceID) dicd on dicd.DefenseInvoiceID = di.DefenseInvoiceID
		INNER JOIN (SELECT DefenseInvoiceID, 
						  SUM(COALESCE(tblAccountsReceivablePayment.Amount,0)) AS AccountsReceiveablePaymentTotal  
					FROM tblDefInvClaimantDetail
						  LEFT JOIN tblOccurrence on tbloccurrence.invoicedetailid = tblDefInvClaimantDetail.InvoiceDetailID
						  LEFT JOIN tblExhaustion ON (tblOccurrence.OccurrenceID = tblExhaustion.OccurrenceID)
						  LEFT JOIN tblAccountsReceivablePayment on (tblExhaustion.ExhaustionID = tblAccountsReceivablePayment.ExhaustionID)
					GROUP BY Defenseinvoiceid)AS ARP ON ARP.DefenseInvoiceID = dicd.DefenseInvoiceID 
		LEFT OUTER JOIN dbo.tblDefenseCounselAddress AS dca ON di.DefenseCounselAddressID = dca.DefenseCounselAddressID
		LEFT OUTER JOIN dbo.tbldefensecounsel as dc on dc.DefenseCounselID = di.FirmID
        LEFT OUTER JOIN dbo.vVendorFirm AS ex ON di.VendorFirmID = ex.FirmID 
        LEFT OUTER JOIN dbo.tblPaymentDefense AS pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID 
		LEFT OUTER JOIN dbo.tblCheck AS c ON pd.CheckID = c.CheckID
		LEFT JOIN tblMatter m ON d.MATTERID = m.MatterID
		LEFT JOIN TBLMATTER M2 ON M2.MATTERID = DICD.MATTERID
		INNER JOIN vDefenseCounselMatters dj ON COALESCE(d.MatterID, DICD.MatterID) = dj.MatterID

--where di.defenseinvoiceid = 16995--16442

GROUP BY d.DocumentID, 
					  ClaimantDocumentType, 
					  d.ClaimantDocumentTypeID,
					  d.Comments, 
					  d.DocumentTitle, 
					  d.CreatedBy, 
					  d.DateCreated, 
					  d.ModifiedBy, 
                      d.DateModified, 
					  d.Accepted, 
					  d.AcceptedBy, 
					  d.DateAccepted, 
					  di.ApprovedBy, 
					  di.DateApproved, 
					  UPPER(coalesce(dca.DefenseCounsel, dc.defensecounsel,'')) , 
                      di.InvoiceNumber, 
					  di.Period, 
					  di.DatePayable,
					  di.InvoiceNotes, 
					  dis.Status,
					  --c.SourceDate, 
                      c.SourceNumber, 
					  di.FirmID,
					    Case when d.ClaimantDocumentTypeID = 41 then coalesce(ex.FirmDisplayName, '') else '' end, 
					  di.VendorFirmID, 
					  --c.SourceDate, 
					  d.MatterID,
					  di.DefenseInvoiceID,
					  dis.StatusID,
					  FileSizeBytes,
					   feeamount, 
					  disbursementamount, 
                      Adjustment, 
					  InvoiceTotal,
					  AccountsReceiveablePaymentTotal,
					  M.Matter,
					  M2.MATTER,
					  dj.DefenseCounselAddressID,
					  dj.PersonID









--GO


GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Invoice_old] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_Invoice_old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Invoice_old] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_Invoice_old] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_Invoice_old] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_Invoice_old] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_Invoice_old] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Invoice_old] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_Invoice_old] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_Invoice_old] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_Invoice_old] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_Invoice_old] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 2
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dt"
            Begin Extent = 
               Top = 6
               Left = 306
               Bottom = 101
               Right = 536
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cdtg"
            Begin Extent = 
               Top = 6
               Left = 574
               Bottom = 135
               Right = 837
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cdg"
            Begin Extent = 
               Top = 6
               Left = 875
               Bottom = 102
               Right = 1112
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "di"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 289
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dis"
            Begin Extent = 
               Top = 138
               Left = 581
               Bottom = 233
               Right = 751
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dicd"
            Begin Extent = 
               Top = 318
               Left = 336
               Bottom = 447
               Right = 550
            End
            DisplayFlags = 280
            TopColumn = 0
      ', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Invoice_old', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'   End
         Begin Table = "mcd"
            Begin Extent = 
               Top = 234
               Left = 579
               Bottom = 363
               Right = 811
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dca"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 531
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dc"
            Begin Extent = 
               Top = 102
               Left = 306
               Bottom = 180
               Right = 475
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ex"
            Begin Extent = 
               Top = 498
               Left = 555
               Bottom = 627
               Right = 725
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pd"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 663
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 594
               Left = 281
               Bottom = 723
               Right = 497
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 30
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Invoice_old', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Invoice_old', NULL, NULL
GO
