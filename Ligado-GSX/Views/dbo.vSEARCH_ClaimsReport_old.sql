SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











CREATE VIEW [dbo].[vSEARCH_ClaimsReport_old]
AS
SELECT     cpi.ClaimantPersonalInfoID, cpi.LastName, cpi.FirstName, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) 
                      END AS SocialSecurityNumber,  cpi.BirthDate AS Birthdate, cpi.DeceasedDate AS DeceasedDate, 
                      c.ClaimID, hd.HighLevelDisease, l.DocketNumber, s.State, j.Jurisdiction, dca.DefenseCounsel AS PrimaryDefenseCounsel, 
                      l.LeadPlaintiffFirstName + ' ' + l.LeadPlaintiffLastName AS LeadPlaintiffName, pa.PlaintiffCounsel, cl.ClaimLawsuitID, cl.IsPrimaryForClaim, cs.ClaimSequence, 
                      cls.ClaimLawsuitStatus, clst.StatusDate AS StatusDate, cl.ActiveTrialDate AS ActiveTrialDate, 
                      COALESCE (l.FileDate, l.ReceivedDate) AS FileDate, l.ServiceDate AS ServiceDate, 
                      clst.ProcessedDate AS ProcessedDate, YEAR(COALESCE (l.FileDate, l.ReceivedDate)) AS YearFiled,
                      settle.SettlementDate AS SettlementDate, '$ ' + CONVERT(varchar(20), settle.SettlementAmount, 1) AS SettlementAmount, 
                      MIN(ps.SourceDate) AS SourceDate, COALESCE (MIN(cp.Occupation), 'Unknown') AS Occupation,
					  MIN(cp.StartDate) AS ProductExposureStart, 
					  FullName, ClaimLawsuitStatusGroupID, l.StateID, c.MatterID, m.Matter, PlaintiffCounselID,dj.DefenseCounselAddressID, l.DefenseCounselAddressID AS DefenseCounselAddressID2, IsMDL,
					  clst.ClaimLawsuitStatusID, l.JurisdictionID, a.AllegationTypeID, at.AllegationType, ProductNotes, hd.HighLevelDiseaseID
FROM         dbo.tblClaim AS c LEFT OUTER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS cl ON cl.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblLawsuit AS l ON l.LawsuitID = cl.LawsuitID LEFT OUTER JOIN
                          (SELECT     ClaimLawsuitStatusTrackingID, ClaimLawsuitID, ClaimLawsuitStatusID, StatusNotes, IsPrimaryStatus, NavStatus, ProcessedDate, StatusDate, 
                                                   SPUsername
                            FROM          dbo.tblClaimLawsuitStatusTracking
                            WHERE      (IsPrimaryStatus = 1)) AS clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatus AS cls ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = c.ClaimSequenceID LEFT OUTER JOIN
                          (SELECT     ClaimDiseaseID, ClaimID, DiseaseID, PrimaryDisease, DiseaseDiagnosisDate, DoctorID, NavDisease, SPUsername
                            FROM          dbo.tblClaimDisease
                            WHERE      (PrimaryDisease = 1)) AS cd ON cd.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblDisease AS d ON d.DiseaseID = cd.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS hd ON hd.HighLevelDiseaseID = d.HighLevelDiseaseID LEFT OUTER JOIN
                      dbo.tblState AS s ON s.StateID = l.StateID LEFT OUTER JOIN
                      dbo.tblJurisdiction AS j ON j.JurisdictionID = l.JurisdictionID LEFT OUTER JOIN
                      dbo.tblPlaintiffCounselAddress AS pa ON pa.PlaintiffCounselAddressID = l.NationalPlaintiffCounselID LEFT OUTER JOIN
                      dbo.tblClaimSettlement AS settle ON settle.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                          (SELECT     pset.ClaimSettlementID, MIN(ch.SourceDate) AS SourceDate
                            FROM          dbo.tblPaymentSettlement AS pset INNER JOIN
                                                   dbo.tblCheck AS ch ON pset.CheckID = ch.CheckID
                            GROUP BY pset.ClaimSettlementID) AS ps ON ps.ClaimSettlementID = settle.ClaimSettlementID LEFT OUTER JOIN
                      dbo.tblAllegation AS a ON a.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                          (SELECT     cemp.ClaimantPersonalInfoID, COALESCE (MIN(o.Occupation), 'Unknown') AS Occupation, MIN(cprod.StartDate) AS StartDate,
										cprod.ProductNotes
                            FROM          dbo.tblClaimantEmployment AS cemp LEFT OUTER JOIN
                                                   dbo.tblClaimantProduct AS cprod ON cemp.ClaimantEmploymentID = cprod.ClaimantEmploymentID LEFT OUTER JOIN
                                                   dbo.tblOccupation AS o ON cemp.OccupationID = o.OccupationID
												   
                            WHERE      (cemp.IsPrimary = 1)
                            GROUP BY cemp.ClaimantPersonalInfoID, ProductNotes) AS cp ON cp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblDefenseJurisdiction AS dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.MatterID
					  INNER JOIN dbo.tblDefenseCounselAddress AS dca ON dj.DefenseCounselAddressID = dca.DefenseCounselAddressID
					  INNER JOIN tblAllegationType at ON a.AllegationTypeID = at.AllegationTypeID
					  INNER JOIN tblMatter m ON m.MatterID = c.MatterID
GROUP BY cpi.ClaimantPersonalInfoID, cpi.LastName, cpi.FirstName, cpi.SocialSecurityNumber, cpi.BirthDate, cpi.DeceasedDate, c.ClaimID, hd.HighLevelDisease, 
                      l.DocketNumber, s.State, j.Jurisdiction, dca.DefenseCounsel, l.LeadPlaintiffFirstName + ' ' + l.LeadPlaintiffLastName, pa.PlaintiffCounsel, cl.ClaimLawsuitID, 
                      cl.IsPrimaryForClaim, cs.ClaimSequence, cls.ClaimLawsuitStatus, clst.StatusDate, cl.ActiveTrialDate, COALESCE (l.FileDate, l.ReceivedDate), l.FileDate, l.ServiceDate, 
                      clst.ProcessedDate, YEAR(COALESCE (l.FileDate, l.ReceivedDate)), settle.SettlementDate, settle.SettlementAmount, clst.ClaimLawsuitStatusID, cp.Occupation, 
                      cp.StartDate, ps.SourceDate, FullName,  ClaimLawsuitStatusGroupID, l.StateID, c.MatterID, m.Matter, PlaintiffCounselID, dj.DefenseCounselAddressID, l.DefenseCounselAddressID, IsMDL,
					  clst.ClaimLawsuitStatusID, l.JurisdictionID, a.AllegationTypeID, at.AllegationType, ProductNotes, hd.HighLevelDiseaseID









GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport_old] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport_old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport_old] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport_old] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_ClaimsReport_old] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_ClaimsReport_old] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_ClaimsReport_old] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport_old] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport_old] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_ClaimsReport_old] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_ClaimsReport_old] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_ClaimsReport_old] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 6
               Left = 303
               Bottom = 114
               Right = 540
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 578
               Bottom = 114
               Right = 790
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 266
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clst"
            Begin Extent = 
               Top = 114
               Left = 304
               Bottom = 222
               Right = 543
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cls"
            Begin Extent = 
               Top = 114
               Left = 581
               Bottom = 222
               Right = 809
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 300
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 0
       ', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimsReport_old', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'  End
         Begin Table = "cd"
            Begin Extent = 
               Top = 222
               Left = 255
               Bottom = 330
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 222
               Left = 494
               Bottom = 330
               Right = 684
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hd"
            Begin Extent = 
               Top = 222
               Left = 722
               Bottom = 300
               Right = 912
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 300
               Left = 38
               Bottom = 393
               Right = 205
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "j"
            Begin Extent = 
               Top = 300
               Left = 722
               Bottom = 378
               Right = 889
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pa"
            Begin Extent = 
               Top = 330
               Left = 243
               Bottom = 438
               Right = 463
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ps"
            Begin Extent = 
               Top = 378
               Left = 779
               Bottom = 456
               Right = 963
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "a"
            Begin Extent = 
               Top = 438
               Left = 38
               Bottom = 546
               Right = 249
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cp"
            Begin Extent = 
               Top = 438
               Left = 287
               Bottom = 531
               Right = 496
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dj"
            Begin Extent = 
               Top = 486
               Left = 534
               Bottom = 594
               Right = 758
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dca"
            Begin Extent = 
               Top = 534
               Left = 287
               Bottom = 642
               Right = 511
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "settle"
            Begin Extent = 
               Top = 6
               Left = 828
               Bottom = 114
               Right = 1068
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimsReport_old', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane3', N'
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimsReport_old', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=3
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimsReport_old', NULL, NULL
GO
