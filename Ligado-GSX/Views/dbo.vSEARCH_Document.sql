SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE VIEW [dbo].[vSEARCH_Document]
AS
SELECT        d.DocumentID, d.DocumentTitle, d.DocumentURL, cdt.ClaimantDocumentType, 0 AS FirmID, '' AS Firm, d.CreatedBy, d.DateCreated, d.AcceptedBy,
                        d.DateAccepted, cdt.ClaimantDocumentTypeID, d.FileSizeBytes, 
						CASE WHEN (COALESCE(Claimantcount.ClaimantCount, 0) > 1 OR COALESCE(Claimantcount.ClaimantCount, 0) = 0) THEN CONVERT(NVARCHAR, COALESCE(Claimantcount.ClaimantCount, 0)) + ' claimants' ELSE CONVERT(NVARCHAR, COALESCE(Claimantcount.ClaimantCount, 0)) + ' claimant' END AS Claimants, 
						CASE WHEN HistoricalDocumentID IS NOT NULL
                        THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS IsHistorical, c.MatterID, d.DocumentSourceID, d.Comments,
                            (SELECT        SUM(locked) AS Locked
                              FROM            dbo.vDOCUMENT_Locked
                              WHERE        (DocumentID = d.DocumentID)) AS Locked,
							  l.StateID, l.JurisdictionID, l.CourtTypeID, 
							  dj.DefenseCounselAddressID AS JurisdictionDefenseCounselAddressID, 
							  cl.DefenseCounselAddressID AS LawsuitDefenseCounselAddressID, ClaimantDocumentGroupID, 
							CASE WHEN mcd.ClaimLawsuitID IS NULL THEN 'All Matter Access' ELSE Matter END AS Matter, mcd.ClaimLawsuitID
FROM            dbo.tblDocuments AS d INNER JOIN
                        dbo.tblClaimantDocumentType AS cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID INNER JOIN
                        dbo.tblMultiClaimantDocument AS mcd ON d.DocumentID = mcd.DocumentID INNER JOIN
                        dbo.tblClaim AS c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID INNER JOIN
                        dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID AND cl.ClaimLawsuitID = COALESCE(mcd.ClaimLawsuitID, cl.ClaimLawsuitID) INNER JOIN
                        dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID INNER JOIN
                        dbo.tblDefenseCounselAddress AS dca ON cl.DefenseCounselAddressID = dca.DefenseCounselAddressID INNER JOIN
                        dbo.tblDefenseJurisdiction AS dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.MatterID 
						INNER JOIN		dbo.tblMatter AS m ON c.MatterID = m.MatterID 
						INNER JOIN (SELECT 
										DocumentID, 
										COUNT(DISTINCT ClaimantPersonalInfoID) AS ClaimantCount 
										FROM tblMultiClaimantDocument multi 
										GROUP BY DocumentID) AS ClaimantCount ON ClaimantCount.DocumentID = d.DocumentID
WHERE (COALESCE(UploadCompleted, 0) = 1 OR CreatedBy = 'Historical'  OR d.ComplaintID IS NOT NULL)
GROUP BY d.DocumentID, d.DocumentTitle, d.DocumentURL, cdt.ClaimantDocumentType, d.CreatedBy, d.DateCreated, d.AcceptedBy, d.DateAccepted,
                        cdt.ClaimantDocumentTypeID, CASE WHEN HistoricalDocumentID IS NOT NULL THEN CONVERT(bit, 1) ELSE CONVERT(bit, 0) END, c.MatterID,
                        d.DocumentSourceID, d.Comments,l.StateID, l.JurisdictionID, l.CourtTypeID, dj.DefenseCounselAddressID, l.DefenseCounselAddressID, ClaimantDocumentGroupID, d.FileSizeBytes, 
							CASE WHEN mcd.ClaimLawsuitID IS NULL THEN 'All Matter Access' ELSE Matter END, mcd.ClaimLawsuitID,
							cl.DefenseCounselAddressID, dj.DefenseCounselAddressID, CASE WHEN (COALESCE(Claimantcount.ClaimantCount, 0) > 1 OR COALESCE(Claimantcount.ClaimantCount, 0) = 0) THEN CONVERT(nvarchar, COALESCE(Claimantcount.ClaimantCount, 0)) + ' claimants' ELSE CONVERT(nvarchar, COALESCE(Claimantcount.ClaimantCount, 0)) + ' claimant' END







GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Document] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_Document] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Document] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_Document] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_Document] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_Document] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_Document] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Document] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_Document] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_Document] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_Document] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_Document] TO [power_user]
GO
