SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vDETAILS_Lawsuits]
AS
SELECT DISTINCT cl.LawsuitID, l.DocketNumber, cl.IsPrimaryForClaim, s.State, 
cl.ClaimLawsuitID, cl.ClaimID, at.AllegationType AS Allegation, CaseCaption, CourtType, l.CourtTypeID,
FileDate, a.ServiceDate, a.ReceivedDate, l.StateID, l.JurisdictionID, Jurisdiction,
l.PlaintiffCounselAddressID, pca.PlaintiffCounsel,
npca.PlaintiffCounsel AS NationalPlaintiffCounsel,
  l.NationalPlaintiffCounselID,
   cl.DefenseCounselAddressID, DefenseCounsel, ActiveTrialDate, dj.DefenseCounselAddressID AS 'DJDefenseCounselAddressID'

FROM            dbo.tblClaimLawsuit AS cl 
				INNER JOIN (SELECT ClaimID, MAtterID FROM tblCLaim) c ON cl.ClaimID = c.ClaimID
				INNER JOIN
                         dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                         dbo.tblState AS s ON s.StateID = l.StateID LEFT OUTER JOIN
                         dbo.tblAllegation AS a ON a.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblAllegationType AS at ON at.AllegationTypeID = a.AllegationTypeID LEFT JOIN
						 tblCourtType ct ON l.CourtTypeID = ct.CourtTypeID LEFT JOIN
						 tblJurisdiction j ON l.JurisdictionID = j.JurisdictionID LEFT JOIN 
						 tblDefenseCounselAddress dca ON cl.DefenseCounselAddressID = dca.DefenseCounselAddressID
						 LEFT JOIN tblPlaintiffCounselAddress npca on l.NationalPlaintiffCounselID = npca.PlaintiffCounselAddressID
						 LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
						 INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.MatterID





GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_Lawsuits] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_Lawsuits] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_Lawsuits] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_Lawsuits] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_Lawsuits] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_Lawsuits] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_Lawsuits] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_Lawsuits] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_Lawsuits] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_Lawsuits] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_Lawsuits] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_Lawsuits] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 6
               Left = 298
               Bottom = 135
               Right = 537
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 783
               Bottom = 118
               Right = 953
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "a"
            Begin Extent = 
               Top = 102
               Left = 575
               Bottom = 210
               Right = 770
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "at"
            Begin Extent = 
               Top = 120
               Left = 808
               Bottom = 198
               Right = 970
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_Lawsuits', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_Lawsuits', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_Lawsuits', NULL, NULL
GO
