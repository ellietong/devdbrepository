SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblSettlementDocumentation_old]
AS
SELECT     spd.SetPymtID, SUM(CASE WHEN DocCategoryID = 1 THEN 1 ELSE 0 END) AS ClaimantIDed, SUM(CASE WHEN DocCategoryID = 2 THEN 1 ELSE 0 END) 
                      AS DxMedicals, SUM(CASE WHEN DocCategoryID = 3 THEN 1 ELSE 0 END) AS DODCert, SUM(CASE WHEN DocCategoryID = 4 THEN 1 ELSE 0 END) AS EstatePapers, 
                      SUM(CASE WHEN DocCategoryID = 5 THEN 1 ELSE 0 END) AS ProductID, SUM(CASE WHEN DocCategoryID = 6 THEN 1 ELSE 0 END) AS EmployExpoHist, 
                      SUM(CASE WHEN DocCategoryID = 7 THEN 1 ELSE 0 END) AS CompServ, SUM(CASE WHEN DocCategoryID = 8 THEN 1 ELSE 0 END) AS SettlementConfirmation, 
                      spd.MMSEA, spd.MedicarePlaintiff, spd.MedicarePlaintiffCounsel
FROM         dbo.tblSettleDocs AS sd INNER JOIN
                      dbo.tblSettlePaymentDocumentation AS spd ON sd.SetPymtID = spd.SetPymtID
GROUP BY spd.SetPymtID, spd.MMSEA, spd.MedicarePlaintiff, spd.MedicarePlaintiffCounsel

GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementDocumentation_old] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementDocumentation_old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementDocumentation_old] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSettlementDocumentation_old] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSettlementDocumentation_old] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSettlementDocumentation_old] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSettlementDocumentation_old] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementDocumentation_old] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementDocumentation_old] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementDocumentation_old] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementDocumentation_old] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementDocumentation_old] TO [power_user]
GO
