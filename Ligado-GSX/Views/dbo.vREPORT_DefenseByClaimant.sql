SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vREPORT_DefenseByClaimant]
AS
SELECT     CAST(d.DocumentID AS varchar(50)) AS DocumentId, d.Comments, d.DocumentURL, d.DocumentTitle, d.CreatedBy, d.DateCreated, d.ModifiedBy, d.DateModified, 
                      d.Accepted, d.AcceptedBy, d.DateAccepted, di.ApprovedBy, di.DateApproved, dca.DefenseCounsel AS Firm, di.InvoiceNumber, di.Period, di.DatePayable, 
                      di.InvoiceNotes AS AdjustmentNotes, dis.Status, '' AS DefenseInvoiceFirm, c.SourceDate AS PaidDate, c.SourceNumber, 
                      dca.DefenseCounselID AS FirmID, COALESCE (ex.VendorFirm, '') AS VendorFirm, c.SourceDate, SUM(CASE WHEN COALESCE (CheckFundingSourceID, 1) = 3 AND 
                      COALESCE (sourceamount, 0) < 0 THEN (dicd.fees * - 1) ELSE COALESCE (dicd.fees, 0) END) + SUM(CASE WHEN COALESCE (CheckFundingSourceID, 1) = 3 AND 
                      COALESCE (sourceamount, 0) < 0 THEN (dicd.costs * - 1) ELSE COALESCE (dicd.costs, 0) END) - SUM(CASE WHEN COALESCE (CheckFundingSourceID, 1) = 3 AND 
                      COALESCE (sourceamount, 0) < 0 THEN (dicd.adjustments * - 1) ELSE COALESCE (dicd.adjustments, 0) END) AS InvoiceTotal, dicd.ClaimantPersonalInfoID, 
                      cpi.FullName AS ClaimantName, ex.VendorFirmID, dis.StatusID, di.DefenseCounselAddressID, di.DefenseInvoiceID
FROM         dbo.tblDocuments AS d LEFT OUTER JOIN
                      dbo.tblDefenseInvoice AS di ON d.DocumentID = di.DocumentID LEFT OUTER JOIN
                      dbo.tblDefenseInvoiceStatus AS dis ON di.StatusID = dis.StatusID LEFT OUTER JOIN
                      dbo.tblVendorFirm AS ex ON di.VendorFirmID = ex.VendorFirmID LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress AS dca ON di.DefenseCounselAddressID = dca.DefenseCounselAddressID LEFT OUTER JOIN
                      dbo.tblPaymentDefense AS pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID LEFT OUTER JOIN
                      dbo.tblDefInvClaimantDetail AS dicd ON dicd.DefenseInvoiceID = di.DefenseInvoiceID LEFT OUTER JOIN
                          (SELECT     CheckID, SourceNumber, SourceDate, SourceAmount, MatterID, CheckFundingTypeID, CheckFundingSourceID, NavClaimResolutionKey, CheckPDF
                            FROM          dbo.tblCheck
                            WHERE      (CheckFundingTypeID IN (1, 3))) AS c ON pd.CheckID = c.CheckID AND dicd.MatterID = c.MatterID LEFT OUTER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = dicd.ClaimantPersonalInfoID
WHERE     (d.ClaimantDocumentTypeID IN (40, 41))
GROUP BY CAST(d.DocumentID AS varchar(50)), d.Comments, d.DocumentURL, d.DocumentTitle, d.CreatedBy, d.DateCreated, d.ModifiedBy, d.DateModified, d.Accepted, 
                      d.AcceptedBy, d.DateAccepted, di.ApprovedBy, di.DateApproved, dca.DefenseCounsel, di.InvoiceNumber, di.Period, di.DatePayable, di.InvoiceNotes, dis.Status, 
                      c.SourceDate, c.SourceNumber, dca.DefenseCounselID, COALESCE (ex.VendorFirm, ''), c.SourceDate, dicd.ClaimantPersonalInfoID, FullName,
                       ex.VendorFirmID, dis.StatusID, di.DefenseCounselAddressID, di.DefenseInvoiceID
HAVING      (SUM(COALESCE (dicd.Fees, 0) + COALESCE (dicd.Costs, 0) - COALESCE (dicd.Adjustments, 0)) <> 0)




GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseByClaimant] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_DefenseByClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseByClaimant] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_DefenseByClaimant] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_DefenseByClaimant] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_DefenseByClaimant] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_DefenseByClaimant] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseByClaimant] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_DefenseByClaimant] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_DefenseByClaimant] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_DefenseByClaimant] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_DefenseByClaimant] TO [power_user]
GO
