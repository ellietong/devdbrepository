SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vCarrierWithGroup]
AS
SELECT CarrierID, CarrierName, c.CarrierGroupID, CarrierGroupName
FROM [ClientSQL].[KGA-Carrier].dbo.tblCarrier c
	LEFT JOIN [ClientSQL].[KGA-Carrier].dbo.tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID



GO
GRANT VIEW DEFINITION ON  [dbo].[vCarrierWithGroup] TO [base_user]
GRANT SELECT ON  [dbo].[vCarrierWithGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vCarrierWithGroup] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vCarrierWithGroup] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vCarrierWithGroup] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vCarrierWithGroup] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vCarrierWithGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vCarrierWithGroup] TO [power_user]
GRANT SELECT ON  [dbo].[vCarrierWithGroup] TO [power_user]
GRANT INSERT ON  [dbo].[vCarrierWithGroup] TO [power_user]
GRANT DELETE ON  [dbo].[vCarrierWithGroup] TO [power_user]
GRANT UPDATE ON  [dbo].[vCarrierWithGroup] TO [power_user]
GO
