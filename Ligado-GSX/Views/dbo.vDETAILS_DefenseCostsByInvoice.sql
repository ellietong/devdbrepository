SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vDETAILS_DefenseCostsByInvoice]
AS
SELECT     cpi.ClaimantPersonalInfoID, COALESCE (di.VendorFirmID, di.DefenseCounselAddressID) AS DefenseCounselAddressID, di.InvoiceNumber, 
                      SUM(dicd.AdjustedTotal) AS DefenseTotal, CASE WHEN IsFullyPaid = 1 THEN 'Paid' ELSE [Status] END AS 'Status', c.SourceDate AS PaidDate, dicd.DefenseInvoiceID, 
                      COALESCE(di.VendorFirmID, 0) AS VendorFirmID, di.FirmID, di.DocumentID, cl.ClaimLawsuitID
FROM         dbo.tblClaimantPersonalInfo AS cpi 
Join tblclaim cm on cm.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
join tblclaimlawsuit cl on cm.claimID = cl.ClaimID
INNER JOIN dbo.tblDefInvClaimantDetail AS dicd ON cl.ClaimLawsuitID = dicd.ClaimLawsuitID 
INNER JOIN dbo.tblDefenseInvoice AS di ON dicd.DefenseInvoiceID = di.DefenseInvoiceID 
INNER JOIN dbo.tblDefenseInvoiceStatus AS ds ON di.StatusID = ds.StatusID 
LEFT OUTER JOIN dbo.tblPaymentDefense AS pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID 
LEFT OUTER JOIN dbo.tblCheck AS c ON pd.CheckID = c.CheckID
WHERE      (DI.StatusID IN (1,2))
GROUP BY cpi.ClaimantPersonalInfoID, di.VendorFirmID, di.DefenseCounselAddressID, di.InvoiceNumber, di.FirmID, di.VendorFirmID, 
                      CASE WHEN IsFullyPaid = 1 THEN 'Paid' ELSE [Status] END, c.SourceDate, dicd.DefenseInvoiceID, di.DocumentID, cl.ClaimLawsuitID





GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_DefenseCostsByInvoice] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = -672
         Left = 0
      End
      Begin Tables = 
         Begin Table = "cpi"
            Begin Extent = 
               Top = 678
               Left = 38
               Bottom = 786
               Right = 275
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dicd"
            Begin Extent = 
               Top = 678
               Left = 313
               Bottom = 786
               Right = 522
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "di"
            Begin Extent = 
               Top = 678
               Left = 560
               Bottom = 786
               Right = 803
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ds"
            Begin Extent = 
               Top = 678
               Left = 841
               Bottom = 756
               Right = 1008
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pd"
            Begin Extent = 
               Top = 678
               Left = 1046
               Bottom = 786
               Right = 1247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 786
               Left = 38
               Bottom = 894
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
  ', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_DefenseCostsByInvoice', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'       Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_DefenseCostsByInvoice', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_DefenseCostsByInvoice', NULL, NULL
GO
