SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vDETAILS_EventTracking]
AS
SELECT     tracking.EventTrackingID, tracking.ClaimLawsuitID, tracking.EventTypeID, et.EventType, tracking.EventDate, tracking.EventDescription, tracking.Username
FROM         dbo.tblEventTracking AS tracking INNER JOIN
                      dbo.tblEventType AS et ON tracking.EventTypeID = et.EventTypeID
GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_EventTracking] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_EventTracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_EventTracking] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_EventTracking] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_EventTracking] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_EventTracking] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_EventTracking] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_EventTracking] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_EventTracking] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_EventTracking] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_EventTracking] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_EventTracking] TO [power_user]
GO
