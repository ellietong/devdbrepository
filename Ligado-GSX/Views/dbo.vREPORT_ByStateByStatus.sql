SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vREPORT_ByStateByStatus]
AS

select distinct
tcl.ClaimLawsuitID
, tcl.ClaimLawsuitTypeID
, tclst.ClaimLawsuitStatusID
, tc.MatterID
, tl.StateID
, ts.FullNameState
, CASE WHEN tclst.ClaimLawsuitStatusID = 41 THEN 1 ELSE 0 END AS STATUS_LITIGATION
, CASE WHEN tclst.ClaimLawsuitStatusID = 42 THEN 1 ELSE 0 END AS STATUS_INVENTORY
, CASE WHEN tclst.ClaimLawsuitStatusID = 29 THEN 1 ELSE 0 END AS STATUS_INACTIVE
, CASE WHEN tclst.ClaimLawsuitStatusID = 43 THEN 1 ELSE 0 END AS STATUS_NOSERVE
, CASE WHEN tclst.ClaimLawsuitStatusID IN (15, 16, 27) THEN 1 ELSE 0 END AS STATUS_DISMISSED
, CASE WHEN tclst.ClaimLawsuitStatusID IN (23, 25) THEN 1 ELSE 0 END AS STATUS_SETTLEMENT
, CASE WHEN tclst.ClaimLawsuitStatusID = 44 THEN 1 ELSE 0 END AS STATUS_WITHDRAWN
, 1 As TOTAL
 from tblClaimLawsuit tcl 
 inner join (select lawsuitid, stateid from tbllawsuit) tl on tcl.LawsuitID = tl.LawsuitID
 inner join (select claimid, matterid from tblClaim) tc on tcl.ClaimID = tc.ClaimID
 inner join (select claimlawsuitid, claimlawsuitstatusid from tblClaimLawsuitStatusTracking where isprimarystatus = 1) tclst on tcl.ClaimLawsuitID = tclst.ClaimLawsuitID
 inner join (select stateid, fullnamestate from tblstate) ts on tl.StateID = ts.StateID
GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_ByStateByStatus] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_ByStateByStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_ByStateByStatus] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_ByStateByStatus] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_ByStateByStatus] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_ByStateByStatus] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_ByStateByStatus] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_ByStateByStatus] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_ByStateByStatus] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_ByStateByStatus] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_ByStateByStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_ByStateByStatus] TO [power_user]
GO
