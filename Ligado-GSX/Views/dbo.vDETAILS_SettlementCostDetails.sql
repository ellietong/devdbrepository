SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vDETAILS_SettlementCostDetails]
AS
SELECT		CL.ClaimLawsuitID,
			rt.ReleaseType, 
			cs.SettlementDueDate, 
			cs.SettlementDate, 
			cs.SettlementFundsRequestDate,
			cs.SettlementAmount, 
			cs.SettlementGroupID, 
			cs.PaidDate,
			COALESCE(sg.SettlementGroup, 'UNKNOWN') AS SettlementGroup, 
            cs.ClaimSettlementID,
			cs.SPUsername
FROM        dbo.tblClaimLawsuit AS CL INNER JOIN
            dbo.tblClaimSettlement AS cs ON CL.ClaimLawsuitID = cs.ClaimLawsuitID LEFT OUTER JOIN
            dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = cs.ReleaseTypeID LEFT JOIN
            dbo.tblClaimSettlementGroup AS sg ON cs.SettlementGroupID = sg.SettlementGroupID
WHERE     (CL.IsPrimaryForClaim = 1)




GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementCostDetails] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementCostDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementCostDetails] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementCostDetails] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_SettlementCostDetails] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_SettlementCostDetails] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_SettlementCostDetails] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementCostDetails] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementCostDetails] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_SettlementCostDetails] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_SettlementCostDetails] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_SettlementCostDetails] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CL"
            Begin Extent = 
               Top = 6
               Left = 287
               Bottom = 114
               Right = 483
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 6
               Left = 521
               Bottom = 114
               Right = 745
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rt"
            Begin Extent = 
               Top = 6
               Left = 783
               Bottom = 84
               Right = 936
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sg"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 99
               Right = 210
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SettlementCostDetails', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SettlementCostDetails', NULL, NULL
GO
