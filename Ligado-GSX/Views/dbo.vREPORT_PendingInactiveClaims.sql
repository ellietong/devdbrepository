SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE VIEW [dbo].[vREPORT_PendingInactiveClaims]
AS
	SELECT distinct
	cl.ClaimLawsuitID,
	pca.PlaintiffCounsel, 
	s.FullNameState AS State,
	HighLevelDisease,
	m.Matter,
	c.MatterID,
	cl.DefenseCounselAddressID,
	dj.DefenseCounselAddressID AS DJDefenseCounselAddressID,
	ClaimLawsuitTypeID
	from tblClaim c
	inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
	inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
	inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
	inner join tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
	INNER JOIN tblState s ON l.StateID = s.StateID
	INNER JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimID
	INNER JOIN tblDisease d ON cd.DiseaseID = d.DiseaseID
	INNER JOIN tblHighLevelDisease hld ON d.HighLevelDiseaseID = hld.HighLevelDiseaseID
	INNER JOIN tblMatter m ON c.MatterID = m.MatterID
	INNER JOIN		tblDefenseJurisdiction dj ON l.StateID = dj.StateID 
			AND l.JurisdictionID = dj.JurisdictionID 
			AND c.MatterID = dj.MatterID
	where	IsPrimaryStatus=1 
			AND IsPrimaryForClaim = 1
			AND ClaimLawsuitStatusID IN (40) 
			AND PrimaryDisease = 1




GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_PendingInactiveClaims] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_PendingInactiveClaims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_PendingInactiveClaims] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_PendingInactiveClaims] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_PendingInactiveClaims] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_PendingInactiveClaims] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_PendingInactiveClaims] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_PendingInactiveClaims] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_PendingInactiveClaims] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_PendingInactiveClaims] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_PendingInactiveClaims] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_PendingInactiveClaims] TO [power_user]
GO
