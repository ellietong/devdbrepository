CREATE ROLE [Occurrence_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Occurrence_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'Occurrence_User', N'cornellt'
GO
EXEC sp_addrolemember N'Occurrence_User', N'jacksonc'
GO
EXEC sp_addrolemember N'Occurrence_User', N'kanga'
GO
EXEC sp_addrolemember N'Occurrence_User', N'labellj'
GO
EXEC sp_addrolemember N'Occurrence_User', N'monahanc'
GO
EXEC sp_addrolemember N'Occurrence_User', N'onyiukeo'
GO
