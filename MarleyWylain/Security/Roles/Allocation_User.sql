CREATE ROLE [Allocation_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Allocation_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'Allocation_User', N'cornellt'
GO
EXEC sp_addrolemember N'Allocation_User', N'jacksonc'
GO
EXEC sp_addrolemember N'Allocation_User', N'kanga'
GO
EXEC sp_addrolemember N'Allocation_User', N'labellj'
GO
EXEC sp_addrolemember N'Allocation_User', N'monahanc'
GO
EXEC sp_addrolemember N'Allocation_User', N'onyiukeo'
GO
