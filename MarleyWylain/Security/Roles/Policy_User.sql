CREATE ROLE [Policy_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Policy_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'Policy_User', N'jacksonc'
GO
EXEC sp_addrolemember N'Policy_User', N'kanga'
GO
EXEC sp_addrolemember N'Policy_User', N'labellj'
GO
EXEC sp_addrolemember N'Policy_User', N'monahanc'
GO
EXEC sp_addrolemember N'Policy_User', N'onyiukeo'
GO
