CREATE ROLE [CheckWriter_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'CheckWriter_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'colek'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'mollardj'
GO
