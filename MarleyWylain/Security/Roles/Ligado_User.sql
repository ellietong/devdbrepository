CREATE ROLE [Ligado_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Ligado_User', N'LigadoMarley'
GO
EXEC sp_addrolemember N'Ligado_User', N'petryszaka'
GO
EXEC sp_addrolemember N'Ligado_User', N'spuser'
GO
