CREATE ROLE [Billing_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Billing_User', N'jacksonc'
GO
EXEC sp_addrolemember N'Billing_User', N'labellj'
GO
EXEC sp_addrolemember N'Billing_User', N'onyiukeo'
GO
