CREATE ROLE [Claims_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Claims_User', N'blairo'
GO
EXEC sp_addrolemember N'Claims_User', N'casillaw'
GO
EXEC sp_addrolemember N'Claims_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'Claims_User', N'colek'
GO
EXEC sp_addrolemember N'Claims_User', N'cornellt'
GO
EXEC sp_addrolemember N'Claims_User', N'eastlacka'
GO
EXEC sp_addrolemember N'Claims_User', N'hagep'
GO
EXEC sp_addrolemember N'Claims_User', N'hughesr'
GO
EXEC sp_addrolemember N'Claims_User', N'husainm'
GO
EXEC sp_addrolemember N'Claims_User', N'jacksonc'
GO
EXEC sp_addrolemember N'Claims_User', N'kanga'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\carthyl'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\hashemik'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\jacobsb'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\marshc'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\ohaverk'
GO
EXEC sp_addrolemember N'Claims_User', N'KCICLLC\pikeg'
GO
EXEC sp_addrolemember N'Claims_User', N'khanm'
GO
EXEC sp_addrolemember N'Claims_User', N'labellj'
GO
EXEC sp_addrolemember N'Claims_User', N'mollardj'
GO
EXEC sp_addrolemember N'Claims_User', N'monahanc'
GO
EXEC sp_addrolemember N'Claims_User', N'onyiukeo'
GO
EXEC sp_addrolemember N'Claims_User', N'owenss'
GO
EXEC sp_addrolemember N'Claims_User', N'potterm'
GO
EXEC sp_addrolemember N'Claims_User', N'zongc'
GO
