SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE FUNCTION [prossc].[fnLatestDANAExposure]
(@ClaimantPersonalInfoID int)

RETURNS datetime
AS
BEGIN

DECLARE @LatestExposure Date
set @LatestExposure =( select MAX(COALESCE(EndDate,'12/4/1980')) as LatestExposureDate
from tblClaimantEmployment ce
left join tblclaimantproduct cp on ce.claimantemploymentid=cp.claimantemploymentid
where ClaimantPersonalInfoID=@ClaimantPersonalInfoID)

Return convert(nvarchar(10),@LatestExposure,101)

End
GO
