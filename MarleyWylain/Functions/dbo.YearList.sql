SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[YearList]
(
	@Start int = 1999
)
RETURNS 
@Temp TABLE 
(
	[Year] int
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	WHILE @start <= YEAR(GETDATE())
	BEGIN
		INSERT INTO @Temp
		SELECT @start
		SET @start = @start + 1
	END
	RETURN
END

GO
