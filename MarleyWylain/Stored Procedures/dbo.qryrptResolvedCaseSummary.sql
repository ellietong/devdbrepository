SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptResolvedCaseSummary] (@ResolvedDateStart  nvarchar(250), @ResolvedDateEnd nvarchar(250), @MatterID int) 
AS
SET NOCOUNT ON

--DECLARE @ResolvedDateStart datetime
--SET @ResolvedDateStart = '1/1/1900'
--DECLARE @ResolvedDateEnd datetime
--SET @ResolvedDateEnd = '1/1/2012'
--declare @matterid int
--set @matterid = null

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @ResolvedSummary TABLE(Matter nvarchar(250),
							ClaimNumber int, 
							   SettlementAmount money,
							   SettlementAverage money,
							   ResolvedDate datetime,
							   Meso_Count int,
							   Meso_SettlementAmount money,
							   Meso_Avg money,
							   LungCancer_Count int,
							   LungCancer_SettlementAmount money,
							   LungCancer_Avg money,
							   OtherCancer_Count int,
							   OtherCancer_SettlementAmount money,
							   OtherCancer_Avg money,
							   NonMalig_Count int,
							   NonMalig_SettlementAmount money,
							   NonMalig_Avg money,
							   Other_Count int,
							   Other_SettlementAmount money,
							   Other_Avg money,
							   NoDisease_Count int,
							   NoDisease_SettlementAmount money,
							   NoDisease_Avg money,
							   Null_Count int,
							   Null_SettlementAmount money,
							   Null_Avg money)
INSERT INTO @ResolvedSummary 
	SELECT Matter,
		COUNT(c.ClaimID) AS ClaimantNumber,
		COALESCE(SUM(SettlementAmount),0) AS SettlementAmount,
		COALESCE(SUM(SettlementAmount),0) / COUNT(c.ClaimID) as SettlementAverage, 
		CONVERT(datetime,(CONVERT(nvarchar, MONTH(COALESCE(SettlementDate,statusdate))) + '/1/' + CONVERT(nvarchar,YEAR(COALESCE(SettlementDate,statusdate, '1/1/1900'))))) AS ResolvedDate,

		SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN 1 ELSE 0 END) as Meso_Count,	
		SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN SettlementAmount ELSE 0 END) as Meso_SettlementAmount,	
		CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN 1 ELSE 0 END) = 0 THEN 0 
			 ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = (1) THEN SettlementAmount ELSE 0 END)/ 
				  SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN 1 ELSE 0 END) END  as Meso_Avg,
				
		SUM(CASE WHEN HLD.HighLevelDiseaseID = 2 THEN 1 ELSE 0 END) as LungCancer_Count,	
		SUM(CASE WHEN HLD.HighLevelDiseaseID = 2 THEN SettlementAmount ELSE 0 END) as LungCancer_SettlementAmount,	
		CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = (2) THEN 1 ELSE 0 END) = 0 THEN 0 
			 ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = (2) THEN SettlementAmount ELSE 0 END)/ 
				  SUM(CASE WHEN HLD.HighLevelDiseaseID = 2 THEN 1 ELSE 0 END) END  as LungCancer_Avg,
				
		SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN 1 ELSE 0 END) as OtherCancer_Count,	
		SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN SettlementAmount ELSE 0 END) as OtherCancer_SettlementAmount,	
		CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN 1 ELSE 0 END) = 0 THEN 0 
			 ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN SettlementAmount ELSE 0 END)/ 
				  SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN 1 ELSE 0 END) END  as OtherCancer_Avg,
				
		SUM(CASE WHEN HLD.HighLevelDiseaseID in (4,7) THEN 1 ELSE 0 END) as NonMalig_Count,	
		SUM(CASE WHEN HLD.HighLevelDiseaseID in (4,7) THEN SettlementAmount ELSE 0 END) as NonMalig_SettlementAmount,	
		CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID in (4,7) THEN 1 ELSE 0 END) = 0 THEN 0 
			 ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID in (4,7)THEN SettlementAmount ELSE 0 END)/ 
				  SUM(CASE WHEN HLD.HighLevelDiseaseID in (4,7) THEN 1 ELSE 0 END) END  as NonMalig_Avg,
				
		SUM(CASE WHEN HLD.HighLevelDiseaseID = 5 THEN 1 ELSE 0 END) as Other_Count,	
		SUM(CASE WHEN HLD.HighLevelDiseaseID = 5 THEN SettlementAmount ELSE 0 END) as Other_SettlementAmount,	
		CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = 5 THEN 1 ELSE 0 END) = 0 THEN 0 
			 ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = 5 THEN SettlementAmount ELSE 0 END)/ 
				  SUM(CASE WHEN HLD.HighLevelDiseaseID = 5 THEN 1 ELSE 0 END) END  as Other_Avg,
				
		SUM(CASE WHEN HLD.HighLevelDiseaseID = 6 THEN 1 ELSE 0 END) as NoDisease_Count,	
		SUM(CASE WHEN HLD.HighLevelDiseaseID = 6 THEN SettlementAmount ELSE 0 END) as NoDisease_SettlementAmount,	
		CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = 6 THEN 1 ELSE 0 END) = 0 THEN 0 
			 ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = 6 THEN SettlementAmount ELSE 0 END)/ 
				  SUM(CASE WHEN HLD.HighLevelDiseaseID = 6 THEN 1 ELSE 0 END) END  as NoDisease_Avg,
				
		SUM(CASE WHEN HLD.HighLevelDiseaseID is null THEN 1 ELSE 0 END) as Null_Count,				
		SUM(CASE WHEN HLD.HighLevelDiseaseID  is null THEN SettlementAmount ELSE 0 END) as Null_SettlementAmount,	
		CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID  is null THEN 1 ELSE 0 END) = 0 THEN 0 
			 ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID is null THEN SettlementAmount ELSE 0 END)/ 
				  SUM(CASE WHEN HLD.HighLevelDiseaseID is null THEN 1 ELSE 0 END) END  as Null_Avg		
	FROM tblClaimantPersonalInfo i 
		LEFT JOIN tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease 
				   From tblClaimDisease CD 
				   Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
		LEFT JOIN tblClaimLawsuit l on l.claimID = c.claimID
		LEFT JOIN tblLawsuit law on l.lawsuitid = law.lawsuitid
		LEFT JOIN tblCLaimantSettlement s on s.claimlawsuitID = l.claimlawsuitID
		LEFT JOIN tblDisease D on CD.DiseaseID = D.DiseaseID
		LEFT JOIN tblHighLevelDisease HLD on HLD.HighLevelDiseaseID=D.HighLevelDiseaseID
		LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=l.ClaimLawsuitID
		--LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		Left JOIN tblMatter m on m.MatterID=c.MatterID
	WHERE IsPrimaryForClaim = 1 
		AND IsPrimaryStatus=1
		AND CONVERT(datetime,(CONVERT(nvarchar, MONTH(COALESCE(SettlementDate,statusdate)))+'/1/'+ CONVERT(nvarchar,YEAR(COALESCE(SettlementDate,statusdate, '1/1/1900'))))) >= @resolvedDateStart 
		AND CONVERT(datetime,(CONVERT(nvarchar, MONTH(coalesce(SettlementDate,statusdate)))+'/1/'+ CONVERT(nvarchar,YEAR(COALESCE(SettlementDate,statusdate, '1/1/1900'))))) <= @resolvedDateEnd
		AND clst.ClaimLawsuitStatusID IN (15,16,17,23,24,25,27,28,30)
		and m.MatterID >= COALESCE(@MatterID, 0) AND m.MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
	GROUP BY Matter, CONVERT(datetime,(CONVERT(nvarchar, MONTH(COALESCE(SettlementDate,statusdate)))+'/1/'+ CONVERT(nvarchar,YEAR(COALESCE(SettlementDate,statusdate, '1/1/1900')))))

SELECT Matter,
	COALESCE(ClaimNumber,0) AS ClaimNumber, 
	COALESCE(resolveddate, 0) AS resolvedDate,
	YEAR(COALESCE(resolveddate, 0)) AS resolvedYear,
	MONTH(COALESCE(resolveddate, 0)) AS resolvedMonth,
	COALESCE(SettlementAmount, 0) AS SettlementAmount,
	COALESCE(SettlementAverage, 0) AS SettlementAverage,
	COALESCE(Meso_Count, 0) AS Meso_Count,
	COALESCE(Meso_SettlementAmount, 0) AS Meso_SettlementAmount,
	COALESCE(Meso_Avg, 0) AS Meso_Avg,
	COALESCE(LungCancer_Count, 0) AS LungCancer_Count,
	COALESCE(LungCancer_SettlementAmount, 0) AS LungCancer_SettlementAmount,
	COALESCE(LungCancer_Avg, 0) AS LungCancer_Avg,
	COALESCE(OtherCancer_Count, 0) AS OtherCancer_Count,
	COALESCE(OtherCancer_SettlementAmount, 0) AS OtherCancer_SettlementAmount,
	COALESCE(OtherCancer_Avg, 0) AS OtherCancer_Avg,
	COALESCE(NonMalig_Count ,0) AS NonMalig_Count,
	COALESCE(NonMalig_SettlementAmount, 0) AS NonMalig_SettlementAmount,
	COALESCE(NonMalig_Avg, 0) AS NonMalig_Avg,
	COALESCE(Other_Count, 0) + COALESCE(NoDisease_Count, 0) + COALESCE(Null_Count, 0) AS Other_Count,
	COALESCE(Other_SettlementAmount, 0) + COALESCE(NoDisease_SettlementAmount, 0) + COALESCE(Null_SettlementAmount, 0) AS Other_SettlementAmount,
	COALESCE(Other_Avg, 0) + COALESCE(NoDisease_Avg, 0) + COALESCE(Null_Avg, 0) AS Other_Avg,
 	@resolvedDateStart AS startdate,
 	@resolvedDateEnd   AS Enddate
FROM @ResolvedSummary 
ORDER BY COALESCE(ResolvedDate, 0)

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedCaseSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptResolvedCaseSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedCaseSummary] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptResolvedCaseSummary] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedCaseSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptResolvedCaseSummary] TO [power_user]
GO
