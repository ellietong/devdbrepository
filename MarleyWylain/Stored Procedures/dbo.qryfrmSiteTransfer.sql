SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROc [dbo].[qryfrmSiteTransfer]
(@CorpHistoryID INT)
AS

SET NOCOUNT ON
SELECT TransferID, SiteID, CorpHistoryID 
FROM tblSiteTransfer
WHERE CorpHistoryID = @CorpHistoryID
Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteTransfer] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteTransfer] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteTransfer] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmSiteTransfer] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteTransfer] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteTransfer] TO [power_user]
GO
