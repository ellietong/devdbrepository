SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptPolicyLimitsWithLimitTypeOption]
(@policyID int = NULL, @limitTypeID int = null)
As

SET NOCOUNT ON

IF @policyID IS NULL
	SELECT LimitID, PolicyID, pl.LimitTypeID, AttachmentPoint, PerOccLimit, LayerPerOccLimit, SIR, SIRAggregateLimit, AggregateLimit, PreviouslyExhaustedAggregate, Layer, LimitNote, plt.LimitType
	FROM tblPolicyLimitType plt INNER JOIN tblPolicyLimits pl ON plt.LimitTypeID = pl.LimitTypeID
	ORDER BY pl.PolicyID, pl.LimitTypeID
ELSE
	SELECT LimitID, PolicyID, pl.LimitTypeID, AttachmentPoint, PerOccLimit, LayerPerOccLimit, SIR, SIRAggregateLimit, AggregateLimit, PreviouslyExhaustedAggregate, Layer, LimitNote, plt.LimitType
	FROM tblPolicyLimitType plt INNER JOIN tblPolicyLimits pl ON plt.LimitTypeID = pl.LimitTypeID
	WHERE PolicyID = @policyID and pl.LimitTypeID = @limitTypeID
	ORDER BY pl.PolicyID, pl.LimitTypeID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyLimitsWithLimitTypeOption] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyLimitsWithLimitTypeOption] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyLimitsWithLimitTypeOption] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptPolicyLimitsWithLimitTypeOption] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyLimitsWithLimitTypeOption] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyLimitsWithLimitTypeOption] TO [power_user]
GO
