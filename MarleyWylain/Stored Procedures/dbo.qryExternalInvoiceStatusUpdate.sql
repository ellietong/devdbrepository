SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


---- =============================================
---- Author:            <Author,,Name>
---- Create date: <Create Date,,>
---- Description: <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalInvoiceStatusUpdate]
AS
BEGIN

/*Query run daily to send a separate alert email to the designated Invoice Contacts for each firm when
an invoice has been accepted or rejected. - BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @Header nvarchar (2000)

SET @Subject = 'Marley-Wylain: Invoice Status Update ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    
SET @Header = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>
<H4>There has been a status change for the following invoice(s):</H4>'

--Create a temp table for any invoices that have had a status change in the past day.
DECLARE @NewlyApprovedInvoices TABLE (invoiceNum nvarchar (50), status nvarchar (50), dateapproved datetime,
firmid int, feeAmount money, disbursementamount money, adjustment money, adjustmentnotes nvarchar (500))
INSERT INTO @NewlyApprovedInvoices
	SELECT
		di.invoicenumber,
		status,
		dateapproved,
		di.DefenseCounselAddressID,
		SUM(dic.Fees),
		SUM(dic.Costs),
		SUM(dic.adjustments),
		COALESCE(di.InvoiceNotes,'N/A')
	FROM tblDefenseInvoice di
	INNER JOIN tblDefenseInvoiceStatus s ON s.StatusID = di.StatusID
	INNER JOIN tblDefInvClaimantDetail dic ON di.DefenseInvoiceID = dic.DefenseInvoiceID
	WHERE DateApproved BETWEEN GETDATE() - 1 AND GETDATE()
	GROUP BY	di.invoicenumber,
				status,
				dateapproved,
				di.DefenseCounselAddressID,
				di.InvoiceNotes

--Declare a cursor to create a separate email for each firm
DECLARE @FirmAddressID nvarchar (20), @firmURL nvarchar (100)

DECLARE firm_Curs CURSOR FOR SELECT
	f.FirmID,
	'https://ligado.kcic.com/Claims/Dashboard?ClientSiteID=3'
FROM @NewlyApprovedInvoices i
INNER JOIN vtblFirmCase f ON i.FirmID = f.FirmID
--where f.firmid <> 132 --DANA
GROUP BY f.FirmID

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmurl

While @@FETCH_STATUS = 0

BEGIN
DECLARE @BodyMessage nvarchar (max)
SET @BodyMessage = @Header

--Declare a cursor and variables to be used in the cursor to create a separate table of complaints for each matter
--DECLARE @NumberPerMatter int
--DECLARE @Matter nVarChar(20)

--DECLARE matterDocs_Curs CURSOR FOR

--SELECT COUNT(DISTINCT invoiceNum), Matter
--FROM @NewlyApprovedInvoices 
--WHERE FirmID = (SELECT @FirmAddressID)
--GROUP BY Matter
--ORDER BY Matter

SET @BodyMessage = @BodyMessage +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid rgb(0,121,193); color: #F6ECF0; width: 700px;}
TD    {font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000}
TH    {border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
#extra {
      text-align: left;
      padding: .3em 0 .3em 0;
      margin-left: 5px;
      margin-right: 5px;
      }
--->
</STYLE>'

--OPEN matterDocs_curs 

--FETCH NEXT FROM matterDocs_curs INTO @NumberPerMatter, @Matter

--WHILE @@fetch_status = 0
--BEGIN
--Header line for each matter's table
SET @BodyMessage = @BodyMessage + '<br>' + 'Invoice(s) with Status Updates:'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid rgb(0,121,193); color: #F6ECF0; width: 700px;">' +
N'<tr><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Number</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Status</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Approved</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Fee Amount</th>' +
'<th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disbursement Amount</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Adjustment Amount</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Payment<br>Pending</th>'

--create a cursor do loop through invoices and print appropriate HTML
DECLARE @invoicenum nvarchar (50), @status nvarchar (100), @dateApproved nvarchar (100), @feeAmount nvarchar (100),
@disbursementAmount nvarchar (100), @adjustment nvarchar (100), @pmtPending nvarchar (100), @adjustmentnotes nvarchar (MAX)

DECLARE invoice_Curs CURSOR FOR SELECT
	CONVERT(nvarchar(50), invoicenum, 1),
	[status],
	CONVERT(varchar, C.dateapproved, 101) AS dateApproved,
	CONVERT(nvarchar(30), COALESCE(C.feeAmount, 0), 1) AS feeAmount,
	CONVERT(nvarchar(30), COALESCE(C.disbursementamount, 0), 1) AS disbursementAmount,
	CONVERT(nvarchar(30), COALESCE(adjustment, 0), 1) AS adjustment,
	CONVERT(nvarchar(30), COALESCE((feeAmount + disbursementAmount - abs(adjustment)), 0), 1) AS pmtPending,
	COALESCE('<u><b style="font-size: 11pt;" >Adjustment Notes</b></u>:   ' + C.adjustmentnotes, '') AS adjustmentNotes

FROM @NewlyApprovedInvoices C
WHERE c.FirmID = (SELECT
	@FirmAddressID)
ORDER BY dateapproved, [status]

OPEN invoice_curs
FETCH NEXT FROM invoice_curs INTO @invoicenum, @status, @dateApproved, @feeAmount,
@disbursementAmount, @adjustment, @pmtPending, @adjustmentnotes

While @@FETCH_STATUS = 0

BEGIN
--start the table row
SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">' + @invoicenum + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">' + @status + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">' + @dateApproved + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">$' + @feeAmount + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">$' + @disbursementAmount + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">$' + @adjustment + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">$' + @pmtPending + '</td></tr>'
--only have a separate row for notes if rec has an adjustment
IF @status = 'Approved with Adjustment'
BEGIN
SET @BodyMessage = @BodyMessage + '<tr><td colspan=7 style="text-align: left; margin-left: 93px;">' + @adjustmentnotes + '</td></tr>'
END
FETCH NEXT FROM invoice_curs INTO @invoicenum, @status, @dateApproved, @feeAmount,
@disbursementAmount, @adjustment, @pmtPending, @adjustmentnotes
END
CLOSE invoice_curs
DEALLOCATE invoice_curs
SET @BodyMessage = @BodyMessage + N'</table>' + N'<br>'

--Repeat for next matter
--FETCH NEXT FROM matterDocs_curs INTO @NumberPerMatter, @Matter
--END             
--CLOSE matterDocs_curs
--DEALLOCATE matterDocs_curs

--Link to access invoice library
SET @BodyMessage = @BodyMessage + N'<b>Click <A Href = ' + (SELECT
	@firmURL)
+ '>here</A> to access the invoice(s).</b><br><br>'


SET @BodyMessage = @BodyMessage + N'<b>If you have any questions regarding the approval status of the above invoice(s), please contact Gary McKenna at Marley-Wylain.</b><br><br>'


SET @BodyMessage = @BodyMessage + N'<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--Declare and set cursor to create the recipient list from the invoice contact field
DECLARE @RecipientList nvarchar (500)
SET @RecipientList = ''
DECLARE @CCRecipientList nvarchar (500)
SET @CCRecipientList = ''

DECLARE @Contact nvarchar (500)
DECLARE contact_curs CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY
FOR SELECT
DISTINCT
	email
FROM vEmailRecipients e
WHERE FirmAddressID = @FirmAddressID and e.RoleID=58


OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS = 0
BEGIN
SET @RecipientList = @RecipientList + @Contact + '; '
FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

-- Populate CC Recipients
--DECLARE @ContactCC nvarchar (500)
--DECLARE contact_cursCC CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY
--FOR SELECT
--DISTINCT
--	email
--FROM vEmailRecipients e
----WHERE e.RoleID=59


--OPEN contact_cursCC
--FETCH NEXT FROM contact_cursCC INTO @contactCC
--WHILE @@FETCH_STATUS = 0
--BEGIN
--SET @CCRecipientList = @CCRecipientList + @ContactCC + '; '
--FETCH NEXT FROM contact_cursCC INTO @contactCC
--END
--CLOSE contact_cursCC
--DEALLOCATE contact_cursCC


--SET @BodyMessage = @BodyMessage + @RecipientList


--print @BodyMessage

--Execute stored procedure to send email
EXEC msdb.dbo.sp_send_dbmail --The Demo ClientProfile sends from Demo Client@kcic.com
@profile_name = 'Claims',
@recipients = @RecipientList,
--@recipients = 'hagep@kcic.com',
@blind_copy_recipients = 'mwc@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
--print @BodyMessage
--Move to next firm
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmurl
END
CLOSE firm_curs
DEALLOCATE firm_curs

END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalInvoiceStatusUpdate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalInvoiceStatusUpdate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalInvoiceStatusUpdate] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryExternalInvoiceStatusUpdate] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalInvoiceStatusUpdate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalInvoiceStatusUpdate] TO [power_user]
GO
