SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmUnionNamedInsured]
AS

SET NOCOUNT ON

SELECT NamedInsuredID, NamedInsured
FROM tblNamedInsured

UNION 

SELECT 0, '<<All Named Insured>>' AS NamedInsured
FROM tblNamedInsured
ORDER BY NamedInsured

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionNamedInsured] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionNamedInsured] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionNamedInsured] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmUnionNamedInsured] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionNamedInsured] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionNamedInsured] TO [power_user]
GO
