SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_SETTLEMENT_qryMedicareRepofPlaintiffCounsel]
(@ClaimLawsuitID int)
As
Set Nocount on

--DECLARE @ClaimLawsuitID INT
--SET @ClaimLawsuitID = 267047

SELECT top 1
COALESCE(FName + ' ' + LName, 'Guy Smiley') AS PlaintiffCounselName,
PlaintiffCounsel,
CPI.FirstName + ' ' + COALESCE(CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(Suffix,'') AS ClaimantName,
CourtType, 
UPPER(S.FullNameState) AS [FullNameState], 
Jurisdiction, 
DocketNumber,
YEAR(GETDATE()) AS CurrentYear


FROM tblClaimantPersonalInfo CPI
LEFT JOIN tblClaimantRelatedParty RP ON RP.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
LEFT JOIN tblClaimantSuffix CS ON CS.SuffixID=CPI.SuffixID
LEFT JOIN tblLegalCapacity LC ON LC.LegalCapacityID=RP.LegalCapacityID
INNER JOIN tblClaim C ON C.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit CL ON CL.ClaimID=C.ClaimID
INNER JOIN tblLawsuit L ON L.LawsuitID=CL.LawsuitID
INNER JOIN tblCourtType CT ON CT.CourtTypeID=L.CourtTypeID
INNER JOIN tblState S ON S.StateID=L.StateID
INNER JOIN tblJurisdiction J ON J.JurisdictionID=L.JurisdictionID
INNER JOIN tblPlaintiffCounselAddress PCA ON PCA.PlaintiffCounselAddressID=L.PlaintiffCounselAddressID
LEFT JOIN (SELECT * FROM vtblPerson) P on P.PlaintiffCounselAddressID=PCA.PlaintiffCounselAddressID
WHERE ClaimLawsuitID = @ClaimLawsuitID 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryMedicareRepofPlaintiffCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryMedicareRepofPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryMedicareRepofPlaintiffCounsel] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryMedicareRepofPlaintiffCounsel] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryMedicareRepofPlaintiffCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryMedicareRepofPlaintiffCounsel] TO [power_user]
GO
