SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmResponsibleDefenseCounselLookup]
as


Select 0 as ResponsibleDefenseCounselAddressID, '<<All Defense Counsels>>' as ResponsibleDefenseCounsel
Union All
Select ResponsibleDefenseCounselAddressID, DefenseCounsel + ' - ' + DefenseCounselCity  as ResponsibleDefenseCounsel
From tblDefenseCounselState dcs Inner Join tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCOunselAddressID = dca.DefenseCounselAddressID
Where IsActiveCounsel = 1
Group By ResponsibleDefenseCounselAddressID, DefenseCounsel, defensecounselcity 
Order By ResponsibleDefenseCounsel

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmResponsibleDefenseCounselLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmResponsibleDefenseCounselLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmResponsibleDefenseCounselLookup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmResponsibleDefenseCounselLookup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmResponsibleDefenseCounselLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmResponsibleDefenseCounselLookup] TO [power_user]
GO
