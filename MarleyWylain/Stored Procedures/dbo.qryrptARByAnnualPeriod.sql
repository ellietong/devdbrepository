SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  Procedure [dbo].[qryrptARByAnnualPeriod]
(@allocationID int = null )
As

SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 83

DECLARE @Policy TABLE (policyid int)
INSERT INTO @Policy

SELECT policyid
FROM vPolicy
GROUP BY PolicyID

DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM tblAllocation

SELECT ar.AllocationID, ar.AnnualPeriodSplit, Sum(ar.SIRAllocated) AS SumOfSIRAllocated, Sum(ar.Allocation) AS SumOfAllocation, Sum(ar.PV) AS SumOfPV, 
	SUM(SIRAllocated + Allocation) AS TotalAllocated, MAX(ar.DateTime) AS MaxOfDateTime, a.AllocationDescription, SUM(ar.AllocationIndemnity) as SumofAllocationIndemnity,
	SUM(ar.AllocationDefense) AS SumofAllocationDefense, SUM(ar.AllocationDefenseOutsideLimits) AS SumofAllocationDefenseOutsideLimits
FROM @Policy p 
INNER JOIN (vAllocation a INNER JOIN vAllocationResults ar ON a.AllocationID = ar.AllocationID) ON p.PolicyID = ar.PolicyID
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID)
GROUP BY ar.AllocationID, ar.AnnualPeriodSplit, a.AllocationDescription
ORDER BY ar.AllocationID, ar.AnnualPeriodSplit

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAnnualPeriod] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByAnnualPeriod] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAnnualPeriod] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptARByAnnualPeriod] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAnnualPeriod] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByAnnualPeriod] TO [power_user]
GO
