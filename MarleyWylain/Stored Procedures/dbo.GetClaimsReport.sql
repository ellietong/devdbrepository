SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--wrap the view vSEARCH_ClaimsReport_test inside of a stored proc to test performance


create procedure [dbo].[GetClaimsReport]
as
/*
	Wrapper stored proc that returns output from vSEARCH_ClaimsReport_test
*/
begin 

 SELECT
       CLAIMLAWSUITID			   
       ,ClaimantPersonalInfoID	    
       ,ActiveTrialDate			    
       ,ClaimID					    
       ,ClaimLawsuitStatus		    
       ,Matter					    
       ,ClaimSequence			         
       ,FullName				    
       ,BirthDate				    
       ,DeceasedDate			    
       ,SocialSecurityNumber                                       
       ,Occupation                                    
       ,HighLevelDisease                             
       ,DocketNumber                                      
       ,ServiceDate                                
       ,FileDate 
       ,State                                        
       ,Jurisdiction                                
       ,ProcessedDate                                 
       ,StatusDate                                 
       ,ProductNotes                                   
       ,ProductExposureStart                                     
       ,SettlementDate                    
	   ,SettlementPaidDate 						  
       ,SettlementAmount                              
       ,SOURCEDATE --AS SettlementPaidDate -- Date paid; --srce date is the source date of the first check that -- is cut for a settlement
       ,PlaintiffCounsel                                   
       ,ClaimLawsuitStatusID                         
       ,StateID 
       ,JurisdictionID
       ,IsMDL                                         
       ,HighLevelDiseaseID = HIGHLEVELDISEASEID                                 
       ,AllegationTypeID = ALLEGATIONTYPEID                                   
       ,PlaintiffCounselID = PLAINTIFFCOUNSELID                                 
       ,MatterID = MATTERID                                           
       ,DefenseCounselAddressID                    
       ,DefenseCounselAddressID2 = DEFENSECOUNSELADDRESSID  
       ,ClaimLawsuitStatusGroupID 
       ,PersonID                       
       ,BrandID                              
       ,Brand                      
       ,BankruptcyStayID                            
       ,BankruptcyStay                     
       ,ClaimLawsuitTypeID                              
       ,ClaimLawsuitType              
       ,JobsiteCity                 
       ,JobsiteStateID                           
       ,Jobsite             
       ,BusinessCaseID                              
       ,BusinessCase                  
	   ,ClaimLawsuitNotes
	   ,TrialNotes 
	   from 
	   vSEARCH_ClaimsReport_test

end
GO
