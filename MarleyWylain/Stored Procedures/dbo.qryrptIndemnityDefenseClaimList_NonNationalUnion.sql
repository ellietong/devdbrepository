SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create Proc [dbo].[qryrptIndemnityDefenseClaimList_NonNationalUnion] (@OccurrenceGroupID int, @ExhaustionDateID int)
As
SET NOCOUNT ON

/*Marley bills Settlements Paid (and not pre-billed) and Defense Paid in the current month.
10/5/11 - Per conversation with G. McKenna, defense expenses will be billed/reported on using the following methodology:
 - Defense for SETTLED claims will use exposure/DX dates in system for known MARLEY products.
 - Red Jacket & Marley Pump - use 1/1/67 default date (if not settled)
 - Hydromatick - use 1/1/70 default date (if not settled)
 - Weil-Mclain - use composite dates (if not settled)
 11/19/2013- JL added breakout for National Union adjustments.  When Claimants on NU Spike list become known, all defense billed for them on the composite 
*/


--Declare @OccurrenceGroupID int
--Declare @ExhaustionDateID int
--Set @OccurrenceGroupID = 57
--set @ExhaustionDateID=174

--select * from tblOccurrenceGroup order by OccurrenceGroupID desc
--select * from tblExhaustionDate order by ExhaustionDateID desc

---Pull in total Amount to be credited on composite for claimants that just became settled (known dates) and are on the National Union Spiked Listing
Declare @totalNUSpiked money 
select @totalNUSpiked =  sum(cost1) from tblOccurrence
where OccurrenceGroupID = @OccurrenceGroupID
and OccurrenceName like '%date adju%' and OccurrenceName like '%national union%'
and InClaim=1


--Determine individual claimant percentages for National Union and Non-NU claim listings for credited amounts
Declare @ClaimantPercentages table (occurrenceid bigint, claimantpersonalinfoid bigint, cost1 money, percentage float)
insert into @ClaimantPercentages 

select OccurrenceID, claimid, cost1, cost1/@totalNUSpiked*-1 from tblOccurrence
where OccurrenceGroupID = @OccurrenceGroupID
and OccurrenceName like '%date adju%' and OccurrenceName like '%national union%'
and InClaim=0



--Determine OccurrenceIDs for known date adjustments
Declare @KnownAdjustmentOccurrenceIDs table (occurrenceid bigint, claimantpersonalinfoid bigint)
insert into @KnownAdjustmentOccurrenceIDs 

select OccurrenceID, claimid
from tblOccurrence
where OccurrenceGroupID = @OccurrenceGroupID
and OccurrenceName like '%date adju%' and OccurrenceName like '%national union%'
and InClaim=1



--Non-National Union

--Determine total to be spread to claimants for Non-National Union Claim listing
Declare @totalNonNationalUnionPortion money
select @totalNonNationalUnionPortion =  sum(cost1) from tblOccurrence
where OccurrenceGroupID=@OccurrenceGroupID
and OccurrenceName like '%composite aggregated%' and OccurrenceName not like '%non%'
and OccurrenceName not like '%1976%'

---Determine Known Date adjustment Amounts for Non-National Union Claim Listing

Declare @NonNUKnownDateAdjustmentsByClaimant table (occurrenceid bigint, claimantpersonalinfoid bigint, cost1 money)
insert into @NonNUKnownDateAdjustmentsByClaimant

select o.occurrenceid,ClaimID, sum(defenseallocation+DefenseOutsideLimitsAllocation) 
from tblExhaustion e
join tblOccurrence o on o.OccurrenceID=e.OccurrenceID
where e.OccurrenceID in (select distinct OccurrenceID from @KnownAdjustmentOccurrenceIDs)
and PolicyDateID not in (52) and ExhaustionDateID=@ExhaustionDateID
group by o.occurrenceid,ClaimID


---Determine composite adjustment Amounts for Non-National Union Claim Listing

Declare @NonNUCompositeAdjustmentByClaimant table (occurrenceid bigint, claimantpersonalinfoid bigint, cost1 money)
insert into @NonNUCompositeAdjustmentByClaimant

select occurrenceid, claimantpersonalinfoid, percentage*@totalNonNationalUnionPortion
from @ClaimantPercentages


--Determine all occurrences to exclude from both claim listings (will be Unioned in with re-calculated amounts at end of query)

Declare @OccurrencesExclude table (occurrenceid bigint, claimantpersonalinfoid bigint)
insert into @OccurrencesExclude 

select OccurrenceID, claimid
from tblOccurrence
where OccurrenceGroupID = @OccurrenceGroupID
and OccurrenceName like '%date adju%' and OccurrenceName like '%national union%'

Declare @NationalUnionSpikesClaimantData table (occurrenceid bigint, claimantpersonalinfoid bigint, claimantname nvarchar(150), SocialSecurityNumber nvarchar(50), state nvarchar(5), disease nvarchar(5), 
filedate datetime, settlementdate datetime, earliestexposuredate datetime, DiagnosisDate datetime, amount money, amounttype nvarchar(5), Adj nvarchar(5))

insert into @NationalUnionSpikesClaimantData
select o.occurrenceid, CPI.ClaimantPersonalInfoID, 	Lastname+', '+FirstName as ClaimantName, coalesce('XXX-XX-'+ Right([SocialSecurityNumber],4),'XXX-XX-') as SocialSecurityNumber, 
	State,	left(HighLevelDisease,1) as Disease,max(FileDate), 	SettlementDate, 
	--Coalesce(TriggerStart1, TriggerStart2) as  EarliestExposureDate,
	case when inclaim=1 then Coalesce(triggerstart1, Triggerstart2) else cast('1/1/1976' as datetime) end as EarliestExposureDate,
	case when inclaim=1 then Coalesce(triggerEnd1, TriggerEnd2) else cast('10/1/1985' as datetime) end as DiagnosisDate, 
	coalesce(cac.cost1, kac.cost1,0) as Amount,
	CASE when OccurrenceTypeID in (1,3) THEN 'I' Else 'E'END  as AmountType, 'A' as Adj
from tblOccurrence O
	join tblOccurrenceGroup OG  on												O.OccurrenceGroupID = OG.OccurrenceGroupID
	left Join tblClaimantPersonalInfo CPI on									CPI.ClaimantPersonalInfoID = o.ClaimID
	inner join (select * from tblClaim where iscurrentclaim=1) c on				c.ClaimantPersonalInfoID = o.ClaimID 
	Left Join (select * from tblClaimDisease where PrimaryDisease = 1) cd on	cd.ClaimID = c.ClaimID
	Left Join tblDisease D on													cd.DiseaseID=D.DiseaseID
	Left Join tblHighLevelDisease HLD on										D.HighLevelDiseaseID=HLD.HighLevelDiseaseID
	Left Join (select * from tblClaimLawsuit where IsPrimaryForClaim =1) cl on	cl.ClaimID=c.ClaimID
	Left Join tblLawsuit L on													CL.LawsuitID=L.LawsuitID 
	Left Join tblState S on														L.StateID=S.StateID
	left Join tblClaimantSettlement CS on										CL.ClaimLawsuitID=CS.CLaimLawsuitID
	left join tblAllegation A on												a.ClaimLawsuitID = CL.ClaimLawsuitID
	left join tblDefendantCompany dc on											dc.DefendantCompanyID = a.DefendantCompanyID
	left join @NonNUCompositeAdjustmentByClaimant cac on							cac.occurrenceid=o.OccurrenceID
	left join @NonNUKnownDateAdjustmentsByClaimant kac on							kac.occurrenceid=o.OccurrenceID
where o.OccurrenceGroupID = @OccurrenceGroupID
and OccurrenceName like '%date adju%' and OccurrenceName like '%national union%'
group by o.occurrenceid, CPI.ClaimantPersonalInfoID, 	Lastname+', '+FirstName , coalesce('XXX-XX-'+ Right([SocialSecurityNumber],4),'XXX-XX-') , 
	State,	left(HighLevelDisease,1), 	SettlementDate, 
	case when inclaim=1 then Coalesce(triggerstart1, Triggerstart2) else cast('1/1/1976' as datetime) end,
	case when inclaim=1 then Coalesce(triggerEnd1, TriggerEnd2) else cast('10/1/1985' as datetime) end, 
	coalesce(cac.cost1, kac.cost1,0),
	CASE when OccurrenceTypeID in (1,3) THEN 'I' Else 'E'END 


select claimantpersonalinfoid, claimantname, SocialSecurityNumber, state, disease, filedate, settlementdate, earliestexposuredate, DiagnosisDate, amounttype, (amount), NULL as EXPNotation, NULL as DXnotation, Adj
from @NationalUnionSpikesClaimantData


UNION ALL

--Select claimant information associated with expenses to be billed
select CPI.ClaimantPersonalInfoID, 
	Lastname+', '+FirstName as ClaimantName, 
	coalesce('XXX-XX-'+ Right([SocialSecurityNumber],4),'XXX-XX-') as SocialSecurityNumber, 
	State,
	left(HighLevelDisease,1) as Disease, 
	max(FileDate), 
	SettlementDate, 
	Coalesce(TriggerStart1, TriggerStart2) as  EarliestExposureDate,
	Coalesce(triggerEnd1, TriggerEnd2) as DiagnosisDate, 
	CASE when OccurrenceTypeID in (1,3) THEN 'I' Else 'E'END  as AmountType,
	o.Cost1+o.Cost3+o.Cost2+o.Cost4 as Amount,
	case when inclaim = 1 AND Triggerstart1 = TriggerStart1 THEN ''
		 WHen Inclaim = 0 AND Triggerstart1 is null THEN '+'
		 When INclaim = 1 AND o.MatterID=2 Then '*'
		 End as ExpNotation,
		Case when TriggerEnd1 is not null Then '' 
		 Else '*' 
		 END as DxNotation,
	CASE When o.Cost2 <>0 or o.Cost4 <>0 or occurrencename like '%adjustment%' Then 'A' 
		 ELSE 'B' 
		 END as ADJ
from tblOccurrence O
	join tblOccurrenceGroup OG  on												O.OccurrenceGroupID = OG.OccurrenceGroupID
	left Join tblClaimantPersonalInfo CPI on									CPI.ClaimantPersonalInfoID = o.ClaimID
	inner join (select * from tblClaim where iscurrentclaim=1) c on				c.ClaimantPersonalInfoID = o.ClaimID 
	Left Join (select * from tblClaimDisease where PrimaryDisease = 1) cd on	cd.ClaimID = c.ClaimID
	Left Join tblDisease D on													cd.DiseaseID=D.DiseaseID
	Left Join tblHighLevelDisease HLD on										D.HighLevelDiseaseID=HLD.HighLevelDiseaseID
	Left Join (select * from tblClaimLawsuit where IsPrimaryForClaim =1) cl on	cl.ClaimID=c.ClaimID
	Left Join tblLawsuit L on													CL.LawsuitID=L.LawsuitID 
	Left Join tblState S on														L.StateID=S.StateID
	left Join tblClaimantSettlement CS on										CL.ClaimLawsuitID=CS.CLaimLawsuitID
	left join tblAllegation A on												a.ClaimLawsuitID = CL.ClaimLawsuitID
	left join tblDefendantCompany dc on											dc.DefendantCompanyID = a.DefendantCompanyID
Where O.OccurrenceGroupID = @OccurrenceGroupID 
	and cl.IsPrimaryForClaim = 1 
	and c.IsCurrentClaim = 1 
	and o.matterID = c.MatterID 
	--and c.ClaimID not in (16839,20992,49299,56204,90833,82601,29855,46115)
	and o.OccurrenceTypeID in (1,2)
	--and Occurrenceid not in (select distinct OccurrenceID from @OccurrencesExclude)

group by CPI.ClaimantPersonalInfoID, Lastname+', '+FirstName, coalesce('XXX-XX-'+ Right([SocialSecurityNumber],4),'XXX-XX-') , 
	State,CASE when OccurrenceTypeID in (1,3) THEN 'I' Else 'E'END ,Coalesce(TriggerStart1, TriggerStart2),
	left(HighLevelDisease,1),  SettlementDate, Coalesce(triggerEnd1, TriggerEnd2) ,o.Cost1+o.Cost3+o.Cost2+o.Cost4,
	Case when TriggerEnd1 is not null Then '' Else '*' END,	case when inclaim = 1 AND Triggerstart1 = TriggerStart1 THEN ''
WHen Inclaim = 0 AND Triggerstart1 is null THEN '+'
When INclaim = 1 AND o.MatterID=2 Then '*'
End , CASE When o.Cost2 <>0 or o.Cost4 <>0 or occurrencename like '%adjustment%' Then 'A' ELSE 'B' END
order by amounttype, ClaimantName, ADJ
RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_NonNationalUnion] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_NonNationalUnion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_NonNationalUnion] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_NonNationalUnion] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_NonNationalUnion] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_NonNationalUnion] TO [power_user]
GO
