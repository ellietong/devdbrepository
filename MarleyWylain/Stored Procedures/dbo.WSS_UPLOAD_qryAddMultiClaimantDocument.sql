SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument]
(@ClaimantPersonalInfoID bigint, @DocumentID bigint)
AS
SET NOCOUNT ON

INSERT INTO tblMultiClaimantDocument (ClaimantPersonalInfoID, DocumentID)
	VALUES(@ClaimantPersonalInfoID, @DocumentID)
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [power_user]
GO
