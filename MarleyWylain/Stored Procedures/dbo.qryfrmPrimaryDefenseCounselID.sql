SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmPrimaryDefenseCounselID]

AS
Set NoCount On 

select DefenseCounsel, DefenseCounselID
from tblDefenseCounsel

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPrimaryDefenseCounselID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPrimaryDefenseCounselID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPrimaryDefenseCounselID] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPrimaryDefenseCounselID] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPrimaryDefenseCounselID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPrimaryDefenseCounselID] TO [power_user]
GO
