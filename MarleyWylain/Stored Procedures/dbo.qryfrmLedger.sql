SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmLedger]
(@carrierID int = null)
AS
SET NOCOUNT ON

If @carrierID= '0'
select * from tblLedger l
inner join tblcarrier c on c.CarrierID=l.CarrierID
order by CarrierName, ledgerid desc

Else

select * from tblLedger 
where CarrierID=@carrierID 
order by LedgerID desc


RETURN 



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmLedger] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger] TO [power_user]
GO
