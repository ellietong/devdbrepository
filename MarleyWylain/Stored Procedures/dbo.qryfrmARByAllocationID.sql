SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmARByAllocationID]
AS
SELECT AllocationID, MIN(CONVERT(varchar, AllocationID) + ' ' + AllocationDescription) AS AllocDesc
FROM tblAllocation
WHERE AllocationID IN (SELECT DISTINCT AllocationID FROM tblAllocationResults)
GROUP BY AllocationID
ORDER BY AllocationID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmARByAllocationID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmARByAllocationID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmARByAllocationID] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmARByAllocationID] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmARByAllocationID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmARByAllocationID] TO [power_user]
GO
