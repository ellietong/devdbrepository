SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qryfrmSubscriberCompanyListFull]
AS
SET NOCOUNT ON

SELECT SubscriberID, SubscriptionCompany 
FROM tblSubscriberCompany 
GROUP BY SubscriberID, SubscriptionCompany
ORDER BY CASE WHEN LEFT(SubscriptionCompany,1)='"' THEN SUBSTRING(SubscriptionCompany,2,LEN(SubscriptionCompany)-1) ELSE SubscriptionCompany END

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriberCompanyListFull] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriberCompanyListFull] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriberCompanyListFull] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriberCompanyListFull] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriberCompanyListFull] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriberCompanyListFull] TO [power_user]
GO
