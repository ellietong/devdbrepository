SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[qryfrmEnviroSitesDetail]
as
set nocount on

SELECT  *
FROM tblEnviroSitesDetail e

return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEnviroSitesDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmEnviroSitesDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEnviroSitesDetail] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmEnviroSitesDetail] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEnviroSitesDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmEnviroSitesDetail] TO [power_user]
GO
