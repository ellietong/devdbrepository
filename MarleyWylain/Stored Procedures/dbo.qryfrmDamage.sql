SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmDamage]
as

Select DamageID, Damage
From tblDamage
Order By Damage

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDamage] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDamage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDamage] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmDamage] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDamage] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDamage] TO [power_user]
GO
