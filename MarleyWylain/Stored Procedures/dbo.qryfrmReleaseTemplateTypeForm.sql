SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmReleaseTemplateTypeForm] (@ClaimLawsuitID int)

as

Set NoCount On

Select *
From tblSettlePaymentDocumentation
Where ClaimLawsuitID = @ClaimLawsuitID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmReleaseTemplateTypeForm] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmReleaseTemplateTypeForm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmReleaseTemplateTypeForm] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmReleaseTemplateTypeForm] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmReleaseTemplateTypeForm] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmReleaseTemplateTypeForm] TO [power_user]
GO
