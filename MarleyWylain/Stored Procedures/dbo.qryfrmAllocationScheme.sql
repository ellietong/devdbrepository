SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmAllocationScheme]
AS
SELECT AllocationSchemeID, AllocationScheme
FROM tblAllocationScheme
ORDER BY AllocationScheme
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationScheme] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationScheme] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationScheme] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationScheme] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationScheme] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationScheme] TO [power_user]
GO
