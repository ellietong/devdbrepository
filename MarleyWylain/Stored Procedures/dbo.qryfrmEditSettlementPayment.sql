SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[qryfrmEditSettlementPayment] (@CheckID int)
AS
SET NOCOUNT ON

select  cs.ClaimSettlementID, FirstName + ' ' + LastName as Claimant, PaymentAmount, COALESCE(IsFullyPaid, 0) AS IsFullyPaid
from tblPaymentSettlement ps
inner join tblClaimSettlement cs on cs.ClaimSettlementID=ps.ClaimSettlementID
inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
inner join tblclaim c on c.ClaimID=cl.ClaimID
inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
where CheckID = @CheckID

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditSettlementPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmEditSettlementPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditSettlementPayment] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditSettlementPayment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditSettlementPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmEditSettlementPayment] TO [power_user]
GO
