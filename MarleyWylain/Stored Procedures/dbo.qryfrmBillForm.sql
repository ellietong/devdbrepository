SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmBillForm]
(@filter varchar(3) = NULL)
As

SET NOCOUNT ON

SELECT *
FROM tblBill
WHERE BillGroupID LIKE COALESCE (@filter, '%')
ORDER BY BillDate, BillGroupID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillForm] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmBillForm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillForm] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmBillForm] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillForm] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmBillForm] TO [power_user]
GO
