SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[qryfrmPolicyLanguageIDLookup]
(@provisionLabelID int = NULL)
AS

SET NOCOUNT ON

IF @provisionLabelID IS NULL

SELECT LanguageID, PolicyLanguage, CONVERT(nvarchar(10), LanguageID) + ' - ' + Policylanguage 
FROM tblLanguage 
ORDER BY LanguageID

ELSE

SELECT LanguageID, PolicyLanguage, CONVERT(nvarchar(10), LanguageID) + ' - ' + PolicyLanguage 
FROM tblLanguage 
WHERE ProvisionLabelID = @provisionLabelID
ORDER BY LanguageID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLanguageIDLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLanguageIDLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLanguageIDLookup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLanguageIDLookup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLanguageIDLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLanguageIDLookup] TO [power_user]
GO
