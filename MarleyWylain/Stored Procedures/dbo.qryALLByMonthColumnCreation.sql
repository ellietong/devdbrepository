SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryALLByMonthColumnCreation] (@AllocationID nvarchar(250), @TriggerStart nvarchar(250), @TriggerEnd nvarchar(250), @strInsuranceProfile nvarchar(250), @strVerticallyUncollapsed nvarchar(250), @strProvision1 nvarchar(250), @OccurrenceID nvarchar(250))
As
Set NoCount On

/*
CREATE BY: JTM
CREATED ON: 
LAST MODIFIED BY:
LAST MODIFIED DATE: 
MODIFICATIONS:

This query populates the table ztblByMonthColumns which is used by the allocation to determine the months triggered.

Query consists of multiple table variables.

1. @base
2. @one
3. @GroupedColumns
4. @ColumnsByMonth


Variables used in query
1.
2.

*/
--Declare @AllocationID nvarchar(250)
--Declare @TriggerStart nvarchar(250)
--Declare @TriggerEnd nvarchar(250)
--Declare @strInsuranceProfile nvarchar(250)
--Declare @strVerticallyUncollapsed nvarchar(250)
--Declare @strProvision1 nvarchar(250)
--Declare @occurrenceID nvarchar(250)

--Set @AllocationID = '2'
--Set @TriggerStart = '1/1/1950'
--Set @TriggerEnd = '1/1/1986'
--Set @strInsuranceProfile = 'pa.InsuranceProgramID = 1'
--Set @strVerticallyUncollapsed = 'ColumnPerOcc > 0.1'
--Set @strProvision1 = '1=1'
--Set @occurrenceID = '41'

Declare @strsql1 nvarchar(MAX)
Declare @strsql2 nvarchar(MAX)
Declare @strsql3 nvarchar(MAX)
Set @strsql1 = ' SET NOCOUNT ON  

Declare @base table (ColumnStartDate datetime, ColumnEndDate datetime, PolicyDateID int, ApplicableLayer int, ColumnID int Identity, InsuranceProgramID int, OccurrenceRemaining money, SmallestRemaining int)
Insert into @base
Select Case When ColumnStartDate < ''' +@TriggerStart+ ''' Then ''' +@TriggerStart+ ''' Else ColumnStartDate End, Case When ColumnEndDate > ''' +@TriggerEnd+ ''' Then ''' +@TriggerEnd+ ''' Else ColumnEndDate End, PolicyDateID,
DENSE_RANK() OVer (Partition By ColumnID Order By AttachmentPoint) as ApplicableLayer, pa.InsuranceProgramID, OccurrenceRemaining, 
DENSE_RANK() OVer (Partition By ColumnID, AttachmentPoint Order By OccurrenceRemaining, PolicyID) as SmallestRemaining
from tblallocationresultscolumn arc
Inner Join ztblpolicyallocation pa on arc.allocationid= pa.allocationid AND arc.InsuranceProgramID = pa.InsuranceProgramID
where arc.allocationid = '+@AllocationID+' AND (ColumnStartDate < PEndDate AND ColumnEndDate > PStartDate) AND OccurrenceRemaining > .1
		 AND (ColumnStartDate < ''' +@TriggerEnd+ ''') AND (ColumnEndDate > ''' +@TriggerStart+ ''') AND ('+@strInsuranceProfile+') AND '+@strVerticallyUncollapsed+' AND '+@strProvision1+'
Order By ColumnID, AttachmentPoint, OccurrenceRemaining/(PerOccLimit/LayerPerOccLimit)

Declare @one table (ColumnStartDate datetime, ColumnEndDate datetime, PolicyDateID int, ColumnID int Identity, InsuranceProgramID int)
Insert Into @one
Select ColumnStartDate, ColumnEndDate, PolicyDateID, InsuranceProgramID
From @base
Where ApplicableLayer = 1 and SmallestRemaining = 1

Declare @GroupedColumns table (AdjustedColumnID int identity, ColumnStartDate datetime, ColumnEndDate datetime, ColumnRemaining money, ColumnMonths int, TotalMonths Int, MinRemaining money, InsuranceProgramID int, PolicyMonths int)
Declare @TempColumnID int
Declare @MaxColumnID int
Set @TempColumnID = (Select MIN(ColumnID) From @one)
Set @MaxColumnID = (Select MAX(ColumnID) from @one)'

SET @strsql2 = '
While @TempColumnID <= @MaxColumnID
Begin
			Declare @ColumnStart Datetime
			Declare @ColumnEnd Datetime
			Declare @ColumnIDStart Datetime
			Declare @ColumnRemaining money
			Declare @InsuranceProgramID int
			Declare @PolicyMonths int
			Set @ColumnIDStart = @TempColumnID
			Set @ColumnStart = (Select ColumnStartDate From @one Where ColumnID = @TempColumnID)
			Declare @PolicyColumn1 int
			Declare @PolicyColumn2 int
			Declare @ForwardStart datetime
			Declare @CurrentEnd datetime
			Set @PolicyColumn1 = (Select PolicyDateID From @one where ColumnID = @TempColumnID)
			Set @PolicyColumn2 = (Select PolicyDateID From @one where ColumnID = @TempColumnID+1)
			Set @CurrentEnd = (Select ColumnEndDate From @one Where ColumnID = @TempColumnID)
			Set @ForwardStart = (Select ColumnStartDate From @one Where ColumnID = @TempColumnID+1)
						
			While @TempColumnID <= @MaxColumnID 
			Begin
				If @PolicyColumn1 <> @PolicyColumn2 OR @ForwardStart <> @CurrentEnd
					Break
				Else
						Set @TempColumnID = @TempColumnID + 1				
						Set @PolicyColumn2 = (Select PolicyDateID From @one where ColumnID = @TempColumnID)
						Set @CurrentEnd = (Select ColumnEndDate From @one Where ColumnID = @TempColumnID)
						Set @ForwardStart = (Select ColumnStartDate From @one Where ColumnID = @TempColumnID+1)
					Continue
			End
		
		Set @InsuranceProgramID = (Select InsuranceProgramID From ztblPolicyAllocation where AllocationID = '+@AllocationID+' and PolicyDateID = @PolicyColumn1)
		Set @ColumnRemaining = (Select OccurrenceRemaining/(OccurrenceRemaining/LayerPerOccLimit) From ztblPolicyAllocation where AllocationID = '+@AllocationID+' and PolicyDateID = @PolicyColumn1)		
		Set @PolicyMonths = (Select datediff(month,pstartdate,penddate) From ztblPolicyAllocation where AllocationID = '+@AllocationID+' and PolicyDateID = @PolicyColumn1)		
		Set @ColumnEnd = (Select ColumnEndDate From @one Where ColumnID = (Case When @TempColumnID>@MaxColumnID Then @MaxColumnID When @TempColumnID = @ColumnIDStart Then @TempColumnID Else @TempColumnID-1 End))
		Insert Into @GroupedColumns (ColumnStartDate, ColumnEndDate, ColumnRemaining, InsuranceProgramID, PolicyMonths)
		Select @ColumnStart, @ColumnEnd, @ColumnRemaining, @InsuranceProgramID, @PolicyMonths
		Set @TempColumnID = (Select Case when @ColumnIDStart = @TempColumnID Then @TempColumnID + 1 Else @TempColumnID End)
End
'

Set @strsql3 = '
Declare @ColumnsByMonth table (AdjustedColumnID int, PStartDate datetime, PEndDate datetime)
Declare @TempColumnID2 int
Declare @MaxColumnID2 int
Set @TempColumnID2 = (Select Min(AdjustedColumnID) From @GroupedColumns)
Set @MaxColumnID2 = (Select MAX(AdjustedColumnID) From @GroupedColumns)

While @TempColumnID2 <= @MaxColumnID2
Begin
			Declare @TempStartDate datetime
			Declare @EndDate datetime
			Declare @DaysRemaining int
			Set @TempStartDate = (Select ColumnStartDate From @GroupedColumns Where AdjustedColumnID = @TempColumnID2)
			Set @EndDate = (Select ColumnEndDate From @GroupedColumns Where AdjustedColumnID = @TempColumnID2)
			Set @DaysRemaining = Convert(int,@EndDate) - Convert(int,@TempStartDate)
			
			WHILE convert(int,(@EndDate - @TempStartDate)) > 0
			BEGIN
			   IF @DaysRemaining < 15
					BREAK
			   ELSE
					Insert Into @ColumnsByMonth
					Select @TempColumnID2, @TempStartDate, Case When DATEADD(mm,1,@TempStartDate) > @enddate Then @EndDate 
							When Convert(int,@EndDate)- DATEADD(mm,1,@TempStartDate)+1 < 15 Then @EndDate
							Else DATEADD(mm,1,@TempStartDate) End
					Set @TempStartDate = DATEADD(mm,1,@TempStartDate)
					Set @DaysRemaining = Convert(int,@EndDate) - Convert(int,@TempStartDate)+1
				  CONTINUE
			END
			
	Set @TempColumnID2 = @TempColumnID2 + 1
END

Update @GroupedColumns
Set ColumnMonths = Coalesce(MonthCount,0)
From (Select AdjustedColumnID, Count(*) as MonthCount From @ColumnsByMonth Group By AdjustedColumnID) a
Right Join @GroupedColumns b on a.AdjustedColumnID = b.AdjustedColumnID

Update @GroupedColumns
Set TotalMonths = (Select Count(*) as MonthCount From @ColumnsByMonth)

Update @GroupedColumns
set ColumnRemaining = (convert(decimal,ColumnMonths)/convert(decimal,PolicyMonths))*ColumnRemaining

Update @GroupedColumns
Set MinRemaining = (Select top 1 Case When ColumnMonths = 0 Then 1000000000 Else ColumnRemaining/(convert(float,ColumnMonths)/Convert(float,TotalMonths)) End as MinRemaining from @GroupedColumns Order By MinRemaining)

Select AdjustedColumnID as ColumnID, ColumnStartDate, ColumnEndDate, ColumnRemaining, ColumnMonths as TriggeredDays, TotalMonths, MinRemaining, InsuranceProgramID
From @GroupedColumns

Insert Into ztblByMonthColumns (ColumnID, ColumnStartDate, ColumnEndDate, ColumnRemaining, TriggeredDays, TotalMonths, MinRemaining, InsuranceProgramID, OccurrenceID, AllocationID)
Select AdjustedColumnID, ColumnStartDate, ColumnEndDate, ColumnRemaining, ColumnMonths, TotalMonths, MinRemaining, InsuranceProgramID, ' + @OccurrenceID + ', ' + @AllocationID + '
From @GroupedColumns
'
EXEC (@strSQL1 + @strsql2 + @strsql3)
--Print (@strsql1) Print(@Strsql2) Print (@strsql3)

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLByMonthColumnCreation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLByMonthColumnCreation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLByMonthColumnCreation] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryALLByMonthColumnCreation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLByMonthColumnCreation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLByMonthColumnCreation] TO [power_user]
GO
