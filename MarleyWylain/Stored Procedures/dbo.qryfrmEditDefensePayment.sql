SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[qryfrmEditDefensePayment] (@CheckID int)
AS
SET NOCOUNT ON

select pd.DefenseInvoiceID, InvoiceNumber + ' - ' + Period as DefenseInvoice, PaymentAmount, COALESCE(IsFullyPaid, 0) AS IsFullyPaid
from tblPaymentDefense pd
inner join tblDefenseInvoice di on di.DefenseInvoiceID=pd.DefenseInvoiceID 
where CheckID = @CheckID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditDefensePayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmEditDefensePayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditDefensePayment] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmEditDefensePayment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmEditDefensePayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmEditDefensePayment] TO [power_user]
GO
