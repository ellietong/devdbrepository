SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create Proc [dbo].[qryrptAutoRelease]
(@ClaimantPersonalInfoID bigint, @ClaimLawsuitID bigint)
As
Set NoCount On

--DECLARE @Claimantpersonalinfoid bigint
--DECLARE @ClaimLawsuitID bigint
--set @Claimantpersonalinfoid = 48611
----set @Claimantpersonalinfoid = 21417 --two claims for one claimant
--Set @ClaimLawsuitID = 49660

-- Find Spouse for Claimant
Declare @Spouse nvarchar(100)
Declare @SpouseCapacity nvarchar(100)
Declare @SpouseInfo table (Spouse nvarchar(100), SpouseCapacity nvarchar(100))
Insert Into @SpouseInfo 
SELECT Distinct crp.FirstName + ' ' + Coalesce(crp.MiddleName + ' ','') + crp.LastName as Spouse, 
Coalesce(lc.LegalCapacity, '[CAPACITY]') as SpouceCapacity
FROM tblClaimantPersonalInfo cpi
	inner join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
	left join tblClaimDisease cd on cd.ClaimID = c.ClaimID
	left join tblDisease d on d.DiseaseID = cd.DiseaseID
	left join tblHighLevelDisease dhld on dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
	left join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblDefendantCompany dc on dc.DefendantCompanyID = a.DefendantCompanyID
	left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	left join tblState laws on laws.StateID = l.StateID
	left join tblClaimantRelatedParty crp on crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblLegalCapacity lc on lc.LegalCapacityID = crp.LegalCapacityID
	left join tblRelationship r on r.RelationshipID = crp.RelationshipID
	left join tblClaimantEmployment ce on ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblPlaintiffCounselAddress pc on pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	left join tblCourtType ct on ct.CourtTypeID = l.CourtTypeID
	left join tblJurisdiction j on j.JurisdictionID = l.jurisdictionID 
	left join tblSettlePaymentDocumentation spd on cl.ClaimLawsuitID = spd.ClaimLawsuitID
Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	and cl.ClaimLawsuitID = @ClaimLawsuitID 
	and lc.LegalCapacityID in (19, 24)

Set @Spouse = (Select Spouse From @SpouseInfo)
Set @SpouseCapacity = (Select SpouseCapacity From @SpouseInfo) 



-- Find Executors for Claimant
DECLARE Executor_Curs CURSOR FOR 

SELECT Distinct crp.FirstName + ' ' + Coalesce(crp.MiddleName + ' ','') + crp.LastName as RPName, 
Coalesce(lc.LegalCapacity, '[CAPACITY]') as ExecutorCapacity	 	
FROM tblClaimantPersonalInfo cpi
	inner join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
	left join tblClaimDisease cd on cd.ClaimID = c.ClaimID
	left join tblDisease d on d.DiseaseID = cd.DiseaseID
	left join tblHighLevelDisease dhld on dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
	left join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblDefendantCompany dc on dc.DefendantCompanyID = a.DefendantCompanyID
	left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	left join tblState laws on laws.StateID = l.StateID
	left join tblClaimantRelatedParty crp on crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblLegalCapacity lc on lc.LegalCapacityID = crp.LegalCapacityID
	left join tblRelationship r on r.RelationshipID = crp.RelationshipID
	left join tblClaimantEmployment ce on ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblPlaintiffCounselAddress pc on pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	left join tblCourtType ct on ct.CourtTypeID = l.CourtTypeID
	left join tblJurisdiction j on j.JurisdictionID = l.jurisdictionID 
	left join tblSettlePaymentDocumentation spd on cl.ClaimLawsuitID = spd.ClaimLawsuitID
	
	
	Where
	cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	and cl.ClaimLawsuitID = @ClaimLawsuitID 
	and lc.LegalCapacityID not in (19, 24) 

DECLARE @ExecutorName nvarchar(100)
DECLARE @ExecutorCapacity nvarchar(100)
Declare @AllExecutorNames nvarchar(200)
Declare @AllExecutorCapacities nvarchar(100)

----Open Cursor to loop through executors
OPEN Executor_Curs
FETCH NEXT FROM Executor_Curs INTO @ExecutorName, @ExecutorCapacity
WHILE @@fetch_status = 0
BEGIN

	If @AllExecutorNames is null 
		Begin 
			Set @AllExecutorNames = @ExecutorName 
		End
	Else
		Begin
			Set @AllExecutorNames = @AllExecutorNames + ', ' + @ExecutorName 
		End
	
	If @AllExecutorCapacities is null
		Begin
			Set @AllExecutorCapacities = @ExecutorCapacity 
		End
	Else
		Begin
			If @AllExecutorCapacities = @ExecutorCapacity 
				Begin
					Set @AllExecutorCapacities = @AllExecutorCapacities 
				End
			Else
				Begin
					Set @AllExecutorCapacities = '[MULTIPLE CAPACITIES FOR EXECUTORS]'
				End
		End
	
FETCH NEXT FROM Executor_Curs INTO @ExecutorName, @ExecutorCapacity
END
CLOSE Executor_Curs
DEALLOCATE Executor_Curs

--Select Information about Claimant for Release
SELECT  Distinct 
 LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4)as SSN,
cpi.FirstName , Coalesce(cpi.MiddleName + ' ','')as MiddleName , cpi.LastName, Coalesce(Suffix,'') as Suffix , laws.State,
pc.PlaintiffCounsel, pc.State as PCState, 
laws.StateID,
coalesce(@AllExecutorNames, '[EXECUTOR]') as Executors,
coalesce(@AllExecutorCapacities, '[CAPACITY]') as ExecutorCapacity,
coalesce(@Spouse,'[SPOUSE]') as Spouse, 
coalesce(@SpouseCapacity, '[CAPACITY]') as SpouseCapacity,
CourtType, 
Jurisdiction,
CaseCaption, 
DocketNumber, 
cl.ClaimLawsuitID, 
ReleaseTemplateTypeID,
c.MatterID,
SettlementGroup   
	
	
	FROM tblClaimantPersonalInfo cpi
	inner join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
	left join tblClaimDisease cd on cd.ClaimID = c.ClaimID
	left join tblDisease d on d.DiseaseID = cd.DiseaseID
	left join tblHighLevelDisease dhld on dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
	left join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblDefendantCompany dc on dc.DefendantCompanyID = a.DefendantCompanyID
	left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	left join tblState laws on laws.StateID = l.StateID
	left join tblClaimantRelatedParty crp on crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblLegalCapacity lc on lc.LegalCapacityID = crp.LegalCapacityID
	left join tblRelationship r on r.RelationshipID = crp.RelationshipID
	left join tblClaimantEmployment ce on ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblPlaintiffCounselAddress pc on pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	left join tblCourtType ct on ct.CourtTypeID = l.CourtTypeID
	left join tblJurisdiction j on j.JurisdictionID = l.jurisdictionID 
	left join tblSettlePaymentDocumentation spd on cl.ClaimLawsuitID = spd.ClaimLawsuitID
	left join tblSettlementGroup sg on sg.SettlementGroupID = cs.SettlementGroupID
	left join tblClaimantSuffix csuf on csuf.SuffixID = cpi.SuffixID
	
	
	Where
	cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	and cl.ClaimLawsuitID = @ClaimLawsuitID 

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoRelease] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAutoRelease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoRelease] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptAutoRelease] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoRelease] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAutoRelease] TO [power_user]
GO
