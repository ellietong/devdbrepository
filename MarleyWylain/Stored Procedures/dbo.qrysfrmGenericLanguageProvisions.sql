SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qrysfrmGenericLanguageProvisions]

As

SET NOCOUNT ON

select * from tblProvisionGenericLanguage



RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmGenericLanguageProvisions] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmGenericLanguageProvisions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmGenericLanguageProvisions] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysfrmGenericLanguageProvisions] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmGenericLanguageProvisions] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmGenericLanguageProvisions] TO [power_user]
GO
