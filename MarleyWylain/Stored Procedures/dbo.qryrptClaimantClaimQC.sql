SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptClaimantClaimQC] (@ClaimantPersonalInfoID int)

as
--declare @ClaimantPersonalInfoID int
--set @ClaimantPersonalInfoID = 57510


Select cpi.ClaimantPersonalInfoID,
cpi.FirstName as ClaimantFirstName, 
cpi.MiddleName as ClaimantMiddleName, 
cpi.LastName as ClaimantLastName, 
Suffix as ClaimantSuffix,
SocialSecurityNumber, 
DeceasedDate, 
MaritalStatus, 
Gender, 
Address1,
Address2,
City,
[State],
Zip,
CASE WHEN Extension IS NULL OR Extension LIKE '' THEN '(' + CONVERT(nvarchar(3),LEFT(Phone,3)) + ') ' + CONVERT(nvarchar(3),LEFT(RIGHT(Phone,7),3)) + '-' + CONVERT(nvarchar(4),RIGHT(Phone,4))
ELSE '(' + CONVERT(nvarchar(3),LEFT(Phone,3)) + ') ' + CONVERT(nvarchar(3),LEFT(RIGHT(Phone,7),3)) + '-' + CONVERT(nvarchar(4),RIGHT(Phone,4)) + ' x' + CONVERT(nvarchar(10),Extension) END AS Phone,
BirthDate,
HealthInsuranceClaimNumber,
Extension,
MedicareBeneficiaryStatus as medicarebeneficiary, IsSmoker, SmokingStart, SmokingEnd, csh.PacksPerDay,
IsCurrentClaim,
	ClaimSequence,
	Matter,
	c.MatterID,
	c.ClaimID
From tblClaimantPersonalInfo cpi 
Left Join tblGender g on g.GenderID = cpi.GenderID
Left Join tblClaimantSuffix cs on cs.SuffixID = cpi.SuffixID
Left Join tblMaritalStatus ms on ms.MaritalStatusID = cpi.MaritalStatusID
Left Join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
Left Join tblState st ON cpi.StateID = st.StateID
left join tblMedicareBeneficiaryStatus mbs on mbs.MedicareBeneficiaryStatusID = cpi.MedicareBeneficiaryStatusID
		Left Join tblClaimSequence csq on csq.claimsequenceID = c.claimsequenceID
	Left Join tblMatter m on c.MatterID = m.MatterID
	LEFT JOIN tblClaimantSmokingHistory csh on csh.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID --AND c.IsCurrentClaim = 1
Group By
cpi.ClaimantPersonalInfoID,
cpi.FirstName , 
cpi.MiddleName , 
cpi.LastName , 
Suffix,
SocialSecurityNumber, 
DeceasedDate, 
MaritalStatus, 
Gender, 
Address1,
Address2,
City,
[State],
Zip,
Extension, Phone,
BirthDate,
HealthInsuranceClaimNumber,
Extension,
MedicareBeneficiaryStatus, IsSmoker, SmokingStart, SmokingEnd, csh.PacksPerDay,
IsCurrentClaim,
	ClaimSequence,
	Matter,c.MatterID,
	c.ClaimID

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantClaimQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantClaimQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantClaimQC] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptClaimantClaimQC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantClaimQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantClaimQC] TO [power_user]
GO
