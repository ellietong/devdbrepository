SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmTriggerLabel]
As

SET NOCOUNT ON

SELECT TriggerID, TriggerDescription
FROM tblTriggerLabel
order by TriggerDescription
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmTriggerLabel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmTriggerLabel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmTriggerLabel] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmTriggerLabel] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmTriggerLabel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmTriggerLabel] TO [power_user]
GO
