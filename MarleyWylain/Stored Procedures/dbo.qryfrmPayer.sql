SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmPayer]

AS
Set Nocount On
SELECT PayerID, Payer 
FROM tblPayer
ORDER BY Payer
RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPayer] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPayer] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPayer] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPayer] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPayer] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPayer] TO [power_user]
GO
