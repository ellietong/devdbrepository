SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptSiteCostsQC] (@vendorid int, @SiteID int)
AS 
SET NOCOUNT ON

 --DECLARE @VendorID INT
 --SET @VendorID = 0
 --DECLARE @Siteid INT
 --SET @Siteid = 0

DECLARE @MAXVENDORID INT
SELECT @MAXVENDORID = max(vendorid) from tblvendor
DECLARE @MaxSiteID INT
SELECT @MaxSiteID = max(siteid) from tblEnviroSitesDetail

DECLARE @Summary TABLE (CostRecordID int, SiteName nvarchar(150), VendorName nvarchar(150), DocumentType nvarchar(100),
ExclP2 bit, StateAbb nvarchar(100), Country nvarchar(100), RecordSourceID int, RecordSource nvarchar(255), CostTypeID int,
InvoicePDFLink nvarchar(255), DateBilled datetime, WorkDesription nvarchar(255), Amount money, PaidDate datetime, 
InvoiceDate datetime, vendorid int, siteid int, ExclusionReason nvarchar(255), Category nvarchar(50), InvPDFLink bit, BatesMin nvarchar(255), 
BatesMax nvarchar(255), Excluded bit)
INSERT INTO @Summary

SELECT c.CostRecordID, CASE WHEN E.SiteName IS NULL THEN 'No Site - General' ELSE E.SiteName END AS 'SiteName', 
v.VendorName, D.DocumentType, ExcludedSite, S.state, Cn.Country, c.RecordSourceID, rs.recordsource, 
CASE WHEN c.CostTypeID IS NULL THEN 0 ELSE c.CostTypeID END AS 'CostTypeID',
c.InvoicePDFLink, c.datebilled, c.WorkDescription, c.ReimbursableAmount,
c.PaidDate, c.InvoiceDate, c.vendorid, c.siteid, ce.CostExclusionType, cc.CostType,
CASE WHEN invoicepdflink IS NULL THEN '0' ELSE '1' END AS 'InvPDFLink',
min(BatesMin) AS 'BatesMin', max(BatesMax) AS 'BatesMax',
CASE WHEN c.recordsourceid IN (10,11,12) THEN '1' ELSE '0' END AS Excluded
FROM  dbo.tblSiteCosts C 
LEFT JOIN tblEnviroSitesDetail E ON C.SiteID = E.SiteID 
LEFT JOIN tblVendor v ON v.VendorID = C.VendorID 
LEFT JOIN tblDocumentType D ON D.DocumentTypeID = C.DocumentTypeID 
LEFT JOIN tblState S ON S.StateID = E.StateID 
LEFT JOIN tblCountry Cn ON Cn.CountryID = E.CountryID 
LEFT JOIN tblCostExclusion ce ON ce.CostExcludeTypeID = C.CostExclusionTypeID 
LEFT JOIN tblCostType cc ON cc.CostTypeID = C.CostTypeID
LEFT JOIN tblRecordSource rs ON c.recordsourceid = rs.recordsourceid
LEFT JOIN tblBatesRanges  B on b.costrecordid = c.CostrecordID
WHERE c.vendorid BETWEEN COALESCE(@vendorid, 0) AND COALESCE(NullIF(@vendorid,0), @MAXVENDORID)AND 
c.siteid BETWEEN COALESCE(@SiteID, 0) AND COALESCE(NullIF(@SiteID,0), @MaxSiteID)



GROUP BY CASE WHEN E.SiteName IS NULL THEN 'No Site - General' ELSE E.SiteName END, 
v.VendorName, D.DocumentType, ExcludedSite, S.state, Cn.Country, rs.recordsource, 
CASE WHEN c.CostTypeID IS NULL THEN 0 ELSE c.CostTypeID END,
c.InvoicePDFLink, c.RecordSourceID, c.datebilled, c.WorkDescription, c.ReimbursableAmount,
c.PaidDate, c.InvoiceDate, c.vendorid, c.siteid, c.costrecordid,
ce.CostExclusionType, cc.CostType, 
CASE WHEN invoicepdflink IS NULL THEN '0' ELSE '1' END,
CASE WHEN c.recordsourceid IN (10,11,12) THEN '1' ELSE '0' END


SELECT s.CostRecordID, s.SiteName, s.VendorName, s.DocumentType, s.ExclP2, s.StateAbb, s.Country, s.RecordSource, s.InvoicePDFLink, 
s.RecordSourceID, s.CostTypeID, s.DateBilled, s.WorkDesription, s.Amount, s.PaidDate, s.InvoiceDate, s.vendorid, s.siteid, s.ExclusionReason, 
s.Category, s.InvPDFLink, s.BatesMin, s.BatesMax, s.Excluded, Notes
FROM @Summary s
INNER JOIN tblSiteCosts sc ON s.costrecordid = sc.costrecordid

ORDER BY s.SiteName, s.Amount

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSiteCostsQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSiteCostsQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSiteCostsQC] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSiteCostsQC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSiteCostsQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSiteCostsQC] TO [power_user]
GO
