SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmDefendantCompany]

as

Select DefendantCompanyID, DefendantCompany
From tblDefendantCompany
Order By DefendantCompany
Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefendantCompany] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDefendantCompany] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefendantCompany] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmDefendantCompany] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefendantCompany] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDefendantCompany] TO [power_user]
GO
