SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  Procedure [dbo].[qryrptBillPoliciesByOcc] (@LimitTypeID int = null, 
												  @BillGroupID int = null, 
												  @BillNumber int = null)
AS

SET NOCOUNT ON

--DECLARE @LimitTypeID int
--DECLARE @BillGroupID int
--DECLARE @BillNumber datetime
--SET @LimitTypeID = 2
--SET @BillGroupID = 0
--SET @BillNumber = 0

DECLARE @maxLimitTypeID int
DECLARE @maxBillGroupID int
DECLARE @maxBillNumber int
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxBillGroupID = MAX(BillGroupID) FROM vBilling
SELECT @maxBillNumber = MAX(BillNumber) FROM vBilling

DECLARE @Policy TABLE (PolicyID int, 
					   PolicyDateID int, 
					   CarrierName nvarchar(255),
					   PolicyNum nvarchar(100), 
					   PStartDate datetime, 
					   PEndDate datetime, 
					   AnnualPeriod int, 
					   Layer int, 
					   SIR money, 
					   AttachmentPoint money, 
					   PerOccLimit money, 
					   AggregateLimit money, 
					   LayerPerOccLimit money)
INSERT INTO @Policy
	SELECT PolicyID, PolicyDateID, CarrierName, PolicyNum, PStartDate, PEndDate, AnnualPeriod, Layer, SIR, 
		AttachmentPoint, PerOccLimit, AggregateLimit, LayerPerOccLimit
	FROM vPolicy p
	WHERE p.LimitTypeID >= COALESCE(@LimitTypeID, 0) AND p.LimitTypeID <= COALESCE(NULLIF(@LimitTypeID, 0), @maxLimitTypeID )
	GROUP BY PolicyID, PolicyDateID, CarrierName, PolicyNum, PStartDate, PEndDate, AnnualPeriod, Layer, SIR, 
		AttachmentPoint, PerOccLimit, AggregateLimit, LayerPerOccLimit

SELECT BillNumber, BillDate, a.AllocationID, bil.BillGroupID, BillGroupName, o.ExpectedPayDate,
	bil.PaymentDueDate, pinfo.PolicyID, CarrierName, PolicyNum, PStartDate, PEndDate, AnnualPeriod, 
	Layer, SIR, AttachmentPoint, PerOccLimit, AggregateLimit, LayerPerOccLimit, OccurrenceName, 
	(CASE WHEN COALESCE(a.TriggerStartID,0) <> 0 THEN 
		CASE
			WHEN TriggerStartID = 1 THEN TriggerStart1
			WHEN TriggerStartID = 2 THEN TriggerStart2
			WHEN TriggerStartID = 3 THEN TriggerStart3
			WHEN TriggerStartID = 4 THEN TriggerStart4
		END
		ELSE a.TriggerStartDate END) AS TriggerStart, 
	(CASE WHEN COALESCE(a.TriggerEndID,0) <> 0 THEN
		CASE
			WHEN TriggerEndID = 1 THEN TriggerEnd1
			WHEN TriggerEndID = 2 THEN TriggerEnd2
			WHEN TriggerEndID = 3 THEN TriggerEnd3
			WHEN TriggerEndID = 4 THEN TriggerEnd4
		END
		ELSE a.TriggerEndDate END) AS TriggerEnd, 
	dbo.fnCostsConsidered(e.AllocationID, e.OccurrenceID) AS IndemnityOcc, 
	IndemnityAllocation AS Indemnity, 
	DefenseAllocation AS Defense, 
	dbo.fnDefenseConsidered(e.AllocationID, e.OccurrenceID) AS DefenseOcc, 
	DefenseOutsideLimitsAllocation AS DefenseOutside, 
	SIRIndemnityAllocation AS SIRIndemnity, 
	SIRDefenseAllocation AS SIRDefense
FROM vBilling bil
	INNER JOIN vExhaustion e ON CAST(bil.ExhaustionDate AS date) = CAST(e.ExhaustionDate AS date)
	INNER JOIN vAllocation a ON a.AllocationID = e.AllocationID 
	INNER JOIN vOccurrence o ON e.OccurrenceID = o.OccurrenceID
	INNER JOIN @Policy pinfo ON e.PolicyDateID = pinfo.PolicyDateID
WHERE BillGroupID >= COALESCE(@BillGroupID, 0) AND BillGroupID <= COALESCE(NULLIF(@BillGroupID, 0), @maxBillGroupID)
	AND BillNumber >= COALESCE(@BillNumber, 0) AND BillNumber <= COALESCE(NULLIF(@BillNumber, 0), @maxBillNumber)
GROUP BY BillNumber, BillDate, a.AllocationID, bil.BillGroupID, BillGroupName, o.ExpectedPayDate,
	bil.PaymentDueDate, pinfo.PolicyID, CarrierName, PolicyNum, PStartDate, PEndDate, AnnualPeriod, 
	Layer, SIR, AttachmentPoint, PerOccLimit, AggregateLimit, LayerPerOccLimit, OccurrenceName, 
	(CASE WHEN COALESCE(a.TriggerStartID,0) <> 0 THEN 
		CASE
			WHEN TriggerStartID = 1 THEN TriggerStart1
			WHEN TriggerStartID = 2 THEN TriggerStart2
			WHEN TriggerStartID = 3 THEN TriggerStart3
			WHEN TriggerStartID = 4 THEN TriggerStart4
		END
		ELSE a.TriggerStartDate END), 
	(CASE WHEN COALESCE(a.TriggerEndID,0) <> 0 THEN
		CASE
			WHEN TriggerEndID = 1 THEN TriggerEnd1
			WHEN TriggerEndID = 2 THEN TriggerEnd2
			WHEN TriggerEndID = 3 THEN TriggerEnd3
			WHEN TriggerEndID = 4 THEN TriggerEnd4
		END
		ELSE a.TriggerEndDate END), 
	dbo.fnCostsConsidered(e.AllocationID, e.OccurrenceID), 
	IndemnityAllocation, 
	DefenseAllocation, 
	dbo.fnDefenseConsidered(e.AllocationID, e.OccurrenceID), 
	DefenseOutsideLimitsAllocation, 
	SIRIndemnityAllocation, 
	SIRDefenseAllocation
ORDER BY BillGroupName, BillNumber, CarrierName, PolicyNum, OccurrenceName, TriggerStart, TriggerEnd

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillPoliciesByOcc] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBillPoliciesByOcc] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillPoliciesByOcc] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptBillPoliciesByOcc] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBillPoliciesByOcc] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBillPoliciesByOcc] TO [power_user]
GO
