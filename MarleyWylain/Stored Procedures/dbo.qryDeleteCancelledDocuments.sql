SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryDeleteCancelledDocuments]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

IF object_id('tempdb..#DocsToDelete') IS NOT NULL
BEGIN
   DROP TABLE #DocsToDelete
END
CREATE TABLE #DocsToDelete (DocID bigint)
INSERT INTO #DocsToDelete
SELECT DocumentID
FROM tblDocuments
WHERE UploadCompleted = 0
	AND DateCreated < GETDATE()-2


DELETE FROM tblMultiClaimantDocument
WHERE DocumentID IN (SELECT * FROM #DocsToDelete)

DELETE FROM tblDocuments
WHERE DocumentID IN (SELECT * FROM #DocsToDelete)


END
GO
