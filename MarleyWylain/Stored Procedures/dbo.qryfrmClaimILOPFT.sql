SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Procedure [dbo].[qryfrmClaimILOPFT] (@ClaimID int)
As
Set NoCount On

Select ClaimID, ILODate, ILODoctorID, ILOProfusion, PFTDate, PFTDLCO_Actual, PFTDoctorID, PFTFEV1_Actual, 
PFTFEV1AndFVC_Actual, PFTFVC_Actual, PFTTLC_Actual, PFTTV, PFTTypeID
From tblClaim c
Where ClaimID = @ClaimID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimILOPFT] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimILOPFT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimILOPFT] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimILOPFT] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimILOPFT] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimILOPFT] TO [power_user]
GO
