SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmMaritalStatus]
as

Select MaritalStatusID, MaritalStatus
From tblMaritalStatus
Order By MaritalStatus

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMaritalStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmMaritalStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMaritalStatus] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmMaritalStatus] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMaritalStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmMaritalStatus] TO [power_user]
GO
