SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmrptAllocationOccurrenceToExclude]
(@allocationID int = NULL)
AS
SET NOCOUNT ON
IF @allocationID IS NULL
	SELECT AllocationID, OccurrenceID
	FROM tblAllocationOccurrenceExclude
ELSE
	SELECT AllocationID, OccurrenceID
	FROM tblAllocationOccurrenceExclude
	WHERE AllocationID = @allocationID
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmrptAllocationOccurrenceToExclude] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmrptAllocationOccurrenceToExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmrptAllocationOccurrenceToExclude] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmrptAllocationOccurrenceToExclude] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmrptAllocationOccurrenceToExclude] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmrptAllocationOccurrenceToExclude] TO [power_user]
GO
