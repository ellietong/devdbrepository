SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[qryfrmViewSettlementDocuments] (@ClaimLawsuitID int)
as

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 17075


SELECT sd.DocumentID,
case when DateCreated is NULL then DocCategory else UPPER(DocCategory) + ': ' + DocumentTitle + ' - ' + convert(nvarchar,month(DateCreated))+'/'+convert(nvarchar,day(DateCreated))+
	'/'+convert(nvarchar,Year(DateCreated)) end  + coalesce(' (' + pagenums + ')', '')  AS DocumentTypeWithDate,
	CASE WHEN SUBSTRING(DocumentURL,8,8) = 'extranet' THEN 'https://admin' + SUBSTRING(DocumentURL,17,LEN(DocumentURL)-15) 
	WHEN SUBSTRING(DocumentURL,9,8) = 'extranet' THEN 'https://admin' + SUBSTRING(DocumentURL,17,LEN(DocumentURL)-15) 
	ELSE DocumentURL END AS DocumentLink
FROM tblSettleDocs sd
INNER JOIN tblSettlePaymentDocumentation pd ON sd.SetPymtID = pd.SetPymtID
LEFT JOIN tblDocumentCategory cd ON sd.DocCategoryID = cd.DocCategoryID
LEFT JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblDocuments d ON sd.DocumentID = d.DocumentID 
inner join tblMultiClaimantDocument mcd on mcd.DocumentID=d.DocumentID and mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
WHERE pd.ClaimLawsuitID = @ClaimLawsuitID
ORDER BY DocumentTypeWithDate

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewSettlementDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewSettlementDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewSettlementDocuments] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmViewSettlementDocuments] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewSettlementDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewSettlementDocuments] TO [power_user]
GO
