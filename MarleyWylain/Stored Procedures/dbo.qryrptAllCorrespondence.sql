SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[qryrptAllCorrespondence]
(@CorrespondenceDate datetime = NULL, @CorrespondenceTypeID INT = null, @ContactMode int = NULL, @site varchar(200) = NULL)

as
set nocount on

--DECLARE @CorrespondenceDate datetime
--DECLARE @CorrespondenceTypeID int
--DECLARE @ContactMode int
--DECLARE @Site int

--SET @CorrespondenceDate= '1/1/1900'
--SET @CorrespondenceTypeID = 0
--SET @ContactMode = 0
--SET @site = 0

Declare @MaxcorrespondenceTypeID int
DECLARE @MaxContactModeID int
DECLARE @MaxSiteID Int
SELECT @MaxSiteID = MAX(siteid) from tblenvirositesdetail
SELECT @MaxcorrespondenceTypeID = MAX(correspondencetypeid) from tblCorrespondenceType
SELECT @MaxContactModeID = MAX(correspondencemodeid) from tblCorrespondenceMode

select cor.correspondenceid, correspondencedate, cor.correspondencetypeid, ct.correspondencetype, 
	cor.correspondencemodeid, cm.correspondencemode, notes, attachments, confirmreceipt, SiteName,
	Company, FirstName + ' ' + LastName AS 'ContactFullName', CarrierName
from tblcorrespondence cor 
	left join tblcorrespondencemode cm on cor.correspondencemodeid = cm.correspondencemodeid
	left join tblcorrespondencetype ct on cor.correspondencetypeid = ct.correspondencetypeid
	left join tblCorrCarrierContact ccc ON cor.CorrespondenceID = ccc.CorrespondenceID
	left join tblCarrierContact cc ON cc.CarrierContactID = ccc.CarrierContactID
	left join tblcarrier carrier on carrier.carrierid = cc.carrierid
	left join tblContact c ON cc.ContactID = c.ContactID
	left join tblSiteNoticed sn on cor.CorrespondenceID = sn.CorrespondenceID
	left join tblEnviroSitesDetail s on sn.SiteID = s.SiteID
Where cor.CorrespondenceDate >= @correspondencedate and cor.correspondencedate <= COALESCE(NULLIF(@CorrespondenceDate, '1/1/1900'), getdate())
AND cor.CorrespondenceTypeID >= coalesce(@correspondenceTypeID, 0) and cor.CorrespondenceTypeID <= coalesce(nullif(@correspondencetypeID, 0), @MaxCorrespondenceTypeID)
AND cm.CorrespondenceModeID >= coalesce(@contactmode, 0) and cm.CorrespondenceModeID <= coalesce(nullif(@contactmode, 0), @maxcontactmodeid)
and cor.CorrespondenceID in (select CorrespondenceID from tblSiteNoticed 
	where SiteID>= coalesce(@site, 0) and SiteID <= coalesce(nullif(@site, 0), @maxsiteid))


order by correspondencedate

return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllCorrespondence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllCorrespondence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllCorrespondence] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptAllCorrespondence] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllCorrespondence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllCorrespondence] TO [power_user]
GO
