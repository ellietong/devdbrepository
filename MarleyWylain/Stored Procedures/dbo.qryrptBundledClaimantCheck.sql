SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptBundledClaimantCheck] (@ClaimantPersonalInfoID bigint)

AS
SET NOCOUNT ON


SELECT ip.InjuredPartyID, ip.FirstName, ip.MiddleName, ip.LastName, ip.SSN, ip.BirthDate,
CASE WHEN ip.FirstName LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1 
WHEN FirstName LIKE (SELECT MiddleName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1
ELSE 0 END AS FlagFirstName,
CASE WHEN ip.MiddleName LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1 
WHEN MiddleName LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1
ELSE 0 END AS FlagMiddleName,
CASE WHEN ip.LastName LIKE (SELECT LastName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END AS FlagLastName,
CASE WHEN ip.SSN LIKE (SELECT SocialSecurityNumber FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END AS FlagSocialSecurityNumber,
CASE WHEN ip.BirthDate LIKE (SELECT BirthDate FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END AS FlagBirthDate
FROM mtblBundledInjuredParty ip
WHERE (((((LEFT(ip.FirstName,3) LIKE (SELECT LEFT(FirstName,3) FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) OR
LEFT(ip.MiddleName,3) LIKE (SELECT LEFT(FirstName,3) FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)) OR
(LEFT (ip.FirstName,3) LIKE (SELECT LEFT(MiddleName,3) FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID))))
AND ip.LastName LIKE (SELECT LastName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)) OR
(ip.SSN LIKE (SELECT SocialSecurityNumber FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)) OR
(ip.BirthDate = (SELECT BirthDate FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)))

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBundledClaimantCheck] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBundledClaimantCheck] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBundledClaimantCheck] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptBundledClaimantCheck] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBundledClaimantCheck] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBundledClaimantCheck] TO [power_user]
GO
