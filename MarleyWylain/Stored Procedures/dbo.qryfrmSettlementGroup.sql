SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmSettlementGroup] as

Select *
From tblSettlementGroup
Order By SettlementGroup

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlementGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlementGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlementGroup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmSettlementGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlementGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlementGroup] TO [power_user]
GO
