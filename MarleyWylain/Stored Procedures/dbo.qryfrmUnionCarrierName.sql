SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmUnionCarrierName]
AS
SET NOCOUNT ON

SELECT CarrierID, CarrierName
FROM tblCarrier
where carrierid in (select carrierid from tblpolicy)
UNION 

SELECT  0, '<<All Carriers>>' AS CarrierName 
FROM tblCarrier

Union 
Select CarrierID, CarrierName
From tblCarrier_ALT

ORDER BY CarrierName

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionCarrierName] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionCarrierName] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionCarrierName] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmUnionCarrierName] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionCarrierName] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionCarrierName] TO [power_user]
GO
