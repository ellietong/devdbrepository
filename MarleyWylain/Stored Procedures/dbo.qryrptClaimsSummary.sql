SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[qryrptClaimsSummary] (@Start nvarchar(250), @End nvarchar(250), @MatterID int) 
AS
SET NOCOUNT ON

--DECLARE @Start nvarchar(250)
--DECLARE @End nvarchar(250)
--SET @Start = '1/1/1900'
--SET @End = '1/1/2012'
--declare @MatterID int
--set @MatterID = null

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter
 
DECLARE @newFilings TABLE(filedate nvarchar(250), claimnumber int)
INSERT INTO @newFilings
	SELECT YEAR(COALESCE(servicedate,filedate,statusdate)) AS filedate1, count(c.ClaimID) AS ClaimNumber
	FROM tblClaimantPersonalInfo i 
		  LEFT JOIN tblClaim c ON i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit l ON l.claimID = c.claimID
		  LEFT JOIN tblLawsuit law ON l.lawsuitid = law.lawsuitid
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=l.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
	WHERE IsPrimaryForClaim = 1
	AND clst.IsPrimaryStatus=1
		  AND COALESCE(servicedate,filedate,statusdate) <= @End AND COALESCE(servicedate,filedate,statusdate)>= @start
	GROUP BY YEAR(COALESCE(servicedate,filedate,statusdate))
	ORDER BY YEAR(COALESCE(servicedate,filedate,statusdate))

DECLARE @ClaimSummary TABLE(Matter nvarchar(250), Statusdate nvarchar(250), SBNDAmount money, SettlementAmount money, Inactive_Count int, DBnd_Count int, Dismissed_Count int, SBND_Count int, Settled_Count int)
INSERT INTO @ClaimSummary
	SELECT Matter,
		YEAR(COALESCE(SettlementDate,statusdate)) AS StatusDate,
		SUM(CASE WHEN cls.ClaimLawsuitStatusID = 25			THEN COALESCE(SettlementAmount,0) ELSE 0 END) AS SBNDAmount,
		SUM(CASE WHEN cls.ClaimLawsuitStatusID = 23			THEN COALESCE(SettlementAmount,0) ELSE 0 END) AS SettlementAmount,
		SUM(CASE WHEN cls.ClaimLawsuitStatusID = 21			THEN 1							  ELSE 0 END) AS Inactive_Count,
		SUM(CASE WHEN cls.ClaimLawsuitStatusID = 17			THEN 1							  ELSE 0 END) AS DBnd_Count,
		SUM(CASE WHEN cls.ClaimLawsuitStatusID IN(15,16,27) THEN 1							  ELSE 0 END) AS Dismissed_Count,
		SUM(CASE WHEN cls.ClaimLawsuitStatusID = 25			THEN 1							  ELSE 0 END) AS SBND_Count,
		SUM(CASE WHEN cls.ClaimLawsuitStatusID IN(23,28)    THEN 1							  ELSE 0 END) AS Settled_Count
	FROM tblClaimantPersonalInfo i 
		  LEFT JOIN tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		  LEFT JOIN tblClaimLawsuit l on l.claimID = c.claimID
		  LEFT JOIN tblLawsuit law on l.lawsuitid = law.lawsuitid
		  LEFT JOIN tblCLaimantSettlement s on s.claimlawsuitID = l.claimlawsuitID
		  LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=l.ClaimLawsuitID
		  LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
		  Left JOIN tblMatter m on m.MatterID=c.MatterID
	WHERE IsPrimaryForClaim = 1
	AND clst.IsPrimaryStatus=1
		  AND COALESCE(SettlementDate,statusdate) >= @Start 
		  AND COALESCE(SettlementDate,statusdate) <= @End
		  and m.MatterID >= COALESCE(@MatterID, 0) AND m.MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
	GROUP BY Matter, YEAR(COALESCE(SettlementDate,statusdate))
	ORDER BY Matter, YEAR(COALESCE(SettlementDate,statusdate))

SELECT Matter,
	YEAR(COALESCE(statusdate,filedate))AS date1,
	COALESCE(SettlementAmount,0) AS SettlementAmount,
	COALESCE(SbndAmount,0)       AS SbndAmount,
	COALESCE(claimnumber, 0)     AS File_Count,
	COALESCE(Inactive_Count,0)   AS Inactive_Count,
	COALESCE(DBnd_Count,0)       AS DBnd_Count,
	COALESCE(Dismissed_Count,0)  AS Dismissed_Count,
	COALESCE(SBND_Count,0)       AS SBND_Count,
	COALESCE(Settled_Count,0)    AS Settled_Count, 
	@Start AS StartDate, 
	@End   AS EndDate
FROM @claimSummary s
	LEFT JOIN @newFilings f ON s.Statusdate = f.filedate
ORDER BY Matter, YEAR(COALESCE(statusdate,filedate))

RETURN




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimsSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimsSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimsSummary] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptClaimsSummary] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimsSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimsSummary] TO [power_user]
GO
