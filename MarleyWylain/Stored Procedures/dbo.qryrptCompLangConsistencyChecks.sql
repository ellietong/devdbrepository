SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qryrptCompLangConsistencyChecks]
(@selectedProvisions nvarchar(1000), @selectedProvisionValues nvarchar(1000), @limittypeid int )
AS 

SET NOCOUNT ON


----Declare the lists of selected IDs; note: these will be passed in, and thus should be commented out in actual stored proc
--DECLARE @selectedProvisions nvarchar(1000)
--SET @selectedProvisions = '1'
--DECLARE @selectedProvisionValues nvarchar(1000)
--SET @selectedProvisionValues = '0'
--declare @limittypeid int
--set @limittypeid = 1


--Declare backup strings of the passed in lists to be used in the ultimate where clauses below
DECLARE @selectedProvisionsList nvarchar(1000)
SET @selectedProvisionsList = @selectedProvisions
DECLARE @selectedProvisionValuesList nvarchar(1000)
SET @selectedProvisionValuesList = @selectedProvisionValues

--Declare the tables to be used for the separated provision and provision value IDs
DECLARE @selectedProvisionsTable TABLE(provisionLabelID int)
DECLARE @selectedProvisionValuesTable TABLE(provisionValueID int)

--Prepare flags if there is only a zero in the table (and thus there should be no filter)
DECLARE @flagProvisionFilter int
DECLARE @flagProvisionValueFilter int
SET @flagProvisionFilter = 0
SET @flagProvisionValueFilter = 0

--Parse out provision and provision value ID strings	
WHILE LEN(@selectedProvisions)>0 BEGIN
	IF CHARINDEX(',',@selectedProvisions) = 0 BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, @selectedProvisions)
		IF CONVERT(int,@selectedProvisions) <> 0
			SET @flagProvisionFilter = 1
		SET @selectedProvisions = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, LEFT(@selectedProvisions, CHARINDEX(',',@selectedProvisions)-1))
		SET @selectedProvisions = RIGHT(@selectedProvisions,LEN(@selectedProvisions)-CHARINDEX(',',@selectedProvisions))
		SET @flagProvisionFilter = 1
	END
END
WHILE LEN(@selectedProvisionValues)>0 BEGIN
	IF CHARINDEX(',',@selectedProvisionValues) = 0 BEGIN
		INSERT INTO @selectedProvisionValuesTable
			SELECT CONVERT(int, @selectedProvisionValues)
		IF CONVERT(int,@selectedProvisionValues) <> 0
			SET @flagProvisionValueFilter = 1
		SET @selectedProvisionValues = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedProvisionValuesTable
			SELECT CONVERT(int, LEFT(@selectedProvisionValues, CHARINDEX(',',@selectedProvisionValues)-1))
		SET @selectedProvisionValues = RIGHT(@selectedProvisionValues,LEN(@selectedProvisionValues)-CHARINDEX(',',@selectedProvisionValues))
		SET @flagProvisionValueFilter = 1
	END
END

IF @flagProvisionFilter = 1 AND @flagProvisionValueFilter = 1
SELECT     el.ExactLanguage, pv.ProvisionValue, c.CarrierName, d.PStartDate,  
l.AttachmentPoint, pr.provisionlabelid, d.penddate, pv.provisionvalueid
FROM         tblPolicy p 
INNER JOIN  tblCarrier c ON p.CarrierID = c.CarrierID 
INNER JOIN  tblProvision pr ON p.PolicyID = pr.PolicyID 
inner join tblexactlanguage el on el.policyid = p.PolicyID
INNER JOIN  tblProvisionLanguage pl ON el.exactlanguageid=pl.ExactLanguageID
					AND  pl.provisionlabelid = pr.provisionlabelid
INNER JOIN tblPolicyLimits l ON p.PolicyID = l.PolicyID 
INNER JOIN tblPolicyDates d ON p.PolicyID = d .PolicyID 
INNER JOIN tblProvisionValue pv ON pr.ProvisionValueID = pv.ProvisionValueID

WHERE     
(l.LimitTypeID = @limittypeid) 
AND (el.ExactLanguage IS NOT NULL)
AND pr.ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) AND 
	(pr.ProvisionValueID IN(SELECT * FROM @selectedProvisionValuesTable) OR 
	(pr.ProvisionValueID IN(SELECT ProvisionValueID FROM tblProvisionValue WHERE ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) AND ProvisionLabelID NOT IN(SELECT ProvisionLabelID FROM tblProvisionValue WHERE ProvisionValueID IN(@selectedProvisionValuesList)))))

ELSE IF @flagProvisionFilter = 1
SELECT     el.ExactLanguage, pv.ProvisionValue, c.CarrierName, d.PStartDate,  
l.AttachmentPoint, pr.provisionlabelid, d.penddate, pv.provisionvalueid
FROM         tblPolicy p 
INNER JOIN  tblCarrier c ON p.CarrierID = c.CarrierID 
INNER JOIN  tblProvision pr ON p.PolicyID = pr.PolicyID 
inner join tblexactlanguage el on el.policyid = p.PolicyID
INNER JOIN  tblProvisionLanguage pl ON el.exactlanguageid=pl.ExactLanguageID
					AND  pl.provisionlabelid = pr.provisionlabelid
INNER JOIN tblPolicyLimits l ON p.PolicyID = l.PolicyID 
INNER JOIN tblPolicyDates d ON p.PolicyID = d .PolicyID 
INNER JOIN tblProvisionValue pv ON pr.ProvisionValueID = pv.ProvisionValueID

WHERE     
(l.LimitTypeID = @limittypeid) 
AND (el.ExactLanguage IS NOT NULL) AND pr.ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable)

ELSE		   
SELECT     el.ExactLanguage, pv.ProvisionValue, c.CarrierName, d.PStartDate,  
l.AttachmentPoint, pr.provisionlabelid, d.penddate, pv.provisionvalueid
FROM         tblPolicy p 
INNER JOIN  tblCarrier c ON p.CarrierID = c.CarrierID 
INNER JOIN  tblProvision pr ON p.PolicyID = pr.PolicyID 
inner join tblexactlanguage el on el.policyid = p.PolicyID
INNER JOIN  tblProvisionLanguage pl ON el.exactlanguageid=pl.ExactLanguageID
					AND  pl.provisionlabelid = pr.provisionlabelid
INNER JOIN tblPolicyLimits l ON p.PolicyID = l.PolicyID 
INNER JOIN tblPolicyDates d ON p.PolicyID = d .PolicyID 
INNER JOIN tblProvisionValue pv ON pr.ProvisionValueID = pv.ProvisionValueID

WHERE     
(l.LimitTypeID = @limittypeid) 
AND (el.ExactLanguage IS NOT NULL)

                      RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCompLangConsistencyChecks] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCompLangConsistencyChecks] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCompLangConsistencyChecks] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCompLangConsistencyChecks] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCompLangConsistencyChecks] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCompLangConsistencyChecks] TO [power_user]
GO
