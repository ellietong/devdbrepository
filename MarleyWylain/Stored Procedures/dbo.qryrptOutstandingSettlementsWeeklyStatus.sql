SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptOutstandingSettlementsWeeklyStatus] (@SettlementDate datetime)

As

SET NOCOUNT ON

--DECLARE @SettlementDate datetime
--SET @SettlementDate = '1/1/2010'

DECLARE @SettlementGroupPaidUnpaid TABLE (SettlementGroupID int, Paid money, Unpaid money, Total money)
INSERT INTO @SettlementGroupPaidUnpaid
	SELECT SettlementGroupID, 
	SUM( CASE WHEN paiddate IS NULL THEN 0 ELSE SettlementAmount END) As Paid,
	SUM( CASE WHEN paiddate IS NULL THEN SettlementAmount ELSE 0 END) as Unpaid, 
	SUM(SettlementAmount) as Total
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupCount TABLE (SettlementGroupID int, ClaimCount nvarchar(50))
INSERT INTO @SettlementGroupCount	
	SELECT SettlementGroupID, COUNT(SettlementDate) as ClaimCount
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupPaidCount TABLE (SettlementGroupID int, PaidCount nvarchar(50))
INSERT INTO @SettlementGroupPaidCount	
	SELECT SettlementGroupID, COUNT(paiddate) as PaidCount
	FROM tblClaimantSettlement
	WHERE SettlementGroupID IN	(SELECT SettlementGroupID
								 FROM tblClaimantSettlement
								 WHERE paiddate IS NOT NULL 
								 GROUP BY SettlementGroupID)
	GROUP BY SettlementGroupID
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupMaxDate TABLE (SettlementGroupID int, MaxSettlementDate datetime)
INSERT INTO @SettlementGroupMaxDate
	SELECT SettlementGroupID, MAX(SettlementDate) as MaxSettlementDate
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID
	ORDER BY MaxSettlementDate


SELECT pca.PlaintiffCounsel as PrimaryLocalPlaintiffCounsel , cs.SettlementDate, PaymentDueDate as DatePayable, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, CheckNumber, ReleaseSignedDate, FullyDocumented, cl.LawsuitID, paiddate,
	@SettlementDate AS SettlementDateAsOf, sgp.Paid, sgp.Unpaid, DocketNumber, COALESCE(sgpc.PaidCount,0) AS PaidCount, sgc.ClaimCount, MaxSettlementDate,
	CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
		 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END AS SupportingDocumentation,
	CASE WHEN ReleaseRcvd = 1
		 THEN coalesce( crr.ReleaseType,'Executed' )
		 ELSE 'Release Missing'  END AS ReleaseExecution, convert(datetime,GETDATE() - 7) as weeklystatusdate
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs			 ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INner JOIN tblLawsuit l						 oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaimLawsuitStatus cls		 ON cl.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID	
	LEFT  JOIN tblSettlementGroup s				 ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblClaimantRelease cr			 ON cs.ClaimantSettlementID = cr.ClaimantSettlementID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT  JOIN @SettlementGroupPaidUnpaid sgp	 ON s.SettlementGroupID = sgp.SettlementGroupID
	LEFT  JOIN @SettlementGroupCount sgc		 ON s.SettlementGroupID= sgc.SettlementGroupID
	LEFT  JOIN @SettlementGroupPaidCount sgpc	 ON s.SettlementGroupID = sgpc.SettlementGroupID
	LEFT  JOIN @SettlementGroupMaxDate sgm		 ON s.SettlementGroupID = sgm.SettlementGroupID
	LEFT JOIN  tblReleasetype crr			 ON crr.ReleaseTypeID =cr.ReleaseTypeID
	LEFT JOIN tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID 
WHERE COALESCE(IsCurrentClaim,0) = 1
	and PaidDate is null
	AND COALESCE(PaidCount,0) <> COALESCE(ClaimCount,0) and PaymentDueDate is not null and SettlementDate >=@SettlementDate and CONVERT(int, GETDATE() - PaymentDueDate) <=7 and CONVERT(int, GETDATE() - PaymentDueDate)>= 0
GROUP BY cs.SettlementDate, PaymentDueDate, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, CheckNumber, ReleaseSignedDate, FullyDocumented, cl.LawsuitID,paiddate, pca.PlaintiffCounsel,
	sgp.Paid, sgp.Unpaid, DocketNumber, sgpc.PaidCount, sgc.ClaimCount, MaxSettlementDate, ReleaseRcvd, GETDATE() - PaymentDueDate ,

CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
			 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END,
	CASE WHEN ReleaseRcvd = 1
		 THEN coalesce(crr.ReleaseType,'Executed' ) 
		 ELSE 'Release Missing'  END
ORDER BY MaxSettlementDate, SettlementGroup, SettlementDate, LastName




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlementsWeeklyStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlementsWeeklyStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlementsWeeklyStatus] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlementsWeeklyStatus] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlementsWeeklyStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlementsWeeklyStatus] TO [power_user]
GO
