SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmClaimInfo] (@ClaimantPersonalInfoID int)
as

Select *
From tblClaim c 
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By ClaimSequenceID

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimInfo] TO [power_user]
GO
