SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryBILLAddPayment]
(@BillGroupID int, @CarrierID int, @SubscriberID int, @PaymentDate Date, @CheckNumber char(10), @Payment money, @Notes nvarchar(500))

AS
SET NOCOUNT 
ON

-- Procedure adds a Ledger record and a payment associated with that record

-- Insert data to tblLedger
INSERT INTO tblLedger (BillGroupID, CarrierID, SubscriberID, LedgerTypeID, LedgerDate, 
					   CheckNumber, Debit, Credit, ExhaustionDateID, BillID, LedgerNote)
VALUES (@BillGroupID, @CarrierID, @SubscriberID, 2, @PaymentDate, 
		@CheckNumber, 0, @Payment, null, null, @Notes)

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryBILLAddPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryBILLAddPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryBILLAddPayment] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryBILLAddPayment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryBILLAddPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryBILLAddPayment] TO [power_user]
GO
