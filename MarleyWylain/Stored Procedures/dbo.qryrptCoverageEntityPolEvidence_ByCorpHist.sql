SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptCoverageEntityPolEvidence_ByCorpHist]
AS 
SET NOCOUNT ON

--Updated: RLS 7/11/07 to group by corporate history profiles

--PastCosts
declare @Amount table (SiteID int, SiteName nvarchar (150), state nvarchar(10), country nvarchar(50), Billed money,
 Adtl_Amt money)
insert into @Amount
select s.siteid, sd.sitename, t.state, c.country,
CASE WHEN (Billed = 1 )THEN sum(amount) ELSE 0 END as Billed,
CASE WHEN ((Billed <>1) AND recordsourceid NOT IN (10,11, 12)) THEN sum(amount) ELSE 0 END as AdtlAmt
from tblSiteCosts s
left Join tblEnviroSitesDetail SD on s.siteid = sd.siteid
left join tblstate t on t.stateid = sd.stateid
left join tblcountry c on c.countryid = sd.countryid
where sd.excludedsite = 0
group by s.siteid, sd.sitename, RecordSourceID, billed, t.state, c.country

declare @PastSite table (SiteID int,  Bill money,  Adtl money)
insert into @PastSite
SELECT SiteID, SUM(billed) as BilledAmt,
	Sum(Adtl_Amt) as AdtlAmt
FROM @Amount
GROUP BY SiteID

--Future Costs
DECLARE @FutureCosts table (SiteID int, FMLiab decimal(18,2), FutureCosts money, PVD money, NRD money)
INSERT INTO @FutureCosts
SELECT fc.SiteID, 
CASE WHEN fc.LiabPercent IS NULL THEN 0 ELSE fc.LiabPercent END AS LiabPercent, 
SUM(CASE WHEN sd.sitetypeid = 4 THEN fc.TotalCosts
	ELSE(fc.TotalCosts * fc.LiabPercent) 
	END) AS FutureCosts,
sum(fc.PVD * fc.LiabPercent) AS 'PVD', sum(fc.NRD * fc.LiabPercent) AS 'NRD'
FROM tblFutureCosts fc
LEFT JOIN  tblEnviroSitesDetail sd on fc.SiteID = sd.SiteID
WHERE sd.ExcludedSite = 0
GROUP BY fc.SiteID, CASE WHEN fc.LiabPercent IS NULL THEN 0 ELSE fc.LiabPercent END


SELECT  sd.corporatehistoryid, hp.corporatehistoryprofile,
sum(CASE WHEN p.Bill is null then 0 else p.Bill end +
CASE WHEN Adtl is null then 0 else Adtl end + 
CASE when FC.futurecosts is null then 0 else fc.futurecosts END +
case when fc.pvd is null then 0 else fc.pvd end +
case when fc.nrd is null then 0 else fc.nrd end) as ProgramTotal
FROM tblEnviroSitesDetail SD
LEFT JOIN tblCorporateHistoryProfile hp ON sd.corporatehistoryid = hp.corporatehistoryid
--LEFT JOIN vOccurrence o ON c.corporatehistoryid = o.corporatehistoryid
LEFT JOIN @PastSite P on p.siteid = Sd.siteid
LEFT JOIN @FutureCosts FC on FC.siteid = sd.siteid
where hp.corporatehistoryprofile IS NOT NULL AND sd.excludedsite <> 1
GROUP BY sd.corporatehistoryid, hp.corporatehistoryprofile

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCoverageEntityPolEvidence_ByCorpHist] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCoverageEntityPolEvidence_ByCorpHist] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCoverageEntityPolEvidence_ByCorpHist] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCoverageEntityPolEvidence_ByCorpHist] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCoverageEntityPolEvidence_ByCorpHist] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCoverageEntityPolEvidence_ByCorpHist] TO [power_user]
GO
