SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE procedure [dbo].[qryfrmUnionMultInsProgPolicies]
as
set nocount on
select p.policyid, c.carriername, i.insuranceprogram, p.policynum, 
        c.carriername + ' ' + p.policynum + ' ' + '(' + i.insuranceprogram + ')' as 'Policies', 
        min(pd.pstartdate) as 'Min Start Date', max(pd.penddate) as 'Max End Date'
from tblpolicy p inner join tblpolicydates pd on p.policyid = pd.policyid 
        inner join tblinsuranceprogram i on p.insuranceprogramid = i.insuranceprogramid
        inner join tblcarrier c on p.carrierid = c.carrierid
where (c.carrierid <> 10 and c.carrierid <> 152)
group by p.policyid, c.carriername, i.insuranceprogram, p.policynum, c.carriername + ' ' + p.policynum + ' ' + '(' + i.insuranceprogram + ')'
order by c.carriername, p.policynum
return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionMultInsProgPolicies] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionMultInsProgPolicies] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionMultInsProgPolicies] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmUnionMultInsProgPolicies] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionMultInsProgPolicies] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionMultInsProgPolicies] TO [power_user]
GO
