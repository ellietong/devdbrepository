SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmExhaustion]
(@exhaustionDateID int = null)
AS
SET NOCOUNT ON
SELECT ExhaustionID AS ID, ExhaustionDate AS Date, AllocationID, OccurrenceID, PolicyDateID, IndemnityAllocation, DefenseAllocation, 
	DefenseOutsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation, LedgerID 
FROM tblExhaustion INNER JOIN tblExhaustionDate ON tblExhaustion.ExhaustionDateID = tblExhaustionDate.ExhaustionDateID
WHERE tblExhaustion.ExhaustionDateID = @exhaustionDateID
ORDER BY tblExhaustion.ExhaustionDateID, AllocationID, LedgerID, ExhaustionID
/*
IF @allocationID = -1
	SET @strSQL = @strSQL + ' WHERE AllocationID IS NULL'
ELSE
	SET @strSQL = @strSQL + ' WHERE AllocationID = ' + @allocationID
*/
--SET @strSQL = @strSQL + ' AND ExhaustionDate = ''' + CONVERT(nvarchar(50), @exhaustionDate, 101) + ''' AND LedgerID '
--SET @strSQL = @strSQL + ' AND ExhaustionDate = ''' + CONVERT(nvarchar(50), @exhaustionDate, 101) + ''''
/*
IF @ledgerID = -1
	SET @strSQL = @strSQL + ' IS NULL'
ELSE
	SET @strSQL = @strSQL + ' = ' + @ledgerID
*/
--SET @strSQL = @strSQL + ' ORDER BY AllocationID, LedgerID, ExhaustionID'
--EXEC sp_executesql @strSQL
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustion] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustion] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustion] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustion] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustion] TO [power_user]
GO
