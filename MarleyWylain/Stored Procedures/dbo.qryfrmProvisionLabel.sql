SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmProvisionLabel]
As

SET NOCOUNT ON

SELECT ProvisionLabelID, Provision
FROM tblProvisionLabel
ORDER BY Provision

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionLabel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionLabel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionLabel] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionLabel] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionLabel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionLabel] TO [power_user]
GO
