SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmCorpHistInsProg]
As

SET NOCOUNT ON

SELECT *
FROM tblCorpHistInsProgram

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorpHistInsProg] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorpHistInsProg] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorpHistInsProg] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCorpHistInsProg] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorpHistInsProg] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorpHistInsProg] TO [power_user]
GO
