SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[zQCqryOccurrenceManualCheck]
(@allocationid int, @occurrenceid int) 
As

SET NOCOUNT ON

SELECT OccurrenceID, AllocationID, p.PolicyID, ar.PStartSplit, ar.PEndSplit, ar.Allocation, SIRAllocated, AttachmentPoint, SIR, PerOccLimit, LayerPerOccLimit , PerOccLimit / LayerPerOccLimit AS [Percent]
FROM (((tblPolicy p INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID) INNER JOIN tblAllocationResults ar ON p.PolicyID = ar.PolicyID) 
	INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID AND pd.PStartDate = ar.PStartSplit) INNER JOIN tblPolicyLimits pl ON p.PolicyID = pl.PolicyID
WHERE ar.AllocationID LIKE COALESCE(@allocationid, 0)  AND ar.OccurrenceID LIKE COALESCE(@occurrenceid, 0)
GROUP BY p.PolicyID, p.PolicyNum, ar.PStartSplit, ar.PEndSplit, AttachmentPoint, SIR, ar.Allocation, SIRAllocated, PerOccLimit / LayerPerOccLimit, PerOccLimit, LayerPerOccLimit, ar.OccurrenceID, ar.AllocationID
ORDER BY ar.PStartSplit

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[zQCqryOccurrenceManualCheck] TO [base_user]
GRANT EXECUTE ON  [dbo].[zQCqryOccurrenceManualCheck] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryOccurrenceManualCheck] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[zQCqryOccurrenceManualCheck] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryOccurrenceManualCheck] TO [power_user]
GRANT EXECUTE ON  [dbo].[zQCqryOccurrenceManualCheck] TO [power_user]
GO
