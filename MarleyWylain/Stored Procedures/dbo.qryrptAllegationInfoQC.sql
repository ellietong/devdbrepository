SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptAllegationInfoQC] (@CLaimLawsuitID int)
as

Select AllegationID, 
	ClaimLawsuitID, 
	DefendantCompany, 
	AllegationType, 
	Damage, 
	Material
From tblAllegation a 
	Left Join tblDamage d on d.damageID = a.damageID
	Left Join tblAllegationType alt on alt.allegationtypeID = a.allegationtypeID
	Left Join tblMaterial m on m.materialID = a.materialID
	Left Join tblDefendantCompany dc on dc.defendantcompanyID = a.defendantcompanyID
Where ClaimLawsuitID = @ClaimLawsuitID
Order By DefendantCompany

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllegationInfoQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllegationInfoQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllegationInfoQC] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptAllegationInfoQC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllegationInfoQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllegationInfoQC] TO [power_user]
GO
