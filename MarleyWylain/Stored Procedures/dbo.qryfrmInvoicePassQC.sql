SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmInvoicePassQC]
--2/8/12 - update to allow new QC form to work.

AS
BEGIN

-- Invoice Total per document including sum of all claimants
select d.DocumentID, di.Status, di.DefenseCounsel, di.Firm as VendorFirm, di.InvoiceNumber, 
di.Period, di.SubmitDate, di.DatePayable, di.InvoiceLink as DocumentURL, di.ApprovedBy, di.DateApproved, di.InvoiceNotes
, di.InvoiceTotal, di.adjustments, di.fees, coalesce(di.QCBy,'') +' '+ coalesce(di.QCDate,'') as QCBy, ClaimantDocumentType as DocumentType, di.MatterID, AcceptedBy, DateAccepted, CreatedBy, 
DateCreated, DetailDocumentTypeID, Comments,di.costs,'https://admin.kcicextranet.com'+d.DocumentURL as URL
from tblDocuments d
inner join vtblDefenseInvoice di on di.DocumentID = d.DocumentID
left join tblClaimantDocumentType dt on dt.ClaimantDocumentTypeID = d.DetailDocumentTypeID

  Where (di.StatusID not in (3,4)) AND (di.QCBy IS NULL)

group by d.DocumentID, di.Status,  di.InvoiceNumber, 
di.Period, di.SubmitDate, di.DatePayable, di.DefenseCounsel,di.Firm  ,di.InvoiceLink,di.ApprovedBy, di.DateApproved,di.InvoiceNotes,
di.InvoiceTotal, di.adjustments, di.fees, DocumentURL, di.QCBy, ClaimantDocumentType, di.MatterID, AcceptedBy, DateAccepted, CreatedBy,
DateCreated, DetailDocumentTypeID, Comments, di.QCDate,di.costs

END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInvoicePassQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmInvoicePassQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInvoicePassQC] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmInvoicePassQC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInvoicePassQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmInvoicePassQC] TO [power_user]
GO
