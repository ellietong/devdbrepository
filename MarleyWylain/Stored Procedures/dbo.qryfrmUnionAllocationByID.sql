SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmUnionAllocationByID]
As

SET NOCOUNT ON

SELECT a.AllocationID AS AllocationID, CONVERT(varchar, a.AllocationID) + ' ' + AllocationDescription AS Expr1, AllocationDescription AS AllocationDescription
FROM tblAllocation a LEFT JOIN tblAllocationResults ar ON a.AllocationID = ar.AllocationID

UNION 

SELECT 0 AS AllocationID, '0 <<All Allocations>>' AS Expr1, '<<All Allocations>>' AS AllocationDescription
FROM tblAllocation
ORDER BY AllocationID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionAllocationByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionAllocationByID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionAllocationByID] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmUnionAllocationByID] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionAllocationByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionAllocationByID] TO [power_user]
GO
