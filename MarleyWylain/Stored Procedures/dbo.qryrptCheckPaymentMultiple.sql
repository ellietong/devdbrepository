SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCheckPaymentMultiple]  
	(
      @CheckID INT = NULL ,
      @SourceDate DATE = NULL
      
    )
AS 
    SET NOCOUNT ON



--Declare @CheckID int
--Set @CheckID = 13765
--Declare @SourceDate DATETIME
--Set @SourceDate = '3/4/2014' 

-- If you are only viewing a single check, bring back check data based on CheckID
IF @CheckID =0


    DECLARE @CheckFundingTypeID INT
    SET @CheckFundingTypeID = ( SELECT   CheckFundingTypeID
                           FROM     tblCheck
                           WHERE    CheckID = @checkID
                         )

    DECLARE @CheckApproved BIT
    SET @CheckApproved = ( SELECT   CASE WHEN ca.CheckID IS NULL THEN 0
                                         ELSE 1
                                    END
                           FROM     tblCheck c
                                    LEFT JOIN tblCheckApproval ca ON c.CheckID = ca.CheckID
                           WHERE    c.CheckID = @CheckID
                         )
	-- Calculate CheckAmount rather than using another input parameter
	DECLARE @CheckAmount TABLE
    (
      CheckID INT ,
      CheckAmount MONEY
    )	INSERT  INTO @CheckAmount
        SELECT  CheckID ,
                SUM(PaymentAmount)
        FROM    tblPaymentDefense
        WHERE   CheckID =@CheckID
        GROUP BY CheckID
        
        UNION
        
        SELECT  CheckID ,
                SUM(PaymentAmount)
        FROM    tblPaymentSettlement
        WHERE   CheckID =@CheckID
        GROUP BY CheckID

If @CheckFundingTypeID = 1 Begin

Select c.CheckID, DefenseCounsel as Firm, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip) as FirmAddress, DefenseCounsel as PayTo, SourceNumber as CheckNo, dbo.fnCurrencyToWords(SourceAmount) as AmountDesc,
SourceAmount as CheckAmount, SourceDate as CheckDate, Null as PlaintiffCounselName, Null as PCounselAddress, CheckFundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
LEFT JOIN @CheckAmount CA ON CA.CheckID = C.CheckID
Where c.CheckID = @CheckID
Group By c.CheckID, DefenseCounsel, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip), DefenseCounsel, SourceNumber, dbo.fnCurrencyToWords(SourceAmount),
SourceAmount, SourceDate, CheckFundingTypeID, Matter

End

If @CheckFundingTypeID = 2 Begin

Select c.CheckID, 
Null as Firm, 
Null as FirmAddress, 
null As PayTo, 
SourceNumber as CheckNo, 
dbo.fnCurrencyToWords(SourceAmount) as AmountDesc,
SourceAmount as CheckAmount, 
SourceDate as CheckDate,
pca.PlaintiffCounsel +' TRUST ACCOUNT' as PlaintiffCounselName,
PlaintiffCounselAddress1 + Case When PLaintiffCOunselAddress2 is null then '' Else ' 
' End + Coalesce(PlaintiffCounselAddress2,'
') + '
' + PlaintiffCounselCity + ', ' +pca.State + ' ' +Convert(nvarchar(10),PlaintiffCounselZip) as PCounselAddress, 
CheckFundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus

From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentSettlement ps on c.CheckID = ps.CheckID
Inner Join tblClaimSettlement cs on cs.ClaimSettlementID = ps.ClaimSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
LEFT JOIN @CheckAmount CA ON CA.CheckID = C.CheckID
--Left  Join tblCounselPayeeName cpn         ON pca.PlaintiffCounselAddressID = cpn.CounselAddressID
Where c.CheckID = @CheckID
Group By c.CheckID, L.PlaintiffCounselAddressID, SourceNumber, dbo.fnCurrencyToWords(SourceAmount), 
SourceAmount,
SourceDate, pca.PlaintiffCounsel+' TRUST ACCOUNT', PlaintiffCounselAddress1 + Case When PLaintiffCOunselAddress2 is null then '' Else ' 
' End + Coalesce(PlaintiffCounselAddress2,'
') + '
' + PlaintiffCounselCity + ', ' +pca.State + ' ' +Convert(nvarchar(10),PlaintiffCounselZip), CheckFundingTypeID, Matter

End

If @CheckFundingTypeID = 3 Begin

Select c.CheckID, VendorFirm as Firm, Address1 + Case When Address2 is null then '' Else ' 
' End + Coalesce(Address2,'
') + '
' + City + ', ' +s.State + ' ' +Convert(nvarchar(10),ZipCode) as FirmAddress, VendorFirm as PayTo, SourceNumber as CheckNo, dbo.fnCurrencyToWords(SourceAmount) as AmountDesc,
SourceAmount as CheckAmount, SourceDate as CheckDate, Null as PlaintiffCounselName, Null as PCounselAddress, CheckFundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join tblVendorFirm v on v.VendorFirmID=di.VendorFirmID
left join tblState s on s.StateID=v.StateID
LEFT JOIN @CheckAmount CA ON CA.CheckID = C.CheckID
Where c.CheckID = @CheckID
Group By c.CheckID, Vendorfirm, Address1 + Case When Address2 is null then '' Else ' 
' End + Coalesce(Address2,'
') + '
' + City + ', ' +s.State + ' ' +Convert(nvarchar(10),ZipCode), VendorFirm, SourceNumber, dbo.fnCurrencyToWords(SourceAmount),
SourceAmount, SourceDate, CheckFundingTypeID, Matter

End


--If trying to generate multiple checks, bring back data based on source date
ELSE IF @SourceDate IS NOT NULL

DECLARE @CheckAmountMulti TABLE
    (
      CheckID INT ,
      CheckAmount MONEY
    )
INSERT  INTO @CheckAmountMulti
        SELECT  CheckID ,
                SUM(PaymentAmount)
        FROM    tblPaymentDefense
        WHERE   CheckID IN ( SELECT CheckID
                             FROM   tblCheck
                             WHERE  SourceDate = @SourceDate )
        GROUP BY CheckID
        UNION
        SELECT  CheckID ,
                SUM(PaymentAmount)
        FROM    tblPaymentSettlement
        WHERE   CheckID IN ( SELECT CheckID
                             FROM   tblCheck
                             WHERE  SourceDate = @SourceDate )
        GROUP BY CheckID

DECLARE @CheckFundingLink TABLE
    (
      CheckID INT ,
      FundingTypeID INT ,
      CheckApproved BIT
    )
INSERT  INTO @CheckFundingLink
        SELECT  c.CheckID ,
                CheckFundingTypeID ,
                CASE WHEN ca.CheckID IS NULL THEN 0
                     WHEN ca.CheckID IS NOT NULL THEN 1
                END
        FROM    tblCheck c
                LEFT JOIN tblCheckApproval ca ON c.CheckID = ca.CheckID
        WHERE   SourceDate = @SourceDate --AND FundingTypeID = @FundingType




Select c.CheckID, DefenseCounsel as Firm, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip) as FirmAddress, DefenseCounsel as PayTo, SourceNumber as CheckNo, dbo.fnCurrencyToWords(SourceAmount) as AmountDesc,
SourceAmount as CheckAmount, SourceDate as CheckDate, Null as PlaintiffCounselName, Null as PCounselAddress, CheckFundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
 LEFT JOIN @CheckAmountMulti ca ON ca.CheckID = c.CheckID
  INNER JOIN @CheckFundingLink cfl ON c.CheckID = cfl.CheckID
Where cfl.FundingTypeID = 1

Group By c.CheckID, DefenseCounsel, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip), DefenseCounsel, SourceNumber, dbo.fnCurrencyToWords(SourceAmount),
SourceAmount, SourceDate, CheckFundingTypeID, Matter

UNION

Select c.CheckID, 
Null as Firm, 
Null as FirmAddress, 
null As PayTo, 
SourceNumber as CheckNo, 
dbo.fnCurrencyToWords(SourceAmount) as AmountDesc,
SourceAmount as CheckAmount, 
SourceDate as CheckDate,
pca.PlaintiffCounsel +' TRUST ACCOUNT' as PlaintiffCounselName,
PlaintiffCounselAddress1 + Case When PLaintiffCOunselAddress2 is null then '' Else ' 
' End + Coalesce(PlaintiffCounselAddress2,'
') + '
' + PlaintiffCounselCity + ', ' +pca.State + ' ' +Convert(nvarchar(10),PlaintiffCounselZip) as PCounselAddress, 
CheckFundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus

From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentSettlement ps on c.CheckID = ps.CheckID
Inner Join tblClaimSettlement cs on cs.ClaimSettlementID = ps.ClaimSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
 LEFT JOIN @CheckAmountMulti ca ON ca.CheckID = c.CheckID
 INNER JOIN @CheckFundingLink cfl ON c.CheckID = cfl.CheckID
--Left  Join tblCounselPayeeName cpn         ON pca.PlaintiffCounselAddressID = cpn.CounselAddressID
Where cfl.FundingTypeID = 2
Group By c.CheckID, L.PlaintiffCounselAddressID, SourceNumber, dbo.fnCurrencyToWords(SourceAmount), 
SourceAmount,
SourceDate, pca.PlaintiffCounsel+' TRUST ACCOUNT', PlaintiffCounselAddress1 + Case When PLaintiffCOunselAddress2 is null then '' Else ' 
' End + Coalesce(PlaintiffCounselAddress2,'
') + '
' + PlaintiffCounselCity + ', ' +pca.State + ' ' +Convert(nvarchar(10),PlaintiffCounselZip), CheckFundingTypeID, Matter

UNION

Select c.CheckID, VendorFirm as Firm, Address1 + Case When Address2 is null then '' Else ' 
' End + Coalesce(Address2,'
') + '
' + City + ', ' +s.State + ' ' +Convert(nvarchar(10),ZipCode) as FirmAddress, VendorFirm as PayTo, SourceNumber as CheckNo, dbo.fnCurrencyToWords(SourceAmount) as AmountDesc,
SourceAmount as CheckAmount, SourceDate as CheckDate, Null as PlaintiffCounselName, Null as PCounselAddress, CheckFundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join tblVendorFirm v on v.VendorFirmID=di.VendorFirmID
left join tblState s on s.StateID=v.StateID
 LEFT JOIN @CheckAmountMulti ca ON ca.CheckID = c.CheckID
 INNER JOIN @CheckFundingLink cfl ON c.CheckID = cfl.CheckID
Where cfl.FundingTypeID = 3
Group By c.CheckID, Vendorfirm, Address1 + Case When Address2 is null then '' Else ' 
' End + Coalesce(Address2,'
') + '
' + City + ', ' +s.State + ' ' +Convert(nvarchar(10),ZipCode), VendorFirm, SourceNumber, dbo.fnCurrencyToWords(SourceAmount),
SourceAmount, SourceDate, CheckFundingTypeID, Matter



RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentMultiple] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentMultiple] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentMultiple] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentMultiple] TO [power_user]
GO
