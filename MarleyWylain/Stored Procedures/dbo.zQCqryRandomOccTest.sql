SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[zQCqryRandomOccTest]
(@allocationid int )
As

SET NOCOUNT ON

SELECT AllocationID, OccurrenceID, PStartSplit, PEndSplit, TriggerStartDateUsed, TriggerEndDateUsed
FROM tblAllocationResults
WHERE  AllocationID LIKE COALESCE(@allocationid, 0)
ORDER BY OccurrenceID, PStartSplit

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[zQCqryRandomOccTest] TO [base_user]
GRANT EXECUTE ON  [dbo].[zQCqryRandomOccTest] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryRandomOccTest] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[zQCqryRandomOccTest] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryRandomOccTest] TO [power_user]
GRANT EXECUTE ON  [dbo].[zQCqryRandomOccTest] TO [power_user]
GO
