SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryfrmVendors]
AS
Set Nocount On
SELECT VendorID, VendorName 
FROM tblVendor 
ORDER BY VendorName
RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmVendors] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmVendors] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmVendors] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmVendors] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmVendors] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmVendors] TO [power_user]
GO
