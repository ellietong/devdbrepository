SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmClaimSequence]
as

Select ClaimSequenceID, ClaimSequence
From tblClaimSequence
Order By ClaimSequence

Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimSequence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimSequence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimSequence] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimSequence] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimSequence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimSequence] TO [power_user]
GO
