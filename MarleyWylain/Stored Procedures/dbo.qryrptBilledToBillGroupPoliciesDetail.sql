SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Procedure [dbo].[qryrptBilledToBillGroupPoliciesDetail]
(@LimitTypeID int = null, @BillID int = 33)
As

SET NOCOUNT ON

DECLARE @maxLimitTypeID int
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy

DECLARE @Policies TABLE (PolicyID int, PolicyDateID int, CName varchar(256), PNum varchar(256), PStart datetime, PEnd datetime, Agg money, QSLimit money, Attach money)
INSERT INTO @Policies

SELECT p.PolicyID, PolicyDateID, CarrierName, PolicyNum, PStartDate, PEndDate, AggregateLimit, LayerPerOccLimit, AttachmentPoint
FROM vPolicy p
	INNER JOIN tblBillGroupCarriers bgc ON p.CarrierID = bgc.CarrierID INNER JOIN tblBill b ON b.BillGroupID = bgc.BillGroupID
WHERE b.BillID LIKE COALESCE(@BillID, 0) 
AND p.LimitTypeID >= COALESCE(@LimitTypeID, 0) AND p.LimitTypeID <= COALESCE(NULLIF(@LimitTypeID, 0), @maxLimitTypeID )
GROUP BY p.PolicyID, PolicyDateID, CarrierName, PolicyNum, PStartDate, PEndDate, AggregateLimit, AttachmentPoint, LayerPerOccLimit
ORDER BY PStartdate, AttachmentPoint

DECLARE @BilledAmts TABLE (PolicyDateID int, Indemnity money, Defense money, DefenseOutside money)

INSERT INTO @BilledAmts
SELECT PolicyDateID, SUM(IndemnityAllocation), SUM(DefenseAllocation), SUM(DefenseOutsideLimitsAllocation)
FROM ((tblExhaustion e INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID) INNER JOIN tblLedger l ON l.ExhaustionDateID = ed.ExhaustionDateID)
	INNER JOIN tblBill b ON l.BillID = b.BillID
WHERE b.BillID LIKE COALESCE(@BillID, 0)
GROUP BY PolicyDateID

DECLARE @PreviousBills TABLE (PolicyDateID int, PrevIndem money, PrevDefense money, 	PrevDefenseOutside money)

INSERT INTO @PreviousBills
SELECT PolicyDateID, SUM(IndemnityAllocation), SUM(DefenseAllocation), SUM(DefenseOutsideLimitsAllocation)
FROM ((tblExhaustion e INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID) INNER JOIN tblLedger l ON l.ExhaustionDateID = ed.ExhaustionDateID)
	INNER JOIN tblBill b ON l.BillID = b.BillID
WHERE b.BillID < @BillID AND b.BillGroupID = (SELECT BillGroupID FROM tblBill WHERE BillID = @BillID)
GROUP BY PolicyDateID

SELECT PolicyID, p.PolicyDateID, CName, PNum, Pstart, Pend, Agg, QSLimit, Attach, COALESCE(Indemnity, 0) AS Indemnity, COALESCE(Defense, 0) AS Defense, 
	COALESCE(DefenseOutside, 0) AS DefenseOutside, COALESCE(PrevIndem, 0) AS PrevIndem, COALESCE(PrevDefense, 0) AS PrevDefense, COALESCE(PrevDefenseOutside, 0) AS PrevDefOut
FROM (@Policies p LEFT OUTER JOIN @BilledAmts b ON p.PolicyDateID = b.PolicyDateID) LEFT OUTER JOIN @PreviousBills pb ON pb.PolicyDateID = p.PolicyDateID

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBilledToBillGroupPoliciesDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBilledToBillGroupPoliciesDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBilledToBillGroupPoliciesDetail] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptBilledToBillGroupPoliciesDetail] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBilledToBillGroupPoliciesDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBilledToBillGroupPoliciesDetail] TO [power_user]
GO
