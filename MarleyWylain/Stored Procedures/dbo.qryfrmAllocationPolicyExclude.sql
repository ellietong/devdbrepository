SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmAllocationPolicyExclude]
(@allocationID int = NULL)
AS

	SELECT AllocationID, PolicyID
	FROM tblAllocationPolicyExclude
	WHERE AllocationID = @allocationID
	
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationPolicyExclude] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationPolicyExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationPolicyExclude] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationPolicyExclude] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationPolicyExclude] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationPolicyExclude] TO [power_user]
GO
