SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC  [dbo].[qrysrptEHistoricSiteCostsForExport] (@CostID int)
AS 
Set Nocount on
-- DECLARE @CostID int
-- SET @CostID = 7790
SELECT s.costrecordid, SiteName, batesmin, batesmax
FROM tblSiteCosts s
left join tblEnviroSitesDetail d on s.siteid = d.siteid
LEFT JOIN tblBatesRanges  B on b.costrecordid = s.costrecordid
WHERE s.costrecordid = @CostID AND NOT (BatesMin IS NULL AND BatesMax IS NULL)
RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptEHistoricSiteCostsForExport] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptEHistoricSiteCostsForExport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptEHistoricSiteCostsForExport] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysrptEHistoricSiteCostsForExport] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptEHistoricSiteCostsForExport] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptEHistoricSiteCostsForExport] TO [power_user]
GO
