SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



create PROC [dbo].[qryfrmInvoiceQCd]
(@DocumentID int, @QCdBy varchar(200))
AS

SET NOCOUNT ON

--Copy Document Record to mBtblDefenseInvoice
Update tblDefenseInvoice 
Set QCBy = @QCdBy, QCDate = GETDATE()
WHERE DocumentID = @DocumentID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInvoiceQCd] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmInvoiceQCd] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInvoiceQCd] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmInvoiceQCd] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInvoiceQCd] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmInvoiceQCd] TO [power_user]
GO
