SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[qryrptCarriersNoticedAndDenialBySite]
(@CorrespondenceDate datetime = NULL, @Site nvarchar(100) = null, @CarrierID int = NULL)
as
set nocount on

--Declare @CorrespondenceDate datetime 
--Set @CorrespondenceDate = '1/1/1900'
--Declare @Site nvarchar(100)
--set @Site = 0
--Declare @Carrierid varchar(200)
--Set @Carrierid = 0

DECLARE @MaxSiteID Int
SELECT @MaxSiteID = MAX(siteid) from tblEnviroSitesDetail

DECLARE @MAXCarrierID Int
SELECT @MAXCarrierID = MAX(carrierID) from tblCarrier


select CorporateHistoryProfile, cor.CorrespondenceTypeID, CorrespondenceDate, sn.SiteID, SiteName, 
	c.CarrierID, c.CarrierName, ct.CorrespondenceType, cor.CorrespondenceID, Notes, con.Company
from tblcorrespondence cor 
	left join tblcorrespondencetype ct on cor.correspondencetypeid = ct.correspondencetypeid
	left join tblCorrCarrierContact ccc on ccc.CorrespondenceID=cor.CorrespondenceID
	left join tblCarrierContact cc on cc.CarrierContactID=ccc.CarrierContactID
	left join tblContact con on con.ContactID=cc.ContactID
	left join tblcarrier c on cc.carrierid = c.carrierid
	left join tblsitenoticed sn on cor.correspondenceid = sn.correspondenceid
	left join tblEnviroSitesDetail esd on sn.SiteID= esd.siteid
	left join tblcorporatehistoryprofile chp on esd.corporatehistoryid = chp.corporatehistoryid

where cor.correspondencetypeid = 5
And (ccc.RecipientTypeID is null or ccc.RecipientTypeID=2)
AND cor.CorrespondenceDate >= @correspondencedate and cor.correspondencedate <= COALESCE(NULLIF(@CorrespondenceDate, '1/1/1900'), getdate())
AND c.CarrierID >= coalesce(@carrierid, 0) and c.CarrierID  <= coalesce(nullif(@carrierid, 0), @Maxcarrierid)
AND sn.SiteID>= coalesce(@site, 0) and sn.SiteID <= coalesce(nullif(@site, 0), @maxsiteid)

group by CorporateHistoryProfile, cor.CorrespondenceTypeID, CorrespondenceDate, sn.SiteId, SiteName, 
	c.CarrierID, c.CarrierName, ct.CorrespondenceType, cor.CorrespondenceID, Notes, con.Company
union
select CorporateHistoryProfile, cor.CorrespondenceTypeID, CorrespondenceDate, sn.SiteId, SiteName, 
	c.CarrierID, c.CarrierName, ct.CorrespondenceType, cor.CorrespondenceID, Notes, con.Company
from tblcorrespondence cor 
	left join tblcorrespondencetype ct on cor.correspondencetypeid = ct.correspondencetypeid
	left join tblCorrCarrierContact ccc on ccc.CorrespondenceID=cor.CorrespondenceID
	left join tblCarrierContact cc on cc.CarrierContactID=ccc.CarrierContactID
	left join tblContact con on con.ContactID=cc.ContactID
	left join tblcarrier c on cc.carrierid = c.carrierid
	left join tblsitenoticed sn on cor.correspondenceid = sn.correspondenceid
	left join tblEnviroSitesDetail esd on sn.siteid = esd.siteid
	left join tblcorporatehistoryprofile chp on esd.corporatehistoryid = chp.corporatehistoryid
	left join tblpolicynoticed pn on cor.correspondenceid = pn.correspondenceid
where cor.correspondencetypeid = 1
And (ccc.RecipientTypeID is null or ccc.RecipientTypeID=2)
AND cor.CorrespondenceDate >= @correspondencedate and cor.correspondencedate <= COALESCE(NULLIF(@CorrespondenceDate, '1/1/1900'), getdate())
AND sn.SiteID >= coalesce(@site, 0) and sn.SiteID <= coalesce(nullif(@site, 0), @maxsiteid)
AND c.CarrierID >= coalesce(@carrierid, 0) and c.CarrierID  <= coalesce(nullif(@carrierid, 0), @Maxcarrierid)


group by CorporateHistoryProfile, cor.CorrespondenceTypeID, CorrespondenceDate, sn.SiteId, SiteName, 
	c.CarrierID, c.CarrierName, ct.CorrespondenceType, cor.CorrespondenceID, Notes, con.Company

return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCarriersNoticedAndDenialBySite] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCarriersNoticedAndDenialBySite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCarriersNoticedAndDenialBySite] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCarriersNoticedAndDenialBySite] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCarriersNoticedAndDenialBySite] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCarriersNoticedAndDenialBySite] TO [power_user]
GO
