SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROCEDURE [dbo].[qrysfrmReviewedInvoices]

AS
BEGIN

-- Invoice Total per document included sum of all claimants
select 
d.DocumentID, di.Status, di.DefenseCounsel, di.Firm as VendorFirm, di.InvoiceNumber, 
di.Period, di.SubmitDate, di.DatePayable, di.InvoiceLink, di.ApprovedBy, di.DateApproved, di.InvoiceNotes, di.SourceNumber as CheckNumber
, di.InvoiceTotal, di.adjustments, di.fees, DocumentURL, coalesce(di.QCBy,'') +' '+ cast(di.qcdate as nvarchar(30)) as QCBy, 
ClaimantDocumentType as DocumentType, di.MatterID, AcceptedBy, 
DateAccepted, CreatedBy, DateCreated, di.sourceamount

from tblDocuments d
	inner join vtblDefenseInvoice di on di.DocumentID = d.DocumentID
	left join tblClaimantDocumentType dt on dt.ClaimantDocumentTypeID = d.DetailDocumentTypeID

Where di.QCBy IS NOT NULL

group by d.DocumentID, di.Status, di.DefenseCounsel, di.Firm, di.InvoiceNumber, di.sourceamount,
di.Period, di.SubmitDate, di.DatePayable, di.InvoiceLink, di.ApprovedBy, di.DateApproved, di.InvoiceNotes, di.SourceNumber, di.QCDate
, di.InvoiceTotal, di.adjustments, di.fees, DocumentURL, di.QCBy, ClaimantDocumentType, di.MatterID, AcceptedBy, DateAccepted, CreatedBy, DateCreated

END


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmReviewedInvoices] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmReviewedInvoices] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmReviewedInvoices] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysfrmReviewedInvoices] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmReviewedInvoices] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmReviewedInvoices] TO [power_user]
GO
