SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROc [dbo].[qryfrmCountry]
AS

SET NOCOUNT ON
SELECT CountryID, Country
from dbo.tblCountry
Order by Country


RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCountry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCountry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCountry] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCountry] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCountry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCountry] TO [power_user]
GO
