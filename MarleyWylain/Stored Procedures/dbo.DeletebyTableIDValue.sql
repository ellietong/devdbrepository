SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*============================================================================
 CREATE A DYNAMIC SQL CODE THAT TAKES IN TABLE NAME, field name and values
 FOR DELETIONS.  IF THERE IS A FOREIGN KEY CONSTRAINT, YOU NEED TO DELETE 
 THE RECORDS IN THAT TABLE FIRST.  EXAMPLE THE CLAIMSETTLEMENTID IN
 TBLPAYMENTSETTLEMENT AND TBLCLAIMSETTLEMENT TABLES.  UNLESS THE FOREIGN 
 KEY HAS DELETE ON CASCADE
 With dynamic SQL we can pass in comma delimited multiple values to be deleted
============================================================================*/
CREATE PROCEDURE [dbo].[DeletebyTableIDValue]
(
	@TableName varchar(25)
	,@FieldName varchar(25)
	,@FieldValue varchar(25)
	--@FieldValue TableFieldValues readonly
)
AS
SET NOCOUNT on
/*
select top 10 * from dbo.tblPaymentDefense where CheckID=9858
	execute dbo.DeletebyTableIDValue
	@TableName = 'dbo.tblPaymentDefense'
	,@FieldName = 'CheckID'
	,@FieldValue = 9862

	execute dbo.DeletebyTableIDValue
	@TableName = 'dbo.tblPaymentSettlement'
	,@FieldName = 'ClaimSettlementID'
	,@FieldValue = 1998

	execute dbo.DeletebyTableIDValue
	@TableName = 'dbo.tblClaimSettlement'
	,@FieldName = 'ClaimSettlementID'
	,@FieldValue = '2000,2001'	

*/
BEGIN TRY
	DECLARE @SQL NVARCHAR(500) = null
	,@Message VARCHAR(250) = NULL
    ,@rowcount SMALLINT = NULL

--	SELECT * FROM tonge.Split (@FieldValue,',')

	SET @sql = 'select count(1) from ' + @TableName  + ' where '+ @FieldName + ' in (' + @FieldValue + ')'
	PRINT @SQL
	EXECUTE sp_executesql @SQL
	
	BEGIN TRAN

	SET @SQL = 'delete from ' + @TableName  + ' where '+ @FieldName + ' in (' + @FieldValue + ')'
	PRINT @SQL
	EXECUTE sp_executesql @SQL
	SELECT @rowcount = @@ROWCOUNT

	IF (@@error <> 0 ) BEGIN
    PRINT 'unexpected error' + @@ERROR	
	ROLLBACK TRAN
	RETURN 1
	END

	COMMIT TRAN
	print CONVERT(VARCHAR(2),@rowcount) + ' rows deleted.'

	
END TRY
BEGIN CATCH
	SELECT
		errorNumber = ERROR_NUMBER()
	   ,errorseverity=ERROR_SEVERITY()
	   ,errorstate = ERROR_STATE()
	   ,errorprocedure = ERROR_PROCEDURE()
	   ,errorline = ERROR_LINE()
	   ,errorMessage = ERROR_MESSAGE()
END CATCH
GO
