SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmRelationship]
as
Select RelationshipID, Relationship
From tblRelationship
Order By Relationship

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmRelationship] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmRelationship] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmRelationship] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmRelationship] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmRelationship] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmRelationship] TO [power_user]
GO
