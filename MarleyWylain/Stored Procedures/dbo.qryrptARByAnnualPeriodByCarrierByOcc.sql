SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptARByAnnualPeriodByCarrierByOcc]
(@allocationID int)
As

SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 57

DECLARE @Policy TABLE (policyid int, CarrierName nvarchar(100))
INSERT INTO @Policy

SELECT PolicyID, CarrierName
FROM vPolicy
GROUP BY PolicyID, CarrierName

SELECT o.OccurrenceName, pinfo.CarrierName, ar.AllocationID, ar.AnnualPeriodSplit, SUM(ar.SIRAllocated) AS SumOfSIRAllocated, SUM(ar.PV) AS SumOfPV, SUM(ar.Allocation) AS SumOfAllocation, 
SUM([SIRAllocated]+[Allocation]) AS TotalAllocated, MAX(ar.DateTime) AS MaxOfDateTime, a.AllocationDescription, ar.TriggerStartDateUsed, ar.TriggerEndDateUsed, o.OccurrenceID, 
SUM(ar.AllocationIndemnity) AS SumOfAllocationIndemnity, SUM(ar.AllocationDefense) AS SumOfAllocationDefense, SUM(ar.AllocationDefenseOutsideLimits) AS SumOfAllocationDefenseOutsideLimits
FROM @Policy pinfo 
INNER JOIN (vOccurrence o INNER JOIN (vAllocation a INNER JOIN vAllocationResults ar ON 
a.AllocationID = ar.AllocationID) ON o.OccurrenceID = ar.OccurrenceID) ON pinfo.PolicyID = ar.PolicyID
WHERE ar.AllocationID LIKE COALESCE(@allocationID, 0)
GROUP BY o.OccurrenceName, pinfo.CarrierName, ar.AllocationID, ar.AnnualPeriodSplit, a.AllocationDescription, ar.TriggerStartDateUsed, ar.TriggerEndDateUsed, o.OccurrenceID
ORDER BY ar.AllocationID, ar.AnnualPeriodSplit

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAnnualPeriodByCarrierByOcc] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByAnnualPeriodByCarrierByOcc] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAnnualPeriodByCarrierByOcc] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptARByAnnualPeriodByCarrierByOcc] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAnnualPeriodByCarrierByOcc] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByAnnualPeriodByCarrierByOcc] TO [power_user]
GO
