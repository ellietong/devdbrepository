SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  Procedure [dbo].[qryrptARCompareTwoRuns]
(@allocation1 int = null, @allocation2 int = null)
As
SET NOCOUNT ON

-- DECLARE @Allocation1 INT
-- SET @Allocation1 = 150
-- DECLARE @Allocation2 INT
-- SET @Allocation2 = 160

DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
CarrierID int, CarrierName nvarchar(250), PolicyHolder bit)
INSERT INTO @Policy

SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, PolicyHolder
FROM vPolicy
GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, PolicyHolder

--create temporary table
DECLARE @qryARCompareTwoRuns TABLE (
	[CarrierGroupInfo] varchar (256),
	[CarrierGroupID] int, 
	[PolicyHolder] bit, 
	[DateTime] datetime, 
	[Run1] int, 
	[Run2] int, 
	[pv1] money, 
	[allocation1] money, 
	[SIRallocated1] money, 
	[pv2] money, 
	[allocation2] money, 
	[SIRallocated2] money
)

--append query results to temporary table
INSERT INTO @qryARCompareTwoRuns
SELECT COALESCE(pinfo.CarrierGroupName, pinfo.CarrierName) AS 'CarrierGroupInfo', pinfo.CarrierGroupID, pinfo.PolicyHolder, MAX(ar.DateTime) AS 'DateTime', @allocation1 as 'Run1', @allocation2 as 'Run2', 
	CASE WHEN ar.AllocationID = @allocation1 THEN SUM(ar.PV) ELSE 0 END AS 'pv1', CASE WHEN ar.AllocationID = @allocation1 THEN SUM(Allocation) ELSE 0 END AS 'Allocation1',
	CASE WHEN ar.AllocationID = @allocation1 THEN SUM(SIRAllocated) ELSE 0 END AS 'SIRAllocated1', CASE WHEN ar.AllocationID = @allocation2 THEN SUM(ar.PV) ELSE 0 END AS 'pv2', 
	CASE WHEN ar.AllocationID = @allocation2 THEN SUM(Allocation) ELSE 0 END AS 'Allocation2', CASE WHEN ar.AllocationID = @allocation2 THEN SUM(SIRAllocated) ELSE 0 END AS 'SIRAllocated2'
FROM @Policy pinfo INNER JOIN vAllocationResults ar ON pinfo.PolicyDateID = ar.PolicyDateID
WHERE (ar.AllocationID = @allocation1) OR (ar.AllocationID = @allocation2) 
GROUP BY COALESCE(pinfo.CarrierGroupName, pinfo.CarrierName), pinfo.CarrierGroupID, pinfo.PolicyHolder, AllocationID

--Query based on temporary table

SELECT CarrierGroupInfo, CarrierGroupID, PolicyHolder, MAX(DateTime) AS 'DateTime', Run1, Run2, SUM(pv1) AS 'pv1', SUM(allocation1) AS 'allocation1', SUM(SIRallocated1) AS 'SIRallocated1', 
	SUM(pv2) AS 'pv2', SUM(allocation2) AS 'allocation2', SUM(SIRallocated2) AS 'SIRallocated2'
FROM @qryARCompareTwoRuns
GROUP BY CarrierGroupInfo, CarrierGroupID, PolicyHolder, Run1, Run2
	
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARCompareTwoRuns] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARCompareTwoRuns] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARCompareTwoRuns] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptARCompareTwoRuns] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARCompareTwoRuns] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARCompareTwoRuns] TO [power_user]
GO
