SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[qryrptBILLSummaryReport] (@BeginBillNumber int, @EndBillNumber int, @BillGroupID int)
AS

SET NOCOUNT ON
 --declare @BeginBillNumber int
 --declare @EndBillNumber int
 --declare @billgroupid int
 --set @BeginBillNumber = 2
 --set @EndBillNumber = 2
 --set @billgroupid =7

-- Past Amounts

DECLARE @PastAmounts TABLE
	(BillNumber int,
	PastBilled money,
	PastPaid money,
	PastBilledLessPaid money,
	PastAdjustments money,
	PastInterest money,
	PastInterestPaid money,
	PastInterestLessPaid money)

IF @BeginBillNumber = 1 
BEGIN
INSERT INTO @PastAmounts (BillNumber,
			PastBilled,
			PastPaid,
			PastBilledLessPaid,
			PastAdjustments,
			PastInterest,
			PastInterestPaid,
			PastInterestLessPaid)
VALUES (@EndBillNumber, 0, 0, 0, 0, 0, 0, 0)
END
ELSE 
BEGIN
INSERT INTO @PastAmounts
SELECT	@EndBillNumber as BillNumber,
	CASE WHEN LedgerTypeID = 1 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastBilled,
	CASE WHEN LedgerTypeID = 2 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastPaid,	
	CASE WHEN LedgerTypeID IN (1,2) THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastBilledLessPaid,
	CASE WHEN LedgerTYpeID = 3 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastAdjustments,
	CASE WHEN LedgerTypeID = 4 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastInterest,
	CASE WHEN LedgerTypeID = 5 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastInterestPaid,
	CASE WHEN LedgerTypeID IN (4,5) THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS PastInterestLessPaid
FROM vBilling b
WHERE BillNumber < @BeginBillNumber and BillGroupID = @BillGroupID
GROUP BY LedgerTypeID
END

-- Past Summed Amounts
DECLARE @PastSummedAmounts TABLE
	(BillNumber int,
	PastBilled money,
	PastPaid money,
	PastBilledLessPaid money,
	PastAdjustments money,
	PastInterest money,
	PastInterestPaid money,
	PastInterestLessPaid money)
INSERT INTO @PastSummedAmounts
SELECT	BillNumber,
	COALESCE(SUM(PastBilled),0) AS PastBilled,
	COALESCE(SUM(PastPaid),0) AS PastPaid,	
	COALESCE(SUM(PastBilledLessPaid),0) AS PastBilledLessPaid,
	COALESCE(SUM(PastAdjustments),0) AS PastAdjustments,
	COALESCE(SUM(PastInterest),0) AS PastInterest,
	COALESCE(SUM(PastInterestPaid),0) AS PastInterestPaid,
	COALESCE(SUM(PastInterestLessPaid),0) AS PastInterestLessPaid
FROM @PastAmounts
GROUP BY BillNumber 

-- Current Amounts
DECLARE @CurrentAmounts TABLE
	(BillNumber int,
	CurrentBilled money,
	CurrentPaid money,
	CurrentBilledLessPaid money,
	CurrentAdjustments money,
	CurrentInterest money,
	CurrentInterestPaid money,
	CurrentInterestLessPaid money)
INSERT INTO @CurrentAmounts
SELECT	@EndBillNumber AS BillNumber,
	CASE WHEN LedgerTypeID = 1 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentBilled,
	CASE WHEN LedgerTypeID = 2 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentPaid,	
	CASE WHEN LedgerTypeID IN (1,2) THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentBilledLessPaid,
	CASE WHEN LedgerTYpeID = 3 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentAdjustments,
	CASE WHEN LedgerTypeID = 4 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentInterest,
	CASE WHEN LedgerTypeID = 5 THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentInterestPaid,
	CASE WHEN LedgerTypeID IN (4,5) THEN COALESCE(SUM(Debit-Credit),0) ELSE 0 END AS CurrentInterestLessPaid
FROM vBilling b
WHERE (BillNumber BETWEEN @BeginBillNumber AND @EndBillNumber) and BillGroupID = @BillGroupID
GROUP BY LedgerTypeID, BillNumber

-- Current Amounts Summed
DECLARE @CurrentSummedAmounts TABLE
	(BillNumber int,
	CurrentBilled money,
	CurrentPaid money,
	CurrentBilledLessPaid money,
	CurrentAdjustments money,
	CurrentInterest money,
	CurrentInterestPaid money,
	CurrentInterestLessPaid money)
INSERT INTO @CurrentSummedAmounts
SELECT	BillNumber,
	SUM(CurrentBilled) AS CurrentBilled,
	SUM(CurrentPaid) AS CurrentPaid,	
	SUM(CurrentBilledLessPaid) AS CurrentBilledLessPaid,
	SUM(CurrentAdjustments) AS CurrentAdjustments,
	SUM(CurrentInterest) AS CurrentInterest,
	SUM(CurrentInterestPaid) AS CurrentInterestPaid,
	SUM(CurrentInterestLessPaid) AS CurrentInterestLessPaid
FROM @CurrentAmounts
GROUP BY BillNumber

--Results
SELECT	pa.BillNumber, BillDate, PaymentDueDate,
	(CurrentBilled) AS CurrentBilled, 
	(PastBilled) AS PastBilled, 
	(CurrentPaid) AS CurrentPaid, 
	(PastPaid) AS PastPaid, 
	(CurrentBilledLessPaid) AS CurrentBilledLessPaid, 
	(PastBilledLessPaid) AS PastBilledLessPaid,
	(CurrentAdjustments) AS CurrentAdjustments,
	(PastAdjustments) AS PastAdjustments,
	(CurrentInterest) AS CurrentInterest,
	(PastInterest) AS PastInterest,
	(CurrentInterestPaid) AS CurrentInterestPaid,
	(PastInterestPaid) AS PastInterestPaid,
	(CurrentInterestLessPaid) AS CurrentInterestLessPaid,
	(PastInterestLessPaid) AS PastInterestLessPaid
FROM @CurrentSummedAmounts ca INNER JOIN @PastSummedAmounts pa ON ca.BillNumber = pa.BillNumber
	INNER JOIN vBilling b ON ca.BillNumber = b.BillNumber
WHERE pa.BillNumber = @EndBillNumber and b.billgroupID = @BillgroupID
GROUP BY pa.BillNumber, BillDate, PaymentDueDate, CurrentBilled,PastBilled,CurrentPaid,PastPaid,
CurrentBilledLessPaid,PastBilledLessPaid,CurrentAdjustments,PastAdjustments,CurrentInterest,
PastInterest,CurrentInterestPaid,PastInterestPaid, CurrentInterestLessPaid,PastInterestLessPaid


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBILLSummaryReport] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBILLSummaryReport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBILLSummaryReport] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptBILLSummaryReport] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBILLSummaryReport] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBILLSummaryReport] TO [power_user]
GO
