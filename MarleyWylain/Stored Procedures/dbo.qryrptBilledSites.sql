SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create proc [dbo].[qryrptBilledSites]
As 
set nocount on


SELECT SD.SiteID, 
SD.SiteName, 
Sum(SC.ReimbursableAmount) AS SumOfAmount 
FROM tblEnviroSitesDetail AS SD 
INNER JOIN tblSiteCosts AS SC
ON SD.SiteID=SC.SiteID
WHERE SC.Billed=1
GROUP BY SD.SiteID, SD.SiteName

return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBilledSites] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBilledSites] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBilledSites] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptBilledSites] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBilledSites] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBilledSites] TO [power_user]
GO
