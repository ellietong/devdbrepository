SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryrptCheckPayment] (@CheckID int, @CheckAmount money)

As

SET NOCOUNT ON

--Declare @CheckID int
--Declare @CheckAmount money
--Set @CheckAmount = 12500.00
--Set @CheckID = 13801

Declare @CheckFundingTypeID int
Set @CheckFundingTypeID = (select CheckFundingTypeID from tblCheck where CheckID = @checkID)

Declare @CheckApproved bit
Set @CheckApproved = (Select Case when ca.CheckID is null then 0 Else 1 End From tblCheck c Left join tblCheckApproval ca on c.CheckID = ca.CheckID where c.CheckID = @CheckID)

If @CheckFundingTypeID = 1 Begin

Select c.CheckID, DefenseCounsel as Firm, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip) as FirmAddress, DefenseCounsel as PayTo, SourceNumber as CheckNo, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)) as AmountDesc,
Coalesce(SourceAmount,@CheckAmount) as CheckAmount, SourceDate as CheckDate, Null as PlaintiffCounselName, Null as PCounselAddress, CheckFundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
Where c.CheckID = @CheckID
Group By c.CheckID, DefenseCounsel, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip), DefenseCounsel, SourceNumber, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)),
Coalesce(SourceAmount,@CheckAmount), SourceDate, CheckFundingTypeID, Matter

End

If @CheckFundingTypeID = 2 Begin

Select c.CheckID, Null as Firm, Null as FirmAddress, null As PayTo, 
SourceNumber as CheckNo, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)) as AmountDesc,
Coalesce(SourceAmount,@CheckAmount) as CheckAmount, SourceDate as CheckDate,
COALESCE(cpn.CounselPayeeName, pca.PlaintiffCounsel)  as PlaintiffCounselName,
PlaintiffCounselAddress1 + Case When PLaintiffCOunselAddress2 is null then '' Else ' 
' End + Coalesce(PlaintiffCounselAddress2,'
') + '
' + PlaintiffCounselCity + ', ' +pca.State + ' ' +Convert(nvarchar(10),PlaintiffCounselZip) as PCounselAddress, CheckFundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentSettlement ps on c.CheckID = ps.CheckID
Inner Join tblClaimSettlement cs on cs.ClaimSettlementID = ps.ClaimSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
Left  Join tblCounselPayeeName cpn         ON pca.PlaintiffCounselAddressID = cpn.CounselAddressID
Where c.CheckID = @CheckID
Group By c.CheckID, L.PlaintiffCounselAddressID, SourceNumber, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)), Coalesce(SourceAmount,@CheckAmount),
SourceDate, COALESCE(cpn.CounselPayeeName, pca.PlaintiffCounsel), PlaintiffCounselAddress1 + Case When PLaintiffCOunselAddress2 is null then '' Else ' 
' End + Coalesce(PlaintiffCounselAddress2,'
') + '
' + PlaintiffCounselCity + ', ' +pca.State + ' ' +Convert(nvarchar(10),PlaintiffCounselZip), CheckFundingTypeID, Matter

End

If @CheckFundingTypeID = 3 Begin

Select c.CheckID, VendorFirm as Firm, Address1 + Case When Address2 is null then '' Else ' 
' End + Coalesce(Address2,'
') + '
' + City + ', ' +s.State + ' ' +Convert(nvarchar(10),ZipCode) as FirmAddress, VendorFirm as PayTo, SourceNumber as CheckNo, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)) as AmountDesc,
Coalesce(SourceAmount,@CheckAmount) as CheckAmount, SourceDate as CheckDate, Null as PlaintiffCounselName, Null as PCounselAddress, CheckFundingTypeID, Matter,
Case when @CheckApproved = 1 Then '' When @CheckApproved = 0 Then 'VOID' End As CheckStatus
From tblCheck c
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join tblVendorFirm v on v.VendorFirmID=di.VendorFirmID
left join tblState s on s.StateID=v.StateID
Where c.CheckID = @CheckID
Group By c.CheckID, Vendorfirm, Address1 + Case When Address2 is null then '' Else ' 
' End + Coalesce(Address2,'
') + '
' + City + ', ' +s.State + ' ' +Convert(nvarchar(10),ZipCode), VendorFirm, SourceNumber, dbo.fnCurrencyToWords(Coalesce(SourceAmount,@CheckAmount)),
Coalesce(SourceAmount,@CheckAmount), SourceDate, CheckFundingTypeID, Matter

End

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPayment] TO [power_user]
GO
