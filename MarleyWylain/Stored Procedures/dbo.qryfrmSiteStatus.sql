SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[qryfrmSiteStatus]
as
set nocount on

SELECT *
FROM tblStatus

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteStatus] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmSiteStatus] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteStatus] TO [power_user]
GO
