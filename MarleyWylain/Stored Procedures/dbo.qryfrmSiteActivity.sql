SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[qryfrmSiteActivity]
as
set nocount on

SELECT *
FROM tblSiteActivity

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteActivity] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmSiteActivity] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteActivity] TO [power_user]
GO
