SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantNameSSN] (@LastNameInitial nvarchar(5))
as 

Set NoCount ON

--Declare @LastNameInitial nvarchar(5)
--Set @LastNameInitial = 'w'

Declare @ClaimantName table (ClaimantPersonalInfoID int,
				LastName nvarchar(40),
				FirstName nvarchar(40),
				MiddleName nvarchar(40),
				Suffix nvarchar (40),
				SocialSecurityNumber nvarchar(80))
Insert Into @ClaimantName
Select cpi.ClaimantPersonalInfoID,
	Case when LastName is null then '[LastName]' else LastName end as LastName,
	Case when FirstName is null then '[FirstName]' else FirstName end as FirstName,
	Case when MiddleName is null then '' else ' ' + MiddleName end as MiddleName,
	Case when Suffix is null then '' else ', ' + Suffix end as Suffix,
	Case when SocialSecurityNumber is null then (Case when DocketNumber is null then '' else '  -  ' + DocketNumber end) else '  -  ' + Left(SocialSecurityNumber,3) + '-'+ Right(Left(SocialSecurityNumber,5),2) + '-'+ Right(SocialSecurityNumber,4) end as SocialSecurityNumber
From tblClaimantPersonalInfo cpi
left join tblClaimantSuffix cs on cs.SuffixID = cpi.SuffixID
left join tblclaim c on cpi.claimantpersonalinfoid = c.claimantpersonalinfoid
left join tblclaimlawsuit cl on c.claimid = cl.claimid
left join tbllawsuit l on cl.lawsuitid = l.lawsuitid
Where LastName like coalesce(@LastNameInitial + '%', '%')
group by cpi.ClaimantPersonalInfoID,
	Case when LastName is null then '[LastName]' else LastName end,
	Case when FirstName is null then '[FirstName]' else FirstName end,
	Case when MiddleName is null then '' else ' ' + MiddleName end,
	Case when Suffix is null then '' else ', ' + Suffix end,
	Case when SocialSecurityNumber is null then (Case when DocketNumber is null then '' else '  -  ' + DocketNumber end) else '  -  ' + Left(SocialSecurityNumber,3) + '-'+ Right(Left(SocialSecurityNumber,5),2) + '-'+ Right(SocialSecurityNumber,4) end

Select ClaimantPersonalInfoID, 
	LastName + ', ' + FirstName + MiddleName + Suffix + SocialSecurityNumber
From @ClaimantName
group by ClaimantPersonalInfoID, 
	LastName + ', ' + FirstName + MiddleName + Suffix + SocialSecurityNumber
Order By LastName + ', ' + FirstName + MiddleName + Suffix + SocialSecurityNumber

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantNameSSN] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantNameSSN] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantNameSSN] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantNameSSN] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantNameSSN] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantNameSSN] TO [power_user]
GO
