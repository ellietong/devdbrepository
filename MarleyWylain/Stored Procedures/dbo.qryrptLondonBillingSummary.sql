SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Proc [dbo].[qryrptLondonBillingSummary]
(@BillExhaustionDateID int)
As
Set Nocount on


--Declare @BillExhaustionDateID int
--Set @BillExhaustionDateID = 112


Declare @Header nvarchar(100)
Declare @ExhaustionTypeID int
Declare @Footer nvarchar(100)
Set @Header = (select Datename(MONTH,exhaustiondate) +' ' + Datename(Year,exhaustiondate) from tblexhaustiondate where ExhaustionDateID=@BillExhaustionDateID)
Set @ExhaustionTypeID = (Select exhaustiontypeid from tblExhaustionDate where ExhaustionDateID = @BillExhaustionDateID group by ExhaustionTypeID)
Set @Footer = (select Datename(MONTH,Dateadd(month,1,exhaustiondate)) +' ' + Datename(Year,Dateadd(month,1,exhaustiondate)) from tblexhaustiondate where ExhaustionDateID=@BillExhaustionDateID)

Declare @Subscriber Table (policydateid int, policynum nvarchar(100), StartDate datetime, EndDate datetime, pstartdate datetime,
	PEndDate datetime, SubscriptionCompany nvarchar(255), SubscriptionSectionPercentage float, SubscriptionCompanyPercent float, subscriptioncompanysolvency nvarchar(30) )
Insert Into @Subscriber
select d.policydateid, policynum, StartDate, EndDate, pstartdate, PEndDate, SubscriptionCompany, SubscriptionSectionPercentage, SubscriptionCompanyPercent	, subscriptioncompanysolvency
from tblSubscriptionSectionCoverage ssc				
inner join tblSubscriptionCoverage sc on sc.SubscriptionSectionCoverageID=ssc.SubscriptionSectionCoverageID				
inner join tblSubscriberCompany s on s.SubscriberID=sc.SubscriberID				
inner join tblPolicy p on p.PolicyID=ssc.PolicyID				
inner join tblPolicyDates d on d.PolicyID=p.PolicyID				
where CarrierID=20 and StartDate=PStartDate 
group by d.policydateid,policynum,StartDate, EndDate, pstartdate,PEndDate, SubscriptionCompany, SubscriptionSectionPercentage, SubscriptionCompanyPercent, subscriptioncompanysolvency


Declare @AllocationAmounts table (SubscriptionCompany nvarchar(255), subscriptioncompanypercent float, AllocationIndemnity27 float, AllocationIndemnity28 float,
AllocationIndemnity29 float, AllocationIndemnity30 float, AllocationDefense27 float, AllocationDefense28 float, AllocationDefense29 float, AllocationDefense30 float,
AllocationTotal27 float, AllocationTotal28 float, AllocationTotal29 float, AllocationTotal30 float, subscriptioncompanysolvency nvarchar(10))
insert @AllocationAmounts
select SubscriptionCompany, subscriptioncompanypercent, 
case when  s.policydateid=27 then coalesce(SUM(indemnityallocation*SubscriptionCompanyPercent),0) end as 'AllocationIndemnity27', 
case when  s.policydateid=28 then coalesce(SUM(indemnityallocation*SubscriptionCompanyPercent),0) end as 'AllocationIndemnity28', 
case when  s.policydateid=29 then coalesce(SUM(indemnityallocation*SubscriptionCompanyPercent),0) end as 'AllocationIndemnity29', 
case when  s.policydateid=30 then coalesce(SUM(indemnityallocation*SubscriptionCompanyPercent),0) end as 'AllocationIndemnity30', 
case when  s.policydateid=27 then coalesce(SUM(defenseallocation*SubscriptionCompanyPercent),0) end as 'AllocationDefense27', 
case when  s.policydateid=28 then coalesce(SUM(defenseallocation*SubscriptionCompanyPercent),0) end as 'AllocationDefense28', 
case when  s.policydateid=29 then coalesce(SUM(defenseallocation*SubscriptionCompanyPercent),0) end as 'AllocationDefense29', 
case when  s.policydateid=30 then coalesce(SUM(defenseallocation*SubscriptionCompanyPercent),0) end as 'AllocationDefense30', 
case when  s.policydateid=27 then coalesce(SUM((indemnityallocation+defenseallocation)*SubscriptionCompanyPercent),0) end as 'AllocationTotal27', 
case when  s.policydateid=28 then coalesce(SUM((indemnityallocation+defenseallocation)*SubscriptionCompanyPercent),0) end as 'AllocationTotal28', 
case when  s.policydateid=29 then coalesce(SUM((indemnityallocation+defenseallocation)*SubscriptionCompanyPercent),0) end as 'AllocationTotal29', 
case when  s.policydateid=30 then coalesce(SUM((indemnityallocation+defenseallocation)*SubscriptionCompanyPercent),0) end as 'AllocationTotal30',
case when subscriptioncompanysolvency like '%Insolvent%' then '[1]' else ' ' end as 'subscriptioncompanysolvency'
from tblExhaustion e
inner join tblPolicyDates pd on e.PolicyDateID=pd.PolicyDateID
left join @Subscriber s on s.policydateid=pd.PolicyDateID
where s.policydateid in (27,28,29,30) and ExhaustionDateID=@BillExhaustionDateID
group by subscriptioncompany, subscriptioncompanypercent, s.policydateid, subscriptioncompanysolvency

select SubscriptionCompany, Coalesce(SUM(AllocationIndemnity27),0) as AllocationIndemnity27, coalesce(sum(AllocationDefense27),0) as AllocationDefense27,
coalesce(sum(AllocationTotal27),0) as AllocationTotal27, Coalesce(SUM(AllocationIndemnity28),0) as AllocationIndemnity28, coalesce(sum(AllocationDefense28),0) as AllocationDefense28,
coalesce(sum(AllocationTotal28),0) as AllocationTotal28, Coalesce(SUM(AllocationIndemnity29),0) as AllocationIndemnity29, coalesce(sum(AllocationDefense29),0) as AllocationDefense29,
coalesce(sum(AllocationTotal29),0) as AllocationTotal29, Coalesce(SUM(AllocationIndemnity30),0) as AllocationIndemnity30, coalesce(sum(AllocationDefense30),0) as AllocationDefense30,
coalesce(sum(AllocationTotal30),0) as AllocationTotal30,  @Header as headerdate, @Footer as footerdate, subscriptioncompanysolvency
from @AllocationAmounts a
group by SubscriptionCompany, subscriptioncompanysolvency
order by SubscriptionCompany

return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptLondonBillingSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptLondonBillingSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptLondonBillingSummary] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptLondonBillingSummary] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptLondonBillingSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptLondonBillingSummary] TO [power_user]
GO
