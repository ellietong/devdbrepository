SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptResolvedByState] (@StateID int, @StartDate datetime, @EndDate datetime, @MatterID int)
AS
SET NOCOUNT ON

--DECLARE @StateID int
--DECLARE @StartDate date
--DECLARE @EndDate date
--SET @StateID = 44
--SET @StartDate='1/1/1900'
--SET @EndDate='1/1/2012'
--declare @matterid int
--set @matterid = null

DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @DiseaseCount Table (Matter nvarchar(250),
							ClaimID int, 
							 FullNameState nvarchar(250), 
							 YearFiled datetime, 
							 Disease nvarchar (100), 
							 LastName nvarchar (100), 
							 FirstName nvarchar(100),
							 SocialSecurityNumber nvarchar(250), 
							 filedate datetime, 
							 servicedate datetime,
							 localplaintiffcounsel nvarchar (200), 
							 jurisdiction nvarchar (200), 
							 docketnumber nvarchar (100), 
							 ClaimSequence nvarchar (200), 
							 LawsuitStatus nvarchar(200), 
							 settlementamount money, 
							 ResolutionDate datetime)
INSERT INTO @DiseaseCount
	SELECT Matter, C.ClaimID, s.FullNameState, year(COALESCE(FileDate,ServiceDate,'1/1/1900')) AS YearFiled, COALESCE(d.HighLevelDiseaseID,5) AS Disease, CPI.LastName, CPI.FirstName, 
		COALESCE('XXX-XX-'+RIGHT(CPI.SocialSecurityNumber,4),'') AS SocialSecurityNumber, 
		COALESCE(L.FileDate,'1/1/1900') AS filedate, 
		COALESCE(L.ServiceDate,'1/1/1900') AS servicedate, 
		pca.PlaintiffCounsel AS localplaintiffcounsel, j.Jurisdiction, DocketNumber, ClaimSequence, 
		CASE WHEN clst.ClaimLawsuitStatusID in (15,27) 
			 THEN 'Dis W/O P' 
			 ELSE ClaimLawsuitStatus END AS LawsuitStatus, 
		COALESCE(SettlementAmount,0) AS settlementamount, 
		COALESCE(SettlementDate,statusdate) AS resolutiondate
	FROM tblClaimantPersonalInfo CPI
		LEFT JOIN tblClaim C ON CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
		LEFT JOIN (SELECT CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease 
				   FROM tblClaimDisease CD 
				   WHERE PrimaryDisease = 1) CD ON CD.ClaimID=C.ClaimID
		LEFT JOIN tblDisease D ON CD.DiseaseID = D.DiseaseID
		LEFT JOIN tblHighLevelDisease HLD ON HLD.HighLevelDiseaseID=D.HighLevelDiseaseID
		LEFT JOIN tblClaimLawsuit CL ON C.ClaimID=CL.ClaimID
		LEFT JOIN tblLawsuit L ON CL.LawsuitID=L.LawsuitID
		LEFT JOIN tblJurisdiction J ON L.JurisdictionID=J.JurisdictionID
		--LEFT JOIN tblDefenseCounselState DCS ON L.DefenseCounselAddressID=DCS.DefenseCounselAddressID AND L.StateID=DCS.StateID
		LEFT JOIN tblState S ON L.StateID=S.StateID
		LEFT JOIN tblClaimSequence css ON css.ClaimSequenceID = c.ClaimSequenceID
		LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=CL.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		LEFT JOIN tblClaimantSettlement cst ON cst.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		Left JOIN tblMatter m on m.MatterID=c.MatterID
	WHERE s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID)
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus=1
		AND clst.ClaimLawsuitStatusID  in (15,16,23,24,27,28,30)
		and m.MatterID >= COALESCE(@MatterID, 0) AND m.MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
	ORDER BY s.[State], COALESCE(StatusDate, SettlementDate, ServiceDate), Jurisdiction, localplaintiffcounsel

SELECT Matter, FullNameState, YearFiled AS 'Year', hd.HighLevelDisease, LastName, FirstName, SocialSecurityNumber, FileDate, ServiceDate, localplaintiffcounsel, Jurisdiction, 
	DocketNumber, ClaimSequence, @startDate AS StartDate, @EndDate AS EndDate, LawsuitStatus, COALESCE(SettlementAmount,0) AS settlementamount, ResolutionDate
FROM @DiseaseCount DC
	LEFT JOIN tblHighLevelDisease hd ON hd.HighLevelDiseaseID = DC.Disease
WHERE ResolutionDate BETWEEN @StartDate AND @EndDate 
GROUP BY Matter, FullNameState, YearFiled, hd.HighLevelDisease, LastName, FirstName, SocialSecurityNumber, FileDate, ServiceDate, localplaintiffcounsel, Jurisdiction, 
	DocketNumber, ClaimSequence,LawsuitStatus, ResolutionDate, COALESCE(SettlementAmount,0)
ORDER BY FullNameState, ResolutionDate, lastname, firstname, YearFiled

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptResolvedByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedByState] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptResolvedByState] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptResolvedByState] TO [power_user]
GO
