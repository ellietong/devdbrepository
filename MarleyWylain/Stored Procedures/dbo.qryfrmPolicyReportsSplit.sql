SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmPolicyReportsSplit] 
(@insuranceProgramID int = NULL)
AS
SET NOCOUNT ON

DECLARE @maxInsuranceProgram int

-- Find max ID values
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM tblInsuranceProgram

SELECT p.PolicyID, p.MinPStartDate, p.CarrierID, p.InsuranceProgramID, p.NamedInsuredID, p.PolicyNum, p.PolicyNotes, p.PolicyPDF, p.InitialDataEntry, p.InitialDataEntryDate, p.PolicyReview, 
	p.PolicyReviewDate, ni.NamedInsured, ip.InsuranceProgram, pd.AnnualPeriod AS SplitAnnualPeriod, pd.PStartDate AS SplitStart, pd.PEndDate AS SplitEnd, c.CarrierName, c.CarrierSolvency, 
	cg.CarrierGroupName, cg.CarrierGroupID, pl.LimitTypeID, pl.AttachmentPoint, pl.PerOccLimit, pl.Layer, pl.LayerPerOccLimit, pl.AggregateLimit, pl.SIR
FROM (((((tblPolicy p INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID) INNER JOIN tblPolicyLimits pl ON p.PolicyID = pl.PolicyID) 
	INNER JOIN tblNamedInsured ni ON p.NamedInsuredID = ni.NamedInsuredID) INNER JOIN tblInsuranceProgram ip ON p.InsuranceProgramID = ip.InsuranceProgramID) 
	INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID) LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID
WHERE p.InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) 
ORDER BY ip.InsuranceProgram, pd.AnnualPeriod, pd.PStartDate

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyReportsSplit] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyReportsSplit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyReportsSplit] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyReportsSplit] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyReportsSplit] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyReportsSplit] TO [power_user]
GO
