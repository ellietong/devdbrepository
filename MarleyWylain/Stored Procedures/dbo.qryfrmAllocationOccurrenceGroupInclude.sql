SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE Procedure [dbo].[qryfrmAllocationOccurrenceGroupInclude]
(@allocationID int)
As

SET NOCOUNT ON

SELECT AllocationID, OccurrenceGroupID
FROM tblAllocationOccurrenceGroupInclude
WHERE AllocationID = @allocationID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationOccurrenceGroupInclude] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationOccurrenceGroupInclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationOccurrenceGroupInclude] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationOccurrenceGroupInclude] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationOccurrenceGroupInclude] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationOccurrenceGroupInclude] TO [power_user]
GO
