SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmOccurrenceGroup]
As

SET NOCOUNT ON

SELECT OccurrenceGroupID, OccurrenceGroup, OccGrpTriggerStart, OccGrpDiscount
FROM tblOccurrenceGroup
ORDER BY OccurrenceGroup

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceGroup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceGroup] TO [power_user]
GO
