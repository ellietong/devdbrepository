SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryrptSettledCaseSummaryByState] (@StateID int, 
					@SettlementYearStart datetime,
					@SettlementYEarEnd datetime,
					@MatterID int) 

as

Set NoCount On

--DECLARE @StateID int
--SET @StateID = 44
--DECLARE @SettlementYearStart datetime
--SET @SettlementYearStart = '1/1/1900'
--DECLARE @SettlementYearEnd datetime
--SET @SettlementYearEnd = '1/1/2012'
--declare @matterid int
--set @matterid = null

DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @SettledSummary table (Matter nvarchar(250),
				ClaimNumber int, 
				StateID int,
				State nvarchar(50),
				--SettlementYear datetime,
				--SettlementAmount money,
				--SettlementAverage money,
				SettlementDate datetime, 
				Meso_Count int,
				Meso_SettlementAmount money,
				Meso_Avg money,
				LungCancer_Count int,
				LungCancer_SettlementAmount money,
				LungCancer_Avg money,
				OtherCancer_Count int,
				OtherCancer_SettlementAmount money,
				OtherCancer_Avg money,
				NonMalig_Count int,
				NonMalig_SettlementAmount money,
				NonMalig_Avg money,
				Other_Count int,
				Other_SettlementAmount money,
				Other_Avg money,
				NoDisease_Count int,
				NoDisease_SettlementAmount money,
				NoDisease_Avg money)

Insert Into @SettledSummary
Select Matter,
	count(c.ClaimID) as ClaimantNumber,
	s.StateID,
	FullNameState,
	--Settlementdate as SettlementYear,
	--sum(SettlementAmount) as SettlementAmount,
	--sum(SettlementAmount)/count(c.ClaimID) as SettlementAverage, 
	convert(datetime,(convert(nvarchar,month(SettlementDate))+'/1/'+ convert(nvarchar,year(SettlementDate)))) as SettlementDate,
	
	sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) as Meso_Count,
	sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end) as Meso_SettlementAmount,
	Case When sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 1 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 1 then 1 else 0 end) end  as Meso_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) as LungCancer_Count,
	sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end) as LungCancer_SettlementAmount,
	Case When sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 2 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 2 then 1 else 0 end) end  as LungCancer_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) as OtherCancer_Count,
	sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end) as OtherCancer_SettlementAmount,
	Case When sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 3 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 3 then 1 else 0 end) end  as OtherCancer_Avg,
			
	sum(Case When HLD.HighLevelDiseaseID = 4 then 1 else 0 end) as NonMalig_Count,
	sum(Case When HLD.HighLevelDiseaseID = 4 then SettlementAmount else 0 end) as NonMalig_SettlementAmount,
	Case When sum(Case When HLD.HighLevelDiseaseID = 4 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 4 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 4 then 1 else 0 end) end  as NonMalig_Avg,
			
	Sum(Case When HLD.HighLevelDiseaseID = 5 then 1 else 0 end) as Other_Count,
	Sum(Case When HLD.HighLevelDiseaseID = 5 then SettlementAmount else 0 end) as Other_SettlementAmount,
	Case When sum(Case When HLD.HighLevelDiseaseID = 5 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 5 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 5 then 1 else 0 end) end  as Other_Avg,
			
	Sum(Case When HLD.HighLevelDiseaseID = 6 then 1 else 0 end) as NoDisease_Count,
	Sum(Case When HLD.HighLevelDiseaseID = 6 then SettlementAmount else 0 end) as NoDisease_SettlementAmount,	
	Case When sum(Case When HLD.HighLevelDiseaseID = 6 then 1 else 0 end) = 0 then 0 
		else sum(Case When HLD.HighLevelDiseaseID = 6 then SettlementAmount else 0 end)/ 
			sum(Case When HLD.HighLevelDiseaseID = 6 then 1 else 0 end) end  as NoDisease_Avg
			
From tblClaimantPersonalInfo i 
	Left Join tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
	Left Join tblClaimLawsuit cl on cl.claimID = c.claimID
	Left Join tblLawsuit l on cl.lawsuitid = l.lawsuitid
	Left Join tblState s on l.StateID = s.StateID
	Inner Join tblClaimantSettlement cs on cs.claimlawsuitID = cl.claimlawsuitID
	Left Join tblDisease d on cd.DiseaseID = d.DiseaseID
	left join tblHighLevelDisease HLD on HLD.HighLevelDiseaseID=d.HighLevelDiseaseID
	Left Join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=cl.ClaimLawsuitID
	Left JOIN tblMatter m on m.MatterID=c.MatterID
Where IsPrimaryForClaim = 1 AND SettlementAmount > 0 AND IsPrimaryStatus=1
	and m.MatterID >= COALESCE(@MatterID, 0) AND m.MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
Group By Matter, s.StateID, FullNameState, convert(datetime,(convert(nvarchar,month(SettlementDate))+'/1/'+ convert(nvarchar,year(SettlementDate)))),
	Settlementdate

Select Matter, [State],
	sum(coalesce(ClaimNumber,0)) as ClaimNumber, 
	--SettlementDate as SettlementYear,
	--sum(coalesce(SettlementAmount,0)) as SettlementAmount,
	--coalesce(SettlementAverage,0) as SettlementAverage,
	SettlementDate as SettlementDate, 
	sum(coalesce(Meso_Count,0)) as Meso_Count,
	sum(coalesce(Meso_SettlementAmount,0)) as Meso_SettlementAmount,
	sum(Case When Meso_Count=0 Then 0 Else coalesce(Meso_Avg,0)/Meso_Count End) as Meso_Avg,
	sum(coalesce(LungCancer_Count,0)) as LungCancer_Count,
	sum(coalesce(LungCancer_SettlementAmount,0)) as LungCancer_SettlementAmount,
	sum(Case When LungCancer_Count=0 Then 0 Else coalesce(LungCancer_Avg,0)/LungCancer_Count End) as LungCancer_Avg,
	sum(coalesce(OtherCancer_Count,0)) as OtherCancer_Count,
	sum(coalesce(OtherCancer_SettlementAmount,0)) as OtherCancer_SettlementAmount,
	sum(Case When OtherCancer_Count=0 Then 0 Else coalesce(OtherCancer_Avg,0)/OtherCancer_Count End) as OtherCancer_Avg,
	sum(coalesce(NonMalig_Count,0))as NonMalig_Count,
	sum(coalesce(NonMalig_SettlementAmount,0))as NonMalig_SettlementAmount,
	sum(Case When NonMalig_Count=0 Then 0 Else coalesce(NonMalig_Avg,0)/NonMalig_Count End) as NonMalig_Avg,
	sum(coalesce(Other_Count,0))as Other_Count,
	sum(coalesce(Other_SettlementAmount,0))as Other_SettlementAmount,
	sum(Case When Other_Count=0 Then 0 Else coalesce(Other_Avg,0)/Other_Count End) as Other_Avg,
	sum(coalesce(NoDisease_Count,0)) as NoDisease_Count,
	sum(coalesce(NoDisease_SettlementAmount,0)) as NoDisease_SettlementAmount,
	sum(Case When NoDisease_Count=0 Then 0 Else coalesce(NoDisease_Avg,0)/NoDisease_Count End) as NoDisease_Avg
From @SettledSummary 
Where SettlementDate between @SettlementYearStart and @SettlementYearEnd
AND StateID >= COALESCE(@StateID, 0) AND StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID)
Group By Matter, SettlementDate, [State], ClaimNumber 
Order By SettlementDate

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSettledCaseSummaryByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSettledCaseSummaryByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSettledCaseSummaryByState] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSettledCaseSummaryByState] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSettledCaseSummaryByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSettledCaseSummaryByState] TO [power_user]
GO
