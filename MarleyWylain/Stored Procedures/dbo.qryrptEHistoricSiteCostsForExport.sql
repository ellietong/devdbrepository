SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*The purpose of this script is to ALTER  a listing of historic cost totals by site to be exported for Carrier Demand requests

Created: 01/09/2008 - CK
Updated: 1/28/08 - add in site ID selection - CK
*/
CREATE PROc  [dbo].[qryrptEHistoricSiteCostsForExport] (@siteids nvarchar(500))
AS 
Set Nocount on

--DECLARE @SiteIDS nvarchar(500)
--set @SiteIDS = ''

--DECLARE VARIABLES
DECLARE @SiteStr nvarchar(4000)
DECLARE @Ssort nvarchar(4000)
DECLARE @SQL nvarchar(4000)
SET @SQL = ''
SET @SiteStr = ''

-- CREATE VARIABLE STRING FOR All SITE IDS
IF @siteids is not null AND @siteids <> '0' AND ltrim(rtrim(@siteids)) <> ''
	SET @SiteStr = '' + @siteids + ''
ELSE 
	BEGIN
		--Declare Cursor
		DECLARE Cur_SiteID CURSOR Local Fast_Forward for
		Select SiteID
		from tblenvirositesdetail
		order by SiteID
		
		--Open Cursor
		OPEN Cur_SiteID
		
		--Fetch Data
		FETCH NEXT FROM Cur_SiteID INTO @Ssort
		
		WHILE @@Fetch_Status = 0
		BEGIN
			SET @SiteStr = @SiteStr + @SSort + ','
				
			FETCH NEXT FROM Cur_SiteID INTO @Ssort
		END		
		CLOSE Cur_SiteID 
		DEALLOCATE Cur_SiteID 	
		SELECT @SiteStr = left (@SiteStr,len(@SiteStr)-1)
	END


SET @SQL = N'
select s.siteid, d.sitename, ReimbursableAmount as SiteCost, s.Vendorid, VendorName, invoicePDFLink,
case when invoicedate is null then paiddate ELSE Invoicedate END as InvDate, s.costrecordid as CostID, 
state, country, Min (BatesMin) as BatesMin, MAx(BatesMax )as BatesMax, excludedsite
from tblSiteCosts s
left join tblEnviroSitesDetail d on s.siteid = d.siteid
inner join tblstate st on d.stateid = st.stateid
inner join tblcountry c on c.countryid = d.countryid
LEFT JOIN tblVendor V on v.vendorid = s.vendorid
LEFT JOIN tblBatesRanges  B on b.costrecordid = s.costrecordid
WHERE  ReimbursableAmount <>0 AND  s.SiteID IN (' + CONVERT(nvarchar (4000), @SiteStr) + ')
Group BY s.siteid, d.sitename, ReimbursableAmount, s.Vendorid, VendorName, invoicePDFLink,
case when invoicedate is null then paiddate ELSE Invoicedate END , s.costrecordid , 
 state, country, excludedsite'

EXEC sp_executeSQL @SQL

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptEHistoricSiteCostsForExport] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptEHistoricSiteCostsForExport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptEHistoricSiteCostsForExport] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptEHistoricSiteCostsForExport] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptEHistoricSiteCostsForExport] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptEHistoricSiteCostsForExport] TO [power_user]
GO
