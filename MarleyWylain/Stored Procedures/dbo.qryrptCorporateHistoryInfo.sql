SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptCorporateHistoryInfo] (@CorporateHistoryID INT)
AS 
SET NOCOUNT ON

--DECLARE @CorporateHistoryID INT
--SET @CorporateHistoryID = 1

DECLARE @InclSites TABLE (CorpHistID INT, InclSites INT)
INSERT INTO @InclSites
SELECT hip.corporatehistoryID, Count(distinct sd.siteid) as InclSites
FROM tblEnviroSitesDetail SD
INNER JOIN tblCorpHistInsProgram hip ON sd.CorporateHistoryID = hip.CorporateHistoryID
INNER JOIN tblCorporateHistoryProfile hp ON hip.corporatehistoryid = hp.corporatehistoryid
Where sd.ExcludedSite <> 1
Group BY hip.corporatehistoryID
SELECT hp.corporatehistoryid, hp.corporatehistoryprofile, 
Count(sd.SiteID) as NumSites,
hip.[from], hip.until, hip.programdescription, ins.inclsites
FROM tblCorporateHistoryProfile hp
INNER JOIN tblCorpHistInsProgram hip ON hp.corporateHistoryID = hip.corporatehistoryid
INNER JOIN tblEnviroSitesDetail sd ON hip.CorporateHistoryID = sd.CorporateHistoryID
LEFT JOIN @InclSites ins ON hp.corporateHistoryID = ins.CorpHistID
WHERE hp.corporatehistoryid = @CorporateHistoryID
GROUP BY hp.corporatehistoryid, hp.corporatehistoryprofile, hip.[from], hip.until, hip.programdescription, ins.inclsites
RETURN 

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorporateHistoryInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCorporateHistoryInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorporateHistoryInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCorporateHistoryInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorporateHistoryInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCorporateHistoryInfo] TO [power_user]
GO
