SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qryrptSubscriptionCoverageByPolicyByCompany]
(@SubscriberID int = NULL, @Solvency varchar(50) = NULL, @InsuranceProgramID int = NULL, @limitTypeID int = NULL, @SchemeID int = NULL)
AS
SET NOCOUNT ON

--DECLARE @InsuranceProgramID int 
--DECLARE @SubscriberID int 
--DECLARE @Solvency varchar(50) 
--DECLARE @limitTypeID int
--DECLARE @SchemeID int
--SET @InsuranceProgramID = 0
--SET @SubscriberID = 0
--SET @Solvency = null
--SET @limitTypeID = 1
--SET @SchemeID = 28

DECLARE @maxInsuranceProgramID int
DECLARE @maxSubscriberID int
DECLARE @maxLimitTypeID int
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxInsuranceProgramID = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxSubscriberID = MAX(SubscriberID) FROM vSubscription

IF @SchemeID <> 0 BEGIN
	SELECT MIN(p.PStartDate) AS MinOfPStartDate, MAX(p.PEndDate) AS MaxOfPEndDate, MIN(p.AnnualPeriod) AS MinOfAnnualPeriod, 
		p.InsuranceProgram, s.SubscriptionCompany, s.SubscriptionCompanySolvency, s.SubscriptionSection,
		s.SubscriptionActualPercentOfPolicy,
		s.SubscriptionAllocationPercentOfPolicy,
		(s.SubscriptionActualPercentOfPolicy * p.PerOccLimit) AS SubscriberPerOccActual, 
		(s.SubscriptionAllocationPercentOfPolicy * p.PerOccLimit) AS SubscriberPerOccAllocation,
		p.CarrierID, p.InsuranceProgramID, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
		SIR, AggregateLimit, Layer, p.PolicyNum, p.PolicyID, s.SubscriberID, s.SubscriptionCoverageID, AgencyName, UWReference, AcctNum, s.StartDate, s.EndDate, SubscriptionSort
	FROM vPolicy p
		INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID
	WHERE p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
		AND p.limitTypeID >= COALESCE(@limitTypeID, 0) AND p.limitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)
		AND s.SubscriberID >= COALESCE(@SubscriberID, 0) AND s.SubscriberID <= COALESCE(NULLIF(@SubscriberID, 0), @maxSubscriberID)
		AND s.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%') 
		AND DoNotReport <>1
		AND s.SubscriberID IN(SELECT SubscriberID FROM vSchemeSubscribers WHERE SchemeID = @SchemeID)
	GROUP BY p.InsuranceProgram, s.SubscriptionCompany, s.SubscriptionCompanySolvency, s.SubscriptionSection,
		s.SubscriptionActualPercentOfPolicy,
		s.SubscriptionAllocationPercentOfPolicy,
		(s.SubscriptionActualPercentOfPolicy * p.PerOccLimit), 
		(s.SubscriptionAllocationPercentOfPolicy * p.PerOccLimit),
		p.CarrierID, p.InsuranceProgramID, 	AttachmentPoint, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, Layer, PolicyNum, p.PolicyID, 
		s.SubscriberID, s.SubscriptionCoverageID, AgencyName, UWReference, AcctNum, s.StartDate, s.EndDate, SubscriptionSort
	ORDER BY SubscriptionSort
END
ELSE BEGIN
	SELECT MIN(p.PStartDate) AS MinOfPStartDate, MAX(p.PEndDate) AS MaxOfPEndDate, MIN(p.AnnualPeriod) AS MinOfAnnualPeriod, 
		p.InsuranceProgram, s.SubscriptionCompany, s.SubscriptionCompanySolvency, s.SubscriptionSection,
		s.SubscriptionActualPercentOfPolicy,
		s.SubscriptionAllocationPercentOfPolicy,
		(s.SubscriptionActualPercentOfPolicy * p.PerOccLimit) AS SubscriberPerOccActual, 
		(s.SubscriptionAllocationPercentOfPolicy * p.PerOccLimit) AS SubscriberPerOccAllocation,
		p.CarrierID, p.InsuranceProgramID, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
		SIR, AggregateLimit, Layer, p.PolicyNum, p.PolicyID, s.SubscriberID, s.SubscriptionCoverageID, AgencyName, UWReference, AcctNum, s.StartDate, s.EndDate, SubscriptionSort
	FROM vPolicy p
		INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID
	WHERE p.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID)
		AND p.limitTypeID >= COALESCE(@limitTypeID, 0) AND p.limitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)
		AND s.SubscriberID >= COALESCE(@SubscriberID, 0) AND s.SubscriberID <= COALESCE(NULLIF(@SubscriberID, 0), @maxSubscriberID)
		AND s.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%') 
		AND DoNotReport <>1
	GROUP BY p.InsuranceProgram, s.SubscriptionCompany, s.SubscriptionCompanySolvency, s.SubscriptionSection,
		s.SubscriptionActualPercentOfPolicy,
		s.SubscriptionAllocationPercentOfPolicy,
		(s.SubscriptionActualPercentOfPolicy * p.PerOccLimit), 
		(s.SubscriptionAllocationPercentOfPolicy * p.PerOccLimit),
		p.CarrierID, p.InsuranceProgramID, 	AttachmentPoint, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, Layer, PolicyNum, p.PolicyID, 
		s.SubscriberID, s.SubscriptionCoverageID, AgencyName, UWReference, AcctNum, s.StartDate, s.EndDate, SubscriptionSort
	ORDER BY SubscriptionSort
END

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompany] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompany] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompany] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompany] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompany] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompany] TO [power_user]
GO
