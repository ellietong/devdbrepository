SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmDefenseInvoiceLookup]
As
Set NoCount On

SELECT DefenseInvoiceID, InvoiceNumber + ' - ' + Period +' - $'+ convert(nvarchar(250),InvoiceTotal) as DefenseInvoice
FROM vtblDefenseInvoice ORDER BY InvoiceNumber, Period

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefenseInvoiceLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmDefenseInvoiceLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefenseInvoiceLookup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmDefenseInvoiceLookup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmDefenseInvoiceLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmDefenseInvoiceLookup] TO [power_user]
GO
