SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmOccurrenceType]
As

SET NOCOUNT ON
SELECT *
FROM tblOccurrenceType
ORDER BY OccurrenceType

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceType] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccurrenceType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccurrenceType] TO [power_user]
GO
