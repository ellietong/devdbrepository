SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qrysrptByYearByLayerProvisionLanguage]
(@policyid int,@provisionlabelid int)
AS
SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

DECLARE @MaxPolicyID INT
SELECT @MaxPolicyID = Max(PolicyID) from tblPolicy

--DECLARE @PolicyID int
--DECLARE @ProvisionLabelID int
--SET @PolicyID = 1
--SET @provisionlabelid = 1

----------------------------------------------------------------------------
DECLARE @SRPTTable TABLE(PolicyID INT, ProvisionLabelID INT, Provision varchar(75), Provisionvalue varchar (300), LanguageID INT, 
	BatesStart varchar(50), BatesEnd varchar(50), ExactLanguage varchar(8000), ProvisionNotes varchar(8000), Location varchar(50), 
	langtype varchar (100),EndorsementNum varchar(50),  m_Sort INT, LocationID INT)

	INSERT INTO @SRPTTable
	SELECT el.PolicyID, 
	PL.ProvisionLabelID, 
	PL.Provision, 
	PVV.Provisionvalue,
	EL.ExactLanguageID, 
	EL.BatesStart, 
	EL.BatesEnd, 
	EL.ExactLanguage, 
	EL.LanguageNotes, 
	LOC.Location, 
	DetailedLocation,
	EL.EndorsementNum, 
	EL.Sort, 
	LOC.LocationID
	FROM  dbo.tblExactLanguage EL 
inner join tblProvisionLanguage pvl on pvl.ExactLanguageID = el.exactlanguageid
LEFT JOIN dbo.tblProvisionLabel PL ON PVL.ProvisionLabelID = PL.ProvisionLabelID
INNER JOIN tblprovision psn on psn.policyid = el.policyid 
INNER JOIN tblprovisionvalue pvv on pvv.provisionlabelid = pl.provisionlabelid and pvv.provisionvalueid = psn.provisionvalueid
LEFT JOIN dbo.tblLocation LOC ON el.LocationID = LOC.LocationID
LEFT join tblDetailedLocation dl on dl.detailedLocationID = el.detailedlocationid
		WHERE el.policyid >= COALESCE(@policyid, 0) AND 
el.policyid <= COALESCE(NULLIF(@policyid, 0), @MaxPolicyID)
		AND pl.ProvisionLabelID = @provisionlabelid
	and isnull(donotshow, 0)<>1
--the above line was not working because donotshow was null for most records, which cannot be compared
--to an int. Now NULLs default to 0
	
SELECT PolicyID, m_sort, ProvisionNotes,location,batesstart,batesend, langtype,endorsementnum,Provisionlabelid,provisionvalue,
CASE 

	WHEN  ExactLanguage <> 'No Policy Language Found.' 
		AND ExactLanguage <> 'No Relevant Policy Language Found.' 
		AND ExactLanguage <> 'Policy Language Not Analyzed.'
		THEN ExactLanguage --+ ' [' + location + ', effective ' + expr1 + ', at ABC-' + right(BatesStart,6) + ']' + '.'
	WHEN ExactLanguage = 'No Policy Language Found.' or ExactLanguage = 'No Relevant Policy Language Found.'
		or ExactLanguage = 'Policy Language Not Analyzed.'
		THEN ExactLanguage 
	WHEN rtrim(ltrim(ExactLanguage)) is null THEN 'Policy Language Not Analyzed.'

END AS Language
From @SRPTTable
where ProvisionLabelID is not null

Group by PolicyID, m_sort, ProvisionNotes,location,batesstart,batesend,langtype,endorsementnum, provisionlabelid,provisionvalue,
CASE 

	WHEN  ExactLanguage <> 'No Policy Language Found.' 
		AND ExactLanguage <> 'No Relevant Policy Language Found.' 
		AND ExactLanguage <> 'Policy Language Not Analyzed.'
		THEN ExactLanguage-- + ' [' + location + ', effective ' + expr1 + ', at ABC-' + right(BatesStart,6) + ']' + '.'
	WHEN ExactLanguage = 'No Policy Language Found.' or ExactLanguage = 'No Relevant Policy Language Found.'
		or ExactLanguage = 'Policy Language Not Analyzed.'
		THEN ExactLanguage 
	WHEN rtrim(ltrim(ExactLanguage)) is null THEN 'Policy Language Not Analyzed.'
--'No Policy Language Found.'
END



--grant execute on dbo.qrysrptByYearByLayerProvisionLanguage to power_user, base_user
SET CONCAT_NULL_YIELDS_NULL ON
RETURN

RETURN




GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptByYearByLayerProvisionLanguage] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptByYearByLayerProvisionLanguage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptByYearByLayerProvisionLanguage] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysrptByYearByLayerProvisionLanguage] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptByYearByLayerProvisionLanguage] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptByYearByLayerProvisionLanguage] TO [power_user]
GO
