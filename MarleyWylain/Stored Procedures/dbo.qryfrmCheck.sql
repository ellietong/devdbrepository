SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmCheck] (@CheckID int)

As
Set NoCount On

Select * from tblCheckApproval Where CheckID = @CheckID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheck] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheck] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheck] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheck] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheck] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheck] TO [power_user]
GO
