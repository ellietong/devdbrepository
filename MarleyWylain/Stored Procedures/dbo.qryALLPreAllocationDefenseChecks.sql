SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryALLPreAllocationDefenseChecks]
(@Scenario int, @AllocationID int, @Errors int OUTPUT)
AS
SET NOCOUNT ON

--PRE ALLOCATION CHECKS

--Do all combinations existant in policies for this scenario have a treatment coded?
--Will be input Parameter
--DECLARE @Scenario int
--SET @Scenario = 2
--DECLARE @Allocationid int
--SET @AllocationID = 1

--Will be output Parameter
--DECLARE @Errors int


SET @Errors = 0

--Create temp table
CREATE TABLE #tempPolicyCombos(PolicyID int)

--Create temp table
CREATE TABLE #tempCurrentCombos(ComboID int)

--Create string to know what the columns of your pivot table should be
DECLARE @pivotColumns nvarchar(2000)
SET @pivotColumns = ''
SELECT @pivotColumns =   @pivotColumns + '[' + cast(ProvisionLabelID AS nvarchar(10)) + '],'
FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @pivotColumns = LEFT(@pivotColumns, LEN(@pivotColumns) - 1)

--Create string to know what the columns of your  temp table should be 
--(same columns as @pivotColums just different format for how it will be used)
DECLARE @alterColumns nvarchar(2000)
SET @alterColumns = ''
 SELECT @alterColumns = @alterColumns + '[' + cast(ProvisionLabelID AS varchar) + '] int,'
 FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @alterColumns = LEFT(@alterColumns, LEN(@alterColumns) - 1)

--Create a string to see if any of the provisions are not coded for a particular policy
--(same columns as @pivotColums just different format for how it will be used)
DECLARE @SumofColumns nvarchar(2000)
SET @SumofColumns = ''
 SELECT @SumofColumns = @SumofColumns + '[' + cast(ProvisionLabelID as varchar) + '] +'
 FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @SumofColumns = LEFT(@SumofColumns, LEN(@SumofColumns) - 1)

--Find number of provisions this scenario is using -- this is used along with finding out what they actually are
--to see if the combination already exists.  (Count is necessary because a combination could use the 3 provisions,
--but also have a 4th and 5th for example so we would not want to use these combinations)
DECLARE @ProvisionCount int
SELECT @ProvisionCount= COUNT(provisionlabelid) 
FROM tblScenarioProvisions WHERE ScenarioID = @Scenario


DECLARE @sql nvarchar(4000)
--Update temp table for policy data to have appropriate columns
SET @sql = 'ALTER TABLE #tempPolicyCombos
ADD ' + @alterColumns
EXEC(@sql)
--perform transform on Policy Provision data and insert it into temp table
SET @sql = 'INSERT #tempPolicyCombos
SELECT  * 
FROM
(SELECT policyid, provisionlabelid, provisionvalueid
FROM tblProvision) datatable
pivot
(MIN(provisionvalueid) for provisionlabelid in (' + @pivotColumns + ')) AS pvt'
EXEC (@sql)


--Update temp table for current combination data to have appropriate columns
SET @sql = 'ALTER TABLE #tempCurrentCombos
ADD ' + @alterColumns
EXEC(@sql)
--perform transform on current combination data and insert it into temp table  
SET @sql = 'INSERT #tempCurrentCombos
select * 
from
(select combination, provisionlabelid, provisionvalueid
from tblcombination where combination in (select combination from tblcombination group by combination
having COUNT(provisionlabelid) = ' +CONVERT(nvarchar(50), @ProvisionCount) + ' )) datatable
pivot
(min(provisionvalueid) for provisionlabelid in ('+@pivotColumns+ ')) pivottable'
EXEC (@sql)




--insert into temp table those coded combos that are not already combos
CREATE TABLE #tempNewCombos(comboID int)
SET @sql = 'ALTER TABLE #tempNewCombos
ADD  ' + @alterColumns 
EXEC(@sql)

SET @sql = 'INSERT #tempNewCombos
SELECT  0,' + @pivotColumns + ' 
from #tempPolicyCombos
WHERE (' + @SumofColumns + ') is not null
EXCEPT
SELECT 0,' + @pivotColumns + ' 
from #tempCurrentCombos   

group by ' + @pivotColumns
EXEC(@sql)

--@Errors = 1 -There are new Provision codings that are not reflected in Combo Table - Need to run combo creator! 
if (SELECT COUNT(comboid) from #tempNewCombos) > 0 
SET @Errors = 1

--@Errors = 2 -There are Combinations that are not yet associated with this Scenario - Need to run combo Creator!
if (SELECT COUNT(comboID) from #tempCurrentCombos where ComboID not in 
	(select ComboID from tblCombinationTreatment where Scenarioid = @Scenario)) > 0
SET @Errors = 2

--@Errors = 3 -There are Combinations for this treatment that do not have treatments coded - Go code treatments!
if (select COUNT(Combination) from tblCombinationTreatment where Scenarioid = @Scenario and TreatmentID is null)>0
SET @Errors = 3

--@Errors = 4 - There are Policies that do not have the necessary provisions coded to determine defense treatment under this scenario -- Review your policy provisions!
Declare @PoliciesMissingProvisions table (PolicyID int)
SET @sql = 'select policyid
from tblPolicy
where PolicyID not in (select PolicyID from tblAllocationPolicyExclude Where AllocationID ='+ CONVERT(nvarchar(50),@allocationID)+')
and PolicyID not in (select PolicyID from tblProvision where ProvisionValueID in 
(select ProvisionValueID from tblAllocationProvisionToExclude Where AllocationID ='+ CONVERT(nvarchar(50),@allocationID)+'))
except
select PolicyID from #tempPolicyCombos WHERE (' + @SumofColumns + ') is not null'
INSERT into @PoliciesMissingProvisions
exec(@sql)

if (Select COUNT(policyid) from @PoliciesMissingProvisions)>0 
SET @Errors = 4


--SELECT @Errors

DROP TABLE #tempPolicyCombos
DROP TABLE #tempCurrentCombos
DROP TABLE #tempNewCombos

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLPreAllocationDefenseChecks] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLPreAllocationDefenseChecks] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLPreAllocationDefenseChecks] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryALLPreAllocationDefenseChecks] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLPreAllocationDefenseChecks] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLPreAllocationDefenseChecks] TO [power_user]
GO
