SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[qryfrmPolicyLimits]
(@policyid int)
as
set nocount on

--DECLARE @PolicyID INT
--SET @policyid = 2

select limitid, policyid, limittypeid, attachmentpoint, perocclimit, layerperocclimit, 
sir, siraggregatelimit, aggregatelimit, previouslyexhaustedaggregate, layer, limitnote
from tblPolicyLimits pl
where policyid = @policyid
GROUP BY limitid, policyid, limittypeid, attachmentpoint, perocclimit, layerperocclimit, 
sir, siraggregatelimit, aggregatelimit, previouslyexhaustedaggregate, layer, limitnote

order by policyid, limittypeid

return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLimits] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLimits] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLimits] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLimits] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLimits] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLimits] TO [power_user]
GO
