SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



Create Procedure [dbo].[qryrptCheckGenerationFirms](@FirmID int, @CheckNo int)

As

SET NOCOUNT ON

DECLARE @maxFirmID int
Declare @MaxCheckNo int
SELECT @maxFirmID = MAX(firmID) from tblDefenseInvoiceFirm
SELECT @MaxCheckNo = MAX(CheckNo) from tblChecks

SELECT c.FirmID,Firm, PayTo, FirmAddress,C.CheckNo, C.AmountDesc, C.checkAmount, C.CheckDate,c.CommentLine,
C.PlaintiffCounselName, C.PCounselAddress, c.FundTypeID
FROM	 tblChecks C 
Left JOIN tblDefenseInvoiceFirm DIF ON C.FirmID = DIF.FirmID
left Join tblDefenseInvoice DI on C.firmID = DI.FirmID 
LEFT JOIN tblDefenseInvoiceStatus DIS ON Di.StatusID = DIS.StatusID

Where c.FirmID >= COALESCE(@FirmID,0) AND c.FirmID <= COALESCE(NULLIF(@FirmID,0),@MaxFirmID)
AND c.CheckNo>= COALESCE(@checkno,0) AND C.CheckNo <= COALESCE(NULLIF(@CheckNo,0),@MaxCheckNo)
Group by c.FirmID,Firm, PayTo, FirmAddress,C.CheckNo, C.AmountDesc, C.checkAmount, C.CheckDate,c.CommentLine,
C.PlaintiffCounselName, C.PCounselAddress, c.FundTypeID
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGenerationFirms] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckGenerationFirms] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGenerationFirms] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckGenerationFirms] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckGenerationFirms] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckGenerationFirms] TO [power_user]
GO
