SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmInsuranceProgram]
AS
SET NOCOUNT ON
SELECT InsuranceProgramID, InsuranceProgram
FROM tblInsuranceProgram
ORDER BY InsuranceProgram
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInsuranceProgram] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmInsuranceProgram] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInsuranceProgram] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmInsuranceProgram] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInsuranceProgram] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmInsuranceProgram] TO [power_user]
GO
