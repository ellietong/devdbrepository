SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmAllocationSIR]
AS
SELECT SIRID, SIRTreatment
FROM tblAllocationSIR
WHERE Hide = 0
ORDER BY SIRTreatment
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationSIR] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationSIR] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationSIR] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationSIR] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationSIR] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationSIR] TO [power_user]
GO
