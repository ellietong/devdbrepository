SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create PROCEDURE [dbo].[qrysfrmExhaustion]
(@policydateid int = null)
AS SELECT     dbo.tblExhaustion.*
FROM         dbo.tblExhaustion
WHERE PolicyDateID = @policydateid


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmExhaustion] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmExhaustion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmExhaustion] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysfrmExhaustion] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmExhaustion] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmExhaustion] TO [power_user]
GO
