SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmCheckAddAll] (@MatterID                  INT,
									  @FundingTypeID             INT,
									  @CheckID                   INT,
									  @VendorFirmID              INT = NULL,
									  @PlaintiffCounselAddressID INT = NULL,
									  @DefenseCounselAddressID   INT = NULL)
AS
SET NOCOUNT ON

------VARIABLES FOR TESTING----
--DECLARE @MatterID                  INT = 1
--DECLARE @FundingTypeID             INT = 1
--DECLARE @CheckID                   INT = 13782
--DECLARE @DefenseCounselAddressID   INT = 123
--DECLARE @PlaintiffCounselAddressID INT = NULL
--DECLARE @VendorFirmID              INT = NULL

IF OBJECT_ID('tempdb..#ExistingClaimSettlements') IS NOT NULL DROP TABLE #ExistingClaimSettlements
CREATE TABLE  #ExistingClaimSettlements (ClaimSettlementID INT)
INSERT INTO   #ExistingClaimSettlements SELECT ClaimSettlementID FROM tblPaymentSettlement WHERE CheckID = @CheckID

IF OBJECT_ID('tempdb..#ExistingDefenseInvoices')  IS NOT NULL DROP TABLE #ExistingDefenseInvoices
CREATE TABLE  #ExistingDefenseInvoices (DefenseInvoiceID INT)
INSERT INTO   #ExistingDefenseInvoices SELECT DefenseInvoiceID   FROM tblPaymentDefense    WHERE CheckID = @CheckID

DECLARE @ApprovedCheck INT = (SELECT CASE WHEN ca.CheckID IS NOT NULL THEN 1 ELSE 0 END
							  FROM tblCheck c 
							      LEFT JOIN tblCheckApproval ca ON c.CheckID = ca.CheckID 
							  WHERE c.CheckID = @CheckID)

IF @ApprovedCheck = 0
BEGIN
	--Defense Invoices
	If @FundingTypeID = 1 
	BEGIN 
		INSERT INTO tblPaymentDefense (DefenseInvoiceID, CheckID, PaymentAmount)
			SELECT di.DefenseInvoiceID, @CheckID, InvoiceTotal - SUM(COALESCE(PaymentAmount,0))
			FROM vtblDefenseInvoice di
				LEFT JOIN tblDefenseInvoice                                                 div ON div.DefenseInvoiceID = di.DefenseInvoiceID
				LEFT JOIN (SELECT DefenseInvoiceID, PaymentAmount 
							FROM tblcheckapproval ca 
								INNER JOIN tblPaymentDefense pd on ca.CheckID = pd.CheckID) a   ON a.DefenseInvoiceID = di.DefenseInvoiceID
			WHERE DefenseCounselAddressID = @DefenseCounselAddressID 
				AND MatterID = @MatterID 
				AND div.DatePaymentApproved IS NOT NULL
				AND di.DefenseInvoiceID NOT IN(SELECT DefenseInvoiceID FROM #ExistingDefenseInvoices)
			GROUP BY di.DefenseInvoiceID, DefenseCounselAddressID, di.InvoiceNumber, di.Period, InvoiceTotal
			HAVING InvoiceTotal - SUM(COALESCE(PaymentAmount,0)) <> 0
	END 
	ELSE 

	--Settlements
	IF @FundingTypeID = 2 
	BEGIN 
		INSERT INTO tblPaymentSettlement (ClaimSettlementID, CheckID, PaymentAmount)
			SELECT cs.ClaimSettlementID, @CheckID, SettlementAmount - SUM(COALESCE(PaymentAmount,0))
			From tblClaim c
				INNER JOIN tblClaimLawsuit                                                     cl ON c.ClaimID         = cl.ClaimID
				INNER JOIN tblClaimSettlement                                                  cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
				INNER JOIN tblLawsuit                                                          l  ON l.LawsuitID       = cl.LawsuitID
				LEFT  JOIN (SELECT ClaimSettlementID, PaymentAmount 
							FROM tblCheckApproval ca 
								INNER JOIN tblPaymentSettlement ps ON ca.CheckID = ps.CheckID) a  ON a.ClaimSettlementID = cs.ClaimSettlementID
			WHERE COALESCE(IsCurrentClaim,0) = 1
				AND c.MatterID = @MatterID
				AND l.PlaintiffCounselAddressID = @PlaintiffCounselAddressID
				AND c.ClaimSequenceID NOT IN (4, 2, 3, 11)
				AND cs.PaymentApprovedDate IS NOT NULL
				AND a.ClaimSettlementID NOT IN(SELECT ClaimSettlementID FROM #ExistingClaimSettlements)
			GROUP BY cs.ClaimSettlementID, SettlementAmount
			HAVING SettlementAmount - SUM(COALESCE(PaymentAmount,0)) <> 0
	END
	ELSE

	--Vendor Invoices
	IF @FundingTypeID = 3 
	BEGIN 
		INSERT INTO tblPaymentDefense (DefenseInvoiceID, CheckID, PaymentAmount)
			SELECT di.DefenseInvoiceID, @CheckID, InvoiceTotal - SUM(COALESCE(PaymentAmount,0))
			FROM vtblDefenseInvoice di
				LEFT JOIN tblDefenseInvoice div ON div.DefenseInvoiceID = di.DefenseInvoiceID
				LEFT JOIN (SELECT DefenseInvoiceID, PaymentAmount 
						   FROM tblCheckApproval ca 
							   INNER JOIN tblPaymentDefense pd ON ca.CheckID = pd.CheckID) a ON a.DefenseInvoiceID = di.DefenseInvoiceID
			WHERE VendorFirmID = @VendorFirmID 
				AND MatterID = @MatterID 
				AND div.DatePaymentApproved IS NOT NULL
				AND di.DefenseInvoiceID NOT IN(SELECT DefenseInvoiceID FROM #ExistingDefenseInvoices)
			GROUP BY di.DefenseInvoiceID, InvoiceTotal
			HAVING InvoiceTotal - SUM(Coalesce(PaymentAmount,0)) <> 0
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddAll] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddAll] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddAll] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddAll] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddAll] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddAll] TO [power_user]
GO
