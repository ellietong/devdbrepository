SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptCoverageEntityPolEvidence] (@CorporateHistoryID int)
AS 
SET NOCOUNT ON

--DECLARE @CorporateHistoryID int
--SET @CorporateHistoryID = 0

DECLARE @MaxCorporateHistoryID int
SET @MaxCorporateHistoryID= (Select MAX(corporatehistoryid) from tblCorporateHistoryProfile)


DECLARE @InclSites TABLE (CorporateHistoryID INT, InclSites INT)
INSERT INTO @InclSites
SELECT sd.CorporateHistoryID, Count(distinct sd.siteid) as InclSites
FROM tblEnviroSitesDetail SD
Where ExcludedSite <> 1
GROUP BY sd.CorporateHistoryID


--PastCosts
declare @Amount table (SiteID int, SiteName nvarchar (150), state nvarchar(10), country nvarchar(50), Billed money,
 Adtl_Amt money)
insert into @Amount
select s.siteid, sd.sitename, t.[state], c.country,
CASE WHEN (Billed = 1 )THEN sum(ReimbursableAmount) ELSE 0 END as Billed,
CASE WHEN ((Billed <>1)) THEN sum(ReimbursableAmount) ELSE 0 END as AdtlAmt
from tblSiteCosts s
left Join tblEnviroSitesDetail SD on s.siteid = sd.siteid
left join tblstate t on t.stateid = sd.stateid
left join tblcountry c on c.countryid = sd.countryid
group by s.siteid, sd.sitename, RecordSourceID, billed, t.[state], c.country

declare @PastSite table (SiteID int,  Bill money,  Adtl money)
insert into @PastSite
SELECT SiteID, SUM(billed) as BilledAmt,
	Sum(Adtl_Amt) as AdtlAmt
FROM @Amount
GROUP BY SiteID

--Future Costs
DECLARE @FutureCosts table (SiteID int, Liab decimal(18,2), FutureCosts money, PVD money, NRD money)
INSERT INTO @FutureCosts
SELECT fc.SiteID, 
CASE WHEN fc.LiabPercent IS NULL THEN 0 ELSE fc.LiabPercent END AS Liab, 
SUM(CASE WHEN sd.sitetypeid = 4 THEN fc.TotalCosts
	ELSE(fc.TotalCosts * fc.LiabPercent) 
	END) AS FutureCosts,
sum(fc.PVD * fc.LiabPercent) AS 'PVD', sum(fc.NRD * fc.LiabPercent) AS 'NRD'
FROM tblFutureCosts fc
LEFT JOIN  tblEnviroSitesDetail sd on fc.SiteID = sd.SiteID
WHERE ExcludedSite <> 1
GROUP BY fc.SiteID, CASE WHEN fc.LiabPercent IS NULL THEN 0 ELSE fc.LiabPercent END


SELECT  sd.CorporateHistoryID, CorporateHistoryProfile, Count(sd.SiteID) as NumSites, I.InclSites,
sum(CASE WHEN p.Bill is null then 0 else p.Bill end +
CASE WHEN Adtl is null then 0 else Adtl end + 
CASE when FC.futurecosts is null then 0 else fc.futurecosts END) as ProgramTotal
FROM tblEnviroSitesDetail SD
LEFT JOIN tblCorporateHistoryProfile chp ON SD.CorporateHistoryID = chp.CorporateHistoryID
LEFT JOIN @PastSite P on p.siteid = Sd.siteid
LEFT JOIN @FutureCosts FC on FC.siteid = sd.siteid
left Join @InclSites I on I.CorporateHistoryID = SD.CorporateHistoryID
WHERE sd.CorporateHistoryID >= COALESCE(@CorporateHistoryID, 0) 
AND SD.CorporateHistoryID <= COALESCE(NULLIF(@CorporateHistoryID, 0), @maxCorporateHistoryID)

GROUP BY sd.CorporateHistoryID, CorporateHistoryProfile, I.InclSites

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCoverageEntityPolEvidence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCoverageEntityPolEvidence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCoverageEntityPolEvidence] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCoverageEntityPolEvidence] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCoverageEntityPolEvidence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCoverageEntityPolEvidence] TO [power_user]
GO
