SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmClaimLawsuitInfo] (@ClaimID int)
as

Select *	
From tblClaimLawsuit
Where ClaimID = @ClaimID
Order By IsPrimaryForClaim

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitInfo] TO [power_user]
GO
