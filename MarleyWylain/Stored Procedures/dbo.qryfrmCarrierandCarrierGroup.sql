SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[qryfrmCarrierandCarrierGroup]
(@carrierID int  = null, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL)
AS
SET NOCOUNT ON

DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)

-- Find max ID values
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
IF @carrierGroupID = 0 SET @carrierGroupID = NULL

SELECT CarrierID, CarrierGroupName, CarrierName, CarrierSolvency, PolicyHolder, CarrierGroupID
FROM vPolicy
WHERE CarrierID >= COALESCE(@carrierID, 0) AND CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
	(CarrierGroupID >= COALESCE(@carrierGroupID, 0) AND CarrierGroupID <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR CarrierGroupID = @carrierGroupID) AND
	CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%')
GROUP BY CarrierID, CarrierGroupName, CarrierName, CarrierSolvency, PolicyHolder, CarrierGroupID
ORDER BY CarrierGroupName, CarrierName

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierandCarrierGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierandCarrierGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierandCarrierGroup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierandCarrierGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierandCarrierGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierandCarrierGroup] TO [power_user]
GO
