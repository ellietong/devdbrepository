SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryfrmPaymentVendorInvoice] (@VendorFirmID int, @MatterID int)
As
Set NoCount On

--declare @VendorFirmID int
--set @VendorFirmID = 10033


select di.DefenseInvoiceID, di.FirmID as VendorFirmID, MatterID, di.InvoiceNumber, di.Period, InvoiceTotal, InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount
from vtblDefenseInvoice di
left join tblDefenseInvoice div on div.DefenseInvoiceID = di.DefenseInvoiceID
left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
			 a on a.DefenseInvoiceID = di.DefenseInvoiceID
where di.firmid is not null and div.DatePaymentApproved is not null and di.FirmID = @VendorFirmID and MatterID = @MatterID
Group By di.DefenseInvoiceID, di.FirmID, di.InvoiceNumber, di.Period, InvoiceTotal, MatterID
Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0 

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [power_user]
GO
