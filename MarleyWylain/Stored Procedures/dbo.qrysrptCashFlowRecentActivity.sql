SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qrysrptCashFlowRecentActivity]
(@StartDate date)
As

SET NOCOUNT ON

--DECLARE @StartDate DATE
--SET @StartDate = '1/1/2009'


SELECT	Sum(Indemnity) as Indemnity, Sum(DefenseCounsel) as DefenseCounsel, 
Sum(NonBillable_DefenseCounsel) as NonBillable_DefenseCounsel, Sum(MiscFees) as MiscFees,
	Sum(Shortfall) as Shortfall, Sum(Bundled) as Bundled, Sum(InvestmentInterest) as InvestmentINterest, 
	Sum(UnpostedInsurer) as UnpostedInsurer, Sum(UnpostedSelf) as UnpostedSelf, 
	Sum(UnpostedCashReceipts) as UnpostedCashReceipts,
	Sum(Indemnity)+Sum(DefenseCOunsel)+Sum(NonBillable_DefenseCounsel)+Sum(MiscFees)+Sum(Shortfall)+Sum(Bundled)+Sum(InvestmentINterest)+Sum(UnpostedInsurer)+Sum(UnpostedSelf)+Sum(UnpostedCashReceipts) as TransferTotal,
	Sum(Total) as Total, MIN([date]) as mindate, MAX([date]) as maxdate
FROM	tblCashFlow CF
Left Join tblTransactionType TT on CF.TransactionTypeID=TT.TransactionTypeID
Where [Date] >= @StartDate

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCashFlowRecentActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCashFlowRecentActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCashFlowRecentActivity] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysrptCashFlowRecentActivity] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCashFlowRecentActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCashFlowRecentActivity] TO [power_user]
GO
