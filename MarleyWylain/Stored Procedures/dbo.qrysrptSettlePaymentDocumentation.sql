SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qrysrptSettlePaymentDocumentation]
(@ClaimantPersonalInfoID bigint)

As
Set NoCount On


--Declare @ClaimantPersonalInfoID bigint
--Set @ClaimantPersonalInfoID = 5185


SELECT sd.SettlementConfirmation, sd.ClaimantIDed, sd.DxMedicals, sd.DODCert, sd.EstatePapers, sd.ProductID, sd.EmployExpoHist, sd.compserv, ReleaseRqstDate,
ExecutedReleaseDate, ReleaseRcvd, MedicareFullyDocumented, MedicareReported, ReleaseToPC, cpi.ClaimantPersonalInfoID, cl.ClaimLawsuitID
FROM tblClaimantPersonalInfo cpi
LEFT JOIN tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
LEFT JOIN tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblSettlementDocumentation sd on spd.SetPymtID = sd.SetPymtID
WHERE cpi.ClaimantPersonalInfoID=@ClaimantPersonalInfoID


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSettlePaymentDocumentation] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysrptSettlePaymentDocumentation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptSettlePaymentDocumentation] TO [power_user]
GO
