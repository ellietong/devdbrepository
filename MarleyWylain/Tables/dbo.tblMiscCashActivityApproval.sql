CREATE TABLE [dbo].[tblMiscCashActivityApproval]
(
[MiscCashActivityApprovalID] [bigint] NOT NULL IDENTITY(1, 1),
[MiscCashActivityID] [bigint] NULL,
[IsApproved] [bit] NULL,
[ApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMiscCashActivityApproval] ADD CONSTRAINT [PK_tblMiscCashActivityApproval] PRIMARY KEY CLUSTERED  ([MiscCashActivityApprovalID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivityApproval] TO [base_user]
GRANT SELECT ON  [dbo].[tblMiscCashActivityApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivityApproval] TO [CheckApproval_User]
GRANT SELECT ON  [dbo].[tblMiscCashActivityApproval] TO [CheckApproval_User]
GRANT INSERT ON  [dbo].[tblMiscCashActivityApproval] TO [CheckApproval_User]
GRANT UPDATE ON  [dbo].[tblMiscCashActivityApproval] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivityApproval] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[tblMiscCashActivityApproval] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivityApproval] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblMiscCashActivityApproval] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivityApproval] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblMiscCashActivityApproval] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblMiscCashActivityApproval] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblMiscCashActivityApproval] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblMiscCashActivityApproval] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblMiscCashActivityApproval] TO [power_user]
GRANT SELECT ON  [dbo].[tblMiscCashActivityApproval] TO [power_user]
GRANT INSERT ON  [dbo].[tblMiscCashActivityApproval] TO [power_user]
GRANT DELETE ON  [dbo].[tblMiscCashActivityApproval] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMiscCashActivityApproval] TO [power_user]
GO
