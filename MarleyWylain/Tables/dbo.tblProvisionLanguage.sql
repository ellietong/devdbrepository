CREATE TABLE [dbo].[tblProvisionLanguage]
(
[ProvisionLanguageID] [int] NOT NULL IDENTITY(1, 1),
[ExactLanguageID] [int] NULL,
[ProvisionLabelID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionLanguage] ADD CONSTRAINT [PK_tblProvisionLanguage_1] PRIMARY KEY CLUSTERED  ([ProvisionLanguageID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblProvisionLanguage_tblExactLanguage] ON [dbo].[tblProvisionLanguage] ([ExactLanguageID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionLanguage] TO [Language_User]
GRANT INSERT ON  [dbo].[tblProvisionLanguage] TO [Language_User]
GRANT DELETE ON  [dbo].[tblProvisionLanguage] TO [Language_User]
GRANT UPDATE ON  [dbo].[tblProvisionLanguage] TO [Language_User]
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionLanguage] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblProvisionLanguage] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblProvisionLanguage] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblProvisionLanguage] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblProvisionLanguage] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblProvisionLanguage] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionLanguage] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvisionLanguage] TO [power_user]
GRANT DELETE ON  [dbo].[tblProvisionLanguage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvisionLanguage] TO [power_user]
GO
