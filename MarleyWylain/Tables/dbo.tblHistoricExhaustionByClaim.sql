CREATE TABLE [dbo].[tblHistoricExhaustionByClaim]
(
[PACEClaimID] [int] NULL,
[PACEPolicyID] [int] NULL,
[TotalIndemnity] [money] NULL,
[MonthlyBillID] [int] NULL,
[AllocationID] [int] NULL,
[PolicyDateID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblHistoricExhaustionByClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblHistoricExhaustionByClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblHistoricExhaustionByClaim] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblHistoricExhaustionByClaim] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblHistoricExhaustionByClaim] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblHistoricExhaustionByClaim] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblHistoricExhaustionByClaim] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblHistoricExhaustionByClaim] TO [power_user]
GRANT SELECT ON  [dbo].[tblHistoricExhaustionByClaim] TO [power_user]
GRANT INSERT ON  [dbo].[tblHistoricExhaustionByClaim] TO [power_user]
GRANT DELETE ON  [dbo].[tblHistoricExhaustionByClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[tblHistoricExhaustionByClaim] TO [power_user]
GO
