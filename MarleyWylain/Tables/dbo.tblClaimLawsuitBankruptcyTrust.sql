CREATE TABLE [dbo].[tblClaimLawsuitBankruptcyTrust]
(
[ClaimLawsuitBankruptcyTrustID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NOT NULL,
[BankruptcyTrustID] [int] NULL,
[ClaimLawsuitBankruptcyTrustNotes] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitBankruptcyTrust] ADD CONSTRAINT [PK_tblClaimLawsuitBankruptcyTrust] PRIMARY KEY CLUSTERED  ([ClaimLawsuitBankruptcyTrustID]) ON [PRIMARY]
GO
