CREATE TABLE [dbo].[tblOccurrence]
(
[OccurrenceID] [bigint] NOT NULL,
[AggregateApplies] [bit] NOT NULL,
[ExpectedPayDate] [datetime] NULL,
[InClaim] [bit] NOT NULL,
[OccurrenceName] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceAddress] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceStateID] [int] NULL,
[OccurrenceGroupID] [int] NULL,
[CorporateHistoryID] [int] NULL,
[OccurrenceTypeID] [int] NULL,
[ClaimID] [bigint] NULL,
[Segment] [int] NULL,
[TriggerStart1] [datetime] NULL,
[TriggerStart1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriggerEnd1] [datetime] NULL,
[TriggerEnd1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriggerStart2] [datetime] NULL,
[TriggerEnd2] [datetime] NULL,
[TriggerStart3] [datetime] NULL,
[TriggerEnd3] [datetime] NULL,
[TriggerStart4] [datetime] NULL,
[TriggerEnd4] [datetime] NULL,
[Cost1] [float] NULL,
[Cost1Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost2] [float] NULL,
[Cost2Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost3] [float] NULL,
[Cost3Explanation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cost4] [float] NULL,
[Cost5] [float] NULL,
[Cost6] [float] NULL,
[Cost7] [float] NULL,
[Cost8] [float] NULL,
[Cost9] [float] NULL,
[Cost10] [float] NULL,
[Cost11] [float] NULL,
[Cost12] [float] NULL,
[OccurrenceNote] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrence] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrence] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblOccurrence] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblOccurrence] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblOccurrence] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblOccurrence] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrence] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccurrence] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccurrence] TO [power_user]
GRANT DELETE ON  [dbo].[tblOccurrence] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccurrence] TO [power_user]
GO
