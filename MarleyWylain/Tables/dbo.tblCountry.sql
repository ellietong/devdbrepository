CREATE TABLE [dbo].[tblCountry]
(
[CountryID] [int] NOT NULL,
[Country] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCountry] TO [base_user]
GRANT SELECT ON  [dbo].[tblCountry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCountry] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCountry] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblCountry] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblCountry] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblCountry] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCountry] TO [power_user]
GRANT SELECT ON  [dbo].[tblCountry] TO [power_user]
GRANT INSERT ON  [dbo].[tblCountry] TO [power_user]
GRANT DELETE ON  [dbo].[tblCountry] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCountry] TO [power_user]
GO
