CREATE TABLE [dbo].[ztblPolicyDateIDTriggeredSig]
(
[PolicyID] [int] NULL,
[AllocationID] [int] NULL,
[OriginalOccurrenceID] [int] NULL,
[ColumnID] [int] NULL,
[BlockTriggered] [bit] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [base_user]
GRANT SELECT ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [power_user]
GRANT SELECT ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [power_user]
GRANT INSERT ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [power_user]
GRANT DELETE ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblPolicyDateIDTriggeredSig] TO [power_user]
GO
