CREATE TABLE [dbo].[tblClaim]
(
[ClaimID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[IsCurrentClaim] [bit] NULL,
[ClaimSequenceID] [int] NULL,
[FullyDocumented] [bit] NULL,
[CauseOfInjuryID] [int] NULL,
[MatterID] [int] NULL,
[ILODoctorID] [int] NULL,
[ILOProfusion] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILODate] [datetime] NULL,
[PFTDoctorID] [int] NULL,
[PFTDate] [datetime] NULL,
[PFTTypeID] [int] NULL,
[PFTFVC_Actual] [float] NULL,
[PFTFVC_Predicted] [float] NULL,
[PFTFEV1_Actual] [float] NULL,
[PFTFEV1_Predicted] [float] NULL,
[PFTFEV1AndFVC_Actual] [float] NULL,
[PFTFEV1AndFVC_Predicted] [float] NULL,
[PFTTLC_Actual] [float] NULL,
[PFTTLC_Predicted] [float] NULL,
[PFTDLCO_Actual] [float] NULL,
[PFTDLCO_Predicted] [float] NULL,
[PFTTV] [float] NULL,
[NavClaimKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimSequence] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPlaintiffDisease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDefenseDisease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavMatterID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPlaintiffDiseaseDate] [datetime] NULL,
[NavDefenseDiseaseDate] [datetime] NULL,
[NavPFTType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavILODoctor] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPFTDoctor] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceReference] [int] NULL,
[IsConsortiumClaimID] [int] NULL,
[IsConsortiumMedicareEligibleID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [PK_mBtblClaim] PRIMARY KEY CLUSTERED  ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaim_tblClaimantPersonalInfo] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaim_ClaimantPersonalInfoID_MatterID] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID], [MatterID]) INCLUDE ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_8264_8263_tblClaim] ON [dbo].[tblClaim] ([IsCurrentClaim], [MatterID]) INCLUDE ([ClaimantPersonalInfoID], [ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaim_MatterID] ON [dbo].[tblClaim] ([MatterID]) INCLUDE ([ClaimantPersonalInfoID], [ClaimID], [ClaimSequenceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim] WITH NOCHECK ADD CONSTRAINT [FK_tblClaim_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID])
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [FK_tblClaim_tblMatter] FOREIGN KEY ([MatterID]) REFERENCES [dbo].[tblMatter] ([MatterID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaim] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaim] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaim] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaim] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaim] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaim] TO [Ligado_User]
GRANT ALTER ON  [dbo].[tblClaim] TO [monahanc]
GRANT VIEW DEFINITION ON  [dbo].[tblClaim] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaim] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaim] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaim] TO [power_user]
GO
