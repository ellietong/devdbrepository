CREATE TABLE [dbo].[tblAllocationSortOrder]
(
[SortOrderAllocationID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NOT NULL,
[SortOrderID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationSortOrder] ADD CONSTRAINT [PK_tblAllocationSortOrder] PRIMARY KEY CLUSTERED  ([SortOrderAllocationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationSortOrder] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationSortOrder] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationSortOrder] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationSortOrder] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationSortOrder] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationSortOrder] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationSortOrder] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationSortOrder] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblAllocationSortOrder] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblAllocationSortOrder] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblAllocationSortOrder] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblAllocationSortOrder] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationSortOrder] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationSortOrder] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationSortOrder] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationSortOrder] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationSortOrder] TO [power_user]
GO
