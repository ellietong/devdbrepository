CREATE TABLE [dbo].[tblAllocationResultsVertical]
(
[ARVerticalID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[OccurrenceID] [bigint] NULL,
[ColumnID] [int] NULL,
[uncollapsedVertical] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsVertical] ADD CONSTRAINT [PK_tblAllocationResultsVertical] PRIMARY KEY NONCLUSTERED  ([ARVerticalID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblAllocationResultsVertical_tblOccurrence] ON [dbo].[tblAllocationResultsVertical] ([AllocationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsVertical] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationResultsVertical] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationResultsVertical] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationResultsVertical] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationResultsVertical] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsVertical] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsVertical] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsVertical] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblAllocationResultsVertical] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblAllocationResultsVertical] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblAllocationResultsVertical] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblAllocationResultsVertical] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsVertical] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsVertical] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationResultsVertical] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationResultsVertical] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationResultsVertical] TO [power_user]
GO
