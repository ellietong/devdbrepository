CREATE TABLE [dbo].[tblSettlementGroup_old]
(
[SettlementGroupID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementGroup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentTermsID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementGroup_old] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementGroup_old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementGroup_old] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSettlementGroup_old] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSettlementGroup_old] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSettlementGroup_old] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSettlementGroup_old] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementGroup_old] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementGroup_old] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementGroup_old] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementGroup_old] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementGroup_old] TO [power_user]
GO
