CREATE TABLE [dbo].[tblClaimLawsuitStatusTracking]
(
[ClaimLawsuitStatusTrackingID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[ClaimLawsuitStatusID] [int] NULL,
[StatusNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimaryStatus] [bit] NULL,
[NavStatus] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessedDate] [datetime] NULL,
[StatusDate] [datetime] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [PK_tblClaimLawsuitStatusTracking] PRIMARY KEY CLUSTERED  ([ClaimLawsuitStatusTrackingID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimLawsuitStatusTracking_tblClaimLawsuit] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuitStatusTracking_ClaimLawsuitID_ClaimLawsuitStatusID] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitID]) INCLUDE ([ClaimLawsuitStatusID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimLawsuitStatusTracking_tblClaimLawsuitStatus] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitStatusID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_8256_8255_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitStatusID]) INCLUDE ([ClaimLawsuitID], [ProcessedDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_8268_8267_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitStatusID], [IsPrimaryStatus], [ProcessedDate]) INCLUDE ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_IsPrimaryStatus_ClaimLawsuitStatusID_ClaimLawsuitID] ON [dbo].[tblClaimLawsuitStatusTracking] ([IsPrimaryStatus], [ClaimLawsuitStatusID]) INCLUDE ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_8254_8253_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([IsPrimaryStatus], [ClaimLawsuitStatusID]) INCLUDE ([ClaimLawsuitID], [ProcessedDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_8266_8265_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([IsPrimaryStatus], [ClaimLawsuitStatusID], [ProcessedDate]) INCLUDE ([ClaimLawsuitID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [FK_tblClaimLawsuitStatusTracking_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID])
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [FK_tblClaimLawsuitStatusTracking_tblClaimLawsuitStatus] FOREIGN KEY ([ClaimLawsuitStatusID]) REFERENCES [dbo].[tblClaimLawsuitStatus] ([ClaimLawsuitStatusID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Ligado_User]
GRANT ALTER ON  [dbo].[tblClaimLawsuitStatusTracking] TO [monahanc]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GO
