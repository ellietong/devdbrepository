CREATE TABLE [dbo].[tblVendorFirm]
(
[VendorFirmID] [int] NOT NULL IDENTITY(10000, 1),
[VendorFirm] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[ZipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldPrimaryKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblVendorFirm] ADD CONSTRAINT [PK_tblVendorFirm] PRIMARY KEY CLUSTERED  ([VendorFirmID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblVendorFirm] ADD CONSTRAINT [FK_tblVendorFirm_tblVendorFirm] FOREIGN KEY ([VendorFirmID]) REFERENCES [dbo].[tblVendorFirm] ([VendorFirmID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblVendorFirm] TO [base_user]
GRANT SELECT ON  [dbo].[tblVendorFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblVendorFirm] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblVendorFirm] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblVendorFirm] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblVendorFirm] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblVendorFirm] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblVendorFirm] TO [power_user]
GRANT SELECT ON  [dbo].[tblVendorFirm] TO [power_user]
GRANT INSERT ON  [dbo].[tblVendorFirm] TO [power_user]
GRANT DELETE ON  [dbo].[tblVendorFirm] TO [power_user]
GRANT UPDATE ON  [dbo].[tblVendorFirm] TO [power_user]
GO
