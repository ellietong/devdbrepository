CREATE TABLE [dbo].[tblExhaustionType]
(
[ExhaustionTypeID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustionType] ADD CONSTRAINT [PK_tblExhaustionType] PRIMARY KEY CLUSTERED  ([ExhaustionTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustionType] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustionType] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustionType] TO [Exhaustion_User]
GRANT INSERT ON  [dbo].[tblExhaustionType] TO [Exhaustion_User]
GRANT DELETE ON  [dbo].[tblExhaustionType] TO [Exhaustion_User]
GRANT UPDATE ON  [dbo].[tblExhaustionType] TO [Exhaustion_User]
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustionType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblExhaustionType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblExhaustionType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblExhaustionType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblExhaustionType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustionType] TO [power_user]
GRANT SELECT ON  [dbo].[tblExhaustionType] TO [power_user]
GRANT INSERT ON  [dbo].[tblExhaustionType] TO [power_user]
GRANT DELETE ON  [dbo].[tblExhaustionType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExhaustionType] TO [power_user]
GO
