CREATE TABLE [dbo].[tblClaimantRelease]
(
[ClaimantReleaseID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimSettlementID] [bigint] NULL,
[ReleaseSignedDate] [datetime] NULL,
[ReleaseSentDate] [datetime] NULL,
[ReleaseReceivedDate] [datetime] NULL,
[ReleaseTypeID] [int] NULL,
[NavClaimResolutionKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavReleaseType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantRelease] ADD CONSTRAINT [PK__tblClaimantRelea__4AD81681] PRIMARY KEY CLUSTERED  ([ClaimantReleaseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimantRelease_tblClaimantSettlement] ON [dbo].[tblClaimantRelease] ([ClaimSettlementID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantRelease] WITH NOCHECK ADD CONSTRAINT [FK_tblClaimantRelease_tblClaimantSettlement] FOREIGN KEY ([ClaimSettlementID]) REFERENCES [dbo].[tblClaimSettlement] ([ClaimSettlementID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantRelease] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantRelease] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantRelease] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantRelease] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantRelease] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantRelease] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimantRelease] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimantRelease] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimantRelease] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimantRelease] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantRelease] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantRelease] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantRelease] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantRelease] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantRelease] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1000000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantReleaseID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantReleaseID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantReleaseID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantReleaseID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantReleaseID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimSettlementID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimSettlementID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimSettlementID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimSettlementID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimSettlementID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseReceivedDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseReceivedDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseReceivedDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseReceivedDate'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseReceivedDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseSignedDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseSignedDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseSignedDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseSignedDate'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseSignedDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseTypeID'
GO
DECLARE @xp int
SELECT @xp=2535
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseTypeID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseTypeID'
GO
