CREATE TABLE [dbo].[tblTriggerLabel]
(
[TriggerID] [int] NOT NULL IDENTITY(1, 1),
[TriggerDescription] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTriggerLabel] ADD CONSTRAINT [PK_tblTriggerLabel] PRIMARY KEY NONCLUSTERED  ([TriggerID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTriggerLabel] TO [base_user]
GRANT SELECT ON  [dbo].[tblTriggerLabel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTriggerLabel] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblTriggerLabel] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblTriggerLabel] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblTriggerLabel] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblTriggerLabel] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblTriggerLabel] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[tblTriggerLabel] TO [Occurrence_User]
GRANT DELETE ON  [dbo].[tblTriggerLabel] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[tblTriggerLabel] TO [Occurrence_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTriggerLabel] TO [power_user]
GRANT SELECT ON  [dbo].[tblTriggerLabel] TO [power_user]
GRANT INSERT ON  [dbo].[tblTriggerLabel] TO [power_user]
GRANT DELETE ON  [dbo].[tblTriggerLabel] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTriggerLabel] TO [power_user]
GO
