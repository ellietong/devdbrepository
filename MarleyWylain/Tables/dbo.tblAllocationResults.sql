CREATE TABLE [dbo].[tblAllocationResults]
(
[AllocationResultID] [bigint] NOT NULL,
[AllocationID] [int] NULL,
[PolicyID] [int] NULL,
[PolicyDateID] [bigint] NULL,
[PStartSplit] [datetime] NULL,
[PEndSplit] [datetime] NULL,
[AnnualPeriodSplit] [int] NULL,
[Allocation] [float] NULL,
[AllocationIndemnity] [float] NULL,
[AllocationDefense] [float] NULL,
[AllocationDefenseOutsideLimits] [float] NULL,
[OccurrenceID] [bigint] NULL,
[TriggerStartDateUsed] [datetime] NULL,
[TriggerEndDateUsed] [datetime] NULL,
[SIRAllocated] [float] NULL,
[SIRAllocatedIndemnity] [float] NULL,
[SIRAllocatedDefense] [float] NULL,
[SIRDiscount] [float] NULL,
[DateTime] [datetime] NULL,
[PV] [decimal] (38, 15) NULL,
[DateToPresentValue] [datetime] NULL,
[PVRate] [decimal] (18, 15) NULL,
[YieldCurveID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResults] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResults] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResults] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblAllocationResults] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblAllocationResults] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblAllocationResults] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblAllocationResults] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResults] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationResults] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationResults] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationResults] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationResults] TO [power_user]
GO
