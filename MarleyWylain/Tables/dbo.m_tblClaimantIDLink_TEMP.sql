CREATE TABLE [dbo].[m_tblClaimantIDLink_TEMP]
(
[OldClaimantPersonalInfoID] [bigint] NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLegacyID] [bigint] NULL,
[NavRefID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblClaimantIDLink_TEMP] TO [power_user]
GO
