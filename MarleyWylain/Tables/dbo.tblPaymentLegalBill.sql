CREATE TABLE [dbo].[tblPaymentLegalBill]
(
[LegalBillID] [bigint] NOT NULL IDENTITY(1, 1),
[CheckID] [bigint] NULL,
[InvoiceID] [bigint] NULL,
[PaymentAmount] [money] NULL,
[IsFullyPaid] [bit] NULL,
[PaymentInvoiceNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPaymentLegalBill] ADD CONSTRAINT [PK_tblPaymentLegalBill] PRIMARY KEY CLUSTERED  ([LegalBillID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPaymentLegalBill] WITH NOCHECK ADD CONSTRAINT [FK_tblPaymentLegalBill_tblCheck] FOREIGN KEY ([CheckID]) REFERENCES [dbo].[tblCheck] ([CheckID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [base_user]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [CheckApproval_User]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [CheckWriter_User]
GRANT INSERT ON  [dbo].[tblPaymentLegalBill] TO [CheckWriter_User]
GRANT UPDATE ON  [dbo].[tblPaymentLegalBill] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPaymentLegalBill] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPaymentLegalBill] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPaymentLegalBill] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [power_user]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [power_user]
GRANT INSERT ON  [dbo].[tblPaymentLegalBill] TO [power_user]
GRANT DELETE ON  [dbo].[tblPaymentLegalBill] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPaymentLegalBill] TO [power_user]
GO
