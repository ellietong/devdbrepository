CREATE TABLE [dbo].[m_tblAmazonKeySizeImport]
(
[Key] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Size] [bigint] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblAmazonKeySizeImport] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblAmazonKeySizeImport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblAmazonKeySizeImport] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblAmazonKeySizeImport] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblAmazonKeySizeImport] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblAmazonKeySizeImport] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblAmazonKeySizeImport] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblAmazonKeySizeImport] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblAmazonKeySizeImport] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblAmazonKeySizeImport] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblAmazonKeySizeImport] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblAmazonKeySizeImport] TO [power_user]
GO
