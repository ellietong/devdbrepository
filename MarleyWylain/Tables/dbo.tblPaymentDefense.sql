CREATE TABLE [dbo].[tblPaymentDefense]
(
[PaymentDefenseID] [bigint] NOT NULL IDENTITY(1, 1),
[CheckID] [bigint] NOT NULL,
[DefenseInvoiceID] [bigint] NULL,
[PaymentAmount] [money] NULL,
[IsFullyPaid] [bit] NULL,
[PaymentInvoiceNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPaymentDefense] ADD CONSTRAINT [FK_tblPaymentDefense_tblCheck] FOREIGN KEY ([CheckID]) REFERENCES [dbo].[tblCheck] ([CheckID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentDefense] TO [base_user]
GRANT SELECT ON  [dbo].[tblPaymentDefense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentDefense] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPaymentDefense] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPaymentDefense] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPaymentDefense] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPaymentDefense] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentDefense] TO [power_user]
GRANT SELECT ON  [dbo].[tblPaymentDefense] TO [power_user]
GRANT INSERT ON  [dbo].[tblPaymentDefense] TO [power_user]
GRANT DELETE ON  [dbo].[tblPaymentDefense] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPaymentDefense] TO [power_user]
GO
