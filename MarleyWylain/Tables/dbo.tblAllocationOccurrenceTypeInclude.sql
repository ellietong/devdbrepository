CREATE TABLE [dbo].[tblAllocationOccurrenceTypeInclude]
(
[OccurrenceTypeIncludeID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[OccurrenceTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationOccurrenceTypeInclude] ADD CONSTRAINT [PK_tblAllocationOccurrenceTypeExclude] PRIMARY KEY NONCLUSTERED  ([OccurrenceTypeIncludeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblAllocationOccurrenceTypeInclude_tblAllocation] ON [dbo].[tblAllocationOccurrenceTypeInclude] ([AllocationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [power_user]
GO
