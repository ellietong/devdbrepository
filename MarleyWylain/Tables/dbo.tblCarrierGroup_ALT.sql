CREATE TABLE [dbo].[tblCarrierGroup_ALT]
(
[CarrierGroupID] [int] NOT NULL IDENTITY(1, 1),
[CarrierGroupName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierGroupNote] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCarrierGroup_ALT] ADD CONSTRAINT [PK_tblCarrierGroup] PRIMARY KEY CLUSTERED  ([CarrierGroupID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCarrierGroup_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblCarrierGroup_ALT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCarrierGroup_ALT] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCarrierGroup_ALT] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblCarrierGroup_ALT] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblCarrierGroup_ALT] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblCarrierGroup_ALT] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCarrierGroup_ALT] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblCarrierGroup_ALT] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblCarrierGroup_ALT] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblCarrierGroup_ALT] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCarrierGroup_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblCarrierGroup_ALT] TO [power_user]
GRANT INSERT ON  [dbo].[tblCarrierGroup_ALT] TO [power_user]
GRANT DELETE ON  [dbo].[tblCarrierGroup_ALT] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCarrierGroup_ALT] TO [power_user]
GO
