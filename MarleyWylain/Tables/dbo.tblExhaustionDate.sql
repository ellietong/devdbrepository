CREATE TABLE [dbo].[tblExhaustionDate]
(
[ExhaustionDateID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionTypeID] [int] NOT NULL,
[ExhaustionDate] [datetime] NOT NULL,
[AllocationID] [int] NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustionDate] ADD CONSTRAINT [PK_tblExhaustionDate] PRIMARY KEY NONCLUSTERED  ([ExhaustionDateID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblExhaustionDate_tblExhaustionType] ON [dbo].[tblExhaustionDate] ([ExhaustionTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustionDate] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustionDate] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustionDate] TO [Exhaustion_User]
GRANT INSERT ON  [dbo].[tblExhaustionDate] TO [Exhaustion_User]
GRANT DELETE ON  [dbo].[tblExhaustionDate] TO [Exhaustion_User]
GRANT UPDATE ON  [dbo].[tblExhaustionDate] TO [Exhaustion_User]
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustionDate] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblExhaustionDate] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblExhaustionDate] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblExhaustionDate] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblExhaustionDate] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustionDate] TO [power_user]
GRANT SELECT ON  [dbo].[tblExhaustionDate] TO [power_user]
GRANT INSERT ON  [dbo].[tblExhaustionDate] TO [power_user]
GRANT DELETE ON  [dbo].[tblExhaustionDate] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExhaustionDate] TO [power_user]
GO
