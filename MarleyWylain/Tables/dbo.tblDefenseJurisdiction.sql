CREATE TABLE [dbo].[tblDefenseJurisdiction]
(
[DefenseJurisdictionID] [int] NOT NULL IDENTITY(1, 1),
[JurisdictionID] [int] NOT NULL,
[DefenseCounselAddressID] [int] NULL,
[CourtTypeID] [int] NOT NULL,
[MatterID] [int] NULL,
[StateID] [int] NOT NULL,
[IsActive] [bit] NOT NULL,
[PersonID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseJurisdiction] ADD CONSTRAINT [tblDefenseJurisdiction_OneIDLookupPerRow_OthersNull] CHECK (([DefenseCounselAddressID] IS NULL AND [PersonID] IS NOT NULL OR [DefenseCounselAddressID] IS NOT NULL AND [PersonID] IS NULL))
GO
ALTER TABLE [dbo].[tblDefenseJurisdiction] ADD CONSTRAINT [PK_tblDefenseJurisdiction] PRIMARY KEY CLUSTERED  ([DefenseJurisdictionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_tblDefenseJurisdiction_StateID__JurisdictionID_Includes] ON [dbo].[tblDefenseJurisdiction] ([DefenseCounselAddressID], [JurisdictionID], [StateID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseJurisdiction] ADD CONSTRAINT [FK_tblDefenseJurisdiction_tblMatter] FOREIGN KEY ([MatterID]) REFERENCES [dbo].[tblMatter] ([MatterID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseJurisdiction] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseJurisdiction] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblDefenseJurisdiction] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblDefenseJurisdiction] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblDefenseJurisdiction] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GO
