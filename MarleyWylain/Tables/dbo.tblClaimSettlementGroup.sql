CREATE TABLE [dbo].[tblClaimSettlementGroup]
(
[SettlementGroupID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementGroup] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentTermsID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimSettlementGroup] ADD CONSTRAINT [PK_mBtblSettlementGroup_1] PRIMARY KEY CLUSTERED  ([SettlementGroupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimSettlementGroup] ADD CONSTRAINT [FK_tblClaimSettlementGroup_tblPaymentTerms] FOREIGN KEY ([PaymentTermsID]) REFERENCES [dbo].[tblPaymentTerms] ([PaymentTermsID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementGroup] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimSettlementGroup] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimSettlementGroup] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementGroup] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimSettlementGroup] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimSettlementGroup] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimSettlementGroup] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimSettlementGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementGroup] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementGroup] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimSettlementGroup] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimSettlementGroup] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimSettlementGroup] TO [power_user]
GO
