CREATE TABLE [dbo].[tblOccurrenceGroup]
(
[OccurrenceGroupID] [int] NOT NULL IDENTITY(1, 1),
[OccurrenceGroup] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccGrpTriggerStart] [datetime] NULL,
[OccGrpDiscount] [real] NULL,
[SiteID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccurrenceGroup] ADD CONSTRAINT [PK_tblOccurrenceGroup] PRIMARY KEY CLUSTERED  ([OccurrenceGroupID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrenceGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrenceGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrenceGroup] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblOccurrenceGroup] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblOccurrenceGroup] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblOccurrenceGroup] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblOccurrenceGroup] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblOccurrenceGroup] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[tblOccurrenceGroup] TO [Occurrence_User]
GRANT DELETE ON  [dbo].[tblOccurrenceGroup] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[tblOccurrenceGroup] TO [Occurrence_User]
GRANT VIEW DEFINITION ON  [dbo].[tblOccurrenceGroup] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccurrenceGroup] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccurrenceGroup] TO [power_user]
GRANT DELETE ON  [dbo].[tblOccurrenceGroup] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccurrenceGroup] TO [power_user]
GO
