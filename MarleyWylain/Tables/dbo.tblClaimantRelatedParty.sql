CREATE TABLE [dbo].[tblClaimantRelatedParty]
(
[ClaimantRelatedPartyID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullName] AS (((coalesce([LastName],'')+', ')+coalesce([FirstName],''))+coalesce(' '+[MiddleName],'')),
[RelationshipID] [int] NULL,
[LegalCapacityID] [int] NULL,
[RPAddress1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPAddress2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[RPZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPMailZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPPhone] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPExtension] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPSocialSecurityNumber] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPTIN] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavRelationship] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLegalCapacity] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUserName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPBirthDate] [datetime] NULL,
[LossOfConsortiumStatusID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantRelatedParty] ADD CONSTRAINT [PK__tblClaimantRelat__48EFCE0F] PRIMARY KEY CLUSTERED  ([ClaimantRelatedPartyID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimantRelatedParty_tblClaimantPersonalInfo] ON [dbo].[tblClaimantRelatedParty] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantRelatedParty] WITH NOCHECK ADD CONSTRAINT [FK_tblClaimantRelatedParty_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantRelatedParty] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantRelatedParty] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantRelatedParty] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantRelatedParty] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantRelatedParty] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantRelatedParty] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantRelatedParty] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimantRelatedParty] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimantRelatedParty] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimantRelatedParty] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimantRelatedParty] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantRelatedParty] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantRelatedParty] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantRelatedParty] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantRelatedParty] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantRelatedParty] TO [power_user]
GO
CREATE FULLTEXT INDEX ON [dbo].[tblClaimantRelatedParty] KEY INDEX [PK__tblClaimantRelat__48EFCE0F] ON [ClaimantName]
GO
ALTER FULLTEXT INDEX ON [dbo].[tblClaimantRelatedParty] ADD ([FirstName] LANGUAGE 1033)
GO
ALTER FULLTEXT INDEX ON [dbo].[tblClaimantRelatedParty] ADD ([MiddleName] LANGUAGE 1033)
GO
ALTER FULLTEXT INDEX ON [dbo].[tblClaimantRelatedParty] ADD ([LastName] LANGUAGE 1033)
GO
