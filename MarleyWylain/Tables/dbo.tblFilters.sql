CREATE TABLE [dbo].[tblFilters]
(
[FilterID] [int] NOT NULL IDENTITY(1, 1),
[Filter] [char] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFilters] ADD CONSTRAINT [PK_TblFilters] PRIMARY KEY CLUSTERED  ([FilterID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblFilters] TO [base_user]
GRANT SELECT ON  [dbo].[tblFilters] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblFilters] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblFilters] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblFilters] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblFilters] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblFilters] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblFilters] TO [power_user]
GRANT SELECT ON  [dbo].[tblFilters] TO [power_user]
GRANT INSERT ON  [dbo].[tblFilters] TO [power_user]
GRANT DELETE ON  [dbo].[tblFilters] TO [power_user]
GRANT UPDATE ON  [dbo].[tblFilters] TO [power_user]
GRANT SELECT ON  [dbo].[tblFilters] TO [Reports_User]
GRANT INSERT ON  [dbo].[tblFilters] TO [Reports_User]
GRANT DELETE ON  [dbo].[tblFilters] TO [Reports_User]
GRANT UPDATE ON  [dbo].[tblFilters] TO [Reports_User]
GO
