CREATE TABLE [dbo].[tblDefenseInvoice]
(
[DefenseInvoiceID] [bigint] NOT NULL IDENTITY(1, 1),
[DocumentID] [bigint] NULL,
[StatusID] [int] NULL,
[FirmID] [int] NULL,
[DefenseCounselAddressID] [bigint] NULL,
[VendorFirmID] [int] NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmitDate] [datetime] NULL,
[DatePayable] [datetime] NULL,
[InvoiceLink] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateApproved] [datetime] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[InvoiceNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LegalBillID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LBInvoiceNumber] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalInvoiceNumber] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DanaApprovalDate] [datetime] NULL,
[LBApprovalDate] [datetime] NULL,
[SubmittedForPayment] [datetime] NULL,
[FundsRequestDate] [datetime] NULL,
[OriginalDanaLegalBillInvoiceID] [bigint] NULL,
[OriginalDanaDefenseInvoiceID] [bigint] NULL,
[PrivateComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedTotal] [money] NULL,
[DatePaymentApproved] [datetime] NULL,
[PaymentApprovalNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsApproved] [bit] NOT NULL CONSTRAINT [DF_tblDefenseInvoice_IsApproved] DEFAULT ((0)),
[IsRecommended] [bit] NOT NULL CONSTRAINT [DF_tblDefenseInvoice_IsRecommended] DEFAULT ((0)),
[RecommendedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateRecommended] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseInvoice] ADD CONSTRAINT [PK_tblDefenseInvoice_New] PRIMARY KEY CLUSTERED  ([DefenseInvoiceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseInvoice] WITH NOCHECK ADD CONSTRAINT [FK_tblDefenseInvoice_tblDocuments] FOREIGN KEY ([DocumentID]) REFERENCES [dbo].[tblDocuments] ([DocumentID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDefenseInvoice] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblDefenseInvoice] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblDefenseInvoice] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblDefenseInvoice] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoice] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseInvoice] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefenseInvoice] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseInvoice] TO [power_user]
GO
