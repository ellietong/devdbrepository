CREATE TABLE [dbo].[tblReports]
(
[ReportID] [int] NOT NULL IDENTITY(1, 1),
[ReportName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportDescription] [nvarchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IPValues] [nvarchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblReports] ADD CONSTRAINT [PK_TblReports] PRIMARY KEY CLUSTERED  ([ReportID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblReports] TO [base_user]
GRANT SELECT ON  [dbo].[tblReports] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblReports] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblReports] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblReports] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblReports] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblReports] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblReports] TO [power_user]
GRANT REFERENCES ON  [dbo].[tblReports] TO [power_user]
GRANT SELECT ON  [dbo].[tblReports] TO [power_user]
GRANT INSERT ON  [dbo].[tblReports] TO [power_user]
GRANT DELETE ON  [dbo].[tblReports] TO [power_user]
GRANT UPDATE ON  [dbo].[tblReports] TO [power_user]
GRANT SELECT ON  [dbo].[tblReports] TO [Reports_User]
GRANT INSERT ON  [dbo].[tblReports] TO [Reports_User]
GRANT DELETE ON  [dbo].[tblReports] TO [Reports_User]
GRANT UPDATE ON  [dbo].[tblReports] TO [Reports_User]
GO
