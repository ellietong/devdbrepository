CREATE TABLE [dbo].[m_tblBundledDefendantMember]
(
[Column 0] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 1] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 2] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 3] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 4] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 5] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 6] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 7] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 8] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 9] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 10] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 11] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 12] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblBundledDefendantMember] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblBundledDefendantMember] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblBundledDefendantMember] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblBundledDefendantMember] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblBundledDefendantMember] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblBundledDefendantMember] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblBundledDefendantMember] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblBundledDefendantMember] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblBundledDefendantMember] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblBundledDefendantMember] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblBundledDefendantMember] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblBundledDefendantMember] TO [power_user]
GO
