CREATE TABLE [dbo].[tblAllocationResultsHorizontal]
(
[ARhorizontalID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[OccurrenceID] [bigint] NULL,
[uncollapsedPrior] [money] NULL,
[uncollapsedPost] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsHorizontal] ADD CONSTRAINT [PK_tblAllocationResultsHorizontal] PRIMARY KEY NONCLUSTERED  ([ARhorizontalID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblAllocationResultsHorizontal_tblAllocation] ON [dbo].[tblAllocationResultsHorizontal] ([AllocationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsHorizontal] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationResultsHorizontal] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationResultsHorizontal] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationResultsHorizontal] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsHorizontal] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsHorizontal] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblAllocationResultsHorizontal] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblAllocationResultsHorizontal] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblAllocationResultsHorizontal] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsHorizontal] TO [power_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationResultsHorizontal] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationResultsHorizontal] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationResultsHorizontal] TO [power_user]
GO
