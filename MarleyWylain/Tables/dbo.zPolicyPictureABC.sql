CREATE TABLE [dbo].[zPolicyPictureABC]
(
[ID] [int] NOT NULL,
[policy_no] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Carrier] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Incept] [datetime] NULL,
[term] [datetime] NULL,
[attach] [money] NULL,
[perocclim] [money] NULL,
[bquotashr] [int] NULL,
[quotashrlim] [money] NULL,
[code] [int] NULL,
[excl_alloc] [int] NULL,
[Consume] [int] NULL,
[TotCost] [int] NULL,
[PHolder] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Layer] [int] NULL,
[Agglim] [money] NULL,
[Separation] [int] NULL,
[Imgfile] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Totcost2] [money] NULL,
[Totcost3] [money] NULL,
[Totcost4] [money] NULL,
[Totcost5] [money] NULL,
[Costinadd] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Totcost6] [money] NULL,
[Totcost7] [money] NULL,
[Totcost8] [money] NULL,
[Totcost9] [money] NULL,
[Totcost10] [money] NULL,
[Totcost11] [money] NULL,
[Totcost12] [money] NULL,
[Totcost13] [money] NULL,
[Totcost14] [money] NULL,
[Totcost15] [money] NULL,
[Totcost16] [money] NULL,
[Totcost17] [money] NULL,
[Totcost18] [money] NULL,
[Totcost19] [money] NULL,
[Totcost20] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[zPolicyPictureABC] TO [base_user]
GRANT SELECT ON  [dbo].[zPolicyPictureABC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zPolicyPictureABC] TO [Ligado_User]
GRANT SELECT ON  [dbo].[zPolicyPictureABC] TO [Ligado_User]
GRANT INSERT ON  [dbo].[zPolicyPictureABC] TO [Ligado_User]
GRANT DELETE ON  [dbo].[zPolicyPictureABC] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[zPolicyPictureABC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[zPolicyPictureABC] TO [power_user]
GRANT SELECT ON  [dbo].[zPolicyPictureABC] TO [power_user]
GRANT INSERT ON  [dbo].[zPolicyPictureABC] TO [power_user]
GRANT DELETE ON  [dbo].[zPolicyPictureABC] TO [power_user]
GRANT UPDATE ON  [dbo].[zPolicyPictureABC] TO [power_user]
GO
