CREATE TABLE [dbo].[tblIsConsortiumMedicareEligible]
(
[IsConsortiumMedicareEligibleID] [int] NOT NULL IDENTITY(1, 1),
[IsConsortiumMedicareEligible] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblIsConsortiumMedicareEligible] TO [base_user]
GRANT SELECT ON  [dbo].[tblIsConsortiumMedicareEligible] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblIsConsortiumMedicareEligible] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblIsConsortiumMedicareEligible] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblIsConsortiumMedicareEligible] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblIsConsortiumMedicareEligible] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblIsConsortiumMedicareEligible] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblIsConsortiumMedicareEligible] TO [power_user]
GRANT SELECT ON  [dbo].[tblIsConsortiumMedicareEligible] TO [power_user]
GRANT INSERT ON  [dbo].[tblIsConsortiumMedicareEligible] TO [power_user]
GRANT DELETE ON  [dbo].[tblIsConsortiumMedicareEligible] TO [power_user]
GRANT UPDATE ON  [dbo].[tblIsConsortiumMedicareEligible] TO [power_user]
GO
