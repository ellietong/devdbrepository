CREATE TABLE [dbo].[m_tblClaimantPersonalInfo_Lookup]
(
[ClaimantPersonalInfoID] [int] NULL,
[INJ_PRTY_KEY] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_ID] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GO
