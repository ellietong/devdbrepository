CREATE TABLE [dbo].[tblExhaustion]
(
[ExhaustionID] [int] NOT NULL,
[ExhaustionDateID] [int] NOT NULL,
[OccurrenceID] [bigint] NULL,
[PolicyDateID] [int] NOT NULL,
[IndemnityAllocation] [float] NOT NULL,
[DefenseAllocation] [float] NOT NULL,
[DefenseOutsideLimitsAllocation] [float] NOT NULL,
[SIRIndemnityAllocation] [float] NOT NULL,
[SIRDefenseAllocation] [float] NOT NULL,
[LedgerID] [int] NULL,
[OutsideConsumption] [float] NOT NULL,
[ConsumptionType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustion] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustion] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblExhaustion] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblExhaustion] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblExhaustion] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblExhaustion] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustion] TO [power_user]
GRANT SELECT ON  [dbo].[tblExhaustion] TO [power_user]
GRANT INSERT ON  [dbo].[tblExhaustion] TO [power_user]
GRANT DELETE ON  [dbo].[tblExhaustion] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExhaustion] TO [power_user]
GO
