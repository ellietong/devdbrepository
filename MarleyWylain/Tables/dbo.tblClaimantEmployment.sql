CREATE TABLE [dbo].[tblClaimantEmployment]
(
[ClaimantEmploymentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[OccupationID] [int] NULL,
[Jobsite] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavEmpStartDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavEmpEndDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavEarliestExposureDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLatestExposureDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimary] [bit] NULL,
[IsSecondaryExposure] [bit] NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavOccupation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmpStartDate] [datetime] NULL,
[EmpEndDate] [datetime] NULL,
[EarliestExposureDate] [datetime] NULL,
[LatestExposureDate] [datetime] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteStateID] [int] NULL,
[Old_OccupationID] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Crook
-- Create date: 11/15/13
-- Description:	Trigger to select a new primary if it gets deleted
-- =============================================
CREATE TRIGGER [dbo].[TRIG_DELETE_PrimaryEmployment]
   ON  [dbo].[tblClaimantEmployment]
   AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	--Temp table to hold our criteria for selecting the next primary record
	DECLARE @EmploymentRecords TABLE(ClaimantEmploymentID bigint, HasNotes bit, HasProducts bit, ExposureLength int)
	
	IF(SELECT COUNT(ClaimantEmploymentID) FROM tblClaimantEmployment ce WHERE IsPrimary = 1 AND ClaimantPersonalInfoID IN (SELECT ClaimantPersonalInfoID FROM deleted)) = 0
	BEGIN

		INSERT INTO @EmploymentRecords
		SELECT ce.ClaimantEmploymentID, 
		CASE WHEN LEN(ce.Comments) > 0 THEN 1 ELSE 0 END,
		CASE WHEN ProductCount > 0 THEN 1 ELSE 0 END,
		DATEDIFF(dd, ce.EmpStartDate, ce.EmpEndDate)
		FROM tblClaimantEmployment ce
		LEFT JOIN (SELECT ClaimantEmploymentID, COUNT(ClaimantProductID) AS ProductCount FROM tblClaimantProduct GROUP BY ClaimantEmploymentID) prod ON ce.ClaimantEmploymentID = prod.ClaimantEmploymentID
		WHERE ClaimantPersonalInfoID IN (SELECT ClaimantPersonalInfoID FROM deleted)
	
		UPDATE tblClaimantEmployment
		SET IsPrimary = 1
		WHERE ClaimantEmploymentID = (
			SELECT TOP 1 ClaimantEmploymentID
			FROM @EmploymentRecords
			ORDER BY HasNotes DESC, HasProducts DESC, ExposureLength DESC)
	END
END

GO
DISABLE TRIGGER [dbo].[TRIG_DELETE_PrimaryEmployment] ON [dbo].[tblClaimantEmployment]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Crook
-- Create date: 11/15/13
-- Description:	If this is the first employment record for the claimant, mark it primary.
--				If this is not the first employment record for the claimant, toggle the primary records
-- =============================================
CREATE TRIGGER [dbo].[TRIG_INSERT_PrimaryEmployment]
   ON  [dbo].[tblClaimantEmployment]	
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;

	-- Temp table of ClaimantPersonalInfoIDs and primary claimant employment counts in case this is a multi-record insert
		DECLARE @PrimaryCounts TABLE (ClaimantPersonalInfoID bigint, PrimaryCount int)
		INSERT INTO @PrimaryCounts
		SELECT ClaimantPersonalInfoID, COUNT(ClaimantEmploymentID)
			FROM tblClaimantEmployment ce 
			WHERE ClaimantPersonalInfoID IN (SELECT ClaimantPersonalInfoID FROM inserted) -- count the existing primary claimant employment records that this claimant has
			AND ClaimantEmploymentID NOT IN (SELECT ClaimantEmploymentID FROM inserted) -- except we want to ignore the records we just inserted
			AND IsPrimary = 1
		GROUP BY ClaimantPersonalInfoID

	--If there aren't any existing primary records, mark the new record as primary
	UPDATE tblClaimantEmployment
	SET IsPrimary = 1
	FROM tblClaimantEmployment ce
	LEFT JOIN @PrimaryCounts counts ON ce.ClaimantPersonalInfoID = counts.ClaimantPersonalInfoID
	WHERE ClaimantEmploymentID IN (SELECT ClaimantEmploymentID FROM inserted)
	AND counts.PrimaryCount = 0






	--END
	

	---- Join the inserted table with the primary count table to see which records we need to toggle the primary on
	--SELECT *
	--FROM inserted i
	--LEFT JOIN @PrimaryCounts counts ON i.ClaimantPersonalInfoID = counts.ClaimantPersonalInfoID

	--Update the records where the existing primary count is 0 and the newly inserted records primary count is also zero
	--UPDATE tblClaimantEmployment
	--SET IsPrimary = 1
	--FROM @PrimaryCounts counts
	--LEFT JOIN inserted i ON counts.ClaimantPersonalInfoID = i.ClaimantPersonalInfoID
	--WHERE counts.PrimaryCount = 0
	--AND i.IsPrimary = 0

	--Disable the UPDATE trigger
	
	----Then update the records where the existing primary count is > 0 and the newly inserted records have a primary (toggle the old ones off)
	--UPDATE tblClaimantEmployment
	--SET IsPrimary = 0
	--FROM tblClaimantEmployment ce
	--INNER JOIN inserted i ON ce.ClaimantPersonalInfoID = i.ClaimantPersonalInfoID
	--LEFT JOIN @PrimaryCounts counts ON ce.ClaimantPersonalInfoID = counts.ClaimantPersonalInfoID
	--WHERE COALESCE(counts.PrimaryCount, 0) > 0
	--AND ce.IsPrimary = 1
	--AND i.IsPrimary = 1

	--Then update the records where the existing primary count is 0 and the newly inserted records have a no primary flagged (toggle the new ones on)
	--UPDATE tblClaimantEmployment
	--SET IsPrimary = 1
	--FROM tblClaimantEmployment ce
	--INNER JOIN inserted i ON ce.ClaimantPersonalInfoID = i.ClaimantPersonalInfoID
	--LEFT JOIN @PrimaryCounts counts ON ce.ClaimantPersonalInfoID = counts.ClaimantPersonalInfoID
	--WHERE COALESCE(counts.PrimaryCount, 0) = 0
	--AND i.IsPrimary = 0

	--AND counts.ClaimantPersonalInfoID IN (SELECT ClaimantPersonalInfoID FROM inserted) -- count the existing primary claimant employment records that this claimant has
	----AND i.ClaimantEmploymentID NOT IN (SELECT ClaimantEmploymentID FROM inserted) -- except we want to ignore the records we just inserted

	--
	
END

GO
DISABLE TRIGGER [dbo].[TRIG_INSERT_PrimaryEmployment] ON [dbo].[tblClaimantEmployment]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Crook
-- Create date: 11/15/13
-- Description:	When an employment record gets toggled to primary, switch the others to primary = false
-- But always make sure there is 1 marked as primary
-- =============================================
CREATE TRIGGER  [dbo].[TRIG_UPDATE_PrimaryEmployment]
   ON  [dbo].[tblClaimantEmployment]
   AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	--Try to get a ClaimantEmploymentID if the status has changed from false to true
	DECLARE @NewPrimaryEmploymentID bigint
	SET @NewPrimaryEmploymentID =  (SELECT d.ClaimantEmploymentID 
									FROM inserted i
									INNER JOIN deleted d ON i.ClaimantEmploymentID = d.ClaimantEmploymentID
									WHERE d.IsPrimary = 0 and i.IsPrimary = 1

									)

	IF(@NewPrimaryEmploymentID IS NOT NULL)
	BEGIN
		--Set the current primary to false
		UPDATE tblClaimantEmployment
		SET IsPrimary = 0
		WHERE IsPrimary = 1
		AND ClaimantPersonalInfoID IN (SELECT ClaimantPersonalInfoID FROM inserted)
		AND ClaimantEmploymentID <> @NewPrimaryEmploymentID

	END

	
	--If the user deselects one as primary without having another already set (or there isn't another)
	DECLARE @OldPrimaryEmploymentIDs TABLE (ClaimantEmploymentID bigint)
	
	INSERT INTO @OldPrimaryEmploymentIDs
	SELECT i.ClaimantEmploymentID
	FROM inserted i
	INNER JOIN deleted d ON i.ClaimantEmploymentID = d.ClaimantEmploymentID
	WHERE d.IsPrimary = 1
	AND i.IsPrimary = 0
	
	--Get the TOP 1 other employment record for that claim
	--SET @NewPrimaryEmploymentID -- TABLE (ClaimantEmploymentID bigint)
	
	--INSERT INTO @NewPrimaryEmploymentIDs
	SET @NewPrimaryEmploymentID =
	(SELECT TOP 1 ClaimantEmploymentID
	FROM tblClaimantEmployment ce
	WHERE ClaimantEmploymentID NOT IN (SELECT ClaimantEmploymentID FROM @OldPrimaryEmploymentIDs)
	AND ClaimantPersonalInfoID IN (SELECT ClaimantPersonalInfoID FROM deleted))

	--If it exists, flag it as primary
	IF(@NewPrimaryEmploymentID IS NOT NULL)
	BEGIN
		UPDATE tblClaimantEmployment
		SET IsPrimary = 1
		WHERE ClaimantEmploymentID = @NewPrimaryEmploymentID
	END
	ELSE --If it doesn't exist, do not allow the Employment to be unset from primary
	BEGIN
		UPDATE tblClaimantEmployment
		SET IsPrimary = 1
		WHERE ClaimantEmploymentID IN (SELECT ClaimantEmploymentID FROM  @OldPrimaryEmploymentIDs )
	END

END

GO
DISABLE TRIGGER [dbo].[TRIG_UPDATE_PrimaryEmployment] ON [dbo].[tblClaimantEmployment]
GO
ALTER TABLE [dbo].[tblClaimantEmployment] ADD CONSTRAINT [PK__mBtblCla__F25662D42665ABE1] PRIMARY KEY CLUSTERED  ([ClaimantEmploymentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimantEmployment_tblClaimantPersonalInfo] ON [dbo].[tblClaimantEmployment] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimantEmployment_CPID_IsPrimary] ON [dbo].[tblClaimantEmployment] ([ClaimantPersonalInfoID], [IsPrimary]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantEmployment] WITH NOCHECK ADD CONSTRAINT [FK_tblClaimantEmployment_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimantEmployment] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [Ligado_User]
GRANT ALTER ON  [dbo].[tblClaimantEmployment] TO [monahanc]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=1125
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=4785
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpEndDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpStartDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavEmpStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavLatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavLatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'NavLatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
