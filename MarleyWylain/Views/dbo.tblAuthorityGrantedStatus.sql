SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblAuthorityGrantedStatus]
AS

	SELECT     StatusID AS AuthorityGrantedStatusID, Status AS AuthorityGrantedStatus
	FROM         [KGA-ClaimsAdmin].dbo.tblYesNoUnknown
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAuthorityGrantedStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblAuthorityGrantedStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAuthorityGrantedStatus] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblAuthorityGrantedStatus] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblAuthorityGrantedStatus] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblAuthorityGrantedStatus] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblAuthorityGrantedStatus] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAuthorityGrantedStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblAuthorityGrantedStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblAuthorityGrantedStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblAuthorityGrantedStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAuthorityGrantedStatus] TO [power_user]
GO
