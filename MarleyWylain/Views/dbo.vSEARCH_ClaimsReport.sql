SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE VIEW [dbo].[vSEARCH_ClaimsReport] AS 

SELECT CLAIMLAWSUITID                                      AS ClaimLawsuitID
       , CLAIMANTPERSONALINFOID                            AS ClaimantPersonalInfoID
       , ACTIVETRIALDATE                                   AS ActiveTrialDate
       , CLAIMID                                           AS ClaimID
       , CLAIMLAWSUITSTATUS                                AS ClaimLawsuitStatus
       , MATTER                                            AS Matter
       , CLAIMSEQUENCE                                     AS ClaimSequence
       --, firstname
       --, lastname
       , LASTNAME + ', ' + FIRSTNAME                       AS FullName
       , BIRTHDATE                                         AS BirthDate
       , DECEASEDDATE                                      AS DeceasedDate
       , ( CASE
             WHEN SSN4DIGIT IS NOT NULL THEN 'XXX-XX-' + SSN4DIGIT
             ELSE NULL
           END )                                           SocialSecurityNumber
       , OCCUPATION                                        AS Occupation
       , HIGHLEVELDISEASE                                  AS HighLevelDisease
       , DOCKETNUMBER                                      AS DocketNumber
       , SERVICEDATE                                       AS ServiceDate
       , COALESCE(CLIENTFILEDATE, FILEDATE, RECEIVEDDATE)  AS FileDate
       , [STATE]                                           AS State
       , JURISDICTION                                      AS Jurisdiction
       , PROCESSEDDATE                                     AS ProcessedDate
       , STATUSDATE                                        AS StatusDate
       , PRODUCTNOTES                                      AS ProductNotes
       , STARTDATE                                         AS ProductExposureStart
       , SETTLEMENTDATE                                    AS SettlementDate
	   , SETTLEMENTPAIDDATE								   As SettlementPaidDate
       , SETTLEMENTAMOUNT                                  AS SettlementAmount
       , SOURCEDATE --AS SettlementPaidDate -- Date paid; source date is the source date of the first check that is cut for a settlement
       , PLAINTIFFCOUNSEL                                  AS PlaintiffCounsel
       , CLAIMLAWSUITSTATUSID                              AS ClaimLawsuitStatusID
       , STATEID                                           AS StateID
       , JURISDICTIONID                                    AS JurisdictionID
       , ISMDL                                             AS IsMDL
       , HIGHLEVELDISEASEID                                AS HighLevelDiseaseID
       , ALLEGATIONTYPEID                                  AS AllegationTypeID
       , PLAINTIFFCOUNSELID                                AS PlaintiffCounselID
       , MATTERID                                          AS MatterID
       , LAWSUITDEFENSECOUNSELADDRESSID                    AS DefenseCounselAddressID
       , DEFCOUNSELJURISDICTIONINFODEFENSECOUNSELADDRESSID AS DefenseCounselAddressID2
       , 0                                                 AS ClaimLawsuitStatusGroupID --NOTE: this is a dummy field because Marley doesn't require it but we still need the field. SK, 1/27/2014 (launch day)
       , CONVERT(INT, NULL)                                AS PersonID
       , CONVERT(INT, NULL)                                AS BrandID
       , CONVERT(NVARCHAR(50), NULL)                       AS Brand
       , CONVERT(INT, NULL)                                AS BankruptcyStayID
       , CONVERT(NVARCHAR(50), NULL)                       AS BankruptcyStay
       , CONVERT(INT, NULL)                                AS ClaimLawsuitTypeID
       , CONVERT(NVARCHAR(50), NULL)                       AS ClaimLawsuitType
       , CONVERT(NVARCHAR(100), NULL)                      AS JobsiteCity
       , CONVERT(INT, NULL)                                AS JobsiteStateID
       , CONVERT(NVARCHAR(1000), NULL)                     AS Jobsite
       , CONVERT(INT, NULL)                                AS BusinessCaseID
       , CONVERT(NVARCHAR(50), NULL)                       AS BusinessCase
	   , ClaimLawsuitNotes
	   , TrialNotes -- FYI TrialNotes is literally the same as ClaimLawsuitNotes; it is here so that GSX can refer to it this way in their CSV exports.
FROM   (SELECT CLAIMLAWSUITID
               , ACTIVETRIALDATE
               , CLAIMID
               , LAWSUITID
               , ISMDL
               , CLAIMLAWSUITSTATUS
               , CLAIMLAWSUITSTATUSID
               , MATTER
               , CLAIMSEQUENCE
               , CLAIMANTPERSONALINFOID
               , FIRSTNAME
               , LASTNAME
               , BIRTHDATE
               , DECEASEDDATE
               , SSN4DIGIT
               , OCCUPATION
               , HIGHLEVELDISEASE
               , DOCKETNUMBER
               , SERVICEDATE
               , FILEDATE
               , RECEIVEDDATE
               , [STATE]
               , JURISDICTION
               , PROCESSEDDATE
               , STATUSDATE
               , PRODUCTNOTES
               , STARTDATE
               , SETTLEMENTDATE
			   , SETTLEMENTPAIDDATE
               , SETTLEMENTAMOUNT
               , SOURCEDATE
               , PLAINTIFFCOUNSEL
               , STATEID
               , JURISDICTIONID
               , HIGHLEVELDISEASEID
               , PLAINTIFFCOUNSELID
               , LAWSUITDEFENSECOUNSEL
               , LAWSUITDEFENSECOUNSELID
               , LAWSUITDEFENSECOUNSELADDRESSID
               , MATTERID
               , ALLEGATIONTYPEID
			   , CLIENTFILEDATE
			   , CLAIMLAWSUITNOTES
			   , TRIALNOTES
        FROM   (SELECT CLAIMLAWSUITID
                       , ACTIVETRIALDATE
                       , CLAIMID
                       , LAWSUITID
                       , ISMDL
                       , CLAIMLAWSUITSTATUS
                       , CLAIMLAWSUITSTATUSID
                       , DOCKETNUMBER
                       , SERVICEDATE
                       , FILEDATE
                       , RECEIVEDDATE
                       , [STATE]
                       , STATEID
                       , JURISDICTION
                       , JURISDICTIONID
                       , PROCESSEDDATE
                       , STATUSDATE
                       , SETTLEMENTDATE
					   , SETTLEMENTPAIDDATE
                       , SETTLEMENTAMOUNT
                       , SOURCEDATE
                       , PLAINTIFFCOUNSEL
                       , PLAINTIFFCOUNSELID
                       , LAWSUITDEFENSECOUNSEL
                       , LAWSUITDEFENSECOUNSELID
                       , LAWSUITDEFENSECOUNSELADDRESSID
                       , ALLEGATIONTYPEID
					   , CLIENTFILEDATE
					   , CLAIMLAWSUITNOTES
					   , TRIALNOTES
                FROM   (SELECT CLAIMLAWSUITID
                               , ACTIVETRIALDATE
                               , CLAIMID
                               , LAWSUITID
                               , ISMDL
                               , SETTLEMENTDATE
							   , SETTLEMENTPAIDDATE
                               , SETTLEMENTAMOUNT
                               , SOURCEDATE
							   , CLAIMLAWSUITNOTES
							   , CLAIMLAWSUITNOTES AS TRIALNOTES
                        FROM   TBLCLAIMLAWSUIT
                               LEFT OUTER JOIN (SELECT CLAIMLAWSUITID      AS settlmentInfoClaimLawsuitid
                                                       , SETTLEMENTDATE
													   , SETTLEMENTPAIDDATE
                                                       , SETTLEMENTAMOUNT
                                                       , MATCHEDSOURCEDATE AS sourcedate
                                                FROM   (SELECT CLAIMSETTLEMENTID
                                                               , EARLIESTCHECKID
                                                               , CLAIMLAWSUITID
                                                               , SETTLEMENTDATE
															   , PAIDDATE As SETTLEMENTPAIDDATE
                                                               , SETTLEMENTAMOUNT
                                                        FROM   TBLCLAIMSETTLEMENT
                                                               LEFT OUTER JOIN (SELECT CLAIMSETTLEMENTID AS SettlementPaymentInfoClaimSettlementID
                                                                                       , MIN(CHECKID)    AS EarliestCheckID
                                                                                FROM   TBLPAYMENTSETTLEMENT
                                                                                       LEFT OUTER JOIN (SELECT CHECKID AS checkInfoCheckID
                                                                                                        FROM   TBLCHECK) checkInfo
                                                                                                    ON ( TBLPAYMENTSETTLEMENT.CHECKID = checkInfo.CHECKINFOCHECKID )
                                                                                GROUP  BY CLAIMSETTLEMENTID) settlementPaymentInfo
                                                                            ON ( TBLCLAIMSETTLEMENT.CLAIMSETTLEMENTID = settlementPaymentInfo.SETTLEMENTPAYMENTINFOCLAIMSETTLEMENTID ))settlementAndCheckInfo
                                                       left outer JOIN (SELECT CHECKID      AS matchedCheckID
                                                                          , SOURCEDATE AS MatchedSourceDate
                                                                   FROM   TBLCHECK)additionalCheckInfo
                                                               ON ( settlementAndCheckInfo.EARLIESTCHECKID = additionalCheckInfo.MATCHEDCHECKID ))settlementInfo
                                            ON ( TBLCLAIMLAWSUIT.CLAIMLAWSUITID = settlementinfo.SETTLMENTINFOCLAIMLAWSUITID )
                        WHERE  ISPRIMARYFORCLAIM = 1) CLInfo
                       LEFT OUTER JOIN(SELECT CLAIMLAWSUITID AS allegationTypeClaimLawsuitID, ClientFileDate
                                              , ALLEGATIONTYPEID
                                       FROM   TBLALLEGATION) AllegationTypeInfo
                                    ON ( CLInfo.CLAIMLAWSUITID = AllegationTypeInfo.ALLEGATIONTYPECLAIMLAWSUITID )
                       INNER JOIN (SELECT trackingInfo.CLAIMLAWSUITSTATUSID
                                               , trackingInfo.CLAIMLAWSUITID AS ClaimLawsuitID_fromTrackingInfo
                                               , statusLabelnfo.CLAIMLAWSUITSTATUS
                                               , PROCESSEDDATE
                                               , STATUSDATE
                                        FROM   (SELECT CLAIMLAWSUITSTATUSID
                                                       , CLAIMLAWSUITID
                                                       , PROCESSEDDATE
                                                       , STATUSDATE
                                                FROM   TBLCLAIMLAWSUITSTATUSTRACKING
                                                WHERE  ISPRIMARYSTATUS = 1) trackingInfo
                                               LEFT OUTER JOIN (SELECT CLAIMLAWSUITSTATUSID
                                                                       , CLAIMLAWSUITSTATUS
                                                                FROM   TBLCLAIMLAWSUITSTATUS)statusLabelnfo
                                                            ON ( trackingInfo.CLAIMLAWSUITSTATUSID = statusLabelnfo.CLAIMLAWSUITSTATUSID )) lawsuitstatusinfo
                                    ON ( CLInfo.CLAIMLAWSUITID = lawsuitstatusinfo.CLAIMLAWSUITID_FROMTRACKINGINFO )
                       LEFT OUTER JOIN (SELECT LAWSUITID                 AS lawsuitinfolawsuitid
                                               , DOCKETNUMBER
                                               , SERVICEDATE
                                               , FILEDATE
                                               , RECEIVEDDATE
                                               , [STATE]
                                               , STATEID
                                               , JURISDICTION
                                               , JURISDICTIONID
                                               , PLAINTIFFCOUNSEL
                                               , PLAINTIFFCOUNSELID
                                               , DEFENSECOUNSEL          AS LawsuitDefenseCounsel
                                               , DEFENSECOUNSELADDRESSID AS LawsuitDefenseCounselAddressID
                                               , DEFENSECOUNSELID        AS LawsuitDefenseCounselID
                                        FROM   TBLLAWSUIT
                                               LEFT OUTER JOIN (SELECT STATEID AS stateInfoStateID
                                                                       , [STATE]
                                                                FROM   TBLSTATE)stateInfo
                                                            ON( TBLLAWSUIT.STATEID = stateInfo.STATEINFOSTATEID )
                                               LEFT OUTER JOIN (SELECT JURISDICTIONID AS jurisdictioninfoInfojurisdictionID
                                                                       , JURISDICTION
                                                                FROM   TBLJURISDICTION)jurisdictionInfo
                                                            ON( TBLLAWSUIT.JURISDICTIONID = jurisdictionInfo.JURISDICTIONINFOINFOJURISDICTIONID )
                                               LEFT OUTER JOIN (SELECT PLAINTIFFCOUNSELADDRESSID AS plaintiffCounselInfoAddressId
                                                                       , PLAINTIFFCOUNSEL
                                                                       , PLAINTIFFCOUNSELID
                                                                FROM   TBLPLAINTIFFCOUNSELADDRESS) plaintiffCounselInfo
                                                            ON ( TBLLAWSUIT.PLAINTIFFCOUNSELADDRESSID = plaintiffCounselInfo.PLAINTIFFCOUNSELINFOADDRESSID )
                                               LEFT OUTER JOIN (SELECT DEFENSECOUNSELADDRESSID AS defenseCounselInfoAddressID
                                                                       , DEFENSECOUNSEL
                                                                       , DEFENSECOUNSELID
                                                                FROM   TBLDEFENSECOUNSELADDRESS)defensecounselinfo
                                                            ON ( TBLLAWSUIT.DEFENSECOUNSELADDRESSID = defensecounselinfo.DEFENSECOUNSELINFOADDRESSID ))lawsuitInfo
                                    ON ( CLInfo.LAWSUITID = lawsuitInfo.LAWSUITINFOLAWSUITID ))LawsuitLevelInfo
               LEFT OUTER JOIN (SELECT CLAIMID_FROMTBLCLAIM     AS claimID_fromtblClaim
                                       , CLAIMANTPERSONALINFOID AS claimantpersonalinfoid
                                       , MATTER
                                       , MATTERID
                                       , CLAIMSEQUENCE
                                       , HIGHLEVELDISEASE
                                       , HIGHLEVELDISEASEID
                                FROM   (SELECT CLAIMID AS claimID_fromtblClaim
                                               , CLAIMANTPERSONALINFOID
                                               , MATTERID
                                               , CLAIMSEQUENCEID
                                        FROM   TBLCLAIM)ClaimInfo
                                       LEFT OUTER JOIN (SELECT MATTERID AS matterID_fromtblMatter
                                                               , MATTER
                                                        FROM   TBLMATTER) matterInfo
                                                    ON ( ClaimInfo.MATTERID = matterInfo.MATTERID_FROMTBLMATTER )
                                       LEFT OUTER JOIN (SELECT CLAIMSEQUENCEID
                                                               , CLAIMSEQUENCE
                                                        FROM   TBLCLAIMSEQUENCE) ClaimSequenceInfo
                                                    ON ( ClaimInfo.CLAIMSEQUENCEID = ClaimSequenceInfo.CLAIMSEQUENCEID )
                                       LEFT OUTER JOIN (SELECT CLAIMID
                                                               , HIGHLEVELDISEASE
                                                               , HIGHLEVELDISEASEID
                                                        FROM   (SELECT CLAIMID
                                                                       , DISEASEID
                                                                       , PRIMARYDISEASE
                                                                FROM   TBLCLAIMDISEASE) theClaimDiseaseInfo
                                                               LEFT OUTER JOIN (SELECT DISEASEID AS diseaseInfodiseaseid
                                                                                       , HIGHLEVELDISEASEID
                                                                                FROM   TBLDISEASE) diseaseInfo
                                                                            ON( theClaimDiseaseInfo.DISEASEID = diseaseInfo.DISEASEINFODISEASEID )
                                                               LEFT OUTER JOIN (SELECT HIGHLEVELDISEASEID AS hldiseaseinfohldiseaseid
                                                                                       , HIGHLEVELDISEASE
                                                                                FROM   TBLHIGHLEVELDISEASE) hlDiseaseInfo
                                                                            ON( diseaseInfo.HIGHLEVELDISEASEID = hlDiseaseInfo.HLDISEASEINFOHLDISEASEID )
                                                        WHERE  theClaimDiseaseInfo.PRIMARYDISEASE = 1) ClaimDiseaseInfo
                                                    ON( ClaimInfo.CLAIMID_FROMTBLCLAIM = ClaimDiseaseInfo.CLAIMID ))ClaimLevelInfo
                            ON ( LawsuitLevelInfo.CLAIMID = ClaimLevelInfo.CLAIMID_FROMTBLCLAIM )
               LEFT OUTER JOIN(SELECT CLAIMID                            AS claimID_forclaimantinfo
                                      , ClaimInfo.CLAIMANTPERSONALINFOID AS personalinfoid
                                      , FIRSTNAME
                                      , LASTNAME
                                      , BIRTHDATE
                                      , DECEASEDDATE
                                      , SSN4DIGIT
                                      , OCCUPATION
                                      , PRODUCTNOTES
                                      , STARTDATE
                               FROM   (SELECT CLAIMID
                                              , CLAIMANTPERSONALINFOID
                                       FROM   TBLCLAIM) ClaimInfo
                                      LEFT OUTER JOIN (SELECT CLAIMANTPERSONALINFOID
                                                              , FIRSTNAME
                                                              , LASTNAME
                                                              , BIRTHDATE
                                                              , DECEASEDDATE
                                                              , RIGHT(SOCIALSECURITYNUMBER, 4) AS SSN4Digit
                                                       FROM   TBLCLAIMANTPERSONALINFO) ClaimantInfo
                                                   ON ( ClaimInfo.CLAIMANTPERSONALINFOID = ClaimantInfo.CLAIMANTPERSONALINFOID )
                                      LEFT OUTER JOIN (SELECT EMPLOYMENTPERSONALINFOID
                                                              , OCCUPATION
                                                              , PRODUCTNOTES
                                                              , EARLIESTSTARTDATE AS startdate
                                                       FROM   (SELECT CLAIMANTPERSONALINFOID AS employmentPersonalInfoId
                                                                      , OCCUPATIONID
                                                                      , CLAIMANTEMPLOYMENTID
                                                               FROM   TBLCLAIMANTEMPLOYMENT
                                                               WHERE  ISPRIMARY = 1) empInfo
                                                              LEFT OUTER JOIN (SELECT OCCUPATIONID
                                                                                      , OCCUPATION
                                                                               FROM   TBLOCCUPATION) occInfo
                                                                           ON ( empInfo.OCCUPATIONID = occInfo.OCCUPATIONID )
                                                              LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS prodInfoClaimentEmploymentId
                                                                                      , MINSTARTDATE       AS EarliestStartDate
                                                                                      , MINIMUMPRODUCTID
                                                                                      , PRODUCTNOTES
                                                                               FROM   (SELECT CLAIMANTEMPLOYMENTID
                                                                                              , MINSTARTDATE
                                                                                              , MIN(PRODUCTID) AS MinimumProductID
                                                                                       FROM   (SELECT *
                                                                                               FROM   (SELECT CLAIMANTEMPLOYMENTID
                                                                                                              , MIN(STARTDATE) AS MinStartDate
                                                                                                       FROM   TBLCLAIMANTPRODUCT
                                                                                                       GROUP  BY CLAIMANTEMPLOYMENTID)MinimumStartDates
                                                                                                      LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS sdEmploymentID
                                                                                                                              , STARTDATE          AS sdStartDate
                                                                                                                              , PRODUCTID
                                                                                                                       FROM   TBLCLAIMANTPRODUCT)StartDatesAndProductIDs
                                                                                                                   ON ( MinimumStartDates.CLAIMANTEMPLOYMENTID = StartDatesAndProductIDs.SDEMPLOYMENTID
                                                                                                                        AND MinimumStartDates.MINSTARTDATE = StartDatesAndProductIDs.SDSTARTDATE ))MinStartDatesAndProductIDs
                                                                                       GROUP  BY CLAIMANTEMPLOYMENTID
                                                                                                 , MINSTARTDATE)EarliestProductForEmployment
                                                                                      LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS nEmploymentID
                                                                                                              , STARTDATE          AS nStartDate
                                                                                                              , PRODUCTID          AS nProductID
                                                                                                              , PRODUCTNOTES
                                                                                                       FROM   TBLCLAIMANTPRODUCT)NotesQuery
                                                                                                   ON ( EarliestProductForEmployment.CLAIMANTEMPLOYMENTID = NotesQuery.NEMPLOYMENTID
                                                                                                        AND EarliestProductForEmployment.MINSTARTDATE = NotesQuery.NSTARTDATE
                                                                                                        AND EarliestProductForEmployment.MINIMUMPRODUCTID = NotesQuery.NPRODUCTID )) prodInfo
                                                                           ON ( empInfo.CLAIMANTEMPLOYMENTID = prodInfo.PRODINFOCLAIMENTEMPLOYMENTID )) claimantEmployment
                                                   ON ( ClaimantInfo.CLAIMANTPERSONALINFOID = claimantEmployment.EMPLOYMENTPERSONALINFOID )) ClaimantLevelInfo
                            ON( LawsuitLevelInfo.CLAIMID = ClaimantLevelInfo.CLAIMID_FORCLAIMANTINFO ))MainQuery
       LEFT OUTER JOIN (SELECT DEFENSECOUNSELADDRESSID AS defCounselJurisdictionInfoDefenseCounselAddressID
                               , STATEID               AS defCounselJurisdictionInfoStateID
                               , JURISDICTIONID        AS defCounselJurisdictionInfoJurisdictionId
                        FROM   TBLDEFENSEJURISDICTION)defCounselJurisdictionInfo
                    ON ( MainQuery.STATEID = defCounselJurisdictionInfo.DEFCOUNSELJURISDICTIONINFOSTATEID
                         AND MainQuery.JURISDICTIONID = defCounselJurisdictionInfo.DEFCOUNSELJURISDICTIONINFOJURISDICTIONID )
GROUP  BY CLAIMLAWSUITID
          , CLAIMANTPERSONALINFOID
          , CLAIMID
          , ISMDL
          , ACTIVETRIALDATE
          , CLAIMLAWSUITSTATUS
          , MATTER
          , CLAIMSEQUENCE
          , FIRSTNAME
          , LASTNAME
          , BIRTHDATE
          , DECEASEDDATE
          , SSN4DIGIT
          , OCCUPATION
          , HIGHLEVELDISEASE
          , ACTIVETRIALDATE
          , DOCKETNUMBER
		  , CLIENTFILEDATE
          , SERVICEDATE
          , FILEDATE
          , RECEIVEDDATE
          , [STATE]
          , JURISDICTION
          , PROCESSEDDATE
          , STATUSDATE
          , PRODUCTNOTES
          , STARTDATE
          , SETTLEMENTDATE
		  , SETTLEMENTPAIDDATE
          , SETTLEMENTAMOUNT
          , SOURCEDATE
          , PLAINTIFFCOUNSEL
          , CLAIMLAWSUITSTATUSID
          , STATEID
          , JURISDICTIONID
          , HIGHLEVELDISEASEID
          , ALLEGATIONTYPEID
          , PLAINTIFFCOUNSELID
          , MATTERID
          , LAWSUITDEFENSECOUNSEL
          , LAWSUITDEFENSECOUNSELID
          , LAWSUITDEFENSECOUNSELADDRESSID
          , DEFCOUNSELJURISDICTIONINFODEFENSECOUNSELADDRESSID 
		  , ClaimLawsuitNotes
		  , TrialNotes







GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_ClaimsReport] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_ClaimsReport] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_ClaimsReport] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_ClaimsReport] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_ClaimsReport] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_ClaimsReport] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[5] 4[7] 2[70] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -1395
      End
      Begin Tables = 
         Begin Table = "MainQuery"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 298
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "defCounselJurisdictionInfo"
            Begin Extent = 
               Top = 6
               Left = 336
               Bottom = 99
               Right = 687
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 8610
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimsReport', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimsReport', NULL, NULL
GO
