SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vSEARCH_ClaimLawsuitClaimant]
AS
SELECT DISTINCT 
                      CL.ClaimLawsuitID, CPI.ClaimantPersonalInfoID, CASE WHEN Suffix IS NULL 
                      THEN CPI.LastName + ', ' ELSE CPI.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                      CASE WHEN SettlementAmount IS NOT NULL THEN CONVERT(nvarchar(250), SettlementAmount, 1) ELSE '' END AS SettlementAmount, CONVERT(nvarchar(100), 
                      cs.SettlementDate, 101) AS SettlementDate, cs.PayToCompany, CONVERT(nvarchar(100), cs.SettlementDueDate, 101) AS SettlementDueDate, cs.SettlementNotes, 
                      cs.SPUsername, PCA.PlaintiffCounselTaxIDNum AS NationalTaxID, PCA.PlaintiffCounsel, CONVERT(nvarchar(100), GETDATE(), 101) AS NotificationDate, 
                      CASE WHEN LeadPlaintiffFirstName IS NOT NULL THEN LeadPlaintiffFirstName + ' ' + LeadPlaintiffLastName ELSE LeadPlaintiffLastName END AS LeadPlaintiff, 
                      L.LeadPlaintiffFirstName, L.LeadPlaintiffLastName, L.PlaintiffCounselAddressID, cs.SPUsername AS CreatedBy, sg.SettlementGroup, sg.PaymentTermsID, 
                      pt.PaymentTerms, clst.ClaimLawsuitStatusID, clstatus.ClaimLawsuitStatus, CPI.LastName, CPI.FirstName, CPI.MiddleName, j.Jurisdiction, L.DocketNumber, 
                      rt.ReleaseType, CONVERT(nvarchar, COALESCE (cs.SettlementDate, '1/1/1900'), 101) AS ResolutionDate, TrialGroup, cl.TrialGroupID,
					  cpi.FullName,  cl.ActiveTrialDate
FROM         dbo.tblClaimLawsuit AS CL LEFT OUTER JOIN
                      dbo.tblClaimSettlement AS cs ON CL.ClaimLawsuitID = cs.ClaimLawsuitID INNER JOIN
                      dbo.tblClaim AS C ON CL.ClaimID = C.ClaimID INNER JOIN
                      dbo.tblClaimantPersonalInfo AS CPI ON C.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblClaimantSuffix AS CLS ON CPI.SuffixID = CLS.SuffixID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID LEFT OUTER JOIN
                      dbo.tblPlaintiffCounselAddress AS PCA ON PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID LEFT OUTER JOIN
                      dbo.tblClaimSettlementGroup AS sg ON cs.SettlementGroupID = sg.SettlementGroupID LEFT OUTER JOIN
                      dbo.tblPaymentTerms AS pt ON sg.PaymentTermsID = pt.PaymentTermsID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatusTracking AS clst ON CL.ClaimLawsuitID = clst.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatus AS clstatus ON clst.ClaimLawsuitStatusID = clstatus.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblJurisdiction AS j ON L.JurisdictionID = j.JurisdictionID LEFT OUTER JOIN
                      dbo.tblReleaseType AS rt ON cs.ReleaseTypeID = rt.ReleaseTypeID
						LEFT JOIN tblTrialGroup tg ON cl.TrialGroupID = tg.TrialGroupID
WHERE     (COALESCE (clst.IsPrimaryStatus, 0) = 1)




GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_ClaimLawsuitClaimant] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CL"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 399
               Right = 282
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "C"
            Begin Extent = 
               Top = 6
               Left = 506
               Bottom = 135
               Right = 738
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CPI"
            Begin Extent = 
               Top = 6
               Left = 776
               Bottom = 135
               Right = 1026
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CLS"
            Begin Extent = 
               Top = 6
               Left = 1064
               Bottom = 101
               Right = 1234
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "L"
            Begin Extent = 
               Top = 102
               Left = 1064
               Bottom = 231
               Right = 1303
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PCA"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 267
            End
            DisplayFlags = 280
            ', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimLawsuitClaimant', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'TopColumn = 0
         End
         Begin Table = "sg"
            Begin Extent = 
               Top = 270
               Left = 513
               Bottom = 382
               Right = 703
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pt"
            Begin Extent = 
               Top = 72
               Left = 298
               Bottom = 167
               Right = 478
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clst"
            Begin Extent = 
               Top = 138
               Left = 516
               Bottom = 267
               Right = 765
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clstatus"
            Begin Extent = 
               Top = 138
               Left = 803
               Bottom = 267
               Right = 1039
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "j"
            Begin Extent = 
               Top = 168
               Left = 305
               Bottom = 263
               Right = 475
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rt"
            Begin Extent = 
               Top = 264
               Left = 305
               Bottom = 359
               Right = 475
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 31
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimLawsuitClaimant', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimLawsuitClaimant', NULL, NULL
GO
