SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vtblFirmsAndVendors]
AS
SELECT     firmid, firm
FROM         vtblFirm
UNION
SELECT     vendorfirmid, vendorfirm
FROM         tblVendorFirm

GO
GRANT VIEW DEFINITION ON  [dbo].[vtblFirmsAndVendors] TO [base_user]
GRANT SELECT ON  [dbo].[vtblFirmsAndVendors] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vtblFirmsAndVendors] TO [power_user]
GRANT SELECT ON  [dbo].[vtblFirmsAndVendors] TO [power_user]
GO
