SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [dbo].[vsearch_ClaimsReport_Marley_lawsuitstatusinfo]
as
	SELECT   claws.CLAIMLAWSUITID
           , ACTIVETRIALDATE
           , CLAIMID
           , claws.LAWSUITID
           , ISMDL
           , CLAIMLAWSUITSTATUS
           , cllawsttrak.CLAIMLAWSUITSTATUSID
           , DOCKETNUMBER
           , laws.SERVICEDATE
           , FILEDATE
           , laws.RECEIVEDDATE
           , st.[STATE]
           , st.STATEID
           , JURISDICTION
           , jurs.JURISDICTIONID
           , PROCESSEDDATE
           , STATUSDATE  
           , PLAINTIFFCOUNSEL
           , PLAINTIFFCOUNSELID          
           , ALLEGATIONTYPEID
			, CLIENTFILEDATE
			, CLAIMLAWSUITNOTES
			, DEFENSECOUNSEL          AS LawsuitDefenseCounsel
            , laws.DEFENSECOUNSELADDRESSID AS LawsuitDefenseCounselAddressID
            , DEFENSECOUNSELID        AS LawsuitDefenseCounselID
		--	,dejur.DefenseCounselAddressID AS defCounselJurisdictionInfoDefenseCounselAddressID
    from 
	  dbo.TBLCLAIMLAWSUIT claws
	  join TBLCLAIMLAWSUITSTATUSTRACKING cllawsttrak
	  on cllawsttrak.ClaimLawsuitID = claws.ClaimLawsuitID
	  and isnull(ISPRIMARYFORCLAIM,0) = 1
	  and isnull(ISPRIMARYSTATUS,0) = 1	
	left join dbo.TBLCLAIMLAWSUITSTATUS cllawstat
	on cllawstat.CLAIMLAWSUITSTATUSID = cllawsttrak.ClaimLawsuitStatusID -- lawsuitstatusinfo
	left join dbo.TBLALLEGATION alleg  -- lawsuitstatusinfo
	on alleg.CLAIMLAWSUITID = cllawsttrak.CLAIMLAWSUITID		
	left join dbo.TBLLAWSUIT laws -- LawsuitLevelInfo
	on laws.LawsuitID = claws.LawsuitID
	left join dbo.TBLSTATE st
	on st.StateID = laws.StateID
	left join dbo.TBLJURISDICTION jurs
	on jurs.JurisdictionID = laws.JurisdictionID
	left join dbo.TBLPLAINTIFFCOUNSELADDRESS plntcnsladdr
	on plntcnsladdr.PlaintiffCounselAddressID = laws.PlaintiffCounselAddressID
	left join dbo.TBLDEFENSECOUNSELADDRESS decnsladdr
	on decnsladdr.DefenseCounselAddressID = laws.DefenseCounselAddressID -- LawsuitLevelInfo
	--left join dbo.TBLDEFENSEJURISDICTION dejur
	--on dejur.StateID = laws.StateID
	--and dejur.JurisdictionID = laws.JurisdictionID
	
	where 
	 isnull(ISPRIMARYFORCLAIM,0) = 1
	 and isnull(ISPRIMARYSTATUS,0) = 1	
	--and ID = 1
	 group by 
	       claws.CLAIMLAWSUITID
           , ACTIVETRIALDATE
           , CLAIMID
           , claws.LAWSUITID
           , ISMDL
           , CLAIMLAWSUITSTATUS
           , cllawsttrak.CLAIMLAWSUITSTATUSID
           , DOCKETNUMBER
           , laws.SERVICEDATE
           , FILEDATE
           , laws.RECEIVEDDATE
           , st.[STATE]
           , st.STATEID
           , JURISDICTION
           , jurs.JURISDICTIONID
           , PROCESSEDDATE
           , STATUSDATE  
           , PLAINTIFFCOUNSEL
           , PLAINTIFFCOUNSELID          
           , ALLEGATIONTYPEID
		    , CLIENTFILEDATE
			, CLAIMLAWSUITNOTES
			, DEFENSECOUNSEL          
            , laws.DEFENSECOUNSELADDRESSID 
            , DEFENSECOUNSELID        
		--	,dejur.DefenseCounselAddressID 
GO
