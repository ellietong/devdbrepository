SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblBrokerRef]
AS
SELECT BrokerRefID, BrokerRefNum
FROM [KGA-Carrier].dbo.tblBrokerRefNum bl

GO
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerRef] TO [base_user]
GRANT SELECT ON  [dbo].[tblBrokerRef] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerRef] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBrokerRef] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBrokerRef] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBrokerRef] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBrokerRef] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBrokerRef] TO [power_user]
GRANT SELECT ON  [dbo].[tblBrokerRef] TO [power_user]
GRANT INSERT ON  [dbo].[tblBrokerRef] TO [power_user]
GRANT DELETE ON  [dbo].[tblBrokerRef] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBrokerRef] TO [power_user]
GO
