SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vClaimLawsuitDeceased]
AS
SELECT     dbo.tblClaimLawsuit.ClaimLawsuitID
FROM         dbo.tblClaimLawsuit INNER JOIN
                      dbo.tblClaim ON dbo.tblClaimLawsuit.ClaimID = dbo.tblClaim.ClaimID INNER JOIN
                      dbo.tblClaimantPersonalInfo ON dbo.tblClaim.ClaimantPersonalInfoID = dbo.tblClaimantPersonalInfo.ClaimantPersonalInfoID
WHERE     (dbo.tblClaimantPersonalInfo.DeceasedDate IS NOT NULL)

GO
GRANT VIEW DEFINITION ON  [dbo].[vClaimLawsuitDeceased] TO [base_user]
GRANT SELECT ON  [dbo].[vClaimLawsuitDeceased] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vClaimLawsuitDeceased] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vClaimLawsuitDeceased] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vClaimLawsuitDeceased] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vClaimLawsuitDeceased] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vClaimLawsuitDeceased] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vClaimLawsuitDeceased] TO [power_user]
GRANT SELECT ON  [dbo].[vClaimLawsuitDeceased] TO [power_user]
GRANT INSERT ON  [dbo].[vClaimLawsuitDeceased] TO [power_user]
GRANT DELETE ON  [dbo].[vClaimLawsuitDeceased] TO [power_user]
GRANT UPDATE ON  [dbo].[vClaimLawsuitDeceased] TO [power_user]
GO
