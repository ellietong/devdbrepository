SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vClaimLawsuitIsFullyDocumented]
AS
SELECT csd.ClaimLawsuitID, 
	CASE WHEN csd.ClaimantIDed           > 0 THEN 1 ELSE 0 END + 
	CASE WHEN csd.DxMedicals             > 0 THEN 1 ELSE 0 END + 
	CASE WHEN csd.DODCert                > 0 THEN 1 ELSE 0 END +
	CASE WHEN csd.EstatePapers           > 0 THEN 1 ELSE 0 END + 
	CASE WHEN csd.ProductID              > 0 THEN 1 ELSE 0 END + 
	CASE WHEN csd.EmployExpoHist         > 0 THEN 1 ELSE 0 END + 
	CASE WHEN csd.CompServ               > 0 THEN 1 ELSE 0 END + 
	CASE WHEN csd.SettlementConfirmation > 0 THEN 1 ELSE 0 END + 
	CASE WHEN csd.MMSEA                  > 0 THEN 1 ELSE 0 END + 
	CASE WHEN csd.PreTrialReport         > 0 THEN 1 ELSE 0 END + 
	CASE WHEN csd.ExecutedRelease        > 0 THEN 1 ELSE 0 END + 
	CASE WHEN csd.AllstateMMSEAForm      > 0 THEN 1 ELSE 0 END AS Total, 
	CASE WHEN cld.ClaimLawsuitID IS NULL THEN csdc.CategoryCount - 2 ELSE csdc.CategoryCount END AS CategoryCount, 
	CASE WHEN (CASE WHEN cld.ClaimLawsuitID IS NULL THEN (CASE WHEN csd.ClaimantIDed           > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.DxMedicals             > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.ProductID              > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.EmployExpoHist         > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.CompServ               > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.SettlementConfirmation > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.MMSEA                  > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.PreTrialReport         > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.ExecutedRelease        > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.AllstateMMSEAForm      > 0 THEN 1 ELSE 0 END) 
													ELSE (CASE WHEN csd.ClaimantIDed           > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.DxMedicals             > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.DODCert                > 0 THEN 1 ELSE 0 END +
														  CASE WHEN csd.EstatePapers           > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.ProductID              > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.EmployExpoHist         > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.CompServ               > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.SettlementConfirmation > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.MMSEA                  > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.PreTrialReport         > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.ExecutedRelease        > 0 THEN 1 ELSE 0 END + 
														  CASE WHEN csd.AllstateMMSEAForm      > 0 THEN 1 ELSE 0 END) END = (CASE WHEN cld.ClaimLawsuitID IS NULL THEN csdc.CategoryCount - 2 ELSE csdc.CategoryCount END))
	     THEN 1 ELSE 0 END AS FullyDocumented
FROM dbo.tblClaimSettlementDocumentation AS csd 
	INNER JOIN (SELECT COUNT(DocCategory) AS CategoryCount FROM dbo.tblClaimSettlementDocumentCategory) AS csdc ON csd.ClaimLawsuitID = csd.ClaimLawsuitID 
	LEFT OUTER JOIN dbo.vClaimLawsuitDeceased AS cld ON csd.ClaimLawsuitID = cld.ClaimLawsuitID


GO
GRANT VIEW DEFINITION ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [base_user]
GRANT SELECT ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [power_user]
GRANT SELECT ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [power_user]
GRANT INSERT ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [power_user]
GRANT DELETE ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [power_user]
GRANT UPDATE ON  [dbo].[vClaimLawsuitIsFullyDocumented] TO [power_user]
GO
