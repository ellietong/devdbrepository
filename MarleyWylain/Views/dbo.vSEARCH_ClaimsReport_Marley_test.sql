SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vSEARCH_ClaimsReport_Marley_test]
AS 
with dedup_defjur as (
	select distinct dejur.DefenseCounselAddressID,dejur.JurisdictionID,dejur.StateID,LawsuitID
	 from dbo.TBLLAWSUIT(nolock) laws
    left join dbo.TBLDEFENSEJURISDICTION(nolock)  dejur  
	on dejur.StateID = laws.StateID
	and dejur.JurisdictionID = laws.JurisdictionID
	where dejur.DefenseCounselAddressID is not null
)	
, lawsuitinfo as (
   SELECT   claws.CLAIMLAWSUITID
           , ACTIVETRIALDATE
           , CLAIMID
           , claws.LAWSUITID
           , ISMDL
           , CLAIMLAWSUITSTATUS
           , cllawsttrak.CLAIMLAWSUITSTATUSID
           , DOCKETNUMBER
           , laws.SERVICEDATE
           , FILEDATE
           , laws.RECEIVEDDATE
           , st.[STATE]
           , st.STATEID
           , JURISDICTION
           , jurs.JURISDICTIONID
           , PROCESSEDDATE
           , STATUSDATE  
           , PLAINTIFFCOUNSEL
           , PLAINTIFFCOUNSELID          
           , ALLEGATIONTYPEID
			, CLIENTFILEDATE
			, CLAIMLAWSUITNOTES
			, DEFENSECOUNSEL          AS LawsuitDefenseCounsel
            , laws.DEFENSECOUNSELADDRESSID AS LawsuitDefenseCounselAddressID
            , DEFENSECOUNSELID        AS LawsuitDefenseCounselID		
    from 
	  dbo.TBLCLAIMLAWSUIT(nolock) claws
	  join TBLCLAIMLAWSUITSTATUSTRACKING(nolock) cllawsttrak
	  on cllawsttrak.ClaimLawsuitID = claws.ClaimLawsuitID
	  and isnull(ISPRIMARYFORCLAIM,0) = 1
	  and isnull(ISPRIMARYSTATUS,0) = 1	
	left join dbo.TBLCLAIMLAWSUITSTATUS(nolock) cllawstat
	on cllawstat.CLAIMLAWSUITSTATUSID = cllawsttrak.ClaimLawsuitStatusID -- lawsuitstatusinfo
	left join dbo.TBLALLEGATION(nolock) alleg  -- lawsuitstatusinfo
	on alleg.CLAIMLAWSUITID = cllawsttrak.CLAIMLAWSUITID		
	left join dbo.TBLLAWSUIT(nolock) laws -- LawsuitLevelInfo
	on laws.LawsuitID = claws.LawsuitID
	left join dbo.TBLSTATE(nolock) st
	on st.StateID = laws.StateID
	left join dbo.TBLJURISDICTION(nolock) jurs
	on jurs.JurisdictionID = laws.JurisdictionID
	left join dbo.TBLPLAINTIFFCOUNSELADDRESS(nolock) plntcnsladdr
	on plntcnsladdr.PlaintiffCounselAddressID = laws.PlaintiffCounselAddressID
	left join dbo.TBLDEFENSECOUNSELADDRESS(nolock) decnsladdr
	on decnsladdr.DefenseCounselAddressID = laws.DefenseCounselAddressID -- LawsuitLevelInfo	
	where 
	 isnull(ISPRIMARYFORCLAIM,0) = 1
	 and isnull(ISPRIMARYSTATUS,0) = 1	
)
, prodInfo as  ( 
 SELECT CLAIMANTEMPLOYMENTID AS prodInfoClaimentEmploymentId
       , MINSTARTDATE       AS EarliestStartDate
       , MINIMUMPRODUCTID
       , PRODUCTNOTES
	FROM   (SELECT CLAIMANTEMPLOYMENTID
               , MINSTARTDATE
               , MIN(PRODUCTID) AS MinimumProductID
        FROM   (SELECT CLAIMANTEMPLOYMENTID,PRODUCTID,MINSTARTDATE
                FROM   (SELECT CLAIMANTEMPLOYMENTID
                               , MIN(STARTDATE) AS MinStartDate
                        FROM   TBLCLAIMANTPRODUCT(nolock)
                        GROUP  BY CLAIMANTEMPLOYMENTID)MinimumStartDates
                       LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS sdEmploymentID
                                               , STARTDATE          AS sdStartDate
                                               , PRODUCTID
                                        FROM   TBLCLAIMANTPRODUCT(nolock))StartDatesAndProductIDs
                                    ON ( MinimumStartDates.CLAIMANTEMPLOYMENTID = StartDatesAndProductIDs.SDEMPLOYMENTID
                                         AND MinimumStartDates.MINSTARTDATE = StartDatesAndProductIDs.SDSTARTDATE ))MinStartDatesAndProductIDs
        GROUP  BY CLAIMANTEMPLOYMENTID, MINSTARTDATE)EarliestProductForEmployment
       LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS nEmploymentID
                               , STARTDATE          AS nStartDate
            , PRODUCTID          AS nProductID
            , PRODUCTNOTES
     FROM   TBLCLAIMANTPRODUCT(nolock))NotesQuery
 ON ( EarliestProductForEmployment.CLAIMANTEMPLOYMENTID = NotesQuery.NEMPLOYMENTID
      AND EarliestProductForEmployment.MINSTARTDATE = NotesQuery.NSTARTDATE
      AND EarliestProductForEmployment.MINIMUMPRODUCTID = NotesQuery.NPRODUCTID )	  
  ) 	
,ClaimLevelInfo as (
	SELECT  
                 claim.CLAIMID
               , MATTER
               , CLAIMSEQUENCE
               , claim.CLAIMANTPERSONALINFOID
               , FIRSTNAME
               , LASTNAME
               , BIRTHDATE
               , DECEASEDDATE
               , SocialSecurityNumber
               , OCCUPATION
               , HIGHLEVELDISEASE 
               , PRODUCTNOTES
               , STATEID
               , hldis.HIGHLEVELDISEASEID
               , claim.MATTERID
			   ,epemp.EarliestStartDate
from dbo.TBLCLAIM(nolock) claim -- ClaimLevelInfo
	--on claim.ClaimID = claws.ClaimID	
	left join dbo.TBLMATTER(nolock) mtr
	on mtr.MatterID = claim.MatterID
	left join dbo.TBLCLAIMSEQUENCE(nolock) cseq
	on cseq.ClaimSequenceID = claim.ClaimSequenceID
	left join dbo.TBLCLAIMDISEASE(nolock) cdis
	on cdis.ClaimID = claim.ClaimID
	and isnull(cdis.PRIMARYDISEASE,0) = 1
	left join dbo.TBLDISEASE dis
	on cdis.DiseaseID = dis.DiseaseID
	left join dbo.TBLHIGHLEVELDISEASE(nolock) hldis
	on hldis.HighLevelDiseaseID = dis.Highleveldiseaseid -- ClaimLevelInfo	
	left join dbo.TBLCLAIMANTPERSONALINFO(nolock) Clmnt --ClaimantLevelInfo
	on clmnt.ClaimantPersonalInfoID = claim.ClaimantPersonalInfoID
	left join dbo.TBLCLAIMANTEMPLOYMENT(nolock) clemp
	on clemp.ClaimantPersonalInfoID = clmnt.ClaimantPersonalInfoID
    and claim.CLAIMANTPERSONALINFOID = clemp.ClaimantPersonalInfoID 
	and ISPRIMARY = 1
	left join dbo.TBLOCCUPATION(nolock) occ
	on occ.OccupationID = clemp.OccupationID
	left join prodInfo epemp
	on clemp.CLAIMANTEMPLOYMENTID = epemp.PRODINFOCLAIMENTEMPLOYMENTID --ClaimantLevelInfo
)
, settlementPaymentInfo as (
SELECT CLAIMSETTLEMENTID AS SettlementPaymentInfoClaimSettlementID
	    , MIN(CHECKID)    AS EarliestCheckID
	FROM   dbo.TBLPAYMENTSETTLEMENT(nolock)
	    LEFT OUTER JOIN (SELECT CHECKID AS checkInfoCheckID
	                        FROM   dbo.TBLCHECK) checkInfo
	                    ON ( dbo.TBLPAYMENTSETTLEMENT.CHECKID = checkInfo.CHECKINFOCHECKID )
	GROUP  BY CLAIMSETTLEMENTID) 
	
, CliInfo as (
	SELECT claws.claimlawsuitid
	, activetrialdate
	, claimid
	, lawsuitid
	, ismdl
	, settlementdate
	, PAIDDATE
	, settlementamount
	, sourcedate
	, ClaimLawsuitNotes
	, ClaimLawsuitNotes AS TrialNotes	
	from dbo.TBLCLAIMLAWSUIT(nolock) claws -- CLInfo		
		left join dbo.TBLCLAIMSETTLEMENT(nolock) clsett
		on claws.CLAIMLAWSUITID = clsett.CLAIMLAWSUITID 	
		and isnull(ISPRIMARYFORCLAIM,0) = 1
		left join settlementPaymentInfo settinfo		
		on clsett.ClaimSettlementID = settinfo.SettlementPaymentInfoClaimSettlementID
		left join dbo.TBLCHECK(nolock) chk
		on settinfo.EarliestCheckID = chk.CheckID  -- CLInfo
  where  isnull(ISPRIMARYFORCLAIM,0) = 1
)


SELECT distinct
 cliInfo.CLAIMLAWSUITID                                      AS ClaimLawsuitID
       , CLAIMANTPERSONALINFOID                            AS ClaimantPersonalInfoID
       , cliInfo.ACTIVETRIALDATE                                   AS ActiveTrialDate
       , cliInfo.CLAIMID                                           AS ClaimID
       , CLAIMLAWSUITSTATUS                                AS ClaimLawsuitStatus
       , MATTER                                            AS Matter
       , CLAIMSEQUENCE                                     AS ClaimSequence
       , LASTNAME + ', ' + FIRSTNAME                       AS FullName
       , BIRTHDATE                                         AS BirthDate
       , DECEASEDDATE                                      AS DeceasedDate
       , ( CASE
             WHEN SocialSecurityNumber IS NOT NULL THEN 'XXX-XX-' + right(SocialSecurityNumber,4)
             ELSE NULL
           END )                                           SocialSecurityNumber
       , OCCUPATION                                        AS Occupation
       , HIGHLEVELDISEASE                                  AS HighLevelDisease
       , DOCKETNUMBER                                      AS DocketNumber
       , SERVICEDATE                                       AS ServiceDate
       , COALESCE(CLIENTFILEDATE, FILEDATE, RECEIVEDDATE)  AS FileDate
       , [STATE]                                           AS State
       , JURISDICTION                                      AS Jurisdiction
       , PROCESSEDDATE                                     AS ProcessedDate
       , STATUSDATE                                        AS StatusDate
       , PRODUCTNOTES                                      AS ProductNotes
       , EarliestStartDate                                 AS ProductExposureStart
       , SETTLEMENTDATE                                    AS SettlementDate
	   , PAIDDATE										   As SettlementPaidDate
       , SETTLEMENTAMOUNT                                  AS SettlementAmount
       , SOURCEDATE --AS SettlementPaidDate -- Date paid; source date is the source date of the first check that is cut for a settlement
       , PLAINTIFFCOUNSEL                                  AS PlaintiffCounsel
       , CLAIMLAWSUITSTATUSID                              AS ClaimLawsuitStatusID
       , lawsuit.STATEID                                   AS StateID
       , lawsuit.JURISDICTIONID                            AS JurisdictionID
       , cliinfo.ISMDL                                     AS IsMDL
       , HIGHLEVELDISEASEID                                AS HighLevelDiseaseID
       , ALLEGATIONTYPEID                                  AS AllegationTypeID
       , PLAINTIFFCOUNSELID                                AS PlaintiffCounselID
       , MATTERID                                          AS MatterID
       , LAWSUITDEFENSECOUNSELADDRESSID                    AS DefenseCounselAddressID
       , defjur.DefenseCounselAddressID                     AS DefenseCounselAddressID2
       , 0                                                 AS ClaimLawsuitStatusGroupID --NOTE: this is a dummy field because Marley doesn't require it but we still need the field. SK, 1/27/2014 (launch day)
       , CONVERT(INT, NULL)                                AS PersonID
       , CONVERT(INT, NULL)                                AS BrandID
       , CONVERT(NVARCHAR(50), NULL)                       AS Brand
       , CONVERT(INT, NULL)                                AS BankruptcyStayID
       , CONVERT(NVARCHAR(50), NULL)                       AS BankruptcyStay
       , CONVERT(INT, NULL)                                AS ClaimLawsuitTypeID
       , CONVERT(NVARCHAR(50), NULL)                       AS ClaimLawsuitType
       , CONVERT(NVARCHAR(100), NULL)                      AS JobsiteCity
       , CONVERT(INT, NULL)                                AS JobsiteStateID
       , CONVERT(NVARCHAR(1000), NULL)                     AS Jobsite
       , CONVERT(INT, NULL)                                AS BusinessCaseID
       , CONVERT(NVARCHAR(50), NULL)                       AS BusinessCase
	   , cliinfo.ClaimLawsuitNotes
	   , TrialNotes -- FYI TrialNotes is literally the same as ClaimLawsuitNotes; it is here so that GSX can refer to it this way in their CSV exports. 

	from cliInfo
	 join lawsuitinfo lawsuit
	on cliInfo.LAWSUITID = lawsuit.LAWSUITID
	and cliInfo.claimlawsuitid = lawsuit.CLAIMLAWSUITID
	and cliInfo.claimid = lawsuit.CLAIMID
	join ClaimLevelInfo claim
	on  cliInfo.claimid = claim.CLAIMID 
	left join dedup_defjur defjur
	on defjur.JurisdictionID = lawsuit.JURISDICTIONID
	and defjur.StateID = lawsuit.STATEID
	and defjur.LawsuitID = lawsuit.LawsuitID
--	where firstname is not null and lastname is not null

--GROUP  BY cliInfo.CLAIMLAWSUITID
--          , CLAIMANTPERSONALINFOID
--          , cliInfo.CLAIMID
--          , cliInfo.ISMDL
--          , cliInfo.ACTIVETRIALDATE
--          , CLAIMLAWSUITSTATUS
--          , MATTER
--          , CLAIMSEQUENCE
--          , FIRSTNAME
--          , LASTNAME
--          , BIRTHDATE
--          , DECEASEDDATE
--          , SSN4DIGIT
--          , OCCUPATION
--          , HIGHLEVELDISEASE
--          , lawsuit.ACTIVETRIALDATE
--          , DOCKETNUMBER
--		  , CLIENTFILEDATE
--          , SERVICEDATE
--          , FILEDATE
--          , RECEIVEDDATE
--          , [STATE]
--          , JURISDICTION
--          , PROCESSEDDATE
--          , STATUSDATE
--          , PRODUCTNOTES
--          , claim.EarliestStartDate 
--          , SETTLEMENTDATE
--		  , cliinfo.PAIDDATE
--          , SETTLEMENTAMOUNT
--          , SOURCEDATE
--          , PLAINTIFFCOUNSEL
--          , CLAIMLAWSUITSTATUSID
--          , lawsuit.STATEID
--          , lawsuit.JURISDICTIONID
--          , HIGHLEVELDISEASEID
--          , ALLEGATIONTYPEID
--          , PLAINTIFFCOUNSELID
--          , MATTERID
--          , lawsuit.LAWSUITDEFENSECOUNSEL
--          , lawsuit.LAWSUITDEFENSECOUNSELID
--          , lawsuit.LAWSUITDEFENSECOUNSELADDRESSID
--          , defjur.DefenseCounselAddressID    
--		  , cliinfo.ClaimLawsuitNotes
--		  , TrialNotes




GO
