SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vREPORT_PendingActiveClaimsByStateByDisease]
AS
	
SELECT 
ClaimLawsuitID,
[State],
CASE WHEN HighLevelDisease = 'Lung Cancer' THEN 1 ELSE 0 END AS LungCancerCount,
CASE WHEN HighLevelDisease = 'Mesothelioma' THEN 1 ELSE 0 END AS MesoCount,
CASE WHEN HighLevelDisease = 'Other Cancer' THEN 1 ELSE 0 END AS OtherCancerCount,
CASE WHEN HighLevelDisease = 'Non-Malignant' THEN 1 ELSE 0 END AS NonMaligCount,
CASE WHEN HighLevelDisease = 'Unknown' THEN 1 ELSE 0 END AS UnknownCount,
CASE WHEN HighLevelDisease = 'Silicosis' THEN 1 ELSE 0 END AS SilicosisCount,
1 AS TotalCount,
MatterID,
DefenseCounselAddressID,
DJDefenseCounselAddressID,
ClaimLawsuitTypeID
 FROM vREPORT_PendingActiveClaims
 






GO
