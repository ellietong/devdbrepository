SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vDETAILS_ClaimantPersonalInfo]
AS
SELECT DISTINCT 
                      cpi.ClaimantPersonalInfoID,
					   --cpi.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
        --              THEN '' ELSE MiddleName + ' ' END AS CombinedName, 
					  
					  cpi.FirstName, cpi.MiddleName, cpi.LastName, CASE WHEN SocialSecurityNumber IS NULL OR LEN(SocialSecurityNumber) = 0
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS MaskedSSN, g.Gender, COALESCE (cpi.GenderID, 4) AS GenderID, COALESCE (cpi.MaritalStatusID, 1) 
                      AS MaritalStatusID, ms.MaritalStatus, COALESCE (NULLIF (COALESCE (cpi.Address1 + CHAR(13) + CHAR(10), '') + COALESCE (cpi.Address2 + CHAR(13) + CHAR(10), '') 
                      + COALESCE (cpi.City + ', ', '') + COALESCE (s.State + ' ', '') + COALESCE (CONVERT(varchar, cpi.Zip), '') + COALESCE ('-' + CONVERT(varchar, cpi.MailZip), ''), ''), '') 
                      AS CompleteAddress, cpi.BirthDate, cpi.DeceasedDate, cpi.City, s.StateID, cpi.Zip, cpi.MailZip, cpi.Address1, cpi.Address2, cpi.IsSmoker, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS SocialSecurityNumber, s.State, lock.Locked, suf.Suffix, cpi.SuffixID,
					  cpi.MedicareBeneficiaryStatusID, MedicareBeneficiaryStatus, HealthInsuranceClaimNumber, cpi.NavInjPartyKey, cpi.NavInjuredPartyID, FullName,
					  l.DefenseCounselAddressID AS LawsuitDefenseCounselAddressID, dj.DefenseCounselAddressID AS JurisdictionDefenseCounselAddressID, CONVERT(int, NULL) AS IsDeceasedStatusID, CONVERT(nvarchar(50), NULL) AS IsDeceasedStatus
FROM         dbo.tblClaimantPersonalInfo AS cpi LEFT OUTER JOIN
                      dbo.tblGender AS g ON g.GenderID = COALESCE (cpi.GenderID, 4) LEFT OUTER JOIN
                      dbo.tblMaritalStatus AS ms ON ms.MaritalStatusID = COALESCE (cpi.MaritalStatusID, 1) LEFT OUTER JOIN
                      dbo.tblClaimantSmokingHistory AS sh ON sh.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblState AS s ON cpi.StateID = s.StateID LEFT OUTER JOIN
                      dbo.tblClaim AS c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID LEFT OUTER JOIN
                      dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID INNER JOIN
                      dbo.tblDefenseJurisdiction AS dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID INNER JOIN
                      dbo.vDETAILS_ClaimantLocked AS lock ON cpi.ClaimantPersonalInfoID = lock.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblSuffix AS suf ON cpi.SuffixID = suf.SuffixID
					  LEFT JOIN tblMedicareBeneficiaryStatus mbs ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID
GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantPersonalInfo] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "cpi"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 22
         End
         Begin Table = "g"
            Begin Extent = 
               Top = 6
               Left = 297
               Bottom = 84
               Right = 448
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ms"
            Begin Extent = 
               Top = 6
               Left = 486
               Bottom = 84
               Right = 640
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sh"
            Begin Extent = 
               Top = 6
               Left = 678
               Bottom = 114
               Right = 883
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 921
               Bottom = 99
               Right = 1072
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 84
               Left = 297
               Bottom = 192
               Right = 508
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 102
               Left = 921
               Bottom = 210
               Right = 1117
            End
            DisplayFlags = 280
            TopColumn = 0
         End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantPersonalInfo', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'         Begin Table = "l"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 250
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dj"
            Begin Extent = 
               Top = 114
               Left = 546
               Bottom = 222
               Right = 754
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "lock"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 300
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "suf"
            Begin Extent = 
               Top = 6
               Left = 1110
               Bottom = 84
               Right = 1261
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantPersonalInfo', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantPersonalInfo', NULL, NULL
GO
