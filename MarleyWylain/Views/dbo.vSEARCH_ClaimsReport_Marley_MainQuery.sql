SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vSEARCH_ClaimsReport_Marley_MainQuery]
AS 
/*=====================================================================================================================
Purpose:  Rewrite from the original vSEARCH_ClaimsReport to make the code easier to follow
Author:   Ellie Tong
Date:     02/02/2015
Example: SELECT
       CLAIMLAWSUITID			   
       ,ClaimantPersonalInfoID	    
       ,ActiveTrialDate			    
       ,ClaimID					    
       ,ClaimLawsuitStatus		    
       ,Matter					    
       ,ClaimSequence			         
       ,FullName				    
       ,BirthDate				    
       ,DeceasedDate			    
       ,SocialSecurityNumber                                       
       ,Occupation                                    
       ,HighLevelDisease      
	   from vSEARCH_ClaimsReport_Marley_test
	   where claimid = 1
=====================================================================================================================*/
with settlementPaymentInfo as (
SELECT CLAIMSETTLEMENTID AS SettlementPaymentInfoClaimSettlementID
        , MIN(CHECKID)    AS EarliestCheckID
 FROM   dbo.TBLPAYMENTSETTLEMENT
        LEFT OUTER JOIN (SELECT CHECKID AS checkInfoCheckID
                         FROM   dbo.TBLCHECK) checkInfo
                     ON ( dbo.TBLPAYMENTSETTLEMENT.CHECKID = checkInfo.CHECKINFOCHECKID )
 GROUP  BY CLAIMSETTLEMENTID) 
 , prodInfo as  ( 
 SELECT CLAIMANTEMPLOYMENTID AS prodInfoClaimentEmploymentId
       , MINSTARTDATE       AS EarliestStartDate
       , MINIMUMPRODUCTID
       , PRODUCTNOTES
	FROM   (SELECT CLAIMANTEMPLOYMENTID
               , MINSTARTDATE
               , MIN(PRODUCTID) AS MinimumProductID
        FROM   (SELECT *
                FROM   (SELECT CLAIMANTEMPLOYMENTID
                               , MIN(STARTDATE) AS MinStartDate
                        FROM   TBLCLAIMANTPRODUCT
                        GROUP  BY CLAIMANTEMPLOYMENTID)MinimumStartDates
                       LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS sdEmploymentID
                                               , STARTDATE          AS sdStartDate
                                               , PRODUCTID
                                        FROM   TBLCLAIMANTPRODUCT)StartDatesAndProductIDs
                                    ON ( MinimumStartDates.CLAIMANTEMPLOYMENTID = StartDatesAndProductIDs.SDEMPLOYMENTID
                                         AND MinimumStartDates.MINSTARTDATE = StartDatesAndProductIDs.SDSTARTDATE ))MinStartDatesAndProductIDs
        GROUP  BY CLAIMANTEMPLOYMENTID, MINSTARTDATE)EarliestProductForEmployment
       LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS nEmploymentID
                               , STARTDATE          AS nStartDate
            , PRODUCTID          AS nProductID
            , PRODUCTNOTES
     FROM   TBLCLAIMANTPRODUCT)NotesQuery
 ON ( EarliestProductForEmployment.CLAIMANTEMPLOYMENTID = NotesQuery.NEMPLOYMENTID
      AND EarliestProductForEmployment.MINSTARTDATE = NotesQuery.NSTARTDATE
      AND EarliestProductForEmployment.MINIMUMPRODUCTID = NotesQuery.NPRODUCTID )	  
  ) 
 ,dedupfnltbl as (
    SELECT  claws.CLAIMLAWSUITID
          , claim.CLAIMANTPERSONALINFOID
          , claws.CLAIMID
          , ISMDL
          , ACTIVETRIALDATE
          , CLAIMLAWSUITSTATUS
          , MATTER
          , CLAIMSEQUENCE
          , FIRSTNAME
          , LASTNAME
          , BIRTHDATE
          , DECEASEDDATE
          , SSN = RIGHT(SOCIALSECURITYNUMBER, 4)
          , OCCUPATION
          , HIGHLEVELDISEASE
          , DOCKETNUMBER
		  , CLIENTFILEDATE
          , laws.SERVICEDATE
          , FILEDATE
          , laws.RECEIVEDDATE
          , st.[STATE]
          , JURISDICTION
          , PROCESSEDDATE
          , STATUSDATE
          , PRODUCTNOTES
          , EarliestStartDate
          , SETTLEMENTDATE
		  , PaidDate
          , SETTLEMENTAMOUNT
          , SOURCEDATE
          , PLAINTIFFCOUNSEL
          , cllawsttrak.CLAIMLAWSUITSTATUSID
          , laws.STATEID
          , jurs.JURISDICTIONID
          , hldis.HIGHLEVELDISEASEID
          , ALLEGATIONTYPEID
          , PLAINTIFFCOUNSELID
          , claim.MATTERID
          , DEFENSECOUNSEL
          , DEFENSECOUNSELID
          , laws.DEFENSECOUNSELADDRESSID
         -- , DefenseCounselAddressID2 = dejur.DEFENSECOUNSELADDRESSID 
		  , ClaimLawsuitNotes
		  , TrialNotes = ClaimLawsuitNotes
		  --,ID = row_number() over (partition by claws.CLAIMLAWSUITID, claws.CLAIMID,claws.LawsuitID,cllawsttrak.CLAIMLAWSUITSTATUSID, hldis.HIGHLEVELDISEASEID
		  --,allegationtypeid, PLAINTIFFCOUNSELID,DEFENSECOUNSELID,laws.DEFENSECOUNSELADDRESSID, dejur.DEFENSECOUNSELADDRESSID
		  --order by claws.CLAIMLAWSUITID)		
		 
	from dbo.TBLCLAIMLAWSUIT claws -- CLInfo
	inner join dbo.TBLCLAIMLAWSUITSTATUSTRACKING cllawsttrak
	on cllawsttrak.ClaimLawsuitID = claws.ClaimLawsuitID
	and isnull(ISPRIMARYSTATUS,0) = 1	
	left join dbo.TBLCLAIMSETTLEMENT clsett
	on claws.CLAIMLAWSUITID = clsett.CLAIMLAWSUITID 	
	left join settlementPaymentInfo settinfo
	on clsett.ClaimSettlementID = settinfo.SettlementPaymentInfoClaimSettlementID
	left join dbo.TBLCHECK chk
	on settinfo.EarliestCheckID = chk.CheckID  -- CLInfo
		
	left join dbo.TBLALLEGATION alleg  -- lawsuitstatusinfo
	on alleg.CLAIMLAWSUITID = claws.CLAIMLAWSUITID
	left join dbo.TBLCLAIMLAWSUITSTATUS cllawstat
	on cllawstat.CLAIMLAWSUITSTATUSID = cllawsttrak.ClaimLawsuitStatusID -- lawsuitstatusinfo
	
	left join dbo.TBLLAWSUIT laws -- LawsuitLevelInfo
	on laws.LawsuitID = claws.LawsuitID
		
	left join dbo.TBLDEFENSEJURISDICTION dejur
	on dejur.StateID = laws.StateID
	and dejur.JurisdictionID = laws.JurisdictionID
	left join dbo.TBLSTATE st
	on st.StateID = laws.StateID
	left join dbo.TBLJURISDICTION jurs
	on jurs.JurisdictionID = laws.JurisdictionID
	left join dbo.TBLPLAINTIFFCOUNSELADDRESS plntcnsladdr
	on plntcnsladdr.PlaintiffCounselAddressID = laws.PlaintiffCounselAddressID
	left join dbo.TBLDEFENSECOUNSELADDRESS decnsladdr
	on decnsladdr.DefenseCounselAddressID = laws.DefenseCounselAddressID -- LawsuitLevelInfo
	
	left join dbo.TBLCLAIM claim -- ClaimLevelInfo
	on claim.ClaimID = claws.ClaimID	
	left join dbo.TBLMATTER mtr
	on mtr.MatterID = claim.MatterID
	left join dbo.TBLCLAIMSEQUENCE cseq
	on cseq.ClaimSequenceID = claim.ClaimSequenceID
	left join dbo.TBLCLAIMDISEASE cdis
	on cdis.ClaimID = claim.ClaimID
	and isnull(cdis.PRIMARYDISEASE,0) = 1
	left join dbo.TBLDISEASE dis
	on cdis.DiseaseID = dis.DiseaseID
	left join dbo.TBLHIGHLEVELDISEASE hldis
	on hldis.HighLevelDiseaseID = dis.Highleveldiseaseid -- ClaimLevelInfo
	
	left join dbo.TBLCLAIMANTPERSONALINFO Clmnt --ClaimantLevelInfo
	on clmnt.ClaimantPersonalInfoID = claim.ClaimantPersonalInfoID
	left join dbo.TBLCLAIMANTEMPLOYMENT clemp
	on clemp.ClaimantPersonalInfoID = clmnt.ClaimantPersonalInfoID
    and claim.CLAIMANTPERSONALINFOID = clemp.ClaimantPersonalInfoID 
	and ISPRIMARY = 1
	left join dbo.TBLOCCUPATION occ
	on occ.OccupationID = clemp.OccupationID
	left join prodInfo epemp
	on clemp.CLAIMANTEMPLOYMENTID = epemp.PRODINFOCLAIMENTEMPLOYMENTID --ClaimantLevelInfo

	where 
	 isnull(ISPRIMARYFORCLAIM,0) = 1
	 and isnull(ISPRIMARYSTATUS,0) = 1	
	)
	 SELECT
        CLAIMLAWSUITID			=CLAIMLAWSUITID                                    
       ,ClaimantPersonalInfoID	= CLAIMANTPERSONALINFOID                            
       ,ActiveTrialDate			= ACTIVETRIALDATE                                   
       ,ClaimID					= CLAIMID                                           
       ,ClaimLawsuitStatus		= CLAIMLAWSUITSTATUS                                
       ,Matter					= MATTER                                            
       ,ClaimSequence			= CLAIMSEQUENCE                                          
       ,FullName				= LASTNAME + ', ' + FIRSTNAME                       
       ,BirthDate				= BIRTHDATE                                         
       ,DeceasedDate			= DECEASEDDATE                                      
       ,SocialSecurityNumber    = ( CASE
								 WHEN SSN IS NOT NULL THEN 'XXX-XX-' + SSN
								 ELSE NULL
							     END )                                           
       ,Occupation = OCCUPATION                                         
       ,HighLevelDisease = HIGHLEVELDISEASE                                   
       ,DocketNumber = DOCKETNUMBER                                       
       ,ServiceDate = SERVICEDATE                                        
       ,FileDate = COALESCE(CLIENTFILEDATE, FILEDATE, RECEIVEDDATE)   
       ,State = [STATE]                                            
       ,Jurisdiction = JURISDICTION                                       
       ,ProcessedDate = PROCESSEDDATE                                      
       ,StatusDate = STATUSDATE                                         
       ,ProductNotes = PRODUCTNOTES                                       
       ,ProductExposureStart = EarliestStartDate                                          
       ,SettlementDate = SETTLEMENTDATE                                     
	   ,SettlementPaidDate = PaidDate								  
       ,SettlementAmount = SETTLEMENTAMOUNT                                   
       ,SOURCEDATE --AS SettlementPaidDate -- Date paid; --srce date is the source date of the first check that -- is cut for a settlement
       ,PlaintiffCounsel = PLAINTIFFCOUNSEL                                   
       ,ClaimLawsuitStatusID = CLAIMLAWSUITSTATUSID                               
       ,StateID = STATEID                                            
       ,JurisdictionID = JURISDICTIONID                                     
       ,IsMDL = ISMDL                                              
       ,HighLevelDiseaseID = HIGHLEVELDISEASEID                                 
       ,AllegationTypeID = ALLEGATIONTYPEID                                   
       ,PlaintiffCounselID = PLAINTIFFCOUNSELID                                 
       ,MatterID = MATTERID                                           
       ,DefenseCounselAddressID                    
       ,DefenseCounselAddressID2 = DEFENSECOUNSELADDRESSID  
       ,ClaimLawsuitStatusGroupID  = 0                                                  --NOTE: this is a dummy field because Marley doesn't require it but we still need the field. SK, 1/27/2014 (launch day)
       ,PersonID = CONVERT(INT, NULL)                                 
       ,BrandID = CONVERT(INT, NULL)                                 
       ,Brand = CONVERT(NVARCHAR(50), NULL)                        
       ,BankruptcyStayID = CONVERT(INT, NULL)                                 
       ,BankruptcyStay = CONVERT(NVARCHAR(50), NULL)                        
       ,ClaimLawsuitTypeID = CONVERT(INT, NULL)                                 
       ,ClaimLawsuitType = CONVERT(NVARCHAR(50), NULL)                        
       ,JobsiteCity = CONVERT(NVARCHAR(100), NULL)                       
       ,JobsiteStateID = CONVERT(INT, NULL)                                 
       ,Jobsite = CONVERT(NVARCHAR(1000), NULL)                      
       ,BusinessCaseID = CONVERT(INT, NULL)                                 
       ,BusinessCase = CONVERT(NVARCHAR(50), NULL)                        
	   ,ClaimLawsuitNotes
	   ,TrialNotes = ClaimLawsuitNotes	  
	 --  ,ID 
	   from dedupfnltbl
	--   where ID = 1


GO
