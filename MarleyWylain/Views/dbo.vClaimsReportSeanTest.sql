SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [dbo].[vClaimsReportSeanTest] As 

SELECT 
claimlawsuitid
       , ClaimantPersonalInfoID
       , ActiveTrialDate
        , ClaimID
       , IsMDL
       , ClaimLawsuitStatus
       , Matter
       , ClaimSequence
       , FirstName
       , LastName
       , BirthDate
       , DeceasedDate
       , ( CASE
             WHEN ssn4digit IS NOT NULL THEN 'XXX-XX-' + ssn4digit
             ELSE NULL
           END ) SocialSecurityNumber
       , Occupation
       , HighLevelDisease
		, DocketNumber
		, ServiceDate
		, Coalesce(FileDate, ReceivedDate) As FileDate
		, [State]
		, Jurisdiction
		, ProcessedDate
		, StatusDate
		, ProductNotes
		, StartDate
		, SettlementDate
		, SettlementAmount
		, SourceDate -- Date paid; source date is the source date of the first check that is cut for a settlement
		, PlaintiffCounsel

FROM   (SELECT claimlawsuitid
               , activetrialdate
               , claimid
               , lawsuitid
               , ismdl
               , claimlawsuitstatus
               , matter
               , claimsequence
               , claimantpersonalinfoid
               , firstname
               , lastname
               , birthdate
               , deceaseddate
               , ssn4digit
               , occupation
               , highleveldisease
				, DocketNumber
				, ServiceDate
				 , FileDate
				 , ReceivedDate
				, [State]
				, Jurisdiction
				, ProcessedDate
				, StatusDate
				, ProductNotes
				, StartDate
				, SettlementDate
				, SettlementAmount
				, SourceDate
				, PlaintiffCounsel
        FROM   (SELECT claimlawsuitid
                       , activetrialdate
                       , claimid
                       , lawsuitid
                       , ismdl
                       , claimlawsuitstatus
					   , DocketNumber
					   , ServiceDate
					   , FileDate
					   , ReceivedDate
					   , [State]
					   , Jurisdiction
					   , ProcessedDate
					   , StatusDate
						, SettlementDate
						, SettlementAmount
						, SourceDate
						, PlaintiffCounsel
                FROM   (SELECT claimlawsuitid
                               , activetrialdate
                               , claimid
                               , lawsuitid
                               , ismdl
							   , SettlementDate
							   , SettlementAmount
							   , SourceDate
                        FROM   tblclaimlawsuit
						left outer join (select ClaimLawsuitID as settlmentInfoClaimLawsuitid, SettlementDate, SettlementAmount, SourceDate from tblClaimSettlement
										left outer join (select ClaimSettlementID, min(checkID) As EarliestCheckID, SourceDate from tblPaymentSettlement
															left outer join (select CheckID as checkInfoCheckID, SourceDate from tblCheck) checkInfo
															on (tblPaymentSettlement.CheckID = checkInfo.checkInfoCheckID)
group by ClaimSettlementID, SourceDate) settlementPaymentInfo on (tblClaimSettlement.ClaimSettlementID = settlementPaymentInfo.ClaimSettlementID)
						)settlementInfo
						on (tblclaimlawsuit.claimlawsuitid = settlementinfo.settlmentInfoClaimLawsuitid)
                        WHERE  isprimaryforclaim = 1) CLInfo

                       LEFT OUTER JOIN (SELECT trackingInfo.claimlawsuitstatusid
                                               , trackingInfo.claimlawsuitid AS ClaimLawsuitID_fromTrackingInfo
                                               , statusLabelnfo.claimlawsuitstatus, ProcessedDate, StatusDate
                                        FROM   (SELECT ClaimLawsuitStatusID, ClaimLawsuitID, ProcessedDate, StatusDate
                                                FROM   tblclaimlawsuitstatustracking
                                                WHERE  isprimarystatus = 1) trackingInfo
                                               LEFT OUTER JOIN (SELECT claimlawsuitstatusid
                                                                       , claimlawsuitstatus
                                                                FROM   tblclaimlawsuitstatus)statusLabelnfo
                                                            ON ( trackingInfo.claimlawsuitstatusid = statusLabelnfo.claimlawsuitstatusid )) lawsuitstatusinfo
                                    ON ( CLInfo.claimlawsuitid = lawsuitstatusinfo.claimlawsuitid_fromtrackinginfo )
						Left outer join (select LawsuitID as lawsuitinfolawsuitid, DocketNumber, ServiceDate, FileDate, ReceivedDate, [State], Jurisdiction, PlaintiffCounsel from tblLawsuit
											left outer join (select stateid as stateInfoStateID, [state] from tblState)stateInfo
											on(tblLawsuit.StateID = stateInfo.stateInfoStateID)
											left outer join (select jurisdictionid as jurisdictioninfoInfojurisdictionID, Jurisdiction from tblJurisdiction)jurisdictionInfo
											on(tblLawsuit.JurisdictionID = jurisdictionInfo.jurisdictioninfoInfojurisdictionID)
											left outer join (select PlaintiffCounselAddressID as plaintiffCounselInfoAddressId, PlaintiffCounsel from tblPlaintiffCounselAddress) plaintiffCounselInfo
											on (tblLawsuit.PlaintiffCounselAddressID = plaintiffCounselInfo.plaintiffCounselInfoAddressId)

						)lawsuitInfo 
						on (CLInfo.LawsuitID = lawsuitInfo.lawsuitinfolawsuitid)
									)LawsuitLevelInfo
               LEFT OUTER JOIN (SELECT claimid_fromtblclaim     AS claimID_fromtblClaim
                                       , claimantpersonalinfoid AS claimantpersonalinfoid
                                       , matter
                                       , claimsequence
                                       , highleveldisease
                                FROM   (SELECT claimid AS claimID_fromtblClaim
                                               , claimantpersonalinfoid
                                               , matterid
                                               , claimsequenceid
                                        FROM   tblclaim)ClaimInfo
                                       LEFT OUTER JOIN (SELECT matterid AS matterID_fromtblMatter
                                                               , matter
                                                        FROM   tblmatter) matterInfo
                                                    ON ( ClaimInfo.matterid = matterInfo.matterid_fromtblmatter )
                                       LEFT OUTER JOIN (SELECT claimsequenceid
                                                               , claimsequence
                                                        FROM   tblclaimsequence) ClaimSequenceInfo
                                                    ON ( ClaimInfo.claimsequenceid = ClaimSequenceInfo.claimsequenceid )
                                       LEFT OUTER JOIN (SELECT claimid
                                                               , highleveldisease
                                                        FROM   (SELECT claimid
                                                                       , diseaseid
                                                                       , primarydisease
                                                                FROM   tblclaimdisease) theClaimDiseaseInfo
                                                               LEFT OUTER JOIN (SELECT diseaseid AS diseaseInfodiseaseid
                                                                                       , highleveldiseaseid
                                                                                FROM   tbldisease) diseaseInfo
                                                                            ON( theClaimDiseaseInfo.diseaseid = diseaseInfo.diseaseinfodiseaseid )
                                                               LEFT OUTER JOIN (SELECT highleveldiseaseid AS hldiseaseinfohldiseaseid
                                                                                       , highleveldisease
                                                                                FROM   tblhighleveldisease) hlDiseaseInfo
                                                                            ON( diseaseInfo.highleveldiseaseid = hlDiseaseInfo.hldiseaseinfohldiseaseid )
                                                        WHERE  theClaimDiseaseInfo.primarydisease = 1) ClaimDiseaseInfo
                                                    ON( ClaimInfo.claimid_fromtblclaim = ClaimDiseaseInfo.claimid ))ClaimLevelInfo
                            ON ( LawsuitLevelInfo.claimid = ClaimLevelInfo.claimid_fromtblclaim )
               LEFT OUTER JOIN(SELECT claimid                            AS claimID_forclaimantinfo
                                      , ClaimInfo.claimantpersonalinfoid AS personalinfoid
                                      , firstname
                                      , lastname
                                      , birthdate
                                      , deceaseddate
                                      , ssn4digit
                                      , occupation
									  , productNotes
									  , StartDate
                               FROM   (SELECT claimid
                                              , claimantpersonalinfoid
                                       FROM   tblclaim) ClaimInfo
                                      LEFT OUTER JOIN (SELECT claimantpersonalinfoid
                                                              , firstname
                                                              , lastname
                                                              , birthdate
                                                              , deceaseddate
                                                              , RIGHT(socialsecuritynumber, 4) AS SSN4Digit
                                                       FROM   tblclaimantpersonalinfo) ClaimantInfo
                                                   ON ( ClaimInfo.claimantpersonalinfoid = ClaimantInfo.claimantpersonalinfoid )
                                      LEFT OUTER JOIN (SELECT employmentpersonalinfoid
                                                              , occupation
															  , ProductNotes
															  , StartDate
                                                       FROM   (SELECT claimantpersonalinfoid AS employmentPersonalInfoId
                                                                      , occupationid
																	  , ClaimantEmploymentID
                                                               FROM   tblclaimantemployment
                                                               WHERE  isprimary = 1) empInfo
                                                              LEFT OUTER JOIN (SELECT occupationid
                                                                                      , occupation
                                                                               FROM   tbloccupation) occInfo
                                                                           ON ( empInfo.occupationid = occInfo.occupationid )
                                                              LEFT OUTER JOIN (SELECT ClaimantEmploymentID as prodInfoClaimentEmploymentId, ProductID, ProductNotes, StartDate
                                                                               FROM   tblClaimantProduct) prodInfo
                                                                           ON ( empInfo.ClaimantEmploymentID = prodInfo.prodInfoClaimentEmploymentId)

																		   
																		   ) claimantEmployment
                                                   ON ( ClaimantInfo.claimantpersonalinfoid = claimantEmployment.employmentpersonalinfoid )) ClaimantLevelInfo
                            ON( LawsuitLevelInfo.claimid = ClaimantLevelInfo.claimid_forclaimantinfo ))MainQuery
						
GROUP  BY claimlawsuitid
          , claimantpersonalinfoid
          , claimid
          , ismdl
          , activetrialdate
          , claimlawsuitstatus
          , matter
          , claimsequence
          , firstname
          , lastname
          , birthdate
          , deceaseddate
          , ssn4digit
          , occupation
          , highleveldisease 
		  , ActiveTrialDate
		  , DocketNumber
		  , ServiceDate
		  , FileDate
		  , ReceivedDate
		  , [State]
		  , Jurisdiction
		  , ProcessedDate
		  , StatusDate
		  , ProductNotes
		  , StartDate
		  , SettlementDate
		  , SettlementAmount
		  , SourceDate
		  , PlaintiffCounsel
GO
GRANT VIEW DEFINITION ON  [dbo].[vClaimsReportSeanTest] TO [base_user]
GRANT SELECT ON  [dbo].[vClaimsReportSeanTest] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vClaimsReportSeanTest] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vClaimsReportSeanTest] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vClaimsReportSeanTest] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vClaimsReportSeanTest] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vClaimsReportSeanTest] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vClaimsReportSeanTest] TO [power_user]
GRANT SELECT ON  [dbo].[vClaimsReportSeanTest] TO [power_user]
GRANT INSERT ON  [dbo].[vClaimsReportSeanTest] TO [power_user]
GRANT DELETE ON  [dbo].[vClaimsReportSeanTest] TO [power_user]
GRANT UPDATE ON  [dbo].[vClaimsReportSeanTest] TO [power_user]
GO
