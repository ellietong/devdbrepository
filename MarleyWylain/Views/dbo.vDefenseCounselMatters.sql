SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vDefenseCounselMatters]
AS
SELECT DefenseCOunselAddressID, PersonID, MatterID FROM tblDefenseJurisdiction
GROUP BY DefenseCounselAddressID, PersonID, MatterID

GO
GRANT VIEW DEFINITION ON  [dbo].[vDefenseCounselMatters] TO [base_user]
GRANT SELECT ON  [dbo].[vDefenseCounselMatters] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDefenseCounselMatters] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDefenseCounselMatters] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDefenseCounselMatters] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDefenseCounselMatters] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDefenseCounselMatters] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDefenseCounselMatters] TO [power_user]
GRANT SELECT ON  [dbo].[vDefenseCounselMatters] TO [power_user]
GRANT INSERT ON  [dbo].[vDefenseCounselMatters] TO [power_user]
GRANT DELETE ON  [dbo].[vDefenseCounselMatters] TO [power_user]
GRANT UPDATE ON  [dbo].[vDefenseCounselMatters] TO [power_user]
GO
