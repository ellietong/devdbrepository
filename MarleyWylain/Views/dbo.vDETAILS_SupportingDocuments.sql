SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*AND (dg.SettlementGroupID NOT IN (2, 4))*/
CREATE VIEW [dbo].[vDETAILS_SupportingDocuments]
AS
SELECT     d.DocumentURL + CASE WHEN ISNUMERIC(PageNums) = 1 AND CHARINDEX(',', PageNums) = 0 THEN '#page=' + PageNums WHEN CHARINDEX(',', PageNums) 
                      - 1 > 0 THEN '#page=' + LEFT(PageNums, CHARINDEX(',', PageNums) - 1) WHEN CHARINDEX('-', PageNums) - 1 > 0 THEN '#page=' + LEFT(PageNums, CHARINDEX('-', 
                      PageNums) - 1) ELSE '' END AS DocumentURL, d.DocumentTitle, sd.PageNums, dc.DocCategory AS SupportingDocumentType, sd.SettleDocID, sd.ClaimLawsuitID, 
                      sd.DocumentID, sd.DocCategoryID, dc.DocCategory, AmazonKey
FROM         dbo.tblClaimSettlementDocuments AS sd INNER JOIN
                      dbo.tblDocuments AS d ON sd.DocumentID = d.DocumentID INNER JOIN
                      dbo.tblClaimSettlementDocumentCategory AS dc ON sd.DocCategoryID = dc.DocCategoryID INNER JOIN
                      dbo.tblClaimantDocumentType AS dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
WHERE     (d.ClaimantDocumentTypeID <> 40)



GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SupportingDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_SupportingDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SupportingDocuments] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_SupportingDocuments] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_SupportingDocuments] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_SupportingDocuments] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_SupportingDocuments] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SupportingDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_SupportingDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_SupportingDocuments] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_SupportingDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_SupportingDocuments] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sd"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 247
               Bottom = 135
               Right = 477
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dc"
            Begin Extent = 
               Top = 6
               Left = 515
               Bottom = 101
               Right = 685
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dt"
            Begin Extent = 
               Top = 6
               Left = 723
               Bottom = 102
               Right = 953
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or ', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SupportingDocuments', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'= 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SupportingDocuments', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SupportingDocuments', NULL, NULL
GO
