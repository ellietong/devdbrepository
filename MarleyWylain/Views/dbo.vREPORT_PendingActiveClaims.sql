
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vREPORT_PendingActiveClaims]
AS
	SELECT distinct
	cl.ClaimLawsuitID,
	pca.PlaintiffCounsel, 
	s.FullNameState AS State,
	HighLevelDisease,
	m.Matter,
	c.MatterID,
	cl.DefenseCounselAddressID,
	dj.DefenseCounselAddressID AS DJDefenseCounselAddressID,
	ClaimLawsuitTypeID
	from tblClaim c
	inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
	inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
	inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
	inner join tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
	INNER JOIN tblState s ON l.StateID = s.StateID
	INNER JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimID
	INNER JOIN tblDisease d ON cd.DiseaseID = d.DiseaseID
	INNER JOIN tblHighLevelDisease hld ON d.HighLevelDiseaseID = hld.HighLevelDiseaseID
	INNER JOIN tblMatter m ON c.MatterID = m.MatterID
	INNER JOIN		tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.MatterID
	where	ClaimLawsuitStatusID in (22) 
			and  IsPrimaryStatus = 1 
			--and IsPrimaryForClaim = 1
			and PrimaryDisease = 1
GO
