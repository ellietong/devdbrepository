SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vREPORT_DISEASE_SettledClaims]
AS

SELECT 
ClaimLawsuitID,
COALESCE(PlaintiffCounsel, 'UNKNOWN') AS PlaintiffCounsel,
CASE WHEN HighLevelDisease = 'Lung Cancer' THEN 1 ELSE 0 END AS LungCancerCount,
CASE WHEN HighLevelDisease = 'Mesothelioma' THEN 1 ELSE 0 END AS MesoCount,
CASE WHEN HighLevelDisease = 'Other Cancer' THEN 1 ELSE 0 END AS OtherCancerCount,
CASE WHEN HighLevelDisease = 'Non-Malignant' THEN 1 ELSE 0 END AS NonMaligCount,
CASE WHEN HighLevelDisease = 'Unknown' THEN 1 ELSE 0 END AS UnknownCount,
1 AS TotalCount,

CASE WHEN HighLevelDisease = 'Lung Cancer' THEN IndemnityPaid ELSE 0 END AS LungCancerIndemnity,
CASE WHEN HighLevelDisease = 'Mesothelioma' THEN IndemnityPaid ELSE 0 END AS MesoIndemnity,
CASE WHEN HighLevelDisease = 'Other Cancer' THEN IndemnityPaid ELSE 0 END AS OtherCancerIndemnity,
CASE WHEN HighLevelDisease = 'Non-Malignant' THEN IndemnityPaid ELSE 0 END AS NonMaligIndemnity,
CASE WHEN HighLevelDisease = 'Unknown' THEN IndemnityPaid ELSE 0 END AS UnknownIndemnity,
IndemnityPaid AS TotalIndemnity,

CASE WHEN HighLevelDisease = 'Lung Cancer' THEN 1 ELSE 0 END AS LungCancerAvg,
CASE WHEN HighLevelDisease = 'Mesothelioma' THEN 1 ELSE 0 END AS MesoAvg,
CASE WHEN HighLevelDisease = 'Other Cancer' THEN 1 ELSE 0 END AS OtherCancerAvg,
CASE WHEN HighLevelDisease = 'Non-Malignant' THEN 1 ELSE 0 END AS NonMaligAvg,
CASE WHEN HighLevelDisease = 'Unknown' THEN 1 ELSE 0 END AS UnknownAvg,
IndemnityPaid AS TotalAvg,

StatusDate,
MatterID,
DefenseCounselAddressID,
DJDefenseCounselAddressID,
ClaimLawsuitTypeID
 FROM vREPORT_Settlements
 




GO
