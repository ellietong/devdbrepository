SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vREPORT_SETTLEMENT_MissingSettlementDocumentation]
AS
SELECT DISTINCT 
                      cpi.ClaimantPersonalInfoID,
					  cpi.FirstName,
					  cpi.LastName,
					  cpi.MiddleName,
					  cpi.FullName AS ClaimantName,
					  cpi.FullName,
					  cpi.BirthDate, 
					  cpi.DeceasedDate, 
					  cs.SettlementDate, 
					  cs.SettlementAmount, 
					  csg.SettlementGroup, 
					  cs.SettlementGroupID, 
					  cl.ClaimLawsuitID, 
					  cl.LawsuitID, 
					  cl.DefenseCounselAddressID,
					  dj.DefenseCounselAddressID AS DJDefenseCounselAddressID,
					  MissingClaimantInfo,
					  MissingDocumentation,
					  c.MatterID,
					  DocCategoryID,
					  ClaimantInfoCategoryID
FROM	tblClaimLawsuit cl
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblClaimSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking cls ON cs.ClaimLawsuitID = cls.ClaimLawsuitID
		LEFT JOIN tblClaimSettlementGroup csg ON cs.SettlementGroupID = csg.SettlementGroupID
		LEFT JOIN vREPORT_SETTLEMENT_MissingClaimantInformation mci ON  c.ClaimantPersonalInfoID = mci.ClaimantPersonalInfoID
		LEFT JOIN vREPORT_SETTLEMENT_MissingClaimDocumentation msd ON cl.ClaimLawsuitID = msd.ClaimLawsuitID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.MatterID
WHERE (MissingClaimantInfo IS NOT NULL OR MissingDocumentation IS NOT NULL)
AND (cls.IsPrimaryStatus = 1) AND (cls.ClaimLawsuitStatusID IN (25))








GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SETTLEMENT_MissingSettlementDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_SETTLEMENT_MissingSettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SETTLEMENT_MissingSettlementDocumentation] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_SETTLEMENT_MissingSettlementDocumentation] TO [power_user]
GO
