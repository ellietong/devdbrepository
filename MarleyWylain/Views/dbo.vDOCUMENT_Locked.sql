SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vDOCUMENT_Locked]
AS

SELECT mcd.DocumentID, COALESCE(COUNT(clst.ClaimLawsuitStatusTrackingID), 0) AS Locked 
   FROM tblMultiClaimantDocument mcd
   INNER JOIN tblClaim c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l							ON cl.LawsuitID	 = l.LawsuitID
	--INNER JOIN tblMultiClaimantDocument mcd			ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE  ClaimLawsuitStatusID IN (15,16,17))clst	ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
WHERE COALESCE(c.IsCurrentClaim,0) = 1 
	AND COALESCE(IsPrimaryForClaim, 0) = 1 
	
GROUP BY mcd.DocumentID
--HAVING COUNT(c.ClaimID)> 0
UNION
--Check for settlements
SELECT mcd.DocumentID, COALESCE(COUNT(DISTINCT paymentSettlementID), 0)
FROM tblMultiClaimantDocument mcd 
	LEFT JOIN  tblClaim c					ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN  tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
	LEFT JOIN  tblClaimSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	LEFT JOIN tblPaymentSettlement ps ON cs.ClaimSettlementID = ps.ClaimSettlementID
GROUP BY mcd.DocumentID
	--HAVING COUNT(c.ClaimID) > 0



GO
GRANT VIEW DEFINITION ON  [dbo].[vDOCUMENT_Locked] TO [base_user]
GRANT SELECT ON  [dbo].[vDOCUMENT_Locked] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDOCUMENT_Locked] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDOCUMENT_Locked] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDOCUMENT_Locked] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDOCUMENT_Locked] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDOCUMENT_Locked] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDOCUMENT_Locked] TO [power_user]
GRANT SELECT ON  [dbo].[vDOCUMENT_Locked] TO [power_user]
GRANT INSERT ON  [dbo].[vDOCUMENT_Locked] TO [power_user]
GRANT DELETE ON  [dbo].[vDOCUMENT_Locked] TO [power_user]
GRANT UPDATE ON  [dbo].[vDOCUMENT_Locked] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDOCUMENT_Locked', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDOCUMENT_Locked', NULL, NULL
GO
