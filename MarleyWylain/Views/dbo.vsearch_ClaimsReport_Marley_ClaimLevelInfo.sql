SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view  [dbo].[vsearch_ClaimsReport_Marley_ClaimLevelInfo]
as
with prodInfo as  ( 
 SELECT CLAIMANTEMPLOYMENTID AS prodInfoClaimentEmploymentId
       , MINSTARTDATE       AS EarliestStartDate
       , MINIMUMPRODUCTID
       , PRODUCTNOTES
	FROM   (SELECT CLAIMANTEMPLOYMENTID
               , MINSTARTDATE
               , MIN(PRODUCTID) AS MinimumProductID
        FROM   (SELECT *
                FROM   (SELECT CLAIMANTEMPLOYMENTID
                               , MIN(STARTDATE) AS MinStartDate
                        FROM   TBLCLAIMANTPRODUCT
                        GROUP  BY CLAIMANTEMPLOYMENTID)MinimumStartDates
                       LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS sdEmploymentID
                                               , STARTDATE          AS sdStartDate
                                               , PRODUCTID
                                        FROM   TBLCLAIMANTPRODUCT)StartDatesAndProductIDs
                                    ON ( MinimumStartDates.CLAIMANTEMPLOYMENTID = StartDatesAndProductIDs.SDEMPLOYMENTID
                                         AND MinimumStartDates.MINSTARTDATE = StartDatesAndProductIDs.SDSTARTDATE ))MinStartDatesAndProductIDs
        GROUP  BY CLAIMANTEMPLOYMENTID, MINSTARTDATE)EarliestProductForEmployment
       LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS nEmploymentID
                               , STARTDATE          AS nStartDate
            , PRODUCTID          AS nProductID
            , PRODUCTNOTES
     FROM   TBLCLAIMANTPRODUCT)NotesQuery
 ON ( EarliestProductForEmployment.CLAIMANTEMPLOYMENTID = NotesQuery.NEMPLOYMENTID
      AND EarliestProductForEmployment.MINSTARTDATE = NotesQuery.NSTARTDATE
      AND EarliestProductForEmployment.MINIMUMPRODUCTID = NotesQuery.NPRODUCTID )	  
  ) 

	
	SELECT  
                 claim.CLAIMID
               , MATTER
               , CLAIMSEQUENCE
               , claim.CLAIMANTPERSONALINFOID
               , FIRSTNAME
               , LASTNAME
               , BIRTHDATE
               , DECEASEDDATE
               , SSN4DIGIT = SocialSecurityNumber
               , OCCUPATION
               , HIGHLEVELDISEASE 
               , PRODUCTNOTES
               , STATEID
               , hldis.HIGHLEVELDISEASEID
               , claim.MATTERID
			   ,epemp.EarliestStartDate


from dbo.TBLCLAIM claim -- ClaimLevelInfo
	--on claim.ClaimID = claws.ClaimID	
	left join dbo.TBLMATTER mtr
	on mtr.MatterID = claim.MatterID
	left join dbo.TBLCLAIMSEQUENCE cseq
	on cseq.ClaimSequenceID = claim.ClaimSequenceID
	left join dbo.TBLCLAIMDISEASE cdis
	on cdis.ClaimID = claim.ClaimID
	and isnull(cdis.PRIMARYDISEASE,0) = 1
	left join dbo.TBLDISEASE dis
	on cdis.DiseaseID = dis.DiseaseID
	left join dbo.TBLHIGHLEVELDISEASE hldis
	on hldis.HighLevelDiseaseID = dis.Highleveldiseaseid -- ClaimLevelInfo
	
	left join dbo.TBLCLAIMANTPERSONALINFO Clmnt --ClaimantLevelInfo
	on clmnt.ClaimantPersonalInfoID = claim.ClaimantPersonalInfoID
	left join dbo.TBLCLAIMANTEMPLOYMENT clemp
	on clemp.ClaimantPersonalInfoID = clmnt.ClaimantPersonalInfoID
    and claim.CLAIMANTPERSONALINFOID = clemp.ClaimantPersonalInfoID 
	and ISPRIMARY = 1
	left join dbo.TBLOCCUPATION occ
	on occ.OccupationID = clemp.OccupationID
	left join prodInfo epemp
	on clemp.CLAIMANTEMPLOYMENTID = epemp.PRODINFOCLAIMENTEMPLOYMENTID --ClaimantLevelInfo

	group by 
				claim.CLAIMID
               , MATTER
               , CLAIMSEQUENCE
               , claim.CLAIMANTPERSONALINFOID
               , FIRSTNAME
               , LASTNAME
               , BIRTHDATE
               , DECEASEDDATE
               , SocialSecurityNumber
               , OCCUPATION
               , HIGHLEVELDISEASE 
               , PRODUCTNOTES
               , STATEID
               , hldis.HIGHLEVELDISEASEID
               , claim.MATTERID
               ,epemp.EarliestStartDate
GO
