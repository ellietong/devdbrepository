SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDETAILS_ClaimLawsuitBankruptcyTrust]
AS

SELECT ClaimLawsuitBankruptcyTrustID,
	ClaimLawsuitID,
	clbt.BankruptcyTrustID,
	BankruptcyTrust,
	ClaimLawsuitBankruptcyTrustNotes
FROM dbo.tblClaimLawsuitBankruptcyTrust clbt
	INNER JOIN dbo.tblBankruptcyTrust bt ON bt.BankruptcyTrustID = clbt.BankruptcyTrustID

GO
