SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vProvisions]
AS

SELECT PR.PolicyID, ProvisionID, pr.ProvisionLabelID, Provision, pr.ProvisionValueID, ProvisionValue
FROM tblProvision PR
LEFT JOIN tblProvisionLabel PL ON PL.ProvisionLabelID = PR.ProvisionLabelID
LEFT JOIN tblProvisionValue PV ON PV.ProvisionValueID = PR.ProvisionValueID



GO
GRANT VIEW DEFINITION ON  [dbo].[vProvisions] TO [base_user]
GRANT SELECT ON  [dbo].[vProvisions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vProvisions] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vProvisions] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vProvisions] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vProvisions] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vProvisions] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vProvisions] TO [power_user]
GRANT SELECT ON  [dbo].[vProvisions] TO [power_user]
GRANT INSERT ON  [dbo].[vProvisions] TO [power_user]
GRANT DELETE ON  [dbo].[vProvisions] TO [power_user]
GRANT UPDATE ON  [dbo].[vProvisions] TO [power_user]
GO
