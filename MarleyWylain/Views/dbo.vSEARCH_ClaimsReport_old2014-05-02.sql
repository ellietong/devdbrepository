SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [dbo].[vSEARCH_ClaimsReport_old2014-05-02] As 

SELECT ClaimLawsuitID As ClaimLawsuitID
       , ClaimantPersonalInfoID As ClaimantPersonalInfoID
       , Convert(date,ActiveTrialDate) As ActiveTrialDate
       , ClaimID As ClaimID
       , ClaimLawsuitStatus As ClaimLawsuitStatus
       , Matter As Matter
       , ClaimSequence As ClaimSequence
       --, firstname
       --, lastname
       , lastname + ', ' + firstname                       AS FullName
       , convert(date,BirthDate) As BirthDate
       , convert(date,DeceasedDate) As DeceasedDate
       , ( CASE
             WHEN ssn4digit IS NOT NULL THEN 'XXX-XX-' + ssn4digit
             ELSE NULL
           END )                                           SocialSecurityNumber
       , Occupation As Occupation
       , HighLevelDisease as HighLevelDisease
       , DocketNumber as DocketNumber
       , convert(date, ServiceDate) As ServiceDate
       , convert(date, COALESCE(filedate, receiveddate))                  AS FileDate
       , [State] As State
       , Jurisdiction As Jurisdiction
       , convert(date, ProcessedDate) as ProcessedDate
       , convert(date, StatusDate) as StatusDate
       , ProductNotes as ProductNotes
       , convert(date,StartDate)                                         AS ProductExposureStart
       , convert(date, SettlementDate) as SettlementDate
       , SettlementAmount As SettlementAmount
       , convert(date, SourceDate) AS SourceDate                                  --  AS SettlementPaidDate -- Date paid; source date is the source date of the first check that is cut for a settlement
       , PlaintiffCounsel As PlaintiffCounsel
       , ClaimLawsuitStatusID as ClaimLawsuitStatusID
       , StateID as StateID
       , JurisdictionID as JurisdictionID
       , IsMDL as IsMDL
       , HighLevelDiseaseID as HighLevelDiseaseID
       , AllegationTypeID as AllegationTypeID
       , PlaintiffCounselID as PlaintiffCounselID
       , MatterID as MatterID
       , lawsuitdefensecounseladdressid                    AS DefenseCounselAddressID
       , defcounseljurisdictioninfodefensecounseladdressid AS DefenseCounselAddressID2, 
		CONVERT(int, NULL) AS PersonID, CONVERT(int, NULL) AS BrandID, CONVERT(nvarchar(50), NULL) AS Brand, CONVERT(int, NULL) AS BankruptcyStayID, CONVERT(nvarchar(50), NULL) AS BankruptcyStay,
		CONVERT(int, NULL) AS ClaimLawsuitTypeID, CONVERT(nvarchar(50), NULL) AS ClaimLawsuitType, CONVERT(nvarchar(100), NULL) AS JobsiteCity, CONVERT(int, NULL) AS JobsiteStateID, CONVERT(nvarchar(1000), NULL) AS Jobsite, CONVERT(int, NULL) AS BusinessCaseID, CONVERT(nvarchar(50), NULL) AS BusinessCase
FROM   (SELECT claimlawsuitid
               , activetrialdate
               , claimid
               , lawsuitid
               , ismdl
               , claimlawsuitstatus
               , claimlawsuitstatusid
               , matter
               , claimsequence
               , claimantpersonalinfoid
               , firstname
               , lastname
               , birthdate
               , deceaseddate
               , ssn4digit
               , occupation
               , highleveldisease
               , docketnumber
               , servicedate
               , filedate
               , receiveddate
               , [state]
               , jurisdiction
               , processeddate
               , statusdate
               , productnotes
               , startdate
               , settlementdate
               , settlementamount
               , sourcedate
               , plaintiffcounsel
               , stateid
               , jurisdictionid
               , highleveldiseaseid
               , plaintiffcounselid
               , matterid
			   , lawsuitdefensecounseladdressid
			   , allegationtypeid
        FROM   (SELECT claimlawsuitid
                       , activetrialdate
                       , claimid
                       , lawsuitid
                       , ismdl
                       , claimlawsuitstatus
                       , claimlawsuitstatusid
                       , docketnumber
                       , servicedate
                       , filedate
                       , receiveddate
                       , [state]
                       , stateid
                       , jurisdiction
                       , jurisdictionid
                       , processeddate
                       , statusdate
                       , settlementdate
                       , settlementamount
                       , sourcedate
                       , plaintiffcounsel
                       , plaintiffcounselid
					   , lawsuitdefensecounseladdressid
					   , allegationtypeid
                FROM   (SELECT claimlawsuitid
                               , activetrialdate
                               , claimid
                               , lawsuitid
                               , ismdl
                               , settlementdate
                               , settlementamount
                               , sourcedate
                        FROM   tblclaimlawsuit
                               LEFT OUTER JOIN (SELECT claimlawsuitid AS settlmentInfoClaimLawsuitid
                                                       , settlementdate
                                                       , settlementamount
                                                       , sourcedate
                                                FROM   tblclaimsettlement
                                                       LEFT OUTER JOIN (SELECT claimsettlementid
                                                                               , Min(checkid) AS EarliestCheckID
                                                                               , Min(sourcedate) AS sourcedate
                                                                        FROM   tblpaymentsettlement
                                                                               LEFT OUTER JOIN (SELECT checkid AS checkInfoCheckID
                                                                                                       , sourcedate
                                                                                                FROM   tblcheck) checkInfo
                                                                                            ON ( tblpaymentsettlement.checkid = checkInfo.checkinfocheckid )
                                                                        GROUP  BY claimsettlementid) settlementPaymentInfo
                                                                    ON ( tblclaimsettlement.claimsettlementid = settlementPaymentInfo.claimsettlementid ))settlementInfo
                                            ON ( tblclaimlawsuit.claimlawsuitid = settlementinfo.settlmentinfoclaimlawsuitid )

                        WHERE  isprimaryforclaim = 1) CLInfo
																	Left outer join(select ClaimLawsuitID as allegationTypeClaimLawsuitID, min(allegationtypeID) as allegationtypeid from tblAllegation group by ClaimLawsuitID) AllegationTypeInfo
											on (CLInfo.ClaimLawsuitID = AllegationTypeInfo.allegationTypeClaimLawsuitID)
                       INNER JOIN (SELECT trackingInfo.claimlawsuitstatusid
                                               , trackingInfo.claimlawsuitid AS ClaimLawsuitID_fromTrackingInfo
                                               , statusLabelnfo.claimlawsuitstatus
                                               , processeddate
                                               , statusdate
                                        FROM   (SELECT claimlawsuitstatusid
                                                       , claimlawsuitid
                                                       , processeddate
                                                       , statusdate
                                                FROM   tblclaimlawsuitstatustracking
                                                WHERE  isprimarystatus = 1) trackingInfo
                                               INNER JOIN (SELECT claimlawsuitstatusid
                                                                       , claimlawsuitstatus
                                                                FROM   tblclaimlawsuitstatus)statusLabelnfo
                                                            ON ( trackingInfo.claimlawsuitstatusid = statusLabelnfo.claimlawsuitstatusid )) lawsuitstatusinfo
                                    ON ( CLInfo.claimlawsuitid = lawsuitstatusinfo.claimlawsuitid_fromtrackinginfo )
                       INNER JOIN (SELECT lawsuitid AS lawsuitinfolawsuitid
                                               , docketnumber
                                               , servicedate
                                               , filedate
                                               , receiveddate
                                               , [state]
                                               , stateid
                                               , jurisdiction
                                               , jurisdictionid
                                               , plaintiffcounsel
                                               , plaintiffcounselid
											   , defensecounsel          AS LawsuitDefenseCounsel
                                               , defensecounseladdressid AS LawsuitDefenseCounselAddressID
                                               , defensecounselid        AS LawsuitDefenseCounselID
                                        FROM   tbllawsuit
                                               LEFT OUTER JOIN (SELECT stateid AS stateInfoStateID
                                                                       , [state]
                                                                FROM   tblstate)stateInfo
                                                            ON( tbllawsuit.stateid = stateInfo.stateinfostateid )
                                               LEFT OUTER JOIN (SELECT jurisdictionid AS jurisdictioninfoInfojurisdictionID
                                                                       , jurisdiction
                                                                FROM   tbljurisdiction)jurisdictionInfo
                                                            ON( tbllawsuit.jurisdictionid = jurisdictionInfo.jurisdictioninfoinfojurisdictionid )
                                               LEFT OUTER JOIN (SELECT plaintiffcounseladdressid AS plaintiffCounselInfoAddressId
                                                                       , plaintiffcounsel
																	   , PlaintiffCounselID
                                                                FROM   tblplaintiffcounseladdress) plaintiffCounselInfo
                                                            ON ( tbllawsuit.plaintiffcounseladdressid = plaintiffCounselInfo.plaintiffcounselinfoaddressid )
												LEFT OUTER JOIN (SELECT defensecounseladdressid AS defenseCounselInfoAddressID
                                                                       , defensecounsel
                                                                       , defensecounselid
                                                                FROM   tbldefensecounseladdress)defensecounselinfo
                                                            ON ( tbllawsuit.defensecounseladdressid = defensecounselinfo.defensecounselinfoaddressid ))lawsuitInfo
                                    ON ( CLInfo.lawsuitid = lawsuitInfo.lawsuitinfolawsuitid ))LawsuitLevelInfo
               INNER JOIN (SELECT claimid_fromtblclaim     AS claimID_fromtblClaim
                                       , claimantpersonalinfoid AS claimantpersonalinfoid
                                       , matter
                                       , matterid
                                       , claimsequence
                                       , highleveldisease
                                       , highleveldiseaseid
                                FROM   (SELECT claimid AS claimID_fromtblClaim
                                               , claimantpersonalinfoid
                                               , matterid
                                               , claimsequenceid
                                        FROM   tblclaim)ClaimInfo
                                       LEFT OUTER JOIN (SELECT matterid AS matterID_fromtblMatter
                                                               , matter
                                                        FROM   tblmatter) matterInfo
                                                    ON ( ClaimInfo.matterid = matterInfo.matterid_fromtblmatter )
                                       LEFT OUTER JOIN (SELECT claimsequenceid
                                                               , claimsequence
                                                        FROM   tblclaimsequence) ClaimSequenceInfo
                                                    ON ( ClaimInfo.claimsequenceid = ClaimSequenceInfo.claimsequenceid )
                                       LEFT OUTER JOIN (SELECT claimid
                                                               , highleveldisease
                                                               , highleveldiseaseid
                                                        FROM   (SELECT claimid
                                                                       , diseaseid
                                                                       , primarydisease
                                                                FROM   tblclaimdisease) theClaimDiseaseInfo
                                                               LEFT OUTER JOIN (SELECT diseaseid AS diseaseInfodiseaseid
                                                                                       , highleveldiseaseid
                                                                                FROM   tbldisease) diseaseInfo
                                                                            ON( theClaimDiseaseInfo.diseaseid = diseaseInfo.diseaseinfodiseaseid )
                                                               LEFT OUTER JOIN (SELECT highleveldiseaseid AS hldiseaseinfohldiseaseid
                                                                                       , highleveldisease
                                                                                FROM   tblhighleveldisease) hlDiseaseInfo
                                                                            ON( diseaseInfo.highleveldiseaseid = hlDiseaseInfo.hldiseaseinfohldiseaseid )
                                                        WHERE  theClaimDiseaseInfo.primarydisease = 1) ClaimDiseaseInfo
                                                    ON( ClaimInfo.claimid_fromtblclaim = ClaimDiseaseInfo.claimid ))ClaimLevelInfo
                            ON ( LawsuitLevelInfo.claimid = ClaimLevelInfo.claimid_fromtblclaim )
               INNER JOIN(SELECT claimid                            AS claimID_forclaimantinfo
                                      , ClaimInfo.claimantpersonalinfoid AS personalinfoid
                                      , firstname
                                      , lastname
                                      , birthdate
                                      , deceaseddate
                                      , ssn4digit
                                      , occupation
                                      , productnotes
                                      , startdate
                               FROM   (SELECT claimid
                                              , claimantpersonalinfoid
                                       FROM   tblclaim) ClaimInfo
                                      LEFT OUTER JOIN (SELECT claimantpersonalinfoid
                                                              , firstname
                                                              , lastname
                                                              , birthdate
                                                              , deceaseddate
                                                              , RIGHT(socialsecuritynumber, 4) AS SSN4Digit
                                                       FROM   tblclaimantpersonalinfo) ClaimantInfo
                                                   ON ( ClaimInfo.claimantpersonalinfoid = ClaimantInfo.claimantpersonalinfoid )
                                      LEFT OUTER JOIN (SELECT employmentpersonalinfoid
                                                              , occupation
                                                              , productnotes
                                                              , startdate
                                                       FROM   (SELECT claimantpersonalinfoid AS employmentPersonalInfoId
                                                                      , occupationid
                                                                      , claimantemploymentid
                                                               FROM   tblclaimantemployment
                                                               WHERE  isprimary = 1) empInfo
                                                              LEFT OUTER JOIN (SELECT occupationid
                                                                                      , occupation
                                                                               FROM   tbloccupation) occInfo
                                                                           ON ( empInfo.occupationid = occInfo.occupationid )
                                                              LEFT OUTER JOIN (SELECT claimantemploymentid as prodinfoclaimentemploymentid,
       productid,
       minstartdate As Startdate,
       productnotes
FROM   (SELECT claimantemploymentid,
               productid,
               minstartdate
        FROM   (SELECT DISTINCT claimantemploymentid,
                                productid
                FROM   tblclaimantproduct) distinctEmploymentProducts
               LEFT OUTER JOIN(SELECT claimantemploymentid AS
                                      minQueryClaimantEmploymentID,
                                      productid            AS minQueryProductID,
                                      Min(startdate)       AS MinStartDate
                               FROM   tblclaimantproduct
                               GROUP  BY claimantemploymentid,
                                         productid)
                              MinStartDatesForEmploymentProducts
                            ON ( distinctEmploymentProducts.claimantemploymentid
                                 =
               MinStartDatesForEmploymentProducts.minqueryclaimantemploymentid
               AND distinctEmploymentProducts.productid =
                   MinStartDatesForEmploymentProducts.minqueryproductid ))
       MinDateQuery
       LEFT OUTER JOIN (SELECT claimantemploymentid AS
                               prodnotesqueryClaimantemloymentid,
                               productid            AS prodnotesqueryProductID,
                               startdate            AS prodnotesqueryStartDate,
                               productnotes
                        FROM   tblclaimantproduct)prodnotesquery
                    ON ( MinDateQuery.claimantemploymentid =
       prodnotesquery.prodnotesqueryclaimantemloymentid
       AND MinDateQuery.productid = prodnotesquery.prodnotesqueryproductid
       AND MinDateQuery.minstartdate = prodnotesquery.prodnotesquerystartdate )) prodInfo
                                                                           ON ( empInfo.claimantemploymentid = prodInfo.prodinfoclaimentemploymentid )) claimantEmployment
                                                   ON ( ClaimantInfo.claimantpersonalinfoid = claimantEmployment.employmentpersonalinfoid )) ClaimantLevelInfo
                            ON( LawsuitLevelInfo.claimid = ClaimantLevelInfo.claimid_forclaimantinfo ))MainQuery
	   
	   LEFT OUTER JOIN (SELECT defensecounseladdressid AS defCounselJurisdictionInfoDefenseCounselAddressID
                               , stateid               AS defCounselJurisdictionInfoStateID
                               , jurisdictionid        AS defCounselJurisdictionInfoJurisdictionId
                        FROM   tbldefensejurisdiction)defCounselJurisdictionInfo
                    ON ( MainQuery.stateid = defCounselJurisdictionInfo.defcounseljurisdictioninfostateid
                         AND MainQuery.jurisdictionid = defCounselJurisdictionInfo.defcounseljurisdictioninfojurisdictionid )

GROUP  BY claimlawsuitid
          , claimantpersonalinfoid
          , claimid
          , ismdl
          , activetrialdate
          , claimlawsuitstatus
          , matter
          , claimsequence
          , firstname
          , lastname
          , birthdate
          , deceaseddate
          , ssn4digit
          , occupation
          , highleveldisease
          , activetrialdate
          , docketnumber
          , servicedate
          , filedate
          , receiveddate
          , [state]
          , jurisdiction
          , processeddate
          , statusdate
          , productnotes
          , startdate
          , settlementdate
          , settlementamount
          , sourcedate
          , plaintiffcounsel
          , claimlawsuitstatusid
          , stateid
          , jurisdictionid
          , highleveldiseaseid
          , plaintiffcounselid
          , matterid 
		  , allegationtypeid
		  , lawsuitdefensecounseladdressid                   
		  , defcounseljurisdictioninfodefensecounseladdressid


GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_ClaimsReport_old2014-05-02] TO [power_user]
GO
