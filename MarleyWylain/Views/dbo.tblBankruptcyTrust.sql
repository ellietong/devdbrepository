SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblBankruptcyTrust]
AS

SELECT BankruptcyTrustID, BankruptcyTrust
FROM [KGA-ClaimsAdmin].dbo.tblBankruptcyTrust

GO
