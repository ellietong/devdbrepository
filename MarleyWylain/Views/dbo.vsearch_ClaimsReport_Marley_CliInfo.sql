SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [dbo].[vsearch_ClaimsReport_Marley_CliInfo]
as

with settlementPaymentInfo as (
SELECT CLAIMSETTLEMENTID AS SettlementPaymentInfoClaimSettlementID
	    , MIN(CHECKID)    AS EarliestCheckID
	FROM   dbo.TBLPAYMENTSETTLEMENT
	    LEFT OUTER JOIN (SELECT CHECKID AS checkInfoCheckID
	                        FROM   dbo.TBLCHECK) checkInfo
	                    ON ( dbo.TBLPAYMENTSETTLEMENT.CHECKID = checkInfo.CHECKINFOCHECKID )
	GROUP  BY CLAIMSETTLEMENTID) 
	
	SELECT claws.claimlawsuitid
	, activetrialdate
	, claimid
	, lawsuitid
	, ismdl
	, settlementdate
	, PAIDDATE
	, settlementamount
	, sourcedate
	, ClaimLawsuitNotes
	, ClaimLawsuitNotes AS TrialNotes
	
	
	from dbo.TBLCLAIMLAWSUIT claws -- CLInfo
		--inner join dbo.TBLCLAIMLAWSUITSTATUSTRACKING cllawsttrak
		--on cllawsttrak.ClaimLawsuitID = claws.ClaimLawsuitID	
		left join dbo.TBLCLAIMSETTLEMENT clsett
		on claws.CLAIMLAWSUITID = clsett.CLAIMLAWSUITID 	
		and isnull(ISPRIMARYFORCLAIM,0) = 1
		left join settlementPaymentInfo settinfo		
		on clsett.ClaimSettlementID = settinfo.SettlementPaymentInfoClaimSettlementID
		left join dbo.TBLCHECK chk
		on settinfo.EarliestCheckID = chk.CheckID  -- CLInfo
	--where isnull(ISPRIMARYFORCLAIM,0) = 1
	group by claws.claimlawsuitid
	, activetrialdate
	, claimid
	, lawsuitid
	, ismdl
	, settlementdate
	, PAIDDATE
	, settlementamount
	, sourcedate
	, ClaimLawsuitNotes
	, ClaimLawsuitNotes

GO
