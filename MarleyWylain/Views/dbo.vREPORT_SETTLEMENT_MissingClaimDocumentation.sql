SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE VIEW [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation]
AS
SELECT	 main.ClaimLawsuitID,
		 dc.DocCategory AS MissingDocumentation,
		 main.DocCategoryID

FROM (
	SELECT ClaimLawsuitID, MissingDocumentation, Satisfied, 
	CASE WHEN MissingDocumentation = 'ClaimantIDed' THEN 1
		 WHEN MissingDocumentation = 'DxMedicals' THEN 2
		 WHEN MissingDocumentation = 'DODCert' THEN 3
		 WHEN MissingDocumentation = 'EstatePapers' THEN 4
		 WHEN MissingDocumentation = 'ProductID' THEN 5
		 WHEN MissingDocumentation = 'EmployExpoHist' THEN 6
		 WHEN MissingDocumentation = 'CompServ' THEN 7
		 WHEN MissingDocumentation = 'SettlementConfirmation' THEN 8
		 WHEN MissingDocumentation = 'MMSEA' THEN 9
		 WHEN MissingDocumentation = 'PreTrialReport' THEN 21
		 WHEN MissingDocumentation = 'ExecutedRelease' THEN 22
		 WHEN MissingDocumentation = 'AllstateMMSEAForm' THEN 23
		 END AS DocCategoryID
	FROM
	(
 
	SELECT DISTINCT		
						  cl.ClaimLawsuitID, 
					 
						  COALESCE (sd.ClaimantIDed, 0) AS ClaimantIDed, 
						  COALESCE (sd.DxMedicals, 0) AS DxMedicals, 
						  CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE (sd.DODCert, 0) END AS DODCert, 
						  CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE (sd.EstatePapers, 0) END AS EstatePapers,
						  COALESCE (sd.ProductID, 0) AS ProductID,
						  --COALESCE (p.productentered, 0) AS ProductEntered, 
						  COALESCE (sd.EmployExpoHist, 0) AS EmployExpoHist, 
						  COALESCE (sd.CompServ,0) AS CompServ,
						  COALESCE (sd.SettlementConfirmation, 0) AS SettlementConfirmation, 
						  COALESCE (sd.MMSEA, 0) AS MMSEA, 
						  COALESCE (sd.PreTrialReport, 0) AS PreTrialReport,
						  COALESCE (sd.ExecutedRelease, 0) AS ExecutedRelease,  
						  COALESCE (sd.AllstateMMSEAForm, 0) AS AllstateMMSEAForm--, 
						  --CASE WHEN (d_1.ClaimantPersonalInfoID IS NOT NULL OR                      ExecutedReleaseDate IS NOT NULL) THEN 1 ELSE 0 END AS ReleaseReceived
	FROM         dbo.tblClaim AS c INNER JOIN
						  dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
						  dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
						  dbo.tblClaimSettlement AS cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID LEFT JOIN
						  dbo.tblClaimSettlementDocumentation AS sd ON cl.ClaimLawsuitID = sd.ClaimLawsuitID LEFT OUTER JOIN
						  dbo.tblClaimLawsuitStatusTracking AS cls ON cls.ClaimLawsuitID = cl.ClaimLawsuitID --LEFT OUTER JOIN
	WHERE     (cls.IsPrimaryStatus = 1) AND (cls.ClaimLawsuitStatusID IN (25))
	)  missingdoc
		UNPIVOT (Satisfied FOR MissingDocumentation IN (ClaimantIDed
		  ,DxMedicals
		  ,DODCert
		  ,EstatePapers
		  ,ProductID
		  ,EmployExpoHist
		  ,CompServ
		  ,SettlementConfirmation
		  ,MMSEA
		  ,PreTrialReport
		  ,ExecutedRelease
		  ,AllstateMMSEAForm) )as unpiv
		WHERE Satisfied = 0


) main
INNER JOIN tblClaimSettlementDocumentCategory dc ON main.DocCategoryID = dc.DocCategoryID





GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [power_user]
GO
