SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*, o.ExpectedPayDate
HAVING        (COALESCE (SUM(ps.PaymentAmount), 0) < cs.SettlementAmount)*/
CREATE VIEW [dbo].[vSETTLEMENT_PaymentApproval]
AS
SELECT        cpi.LastName + ', ' + cpi.FirstName + COALESCE (' ' + cpi.MiddleName, '') AS ClaimantName, cl.ClaimLawsuitID, cs.ClaimSettlementID, c.ClaimantPersonalInfoID, pca.PlaintiffCounsel, cs.SettlementDate, 
                         cs.SettlementAmount, m.Matter, CASE WHEN PaymentApprovedBy IS NULL AND PaymentApprovedDate IS NULL THEN 0 ELSE 1 END AS IsApproved, CASE WHEN PaymentRecommendedBy IS NULL AND 
                         PaymentRecommendedDate IS NULL THEN 0 ELSE 1 END AS IsRecommended, CONVERT(money, NULL) AS TotalARAmount, CONVERT(datetime, NULL) AS ExpectedPayDate, cs.SettlePaymentComments AS PrivateComment
FROM            dbo.tblClaim AS c INNER JOIN
                         dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                         dbo.tblMatter AS m ON c.MatterID = m.MatterID INNER JOIN
                         dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                         dbo.tblClaimLawsuitStatusTracking AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID INNER JOIN
                         dbo.tblLawsuit AS l ON l.LawsuitID = cl.LawsuitID LEFT OUTER JOIN
                         dbo.tblPlaintiffCounselAddress AS pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID INNER JOIN
                         dbo.tblClaimSettlement AS cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblPaymentSettlement AS ps ON cs.ClaimSettlementID = ps.ClaimSettlementID
WHERE        (COALESCE (clst.IsPrimaryStatus, 0) = 1) AND (clst.ClaimLawsuitStatusID IN (23)) AND (cs.SettlementAmount >= 0) AND (cs.PaymentApprovedBy IS NULL) AND (ps.ClaimSettlementID IS NULL)
GROUP BY cpi.LastName + ', ' + cpi.FirstName + COALESCE (' ' + cpi.MiddleName, ''), cl.ClaimLawsuitID, cs.ClaimSettlementID, c.ClaimantPersonalInfoID, cl.LawsuitID, pca.PlaintiffCounsel, cs.SettlementDate, 
                         cs.SettlementAmount, m.Matter, CASE WHEN PaymentApprovedBy IS NULL AND PaymentApprovedDate IS NULL THEN 0 ELSE 1 END, CASE WHEN PaymentRecommendedBy IS NULL AND 
                         PaymentRecommendedDate IS NULL THEN 0 ELSE 1 END, cs.SettlePaymentComments

GO
GRANT VIEW DEFINITION ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [base_user]
GRANT SELECT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT SELECT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT INSERT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT DELETE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT UPDATE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[7] 2[33] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 286
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 6
               Left = 324
               Bottom = 136
               Right = 590
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 6
               Left = 628
               Bottom = 102
               Right = 814
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 852
               Bottom = 136
               Right = 1095
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clst"
            Begin Extent = 
               Top = 6
               Left = 1133
               Bottom = 136
               Right = 1398
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 293
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pca"
            Begin Extent = 
               Top = 138
               Left = 331
               Bottom = 268
               Right = 576
            End
            DisplayFlags = 280
            TopColumn = 0
         ', 'SCHEMA', N'dbo', 'VIEW', N'vSETTLEMENT_PaymentApproval', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'End
         Begin Table = "cs"
            Begin Extent = 
               Top = 138
               Left = 614
               Bottom = 268
               Right = 874
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ps"
            Begin Extent = 
               Top = 138
               Left = 912
               Bottom = 268
               Right = 1152
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSETTLEMENT_PaymentApproval', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSETTLEMENT_PaymentApproval', NULL, NULL
GO
