SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vSearchDocument_old]
AS
SELECT DISTINCT 
                         d.DocumentID, d.DocumentTitle, d.DocumentURL, cdt.ClaimantDocumentType, f.FirmID, f.Firm, d.CreatedBy, d.DateCreated, d.AcceptedBy, d.DateAccepted, 
                         cdt.ClaimantDocumentTypeID
FROM            dbo.tblDocuments AS d INNER JOIN
                         dbo.tblClaimantDocumentType AS cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID LEFT OUTER JOIN
                         dbo.tblMultiClaimantDocument AS mcd ON d.DocumentID = mcd.DocumentID LEFT OUTER JOIN
                         dbo.tblClaim AS c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID LEFT OUTER JOIN
                         dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID LEFT OUTER JOIN
                         dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                         dbo.tblDefenseCounselAddress AS dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID LEFT OUTER JOIN
                         dbo.vtblFirm AS f ON dca.DefenseCounselID = f.FirmID LEFT OUTER JOIN
                         dbo.tblDefenseJurisdiction AS dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID

GO
GRANT VIEW DEFINITION ON  [dbo].[vSearchDocument_old] TO [base_user]
GRANT SELECT ON  [dbo].[vSearchDocument_old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSearchDocument_old] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSearchDocument_old] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSearchDocument_old] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSearchDocument_old] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSearchDocument_old] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSearchDocument_old] TO [power_user]
GRANT SELECT ON  [dbo].[vSearchDocument_old] TO [power_user]
GRANT INSERT ON  [dbo].[vSearchDocument_old] TO [power_user]
GRANT DELETE ON  [dbo].[vSearchDocument_old] TO [power_user]
GRANT UPDATE ON  [dbo].[vSearchDocument_old] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[50] 4[25] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cdt"
            Begin Extent = 
               Top = 6
               Left = 306
               Bottom = 101
               Right = 536
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mcd"
            Begin Extent = 
               Top = 102
               Left = 306
               Bottom = 231
               Right = 538
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 270
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 234
               Left = 308
               Bottom = 363
               Right = 530
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 399
               Right = 277
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dca"
            Begin Extent = 
               Top = 366
               Left = 315
               Bottom = 495
               Right = 542
            End
            DisplayFlags = 280
            Top', 'SCHEMA', N'dbo', 'VIEW', N'vSearchDocument_old', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'Column = 0
         End
         Begin Table = "f"
            Begin Extent = 
               Top = 6
               Left = 574
               Bottom = 135
               Right = 788
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dj"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 663
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSearchDocument_old', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSearchDocument_old', NULL, NULL
GO
