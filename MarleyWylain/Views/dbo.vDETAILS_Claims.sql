SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDETAILS_Claims]
AS
SELECT DISTINCT 
                      cpi.ClaimantPersonalInfoID, 
					  c.ClaimID, 
					  cls.ClaimLawsuitStatus, 
					  hddis.HighLevelDisease AS Disease, 
					  m.Matter, 
					  StatusDate AS ResolutionDate, 
					  clst.ProcessedDate,
					  IsCurrentClaim,
					  l.DefenseCounselAddressID AS DefenseCounselAddressID, 
					  dj.DefenseCounselAddressID AS DJDefenseCounselAddressID,
					  CONVERT(nvarchar(50), NULL) AS ClaimLawsuitType,
					  CONVERT(nvarchar(50), NULL) AS BankruptcyStay,
					  CONVERT(nvarchar(50), NULL) AS Brand
FROM         dbo.tblClaimantPersonalInfo AS cpi INNER JOIN
                      dbo.tblClaim AS c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID LEFT OUTER JOIN
                      dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                      dbo.tblDefenseJurisdiction AS dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress AS dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID LEFT OUTER JOIN
                      dbo.tblMatter AS m ON m.MatterID = c.MatterID LEFT OUTER JOIN
                          (SELECT     ClaimDiseaseID, ClaimID, DiseaseID, PrimaryDisease, DiseaseDiagnosisDate, DoctorID, NavDisease, SPUsername
                            FROM          dbo.tblClaimDisease
                            WHERE      (PrimaryDisease = 1)) AS cd ON cd.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblDisease AS ddis ON ddis.DiseaseID = cd.DiseaseID LEFT OUTER JOIN
                      dbo.tblHighLevelDisease AS hddis ON hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID INNER JOIN
                          (SELECT     ClaimLawsuitStatusTrackingID, ClaimLawsuitID, ClaimLawsuitStatusID, StatusNotes, IsPrimaryStatus, NavStatus, ProcessedDate, StatusDate, 
                                                   SPUsername
                            FROM          dbo.tblClaimLawsuitStatusTracking
                            WHERE      (IsPrimaryStatus = 1)) AS clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatus AS cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
					  WHERE IsPrimaryForClaim = 1

GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_Claims] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_Claims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_Claims] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_Claims] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_Claims] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_Claims] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_Claims] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_Claims] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_Claims] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_Claims] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_Claims] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_Claims] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "cpi"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 288
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 326
               Bottom = 135
               Right = 558
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 596
               Bottom = 135
               Right = 818
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 6
               Left = 856
               Bottom = 135
               Right = 1095
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dj"
            Begin Extent = 
               Top = 6
               Left = 1341
               Bottom = 135
               Right = 1568
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dca"
            Begin Extent = 
               Top = 138
               Left = 268
               Bottom = 267
               Right = 495
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 6
               Left = 1133
               Bottom = 101
               Right = 1303
            End
            DisplayFlags = 280
            TopC', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_Claims', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'olumn = 0
         End
         Begin Table = "cd"
            Begin Extent = 
               Top = 234
               Left = 1111
               Bottom = 363
               Right = 1314
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ddis"
            Begin Extent = 
               Top = 102
               Left = 1133
               Bottom = 231
               Right = 1325
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hddis"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 233
               Right = 230
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clst"
            Begin Extent = 
               Top = 270
               Left = 258
               Bottom = 399
               Right = 507
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cls"
            Begin Extent = 
               Top = 138
               Left = 555
               Bottom = 267
               Right = 791
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2535
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1575
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_Claims', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_Claims', NULL, NULL
GO
