SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vSEARCH_InvoiceDocument]
AS
SELECT 
                      d.DocumentID, 
					  ClaimantDocumentType, 
					  d.Comments, 
					  d.DocumentTitle, 
					  d.CreatedBy, 
					  d.DateCreated, 
					  d.ModifiedBy, 
                      d.DateModified, 
					  d.Accepted, 
					  d.AcceptedBy, 
					  d.DateAccepted, 
					  di.ApprovedBy,  
					  di.DateApproved,
					  di.SubmittedTotal,
					  dca.DefenseCounsel AS Firm, 
                      di.InvoiceNumber, 
					  di.Period, 
					  di.InvoiceNotes AS AdjustmentNotes,  
					  di.FirmID AS FirmID,
					  COALESCE (ex.DefenseCounsel, '') AS VendorFirm, 
					  di.VendorFirmID, 
					  d.MatterID,
					  di.DefenseInvoiceID,
					  FileSizeBytes,
					  DocumentURL AS DocumentLink,
					  di.StatusID
FROM         (SELECT d.*, dt.ClaimantDocumentType FROM dbo.tblDocuments AS d INNER JOIN
                      dbo.tblClaimantDocumentType AS dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID INNER JOIN
                      dbo.tblClaimantDocumentTypeGroup AS cdtg ON dt.ClaimantDocumentTypeID = cdtg.ClaimantDocumentTypeID INNER JOIN
                      dbo.tblClaimantDocumentGroup AS cdg ON cdtg.ClaimantDocumentGroupID = cdtg.ClaimantDocumentGroupID
					  WHERE     (cdtg.ClaimantDocumentGroupID = 2) ) d LEFT OUTER JOIN
                      dbo.tblDefenseInvoice AS di ON d.DocumentID = di.DocumentID LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress AS dca ON di.DefenseCounselAddressID = dca.DefenseCounselAddressID LEFT OUTER JOIN
                      dbo.tblDefenseCounsel AS ex ON di.VendorFirmID = ex.DefenseCounselID
					  where defenseinvoiceid is not null and firmid is not null and VendorFirmID is not null
GROUP BY d.DocumentID, 
					  ClaimantDocumentType, 
					  d.DocumentTitle, 
					  d.CreatedBy, 
					  d.DateCreated, 
					  d.ModifiedBy, 
                      d.DateModified, 
					  d.Accepted, 
					  d.AcceptedBy, 
					  d.DateAccepted, 
					  di.ApprovedBy, 
					  					  di.DateApproved,

					  di.SubmittedTotal,
					  dca.DefenseCounsel, 
                      di.InvoiceNumber, 
					  di.Period, 
					  di.InvoiceNotes, 
					  di.FirmID,
					  COALESCE (ex.DefenseCounsel, ''), 
					  di.VendorFirmID, 
					  d.MatterID,
					  di.DefenseInvoiceID,
					  FileSizeBytes,
						  d.Comments, 
				  DocumentURL,
					  di.StatusID

GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_InvoiceDocument] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_InvoiceDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_InvoiceDocument] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_InvoiceDocument] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_InvoiceDocument] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_InvoiceDocument] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_InvoiceDocument] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_InvoiceDocument] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_InvoiceDocument] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_InvoiceDocument] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_InvoiceDocument] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_InvoiceDocument] TO [power_user]
GO
