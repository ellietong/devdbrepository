
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vREPORT_DefenseSummary]
AS
SELECT   cpi.ClaimantPersonalInfoID,  dicd.ClaimLawsuitID, cpi.FullName AS ClaimantName, dicd.InvoiceDetailID,
		SUM(CASE WHEN checkfundingsourceid = 3 AND sourceamount < 0 THEN (dicd.fees * - 1) ELSE dicd.fees END) 
		+ SUM(CASE WHEN checkfundingsourceid = 3 AND sourceamount < 0 THEN (dicd.costs * - 1) ELSE (dicd.costs) END) 
        - SUM(CASE WHEN checkfundingsourceid = 3 AND sourceamount < 0 THEN (dicd.adjustments * - 1) ELSE (dicd.adjustments) END) AS DefenseTotal, 
		di.FirmID, c.SourceDate--, di.DefenseCounselAddressID
FROM         dbo.tblDocuments AS d LEFT OUTER JOIN
                      dbo.tblDefenseInvoice AS di ON d.DocumentID = di.DocumentID LEFT OUTER JOIN
                      dbo.tblDefenseInvoiceStatus AS dis ON di.StatusID = dis.StatusID LEFT OUTER JOIN
                      dbo.tblVendorFirm AS ex ON di.VendorFirmID = ex.VendorFirmID LEFT OUTER JOIN
					   --dbo.tblDefenseCounselAddress AS dca ON di.DefenseCounselAddressID = dca.DefenseCounselAddressID LEFT OUTER JOIN
                      dbo.tblPaymentDefense AS pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID LEFT OUTER JOIN
                      dbo.tblDefInvClaimantDetail AS dicd ON dicd.DefenseInvoiceID = di.DefenseInvoiceID LEFT OUTER JOIN
                          (SELECT     CheckID, SourceNumber, SourceDate, SourceAmount, MatterID, CheckFundingTypeID, CheckFundingSourceID, NavClaimResolutionKey, CheckPDF
                            FROM          dbo.tblCheck) AS c ON pd.CheckID = c.CheckID AND dicd.MatterID = c.MatterID INNER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = dicd.ClaimantPersonalInfoID
WHERE     (d.ClaimantDocumentTypeID IN (40, 41)) 
--and c.Sourcedate BETWEEN '1/1/1900' and '12/31/2015' 
GROUP BY cpi.ClaimantPersonalInfoID, dicd.ClaimLawsuitID, dicd.InvoiceDetailID, cpi.FullName, di.FirmID, c.SourceDate--, di.DefenseCounselAddressID







GO

GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseSummary] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_DefenseSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseSummary] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_DefenseSummary] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_DefenseSummary] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_DefenseSummary] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_DefenseSummary] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseSummary] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_DefenseSummary] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_DefenseSummary] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_DefenseSummary] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_DefenseSummary] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "di"
            Begin Extent = 
               Top = 6
               Left = 296
               Bottom = 114
               Right = 539
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dis"
            Begin Extent = 
               Top = 6
               Left = 577
               Bottom = 84
               Right = 744
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ex"
            Begin Extent = 
               Top = 84
               Left = 577
               Bottom = 192
               Right = 744
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pd"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 239
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dicd"
            Begin Extent = 
               Top = 114
               Left = 277
               Bottom = 222
               Right = 486
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 275
            End
            DisplayFlags = 280
            TopColumn = 0
        ', 'SCHEMA', N'dbo', 'VIEW', N'vREPORT_DefenseSummary', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' End
         Begin Table = "c"
            Begin Extent = 
               Top = 192
               Left = 524
               Bottom = 300
               Right = 732
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1905
         Width = 1500
         Width = 1500
         Width = 2265
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vREPORT_DefenseSummary', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vREPORT_DefenseSummary', NULL, NULL
GO
