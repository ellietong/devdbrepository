
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vDETAILS_LawsuitDetails]
AS
SELECT      cl.ClaimLawsuitID, 
			c.ClaimID, 
			s.State, 
			j.Jurisdiction, 
			cs.ClaimSequence, 
			cls.ClaimLawsuitStatus, 
			l.ServiceDate, 
			l.FileDate, 
			l.CaseCaption, 
            l.LeadPlaintiffLastName + ', ' + l.LeadPlaintiffFirstName AS LeadPlaintiffName, 
			l.LeadPlaintiffFirstName, 
			l.LeadPlaintiffLastName, 
			cl.LawsuitID, 
			cl.ActiveTrialDate, 
            pca.PlaintiffCounsel, 
			dca.DefenseCounsel AS DefenseCounselAddress, 
			l.PlaintiffCounselAddressID, 
			Convert(bigint,l.DefenseCounselAddressID) as DefenseCounselAddressID, 
			l.StateID, 
			l.JurisdictionID, 
            l.NationalPlaintiffCounselID, 
			clst.ClaimLawsuitStatusID, 
			c.ClaimSequenceID, 
			npca.PlaintiffCounsel AS NationalPlaintiffCounsel, 
			l.DocketNumber, 
			ct.CourtType, 
            l.CourtTypeID, 
			l.ReceivedDate, 
			cl.IsPrimaryForClaim, 
			cl.IsMDL, 
			cl.TrialRiskID,
			tr.TrialRisk,
			CONVERT(datetime, NULL) AS ComplaintDOFE, 
			cl.IndividualDocketNumber,
			CONVERT(nvarchar(500), NULL) AS LawsuitNotes,
			CONVERT(nvarchar(500), NULL) AS ClaimLawsuitNotes,
			CONVERT(nvarchar(50), NULL) AS Brand,
			CONVERT(int, NULL) AS BrandID,
			CONVERT(money, NULL) AS DemandAmount,
			CONVERT(datetime, NULL) AS DemandDate,
			CONVERT(int, NULL) AS BankruptcyStayID,
			CONVERT(nvarchar(50), NULL) AS BankruptcyStay,
			CONVERT(int, NULL) AS ClaimLawsuitTypeID,
			CONVERT(nvarchar(50), NULL) AS ClaimLawsuitType,
			CONVERT(nvarchar(500), NULL) AS DefendantCompany,
			CONVERT(int, NULL) AS AuthorityGrantedStatusID,
			CONVERT(nvarchar(500), NULL) AS AuthorityGrantedStatus,
			CONVERT(money, null) AS AuthorityAmountRequested,
			CONVERT(money, null) AS SettlementLowAmount,
			CONVERT(money, null) AS SettlementHighAmount,
			CONVERT(int, null) AS EstimatedClientAllocation,
			cl.PlaintiffNotes,
			cl.DefenseExpertNotes
FROM         dbo.tblClaimantPersonalInfo AS cpi LEFT OUTER JOIN
                      dbo.tblClaim AS c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID LEFT OUTER JOIN
                      dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID INNER JOIN
                      dbo.tblState AS s ON s.StateID = l.StateID INNER JOIN
                      dbo.tblJurisdiction AS j ON j.JurisdictionID = l.JurisdictionID INNER JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = c.ClaimSequenceID LEFT OUTER JOIN
                      dbo.tblDefenseJurisdiction AS dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress AS dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID LEFT OUTER JOIN
                          (SELECT     ClaimLawsuitStatusTrackingID, ClaimLawsuitID, ClaimLawsuitStatusID, StatusNotes, IsPrimaryStatus, NavStatus, ProcessedDate, StatusDate, 
                                                   SPUsername
                            FROM          dbo.tblClaimLawsuitStatusTracking
                            WHERE      (IsPrimaryStatus = 1)) AS clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatus AS cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblPlaintiffCounselAddress AS pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID LEFT OUTER JOIN
                      dbo.tblPlaintiffCounselAddress AS npca ON l.NationalPlaintiffCounselID = npca.PlaintiffCounselAddressID LEFT JOIN
                      dbo.tblCourtType AS ct ON l.CourtTypeID = ct.CourtTypeID
					  			LEFT JOIN tblTrialRisk tr on cl.TrialRiskID = tr.TrialRiskID




GO

GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_LawsuitDetails] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_LawsuitDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_LawsuitDetails] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_LawsuitDetails] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_LawsuitDetails] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_LawsuitDetails] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_LawsuitDetails] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_LawsuitDetails] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_LawsuitDetails] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_LawsuitDetails] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_LawsuitDetails] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_LawsuitDetails] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "cpi"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 288
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 326
               Bottom = 135
               Right = 558
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 596
               Bottom = 135
               Right = 818
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 6
               Left = 856
               Bottom = 135
               Right = 1095
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 1133
               Bottom = 118
               Right = 1303
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "j"
            Begin Extent = 
               Top = 120
               Left = 1133
               Bottom = 215
               Right = 1303
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 233
               Right = 220
            End
            DisplayFlags = 280
            TopCo', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_LawsuitDetails', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'lumn = 0
         End
         Begin Table = "dj"
            Begin Extent = 
               Top = 138
               Left = 258
               Bottom = 267
               Right = 485
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dca"
            Begin Extent = 
               Top = 138
               Left = 523
               Bottom = 267
               Right = 750
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clst"
            Begin Extent = 
               Top = 270
               Left = 498
               Bottom = 399
               Right = 747
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cls"
            Begin Extent = 
               Top = 270
               Left = 785
               Bottom = 399
               Right = 1021
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pca"
            Begin Extent = 
               Top = 348
               Left = 1059
               Bottom = 477
               Right = 1288
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "npca"
            Begin Extent = 
               Top = 234
               Left = 38
               Bottom = 342
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ct"
            Begin Extent = 
               Top = 138
               Left = 788
               Bottom = 216
               Right = 939
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 29
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_LawsuitDetails', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_LawsuitDetails', NULL, NULL
GO
