SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vMissingSettlementDocumentation]
AS
SELECT DISTINCT 
                         TOP (100) PERCENT cpi.ClaimantPersonalInfoID AS ClaimantID, cpi.ClaimantPersonalInfoID, c.ClaimID, cpi.LastName + ', ' + CASE WHEN cpi.FirstName IS NULL 
                         THEN '' ELSE cpi.FirstName + ' ' END + CASE WHEN cpi.MiddleName IS NULL THEN '' ELSE cpi.MiddleName + ' ' END AS ClaimantName, cpi.BirthDate, 
                         cpi.DeceasedDate, clst.StatusDate, cs.SettlementDate, cs.SettlementAmount, cs.ClientAmount, sg.SettlementGroup, dbo.fnGetDOFE(cpi.ClaimantPersonalInfoID) 
                         AS DOFE, cd.Disease, cd.DiseaseDiagnosisDate AS DiagnosisDate, mbs.MedicareBeneficiaryStatus, cs.SettlementGroupID, cl.ClaimLawsuitID, 
                         cs.SettlementsFundsRequestDate, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) 
                         END AS SocialSecurityNumber, cpi.HealthInsuranceClaimNumber AS HICN, COALESCE (dbo.fnMissingClaimantInformation(cl.ClaimLawsuitID), 
                         'All required claimant information submitted.') AS MissingClaimantInfo, COALESCE (dbo.fnMissingDocumentation(cl.ClaimLawsuitID), 
                         'All required documents submitted.') AS MissingDocumentation, cpi.ClaimantPersonalInfoID AS Expr1, cpi.FirstName, cpi.LastName, cpi.MiddleName, 
                         cpi.BirthDate AS DOB, cpi.DeceasedDate AS DOD, CASE WHEN socialsecuritynumber IS NULL THEN 0 ELSE 1 END AS SSN, cl.LawsuitID, 
                         COALESCE (sd.ClaimantIDed, 0) AS ClaimantIDed, CASE WHEN socialsecuritynumber IS NULL THEN 0 ELSE 1 END AS Expr7, CASE WHEN birthdate IS NULL 
                         THEN 0 ELSE 1 END AS Expr8, COALESCE (sd.DxMedicals, 0) AS DxMedicals, CASE WHEN cd.DiseaseID IS NULL THEN 0 ELSE 1 END AS DiseaseID, 
                         CASE WHEN cd.DiseaseDiagnosisDate IS NULL THEN 0 ELSE 1 END AS Expr9, CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE (sd.DODCert, 0) 
                         END AS DODCert, CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE (sd.EstatePapers, 0) END AS Estatepaper, COALESCE (sd.ProductID, 0) 
                         AS ProductID, COALESCE (sd.EmployExpoHist, 0) AS EmployExpoHist, COALESCE (sd.SettlementConfirmation, 0) AS SettlementConfirmation, 
                         CASE WHEN (d_1.ClaimantPersonalInfoID IS NOT NULL OR
                         ExecutedReleaseDate IS NOT NULL) THEN 1 ELSE 0 END AS ReleaseReceived, CASE WHEN du.comments LIKE '%utica %' THEN 1 ELSE 0 END AS UticaPacket, 
                         CASE WHEN l.FileDate IS NOT NULL THEN 1 ELSE 0 END AS FileDate, CASE WHEN l.ServiceDate IS NOT NULL THEN 1 ELSE 0 END AS ServiceDate, 
                         CASE WHEN JurisdictionID IS NOT NULL THEN 1 ELSE 0 END AS Jurisdiction, CASE WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' AND 
                         DeceasedDate IS NOT NULL AND LegalCapacityID IS NULL THEN 0 WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' AND 
                         DeceasedDate IS NOT NULL AND LegalCapacityID = 14 THEN 0 ELSE 1 END AS RPLegalCapacity, CASE WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) 
                         >= '12/5/1980' AND DeceasedDate IS NOT NULL AND RelationshipID IS NULL THEN 0 WHEN relationshipid = 6 OR
                         RelationshipID IS NULL AND crp.firstname IS NOT NULL THEN 0 ELSE 1 END AS RPRelationship, CASE WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) 
                         >= '12/5/1980' AND DeceasedDate IS NOT NULL AND RPAddress1 IS NULL AND cpi.MedicareBeneficiaryStatusID IN (1, 2) THEN 0 ELSE 1 END AS RPaddress, 
                         CASE WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' AND DeceasedDate IS NOT NULL AND RPPhone IS NULL AND 
                         cpi.MedicareBeneficiaryStatusID IN (1, 2) THEN 0 ELSE 1 END AS RPPhone, CASE WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' AND 
                         DeceasedDate IS NOT NULL AND RPSocialSecurityNumber IS NULL AND cpi.MedicareBeneficiaryStatusID IN (1, 2) THEN 0 ELSE 1 END AS RPSSN, 
                         spd.SetPymtID
FROM            dbo.tblClaim AS c INNER JOIN
                         dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                         dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                         dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                         dbo.tblClaimantSettlement AS cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID INNER JOIN
                         dbo.tblClaimLawsuitStatusTracking AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblSettlementGroup AS sg ON cs.SettlementGroupID = sg.SettlementGroupID LEFT OUTER JOIN
                         dbo.tblClaimantEmployment AS ce ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                         dbo.tblSettlePaymentDocumentation AS spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblSettlementDocumentation AS sd ON sd.SetPymtID = spd.SetPymtID LEFT OUTER JOIN
                             (SELECT        cd1.ClaimDiseaseID, cd1.ClaimID, cd1.DiseaseID, cd1.PrimaryDisease, cd1.DiseaseDiagnosisDate, cd1.DoctorID, cd1.SPUsername, 
                                                         cd1.DiseaseID_OLD, cd1.IsDefenseDisease, d1.Disease
                               FROM            dbo.tblClaimDisease AS cd1 INNER JOIN
                                                         dbo.tblDisease AS d1 ON d1.DiseaseID = cd1.DiseaseID
                               WHERE        (cd1.PrimaryDisease = 1) AND (d1.HighLevelDiseaseID <> 5)) AS cd ON c.ClaimID = cd.ClaimID LEFT OUTER JOIN
                         dbo.tblMedicareBeneficiaryStatus AS mbs ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID LEFT OUTER JOIN
                             (SELECT        ce.ClaimantPersonalInfoID, CASE WHEN MAX(JobSiteStateID) IS NULL THEN 0 ELSE 1 END AS ExposureLocationEntered
                               FROM            dbo.tblClaimantEmployment AS ce INNER JOIN
                                                         dbo.tblClaim AS c ON ce.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID INNER JOIN
                                                         dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID
                               GROUP BY ce.ClaimantPersonalInfoID) AS e ON cpi.ClaimantPersonalInfoID = e.ClaimantPersonalInfoID LEFT OUTER JOIN
                             (SELECT        mcd.ClaimantPersonalInfoID
                               FROM            dbo.tblMultiClaimantDocument AS mcd INNER JOIN
                                                         dbo.tblDocuments AS d ON d.DocumentID = mcd.DocumentID
                               WHERE        (d.ClaimantDocumentTypeID = 31)
                               GROUP BY mcd.ClaimantPersonalInfoID) AS d_1 ON d_1.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                             (SELECT        mcd.ClaimantPersonalInfoID, d.Comments
                               FROM            dbo.tblMultiClaimantDocument AS mcd INNER JOIN
                                                         dbo.tblDocuments AS d ON d.DocumentID = mcd.DocumentID
                               WHERE        (d.ClaimantDocumentTypeID = 10)
                               GROUP BY mcd.ClaimantPersonalInfoID, d.Comments) AS du ON du.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                             (SELECT        ClaimantRelatedPartyID, ClaimantPersonalInfoID, FirstName, MiddleName, LastName, RelationshipID, LegalCapacity, LegalCapacityID, RPAddress1, 
                                                         RPAddress2, RPCity, StateID, RPZip, RPMailZip, RPPhone, RPExtension, RPSocialSecurityNumber, RPTIN, SPUserName
                               FROM            dbo.tblClaimantRelatedParty
                               WHERE        (LegalCapacityID IN (1, 2, 3, 4, 5, 6, 7, 8, 15, 17, 30, 31))) AS crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE        (clst.IsPrimaryStatus = 1) AND (clst.ClaimLawsuitStatusID = 25) AND (YEAR(cs.SettlementDate) >= 2010) AND (ce.IsPrimary = 1)

GO
GRANT VIEW DEFINITION ON  [dbo].[vMissingSettlementDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[vMissingSettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vMissingSettlementDocumentation] TO [power_user]
GRANT SELECT ON  [dbo].[vMissingSettlementDocumentation] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 270
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 6
               Left = 308
               Bottom = 135
               Right = 558
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 596
               Bottom = 135
               Right = 818
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 6
               Left = 856
               Bottom = 135
               Right = 1126
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 6
               Left = 1164
               Bottom = 135
               Right = 1413
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clst"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 287
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sg"
            Begin Extent = 
               Top = 138
               Left = 325
               Bottom = 267
               Right = 549
            End
            DisplayFlags = 280
            TopColumn = 0
        ', 'SCHEMA', N'dbo', 'VIEW', N'vMissingSettlementDocumentation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' End
         Begin Table = "ce"
            Begin Extent = 
               Top = 138
               Left = 587
               Bottom = 267
               Right = 803
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "spd"
            Begin Extent = 
               Top = 138
               Left = 841
               Bottom = 267
               Right = 1073
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sd"
            Begin Extent = 
               Top = 138
               Left = 1111
               Bottom = 267
               Right = 1328
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cd"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 399
               Right = 241
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mbs"
            Begin Extent = 
               Top = 270
               Left = 279
               Bottom = 365
               Right = 518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 270
               Left = 556
               Bottom = 365
               Right = 778
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d_1"
            Begin Extent = 
               Top = 270
               Left = 816
               Bottom = 348
               Right = 1030
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "du"
            Begin Extent = 
               Top = 270
               Left = 1068
               Bottom = 365
               Right = 1282
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crp"
            Begin Extent = 
               Top = 348
               Left = 816
               Bottom = 477
               Right = 1036
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vMissingSettlementDocumentation', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vMissingSettlementDocumentation', NULL, NULL
GO
