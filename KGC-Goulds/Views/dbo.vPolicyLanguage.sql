SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vPolicyLanguage]
AS

SELECT ProvisionLanguageID, PolicyID, plang.LanguageID, PolicyLanguage, ExactLanguage, BatesStart, BatesEnd, ProvisionNotes, plang.LocationID, 
Location, EndorsementNum, EffectiveDate, plang.ProvisionLabelID, Provision
FROM tblProvisionLanguage PLANG
LEFT JOIN tblLanguage LANG ON LANG.LanguageID = PLANG.LanguageID
LEFT JOIN tblProvisionLabel PLABEL ON PLABEL.ProvisionLabelID = PLANG.ProvisionLabelID
LEFT JOIN tblLocation LOC ON LOC.LocationID = PLANG.LocationID

GO
GRANT VIEW DEFINITION ON  [dbo].[vPolicyLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[vPolicyLanguage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPolicyLanguage] TO [power_user]
GRANT SELECT ON  [dbo].[vPolicyLanguage] TO [power_user]
GO
