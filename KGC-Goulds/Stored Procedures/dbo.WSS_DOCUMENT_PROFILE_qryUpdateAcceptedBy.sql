SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_PROFILE_qryUpdateAcceptedBy]
    (
      @DocId INT ,
      @AcceptedBy NVARCHAR(250)
    )
AS 
    UPDATE  tblDocuments
    SET     Accepted = 1 ,
            AcceptedBy = @AcceptedBy ,
            DateAccepted = GETDATE()
    WHERE   DocumentID = @DocId
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryUpdateAcceptedBy] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryUpdateAcceptedBy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryUpdateAcceptedBy] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryUpdateAcceptedBy] TO [power_user]
GO
