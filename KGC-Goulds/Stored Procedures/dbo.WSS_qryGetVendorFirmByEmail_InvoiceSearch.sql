SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_qryGetVendorFirmByEmail_InvoiceSearch]
	-- Add the parameters for the stored procedure here
    @emailaddress NVARCHAR(1000)
AS --DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'dpicone@hptylaw.com'

    DECLARE @firmID INT
    SET @firmID = ( SELECT  FirmID
                    FROM    vtblPerson
                    WHERE   Email = @emailaddress
                  )
    BEGIN
        DECLARE @emaildomain NVARCHAR(1000)
        SET @emaildomain = SUBSTRING(@emailaddress,
                                     CHARINDEX('@', @emailaddress),
                                     LEN(@emailaddress) - CHARINDEX('@',
                                                              @emailaddress)
                                     + 1)
        IF @emaildomain = '@kcicllc.com' OR @emailDomain='@kcic.com' OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1)
            OR @firmID IN ( 277 ) 
            BEGIN
                SELECT DISTINCT
                        VendorFirmID ,
                        VendorFirm
                FROM    m_tblVendorFirm
                WHERE   VendorFirmID <> 10122
                UNION
                SELECT  NULL AS VendorFirmID ,
                        '' AS VendorFirm
                UNION
                SELECT  '' AS VendorFirmID ,
                        '<<No Vendor Firm>>' AS VendorFirm
                FROM    m_tblVendorFirm
                WHERE   VendorFirmID <> 10122
                ORDER BY VendorFirm
            END

        ELSE 
            BEGIN
                SELECT DISTINCT
                        VendorFirmID ,
                        VendorFirm
                FROM    m_tblVendorFirm
                WHERE   VendorFirmID <> 10122
                        AND VendorFirmID IN (
                        SELECT  di.VendorFirmID
                        FROM    tblDefenseInvoice di
                                INNER JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselID = di.FirmID
                                INNER JOIN m_tblVendorFirm v ON v.VendorFirmID = di.VendorFirmID
                        WHERE   dca.DefenseCounselID = @firmID )
                UNION
                SELECT  NULL AS VendorFirmID ,
                        '' AS VendorFirm
                UNION
                SELECT  '' AS VendorFirmID ,
                        '<<No Vendor Firm>>' AS VendorFirm
                FROM    m_tblVendorFirm
                WHERE   VendorFirmID <> 10122
                ORDER BY VendorFirm
            END
		
	 
    END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetVendorFirmByEmail_InvoiceSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetVendorFirmByEmail_InvoiceSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetVendorFirmByEmail_InvoiceSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetVendorFirmByEmail_InvoiceSearch] TO [power_user]
GO
