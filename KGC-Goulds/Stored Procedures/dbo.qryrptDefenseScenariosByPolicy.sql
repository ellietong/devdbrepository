SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptDefenseScenariosByPolicy] (@LimitTypeID int, @ScenarioID int)

As
SET NOCOUNT ON

--DECLARE @LimitTypeID int
--DECLARE @ScenarioID int
--SET @LimitTypeID = 1
--SET @ScenarioID = 0

DECLARE @maxScenarioID int
SELECT @maxScenarioID = MAX (ScenarioID) from tblScenario

DECLARE @PolicyTreatments table (policyid int, comboid int, treatmentid int, scenarioid int)

-------------------------------------------------------------------
-------------------------------------------------------------------
DECLARE @Scenario int
DECLARE Scenario_Curs CURSOR FOR
		
SELECT s.ScenarioID
FROM tblScenario s
INNER JOIN tblScenarioProvisions sp ON s.ScenarioID = sp.ScenarioID
WHERE s.ScenarioID >= COALESCE(@ScenarioID, 0) AND s.ScenarioID <= COALESCE(NULLIF(@ScenarioID, 0), @maxScenarioID) 
ORDER BY s.ScenarioID

OPEN Scenario_Curs 

FETCH NEXT FROM Scenario_Curs INTO @Scenario

WHILE @@fetch_status = 0
BEGIN
-------------------------------------------------------------------
-------------------------------------------------------------------


--Create temp table
CREATE TABLE #tempPolicyCombos(PolicyID int)

--Create temp table
CREATE TABLE #tempCurrentCombos(ComboID int)

--Create string to know what the columns of your pivot table should be
DECLARE @pivotColumns nvarchar(2000)
SET @pivotColumns = ''
SELECT @pivotColumns =   @pivotColumns + '[' + cast(ProvisionLabelID AS nvarchar(10)) + '],'
FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @pivotColumns = LEFT(@pivotColumns, LEN(@pivotColumns) - 1)

--Create string to know what the columns of your  temp table should be 
--(same columns as @pivotColums just different format for how it will be used)
DECLARE @alterColumns nvarchar(2000)
SET @alterColumns = ''
 SELECT @alterColumns = @alterColumns + '[' + cast(ProvisionLabelID AS varchar) + '] int,'
 FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @alterColumns = LEFT(@alterColumns, LEN(@alterColumns) - 1)

--Create a string to see if any of the provisions are not coded for a particular policy
--(same columns as @pivotColums just different format for how it will be used)
DECLARE @SumofColumns nvarchar(2000)
SET @SumofColumns = ''
 SELECT @SumofColumns = @SumofColumns + '[' + cast(ProvisionLabelID as varchar) + '] +'
 FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @SumofColumns = LEFT(@SumofColumns, LEN(@SumofColumns) - 1)


--Create a string to see if any of the provisions are not coded for a particular policy
--(same columns as @pivotColums just different format for how it will be used)
DECLARE @ColumnsJoin nvarchar(2000)
SET @ColumnsJoin = ''
 SELECT @ColumnsJoin = @ColumnsJoin + 'a.[' + cast(ProvisionLabelID as varchar) + '] = b.['+ cast(ProvisionLabelID as varchar) + '] AND '
 FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @ColumnsJoin = LEFT(@ColumnsJoin, LEN(@ColumnsJoin) - 4)

--select @ColumnsJoin


DECLARE @sql nvarchar(4000)
--Update temp table for policy data to have appropriate columns
SET @sql = 'ALTER TABLE #tempPolicyCombos
ADD ' + @alterColumns
EXEC(@sql)
--perform transform on Policy Provision data and insert it into temp table
SET @sql = 'INSERT #tempPolicyCombos
SELECT  * 
FROM
(SELECT policyid, provisionlabelid, provisionvalueid
FROM tblProvision) datatable
pivot
(MIN(provisionvalueid) for provisionlabelid in (' + @pivotColumns + ')) AS pvt'
EXEC (@sql)


--Update temp table for current combination data to have appropriate columns
SET @sql = 'ALTER TABLE #tempCurrentCombos
ADD ' + @alterColumns
EXEC(@sql)
--perform transform on current combination data and insert it into temp table  
SET @sql = 'INSERT #tempCurrentCombos
select * 
from
(select combination, provisionlabelid, provisionvalueid
from tblcombination where combination in (Select Combination from
tblCombinationTreatment where scenarioid = ' +CONVERT(nvarchar(50), @scenario) + ' )) datatable
pivot
(min(provisionvalueid) for provisionlabelid in ('+@pivotColumns+ ')) pivottable'
EXEC (@sql)

SET @sql= '
select PolicyID, comboid, treatmentID, ScenarioID
from #tempCurrentCombos a
inner join #tempPolicyCombos b on ' + @ColumnsJoin + ' 
inner join tblCombinationTreatment c on c.combination = a.comboID
where ScenarioID = ' + CONVERT(nvarchar(50), @scenario)
INSERT INTO @PolicyTreatments
EXEC (@sql)

DROP TABLE #tempPolicyCombos
DROP TABLE #tempCurrentCombos


--Repeat for next scenario
FETCH NEXT FROM Scenario_Curs INTO @Scenario
END			
CLOSE Scenario_Curs
DEALLOCATE Scenario_Curs


select Scenario, t.Treatment, comboid, pt.TreatmentID, pt.ScenarioID, CarrierGroupName, CarrierName,
CarrierSolvency, PolicyNum, MIN(PStartDate) AS PStartDate, MAX(PEndDate) AS PEndDate,
AttachmentPoint, PerOccLimit, LayerPerOccLimit, AggregateLimit 
from @PolicyTreatments pt
INNER JOIN vPolicy p ON p.PolicyID = pt.policyid
LEFT JOIN tblTreatment t ON pt.treatmentid = t.TreatmentID
INNER JOIN tblScenario s ON s.ScenarioID = pt.scenarioid
WHERE LimitTypeID = @LimitTypeID
GROUP BY Scenario, t.Treatment, comboid, pt.TreatmentID, pt.ScenarioID, CarrierGroupName, CarrierName,
CarrierSolvency, PolicyNum,
AttachmentPoint, PerOccLimit, LayerPerOccLimit, AggregateLimit 
ORDER BY pt.ScenarioID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseScenariosByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseScenariosByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseScenariosByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseScenariosByPolicy] TO [power_user]
GO
