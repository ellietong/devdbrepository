SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_INVOICES_qryRecentInvoices] (@emailaddress nvarchar(1000) = NULL)
AS

--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'hagep@kcicllc.com'

DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE dt.ClaimantDocumentGroupID = 2 AND di.DefenseInvoiceID IS NOT NULL'

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF ((@DefenseCounselID = 277) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' '
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND dinv.FirmID = ' + CONVERT(nvarchar(20),@DefenseCounselID)
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
    'SELECT DISTINCT TOP 200
        CAST(d.DocumentID AS VARCHAR(50)) AS DocumentId ,
        ClaimantDocumentType ,
        dg.ClaimantDocumentGroup ,
        d.Comments ,
        d.DocumentURL ,
        d.DocumentTitle ,
        d.CreatedBy ,
        d.DateCreated ,
        d.ModifiedBy ,
        d.DateModified ,
        d.Accepted ,
        d.AcceptedBy ,
        d.DateAccepted ,
        di.ApprovedBy ,
        di.DateApproved ,
        f.Firm ,
        di.InvoiceNumber ,
        di.Period ,
        di.DatePayable ,
        di.fees AS FeeAmount ,
        di.costs AS DisbursementAmount ,
        di.adjustments AS Adjustment ,
        di.InvoiceNotes AS AdjustmentNotes ,
        dis.Status ,
        '''' AS DefenseInvoiceFirm ,
        c.sourcedate AS DatePaid ,
        c.SourceNumber AS CheckNumber ,
        di.FirmID ,
		COALESCE(di.Firm, '''') AS VendorFirm, 
        c.SourceDate ,
        di.InvoiceTotal AS InvoiceTotal
FROM    tblDocuments d
        INNER JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
        INNER JOIN tblClaimantDocumentGroup dg ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
        LEFT JOIN (SELECT * FROM vtblDefenseInvoice WHERE InvoiceLink IS NULL ) di ON d.DocumentID = di.DocumentID 
        LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID
        LEFT JOIN tblDefenseInvoice dinv ON dinv.DocumentID = d.DocumentID
        LEFT JOIN vtblFirm f ON dinv.FirmID = f.FirmID
        LEFT JOIN m_tblVendorFirm ex ON dinv.VendorFirmID = ex.VendorFirmID
        LEFT JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
        LEFT JOIN tblCheck c ON pd.CheckID = c.CheckID ' +
    @WhereString + '
	ORDER BY Accepted ASC, DateCreated DESC
	OPTION (RECOMPILE)'

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryRecentInvoices] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryRecentInvoices] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryRecentInvoices] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryRecentInvoices] TO [power_user]
GO
