SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE PROC [dbo].[WSS_REPORTS_SUMMARY_qryDismissedCasesByJurisdiction]
(@MatterID int,@BeginDate DATETIME, @EndDate DATETIME, @EmailAddress nvarchar(250))

As
Set NoCount On
SET @EmailAddress = 'hagep@kcic.com'
--DECLARE	@MatterID int = null,
--		@BeginDate DATETIME = '1/1/2005',
--		@EndDate DateTime = '1/1/2007',
--		@EmailAddress nvarchar(max) = 'owenss@kcic.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)


DECLARE @WhereString NVARCHAR(MAX) 
SET @WhereString = ' WHERE  COALESCE(IsPrimaryStatus, 0) = 1
							AND cst.ClaimLawsuitStatusID IN (15, 16, 17, 27, 28, 19, 34, 35, 37, 41)
						    AND cst.StatusDate between ''' + COALESCE(CONVERT(NVARCHAR, @BeginDate),'1/1/'+CONVERT(NVARCHAR(4), YEAR(GETDATE()))) + ''' AND ''' + COALESCE(CONVERT(NVARCHAR, @EndDate), CONVERT(NVARCHAR,GETDATE())) + ''''

DECLARE @GroupByString NVARCHAR(MAX) 
SET @GroupByString = 'GROUP BY'

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @GroupByString = @GroupByString + ' dj.DefenseCounselAddressID '
END

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @QueryString NVARCHAR(MAX)

 	IF object_id('tempdb..#DiseaseCount') IS NOT NULL
	BEGIN
	   DROP TABLE #DiseaseCount
	END
	CREATE TABLE #DiseaseCount 
(Matter NVARCHAR(100), MatterID int, claimlawsuitstatus NVARCHAR (500), claimlawsuitid int,
	MoYearDismissed DATETIME, YearDismissed DATETIME,MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int, unknowncount1 int)

SET @QueryString = 
	'SELECT DISTINCT Matter, C.MatterID,  css.claimlawsuitstatus, cl.claimlawsuitid,
 CONVERT(DATETIME,(CONVERT(NVARCHAR,Month(cst.statusDate))+''/1/''+ CONVERT(NVARCHAR,Year(cst.StatusDate)))) as MoYearDismissed,
  CONVERT(NVARCHAR,Year(cst.StatusDate)) as YearDismissed,
--Meso
CASE	WHEN DMeso.HighLevelDiseaseID = 1 THEN  1
ELSE 0 END AS MesoCount,
--Lung Cancer
CASE	WHEN DLC.HighLevelDiseaseID = 2 THEN  1
ELSE 0 END as LungCancerCount,
--Other Cancer
CASE	WHEN DOC.HighLevelDiseaseID = 3 THEN  1
ELSE 0 END as OtherCancerCount,
--NonMalig, Silicosis, Pneumoconiosis
CASE	WHEN DNM.HighLevelDiseaseID IN (4,7,8) THEN  1
ELSE 0 END as NonMaligCount,
--Unknown
CASE	WHEN DUnk.HighLevelDiseaseID IN (5,6) THEN  1
ELSE 0 END AS UnknownCount,
CASE	WHEN DUnk.HighLevelDiseaseID is NULL THEN  1
ELSE 0 END AS UnknownCount1


FROM tblClaimantPersonalInfo CPI
LEFT JOIN  tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (SELECT CD.ClaimDiseaseID,CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease FROM tblClaimDisease CD WHERE PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
LEFT JOIN tblClaimSequence csq ON c.ClaimSequenceID = csq.ClaimSequenceID
--Meso
LEFT JOIN  tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
LEFT JOIN  tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
LEFT JOIN  tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
LEFT JOIN  tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Unknown
LEFT JOIN  tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID


LEFT JOIN  tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
LEFT JOIN  tblLawsuit L on CL.LawsuitID=L.LawsuitID
		' + @JoinString + '
LEFT JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID = CL.ClaimLawsuitID
LEFT JOIN tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblClaimLawsuitStatus css on css.ClaimLawsuitStatusID = cst.ClaimLawsuitStatusID
	' + @WhereString + '
	
	
	'

 	IF object_id('tempdb..#DiseaseCountTemp') IS NOT NULL
	BEGIN
	   DROP TABLE #DiseaseCountTemp
	END
	CREATE TABLE #DiseaseCountTemp 
(Matter NVARCHAR(100), MatterID int, claimlawsuitstatus NVARCHAR (500), claimlawsuitid int,
	MoYearDismissed DATETIME, YearDismissed DATETIME,MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int, unknowncount1 int)


Insert Into #DiseaseCountTemp
EXEC sp_executesql @QueryString


SELECT SUM(MesoCount) as MesoCount,
	SUM(LungCancerCount) as LungCancerCount, SUM(OtherCancerCount) as OtherCancerCount,
	SUM(NonMaligCount) as NonMaligCount, SUM(UnknownCount+unknowncount1) as UnknownCount, @BeginDate as StartDate, @EndDate as ENDDate,
MoYearDismissed, 		YearDismissed, DATENAME(month, MoYearDismissed) AS Month, DATENAME(year, MoYearDismissed) As Year,
SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + unknowncount1) AS MonthTotal , DATENAME(month, MoYearDismissed) + ' ' + DATENAME(year, MoYearDismissed) AS MonthYear
FROM #DiseaseCountTemp
GROUP BY MoYearDismissed, YearDismissed

Return









GO
