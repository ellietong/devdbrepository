SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null,@StartDate DATETIME=null, @EndDate DATETIME=null, @EmailAddress NVARCHAR(250))  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--DECLARE	@MatterID int = null,
--		@StartDate DATETIME = '1/1/2005',
--		@EndDate DateTime = '1/1/2007',
--		@EmailAddress nvarchar(max) = 'owenss@kcic.com'
SET @EmailAddress = 'hagep@kcic.com'
DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)


DECLARE @WhereString NVARCHAR(MAX) 
SET @WhereString = ' WHERE COALESCE(SettlementDate,statusdate) between ''' + COALESCE(CONVERT(NVARCHAR, @StartDate),'1/1/1900') + ''' AND ''' + COALESCE(CONVERT(NVARCHAR, @EndDate), '1/1/2100') + '''
		AND ClaimLawsuitStatusID IN (23,25) 
		AND  IsPrimaryStatus=1'

DECLARE @GroupByString NVARCHAR(MAX) 
SET @GroupByString = 'GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN  ''Unknown'' ELSE hld.HighLevelDisease END '

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @GroupByString = @GroupByString + ' ,dj.DefenseCounselAddressID '

END

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @QueryString NVARCHAR(MAX)

 	IF object_id('tempdb..#Settlement') IS NOT NULL
	BEGIN
	   DROP TABLE #Settlement
	END
	CREATE TABLE #Settlement 
(HighLevelDisease NVARCHAR(25), Count int, Settlement money, AverageIndemnity money)


SET @QueryString = 
	'SELECT DISTINCT  CASE WHEN hld.HighLevelDisease IS NULL THEN  ''Unknown'' ELSE hld.HighLevelDisease END as HighLevelDisease, COUNT(distinct(cl.ClaimLawsuitID)) as Count, SUM(SettlementAmount) as IndemnityPaid, SUM(SettlementAmount)/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
	FROM tblClaim c
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID=l.LawsuitID
		' + @JoinString + '
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
	INNER JOIN (SELECT * FROM tblClaimDisease WHERE primarydisease=1) cd ON c.ClaimID=cd.ClaimID
	INNER JOIN tblClaimantSettlement cs ON cs.ClaimLawsuitID=cl.ClaimLawsuitID
	LEFT JOIN tblDisease d ON d.DiseaseID=cd.DiseaseID
	LEFT JOIN tblHighLevelDisease hld ON hld.HighLevelDiseaseID=d.HighLevelDiseaseID
	' + @WhereString + '
	' + @GroupByString + '
	'

 	IF object_id('tempdb..#SettlementTemp') IS NOT NULL
	BEGIN
	   DROP TABLE #SettlementTemp
	END
	CREATE TABLE #SettlementTemp 
(HighLevelDisease NVARCHAR(25), Count int, Settlement money, AverageIndemnity money)


Insert Into #SettlementTemp
EXEC sp_executesql @QueryString


SELECT hld.HighLevelDisease, COALESCE(Count,0) AS Count, COALESCE(Settlement,0) AS IndemnityPaid, COALESCE(AverageIndemnity,0) AS AverageIndemnity
FROM tblHighLevelDisease hld
LEFT JOIN #SettlementTemp s ON hld.HighLevelDisease=s.HighLevelDisease
WHERE HighLevelDiseaseID !=6
ORDER BY COUNT desc

END








GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByDiseaseByYear] TO [power_user]
GO
