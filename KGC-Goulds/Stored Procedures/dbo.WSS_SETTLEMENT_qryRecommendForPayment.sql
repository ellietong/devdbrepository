SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_SETTLEMENT_qryRecommendForPayment]
(@ClaimantSettlementID bigint, @IsRecommended bit, @Emailaddress nvarchar(250))
AS
SET NOCOUNT ON

UPDATE tblClaimantSettlement
SET PaymentRecommendedDate = CASE WHEN @IsRecommended = 1 THEN GETDATE() ELSE NULL END,
	PaymentRecommendedBy = CASE WHEN @IsRecommended = 1 THEN @Emailaddress ELSE NULL END
WHERE ClaimantSettlementID = COALESCE(@ClaimantSettlementID, 0)



GO
