SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author: 
-- Create date: 
-- Description: 
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryGetLastNameByID]
    (
      @ClaimantPersonalInfoID nvarchar(250)
    )
AS 
    BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
        SET NOCOUNT ON;
--DECLARE @ClaimantpersonalInfoID bigint
--SET @ClaimantpersonalInfoID = '3010445894'

        SELECT TOP 1
                LTRIM(RTRIM(LastName)) AS LastName ,
                cpi.ClaimantPersonalInfoID ,
                cpi.INJ_PRTY_ID
        FROM    tblClaimantPersonalInfo cpi
                LEFT JOIN m_tblClaimantPersonalInfo_Lookup cpil ON cpi.ClaimantPersonalInfoID = cpil.ClaimantPersonalInfoID
        WHERE   CONVERT(NVARCHAR(250),cpi.ClaimantPersonalInfoID) LIKE LTRIM(RTRIM(@ClaimantpersonalInfoID))
                OR cpi.Reference_ID LIKE LTRIM(RTRIM(@ClaimantpersonalInfoID))

    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryGetLastNameByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryGetLastNameByID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryGetLastNameByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryGetLastNameByID] TO [power_user]
GO
