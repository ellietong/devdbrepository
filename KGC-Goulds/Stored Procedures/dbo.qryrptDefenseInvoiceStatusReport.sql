SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptDefenseInvoiceStatusReport]

As
SET NOCOUNT ON

SELECT	Firm, PayTo, InvoiceNumber, Period, InvoiceTotal,SubmitDate,DatePayable, FeeAmount,
	DisbursementAmount, Adjustment, DI.StatusID, Status,
CASE	When (FeeAmount is null AND DisbursementAmount is null) or (FeeAmount+DisbursementAmount=0) Then InvoiceTotal
	Else FeeAmount+DisbursementAmount
	End as Subtotal,
CASE	When (FeeAmount is null AND DisbursementAmount is null) or (FeeAmount+DisbursementAmount=0) Then InvoiceTotal
	Else FeeAmount+DisbursementAmount-Adjustment
	End as AmountPayable
FROM	tblDefenseInvoice DI
INNER JOIN	tblDefenseInvoiceFirm DIF ON DI.FirmID = DIF.FirmID
INNER JOIN	tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
WHERE	DIS.StatusID = 3
ORDER BY Firm, Status, DatePayable

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseInvoiceStatusReport] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseInvoiceStatusReport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseInvoiceStatusReport] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseInvoiceStatusReport] TO [power_user]
GO
