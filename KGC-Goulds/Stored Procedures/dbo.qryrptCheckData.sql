SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptCheckData] as
SET NOCOUNT ON

--declare @CheckData Table (
--checkdate datetime,
--Checkno int,
--plaintiffcounselname nvarchar (200),
--pcounseladdress nvarchar (max),
--checkamount money,
--amountdesc nvarchar (max),
--fundingtypeID int,
--matter nvarchar (100),
--settlementgroup int,
--[CountofSettlements] int, 
--[CountOfTotalSettlements] int,
--[PaymentAmount] money,
--[SettlementAmount] money,
--checkstatus nvarchar (10))

--Insert into @CheckData
Select
'9/16/2011' as checkdate,
3246 as Checkno,
'LOCKS LAW FIRM' as plaintiffcounselname,
'1500 Walnut Street, 20th Floor
Philadelphia, PA 19102' as pcounseladdress,
85000 as checkamount,
dbo.fnCurrencyToWords (85000) as amountdesc,
2 as fundingtypeID,
'Weil-McLain' as matter,
244 as settlementgroup, 
1 as [CountofSettlements], 
1 as [CountOfTotalSettlements],
85000 as [PaymentAmount],
85000 as [SettlementAmount],
'' as checkstatus

return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckData] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckData] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckData] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckData] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckData] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckData] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckData] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckData] TO [power_user]
GO
