SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryInvoiceYears]
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @Year INT
        SET @Year = 2011--DATEPART(year, DATEADD(year,-3,GETDATE()))
    
        DECLARE @Years TABLE ( [Year] INT )
        WHILE @Year <= YEAR(GETDATE()) 
            BEGIN
                INSERT  INTO @Years
                        SELECT  @Year
                SET @Year = @Year + 1
    
            END
        SELECT  NULL AS Year
        UNION
        SELECT  [YEAR]
        FROM    @Years
    END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInvoiceYears] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInvoiceYears] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInvoiceYears] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInvoiceYears] TO [power_user]
GO
