SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptDefenseClaimList_ByCarrier] (@OccurrenceGroupID int, @CarrierID int)
As
SET NOCOUNT ON

--Declare @OccurrenceGroupID int
--Declare @CarrierID int
--Set @OccurrenceGroupID = 102
--SET @CarrierID = 94



--Pull allocation amounts for requested Carrier
DECLARE @CarrierAllocation Table (OccurrenceID int, AllocatedAmount float, CarrierName Nvarchar (100))
Insert into @CarrierAllocation
SELECT OC.OccurrenceID, sum(a.Allocation) as AllocatedAmount, CarrierName
from tblAllocationResults A
Join tblOccurrence OC on OC.OccurrenceID = A.OccurrenceID
Join tblPolicy P on p.PolicyID = a.PolicyID
Join tblCarrier C on c.CarrierID = p.CarrierID
where oc.OccurrenceGroupID = @OccurrenceGroupID AND p.CarrierID = @CarrierID
and occurrencetypeid = 2
Group by OC.OccurrenceID, CarrierName


Select di.defenseinvoiceid, upper(defensecounsel) as DefenseCounsel, vendorfirm as Vendor, dic.claimantpersonalinfoid as ClaimantID, lastname+', '+firstname as ClaimantName, 
'XXX-XX-'+right(coalesce(socialsecuritynumber,''),4) as socialsecuritynumber, s.state, jurisdiction, ltrim(rtrim(dic.invoicenumber)) as invoicenumber, 
case when cost1<0 then fees*1 else fees end as fees, 
case when cost1<0 then costs*1 else costs end as costs,
case when cost1<0 then (fees+costs)*1 else (fees+Costs) end as Total, 
case when cost1<0 then (fees+costs-adjustments)*1 else (fees+costs-adjustments) end as AdjustedTotal, 
case when dic.DOFE = 9999 then 1967 else dic.dofe end as DOFE,
Coalesce(convert(money,allocatedamount), 0) as AllocAmount, cost1,
coalesce(CarrierName,'') as CarrierName, 
OccurrenceTypeID
from tbldefenseinvoice di
inner join tbldefinvclaimantdetail dic on dic.documentid = di.documentid
inner join tblpaymentdefense pd on pd.defenseinvoiceid = di.defenseinvoiceid
inner join tblcheck ch on ch.checkid = pd.checkid
left join tblclaimantpersonalinfo cpi on cpi.claimantpersonalinfoid = dic.claimantpersonalinfoid
left join (select * from tblclaim where iscurrentclaim =1) c on c.claimantpersonalinfoid = cpi.claimantpersonalinfoid
left join (select * from tblclaimlawsuit where isprimaryforclaim = 1) cl on cl.claimid = c.claimid
left join tbllawsuit l on l.lawsuitid = cl.lawsuitid
left join tbljurisdiction j on j.jurisdictionid = l.jurisdictionid
left join tblstate s on s.stateid = l.stateid
left join tbldefensecounseladdress dca on dca.defensecounseladdressid = di.defensecounseladdressid
left join m_tblVendorFirm v on v.VendorFirmID = di.VendorFirmID
left join tblOccurrence o on dic.InvoiceDetailID = o.OccurrenceStateID
--left join (select * from tbloccurrence where occurrencegroupid = @OccurrenceGroupID and occurrencetypeid = 2) o on o.claimid = dic.claimantpersonalinfoid and 
--		  --abs(round(o.cost1,2)) = abs(round((fees+costs-adjustments),2)) and di.defenseinvoiceid = convert(int,o.occurrencenote)
--		  abs(o.cost1) = abs(fees+costs-adjustments) and di.defenseinvoiceid = convert(int,o.occurrencenote)
--and o.occurrencename <> dic.invoicenumber
left join @carrierallocation ca on ca.occurrenceid = o.occurrenceid
where --sourcedate between '6/1/2013' and '6/30/2013'
month(sourcedate) = month(expectedpaydate) and year(sourcedate) = year(expectedpaydate)
and 
FundingSourceID <>3
group by di.defenseinvoiceid, defensecounsel, vendorfirm, dic.claimantpersonalinfoid , lastname+', '+firstname , 
'XXX-XX-'+right(coalesce(socialsecuritynumber,''),4) , s.state, jurisdiction, ltrim(rtrim(dic.invoicenumber)) , 
case when cost1<0 then fees*1 else fees end, 
case when cost1<0 then costs*1 else costs end ,
case when cost1<0 then (fees+costs)*1 else (fees+Costs) end, 
case when cost1<0 then (fees+costs-adjustments)*1 else (fees+costs-adjustments) end , 
case when dic.DOFE = 9999 then 1967 else dic.dofe end,
Coalesce(convert(money,allocatedamount), 0) , cost1,
coalesce(CarrierName,''), 
OccurrenceTypeID
 having sum(fees+costs-adjustments)<>0
--compute sum(case when cost1<0 then (fees+costs-adjustments)*1 else (fees+costs-adjustments) end), sum(Coalesce(convert(money,allocatedamount), 0))







GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier] TO [power_user]
GO
