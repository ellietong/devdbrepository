SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_REPORTS_qryClaimantInfoCategoryList]
AS
SET NOCOUNT ON

SELECT *, CASE WHEN ClaimantInfoCategoryID = 1001 THEN 'SSN' 
			   WHEN ClaimantInfoCategoryID = 1002 THEN 'BirthDate'
			   WHEN ClaimantInfoCategoryID = 1003 THEN 'DiseaseID' 
			   WHEN ClaimantInfoCategoryID = 1004 THEN 'DiagnosisDate' 
			   WHEN ClaimantInfoCategoryID = 1005 THEN 'ProductEntered' 
			   WHEN ClaimantInfoCategoryID = 1006 THEN 'LegalCapacity' 
			   WHEN ClaimantInfoCategoryID = 1007 THEN 'FileDate'
			   WHEN ClaimantInfoCategoryID = 1008 THEN 'ServiceDate'
			   WHEN ClaimantInfoCategoryID = 1009 THEN 'Jurisdiction'
			   WHEN ClaimantInfoCategoryID = 1010 THEN 'RPLegalCapacity'
			   WHEN ClaimantInfoCategoryID = 1011 THEN 'RPRelationship'
			   WHEN ClaimantInfoCategoryID = 1012 THEN 'RPaddress'
			   WHEN ClaimantInfoCategoryID = 1013 THEN 'RPPhone'
			   WHEN ClaimantInfoCategoryID = 1014 THEN 'RPSSN'
			   END AS MSDCategory
FROM tblClaimantInformationCategory
WHERE ClaimantInfoCategoryID IN (1001, 1002, 1003, 1004)
UNION
SELECT NULL, '', ''
ORDER BY ClaimantInfoCategory



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryClaimantInfoCategoryList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryClaimantInfoCategoryList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryClaimantInfoCategoryList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryClaimantInfoCategoryList] TO [power_user]
GO
