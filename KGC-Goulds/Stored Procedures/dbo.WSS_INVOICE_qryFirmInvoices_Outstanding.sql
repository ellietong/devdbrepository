SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] (@emailaddress nvarchar(1000) = NULL)
AS

--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'hagep@kcicllc.com'

DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE dt.ClaimantDocumentGroupID = 2 AND (di.StatusID IS NULL OR di.StatusID = 3) AND di.DefenseInvoiceID IS NOT NULL'

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF ((@DefenseCounselID = 277) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'))
	SET @JoinString = ' '
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND dinv.FirmID = ' + CONVERT(nvarchar(20),@DefenseCounselID)
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT CAST(d.DocumentID as varchar(50)) as DocumentId,
		d.DocumentTitle,
		d.DocumentURL,				
		di.FirmID,
		f.Firm,		
		dt.ClaimantDocumentType, 
		di.Period,
		dinv.InvoiceTotal,
		di.DatePayable,
		dis.[Status],
		d.AcceptedBy,
		d.DateAccepted,
		d.DateCreated,
		Accepted,
		COALESCE(dinv.Firm, '''') AS VendorFirm 
	FROM tblDocuments d
		INNER JOIN tblClaimantDocumentType dt	ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
		INNER JOIN tblClaimantDocumentGroup dg	ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
		LEFT JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
		LEFT JOIN vtblDefenseInvoice dinv		ON dinv.DocumentID = d.DocumentID
		LEFT JOIN  tblDefenseInvoiceStatus dis	ON di.StatusID = dis.StatusID 
		LEFT JOIN  vtblFirm f					ON di.FirmID = f.FirmID
		LEFT JOIN  tblPaymentDefense pd			ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN  tblCheck c					ON pd.CheckID = c.CheckID
		LEFT JOIN m_tblVendorFirm ex		  ON di.VendorFirmID = ex.VendorFirmID ' +
    @WhereString + '
	ORDER BY Accepted ASC, DateCreated DESC
	OPTION (RECOMPILE)'

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [power_user]
GO
