SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptPolicyReportsSplitGrouped]
(@policyid int = null)
As
set nocount on

--DECLARE @PolicyID INT
--SET @policyid = 2

DECLARE @MAXpolicyid INT
-- Find max ID values
SELECT @MAXpolicyid = MAX(policyid) FROM tblPolicy

DECLARE @Provisions TABLE (PolicyID INT, Provision nvarchar(100), ProvisionValue nvarchar(255))
INSERT INTO @Provisions

SELECT PolicyID, Provision, ProvisionValue
FROM vProvisions pv
WHERE ProvisionValueID = 15
GROUP BY PolicyID, Provision, ProvisionValue

DECLARE @Policy TABLE (PolicyID int, PolicyNum nvarchar(100), CarrierID int, CarrierName nvarchar(255), PolicyNotes nvarchar (255),
InitialDataEntry nvarchar(50), InitialDataEntryDate datetime, PolicyReview nvarchar(50), 
PolicyReviewDate datetime, NamedInsuredID int, NamedInsured nvarchar(255), InsuranceProgramID int, InsuranceProgram nvarchar(255),
PStartDate datetime, PEndDate datetime, AnnualPeriod int)
INSERT INTO @Policy

SELECT PolicyID, PolicyNum, CarrierID, CarrierName, PolicyNotes, InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate,
NamedInsuredID, NamedInsured, InsuranceProgramID, InsuranceProgram, PStartDate, PEndDate, AnnualPeriod
FROM vPolicy
where DoNotReport <>1
GROUP BY PolicyID, PolicyNum, CarrierID, CarrierName, PolicyNotes, InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate,
NamedInsuredID, NamedInsured, InsuranceProgramID, InsuranceProgram, PStartDate, PEndDate, AnnualPeriod

SELECT
InsuranceProgram,
a.PolicyID, CarrierName,
PolicyNum, PStartDate AS Splitstart, PEndDate AS splitend,
AnnualPeriod AS SplitAnnualPeriod,
NamedInsured,
PolicyNotes,
InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate, 
CASE WHEN ProvisionValue IS NULL THEN 'N/A' ELSE ProvisionValue END AS ProvisionValue

FROM @Policy A
LEFT JOIN @Provisions pv ON A.PolicyID = pv.PolicyID
WHERE a.policyid >= COALESCE(@policyid, 0) AND a.policyid <= COALESCE(NULLIF(@policyid, 0), @MAXpolicyid)

GROUP BY  InsuranceProgram,
a.PolicyID, CarrierName,
PolicyNum, PStartDate, PEndDate, AnnualPeriod, NamedInsured, PolicyNotes,
InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate, 
CASE WHEN ProvisionValue IS NULL THEN 'N/A' ELSE ProvisionValue END

ORDER BY InsuranceProgram, AnnualPeriod, PStartDate
return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplitGrouped] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplitGrouped] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplitGrouped] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplitGrouped] TO [power_user]
GO
