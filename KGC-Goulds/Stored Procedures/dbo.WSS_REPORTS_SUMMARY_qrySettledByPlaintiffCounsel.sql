SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROC [dbo].[WSS_REPORTS_SUMMARY_qrySettledByPlaintiffCounsel] ( @SettlementDateStart  NVARCHAR(250),	@SettlementDateEND  NVARCHAR(250), @MatterID int, @EmailAddress nvarchar(250)) 

as

Set NoCount On
SET @EmailAddress = 'hagep@kcic.com'
--DECLARE @MatterID int = null,
--		@SettlementDateStart DATETIME = '1/1/2005',
--		@SettlementDateEnd DateTime = '1/1/2007',
--		@EmailAddress nvarchar(max) = 'owenss@kcic.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @EmailAddress)


DECLARE @WhereString NVARCHAR(MAX) 
SET @WhereString = ' WHERE COALESCE(settlementdate, statusdate) >= ''' + COALESCE(CONVERT(NVARCHAR, @SettlementDateStart),'1/1/'+CONVERT(NVARCHAR(4), YEAR(GETDATE()))) + ''' AND COALESCE(settlementdate, statusdate) <= ''' + COALESCE(CONVERT(NVARCHAR, @SettlementDateEND), CONVERT(NVARCHAR,GETDATE())) + ''' 
						 AND ClaimLawsuitStatusID IN (23,25)
						 AND COALESCE(cst.IsPrimaryStatus,0) = 1 '

DECLARE @GroupByString NVARCHAR(MAX) 
SET @GroupByString = 'GROUP BY PlaintiffCounsel'


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@EmailAddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @EmailAddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @GroupByString = @GroupByString + ' ,dj.DefenseCounselAddressID '
END

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @QueryString NVARCHAR(MAX)

 	IF object_id('tempdb..#SettlementSummary') IS NOT NULL
	BEGIN
	   DROP TABLE #SettlementSummary
	END
	CREATE TABLE #SettlementSummary (ClaimNumber int, 
							
				SettlementAmount money,
				SettlementAverage money,
				
				Meso_Count int,
				Meso_SettlementAmount money,
				Meso_Avg money,
				LungCancer_Count int,
				LungCancer_SettlementAmount money,
				LungCancer_Avg money,
				OtherCancer_Count int,
				OtherCancer_SettlementAmount money,
				OtherCancer_Avg money,
				NonMalig_Count int,
				NonMalig_SettlementAmount money,
				NonMalig_Avg money,
				Other_Count int,
				Other_SettlementAmount money,

				NoDisease_Count int,
				NoDisease_SettlementAmount money,

				Null_Count int,
				Null_SettlementAmount money,
				PlaintiffCounsel NVARCHAR(200))


SET @QueryString = 
	'SELECT DISTINCT count(distinct cl.ClaimLawsuitID) as ClaimantNumber,
		COALESCE(SUM(SettlementAmount),0) as SettlementAmount,
	COALESCE(SUM(SettlementAmount),0)/count(c.ClaimID) as SettlementAverage, 
	
	
SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN 1 ELSE 0 END) as Meso_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN SettlementAmount ELSE 0 END) as Meso_SettlementAmount,	
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN 1 ELSE 0 END) = 0 THEN 0 
		ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = (1) THEN SettlementAmount ELSE 0 END)/ 
			SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN 1 ELSE 0 END) END  as Meso_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseaseID = 2 THEN 1 ELSE 0 END) as LungCancer_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseaseID = 2 THEN SettlementAmount ELSE 0 END) as LungCancer_SettlementAmount,	
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = (2) THEN 1 ELSE 0 END) = 0 THEN 0 
		ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = (2) THEN SettlementAmount ELSE 0 END)/ 
			SUM(CASE WHEN HLD.HighLevelDiseaseID = 2 THEN 1 ELSE 0 END) END  as LungCancer_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN 1 ELSE 0 END) as OtherCancer_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN SettlementAmount ELSE 0 END) as OtherCancer_SettlementAmount,	
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN 1 ELSE 0 END) = 0 THEN 0 
		ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN SettlementAmount ELSE 0 END)/ 
			SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN 1 ELSE 0 END) END  as OtherCancer_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseaseID IN (4,7, 8) THEN 1 ELSE 0 END) as NonMalig_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseaseID IN (4,7,8) THEN SettlementAmount ELSE 0 END) as NonMalig_SettlementAmount,	
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID IN (4,7,8) THEN 1 ELSE 0 END) = 0 THEN 0 
		ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID IN (4,7,8)THEN SettlementAmount ELSE 0 END)/ 
			SUM(CASE WHEN HLD.HighLevelDiseaseID IN (4,7,8) THEN 1 ELSE 0 END) END  as NonMalig_Avg,
			
	SUM(CASE WHEN COALESCE(HLD.HighLevelDiseaseID,0) IN (0, 5, 6) THEN 1 ELSE 0 END) as Other_Count,	
	SUM(CASE WHEN COALESCE(HLD.HighLevelDiseaseID,0) IN (0, 5, 6) THEN SettlementAmount ELSE 0 END) as Other_SettlementAmount,
			
	0 as NoDisease_Count,
	0 as NoDisease_SettlementAmount,		
			
	SUM(CASE WHEN HLD.HighLevelDiseaseID is null THEN 1 ELSE 0 END) as Null_Count,				
	SUM(CASE WHEN HLD.HighLevelDiseaseID  is null THEN SettlementAmount ELSE 0 END) as Null_SettlementAmount,
	Coalesce(UPPER(PlaintiffCounsel), ''UNKNOWN'') AS PlaintiffCounsel
	
FROM tblClaim c 
	LEFT Join (SELECT CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease FROM tblClaimDisease CD WHERE PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
	INNER Join tblClaimLawsuit cl on  cl.claimID = c.claimID
	INNER Join tblLawsuit l on cl.lawsuitid = l.lawsuitid
			' + @JoinString + '
	INNER Join tblCLaimantSettlement s on s.claimlawsuitID = cl.claimlawsuitID
	LEFT JOIN  tblDisease D on CD.DiseaseID = D.DiseaseID
	LEFT JOIN tblHighLevelDisease HLD on HLD.HighLevelDiseaseID=D.HighLevelDiseaseID
	INNER join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		' + @WhereString + '
		' + @GroupByString + '
'

 	IF object_id('tempdb..#SettlementSummaryTemp') IS NOT NULL
	BEGIN
	   DROP TABLE #SettlementSummaryTemp
	END
	CREATE TABLE #SettlementSummaryTemp 
				(ClaimNumber int, 
							
				SettlementAmount money,
				SettlementAverage money,
				
				Meso_Count int,
				Meso_SettlementAmount money,
				Meso_Avg money,
				LungCancer_Count int,
				LungCancer_SettlementAmount money,
				LungCancer_Avg money,
				OtherCancer_Count int,
				OtherCancer_SettlementAmount money,
				OtherCancer_Avg money,
				NonMalig_Count int,
				NonMalig_SettlementAmount money,
				NonMalig_Avg money,
				Other_Count int,
				Other_SettlementAmount money,

				NoDisease_Count int,
				NoDisease_SettlementAmount money,

				Null_Count int,
				Null_SettlementAmount money,
				PlaintiffCounsel NVARCHAR(200))

Insert Into #SettlementSummaryTemp
EXEC sp_executesql @QueryString

SELECT 
SUM(COALESCE(ClaimNumber,0)) as ClaimNumber, 
	SUM(COALESCE(SettlementAmount,0)) as SettlementAmount,
	SUM(COALESCE(SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Claimnumber, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Claimnumber, 0)) END AS SettlementAverage,
	SUM(COALESCE(Meso_Count,0)) as Meso_Count,
	SUM(COALESCE(Meso_SettlementAmount,0)) as Meso_SettlementAmount,
	SUM(COALESCE(Meso_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Meso_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Meso_Count, 0)) END AS Meso_Avg,
	SUM(COALESCE(LungCancer_Count,0)) as LungCancer_Count,
	SUM(COALESCE(LungCancer_SettlementAmount,0)) as LungCancer_SettlementAmount,
	SUM(COALESCE(LungCancer_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(LungCancer_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(LungCancer_Count, 0)) END AS LungCancer_Avg,
	SUM(COALESCE(OtherCancer_Count,0)) as OtherCancer_Count,
	SUM(COALESCE(OtherCancer_SettlementAmount,0)) as OtherCancer_SettlementAmount,
	SUM(COALESCE(OtherCancer_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(OtherCancer_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(OtherCancer_Count, 0)) END AS Othercancer_Avg,
	SUM(COALESCE(NonMalig_Count,0))as NonMalig_Count,
	SUM(COALESCE(NonMalig_SettlementAmount,0))as NonMalig_SettlementAmount,
	SUM(COALESCE(NonMalig_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(NonMalig_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(NonMalig_Count, 0)) END AS NonMalig_Avg,
	SUM(COALESCE(Other_Count,0)) as Unknown_Count,
	SUM(COALESCE(Other_SettlementAmount,0)) as Unknown_SettlementAmount,
	SUM(COALESCE(Other_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Other_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Other_Count, 0)) END AS Unknown_Avg,
 	PlaintiffCounsel
FROM #SettlementSummaryTemp 
GROUP BY PlaintiffCounsel

RETURN






GO
