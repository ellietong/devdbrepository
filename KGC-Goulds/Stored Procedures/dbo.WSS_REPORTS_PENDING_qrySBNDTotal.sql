SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qrySBNDTotal]  (@matterID int, @EndDate DATETIME=null, @EmailAddress NVARCHAR(250)) 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @EmailAddress = 'hagep@kcic.com'
--DECLARE @MatterID int = null,
--		@EndDate DateTime = '1/1/2007',
--		@EmailAddress nvarchar(max) = 'owenss@kcic.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)


DECLARE @WhereString NVARCHAR(MAX) 
SET @WhereString = ' WHERE ClaimLawsuitStatusID IN (25) 
						AND  IsPrimaryStatus=1 '

DECLARE @GroupByString NVARCHAR(MAX) 
SET @GroupByString = 'GROUP BY'

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @GroupByString = @GroupByString + ' dj.DefenseCounselAddressID '
END

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @QueryString NVARCHAR(MAX)

SET @QueryString = 
	'SELECT DISTINCT COUNT(distinct cl.ClaimLawsuitID) as Count
	FROM tblClaim c WITH (READCOMMITTED)
	INNER JOIN tblClaimLawsuit cl WITH (READCOMMITTED) ON c.ClaimID=cl.ClaimID
	INNER JOIN tblLawsuit l WITH (READCOMMITTED) ON cl.LawsuitID=l.LawsuitID
			' + @JoinString + '
	INNER JOIN tblClaimLawsuitStatusTracking clst WITH (READCOMMITTED) ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
	LEFT JOIN tblState s WITH (READCOMMITTED) ON l.StateID=s.StateID
		' + @WhereString + '
	ORDER BY COUNT(distinct cl.ClaimLawsuitID) desc'


--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

END








GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qrySBNDTotal] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qrySBNDTotal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qrySBNDTotal] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qrySBNDTotal] TO [power_user]
GO
