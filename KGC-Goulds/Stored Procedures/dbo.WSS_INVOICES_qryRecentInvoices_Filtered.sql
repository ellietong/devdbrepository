SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_INVOICES_qryRecentInvoices_Filtered] (@emailaddress			nvarchar(1000) = NULL, 
															  @FilterStatusID		int = NULL, 
															  @FilterDocumentTypeID int = NULL, 
															  @FilterFirmID			int = NULL)
AS
SET NOCOUNT ON
 
----The following set of variables can be used for testing.
--DECLARE @emailaddress nvarchar(max)
--DECLARE @FilterStatusID int
--DECLARE @FilterDocumentTypeID int
--DECLARE @FilterFirmID int
--DECLARE @FilterExpertFirmID int
--SET @emailaddress         = 'sreich@dehay.com'
--SET @FilterStatusID       = null
--SET @FilterDocumentTypeID = null
--SET @FilterFirmID         = 10
--SET @FilterExpertFirmID   = null
 
DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)
 
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE dt.ClaimantDocumentGroupID = 2 AND di.DefenseInvoiceID IS NOT NULL'
IF @FilterDocumentTypeID IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND ClaimantDocumentTypeID = ' + CONVERT(nvarchar(20), @FilterDocumentTypeID) END
IF @FilterFirmID		 IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND dinv.FirmID = ' + CONVERT(nvarchar(20), @FilterFirmID) END
IF @FilterStatusID		 IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND dinv.StatusID = ' + CONVERT(nvarchar(20), @FilterStatusID) END
 
 
--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF ((@DefenseCounselID = 277) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' '
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND dinv.FirmID = ' + CONVERT(nvarchar(20),@DefenseCounselID)
END
 
--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT TOP 200
                    CAST(d.DocumentID AS VARCHAR(50)) AS DocumentId ,
                    dt.ClaimantDocumentType ,
                    dg.ClaimantDocumentGroup ,
                    d.Comments ,
                    d.DocumentURL ,
                    d.DocumentTitle ,
                    d.CreatedBy ,
                    d.DateCreated ,
                    d.ModifiedBy ,
                    d.DateModified ,
                    d.Accepted ,
                    d.AcceptedBy ,
                    d.DateAccepted ,
                    di.ApprovedBy ,
                    di.DateApproved ,
                    f.Firm ,
                    di.InvoiceNumber ,
                    di.Period ,
                    di.DatePayable ,
                    di.fees AS FeeAmount ,
                    di.costs AS DisbursementAmount ,
                    di.adjustments AS Adjustment ,
                    di.Invoicenotes AS AdjustmentNotes ,
                    dis.Status ,
                    '''' AS DefenseInvoiceFirm , 				
                    c.sourcedate AS DatePaid ,
                    c.SourceNumber AS CheckNumber ,
                    di.FirmID ,
					COALESCE(di.Firm, '''') AS VendorFirm, 
                    c.SourceDate ,
                    di.InvoiceTotal AS InvoiceTotal
            FROM    tblDocuments d
                    INNER JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
                    INNER JOIN tblClaimantDocumentGroup dg ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
                    LEFT JOIN vtblDefenseInvoice di ON d.DocumentID = di.DocumentID 
                    LEFT JOIN tblDefenseInvoiceStatus dis ON di.statusid = dis.StatusID 
                    LEFT JOIN tblDefenseInvoice dinv ON dinv.DocumentID = d.DocumentID
                    LEFT JOIN vtblFirm f ON dinv.FirmID = f.FirmID
                    LEFT JOIN m_tblVendorFirm ex ON dinv.VendorFirmID = ex.VendorFirmID
                    LEFT JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
                    LEFT JOIN tblCheck c ON pd.CheckID = c.CheckID ' +
    @WhereString + '
	ORDER BY Accepted ASC, DateCreated DESC
	OPTION (RECOMPILE)'
 
PRINT @QueryString
 
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryRecentInvoices_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryRecentInvoices_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryRecentInvoices_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryRecentInvoices_Filtered] TO [power_user]
GO
