SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[WSS_REPORTS_qrySettlementDocCategoryList]
AS
SET NOCOUNT ON

SELECT MAX(DocCategoryID) + 1, 'Executed Release' AS DocCategory, 'ReleaseReceived' AS SDCategory
FROM tblDocumentCategory
UNION
SELECT MAX(DocCategoryID) + 2, 'Utica Packet' AS DocCategory, 'UticaPacket' AS SDCategory
FROM tblDocumentCategory
UNION
SELECT NULL, '', ''
ORDER BY DocCategory



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementDocCategoryList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementDocCategoryList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementDocCategoryList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementDocCategoryList] TO [power_user]
GO
