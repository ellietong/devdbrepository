SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_INVOICES_InvoiceSearch]
(@FirmID int, @VendorFirmID int, @InvoiceNumber nvarchar(50), @StatusID bigint, @PaidDateStart date, @PaidDateEnd date, @Period nvarchar(50),@emailaddress nvarchar(250))
AS
 
--------VARIABLES FOR TESTING----
--DECLARE @FirmID int
--DECLARE @VendorFirmID int
--DECLARE @StatusID bigint
--DECLARE @PaidDateStart date
--DECLARE @PaidDateEnd date
--DECLARE @Period nvarchar(50)
--DECLARE @emailaddress nvarchar(250)
--Declare @invoicenumber nvarchar(250)
--SET @FirmID		   = 306
--SET @VendorFirmID  = NULL
--SET @StatusID	   = NULL
--SET @PaidDateStart = NULL
--SET @PaidDateEnd   = NULL
--SET @Period		   = NULL
--SET @emailaddress  = 'snyderd@kcicllc.com'
--SET @invoicenumber = null
 
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE dt.ClaimantDocumentGroupID = 2 AND di.DefenseInvoiceID IS NOT NULL'
IF @FirmID			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.FirmID = ' + CONVERT(nvarchar(20),@FirmID) END
IF @VendorFirmID	IS NOT NULL BEGIN IF @VendorFirmID = 0 SET @WhereString = @WhereString + ' AND di.VendorFirmID <= 10000' ELSE SET @WhereString = @WhereString + ' AND di.VendorFirmID = ' + CONVERT(nvarchar(20),@VendorFirmID) END
IF @StatusID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND dis.StatusID = ' + CONVERT(nvarchar(20),@StatusID) END
IF @PaidDateStart	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND c.Sourcedate >= ' + CONVERT(nvarchar(20),@PaidDateStart) END
IF @PaidDateEnd		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND c.Sourcedate <= ' + CONVERT(nvarchar(20),@PaidDateEnd) END
IF @Period			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.Period LIKE ''%'' + @PeriodParam + ''%''' END
IF @InvoiceNumber	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.InvoiceNumber LIKE ''%'' + @InvoiceNumberParam + ''%''' END
 
--These IF statements determine additional WHERE statements by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)
 
DECLARE @JoinString nvarchar(max)
IF ((@DefenseCounselID = 277) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' '
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND di.FirmID = ' + CONVERT(nvarchar(20),@DefenseCounselID)
END
 
----Exclude all VOID checks (added by CY, 9/5/13, per A. Kang)
--SET @WhereString = @WhereString + ' AND COALESCE(c.FundingTypeID,0) <> 3'
 
DECLARE @QueryString nvarchar(max)
SET @QueryString = '
		SELECT DISTINCT CAST(d.DocumentID AS varchar(50)) AS DocumentId, 
		dt.ClaimantDocumentType, 
		dg.ClaimantDocumentGroup, 
		d.Comments,
		d.DocumentURL, 
		d.DocumentTitle, 
		d.CreatedBy, 
		d.DateCreated, 
		d.Accepted, 
		d.AcceptedBy, 
		d.DateAccepted, 
		di.ApprovedBy, 
		di.DateApproved, 
		dc.DefenseCounsel as Firm,
		di.InvoiceNumber, 
		di.Period,	
		di.DatePayable, 
		sum(coalesce(dic.fees,0)) as feeamount, 
		sum(coalesce(dic.costs,0)) as disbursementamount, 
		sum(coalesce(dic.adjustments,0)) as Adjustment,
		di.InvoiceNotes as AdjustmentNotes, 
		dis.Status, '''' AS DefenseInvoiceFirm, 
		c.SourceDate as PaidDate, 
		c.SourceNumber as CheckNumber, 
		di.FirmID, 
		coalesce(vendorfirm, defensecounsel, '''') as VendorFirm,
		c.SourceDate, 
		sum(coalesce(dic.fees,0)+coalesce(dic.costs,0) - coalesce(dic.adjustments,0)) AS InvoiceTotal
	FROM tblDocuments d
		LEFT JOIN tblClaimantDocumentType dt  ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
		LEFT JOIN tblClaimantDocumentGroup dg ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
		LEFT JOIN tblDefenseInvoice di	      ON d.DocumentID = di.DocumentID  
		LEFT JOIN tblDefInvClaimantDetail dic ON dic.DocumentID = di.DocumentID
		LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID 
		LEFT JOIN tblDefenseCounseladdress dc ON di.defensecounseladdressid = dc.DefenseCounseladdressID
		LEFT JOIN m_tblVendorFirm ex		  ON di.VendorFirmID = ex.VendorFirmID
		LEFT JOIN tblPaymentDefense pd		  ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN tblCheck c				  ON pd.CheckID = c.CheckID'+
	@WhereString + ' 
	GROUP BY CAST(d.DocumentID AS varchar(50)), 
		dt.ClaimantDocumentType, 
		dg.ClaimantDocumentGroup, 
		d.Comments,
		d.DocumentURL, 
		d.DocumentTitle, 
		d.CreatedBy, 
		d.DateCreated, 
		d.Accepted, 
		d.AcceptedBy, 
		d.DateAccepted, 
		di.ApprovedBy, 
		di.DateApproved, 
		dc.DefenseCounsel ,
		di.InvoiceNumber, 
		di.Period,	
		di.DatePayable, 
		di.InvoiceNotes, 
		dis.Status, 
		c.SourceDate,
		c.SourceNumber, 
		di.FirmID, 
		coalesce(vendorfirm, defensecounsel, ''''),
		c.SourceDate
	ORDER BY DateCreated DESC, Accepted ASC 
	OPTION (RECOMPILE)'
	
--PRINT @QueryString
 
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@PeriodParam nvarchar(max), @InvoiceNumberParam nvarchar(250)', @PeriodParam = @Period, @InvoiceNumberParam = @InvoiceNumber
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_InvoiceSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_InvoiceSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_InvoiceSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_InvoiceSearch] TO [power_user]
GO
