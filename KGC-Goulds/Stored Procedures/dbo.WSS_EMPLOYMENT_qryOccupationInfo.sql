SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryOccupationInfo] (@ClaimantPersonalInfoID int, 
														  @emailaddress			  nvarchar(250))
AS
SET NOCOUNT ON

--The following set of variables can be used for testing.
--DECLARE @ClaimantPersonalInfoID int
--DECLARE @emailaddress nvarchar(250)
--SET @ClaimantPersonalInfoID = 109693
--SET @emailaddress = 'owenss@kcicllc.com'

IF RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR (SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1
BEGIN
	SELECT ce.ClaimantEmploymentID, 
		CASE WHEN o.OccupationID IS NULL THEN oa.OccupationID ELSE COALESCE(o.OccupationID, 1) END AS OccupationID,
		CASE WHEN o.Occupation IS NULL THEN oa.Occupation ELSE o.Occupation END AS Occupation,
		Jobsite, 
		EmpStartDate, 
		EmpEndDate, 
		cpi.FirstName + ' ' + cpi.LastName AS Claimant,
		JobsiteCity, 
		JobsiteStateID,
		s.State AS JobsiteState, 
		IsPrimary AS IsPrimaryJobsite
	FROM tblClaimantEmployment ce 
		INNER JOIN tblClaimantPersonalInfo cpi	ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN  tblOccupation o				ON o.OccupationID = ce.OccupationID
		LEFT JOIN tblOccupation_ALT	oa			ON oa.OccupationID = ce.OccupationID
		LEFT JOIN  tblClaim c					ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN  tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
		LEFT JOIN  tblLawsuit l					ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN  tblState s					ON ce.JobsiteStateID = s.StateID
	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	GROUP BY ce.ClaimantEmploymentID, 
		o.Occupation, 
		o.OccupationID,
		oa.Occupation,
		oa.OccupationID,
		Jobsite, 
		EmpStartDate, 
		EmpEndDate, 
		cpi.FirstName + ' ' + cpi.LastName,
		JobsiteCity, 
		JobsiteStateID,
		s.State, 
		IsPrimary 
	ORDER BY IsPrimary DESC
END
ELSE
BEGIN
	SELECT DISTINCT ce.ClaimantEmploymentID, 
		CASE WHEN o.OccupationID IS NULL THEN oa.OccupationID ELSE COALESCE(o.OccupationID, 1) END AS OccupationID,
		CASE WHEN o.Occupation IS NULL THEN oa.Occupation ELSE o.Occupation END AS Occupation,
		o.Occupation, 
		Jobsite, 
		EmpStartDate, 
		EmpEndDate, 
		cpi.FirstName + ' ' + cpi.LastName AS Claimant,
		JobsiteCity, 
		JobsiteStateID, 
		s.State AS JobsiteState, 
		IsPrimary AS IsPrimaryJobsite
	FROM tblClaimantEmployment ce 
		INNER JOIN tblClaimantPersonalInfo cpi	ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN  tblOccupation o				ON o.OccupationID = ce.OccupationID
		LEFT JOIN tblOccupation_ALT	oa			ON oa.OccupationID = ce.OccupationID
		LEFT JOIN  tblClaim c					ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN  tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
		LEFT JOIN  tblLawsuit l					ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN  tblState s					ON ce.JobsiteStateID = s.StateID
		INNER JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID=dca.DefenseCounselAddressID
		INNER JOIN tblDefenseJurisdiction dj	ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
		AND (dj.DefenseCounselAddressID IN (SELECT DefenseCounselAddressID 
											FROM vtblPerson p 
												INNER JOIN tblDefenseCOunselAddress dca ON p.FirmID=dca.DefenseCounselID 
											WHERE Email = @emailaddress) 
			OR l.DefenseCounselAddressID IN(SELECT DefenseCounselAddressID 
											FROM vtblPerson p 
												INNER JOIN tblDefenseCOunselAddress dca ON p.FirmID=dca.DefenseCounselID 
											WHERE Email = @emailaddress))
	GROUP BY ce.ClaimantEmploymentID, 
		o.Occupation, 
		o.OccupationID,
		oa.Occupation,
		oa.OccupationID,
		Jobsite, 
		EmpStartDate, 
		EmpEndDate, 
		cpi.FirstName + ' ' + cpi.LastName,
		JobsiteCity, 
		JobsiteStateID, 
		s.State, 
		IsPrimary 									
	ORDER BY IsPrimary DESC
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryOccupationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryOccupationInfo] TO [power_user]
GO
