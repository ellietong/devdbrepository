SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryUpdClaim] (	@DiseaseID int, 
												@DoctorID bigint, 
												@DiseaseDiagnosisDate datetime, 
												@LawsuitID bigint, 
												@ActiveTrialDate datetime,
												@PrimaryLawsuit nvarchar(250), 
												@IsMDL nvarchar(250),
												@ClaimID bigint,
												@Username nvarchar(250),
												@TrialGroupID int, 
												@ClaimLawsuitID int)
AS

UPDATE tblLawsuit
SET SPUsername = @Username
WHERE LawsuitID = @LawsuitID
	
--UPDATE tblDoctor
--SET DoctorNote = @Specialty
--WHERE DoctorID= @DoctorID	
	
UPDATE tblClaimLawsuit
	SET ActiveTrialDate = @ActiveTrialDate,
		IsPrimaryForClaim = Case When @PrimaryLawsuit= 'YES' Then 1 else 0 END,
		IsMDL = Case When @IsMDL= 'YES' Then 1 else 0 END,
		TrialGroupID = @TrialGroupID,
		SPUsername = @Username
	WHERE ClaimLawsuitID = @ClaimLawsuitID
	
--UPDATED 11/17/2014 by CY: Check if the current ClaimLawsuitID is StatusID 39 (uncomfirmed); if so, and an ActiveTrialDate was filled in, change to StatusID 22 (Open)
IF YEAR(@ActiveTrialDate) >= 2012
BEGIN
	DECLARE @PrimaryClaimLawsuitStatusID INT
	SELECT @PrimaryClaimLawsuitStatusID = ClaimLawsuitStatusID FROM dbo.tblClaimLawsuitStatusTracking WHERE ClaimLawsuitID = @ClaimLawsuitID AND IsPrimaryStatus = 1

	IF @PrimaryClaimLawsuitStatusID = 39
	BEGIN
		UPDATE tblClaimLawsuitStatusTracking
			SET IsPrimaryStatus = 0
			WHERE ClaimLawsuitID = @ClaimLawsuitID
				AND IsPrimaryStatus = 1

		INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, ProcessedDate, StatusNotes)
			SELECT @ClaimLawsuitID, 22, GETDATE(), 1, GETDATE(), 'Per request of ' + @Username + ', Unconfirmed cases with Trial dates should be changed to Open.'
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GO
