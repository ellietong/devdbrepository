SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrpt1099Data] (@TaxYear int)
as
--This query is used to populate the 1099 forms for each tax year.
--DECLARE @TaxYear int
--Set @TaxYear = 2011

Set nocount on
DECLARE @Address table (PayeeID nvarchar (100), Payee nvarchar(500), Address1 nvarchar (500), Address2 nvarchar(400), City nvarchar(400), Zip nvarchar (50), State nvarchar(50), TaxID nvarchar(500))
INSERT into @Address
select 'P'+CAST(pca.PlaintiffCounselID AS NCHAR), plaintiffcounsel, pca.PlaintiffCounselAddress1, pca.PlaintiffCounselAddress2, pca.PlaintiffCounselCity,
 pca.PlaintiffCounselZip, State, COALESCE(taxid,PlaintiffCounselTaxIDNum)
from tblPlaintiffCounselAddress pca 
inner join [KGA-ClaimsAdmin].[dbo].[tblPlaintiffCounselAddress] p on p.PlaintiffCounselAddressID=pca.PlaintiffCounselAddressID
WHERE pca.PayeeAddress=1
UNION 
Select 'D'+ CAST(f.firmid AS NCHAR), Firm,  
FirmAddress1, FirmAddress2, FirmCity, firmZip + CASE WHEN firmmailzip is null then '' else '-'+FirmMailZip End AS Zip, State,taxID
from vtblFirm F 
LEFT JOIN [Ligado-ExtranetSecurity].dbo.FirmAddress fa on fa.firmID = f.FirmID
LEFT JOIN tblState s on s.StateID = fa.firmstateID
WHERE payeeaddress=1
UNION 
Select 'V'+CAST(VF.VendorFirmID AS NCHAR) AS PayeeID, VendorFirm AS Payee, Address1, Address2, City, ZipCode, State,TaxID
from m_tblvendorfirm vf LEFT JOIN tblState s on s.StateID = VF.StateID

SELECT 'V'+CAST(VF.VendorFirmID AS NCHAR) AS PayeeID, VendorFirm AS Payee, a.TaxID, a.Address1, a.Address2, a.City, a.Zip, a.State, 
NULL AS Box14, sum(PaymentAmount) AS Box7
FROM tblCheck C
INNER JOIN tblPaymentDefense PD ON PD.CheckID = C.CheckID
INNER JOIN tblDefenseInvoice DI ON DI.DefenseInvoiceID = PD.DefenseInvoiceID
LEFT JOIN m_tblVendorFirm VF ON VF.VendorFirmID = DI.VendorFirmID
inner join (select * from @Address WHERE LEFT(payeeid,1)='V') a on a.PayeeID='V'+CAST(VF.VendorFirmID AS NCHAR)
WHERE YEAR(SourceDate) = @TaxYear AND FundingTypeID = 3
GROUP BY vf.VendorFirmID, VendorFirm, a.TaxID, a.Address1, a.Address2, a.City, a.Zip, a.State

UNION 

SELECT 'D'+ CAST(CASE WHEN f.firmid = 188 then vendorfirmid else f.firmid end AS NCHAR), a.Payee, a.taxID, a.Address1, a.Address2, a.City, a.Zip, a.State, 
NULL AS Box14, sum(PaymentAmount) AS Box7
FROM tblCheck C
INNER JOIN tblPaymentDefense PD ON PD.CheckID = C.CheckID
INNER JOIN tblDefenseInvoice DI ON DI.DefenseInvoiceID = PD.DefenseInvoiceID
LEFT JOIN vtblFirm F ON F.FirmID = DI.FirmID
inner join (select * from @Address WHERE LEFT(payeeid,1)='D') a on a.PayeeID='D'+ CAST(CASE WHEN f.firmid = 188 then vendorfirmid else f.firmid end AS NCHAR)
WHERE YEAR(SourceDate) = 2011 AND FundingTypeID = 1 
-- Exclude Segal, include in final UNION ed query
and f.FirmID <>166
GROUP BY CASE WHEN f.firmid = 188 then vendorfirmid else f.firmid end, a.Payee,  a.taxID, a.Address1, a.Address2, a.City, a.Zip, a.State
 
UNION 

SELECT 'P'+CAST(pca.PlaintiffCounselID AS NCHAR), PCA.PlaintiffCounsel, A.TaxID, a.Address1, a.Address2, a.City, a.Zip, a.State, 
--subract $675000 from Weitz for Void/Refund (original check written in 2010, re-issued check made out to Segal Trust)
--CASE WHEN 'P'+CAST(pca.PlaintiffCounselID AS NCHAR) = 'P150' then sum(PaymentAmount)-675000 else SUM(PaymentAmount) end AS Box3, 0 AS Box7
SUM(PaymentAmount) AS Box14, NULL AS Box7
FROM tblCheck C
INNER JOIN tblPaymentSettlement PS ON PS.CheckID = C.CheckID
INNER JOIN tblClaimantSettlement CS ON CS.ClaimantSettlementID = PS.ClaimantSettlementID
INNER JOIN tblClaimLawsuit CL ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
INNER JOIN tblLawsuit L ON L.LawsuitID = CL.LawsuitID
inner JOIN tblPlaintiffCounselAddress PCA ON PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
inner join (select * from @Address WHERE LEFT(payeeid,1)='P') a on a.PayeeID='P'+CAST(pca.PlaintiffCounselID AS NCHAR)
WHERE YEAR(SourceDate) = 2011
-- Exclude Settlement payment paid to Segal trust and the Bifferato payment (pd to Simmons), include in later UNION 'ed queries
and pca.PlaintiffCounselAddressID <>816 and PlaintiffCounselID <>713 and PlaintiffCounselID<>331
GROUP BY 'P'+CAST(pca.PlaintiffCounselID AS NCHAR), PCA.PlaintiffCounsel, A.TaxID, a.Address1, a.Address2, a.City, a.Zip, a.State

UNION 
--Separate query for Segal to handle payments made for defense and settlement payment made to Segal (NY) trust
Select 'D166' AS PayeeID, 'Segal, McCambridge, Singer & Mahoney, Ltd', '36-3419170' AS taxid,
'233 S. Wacker Drive', 'Ste. 5500', 'Chicago', '60606','IL', SUM(ps.paymentamount) AS box14, SUM(pd.paymentamount) AS box7
from tblCheck c
LEFT JOIN tblPaymentSettlement ps on ps.CheckID=c.CheckID
LEFT JOIN tblPaymentDefense pd on pd.CheckID=c.CheckID
LEFT JOIN tblClaimantSettlement cs on cs.ClaimantSettlementID=ps.ClaimantSettlementID
LEFT JOIN tblClaimLawsuit CL ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
LEFT JOIN tblLawsuit L ON L.LawsuitID = CL.LawsuitID
LEFT JOIN tblPlaintiffCounselAddress PCA ON PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
LEFT JOIN tblDefenseInvoice DI ON DI.DefenseInvoiceID = PD.DefenseInvoiceID
LEFT JOIN vtblFirm F ON F.FirmID = DI.FirmID
WHERE ((f.FirmID =166 AND FundingTypeID = 1) or pca.PlaintiffCounselAddressID=816) and YEAR(SourceDate) = @TaxYear
and SourceAmount >0

UNION 
--Separante query for Bifferato amounts actually paid to Simmons firm and all other Simmons payments
Select 'P331' AS PayeeID, 'Simmons Browder Gianaris Angelides & Barnerd LLC', '20-0410905' AS taxid,
'One Court Street', '', 'Alton', '62002','IL', SUM(ps.paymentamount) AS box14, NULL AS box7
from tblCheck c
LEFT JOIN tblPaymentSettlement ps on ps.CheckID=c.CheckID
LEFT JOIN tblPaymentDefense pd on pd.CheckID=c.CheckID
LEFT JOIN tblClaimantSettlement cs on cs.ClaimantSettlementID=ps.ClaimantSettlementID
LEFT JOIN tblClaimLawsuit CL ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
LEFT JOIN tblLawsuit L ON L.LawsuitID = CL.LawsuitID
LEFT JOIN tblPlaintiffCounselAddress PCA ON PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
WHERE pca.PlaintiffCounselID in (713,331)  and YEAR(SourceDate) = @TaxYear
and SourceAmount >0

Order by Payee


return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrpt1099Data] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrpt1099Data] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrpt1099Data] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrpt1099Data] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrpt1099Data] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrpt1099Data] TO [power_user]
GO
