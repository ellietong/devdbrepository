SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] (@OccurrenceGroupID int, @CarrierID int)
As
SET NOCOUNT ON


--Declare @OccurrenceGroupID int
--Declare @CarrierID int
--Set @OccurrenceGroupID = 96
--SET @CarrierID = 94

--Pull allocation amounts for requested Carrier
DECLARE @CarrierAllocation Table (OccurrenceID int, AllocatedAmount float, CarrierName Nvarchar (100))
Insert into @CarrierAllocation
SELECT OC.OccurrenceID, sum(a.Allocation) as AllocatedAmount, CarrierName
from tblAllocationResults A
Join tblOccurrence OC on OC.OccurrenceID = A.OccurrenceID
Join tblPolicy P on p.PolicyID = a.PolicyID
Join tblCarrier C on c.CarrierID = p.CarrierID
where oc.OccurrenceGroupID = @OccurrenceGroupID AND p.CarrierID = @CarrierID
Group by OC.OccurrenceID, CarrierName
--select * from @CarrierAllocation

--Select claimant information associated with expenses to be billed
select CPI.ClaimantPersonalInfoID, upper(Lastname)+', '+upper(FirstName) as ClaimantName, coalesce('XXX-XX-'+ Right([SocialSecurityNumber],4),'XXX-XX-') as SocialSecurityNumber, 
State,jurisdiction, 
	left(HighLevelDisease,1) as Disease, FileDate, SettlementDate, 
	Coalesce(TriggerStart1, TriggerStart2) as  EarliestExposureDate,
	Case when Coalesce(triggerEnd1, TriggerEnd2) = '1/1/2100' then settlementdate else Coalesce(triggerEnd1, TriggerEnd2) end as DiagnosisDate, 
	CASE when OccurrenceTypeID = 1 THEN 'I' Else 'E'END  as AmountType,o.Cost1+o.Cost3+o.Cost2+o.Cost4 as Amount,
	case when inclaim = 1 AND Triggerstart1 = TriggerStart1 THEN ''
WHen Inclaim = 1 AND Triggerstart1 is null THEN '+'
When INclaim = 0 Then '*'
End as ExpNotation,
Case when TriggerEnd1 is not null Then '' Else '*' END as DxNotation,
CASE When o.Cost2 <>0 or o.Cost4 <>0 Then 'A' ELSE 'B' END as ADJ, coalesce(convert(money,CA.AllocatedAmount),0) as AllocAmount, CarrierName
from tblOccurrence O
join tblOccurrenceGroup OG  on O.OccurrenceGroupID = OG.OccurrenceGroupID
left Join tblClaimantPersonalInfo CPI on CPI.ClaimantPersonalInfoID = o.ClaimID
inner join (select * from tblClaim where iscurrentclaim=1) c on c.ClaimantPersonalInfoID= o.ClaimID 
Left Join (select * from tblClaimDisease where PrimaryDisease = 1) cd on cd.ClaimID = c.ClaimID
Left Join tblDisease D on cd.DiseaseID=D.DiseaseID
Left Join tblHighLevelDisease HLD on D.HighLevelDiseaseID=HLD.HighLevelDiseaseID
Left Join (select * from tblClaimLawsuit where IsPrimaryForClaim =1) cl on cl.ClaimID=c.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID 
Left Join tblState S on L.StateID=S.StateID
left Join tblClaimantSettlement CS on CL.ClaimLawsuitID=CS.CLaimLawsuitID
left join tblAllegation A on a.ClaimLawsuitID = CL.ClaimLawsuitID
left join tblDefendantCompany dc on dc.DefendantCompanyID = a.DefendantCompanyID
left join @CarrierAllocation CA on ca.OccurrenceID = o.OccurrenceID
left join tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
Where O.OccurrenceGroupID = @OccurrenceGroupID and cl.IsPrimaryForClaim = 1 and c.IsCurrentClaim = 1 --and o.matterID = c.MatterID 
and o.OccurrenceTypeID = 1
group by CPI.ClaimantPersonalInfoID, upper(Lastname)+', '+upper(FirstName), coalesce('XXX-XX-'+ Right([SocialSecurityNumber],4),'XXX-XX-') , 
	State,CASE when OccurrenceTypeID = 1 THEN 'I' Else 'E'END ,
	Coalesce(TriggerStart1, TriggerStart2),
	left(HighLevelDisease,1), FileDate, SettlementDate, Coalesce(triggerEnd1, TriggerEnd2) ,o.Cost1+o.Cost3+o.Cost2+o.Cost4,
	Case when TriggerEnd1 is not null Then '' Else '*' END,case when inclaim = 1 AND Triggerstart1 = TriggerStart1 THEN ''
WHen Inclaim = 1 AND Triggerstart1 is null THEN '+'
When INclaim = 0 Then '*' End  , CASE When o.Cost2 <>0 or o.Cost4 <>0 Then 'A' ELSE 'B' END,CA.AllocatedAmount, CarrierName,
Jurisdiction
Order by Coalesce(TriggerStart1, TriggerStart2), upper(Lastname)+', '+upper(FirstName)
	


RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptIndemnityDefenseClaimList_ByCarrier] TO [power_user]
GO
