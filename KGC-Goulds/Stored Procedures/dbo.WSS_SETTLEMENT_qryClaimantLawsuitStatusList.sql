SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryClaimantLawsuitStatusList]
AS
SET NOCOUNT ON

SELECT ClaimLawsuitStatusID, 
	ClaimLawsuitStatus 
FROM tblClaimLawsuitStatus
WHERE ClaimLawsuitStatusID IN (25)
UNION
SELECT NULL, ''
ORDER BY ClaimLawsuitStatus
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryClaimantLawsuitStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryClaimantLawsuitStatusList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryClaimantLawsuitStatusList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryClaimantLawsuitStatusList] TO [power_user]
GO
