SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCashActivity] (--@MatterID int, 
@BalanceStartDate datetime, @BalanceEndDate datetime)

As
Set NoCount On

--Declare @MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime
--Set @MatterID = 1
--Set @BalanceStartDate = '10/10/2011'
--Set @BalanceEndDate = '11/18/2011'

--10/17/11 - EMS - hard coded starting balance to PACE 12/31/10 ending balance = $-352,408.55
--10/17/11 - CAS - removed (added to) $2,263.70 from hardcoded starting point. Starting balance now coded as: $-350,144.85 to reconcile with BOA Sept 2011 statement
--10/17/11 - HF - in Settlement detail, removed line item excluding voided checks based on FundingSourceID; added line item so that the comment line for voided checks reads "VOID"
--10/24/11 - CAS - re-adjust cash balance by $2,250.00 to unadjust for uncleared check #2576. New balance to be $-352,394.85 
--					PACE had cleared the check on their activity report before check cleared bank throwing off the reconciliations.
--10/31/11 - HF - added line items in Settlement and Defense details to exclude "Refund" (FundingSourceID 4) checks
--11/14/11 - HF - removed Matter from query
--11/21/11 - CAS - In Defense section, change to left join on tblPaymentDefense and tblDefenseInvoice to allow for Voids to pull.

Declare @StartingBalance money
Set @StartingBalance =-352394.85+ (
Coalesce((Select SUM(CashActivityAmount) From tblMiscCashActivity mca 
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID 
where CashActivityDate >= '1/1/2011' and CashActivityDate < @BalanceStartDate),0)
+
Coalesce((Select SUM(SourceAmount)*-1 From tblCheck c
Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
Where SourceDate >= '1/1/2011' and SourceDate < @BalanceStartDate),0)
)

--Declare @Matter nvarchar(250)
--Set @Matter = (Select Matter from tblMatter where MatterID = @MatterID)

Declare @Detail table (--Matter nvarchar(250), 
FundingType nvarchar(250), FundingSource nvarchar(250), SourceDate datetime, SourceNumber nvarchar(250), Indemnity money, Defense money, MiscFees money, CashReceipts money, Comment nvarchar(Max), CheckStatus bit)
Insert Into @Detail

--Defense
Select --Matter, 
FundingType, FundingSource, SourceDate, SourceNumber, 0, SourceAmount*-1 as SourceAmount, 0, 0, 
--case when di.DefenseCounselAddressID IS null then VendorFirm when di.VendorFirmID IS null then DefenseCounsel else DefenseCounsel+' on behalf of '+VendorFirm end,
CASE WHEN C.FundingSourceID = 3 THEN 'VOID' ELSE coalesce(vendorfirm, defensecounsel) end as Comment, IsCheckCleared
From tblCheck c
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Left Join tblPaymentDefense pd on pd.CheckID = c.CheckID
Left Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
--Inner Join tblDefenseInvoiceFirm dif on dif.FirmID = di.FirmID
Left join tblDefenseCounselAddress da on da.DefenseCounselAddressID=di.DefenseCounselAddressID
left join m_tblVendorFirm v on v.VendorFirmID=di.VendorFirmID
--Inner Join tblMatter m on c.MatterID = m.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @BalanceStartDate and SourceDate <= @BalanceEndDate) --AND c.MatterID = @MatterID 
AND c.FundingTypeID in (1,3) AND C.FundingSourceID <>4
Group by --Matter, 
FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, c.FundingSourceID, coalesce(vendorfirm, defensecounsel), IsCheckCleared
--case when di.DefenseCounselAddressID IS null then VendorFirm when di.VendorFirmID IS null then DefenseCounsel else DefenseCounsel+' on behalf of '+VendorFirm end

Union ALL

--Settlement
Select --Matter, 
FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1 as SourceAmount, 0, 0, 0, 
CASE WHEN C.FundingSourceID = 3 THEN 'VOID' ELSE PlaintiffCounsel END AS Comment, IsCheckCleared
From tblCheck c
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
--left Join tblMatter m on c.MatterID = m.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @BalanceStartDate and SourceDate <= @BalanceEndDate) --AND c.MatterID = @MatterID 
AND c.FundingTypeID = 2 AND C.FundingSourceID <>4
Group by --Matter, 
FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, c.FundingSourceID, PlaintiffCounsel, IsCheckCleared

Union ALL

Select --Matter, 
CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, CashActivityAmount, 0, CashActivityNotes, Null
From tblMiscCashActivity mca
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
--Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @BalanceStartDate and CashActivityDate <= @BalanceEndDate) --AND mca.MatterID = @MatterID 
AND CashActivityAmount < 0

Union ALL

Select --Matter, 
CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, 0, CashActivityAmount, CashActivityNotes, Null
From tblMiscCashActivity mca
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
--Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @BalanceStartDate and CashActivityDate <= @BalanceEndDate) --AND mca.MatterID = @MatterID 
AND CashActivityAmount > 0

Union ALL

Select --@Matter, 
'Starting Balance', 'N/A', @BalanceStartDate-1 , 'N/A', 0, 0, 0, Coalesce(@StartingBalance,0), 'Starting Balance', Null

Select *, @BalanceStartDate as BalanceStartDate, @BalanceEndDate as BalanceEndDate from @Detail Order By SourceDate



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [power_user]
GO
