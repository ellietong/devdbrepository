SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROC [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaimsCountByJurisdictionByYear_Monthly] 
(@StartDate DATETIME=null, @EndDate DATETIME=null, @EmailAddress NVARCHAR(250)) 
AS
SET @EmailAddress = 'hagep@kcic.com'
Set NoCount ON

--DECLARE @StartDate DATETIME = '1/1/2005',
--		@EndDate DateTime = '1/1/2007',
--		@EmailAddress nvarchar(max) = 'owenss@kcic.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)


DECLARE @WhereString NVARCHAR(MAX) 
SET @WhereString = ' WHERE COALESCE(cst.IsPrimaryStatus,1) = 1
							AND cst.ClaimLawsuitStatusID IN (22)  '

DECLARE @GroupByString NVARCHAR(MAX) 
SET @GroupByString = 'GROUP BY'

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @GroupByString = @GroupByString + ' dj.DefenseCounselAddressID '
END

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @QueryString NVARCHAR(MAX)

 	IF object_id('tempdb..#DiseaseCount') IS NOT NULL
	BEGIN
	   DROP TABLE #DiseaseCount
	END
	CREATE TABLE #DiseaseCount 
	(Matter NVARCHAR(100), MatterID int, State NVARCHAR(150), YearFiled DATETIME, monthFiledID int, 
monthFiledName NVARCHAR(100), statusdate DATETIME,claimlawsuitid int,
MesoCount int, LungCancerCount int, OtherCancerCount int, 
NonMaligCount int,  UnknownCount int, UnknownCount1 int)


SET @QueryString = 
	'SELECT DISTINCT Matter, C.MatterID, s.FullNameState as state,  
		case WHEN ProcessedDate is null THEN  ''1/1/1900'' ELSE ProcessedDate END as YearFiled, 
		MONTH(COALESCE(ProcessedDate, ''1/1/1900'')), Datename(month, COALESCE(ProcessedDate, ''1/1/1900'')),
		COALESCE(statusdate,''1/1/1900''), cl.claimlawsuitid,
		--Meso
		CASE	WHEN DMeso.HighLevelDiseaseID = 1 THEN  1
		ELSE 0 END AS MesoCount,
		--Lung Cancer
		CASE	WHEN DLC.HighLevelDiseaseID = 2 THEN  1
		ELSE 0 END as LungCancerCount,
		--Other Cancer
		CASE	WHEN DOC.HighLevelDiseaseID = 3 THEN  1
		ELSE 0 END as OtherCancerCount,
		--NonMalig & silicosis & Pneumoconiosis
		CASE	WHEN DNM.HighLevelDiseaseID IN (4, 7, 8) THEN  1
		ELSE 0 END as NonMaligCount,
		--Unknown & no disease
		CASE	WHEN DUnk.HighLevelDiseaseID IN (5, 6) THEN  1
		ELSE 0 END AS UnknownCount,
		--NULLs
		CASE	WHEN DUnk.HighLevelDiseaseID is NULL THEN  1
		ELSE 0 END AS UnknownCount1
	FROM tblClaimantPersonalInfo CPI
	LEFT JOIN  tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
	LEFT Join (SELECT CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease FROM tblClaimDisease CD WHERE PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
	LEFT JOIN tblMatter M on c.MatterID = M.MatterID
	LEFT JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID
	--Meso
	LEFT JOIN  tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID
	--Lung Cancer
	LEFT JOIN  tblDisease DLC on CD.DiseaseID=DLC.DiseaseID
	--Other Cancer
	LEFT JOIN  tblDisease DOC on CD.DiseaseID=DOC.DiseaseID
	--Non-Malig
	LEFT JOIN  tblDisease DNM on CD.DiseaseID=DNM.DiseaseID
	--Silicosis
	LEFT JOIN  tblDisease DSC on CD.DiseaseID=DSC.DiseaseID
	--No Disease
	LEFT JOIN  tblDisease DND on CD.DiseaseID=DND.DiseaseID
	--Unknown
	LEFT JOIN  tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID
	LEFT JOIN  tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
	LEFT JOIN  tblLawsuit L on CL.LawsuitID=L.LawsuitID
			' + @JoinString + '
	LEFT JOIN  tblState S on L.StateID=S.StateID
	LEFT JOIN tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = cl.ClaimLawsuitID
	' + @WhereString

 	IF object_id('tempdb..#DiseaseCountTemp') IS NOT NULL
	BEGIN
	   DROP TABLE #DiseaseCountTemp
	END
	CREATE TABLE #DiseaseCountTemp 
(Matter NVARCHAR(100), MatterID int, State NVARCHAR(150), YearFiled DATETIME, monthFiledID int, 
monthFiledName NVARCHAR(100), statusdate DATETIME,claimlawsuitid int,
MesoCount int, LungCancerCount int, OtherCancerCount int, 
NonMaligCount int,  UnknownCount int, UnknownCount1 int)

Insert Into #DiseaseCountTemp
EXEC sp_executesql @QueryString


 SELECT  YEAR(@EndDate) AS CloseYear, State,  SUM(MesoCount) as MesoCount,
	SUM(LungCancerCount) as LungCancerCount, SUM(OtherCancerCount) as OtherCancerCount ,@StartDate as StartDate, @EndDate as ENDDate,
	SUM(NonMaligCount) as NonMaligCount, SUM(UnknownCount+UnknownCount1) as UnknownCount, SUM(UnknownCount+UnknownCount1+MesoCount+LungCancerCount+OtherCancerCount+NonMaligCount) AS Total
FROM #DiseaseCountTemp
GROUP BY  State
ORDER BY  State

RETURN








GO
