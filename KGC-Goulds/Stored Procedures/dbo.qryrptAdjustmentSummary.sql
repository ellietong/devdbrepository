SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[qryrptAdjustmentSummary]
(@BillExhaustionDateID int,  @AdjustmentExhaustionDateID int)
As
Set Nocount on



--Declare @BillExhaustionDateID int
--Declare @AdjustmentExhaustionDateID int

--Set @BillExhaustionDateID = 96
--Set @AdjustmentExhaustionDateID = 101



Declare @Header nvarchar(100)
Declare @ExhaustionTypeID int
Declare @Footer nvarchar(100)
Set @Header = (select Datename(MONTH,exhaustiondate) +' ' + Datename(Year,exhaustiondate) from tblexhaustiondate where ExhaustionDateID=@AdjustmentExhaustionDateID)
Set @ExhaustionTypeID = (Select exhaustiontypeid from tblExhaustionDate where ExhaustionDateID = @BillExhaustionDateID group by ExhaustionTypeID)
Set @Footer = (select Datename(MONTH,exhaustiondate) +' ' + Datename(Year,exhaustiondate) from tblexhaustiondate where ExhaustionDateID=@BillExhaustionDateID)

----Set Policy Date Type
--DECLARE @PolicyDateTypeID int
--SET @PolicyDateTypeID = (Case When @ExhaustionTypeID=6 Then '2' Else '1' END)


------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------

Declare @CurrentAllocation Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint, OutsideConsumption money)
Insert Into @CurrentAllocation
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, e.PolicyDateID, 
IndemnityAllocation,DefenseAllocation , DefenseOutsideLimitsAllocation, 
SIRIndemnityAllocation, SIRDefenseAllocation, LedgerID, OutsideConsumption
From tblExhaustion E 
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Left Join tblOccurrence O on E.OccurrenceID = O.OccurrenceID
Where ExhaustionTypeID = @ExhaustionTypeID and ED.ExhaustionDateID = @BillExhaustionDateID


Declare @CurrentAllocationTotals table (ExhaustionTypeID int, PolicyDateID int, Indemnity money, Defense money,
	DefenseOutsideLimits money, TotalDefense money, SIRIndemnityAllocation money, SIRDefenseAllocation money, OutsideConsumption money)
Insert Into @CurrentAllocationTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(IndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(DefenseAllocation), 0) as Defense,
	coalesce(Sum(DefenseOutsideLimitsAllocation),0) as DefenseOutsideLimits,
	coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
	coalesce(Sum(SIRIndemnityAllocation), 0) as SIRIndemnity,
	coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense,
	coalesce(Sum(OutsideConsumption),0) as OutsideConsumption
		From @CurrentAllocation
Group By ExhaustionTypeID, PolicyDateID

Declare @CurrentAdjustment Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint, OutsideConsumption money)
Insert Into @CurrentAdjustment
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, e.PolicyDateID, 
IndemnityAllocation,DefenseAllocation , DefenseOutsideLimitsAllocation, 
SIRIndemnityAllocation, SIRDefenseAllocation, LedgerID, OutsideConsumption
From tblExhaustion E 
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Left Join tblOccurrence O on E.OccurrenceID = O.OccurrenceID
Where ExhaustionTypeID = @ExhaustionTypeID and ED.ExhaustionDateID = @AdjustmentExhaustionDateID


Declare @CurrentAdjustmentTotals table (ExhaustionTypeID int, PolicyDateID int, AdjustmentIndemnity money, AdjustmentDefense money,
	AdjustmentDefenseOutsideLimits money, AdjustmentTotalDefense money, AdjustmentSIRIndemnityAllocation money, AdjustmentSIRDefenseAllocation money, AdjustmentOutsideConsumption money)
Insert Into @CurrentAdjustmentTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(IndemnityAllocation), 0) as AdjustmentIndemnity,
	coalesce(Sum(DefenseAllocation), 0) as AdjustmentDefense,
	coalesce(Sum(DefenseOutsideLimitsAllocation),0) as AdjustmentDefenseOutsideLimits,
	coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as AdjustmentTotalDefense,
	coalesce(Sum(SIRIndemnityAllocation), 0) as AdjustmentSIRIndemnity,
	coalesce(Sum(SIRDefenseAllocation), 0) as AdjustmentSIRDefense,
	coalesce(Sum(OutsideConsumption),0) as AdjustmentOutsideConsumption
		From @CurrentAdjustment
Group By ExhaustionTypeID, PolicyDateID


--Query Results
Declare @Results Table(policyid bigint, policydateid bigint, carriername nvarchar(100), policynum nvarchar(100), 
	pstartdate nvarchar(100), penddate  nvarchar(100),  Indemnity money, Defense money, OutsideConsumption money, 
	DefenseOutsideLimits money, TotalDefense money)
Insert Into @Results
																																			
--London
select  p.policyid, pd.PolicyDateID, CarrierName, policynum,  convert(nvarchar(100), pstartdate, 101) as pstartdate, convert(nvarchar(100), PEndDate, 101)  as penddate,  sum(IndemnityAllocation) as Indemnity, sum(defenseallocation) as Defense, sum(outsideconsumption) as outsideconsumption, sum(defenseoutsidelimitsallocation) as defenseoutsidelimits, sum(DefenseAllocation+defenseoutsidelimitsallocation) as TotalDefense
from tblPolicyDates pd
inner join tblPolicy p on p.PolicyID=pd.PolicyID
inner join tblcarrier c on c.carrierid=p.carrierid
inner join tblexhaustion e on e.policydateid=pd.policydateid
where c.CarrierID=20 and exhaustiondateid = @BillExhaustionDateID
group by pd.PolicyDateID,  carriername, policynum, pstartdate, penddate, p.policyid


UNION

--Most Carriers
select p.policyid, pd.policydateid, CarrierName, 
p.policynum, convert(nvarchar(100), pstartdate, 101) as pstartdate, convert(nvarchar(100), PEndDate, 101)  as penddate,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(OutsideConsumption,0) as OutsideConsumption,
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense
 from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
Where c.carrierid not in (10,20, 100000) 


--Adjustment Results
Declare @Adjustment Table (policyid bigint, policydateid bigint, carriername nvarchar(100), policynum nvarchar(100), 
	pstartdate nvarchar(100), penddate  nvarchar(100),  adjustmentIndemnity money, adjustmentDefense money, adjustmentOutsideConsumption money, 
	adjustmentDefenseOutsideLimits money, adjustmentTotalDefense money)
insert into @Adjustment

--London
select   p.policyid, pd.PolicyDateID, CarrierName, policynum, pstartdate, penddate, 
sum(IndemnityAllocation) as adjustmentIndemnity, sum(defenseallocation) as adjustmentDefense, sum(outsideconsumption) as adjustmentOutsideConsumption, sum(defenseoutsidelimitsallocation) as adjustmentDefenseOutsideLimits, sum(DefenseAllocation+defenseoutsidelimitsallocation) as adjustmentTotalDefense
from tblPolicyDates pd
inner join tblPolicy p on p.PolicyID=pd.PolicyID
inner join tblcarrier c on c.carrierid=p.carrierid
inner join tblexhaustion e on e.policydateid=pd.policydateid
where c.CarrierID=20 and exhaustiondateid= @AdjustmentExhaustionDateID 
group by pd.PolicyDateID,  carriername, policynum, pstartdate, penddate, p.policyid


UNION

--Most Carriers
select   p.policyid, pd.PolicyDateID, CarrierName, policynum, pstartdate, penddate, 
	coalesce(adjustmentIndemnity,0) as adjustmentIndemnity, 
	coalesce(adjustmentDefense,0) as adjustmentDefense, 
	coalesce(adjustmentOutsideConsumption,0) as adjustmentOutsideConsumption,
	coalesce(adjustmentDefenseOutsideLimits,0) as adjustmentDefenseOutsideLimits, 
	coalesce(adjustmentTotalDefense,0) as adjustmentTotalDefense
 from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAdjustmentTotals CAAT on PD.PolicyDateID=CAAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
Where c.carrierid not in (10,20, 100000)

Declare @indemnityproducer money
select @indemnityproducer =Indemnity from @CurrentAllocationTotals CET where  CET.PolicyDateID=128 

Declare @defenseproducer money
select @defenseproducer =Defense from @CurrentAllocationTotals CET  where  CET.PolicyDateID=128 

Declare @defenseoutsidelimitsproducer money
select @defenseoutsidelimitsproducer =DefenseOutsideLimits from @CurrentAllocationTotals CET  where  CET.PolicyDateID=128 

Declare @adjustmentindemnityproducer money
select @adjustmentindemnityproducer =adjustmentIndemnity from @CurrentadjustmentTotals CAT where  CAT.PolicyDateID=128 

Declare @adjustmentdefenseproducer money
select @adjustmentdefenseproducer =adjustmentDefense from @CurrentadjustmentTotals CAT  where  CAT.PolicyDateID=128 

Declare @adjustmentdefenseoutsidelimitsproducer money
select @adjustmentdefenseoutsidelimitsproducer =adjustmentDefenseOutsideLimits from @CurrentadjustmentTotals CAT  where  CAT.PolicyDateID=128 

select  r.policyid, r.policydateid, r.carriername, r.policynum, r.pstartdate, r.penddate,   SUM(r.Indemnity) AS Indemnity, SUM(r.Defense) AS Defense, SUM(r.OutsideConsumption) AS OutsideConsumption, 
SUM(r.DefenseOutsideLimits) AS DefenseOutsideLimits, SUM(r.TotalDefense) AS TotalDefense, sum(r.Indemnity+r.TotalDefense) as TotalMonthlyBilling, SUM(adjustmentIndemnity) AS adjustmentIndemnity, SUM(adjustmentDefense) AS adjustmentDefense, SUM(adjustmentOutsideConsumption) AS adjustmentOutsideConsumption, 
SUM(adjustmentDefenseOutsideLimits) AS adjustmentDefenseOutsideLimits, SUM(adjustmentTotalDefense) AS adjustmentTotalDefense, sum(adjustmentIndemnity+adjustmentTotalDefense) as AdjustmentTotalBilling, @indemnityproducer as indemnityproducer, sum(@defenseproducer+@defenseoutsidelimitsproducer) as defenseproducer, SUM(@indemnityproducer+@defenseproducer+@defenseoutsidelimitsproducer) as TotalMonthlyBillingProducer,
@adjustmentindemnityproducer as adjustmentindemnityproducer, sum(@adjustmentdefenseproducer+@adjustmentdefenseoutsidelimitsproducer) as  adjustmentdefenseproducer,  SUM(@adjustmentindemnityproducer+@adjustmentdefenseproducer+@adjustmentdefenseoutsidelimitsproducer) as AdjustmentMonthlyBillingProducer, @header as headerDate, @Footer as footerdate
From @Results r
left join @Adjustment a on r.policydateid=a.policydateid
where r.policyid not in (5)
GROUP BY r.policyid, r.policydateid, r.carriername, r.policynum, r.pstartdate, r.penddate 
order by carriername, cast(r.pstartdate as date) , Cast(r.penddate as DATE)

return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAdjustmentSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAdjustmentSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAdjustmentSummary] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptAdjustmentSummary] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAdjustmentSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAdjustmentSummary] TO [power_user]
GO
