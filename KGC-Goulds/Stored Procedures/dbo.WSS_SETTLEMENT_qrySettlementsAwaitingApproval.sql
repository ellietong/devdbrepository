SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE PROC [dbo].[WSS_SETTLEMENT_qrySettlementsAwaitingApproval]  
(@emailaddress nvarchar(250), @SettlementGroupID int = NULL)--, @SettlementDate datetime)
 
As
 
SET NOCOUNT ON
--Test vars
--DECLARE @SettlementGroupID int = null
--DECLARE @EmailAddress nvarchar(250) = 'crookc@kcicllc.com'
 
DECLARE @MaxSettlementGroupID int = (SELECT MAX(SettlementGroupID) FROM tblSettlementGroup)
 
DECLARE @DefenseCounselAddressID int = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
--Only take the max release documents CPC 1/30/2013 - this is ugly and might be able to be simplified but I can't think of how to do it
IF OBJECT_ID('tempdb..#ClaimLawsuitReleaseDocs') IS NOT NULL 
    BEGIN
        DROP TABLE ClaimLawsuitReleaseDocs
    END
CREATE TABLE #ClaimLawsuitReleaseDocs (ReleaseAmazonKey nvarchar(MAX), ClaimLawsuitID bigint)
INSERT  INTO #ClaimLawsuitReleaseDocs
SELECT AmazonKey AS ReleaseAmazonKey, cl.ClaimLawsuitID
FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaim c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
WHERE IsPrimaryForClaim = 1
		AND IsCurrentClaim = 1
		AND clst.ClaimLawsuitStatusID = 23
		AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
		AND ClaimantDocumentTypeID = 31
		AND D.DocumentID = (SELECT MAX(doc.DocumentID) 
							FROM tblDocuments doc 
							INNER JOIN tblMultiClaimantDocument mdoc ON doc.DocumentID = mdoc.DocumentID
							WHERE doc.ClaimantDocumentTypeID = 31 AND mdoc.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID)	
ORDER BY DateCreated DESC
 
DECLARE @SettlementDate datetime
SET @SettlementDate = '1/1/1900'--DATEADD(mm, -18, GETDATE())
 
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' AND 1=1'
DECLARE @OrderByFields nvarchar(MAX)
DECLARE @JoinString nvarchar(max)
 
IF(SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) = 264 OR @emailaddress LIKE '%@kcicllc.com'--ITT/KCIC
BEGIN
	SET @JoinString = ' LEFT JOIN  tblDefenseCounselAddress dca	ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID '
 
	SET @OrderByFields = 'IsRecommended DESC, PaymentRecommendedDate, IsApproved ASC, PaymentApprovedDate, SettlementDate ASC, 
			SettlementGroup,
			cpi.LastName + '', '' + FirstName + COALESCE('' '' + MiddleName, '''')'
END
ELSE IF (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) = 330
BEGIN 
	SET @JoinString = ' LEFT JOIN  tblDefenseCounselAddress dca	ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID '
 
	SET @OrderByFields = 'IsRecommended ASC, SettlementDate, 
			SettlementGroup,
			cpi.LastName + '', '' + FirstName + COALESCE('' '' + MiddleName, '''')'
END 
ELSE
BEGIN
	SET @OrderByFields = 'IsRecommended ASC, SettlementDate, 
			SettlementGroup,
			cpi.LastName + '', '' + FirstName + COALESCE('' '' + MiddleName, '''')'
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'			
END
 
DECLARE @QueryString nvarchar(MAX) = '
 
SELECT cpi.LastName + '', '' + FirstName + COALESCE('' '' + MIddleName, '''') As ClaimantName, 
cl.ClaimLawsuitID,
cs.ClaimantSettlementID,
c.ClaimantPersonalInfoID,
cl.LawsuitID,
PlaintiffCounsel,
SettlementDate,
UPPER(SettlementGroup) AS SettlementGroup,
cs.SettlementGroupID,
cs.PaymentRecommendedBy,
cs.PaymentRecommendedDate,
cs.PaymentApprovedBy,
cs.PaymentApprovedDate,
ExecutedReleaseDate,
spd.ReleaseSignedDate,
SettlementAmount,
CASE	WHEN PaymentRecommendedBy IS NULL THEN ''Awaiting Recommendation to Pay''
		WHEN PaymentApprovedBy IS NULL THEN ''Awaiting Approval to Pay'' END AS Status,
CASE	WHEN PaymentApprovedBy IS NULL AND PaymentApprovedDate IS NULL THEN 0 ELSE 1 END AS IsApproved,
CASE	WHEN PaymentRecommendedBy IS NULL AND PaymentRecommendedDate IS NULL THEN 0 ELSE 1 END AS IsRecommended,
ReleaseAmazonKey,
cs.clientamount
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
	INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT join tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
	LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	LEFT JOIN #ClaimLawsuitReleaseDocs releasedoc ON cl.ClaimLawsuitID = releasedoc.ClaimLawsuitID' +
	@JoinString + ' 
 
WHERE PaymentApprovedBy IS NULL
		AND PaymentApprovedDate IS NULL
		AND COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
		AND clst.ClaimLawsuitStatusID = 23
		AND ProcessedDate >= ''' + CONVERT(nvarchar, @SettlementDate, 101) + '''
		AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
		AND SettlementAmount > 0
		AND ClientAmount<>0
		AND SettlementDate >= ''1/1/2009''' + 
		
		@WhereString + '
		--AND ExecutedReleaseDate IS NOT NULL
		--AND SettlementReviewedByKCIC = 1
		--AND cs.SettlementGroupID >= COALESCE(@SettlementGroupID, 0) AND cs.SettlementGroupID <= COALESCE(@SettlementGroupID, @MaxSettlementGroupID)
		--TODO: add logic here to make sure the settlements have received authority
GROUP BY	cpi.LastName + '', '' + FirstName + COALESCE('' '' + MIddleName, ''''), 
			cl.ClaimLawsuitID,
			c.ClaimantPersonalInfoID,
			cl.LawsuitID,
			PlaintiffCounsel,
			SettlementDate,
			SettlementGroup,
			cs.SettlementGroupID,
			cs.PaymentRecommendedBy,
			cs.PaymentRecommendedDate,
			cs.PaymentApprovedBy,
			cs.PaymentApprovedDate,
			ClaimantSettlementID,
			ExecutedReleaseDate,
			SettlementAmount,
			spd.ReleaseSignedDate,
			ReleaseAmazonKey,
			cs.clientamount
ORDER BY '	+ @OrderByFields
 
PRINT SubString(@QueryString, 1, 4000)
PRINT SubString(@QueryString, 4001, 4000)
 
EXEC sp_executesql @QueryString
 

GO
