SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qrysrptCheckDetailDefense] (@CheckID int)
AS
Set NoCount On
Select DI.InvoiceNumber, Period, sum(fees+costs-adjustments)InvoiceTotal, PaymentAmount
from tblPaymentDefense pd
Inner Join vtblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Where CheckID = @CheckID
group by DI.InvoiceNumber, Period, InvoiceTotal, PaymentAmount
order by DI.InvoiceNumber
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailDefense] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailDefense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailDefense] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailDefense] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailDefense] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailDefense] TO [power_user]
GO
