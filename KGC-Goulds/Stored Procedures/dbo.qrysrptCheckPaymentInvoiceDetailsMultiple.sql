SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qrysrptCheckPaymentInvoiceDetailsMultiple] (@DefenseInvoiceID int)

AS
Set NoCount On

--DECLARE @DEFENSEINVOICEID INT
--SET @DEFENSEINVOICEID=76

SELECT DICD.InvoiceNumber, DICD.ClaimantPersonalInfoID, FirstName + ' ' + LastName AS CLAIMANTNAME, DOFE, Fees, Costs, Adjustments, InsurerAmount, ClientAmount
FROM tblDefenseInvoice DI
INNER JOIN tblDefInvClaimantDetail DICD ON DICD.DocumentID=DI.DocumentID
INNER JOIN tblPaymentDefense PD ON PD.DefenseInvoiceID=DI.DefenseInvoiceID
INNER JOIN tblCheck C ON C.CheckID=PD.CheckID
INNER JOIN tblClaimantPersonalInfo CPI ON CPI.ClaimantPersonalInfoID=DICD.ClaimantPersonalInfoID
WHERE DI.DefenseInvoiceID = @DefenseInvoiceID
GO
GRANT ALTER ON  [dbo].[qrysrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qrysrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GRANT CONTROL ON  [dbo].[qrysrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qrysrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GO
