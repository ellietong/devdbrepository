SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HISTORICAL_qryUpdateMergedFilePath]
(@DocumentNumber nvarchar(MAX), @Location nvarchar(MAX))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE [FINAL_DocXRef_GLD]
	SET MergedFilePath = @Location
	WHERE DOCUMENT_NUMBER = @DocumentNumber
END

GO
GRANT VIEW DEFINITION ON  [dbo].[HISTORICAL_qryUpdateMergedFilePath] TO [base_user]
GRANT EXECUTE ON  [dbo].[HISTORICAL_qryUpdateMergedFilePath] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[HISTORICAL_qryUpdateMergedFilePath] TO [power_user]
GRANT EXECUTE ON  [dbo].[HISTORICAL_qryUpdateMergedFilePath] TO [power_user]
GO
