SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_INVOICE_qryLegalBillStatusFilterList]
AS
SET NOCOUNT ON

SELECT LegalBillStatusID, 
	CASE WHEN LegalBillStatus = 'Finalized' THEN 'Finalized / Paid' ELSE LegalBillStatus END AS LegalBillStatus
FROM m_tblLegalBillStatus
UNION
SELECT NULL, '<< All Statuses >>'
ORDER BY LegalBillStatus

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryLegalBillStatusFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryLegalBillStatusFilterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryLegalBillStatusFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryLegalBillStatusFilterList] TO [power_user]
GO
