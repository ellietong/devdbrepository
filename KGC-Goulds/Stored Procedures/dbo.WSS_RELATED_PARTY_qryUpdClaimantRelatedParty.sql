SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty]
    (
      @ClaimantRelatedPartyID BIGINT ,
      @FirstName NVARCHAR(500) ,
      @MiddleName NVARCHAR(500) ,
      @LastName NVARCHAR(500) ,
      @RelationshipID INT ,
      @LegalCapacityID INT ,
      @FullSSN NVARCHAR(15) ,
      @RPAddress1 NVARCHAR(250) ,
      @RPAddress2 NVARCHAR(250) ,
      @RPCity NVARCHAR(250) ,
      @StateID INT ,
      @RPZip NVARCHAR(5) ,
      @RPMailZip NVARCHAR(5) ,
      @RPPhone NVARCHAR(20) ,
      @RPExtension NVARCHAR(10)
    )
AS 
    SET NOCOUNT ON

    UPDATE  tblClaimantRelatedParty
    SET     FirstName = UPPER(@FirstName) ,
            MiddleName = UPPER(@MiddleName) ,
            LastName = UPPER(@LastName) ,
            RelationshipID = @RelationshipID ,
            LegalCapacityID = @LegalCapacityID ,
            RPSocialSecurityNumber = CASE WHEN @FullSSN = '0'
                                               OR LEN(@FullSSN) = 0 THEN NULL
                                          ELSE REPLACE(REPLACE(@FullSSN, '-',
                                                              ''), ' ', '')
                                     END ,
            RPAddress1 = UPPER(@RPAddress1) ,
            RPAddress2 = UPPER(@RPAddress2) ,
            RPCity = UPPER(@RPCity) ,
            StateID = @StateID ,
            RPZip = @RPZip ,
            RPMailZip = @RPMailZip ,
            RPPhone = CONVERT(BIGINT, REPLACE(REPLACE(REPLACE(@RPPhone, '-',
                                                              ''), '(', ''),
                                              ')', '')) ,
            RPExtension = @RPExtension
    WHERE   ClaimantRelatedPartyID = @ClaimantRelatedPartyID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [power_user]
GO
