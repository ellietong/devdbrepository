SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_INVOICES_qryFirmInvoices_AwaitingApproval] (@emailaddress nvarchar(1000) = NULL)
AS

--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'hagep@kcicllc.com'

DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE di.InvoiceLink IS NULL AND dt.ClaimantDocumentGroupID = 2 AND di.StatusID IN (1, 2)'
--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF (@DefenseCounselID IN (277) OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'))
	SET @JoinString = ' '
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND di.FirmID = ' + CONVERT(nvarchar(20),@DefenseCounselID)
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
    'SELECT DISTINCT CAST(d.DocumentID as varchar(50)) as DocumentId,
		d.DocumentTitle,
		d.DocumentURL,				
		di.FirmID,
		f.Firm,		
		dt.ClaimantDocumentType, 
		di.Period,
		di.InvoiceNumber,
		di.DatePayable,
		dis.[Status],
		d.AcceptedBy,
		d.DateAccepted,
		d.DateCreated,
		v.VendorFirm as VendorFirm,
		di.SubmitDate,
		di.InvoiceNotes,
		CASE WHEN (dicd.Fees + dicd.Costs - dicd.Adjustments) <> 0 THEN ROUND((CAST(UticaAmount AS Float)/(dicd.Fees + dicd.Costs - dicd.Adjustments)), 4) ELSE 0 END AS UticaShare,
		--CASE WHEN IsRecommended = 1 THEN ''Yes'' ELSE '''' END AS IsRecommended,
		--IsRecommended AS IsRecommendedID,
		dinv.InvoiceTotal,
		dicd.Adjustments,
		UticaAmount, 
		GouldsAmount,
		Coalesce(IsRecommended,0) AS IsRecommended
	FROM tblDocuments d
		INNER JOIN tblClaimantDocumentType dt	ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
		INNER JOIN tblClaimantDocumentGroup dg	ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
		LEFT JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
		LEFT JOIN vtblDefenseInvoice dinv		ON dinv.DocumentID = d.DocumentID
		LEFT JOIN  tblDefenseInvoiceStatus dis	ON di.StatusID = dis.StatusID 
		LEFT JOIN  vtblFirm f					ON di.FirmID = f.FirmID
		LEFT JOIN  tblPaymentDefense pd			ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN  tblCheck c					ON pd.CheckID = c.CheckID
		LEFT JOIN  m_tblvendorfirm v  			ON v.VendorFirmID = di.VendorFirmID
		LEFT JOIN (SELECT SUM(InsurerAmount) AS UticaAmount, 
						  SUM(Adjustments) as Adjustments, 
						  SUM(Fees) AS Fees, 
						  SUM(Costs) AS Costs, 
						  SUM(ClientAmount) AS GouldsAmount, 
						  DocumentID
				   FROM tblDefInvClaimantDetail
				   GROUP BY DocumentID) dicd on di.DocumentID = dicd.DocumentID ' +
    @WhereString + '
	ORDER BY IsRecommended desc, SubmitDate
	OPTION (RECOMPILE)'

Print @DefenseCounselID

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryFirmInvoices_AwaitingApproval] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryFirmInvoices_AwaitingApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryFirmInvoices_AwaitingApproval] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryFirmInvoices_AwaitingApproval] TO [power_user]
GO
