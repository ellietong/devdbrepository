SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptAdditionalNamedIns]
(@policyid int = null)
As

SET NOCOUNT ON

SELECT * 
FROM tblAdditionalNamedInsured
WHERE @policyid = PolicyID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAdditionalNamedIns] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAdditionalNamedIns] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAdditionalNamedIns] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAdditionalNamedIns] TO [power_user]
GO
