SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HISTORICAL_qryGetUniqueFiles]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT COALESCE(LTRIM(RTRIM(Document_Title)), Document_Number) AS DocumentTitle, 
			Document_Number AS DocumentNumber,
			COUNT(DISTINCT [FILE_NAME]) AS NumberOfPages
    FROM [FINAL_DocXRef_GLD]
    WHERE MergedFilePath IS NULL AND COALESCE(DOCUMENT_TITLE, Document_Number) NOT LIKE '%.%'
    GROUP BY DOCUMENT_TITLE, DOCUMENT_NUMBER
    ORDER BY COUNT(DISTINCT [FILE_NAME]) DESC, DocumentNumber ASC
END

GO
GRANT VIEW DEFINITION ON  [dbo].[HISTORICAL_qryGetUniqueFiles] TO [base_user]
GRANT EXECUTE ON  [dbo].[HISTORICAL_qryGetUniqueFiles] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[HISTORICAL_qryGetUniqueFiles] TO [power_user]
GRANT EXECUTE ON  [dbo].[HISTORICAL_qryGetUniqueFiles] TO [power_user]
GO
