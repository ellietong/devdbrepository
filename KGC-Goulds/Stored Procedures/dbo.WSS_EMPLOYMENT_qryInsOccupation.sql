SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryInsOccupation]
    (
      @Occupation NVARCHAR(250)
    )
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

IF (SELECT COUNT(OccupationID) FROM tblOccupation WHERE Occupation LIKE '' + RTRIM(LTRIM(@Occupation)) + '') > 0
		BEGIN 
		SELECT TOP 1 OccupationID 
		FROM tblOccupation o		
		WHERE Occupation LIKE '' + RTRIM(LTRIM(@Occupation)) + ''
	END
ELSE 
	BEGIN
		INSERT INTO tblOccupation_ALT ( Occupation)
		SELECT  @Occupation
		
		SELECT  MAX(OccupationID)
		FROM    tblOccupation
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsOccupation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsOccupation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsOccupation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsOccupation] TO [power_user]
GO
