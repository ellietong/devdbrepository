SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryGetPercentagesByDOFE]
	-- Add the parameters for the stored procedure here
@ClaimantID nvarchar(50), @InvoiceDOFE int 
AS
BEGIN
 
--DECLARE @ClaimantID nvarchar(50) = '116723'
--DECLARE @InvoiceDOFE int 
--SET @InvoiceDOFE =  '1980' /*DOFE Match*/ 
--SET @InvoiceDOFE =  '1980' /*DOFE Mismatch*/

	SET NOCOUNT ON;

DECLARE @ClaimantPersonalInfoID int
SET @ClaimantPersonalInfoID = (SELECT ClaimantpersonalinfoID
							   FROM tblClaimantPersonalInfo cpi
							   WHERE CONVERT(NVARCHAR(250),cpi.ClaimantPersonalInfoID) LIKE LTRIM(RTRIM(@ClaimantID))
									 OR cpi.Reference_ID LIKE LTRIM(RTRIM(@ClaimantID)))
--Get DOFE for Claimant
DECLARE @ClaimantDOFE float = NULL
SET @ClaimantDOFE = 
(
	SELECT COALESCE (CONVERT(nvarchar(50), (CASE WHEN CONVERT(nvarchar(50), COALESCE(YEAR(MIN(empstartdate)), YEAR(MIN(startdate))), 101) IS NULL
												 THEN '9999'
											ELSE
												(CASE WHEN COALESCE(YEAR(MIN(empstartdate)), Getdate()) < COALESCE(YEAR(MIN(startdate)), Getdate()) 
													  THEN CONVERT(nvarchar(50), YEAR(MIN(empstartdate)), 101) 
													  ELSE CONVERT(nvarchar(50), YEAR(MIN(startdate)), 101) END)
											END), 101), '9999') AS DOFE
	FROM tblClaimantEmployment ce
		LEFT JOIN tblClaimantProduct cp on ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
)


IF @ClaimantDOFE <> 9999 --If the claimant has a DOFE
	BEGIN
		
		--Get the Utica and Goulds Percentages based on the DOFE from the claimant's data
		DECLARE @ClaimantPercentageTable TABLE (ClaimantID int, ClaimantDOFE int, UticaClaimantPercent float, GouldsClaimantPercent float)
		INSERT INTO @ClaimantPercentageTable 
		SELECT DISTINCT @ClaimantPersonalInfoID, DOFE, COALESCE(UticaPercent, 0), COALESCE(GouldsPercent, 0)
		FROM m_tblInsurerPercentageByYear 
		WHERE DOFE = @ClaimantDOFE
		GROUP BY DOFE, UticaPercent, GouldsPercent
		
		--Get the Utica and Goulds Percentages based on the DOFE from tbe excel spreadsheet
		DECLARE @InvoicePercentageTable TABLE (ClaimantID int, InvoiceDOFE int, UticaInvoicePercent float, GouldsInvoicePercent float)
		INSERT INTO @InvoicePercentageTable 
		SELECT DISTINCT @ClaimantPersonalInfoID, DOFE, COALESCE(UticaPercent, 0), COALESCE(GouldsPercent, 0)
		FROM m_tblInsurerPercentageByYear 
		WHERE DOFE = @InvoiceDOFE
		
		
		DECLARE @PercentageTable TABLE (ClaimantID int, UticaInvoicePercent float, GouldsInvoicePercent float, UticaClaimantPercent float, GouldsClaimantPercent float)
		INSERT INTO @PercentageTable
		SELECT DISTINCT @ClaimantPersonalInfoID, UticaInvoicePercent, GouldsInvoicePercent, UticaClaimantPercent, GouldsClaimantPercent
		FROM @ClaimantPercentageTable cpt
		LEFT JOIN @InvoicePercentageTable ipt on cpt.ClaimantID = ipt.ClaimantID			
		
		--Compare Utica and Goulds percentages for claimant's DOFE to Utica and Goulds percentages invoice's DOFE
		SELECT CASE WHEN UticaInvoicePercent = UticaClaimantPercent 
				    THEN 0 
				    ELSE 1 END 
								+ 
			  CASE WHEN GouldsInvoicePercent = GouldsClaimantPercent 
					THEN 0 
					ELSE 1 END 
								AS CountMistmatch		
		FROM @PercentageTable pt
		
	END
ELSE
	IF (@InvoiceDOFE = 9999 AND @ClaimantDOFE = 9999)
		SELECT 0
	ELSE
		SELECT 1	 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryGetPercentagesByDOFE] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryGetPercentagesByDOFE] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryGetPercentagesByDOFE] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryGetPercentagesByDOFE] TO [power_user]
GO
