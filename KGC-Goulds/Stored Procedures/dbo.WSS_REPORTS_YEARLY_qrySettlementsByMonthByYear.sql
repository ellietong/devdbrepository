SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_YEARLY_qrySettlementsByMonthByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @StartDate DATETIME=null, @EndDate DATETIME=null, @EmailAddress NVARCHAR(250))  
AS

--DECLARE 
--@MatterID int=1, 
--@StartDate DATETIME='1/1/2013',
--@EndDate DATETIME='12/31/2013'

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET @EmailAddress = 'hagep@kcic.com'	
--DECLARE @MatterID int = null,
--		@StartDate DATETIME = null,
--		@EndDate DateTime = null,
--		@EmailAddress nvarchar(max) = 'owenss@kcic.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)


DECLARE @WhereString NVARCHAR(MAX) 
SET @WhereString = ' WHERE COALESCE(SettlementDate,statusdate) between ''' + COALESCE(CONVERT(NVARCHAR, @StartDate),'1/1/1900') + ''' AND ''' + COALESCE(CONVERT(NVARCHAR, @EndDate), '1/1/2100') + ''' 
					AND ClaimLawsuitStatusID IN (23,25) 
					AND  IsPrimaryStatus=1 '

DECLARE @GroupByString NVARCHAR(MAX) 
SET @GroupByString = 'GROUP BY DATENAME("mm", COALESCE(SettlementDate,statusdate))+'' ''+CONVERT(NVARCHAR,YEAR(COALESCE(SettlementDate,statusdate))), MONTH(COALESCE(SettlementDate,statusdate)), YEAR(COALESCE(SettlementDate,statusdate))'

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @GroupByString = @GroupByString + ' ,dj.DefenseCounselAddressID '
END

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @QueryString NVARCHAR(MAX)

SET @QueryString = 
	'SELECT DISTINCT  DATENAME("mm", COALESCE(SettlementDate,statusdate))+'' ''+CONVERT(NVARCHAR,YEAR(COALESCE(SettlementDate,statusdate))) AS [MonthName], COUNT(distinct(cl.ClaimLawsuitID)) AS Count, MONTH(COALESCE(SettlementDate,statusdate)) As MonthID
FROM tblClaim c
INNER JOIN tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
INNER JOIN tblLawsuit l ON cl.LawsuitID=l.LawsuitID
		' + @JoinString + '
INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
INNER JOIN tblClaimantSettlement cs ON cs.ClaimLawsuitID=cl.ClaimLawsuitID
	' + @WhereString + '
	' + @GroupByString + '
ORDER BY MONTH(COALESCE(SettlementDate,statusdate))
'
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

END







GO
