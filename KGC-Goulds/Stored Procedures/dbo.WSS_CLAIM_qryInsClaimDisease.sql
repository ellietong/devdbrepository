SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryInsClaimDisease] 
	-- Add the parameters for the stored procedure here
    (
      @ClaimID INT ,
      @DiseaseID INT ,
      @DiseaseDiagnosisDate DATETIME ,
      @DoctorID INT ,
      @Specialty NVARCHAR(1000) ,
      @chkIsPrimaryDisease BIT ,
      @chkIsDefenseDisease BIT,
      @Emailaddress NVARCHAR(250)
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
    
        IF @chkIsPrimaryDisease <> 0 
            BEGIN
                UPDATE  tblClaimDisease
                SET     PrimaryDisease = 0
                WHERE   ClaimID = @ClaimID
            END

        INSERT  INTO tblClaimDisease
                ( ClaimID ,
                  DiseaseID ,
                  DiseaseDiagnosisDate ,
                  DoctorID ,
                  SPUsername ,
                  IsDefenseDisease ,
                  PrimaryDisease
                )
        VALUES  ( @ClaimID ,
                  ( @DiseaseID ) ,
                  ( @DiseaseDiagnosisDate ) ,
                  @DoctorID ,
                  @Emailaddress ,
                  @chkIsDefenseDisease ,
                  @chkIsPrimaryDisease
                )

        UPDATE  tblDoctor
        SET     DoctorNote = @Specialty
        WHERE   DoctorID = @DoctorID	


    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsClaimDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsClaimDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsClaimDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsClaimDisease] TO [power_user]
GO
