SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[WSS_INVOICES_qryUpdInvoicesRecommended] (@DocumentID int, @EmailAddress nvarchar(1000), @IsRecommended bit = null)
AS

BEGIN
	UPDATE tblDefenseInvoice
	SET RecommendedBy = @EmailAddress,
		IsRecommended = @IsRecommended,
		DateRecommended =  GETDATE()
	WHERE DocumentID = @DocumentID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdInvoicesRecommended] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdInvoicesRecommended] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdInvoicesRecommended] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdInvoicesRecommended] TO [power_user]
GO
