SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_UPLOAD_updDocumentByNCC]
    (
      @ClaimantDocumentTypeID INT ,					--aka ClaimantDocumentTypeID
	--@ClaimantPersonalInfoID bigint = null,	
      @Comments NVARCHAR(4000) = NULL ,			--comments are optional		
      @DocumentURL NVARCHAR(4000) ,
      @DocumentTitle NVARCHAR(4000) ,
      @MatterID INT = NULL ,					--this is the guid assigned by SharePoint
      @ModifiedBy NVARCHAR(250) ,
      @DateModified DATETIME,
      @DocumentSourceID int,
      @DocID int 

    )
AS 
    BEGIN
        UPDATE  tblDocuments
        SET     ClaimantDocumentTypeID = @ClaimantDocumentTypeID ,
                Comments = @Comments ,
                DocumentURL = @DocumentURL ,
                DocumentTitle = @DocumentTitle ,	
                ModifiedBy = @ModifiedBy ,
                DateModified = @DateModified ,
                DocumentSourceID = @DocumentSourceID ,
                MatterID = @MatterID
        WHERE   DocumentID = @DocID

        SELECT  @DocID
    END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_updDocumentByNCC] TO [power_user]
GO
