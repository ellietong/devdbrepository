SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail]
(@FirmID int, @InvoiceNumber nchar(10), @InvoiceDate date, @ClaimantPersonalInfoID int, @Fees money, @Costs money, 
@Adjustments money, @Notes nvarchar(mAX), @PACEClaimID int,  @DocumentID int, @DOFE int)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO tblDefInvClaimantDetail ([FirmID]
      ,[InvoiceNumber]
      ,[InvoiceDate]
      ,[ClaimantPersonalInfoID]
      ,[DOFE]
      ,[Fees]
      ,[Costs]
      ,[Adjustments]
      ,[Notes]
      ,[PACEClaimID]
      ,[DocumentID]) VALUES (@FirmID, @InvoiceNumber, @InvoiceDate, @ClaimantPersonalInfoID, @DOFE, @Fees, @Costs, @Adjustments, @Notes, @PACEClaimID, @DocumentID)

		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInsertDefInvoiceClaimantDetail] TO [power_user]
GO
