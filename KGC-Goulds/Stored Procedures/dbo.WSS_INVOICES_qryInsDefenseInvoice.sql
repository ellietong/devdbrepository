SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryInsDefenseInvoice]
	-- Add the parameters for the stored procedure here
	--@DocID int = null,
	----@DocumentURL nvarchar(4000),
	--@InvoiceNumber nvarchar(50) = null,
	--@Period nvarchar(50) = null,
	--@DatePayable datetime = null,
	----@FeeAmount money = null,
	----@DisbursementAmount money = null,
 --   --@Adjustment money = null,
 --   --@AdjustmentNotes nvarchar(4000) = null,
 --   @StatusID int = null,
 --   @FirmID int=null,
 --   --@PrivateComments nvarchar(4000) = null,
 --   @VendorFirmID int=null
 --   --@Approved int=null,
 --   --@ApprovedBy nvarchar(250) = null,    
	----@DateApproved date=null   
	
	(
	
	@DocID int ,
	@InvoiceNumber nvarchar(50) ,
	@Period nvarchar(50) ,
	@DatePayable datetime ,
    @StatusID int ,
    @FirmID int ,
    @VendorFirmID int,
    @SpreadsheetAmazonKey nvarchar(MAX) = null
    )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
INSERT INTO tblDefenseInvoice (DocumentID, StatusID, FirmID, DefenseCounselAddressID, VendorFirmID, InvoiceNumber, 
Period, SubmitDate, DatePayable, SpreadsheetAmazonKey)
VALUES (
	@DocID,
	@StatusID,
	@FirmID,
	(Select DefenseCounselAddressID from tblDefenseCounselAddress where PayeeAddress=1 and DefenseCounselID=@FirmID),
	CASE WHEN (Select @VendorFirmID) = 0 THEN @FirmID ELSE @VendorFirmID END,
	@InvoiceNumber,
	@Period,
	GETDATE(),
	@DatePayable,
	@SpreadsheetAmazonKey)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInsDefenseInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInsDefenseInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryInsDefenseInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryInsDefenseInvoice] TO [power_user]
GO
