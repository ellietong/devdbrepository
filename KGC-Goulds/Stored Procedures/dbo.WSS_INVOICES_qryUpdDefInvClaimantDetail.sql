SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryUpdDefInvClaimantDetail]
    (
      @InvoiceDetailID INT ,
      @Adjustments MONEY ,
	  @InsurerAmount MONEY ,
      @ClientAmount MONEY ,
      @Notes NVARCHAR(MAX)
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        UPDATE  tblDefInvClaimantDetail
        SET     Adjustments = @Adjustments ,
                Notes = @Notes ,
                InsurerAmount = @InsurerAmount,
                ClientAmount = @ClientAmount
        WHERE   InvoiceDetailID = @InvoiceDetailID
	
	
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdDefInvClaimantDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdDefInvClaimantDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdDefInvClaimantDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdDefInvClaimantDetail] TO [power_user]
GO
