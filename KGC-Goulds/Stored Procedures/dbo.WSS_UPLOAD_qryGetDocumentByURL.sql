SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryGetDocumentByURL] ( @URL NVARCHAR(4000) )
AS 
    SELECT  COUNT(*)
    FROM    tblDocuments
    WHERE   DocumentURL = @URL
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetDocumentByURL] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetDocumentByURL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetDocumentByURL] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetDocumentByURL] TO [power_user]
GO
