SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RELATED_PARTY_qryInsClaimantRelatedParty]
    (
      @FirstName NVARCHAR(100) ,
      @MiddleName NVARCHAR(100) ,
      @LastName NVARCHAR(100) ,
      @RelationshipID INT ,
      @LegalCapacityID INT ,
      @Address1 NVARCHAR(250) ,
      @Address2 NVARCHAR(250) ,
      @City NVARCHAR(250) ,
      @StateID INT ,
      @Zip NVARCHAR(100) ,
      @MailZip NVARCHAR(100) ,
      @Phone NVARCHAR(100) ,
      @Extension NVARCHAR(100) ,
      @SSN NVARCHAR(50) ,
      @ClaimantPersonalInfoID BIGINT ,
      @emailaddress NVARCHAR(100)
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        INSERT  INTO tblClaimantRelatedParty
                SELECT  @ClaimantPersonalInfoID ,
                        UPPER(@FirstName) ,
                        UPPER(@MiddleName) ,
                        UPPER(@LastName) ,
                        @RelationshipID ,
                        NULL AS LegalCapacity ,
                        @LegalCapacityID ,
                        UPPER(@Address1) ,
                        UPPER(@Address2) ,
                        UPPER(@City) ,
                        @StateID ,
                        @Zip ,
                        @MailZip ,
                        CONVERT(BIGINT, REPLACE(REPLACE(REPLACE(@Phone, '-',
                                                              ''), '(', ''),
                                                ')', '')) ,
                        @Extension ,
                        @SSN ,
                        NULL ,
                        @Emailaddress
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryInsClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryInsClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryInsClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryInsClaimantRelatedParty] TO [power_user]
GO
