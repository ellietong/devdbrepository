SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_AUTHORITY_EMAIL_DAILY_qryNewApprovals] 
AS
SET NOCOUNT ON
 
BEGIN
 
 
/*
	TEST VARS
*/
--DECLARE @SettlementAuthorityID bigint = 1
 
--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)
 
Declare @Today10AM datetime
Declare @Yesterday10AM datetime
SET @Yesterday10AM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-1))+' 10:00:00.000')
SET @Today10AM = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 10:00:00.000')
DECLARE @SendEmail bit = 0
	
	SET @Subject = 	'Goulds Payment Approval Summary ' + CONVERT(varchar, getdate(), 107) 
 
	--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
	--KCIC logo is temporarily being pulled from D. Snyder's Google site	
 
 
	SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
 
 
 
 
	--Declare and populate the body of the email for individual firms
	DECLARE @BodyMessage nvarchar(MAX)
 
	--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
	--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
	SET @BodyMessage = @LogoHeader + 
	'<STYLE TYPE="text/css">
	<!--
	TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
	TD	
	{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
		
	}
	TH	
	{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
	UL {margin:0; 
		
	padding:0;}	
 
 
	--->
	</STYLE>'
 
 
 
	SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
	--CSS
	DECLARE @MainHeaderStyle nvarchar(MAX) = 'background-color: #0079C1; font-weight: bold; color: white; padding: 4px;'
	DECLARE @tdStyle nvarchar(MAX) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'
 
IF(SELECT COUNT(*)
	FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
	INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID	
	LEFT join tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
	LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
			AND clst.ClaimLawsuitStatusID = 23
			AND cs.PaymentApprovedDate BETWEEN @Yesterday10AM AND @Today10AM
			AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
			--AND ExecutedReleaseDate IS NOT NULL
			) > 0
BEGIN
	SET @SendEmail = 1
	
 
	--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
	set @BodyMessage = @BodyMessage + '<H4>Pending the validation of the DOFE for each claimant, the following settlement(s) have been approved for payment.<br/><br/>' + 
	'To view additional information please click ' + 
	'<a href="https://www.kcicextranet.com/clients/itt/gcp/Pages/SettlementPaymentApproval.aspx?approval=">here</a>. </H4>'
 
	--PRINT TABLE HEADERS
	SET @BodyMessage = @BodyMessage + '
	<table>
		<thead>
			<tr>
				<th style="' + @MainHeaderStyle + '">Claimant</th>
				<th style="' + @MainHeaderStyle + '">Settlement Group</th>
				<th style="' + @MainHeaderStyle + '">Plaintiff Counsel</th>
				
				<th style="' + @MainHeaderStyle + '">Settlement Date</th>
				<th style="' + @MainHeaderStyle + '">Goulds'' Amount</th>
				
			</tr>
		</thead>
	'
	--PRINT TABLE OF CLAIMS
	SET @BodyMessage = @BodyMessage +
 
		COALESCE((CAST((SELECT 
			[td/@style] = @tdStyle,
			[td/a/@href] = 'https://www.kcicextranet.com/clients/itt/gcp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar, c.ClaimantPersonalInfoID)+'&claimanttab=1&claimtab=2',
			[td/a] = cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''),
			[td] = '', '',
			[td/@style] = @tdStyle,
			td = COALESCE(UPPER(SettlementGroup), 'Unknown'), '',
			[td/@style] = @tdStyle,
			td = COALESCE(PlaintiffCounsel, 'Unknown'), '',
			[td/@style] = @tdStyle,
			--td = CONVERT(nvarchar, ExecutedReleaseDate, 101), '',
			--[td/@style] = @tdStyle,
			td = CONVERT(nvarchar, SettlementDate, 101), '',
			[td/@style] = @tdStyle,
			td = '$' + CONVERT(nvarchar, CONVERT(money, cs.ClientAmount)), ''
			
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
		INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		INNER JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID	
		LEFT join tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		LEFT  JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
		LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
			AND clst.ClaimLawsuitStatusID = 23
			AND cs.PaymentApprovedDate BETWEEN @Yesterday10AM AND @Today10AM
			AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
			--AND ExecutedReleaseDate IS NOT NULL
			--AND cs.SettlementGroupID >= COALESCE(@SettlementGroupID, 0) AND cs.SettlementGroupID <= COALESCE(@SettlementGroupID, @MaxSettlementGroupID)
			--TODO: add logic here to make sure the settlements have received authority
	GROUP BY	cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''), 
				c.ClaimantPersonalInfoID,
				PlaintiffCounsel,
				SettlementDate,
				SettlementGroup,
				cs.PaymentApprovedBy,
				cs.PaymentApprovedDate,
				ClaimantSettlementID,
				ExecutedReleaseDate,
				cs.ClientAmount
	FOR XML PATH('tr'), type) AS NVARCHAR(MAX))), '')
 
	--CLOSE TABLE
	SET @BodyMessage = @BodyMessage + '</table><br>'
 
END
--KCIC address and disclaimer
	SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
	733 10th Street, NW, Suite 3001<br>
	Washington, DC 20001<br>
	(t) 202-772-2300<br>
	(f) 202-772-2333'
 
--PRINT SubString(@BodyMessage, 1, 4000)
--PRINT SubString(@BodyMessage, 4001, 4000)
--PRINT SubString(@BodyMessage, 8001, 4000) 
 
DECLARE @RecipientList nvarchar(500)
SET @RecipientList='loree.shelko@itt.com;Craig.Johnson@itt.com' --ITT's NCC and KCIC users
 
----QC Recipient List
--Set @Bodymessage = @BodyMessage + 'Recipients' + @RecipientList
--PRINT @RecipientList
 
PRINT @SendEmail
 
IF(@SendEmail = 1)
BEGIN
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The Profile_Name sends from [client]@kcicllc.com email address
@profile_name='GouldsProfile',
@recipients = @RecipientList,
--@recipients = 'hagep@kcicllc.com;scottc@kcicllc.com',--;shockleym@kcicllc.com;kanga@kcicllc.com',--;prossc@kcicllc.com',
@copy_recipients = 'scottc@kcicllc.com;goulds@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
 
 
 
END
END
 
 

GO
