SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryAddToTrialGroup]
    (
      @ClaimLawsuitID BIGINT ,
      @TrialGroupID INT ,
      @ActiveTrialDate DATE ,
      @SPUsername NVARCHAR(250)
    )
AS 
BEGIN

--Update TrialGroupID, ActiveTrialDate & SPUsername fields
UPDATE  tblClaimLawsuit
	SET TrialGroupID = @TrialGroupID,
		ActiveTrialDate = @ActiveTrialDate,
		SPusername = @SPUsername
	WHERE ClaimLawsuitID = @ClaimLawsuitID

--UPDATED 11/17/2014 by CY: Check if the current ClaimLawsuitID is StatusID 39 (uncomfirmed); if so, and an ActiveTrialDate was filled in, change to StatusID 22 (Open)
IF YEAR(@ActiveTrialDate) >= 2012
BEGIN
	DECLARE @PrimaryClaimLawsuitStatusID INT
	SELECT @PrimaryClaimLawsuitStatusID = ClaimLawsuitStatusID FROM dbo.tblClaimLawsuitStatusTracking WHERE ClaimLawsuitID = @ClaimLawsuitID AND IsPrimaryStatus = 1

	IF @PrimaryClaimLawsuitStatusID = 39
	BEGIN
		UPDATE tblClaimLawsuitStatusTracking
			SET IsPrimaryStatus = 0
			WHERE ClaimLawsuitID = @ClaimLawsuitID
				AND IsPrimaryStatus = 1

		INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, ProcessedDate, StatusNotes)
			SELECT @ClaimLawsuitID, 22, GETDATE(), 1, GETDATE(), 'Per request of ' + @SPUsername + ', Unconfirmed cases with Trial dates should be changed to Open.'
	END
END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryAddToTrialGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryAddToTrialGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryAddToTrialGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryAddToTrialGroup] TO [power_user]
GO
