SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[HISTORICAL_qryGetDocumentPages]
(@DocumentNumber nvarchar(MAX))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--DECLARE @DocumentNumber nvarchar(MAX) = '02081306'

	SELECT [FILE_NAME] AS [FileName], FOLDER
	FROM [FINAL_DocXRef_GLD]
	WHERE DOCUMENT_NUMBER = @DocumentNumber
	GROUP BY FILE_NAME, Folder
	ORDER BY FileName
END

GO
GRANT VIEW DEFINITION ON  [dbo].[HISTORICAL_qryGetDocumentPages] TO [base_user]
GRANT EXECUTE ON  [dbo].[HISTORICAL_qryGetDocumentPages] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[HISTORICAL_qryGetDocumentPages] TO [power_user]
GRANT EXECUTE ON  [dbo].[HISTORICAL_qryGetDocumentPages] TO [power_user]
GO
