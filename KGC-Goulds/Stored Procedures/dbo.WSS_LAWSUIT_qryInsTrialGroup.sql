SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryInsTrialGroup]
    (
      @TrialGroup NVARCHAR(200) ,
      @SPUsername NVARCHAR(250)
    )
AS 
    BEGIN
        INSERT  INTO tblTrialGroup
                SELECT  @TrialGroup ,
                        @SPUsername
	
        SELECT  @@IDENTITY
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryInsTrialGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryInsTrialGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryInsTrialGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryInsTrialGroup] TO [power_user]
GO
