SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[WSS_SERVICES_qryTrialGroupList]
(@PrefixText nvarchar(200), @Emailaddress nvarchar(250) )
AS
BEGIN
	SELECT TrialGroupID, TrialGroup --+ ' (' + CONVERT(nvarchar, TrialGroupID) + ')'
	FROM tblTrialGroup
	WHERE TrialGroup LIKE '%' + @PrefixText + '%'
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryTrialGroupList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryTrialGroupList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryTrialGroupList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryTrialGroupList] TO [power_user]
GO
