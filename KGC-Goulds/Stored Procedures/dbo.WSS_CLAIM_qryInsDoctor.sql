SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryInsDoctor]
    (
      @DoctorFName NVARCHAR(250) ,
      @DoctorMName NVARCHAR(250) ,
      @DoctorLName NVARCHAR(250) ,
      @DoctorSpecialty NVARCHAR(250) ,
      @SPUserName NVARCHAR(250)
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        INSERT  INTO tblDoctor
                ( DoctorFirstName ,
                  DoctorMiddleName ,
                  DoctorLastName ,
                  DoctorNote
                )
                SELECT  @DoctorFName ,
                        @DoctorMName ,
                        @DoctorLName ,
                        @DoctorSpecialty
		
        SELECT  MAX(DoctorID)
        FROM    tblDoctor
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsDoctor] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsDoctor] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsDoctor] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsDoctor] TO [power_user]
GO
