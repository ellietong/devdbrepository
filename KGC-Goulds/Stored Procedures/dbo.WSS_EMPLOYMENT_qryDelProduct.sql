SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryDelProduct]
    (
      @ClaimantProductID INT ,
      @emailaddress NVARCHAR(250)
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    --Update the record first to record who is deleting it
        UPDATE  tblClaimantProduct
        SET     SPUsername = @emailaddress
        WHERE   ClaimantProductID = @ClaimantProductID
    
        DELETE  FROM tblClaimantProduct
        WHERE   ClaimantProductID = @ClaimantProductID
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryDelProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryDelProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryDelProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryDelProduct] TO [power_user]
GO
