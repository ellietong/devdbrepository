SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryGetClaimantBirthDateByID]
	-- Add the parameters for the stored procedure here
@ClaimantPersonalInfoID bigint,
@EmailAddress nvarchar(250) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT COALESCE(DATEADD(dd, DATEDIFF(dd, 0, BirthDate), 0), '1/1/1900')
FROM tblClaimantPersonalInfo
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryGetClaimantBirthDateByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryGetClaimantBirthDateByID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryGetClaimantBirthDateByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryGetClaimantBirthDateByID] TO [power_user]
GO
