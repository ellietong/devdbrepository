SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryBILLInsertExhaustion]
(@exhaustionDate datetime, @allocationID int, @description varchar(100), @exhaustionDateID int OUT, @exhaustionTypeID int)
AS
-- Procedure takes exhaustion date, allocation ID, description, and bill group ID inputs and populates tblExhaustion and tblExhaustionDate, which memorialize and track the exhaustion 
-- If a billing module is included, tblLedger (and the bill) is also updated using the stored procedure UpdateLedger
SET NOCOUNT ON
-- Insert data to tblExhaustionDate
INSERT INTO tblExhaustionDate (ExhaustionDate, ExhaustionTypeID, AllocationID, Description)
VALUES (@exhaustionDate, @exhaustionTypeID, @allocationID, @description)
-- Capture exhaustion date ID
SET @exhaustionDateID = @@IDENTITY
-- Insert data to tblExhaustion from tblAllocationResults

INSERT INTO tblExhaustion (ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation, DefenseAllocation, DefenseOutsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation)
SELECT @exhaustionDateID, OccurrenceID, PolicyDateID, AllocationIndemnity, AllocationDefense, AllocationDefenseOutsideLimits, SIRAllocatedIndemnity, SIRAllocatedDefense
FROM tblAllocationResults
WHERE AllocationID = @allocationID

------------------GOULDS SPECIFIC---------------------------------------------------------------------------------------------------

----MOVE DEFENSE ALLOCATION FROM LAST CONTINENTAL POLICY TO UNCOLLAPSED TABLE (TO GO TO PRODUCER)
insert into tblAllocationResultsHorizontal (AllocationID, OccurrenceID, uncollapsedPost)
select @allocationid, occurrenceid, defenseoutsidelimitsallocation
from tblExhaustion
where ExhaustionDateID = @EXHAUSTIONDATEID and PolicyDateID = 166 and DefenseOutsideLimitsAllocation <>0

update tblExhaustion set DefenseOutsideLimitsAllocation = 0 where PolicyDateID = 166 and DefenseOutsideLimitsAllocation <>0
and ExhaustionDateID = @EXHAUSTIONDATEID


----REALLOCATE UTICA AMOUNTS TO PRODUCER WHERE UTICA PAID $0 AND GOULDS PAID >$0 (Defense)

-- Step 1 - insert records into tblAllocationResultsHorizontal to be picked up by Producer. Occurrences to reallocate flagged with Segment= 2.
insert into tblallocationresultshorizontal (allocationid, occurrenceid, uncollapsedprior)
select @allocationid, e.occurrenceid, sum(defenseoutsidelimitsallocation+defenseallocation+indemnityallocation)
from tblexhaustion e
inner join tbloccurrence o on o.occurrenceid=e.occurrenceid
where segment = 2
and policydateid in (157, 159, 168, 170, 172, 174, 177)
and exhaustiondateid=@exhaustiondateid
group by e.occurrenceid


-- Step 2 - insert records to back amounts out of Utica policies.  Occurrences to reallocate flagged with Segment= 2
insert into tblexhaustion (ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation, DefenseAllocation, 
DefenseOutsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation, LedgerID, OutsideConsumption, ConsumptionType, BillNumber)
select exhaustiondateid, e.occurrenceid, policydateid, indemnityallocation*-1, defenseallocation*-1, defenseoutsidelimitsallocation*-1,
SIRIndemnityAllocation, SIRDefenseAllocation, LedgerID, OutsideConsumption, ConsumptionType, BillNumber
from tblexhaustion e
inner join tbloccurrence o on o.occurrenceid = e.occurrenceid
where segment = 2
--UTICA PolicyDateID's
and policydateid in (157, 159, 168, 170, 172, 174, 177)
and exhaustiondateid=@exhaustiondateid



RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryBILLInsertExhaustion] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryBILLInsertExhaustion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryBILLInsertExhaustion] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryBILLInsertExhaustion] TO [power_user]
GO
