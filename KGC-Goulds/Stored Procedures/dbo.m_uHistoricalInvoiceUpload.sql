SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_uHistoricalInvoiceUpload]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT InvoiceIdentifier
	FROM Import_InvoiceXReference_Goulds
	
	SELECT InvoiceIdentifier, 'J:\ITT_GOULDS_PACE_IMAGES\Invoiceattachments\' + [FILENAME] AS FileName
	FROM Import_InvoiceXReference_Goulds

END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_uHistoricalInvoiceUpload] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_uHistoricalInvoiceUpload] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_uHistoricalInvoiceUpload] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_uHistoricalInvoiceUpload] TO [power_user]
GO
