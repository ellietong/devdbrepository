SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmPaymentVendorInvoice] (@VendorFirmID int)
AS
SET NOCOUNT ON

--declare @VendorFirmID int
--set @VendorFirmID = 10235

SELECT di.DefenseInvoiceID, 
	FirmID as VendorFirmID, 
	InvoiceNumber, 
	Period, 
	InvoiceTotal, 
	ClientTotal-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount,
	ClientTotal
FROM vtblDefenseInvoice di
	LEFT JOIN (SELECT DefenseInvoiceID, PaymentAmount 
			   FROM tblCheckApproval ca 
			       Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID) a on a.DefenseInvoiceID = di.DefenseInvoiceID
WHERE FirmID IS NOT NULL
	AND StatusID = 6
GROUP BY di.DefenseInvoiceID, FirmID, InvoiceNumber, Period, InvoiceTotal, ClientTotal
HAVING ClientTotal - SUM(Coalesce(PaymentAmount,0)) <> 0 
	AND FirmID = @VendorFirmID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentVendorInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentVendorInvoice] TO [power_user]
GO
