SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch]
	-- Add the parameters for the stored procedure here
	@emailaddress nvarchar(1000)
AS
--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'mackley@melicklaw.com'

DECLARE @firmID int
SET @firmID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)
BEGIN
	DECLARE @emaildomain nvarchar(1000)
	SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress), LEN(@emailaddress)-CHARINDEX('@', @emailaddress)+1)
	IF @emaildomain = '@kcicllc.com' OR @emailDomain='@kcic.com' OR @firmID IN (277) OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1)
	BEGIN
		SELECT DISTINCT DefenseCounselID, DefenseCounsel  
		FROM tblDefenseCounsel
		WHERE DefenseCounselID NOT IN (277)
		UNION 
		SELECT NULL, ''
		ORDER BY DefenseCounsel
	END
	ELSE
	BEGIN
		Select dca.DefenseCounselID , dca.DefenseCounsel 
		From  dbo.tblDefenseCounselAddress dca
		Where dca.DefenseCounselID = @FirmID
		UNION 
		SELECT NULL, ''
		ORDER BY dca.DefenseCounsel
	END
		
	 
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetFirmByEmail_InvoiceSearch] TO [power_user]
GO
