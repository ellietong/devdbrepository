SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptSummaryReportByCarrierByPolicy]
(@BillExhaustionDateID int, @LimitTypeID int, @ExhaustionTypeID int)
As
Set Nocount on


--Declare @LimitTypeID int
--Declare @ExhaustionTypeID int 
--Declare @BillExhaustionDateID int

 
-- Set @LimitTypeID = 1
-- Set @ExhaustionTypeID = 2
-- Set @BillExhaustionDateID = 197
 

 --Previous Exhaustion
Declare @Exhaustion Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint,OutsideConsumption money )
Insert Into @Exhaustion
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation,
	DefenseAllocation, DefenseOUtsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation,
	LedgerID, OutsideConsumption 
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID<@BillExhaustionDateID 

Declare @PreviousExhaustionTotals table (ExhaustionTypeID int, PolicyDateID int, PreviousAllocationAmt money, OutsideConsumption money)
Insert Into @PreviousExhaustionTotals
Select ExhaustionTypeID, PolicyDateID,
	case when policydateid in (180, 183, 186) then (coalesce(Sum(DefenseAllocation + IndemnityAllocation),0)) else 0 end,
	OutsideConsumption 
From @Exhaustion
Group By ExhaustionTypeID, PolicyDateID, OutsideConsumption

--Current Allocation
Declare @CurrentAllocation Table (ExhaustionTypeID int, ExhaustionID bigint, ExhaustionDateID int, OccurrenceID bigint, PolicyDateID bigint,
	IndemnityAllocation money, DefenseAllocation money, DefenseOutsideLimitsAllocation money, SIRIndemnityAllocation money,
	SIRDefenseAllocation money, LedgerID bigint, OutsideConsumption money)
Insert Into @CurrentAllocation
Select ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, OccurrenceID, PolicyDateID, IndemnityAllocation,
	DefenseAllocation, DefenseOutsideLimitsAllocation, SIRIndemnityAllocation, SIRDefenseAllocation,
	LedgerID, OutsideConsumption 
From tblExhaustion E
Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
Where ExhaustionTypeID=@ExhaustionTypeID and ED.ExhaustionDateID=@BillExhaustionDateID

Declare @CurrentAllocationTotals table (ExhaustionTypeID int, PolicyDateID int, Indemnity money, Defense money,
	DefenseOutsideLimits money, TotalDefense money, SIRIndemnityAllocation money, SIRDefenseAllocation money)
Insert Into @CurrentAllocationTotals
Select ExhaustionTypeID, PolicyDateID,
	coalesce(Sum(IndemnityAllocation), 0) as Indemnity,
	coalesce(Sum(DefenseAllocation), 0) as Defense,
	coalesce(Sum(DefenseOutsideLimitsAllocation),0) as DefenseOutsideLimits,
	coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
	coalesce(Sum(SIRIndemnityAllocation), 0) as SIRIndemnity,
	coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense
From @CurrentAllocation
Group By ExhaustionTypeID, PolicyDateID

--Query Results

Declare @UncollapsedAmounts table (occurrenceid int, defense money, indemnity money)
insert into @uncollapsedAmounts
select o.occurrenceid, 
coalesce(sum(case when OccurrenceTypeID = 2 then coalesce((uncollapsedPost),0) + coalesce((uncollapsedPrior),0) end),0) As Defense,
coalesce(sum(case when OccurrenceTypeID = 1 then coalesce((uncollapsedPost),0) + coalesce((uncollapsedPrior),0) end),0) As Indmenity
from tbloccurrence o
left join tblallocationresultshorizontal arh on arh.occurrenceid = o.occurrenceid
where arh.allocationid = (select allocationid from tblExhaustionDate where ExhaustionDateID = @BillExhaustionDateID)
group by o.occurrenceid
union
select o.occurrenceid, 
coalesce(sum(case when OccurrenceTypeID = 2 then coalesce((uncollapsedVertical),0) end),0) As Defense,
coalesce(sum(case when OccurrenceTypeID = 1 then coalesce((uncollapsedVertical),0) end),0) As Indmenity
from tbloccurrence o
left join tblallocationresultsvertical arv on arv.occurrenceid = o.occurrenceid
where arv.allocationid = (select allocationid from tblExhaustionDate where ExhaustionDateID = @BillExhaustionDateID)
group by o.occurrenceid

Declare @Results Table(OrderID int, OrderName nvarchar(100),policyid bigint, policydateid bigint, carriername nvarchar(100), layer bigint, policynum nvarchar(100), 
pstartdate nvarchar(100), penddate nvarchar(100), attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), Indemnity money, Defense money, DefenseOutsideLimits money, TotalDefense money, PreviousAllocationAmt money,
	Balance money, OutsideConsumption money)
	
Insert Into @Results
select  '1' as OrderID, 'All Insurers', p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum,  convert(nvarchar(100), pstartdate, 101) as pstartdate, 
convert(nvarchar(100), PEndDate, 101)  as penddate,
	pl.attachmentpoint, pl.perocclimit, pl.aggregatelimit,
CASE when pd.PolicyDateID = 166 then 'I'
	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
 	Case when pd.policydateid in (180, 183, 186) then (coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)- coalesce(PreviousAllocationAmt,0)) else 0 end,
 	coalesce(OutsideConsumption,0) as OutsideConsumption
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where limittypeid = @LimitTypeID and CAT.ExhaustionTypeID = @ExhaustionTypeID and provisionlabelID=1 
union
----Goulds Producer
select  '2' as OrderID, 'Producer', '', '', 'Goulds Pumps'  as carriername,'100' as layer, 
'Producer' as PolicyNum, ' ' as pstartdate, ' ' as penddate,
'' as attachmentpoint, '-1000000000'  as perocclimit, '-1000000000'  as aggregatelimit, '' as defenseform,
sum(coalesce(indemnity,0)) as Indemnity,
sum(coalesce(defense,0)) as Defense,
'' as defenseoutsidelimits, 
sum(coalesce(defense,0)) as TotalDefense,
0 as previousallocationAmt, 
'-1000000000' as Balance , 
'' as outsideconsumption
from @UncollapsedAmounts


-- Header Summary
Declare @HeaderSummary Table (ExhaustionType nvarchar(100), Description nvarchar(100), DefenseForm nvarchar(10), footerdate nvarchar(50))
Insert into @HeaderSummary
Select ExhaustionType, [Description], 'H', Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@BillExhaustionDateID
union
Select ExhaustionType, [Description], 'I', Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where eD.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@BillExhaustionDateID
union
Select ExhaustionType, [Description], 'G', Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@BillExhaustionDateID
union
Select ExhaustionType, [Description], '', Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
Where ed.ExhaustionTypeID=@ExhaustionTypeID and ExhaustionDateID=@BillExhaustionDateID


Select R.*, ExhaustionType, [Description], hs.footerdate
From @Results R
Inner Join @HeaderSummary HS on R.DefenseForm=HS.DefenseForm
group by  r.policyid , r.OrderID, r.OrderName, r.policydateid , r.carriername , r.layer , r.policynum , r.pstartdate , r.penddate , r.attachmentpoint , r.perocclimit , r.aggregatelimit ,
	r.DefenseForm  , indemnity , Defense , DefenseOutsideLimits , TotalDefense , PreviousAllocationAmt ,
	Balance , OutsideConsumption , ExhaustionType, [Description], hs.footerdate
	order by carriername,   cast(r.pstartdate as date) , Cast(r.penddate as DATE),  policynum
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GO
