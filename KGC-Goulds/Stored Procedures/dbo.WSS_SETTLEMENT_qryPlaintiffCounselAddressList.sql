SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList]
AS 
    SET NOCOUNT ON

    SELECT  b.PlaintiffCounselAddressID ,
            PlaintiffCounsel + ' ('
            + CASE WHEN PlaintiffCounselCity IS NULL THEN 'UNKNOWN'
                   ELSE PlaintiffCounselCity
              END + ')' AS PlaintiffCounsel
    FROM    tblPlaintiffCounselAddress b
			inner join tblLawsuit l on l.PlaintiffCounselAddressID = b.PlaintiffCounselAddressID
    WHERE   PlaintiffCounsel IS NOT NULL
            AND PlaintiffCounsel <> ''
    Group by b.PlaintiffCounselAddressID,     PlaintiffCounsel + ' ('
            + CASE WHEN PlaintiffCounselCity IS NULL THEN 'UNKNOWN'
                   ELSE PlaintiffCounselCity
              END + ')'    
    UNION
    SELECT  NULL ,
            NULL
    ORDER BY b.PlaintiffCounsel + ' ('
            + CASE WHEN PlaintiffCounselCity IS NULL THEN 'UNKNOWN'
                   ELSE PlaintiffCounselCity
              END + ')' ,
            PlaintiffCounselAddressID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList] TO [power_user]
GO
