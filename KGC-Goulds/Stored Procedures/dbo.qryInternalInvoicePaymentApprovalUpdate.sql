SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCedure [dbo].[qryInternalInvoicePaymentApprovalUpdate]
AS
BEGIN
/*Query run daily to send an internal email listing any invoices which have been approved or rejected
and are ready to be paid. */

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (MAX)
DECLARE @LogoHeader nvarchar (2000)
DECLARE @Records int = 0

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

SET @Subject = 'Goulds: New Defense Invoices Approved/Rejected for Payment as of ' + CONVERT(varchar, GETDATE(), 107)

--Create a temp table for any invoices that have had a status change in the past day.
DECLARE @NewInvoicesTable TABLE (invoiceNum nvarchar (50), status nvarchar (50), dateapproved datetime,
firmid int, feeAmount money, disbursementamount money, adjustment money, adjustmentnotes nvarchar (500),
docid nvarchar (100), FirmAddressID int, DefenseCounsel nvarchar (100), VendorFirm nvarchar (100), invoicetype nvarchar (100))
INSERT INTO @NewInvoicesTable
	SELECT
		di.InvoiceNumber,
		[Status],
		DateApproved,
		di.FirmID,
		SUM(dic.Fees),
		SUM(dic.Costs),
		SUM(dic.adjustments),
		di.InvoiceNotes,
		di.DocumentID,
		di.DefenseCounselAddressID,
		dc.DefenseCounsel,
		v.VendorFirm,
		ClaimantDocumentType
	FROM tblDefenseInvoice di
	JOIN tblDocuments d ON d.DocumentID = di.DocumentID
	INNER JOIN tblDefenseInvoiceStatus s ON s.StatusID = di.StatusID
	INNER JOIN tblDefInvClaimantDetail dic ON dic.DocumentID = di.DocumentID
	LEFT JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselAddressID = di.DefenseCounselAddressID
	LEFT JOIN tblDefenseCounsel dc ON dc.DefenseCounselID = dca.DefenseCounselID
	LEFT JOIN m_tblVendorFirm v ON v.VendorFirmID = di.VendorFirmID
	LEFT JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
	WHERE DatePaymentApproved BETWEEN GETDATE() - 1 AND GETDATE()
	AND d.ClaimantDocumentTypeID IN (40, 41, 254, 260)
	GROUP BY	di.InvoiceNumber,
				[Status],
				DateApproved,
				di.FirmID,
				di.InvoiceNotes,
				di.DocumentID,
				di.DefenseCounselAddressID,
				dc.DefenseCounsel,
				v.VendorFirm,
				ClaimantDocumentType


BEGIN
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
	<!--
	TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
	TD	
	{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
		
	}
	TH	
	{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
	UL {margin:0; 
		
	padding:0;}	


	--->
	</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

IF (SELECT
	COUNT(invoiceNum)
FROM @NewInvoicesTable) > 0
BEGIN
--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @BodyMessage + '<H4>The following Invoices have been Approved for Payment in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Defense Counsel</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Vendor</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Number</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Type</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Status</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Approved</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Fee Amount</th>' + '<th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disbursement Amount</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Adjustment Amount</th><th style="border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Payment Pending</th>' +

CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		DefenseCounsel)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		VendorFirm)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		invoiceNum)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		invoicetype)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		[status])
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(varchar, dateapproved, 101))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(30), COALESCE(feeAmount, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(30), COALESCE(disbursementamount, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(30), COALESCE(adjustment, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(30), CONVERT(nvarchar(30), COALESCE((feeAmount + disbursementAmount - adjustment), 0), 1)))
	AS td,
	''
FROM @NewInvoicesTable
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'
END
ELSE
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>No Invoices have been Approved for Payment in the last 24 hours. </H4>'
END

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

SET @BodyMessage = @BodyMessage + N'<br><br>
		KCIC, LLC<br>
	    733 10th Street, NW, Suite 3001<br>
		Washington, DC 20001<br>
		(t) 202-772-2300<br>
		(f) 202-772-2333'

EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',

								@recipients = 'goulds@kcic.com',
								@blind_copy_recipients = 'scottc@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'
END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalInvoicePaymentApprovalUpdate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalInvoicePaymentApprovalUpdate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalInvoicePaymentApprovalUpdate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalInvoicePaymentApprovalUpdate] TO [power_user]
GO
