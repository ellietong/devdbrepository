SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByOccByPolicy]
(@allocationID int, @CarrierID INT, @Solvency varchar (50)= NULL, @CarrierGroupID int, @InsuranceProgramID INT , @OccurrenceTypeIDs nvarchar(100) = NULL)
AS
SET NOCOUNT ON
 
--DECLARE @AllocationID int
--SET @AllocationID = 67
--DECLARE @CarrierID int
--SET @CarrierID = 0
--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = 0
--DECLARE @Solvency varchar (50)
--SET @Solvency = null
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID = 0
--DECLARE @OccurrenceTypeIDs nvarchar(100)
--SET @OccurrenceTypeIDs = null
 
---------------------------------------------------------------------------------------
--       DECLARE ALL VARIABLES
---------------------------------------------------------------------------------------
DECLARE @maxAllocationID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @maxInsuranceProgramID int
DECLARE @strSQL nvarchar (4000)
DECLARE @prmDef nvarchar (1000)
DECLARE @tmpSolvency nvarchar (50)
DECLARE @strOccurrences nvarchar (4000)
DECLARE @carrierGroupInfo nvarchar (50)
DECLARE @carrierInfo nvarchar (50)
DECLARE @maxOccurrenceTypeID int
DECLARE @index int
DECLARE @LimitTypeID int
---------------------------------------------------------------------------------------
--      Set Variables to initial values
---------------------------------------------------------------------------------------
SET @carrierGroupInfo = ''
SET @strSQL = ''
SET @strOccurrences = ''
SET @tmpSolvency = ''
SET @prmDef = ''
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(coalesce(CarrierGroupID,0)) FROM vPolicy
SELECT @maxInsuranceProgramID = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxOccurrenceTypeID = MAX(OccurrenceTypeID) FROM vOccurrence
SET @LimitTypeID = (SELECT LimitTypeID FROM tblAllocation WHERE AllocationID = @allocationID)
---------------------------------------------------------------------------------------
--      Update Null Strings Set above that aren't resolved in
--              WHERE clause (1)solvency, 2)Occurrence Types
---------------------------------------------------------------------------------------
--      1       Update Solvency Info
IF @Solvency = '<<ALL>>' OR @Solvency IS NULL OR @Solvency = ''
        SET @tmpSolvency = '%'
ELSE 
        SET @tmpSolvency = @Solvency
----------------------------------
--      2       Setup the Occurrence String.
--              Test if @OccurrenceTypeIDs is null, if so, build a string of ALL occurrence types
IF (LTRIM(RTRIM(@OccurrenceTypeIDs)) <> '' AND @OccurrenceTypeIDs IS NOT NULL)  
        SET @strOccurrences = @OccurrenceTypeIDs  
ELSE 
BEGIN
        SET @strOccurrences = '1, '     
        SET @index = 2
        
        WHILE @index <= @maxOccurrenceTypeID
        BEGIN
                SET @strOccurrences = @strOccurrences + CONVERT(nvarchar, @index) + ', '
                SET @index = @index + 1
        END
        SET @strOccurrences = LEFT(@strOccurrences, LEN(@strOccurrences) - 1)
END
---------------------------------------------------------------------------------------
--       Build the zOccurrencesConsideredInAllocation Table 
---------------------------------------------------------------------------------------
SELECT OccurrenceID, COALESCE(o.Cost1,0)*a.Cost1 + COALESCE(o.Cost2,0)*a.Cost2 AS 'DefenseConsidered', 
COALESCE(o.Cost3,0)*a.Cost3 + 
COALESCE(o.Cost4,0)*a.cost4 + 
COALESCE(o.Cost5,0)*a.cost5 + 
COALESCE(o.Cost6,0)*a.cost6 + 
COALESCE(o.Cost7,0)*a.cost7 +  
COALESCE(o.Cost8,0)*a.cost8 + 
COALESCE(o.Cost9,0)*a.cost9 + 
COALESCE(o.Cost10,0)*a.cost10+ 
COALESCE(o.Cost11,0)*a.cost11 + 
COALESCE(o.Cost12,0)*a.cost12 AS 'CostsConsidered', @allocationid AS AllocationID
INTO #tmpOccurrencesConsideredinAllocation
FROM vOccurrence o, vAllocation a
WHERE OccurrenceID NOT IN
        (SELECT aoe.OccurrenceID 
        FROM tblAllocation a 
  INNER JOIN tblAllocationOccurrenceExclude aoe ON a.AllocationID = aoe.AllocationID 
        WHERE a.AllocationID = @allocationID
        ) AND 
OccurrenceTypeID IN
        (SELECT aote.OccurrenceTypeID 
        FROM tblAllocation a INNER JOIN tblAllocationOccurrenceTypeInclude aote ON a.AllocationID = aote.AllocationID 
        WHERE a.AllocationID = @allocationID
        ) AND 
OccurrenceGroupID IN
        (SELECT aoge.OccurrenceGroupID
        FROM tblAllocation a INNER JOIN tblAllocationOccurrenceGroupInclude aoge ON a.AllocationID = aoge.AllocationID
        WHERE a.AllocationID = @allocationID) AND
OccurrenceID is not null
AND a.AllocationID = @Allocationid
 
---------------------------------------------------------------------------------------
--       Build temp tables for allocation totals
--------------------------------------------------------------------------------------- 

CREATE TABLE #A (AllocationID int, TotalAggregateLimit money, TotalSIR money, TotalAllocation money, PVTotalAllocation money)
INSERT INTO #A
SELECT AllocationID, SUM(AggregateLimit) AS AggregateLimit, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID

CREATE TABLE #CA(AllocationID int, CarrierID int, CarrierAggregateLimit money, CarrierTotalSIR money, CarrierTotalAllocation money, PVCarrierTotalAllocation money)
INSERT INTO #CA
SELECT AllocationID, CarrierID, SUM(AggregateLimit) AS AggregateLimit, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVCarrierTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, CarrierID

CREATE TABLE #CGA (AllocationID int, CarrierGroupID int, CarrierGroupAggregateLimit money, CarrierGroupTotalSIR money, CarrierGroupTotalAllocation money, PVCarrierGroupTotalAllocation money)
INSERT INTO #CGA
SELECT AllocationID, CarrierGroupID, SUM(AggregateLimit) AS AggregateLimit, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVCarrierGroupTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, CarrierGroupID 

CREATE TABLE #PA(AllocationID int, PolicyDateID int, PolicyAggregateLimit money, PolicyTotalSIR money, PolicyTotalAllocation money, PVPolicyTotalAllocation money)
INSERT INTO #PA
SELECT AllocationID, ar.PolicyDateID, SUM(AggregateLimit) AS AggregateLimit, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, ar.PolicyDateID

CREATE TABLE #OA(AllocationID int, OccurrenceID int, OccTotalSIR money, OccTotalAllocation money, PVOccTotalAllocation money)
INSERT INTO #OA
SELECT AllocationID, ar.OccurrenceID, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vOccurrence o ON ar.OccurrenceID = o.OccurrenceID
WHERE AllocationID = @AllocationID
GROUP BY AllocationID, ar.OccurrenceID

CREATE TABLE #OG(AllocationID int, OccurrenceGroupID int, OccGrpTotSIR money, OccGrpTotAlloc money, PVOccGrpTotAlloc money)
INSERT INTO #OG
SELECT AllocationID, o.OccurrenceGroupID, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vOccurrence o ON ar.OccurrenceID = o.OccurrenceID
WHERE AllocationID = @AllocationID
GROUP BY AllocationID,o.OccurrenceGroupID

---------------------------------------------------------------------------------------
--       Build the SQL String
---------------------------------------------------------------------------------------
SET @StrSQL = N'SELECT pinfo.PolicyID,PolicyNum,pinfo.InsuranceProgram,pinfo.InsuranceProgramID,o.OccurrenceTypeID,o.OccurrenceName,o.OccurrenceID,o.OccurrenceGroupID,TriggerStart1, 
o.OccurrenceGroup,o.ExpectedPayDate,pinfo.CarrierID,CarrierSolvency,CarrierName,pinfo.CarrierGroupID,CarrierGroupName,
CASE WHEN CarrierGroupName IS NULL THEN CarrierName ELSE CarrierGroupName END AS CarrierGroupInfo,
CASE WHEN CarrierName IS NULL THEN CarrierGroupName ELSE CarrierName END AS CarrierInfo,ar.TriggerStartDateUsed,ar.DateTime,ar.Allocation,
CONVERT(money, ar.PV)AS PV,ar.SIRAllocated,ar.PVRate,ar.DateToPresentValue,ar.TriggerEndDateUsed,a.AllocationID,a.AllocationDescription,
tmp.CostsConsidered,tmp.DefenseConsidered,AttachmentPoint,PerOccLimit,LayerPerOccLimit,SIR,AggregateLimit,Layer,YieldCurveDate as PVRateDate,
pstartdate,penddate,annualperiod,actualpstartdate,actualpenddate,COALESCE(TotalSIR,0)AS TotalSIR,COALESCE(CarrierTotalSIR,0)AS CarrierTotalSIR,COALESCE(CarrierGroupTotalSIR,CarrierTotalSIR)AS CarrierGroupTotalSIR,
COALESCE(TotalAllocation,0)AS TotalAllocation,COALESCE(CarrierTotalAllocation,0)AS CarrierTotalAllocation,COALESCE(CarrierGroupTotalAllocation,CarrierTotalAllocation)AS CarrierGroupTotalAllocation,
COALESCE(PVTotalAllocation,0)AS PVTotalAllocation,COALESCE(PVCarrierTotalAllocation,0)AS PVCarrierTotalAllocation,COALESCE(PVCarrierGroupTotalAllocation,PVCarrierTotalAllocation)AS PVCarrierGroupTotalAllocation,PolicyTotalSIR,PolicyTotalAllocation,PVPolicyTotalAllocation,OccTotalSIR,OccTotalAllocation,PVOccTotalAllocation,
OccGrpTotSIR,OccGrpTotAlloc
FROM vPolicy pinfo INNER JOIN vAllocationResults ar ON pinfo.PolicydateID=ar.PolicydateID
INNER JOIN vAllocation a ON a.AllocationID=ar.AllocationID
INNER JOIN vOccurrence o ON o.OccurrenceID=ar.OccurrenceID
INNER JOIN #tmpOccurrencesConsideredinAllocation tmp ON (a.AllocationID=tmp.AllocationID AND o.OccurrenceID=tmp.OccurrenceID) 
LEFT JOIN tblYieldCurve yc on yc.YieldCurveID=ar.YieldCurveID 
LEFT JOIN #A at ON ar.AllocationID=at.AllocationID 
LEFT JOIN #CA cat ON ar.AllocationID=cat.AllocationID AND pinfo.CarrierID=cat.CarrierID
LEFT JOIN #CGA cgat ON ar.AllocationID=cgat.AllocationID AND pinfo.CarrierGroupID=cgat.CarrierGroupID
LEFT JOIN #PA pa ON ar.AllocationID=pa.AllocationID AND ar.PolicyDateID=pa.PolicyDateID
LEFT JOIN #OA oa ON ar.AllocationID=oa.AllocationID AND ar.OccurrenceID=oa.OccurrenceID
LEFT JOIN #OG og ON ar.AllocationID=og.AllocationID AND o.OccurrenceGroupID=og.OccurrenceGroupID
WHERE a.AllocationID>=COALESCE('+ CONVERT (nvarchar, @allocationID) +', 0) AND 
a.AllocationID<=COALESCE(NULLIF(' + CONVERT (nvarchar, @allocationID) + ', 0), ' + CONVERT (nvarchar, @maxAllocationID) + ' ) AND
pinfo.CarrierID>=COALESCE('+ CONVERT (nvarchar, @CarrierID) +', 0) AND
pinfo.CarrierID<=COALESCE(NULLIF(' + CONVERT (nvarchar, @CarrierID) + ', 0), ' + CONVERT (nvarchar, @maxCarrierID) + ' ) AND
pinfo.CarrierSolvency LIKE @SolvencyPrm AND '
IF @CarrierGroupID <> 0
        SET @strSQL = @strSQL + 'pinfo.CarrierGroupID=' + CONVERT(nvarchar, @CarrierGroupID) + ' AND '
SET @strSQL = @strSQL + '
        pinfo.InsuranceProgramID>=COALESCE('+ CONVERT (nvarchar, @InsuranceProgramID) +', 0) AND
        pinfo.InsuranceProgramID<=COALESCE(NULLIF(' + CONVERT (nvarchar, @InsuranceProgramID) + ', 0), ' + CONVERT (nvarchar, @maxInsuranceProgramID) + ' ) AND
        o.OccurrenceTypeID IN(' + CONVERT (nvarchar (4000),@strOccurrences) + ') AND 
        pinfo.limittypeid=' + CONVERT (nvarchar, @limittypeid) + '
GROUP BY pinfo.PolicyID,PolicyNum,InsuranceProgram,pinfo.InsuranceProgramID,o.ExpectedPayDate,o.OccurrenceTypeID,o.OccurrenceName,o.OccurrenceID,o.OccurrenceGroupID,o.TriggerStart1,
        o.OccurrenceGroup,pinfo.CarrierID,CarrierSolvency,CarrierName,pinfo.CarrierGroupID,CarrierGroupName,ar.TriggerStartDateUsed,ar.DateTime,ar.Allocation,PV,SIRAllocated,
        PVRate,Datetopresentvalue,TriggerEndDateUsed,a.AllocationID,a.AllocationDescription,tmp.CostsConsidered,tmp.DefenseConsidered,AttachmentPoint,PerOccLimit,LayerPerOccLimit,SIR,
        AggregateLimit,Layer,YieldCurveDate,pstartdate,penddate,annualperiod,actualpstartdate,actualpenddate,TotalSIR,CarrierTotalSIR,CarrierGroupTotalSIR,TotalAllocation,CarrierTotalAllocation,
        CarrierGroupTotalAllocation,PVTotalAllocation,PVCarrierTotalAllocation,PVCarrierGroupTotalAllocation,PolicyTotalSIR,PolicyTotalAllocation,PVPolicyTotalAllocation,OccTotalSIR,OccTotalAllocation,PVOccTotalAllocation,
        OccGrpTotSIR,OccGrpTotAlloc,PVOccGrpTotAlloc'
---------------------------------------------------------------------------------------
--      Declare the Parameters that will be passed to the created SQL string.
---------------------------------------------------------------------------------------
SET @prmDef = N'@SolvencyPrm nvarchar (50)'
---------------------------------------------------------------------------------------
--      Run/Execute the SQL Statement Created
---------------------------------------------------------------------------------------
EXEC sp_executeSQL @strSQL, @prmDef, @SolvencyPrm = @tmpSolvency

DROP TABLE #tmpOccurrencesConsideredinAllocation
DROP TABLE #A
DROP TABLE #CA
DROP TABLE #CGA
DROP TABLE #PA
DROP TABLE #OA
DROP TABLE #OG

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccByPolicy] TO [power_user]
GO
