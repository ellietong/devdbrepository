SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmPolicyLanguage]
(@policyID int = null)
AS

SET NOCOUNT ON

SELECT  el.*
FROM tblExactLanguage el 

WHERE PolicyID = @policyID


RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLanguage] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLanguage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyLanguage] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyLanguage] TO [power_user]
GO
