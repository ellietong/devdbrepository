SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryUpdDefenseInvoiceNCC]
(
	@FirmID int,
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
    @AdjustmentNotes nvarchar(4000) = null,
    @StatusID int = null,
	@ApprovedBy nvarchar(50), --= null,
	@DateApproved	datetime, --= null,
	@DocID int=null,
	@VendorFirmID int=null
)

AS
BEGIN


Update tblDefenseInvoice
SET 
	InvoiceNumber			= @InvoiceNumber,
	Period					= @Period,
	DatePayable				= CASE WHEN @StatusID IN (1, 2) AND VendorFirmID IS NOT NULL AND @VendorFirmID <> 0 THEN GETDATE()+1 ELSE @DatePayable END,
    InvoiceNotes			= @AdjustmentNotes,
    StatusID				= @StatusID,
    DateApproved			= @DateApproved,
    ApprovedBy				= @ApprovedBy,
    RecommendedBy			= CASE WHEN @StatusID IN (1, 2) THEN @ApprovedBy ELSE NULL END,
	IsRecommended			= CASE WHEN @StatusID IN (1, 2) THEN 1 ELSE 0 END,
	DateRecommended			=  GETDATE()    
    
Where DocumentID=@DocID

Select @DocID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdDefenseInvoiceNCC] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdDefenseInvoiceNCC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdDefenseInvoiceNCC] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdDefenseInvoiceNCC] TO [power_user]
GO
