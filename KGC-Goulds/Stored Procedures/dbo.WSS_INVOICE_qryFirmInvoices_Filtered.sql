SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_INVOICE_qryFirmInvoices_Filtered] (@emailaddress			nvarchar(1000) = NULL, 
															  @FilterStatusID		int, 
															  @FilterDocumentTypeID int, 
															  @FilterFirmID			int, 
															  @FilterExpertFirmID	int)
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @emailaddress nvarchar(max)
--DECLARE @FilterStatusID int
--DECLARE @FilterDocumentTypeID int
--DECLARE @FilterFirmID int
--DECLARE @FilterExpertFirmID int
--SET @emailaddress         = 'sreich@dehay.com'
--SET @FilterStatusID       = null
--SET @FilterDocumentTypeID = null
--SET @FilterFirmID         = 10
--SET @FilterExpertFirmID   = null

DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE dt.DocumentGroupID = 2 AND ((COALESCE(d.statusid,0)!=5) OR (COALESCE(di.StatusID,0)!=5))'
IF @FilterDocumentTypeID IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND ClaimantDocumentTypeID = ' + CONVERT(nvarchar(20), @FilterDocumentTypeID) END
IF @FilterFirmID		 IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND d.FirmID = ' + CONVERT(nvarchar(20), @FilterFirmID) END
IF @FilterStatusID		 IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND d.StatusID = ' + CONVERT(nvarchar(20), @FilterStatusID) END
IF @FilterExpertFirmID	 IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND d.ExpertFirmID = ' + CONVERT(nvarchar(20), @FilterExpertFirmID) END

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF (@DefenseCounselID IN (132)) OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' '
ELSE IF (@DefenseCounselID IN (100, 117))
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND f.FirmID IN (100, 117)'
END
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND f.FirmID = ' + CONVERT(nvarchar(20),@DefenseCounselID)
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT CAST(d.DocumentID AS varchar(50)) AS DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.DocumentURL,
		d.DocumentTitle,
		d.DateCreated,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		f.Firm,
		d.Period,
		dis.Status,	
		di.DatePaid, 
		COALESCE(ExpertFirm, '''') AS ExpertFirm,
		SourceDate,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt			ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg				ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT JOIN tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
		LEFT JOIN tblDefenseInvoiceStatus dis	ON d.StatusID = dis.StatusID 
		LEFT JOIN vtblFirm f					ON d.FirmID = f.FirmID
		LEFT JOIN m_tblExpertFirm ex			ON d.ExpertFirmID = ex.ExpertFirmID
		LEFT JOIN tblPaymentDefense pd			ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN tblCheck c					ON pd.CheckID = c.CheckID ' +
    @WhereString + '
	ORDER BY Accepted ASC, DateCreated DESC
	OPTION (RECOMPILE)'

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Filtered] TO [power_user]
GO
