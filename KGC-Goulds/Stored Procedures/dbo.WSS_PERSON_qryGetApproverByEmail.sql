SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_PERSON_qryGetApproverByEmail]
    (
      @ApproverTypeID int
    )
AS -- Declare the temporary fields used to track firm and email domain
    DECLARE @PersonID INT

IF @ApproverTypeID = 1 --Invoice Approvers
BEGIN
	SELECT DISTINCT CONVERT(nvarchar(10),PersonID) AS PersonID
	FROM vtblPerson
	WHERE PersonID IN (3991, 3460,4934,3462) --Lorre, Craig, and Tara
END 
ELSE -- Settlement Approvers
BEGIN
	SELECT DISTINCT CONVERT(nvarchar(10),PersonID) AS PersonID
	FROM vtblPerson 
	WHERE PersonID IN (3991, 3460, 4934,3462) --Lorre, Craig and Tara
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_PERSON_qryGetApproverByEmail] TO [power_user]
GO
