SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROC [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_ByStateTotals] (@StartDate DATETIME, @EndDate DATETIME,@MatterID int, @EmailAddress nvarchar(250))

AS
SET NOCOUNT ON
SET @EmailAddress = 'hagep@kcic.com'
--DECLARE @MatterID int = null,
--		@StartDate DATETIME = '1/1/2010',
--		@EndDate DateTime = '1/1/2011',
--		@EmailAddress nvarchar(max) = 'owenss@kcic.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)


DECLARE @WhereString NVARCHAR(MAX) 
SET @WhereString = ' WHERE COALESCE(filedate, ''1/1/1900'') between ''' + COALESCE(CONVERT(NVARCHAR, @StartDate),'1/1/'+CONVERT(NVARCHAR(4), YEAR(GETDATE()))) + ''' AND ''' + COALESCE(CONVERT(NVARCHAR, @EndDate), CONVERT(NVARCHAR,GETDATE())) + ''''

DECLARE @GroupByString NVARCHAR(MAX) 
SET @GroupByString = 'GROUP BY'

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @GroupByString = @GroupByString + ' dj.DefenseCounselAddressID '
END

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @QueryString NVARCHAR(MAX)

 	IF object_id('tempdb..#DiseaseCount') IS NOT NULL
	BEGIN
	   DROP TABLE #DiseaseCount
	END
	CREATE TABLE #DiseaseCount 
(ClaimID int, Matter NVARCHAR(100), MatterID int, YearFiled NVARCHAR(50), MoYearFiled NVARCHAR(50), MonthID int, MesoCount int, LungCancerCount int, 
OtherCancerCount int, NonMaligCount int,  UnknownCount int, unknowncount1 int, [state] NVARCHAR(100), stateid int)


SET @QueryString = 
	'SELECT DISTINCT Cl.ClaimlawsuitID, Matter, C.MatterID, Coalesce(YEAR(fileDate), 1900) as YearFiled, 
COALESCE(DateName(month,filedate), ''Unknown'') as MonthFiled,
COALESCE(month(filedate), 0) as MonthID,
--Meso
CASE	WHEN COALESCE(DMeso.HighLevelDiseaseID, 0) = 1 THEN  1
ELSE 0 END AS MesoCount,
--Lung Cancer
CASE	WHEN COALESCE(DLC.HighLevelDiseaseID, 0) = 2 THEN  1
ELSE 0 END as LungCancerCount,
--Other Cancer
CASE	WHEN COALESCE(DOC.HighLevelDiseaseID, 0) = 3 THEN  1
ELSE 0 END as OtherCancerCount,
--NonMalig, silicosis, pneumoconiosis
CASE	WHEN COALESCE(DNM.HighLevelDiseaseID, 0) IN (4, 7, 8) THEN  1
ELSE 0 END as NonMaligCount,
--Silicosis
--CASE	WHEN COALESCE(DNM.HighLevelDiseaseID, 0) = 7 THEN  1
--ELSE 0 END as Silicosis,
--Unknown
CASE	WHEN COALESCE(DUnk.HighLevelDiseaseID, 0) IN (0,5,6) THEN  1
ELSE 0 END AS UnknownCount,
CASE	WHEN COALESCE(DUnk.HighLevelDiseaseID, 0) is NULL THEN  1
ELSE 0 END AS UnknownCount1, s.FullNameState,
l.StateID
FROM tblClaimantPersonalInfo CPI
LEFT JOIN  tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (SELECT CD.ClaimDiseaseID,CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease FROM tblClaimDisease CD WHERE PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
LEFT JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID
--Meso
LEFT JOIN  tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
LEFT JOIN  tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
LEFT JOIN  tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
LEFT JOIN  tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Unknown
LEFT JOIN  tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID

LEFT JOIN  tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
LEFT JOIN  tblLawsuit L on CL.LawsuitID=L.LawsuitID
		' + @JoinString + '
LEFT JOIN  tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
left JOIN tblState s ON l.StateID = s.StateID
	' + @WhereString + ' 
Order BY Coalesce(YEAR(fileDate), 1900)'

 	IF object_id('tempdb..#DiseaseCountTemp') IS NOT NULL
	BEGIN
	   DROP TABLE #DiseaseCountTemp
	END
	CREATE TABLE #DiseaseCountTemp 
(ClaimID int, Matter NVARCHAR(100), MatterID int, YearFiled NVARCHAR(50), MoYearFiled NVARCHAR(50), MonthID int, MesoCount int, LungCancerCount int, 
OtherCancerCount int, NonMaligCount int,  UnknownCount int, unknowncount1 int, [state] NVARCHAR(100), stateid int)


Insert Into #DiseaseCountTemp
EXEC sp_executesql @QueryString


--select *
--from #DiseaseCountTemp

SELECT COUNT(ClaimID) AS NewFilings, @StartDate as StartDate, @EndDate as ENDDate,
SUM(MesoCount) as MesoFilings, SUM(LungCancerCount) as LCFilings, SUM(OtherCancerCount) as OCFilings,
SUM(NonMaligCount) as NMFILINGS,  SUM(UnknownCount+unknowncount1) as UnknownFilings, [state], stateid,
SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + Unknowncount1) AS TotalCount
FROM #DiseaseCountTemp DC
GROUP BY  [state], stateid
ORDER BY [State]

RETURN

RETURN








GO
