SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryAvailableFunds]
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
	
        DECLARE @MatterID INT ,
            @BalanceStartDate DATETIME ,
            @BalanceEndDate DATETIME
        SET @MatterID = 1
        SET @BalanceStartDate = '8/1/2010'
        SET @BalanceEndDate = GETDATE()

        DECLARE @StartingBalance MONEY
        SET @StartingBalance = ( COALESCE(( SELECT  SUM(CashActivityAmount)
                                            FROM    tblMiscCashActivity mca
                                                    INNER JOIN tblMiscCashActivityApproval mcaa ON mca.MiscCashActivityID = mcaa.MiscCashActivityID
                                            WHERE   CashActivityDate >= '1/1/2010'
                                                    AND CashActivityDate < @BalanceStartDate
                                                    AND MatterID = @MatterID
                                          ), 0)
                                 + COALESCE(( SELECT    SUM(SourceAmount) * -1
                                              FROM      tblCheck c
                                                        INNER JOIN tblCheckApproval ca ON c.CheckID = ca.CheckID
                                              WHERE     SourceDate >= '1/1/2010'
                                                        AND SourceDate < @BalanceStartDate
                                                        AND MatterID = @MatterID
                                            ), 0) )

        DECLARE @Matter NVARCHAR(250)
        SET @Matter = ( SELECT  Matter
                        FROM    tblMatter
                        WHERE   MatterID = @MatterID
                      )

        DECLARE @Detail TABLE
            (
              Indemnity MONEY ,
              Defense MONEY ,
              MiscFees MONEY ,
              CashReceipts MONEY ,
              OutstandingUnapprovedAmount MONEY ,
              ApprovedNotPaidAmount MONEY
            )
        INSERT  INTO @Detail

--Defense
                SELECT  0 ,
                        SourceAmount * -1 AS SourceAmount ,
                        0 ,
                        0 ,
                        0 ,
                        0
                FROM    tblCheck c
                        INNER JOIN tblCheckApproval ca ON ca.CheckID = c.CheckID
                        INNER JOIN tblPaymentDefense pd ON pd.CheckID = c.CheckID
                        INNER JOIN tblDefenseInvoice di ON di.DefenseInvoiceID = pd.DefenseInvoiceID
                        INNER JOIN tblDefenseInvoiceFirm dif ON dif.FirmID = di.FirmID
                        INNER JOIN tblMatter m ON c.MatterID = m.MatterID
                        INNER JOIN tblCheckFundingSource cfs ON cfs.FundingSourceID = c.FundingSourceID
                        INNER JOIN tblCheckFundingType cft ON cft.FundingTypeID = c.FundingTypeID
                WHERE   ( SourceDate >= @BalanceStartDate
                          AND SourceDate <= @BalanceEndDate
                        )
                        AND c.MatterID = @MatterID
                        AND c.FundingTypeID = 1
                GROUP BY SourceAmount * -1
                UNION ALL

--Settlement
                SELECT  SourceAmount * -1 AS SourceAmount ,
                        0 ,
                        0 ,
                        0 ,
                        0 ,
                        0
                FROM    tblCheck c
                        INNER JOIN tblCheckApproval ca ON ca.CheckID = c.CheckID
                        INNER JOIN tblPaymentSettlement ps ON ps.CheckID = c.CheckID
                        INNER JOIN tblClaimantSettlement cs ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
                        INNER JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
                        INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
                        INNER JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
                        INNER JOIN tblMatter m ON c.MatterID = m.MatterID
                        INNER JOIN tblCheckFundingSource cfs ON cfs.FundingSourceID = c.FundingSourceID
                        INNER JOIN tblCheckFundingType cft ON cft.FundingTypeID = c.FundingTypeID
                WHERE   ( SourceDate >= @BalanceStartDate
                          AND SourceDate <= @BalanceEndDate
                        )
                        AND c.MatterID = @MatterID
                        AND c.FundingTypeID = 2
                GROUP BY Matter ,
                        FundingType ,
                        FundingSource ,
                        SourceDate ,
                        SourceNumber ,
                        SourceAmount * -1 ,
                        PlaintiffCounsel ,
                        IsCheckCleared
                UNION ALL
                SELECT  0 ,
                        0 ,
                        CashActivityAmount ,
                        0 ,
                        0 ,
                        0
                FROM    tblMiscCashActivity mca
                        INNER JOIN tblMiscCashActivityApproval mcaa ON mca.MiscCashActivityID = mcaa.MiscCashActivityID
                        INNER JOIN tblMiscCashActivityType mcat ON mcat.CashActivityTypeID = mca.CashActivityTypeID
                        INNER JOIN tblMatter m ON m.MatterID = mca.MatterID
                WHERE   ( CashActivityDate >= @BalanceStartDate
                          AND CashActivityDate <= @BalanceEndDate
                        )
                        AND mca.MatterID = @MatterID
                        AND CashActivityAmount < 0
                UNION ALL
                SELECT  0 ,
                        0 ,
                        0 ,
                        CashActivityAmount ,
                        0 ,
                        0
                FROM    tblMiscCashActivity mca
                        INNER JOIN tblMiscCashActivityApproval mcaa ON mca.MiscCashActivityID = mcaa.MiscCashActivityID
                        INNER JOIN tblMiscCashActivityType mcat ON mcat.CashActivityTypeID = mca.CashActivityTypeID
                        INNER JOIN tblMatter m ON m.MatterID = mca.MatterID
                WHERE   ( CashActivityDate >= @BalanceStartDate
                          AND CashActivityDate <= @BalanceEndDate
                        )
                        AND mca.MatterID = @MatterID
                        AND CashActivityAmount > 0
                UNION ALL
                SELECT  0 ,
                        0 ,
                        0 ,
                        COALESCE(@StartingBalance, 0) ,
                        0 ,
                        0
                UNION ALL
 
--outstanding, unapproved invoices
                SELECT  0 ,
                        0 ,
                        0 ,
                        0 ,
                        COALESCE(invoicetotal, 0) ,
                        0
                FROM    tblDefInvClaimantDetail dicd
                        INNER JOIN vtblDefenseInvoice d ON dicd.DocumentID = d.DocumentID
-- FROM tblDefenseInvoice di
--WHERE CheckNumber IS NULL AND DatePaid IS NULL AND
                WHERE   COALESCE(d.StatusID, 0) NOT IN ( 1, 2 )
                UNION ALL

--approved but not yet paid
                SELECT  0 ,
                        0 ,
                        0 ,
                        0 ,
                        0 ,
                        COALESCE(invoicetotal, 0)
                FROM    vtblDefenseInvoice di
                        INNER JOIN tblDocuments d ON di.DocumentID = d.DocumentID
                WHERE   SourceNumber IS NULL
                        AND SourceDate IS NULL

        SELECT  SUM(COALESCE(Indemnity, 0) + COALESCE(Defense, 0)
                    + COALESCE(MiscFees, 0) + COALESCE(CashReceipts, 0)) AS AvailableFunds ,
                SUM(OutstandingUnapprovedAmount) AS OutstandingUnapprovedAmount ,
                SUM(ApprovedNotPaidAmount) AS ApprovedNotPaidAmount
        FROM    @Detail
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryAvailableFunds] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryAvailableFunds] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryAvailableFunds] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryAvailableFunds] TO [power_user]
GO
