SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_SMOKING_qryDelClaimantSmokingHistory]
    (
      @ClaimantSmokingHistoryID BIGINT
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        DELETE  FROM tblClaimantSmokingHistory
        WHERE   ClaimantSmokingHistoryID = @ClaimantSmokingHistoryID
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SMOKING_qryDelClaimantSmokingHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SMOKING_qryDelClaimantSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SMOKING_qryDelClaimantSmokingHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SMOKING_qryDelClaimantSmokingHistory] TO [power_user]
GO
