SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qryTopTenSBNDByState] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null,@EndDate DATETIME=null, @EmailAddress NVARCHAR(250)) 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @EmailAddress = 'hagep@kcic.com'
--DECLARE @MatterID int = null,
--		@EndDate DateTime = '1/1/2007',
--		@EmailAddress nvarchar(max) = 'sjesser@hinshawlaw.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)


DECLARE @WhereString NVARCHAR(MAX) 
SET @WhereString = ' WHERE ClaimLawsuitStatusID IN (25) and isprimarystatus = 1'

DECLARE @GroupByString NVARCHAR(MAX) 
SET @GroupByString = 'GROUP BY s.FullNameState'

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND isprimarystatus = 1 AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) +
					 ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @GroupByString = @GroupByString + ' , dj.stateid '
END

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @QueryString NVARCHAR(MAX)

SET @QueryString = 
	'SELECT DISTINCT top 10 s.FullNameState AS State, COUNT(distinct(cl.ClaimLawsuitID)) as Count
	FROM tblClaim c
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID=l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
	INNER JOIN tblState s ON l.StateID=s.StateID
			' + @JoinString + '

		' + @WhereString + '
		' + @GroupByString + '
	ORDER BY COUNT(distinct(cl.ClaimLawsuitID)) desc, s.fullnamestate'


--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenSBNDByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenSBNDByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenSBNDByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenSBNDByState] TO [power_user]
GO
