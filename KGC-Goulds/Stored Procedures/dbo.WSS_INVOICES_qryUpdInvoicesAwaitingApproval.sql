SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] (@DocumentID int, @EmailAddress nvarchar(1000), @StatusID int = null, @ApprovalNotes nvarchar(4000))
AS

BEGIN
	UPDATE tblDefenseInvoice
	SET StatusID = @StatusID,
		PaymentApprovedBy = @EmailAddress,
		DatePaymentApproved = GETDATE(),
		ApprovalNotes = @ApprovalNotes
	WHERE DocumentID = @DocumentID
END






GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_qryUpdInvoicesAwaitingApproval] TO [power_user]
GO
