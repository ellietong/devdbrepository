SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryDocument] ( @DocumentID INT					--aka ClaimantDocumentTypeID
                                                  )
AS --1/31/12 EMS updated to pull defense invoice info from vtbldefenseinvoice

    SELECT DISTINCT
            d.ClaimantDocumentTypeID ,
            mcd.ClaimantPersonalInfoID ,
            Comments ,
            DocumentURL ,
            DocumentTitle ,
	--SPDocId,
            CreatedBy ,
            d.DateCreated ,
            di.InvoiceNumber ,
            di.Period ,
            di.DatePayable ,
            ModifiedBy ,
            DateModified ,
            dic.fees AS FeeAmount ,
            dic.costs AS DisbursementAmount ,
            dic.adjustments AS Adjustment ,
            di.InvoiceNotes AS AdjustmentNotes ,
            di.StatusID ,
            di.FirmID ,
            d.MatterID , 
    --PrivateComments,
            DocumentSourceID ,
            di.VendorFirmID ,
            cdt.ClaimantDocumentGroupID ,
            IsFullyPaid ,
            d.DocumentID ,
    --DismissalDate,
            d.QCDate AS DismissalProcessDate ,
    --d.ClaimLawsuitStatusID,
            NULL AS Temphash ,
            Accepted AS AutoAccept
    FROM    tblDocuments d
            LEFT JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
            INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
            LEFT JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID
            LEFT JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
            LEFT JOIN m_tblVendorFirm vf ON di.VendorFirmID = vf.VendorFirmID
            LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID 
	--LEFT JOIN vtblFirm f ON d.FirmID = f.FirmID
            LEFT JOIN tblClaimantDocketNumbers cdn ON mcd.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
            LEFT JOIN tblDefInvClaimantDetail dic ON dic.DocumentID = di.DocumentID
    WHERE   d.DocumentID = @DocumentID
            AND COALESCE(IsFullyPaid, 0) <> 1

--FROM tblDocuments d
--	INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
--	LEFT JOIN vtblDefenseInvoice di ON d.DocumentID = di.DocumentID 
--	LEFT JOIN tblPaymentDefense pd on di.DefenseInvoiceID=pd.DefenseInvoiceID
--	LEFT JOIN m_tblVendorFirm vf ON d.VendorFirmID = vf.VendorFirmID 
--	LEFT JOIN tblDefenseInvoiceStatus dis ON di.StatusID = dis.StatusID 
--	LEFT JOIN vtblFirm f ON d.FirmID = f.FirmID
--	LEFT JOIN tblClaimantDocketNumbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
--	LEFT JOIN tblDefInvClaimantDetail dic on dic.DocumentID=di.DocumentID
--WHERE d.DocumentID = @DocumentID AND COALESCE(IsFullyPaid, 0) <> 1


    RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocument] TO [power_user]
GO
