SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] (@SourceDate datetime)

As

SET NOCOUNT ON

--Declare @SourceDate datetime
--Set @SourceDate = '2/15/2013'

Declare @FundingInvoices table(CheckID bigint, DefenseCounselID bigint, Firm nvarchar(250), InvoiceNumber nvarchar(250), DefenseInvoiceID int,
								InvoiceTotal money, UticaPortion money, GouldsPortion money, Period date, SubmittedDate date)
Insert into @FundingInvoices
	select  c.CheckID,
			DefenseCounselID as DefenseCounselID,
			DefenseCounsel as Firm,
			di.InvoiceNumber,
			di.DefenseInvoiceID as DefenseInvoiceID,
			SUM((Fees + Costs) - Adjustments) as InvoiceTotal,
			SUM(InsurerAmount) as UticaPortion,
			SUM(ClientAmount) as GouldsPortion,
			MAX(DATENAME("mm", di.Period) + ' ' + DATENAME("yy", di.Period)) as Period,
			MAX(SubmitDate) as SubmittedDate
	from tblDefenseInvoice di
		inner join tblDefInvClaimantDetail dicd on di.DocumentID = dicd.DocumentID
		inner join tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		inner join tblCheck c on pd.CheckID = c.CheckID
		Left Join tblDefenseCounselAddress da on di.DefenseCounselAddressID=da.DefenseCounselAddressID
	Where c.SourceDate = @SourceDate and FundingTypeID=1
	Group by c.CheckID, DefenseCounselID, DefenseCounsel, di.InvoiceNumber, di.DefenseInvoiceID

--MAYBE USED FOR GOULDS??
Union

select  c.CheckID,
		v.VendorFirmID as DefenseCounselID,
		vendorfirm as Firm,
		di.InvoiceNumber as InvoiceNumber,
		di.DefenseInvoiceID as DefenseInvoiceID,
		SUM((Fees + Costs) - Adjustments) as InvoiceTotal,
			SUM(InsurerAmount) as UticaPortion,
			SUM(ClientAmount) as GouldsPortion,
		MAX(DATENAME("mm", di.Period) + ' ' + DATENAME("yy", di.Period)),
		MAX(SubmitDate) as SubmittedDate
from tblPaymentDefense pd
inner join tbldefenseinvoice di on pd.DefenseInvoiceID=di.defenseInvoiceID
inner join tblDefInvClaimantDetail dicd on di.DocumentID = dicd.DocumentID
inner join tblCheck c on pd.CheckID=c.CheckID
inner Join m_tblVendorFirm v on v.VendorFirmID = di.VendorFirmID
Where c.SourceDate = @SourceDate  and FundingTypeID=3
Group by c.CheckID, v.VendorFirmID, vendorfirm, di.InvoiceNumber, di.DefenseInvoiceID

Select f.CheckID, f.Firm, max(f.Period) as Period, max(f.SubmittedDate) as SubmittedDate, f.InvoiceNumber, f.DefenseInvoiceID,
	sum(f.InvoiceTotal) as InvoiceTotal, SUM(UticaPortion) as UticaPortion, SUM(GouldsPortion) as GouldsPortion
From @FundingInvoices f
Group BY f.CheckID, f.DefenseCounselID, f.Firm, f.InvoiceNumber, f.DefenseInvoiceID
Order By f.CheckID, f.Firm, max(f.Period), f.InvoiceNumber
GO
GRANT ALTER ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT CONTROL ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GRANT CONTROL ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentInvoiceDetailsMultiple] TO [power_user]
GO
