SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs]  

(
 @emailaddress		nvarchar(250), 
 @SettlementGroupID int = NULL,
 @MSDCategory		nvarchar(50),
 @SDCategory		nvarchar(50), 
 @ClaimantName		nvarchar(max),
 @ClaimantIDs		integer_list_tbltype READONLY
 )

AS

SET NOCOUNT ON

--DECLARE @EmailAddress nvarchar(250) = 'owenss@kcic.com'
--DECLARE @SettlementGroupID int = NULL

DECLARE @MaxSettlementGroupID int = (SELECT MAX(SettlementGroupID) FROM tblSettlementGroup)

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

IF object_id('tempdb..#ClaimantIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimantIDs
END
CREATE TABLE #ClaimantIDs (n int)
INSERT INTO #ClaimantIDs
	SELECT n FROM @ClaimantIDs

DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE 1=1 '

IF @ClaimantName			  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND (ClaimantName like ''%' + @ClaimantName + '%'')' END
IF (SELECT COUNT(n) FROM #ClaimantIDs) <> 0 BEGIN SET @WhereString = @WhereString + ' AND msd.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)' END
IF @MSDCategory				  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(' + @MSDCategory + ', 0) = 0' END
IF @SDCategory				  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND ' + @SDCategory + ' = 0' END
IF @SettlementGroupID		  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND SettlementGroupID = ' + CONVERT(nvarchar(20), @SettlementGroupID) END

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR (RIGHT(@emailaddress, 8) = 'kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblLawsuit l ON msd.LawsuitID = l.LawsuitID
						INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID
						INNER JOIN tblDefenseCounselAddress dca on dj.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END


DECLARE @QueryString NVARCHAR(MAX) = ''
SET @QueryString = '
SELECT DISTINCT ClaimantID, 
				ClaimantPersonalInfoID,
				ClaimID,
				ClaimantName,
				Birthdate, 
				DeceasedDate,
				StatusDate,
				SettlementDate, 
				SettlementAmount, 
				ClientAmount,
				SettlementGroup,
				DOFE,
				Disease,
				DiagnosisDate,
				MedicareBeneficiaryStatus,
				SettlementGroupID,
				ClaimLawsuitID,
				SettlementsFundsRequestDate,
				SocialSecurityNumber AS SSN,
				HICN,
				MissingClaimantInfo,
				MissingDocumentation
FROM vMissingSettlementDocumentation msd
'
+ @JoinString +
@WhereString +
' ORDER BY ClaimantName 
  OPTION (RECOMPILE)'

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs] TO [power_user]
GO
