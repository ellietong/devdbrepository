CREATE TABLE [dbo].[tblDefendantCompanyInsProg]
(
[DefendantCompanyInsCoID] [int] NOT NULL,
[DefendantCompanyID] [int] NULL,
[InsuranceProgramID] [int] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDefendantCompanyInsProg] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefendantCompanyInsProg] TO [Claims_User]
GO
