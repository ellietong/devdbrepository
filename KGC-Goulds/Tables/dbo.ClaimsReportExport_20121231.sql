CREATE TABLE [dbo].[ClaimsReportExport_20121231]
(
[ClaimantID] [float] NULL,
[Claimant Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [float] NULL,
[Claim Sequence] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Birth Date] [datetime] NULL,
[Deceased Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Occupation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product Exposure Start] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active Trial Group] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active Trial Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Service Date] [datetime] NULL,
[File Date] [datetime] NULL,
[Processed Date] [datetime] NULL,
[Status Date] [datetime] NULL,
[Docket Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff Counsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settlement Date] [datetime] NULL,
[Settlement Amount] [money] NULL,
[Settlement Paid Date] [datetime] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ClaimsReportExport_20121231] TO [Claims_User]
GO
