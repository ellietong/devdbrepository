CREATE TABLE [dbo].[tblSettlementShare]
(
[SettlementShareID] [int] NOT NULL,
[ClaimLawsuitID] [int] NULL,
[InsuranceProgramID] [int] NULL,
[SettlementShare] [decimal] (18, 0) NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSettlementShare] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementShare] TO [Claims_User]
GO
