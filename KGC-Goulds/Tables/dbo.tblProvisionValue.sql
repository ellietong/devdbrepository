CREATE TABLE [dbo].[tblProvisionValue]
(
[ProvisionValueID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionLabelID] [int] NOT NULL,
[ProvisionValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionValue] ADD CONSTRAINT [PK__tblProvisionValu__24927208] PRIMARY KEY CLUSTERED  ([ProvisionValueID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionValue] ADD CONSTRAINT [FK_tblProvisionValue_tblProvisionLabel] FOREIGN KEY ([ProvisionLabelID]) REFERENCES [dbo].[tblProvisionLabel] ([ProvisionLabelID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblProvisionValue] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionValue] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblProvisionValue] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvisionValue] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvisionValue] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionValue] TO [Provision_User]
GRANT INSERT ON  [dbo].[tblProvisionValue] TO [Provision_User]
GRANT DELETE ON  [dbo].[tblProvisionValue] TO [Provision_User]
GRANT UPDATE ON  [dbo].[tblProvisionValue] TO [Provision_User]
GO
