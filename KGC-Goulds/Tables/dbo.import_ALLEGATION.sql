CREATE TABLE [dbo].[import_ALLEGATION]
(
[CLM_LWST_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALLEGATION] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DAMAGE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MATERIAL] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[import_ALLEGATION] TO [base_user]
GRANT SELECT ON  [dbo].[import_ALLEGATION] TO [Claims_User]
GO
