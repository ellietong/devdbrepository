CREATE TABLE [dbo].[Import_tblAllocationDetails_Goulds]
(
[ID] [int] NOT NULL,
[ScenarioID] [int] NULL,
[AllocationID] [int] NULL,
[ClaimID] [int] NULL,
[PolicyID] [int] NULL,
[SegmentID] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EffectiveDays] [decimal] (18, 2) NULL,
[PercentOfDays] [decimal] (18, 2) NULL,
[TotalIndemnity] [money] NULL,
[TotalExpense] [money] NULL,
[AmountType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_tblAllocationDetails_Goulds] TO [base_user]
GO
