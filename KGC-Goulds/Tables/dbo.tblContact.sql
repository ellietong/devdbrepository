CREATE TABLE [dbo].[tblContact]
(
[ContactID] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Position] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactTypeID] [int] NOT NULL,
[Company] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CareOf] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine1] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine2] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine3] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[Zip] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BadAddress] [bit] NULL,
[ContactClaimTypeID] [int] NOT NULL,
[InternalContact] [bit] NOT NULL CONSTRAINT [DF_tblContact_InternalContact] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblContact] ADD CONSTRAINT [PK_tblContact] PRIMARY KEY CLUSTERED  ([ContactID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblContact] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblContact] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblContact] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblContact] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblContact] TO [Notice_User]
GRANT SELECT ON  [dbo].[tblContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblContact] TO [power_user]
GO
