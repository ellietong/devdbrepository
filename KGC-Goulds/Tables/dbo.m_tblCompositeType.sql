CREATE TABLE [dbo].[m_tblCompositeType]
(
[CompositeTypeID] [int] NOT NULL IDENTITY(1, 1),
[CompositeType] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblCompositeType] ADD CONSTRAINT [PK_m_tblCompositeType] PRIMARY KEY CLUSTERED  ([CompositeTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblCompositeType] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblCompositeType] TO [Billing_User]
GRANT INSERT ON  [dbo].[m_tblCompositeType] TO [Billing_User]
GRANT DELETE ON  [dbo].[m_tblCompositeType] TO [Billing_User]
GRANT UPDATE ON  [dbo].[m_tblCompositeType] TO [Billing_User]
GRANT SELECT ON  [dbo].[m_tblCompositeType] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblCompositeType] TO [Claims_User]
GRANT DELETE ON  [dbo].[m_tblCompositeType] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblCompositeType] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblCompositeType] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[m_tblCompositeType] TO [Occurrence_User]
GRANT DELETE ON  [dbo].[m_tblCompositeType] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[m_tblCompositeType] TO [Occurrence_User]
GO
