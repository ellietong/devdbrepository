CREATE TABLE [dbo].[Import_tblPolicyBills_Goulds]
(
[ScenarioID] [int] NULL,
[AllocationID] [int] NULL,
[PolicyID] [int] NULL,
[PolicyType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConsumptionType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppliedIndemnity] [money] NULL,
[AppliedExpense] [money] NULL,
[NonAppliedExpense] [money] NULL,
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_tblPolicyBills_Goulds] TO [base_user]
GO
