CREATE TABLE [dbo].[tblClaimantInformationCategory]
(
[ClaimantInfoCategoryID] [int] NOT NULL IDENTITY(1, 1),
[ClaimantInfoCategory] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantInformationCategory] ADD CONSTRAINT [PK_tblClaimantInformationCategory] PRIMARY KEY CLUSTERED  ([ClaimantInfoCategoryID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantInformationCategory] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantInformationCategory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantInformationCategory] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantInformationCategory] TO [power_user]
GO
