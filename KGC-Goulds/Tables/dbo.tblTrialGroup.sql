CREATE TABLE [dbo].[tblTrialGroup]
(
[TrialGroupID] [bigint] NOT NULL IDENTITY(1, 1),
[TrialGroup] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[spusername] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTrialGroup] ADD CONSTRAINT [PK_tblTrialGroup] PRIMARY KEY CLUSTERED  ([TrialGroupID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblTrialGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblTrialGroup] TO [Claims_User]
GO
