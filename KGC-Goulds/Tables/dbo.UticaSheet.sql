CREATE TABLE [dbo].[UticaSheet]
(
[Claimant Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice Date] [datetime] NULL,
[Invoice #] [float] NULL,
[Fees] [money] NULL,
[Costs] [money] NULL,
[Goulds Amount] [money] NULL,
[Utica Amount] [money] NULL,
[Utica Adjustment] [money] NULL,
[Utica Share] [float] NULL,
[Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Total] [money] NULL,
[Utica Invoice Total] [money] NULL,
[Utica Approved] [money] NULL,
[Utica Paid] [money] NULL,
[Utica Adj] [money] NULL,
[Utica %] [float] NULL,
[Goulds Amount1] [money] NULL
) ON [PRIMARY]
GO
