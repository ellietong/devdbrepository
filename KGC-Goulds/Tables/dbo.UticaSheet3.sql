CREATE TABLE [dbo].[UticaSheet3]
(
[Claimant Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOFE] [float] NULL,
[InvoiceAmount] [money] NULL,
[ApprovedAmout] [money] NULL,
[UticaAmount] [money] NULL,
[Payee] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UticaDOFE] [float] NULL
) ON [PRIMARY]
GO
