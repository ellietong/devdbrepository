CREATE TABLE [dbo].[tblDiscountOccType]
(
[DiscountOccTypeID] [int] NOT NULL IDENTITY(1, 1),
[OccurrenceTypeID] [int] NULL,
[OccTypeDiscount] [money] NULL,
[DiscountID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountOccType] ADD CONSTRAINT [PK_tblDiscountOccType] PRIMARY KEY CLUSTERED  ([DiscountOccTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountOccType] ADD CONSTRAINT [FK_tblDiscountOccType_tblOccurrenceType] FOREIGN KEY ([OccurrenceTypeID]) REFERENCES [dbo].[tblOccurrenceType] ([OccurrenceTypeID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblDiscountOccType] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiscountOccType] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDiscountOccType] TO [power_user]
GRANT INSERT ON  [dbo].[tblDiscountOccType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDiscountOccType] TO [power_user]
GO
