CREATE TABLE [dbo].[DefenseQueryResults]
(
[DefenseInvoiceID] [float] NULL,
[InvoiceLink] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Defensecounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantID] [float] NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialSecurityNumber] [float] NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [float] NULL,
[fees] [money] NULL,
[costs] [money] NULL,
[Total] [money] NULL,
[AdjustedTotal] [money] NULL,
[DOFE] [float] NULL,
[InsurerShare] [float] NULL,
[InsurerAmount] [money] NULL,
[ClientShare] [float] NULL,
[ClientAmount] [money] NULL
) ON [PRIMARY]
GO
