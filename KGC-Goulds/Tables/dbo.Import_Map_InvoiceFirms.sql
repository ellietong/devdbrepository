CREATE TABLE [dbo].[Import_Map_InvoiceFirms]
(
[submitted_from_company] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselID] [float] NULL,
[defensecounseladdressid] [float] NULL,
[F5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
