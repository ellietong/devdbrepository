CREATE TABLE [dbo].[Import_INVOICES]
(
[InvoiceID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reference ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_PERIOD_DATE] [date] NULL,
[INVOICE_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_IDENTIFIER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_TYPE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_FEE_AMT] [float] NULL,
[INVOICE_DISBURSEMENT_AMT] [float] NULL,
[ADJUSTMENT_AMT] [float] NULL,
[InvoiceTotal] [float] NULL,
[INVOICE_STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUBMITTAL_DATE] [date] NULL,
[CHECK_NUMBER] [int] NULL,
[CHECK_DATE] [date] NULL,
[SUBMITTED_FROM_COMPANY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALLOCATION_INVOICE NUMBER] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALLOCATION_FEE_AMT] [money] NULL,
[ALLOCATION_DISBURSEMENT_AMT] [money] NULL,
[ALLOCATION_TOTAL] [money] NULL,
[GOULDS SHARE] [money] NULL,
[UTICAS SHARE] [money] NULL,
[UTICA ADJUSTMENT] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_INVOICES] TO [base_user]
GRANT SELECT ON  [dbo].[Import_INVOICES] TO [Claims_User]
GO
