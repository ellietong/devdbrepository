CREATE TABLE [dbo].[tblAllocationResultsHorizontal]
(
[ARhorizontalID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[OccurrenceID] [bigint] NULL,
[uncollapsedPrior] [money] NULL,
[uncollapsedPost] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsHorizontal] ADD CONSTRAINT [PK_tblAllocationResultsHorizontal] PRIMARY KEY NONCLUSTERED  ([ARhorizontalID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsHorizontal] ADD CONSTRAINT [FK_tblAllocationResultsHorizontal_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationResultsHorizontal] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationResultsHorizontal] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationResultsHorizontal] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationResultsHorizontal] TO [power_user]
GRANT DELETE ON  [dbo].[tblAllocationResultsHorizontal] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationResultsHorizontal] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResultsHorizontal', 'COLUMN', N'OccurrenceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResultsHorizontal', 'COLUMN', N'OccurrenceID'
GO
