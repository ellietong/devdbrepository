CREATE TABLE [dbo].[Import_Map_Suffix]
(
[SUFFIX] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuffixID] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_Map_Suffix] TO [base_user]
GRANT SELECT ON  [dbo].[Import_Map_Suffix] TO [Claims_User]
GO
