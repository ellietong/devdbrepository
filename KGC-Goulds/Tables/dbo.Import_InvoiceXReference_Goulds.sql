CREATE TABLE [dbo].[Import_InvoiceXReference_Goulds]
(
[INVOICEIDENTIFIER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILENAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
