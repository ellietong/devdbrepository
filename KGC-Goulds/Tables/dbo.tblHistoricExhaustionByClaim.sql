CREATE TABLE [dbo].[tblHistoricExhaustionByClaim]
(
[PACEClaimID] [int] NULL,
[PACEPolicyID] [int] NULL,
[TotalIndemnity] [money] NULL,
[MonthlyBillID] [int] NULL,
[AllocationID] [int] NULL,
[PolicyDateID] [int] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblHistoricExhaustionByClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblHistoricExhaustionByClaim] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblHistoricExhaustionByClaim] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblHistoricExhaustionByClaim] TO [power_user]
GO
