CREATE TABLE [dbo].[m_tblClaimantPersonalInfo_Lookup]
(
[ClaimantPersonalInfoID] [int] NULL,
[INJ_PRTY_KEY] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INJ_PRTY_ID] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_m_tblClaimantPersonalinfo_Lookup_CPPID] ON [dbo].[m_tblClaimantPersonalInfo_Lookup] ([ClaimantPersonalInfoID]) INCLUDE ([INJ_PRTY_ID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblClaimantPersonalInfo_Lookup] TO [power_user]
GO
