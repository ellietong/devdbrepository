CREATE TABLE [dbo].[tblSiteType]
(
[SiteTypeID] [int] NOT NULL IDENTITY(1, 1),
[SiteType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSiteType] ADD CONSTRAINT [PK_tblSiteType] PRIMARY KEY CLUSTERED  ([SiteTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSiteType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSiteType] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblSiteType] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblSiteType] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblSiteType] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblSiteType] TO [Enviro_User]
GRANT SELECT ON  [dbo].[tblSiteType] TO [power_user]
GRANT INSERT ON  [dbo].[tblSiteType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSiteType] TO [power_user]
GO
