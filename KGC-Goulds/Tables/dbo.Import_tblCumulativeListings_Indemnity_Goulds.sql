CREATE TABLE [dbo].[Import_tblCumulativeListings_Indemnity_Goulds]
(
[MonthlyBillID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Month] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [int] NOT NULL,
[Injured Party ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantLast] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantFirst] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantSSN] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateFiled] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILE DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementDate] [datetime] NULL,
[ExposureDate] [datetime] NULL,
[DiagnosisDate] [datetime] NULL,
[Condition] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SumOfClaimAmount] [money] NULL,
[CONTINENTAL CASUALTY1] [money] NULL,
[PACIFIC EMPLOYER] [money] NULL,
[JURISDICTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[first Exposure Date] [datetime] NULL,
[RSLTN_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FExposure] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstExposure] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reference ID - ONLY FOR BEFORE 72577] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_tblCumulativeListings_Indemnity_Goulds] TO [base_user]
GO
