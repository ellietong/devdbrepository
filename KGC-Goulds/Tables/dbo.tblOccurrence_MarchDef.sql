CREATE TABLE [dbo].[tblOccurrence_MarchDef]
(
[OccurrenceID] [float] NULL,
[AggregateApplies] [float] NULL,
[ExpectedPayDate] [datetime] NULL,
[InClaim] [float] NULL,
[OccurrenceName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrenceGroupID] [float] NULL,
[CorporateHistoryID] [float] NULL,
[OccurrenceTypeID] [float] NULL,
[ClaimID] [float] NULL,
[Segment] [float] NULL,
[TriggerStart1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TriggerEnd1] [datetime] NULL,
[Cost1] [money] NULL,
[Cost3] [money] NULL,
[OccurrenceNote] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
