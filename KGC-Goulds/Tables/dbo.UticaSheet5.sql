CREATE TABLE [dbo].[UticaSheet5]
(
[Claimant Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DOFE] [float] NULL,
[InvoiceAmount] [money] NULL,
[ApprovedAmount] [money] NULL,
[UticaAmount] [money] NULL,
[payee] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UticaDOFE] [float] NULL,
[DOFE QC] [float] NULL
) ON [PRIMARY]
GO
