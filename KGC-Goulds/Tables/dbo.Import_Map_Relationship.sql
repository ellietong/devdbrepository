CREATE TABLE [dbo].[Import_Map_Relationship]
(
[RELATIONSHIP] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MappedRelationshipID] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_Map_Relationship] TO [base_user]
GRANT SELECT ON  [dbo].[Import_Map_Relationship] TO [Claims_User]
GO
