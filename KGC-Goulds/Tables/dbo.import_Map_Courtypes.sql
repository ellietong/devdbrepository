CREATE TABLE [dbo].[import_Map_Courtypes]
(
[COURT_TYPE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KCIC CourtTypeID (Mapped)] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[import_Map_Courtypes] TO [base_user]
GRANT SELECT ON  [dbo].[import_Map_Courtypes] TO [Claims_User]
GO
