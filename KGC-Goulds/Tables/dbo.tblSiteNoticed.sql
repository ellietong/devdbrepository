CREATE TABLE [dbo].[tblSiteNoticed]
(
[SiteNoticedID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NOT NULL,
[SiteID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSiteNoticed] ADD CONSTRAINT [PK_tblSiteNoticed] PRIMARY KEY CLUSTERED  ([SiteNoticedID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSiteNoticed] ADD CONSTRAINT [FK_tblSiteNoticed_tblCorrespondence] FOREIGN KEY ([CorrespondenceID]) REFERENCES [dbo].[tblCorrespondence] ([CorrespondenceID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblSiteNoticed] TO [base_user]
GRANT SELECT ON  [dbo].[tblSiteNoticed] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblSiteNoticed] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblSiteNoticed] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblSiteNoticed] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblSiteNoticed] TO [Notice_User]
GRANT SELECT ON  [dbo].[tblSiteNoticed] TO [power_user]
GRANT INSERT ON  [dbo].[tblSiteNoticed] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSiteNoticed] TO [power_user]
GO
