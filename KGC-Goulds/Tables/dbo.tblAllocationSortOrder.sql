CREATE TABLE [dbo].[tblAllocationSortOrder]
(
[SortOrderAllocationID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NOT NULL,
[SortOrderID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationSortOrder] ADD CONSTRAINT [PK_tblAllocationSortOrder] PRIMARY KEY CLUSTERED  ([SortOrderAllocationID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblAllocationSortOrder] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationSortOrder] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationSortOrder] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationSortOrder] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationSortOrder] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationSortOrder] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblAllocationSortOrder] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationSortOrder] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationSortOrder] TO [power_user]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'AllocationID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'AllocationID'
GO
DECLARE @xp smallint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'AllocationID'
GO
DECLARE @xp smallint
SELECT @xp=6720
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'AllocationID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'AllocationID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'SortOrderAllocationID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'SortOrderAllocationID'
GO
DECLARE @xp smallint
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'SortOrderAllocationID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'SortOrderAllocationID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'SortOrderAllocationID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'SortOrderID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'SortOrderID'
GO
DECLARE @xp smallint
SELECT @xp=3
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'SortOrderID'
GO
DECLARE @xp smallint
SELECT @xp=12825
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'SortOrderID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationSortOrder', 'COLUMN', N'SortOrderID'
GO
