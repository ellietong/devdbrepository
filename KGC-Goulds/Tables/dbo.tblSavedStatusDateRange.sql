CREATE TABLE [dbo].[tblSavedStatusDateRange]
(
[SavedStatusDateID] [int] NOT NULL IDENTITY(1, 1),
[StatusDateRange] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedStatusDateRange] ADD CONSTRAINT [PK_tblSavedStatusDateRange] PRIMARY KEY CLUSTERED  ([SavedStatusDateID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSavedStatusDateRange] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedStatusDateRange] TO [Claims_User]
GO
