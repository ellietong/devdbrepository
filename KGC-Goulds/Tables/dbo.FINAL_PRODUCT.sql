CREATE TABLE [dbo].[FINAL_PRODUCT]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRODUCT_NAME] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BRAND] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRODUCT_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MANUFACTURER] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[START_DATE] [datetime] NULL,
[END_DATE] [datetime] NULL,
[IS_PRIMARY] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMMENTS] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[FINAL_PRODUCT] TO [Claims_User]
GO
