CREATE TABLE [dbo].[tblDiscount]
(
[DiscountID] [int] NOT NULL IDENTITY(1, 1),
[DiscountDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeneralDiscount] [money] NULL,
[AllocationID] [int] NULL,
[ReportFormat] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LimitCarrierGroupID] [int] NULL,
[LimitCarrierID] [int] NULL,
[IncludeDescription] [bit] NULL,
[IncludeActualDiscounts] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscount] ADD CONSTRAINT [PK_tblDiscount] PRIMARY KEY CLUSTERED  ([DiscountID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscount] ADD CONSTRAINT [FK_tblDiscount_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblDiscount] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiscount] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDiscount] TO [power_user]
GRANT INSERT ON  [dbo].[tblDiscount] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDiscount] TO [power_user]
GO
