CREATE TABLE [dbo].[uticasheet1]
(
[Claimant Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceAmt] [money] NULL,
[AmtApproved] [money] NULL,
[UticaAmt] [money] NULL,
[UticaAdjustment] [money] NULL,
[Utica%] [float] NULL,
[GouldsAmt] [money] NULL,
[ClaimantPersonalInfoID] [float] NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
