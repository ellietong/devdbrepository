CREATE TABLE [dbo].[tblClaimantProduct_Archive_20120309]
(
[ClaimantPersonalInfoID] [bigint] NULL,
[ClaimantProductID] [bigint] NOT NULL,
[ClaimantEmploymentID] [bigint] NULL,
[ProductID] [int] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductTypeID] [int] NULL,
[ProductType] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductManufacturerID] [int] NULL,
[ProductManufacturer] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductBrandID] [int] NULL,
[ProductBrand] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductNameID] [int] NULL,
[ProductName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimantProduct_Archive_20120309] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantProduct_Archive_20120309] TO [Claims_User]
GO
