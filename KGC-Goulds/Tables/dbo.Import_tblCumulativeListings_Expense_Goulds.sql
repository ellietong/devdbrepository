CREATE TABLE [dbo].[Import_tblCumulativeListings_Expense_Goulds]
(
[Month] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PACE Reference] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimSequenceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date of First Exposure] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Edited - State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Defense Counsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOCIAL SECURITY NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Edited - SSN] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[SettlementDate] [datetime] NULL,
[Condition] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimAmount] [money] NULL,
[ACEExp] [money] NULL,
[CNAExp] [money] NULL,
[Fees] [money] NULL,
[Disbursements] [money] NULL,
[Fees & Disbursements] [money] NULL,
[ExposureDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_tblCumulativeListings_Expense_Goulds] TO [base_user]
GO
