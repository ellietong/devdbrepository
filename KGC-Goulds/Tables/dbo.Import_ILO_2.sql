CREATE TABLE [dbo].[Import_ILO_2]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME_OF_READER] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROFUSION_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILO_DATE] [datetime] NULL
) ON [PRIMARY]
GO
