CREATE TABLE [dbo].[tblLedger]
(
[LedgerID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupID] [int] NULL,
[CarrierID] [int] NOT NULL,
[SubscriberID] [int] NULL,
[LedgerTypeID] [int] NOT NULL,
[LedgerDate] [datetime] NULL,
[CheckNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Debit] [money] NOT NULL CONSTRAINT [DF_tblLedger_Debit_2] DEFAULT ((0)),
[Credit] [money] NULL CONSTRAINT [DF_tblLedger_Credit_2] DEFAULT ((0)),
[Exhaustiondateid] [int] NULL,
[BillID] [int] NULL,
[LedgerNote] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppliedToBalance] [bit] NULL CONSTRAINT [DF_tblLedger_AppliedToBalance_1] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLedger] ADD CONSTRAINT [PK_tblLedger] PRIMARY KEY CLUSTERED  ([LedgerID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLedger] TO [base_user]
GRANT SELECT ON  [dbo].[tblLedger] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLedger] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblLedger] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblLedger] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblLedger] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblLedger] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblLedger] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblLedger] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblLedger] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblLedger] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblLedger] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblLedger] TO [power_user]
GRANT SELECT ON  [dbo].[tblLedger] TO [power_user]
GRANT INSERT ON  [dbo].[tblLedger] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLedger] TO [power_user]
GO
