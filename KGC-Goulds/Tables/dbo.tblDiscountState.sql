CREATE TABLE [dbo].[tblDiscountState]
(
[DiscountStateID] [int] NOT NULL IDENTITY(1, 1),
[StateID] [int] NULL,
[StateDiscount] [money] NULL,
[DiscountID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountState] ADD CONSTRAINT [PK_tblDiscountState] PRIMARY KEY CLUSTERED  ([DiscountStateID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDiscountState] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiscountState] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDiscountState] TO [power_user]
GRANT INSERT ON  [dbo].[tblDiscountState] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDiscountState] TO [power_user]
GO
