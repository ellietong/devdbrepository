CREATE TABLE [dbo].[import_map_PLCounsel]
(
[PlaintiffCounselAddressID] [float] NULL,
[CompanyName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[import_map_PLCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[import_map_PLCounsel] TO [Claims_User]
GO
