CREATE TABLE [dbo].[Import_ILO]
(
[INJ_PRTY_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME_OF_READER] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROFUSION_TYPE] [nvarchar] (125) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_ILO] TO [base_user]
GRANT SELECT ON  [dbo].[Import_ILO] TO [Claims_User]
GO
