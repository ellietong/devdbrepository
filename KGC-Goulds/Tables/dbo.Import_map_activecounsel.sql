CREATE TABLE [dbo].[Import_map_activecounsel]
(
[DefenseJurisdictionID] [float] NULL,
[IsActive] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_map_activecounsel] TO [base_user]
GRANT SELECT ON  [dbo].[Import_map_activecounsel] TO [Claims_User]
GO
