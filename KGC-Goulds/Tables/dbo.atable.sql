CREATE TABLE [dbo].[atable]
(
[id] [int] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[atable] TO [base_user]
GRANT SELECT ON  [dbo].[atable] TO [Claims_User]
GO
