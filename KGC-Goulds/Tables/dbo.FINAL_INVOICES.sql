CREATE TABLE [dbo].[FINAL_INVOICES]
(
[InvoiceID] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reference ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_PERIOD_DATE] [datetime] NULL,
[INVOICE_NUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_TYPE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVOICE_FEE_AMT] [money] NULL,
[INVOICE_DISBURSEMENT_AMT] [money] NULL,
[ADJUSTMENT_AMT] [money] NULL,
[InvoiceTotal] [money] NULL,
[INVOICE_STATUS] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHECK_NUMBER] [int] NULL,
[CHECK_DATE] [datetime] NULL,
[SUBMITTED_FROM_COMPANY] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor ID] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vendor] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALLOCATION_INVOICE NUMBER] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALLOCATION_FEE_AMT] [money] NULL,
[ALLOCATION_DISBURSEMENT_AMT] [money] NULL,
[ALLOCATION_TOTAL] [money] NULL,
[GOULDS SHARE] [money] NULL,
[UTICAS SHARE] [money] NULL,
[UTICA ADJUSTMENT] [money] NULL,
[VOIDED] [bit] NOT NULL,
[SUBMITTAL_DATE] [datetime] NULL,
[APPROVAL_DATE] [datetime] NULL,
[INVOICE_IDENTIFIER] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[FINAL_INVOICES] TO [Claims_User]
GO
