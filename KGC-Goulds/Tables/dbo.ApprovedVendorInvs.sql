CREATE TABLE [dbo].[ApprovedVendorInvs]
(
[DefenseInvoiceID] [float] NULL,
[DocumentID] [float] NULL,
[IndivInvoiceNo] [float] NULL,
[InvoiceDetailID] [float] NULL,
[TotalAmt] [money] NULL,
[UticaPercent] [float] NULL,
[UticaAmt] [money] NULL,
[ClaimantName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmittedDOFE] [float] NULL
) ON [PRIMARY]
GO
