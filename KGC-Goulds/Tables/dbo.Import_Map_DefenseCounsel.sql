CREATE TABLE [dbo].[Import_Map_DefenseCounsel]
(
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PACESource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KCIC DefenseCounselAddressID (Mapped)] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_Map_DefenseCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[Import_Map_DefenseCounsel] TO [Claims_User]
GO
