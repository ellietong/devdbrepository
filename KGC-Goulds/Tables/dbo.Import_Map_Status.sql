CREATE TABLE [dbo].[Import_Map_Status]
(
[PACESource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KCIC ClaimLawsuitStatusID (Mapped)] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_Map_Status] TO [base_user]
GRANT SELECT ON  [dbo].[Import_Map_Status] TO [Claims_User]
GO
