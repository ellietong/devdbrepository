CREATE TABLE [dbo].[Final_SettlementAmt_Updates]
(
[clm_lwst_key] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullSettlementAmt] [money] NULL,
[INSURER_AMT] [money] NULL,
[CLIENT_AMT] [money] NULL,
[settle_Amt] [money] NULL
) ON [PRIMARY]
GO
