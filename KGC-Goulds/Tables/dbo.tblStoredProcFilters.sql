CREATE TABLE [dbo].[tblStoredProcFilters]
(
[StoredProcID] [int] NOT NULL,
[FilterID] [int] NULL,
[ID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblStoredProcFilters] ADD CONSTRAINT [PK_TblStoredProcFilters] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblStoredProcFilters] TO [base_user]
GRANT SELECT ON  [dbo].[tblStoredProcFilters] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblStoredProcFilters] TO [power_user]
GRANT INSERT ON  [dbo].[tblStoredProcFilters] TO [power_user]
GRANT UPDATE ON  [dbo].[tblStoredProcFilters] TO [power_user]
GRANT SELECT ON  [dbo].[tblStoredProcFilters] TO [Reports_User]
GRANT INSERT ON  [dbo].[tblStoredProcFilters] TO [Reports_User]
GRANT DELETE ON  [dbo].[tblStoredProcFilters] TO [Reports_User]
GRANT UPDATE ON  [dbo].[tblStoredProcFilters] TO [Reports_User]
GO
