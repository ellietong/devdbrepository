CREATE TABLE [dbo].[m_tblInsurerPercentageByYear]
(
[DOFE] [float] NULL,
[UticaPercent] [float] NULL,
[GouldsPercent] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblInsurerPercentageByYear] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblInsurerPercentageByYear] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblInsurerPercentageByYear] TO [power_user]
GO
