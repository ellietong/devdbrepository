CREATE TABLE [dbo].[Import_tblPolicyInfo_Goulds]
(
[ScenarioID] [int] NULL,
[PolicyID] [int] NOT NULL,
[InsurerID] [int] NULL,
[InsurerName] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[LimitAmount] [money] NULL,
[Layer] [tinyint] NULL,
[TowerYear] [smallint] NULL,
[PolDefForm] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsQuotaShare] [bit] NOT NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_tblPolicyInfo_Goulds] TO [base_user]
GO
