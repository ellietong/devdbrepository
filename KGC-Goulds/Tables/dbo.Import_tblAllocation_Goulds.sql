CREATE TABLE [dbo].[Import_tblAllocation_Goulds]
(
[ScenarioID] [int] NULL,
[AllocationID] [int] NOT NULL,
[MonthlyBillID] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillNumber] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Month] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Year] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_tblAllocation_Goulds] TO [base_user]
GO
