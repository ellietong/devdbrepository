CREATE TABLE [dbo].[tblCheck]
(
[CheckID] [bigint] NOT NULL IDENTITY(1, 1),
[SourceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceDate] [datetime] NULL,
[SourceAmount] [money] NULL,
[MatterID] [int] NULL CONSTRAINT [DF_tblCheck_MatterID] DEFAULT ((1)),
[FundingTypeID] [int] NULL,
[FundingSourceID] [int] NULL,
[CheckPDF] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCheck] ADD CONSTRAINT [PK_tblCheck] PRIMARY KEY CLUSTERED  ([CheckID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCheck] TO [base_user]
GRANT SELECT ON  [dbo].[tblCheck] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCheck] TO [CheckApproval_User]
GRANT SELECT ON  [dbo].[tblCheck] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheck] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[tblCheck] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheck] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblCheck] TO [CheckWriter_User]
GRANT INSERT ON  [dbo].[tblCheck] TO [CheckWriter_User]
GRANT UPDATE ON  [dbo].[tblCheck] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblCheck] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblCheck] TO [prossc]
GO
