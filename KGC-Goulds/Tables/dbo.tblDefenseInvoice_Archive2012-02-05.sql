CREATE TABLE [dbo].[tblDefenseInvoice_Archive2012-02-05]
(
[DefenseInvoiceID] [bigint] NOT NULL IDENTITY(1, 1),
[StatusID] [int] NULL,
[FirmID] [bigint] NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeAmount] [money] NULL CONSTRAINT [DF_mBtblDefenseInvoice_FeeAmount] DEFAULT ((0)),
[DisbursementAmount] [money] NULL CONSTRAINT [DF_mBtblDefenseInvoice_DisbursementAmount] DEFAULT ((0)),
[Adjustment] [money] NULL CONSTRAINT [DF_mBtblDefenseInvoice_Adjustment] DEFAULT ((0)),
[InvoiceTotal] [money] NULL,
[SubmitDate] [datetime] NULL,
[DatePayable] [datetime] NULL,
[CheckNumber] [bigint] NULL,
[FundsRequestDate] [datetime] NULL,
[DatePaid] [datetime] NULL,
[DocumentID] [bigint] NULL,
[AdjustmentNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceLink] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckLink] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[VendorFirmID] [int] NULL,
[FirmID_old] [int] NULL,
[VendorFirmID_old] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseInvoice_Archive2012-02-05] ADD CONSTRAINT [PK_mBtblDefenseInvoiceTracking] PRIMARY KEY CLUSTERED  ([DefenseInvoiceID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDefenseInvoice_Archive2012-02-05] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice_Archive2012-02-05] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefenseInvoice_Archive2012-02-05] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseInvoice_Archive2012-02-05] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseInvoice_Archive2012-02-05] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefenseInvoice_Archive2012-02-05] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseInvoice_Archive2012-02-05] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseInvoice_Archive2012-02-05] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'Adjustment'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'Adjustment'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'Adjustment'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'Adjustment'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'Adjustment'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'Adjustment'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'CheckNumber'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'CheckNumber'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'CheckNumber'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'CheckNumber'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'CheckNumber'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'CheckNumber'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DatePayable'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DatePayable'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DatePayable'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DefenseInvoiceID'
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DefenseInvoiceID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DefenseInvoiceID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DefenseInvoiceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DefenseInvoiceID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DefenseInvoiceID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DisbursementAmount'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DisbursementAmount'
GO
DECLARE @xp int
SELECT @xp=1065
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DisbursementAmount'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DisbursementAmount'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DisbursementAmount'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'DisbursementAmount'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FeeAmount'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FeeAmount'
GO
DECLARE @xp int
SELECT @xp=1245
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FeeAmount'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FeeAmount'
GO
EXEC sp_addextendedproperty N'MS_Format', N'$#,##0.00;($#,##0.00)', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FeeAmount'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FeeAmount'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FirmID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FirmID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'FirmID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'InvoiceNumber'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'InvoiceNumber'
GO
DECLARE @xp int
SELECT @xp=1980
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'InvoiceNumber'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'InvoiceTotal'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'InvoiceTotal'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'InvoiceTotal'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'Period'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'Period'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'Period'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'StatusID'
GO
DECLARE @xp int
SELECT @xp=3
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'StatusID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'StatusID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'StatusID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'StatusID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'SubmitDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'SubmitDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoice_Archive2012-02-05', 'COLUMN', N'SubmitDate'
GO
