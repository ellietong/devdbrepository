CREATE TABLE [dbo].[Import_tblHistoricalClaims_Expense_Jan06_Jun12_Goulds]
(
[Invoice Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date of First Exposure] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAID DATE] [datetime] NULL,
[Defense Counsel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CNA - RDU 1246676] [float] NULL,
[CNA - RDU 2928288] [float] NULL,
[CNA Total] [float] NULL,
[UTICA] [float] NULL,
[ACE - XMO 01427 (A)] [float] NULL,
[ACE - XMO 01427 (B)] [float] NULL,
[ACE - XMO 015223] [float] NULL,
[ACE Total] [float] NULL,
[GOULDS(Producer)] [float] NULL,
[GOULDS (CNA - G)] [float] NULL,
[GOULDS (Dummy)] [float] NULL,
[Goulds Total] [float] NULL,
[Total] [float] NULL,
[Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Paid Year] [smallint] NULL,
[MBID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claim Sequence] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_tblHistoricalClaims_Expense_Jan06_Jun12_Goulds] TO [base_user]
GO
