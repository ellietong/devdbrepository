CREATE TABLE [dbo].[tblHistoricCompositeAdjustments]
(
[ClaimNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimantName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Adjustment] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate ] [datetime] NULL,
[SettledDate] [datetime] NULL,
[ExposureDate] [datetime] NULL,
[DiagnosisDate] [datetime] NULL,
[AmountType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblHistoricCompositeAdjustments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblHistoricCompositeAdjustments] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblHistoricCompositeAdjustments] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblHistoricCompositeAdjustments] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblHistoricCompositeAdjustments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblHistoricCompositeAdjustments] TO [Claims_User]
GO
