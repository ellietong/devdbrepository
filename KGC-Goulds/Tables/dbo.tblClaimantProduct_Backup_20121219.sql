CREATE TABLE [dbo].[tblClaimantProduct_Backup_20121219]
(
[ClaimantProductID] [bigint] NULL,
[ClaimantEmploymentID] [bigint] NULL,
[ProductID] [int] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryProduct] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
