CREATE TABLE [dbo].[tblHistoricalMultiClaimantDocuments]
(
[ClaimantDocumentLinkID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[ClaimantDocumentID] [bigint] NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDocumentNumber] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDocumentNameInFolder] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblHistoricalMultiClaimantDocuments] ADD CONSTRAINT [PK_tblHistoricalMultiClaimantDocuments] PRIMARY KEY CLUSTERED  ([ClaimantDocumentLinkID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblHistoricalMultiClaimantDocuments] ADD CONSTRAINT [FK_tblHistoricalMultiClaimantDocuments_tblClaimantDocuments] FOREIGN KEY ([ClaimantDocumentID]) REFERENCES [dbo].[tblClaimantDocuments] ([ClaimantDocumentID])
GO
ALTER TABLE [dbo].[tblHistoricalMultiClaimantDocuments] WITH NOCHECK ADD CONSTRAINT [FK_tblHistoricalMultiClaimantDocuments_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[tblHistoricalMultiClaimantDocuments] NOCHECK CONSTRAINT [FK_tblHistoricalMultiClaimantDocuments_tblClaimantPersonalInfo]
GO
GRANT SELECT ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblHistoricalMultiClaimantDocuments] TO [Claims_User]
GO
