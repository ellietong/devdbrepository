CREATE TABLE [dbo].[DiseaseLookup]
(
[PACEDisease] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiseaseID] [int] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[DiseaseLookup] TO [base_user]
GRANT SELECT ON  [dbo].[DiseaseLookup] TO [Claims_User]
GO
