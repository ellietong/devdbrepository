CREATE TABLE [dbo].[tblDisease_ALT]
(
[DiseaseID_Alt] [int] NOT NULL,
[ICD9] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICD9Desc] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighLevelDiseaseID] [int] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDisease_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblDisease_ALT] TO [Claims_User]
GO
