CREATE TABLE [dbo].[tblSiteActivity]
(
[SiteActivityID] [int] NOT NULL IDENTITY(1, 1),
[SiteActivity] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSiteActivity] ADD CONSTRAINT [PK_mEtblSiteActivity] PRIMARY KEY CLUSTERED  ([SiteActivityID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSiteActivity] TO [base_user]
GRANT SELECT ON  [dbo].[tblSiteActivity] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblSiteActivity] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblSiteActivity] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblSiteActivity] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblSiteActivity] TO [Enviro_User]
GRANT SELECT ON  [dbo].[tblSiteActivity] TO [power_user]
GRANT INSERT ON  [dbo].[tblSiteActivity] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSiteActivity] TO [power_user]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', 'COLUMN', N'SiteActivity'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', 'COLUMN', N'SiteActivity'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', 'COLUMN', N'SiteActivity'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', 'COLUMN', N'SiteActivity'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', 'COLUMN', N'SiteActivity'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', 'COLUMN', N'SiteActivityID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', 'COLUMN', N'SiteActivityID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', 'COLUMN', N'SiteActivityID'
GO
DECLARE @xp smallint
SELECT @xp=1680
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', 'COLUMN', N'SiteActivityID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblSiteActivity', 'COLUMN', N'SiteActivityID'
GO
