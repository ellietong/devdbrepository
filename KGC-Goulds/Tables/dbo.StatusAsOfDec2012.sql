CREATE TABLE [dbo].[StatusAsOfDec2012]
(
[ClaimLawsuitID] [int] NULL,
[Dec2012Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[StatusAsOfDec2012] TO [base_user]
GRANT SELECT ON  [dbo].[StatusAsOfDec2012] TO [Claims_User]
GRANT INSERT ON  [dbo].[StatusAsOfDec2012] TO [Claims_User]
GO
