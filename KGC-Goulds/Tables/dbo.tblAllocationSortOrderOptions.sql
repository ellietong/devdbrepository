CREATE TABLE [dbo].[tblAllocationSortOrderOptions]
(
[SortOrderID] [int] NOT NULL IDENTITY(1, 1),
[SortOrderField] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrderFieldSQL] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationSortOrderOptions] ADD CONSTRAINT [PK_tblAllocationSortOrderOptions] PRIMARY KEY CLUSTERED  ([SortOrderID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblAllocationSortOrderOptions] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationSortOrderOptions] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationSortOrderOptions] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationSortOrderOptions] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationSortOrderOptions] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationSortOrderOptions] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblAllocationSortOrderOptions] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationSortOrderOptions] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationSortOrderOptions] TO [power_user]
GO
