CREATE TABLE [dbo].[Import_Map_Disease]
(
[Plaintiff/Defense Disease] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KCIC Mapping] [float] NULL,
[F3] [float] NULL,
[F4] [float] NULL,
[F5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F6] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KCIC Lookup Table] [float] NULL,
[F8] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F9] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_Map_Disease] TO [base_user]
GRANT SELECT ON  [dbo].[Import_Map_Disease] TO [Claims_User]
GO
