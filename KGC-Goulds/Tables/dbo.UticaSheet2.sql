CREATE TABLE [dbo].[UticaSheet2]
(
[Claimant Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceAmt] [money] NULL,
[AmtApproved] [money] NULL,
[UticaAmt] [money] NULL,
[UticaAdjustment] [money] NULL,
[Utica%] [float] NULL,
[GouldsAmt] [money] NULL
) ON [PRIMARY]
GO
