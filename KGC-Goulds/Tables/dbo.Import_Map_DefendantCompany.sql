CREATE TABLE [dbo].[Import_Map_DefendantCompany]
(
[PACESource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KCIC DefendantCompanyID (Mapped)] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_Map_DefendantCompany] TO [base_user]
GRANT SELECT ON  [dbo].[Import_Map_DefendantCompany] TO [Claims_User]
GO
