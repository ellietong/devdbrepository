CREATE TABLE [dbo].[Import_tblClaimMaster_Goulds]
(
[ID] [int] NOT NULL,
[ClaimID] [int] NULL,
[ReferenceID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProducerID] [int] NULL,
[MonthlyBillID] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimSeqNumber] [int] NULL,
[AmountType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[Import_tblClaimMaster_Goulds] TO [base_user]
GO
