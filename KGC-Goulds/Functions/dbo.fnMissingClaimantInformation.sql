SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE FUNCTION [dbo].[fnMissingClaimantInformation]
(@ClaimLawsuitID bigint)

RETURNS NVARCHAR(MAX)
AS
BEGIN

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 25776

DECLARE @employment table (claimantpersonalinfoid int, ExposureLocationEntered int)
insert into @employment 
SELECT ce.ClaimantPersonalInfoID, CASE WHEN max(JobSiteStateID) is NULL  THEN 0  ELSE 1  END AS ExposureLocationEntered
FROM tblClaimantEmployment ce 
INNER JOIN tblClaim c on ce.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl on c.ClaimID = cl.ClaimID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
GROUP BY ce.ClaimantPersonalInfoID 

--SELECT * FROM @employment

--IF object_id('tempdb..@CurrentClaimantInfo') IS NOT NULL
--BEGIN
--   DROP TABLE @CurrentClaimantInfo
--END
DECLARE @CurrentClaimantInfo TABLE  (FileDate bit, 
									ServiceDate bit, 
									Jurisdiction bit, 
									RPLegalCapacity bit,  
									RPRelationship bit, 
									RPaddress bit, 
									RPPhone bit, 
									RPSSN bit)
INSERT INTO @CurrentClaimantInfo
SELECT distinct CASE WHEN l.FileDate IS NOT NULL THEN 1 ELSE 0 END AS FileDate,
				CASE WHEN l.ServiceDate IS NOT NULL THEN 1 ELSE 0 END AS ServiceDate,
				CASE WHEN JurisdictionID IS NOT NULL THEN 1 ELSE 0 END AS Jurisdiction,
				CASE WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' AND DeceasedDate IS NOT NULL AND LegalCapacityID is NULL THEN 0 
					 WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' AND DeceasedDate IS NOT NULL AND LegalCapacityID=14 THEN 0 
					 ELSE 1 END AS RPLegalCapacity,
				CASE WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' AND DeceasedDate IS NOT NULL AND RelationshipID is NULL THEN 0 
					 WHEN relationshipid=6 or RelationshipID is NULL AND crp.firstname IS NOT NULL THEN 0 ELSE 1 END AS RPRelationship,
				CASE WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' AND DeceasedDate IS NOT NULL AND RPAddress1 is NULL AND cpi.MedicareBeneficiaryStatusID in (1,2) THEN 0 ELSE 1 END AS RPaddress,
				CASE WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' AND DeceasedDate IS NOT NULL AND RPPhone is NULL AND cpi.MedicareBeneficiaryStatusID in (1,2)THEN 0 ELSE 1 END AS RPPhone,
				CASE WHEN dbo.fnLatestExposure(cpi.claimantpersonalinfoid) >= '12/5/1980' AND DeceasedDate IS NOT NULL AND RPSocialSecurityNumber is NULL AND cpi.MedicareBeneficiaryStatusID in (1,2) THEN 0 ELSE 1 END AS RPSSN 

FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi	 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl	 ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit l ON l.LawsuitID=cl.LawsuitID
	LEFT JOIN (SELECT * FROM tblClaimantRelatedParty WHERE LegalCapacityID in (1,2,3,4,5,6,7,8,15,17,30,31))crp	 ON crp.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID


--SELECT * FROM @CurrentClaimantInfo

--IF object_id('tempdb..#MissingClaimantInfo') IS NOT NULL
--BEGIN
--   DROP TABLE #MissingClaimantInfo
--END
DECLARE @MissingClaimantInfo  TABLE  (ID int NOT NULL IDENTITY(1,1), MissingClaimantInformation nvarchar(500), Remainder int)
INSERT INTO @MissingClaimantInfo
SELECT 'File Date', NULL
FROM @CurrentClaimantInfo
WHERE FileDate = 0
UNION
SELECT 'Service Date', NULL
FROM @CurrentClaimantInfo
WHERE ServiceDate = 0
UNION
SELECT 'Jurisdiction', NULL
FROM @CurrentClaimantInfo
WHERE Jurisdiction = 0
UNION
SELECT 'Related Party Legal Capacity', NULL
FROM @CurrentClaimantInfo
WHERE RPLegalCapacity = 0
UNION
SELECT 'Related Party Relationship', NULL
FROM @CurrentClaimantInfo
WHERE RPRelationship = 0
UNION
SELECT 'Related Party SSN', NULL
FROM @CurrentClaimantInfo
WHERE RPSSN = 0
UNION
SELECT 'Related Party Address ', NULL
FROM @CurrentClaimantInfo
WHERE RPaddress = 0
UNION
SELECT 'Related Party Phone', NULL
FROM @CurrentClaimantInfo
WHERE RPPhone = 0


	--Calculate the remainder for every-other row highlighting
	UPDATE @MissingClaimantInfo
	SET Remainder = fdp.Remainder 
	FROM (SELECT ID, (ID % 2) AS Remainder FROM @MissingClaimantInfo) fdp
	INNER JOIN @MissingClaimantInfo ofdp ON fdp.ID = ofdp.ID
	WHERE ofdp.ID = fdp.ID

--SELECT *
--FROM @MissingClaimantInfo

	DECLARE @Remainder TABLE (arID int, RowCounter int, Remainder int)
	INSERT INTO @Remainder
	SELECT ID,
		ROW_NUMBER() OVER (ORDER BY ID) AS Row_Counter,  ROW_NUMBER() OVER (ORDER BY ID)% 2 AS Remainder    
	FROM @MissingClaimantInfo

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF; width:200px;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF; width:200px;'


DECLARE @MissingClaimantInfoTable nvarchar(max) = ''

SET @MissingClaimantInfoTable = @MissingClaimantInfoTable + N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0;">' + 
	

N'<tr>
' +
	
	
CAST ( (
SELECT CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleB END AS 'td/@style', (SELECT COALESCE(MissingClaimantInformation, '')) as td, ''
	FROM @MissingClaimantInfo
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	
	--SELECT @MissingClaimantInfoTable
	RETURN @MissingClaimantInfoTable --AS MissingClaimantInfo



END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnMissingClaimantInformation] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnMissingClaimantInformation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnMissingClaimantInformation] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnMissingClaimantInformation] TO [power_user]
GO
