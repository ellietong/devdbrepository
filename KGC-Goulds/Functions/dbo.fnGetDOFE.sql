SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE FUNCTION [dbo].[fnGetDOFE]
(@ClaimantPersonalInfoID int)

RETURNS NVARCHAR(10)
AS
BEGIN

--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 31192

DECLARE @DOFE nvarchar(10)
SET @DOFE = (
	SELECT COALESCE (CONVERT(nvarchar(50), (CASE WHEN CONVERT(nvarchar(50), COALESCE(MIN(empstartdate), MIN(startdate)), 101) IS NULL
											THEN 'UNKNOWN'
											ELSE
												(CASE WHEN COALESCE(MIN(empstartdate), Getdate()) < COALESCE(MIN(startdate), Getdate()) 
													  THEN CONVERT(nvarchar(50),MIN(empstartdate), 101) 
													  ELSE CONVERT(nvarchar(50),MIN(startdate), 101)
												 END)
											END), 101), 'UNKNOWN') AS DOFE
	FROM tblClaimantEmployment ce
	LEFT JOIN tblClaimantProduct cp on ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
)

--SELECT @DOFE
RETURN @DOFE
--PRINT @DOFE

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDOFE] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnGetDOFE] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnGetDOFE] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnGetDOFE] TO [power_user]
GO
