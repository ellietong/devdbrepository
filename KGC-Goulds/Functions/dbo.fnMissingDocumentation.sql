SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



CREATE FUNCTION [dbo].[fnMissingDocumentation]
(@ClaimLawsuitID bigint)

RETURNS NVARCHAR(MAX)
AS
BEGIN

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 170238

DECLARE @CurrentDocumentation TABLE  (Release bit, 
									  UticaPacket bit	
									  )
INSERT INTO @CurrentDocumentation
SELECT DISTINCT CASE WHEN (d.ClaimantPersonalInfoID IS not NULL OR ExecutedReleaseDate IS not NULL) THEN 1 ELSE 0 END AS releasereceived,
				CASE WHEN du.comments like '%utica %' THEN 1 ELSE 0 END AS UticaPacket
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi	 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl	 ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblSettlePaymentDocumentatiON spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT JOIN (SELECT claimantpersonalinfoid 
			   FROM tblMultiClaimantDocument mcd 
				   INNER JOIN tbldocuments d on d.DocumentID = mcd.DocumentID 
			   WHERE ClaimantDocumentTypeID = 31 
			   GROUP BY ClaimantPersonalInfoID) d on d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN (SELECT ClaimantPersonalInfoID, Comments 
			   FROM tblMultiClaimantDocument mcd inner join tbldocuments d ON d.DocumentID = mcd.DocumentID 
			   WHERE ClaimantDocumentTypeID = 10 
			   GROUP BY ClaimantPersonalInfoID, comments) du ON du.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID

WHERE cl.ClaimLawsuitID = @ClaimLawsuitID

--SELECT * FROM @CurrentDocumentation


DECLARE @MissingDocumentation TABLE  (ID int NOT NULL IDENTITY(1,1), MissingDocumentation nvarchar(500), Remainder int)
INSERT INTO @MissingDocumentation
SELECT 'Executed Released', NULL
FROM @CurrentDocumentation
WHERE Release = 0
UNION
SELECT 'Utica Packet', NULL
FROM @CurrentDocumentation
WHERE UticaPacket = 0


	--Calculate the remainder for every-other row highlighting
	UPDATE @MissingDocumentation
	SET Remainder = fdp.Remainder 
	FROM (SELECT ID, (ID % 2) AS Remainder FROM @MissingDocumentation) fdp
	INNER JOIN @MissingDocumentation ofdp ON fdp.ID = ofdp.ID
	WHERE ofdp.ID = fdp.ID

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF; width:200px;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF; width:200px;'

DECLARE @MissingDocumentationTable nvarchar(max) = ''

SET @MissingDocumentationTable = @MissingDocumentationTable + N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0;">' + 
	

N'<tr>
' +
	
	
CAST ( (
SELECT	CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleB END AS 'td/@style', (SELECT COALESCE(MissingDocumentation, '')) as td, ''
	FROM @MissingDocumentation
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	

	--SELECT @MissingDocumentationTable
	RETURN @MissingDocumentationTable 



END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnMissingDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnMissingDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnMissingDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnMissingDocumentation] TO [power_user]
GO
