CREATE ROLE [power_user]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'power_user', N'spuser'
GO
