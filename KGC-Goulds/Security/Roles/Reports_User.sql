CREATE ROLE [Reports_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Reports_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'Reports_User', N'eastlacka'
GO
EXEC sp_addrolemember N'Reports_User', N'kanga'
GO
EXEC sp_addrolemember N'Reports_User', N'khanm'
GO
EXEC sp_addrolemember N'Reports_User', N'labellj'
GO
EXEC sp_addrolemember N'Reports_User', N'prossc'
GO
EXEC sp_addrolemember N'Reports_User', N'shockleym'
GO
