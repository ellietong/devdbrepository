IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'KCICLLC\chaudoinr')
CREATE LOGIN [KCICLLC\chaudoinr] FROM WINDOWS
GO
CREATE USER [chaudoinr] FOR LOGIN [KCICLLC\chaudoinr]
GO
GRANT CREATE PROCEDURE TO [chaudoinr]
GRANT CREATE TABLE TO [chaudoinr]
GRANT CREATE VIEW TO [chaudoinr]
