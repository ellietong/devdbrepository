SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPOLICY_InsuranceProgram]
AS
	SELECT InsuranceProgramID, InsuranceProgram
	FROM tblInsuranceProgram

GO
