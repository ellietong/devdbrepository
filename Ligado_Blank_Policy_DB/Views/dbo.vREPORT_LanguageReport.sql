SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE VIEW [dbo].[vREPORT_LanguageReport]
AS
	SELECT p.PolicyID, p.PolicyNum, prov.ProvisionLabelID, prov.ProvisionValueID, pl.Provision, el.ExactLanguage, p.AmazonKey
	FROM tblPolicy p
	INNER JOIN tblExactLanguage el ON el.PolicyID = p.PolicyID
	INNER JOIN tblProvision prov ON p.PolicyID = prov.PolicyID
	INNER JOIN tblProvisionLabel pl ON prov.ProvisionLabelID = pl.ProvisionLabelID
	INNER JOIN tblProvisionValue pv ON prov.ProvisionValueID = pv.ProvisionValueID AND prov.ProvisionLabelID = pv.ProvisionLabelID


GO
