CREATE TABLE [dbo].[tblDocuments]
(
[DocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[DetailDocumentTypeID] [int] NOT NULL,
[LawsuitID] [bigint] NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[Comments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DocumentTitle] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPDocId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [datetime] NOT NULL,
[SettlementGroupID] [bigint] NULL,
[SettlementDate] [datetime] NULL,
[SettlementAmount] [money] NULL,
[InvoiceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatePayable] [datetime] NULL,
[FeeAmount] [money] NULL,
[DateOfFiling] [datetime] NULL,
[DateOfDeath] [datetime] NULL,
[ModifiedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateModified] [datetime] NOT NULL,
[Accepted] [bit] NULL,
[AcceptedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAccepted] [datetime] NULL,
[DisbursementAmount] [money] NULL,
[Adjustment] [money] NULL,
[AdjustmentNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusID] [int] NULL,
[ApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateApproved] [datetime] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[FirmID] [int] NULL,
[DocumentSourceID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDocuments] ADD CONSTRAINT [PK_mBtblDocuments] PRIMARY KEY CLUSTERED  ([DocumentID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DetailDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DetailDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DetailDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'SettlementGroupID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'SettlementGroupID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'SettlementGroupID'
GO
