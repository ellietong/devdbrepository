CREATE TABLE [dbo].[IMPORT_tblPolicy]
(
[PolicyID] [float] NULL,
[MinPStartDate] [datetime] NULL,
[CarrierID] [float] NULL,
[NamedInsuredID] [float] NULL,
[InsuranceProgramID] [float] NULL,
[PolicyNum] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyNotes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoNotReport] [float] NULL
) ON [PRIMARY]
GO
