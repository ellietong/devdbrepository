CREATE TABLE [dbo].[m_tblSubscriberAvailability]
(
[SubscriberAvailabilityTypeID] [int] NOT NULL IDENTITY(1, 1),
[SubscriberID] [int] NULL,
[AvailabilityTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblSubscriberAvailability] ADD CONSTRAINT [PK__m_tblSub__25621F7E24A84BF8] PRIMARY KEY CLUSTERED  ([SubscriberAvailabilityTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblSubscriberAvailability] TO [base_user]
GRANT ALTER ON  [dbo].[m_tblSubscriberAvailability] TO [fowlerh]
GRANT SELECT ON  [dbo].[m_tblSubscriberAvailability] TO [fowlerh]
GRANT INSERT ON  [dbo].[m_tblSubscriberAvailability] TO [fowlerh]
GRANT DELETE ON  [dbo].[m_tblSubscriberAvailability] TO [fowlerh]
GRANT UPDATE ON  [dbo].[m_tblSubscriberAvailability] TO [fowlerh]
GRANT SELECT ON  [dbo].[m_tblSubscriberAvailability] TO [Policy_User]
GRANT INSERT ON  [dbo].[m_tblSubscriberAvailability] TO [Policy_User]
GRANT DELETE ON  [dbo].[m_tblSubscriberAvailability] TO [Policy_User]
GRANT UPDATE ON  [dbo].[m_tblSubscriberAvailability] TO [Policy_User]
GO
