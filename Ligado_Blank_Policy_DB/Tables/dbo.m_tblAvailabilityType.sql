CREATE TABLE [dbo].[m_tblAvailabilityType]
(
[AvailabilityTypeID] [int] NOT NULL IDENTITY(1, 1),
[AvailabilityType] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblAvailabilityType] ADD CONSTRAINT [PK__m_tblAva__7A4AFFEA20D7BB14] PRIMARY KEY CLUSTERED  ([AvailabilityTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblAvailabilityType] TO [base_user]
GRANT ALTER ON  [dbo].[m_tblAvailabilityType] TO [fowlerh]
GRANT SELECT ON  [dbo].[m_tblAvailabilityType] TO [fowlerh]
GRANT INSERT ON  [dbo].[m_tblAvailabilityType] TO [fowlerh]
GRANT DELETE ON  [dbo].[m_tblAvailabilityType] TO [fowlerh]
GRANT UPDATE ON  [dbo].[m_tblAvailabilityType] TO [fowlerh]
GRANT SELECT ON  [dbo].[m_tblAvailabilityType] TO [Policy_User]
GRANT INSERT ON  [dbo].[m_tblAvailabilityType] TO [Policy_User]
GRANT DELETE ON  [dbo].[m_tblAvailabilityType] TO [Policy_User]
GRANT UPDATE ON  [dbo].[m_tblAvailabilityType] TO [Policy_User]
GO
