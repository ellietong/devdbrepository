CREATE TABLE [dbo].[tblTaskCategoryLookup]
(
[TaskCategoryID] [int] NOT NULL IDENTITY(1, 1),
[TaskCategory] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTaskCategoryLookup] ADD CONSTRAINT [PK_tblTaskCategoryLookup] PRIMARY KEY CLUSTERED  ([TaskCategoryID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblTaskCategoryLookup] TO [base_user]
GRANT SELECT ON  [dbo].[tblTaskCategoryLookup] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblTaskCategoryLookup] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblTaskCategoryLookup] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblTaskCategoryLookup] TO [Notice_User]
GRANT SELECT ON  [dbo].[tblTaskCategoryLookup] TO [power_user]
GRANT INSERT ON  [dbo].[tblTaskCategoryLookup] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTaskCategoryLookup] TO [power_user]
GO
