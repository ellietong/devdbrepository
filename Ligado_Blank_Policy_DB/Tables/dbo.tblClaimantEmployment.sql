CREATE TABLE [dbo].[tblClaimantEmployment]
(
[ClaimantEmploymentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[OccupationID] [int] NULL,
[Jobsite] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmpStartDate] [datetime] NULL,
[EmpEndDate] [datetime] NULL,
[EarliestExposureDate] [datetime] NULL,
[LatestExposureDate] [datetime] NULL,
[IsPrimary] [bit] NULL,
[JobSiteCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobSiteStateID] [int] NULL,
[IsSecondaryExposure] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantEmployment] ADD CONSTRAINT [PK__mBtblCla__F25662D42665ABE1] PRIMARY KEY CLUSTERED  ([ClaimantEmploymentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantEmployment] ADD CONSTRAINT [FK_tblClaimantEmployment_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantEmployment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantEmployment] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantEmploymentID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantPersonalInfoID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantPersonalInfoID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'ClaimantPersonalInfoID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=1125
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Comments'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EarliestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpEndDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpStartDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'EmpStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=4785
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'Jobsite'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'LatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'LatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'LatestExposureDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantEmployment', 'COLUMN', N'OccupationID'
GO
