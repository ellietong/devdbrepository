CREATE TABLE [dbo].[tblPaymentSettlement]
(
[PaymentSettlementID] [bigint] NOT NULL IDENTITY(1, 1),
[CheckID] [bigint] NOT NULL,
[ClaimantSettlementID] [bigint] NULL,
[PaymentAmount] [money] NULL,
[IsFullyPaid] [bit] NULL,
[PaymentSettlementNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPaymentSettlement] ADD CONSTRAINT [PK_tblPaymentSettlement] PRIMARY KEY CLUSTERED  ([PaymentSettlementID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[tblPaymentSettlement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentSettlement] TO [CheckApproval_User]
GRANT SELECT ON  [dbo].[tblPaymentSettlement] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentSettlement] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[tblPaymentSettlement] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentSettlement] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblPaymentSettlement] TO [CheckWriter_User]
GRANT INSERT ON  [dbo].[tblPaymentSettlement] TO [CheckWriter_User]
GRANT UPDATE ON  [dbo].[tblPaymentSettlement] TO [CheckWriter_User]
GO
