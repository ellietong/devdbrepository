CREATE TABLE [dbo].[tblCostExclusion]
(
[CostExcludeTypeID] [int] NOT NULL IDENTITY(1, 1),
[CostExclusionType] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCostExclusion] ADD CONSTRAINT [PK_mEtblCostExclusion] PRIMARY KEY CLUSTERED  ([CostExcludeTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCostExclusion] TO [base_user]
GRANT SELECT ON  [dbo].[tblCostExclusion] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblCostExclusion] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblCostExclusion] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblCostExclusion] TO [Enviro_User]
GRANT SELECT ON  [dbo].[tblCostExclusion] TO [power_user]
GRANT INSERT ON  [dbo].[tblCostExclusion] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCostExclusion] TO [power_user]
GO
