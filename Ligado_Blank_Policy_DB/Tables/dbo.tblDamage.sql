CREATE TABLE [dbo].[tblDamage]
(
[DamageID] [int] NOT NULL IDENTITY(1, 1),
[Damage] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDamage] ADD CONSTRAINT [PK_mBtblDamage] PRIMARY KEY CLUSTERED  ([DamageID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDamage] TO [base_user]
GRANT SELECT ON  [dbo].[tblDamage] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDamage] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDamage] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDamage] TO [power_user]
GRANT INSERT ON  [dbo].[tblDamage] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDamage] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDamage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage', 'COLUMN', N'Damage'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage', 'COLUMN', N'Damage'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage', 'COLUMN', N'Damage'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage', 'COLUMN', N'DamageID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage', 'COLUMN', N'DamageID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDamage', 'COLUMN', N'DamageID'
GO
