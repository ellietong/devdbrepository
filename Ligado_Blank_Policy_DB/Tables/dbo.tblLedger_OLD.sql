CREATE TABLE [dbo].[tblLedger_OLD]
(
[LedgerID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupID] [int] NOT NULL,
[CarrierID] [int] NULL,
[SubscriberID] [int] NULL,
[LedgerTypeID] [int] NOT NULL,
[LedgerDate] [datetime] NOT NULL,
[CheckNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Debit] [money] NOT NULL CONSTRAINT [DF_tblLedger_Debit] DEFAULT ((0)),
[Credit] [money] NOT NULL CONSTRAINT [DF_tblLedger_Credit] DEFAULT ((0)),
[ExhaustionDateID] [int] NULL,
[BillID] [int] NULL,
[LedgerNote] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppliedToBalance] [bit] NULL CONSTRAINT [DF_tblLedger_AssociatedWithBill] DEFAULT ((0))
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLedger_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblLedger_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLedger_OLD] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblLedger_OLD] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblLedger_OLD] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblLedger_OLD] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblLedger_OLD] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblLedger_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblLedger_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblLedger_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLedger_OLD] TO [power_user]
GO
