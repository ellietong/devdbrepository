CREATE TABLE [dbo].[tblContactType]
(
[ContactTypeID] [int] NOT NULL IDENTITY(1, 1),
[ContactType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblContactType] ADD CONSTRAINT [PK_tblContactType] PRIMARY KEY CLUSTERED  ([ContactTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblContactType] TO [base_user]
GRANT SELECT ON  [dbo].[tblContactType] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblContactType] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblContactType] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblContactType] TO [Notice_User]
GRANT SELECT ON  [dbo].[tblContactType] TO [power_user]
GRANT INSERT ON  [dbo].[tblContactType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblContactType] TO [power_user]
GO
