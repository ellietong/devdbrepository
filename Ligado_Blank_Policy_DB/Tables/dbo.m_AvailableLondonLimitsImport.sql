CREATE TABLE [dbo].[m_AvailableLondonLimitsImport]
(
[PolicyDateID] [int] NULL,
[AvailableLimits] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_AvailableLondonLimitsImport] TO [base_user]
GRANT ALTER ON  [dbo].[m_AvailableLondonLimitsImport] TO [fowlerh]
GRANT SELECT ON  [dbo].[m_AvailableLondonLimitsImport] TO [fowlerh]
GRANT INSERT ON  [dbo].[m_AvailableLondonLimitsImport] TO [fowlerh]
GRANT DELETE ON  [dbo].[m_AvailableLondonLimitsImport] TO [fowlerh]
GRANT UPDATE ON  [dbo].[m_AvailableLondonLimitsImport] TO [fowlerh]
GRANT SELECT ON  [dbo].[m_AvailableLondonLimitsImport] TO [Policy_User]
GRANT INSERT ON  [dbo].[m_AvailableLondonLimitsImport] TO [Policy_User]
GRANT DELETE ON  [dbo].[m_AvailableLondonLimitsImport] TO [Policy_User]
GRANT UPDATE ON  [dbo].[m_AvailableLondonLimitsImport] TO [Policy_User]
GRANT SELECT ON  [dbo].[m_AvailableLondonLimitsImport] TO [power_user]
GRANT INSERT ON  [dbo].[m_AvailableLondonLimitsImport] TO [power_user]
GRANT DELETE ON  [dbo].[m_AvailableLondonLimitsImport] TO [power_user]
GRANT UPDATE ON  [dbo].[m_AvailableLondonLimitsImport] TO [power_user]
GO
