CREATE TABLE [dbo].[tblCorporateHistoryProfile]
(
[CorporateHistoryID] [int] NOT NULL IDENTITY(1, 1),
[CorporateHistoryProfile] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorporateHistoryProfile] ADD CONSTRAINT [PK_tblCorporateHistoryProfile] PRIMARY KEY CLUSTERED  ([CorporateHistoryID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCorporateHistoryProfile] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorporateHistoryProfile] TO [CorpHist_User]
GRANT INSERT ON  [dbo].[tblCorporateHistoryProfile] TO [CorpHist_User]
GRANT DELETE ON  [dbo].[tblCorporateHistoryProfile] TO [CorpHist_User]
GRANT UPDATE ON  [dbo].[tblCorporateHistoryProfile] TO [CorpHist_User]
GRANT SELECT ON  [dbo].[tblCorporateHistoryProfile] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblCorporateHistoryProfile] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblCorporateHistoryProfile] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblCorporateHistoryProfile] TO [Enviro_User]
GRANT SELECT ON  [dbo].[tblCorporateHistoryProfile] TO [power_user]
GRANT INSERT ON  [dbo].[tblCorporateHistoryProfile] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCorporateHistoryProfile] TO [power_user]
GO
