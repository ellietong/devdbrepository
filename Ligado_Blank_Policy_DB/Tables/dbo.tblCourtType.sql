CREATE TABLE [dbo].[tblCourtType]
(
[CourtTypeID] [int] NOT NULL IDENTITY(1, 1),
[CourtType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCourtType] ADD CONSTRAINT [PK_mBtblCourtType] PRIMARY KEY CLUSTERED  ([CourtTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCourtType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCourtType] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblCourtType] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblCourtType] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblCourtType] TO [power_user]
GRANT INSERT ON  [dbo].[tblCourtType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCourtType] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType', 'COLUMN', N'CourtType'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType', 'COLUMN', N'CourtType'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType', 'COLUMN', N'CourtType'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblCourtType', 'COLUMN', N'CourtTypeID'
GO
