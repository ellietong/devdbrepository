CREATE TABLE [dbo].[tblDefenseJurisdiction]
(
[DefenseJurisdictionID] [int] NOT NULL IDENTITY(1, 1),
[JurisdictionID] [int] NOT NULL,
[DefenseCounselAddressID] [int] NOT NULL,
[CourtTypeID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[StateID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseJurisdiction] ADD CONSTRAINT [PK_tblDefenseJurisdiction] PRIMARY KEY CLUSTERED  ([DefenseJurisdictionID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseJurisdiction] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseJurisdiction] TO [hagep]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [hagep]
GRANT INSERT ON  [dbo].[tblDefenseJurisdiction] TO [hagep]
GRANT DELETE ON  [dbo].[tblDefenseJurisdiction] TO [hagep]
GRANT UPDATE ON  [dbo].[tblDefenseJurisdiction] TO [hagep]
GO
