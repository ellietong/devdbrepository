CREATE TABLE [dbo].[tblExhaustionType]
(
[ExhaustionTypeID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustionType] ADD CONSTRAINT [PK_tblExhaustionType] PRIMARY KEY CLUSTERED  ([ExhaustionTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblExhaustionType] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustionType] TO [Exhaustion_User]
GRANT INSERT ON  [dbo].[tblExhaustionType] TO [Exhaustion_User]
GRANT DELETE ON  [dbo].[tblExhaustionType] TO [Exhaustion_User]
GRANT UPDATE ON  [dbo].[tblExhaustionType] TO [Exhaustion_User]
GRANT SELECT ON  [dbo].[tblExhaustionType] TO [power_user]
GRANT INSERT ON  [dbo].[tblExhaustionType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExhaustionType] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType', 'COLUMN', N'ExhaustionType'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType', 'COLUMN', N'ExhaustionType'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType', 'COLUMN', N'ExhaustionType'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType', 'COLUMN', N'ExhaustionTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType', 'COLUMN', N'ExhaustionTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType', 'COLUMN', N'ExhaustionTypeID'
GO
