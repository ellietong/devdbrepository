CREATE ROLE [Subscription_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Subscription_User', N'fowlerh'
GO
EXEC sp_addrolemember N'Subscription_User', N'gutzlern'
GO
EXEC sp_addrolemember N'Subscription_User', N'prossc'
GO
