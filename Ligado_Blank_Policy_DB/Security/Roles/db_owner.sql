EXEC sp_addrolemember N'db_owner', N'administrator'
GO
EXEC sp_addrolemember N'db_owner', N'blairo'
GO
EXEC sp_addrolemember N'db_owner', N'crookc'
GO
EXEC sp_addrolemember N'db_owner', N'farmservice'
GO
EXEC sp_addrolemember N'db_owner', N'hankee'
GO
EXEC sp_addrolemember N'db_owner', N'owenss'
GO
EXEC sp_addrolemember N'db_owner', N'scottc'
GO
EXEC sp_addrolemember N'db_owner', N'shockleym'
GO
EXEC sp_addrolemember N'db_owner', N'sochurekn'
GO
EXEC sp_addrolemember N'db_owner', N'spuser'
GO
EXEC sp_addrolemember N'db_owner', N'youngc'
GO
