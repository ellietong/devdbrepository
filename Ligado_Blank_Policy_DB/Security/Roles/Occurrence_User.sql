CREATE ROLE [Occurrence_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Occurrence_User', N'fowlerh'
GO
EXEC sp_addrolemember N'Occurrence_User', N'hankee'
GO
EXEC sp_addrolemember N'Occurrence_User', N'youngc'
GO
