CREATE ROLE [Language_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Language_User', N'brownp'
GO
EXEC sp_addrolemember N'Language_User', N'donzer'
GO
EXEC sp_addrolemember N'Language_User', N'fowlerh'
GO
EXEC sp_addrolemember N'Language_User', N'gutzlern'
GO
EXEC sp_addrolemember N'Language_User', N'heymanm'
GO
EXEC sp_addrolemember N'Language_User', N'kanga'
GO
EXEC sp_addrolemember N'Language_User', N'prossc'
GO
