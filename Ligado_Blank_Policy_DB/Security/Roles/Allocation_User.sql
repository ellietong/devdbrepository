CREATE ROLE [Allocation_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Allocation_User', N'fowlerh'
GO
EXEC sp_addrolemember N'Allocation_User', N'hankee'
GO
EXEC sp_addrolemember N'Allocation_User', N'youngc'
GO
