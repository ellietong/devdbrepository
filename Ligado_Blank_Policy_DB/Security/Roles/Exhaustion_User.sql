CREATE ROLE [Exhaustion_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Exhaustion_User', N'fowlerh'
GO
EXEC sp_addrolemember N'Exhaustion_User', N'hankee'
GO
EXEC sp_addrolemember N'Exhaustion_User', N'youngc'
GO
