CREATE ROLE [Reports_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Reports_User', N'brownp'
GO
EXEC sp_addrolemember N'Reports_User', N'donzer'
GO
EXEC sp_addrolemember N'Reports_User', N'gutzlern'
GO
EXEC sp_addrolemember N'Reports_User', N'heymanm'
GO
EXEC sp_addrolemember N'Reports_User', N'power_user'
GO
