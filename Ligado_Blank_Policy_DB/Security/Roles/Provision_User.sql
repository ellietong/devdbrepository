CREATE ROLE [Provision_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Provision_User', N'brownp'
GO
EXEC sp_addrolemember N'Provision_User', N'donzer'
GO
EXEC sp_addrolemember N'Provision_User', N'fowlerh'
GO
EXEC sp_addrolemember N'Provision_User', N'gutzlern'
GO
EXEC sp_addrolemember N'Provision_User', N'heymanm'
GO
EXEC sp_addrolemember N'Provision_User', N'kanga'
GO
EXEC sp_addrolemember N'Provision_User', N'prossc'
GO
