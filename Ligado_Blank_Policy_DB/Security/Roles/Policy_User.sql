CREATE ROLE [Policy_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Policy_User', N'brownp'
GO
EXEC sp_addrolemember N'Policy_User', N'donzer'
GO
EXEC sp_addrolemember N'Policy_User', N'fowlerh'
GO
EXEC sp_addrolemember N'Policy_User', N'gutzlern'
GO
EXEC sp_addrolemember N'Policy_User', N'heymanm'
GO
EXEC sp_addrolemember N'Policy_User', N'kanga'
GO
EXEC sp_addrolemember N'Policy_User', N'owenss'
GO
EXEC sp_addrolemember N'Policy_User', N'prossc'
GO
EXEC sp_addrolemember N'Policy_User', N'youngc'
GO
