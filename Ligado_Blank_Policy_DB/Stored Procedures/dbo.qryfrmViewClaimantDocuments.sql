SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmViewClaimantDocuments] (@ClaimantPersonalInfoID int)
as

Select ClaimantDocumentID, 
	ClaimantPersonalInfoID,
	case when DateTimeEntered is NULL then ClaimantDocumentType+ ': ' + DocumentTitle else ClaimantDocumentType + ': ' +DocumentTitle+ ': ' + convert(nvarchar,month(DateTimeEntered))+'/'+convert(nvarchar,day(DateTimeEntered))+'/'+convert(nvarchar,Year(DateTimeEntered)) end AS DocumentTypeWithDate, 
	DocumentLink
From tblClaimantDocuments cd 
Inner Join tblClaimantDOcumentType ct on cd.claimantdocumentTypeID = ct.ClaimantDocumentTypeID
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID

UNION

Select DocumentID AS ClaimantDocumentID,
	ClaimantPersonalInfoID,
	case when DateCreated is NULL then ClaimantDocumentType + ': ' + DocumentTitle else
	ClaimantDocumentType + ': ' + DocumentTitle + ': ' + convert(nvarchar,month(DateCreated))+'/'+convert(nvarchar,day(DateCreated))+
	'/'+convert(nvarchar,Year(DateCreated)) end AS DocumentTypeWithDate,
	CASE WHEN SUBSTRING(DocumentURL,8,8) = 'extranet' THEN 'https://admin' + SUBSTRING(DocumentURL,17,LEN(DocumentURL)-15) 
	WHEN SUBSTRING(DocumentURL,9,8) = 'extranet' THEN 'https://admin' + SUBSTRING(DocumentURL,17,LEN(DocumentURL)-15) 
	ELSE DocumentURL END AS DocumentLink
FROM tblDocuments d
Inner Join tblClaimantDocumentType ct on d.DetailDocumentTypeID = ct.ClaimantDocumentTypeID
WHERE DetailDocumentTypeID NOT IN (40,41) AND ClaimantPersonalInfoID = @CLaimantPersonalInfoID 

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewClaimantDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewClaimantDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewClaimantDocuments] TO [power_user]
GO
