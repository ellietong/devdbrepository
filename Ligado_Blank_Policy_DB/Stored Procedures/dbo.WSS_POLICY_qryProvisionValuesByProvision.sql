SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryProvisionValuesByProvision]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT DISTINCT Provision, ProvisionValue
FROM tblProvisionLabel PL
	INNER JOIN tblProvision Pr ON PL.ProvisionLabelID = Pr.ProvisionLabelID
	INNER JOIN tblProvisionValue PV ON Pr.ProvisionValueID = PV.ProvisionValueID
	INNER JOIN tblPolicy P ON Pr.PolicyID = P.PolicyID
GROUP BY Provision, ProvisionValue
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryProvisionValuesByProvision] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryProvisionValuesByProvision] TO [base_user]
GO
