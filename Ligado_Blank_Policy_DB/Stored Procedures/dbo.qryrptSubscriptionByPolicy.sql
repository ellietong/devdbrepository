SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptSubscriptionByPolicy]
	(@LimitTypeID INT = NULL)
AS
SET NOCOUNT ON

--DECLARE @LimitTypeID INT
--SET @LimitTypeID = 1

SELECT P.PolicyID,
		CarrierName,
		PolicyNum,
		MIN(PStartDate) AS PolicyStart, 
		MAX(PEndDate) AS PolicyEnd, 
		AttachmentPoint,
		PerOccLimit,
		LayerPerOccLimit,
		SubscriptionCompany,
		StartDate AS SubStart,
		EndDate AS SubEnd,
		SubscriptionCompanyPercent,
		(SubscriptionCompanyPercent * PerOccLimit) AS SubPerOcc
FROM tblPolicy P
INNER JOIN tblCarrier C ON C.CarrierID = P.CarrierID
INNER JOIN tblPolicyDates PD ON PD.PolicyID = P.PolicyID
INNER JOIN tblPolicyLimits PL ON PL.PolicyID = P.PolicyID
INNER JOIN tblSubscriptionSectionCoverage SSC ON SSC.PolicyID = P.PolicyID
INNER JOIN tblSubscriptionCoverage SC ON SC.SubscriptionSectionCoverageID = SSC.SubscriptionSectionCoverageID
INNER JOIN tblSubscriberCompany SCo ON SCo.SubscriberID = SC.SubscriberID
WHERE LimitTypeID = @LimitTypeID
GROUP BY P.PolicyID,
			CarrierName,
			PolicyNum,
			AttachmentPoint,
			PerOccLimit,
			LayerPerOccLimit,
			SubscriptionCompany,
			StartDate,
			EndDate,
			SubscriptionCompanyPercent,
			(SubscriptionCompanyPercent * PerOccLimit)
ORDER BY PolicyStart, AttachmentPoint, SubStart, SubscriptionCompany


RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionByPolicy] TO [Policy_User]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionByPolicy] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionByPolicy] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionByPolicy] TO [Subscription_User]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionByPolicy] TO [Subscription_User]
GO
