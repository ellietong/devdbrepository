SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryPolicyDetails] 
	-- Add the parameters for the stored procedure here
	(@PolicyID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT DISTINCT P.PolicyID, P.CarrierID, C.CarrierID, C.CarrierGroupID, CarrierGroupName, CarrierName, PolicyNum, P.NamedInsuredID, NI.NamedInsuredID, NamedInsured AS PrimaryNamedInsured, InsuranceProgram, P.AmazonKey, MIN(PStartDate) AS StartDate, MAX(PEndDate) AS EndDate
FROM			tblPolicy P
		INNER JOIN tblCarrier C ON P.CarrierID = C.CarrierID
		LEFT JOIN tblCarrierGroup CG ON C.CarrierGroupID = CG.CarrierGroupID
		INNER JOIN tblNamedInsured NI ON P.NamedInsuredID = NI.NamedInsuredID
		INNER JOIN tblInsuranceProgram IP ON P.InsuranceProgramID = IP.InsuranceProgramID
		INNER JOIN tblPolicyDates PD ON P.PolicyID = PD.PolicyID
WHERE   P.PolicyID = @PolicyID
GROUP BY P.PolicyID, P.CarrierID, C.CarrierID, CarrierName, CarrierGroupName, C.CarrierGroupID, PolicyNum, P.NamedInsuredID, NI.NamedInsuredID, NamedInsured, InsuranceProgram, AmazonKey
ORDER BY PolicyNum, Min(PStartDate), MAX(PEndDate)		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryPolicyDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryPolicyDetails] TO [base_user]
GO
