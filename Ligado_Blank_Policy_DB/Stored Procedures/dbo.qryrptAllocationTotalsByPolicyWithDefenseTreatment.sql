SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptAllocationTotalsByPolicyWithDefenseTreatment] (@allocationid int)

AS

SET NOCOUNT ON

--DECLARE @AllocationID int
--Set @AllocationID = 57


Declare @ExhaustionTYpeID int
Set @exhaustiontypeid = (Select ExhaustionTypeID From tblAllocation Where AllocationID = @allocationID)

Declare @ExhaustionDateID int
Set @ExhaustionDateID = (Select ExhaustionDateID From tblAllocation Where AllocationID = @allocationID)

Declare @LimitTypeID int
Set @LimitTypeID = (Select LimitTypeID From tblAllocation Where AllocationID = @AllocationID)


Declare @Exhaustion TABLE (PolicyDateID int, IndemnityExhaustion money, DefenseExhaustion money, PerOccLess money)
INSERT INTO @Exhaustion

SELECT e.PolicyDateID, SUM(IndemnityAllocation) AS IndemnityExhaustion, SUM(DefenseAllocation) AS DefenseExhaustion,
CASE WHEN (AggregateLimit - SUM(COALESCE(IndemnityAllocation,0) + COALESCE(DefenseAllocation,0))) >= PerOcclimit THEN 0 
	WHEN (AggregateLimit - SUM(COALESCE(IndemnityAllocation,0) + COALESCE(DefenseAllocation,0))) <= 0 THEN PerOcclimit
	ELSE (PerOccLimit - (AggregateLimit - SUM(COALESCE(IndemnityAllocation,0) + COALESCE(DefenseAllocation,0)))) END AS PerOccLess
FROM tblExhaustion e
INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID
INNER JOIN vPolicy p ON e.PolicyDateID = p.PolicyDateID
WHERE e.ExhaustionDateID <= @ExhaustionDateID AND ExhaustionTypeID = @ExhaustionTypeID AND LimitTypeID = @LimitTypeID
GROUP BY e.PolicyDateID, AggregateLimit, PerOccLimit


SELECT @allocationid AS AllocationID, AllocationDescription, 
pd.AnnualPeriod, CarrierName, PolicyNum, pd.PStartDate, pd.PEndDate, 
	pl.AttachmentPoint, pl.PerOccLimit, pl.LayerPerOccLimit, pl.AggregateLimit, pl.SIR,
	'Defense Treatment', Treatment,
	SUM(AllocationIndemnity) AS 'AllocationIndemnity',
	SUM(AllocationDefense) AS 'AllocationDefense',
	SUM(AllocationDefenseOutsideLimits) AS 'AllocationDOL',
	SUM(AllocationDefense + AllocationIndemnity) AS 'TotalAllocatedToLimits',
	SUM(AllocationDefenseOutsideLimits + AllocationDefense + AllocationIndemnity) AS 'TotalAllocatedIncludingDOL',
	CASE WHEN (pl.PerOccLimit - SUM(AllocationDefense + AllocationIndemnity) - COALESCE(PerOccLess,0)) < .01 AND (pl.PerOccLimit - SUM(AllocationDefense + AllocationIndemnity) - COALESCE(PerOccLess,0)) > -.01  THEN 0
		 ELSE (pl.PerOccLimit - SUM(AllocationDefense + AllocationIndemnity) - COALESCE(PerOccLess,0)) END AS 'LimitsRemainingAfterAllocation',
	CASE WHEN (pl.AggregateLimit - SUM(AllocationDefense + AllocationIndemnity) - (COALESCE(e.IndemnityExhaustion,0) + COALESCE(e.IndemnityExhaustion,0))) < .01 
		AND (pl.AggregateLimit - SUM(AllocationDefense + AllocationIndemnity) - (COALESCE(e.IndemnityExhaustion,0) + COALESCE(e.IndemnityExhaustion,0))) > -.01  THEN 0
		 ELSE (pl.AggregateLimit - SUM(AllocationDefense + AllocationIndemnity) - (COALESCE(e.IndemnityExhaustion,0) + COALESCE(e.IndemnityExhaustion,0))) END AS 'AggregateLimitsRemainingAfterAllocation',
	COALESCE(e.IndemnityExhaustion,0) AS IndemnityExhaustion, COALESCE(e.DefenseExhaustion,0) AS DefenseExhaustion, 
	(COALESCE(e.IndemnityExhaustion,0) + COALESCE(e.DefenseExhaustion,0)) AS Exhaustion,
	SUM(SIRAllocatedDefense) AS 'SIRAllocatedDefense',
	SUM(SIRAllocatedIndemnity) AS 'SIRAllocatedIndemnity',
	SUM(ar.SIRAllocated) AS 'SIRAllocated'
FROM tblPolicy p
	INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
	INNER JOIN tblPolicyLimits pl ON p.PolicyID = pl.PolicyID
	INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID
	INNER JOIN tblAllocationResults ar ON pd.PolicyDateID = ar.PolicyDateID
	INNER JOIN tblAllocation a ON ar.AllocationID=a.AllocationID
	Inner Join (Select * From ztblPolicyAllocation Where AllocationID = @AllocationID) z on z.PolicyDateID = pd.PolicyDateID
	Inner Join tblTreatment t ON z.Provision1 = t.TreatmentID 
	LEFT JOIN @Exhaustion e ON pd.PolicyDateID = e.PolicyDateID

WHERE ar.AllocationID = @AllocationID AND pl.LimitTypeID = @LimitTypeID
GROUP BY CarrierName, PolicyNum, pd.PStartDate, pd.PEndDate, 
	pl.AttachmentPoint, pl.PerOccLimit, pl.AggregateLimit, pl.LayerPerOccLimit, pl.AggregateLimit, pl.SIR,
	Treatment, pd.AnnualPeriod, AllocationDescription, e.IndemnityExhaustion, e.DefenseExhaustion, PerOccLess
ORDER BY pd.AnnualPeriod, AttachmentPoint, PerOccLimit
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationTotalsByPolicyWithDefenseTreatment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationTotalsByPolicyWithDefenseTreatment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationTotalsByPolicyWithDefenseTreatment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationTotalsByPolicyWithDefenseTreatment] TO [power_user]
GO
