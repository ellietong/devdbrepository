SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[zQCqryPVRateByAllocation]
(@allocationID int)
As

SET NOCOUNT ON

SELECT o.ExpectedPayDate, ar.PVRate, ar.DateToPresentValue
FROM tblOccurrence o INNER JOIN tblAllocationResults ar ON o.OccurrenceID = ar.OccurrenceID
WHERE ar.AllocationID = @allocationID
GROUP BY o.ExpectedPayDate, ar.PVRate, ar.DateToPresentValue, ar.AllocationID
ORDER BY o.ExpectedPayDate

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[zQCqryPVRateByAllocation] TO [base_user]
GRANT EXECUTE ON  [dbo].[zQCqryPVRateByAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryPVRateByAllocation] TO [power_user]
GRANT EXECUTE ON  [dbo].[zQCqryPVRateByAllocation] TO [power_user]
GO
