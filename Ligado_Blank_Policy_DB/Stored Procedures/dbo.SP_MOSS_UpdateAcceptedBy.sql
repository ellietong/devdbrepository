SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MOSS_UpdateAcceptedBy]
	@SPDocId Varchar(50),
	@AcceptedBy NVarChar(250)
AS
BEGIN
	Update tblDocuments
	Set Accepted = 1,
	AcceptedBy = @AcceptedBy,
	DateAccepted = GetDate()
WHERE SPDocId = @SPDocId
END
GO
