SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryInternalNewComplaintsPast5DayAcceptanceEmail]
AS
BEGIN

/*Query run daily to send an internal email listing any Complaints which are over 5
days old and have not yet been accepted. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(max)

SET @Subject =    'Demo Client: Unaccepted Complaints Over 5 Days Old'
--Set body font, insert KCIC logo/banner at the top of the email, and set table styles. 
--These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>The following firms have unaccepted complaints are over 5 days old: </H4>
<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

--Create temp table to hold all complaints over 5 days old which have not been accepted
--DocTitle and firmAbb are found by parsing the documentURL
DECLARE @NewComplaintsTable TABLE (docTitle nvarchar(50), firmID int, lName nvarchar(50), fname nvarchar(50))
INSERT INTO @NewComplaintsTable
SELECT DocumentTitle,FirmID, LastName, FirstName      
FROM tblDocuments d
INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
WHERE datecreated < GETDATE()-5 
      AND detailDocumentTypeID = 8
      AND Accepted IS NULL
      and PATINDEX('%/dcp/%', DocumentURL) = 0
      AND FirmID NOT IN (37)

--Declare variables to be used in the firm cursor to list documents by firm
DECLARE @firmID nvarchar(10), @firmURL nvarchar(100), @firmname nvarchar(100), @docCount int
SET @firmURL = 'https://admin.kcicllc.com/cp/dcp/Defense%20Counsel%20Documents/Forms/AllItems.aspx'
DECLARE firm_Curs CURSOR FOR

SELECT nct.firmID, f.firm, COUNT(DISTINCT doctitle) 
FROM vtblFirm f
RIGHT JOIN @NewComplaintsTable nct ON nct.firmID = f.firmID
GROUP BY nct.firmID,  f.firm

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @firmID, @firmname, @docCount

WHILE @@FETCH_STATUS = 0

BEGIN
      --Header for each firm with link to the document library
      SET @BodyMessage = @BodyMessage + '<br>'+ CONVERT(nvarchar(5), (SELECT @docCount))+ ' Unaccepted Complaint(s) for <A HREF = ' + (select @firmURL)+'> ' + (select @firmName) +'</A>:'
      --List documents in a table               
      SET @BodyMessage = @bodymessage + 
            N'<table>' + 
            --Set column headings and populate cells from the cursor
            N'<tr><th>Document Title</th><th>Claimant Last Name</th><th>Claimant First Name</th>' +
            CAST ( (
                  SELECT  td = docTitle, '', td = lname, '', td = fname, ''
                  FROM @NewComplaintsTable C
                  Where  C.firmID = (select @firmID)
                  order by lname
                  for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
            N'</table>' +  N'<br/>' 

--Move to next firm                 
FETCH NEXT FROM firm_curs INTO @firmID, @firmname, @doccount
END
CLOSE firm_curs
DEALLOCATE firm_curs

SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person respoDemo Clientble for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'
--Check if the email needs to be sent
IF (SELECT COUNT(DISTINCT docTitle)
FROM @NewComplaintsTable)<>0 

BEGIN
      --Send email
      EXEC msdb.dbo.sp_send_dbmail
      @profile_name='Admin',
      @recipients = 'DemoClient@kcicllc.com',
      @copy_recipients = 'gearyj@kcicllc.com; selfL@kcicllc.com; shockleym@kcicllc.com; johnn@kcicllc.com; montgomeryi@kcicllc.com; dayc@kcicllc.com',
      @subject= @subject,
      @body= @BodyMessage,
      @body_format = 'html'
END

END
GO
