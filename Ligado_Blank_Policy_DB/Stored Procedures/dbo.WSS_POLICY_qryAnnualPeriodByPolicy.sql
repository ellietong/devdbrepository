SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryAnnualPeriodByPolicy]
	-- Add the parameters for the stored procedure here
@PolicyID int	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
DECLARE @AnnualPeriod TABLE (AnnPer varchar(5))
INSERT INTO @AnnualPeriod
SELECT DISTINCT AnnualPeriod
FROM tblSubscriptionSectionCoverage SSC
INNER JOIN tblPolicy P ON SSC.PolicyID = P.PolicyID
INNER JOIN tblPolicyDates PD ON P.PolicyID = PD.PolicyID
WHERE SubscriptionSectionCoverageID IN (SELECT SubscriptionSectionCoverageID FROM tblSubscriptionSectionCoverage
WHERE PolicyID = @PolicyID)

SELECT DISTINCT AnnPer
FROM @AnnualPeriod
UNION
SELECT ''
ORDER BY AnnPer

END
GO
