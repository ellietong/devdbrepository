SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmSettlementGroupInfoEntry]
as
 
Select *
From tblSettlementGroup
Order By SettlementGroup
 
Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlementGroupInfoEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlementGroupInfoEntry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlementGroupInfoEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlementGroupInfoEntry] TO [power_user]
GO
