SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysrptClaimantEmployment]
(@ClaimantPersonalInfoID bigint)

As
Set NoCount On
Select ClaimantEmploymentID, Occupation, Jobsite, Comments, EmpStartDate, EmpEndDate, EarliestExposureDate, LatestExposureDate, IsPrimary
From tblClaimantEmployment CE
Inner Join tblOccupation o on o.OccupationID = CE.OccupationID
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By EmpStartDate

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantEmployment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantEmployment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantEmployment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantEmployment] TO [power_user]
GO
