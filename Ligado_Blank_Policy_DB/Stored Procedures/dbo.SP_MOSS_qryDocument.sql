SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MOSS_qryDocument]
(
	@DocumentID int					--aka ClaimantDocumentTypeID
)
AS
SELECT
	DetailDocumentTypeID,
	ClaimantPersonalInfoID,
	Comments,
	DocumentURL,
	DocumentTitle,
	SPDocId,
	CreatedBy,
	DateCreated,
	InvoiceNumber,
	Period,
	DatePayable,
	ModifiedBy,
	DateModified,
	FeeAmount,
	DisbursementAmount,
    Adjustment,
    AdjustmentNotes,
    StatusID,
    FirmID
FROM tblDocuments
WHERE DocumentID = @DocumentID

RETURN
GO
