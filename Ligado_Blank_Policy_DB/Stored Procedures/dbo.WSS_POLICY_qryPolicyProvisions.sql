SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryPolicyProvisions] 
	-- Add the parameters for the stored procedure here
	(@PolicyID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT Provision, ProvisionValue
FROM tblProvision Pr
INNER JOIN tblProvisionValue PV ON Pr.ProvisionValueID = PV.ProvisionValueID
INNER JOIN tblProvisionLabel PL ON PR.ProvisionLabelID = PL.ProvisionLabelID
WHERE PolicyID = @PolicyID	
ORDER BY Provision
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryPolicyProvisions] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryPolicyProvisions] TO [base_user]
GO
