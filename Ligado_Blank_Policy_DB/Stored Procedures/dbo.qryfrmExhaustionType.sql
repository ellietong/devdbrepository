SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmExhaustionType]
AS

SET NOCOUNT ON

SELECT ExhaustionTypeID, ExhaustionType
FROM tblExhaustionType
ORDER BY ExhaustionTypeID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustionType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustionType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustionType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustionType] TO [power_user]
GO
