SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_MOSS_qryAllClaims]
@ClaimantPersonalInfoID bigint=null
AS
BEGIN
SELECT     C.ClaimID, hddis.HighLevelDisease AS Disease, S.State, J.Jurisdiction, cs.ClaimSequence, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, L.DocketNumber, L.CaseCaption, 
                      L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  pca.PlaintiffCounsel, 
                      r_1.DefenseCounsel AS PrimaryDefenseCounsel,  CPI.ClaimantPersonalInfoID, ClRel.ReleaseSignedDate, rt.ReleaseType, 
                      CL.StatusNotes, SettlementGroupID, ReleaseReceivedDate, PaymentDueDate, PaidDate, CheckNumber
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
                      dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                          (SELECT     dcs.DefenseCounseladdressID, d.DefenseCounsel, dcs.ResponsibleDefenseCounseladdressID, dcs.StateID, 
                                                   r.DefenseCounsel AS ResponsibleDefenseCOunsel, dcs.IsActiveCounsel
                            FROM          dbo.tblDefenseCounselState AS dcs INNER JOIN
											dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
											inner join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
											inner join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID INNER JOIN
                                                   dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
                            GROUP BY d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounseladdressID, dcs.ResponsibleDefenseCounseladdressID, dcs.IsActiveCounsel, dcs.StateID) 
                      AS r_1 ON r_1.DefenseCounseladdressID = L.DefenseCounselAddressID AND r_1.StateID = L.StateID
                      left join (SELECT * from tblClaimDisease where PrimaryDisease = 1) as CD on CD.ClaimID = c.ClaimID
					left join tblDisease ddis on ddis.DiseaseID = cd.DiseaseID
					left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID left outer join
                      dbo.tblClaimLawsuitStatus AS cls ON CL.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblClaimantRelease AS ClRel ON ClRel.ClaimantSettlementID = ClaimSett.ClaimantSettlementID LEFT OUTER JOIN
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = ClRel.ReleaseTypeID
                      left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
WHERE CPI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND cl.IsPrimaryForClaim = 1
END
GO
