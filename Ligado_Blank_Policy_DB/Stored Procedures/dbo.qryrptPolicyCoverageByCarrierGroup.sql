SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  Procedure [dbo].[qryrptPolicyCoverageByCarrierGroup]
(@insuranceProgramID int = NULL, @limitTypeID int = NULL, @carrierGroupID int = NULL, @NamedInsuredID int = NULL,
@startDate int = NULL, @endDate int = NULL)
As

SET NOCOUNT ON

--DECLARE @insuranceProgramID int 
--DECLARE @limitTypeID int
--DECLARE @CarrierGroupID int
--DECLARE @NamedInsuredID int 
--
--SET @insuranceProgramID = 0
--SET @limitTypeID = 0
--SET @CarrierGroupID = 0
--SET @NamedInsuredID = 0

DECLARE @maxInsuranceProgram int
DECLARE @maxLimitTypeID int
DECLARE @maxCarrierGroupID int
DECLARE @maxNamedInsuredID int
DECLARE @maxEndDate int
-- Find max ID values
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxNamedInsuredID = MAX(NamedInsuredID) FROM vPolicy
SELECT @maxEndDate = MAX(annualperiod) FROM vPolicy
IF @carrierGroupID = 0 SET @carrierGroupID = NULL

SELECT CASE WHEN CarrierGroupName IS NULL THEN CarrierName ELSE CarrierGroupName END AS 'CarrierGroupInfo', SUM(PerOccLimit) AS SumOfPerOccLimit, SUM(SIR) AS SumOfSIR, 
	MIN(ActualPStartDate) AS MinOfPStartDate, MAX(ActualPEndDate) AS MaxOfPEndDate, CarrierGroupID, InsuranceProgram, InsuranceProgramID, NamedInsured, LimitTypeID
FROM vPolicy pinfo
WHERE InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND
	LimitTypeID >= COALESCE(@limitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	(coalesce(CarrierGroupID, 0) >= COALESCE(@carrierGroupID, 0) AND coalesce(CarrierGroupID, 0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(CarrierGroupID, 0) = @carrierGroupID) AND
	NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsuredID)
	and DoNotReport<>1
	AND AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
GROUP BY CASE WHEN CarrierGroupName IS NULL THEN CarrierName ELSE CarrierGroupName END, CarrierGroupID, InsuranceProgram, InsuranceProgramID, NamedInsured, LimitTypeID

RETURN

/* -- Single limit
SELECT 
--IIf(IsNull([carriergroupname]),[carriername],[carriergroupname]) AS CarrierGroupInfo, 
	CASE
	WHEN CarrierGroupName is null THEN [CarrierName]
	ELSE [CarrierGroupName]
	END as 'CarrierGroupInfo',
Sum(tblPolicy.PerOccLimit) AS SumOfPerOccLimit, Sum(tblPolicy.SIR) AS SumOfSIR, Min(tblPolicyDates.ActualPStartDate) AS MinOfPStartDate, 
Max(tblPolicyDates.ActualPEndDate) AS MaxOfPEndDate, tblCarrierGroup.CarrierGroupID, tblInsuranceProgram.InsuranceProgram, tblInsuranceProgram.InsuranceProgramID
FROM (tblInsuranceProgram INNER JOIN (tblCarrierGroup RIGHT JOIN (tblCarrier INNER JOIN tblPolicy ON tblCarrier.CarrierID = tblPolicy.CarrierID) 
ON tblCarrierGroup.CarrierGroupID = tblCarrier.CarrierGroupID) ON tblInsuranceProgram.InsuranceProgramID = tblPolicy.InsuranceProgramID) LEFT JOIN tblPolicyDates 
ON tblPolicy.PolicyID = tblPolicyDates.PolicyID
GROUP BY 
--IIf(IsNull([carriergroupname]),[carriername],[carriergroupname]), 
	CASE
	WHEN CarrierGroupName is null THEN [CarrierName]
	ELSE [CarrierGroupName]
	END,
tblCarrierGroup.CarrierGroupID, tblInsuranceProgram.InsuranceProgram, tblInsuranceProgram.InsuranceProgramID;
*/


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyCoverageByCarrierGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyCoverageByCarrierGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyCoverageByCarrierGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyCoverageByCarrierGroup] TO [power_user]
GO
