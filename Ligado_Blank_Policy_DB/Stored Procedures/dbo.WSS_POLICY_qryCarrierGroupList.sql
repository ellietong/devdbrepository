SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryCarrierGroupList]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT C.CarrierGroupID, CarrierGroupName 
FROM tblPolicy p 
INNER JOIN tblCarrier C ON P.CarrierID = C.CarrierID 
INNER JOIN tblCarrierGroup CG ON C.CarrierGroupID = CG.CarrierGroupID 
GROUP BY C.CarrierGroupID, CarrierGroupName 
UNION
SELECT NULL, ''
ORDER BY CarrierGroupName
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryCarrierGroupList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryCarrierGroupList] TO [base_user]
GO
