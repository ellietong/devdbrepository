SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryPoliciesByProvisionValueByProvision]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT Provision, ProvisionValue, Pr.PolicyID
FROM tblProvisionLabel PL
	INNER JOIN tblProvision Pr ON PL.ProvisionLabelID = Pr.ProvisionLabelID
	INNER JOIN tblProvisionValue PV ON Pr.ProvisionValueID = PV.ProvisionValueID
	INNER JOIN tblPolicy P ON Pr.PolicyID = P.PolicyID
WHERE InsuranceProgramID <> 3	
GROUP BY Provision, ProvisionValue, Pr.PolicyID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryPoliciesByProvisionValueByProvision] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryPoliciesByProvisionValueByProvision] TO [base_user]
GO
