SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_POLICY_qryFollowsFormTab]
    (
      @PolicyDateID INT ,
      @ProvisionLabelID INT 
    ----declare @PolicyDateID INT
    ----declare @ProvisionLabelID INT 
    
    ----set @PolicyDateID = 1043
    ----set @ProvisionLabelID =  1
        
    )
AS 
    SET NOCOUNT ON;

    SELECT  p.PolicyID ,
            PolicyNum ,
            Provision ,
            fff.PolicyDateID ,
            CarrierName ,
            ProvisionValue ,
            AttachmentPoint ,
            PerOccLimit ,
            LayerPerOccLimit AS LayerLimit,
            Convert(varchar,CONVERT(DATE,pdf.PStartDate),120) + ' To ' + Convert(varchar,CONVERT(DATE,pdf.PEndDate),120) as PolicyPeriod
    FROM    tblPolicy p
            INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
            INNER JOIN tblPolicyLimits pl ON p.PolicyID = pl.PolicyID
            INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID
            LEFT JOIN tblProvision pr ON p.PolicyID = pr.PolicyID
            LEFT  JOIN tblProvisionLabel plbl ON pr.ProvisionLabelID = plbl.ProvisionLabelID
            LEFT  JOIN tblProvisionValue pvalz ON pvalz.ProvisionValueID = pr.ProvisionValueID
			INNER JOIN dbo.tblFollowsForm fff ON fff.FFPolicyDateID = pd.PolicyDateID
			LEFT JOIN tblPolicyDates pdf on pdf.PolicyDateID = fff.FFPolicyDateID
    WHERE   fff.PolicyDateID = @PolicyDateID
            AND plbl.ProvisionLabelID = @ProvisionLabelID
    GROUP BY p.PolicyID ,
            PolicyNum ,
            Provision ,
            fff.PolicyDateID ,
            p.PolicyID ,
            PolicyNum ,
            Provision ,
            fff.PolicyDateID ,
            CarrierName ,
            ProvisionValue ,
            AttachmentPoint ,
            PerOccLimit ,
            LayerPerOccLimit,
            Convert(varchar,CONVERT(DATE,pdf.PStartDate),120) + ' To ' + Convert(varchar,CONVERT(DATE,pdf.PEndDate),120),
            Layer
            order by Convert(varchar,CONVERT(DATE,pdf.PStartDate),120) + ' To ' + Convert(varchar,CONVERT(DATE,pdf.PEndDate),120), Layer
            
        
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryFollowsFormTab] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryFollowsFormTab] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryFollowsFormTab] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryFollowsFormTab] TO [power_user]
GO
