SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE procedure [dbo].[qryfrmCopyOccurrence] (@occurrenceID int) 
AS

SET NOCOUNT ON

--DECLARE @occurrenceID int
--SET @occurrenceID = 22

--Copy Occurrence Record
INSERT INTO tblOccurrence (AggregateApplies,
						   ExpectedPayDate,
						   InClaim,
						   OccurrenceName,
						   OccurrenceAddress,
						   OccurrenceCity,
						   OccurrenceStateID,
						   OccurrenceGroupID,
						   CorporateHistoryID,
						   OccurrenceTypeID,
						   TriggerStart1,
						   TriggerStart1Explanation,
						   TriggerEnd1,
						   TriggerEnd1Explanation,
						   TriggerStart2,
						   TriggerEnd2,
						   TriggerStart3,
						   TriggerEnd3,
						   TriggerStart4,
						   TriggerEnd4,
						   Cost1,
						   Cost1Explanation,
						   Cost2,
						   Cost2Explanation,
						   Cost3,
						   Cost3Explanation,
						   Cost4,
						   Cost5,
						   Cost6,
						   Cost7,
						   Cost8,
						   Cost9,
						   Cost10,
						   Cost11,
						   Cost12,
						   OccurrenceNote)
	SELECT AggregateApplies,
		   ExpectedPayDate,
		   InClaim,
		   'Copy of ' + OccurrenceName,
		   OccurrenceAddress,
		   OccurrenceCity,
		   OccurrenceStateID,
		   OccurrenceGroupID,
		   CorporateHistoryID,
		   OccurrenceTypeID,
		   TriggerStart1,
		   TriggerStart1Explanation,
		   TriggerEnd1,
		   TriggerEnd1Explanation,
		   TriggerStart2,
		   TriggerEnd2,
		   TriggerStart3,
		   TriggerEnd3,
		   TriggerStart4,
		   TriggerEnd4,
		   Cost1,
		   Cost1Explanation,
		   Cost2,
		   Cost2Explanation,
		   Cost3,
		   Cost3Explanation,
		   Cost4,
		   Cost5,
		   Cost6,
		   Cost7,
		   Cost8,
		   Cost9,
		   Cost10,
		   Cost11,
		   Cost12,
		   OccurrenceNote
	FROM tblOccurrence
	WHERE OccurrenceID = @occurrenceID
	
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyOccurrence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyOccurrence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyOccurrence] TO [Occurrence_User]
GRANT EXECUTE ON  [dbo].[qryfrmCopyOccurrence] TO [Occurrence_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyOccurrence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyOccurrence] TO [power_user]
GO
