SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_POLICY_qryFollowsFormLanguage]
    (
      @PolicyID INT 
     
    )
AS 
    SET NOCOUNT ON;

    SELECT  p.PolicyID ,
            ProvisionValue
            
        
    FROM    tblPolicy p
			INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID
			Inner join tblProvisionValue pv on pv.ProvisionValueID = pr.ProvisionValueID
			
	
    WHERE   pr.PolicyID = @PolicyID and pr.ProvisionLabelID = 15
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryFollowsFormLanguage] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryFollowsFormLanguage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryFollowsFormLanguage] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryFollowsFormLanguage] TO [power_user]
GO
