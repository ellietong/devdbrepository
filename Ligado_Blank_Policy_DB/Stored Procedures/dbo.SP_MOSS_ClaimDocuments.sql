SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[SP_MOSS_ClaimDocuments]
AS
SELECT     cl.ClaimID, d.DocumentID, cdt.ClaimantDocumentType, d.LawsuitID, d.ClaimantPersonalInfoID, d.DocumentURL, d.SPDocId, d.DateCreated, 
                      d.AcceptedBy, d.DateAccepted
FROM         dbo.tblClaimLawsuit AS cl INNER JOIN
                      dbo.tblDocuments AS d ON cl.LawsuitID = d.LawsuitID INNER JOIN
                      dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID



GO
