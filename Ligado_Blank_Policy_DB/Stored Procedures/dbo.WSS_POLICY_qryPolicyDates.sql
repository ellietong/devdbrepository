SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryPolicyDates]
	-- Add the parameters for the stored procedure here
(@policyID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT *
FROM tblPolicyDates
WHERE PolicyID = @policyID
ORDER BY PStartDate
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryPolicyDates] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryPolicyDates] TO [base_user]
GO
