SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryProvisionsPerPolicyList]
	-- Add the parameters for the stored procedure here
	(@PolicyID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
SELECT DISTINCT P.ProvisionLabelID, Provision
FROM tblProvision P
INNER JOIN tblProvisionLabel PL ON P.ProvisionLabelID = PL.ProvisionLabelID
WHERE PolicyID = @PolicyID
UNION 
SELECT NULL, '<<All Provisions>>'
ORDER BY Provision

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryProvisionsPerPolicyList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryProvisionsPerPolicyList] TO [base_user]
GO
