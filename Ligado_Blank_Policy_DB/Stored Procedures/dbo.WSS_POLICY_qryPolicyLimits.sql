SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryPolicyLimits]
	-- Add the parameters for the stored procedure here
	(@PolicyID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT LimitID, PolicyID, PL.LimitTypeID, LimitType, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
SIR, SIRAggregateLimit, AggregateLimit, PreviouslyExhaustedAggregate, Layer
FROM tblPolicyLimits PL
INNER JOIN tblPolicyLimitType PLT ON PL.LimitTypeID = PLT.LimitTypeID
WHERE PolicyID = @PolicyID
GROUP BY LimitID, PolicyID, PL.LimitTypeID, LimitType, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
SIR, SIRAggregateLimit, AggregateLimit, PreviouslyExhaustedAggregate, Layer
ORDER BY PolicyID, LimitTypeID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryPolicyLimits] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryPolicyLimits] TO [base_user]
GO
