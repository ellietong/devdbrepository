SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryFollowsFormTabYears]
(@PolicyID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT      p.PolicyID ,
                PolicyNum ,
                Convert(varchar,CONVERT(DATE,pd.PStartDate),120) + ' To ' + Convert(varchar,CONVERT(DATE,pd.PEndDate),120) as PolicyYears,
                pd.PolicyDateID
        FROM    tblPolicy p
                INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID
	WHERE p.PolicyID = @PolicyID
	Order by Pd.PStartDate
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryFollowsFormTabYears] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryFollowsFormTabYears] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryFollowsFormTabYears] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryFollowsFormTabYears] TO [power_user]
GO
