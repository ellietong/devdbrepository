SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qrySubscriptionSectionByPolicy]
	-- Add the parameters for the stored procedure here
	@PolicyID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT DISTINCT SSC.SubscriptionSectionID, SubscriptionSection 
FROM tblSubscriptionSectionCoverage SSC
INNER JOIN tblSubscriptionSection SS ON SSC.SubscriptionSectionID = SS.SubscriptionSectionID
WHERE PolicyID = @PolicyID
UNION 
SELECT NULL, ''
ORDER BY SubscriptionSection


END
GO
