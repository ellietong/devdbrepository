SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MOSS_qryClaimantSearch] (@ClaimantName nvarchar(max), @Birthdate nvarchar(20), @SSN nvarchar(20))
AS
--DECLARE @ClaimantName nvarchar(max)
--SET @ClaimantName = 'tkal'
--DECLARE @Birthdate nvarchar(20)
--SET @Birthdate = null
--DECLARE @SSN nvarchar(20)
--SET @SSN = null
SELECT DISTINCT 
                      CPI.ClaimantPersonalInfoID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END + '- (' + COALESCE(DocketNumbers, 'N/A') + ')' AS ClaimantName, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS SSNSearch,
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
                      CONVERT(VARCHAR(10), CPI.BirthDate, 101) AS BirthDate
                        
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
                      LEFT JOIN tblClaimantDocketNumbers cdn ON CPI.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
WHERE (CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
       THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' end) like '%'+coalesce(@ClaimantName, '')+'%'
       AND coalesce(CONVERT(VARCHAR(10), CPI.BirthDate, 101), '') like '%'+ coalesce(@BirthDate, '') +'%'
       AND Coalesce(socialsecurityNumber, '') like '%'+coalesce(@SSN, '')+'%'
		OR LastName LIKE '%NOT FOUND%'
ORDER BY ClaimantName
GO
