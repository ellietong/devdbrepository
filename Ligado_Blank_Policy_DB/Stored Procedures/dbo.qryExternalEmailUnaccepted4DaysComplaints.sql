SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailUnaccepted4DaysComplaints]
AS
BEGIN

/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a complaint is over 4 days old and has not yet been accepted. - BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @Header nvarchar(2000)

SET @Subject =    'Demo Client: Unaccepted Complaints Over 4 Days Old'
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.
SET @Header = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>The following complaints are over 4 days old and have not yet been accepted: </H4>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (docTitle nvarchar(50), firmID int, lName nvarchar(50), 
      fName nvarchar(50))
INSERT INTO @NewComplaintsTable
SELECT DocumentTitle,FirmID, LastName, FirstName
FROM tblDocuments d
INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
WHERE datecreated < GETDATE()-4 
      AND detailDocumentTypeID IN (8,4)
      AND Accepted IS NULL
      and PATINDEX('%/dcp/%', DocumentURL) = 0
      and d.FirmID NOT IN (37)
--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.
DECLARE @firmID nvarchar(10), @firmURL nvarchar(100)

DECLARE firm_Curs CURSOR FOR

SELECT nct.firmID, 
      --Heyl Royster only has one record in tblFirm, but two document libraries for Edwardsville and Peoria.
      --This case statement modifies the URL from tblFirm to reference the correct document library
      'https://extranet.kcicllc.com/cp/dcp/Defense%20Counsel%20Documents/Forms/AllItems.aspx' AS FirmURL
FROM @NewComplaintsTable nct
      LEFT JOIN vtblFirm f on nct.firmID = f.firmID
GROUP BY nct.firmID

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @firmID, @firmurl
WHILE @@FETCH_STATUS = 0
BEGIN

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @Header + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH    {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

--Declare a cursor and variables to be used in the cursor to create a separate table of complaints for each state
--DECLARE @NumberPerState int
--DECLARE @State nVarChar(20)

--DECLARE stateDocs_Curs CURSOR FOR
            
--SELECT COUNT(DISTINCT doctitle), [state] 
--FROM @NewComplaintsTable 
--WHERE firmAbb = (SELECT @firmID)
--GROUP BY [state]
--ORDER BY [state]

--OPEN stateDocs_curs 

--FETCH NEXT FROM stateDocs_curs INTO @NumberPerState, @State

--WHILE @@fetch_status = 0
--BEGIN
--Header line for each state's table
      SET @BodyMessage = @BodyMessage + '<br> Complaint(s):'            
                              
      --Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
      SET @BodyMessage = @BodyMessage + N'<table>' + 
            N'<tr><th>Document Title</th><th>Claimant Last Name</th><th>Claimant First Name</th>' +
            CAST ( (
                  SELECT  td = docTitle, '', td = lName, '', td = fname, ''
                  FROM @NewComplaintsTable C
                  WHERE C.firmID = (select @firmID)
                  ORDER BY lname
            for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
            N'</table>' +  N'<br/>'       

--Repeat for next state
--FETCH NEXT FROM stateDocs_curs INTO @NumberPerState, @State
--END             
--CLOSE stateDocs_curs
--DEALLOCATE stateDocs_curs

--Link for document library
SET @BodyMessage = @BodyMessage + '<br>Click <A Href = ' + (SELECT @firmURL) + '> here</A> to access these documents.<br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person respoDemo Clientble for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

  
DECLARE @RecipientList nvarchar(500)
SET @RecipientList=''

--Due to special situations with Heyl Royster and Hawkins and Parnell, the email list is hard coded for those firms.
--IF (SELECT @firmID) = 'hr' 
--BEGIN
--    SET @RecipientList = 'enewfiles@heylroyster.com'
--END
--ELSE IF (SELECT @firmID) = 'hrp'
--BEGIN
--    SET @RecipientList = 'peonewfiles@heylroyster.com'
--END
IF (SELECT @firmID) = 18
BEGIN
      SET @RecipientList = ''--'dmarshall@hplegal.com; sking@hplegal.com; tvlahinic@hplegal.com; wwood@hplegal.com'
END
ELSE IF (SELECT @firmID) = 19 
BEGIN
      SET @RecipientList = ''--'sking@hplegal.com; tvlahinic@hplegal.com; wwood@hplegal.com; hberner@hplegal.com'
END
ELSE
--For all other firms, use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar(500)
DECLARE contact_curs CURSOR FOR

SELECT email 
FROM vtblPerson p
WHERE FirmID = (SELECT @firmID)
      AND complaintContact = 1
      
OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS=0
BEGIN 

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The Demo ClientProfile sends from Demo Client@kcicllc.com
@profile_name='DemoClientProfile',
@recipients = @RecipientList,
@blind_copy_recipients = 'DemoClient@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

--Move to the next firm to create a new email
FETCH NEXT FROM firm_curs INTO @firmID, @firmurl
END
CLOSE firm_curs
DEALLOCATE firm_curs

END
GO
