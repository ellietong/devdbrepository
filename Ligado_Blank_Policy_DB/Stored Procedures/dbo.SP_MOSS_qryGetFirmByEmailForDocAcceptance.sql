SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[SP_MOSS_qryGetFirmByEmailForDocAcceptance]
	(@emailaddress nvarchar(1000))

AS

BEGIN
	-- Declare the temporary fields used to track firm and email domain
	DECLARE @firmID int
	DECLARE @emaildomain nvarchar(1000)
	
	--Parse out the @emaildomain from the @emailaddress
	SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress), LEN(@emailaddress)-CHARINDEX('@', @emailaddress)+1)
	
	--Set @firmID based on the @emaildomain
	IF @emaildomain = '@kcicllc.com' BEGIN
		SET @firmID = 0 
	END
	ELSE BEGIN
		SET @firmID = (SELECT DISTINCT FirmID FROM vtblPerson INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID WHERE @emailaddress = vtblPerson.Email)
	END
	
	--Return the @firmID
	SELECT @firmID AS FirmID
END
GO
