SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[m_qrysrptFollowsFormLanguage] (@PolicyID int)

AS

Set NoCount On

--DECLARE @PolicyID int

--SET @PolicyID = 127

DECLARE @ProvisionLabelID int
SET @ProvisionLabelID = 15

DECLARE @maxProvisionLablelID int
SELECT @maxProvisionLablelID = MAX(ProvisionLabelID) FROM tblProvisionLabel

--------------------------------------------------------------
/*Provisions And Language By Policy*/
--------------------------------------------------------------
SELECT p.PolicyID, pv.ProvisionLabelID, Provision, pv.ProvisionValueID, ProvisionValue, ExactLanguage,
      BatesStart, BatesEnd, Location, DetailedLocation AS LangType, EndorsementNum, FormNumber, Sort
FROM tblPolicy p
INNER JOIN tblProvision pv ON p.PolicyID = pv.PolicyID
INNER JOIN tblProvisionValue pvv ON pv.ProvisionValueID = pvv.ProvisionValueID
INNER JOIN tblProvisionLabel pvl ON pv.ProvisionLabelID = pvl.ProvisionLabelID
INNER JOIN tblExactLanguage el on el.PolicyID=p.PolicyID 
INNER JOIN tblProvisionLanguage plang ON plang.ExactLanguageID=el.ExactLanguageID AND pv.ProvisionLabelID = plang.ProvisionLabelID
LEFT JOIN tblLocation loc ON el.LocationID = loc.LocationID
LEFT JOIN tblDetailedLocation lt ON el.DetailedLocationID = lt.DetailedLocationID
LEFT JOIN tblForms f on f.FormID = el.FormID
WHERE pv.ProvisionLabelID >= COALESCE(@ProvisionLabelID, 0) AND pv.ProvisionLabelID <= COALESCE(NULLIF(@ProvisionLabelID, 0), @maxProvisionLablelID)
AND p.PolicyID = @PolicyID
GROUP BY p.PolicyID, pv.ProvisionLabelID, Provision, pv.ProvisionValueID, ProvisionValue, ExactLanguage,
BatesStart, BatesEnd, Location, DetailedLocation, EndorsementNum, FormNumber, Sort
GO
GRANT VIEW DEFINITION ON  [dbo].[m_qrysrptFollowsFormLanguage] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qrysrptFollowsFormLanguage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_qrysrptFollowsFormLanguage] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_qrysrptFollowsFormLanguage] TO [power_user]
GO
