SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmCheckApproval] (@CheckID int, @CheckTotal money, @MatterID int)
As
Set NoCount On

Insert Into tblCheckApproval (CheckID, ApprovedBy, ApprovedDate, IsApproved)
Select @CheckID, System_User, GETDATE(), 1 As IsApproved

Update tblCheck
Set SourceAmount = @CheckTotal
Where CheckID = @CheckID

Update tblCheck
Set SourceNumber = (Select MAX(Coalesce(SourceNumber,0))+1
From tblCheck c
Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
Where MatterID = @MatterID AND FundingSourceID = (select FundingSourceID from tblCheck where CheckID = @CheckID))
Where CheckID = @CheckID

---Invoices---
Declare @FullyPaidInvoices table (DefenseInvoiceID int)
insert Into @FullyPaidInvoices
Select di.DefenseInvoiceID
From tblDefenseInvoice di
Inner Join tblPaymentDefense pd on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join tblCheckApproval ca on ca.CheckID = pd.CheckID
Where IsFullyPaid <> 1 or IsFullyPaid is null
Group By InvoiceTotal, di.DefenseInvoiceID
Having SUM(PaymentAmount) = InvoiceTotal

Update tblPaymentDefense
Set IsFullyPaid = 1
Where DefenseInvoiceID in (Select DefenseInvoiceID From @FullyPaidInvoices)

---Settlements---
Declare @FullyPaidSettlements table (ClaimantSettlementID int)
insert Into @FullyPaidSettlements
Select cs.ClaimantSettlementID
From tblClaimantSettlement cs
Inner Join tblPaymentSettlement pd on cs.ClaimantSettlementID = pd.ClaimantSettlementID
Inner Join tblCheckApproval ca on ca.CheckID = pd.CheckID
Where IsFullyPaid <> 1 or IsFullyPaid is null
Group By SettlementAmount, cs.ClaimantSettlementID
Having SUM(PaymentAmount) = SettlementAmount

Update tblPaymentSettlement
Set IsFullyPaid = 1
Where ClaimantSettlementID in (Select ClaimantSettlementID From @FullyPaidSettlements)

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckApproval] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheckApproval] TO [CheckApproval_User]
GO
