SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--adds m_BinderNumber
CREATE Procedure [dbo].[m_qrysrptFollowsForm_WithTabNumbers] (@PolicyDateID int)
As
set nocount on


--DECLARE @PolicyDateID int
--SET @PolicyDateID = 187

Declare @FFTreatment table (PolicyDateID int, ProvisionValue nvarchar(100))
Insert Into @FFTreatment 
Select pd.PolicyDateID, ProvisionValue
From tblProvision prov Inner Join tblProvisionValue provv on provv.ProvisionValueID = prov.ProvisionValueID 
	Inner Join tblPolicy p on p.PolicyID = prov.PolicyID 
	Inner Join tblPolicyDates pd on pd.PolicyID = p.PolicyID 
Where pd.PolicyDateID = @PolicyDateID 
	and prov.ProvisionLabelID = 15 
Group By pd.PolicyDateID, ProvisionValue



SELECT ff.PolicyDateID,
'Follows Form Treatment - ' + fft.ProvisionValue as FollowsFormTreatment,
0 as m_TabNumber,
ff.FFPolicyDateID, 
p.PolicyID,
CarrierName,
PolicyNum,  
ActualPStartDate, 
ActualPEndDate, 
AnnualPeriod,  
AttachmentPoint, 
PerOccLimit, 
LayerPerOccLimit,
Layer,
0 as m_BinderNumber, 
'' as NoActionClause
FROM tblFollowsForm ff Inner Join tblPolicyDates pd on pd.PolicyDateID = ff.FFPolicyDateID 
        Inner Join tblPolicy p on p.PolicyID = pd.PolicyID 
      Inner Join tblPolicyLimits pl on pl.PolicyID = p.PolicyID 
      Inner Join tblCarrier c on c.CarrierID = p.CarrierID
      Inner Join @FFTreatment fft on fft.PolicyDateID = ff.PolicyDateID
     -- Inner Join (Select PolicyID, ProvisionValue 
					--From tblProvision prov 
					--	Inner Join tblProvisionValue provv on prov.ProvisionValueID = provv.ProvisionValueID
					--Where prov.ProvisionLabelID = 31) a on a.PolicyID = p.PolicyID       
Where LimitTypeID = 1 AND @PolicyDateID = ff.PolicyDateID
GROUP BY ff.PolicyDateID,
ff.FFPolicyDateID, 
p.PolicyID,
CarrierName,
PolicyNum,  
ActualPStartDate, 
ActualPEndDate, 
AnnualPeriod,  
AttachmentPoint, 
PerOccLimit, 
LayerPerOccLimit,
Layer,
fft.ProvisionValue
ORDER BY FF.PolicyDateID, 
      AttachmentPoint DESC

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[m_qrysrptFollowsForm_WithTabNumbers] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qrysrptFollowsForm_WithTabNumbers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_qrysrptFollowsForm_WithTabNumbers] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_qrysrptFollowsForm_WithTabNumbers] TO [power_user]
GO
