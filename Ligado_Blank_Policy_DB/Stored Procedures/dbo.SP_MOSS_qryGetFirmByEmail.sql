SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MOSS_qryGetFirmByEmail]
	-- Add the parameters for the stored procedure here
	@emailaddress nvarchar(1000)
AS
BEGIN
	DECLARE @emaildomain nvarchar(1000)
	SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress), LEN(@emailaddress)-CHARINDEX('@', @emailaddress)+1)
	IF @emaildomain = '@kcicllc.com'
	BEGIN
		SELECT DISTINCT FirmID, DefenseCounsel as Firm FROM vtblPerson INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
	END
	ELSE
	BEGIN
		SELECT DISTINCT FirmID, DefenseCounsel as Firm FROM vtblPerson INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
		WHERE @emailaddress = vtblPerson.Email
	END
		
	 
END
GO
