SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptCashFlowSummary]

As

SET NOCOUNT ON

SELECT	Year([Date]) as [Year], Sum(Indemnity) as Indemnity, Sum(DefenseCOunsel) as DefenseCounsel, Sum(NonBillable_DefenseCOunsel) as NonBillableDefenseCounsel, Sum(MiscFees) as MiscFees,
	Sum(Shortfall) as Shortfall, Sum(Bundled) as Bundled, Sum(InvestmentInterest) as InvestmentInterest, Sum(UnpostedAIG) as UnpostedAIG, Sum(UnpostedNSISelf) as UnpostedNSISelf, Sum(UnpostedCashReceipts) as UnpostedCashReceipts,
	Sum(Total) as Total
FROM	tblCashFlow CF
Group By Year([Date])	
Order By Year([Date])

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashFlowSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashFlowSummary] TO [power_user]
GO
