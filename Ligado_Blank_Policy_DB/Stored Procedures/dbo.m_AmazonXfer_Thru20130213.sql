SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_AmazonXfer_Thru20130213]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT p.PolicyID AS DocumentID, 
			SPLink AS DocumentURL, 
			REPLACE(REPLACE(p.PolicyNum, '\', ''), '/', '') AS DocumentTitle,
			'Policy' AS ClaimantDocumentType,
			0 AS ClaimantDocumentTypeID,
			 AmazonKey
    FROM tblPolicy p
   
  
    WHERE AmazonKey IS NULL 
	AND SPLink IS NOT NULL
    ORDER BY PolicyID ASC
  END
  
--SELECT * FROM tblDocuments WHERE DocumentURL = '/archive/iuna_nosroc/Historical Documents/PACEDOCS/ae678a7d-0747-46ec-9b48-7a01ae100e0c.tiff'
--  SELECT * FROM tblDocuments WHERE DocumentID= 2037816
--SELECT * FROM tblDocuments WHERE AmazonKey = 'PRODUCTION/CLAIMSADMIN/MOTIONS/a57009e1-9335-46f3-958f-8e310fc34866.tiff'


--SELECT COUNT(DISTINCT DocumentURL) FROM tblDocuments WHERE AmazonKey IS NULL AND LEFT(DocumentURL, 9) = '/archive/'
GO
