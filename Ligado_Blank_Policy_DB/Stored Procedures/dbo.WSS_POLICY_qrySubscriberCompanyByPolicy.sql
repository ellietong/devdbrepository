SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qrySubscriberCompanyByPolicy]
	-- Add the parameters for the stored procedure here
@PolicyID int	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
SELECT DISTINCT SC.SubscriberID, SubscriptionCompany
FROM tblSubscriptionCoverage SC
LEFT JOIN tblSubscriberCompany SCo ON SC.SubscriberID = SCo.SubscriberID
LEFT JOIN tblSubscriptionSectionCoverage SSC ON SC.SubscriptionSectionCoverageID = SSC.SubscriptionSectionCoverageID
WHERE PolicyID = @PolicyID
UNION 
SELECT NULL, ''
ORDER BY SubscriptionCompany

END
GO
