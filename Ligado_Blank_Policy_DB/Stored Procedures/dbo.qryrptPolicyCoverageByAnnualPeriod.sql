SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  Procedure [dbo].[qryrptPolicyCoverageByAnnualPeriod]
(@insuranceProgramID int = NULL, @limitTypeID int = NULL, @namedInsuredID int = NULL, @startDate int = NULL, @endDate int = NULL)
As

SET NOCOUNT ON

--DECLARE @insuranceProgramID int 
--DECLARE @limitTypeID int
--DECLARE @namedInsuredID int
--SET @insuranceProgramID = 0
--SET @limitTypeID = 0
--SET @NamedInsuredID = 0

DECLARE @maxInsuranceProgram int
DECLARE @maxLimitTypeID int
DECLARE @maxNamedInsuredID int
DECLARE @maxEndDate int
-- Find max ID values
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxNamedInsuredID = MAX(namedInsuredID) FROM vPolicy
SELECT @maxEndDate = MAX(annualperiod) FROM vPolicy

SELECT InsuranceProgram, AnnualPeriod, LimitTypeID, SUM(PerOccLimit) AS SumOfPerOccLimit, SUM(SIR) AS SumOfSIR, NamedInsuredID
FROM vPolicy pinfo
WHERE InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND
	LimitTypeID >= COALESCE(@limitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	NamedInsuredID >= COALESCE(@namedInsuredID, 0) AND NamedInsuredID <= COALESCE(NULLIF(@namedInsuredID, 0), @maxNamedInsuredID)
	and DoNotReport <> 1
	AND AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
GROUP BY InsuranceProgram, AnnualPeriod, LimitTypeID, NamedInsuredID

RETURN

/* Single limit
SELECT tblInsuranceProgram.InsuranceProgram, tblPolicyDates.AnnualPeriod, SUM(tblPolicy.PerOccLimit) AS SumOfPerOccLimit, SUM(tblPolicy.SIR) AS SumOfSIR
FROM tblPolicy LEFT OUTER JOIN tblInsuranceProgram ON tblPolicy.InsuranceProgramID = tblInsuranceProgram.InsuranceProgramID LEFT OUTER JOIN tblPolicyDates ON 
	tblPolicy.PolicyID = tblPolicyDates.PolicyID
GROUP BY tblInsuranceProgram.InsuranceProgram, tblPolicyDates.AnnualPeriod
*/


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyCoverageByAnnualPeriod] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyCoverageByAnnualPeriod] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyCoverageByAnnualPeriod] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyCoverageByAnnualPeriod] TO [power_user]
GO
