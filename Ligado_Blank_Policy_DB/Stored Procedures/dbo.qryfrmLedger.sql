SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmLedger]
(@carrierID int = null)
AS
SET NOCOUNT ON

SELECT *
FROM tblLedger
WHERE CarrierID = @carrierID
order by BillDate


RETURN 
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLedger] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLedger] TO [power_user]
GO
