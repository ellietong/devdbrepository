SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qrySubscriberCompanyList] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT SubscriberID, SubscriptionCompany 
FROM tblSubscriberCompany
UNION 
SELECT NULL, ''
ORDER BY SubscriptionCompany
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qrySubscriberCompanyList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qrySubscriberCompanyList] TO [base_user]
GO
