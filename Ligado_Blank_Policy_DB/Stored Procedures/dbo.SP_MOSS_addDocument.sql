SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MOSS_addDocument]
(
	@DetailDocumentTypeID int,					--aka ClaimantDocumentTypeID
	@ClaimantPersonalInfoID bigint = null,
	@CreatedBy nvarchar(250),
	@DateCreated datetime,
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@SPDocId nvarchar(50),	
	@Comments nvarchar(4000) = null,			--comments are optional	
	@FirmID int,

	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,
    @Adjustment money = null,
    @AdjustmentNotes nvarchar(4000) = null,
    @StatusID int = null
)

AS
INSERT INTO tblDocuments (
	DetailDocumentTypeID,	
	ClaimantPersonalInfoID,
	Comments,
	DocumentURL,
	DocumentTitle,
	SPDocId,
	CreatedBy,
	DateCreated,
	InvoiceNumber,
	Period,
	DatePayable,
	FeeAmount,
	DisbursementAmount,
    Adjustment,
    AdjustmentNotes,
    StatusID,	
	ModifiedBy,
	DateModified,
	FirmID
)
VALUES (	
	@DetailDocumentTypeID,
	@ClaimantPersonalInfoID,
	@Comments,
	@DocumentURL,
	@DocumentTitle,
	@SPDocId,
	@CreatedBy,
	@DateCreated,
	@InvoiceNumber,
	@Period,
	@DatePayable,
	@FeeAmount,
	@DisbursementAmount,
    @Adjustment,
    @AdjustmentNotes,
    @StatusID,	
	@CreatedBy,
	@DateCreated,	
	@FirmID
)


Select @@Identity
GO
