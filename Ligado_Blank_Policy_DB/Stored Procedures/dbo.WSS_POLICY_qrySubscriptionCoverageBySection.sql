SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qrySubscriptionCoverageBySection]
	-- Add the parameters for the stored procedure here
	(@SubscriptionSectionID int, @SubscriberID int, @AnnualPeriodID int, @PolicyID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @MaxSubscriptionSectionID int
SET @MaxSubscriptionSectionID = (SELECT MAX(SubscriptionSectionID) FROM tblSubscriptionSection)
DECLARE @MaxSubscriberID int
SET @MaxSubscriberID = (SELECT MAX(SubscriberID) FROM tblSubscriberCompany)
DECLARE @MaxAnnualPeriodID int
SET @MaxAnnualPeriodID = (SELECT MAX (AnnualPeriod) FROM tblPolicyDates)

    -- Insert statements for procedure here
SELECT DISTINCT SSC.PolicyID, SubscriptionCoverageID, SubscriptionCompany, AgencyName, AgencyPercentOfSection, GroupWithinAgencyPercent, SubscriptionCompanyPercent, SubscriptionSectionPercentage, StartDate, EndDate, AcctNum, UWReference 
FROM tblSubscriptionCoverage SC
	INNER JOIN tblSubscriberCompany SCo ON SC.SubscriberID = SCo.SubscriberID
	LEFT JOIN tblSubscriptionSectionCoverage SSC ON SC.SubscriptionSectionCoverageID = SSC.SubscriptionSectionCoverageID
	LEFT JOIN tblSubscriptionSection SS ON SSC.SubscriptionSectionID = SS.SubscriptionSectionID
	LEFT JOIN tblSubscriberAgency SA ON SC.AgencyID = SA.AgencyID
	LEFT JOIN tblPolicy P ON SSC.PolicyID = P.PolicyID
	LEFT JOIN tblPolicyDates PD ON P.PolicyID = PD.PolicyID	
WHERE SSC.PolicyID = @PolicyID
	AND SSC.SubscriptionSectionID >= COALESCE (@SubscriptionSectionID, 0) AND SSC.SubscriptionSectionID <=  COALESCE (@SubscriptionSectionID, @MaxSubscriptionSectionID)
	AND SCo.SubscriberID >= COALESCE (@SubscriberID, 0) AND SCo.SubscriberID <= COALESCE (@SubscriberID, @MaxSubscriberID)
	AND AnnualPeriod >= COALESCE(@AnnualPeriodID, 0) AND AnnualPeriod <= COALESCE(@AnnualPeriodID, @MaxAnnualPeriodID)
ORDER BY SubscriptionCompany
END
GO
