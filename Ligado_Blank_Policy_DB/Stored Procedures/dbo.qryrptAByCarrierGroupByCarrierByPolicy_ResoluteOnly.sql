SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptAByCarrierGroupByCarrierByPolicy_ResoluteOnly]
(@AllocationID INT = NULL) 
AS        

SET NOCOUNT ON
 
--DECLARE @AllocationID INT
--SET @AllocationID = 6

DECLARE @ResolutePortion TABLE (PolicyDateID INT, ResolutePortion FLOAT)
INSERT INTO @ResolutePortion
SELECT PolicyDateID, 
SUM(COALESCE(CASE WHEN SubscriberID IN (1, 36, 37, 43, 61) THEN SubscriptionCompanyPercent END, 0)) AS ResolutePortion
FROM tblPolicyDates PD
INNER JOIN tblPolicy P ON P.PolicyID = PD.PolicyID
INNER JOIN tblSubscriptionSectionCoverage SSC ON SSC.PolicyID = P.PolicyID
INNER JOIN tblSubscriptionCoverage SC ON SC.SubscriptionSectionCoverageID = SSC.SubscriptionSectionCoverageID
WHERE CarrierID = 20 AND PolicyDateID NOT IN (1343, 1344, 1345)
GROUP BY PolicyDateID

UNION

SELECT PolicyDateID, 
CASE WHEN PolicyDateID IN (SELECT PolicyDateID 
							FROM tblPolicyDates PD 
							INNER JOIN tblPolicy P ON P.PolicyID = PD.PolicyID 
							WHERE CarrierID = 400) THEN 0.41 
	 WHEN PolicyDateID = 1343 THEN 0.086 
	 WHEN PolicyDateID IN (1344, 1345) THEN 0.096 
	 WHEN PolicyDateID IN (SELECT PolicyDateID 
							FROM tblPolicyDates PD 
							INNER JOIN tblPolicy P ON P.PolicyID = PD.PolicyID 
							WHERE CarrierID IN (4, 139, 74, 79, 53, 18, 25, 13, 72, 344, 5, 379, 148, 439, 147, 158)) THEN 1 END AS ResolutePortion
FROM tblPolicyDates PD
INNER JOIN tblPolicy P ON P.PolicyID = PD.PolicyID
WHERE PolicyDateID IN (1343, 1344, 1345) OR CarrierID IN (4, 139, 74, 79, 53, 18, 25, 13, 72, 344, 5, 379, 148, 439, 147, 158, 400)

SELECT AR.AllocationID, AllocationDescription, PD.PolicyID, CarrierGroupName, CarrierName, PolicyNum, AttachmentPoint, PerOccLimit, LayerPerOccLimit, AggregateLimit,
MIN(PStartDate) AS StartDate, MAX(PEndDate) AS EndDate,
SUM(ResolutePortion * AllocationIndemnity) AS ResoluteIndemnityAllocation,
SUM(ResolutePortion * (AllocationDefense + AllocationDefenseOutsideLimits)) AS ResoluteDefenseAllocation,
SUM(ResolutePortion * Allocation) AS ResoluteTotalAllocation
FROM tblAllocationResults AR
INNER JOIN @ResolutePortion RP ON RP.PolicyDateID = AR.PolicyDateID
INNER JOIN tblPolicyDates PD ON PD.PolicyDateID = AR.PolicyDateID 
INNER JOIN tblPolicy P ON P.PolicyID = PD.PolicyID
INNER JOIN tblCarrier C ON C.CarrierID = P.CarrierID
LEFT JOIN tblCarrierGroup CG ON CG.CarrierGroupID = C.CarrierGroupID
INNER JOIN tblPolicyLimits PL ON PL.PolicyID = P.PolicyID
INNER JOIN tblAllocation A ON A.AllocationID = AR.AllocationID 
WHERE AR.AllocationID = @AllocationID
AND InsuranceProgramID = 1
AND PL.LimitTypeID IN (SELECT LimitTypeID FROM tblAllocation WHERE AllocationID = @AllocationID)
GROUP BY AR.AllocationID, AllocationDescription, PD.PolicyID, CarrierGroupName, CarrierName, PolicyNum, AttachmentPoint, PerOccLimit, LayerPerOccLimit, AggregateLimit
ORDER BY CarrierGroupName, CarrierName, StartDate, AttachmentPoint
GO
