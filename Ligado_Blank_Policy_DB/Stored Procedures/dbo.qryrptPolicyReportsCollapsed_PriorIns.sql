SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryrptPolicyReportsCollapsed_PriorIns]
(@insuranceProgramID int = NULL,@NamedInsuredID int = null, @limitTypeID int = NULL, @carrierID int = NULL, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL,
@lowLayer int = NULL, @highLayer int = NULL, @lowAttach nvarchar(50) = NULL, @highAttach nvarchar(50) = NULL, @startDate int = NULL, @endDate int = NULL)
As

SET NOCOUNT ON

DECLARE @maxInsuranceProgram int
DECLARE @maxNamedInsured Int
DECLARE @maxLimitTypeID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)
DECLARE @maxLayer int
DECLARE @maxAttach money
DECLARE @maxEndDate int

-- Find max ID values
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxNamedInsured = MAX(NamedInsuredID) FROM vPolicy
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxLayer = MAX(Layer) FROM vPolicy
SELECT @maxAttach = MAX(AttachmentPoint) FROM vPolicy
SELECT @maxEndDate = MAX(annualperiod) FROM vPolicy

IF @carrierGroupID = 0 SET @carrierGroupID = NULL

SELECT pinfo.PolicyID, PolicyNum, CarrierID, CarrierName, CarrierSolvency, CarrierGroupID, COALESCE(CarrierGroupName,CarrierName) as CarrierGroupName, NamedInsured, InsuranceProgram, NamedInsuredID,
	1 AS NumOccurrences, ActualPStartDate AS MinOfPStartDate, ActualPEndDate AS MaxOfPEndDate, AttachmentPoint AS AttachmentPoint, 
	AnnualPeriod AS MinOfAnnualPeriod, SIR AS SIR, PerOccLimit AS PerOccLimit, LayerPerOccLimit AS LayerPerOccLimit, AggregateLimit AS AggregateLimit, 
	Layer AS Layer, PerOccLimit AS TotalLimitsPerPolicy, ProvisionValue
FROM vPolicy pinfo
inner join (select PolicyID, ProvisionValue from tblProvision p inner join tblProvisionValue pv on pv.ProvisionValueID = p.ProvisionValueID where p.ProvisionValueID in (130,131,132,133,134,135,149,169)) prov on prov.PolicyID = pinfo.PolicyID
WHERE InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND
 	NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsured ) AND
	LimitTypeID >= COALESCE(@limitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	CarrierID >= COALESCE(@carrierID, 0) AND CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
	(coalesce(CarrierGroupID, 0) >= COALESCE(@carrierGroupID, 0) AND coalesce(CarrierGroupID, 0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(CarrierGroupID, 0) = @carrierGroupID) AND
	CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND Layer BETWEEN COALESCE(@lowLayer, 0) AND COALESCE(@highLayer, @maxLayer) AND
	AttachmentPoint BETWEEN COALESCE(CONVERT(money, @lowAttach), 0) AND COALESCE(CONVERT(money, @highAttach), @maxAttach)
	and DoNotReport <>1
	AND annualperioD >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
ORDER BY InsuranceProgram

RETURN
GO
