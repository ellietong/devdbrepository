SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryBrokerInformation] 
	-- Add the parameters for the stored procedure here
	(@PolicyID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT BrokerType, Broker, BrokerRefNum 
FROM tblBrokerPolicy BP
INNER JOIN tblBroker B ON BP.BrokerID = B.BrokerID
INNER JOIN tblBrokerType BT ON BP.BrokerTypeID = BT.BrokerTypeID
LEFT JOIN tblBrokerRef BR ON BP.BrokerRefID = BR.BrokerRefID
WHERE PolicyID = @PolicyID
END
GO
