SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmUnionCorporateProfile]
As

SET NOCOUNT ON

SELECT CorporateHistoryID, CorporateHistoryProfile
FROM tblCorporateHistoryProfile

UNION 

SELECT 0, '<<All Corporate History Profiles>>' AS CorporateHistoryProfile
FROM tblCorporateHistoryProfile
ORDER BY CorporateHistoryProfile

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionCorporateProfile] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionCorporateProfile] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionCorporateProfile] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionCorporateProfile] TO [power_user]
GO
