SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptAdditionalNamedIns]
(@policyid int = null)
As

SET NOCOUNT ON

SELECT an.policyid, an.additionalnamedinstypeid, an.namedinsuredid,
an.effectivedate, an.bates, n.namedinsured, an.additionalnamedinsid
FROM tblAdditionalNamedInsured an
inner join tblNamedInsured n on n.NamedInsuredID =an.NamedInsuredID
WHERE @policyid = an.PolicyID
order by n.namedinsured

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAdditionalNamedIns] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAdditionalNamedIns] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAdditionalNamedIns] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAdditionalNamedIns] TO [power_user]
GO
