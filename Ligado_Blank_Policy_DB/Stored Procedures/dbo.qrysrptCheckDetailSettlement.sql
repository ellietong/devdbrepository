SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qrysrptCheckDetailSettlement] (@CheckID int)

AS
Set NoCount On


Declare @SGTotals table (SettlementGroupID int, CountOfTotalSettlements int)
Insert Into @SGTotals
Select SettlementGroupID, COUNT(*)
From tblClaimantSettlement cs
Inner Join tblPaymentSettlement ps on ps.ClaimantSettlementID = cs.ClaimantSettlementID
Where CheckID = @CheckID
Group By SettlementGroupID

Select CheckID, SettlementGroup, COUNT(*) as CountOfSettlements, CountOfTotalSettlements, SUM(SettlementAmount) as SettlementAmount, Sum(PaymentAmount) as PaymentAmount
from tblPaymentSettlement ps
Inner Join tblClaimantSettlement cs on cs.ClaimantSettlementID = ps.ClaimantSettlementID
Inner Join tblSettlementGroup sg on sg.SettlementGroupID = cs.SettlementGroupID
Inner Join @SGTotals sgt on sgt.SettlementGroupID=cs.SettlementGroupID
Where CheckID = @CheckID
Group By CheckID, SettlementGroup, CountOfTotalSettlements


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [CheckQC_User]
GO
