SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_MOSS_qryrptSharePointActivity]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE @allActions TABLE (DocumentID int, firmID int, firm nvarchar(500), NumCreated int, NumAccepted int, NumApproved int, NumModified int, DateCreated datetime, DocumentType nvarchar(500))


-- CONVERT(datetime, DateAccepted, 101) AS ActionDate,

INSERT INTO @allActions
--Number of documents UPLOADED in last day by firm
SELECT DocumentID, COALESCE(f.FirmID, 0) AS FirmID, COALESCE(Firm, 'KCIC') AS Firm, COUNT(DocumentID) AS NumCreated, 0 AS NumAccepted, 0 AS NumApproved, 0 AS NumModified, DateCreated, COALESCE(cdt.ClaimantDocumentType, 'INVOICE') AS ClaimantDocumentType--, cdt.ClaimantDocumentType
 FROM tblDocuments d
LEFT JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
LEFT JOIN vtblPerson p ON p.Email = d.CreatedBy
LEFT JOIN vtblFirm f ON p.FirmID = f.FirmID
WHERE DateCreated IS NOT NULL --(DateCreated BETWEEN GETDATE()-7 AND GETDATE())
GROUP BY  Firm, f.FirmID, DateCreated, DocumentID, d.CreatedBy, cdt.ClaimantDocumentType--, ClaimantDocumentType
ORDER BY f.Firm

INSERT INTO @allActions
--Number of documents ACCEPTED in last day by firm
SELECT DocumentID, COALESCE(f.FirmID, 0) AS FirmID, COALESCE(Firm, 'KCIC') AS Firm,  0 AS NumCreated, COUNT(DocumentID) AS NumAccepted, 0 AS NumApproved, 0 AS NumModified, DateAccepted, COALESCE(cdt.ClaimantDocumentType, 'INVOICE') AS ClaimantDocumentType--, cdt.ClaimantDocumentType
 FROM tblDocuments d
LEFT JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
LEFT JOIN vtblPerson p ON p.Email = d.AcceptedBy
LEFT JOIN vtblFirm f ON p.FirmID = f.FirmID
WHERE DateAccepted IS NOT NULL --(DateAccepted BETWEEN GETDATE()-7 AND GETDATE())
GROUP BY  Firm, f.FirmID, DateAccepted, DocumentID, cdt.ClaimantDocumentType--, d.CreatedBy--, ClaimantDocumentType
ORDER BY f.Firm

INSERT INTO @allActions
--Number of documents APPROVED in last day by firm
SELECT DocumentID, COALESCE(f.FirmID, 0) AS FirmID, COALESCE(Firm, 'KCIC') AS Firm,  0 AS NumCreated, 0 AS NumAccepted, COUNT(DocumentID) AS NumApproved, 0 AS NumModified, DateApproved, COALESCE(cdt.ClaimantDocumentType, 'INVOICE') AS ClaimantDocumentType--, cdt.ClaimantDocumentType
 FROM tblDocuments d
LEFT JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
LEFT JOIN vtblPerson p ON p.Email = d.ApprovedBy
LEFT JOIN vtblFirm f ON p.FirmID = f.FirmID
WHERE DateApproved IS NOT NULL--(DateApproved BETWEEN GETDATE()-7 AND GETDATE())
GROUP BY  Firm, f.FirmID, DateApproved, DocumentID, cdt.ClaimantDocumentType--, d.CreatedBy--, ClaimantDocumentType
ORDER BY f.Firm

INSERT INTO @allActions
--Number of documents MODIFIED in last day by firm
SELECT DocumentID, COALESCE(f.FirmID, 0) AS FirmID, COALESCE(Firm, 'KCIC') AS Firm,  0 AS NumCreated, 0 AS NumAccepted, 0 AS NumApproved, COUNT(DocumentID) AS NumModified, DateModified, COALESCE(cdt.ClaimantDocumentType, 'INVOICE') AS ClaimantDocumentType--, cdt.ClaimantDocumentType
 FROM tblDocuments d
LEFT JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
LEFT JOIN vtblPerson p ON p.Email = d.ModifiedBy
LEFT JOIN vtblFirm f ON p.FirmID = f.FirmID
WHERE DateModified IS NOT NULL--(DateModified BETWEEN GETDATE()-7 AND GETDATE())
AND DocumentID NOT IN (SELECT DocumentID
	FROM tblDocuments d
	WHERE (DateCreated = DateModified) OR (DateModified = DateAccepted) OR (DateModified = DateApproved))
GROUP BY  Firm, f.FirmID, DateModified, DocumentID, cdt.ClaimantDocumentType
ORDER BY f.Firm


SELECT CASE firm WHEN 'KCIC' THEN 'KCIC' ELSE 'Defense Counsels' END AS Firm, 
COALESCE(NumCreated, 0) AS NumCreated,
COALESCE(NumAccepted, 0) AS NumAccepted,
COALESCE(NumModified, 0) AS NumModified,
COALESCE(NumApproved, 0) AS NumApproved,
DocumentType
FROM @allActions
ORDER BY Firm, DocumentType

END
GO
