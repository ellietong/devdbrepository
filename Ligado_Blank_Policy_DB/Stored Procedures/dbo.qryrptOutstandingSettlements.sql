SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptOutstandingSettlements] (@MatterID int, @SettlementDate datetime)

As

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 3
--DECLARE @SettlementDateStart datetime
--SET @SettlementDateStart = '1/1/1900'
--DECLARE @SettlementDateEnd datetime
--SET @SettlementDateEnd = '1/1/2012'
--DECLARE @SettlementDate datetime
--SET @SettlementDate = '1/1/1900'

DECLARE @MaxMatterID int
SELECT @MaxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @SettlementGroupPaidUnpaid TABLE (SettlementGroupID int, Paid money, Total money)
INSERT INTO @SettlementGroupPaidUnpaid
	SELECT SettlementGroupID, 
	SUM( CASE WHEN PaidDate IS NULL THEN 0 ELSE SettlementAmount END) As Paid,
	SUM(SettlementAmount) as Total
	FROM tblClaimantSettlement cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
	WHERE
	--COALESCE(c.IsCurrentClaim,0) = 1 AND
	c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	GROUP BY SettlementGroupID
	--COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupCount TABLE (SettlementGroupID int, ClaimCount nvarchar(50))
INSERT INTO @SettlementGroupCount	
	SELECT SettlementGroupID, COUNT(SettlementDate) as ClaimCount
	FROM tblClaimantSettlement 	cs
	inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblClaim c on c.ClaimID = cl.ClaimID
		where
		--COALESCE(c.IsCurrentClaim,0) = 1 AND
		c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	GROUP BY SettlementGroupID
	--COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupPaidCount TABLE (SettlementGroupID int, PaidCount nvarchar(50))
INSERT INTO @SettlementGroupPaidCount	
	SELECT SettlementGroupID, COUNT(PaidDate) as PaidCount
	FROM tblClaim c
		INNER JOIN tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
		INNER JOIN tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
	--FROM tblClaimantSettlement cs
	--inner join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	--inner join tblClaim c on c.ClaimID = cl.ClaimID
	WHERE SettlementGroupID IN	(SELECT SettlementGroupID
								 FROM tblClaimantSettlement
								 WHERE PaidDate IS NOT NULL 
								 GROUP BY SettlementGroupID)
								 	--and COALESCE(c.IsCurrentClaim,0) = 1
								 	AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	GROUP BY SettlementGroupID
	--COALESCE(c.IsCurrentClaim,0)
	ORDER BY SettlementGroupID


DECLARE @SettlementGroupMaxDate TABLE (SettlementGroupID int, MaxSettlementDate datetime)
INSERT INTO @SettlementGroupMaxDate
	SELECT SettlementGroupID, MAX(SettlementDate) as MaxSettlementDate
	FROM tblClaimantSettlement
	GROUP BY SettlementGroupID 
	ORDER BY MaxSettlementDate


SELECT pl.PlaintiffCounsel, cs.SettlementDate, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, CheckNumber, spd.ReleaseSignedDate, FullyDocumented, c.MatterID, Matter, cl.LawsuitID, paiddate,
	@SettlementDate AS SettlementDateAsOf, DocketNumber, COALESCE(sgpc.PaidCount,0) AS PaidCount, sgc.ClaimCount, MaxSettlementDate,
	CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
		 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END AS SupportingDocumentation,
	CASE WHEN spd.ReleaseRcvd = 1
		 THEN coalesce( crr.ReleaseType,'Executed' )
		 ELSE 'Release Missing'  END AS ReleaseExecution, Case when PaidDate is not null then cs.SettlementAmount else 0 end as Paid
FROM tblClaimantPersonalInfo cpi
	INNER JOIN tblClaim c						 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMatter m						 ON c.MatterID = m.MatterID		
	INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimantSettlement cs			 ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblLawsuit l						 oN l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pl	 ON l.PlaintiffCounselAddressID = pl.PlaintiffCounselAddressID
	INNER Join tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
	--INNER JOIN tblClaimLawsuitStatus cls		 ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID	
	LEFT  JOIN tblSettlementGroup s				 ON cs.SettlementGroupID = s.SettlementGroupID
	--LEFT  JOIN tblClaimantDocketNumbers cd		 ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
	LEFT  JOIN @SettlementGroupPaidUnpaid sgp	 ON s.SettlementGroupID = sgp.SettlementGroupID
	LEFT  JOIN @SettlementGroupCount sgc		 ON s.SettlementGroupID= sgc.SettlementGroupID
	LEFT  JOIN @SettlementGroupPaidCount sgpc	 ON s.SettlementGroupID = sgpc.SettlementGroupID
	LEFT  JOIN @SettlementGroupMaxDate sgm		 ON s.SettlementGroupID = sgm.SettlementGroupID
	LEFT JOIN  tblReleasetype crr				 ON crr.ReleaseTypeID =spd.ReleaseTypeID
	--LEFT JOIN tblClaimSequence csq				 ON c.ClaimSequenceID = csq.ClaimSequenceID
WHERE MaxSettlementDate >= @SettlementDate
	--AND COALESCE(IsCurrentClaim,0) = 1
	--AND c.MatterID = @MatterID
	AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(NULLIF(@MatterID, 0), @MaxMatterID)
	AND COALESCE(PaidCount,0) <> COALESCE(ClaimCount,0)
	--AND csq.ClaimSequenceID not in (4,2,3,11)
	AND clst.ClaimLawsuitStatusID in (23,24,25,28)
	AND cl.IsPrimaryForClaim=1
	AND IsPrimaryStatus = 1

GROUP BY cs.SettlementDate, LastName, FirstName, MiddleName, s.SettlementGroupID, s.SettlementGroup, c.ClaimantPersonalInfoID,
	cs.SettlementAmount, CheckNumber, spd.ReleaseSignedDate, FullyDocumented, c.MatterID, Matter, cl.LawsuitID,paiddate,pl.PlaintiffCounsel,
	  DocketNumber, sgpc.PaidCount, sgc.ClaimCount, MaxSettlementDate,spd.ReleaseRcvd,

CASE WHEN (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
			 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END,
	CASE WHEN spd.ReleaseRcvd = 1
		 THEN coalesce( crr.ReleaseType,'Executed' ) 
		 ELSE 'Release Missing'  END
ORDER BY MaxSettlementDate, SettlementGroup, SettlementDate, LastName
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlements] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlements] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOutstandingSettlements] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOutstandingSettlements] TO [power_user]
GO
