SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qrysrptSettlePaymentDocumentation]
(@ClaimantPersonalInfoID bigint)

As
Set NoCount On


--Declare @ClaimantPersonalInfoID bigint
--Set @ClaimantPersonalInfoID = 5185


SELECT SettlementConfirmation, ClaimantIDed, DxMedicals, DODCert, EstatePapers, ProductID, EmployExpoHist, ReleaseRqstDate,
ExecutedReleaseDate, ReleaseRcvd, MedicareFullyDocumented, MedicareReported, ReleaseToPC
FROM tblClaimantPersonalInfo cpi
LEFT JOIN tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
LEFT JOIN tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID
WHERE cpi.ClaimantPersonalInfoID=@ClaimantPersonalInfoID


GO
