SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[SP_MOSS_qryFirmDocuments](@emailaddress nvarchar (1000)=null)
AS

--Declare and set the temporary field used to check the user's firm based on their login email address
DECLARE @firmid int
SET @firmid = (SELECT DISTINCT d.FirmID FROM tblDocuments d	INNER JOIN vtblPerson p ON d.FirmID = p.FirmID WHERE p.Email = @emailaddress)

IF ((@FirmID = 24) OR (@firmid = 42) OR (LEFT(@emailaddress,5) = 'kcic\')) BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.ClaimantPersonalInfoID, d.DocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, f.Firm, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + 
		CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName
	FROM dbo.tblDocuments AS d
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		INNER JOIN vtblPerson AS p ON d.FirmID = p.FirmID
		INNER JOIN tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
		INNER JOIN vtblFirm AS f ON d.FirmID = f.FirmID
		INNER JOIN tblClaim As c ON c.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
	WHERE cdt.ClaimantDocumentTypeID <> 1
END ELSE BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.ClaimantPersonalInfoID, d.DocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, f.Firm, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + 
		CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName
	FROM dbo.tblDocuments AS d
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		INNER JOIN vtblPerson AS p ON d.FirmID = p.FirmID
		INNER JOIN tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
		INNER JOIN vtblFirm AS f ON d.FirmID = f.FirmID
		INNER JOIN tblClaim As c ON c.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
	WHERE cdt.ClaimantDocumentTypeID <> 1
		AND @firmid = d.FirmID
END
GO
