SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_REPORTS_qryAgingAccountsReceivable]
AS
SET NOCOUNT ON

SELECT COALESCE(CarrierName, 'Resco') as CarrierName, --Non-pivoted column
		--columns to pivot
		SUM(COALESCE(AmountCurrent, 0)) AS AmountCurrent,
		SUM(COALESCE(Amount30, 0)) AS Amount30,
		SUM(COALESCE(Amount60,0)) AS Amount60,
		SUM(COALESCE(Amount90, 0)) AS Amount90,
		SUM(COALESCE(Amount120, 0)) AS Amount120,
		SUM(COALESCE(AmountLong, 0)) AS AmountLong,
		SUM(COALESCE(Amount30,0) + COALESCE(Amount60,0) + COALESCE(Amount90,0) + COALESCE(Amount120,0) + COALESCE(AmountLong,0)) AS AmountOverdue,
		SUM(COALESCE(Amount30,0) + COALESCE(Amount60,0) + COALESCE(Amount90,0) + COALESCE(Amount120,0) + COALESCE(AmountLong,0) +COALESCE(AmountCurrent, 0)) AS AmountTotal,
		SUM(CASE WHEN COALESCE(Amount30, 0) > 0 THEN Count ELSE 0 END) AS Count30,
		SUM(CASE WHEN COALESCE(Amount60, 0) > 0 THEN Count ELSE 0 END) AS Count60,
		SUM(CASE WHEN COALESCE(Amount90, 0) > 0 THEN Count ELSE 0 END) AS Count90,
		SUM(CASE WHEN COALESCE(Amount120, 0) > 0 THEN Count ELSE 0 END) AS Count120,
		SUM(CASE WHEN COALESCE(AmountLong, 0) > 0 THEN Count ELSE 0 END) AS CountLong,
		SUM(Count) AS CountTotal, 
		 ROW_NUMBER() OVER (ORDER BY COALESCE(CarrierName, 'Resco'))% 2 AS Remainder
	FROM
		--where most of the magic happens
		(SELECT CarrierGroupName AS CarrierName, 
				SUM(COALESCE(ar.Amount,0)-COALESCE(arp.Amount, 0)) as Amount, 
				CASE	WHEN GETDATE() - PaymentDueDate <= 0  THEN 'AmountCurrent'
						WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 30 THEN 'Amount30'
						WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 60 THEN 'Amount60'
						WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 90 THEN 'Amount90'
						WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 120 THEN 'Amount120'
						WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate > 120 THEN 'AmountLong' END AS AmountColumn,
				COUNT(DISTINCT ar.TravelersBillID) AS Count
	FROM tblAccountsReceivable ar
		LEFT JOIN tblAccountsReceivablePayment arp	ON ar.AccountsReceivableID = arp.AccountsReceivableID
		LEFT JOIN tblTravelersBill tb ON ar.TravelersBillID = tb.TravelersBillID
		LEFT JOIN tblPolicy p ON ar.PolicyID = p.PolicyID
		LEFT JOIN tblCarrier c ON p.CarrierID = c.CarrierID
		LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID
	WHERE PaymentDueDate IS NOT NULL AND PayeeTypeID = 1 and BillStatusID <> 2 and AllocationQCConfirmation = 1
	GROUP BY CarrierGroupName, 
		CASE	WHEN GETDATE() - PaymentDueDate <= 0  THEN 'AmountCurrent'
				WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 30 THEN 'Amount30'
				WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 60 THEN 'Amount60'
				WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 90 THEN 'Amount90'
				WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 120 THEN 'Amount120'
				WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate > 120 THEN 'AmountLong' END
	HAVING SUM(COALESCE(arp.Amount, 0)) <> SUM(COALESCE(ar.Amount, 0)))

		AS counts

	PIVOT

	(
		SUM(Amount)

	FOR
	AmountColumn

		IN (AmountCurrent, Amount30, Amount60, Amount90, Amount120, AmountLong)

	) AS Pivoted
	GROUP BY CarrierName

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryAgingAccountsReceivable] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryAgingAccountsReceivable] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryAgingAccountsReceivable] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryAgingAccountsReceivable] TO [power_user]
GO
