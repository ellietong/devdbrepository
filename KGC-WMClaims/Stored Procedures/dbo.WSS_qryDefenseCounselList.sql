SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_qryDefenseCounselList] (@emailaddress nvarchar(1000)=null)
AS

	SELECT DISTINCT DefenseCounselAddressID as DefenseCounselAddressID, DefenseCounsel as DefenseCounsel 
	FROM tblDefenseCounselAddress 
	UNION SELECT NULL,NULL
	ORDER BY DefenseCounsel


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDefenseCounselList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDefenseCounselList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDefenseCounselList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDefenseCounselList] TO [power_user]
GO
