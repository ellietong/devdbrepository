SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryPendingMattersActiveByState]
	(@StartDate datetime,
	 @EndDate datetime)		
AS
BEGIN
	SET NOCOUNT ON;
	--Test Variables
	--DECLARE @StartDate datetime = '1/1/1900'
	--DECLARE @EndDate datetime = GETDATE()
	
	--SELECT 
	--	COALESCE(FullNameState, 'Unknown') As State, 
	--	COUNT(DISTINCT(cl.ClaimantLawsuitID)) AS Count
	--FROM tblClaimantLawsuit cl 
	--	LEFT JOIN tblLawsuit l				ON cl.LawsuitID=l.LawsuitID
	--	LEFT JOIN tblLawsuitStatus AS ls	ON cL.LawsuitStatusID = ls.LawsuitStatusID
	--	LEFT JOIN tblState s				ON l.StateID=s.StateID
	--WHERE COALESCE(filedate, servicedate, statusdate, '1/1/1900') <= @EndDate
	--	AND (ls.LawsuitStatusID = 6 
	--			OR (ls.LawsuitStatusID not in (6, 11) and statusdate >= @EndDate))
	--GROUP BY FullNameState
	--ORDER BY COUNT(DISTINCT(cl.ClaimantLawsuitID)) DESC


END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryPendingMattersActiveByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryPendingMattersActiveByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryPendingMattersActiveByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryPendingMattersActiveByState] TO [power_user]
GO
