SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimDetails]
	-- Add the parameters for the stored procedure here
@ClaimantPersonalInfoID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--SELECT  DISTINCT   CL.ClaimantLawsuitID, DocketNumber, CaseCaption, State, ServiceDate, CL.ClaimantPersonalInfoID
--FROM			  tblClaimantLawsuit AS CL 
--				  INNER JOIN tblClaimantPersonalInfo CPI ON CL.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
--				  INNER JOIN dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID 
--				  LEFT JOIN dbo.tblState AS S ON S.StateID = L.StateID 
--                  LEFt JOIN dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID  
--                  left join tblClaimLawsuitDisease as ClD on ClD.ClaimantLawsuitID = cl.ClaimantLawsuitID
--				  left join tblDisease ddis on ddis.DiseaseID = ClD.DiseaseID
				
--				  left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID --OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
--				  LEFT JOIN tblClaimantEmployment CE ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID   
--                  LEFT JOIN dbo.tblLawsuitStatus AS ls ON CL.LawsuitStatusID = ls.LawsuitStatusID 
--                  left JOIN tblClaimType CT ON CT.ClaimTypeID = CPI.ClaimTypeID
--WHERE CL.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimDetails] TO [power_user]
GO
