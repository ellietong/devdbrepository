SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantTravelersDocuments] 
	-- Add the parameters for the stored procedure here
	@ClaimLawsuitID int = 0, @ClaimID bigint = NULL--, @LawsuitID int = 0 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	
	--DECLARE @ClaimantPersonalInfoID bigint = 2

IF @ClaimLawsuitID > 0
BEGIN
	--DECLARE @ClaimLawsuitID int = (SELECT ClaimLawsuitID FROM tblClaimLawsuit WHERE ClaimID = @ClaimID AND LawsuitID = @LawsuitID)

	SELECT    DISTINCT(d.DocumentID), d.DocumentTitle, d.ClaimantDocumentTypeID, ClaimantDocumentType, Notes, d.AmazonKey, tb.MatterID, Matter,
		tb.DOFE, tb.PaymentDueDate, COALESCE(tb.InvoiceNumber, 'N/A') AS InvoiceNumber, tb.DefenseCounsel as DefenseCounsel,
		CASE WHEN tb.InvoiceNumber IS NULL THEN tb.SettlementAmount ELSE InvoicedTotal END AS TotalCosts,
		CASE WHEN tb.InvoiceNumber IS NULL THEN tb.SettlementAmount ELSE FinalTotal END AS AllocatedAmount,
		CASE WHEN ((cl.DOFE <> tb.DOFE) OR (c.MatterID <> tb.MatterID)) AND (tb.BillStatusID <> 3) THEN 1 ELSE 0 END AS HighlightRow,
		CASE WHEN ((c.MatterID <> tb.MatterID)) AND (tb.BillStatusID <> 3) THEN 1 ELSE 0 END AS HighlightClaimType,
		CASE WHEN ((cl.DOFE <> tb.DOFE)) AND (tb.BillStatusID <> 3)  THEN 1 ELSE 0 END AS HighlightDOFE, Status, tb.BillStatusID,
		d.TravelersBillID, tb.InvoiceDocumentID, AllocatedInvoiceAmountQC, invd.AmazonKey AS InvoiceAmazonKey, AllocationQCConfirmation, tb.ClaimLawsuitID,
		'' InvoiceURL, '' DocumentURL --tb.AllocationQCStatusID, AllocationQCStatus
		FROM tblDocuments D     
			 INNER JOIN tblMultiClaimantDocument MCD ON d.DocumentID = MCD.DocumentID      
			 LEFT JOIN tblClaimantPersonalInfo AS CPI ON CPI.ClaimantPersonalInfoID = MCD.ClaimantPersonalInfoID
			 INNER JOIN tblClaimantDocumentType AS DT ON DT.ClaimantDocumentTypeID = D.ClaimantDocumentTypeID  
			 INNER JOIN tblTravelersBill tb ON d.TravelersBillID = tb.TravelersBillID
			 INNER JOIN tblMatter ct ON ct.MatterID = tb.MatterID
			 INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
			 LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = c.ClaimID
			 INNER JOIN tblBillStatus bs ON tb.BillStatusID = bs.BillStatusID
			 LEFT JOIN tblDocuments invd ON tb.InvoiceDocumentID = invd.DocumentID
			 --LEFT JOIN tblAllocationQCStatus aqcs ON tb.AllocationQCStatusID = aqcs.AllocationQCStatusID
	WHERE tb.ClaimLawsuitID = @ClaimLawsuitID 
		AND CL.ClaimLawsuitID = @ClaimLawsuitID 
		AND d.ClaimantDocumentTypeID IN (48, 49) 
		AND tb.BillStatusID <> 2

END
ELSE
BEGIN
	SELECT    DISTINCT(d.DocumentID), d.DocumentTitle, d.ClaimantDocumentTypeID, ClaimantDocumentType, Notes, d.AmazonKey, tb.MatterID, Matter,
	tb.DOFE, tb.PaymentDueDate, COALESCE(tb.InvoiceNumber, 'N/A') AS InvoiceNumber, tb.DefenseCounsel as DefenseCounsel,
	CASE WHEN tb.InvoiceNumber IS NULL THEN tb.SettlementAmount ELSE InvoicedTotal END AS TotalCosts,
	CASE WHEN tb.InvoiceNumber IS NULL THEN tb.SettlementAmount ELSE FinalTotal END AS AllocatedAmount,
	CASE WHEN ((cl.DOFE <> tb.DOFE) OR (c.MatterID <> tb.MatterID)) AND (tb.BillStatusID <> 3) THEN 1 ELSE 0 END AS HighlightRow,
	CASE WHEN ((c.MatterID <> tb.MatterID)) AND (tb.BillStatusID <> 3) THEN 1 ELSE 0 END AS HighlightClaimType,
	CASE WHEN ((cl.DOFE <> tb.DOFE)) AND (tb.BillStatusID <> 3)  THEN 1 ELSE 0 END AS HighlightDOFE, Status, tb.BillStatusID,
	d.TravelersBillID, tb.InvoiceDocumentID, AllocatedInvoiceAmountQC, invd.AmazonKey AS InvoiceAmazonKey, AllocationQCConfirmation, tb.ClaimLawsuitID,
	'' InvoiceURL, '' DocumentURL --tb.AllocationQCStatusID, AllocationQCStatus
	FROM tblDocuments D     
		 INNER JOIN tblMultiClaimantDocument MCD ON d.DocumentID = MCD.DocumentID      
		 LEFT JOIN tblClaimantPersonalInfo AS CPI ON CPI.ClaimantPersonalInfoID = MCD.ClaimantPersonalInfoID
		 INNER JOIN tblClaimantDocumentType AS DT ON DT.ClaimantDocumentTypeID = D.ClaimantDocumentTypeID  
		 INNER JOIN tblTravelersBill tb ON d.TravelersBillID = tb.TravelersBillID
		 INNER JOIN tblMatter ct ON ct.MatterID = tb.MatterID
		 INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
		 LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = c.ClaimID
		 INNER JOIN tblBillStatus bs ON tb.BillStatusID = bs.BillStatusID
		 LEFT JOIN tblDocuments invd ON tb.InvoiceDocumentID = invd.DocumentID
		 --LEFT JOIN tblAllocationQCStatus aqcs ON tb.AllocationQCStatusID = aqcs.AllocationQCStatusID
	WHERE tb.ClaimLawsuitID IN (SELECT ClaimLawsuitID FROM tblClaimLawsuit WHERE ClaimID = @ClaimID) 
		AND cl.ClaimLawsuitID IN (SELECT ClaimLawsuitID FROM tblClaimLawsuit WHERE ClaimID = @ClaimID) 
		AND tb.BillStatusID <> 2 --C.ClaimID = @ClaimID AND d.ClaimantDocumentTypeID IN (48, 49) AND tb.BillStatusID <> 2 	
END
END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantTravelersDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantTravelersDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantTravelersDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantTravelersDocuments] TO [power_user]
GO
