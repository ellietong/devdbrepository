SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryClaimantsMatterMismatch] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @TravelersComparison TABLE (ClaimantPersonalInfoID bigint, ConfirmationStatusID int)
	
	INSERT INTO @TravelersComparison	
	SELECT 
		cpi.ClaimantPersonalInfoID, 
		CASE WHEN (SUM(CASE WHEN tb.MatterID <> c.MatterID THEN 1 ELSE 0 END) > 0) THEN 4 ELSE 0 END --Claim Type Mismatch
	FROM tblClaimantPersonalInfo cpi
		INNER JOIN tblClaim C ON cpi.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID  
		LEFT JOIN (SELECT * FROM tblDocuments WHERE ClaimantDocumentTypeID IN (48, 49))d ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
		LEFT JOIN (SELECT * FROM tblTravelersBill WHERE BillStatusID = 1) tb ON CL.ClaimLawsuitID = tb.ClaimLawsuitID--only current bills
	GROUP BY cpi.ClaimantPersonalInfoID

	SELECT DISTINCT 
		CPI.ClaimantPersonalInfoID AS ClaimantPersonalInfoID, 
		COUNT(DISTINCT(CPI.ClaimantPersonalInfoID)) As Count,  
		CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
		CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName,
		CASE WHEN Matter IS NULL THEN '' ELSE Matter END AS Matter,
		CL.DOFE, 
		SUM(COALESCE(FinalTotal, 0) + COALESCE(tb.SettlementAmount, 0)) as TotalCosts, 
		Notes, 
		ConfirmationStatus, 
		FirstName, 
		MiddleName, 
		LastName, 
		c.MatterID,
		Gender, 
		BirthDate, 
		DeceasedDate, 
		SocialSecurityNumber, 
		CPI.SuffixID AS SuffixID, 
		Suffix
	FROM dbo.tblClaimantPersonalInfo AS CPI 
		INNER JOIN tblClaim C ON C.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit CL ON C.ClaimID = CL.ClaimID
		LEFT JOIN dbo.tblMatter m		ON m.MatterID = C.MatterID
		LEFT JOIN tblDocuments d			ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
		LEFT JOIN tblTravelersBill tb		ON d.TravelersBillID = tb.TravelersBillID
		LEFT JOIN @TravelersComparison tc	ON cpi.ClaimantPersonalInfoID = tc.ClaimantPersonalInfoID
		LEFT JOIN tblGender G				ON CPI.GenderID = G.GenderID
		INNER JOIN tblConfirmationStatus cs ON tc.ConfirmationStatusID = cs.ConfirmationStatusID
		LEFT JOIN tblSuffix Su				ON CPI.SuffixID = Su.SuffixID
	GROUP BY 
		CPI.ClaimantPersonalInfoID,  
		CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END, 
		CASE WHEN Matter IS NULL THEN '' ELSE Matter END, cl.DOFE, 
		Notes, 
		ConfirmationStatus,
		FirstName, 
		MiddleName, 
		LastName, 
		c.MatterID,
		Gender, 
		BirthDate, 
		DeceasedDate, 
		SocialSecurityNumber, 
		CPI.SuffixID, 
		Suffix 
	ORDER BY ClaimantName

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryClaimantsMatterMismatch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryClaimantsMatterMismatch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryClaimantsMatterMismatch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryClaimantsMatterMismatch] TO [power_user]
GO
