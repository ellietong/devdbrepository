SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_addDocument]
(
	@DocumentTypeID int,					--aka ClaimantDocumentTypeID
	@ClaimantPersonalInfoID bigint = null,
	@CreatedBy nvarchar(250),
	@DateCreated datetime,
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@SPDocId nvarchar(50),	
	@Comments nvarchar(4000) = null,			--comments are optional	
	@TravelersBillID int = null
	--@FirmID int = null,
	--@DocumentSourceID int,
	--@MatterID int = null,
	--These are null unless the document is an Invoice document
	----@InvoiceNumber nvarchar(50) = null,
	--@Period nvarchar(50) = null,
	--@DatePayable datetime = null,
	--@FeeAmount money = null,
	--@DisbursementAmount money = null,
 --   @Adjustment money = null,
 --   @AdjustmentNotes nvarchar(4000) = null,
 --   @StatusID int = null,
    --@VendorFirmID int = null,
    --@PrivateComments nvarchar(4000) = null,
    --@Approved bit = null,
    --@ApprovedBy nvarchar(250) = null,
    --@DateApproved datetime = null
)

AS
DECLARE @MaxDocID int
SET @MaxDocID = (SELECT IDENT_CURRENT('tblDocuments')+IDENT_INCR('tblDocuments'))

INSERT INTO tblDocuments (
	[ClaimantDocumentTypeID]
  ,[Comments]
  ,[DocumentURL]
  ,[DocumentTitle]
  ,[SPDocId]
  ,[CreatedBy]
  ,[DateCreated]
  ,[ModifiedBy]
  ,[DateModified]
  ,[TravelersBillID]
)
VALUES (	
	@DocumentTypeID,
	@Comments,
	REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
	@DocumentTitle,
	@SPDocId,
	@CreatedBy,
	@DateCreated,
	@CreatedBy, --ModifiedBy
	@DateCreated, --DateModified
	@TravelersBillID
)


SELECT @MaxDocID

INSERT INTO tblMultiClaimantDocument (ClaimantPersonalInfoID, DocumentID)
	VALUES(@ClaimantPersonalInfoID, @MaxDocID)
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_addDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_addDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_addDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_addDocument] TO [power_user]
GO
