SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryOutstandingAccountsReceivable]
--(@CarrierGroupID int)
AS
BEGIN
SET NOCOUNT ON;

	 --SET NOCOUNT ON added to prevent extra result sets from
	 --interfering with SELECT statements.

	--DECLARE @CarrierGroupID int = 10000

SELECT ar.AccountsReceivableID, ar.Amount AS ReceivableAmount, ar.TravelersBillID, ar.PolicyID, ar.Amount - COALESCE(arp.PaidAmount, 0) AS Remaining, 			
        p.PolicyNum, CASE WHEN d.ClaimantDocumentTypeID = 48 THEN tb.InvoiceNumber ELSE 'N\A' END AS InvoiceNumber, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 			
		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, FirstName, MiddleName, LastName, c.CarrierName, dca.defensecounsel AS FirmName, tb.DefenseCounselAddressID AS FirmID, d.ClaimantPersonalInfoID, ClaimantDocumentType,	
		  matter, PayeeType, InvoiceDate, BillDate	
    FROM tblAccountsReceivable ar			
    INNER JOIN tblPolicy p ON p.PolicyID = ar.PolicyID			
    INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID			
    INNER JOIN tblTravelersBill tb ON ar.TravelersBillID = tb.TravelersBillID			
    INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID			
    INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID			
    INNER JOIN tblClaimantPersonalInfo CPI on mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID			
    INNER JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantdocumentTypeID			
    INNER JOIN tblMatter ct ON tb.MatterID = ct.MatterID			
    LEFT JOIN tblPayeeType pt ON ar.PayeeTypeID = pt.PayeeTypeID			
    LEFT JOIN tblDefenseCounselAddress dca ON tb.DefenseCounselAddressID = dca.DefenseCounselAddressID			
    LEFT JOIN (SELECT AccountsReceivableID, SUM(Amount) AS PaidAmount FROM tblAccountsReceivablePayment GROUP BY AccountsReceivableID) arp ON ar.AccountsReceivableID = arp.AccountsReceivableID			
			WHERE tb.billstatusid<>2 AND ar.Amount - COALESCE(arp.PaidAmount, 0) <> 0	
			and pt.payeetypeid=1
and dca.DefenseCounsel  like '%Kaufman%'			
--and CarrierName like '%trav%' 	
and ClaimantDocumentType like '%exp%'		
--and tb.PaymentDueDate in ('11/14/2013')		
and tb.invoicenumber = '919438'	
--		and ar.AccountsReceivableID = 108933



END
GO
GRANT ALTER ON  [dbo].[WSS_qryOutstandingAccountsReceivable] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOutstandingAccountsReceivable] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOutstandingAccountsReceivable] TO [base_user]
GRANT ALTER ON  [dbo].[WSS_qryOutstandingAccountsReceivable] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOutstandingAccountsReceivable] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOutstandingAccountsReceivable] TO [power_user]
GO
