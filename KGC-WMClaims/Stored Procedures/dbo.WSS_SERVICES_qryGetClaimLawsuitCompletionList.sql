SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] 
(@prefixText nvarchar(MAX), @Emailaddress nvarchar(100), @count int = 500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 
 
DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
 
 
DECLARE @NoFilters bit
SET @NoFilters = (SELECT (/*dca.NoFilters |*/ vp.NoFilters) AS NoFilters FROM vtblPerson vp
					INNER JOIN tblDefenseCounselAddress dca ON vp.FirmAddressID = dca.DefenseCounselAddressID
					WHERE Email = @emailaddress)
 
IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1) --OR @FirmAddressID IN (SELECT DefenseCounselAddressID FROM tblDefenseCounselAddress WHERE DefenseCounselID IN(188)))
BEGIN
    SET ROWCOUNT @count
    -- Insert statements for procedure here
	SELECT DISTINCT cl.ClaimLawsuitID AS ClaimLawsuitID, CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
								  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + '' END	  + '  (' + ( DocketNumber + ' - ' + Matter + COALESCE(' - ' + DefendantCompany, '') + ')') AS DocketNumber--SELECT DISTINCT l.LawsuitID AS LawsuitID, DocketNumber + '  (' + (CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
	--							  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + '' END +')') AS DocketNumber
	FROM tblLawsuit L 
	INNER JOIN tblClaimLawsuit cl on L.LawsuitID = cl.LawsuitID
	--FROM tblClaimLawsuit cl
	INNER JOIN tblClaim c on cl.ClaimID = c.ClaimID
	INNER JOIN tblMatter m on c.MatterID = m.MatterID
	LEFT JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantSuffix cls ON cpi.SuffixID = cls.SuffixID 
	LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	LEFT JOIN tblDefendantCompany dc ON a.DefendantCompanyID = dc.DefendantCompanyID
	WHERE COALESCE(IsCurrentClaim, 0) = 1 AND cl.ClaimLawsuitID IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1 AND ClaimLawsuitStatusID IN (22, 39))
	AND LastName +', '+FirstName LIKE  COALESCE('%' + @PrefixText, '') + '%'--COALESCE('%' + 'ncc', '') + '%'
	ORDER BY DocketNumber
END
ELSE	
BEGIN
    SET ROWCOUNT @count
	SELECT DISTINCT cl.ClaimLawsuitID AS ClaimLawsuitID, CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
								  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + '' END	  + '  (' + ( DocketNumber + ' - ' + Matter + COALESCE(' - ' + DefendantCompany, '') + ')') AS DocketNumber--SELECT DISTINCT l.LawsuitID AS LawsuitID, DocketNumber + '  (' + (CASE WHEN Suffix IS NULL THEN cpi.LastName + ', ' ELSE cpi.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
	--							  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + '' END +')') AS DocketNumber
	FROM tblLawsuit L 
	INNER JOIN tblClaimLawsuit cl on L.LawsuitID = cl.LawsuitID
	--FROM tblClaimLawsuit cl
	INNER JOIN tblClaim c on cl.ClaimID = c.ClaimID
	INNER JOIN tblMatter m on c.MatterID = m.MatterID	
	LEFT JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaimantPersonalInfo cpi on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID and dj.matterid = c.matterid
	LEFT JOIN tblDefenseCounselAddress dca2 on dj.DefenseCounselAddressID=dca2.DefenseCounselAddressID 
	LEFT JOIN tblClaimantSuffix cls on cpi.SuffixID = cls.SuffixID 
	LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
	LEFT JOIN tblDefendantCompany dc ON a.DefendantCompanyID = dc.DefendantCompanyID
	WHERE COALESCE(IsCurrentClaim, 0) = 1 AND cl.ClaimLawsuitID IN (SELECT ClaimLawsuitID FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1 AND ClaimLawsuitStatusID IN (22, 39)) 
	/*AND*/ AND LastName +', '+FirstName LIKE  COALESCE('%' + @PrefixText, '') + '%'  
	AND (dj.DefenseCounselAddressID = @FirmAddressID OR l.DefenseCounselAddressID = @FirmAddressID) 
	ORDER BY DocketNumber
END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetClaimLawsuitCompletionList] TO [power_user]
GO
