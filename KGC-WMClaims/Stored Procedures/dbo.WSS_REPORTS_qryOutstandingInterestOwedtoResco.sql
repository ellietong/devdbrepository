SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[WSS_REPORTS_qryOutstandingInterestOwedtoResco]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
DECLARE @Payments TABLE (AccountsReceivableID int, TravelersBillID int, Amount money, Paid money, Difference money, RescoDueDate date, DatePaid date, CarrierBilled nvarchar(500))
INSERT INTO @Payments
SELECT ar.AccountsReceivableID, ar.TravelersBillID, ar.Amount, SUM(COALESCE(arp.Amount,0)) AS 'AR Payment Amount', SUM(COALESCE(ar.Amount, 0)) - SUM(COALESCE(arp.Amount, 0)), RescoDueDate, DatePaid, CarrierName
FROM tblAccountsReceivable ar
LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
INNER JOIN tblPolicy p on ar.PolicyID = p.PolicyID
INNER JOIN tblCarrier c on p.CarrierID = c.CarrierID
LEFT JOIN tblPayment pay on arp.PaymentID = pay.PaymentID
INNER JOIN tblTravelersBill tb on ar.TravelersBillID = tb.TravelersBillID
WHERE PayeeTypeID = 2 --AND coalesce(ar.RescoDueDate, tb.PaymentDueDate) < GETDATE() and coalesce(DatePaid, '1/1/2050') > coalesce(ar.RescoDueDate, tb.PaymentDueDate) 
GROUP BY ar.AccountsReceivableID, ar.Amount, RescoDueDate, CarrierName, ar.TravelersBillID, DatePaid
--SELECT * FROM @Payments

	--SELECT 
	--CASE WHEN (SettlementAmount IS NULL OR SettlementAmount = 0) AND InvoiceNumber IS NOT NULL THEN 'Invoice: ' + InvoiceNumber ELSE 'Settlement' END AS BillType,
	--	CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
	--	CASE WHEN FirstName IS NULL 
	--	THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName,
	--	Amount AS 'InitialBalance', Paid AS 'Amount Paid', RescoDueDate, DatePaid, CarrierBilled, ClaimType, Firm AS Payee,
	--	sum(Amount) * POWER(convert(float,1.1),DATEDIFF(day, coalesce(RescoDueDate, PaymentDueDate), coalesce(datepaid, getdate()))/365.25) - sum(Amount) as InterestAccrued,
	--sum(Amount) * POWER(convert(float,1.1),DATEDIFF(day, coalesce(RescoDueDate, PaymentDueDate), coalesce(datepaid, getdate()))/365.25) - coalesce(sum(Paid), 0) as RemainingBalance
	--FROM tblTravelersBill tb
	--INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
	--INNER JOIN tblClaimantPersonalInfo cpi ON d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	--LEFT JOIN tblSuffix s ON cpi.SuffixID = s.SuffixID
	--INNER JOIN @Payments pay ON tb.TravelersBillID = pay.TravelersBillID
	--INNER JOIN tblClaimType ct on tb.ClaimTypeID = ct.ClaimTypeID
	--INNER JOIN tblFirm f on tb.FirmID = f.FirmID
	--GROUP BY CarrierBilled, ClaimType, Firm, InvoiceNumber, RescoDueDate, DatePaid, LastName, FirstName, MiddleName, Suffix, cpi.SuffixID, Amount, Paid, PaymentDueDate, SettlementAmount
	--ORDER BY CarrierBilled, BillType
	
	SELECT 
	CASE WHEN (SettlementAmount IS NULL OR SettlementAmount = 0) AND InvoiceNumber IS NOT NULL THEN 'Invoice: ' + InvoiceNumber ELSE 'Settlement' END AS BillType,
		CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
		CASE WHEN FirstName IS NULL 
		THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName,
		Amount AS 'AmountPaidByResco', Paid AS 'AmountPaid', RescoDueDate, DatePaid, CarrierBilled, ClaimType, Firm AS Payee,
		CASE WHEN sum([Difference]) * POWER(convert(float,1.1),DATEDIFF(day, coalesce(RescoDueDate, PaymentDueDate), coalesce(datepaid, getdate()))/365.25) - sum([Difference]) < 0 THEN 0 ELSE sum([Difference]) * POWER(convert(float,1.1),DATEDIFF(day, coalesce(RescoDueDate, PaymentDueDate), coalesce(datepaid, getdate()))/365.25) - sum([Difference]) END as InterestAccrued,
	CASE WHEN sum([Difference]) * POWER(convert(float,1.1),DATEDIFF(day, coalesce(RescoDueDate, PaymentDueDate), coalesce(datepaid, getdate()))/365.25) - sum([Difference]) < 0  THEN sum(Amount) ELSE sum(Amount) * POWER(convert(float,1.1),DATEDIFF(day, coalesce(RescoDueDate, PaymentDueDate), coalesce(datepaid, getdate()))/365.25) - coalesce(sum(Paid), 0) END as RemainingBalance
	FROM tblTravelersBill tb
	INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
	INNER JOIN tblClaimantPersonalInfo cpi ON d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblSuffix s ON cpi.SuffixID = s.SuffixID
	INNER JOIN @Payments pay ON tb.TravelersBillID = pay.TravelersBillID
	INNER JOIN tblClaimType ct on tb.ClaimTypeID = ct.ClaimTypeID
	INNER JOIN tblFirm f on tb.FirmID = f.FirmID
	--WHERE	coalesce(ar.RescoDueDate, tb.PaymentDueDate) < GETDATE() and coalesce(DatePaid, '1/1/2050') > coalesce(ar.RescoDueDate, tb.PaymentDueDate) 
	GROUP BY CarrierBilled, ClaimType, Firm, InvoiceNumber, RescoDueDate, DatePaid, LastName, FirstName, MiddleName, Suffix, cpi.SuffixID, Amount, Paid, PaymentDueDate, SettlementAmount
	ORDER BY CarrierBilled, BillType	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingInterestOwedtoResco] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingInterestOwedtoResco] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingInterestOwedtoResco] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingInterestOwedtoResco] TO [power_user]
GO
