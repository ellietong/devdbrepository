SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantDocumentList]
	-- Add the parameters for the stored procedure here
@ClaimantPersonalInfoID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	----SET NOCOUNT ON;
	--DECLARE @ClaimantPersonalInfoID bigint = 9
    -- Insert statements for procedure here
	SELECT  DISTINCT   d.DocumentID, DocumentTitle
	FROM tblDocuments D          
		 LEFT JOIN tblClaimantPersonalInfo AS CPI ON CPI.ClaimantPersonalInfoID = D.ClaimantPersonalInfoID
		 /*INNER*/ LEFT JOIN tblClaimantDocumentType AS DT ON DT.ClaimantDocumentTypeID = D.ClaimantDocumentTypeID 
		 INNER JOIN tblMultiClaimantDocument mcd on mcd.DocumentID = d.DocumentID 
	WHERE mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND D.ClaimantDocumentTypeID NOT IN (48, 49)
	AND d.DocumentID NOT IN (SELECT DISTINCT COALESCE(InvoiceDocumentID, 0) FROM tblTravelersBill) --exclude bills & invoices
	UNION
	SELECT NULL, 'Silent'
	ORDER BY DocumentID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantDocumentList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantDocumentList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantDocumentList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantDocumentList] TO [power_user]
GO
