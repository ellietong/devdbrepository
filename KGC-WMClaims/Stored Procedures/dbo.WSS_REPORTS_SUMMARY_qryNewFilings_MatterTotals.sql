SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_MatterTotals] (@StartDate datetime, 
                                                                                      @EndDate datetime, 
                                                                                      @MatterID int)
AS
Set NoCount ON

--DECLARE @StartDate	datetime
--DECLARE @EndDate	datetime
--DECLARE @StateID	int
--DECLARE @Matterid	int

--SET @StartDate	= '1/1/2012'
--SET @EndDate	= '9/30/2012'
--SET @StateID	= null
--SET @Matterid	= null

-- Find Max ID Values
DECLARE @MaxStateID int
SELECT @MaxStateID = MAX(stateid) FROM tblState
DECLARE @maxMatterID INT
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter
DECLARE @MaxResponsibleDefenseCounselID INT

DECLARE @DiseaseCount TABLE (Matter nvarchar(100), MatterID int, State nvarchar(150), YearFiled datetime, monthFiledID int, monthFiledName nvarchar(100), statusdate datetime, MesoCount int, LungCancerCount int, OtherCancerCount int, 
							 NonMaligCount int,  UnknownCount int, UnknownCount1 int)
INSERT INTO @DiseaseCount
SELECT Matter, 
	C.MatterID, 
	FullNameState as state,  
	CASE WHEN Coalesce(servicedate, FileDate, receiveddate) is null then  '1/1/1900' ELSE CONVERT(datetime,('1/1/'+convert(nvarchar,year(Coalesce(servicedate, FileDate, receiveddate)))))END AS YearFiled, 
	MONTH(COALESCE(servicedate, FileDate, receiveddate, '1/1/1900')), 
	Datename(month, COALESCE(servicedate, FileDate, receiveddate, '1/1/1900')),
	COALESCE(statusdate,'1/1/1900'),
	Case when DMeso.HighLevelDiseaseID = 1 Then 1 Else 0 End AS MesoCount,
	Case when DLC.HighLevelDiseaseID = 2 Then 1 Else 0 End as LungCancerCount,
	Case when DOC.HighLevelDiseaseID = 3 Then 1 Else 0 End as OtherCancerCount,
	Case when DNM.HighLevelDiseaseID IN (4, 7) Then 1 Else 0 End as NonMaligCount,
	Case when DUnk.HighLevelDiseaseID IN (5, 6) Then 1 Else 0 End AS UnknownCount,
	Case when DUnk.HighLevelDiseaseID is NULL Then 1 Else 0 End AS UnknownCount1
FROM tblClaim C 
	LEFT JOIN (SELECT * 
			   FROM tblClaimDisease CD 
			   WHERE PrimaryDisease = 1) CD		ON CD.ClaimID = C.ClaimID
	LEFT JOIN tblMatter M						ON c.MatterID = M.MatterID
	LEFT JOIN tblDisease DMeso					ON CD.DiseaseID = DMeso.DiseaseID
	LEFT JOIN tblDisease DLC					ON CD.DiseaseID = DLC.DiseaseID
	LEFT JOIN tblDisease DOC					ON CD.DiseaseID = DOC.DiseaseID
	LEFT JOIN tblDisease DNM					ON CD.DiseaseID = DNM.DiseaseID
	LEFT JOIN tblDisease DSC					ON CD.DiseaseID = DSC.DiseaseID
	LEFT JOIN tblDisease DND					ON CD.DiseaseID = DND.DiseaseID
	LEFT JOIN tblDisease DUnk					ON CD.DiseaseID = DUnk.DiseaseID
	LEFT JOIN tblClaimLawsuit CL				ON C.ClaimID = CL.ClaimID
	LEFT JOIN tblLawsuit L						ON CL.LawsuitID = L.LawsuitID
	LEFT JOIN tblState S						ON L.StateID = S.StateID
	LEFT JOIN tblClaimLawsuitStatusTracking cst ON cst.ClaimLawsuitID = cl.ClaimLawsuitID
WHERE  /*s.StateID >= COALESCE(@StateID, 0) 
	AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID)*/ 
	--AND IsPrimaryForClaim = 1 
	/*AND*/ coalesce(cst.IsPrimaryStatus, 0) = 1
	AND C.MatterID >= COALESCE(@MatterID, 0) 
	AND C.MatterID <= COALESCE(NULLIF(@MatterID,0), @maxMatterID)
	--AND (cst.ClaimLawsuitStatusID IN (7, 8)  
	/*AND coalesce(processeddate, '1/1/1900') >= @StartDate
	AND coalesce(processeddate, '1/1/1900') <= @endDate*/--)
	
 SELECT Matter, 
	MatterID, 
	Sum(MesoCount) AS MesoFilings,
	Sum(LungCancerCount) AS LCFilings, 
	Sum(OtherCancerCount) AS OCFilings,
	Sum(NonMaligCount) AS NMFilings, 
	Sum(UnknownCount+UnknownCount1) AS UnknownFilings, 
	Sum(UnknownCount+UnknownCount1+MesoCount+LungCancerCount+OtherCancerCount+NonMaligCount) AS TotalCount
FROM @DiseaseCount
WHERE coalesce(yearfiled, '1/1/1900') BETWEEN COALESCE(@StartDate, '1/1/1900') AND COALESCE(@EndDate, GETDATE())
GROUP BY Matter, 
	MatterID
ORDER BY Matter
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_MatterTotals] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_MatterTotals] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_MatterTotals] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_MatterTotals] TO [power_user]
GO
