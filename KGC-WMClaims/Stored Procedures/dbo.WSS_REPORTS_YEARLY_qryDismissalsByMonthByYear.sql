SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByMonthByYear] 
--(@MatterID int = NULL, 
--                                                                  @YearID   int, 
--                                                                  @MonthID  int) 
                                                                  
	(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null)
                                                                  
AS
SET NOCOUNT ON
	
----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      AND all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--DECLARE @year int
--DECLARE @Month int
--SET @year = @YearID
--SET @Month = @MonthID

SELECT DATENAME("mm", ProcessedDate) AS [MonthName], 
	COUNT(DISTINCT(cl.ClaimLawsuitID)) AS 'Count', 
	MONTH(ProcessedDate) AS MonthID
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT  JOIN tblState s                         ON l.StateID = s.StateID
WHERE  COALESCE(Statusdate,ProcessedDate)BETWEEN COALESCE(@StartDate,'1/1/1900')
		AND COALESCE(@EndDate, '1/1/2100')
	AND ClaimLawsuitStatusID IN (15,16,17,19,36,27) 
	AND IsPrimaryForClaim = 1 
	AND IsPrimaryStatus = 1
	AND MatterID  >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
GROUP BY DATENAME("mm", ProcessedDate), MONTH(ProcessedDate)
ORDER BY MONTH(ProcessedDate)


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByMonthByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByMonthByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByMonthByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryDismissalsByMonthByYear] TO [power_user]
GO
