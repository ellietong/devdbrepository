SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdClaimant]
	-- Add the parameters for the stored procedure here
	(@ClaimantPersonalInfoID int, @FirstName nvarchar (250), @MiddleName nvarchar (250), @LastName nvarchar (250), @ClaimTypeID int, @DOFE datetime, @Notes nvarchar (1000), @SuffixID int, @ClaimTypeSource int, @CTSourcePageNum nvarchar(50), @DofeSource int, @DofeSourcePageNum nvarchar(50), @Emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 --   -- Insert statements for procedure here
	--UPDATE tblClaimantPersonalInfo
 --   SET FirstName = UPPER(@FirstName),
	--	MiddleName = UPPER(@MiddleName),
	--	LastName = UPPER(@LastName),
	--	ClaimTypeID = @ClaimTypeID, 
	--	DOFE =@DOFE,
	--	Notes = @Notes,
	--	SuffixID = @SuffixID,
	--	ClaimTypeSource = @ClaimTypeSource,
	--	ClaimTypeSourcePageNum = @CTSourcePageNum,
	--	DofeSource = @DofeSource,
	--	DofeSourcePageNum = @DofeSourcePageNum,
	--	SPUsername = @Emailaddress--,
	--	--DOFESourceURL = (SELECT DocumentURL FROM tblDocuments WHERE DocumentID = @DofeSource),
	--	--CTSourceURL = (SELECT DocumentURL FROM tblDocuments WHERE DocumentID = @ClaimTypeSource)
	--WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimant] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimant] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimant] TO [power_user]
GO
