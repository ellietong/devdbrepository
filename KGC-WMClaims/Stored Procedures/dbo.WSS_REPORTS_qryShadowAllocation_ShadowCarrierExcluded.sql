SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryShadowAllocation_ShadowCarrierExcluded] 
AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @PreviousExhaustionTravelers TABLE (PolicyID int, Exhaustion money, BilledAmount money)
	INSERT INTO @PreviousExhaustionTravelers
	SELECT	ar.PolicyID, 
			Sum(Case	When PaymentType like 'Expense' and ProvisionValueID in (2) Then Amount
						When PaymentType like 'Indemnity' Then Amount else 0 End) As Amount, 
						Sum(Amount ) As BilledAmount
	FROM tblTravelersBill tb
		INNER JOIN (select * from tblAccountsReceivable where PayeeTypeID <> 2) ar on tb.TravelersBillID=ar.TravelersBillID
		INNER JOIN tblPolicy p on ar.PolicyID = p.PolicyID
		INNER JOIN (Select ProvisionLabelID, ProvisionValueID, PolicyID From tblprovision where provisionlabelID = 1) pro on p.PolicyID =pro.policyID
	WHERE tb.AllocationQCConfirmation = 1 and BillStatusID <> 2 
	GROUP BY ar.PolicyID



	DECLARE @PaymentsMade TABLE (PolicyID int, TotalPayments money, TotalReceivable money)
	INSERT INTO @PaymentsMade
	SELECT ar.PolicyID,  SUM(COALESCE(case when pay.CarrierGroupID = 0 and ar.PolicyID <> 255 then 0 else arp.Amount end, 0)), sum(ar.Amount)
	FROM tblAccountsReceivablePayment arp
	INNER JOIN tblAccountsReceivable ar ON arp.AccountsReceivableID = ar.AccountsReceivableID
    inner join tblPayment pay on pay.PaymentID=arp.PaymentID
	GROUP BY ar.PolicyID

	DECLARE @PreviousLondonInsolvent TABLE (PolicyID int, InsolventPortion money)
	insert into @PreviousLondonInsolvent
	select p.PolicyID, sum(RespreadIndemnity + case when ProvisionValueID = 2 then RespreadDefense + RespreadDefenseOutsideLimits else 0 end)
	FROM tblTravelersBill tb
		INNER JOIN tblOccurrence o on tb.TravelersBillID= o.TravelersBillID
		inner join (select * from m_tblAllocationResultsLondonRespread where AllocationID = 1)  lr on lr.occurrenceid = o.occurrenceid
		INNER JOIN tblPolicy p on lr.londonpolicyid = p.PolicyID
		INNER JOIN (Select ProvisionLabelID, ProvisionValueID, PolicyID From tblprovision where provisionlabelID = 1) pro on p.PolicyID =pro.policyID
	WHERE tb.AllocationQCStatusID = 1 and BillStatusID <> 2 
	GROUP BY p.PolicyID	

	
	SELECT p.policyid,
				CASE WHEN InsuranceProgramID =1 THEN 'Schedule A1'
				WHEN InsuranceProgramID in (2,5,6,7) THEN 'Schedule A2'
				WHEN InsuranceProgramID in (3) THEN 'Schedule A3'
				WHEN InsuranceProgramID in (4) THEN 'Schedule A4' END AS Schedule,
		CarrierName, 
		PolicyNum, 
		PStartDate, 
		PEndDate, 
		PerOccLimit as AvailableLimit, 
		Coalesce(pet.BilledAmount,0) as AmountBilled, 
		Coalesce(pet.billedAmount,0) as ExhaustionTravelers,
		PerOccLimit-Coalesce(pet.Exhaustion,0) - coalesce(ins.insolventportion, 0) as RemainingLimitTravelers, 
		COALESCE(pm.TotalPayments,0) AS TotalPayments,
		Coalesce(pet.BilledAmount,0) - COALESCE(pm.TotalPayments,0) AS TotalReceivable
	FROM tblPolicy p
	INNER JOIN tblPolicyDates pd				ON p.PolicyID = pd.PolicyID
	INNER JOIN tblCarrier c						ON p.CarrierID = c.CarrierID
	INNER JOIN tblPolicyLimits pl				ON pl.PolicyID = p.PolicyID
	LEFT JOIN @PreviousExhaustionTravelers pet	ON pet.PolicyID = p.PolicyID
	LEFT JOIN @PaymentsMade pm					ON pm.PolicyID = p.PolicyID
	LEFT JOIN @PreviousLondonInsolvent ins		on ins.PolicyID=p.PolicyID
	WHERE p.CarrierID <> 1002 --and p.PolicyID <>255
	Order By	Schedule, 
				PStartDate, 
				AttachmentPoint

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryShadowAllocation_ShadowCarrierExcluded] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryShadowAllocation_ShadowCarrierExcluded] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryShadowAllocation_ShadowCarrierExcluded] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryShadowAllocation_ShadowCarrierExcluded] TO [power_user]
GO
