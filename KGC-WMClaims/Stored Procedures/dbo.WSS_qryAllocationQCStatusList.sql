SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryAllocationQCStatusList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AllocationQCStatusID, AllocationQCStatus
	FROM tblAllocationQCStatus
	UNION
	SELECT 0, ''
	ORDER BY AllocationQCStatus
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAllocationQCStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAllocationQCStatusList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAllocationQCStatusList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAllocationQCStatusList] TO [power_user]
GO
