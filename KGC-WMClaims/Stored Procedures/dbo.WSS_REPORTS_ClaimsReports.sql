SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_REPORTS_ClaimsReports]
(@LawsuitStatusID int,  @StateID int, @JurisdictionID int, @ClaimTypeID int)
AS
BEGIN
 
------VARIABLES FOR TESTING----
--DECLARE @LawsuitStatusID int
--DECLARE @StateID int
--DECLARE @JurisdictionID int
--DECLARE @ClaimTypeID int
--SET @LawsuitStatusID = NULL
--SET @StateID = NULL
--SET @JurisdictionID = NULL
----SET @emailaddress = 'youngc@kcicllc.com'
--SET @ClaimTypeID = NULL
 
--Set up ranges for use in WHERE statements below.
DECLARE @MaxStateID int
DECLARE @MaxJurisdictionID int
DECLARE @MaxLawsuitStatusID int
DECLARE @MaxClaimTypeID int
SET @MaxStateID = (SELECT MAX(StateID) FROM tblState)
SET @MaxJurisdictionID = (SELECT MAX(JurisdictionID) FROM tblJurisdiction)
SET @MaxLawsuitStatusID = (SELECT MAX(ClaimLawsuitStatusID) FROM tblClaimLawsuitStatus)
SET @MaxClaimTypeID = (SELECT MAX(MatterID) FROM tblMatter)
 
 
--Prepare a comma-delineated list of diseases by ClaimID for use in reporting.
DECLARE @ColumnListComma nvarchar(MAX)
DECLARE @ColumnListPlus nvarchar(MAX)
DECLARE @Command nvarchar(MAX)
SET @ColumnListComma = (
	SELECT LEFT(DiseaseList, LEN(DiseaseList)-1) FROM (
		SELECT  DISTINCT
					'[' + Disease + ']' + ', ' AS [text()] 
				FROM 
					tblDisease
				FOR XML PATH('')  ) dl (DiseaseList))   
SET @ColumnListPlus = (
	SELECT LEFT(DiseaseList, LEN(DiseaseList)-1) FROM (
		SELECT DISTINCT
					'CASE WHEN COALESCE([' + Disease + '], 0) = 1 THEN ''' + REPLACE(Disease, '''', '"') + ', '' ELSE '''' END + ' AS [text()] 
				FROM 
					tblDisease
				FOR XML PATH('')  ) dl (DiseaseList))        
SET @Command = 'SELECT ClaimantLawsuitID, ' + @ColumnListPlus  +
' AS Diseases FROM
(SELECT DISTINCT cd.ClaimantLawsuitID, Disease, COUNT(cd.ClaimantLawsuitDiseaseID) AS DiseaseCount FROM tblDisease d
LEFT JOIN (SELECT * FROM tblClaimLawsuitDisease) cd ON cd.DiseaseID = d.DiseaseID
GROUP BY cd.ClaimantLawsuitID, Disease
HAVING COUNT(cd.ClaimantLawsuitDiseaseID) > 0
)
    AS dlist
PIVOT
(
    Sum(DiseaseCount)
FOR
Disease
    IN ( ' + @ColumnListComma + ')
) AS piv
WHERE ClaimantLawsuitID IS NOT NULL'
 
--This temp table pulls the disease information from the pivot table query created above, putting them in a form usable by the queries below.
IF object_id('tempdb..#ClaimDisease') IS NOT NULL
BEGIN
   DROP TABLE #ClaimDisease
END
CREATE TABLE #ClaimDisease (ClaimantLawsuitID bigint, Diseases nvarchar(MAX))
INSERT INTO #ClaimDisease 
	EXEC(@Command)
 
--SELECT * FROM #ClaimDisease
 
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE COALESCE(ls.LawsuitStatusGroupID,0) <> 2'
IF @LawsuitStatusID			  IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND cl.LawsuitStatusID >= ' + CONVERT(nvarchar(20),COALESCE(@LawsuitStatusID, 0)) + ' AND cl.LawsuitStatusID <= ' + CONVERT(nvarchar(20),COALESCE(@LawsuitStatusID, @MaxLawsuitStatusID)) END
IF @StateID					  IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.StateID >= ' + CONVERT(nvarchar(20),COALESCE(@StateID, 0)) + ' AND l.StateID <= ' + CONVERT(nvarchar(20),COALESCE(@StateID, @MaxStateID)) END
IF @JurisdictionID			  IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.JurisdictionID >= ' + CONVERT(nvarchar(20),COALESCE(@JurisdictionID, 0)) + ' AND l.JurisdictionID <= ' + CONVERT(nvarchar(20),COALESCE(@JurisdictionID, @MaxJurisdictionID)) END
IF @ClaimTypeID				  IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND cpi.ClaimTypeID >= ' + CONVERT(nvarchar(20),COALESCE(@ClaimTypeID, 0)) + ' AND cpi.ClaimTypeID <= ' + CONVERT(nvarchar(20),COALESCE(@ClaimTypeID, @MaxClaimTypeID)) END
 
 
--------------These IF statements determine additional WHERE statements by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
------------SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),@FirmAddressID) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),@FirmAddressID) + ')'
 
--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT cl.ClaimantLawsuitID, null as ClaimLawsuitStatus, null as NCCReferenceID, cpi.ClaimantPersonalInfoID, s.State, j.Jurisdiction,
		cpi.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL 
		THEN '''' ELSE MiddleName + '' '' END AS ClaimantName, null AS SSN, 
		BirthDate, cpi.DeceasedDate, ls.LawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, 
		l.DocketNumber, l.CaseCaption, l.LeadPlaintiffLastName + COALESCE('', '' + l.LeadPlaintiffFirstName,'''') AS LeadPlaintiffName, 
		null as PlaintiffCounsel, null AS PrimaryDefenseCounsel,   null AS ActiveTrialDate, cpi.ClaimTypeID, ClaimType,
		StatusDate AS ResolutionDate, NULL AS Occupation, LEFT(cd1.Diseases, LEN(cd1.Diseases)-1) AS Diseases, SSN, DOFE, SettlementDate, SettlementAmount
	 FROM tblClaimantPersonalInfo cpi 
		LEFT JOIN tblClaimantLawsuit cl ON cpi.ClaimantPersonalInfoID = cl.ClaimantPersonalInfoID 
		LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID 
 
		LEFT JOIN  tblState s ON s.StateID = l.StateID 
		LEFT JOIN  tblJurisdiction j ON j.JurisdictionID = l.JurisdictionID
		LEFT JOIN tblClaimType ct ON cpi.ClaimTypeID = ct.ClaimTypeID
 
		LEFT JOIN tblLawsuitStatus AS ls ON Ls.LawsuitStatusID = cl.LawsuitStatusID
 
		----LEFT JOIN  (SELECT * FROM tblClaimantEmployment WHERE COALESCE(IsPrimary, 0) = 1) ce ON cpi.ClaimantPersonalInfoID = ce.ClaimantPersonalInfoID
 
		LEFT JOIN  #ClaimDisease cd1 ON cl.ClaimantLawsuitID = cd1.ClaimantLawsuitID ' +
	 @WhereString + '
	 OPTION (RECOMPILE)'
 
PRINT @QueryString
--Finally, the query created above is executed.
EXEC (@QueryString)
 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_ClaimsReports] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_ClaimsReports] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_ClaimsReports] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_ClaimsReports] TO [power_user]
GO
