SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_INVOICE_qryExpertInvoiceFirmFilterList]
AS
SET NOCOUNT ON

--SELECT ExpertFirmID, ExpertFirm 
--FROM m_tblExpertFirm
--UNION
--SELECT null, '<< All Expert Firms >>'
--ORDER BY ExpertFirm

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryExpertInvoiceFirmFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryExpertInvoiceFirmFilterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryExpertInvoiceFirmFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryExpertInvoiceFirmFilterList] TO [power_user]
GO
