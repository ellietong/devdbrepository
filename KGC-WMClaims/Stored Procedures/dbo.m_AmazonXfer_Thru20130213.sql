SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_AmazonXfer_Thru20130213]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT d.DocumentID, DocumentURL, REPLACE(REPLACE(DocumentTitle, '\', ''), '/', '') AS DocumentTitle,
			d.ClaimantDocumentTypeID,REPLACE(REPLACE(ClaimantDocumentType, '\', '_'), '/', '_') AS ClaimantDocumentType,
			 AmazonKey
    FROM tblDocuments d
    INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
    INNER JOIN (SELECT MIN(DocumentID) AS DocID, DocumentURL AS DocURL
			 FROM tblDocuments 
			 GROUP BY DocumentURL) doc ON d.DocumentID	 = doc.DocID
   -- INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID=mcd.DocumentID
    WHERE AmazonKey IS NULL 
    --AND DocumentURL IS NOT NULL
   -- AND DateCreated < CONVERT(date, GETDATE())
   --AND DocumentID NOT IN (6159,7558,8315,8349, 8483)--(3985, 4394,6159,6215,6510)
   -- AND LEFT(DocumentURL, 9) = '/archive/'
    
    GROUP BY d.DocumentID, DocumentURL, REPLACE(REPLACE(DocumentTitle, '\', ''), '/', '') ,
			d.ClaimantDocumentTypeID,REPLACE(REPLACE(ClaimantDocumentType, '\', '_'), '/', '_'),
			 AmazonKey
    ORDER BY DocumentID ASC
  END
  --SELECT * FROM tblDocuments WHERE DocumentID = 8483
--SELECT * FROM tblDocuments WHERE DocumentURL = '/archive/iuna_nosroc/Historical Documents/PACEDOCS/ae678a7d-0747-46ec-9b48-7a01ae100e0c.tiff'
--  SELECT * FROM tblDocuments WHERE DocumentID= 2037816
--SELECT * FROM tblDocuments WHERE AmazonKey = 'PRODUCTION/CLAIMSADMIN/MOTIONS/a57009e1-9335-46f3-958f-8e310fc34866.tiff'

--/docs/wm/Shared Documents/Vorys Sater Seymour Pease/8483_Mercy Medical Center Medical Records 3.pdf
--SELECT COUNT(DISTINCT DocumentURL) FROM tblDocuments WHERE AmazonKey IS NULL AND LEFT(DocumentURL, 9) = '/archive/'
GO
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonXfer_Thru20130213] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_AmazonXfer_Thru20130213] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_AmazonXfer_Thru20130213] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_AmazonXfer_Thru20130213] TO [power_user]
GO
