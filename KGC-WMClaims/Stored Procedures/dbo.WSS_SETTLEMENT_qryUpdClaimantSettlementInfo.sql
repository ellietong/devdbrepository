SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo]
	-- Add the parameters for the stored procedure here
(@ClaimantPersonalInfoID int, @ClaimLawsuitID int, @LastName nvarchar(250), @FirstName nvarchar(250),
@MiddleName nvarchar(250), @SuffixID int, @SettlementAmount money, @SettlementDate date, @PaymentDueDate date,
@NotificationDate date, @TaxID nvarchar(250) = null, @EmailAddress nvarchar(250), @PlaintiffCounselAddressID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @LawsuitID int
SET @LawsuitID = (SELECT LawsuitID FROM tblClaimLawsuit WHERE ClaimLawsuitID = @ClaimLawsuitID)

UPDATE tblClaimantPersonalInfo
SET FirstName = UPPER(@FirstName),
	MiddleName = UPPER(@MiddleName),
	LastName = UPPER(@LastName),
	SuffixID = @SuffixID,
	SPUsername = @EmailAddress
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID

IF @ClaimLawsuitID IN (Select ClaimLawsuitID FROM tblClaimantSettlement)
BEGIN
UPDATE tblClaimantSettlement
SET SettlementAmount = @SettlementAmount,
	SettlementDate = @SettlementDate,
	SettlementDueDate = @PaymentDueDate,
	SettlementNotificationDate = @NotificationDate,
	SPUsername = @EmailAddress
WHERE ClaimLawsuitID = @ClaimLawsuitID
END
ELSE
BEGIN
INSERT INTO tblClaimantSettlement (ClaimLawsuitID, SettlementAmount, SettlementDueDate, SettlementDate, SPUsername)
VALUES (@ClaimLawsuitID, @SettlementAmount, @PaymentDueDate, @SettlementDate, @EmailAddress)
END

UPDATE tblLawsuit	
SET PlaintiffCounselAddressID = @PlaintiffCounselAddressID,
	SPUsername = @EmailAddress
WHERE LawsuitID = @LawsuitID	

UPDATE tblClaimLawsuitStatusTracking
SET IsPrimaryStatus = 0,
	SPUsername = @EmailAddress
WHERE ClaimLawsuitID = @ClaimLawsuitID

INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, ProcessedDate, SPUsername)
VALUES(@ClaimLawsuitID, 23, @SettlementDate, 1, convert(date,GETDATE()), @EmailAddress)

select * from tblClaimLawsuitStatus
 	

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [power_user]
GO
