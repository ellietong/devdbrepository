SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qrySettlementDocCategoryList]
AS
SET NOCOUNT ON

SELECT DocCategoryID, 
	DocCategory 
FROM tblDocumentCategory
--WHERE DocCategoryID <> 10
UNION
SELECT NULL, '-- Select a Category --'
ORDER BY DocCategory

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocCategoryList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocCategoryList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocCategoryList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocCategoryList] TO [power_user]
GO
