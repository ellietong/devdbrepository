SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIM_qryDisease]
AS

SELECT DiseaseID, 
	Disease + ' (ICD9: ' + COALESCE(CONVERT(nvarchar(10),ICD9),'N/A') + '), ' + HighLevelDisease AS Disease, 
	'1' as orderflag
FROM tblDisease dn
	LEFT JOIN tblHighLevelDisease hld ON dn.HighLevelDiseaseID = hld.HighLevelDiseaseID 

UNION
SELECT NULL, 
	'', 
	0
ORDER BY orderflag, Disease

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDisease] TO [power_user]
GO
