SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryPayeeTypeList]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PayeeTypeID, PayeeType
	FROM tblPayeeType
	UNION 
	SELECT NULL, ''
	ORDER BY PayeeType
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryPayeeTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryPayeeTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryPayeeTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryPayeeTypeList] TO [power_user]
GO
