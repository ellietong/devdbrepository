SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryNewClaimantFullName] 
	-- Add the parameters for the stored procedure here
	@EmailAddress nvarchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--DECLARE @MaxCPID int
--SET @MaxCPID = (SELECT IDENT_CURRENT('tblClaimantPersonalInfo')+IDENT_INCR('tblClaimantPersonalInfo'))
--SELECT @MaxCPID
SELECT CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + '' END + CASE WHEN MiddleName IS NULL THEN '' ELSE + ' ' + MiddleName END + ' ' + LastName AS FullClaimantName
FROM tblClaimantPersonalInfo
WHERE ClaimantPersonalInfoID = (SELECT MAX(ClaimantPersonalInfoID) FROM tblClaimantPersonalInfo)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryNewClaimantFullName] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryNewClaimantFullName] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryNewClaimantFullName] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryNewClaimantFullName] TO [power_user]
GO
