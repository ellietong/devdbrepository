SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInsClaim]
	(@DiseaseID int = NULL, 
	@DiseaseDiagnosisDate datetime = NULL, @Notes nvarchar(1000) = NULL,
	@DocketNumber nvarchar(100) = NULL,
	@CaseCaption nvarchar(1000) = NULL,
	@LeadPlaintiffLastName nvarchar(100) = NULL,
	@LeadPlaintiffFirstName nvarchar(100) = NULL,
	@JurisdictionID int = NULL, 
	@StateID int = NULL,
	@FileDate datetime = NULL,
	@ServiceDate datetime = NULL,
	@StatusDate datetime = NULL,
	@LawsuitStatusID int = NULL,
	@ClaimantPersonalInfoID bigint,
	@LawsuitID int = NULL,
	@SettlementAmount smallmoney = NULL,
	@SettlementDate datetime = NULL,
	@FirmID int = NULL, 
	@Emailaddress nvarchar(250))
AS
--BEGIN


--INSERT INTO tblLawsuit (DocketNumber, CaseCaption, LeadPlaintiffFirstName, LeadPlaintiffLastName, JurisdictionID, FileDate, ServiceDate, StateID, FirmID, SPUsername) --SettlementDate
--VALUES ((UPPER(@DocketNumber)), (UPPER(@CaseCaption)), (UPPER(@LeadPlaintiffFirstName)), (UPPER(@LeadPlaintiffLastName)), 
--		(@JurisdictionID), (@FileDate), (@ServiceDate), (@StateID), (@FirmID), (@Emailaddress)) --SettlementDate	
		
--INSERT INTO tblClaimantLawsuit (ClaimantPersonalInfoID, LawsuitID, SettlementAmount, SettlementDate, LawsuitStatusID, StatusDate, SPUsername)
--VALUES ((@ClaimantPersonalInfoID), CASE WHEN @LawsuitID IS NOT NULL THEN @LawsuitID ELSE (SELECT MAX(LawsuitID) FROM tblLawsuit) END, @SettlementAmount, @SettlementDate, @LawsuitStatusID, @StatusDate, @Emailaddress)

----INSERT INTO tblClaimLawsuitDisease (ClaimantLawsuitID, DiseaseID, DiagnosisDate, Notes)
----VALUES ((SELECT MAX(ClaimantLawsuitID) FROM tblClaimantLawsuit), (@DiseaseID), (@DiseaseDiagnosisDate), (UPPER(@Notes)))
		
--END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsClaim] TO [power_user]
GO
