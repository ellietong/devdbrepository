SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryClaimDisease] 
	-- Add the parameters for the stored procedure here
	@ClaimID bigint = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimDiseaseID, CD.DiseaseID, Disease, DiseaseDiagnosisDate as DiseaseDiagnosisDate, Notes, CASE WHEN PrimaryDisease = 1 THEN 'YES' ELSE 'NO' END AS PrimaryDisease, PrimaryDisease AS PrimaryDiseaseID  
	FROM tblClaimDisease CD
	LEFT JOIN (select * from tblDisease union select * from tbldisease_alt) ddis on ddis.DiseaseID = CD.DiseaseID
	WHERE ClaimID = @ClaimID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryClaimDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryClaimDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryClaimDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryClaimDisease] TO [power_user]
GO
