SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaims] (@MatterID int)
AS
SET NOCOUNT ON

/*---Debugging---*/
--DECLARE @MatterID INT
--SET @MatterID = NULL
/*---End Debugging---*/

DECLARE @MaxMatterID INT
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @RawComplaints TABLE (	ClaimID		INT,
								rcMatterID	INT,
								Matter		NVARCHAR(100),
								StatusDate	DATETIME,
								ClaimLawsuitStatusID INT,
								DiseaseID	INT,
								DiseaseID_Alt INT,
								HighLevelDiseaseID INT,
								State NVARCHAR(100),
								PlaintiffCounsel NVARCHAR(200)
							  )
INSERT	INTO @RawComplaints
	SELECT  c.ClaimID,
			c.MatterID AS rcMatterID,
			m.Matter,
			MAX(StatusDate) AS MaxStatusDate,
			clst.ClaimLawsuitStatusID,
			d.DiseaseID,
			da.DiseaseID_Alt,
			--COALESCE(hld.HighLevelDisease,'Unknown') AS HighLevelDisease,
			COALESCE(hld.HighLevelDiseaseID,6),
			CASE	WHEN L.StateID=45 AND L.JurisdictionID IN (386,387)
						THEN 'Illinois (Madison)'
					WHEN L.StateID=45 AND L.JurisdictionID NOT IN (386,387)
						THEN 'Illinois (Other)'
				ELSE COALESCE(S.FullNameState, 'Unknown')
			END AS 'State',
			COALESCE(PlaintiffCounsel, 'Unknown') AS PlaintiffCounsel
	FROM	tblClaim c 
		INNER JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblMatter m							ON c.MatterID = m.MatterID
		LEFT JOIN tblClaimLawsuitStatusTracking	clst	ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls				ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN (select * from tblClaimDisease clmd WHERE clmd.primarydisease = 1) cd on c.claimid=cd.claimid
		LEFT JOIN tblDisease d							ON cd.DiseaseID = d.DiseaseID
		LEFT JOIN tblDisease_ALT da						ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT JOIN tblHighLevelDisease hld				ON d.HighLevelDiseaseID = hld.HighLevelDiseaseID OR da.HighLevelDiseaseID = hld.HighLevelDiseaseID
		LEFT JOIN tblLawsuit l							ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN tblState s							ON l.StateID = s.StateID
		LEFT JOIN tblPlaintiffCounselAddress pca		ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE IsPrimaryStatus = 1
		AND clst.CLaimLawsuitStatusID IN (22,29)
		AND cl.IsPrimaryForClaim = 1
		AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(@MatterID, @MaxMatterID)
	
	GROUP BY c.ClaimID,
			 c.MatterID,
			 m.Matter,
			 clst.ClaimLawsuitStatusID,
			 d.DiseaseID,
			 da.DiseaseID_Alt,
			 hld.HighLevelDiseaseID,
			 (CASE	WHEN L.StateID=45 AND L.JurisdictionID IN (386,387)
						THEN 'Illinois (Madison)'
					WHEN L.StateID=45 AND L.JurisdictionID NOT IN (386,387)
						THEN 'Illinois (Other)'
				ELSE COALESCE(S.FullNameState, 'Unknown')
			END),
			COALESCE(PlaintiffCounsel, 'Unknown')
	ORDER BY c.ClaimID

	SELECT RawComplaints.Matter,
			RawComplaints.rcMatterID,
			(SELECT COUNT(DISTINCT(rc.ClaimID)) FROM @RawComplaints AS rc WHERE rc.HighLevelDiseaseID = 1 AND rc.rcMatterID=RawComplaints.rcMatterID) AS MesoCount,
			(SELECT COUNT(DISTINCT(rc.ClaimID)) FROM @RawComplaints AS rc WHERE rc.HighLevelDiseaseID = 2 AND rc.rcMatterID=RawComplaints.rcMatterID) AS LungCancerCount,
			(SELECT COUNT(DISTINCT(rc.ClaimID)) FROM @RawComplaints AS rc WHERE rc.HighLevelDiseaseID = 3 AND rc.rcMatterID=RawComplaints.rcMatterID) AS OtherCancerCount,
			(SELECT COUNT(DISTINCT(rc.ClaimID)) FROM @RawComplaints AS rc WHERE rc.HighLevelDiseaseID IN (4,7,8) AND rc.rcMatterID=RawComplaints.rcMatterID) AS NonMaligCount,
			(SELECT COUNT(DISTINCT(rc.ClaimID)) FROM @RawComplaints AS rc WHERE rc.HighLevelDiseaseID IN (5,6,NULL) AND rc.rcMatterID=RawComplaints.rcMatterID) AS UnknownCount,
			(SELECT COUNT(DISTINCT(rc.ClaimID)) FROM @RawComplaints AS rc WHERE rc.HighLevelDiseaseID = 1 AND rc.rcMatterID=RawComplaints.rcMatterID)
				+  (SELECT COUNT(DISTINCT(rc.ClaimID)) FROM @RawComplaints AS rc WHERE rc.HighLevelDiseaseID = 2 AND rc.rcMatterID=RawComplaints.rcMatterID)
				+  (SELECT COUNT(DISTINCT(rc.ClaimID)) FROM @RawComplaints AS rc WHERE rc.HighLevelDiseaseID = 3 AND rc.rcMatterID=RawComplaints.rcMatterID)
				+  (SELECT COUNT(DISTINCT(rc.ClaimID)) FROM @RawComplaints AS rc WHERE rc.HighLevelDiseaseID IN (4,7,8) AND rc.rcMatterID=RawComplaints.rcMatterID)
				+  (SELECT COUNT(DISTINCT(rc.ClaimID)) FROM @RawComplaints AS rc WHERE rc.HighLevelDiseaseID IN (5,6,NULL) AND rc.rcMatterID=RawComplaints.rcMatterID)
				AS Total
	FROM @RawComplaints AS RawComplaints
	GROUP BY Matter,
	rcMatterID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryPendingClaims] TO [power_user]
GO
