SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInsPayment](@DocumentID int, @DatePaid datetime, @Amount money, @PaymentTypeID int, @CarrierGroupID int, @Emailaddress nvarchar(250))
AS
BEGIN
	INSERT INTO tblPayment (DocumentID, DatePaid, DateEntered, Amount, PaymentTypeID, CarrierGroupID, SPUsername)
	VALUES (@DocumentID, @DatePaid, GETDATE(), @Amount, @PaymentTypeID, @CarrierGroupID, @Emailaddress)
	SELECT MAX(PaymentID) FROM tblPayment
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsPayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsPayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsPayment] TO [power_user]
GO
