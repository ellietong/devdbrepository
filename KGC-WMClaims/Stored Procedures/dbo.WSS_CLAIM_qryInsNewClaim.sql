SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryInsNewClaim]
		(@DiseaseID int = NULL, 
		@DiseaseDiagnosisDate datetime = NULL, 
		@Notes nvarchar(1000) = NULL,
		@DocketNumber nvarchar(100) = NULL,
		@CaseCaption nvarchar(1000) = NULL,
		@LeadPlaintiffLastName nvarchar(100) = NULL,
		@LeadPlaintiffFirstName nvarchar(100) = NULL,
		@JurisdictionID int = NULL, 
		@StateID int = NULL,
		@FileDate datetime = NULL,
		@ServiceDate datetime = NULL,
		@StatusDate datetime = NULL,
		@LawsuitStatusID int = NULL,
		@ClaimantPersonalInfoID bigint,
		@LawsuitID int = NULL,
		@SettlementAmount smallmoney = NULL,
		@SettlementDate datetime = NULL,
		@FirmID int = NULL, 
		@Emailaddress nvarchar(250),
		@ClaimDiseaseID			bigint = NULL, 
		@ActiveTrialDate			datetime = NULL, 
		@ILODoctorID				int = NULL, 
		@PFTDoctorID				int = NULL, 
		@ILOProfusion			nvarchar(100) = NULL, 
		@ILODate					datetime = NULL,
		@PFTDate					datetime = NULL, 
		@PFTTypeID				int = NULL, 
		@PFTFVC_Actual			float = NULL, 
		@PFTFVC_Predicted		float = NULL,
		@PFTFEV1_Actual			float = NULL, 
		@PFTFEV1_Predicted		float = NULL,
		@PFTFEV1FVC_Actual		float = NULL, 
		@PFTFEV1FVC_Predicted	float = NULL,
		@PFTTLC_Actual			float = NULL, 
		@PFTTLC_Predicted		float = NULL,
		@PFTDLCO_Actual			float = NULL, 
		@PFTDLCO_Predicted		float = NULL,
		@PFTTV					float = NULL, 
		@ClaimID					bigint = NULL,
		@ClaimSequenceID			int = NULL,
		@AllegationTypeID		int = NULL,
		@PlaintiffCounselAddressID int = NULL,
		@ClaimLawsuitStatusID	int = NULL,
		@DefenseCounselAddressID int = null,
		@IsCurrentClaim			int = NULL,
		@DOFE					datetime = NULL,
		@MatterID				int = NULL,
		@IndividualDocketNumber nvarchar(50) = NULL,
		@IsPrimaryForClaim		bit = NULL,
		@ReceivedDate			datetime = NULL,
		@CourtTypeID int = NULL)
AS
--BEGIN


INSERT INTO tblLawsuit (DocketNumber, CaseCaption, LeadPlaintiffFirstName, LeadPlaintiffLastName, 
		JurisdictionID, FileDate, StateID, CourtTypeID, DefenseCounselAddressID, PlaintiffCounselAddressID, SPUsername) --SettlementDate
VALUES ((UPPER(@DocketNumber)), (UPPER(@CaseCaption)), (UPPER(@LeadPlaintiffFirstName)), (UPPER(@LeadPlaintiffLastName)), 
		(@JurisdictionID), (@FileDate), (@StateID), @CourtTypeID, (@DefenseCounselAddressID), @PlaintiffCounselAddressID, (@Emailaddress)) --SettlementDate	

DECLARE @NewLawsuitID int = (SELECT MAX(LawsuitID) FROM tblLawsuit)

INSERT INTO tblClaim (ClaimantPersonalInfoID, IsCurrentClaim, ClaimSequenceID, DOFE, MatterID, SPUsername)
VALUES (@ClaimantPersonalInfoID, @IsCurrentClaim, @ClaimSequenceID, @DOFE, @MatterID, @Emailaddress)

DECLARE @NewClaimID int = (SELECT MAX(ClaimID) FROM tblClaim)

INSERT INTO tblClaimLawsuit (LawsuitID, IndividualDocketNumber, IsPrimaryForClaim, ClaimID, ActiveTrialDate, ServiceDate, ReceivedDate, SPUsername)		
VALUES (@NewLawsuitID, @IndividualDocketNumber, @IsPrimaryForClaim, @NewClaimID, @ActiveTrialDate, @ServiceDate, @ReceivedDate, @Emailaddress)

DECLARE @NewClaimLawsuitID int = (SELECT MAX(ClaimLawsuitID) FROM tblClaimLawsuit)

INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, SPUsername)
VALUES (@NewClaimLawsuitID, @ClaimLawsuitStatusID, @StatusDate, 1, @Emailaddress)
 

----INSERT INTO tblClaimLawsuitDisease (ClaimantLawsuitID, DiseaseID, DiagnosisDate, Notes)
----VALUES ((SELECT MAX(ClaimantLawsuitID) FROM tblClaimantLawsuit), (@DiseaseID), (@DiseaseDiagnosisDate), (UPPER(@Notes)))
		
--END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsNewClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsNewClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsNewClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsNewClaim] TO [power_user]
GO
