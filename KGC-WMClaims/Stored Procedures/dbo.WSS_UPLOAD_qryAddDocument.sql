SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROC [dbo].[WSS_UPLOAD_qryAddDocument] (@ClaimantDocumentTypeID		int,					--aka ClaimantDocumentTypeID
											  @ClaimantPersonalInfoID	bigint = NULL,
											  @CreatedBy				nvarchar(250),
											  @DateCreated				datetime,
											  @DocumentURL				nvarchar(4000),
											  @DocumentTitle			nvarchar(4000),
											  @Comments					nvarchar(4000) = NULL,	--comments are optional	
											  @FirmID					int = NULL,
											  @DocumentSourceID			int = NULL,
      										  @DefendantCompanyID		int = NULL,
    										  @TravelersBillID			int = NULL,
    										  @SPDocID					nvarchar(50) = NULL,
											  --These are null unless the document is an Invoice document
											  @InvoiceNumber			nvarchar(50) = NULL,
											  @Period					nvarchar(50) = NULL,
											  @DatePayable				datetime = NULL,
											  @FeeAmount				money = NULL,
											  @DisbursementAmount		money = NULL,
    										  @Adjustment				money = NULL,
    										  @AdjustmentNotes			nvarchar(4000) = NULL,
    										  @StatusID					int = NULL,
    										  @MatterID					int = NULL,
    										  @ExpertFirmID				int = NULL,
    										  @PrivateComments			nvarchar(4000) = NULL,
    										  @Approved					bit = NULL,
    										  @ApprovedBy				nvarchar(250) = NULL,
    										  @DateApproved				datetime = NULL)
 
AS
 
DECLARE @MaxDocID int
SET @MaxDocID = (SELECT IDENT_CURRENT('tblDocuments')+IDENT_INCR('tblDocuments'))
 
IF(RIGHT(@CreatedBy, 11) = 'kcicllc.com') OR RIGHT(@CreatedBy, 6) = 'wm.com' OR (@ClaimantDocumentTypeID IN (40, 41, 42))
BEGIN
INSERT INTO tblDocuments (ClaimantDocumentTypeID,	
						  ClaimantPersonalInfoID,
						  Comments,
						  DocumentURL,
						  DocumentTitle,
						  CreatedBy,
						  DateCreated,
						  InvoiceNumber,
						  Period,
						  DatePayable,
						  FeeAmount,
						  DisbursementAmount,
						  Adjustment,
						  AdjustmentNotes,
						  StatusID,	
						  ModifiedBy,
						  DateModified,
						  FirmID,
						  DocumentSourceID,
						  MatterID,
						  ApprovedBy,
						  DateApproved,
						  DefendantCompanyID,
						  TravelersBillID)
	VALUES (@ClaimantDocumentTypeID,
			@ClaimantPersonalInfoID,
			@Comments,
			REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
			@DocumentTitle,
			@CreatedBy,
			@DateCreated,
			@InvoiceNumber,
			@Period,
			@DatePayable,
			@FeeAmount,
			@DisbursementAmount,
			@Adjustment,
			@AdjustmentNotes,
			@StatusID,	
			@CreatedBy,
			@DateCreated,	
			@FirmID,
			@DocumentSourceID,
			@MatterID,
			@ApprovedBy,
			@DateApproved,
			@DefendantCompanyID,
			@TravelersBillID)
 
SELECT @MaxDocID
END
 
ELSE
BEGIN
INSERT INTO tblDocuments (ClaimantDocumentTypeID,	
						  ClaimantPersonalInfoID,
						  Comments,
						  DocumentURL,
						  DocumentTitle,
						  CreatedBy,
						  DateCreated,
						  InvoiceNumber,
						  Period,
						  DatePayable,
						  FeeAmount,
						  DisbursementAmount,
						  Adjustment,
						  AdjustmentNotes,
						  StatusID,	
						  ModifiedBy,
						  DateModified,
						  FirmID,
						  DocumentSourceID,
						  MatterID,
						  ApprovedBy,
						  DateApproved,
						  Accepted,
						  AcceptedBy,
						  DateAccepted,
						  DefendantCompanyID,
						  TravelersBillID)
	VALUES (@ClaimantDocumentTypeID,
			@ClaimantPersonalInfoID,
			@Comments,
			REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
			@DocumentTitle,
			@CreatedBy,
			@DateCreated,
			@InvoiceNumber,
			@Period,
			@DatePayable,
			@FeeAmount,
			@DisbursementAmount,
			@Adjustment,
			@AdjustmentNotes,
			@StatusID,	
			@CreatedBy,
			@DateCreated,	
			@FirmID,
			@DocumentSourceID,
			@MatterID,
			@ApprovedBy,
			@DateApproved,
			1,
			'rescocip@kcicllc.com',
			GETDATE(),
			@DefendantCompanyID,
			@TravelersBillID)
SELECT @MaxDocID
END
 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [power_user]
GO
