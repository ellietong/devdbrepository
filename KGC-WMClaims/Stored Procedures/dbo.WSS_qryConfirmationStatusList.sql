SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryConfirmationStatusList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ConfirmationStatusID, ConfirmationStatus
	FROM tblConfirmationStatus
	UNION
	SELECT NULL, ''
	ORDER BY ConfirmationStatus
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryConfirmationStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryConfirmationStatusList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryConfirmationStatusList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryConfirmationStatusList] TO [power_user]
GO
