SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryOccupationList]
AS

SELECT OccupationID, 
	Occupation 
FROM [KGA-ClaimsAdmin].[dbo].tblOccupation
UNION
SELECT NULL, ''
ORDER BY Occupation

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryOccupationList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryOccupationList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryOccupationList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryOccupationList] TO [power_user]
GO
