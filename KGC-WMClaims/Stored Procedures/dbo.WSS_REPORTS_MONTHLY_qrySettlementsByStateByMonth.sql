SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_MONTHLY_qrySettlementsByStateByMonth] (@MatterID int = NULL, 
                                                                     @MonthID  int, 
                                                                     @YearID   int) 
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

SELECT FullNameState, 
	COUNT(DISTINCT(cl.ClaimLawsuitID)) AS 'Count', 
	SUM(SettlementAmount) AS IndemnityPaid, 
	SUM(SettlementAmount) / COUNT(DISTINCT(cl.ClaimLawsuitID)) AS AverageIndemnity
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaimantSettlement cs           ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT  JOIN tblState s                         ON l.StateID = s.StateID
WHERE  MONTH(COALESCE(SettlementDate,ProcessedDate)) = @month 
	AND YEAR(COALESCE(SettlementDate,ProcessedDate)) = @year 
	AND ClaimLawsuitStatusID in (23,24,25,28)  
	AND IsPrimaryForClaim = 1 
	AND IsPrimaryStatus=1
	AND MatterID >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
GROUP BY FullNameState
ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qrySettlementsByStateByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qrySettlementsByStateByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qrySettlementsByStateByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qrySettlementsByStateByMonth] TO [power_user]
GO
