SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_FIRM_qryGetFirm] 
AS

	SELECT DefenseCounselAddressID AS FirmID, DefenseCounsel AS Firm FROM tblDefenseCounselAddress
	UNION
	SELECT NULL, ''
	ORDER BY Firm

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_FIRM_qryGetFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_FIRM_qryGetFirm] TO [power_user]
GO
