SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryOutstandingTravelersBills]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	DECLARE @Payments TABLE (AccountsReceivableID int, Amount money, Paid money)
	INSERT INTO @Payments
	SELECT ar.AccountsReceivableID, ar.Amount, SUM(COALESCE(arp.Amount,0))
	FROM tblAccountsReceivable ar
	LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	GROUP BY ar.AccountsReceivableID, ar.Amount

	--Get over-/under-/un-paid Travelers Bills
	DECLARE @BillsWithIssues TABLE (TravelersBillID int, TotalAmount money, TotalPaid money, TotalDifference money)
	INSERT INTO @BillsWithIssues
	SELECT TravelersBillID, SUM(COALESCE(pay.Amount, 0)), SUM(COALESCE(pay.Paid, 0)), SUM(COALESCE(pay.Amount, 0)) - SUM(COALESCE(pay.Paid, 0))
	FROM @Payments pay
	INNER JOIN tblAccountsReceivable ar ON pay.AccountsReceivableID = ar.AccountsReceivableID
	WHERE PayeeTypeID <> 2
	GROUP BY TravelersBillID
	HAVING SUM(COALESCE(pay.Paid, 0)) <> SUM(COALESCE(pay.Amount, 0))
	ORDER BY TravelersBillID
		
	SELECT 
	d.TravelersBillID, CASE WHEN (tb.SettlementAmount IS NULL OR tb.SettlementAmount = 0) AND tb.InvoiceNumber IS NOT NULL THEN 'Invoice: ' + tb.InvoiceNumber ELSE 'Settlement' END AS BillType,
		CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
		CASE WHEN FirstName IS NULL 
		THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName,
		TotalAmount, TotalPaid, TotalDifference
	FROM tblTravelersBill tb
	INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
	INNER JOIN tblMultiClaimantDocument MCD ON d.DocumentID = MCD.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON MCD.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblSuffix s ON cpi.SuffixID = s.SuffixID
	INNER JOIN @BillsWithIssues bwi ON tb.TravelersBillID = bwi.TravelersBillID
	WHERE tb.TravelersBillID <> 0
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingTravelersBills] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingTravelersBills] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingTravelersBills] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingTravelersBills] TO [power_user]
GO
