SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryGetClaimantLawsuitID]
	-- Add the parameters for the stored procedure here
@ClaimID int, @LawsuitID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT ClaimLawsuitID 
FROM tblClaimLawsuit 
WHERE ClaimID = @ClaimID AND LawsuitID = @LawsuitID --IsPrimaryForClaim = 1

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetClaimantLawsuitID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetClaimantLawsuitID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetClaimantLawsuitID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetClaimantLawsuitID] TO [power_user]
GO
