SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_REPORTS_qrySettlementsByClaimTypeByMonth] 
	(@MonthID int,
	 @YearID int) 
AS
BEGIN
	SET NOCOUNT ON;
    --Test Variables
	--Declare @MonthID int = 3
	--Declare @YearID int = 2003


	--SELECT 
	--	ClaimType, 
	--	COUNT(DISTINCT(cl.ClaimantLawsuitID)) as [Count], 
	--	SUM(SettlementAmount) as IndemnityPaid, 
	--	SUM(SettlementAmount)/COUNT(DISTINCT(cl.ClaimantLawsuitID)) as AverageIndemnity
	--FROM tblClaimantPersonalInfo cpi
	--	INNER JOIN tblClaimantLawsuit cl	ON cpi.ClaimantPersonalInfoID=cl.ClaimantPersonalInfoID
	--	LEFT JOIN tblLawsuit l				ON cl.LawsuitID=l.LawsuitID
	--	LEFT JOIN tblLawsuitStatus clst	ON cl.LawsuitStatusID=clst.LawsuitStatusID
	--	LEFT JOIN tblState s				ON l.StateID=s.StateID
	--	LEFT JOIN tblClaimType ct			ON ct.ClaimTypeID = cpi.ClaimTypeID
	--WHERE	MONTH(SettlementDate)		=	@MonthID 
	--		AND YEAR(SettlementDate)	=	@YearID 
	--		AND LawsuitStatusGroupID	=	1
	--GROUP BY ClaimType
	--ORDER BY ClaimType


END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementsByClaimTypeByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementsByClaimTypeByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementsByClaimTypeByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementsByClaimTypeByMonth] TO [power_user]
GO
