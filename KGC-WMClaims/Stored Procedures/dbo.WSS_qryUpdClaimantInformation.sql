SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdClaimantInformation]
	-- Add the parameters for the stored procedure here
	(@ClaimantPersonalInfoID int, @FirstName nvarchar (250), @MiddleName nvarchar (250) = NULL, @LastName nvarchar (250), @ClaimTypeID int, @DOFE datetime, @ActualDOFE datetime, @Notes nvarchar (1000), @GenderID int = NULL, @BirthDate datetime = NULL, @DeceasedDate datetime = NULL, @FULLSSN nvarchar(50) = NULL, @SuffixID int = NULL, @Emailaddress nvarchar(250), @InsurerNotification date = NULL, @TravelersResponse date = NULL)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 --   -- Insert statements for procedure here
	--UPDATE tblClaimantPersonalInfo
 --   SET FirstName = UPPER(@FirstName),
	--	MiddleName = UPPER(@MiddleName),
	--	LastName = UPPER(@LastName),
	--	ClaimTypeID = @ClaimTypeID, 
	--	DOFE = @DOFE,
	--	ActualDOFE = @ActualDOFE,
	--	Notes = @Notes,
	--	--GenderID = COALESCE(@GenderID, GenderID),
	--	--BirthDate = COALESCE(@DOB, BirthDate),
	--	--DeceasedDate = COALESCE(@DOD, DeceasedDate),
	--	--SSN = COALESCE(@SSN, SSN)
	--	GenderID = @GenderID,
	--	BirthDate = @BirthDate,
	--	DeceasedDate = @DeceasedDate,
	--	SSN = REPLACE(REPLACE(@FULLSSN, '-', ''), ' ', ''),
	--	SuffixID = @SuffixID,
	--	SPUsername = @Emailaddress,
	--	InsurerNotification = @InsurerNotification,
	--	TravelersResponse = @TravelersResponse
	--WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantInformation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantInformation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantInformation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantInformation] TO [power_user]
GO
