SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SERVICES_qryLawsuitCompletionList] 
(@PrefixText nvarchar(100), @Emailaddress nvarchar(100))
AS
BEGIN

--DECLARE @emailaddress nvarchar(250) = 'mlo@blakekirchner.com'
--DECLARE @PrefixText nvarchar(100) = '1'

SET NOCOUNT ON

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)

DECLARE @NoFilters bit
SET @NoFilters = (SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress)

IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com' OR @NoFilters = 1 )
BEGIN
	SELECT DISTINCT l.LawsuitID, DocketNumber + CASE WHEN CaseCaption IS NULL THEN ' (' + 'UNKNOWN' + ')' ELSE ' (' + CaseCaption + ')' END AS CaseCaption
	FROM tblClaim c 
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblClaimDisease cd ON c.ClaimID	= cd.ClaimID
	LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
	WHERE DocketNumber LIKE COALESCE('%' + @PrefixText, '') + '%'
	ORDER BY DocketNumber + CASE WHEN CaseCaption IS NULL THEN ' (' + 'UNKNOWN' + ')' ELSE ' (' + CaseCaption + ')' END 

END
ELSE
BEGIN
	SELECT DISTINCT l.LawsuitID, DocketNumber + CASE WHEN CaseCaption IS NULL THEN ' (' + 'UNKNOWN' + ')' ELSE ' (' + CaseCaption + ')' END AS CaseCaption
	FROM tblClaim c 
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblClaimDisease cd ON c.ClaimID	= cd.ClaimID
	LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
	LEFT JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.matterID
	WHERE DocketNumber LIKE COALESCE('%' + @PrefixText, '') + '%'
	AND (l.DefenseCounselAddressID = @FirmAddressID OR dj.DefenseCounselAddressID = @FirmAddressID)
	ORDER BY DocketNumber + CASE WHEN CaseCaption IS NULL THEN ' (' + 'UNKNOWN' + ')' ELSE ' (' + CaseCaption + ')' END 


END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryLawsuitCompletionList] TO [power_user]
GO
