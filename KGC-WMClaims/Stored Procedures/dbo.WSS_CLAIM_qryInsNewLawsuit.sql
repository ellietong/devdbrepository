SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryInsNewLawsuit]
	-- Add the parameters for the stored procedure here
		(@DocketNumber nvarchar(100) = NULL,
		@CaseCaption nvarchar(1000) = NULL,
		@LeadPlaintiffLastName nvarchar(100) = NULL,
		@LeadPlaintiffFirstName nvarchar(100) = NULL,
		@JurisdictionID int = NULL,
		@AllegationID int = NULL, 
		@StateID int = NULL,
		@FileDate datetime = NULL,
		@Emailaddress nvarchar(250),	
		@DefenseCounselAddressID int = NULL,
		@PlaintiffCounselAddressID int = NULL,
		@ActiveTrialDate			datetime = NULL,
		@CourtTypeID int = NULL) 				
					
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
INSERT INTO tblLawsuit (DocketNumber, CaseCaption, LeadPlaintiffFirstName, LeadPlaintiffLastName, 
		JurisdictionID, FileDate, StateID, CourtTypeID, DefenseCounselAddressID, PlaintiffCounselAddressID, SPUsername) 
VALUES ((UPPER(@DocketNumber)), (UPPER(@CaseCaption)), (UPPER(@LeadPlaintiffFirstName)), (UPPER(@LeadPlaintiffLastName)), 
		(@JurisdictionID), (@FileDate), (@StateID), @CourtTypeID, (@DefenseCounselAddressID), @PlaintiffCounselAddressID, (@Emailaddress))
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsNewLawsuit] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsNewLawsuit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsNewLawsuit] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsNewLawsuit] TO [power_user]
GO
