SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryInsAdditionalClaimLawsuit]

(@ClaimantPersonalInfoID int,  
@DOFE datetime = NULL, 
@LawsuitID int = NULL, 
@ServiceDate datetime, 
@MatterID int, 
@ClaimSequenceID int = 1, 
@ClaimID  int = NULL, 
@ReceivedDate datetime = NULL, 
@EmailAddress nvarchar(250), 
@ClaimLawsuitStatusID int = NULL,
@DocketNumber nvarchar(100) = NULL,
@CaseCaption nvarchar(1000) = NULL,
@LeadPlaintiffLastName nvarchar(100) = NULL,
@LeadPlaintiffFirstName nvarchar(100) = NULL,
@JurisdictionID int = NULL,
@AllegationID int = NULL, 
@StateID int = NULL,
@FileDate datetime = NULL,
@DefenseCounselAddressID int = NULL,
@PlaintiffCounselAddressID int = NULL,
@ActiveTrialDate			datetime = NULL,
@CourtTypeID int = NULL,
@FirmID int = NULL,
@DefendantCompanyID int = NULL
) --@IsCurrentClaim bit = NULL
	
AS
BEGIN

IF @ClaimID IS NOT NULL BEGIN
	IF @LawsuitID IS NOT NULL BEGIN  --Insert new Claim Lawsuit for existing claim and lawsuit
		

		--First, unflag the existing claimlawsuit as being primary
		UPDATE tblClaimLawsuit 
		SET IsPrimaryForClaim = 0
		WHERE ClaimLawsuitID IN (SELECT ClaimLawsuitID from tblClaimLawsuit where ClaimID = @ClaimID)
		
		--UPDATE tblClaimLawsuitStatusTracking
		--SET IsPrimaryStatus = 0,
		--StatusDate= convert(date,GETDATE()),
		----ClaimLawsuitID = COALESCE(@ClaimLawsuitStatusID, ClaimLawsuitStatusID),
		--SPUsername = @EmailAddress
		--WHERE ClaimLawsuitID IN (SELECT ClaimLawsuitID from tblClaimLawsuit where ClaimID = @ClaimID AND LawsuitID = @LawsuitID)

		INSERT INTO tblClaimLawsuit (LawsuitID, IsPrimaryForClaim, ClaimID, ServiceDate, ReceivedDate, ActiveTrialDate, DOFE, SPUsername)		
		VALUES (@LawsuitID, 1, @ClaimID, @ServiceDate, @ReceivedDate, @ActiveTrialDate, @DOFE, @Emailaddress)

		DECLARE @ClaimLawsuitID int = (SELECT MAX(ClaimLawsuitID) FROM tblClaimLawsuit)

		INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, ProcessedDate, SPUsername)
		VALUES (@ClaimLawsuitID, COALESCE(@ClaimLawsuitStatusID, 22), GETDATE(), 1, GETDATE(), @Emailaddress)
		
		IF(@DefendantCompanyID IS NOT NULL)
			INSERT INTO tblAllegation (ClaimLawsuitID, DefendantCompanyID)
			SELECT @ClaimLawsuitID, @DefendantCompanyID
	END
	ELSE --NEW LAWSUIT
	BEGIN

		INSERT INTO tblLawsuit (DocketNumber, CaseCaption, LeadPlaintiffFirstName, LeadPlaintiffLastName, 
				JurisdictionID, FileDate, StateID, CourtTypeID, DefenseCounselAddressID, PlaintiffCounselAddressID, SPUsername) 
		VALUES ((UPPER(@DocketNumber)), (UPPER(@CaseCaption)), (UPPER(@LeadPlaintiffFirstName)), (UPPER(@LeadPlaintiffLastName)), 
				(@JurisdictionID), (@FileDate), (@StateID), @CourtTypeID, (@FirmID), @PlaintiffCounselAddressID, (@Emailaddress))

		DECLARE @NewLawsuitID_1 int = (SELECT MAX(LawsuitID) FROM tblLawsuit)

		--UPDATE tblClaimLawsuitStatusTracking
		--SET IsPrimaryStatus = 0,
		--StatusDate= convert(date,GETDATE()),
		----ClaimLawsuitID = COALESCE(@ClaimLawsuitStatusID, ClaimLawsuitStatusID),
		--SPUsername = @EmailAddress
		--WHERE ClaimLawsuitID IN (SELECT ClaimLawsuitID from tblClaimLawsuit where ClaimID = @ClaimID)
		
		UPDATE tblClaimLawsuit 
		SET IsPrimaryForClaim = 0
		WHERE ClaimLawsuitID IN (SELECT ClaimLawsuitID from tblClaimLawsuit where ClaimID = @ClaimID)
		
		INSERT INTO tblClaimLawsuit (LawsuitID, IsPrimaryForClaim, ClaimID, ServiceDate, ReceivedDate, ActiveTrialDate, DOFE, SPUsername)		
		VALUES (@NewLawsuitID_1, 1, @ClaimID, @ServiceDate, @ReceivedDate, @ActiveTrialDate, @DOFE, @Emailaddress)

		DECLARE @ClaimLawsuitID_1 int = (SELECT MAX(ClaimLawsuitID) FROM tblClaimLawsuit)

		INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, ProcessedDate, SPUsername)
		VALUES (@ClaimLawsuitID_1, COALESCE(@ClaimLawsuitStatusID, 22), GETDATE(), 1, GETDATE(), @Emailaddress)
			
		--Insert Allegation record 		
		IF(@DefendantCompanyID IS NOT NULL)
			INSERT INTO tblAllegation (ClaimLawsuitID, DefendantCompanyID)
			SELECT @ClaimLawsuitID_1, @DefendantCompanyID			
	END
END
ELSE --NEW CLAIM
BEGIN 
	IF @LawsuitID IS NOT NULL BEGIN  --EXISTING LAWSUIT

		UPDATE tblClaim
		SET IsCurrentClaim = 0 
		WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND MatterID = @MatterID

		INSERT INTO tblClaim (ClaimantPersonalInfoID, IsCurrentClaim, ClaimSequenceID, MatterID, SPUsername)
		VALUES (@ClaimantPersonalInfoID, 1, @ClaimSequenceID, @MatterID, @Emailaddress)

		DECLARE @NewClaimID int = (SELECT MAX(ClaimID) FROM tblClaim)

		INSERT INTO tblClaimLawsuit (LawsuitID, IsPrimaryForClaim, ClaimID, ServiceDate, ReceivedDate, ActiveTrialDate, DOFE, SPUsername)		
		VALUES (@LawsuitID, 1, @NewClaimID, @ServiceDate, @ReceivedDate, @ActiveTrialDate, @DOFE, @Emailaddress)

		DECLARE @NewClaimLawsuitID int = (SELECT MAX(ClaimLawsuitID) FROM tblClaimLawsuit)


		INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, ProcessedDate, SPUsername)
		VALUES (@NewClaimLawsuitID, COALESCE(@ClaimLawsuitStatusID, 22), GETDATE(), 1, GETDATE(), @Emailaddress)
	
		--Insert Allegation record 		
		IF(@DefendantCompanyID IS NOT NULL)
			INSERT INTO tblAllegation (ClaimLawsuitID, DefendantCompanyID)
			SELECT @NewClaimLawsuitID, @DefendantCompanyID		
	END
	ELSE --NEW LAWSUIT
	BEGIN

		INSERT INTO tblLawsuit (DocketNumber, CaseCaption, LeadPlaintiffFirstName, LeadPlaintiffLastName, 
				JurisdictionID, FileDate, StateID, CourtTypeID, DefenseCounselAddressID, PlaintiffCounselAddressID, SPUsername) 
		VALUES ((UPPER(@DocketNumber)), (UPPER(@CaseCaption)), (UPPER(@LeadPlaintiffFirstName)), (UPPER(@LeadPlaintiffLastName)), 
				(@JurisdictionID), (@FileDate), (@StateID), @CourtTypeID, (@FirmID), @PlaintiffCounselAddressID, (@Emailaddress))
		
		DECLARE @NewLawsuitID_2 int = (SELECT MAX(LawsuitID) FROM tblLawsuit)	
				
		UPDATE tblClaim
		SET IsCurrentClaim = 0 
		WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND MatterID = @MatterID

		INSERT INTO tblClaim (ClaimantPersonalInfoID, IsCurrentClaim, ClaimSequenceID, MatterID, SPUsername)
		VALUES (@ClaimantPersonalInfoID, 1, @ClaimSequenceID, @MatterID, @Emailaddress)

		DECLARE @NewClaimID_2 int = (SELECT MAX(ClaimID) FROM tblClaim)

		INSERT INTO tblClaimLawsuit (LawsuitID, IsPrimaryForClaim, ClaimID, ServiceDate, ReceivedDate, ActiveTrialDate, DOFE, SPUsername)		
		VALUES (@NewLawsuitID_2, 1, @NewClaimID_2, @ServiceDate, @ReceivedDate, @ActiveTrialDate, @DOFE, @Emailaddress)

		DECLARE @NewClaimLawsuitID_2 int = (SELECT MAX(ClaimLawsuitID) FROM tblClaimLawsuit)


		INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, IsPrimaryStatus, ProcessedDate, SPUsername)
		VALUES (@NewClaimLawsuitID_2, COALESCE(@ClaimLawsuitStatusID, 22), GETDATE(), 1, GETDATE(), @Emailaddress)
				
		--Insert Allegation record 		
		IF(@DefendantCompanyID IS NOT NULL)
			INSERT INTO tblAllegation (ClaimLawsuitID, DefendantCompanyID)
			SELECT @NewClaimLawsuitID_2, @DefendantCompanyID	
	END	

END

		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsAdditionalClaimLawsuit] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsAdditionalClaimLawsuit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryInsAdditionalClaimLawsuit] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryInsAdditionalClaimLawsuit] TO [power_user]
GO
