SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryClaimantRelatedPartyInfo]
@ClaimantPersonalInfoID bigint=null, @emailaddress nvarchar(250)
AS
--BEGIN
----DECLARE @Claimantpersonalinfoid bigint
----SET @ClaimantPersonalInfoID = 30661
----DECLARE @emailaddress nvarchar(150)
----SET @emailaddress = 'marleywylainncc'

--Select ClaimantRelatedPartyID, 
--	ClaimantPersonalInfoID,  
--	COALESCE(LastName, '') + ', ' + CASE WHEN FirstName IS NULL 
--    THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
--    LastName, FirstName, MiddleName,
--	Relationship, crp.RelationshipID,
--	LC.LegalCapacityID, LegalCapacity
--From tblClaimantRelatedParty crp 
--LEFT JOIN tblRelationship r on r.RelationshipID=crp.RelationshipID
--LEFT JOIN tblLegalCapacity LC ON LC.LegalCapacityID = crp.LegalCapacityID
--WHERE ClaimantPersonalInfoID=@ClaimantPersonalInfoID
--END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantRelatedPartyInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantRelatedPartyInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantRelatedPartyInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantRelatedPartyInfo] TO [power_user]
GO
