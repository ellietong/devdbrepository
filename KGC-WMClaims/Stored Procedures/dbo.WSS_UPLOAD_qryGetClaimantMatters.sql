SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROCEDURE [dbo].[WSS_UPLOAD_qryGetClaimantMatters]
 @ClaimantPersonalInfoID int, @MatterID int
	-- Add the parameters for the stored procedure here

AS	
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @Matters TABLE (MatterID int, Matter nvarchar(250))  
INSERT INTO @Matters
SELECT C.MatterID, Matter FROM 
tblClaim C INNER JOIN tblMatter M ON C.MatterID = M.MatterID
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
UNION
SELECT NULL, ''
ORDER BY Matter

SELECT COUNT(MatterID) AS MatterCount 
FROM @Matters
WHERE MatterID = @MatterID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetClaimantMatters] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetClaimantMatters] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetClaimantMatters] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetClaimantMatters] TO [power_user]
GO
