SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyInsurerNotification_Treco_Rust]
(@BodyMessage nvarchar(MAX) output)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT
--BEGIN	


set @BodyMessage = @BodyMessage + '
		<p style="font-family: Calibri; font-size: 10pt; text-align: left; ">
			TRECO CONSTRUCTION SERVICES, INC., AND<br/>
			THE RUST ENGINEERING COMPANY INSURERS<br/> 	
			RUST INTERNATIONAL, INC.<br/>
			NATIONAL INDUSTRIAL CONSTRUCTORS, INC.<br/> 

			
		</p>
		<p style="font-family: Calibri; font-size: 10pt; font-weight: bold; text-align: left; border: 1px solid transparent; " colspan="9">	
			<u>NOTIFICATION OF CLAIM</u><br/>
			
		<p/>
		<p style="font-family: Calibri; font-size: 10pt; font-weight: bold; text-align: left; border: 1px solid transparent; " colspan="9">
	RE: See the Table Below
		<p/>	
<p style="font-family: Calibri; font-size: 10pt; text-align: left; border: 1px solid transparent; " colspan="9">			
	Dear Insurers:<br/>
	<br>
		We, the insured, give you formal notice of these claims against us for the states of<br/>
		West Virginia, Ohio, Illinois, Indiana, Pennsylvania, Texas and Missouri.<br/>
	<br/>
		1.	Acknowledgment of your receipt of this letter;
	<br/>
		2.	Identification of your file number and designation of the individual at your company who will be handling these matters;
	<br/>
		3.	Your immediate and unequivocal participation in defense of these matters;

		4.	Indication that you will indemnify your insured if obligated to do so under the insurance policies.
	<br/>
		Waste Management, Inc. may be the holder of these alleged liabilities due to the<br/>
	corporate history behind the Rust family of companies, including but not limited to The Rust<br/>
	Engineering Company, Treco Construction Services, Inc., Rust International, Inc., Kellogg,<br/>
	National Industrial Constructors, Rust Construction, Inc. and Swindell. That history has been<br/>
	provided to you previously and will not be reiterated here, unless a specific request is made for same.<br/>
<br/>
		We, the insured, believe that these matters can, and should be resolved with full<br/>
	participation of its insurers, and look to you for prompt and unequivocal assumption of<br/>
	defense and indemnity.<br/>  

<br/>
											Sincerely,<br/>
											<br/>

											RUST INTERNATIONAL, INC.<br/>
											<br/>
											<br/>
											_____________________________
											<br/>
											Ashley Harper<br/>
											Waste Management, Inc.<br/>
											Litigation Counsel<br/>	
	
 </p>'	
--END	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Treco_Rust] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Treco_Rust] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Treco_Rust] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Treco_Rust] TO [power_user]
GO
