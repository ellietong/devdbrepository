SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetLawsuitCompletionList]
(@prefixText nvarchar(MAX), @count int = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--DECLARE @prefix nvarchar(MAX) = 'a'
	--DECLARE @count int = 5

    -- Insert statements for procedure here
    SET ROWCOUNT @count
	SELECT L.LawsuitID, COALESCE(DocketNumber, '') + ' (' + COALESCE(CaseCaption, 'N/A') + ')' AS Lawsuit
	FROM tblLawsuit L WITH (READCOMMITTED) WHERE DocketNumber LIKE ('%' + @prefixText + '%')
	SET ROWCOUNT 0
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetLawsuitCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetLawsuitCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetLawsuitCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetLawsuitCompletionList] TO [power_user]
GO
