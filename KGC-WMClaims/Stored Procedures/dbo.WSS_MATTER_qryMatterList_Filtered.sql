SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_MATTER_qryMatterList_Filtered]
 @ClaimantPersonalInfoID int
	-- Add the parameters for the stored procedure here

AS	
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	


    -- Insert statements for procedure here
SELECT C.MatterID, Matter FROM 
tblClaim C INNER JOIN tblMatter M ON C.MatterID = M.MatterID
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
UNION
SELECT NULL, ''
ORDER BY Matter
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_MATTER_qryMatterList_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_MATTER_qryMatterList_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_MATTER_qryMatterList_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_MATTER_qryMatterList_Filtered] TO [power_user]
GO
