SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryDocumentType] (@invoiceFlag    int = 0, 
											   @settlementFlag bit = 0)
AS
SET NOCOUNT ON

--DECLARE @invoiceFlag bit
--DECLARE @settlementFlag bit
--SET @invoiceFlag    = 0
--SET @settlementFlag = 0

--IF @invoiceFlag = 0
--BEGIN
--	IF @settlementFlag = 1
--	BEGIN
--		SELECT ClaimantDocumentTypeID, 
--			ClaimantDocumentType, 
--			DocumentGroupID
--		FROM tblClaimantDocumentType
--		WHERE DocumentGroupID IN (1, 3, 4)
--			AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <>16
--		ORDER BY ClaimantDocumentType
--	END
--	ELSE
--	BEGIN
--		SELECT ClaimantDocumentTypeID, 
--			ClaimantDocumentType, 
--			DocumentGroupID
--		FROM tblClaimantDocumentType
--		WHERE DocumentGroupID IN (1, 3)
--			AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <> 16
--		ORDER BY ClaimantDocumentType
--	END
--END
--ELSE IF @invoiceFlag = 1
--BEGIN
--		SELECT ClaimantDocumentTypeID, 
--			ClaimantDocumentType, 
--			DocumentGroupID
--		FROM tblClaimantDocumentType
--		WHERE DocumentGroupID = 2 
--			AND ClaimantDocumentTypeID NOT IN (41, 42)
--		ORDER BY ClaimantDocumentType
--END
--ELSE
--BEGIN
--		SELECT ClaimantDocumentTypeID, 
--			ClaimantDocumentType, 
--			DocumentGroupID
--		FROM tblClaimantDocumentType
--		WHERE DocumentGroupID = 2
--		ORDER BY ClaimantDocumentType
--END

SELECT ClaimantDocumentTypeID, ClaimantDocumentType FROM tblClaimantDocumentType
WHERE ClaimantDocumentTypeID not in (16, 34, 48, 49, 29)/**exclude dismissal, settlement confirmation letter, travelers bills and payment types**/
ORDER BY ClaimantDocumentType
--UNION
--SELECT NULL, '<< Document Type >>'
--ORDER BY ClaimantDocumentType
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [power_user]
GO
