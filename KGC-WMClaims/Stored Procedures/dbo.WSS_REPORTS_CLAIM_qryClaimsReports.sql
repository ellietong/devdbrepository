SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROC [dbo].[WSS_REPORTS_CLAIM_qryClaimsReports] 
 
 
(@ClaimLawsuitStatusID int, 
										  @HighLevelDiseaseID   int, 
										  @StateID              int, 
										  @PlaintiffCounselAddressID   bigint, 
										  @JurisdictionID       int, 
										  @MatterID             int, 
										  @IsMDL                int, 
										  --@AllegationTypeID     int, 
										  @ActiveTrialStart     date, 
										  @ActiveTrialEnd       date, 
										  @ResolutionStart      date, 
										  @ResolutionEnd        date, 
										  @ProcessStart         date, 
										  @ProcessEnd           date, 
										  @EmailAddress         nvarchar(250))
AS
 
----VARIABLE FOR TESTING
--DECLARE @ClaimLawsuitStatusID int
--DECLARE @HighLevelDiseaseID   int
--DECLARE @StateID              int
--DECLARE @PlaintiffCounselID   bigint
--DECLARE @JurisdictionID       int
--DECLARE @MatterID             int
--DECLARE @IsMDL                int
--DECLARE @AllegationTypeID     int
--DECLARE @ActiveTrialStart     date
--DECLARE @ActiveTrialEnd       date
--DECLARE @ResolutionStart      date
--DECLARE @ResolutionEnd        date
--DECLARE @ProcessStart         date
--DECLARE @ProcessEnd           date
--DECLARE @EmailAddress         nvarchar(250)
--SET @ClaimLawsuitStatusID = 22
--SET @HighLevelDiseaseID   = NULL
--SET @StateID              = NULL
--SET @PlaintiffCounselID   = NULL
--SET @JurisdictionID       = NULL
--SET @MatterID             = NULL
--SET @IsMDL                = 0
--SET @AllegationTypeID     = NULL
--SET @ActiveTrialStart     = NULL
--SET @ActiveTrialEnd       = NULL
--SET @ResolutionStart      = NULL
--SET @ResolutionEnd        = NULL
--SET @ProcessStart         = NULL
--SET @ProcessEnd           = NULL
--SET @EmailAddress         = NULL
 
--Prepare permissions-related fields for use in queries below.
DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID 
								FROM vtblPerson 
								WHERE Email = @emailaddress)
 
 
 
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString =  ' WHERE 1=1 '--' WHERE IsPrimaryForClaim = 1'
IF @ClaimLawsuitStatusID	IS Not NULL BEGIN SET @WhereString = @WhereString + ' AND cls.claimlawsuitstatusid = ' + CONVERT(nvarchar(10),@claimlawsuitstatusid) END 
IF @HighLevelDiseaseID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND ddisease.HighLevelDiseaseID = ' + CONVERT(nvarchar(20), @highleveldiseaseid) END
IF @StateID					IS NOT NULL	BEGIN SET @WhereString = @WhereString + ' AND l.stateid = ' + CONVERT(nvarchar(10), @stateid) END 
IF @PlaintiffCounselAddressID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(pca.PlaintiffCounselID, 0) = ' + CONVERT(nvarchar(20),@PlaintiffCounselAddressID) END
IF @JurisdictionID			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.JurisdictionID = ' + CONVERT(nvarchar(20),@JurisdictionID) END
IF @MatterID				IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND c.matterid = ' + CONVERT(nvarchar(20), @matterid) END
IF @IsMDL					IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND cl.ismdl = ' + CONVERT(nvarchar(20), @ismdl) END
--IF @AllegationTypeID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND a.allegationtypeid = ' + CONVERT(nvarchar(20), @allegationtypeid) END 
IF @ActiveTrialStart		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(cl.ActiveTrialDate, CONVERT(date, ''1/1/1900'')) >= ''' + CONVERT(nvarchar(100),COALESCE(@ActiveTrialStart, CONVERT(date, '1/1/1900')))+'''' END
IF @ActiveTrialEnd			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(cl.ActiveTrialDate, CONVERT(date, ''1/1/1900'')) <=''' + CONVERT(nvarchar(1000),COALESCE(@ActiveTrialEnd, CONVERT(date, '1/1/2100')))+'''' END
IF @ResolutionStart			IS NOT NULL OR @ResolutionEnd IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND CONVERT(date, ''1/1/1900'')) <= COALESCE(' + CONVERT(nvarchar(100),@ResolutionEnd) + ', CONVERT(date, ''1/1/2100'')) AND COALESCE(CASE WHEN clst.ClaimLawsuitStatusID IN (15,16,17, 36, 19, 27) THEN clst.StatusDate WHEN clst.ClaimLawsuitStatusID in (23, 25) THEN csett.SettlementDate ELSE NULL END, CONVERT(date, ''1/1/1900'')) >= COALESCE(' + CONVERT(nvarchar(100),@ResolutionStart) + ', CONVERT(date, ''1/1/1900''))' END
IF @ProcessStart			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND ProcessedDate >=''' + CONVERT(nvarchar(100),@ProcessStart)+'''' END
IF @ProcessEnd				IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND ProcessedDate <=''' + CONVERT(nvarchar(100),@ProcessEnd)+'''' END
 
--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString  = ''
ELSE
BEGIN
	SET @JoinString  = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.matterID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),@DefenseCounselAddressID) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),@DefenseCounselAddressID) + ')'
END
 
--Here the final query is pieced together using the WHERE string created above.
DECLARE @QueryString nvarchar(max)
SET @QueryString = '
	SELECT c.ClaimID, 
		c.ClaimantPersonalInfoID, 
		IsMDL, 
		DHLD.HighLevelDisease AS Disease, 
		s.State, 
		j.Jurisdiction, 
		cs.ClaimSequence, 
		cpi.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' 
		                                                  ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' 
                                                                                                                  ELSE MiddleName + '' '' END AS ClaimantName, 
        CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
		CONVERT(VARCHAR(10), CPI.BirthDate, 101) AS BirthDate, 
		CPI.DeceasedDate, 
		CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) 
			 WHEN Month(BirthDate) > Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 
			 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 
																										   ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
		cls.ClaimLawsuitStatus, 
		CONVERT(date, cl.ServiceDate) AS ServiceDate, 
		CONVERT(date, L.FileDate) AS FileDate, 
		l.DocketNumber, 
		l.CaseCaption, 
		l.LeadPlaintiffLastName + '', '' + l.LeadPlaintiffFirstName AS LeadPlaintiffName, 
		pca.PlaintiffCounsel, 
		dca.DefenseCounsel AS PrimaryDefenseCounsel, 
		m.Matter, 
		cl.ActiveTrialDate, 
		CONVERT(date,ProcessedDate) AS ProcessDate
	FROM tblClaimantPersonalInfo cpi 
		INNER JOIN tblClaim c                          ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID 
		INNER JOIN tblClaimLawsuit cl                  ON c.ClaimID = cl.ClaimID 
		INNER JOIN tblLawsuit l                        ON cl.LawsuitID = l.LawsuitID ' +
		@JoinString + '
		LEFT  JOIN tblPlaintiffCounselAddress pca   ON pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid 
		INNER JOIN tblState s						   ON s.StateID = l.StateID 
		INNER JOIN tblJurisdiction j				   ON j.JurisdictionID = l.JurisdictionID 
		/*INNER*/	LEFT JOIN tblClaimSequence cs              ON cs.ClaimSequenceID = C.ClaimSequenceID 
		--LEFT  JOIN (SELECT dcs.DefenseCounselAddressID, 
		--			    d.DefenseCounsel,
		--			    dcs.ResponsibleDefenseCounselAddressID,
		--			    dcs.StateID,
		--			    r.DefenseCounsel AS ResponsibleDefenseCounsel,
		--			    dcs.IsActiveCounsel
		--			FROM tblDefenseCounselState dcs 
		--				INNER JOIN tblDefenseCounselAddress dca  ON dca.defensecounseladdressid = dcs.defensecounseladdressid 
		--				INNER JOIN tblDefenseCounselAddress dcar ON dcar.defensecounseladdressid = dcs.responsibledefensecounseladdressid 
		--				INNER JOIN tblDefenseCounsel d           ON d.DefenseCounselID = dca.DefenseCounselID 
		--				INNER JOIN tblDefenseCounsel r           ON r.DefenseCounselID = dcar.DefenseCounselID) r_1 ON r_1.DefenseCounseladdressID = l.DefenseCounseladdressID AND r_1.StateID = l.StateID ' + '
		LEFT  JOIN tblDefenseCounselAddress dca		   ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
		LEFT  JOIN (SELECT * 
		            FROM tblClaimDisease 
		            WHERE PrimaryDisease = 1) cd    ON cd.ClaimID = c.ClaimID 
		LEFT  JOIN tblDisease ddisease              ON cd.DiseaseID = ddisease.DiseaseID 
		LEFT  JOIN tblHighLevelDisease dhld         ON dhld.HighLevelDiseaseID = ddisease.HighLevelDiseaseID 
		INNER JOIN (SELECT * 
		            FROM tblClaimLawsuitStatusTracking 
		            WHERE IsPrimaryStatus = 1) clst    ON cl.ClaimLawsuitID = clst.ClaimLawsuitID 
		INNER JOIN tblClaimLawsuitStatus cls		   ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN tblClaimLawsuitStatusGroup clsg	   ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID
		LEFT  JOIN tblClaimantSettlement csett		   ON cl.ClaimLawsuitID = csett.ClaimLawsuitID
		LEFT  JOIN tblMatter m						   ON c.MatterID = m.MatterID ' +
	@WhereString + '
	ORDER BY CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' 
															     ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' 
															                                                               ELSE MiddleName + '' '' END'
									                                                               
--Finally, the query created above is executed.
 
Print Substring(@Querystring, 1, 4000)
Print Substring(@QueryString, 4001, 4000)
Print Substring(@QueryString, 8001, 4000)
EXEC sp_executesql @QueryString
 
--(@ClaimLawsuitStatusGroupID int, 
--                                                       @HighLevelDiseaseID        int, 
--                                                       @StateID                   int, 
--                                                       @PlaintiffCounselID        bigint, 
--                                                       @JurisdictionID            int, 
--                                                       @MatterID                  int, 
--                                                       @IsMDL                     int, 
--                                                       @emailaddress              nvarchar(250), 
--                                                       @AllegationTypeID          int)
--AS
--SET NOCOUNT ON
 
------VARIABLES FOR TESTING----
----DECLARE @ClaimLawsuitStatusGroupID int
----DECLARE @HighLevelDiseaseID        int
----DECLARE @StateID                   int
----DECLARE @PlaintiffCounselID        bigint
----DECLARE @JurisdictionID            int
----DECLARE @MatterID                  int
----DECLARE @IsMDL                     int
----DECLARE @emailaddress              nvarchar(250)
----DECLARE @AllegationTypeID         int
----SET @ClaimLawsuitStatusGroupID = NULL
----SET @HighLevelDiseaseID        = NULL
----SET @StateID                   = NULL
----SET @PlaintiffCounselID        = NULL
----SET @JurisdictionID            = NULL
----SET @MatterID                  = NULL
----SET @IsMDL                     = NULL
----SET @emailaddress              = 'crookc@kcicllc.com' 
----SET @AllegationTypeID          = NULL
 
----NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
----      and all original StatusDate references have been replaced with ProcessedDate.
 
----Prepare permissions-related fields for use in queries below.
--DECLARE @DefenseCounselAddressID int
--SET @DefenseCounselAddressID = (SELECT FirmAddressID 
--								FROM vtblPerson 
--								WHERE Email = @emailaddress)
 
----These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
--DECLARE @WhereString nvarchar(max)
--SET @WhereString = ' WHERE COALESCE(cl.IsPrimaryForClaim, 0) = 1 AND clst.IsPrimaryStatus = 1'
--IF @ClaimLawsuitStatusGroupID	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND cls.ClaimLawsuitStatusGroupID = ' + CONVERT(nvarchar(20),@ClaimLawsuitStatusGroupID) END
--IF @HighLevelDiseaseID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND (ddisease.HighLevelDiseaseID = ' + CONVERT(nvarchar(20),@HighLevelDiseaseID) + ' OR da.HighLevelDiseaseID = ' + CONVERT(nvarchar(20),@HighLevelDiseaseID) + ')' END
--IF @StateID					IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.StateID = ' + CONVERT(nvarchar(20),@StateID) END
--IF @PlaintiffCounselID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND pca.PlaintiffCounselID = ' + CONVERT(nvarchar(20),@PlaintiffCounselID) END
--IF @JurisdictionID			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.JurisdictionID  = ' + CONVERT(nvarchar(20),@JurisdictionID) END
--IF @MatterID				IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar(20),@MatterID) END
--IF @IsMDL					IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND IsMDL = ' + CONVERT(nvarchar(20),@IsMDL) END
--IF @AllegationTypeID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND a.AllegationTypeID = ' + CONVERT(nvarchar(20),@AllegationTypeID) END
 
----These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
--DECLARE @JoinString nvarchar(max)
--IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
--	SET @JoinString  = ''
--ELSE
--BEGIN
--	SET @JoinString  = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID '
--	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),@DefenseCounselAddressID) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),@DefenseCounselAddressID) + ')'
--END
 
----Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
--DECLARE @QueryString nvarchar(max)
--SET @QueryString = 
--	'SELECT DISTINCT c.ClaimID, 
--		c.ClaimantPersonalInfoID, 
--		IsMDL, 
--		dhld.HighLevelDisease AS Disease, 
--		s.State, 
--		j.Jurisdiction, 
--		cs.ClaimSequence, 
--		cpi.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' 
--															ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' 
--																													  ELSE MiddleName + '' '' END AS ClaimantName, 
--		CASE WHEN SocialSecurityNumber IS NULL THEN '''' 
--											   ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
--		BirthDate, 
--		cpi.DeceasedDate, 
--		cls.ClaimLawsuitStatus, 
--		CONVERT(date, l.ServiceDate) AS ServiceDate, 
--		CONVERT(date, l.FileDate) AS FileDate, 
--		l.DocketNumber, 
--		l.CaseCaption, 
--		l.LeadPlaintiffLastName + '', '' + l.LeadPlaintiffFirstName AS LeadPlaintiffName, 
--		pca.PlaintiffCounsel, 
--		DefenseCounsel AS PrimaryDefenseCounsel, 
--		m.Matter, 
--		cl.ActiveTrialDate, 
--		a.AllegationTypeID, 
--		AllegationType,
--		CASE WHEN clst.ClaimLawsuitStatusID !=22 THEN StatusDate
--												 ELSE Null END AS ResolutionDate
--	 FROM tblClaimantPersonalInfo cpi 
--		INNER JOIN tblClaim c                          ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID 
--		INNER JOIN tblClaimLawsuit cl                  ON c.ClaimID = cl.ClaimID 
--		INNER JOIN tblLawsuit l                        ON cl.LawsuitID = l.LawsuitID ' +
--		@JoinString + '
--		LEFT  JOIN tblPlaintiffCounselAddress pca	   ON pca.plaintiffcounseladdressid = l.plaintiffcounseladdressid 
--		INNER JOIN tblState s						   ON s.StateID = l.StateID 
--		INNER JOIN tblJurisdiction j				   ON j.JurisdictionID = l.JurisdictionID 
--		INNER JOIN tblClaimSequence cs				   ON cs.ClaimSequenceID = c.ClaimSequenceID 
--		LEFT  JOIN tblDefenseCounselAddress dca		   ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
--		LEFT  JOIN (SELECT * 
--		            FROM tblClaimDisease 
--		            WHERE PrimaryDisease = 1) cd       ON cd.ClaimID = c.ClaimID 
--		LEFT  JOIN tblDisease ddisease				   ON cd.DiseaseID = ddisease.DiseaseID 
--		LEFT  JOIN tblDisease_ALT da				   ON cd.DiseaseID = da.DiseaseID_Alt 
--		LEFT  JOIN tblHighLevelDisease dhld			   ON dhld.HighLevelDiseaseID = ddisease.HighLevelDiseaseID OR dhld.HighLevelDiseaseID = da.HighLevelDiseaseID 
--		INNER JOIN (SELECT * 
--		            FROM tblClaimLawsuitStatusTracking 
--		            WHERE IsPrimaryStatus = 1) clst    ON cl.ClaimLawsuitID = clst.ClaimLawsuitID 
--		INNER JOIN tblClaimLawsuitStatus cls		   ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
--		LEFT JOIN tblClaimLawsuitStatusGroup clsg	   ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID
--		LEFT  JOIN tblClaimantSettlement csett		   ON cl.ClaimLawsuitID = csett.ClaimLawsuitID
--		LEFT  JOIN tblMatter m						   ON c.MatterID = m.MatterID
--		LEFT  JOIN tblAllegation a					   ON cl.ClaimLawsuitID = a.ClaimLawsuitID
--		LEFT  JOIN tblAllegationType at				   ON a.AllegationTypeID = at.AllegationTypeID ' + 
--	 @WhereString + '
--	 OPTION (RECOMPILE)'
 
----Finally, the query created above is executed.
--EXEC sp_executesql @QueryString
 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_CLAIM_qryClaimsReports] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_CLAIM_qryClaimsReports] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_CLAIM_qryClaimsReports] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_CLAIM_qryClaimsReports] TO [power_user]
GO
