SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_qryLegalCapacityList]
as
--Select LegalCapacityID, LegalCapacity
--From tblLegalCapacity
--UNION
--SELECT NULL, ''
--Order By LegalCapacity
Return
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryLegalCapacityList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryLegalCapacityList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryLegalCapacityList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryLegalCapacityList] TO [power_user]
GO
