SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_QC_qryAllocationCompareByTravelersByID]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Create Table #AccountsReceivable  (policyID int, TravelersBillID int, Amount money)
	Insert into #AccountsReceivable
	Select PolicyID, ar.TravelersBillID, SUM(Amount)
	From tblAccountsReceivable ar
	inner join tblTravelersBill tb on tb.TravelersBillID=ar.TravelersBillID
	where coalesce(tb.AllocationQCConfirmation,0) <> 1
	Group By PolicyID, ar.TravelersBillID

	SELECT occurrenceID, LondonPolicyID, SUM(COALESCE(RespreadIndemnity, 0) + COALESCE(RespreadDefense, 0) + COALESCE(RespreadDefenseOutsideLimits,0)) AS LondonRespread
	INTO #LondonRespread
	FROM m_tblAllocationResultsLondonRespread GROUP BY OccurrenceID, LondonPolicyID

	SELECT tb.TravelersBillID, pd.PolicyID AS KCICPolicyID, 
	(COALESCE(IndemnityAllocation, 0) + COALESCE(DefenseAllocation, 0) + COALESCE(DefenseOutsideLimitsAllocation, 0)) - COALESCE(LondonRespread, 0) AS KCICAllocation,
	acctrec.PolicyID AS TravelersPolicyID, acctrec.Amount AS TravelersAllocation,
	(COALESCE(IndemnityAllocation,0) + COALESCE(DefenseAllocation,0) + COALESCE(DefenseOutsideLimitsAllocation,0))-COALESCE(LondonRespread,0) - acctrec.Amount AS [Difference],
	p.PolicyNum, c.CarrierName, pd.PStartDate, pd.PEndDate,
	CASE WHEN ABS((COALESCE(IndemnityAllocation,0) + COALESCE(DefenseAllocation,0) + COALESCE(DefenseOutsideLimitsAllocation,0))-COALESCE(LondonRespread,0) - acctrec.Amount) > .05 THEN 1 ELSE 0 END AS Flag
	FROM tblExhaustion e
	INNER JOIN tblOccurrence o ON e.OccurrenceID = o.OccurrenceID
	INNER JOIN tblTravelersBill tb ON o.TravelersBillID = tb.TravelersBillID
	INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
	LEFT JOIN #AccountsReceivable acctrec ON acctrec.TravelersBillID = tb.TravelersBillID AND acctrec.PolicyID = pd.PolicyID
	INNER JOIN tblPolicy p ON pd.PolicyID = p.PolicyID
	INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
	LEFT JOIN #LondonRespread lr  ON e.OccurrenceID = lr.OccurrenceID AND pd.PolicyID  = lr.LondonPolicyID
	WHERE coalesce(tb.AllocationQCConfirmation,0) <> 1 and BillStatusID <>2
	GROUP BY
	tb.TravelersBillID, pd.PolicyID, 
	(COALESCE(IndemnityAllocation, 0) + COALESCE(DefenseAllocation, 0) + COALESCE(DefenseOutsideLimitsAllocation, 0))-COALESCE(LondonRespread,0),
	acctrec.PolicyID, acctrec.Amount ,
	ABS((COALESCE(IndemnityAllocation,0) + COALESCE(DefenseAllocation,0) + COALESCE(DefenseOutsideLimitsAllocation,0)) -COALESCE(LondonRespread, 0) - acctrec.Amount),
	p.PolicyNum, c.CarrierName, pd.PStartDate, pd.PEndDate
	ORDER BY PStartDate
	
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_QC_qryAllocationCompareByTravelersByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_QC_qryAllocationCompareByTravelersByID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_QC_qryAllocationCompareByTravelersByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_QC_qryAllocationCompareByTravelersByID] TO [power_user]
GO
