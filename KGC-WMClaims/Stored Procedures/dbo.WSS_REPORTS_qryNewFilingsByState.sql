SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByState] 
	(@StartDate datetime, @EndDate datetime) 
AS
BEGIN
	SET NOCOUNT ON;
	----Test Variables
	----DECLARE @StartDate datetime = '1/1/1900'
	----DECLARE @EndDate datetime = GETDATE()

	--SELECT 
	--	COALESCE(FullNameState, 'Unknown') AS State, 
	--	COUNT(DISTINCT(cpi.ClaimantPersonalInfoID)) AS Count
	--FROM tblClaimantPersonalInfo cpi 
	--	LEFT JOIN tblClaimantLawsuit cl on cl.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	--	LEFT JOIN tblLawsuit l						ON cl.LawsuitID=l.LawsuitID
	--	LEFT JOIN (SELECT * 
	--				FROM tblLawsuitStatus 
	--				WHERE LawsuitStatusID=6) clst	ON cl.LawsuitStatusID=clst.LawsuitStatusID
	--	LEFT JOIN tblState s						ON l.StateID=s.StateID
	--WHERE COALESCE(ServiceDate,FileDate,Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
	--GROUP BY FullNameState
	--ORDER BY COUNT(DISTINCT(cpi.ClaimantPersonalInfoID)) DESC


END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByState] TO [power_user]
GO
