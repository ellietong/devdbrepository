SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmail5DaysBeforeOverdue]
-- Add the parameters for the stored procedure here
AS
BEGIN
/*Query run daily to send an alert email to send a summary of the new tenders that have been created in the past week. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

DECLARE @Today DATE
SET @Today = '1/29/2012' --getdate()

SET @Subject = 'Waste Management: Payments Overdue in 5 Days ' + CONVERT(nvarchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


DECLARE @5DayPayments TABLE (ID int, CarrierName nvarchar (250), DefenseCounsel nvarchar (500), ClaimantName nvarchar (300), ClaimType nvarchar (250), InvoiceNumber nvarchar (250), AmountBilled nvarchar (150), DueDate nvarchar (250), Owed money, Remainder int)
DECLARE @5DayPayment TABLE (ID int NOT NULL IDENTITY (1, 1) PRIMARY KEY, CarrierName nvarchar (250), DefenseCounsel nvarchar (500), ClaimantName nvarchar (300), ClaimType nvarchar (250), InvoiceNumber nvarchar (250), AmountBilled nvarchar (150), DueDate nvarchar (250), Owed money) --, Remainder int)
DECLARE @Payments TABLE (AccountsReceivableID int, Amount money, Paid money, PayeeTypeID nvarchar (250))
INSERT INTO @Payments
	SELECT
		ar.AccountsReceivableID,
		ar.Amount,
		SUM(COALESCE(arp.Amount, 0)),
		PayeeTypeID
	FROM tblAccountsReceivable ar
	LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	GROUP BY	ar.AccountsReceivableID,
				ar.Amount,
				PayeeTypeID

INSERT INTO @5DayPayment
	SELECT
		CarrierName,
		dca.DefenseCounsel AS Payee,
		CPI.LastName + ' ' + CASE
			WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix
		END + ', ' +
		CASE
			WHEN FirstName IS NULL THEN '' ELSE FirstName + ' '
		END + CASE
			WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' '
		END AS ClaimantName,
		Matter,
		CASE
			WHEN (tb.SettlementAmount IS NULL OR tb.SettlementAmount = 0) AND tb.InvoiceNumber IS NOT NULL THEN tb.InvoiceNumber ELSE 'Settlement'
		END,
		SUM(ar.Amount) AS 'Amount Billed',
		CONVERT(nvarchar(100), (PaymentDueDate), 103),
		SUM(ar.Amount) - SUM(COALESCE(pay.Paid, 0)) AS Outstanding
	FROM tblAccountsReceivable ar
	LEFT JOIN tblTravelersBill tb ON tb.TravelersBillID = ar.TravelersBillID
	INNER JOIN tblDefenseCounselAddress dca ON tb.DefenseCounselAddressID = dca.DefenseCounselAddressID
	INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
	INNER JOIN tblClaimantPersonalInfo cpi ON d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblSuffix s ON cpi.SuffixID = s.SuffixID
	INNER JOIN tblPolicy p ON ar.PolicyID = P.PolicyID
	INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
	LEFT JOIN tblClaim cl ON cpi.ClaimantPersonalInfoID = cl.ClaimantPersonalInfoID
	LEFT JOIN tblPayeeType pt ON ar.PayeeTypeID = pt.PayeeTypeID
	LEFT JOIN tblMatter m ON m.MatterID = cl.MatterID
	LEFT JOIN @Payments pay ON pay.AccountsReceivableID = ar.AccountsReceivableID
	WHERE PaymentDueDate = CAST(DATEADD(DAY, 5, @Today) AS date)
	GROUP BY	CarrierName,
				dca.DefenseCounsel,
				Matter,
				PayeeType,
				LastName,
				FirstName,
				MiddleName,
				cpi.SuffixID,
				Suffix,
				tb.InvoiceNumber,
				tb.SettlementAmount,
				InvoicedTotal,
				PaymentDueDate
	HAVING SUM(ar.Amount) - SUM(COALESCE(pay.Paid, 0)) > 0
	ORDER BY CarrierName, Matter, PaymentDueDate, ClaimantName, tb.InvoiceNumber

INSERT INTO @5DayPayments
	SELECT
		*,
		ROW_NUMBER() OVER (ORDER BY ID) % 2 AS Remainder
	FROM @5DayPayment

--SELECT * FROM @5DayPayments

DECLARE @CarrierList TABLE (CarrierName nvarchar (250))
INSERT INTO @CarrierList
	SELECT DISTINCT
		CarrierName
	FROM @5DayPayments

DECLARE @FirmList TABLE (DefenseCounsel nvarchar (250))
INSERT INTO @FirmList
	SELECT DISTINCT
		DefenseCounsel
	FROM @5DayPayments

DECLARE @OwedtoPayee TABLE (CarrierName nvarchar (250), DefenseCounsel nvarchar (500), OwedToPayee money)
INSERT INTO @OwedtoPayee
	SELECT
		CarrierName,
		DefenseCounsel,
		SUM(Owed) AS 'OwedtoPayee'
	FROM (SELECT
		*
	FROM @5DayPayment)
	tblp
	GROUP BY	CarrierName,
				DefenseCounsel
	ORDER BY CarrierName, DefenseCounsel
--SELECT * FROM @OwedtoPayee	

DECLARE @OwedByCarrier TABLE (CarrierName nvarchar (250), OwedByCarrier money)
INSERT INTO @OwedByCarrier
	SELECT
		CarrierName,
		SUM(OwedToPayee)
	FROM (SELECT
		*
	FROM @OwedtoPayee)
	tblotp
	GROUP BY CarrierName
	ORDER BY CarrierName

--Declare and populate the body of the email for each tender type
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Calibri; 
      font-size: 9pt;
      text-align: center;
      border: 1px solid transparent;
      /*Added*/
      border-top: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH    {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
UL {margin:0; 
	padding:0;}	 
.Aging TH    { border: none;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}	
.FOOT    {/*border-top: 2px solid #FDBB30;*/
      padding: .3em;
      color: #F6ECF0;
       background: #000;
       border: none;}     
.Even {background-color: #D6E8FF;}              
    
--->
</STYLE>'

DECLARE @LastCarrierName nvarchar (250) = ''
DECLARE @LastPayee nvarchar (250) = ''

IF (SELECT
	COUNT(CarrierName)
FROM @5DayPayments) > 0
BEGIN
SET @BodyMessage = @BodyMessage + '<H2>Details of Amounts Overdue in 5 Days</H2>'

--create a cursor do loop through invoices and print appropriate HTML
DECLARE @CarrierName nvarchar (250), @DefenseCounsel nvarchar (500), @ClaimantName nvarchar (300), @ClaimType nvarchar (250), @InvoiceNumber nvarchar (250),
@Payee nvarchar (100), @AmountBilled money, @DueDate nvarchar (150), @Owed money, @Remainder int, @CarrierNameOTP nvarchar (250), @DefenseCounselOTP nvarchar (500), @CarrierNameTO nvarchar (250), @OwedToPayeeOTP money, @OwedByCarrierTO money --, @LastCarrierName nvarchar(250), @LastPayee nvarchar(250)


DECLARE Carrier_curs CURSOR FOR SELECT DISTINCT
	CarrierName
FROM @5DayPayments DP
SET @BodyMessage = @BodyMessage + N'<table border=0 cellspacing=0 cellpadding=0>'
SET @BodyMessage = @BodyMessage + N'<tr><th>Carrier</th><th>Payee</th><th>Claim Type</th><th>Claimant Name</th><th>Invoice Number</th><th>Amount Owed</th></th><th>Due Date</th>'

OPEN Carrier_curs
FETCH NEXT FROM Carrier_curs INTO @CarrierName

WHILE @@FETCH_STATUS = 0
BEGIN
--Write out the carrier name, if necessary
IF @LastCarrierName < > @CarrierName
BEGIN
SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage + '<td style="text-align: left; font-weight:bold; background-color: #989898">' + @CarrierName + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="text-align: left; background-color: #989898" colspan="6"></td>'
SET @BodyMessage = @BodyMessage + '</tr>'
SET @LastPayee = ''
END

DECLARE Firm_curs CURSOR FOR SELECT DISTINCT
	DefenseCounsel
FROM @5DayPayments DP
WHERE CarrierName = @CarrierName

OPEN Firm_curs
FETCH NEXT FROM Firm_curs INTO @DefenseCounsel


While @@FETCH_STATUS = 0
BEGIN
IF @LastPayee < > @DefenseCounsel
BEGIN
SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage + '<td style="text-align: left; background-color: #D8D8D8"> </td>'
SET @BodyMessage = @BodyMessage + '<td style="text-align: left; font-weight:bold; background-color: #D8D8D8">' + @DefenseCounsel + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="text-align: left; background-color: #D8D8D8" colspan="5"></td>'
SET @BodyMessage = @BodyMessage + '</tr>'
END


DECLARE Info_curs CURSOR FOR SELECT
	ClaimType,
	ClaimantName,
	InvoiceNumber,
	AmountBilled,
	DueDate,
	Remainder
FROM @5DayPayments DP
WHERE CarrierName = @CarrierName AND DefenseCounsel = @DefenseCounsel

OPEN Info_curs
FETCH NEXT FROM Info_curs INTO @ClaimType, @ClaimantName, @InvoiceNumber, @AmountBilled, @DueDate, @Remainder

While @@FETCH_STATUS = 0


BEGIN

IF @Remainder = 0 BEGIN
SET @BodyMessage = @BodyMessage + '<tr class="Even">'
END
ELSE BEGIN
SET @BodyMessage = @BodyMessage + '<tr>'
END

SET @BodyMessage = @BodyMessage + '<td> </td>'
SET @BodyMessage = @BodyMessage + '<td> </td>'
SET @BodyMessage = @BodyMessage + '<td style="text-align: left">' + @ClaimType + '</td>' --</tr>'
SET @BodyMessage = @BodyMessage + '<td style="text-align: left">' + @ClaimantName + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + @InvoiceNumber + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @AmountBilled, 1) + '</td>'
SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + @DueDate + '</td></tr>'

IF (@LastPayee = '' OR @LastPayee < > @DefenseCounsel)
SET @LastPayee = @DefenseCounsel
FETCH NEXT FROM Info_curs INTO @ClaimType, @ClaimantName, @InvoiceNumber, @AmountBilled, @DueDate, @Remainder

END
CLOSE Info_curs
DEALLOCATE Info_curs


DECLARE OwedtoPayee_curs CURSOR FOR SELECT
	CarrierName,
	DefenseCounsel,
	OwedToPayee
FROM @OwedtoPayee DP
WHERE CarrierName = @CarrierName AND DefenseCounsel = @DefenseCounsel

OPEN OwedtoPayee_curs
FETCH NEXT FROM OwedtoPayee_curs INTO @CarrierName, @DefenseCounsel, @OwedToPayeeOTP

While @@FETCH_STATUS = 0


BEGIN

SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage + '<td colspan="6" style="text-align: left; font-weight:bold; background-color: #D8D8D8">' + 'Total Owed by &nbsp;' + @CarrierName + '&nbsp;to &nbsp;' + @DefenseCounsel + '</td>'
SET @BodyMessage = @BodyMessage + '<td colspan="1" style="text-align: right; font-weight:bold; background-color: #D8D8D8">' + '$' + CONVERT(varchar(12), @OwedToPayeeOTP, 1) + '</td>'
SET @BodyMessage = @BodyMessage + '</tr>'

FETCH NEXT FROM OwedtoPayee_curs INTO @CarrierName, @DefenseCounsel, @OwedToPayeeOTP


END
CLOSE OwedtoPayee_curs
DEALLOCATE OwedtoPayee_curs

FETCH NEXT FROM Firm_curs INTO @DefenseCounsel

END
CLOSE Firm_curs
DEALLOCATE Firm_curs


DECLARE OwedByCarrier_curs CURSOR FOR SELECT
	CarrierName,
	OwedByCarrier
FROM @OwedByCarrier DP
WHERE CarrierName = @CarrierName

OPEN OwedByCarrier_curs
FETCH NEXT FROM OwedByCarrier_curs INTO @CarrierName, @OwedByCarrierTO

While @@FETCH_STATUS = 0


BEGIN

SET @BodyMessage = @BodyMessage + '<tr>'
SET @BodyMessage = @BodyMessage + '<td colspan="6" style="font-weight:bold; text-align: left; font-weight:bold; background-color: #989898"">' + 'Total Owed by &nbsp;' + @CarrierName + '</td>'
SET @BodyMessage = @BodyMessage + '<td colspan="1" style="font-weight:bold; text-align: right; font-weight:bold; background-color: #989898"">' + '$' + CONVERT(varchar(12), @OwedByCarrierTO, 1) + '</td>'
SET @BodyMessage = @BodyMessage + '</tr>'

FETCH NEXT FROM OwedByCarrier_curs INTO @CarrierName, @OwedToPayeeOTP


END
CLOSE OwedByCarrier_curs
DEALLOCATE OwedByCarrier_curs


SET @BodyMessage = @BodyMessage + '</tr>'
IF (@LastCarrierName = '' OR @LastCarrierName < > @CarrierName)
SET @LastCarrierName = @CarrierName
FETCH NEXT FROM Carrier_curs INTO @CarrierName

END
CLOSE Carrier_curs
DEALLOCATE Carrier_curs


SET @BodyMessage = @BodyMessage + N'</table>' + N'<br>'

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')
END


----Link for document library

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

--set @BodyMessage = @BodyMessage + '<br><br>Recipients: ' + @RecipientList
PRINT @BodyMessage
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail --The wasteManagementProfile sends from @kcic.com
@profile_name = 'Claims',
@recipients = 'owenss@kcic.com', --'hubbardb@kcic.com',--
--@copy_recipients = '',
--@blind_copy_recipients = '',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmail5DaysBeforeOverdue] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmail5DaysBeforeOverdue] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmail5DaysBeforeOverdue] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmail5DaysBeforeOverdue] TO [power_user]
GO
