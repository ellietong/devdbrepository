SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInvoiceList] (@ClaimantPersonalInfoID int, @unlinked bit = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF(@unlinked = 1)
    BEGIN		
		SELECT d.DocumentID, DocumentTitle FROM tblDocuments d
		left join tblMultiClaimantDocument mcd ON mcd.DocumentID = d.DocumentID
		WHERE ClaimantDocumentTypeID = 40 
		AND mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND d.DocumentID NOT IN (SELECT DISTINCT COALESCE(InvoiceDocumentID, 0) 
								 FROM tblTravelersBill tb
									  INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
									  INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
								 WHERE mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID)
		UNION
		SELECT null, '<< Link an Invoice >>'
		ORDER BY DocumentTitle		
    END
    ELSE
    BEGIN
		SELECT d.DocumentID, DocumentTitle FROM tblDocuments d
		left join tblMultiClaimantDocument mcd ON mcd.DocumentID = d.DocumentID
		WHERE ClaimantDocumentTypeID = 40
		AND mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		UNION
		SELECT null, '<< Link an Invoice >>'
		ORDER BY DocumentTitle
    END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInvoiceList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInvoiceList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInvoiceList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInvoiceList] TO [power_user]
GO
