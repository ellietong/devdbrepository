SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_qryClaimantInfo](@ClaimantPersonalInfoID int = NULL, @emailaddress nvarchar(1000) = null)
AS 
SET NOCOUNT ON 


DECLARE @MaxCPI bigint
SET @MaxCPI = (SELECT MAX(ClaimantPersonalInfoID) FROM tblClaimantPersonalInfo)		
	
	SELECT cpi.LastName + ', ' + cpi.FirstName AS Claimant,
		cpi.ClaimantPersonalInfoID, FirstName, LastName
	FROM dbo.tblLawsuit l
		INNER JOIN tblClaimantLawsuit cl ON l.LawsuitID = cl.LawsuitID
		INNER JOIN tblClaimantPersonalInfo cpi ON cl.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	WHERE cl.ClaimantPersonalInfoID  BETWEEN COALESCE(@ClaimantPersonalInfoID, 0) AND COALESCE(@ClaimantPersonalInfoID, @MaxCPI)
	GROUP BY cpi.LastName + ', ' + cpi.FirstName,
		cpi.ClaimantPersonalInfoID, FirstName, LastName
	ORDER BY Claimant

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantInfo] TO [power_user]
GO
