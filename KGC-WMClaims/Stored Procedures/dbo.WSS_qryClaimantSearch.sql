SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantSearch] 
	-- Add the parameters for the stored procedure here
	(@ClaimantName nvarchar(max), @ClaimTypeID int, @ConfirmationStatusID int, @FirmID int) --@ConfirmationStatusID int
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--DECLARE @ClaimantName nvarchar(max) = ''
	--DECLARE @ClaimTypeID int = null
	--DECLARE @ConfirmationStatusID int = null 
	--DECLARE @FirmID int = null
	
	
DECLARE @ColumnListComma nvarchar(MAX)
DECLARE @ColumnListPlus nvarchar(MAX)



--SET @ColumnListComma = (
--SELECT LEFT(FirmList, LEN(FirmList)-1) FROM (
--SELECT  DISTINCT
--            '[' + REPLACE(Firm, '&', 'AND') + ']' + ', ' AS [text()] 
--        FROM 
--            tblFirm
--        FOR XML PATH('')  ) fl (FirmList))
        
--SET @ColumnListPlus = (
--SELECT LEFT(FirmList, LEN(FirmList)-1) FROM (
--SELECT DISTINCT
--            'CASE WHEN COALESCE([' + REPLACE(Firm, '&', 'AND') + '], 0) = 1 THEN ''' + REPLACE(Firm, '''', '"') + ', '' ELSE '''' END + ' AS [text()] 
--        FROM 
--            tblFirm
--        FOR XML PATH('')  ) fl (FirmList))
        
        
--DECLARE @Command nvarchar(MAX)

--SET @Command = '
--SELECT ClaimantPersonalInfoID, ' + @ColumnListPlus + ' FROM
--(SELECT DISTINCT ClaimantPersonalInfoID, REPLACE(f.Firm, ''&'', ''AND'') AS Firm , COUNT(DISTINCT l.FirmID) AS FirmCount FROM tblClaimantLawsuit cl
--LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
--INNER JOIN (SELECT * FROM tblFirm) f ON l.FirmID = f.FirmID
--GROUP BY ClaimantPersonalInfoID, Firm
----HAVING COUNT(DISTINCT l.FirmID) > 0
--)
--    AS dlist PIVOT

--(

--    Sum(FirmCount)
    
    
--FOR

--Firm
--IN (' + @ColumnListComma + ')

--) AS piv
--'

	
--	DECLARE @TravelersComparison TABLE (ClaimantPersonalInfoID bigint, ConfirmationStatusID int)
--	DECLARE @MaxConfirmationStatusID int = (SELECT MAX(ConfirmationStatusID) FROM tblConfirmationStatus)
--	DECLARE @MaxClaimTypeID int = (SELECT MAX(ClaimTypeID) FROM tblClaimType)
--	DECLARE @MaxFirmID int = (SELECT MAX(FirmID) FROM tblFirm)

	
--	INSERT INTO @TravelersComparison	
--SELECT cpi.ClaimantPersonalInfoID, CASE WHEN SUM(CASE WHEN COALESCE(d.TravelersBillID, 0)=0  THEN 1 ELSE 0 END) > 0 THEN 2 --No travelers docs
--		WHEN SUM(CASE WHEN tb.DOFE <> cpi.DOFE THEN 1 ELSE 0 END) > 0 THEN 5 --DOFE mismatch
--		WHEN SUM(CASE WHEN tb.ClaimTypeID <> cpi.ClaimTypeID THEN 1 ELSE 0 END) > 0 THEN 4 ELSE 1 END --Claim Type Mismatch
--	FROM tblClaimantPersonalInfo cpi 
--	LEFT JOIN (SELECT * FROM tblDocuments WHERE DocumentTypeID IN (3, 4))d ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
--	LEFT JOIN (SELECT * FROM tblTravelersBill WHERE BillStatusID = 1) tb ON d.TravelersBillID = tb.TravelersBillID--only current bills
--GROUP BY cpi.ClaimantPersonalInfoID

--DECLARE @DefenseCounsels TABLE (ClaimantPersonalInfoID bigint, Firms nvarchar(MAX))
--INSERT INTO @DefenseCounsels
--EXEC (@Command)

--	SELECT DISTINCT CPI.ClaimantPersonalInfoID AS ClaimantPersonalInfoID,  CPI.LastName  + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE ' ' + Suffix END + ', '  +
--CASE WHEN FirstName IS NULL 
--		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, 
--			  CASE WHEN ClaimType IS NULL THEN '' ELSE ClaimType END AS ClaimType, cpi.DOFE, SUM(COALESCE(FinalTotal, 0) + COALESCE(tb.SettlementAmount, 0)) as TotalCosts, Notes, 
--			  ConfirmationStatus, FirstName, MiddleName, LastName, CPI.ClaimTypeID, Gender, BirthDate, DeceasedDate, SSN, CPI.SuffixID AS SuffixID, Suffix, DofeSourcePageNum, ClaimTypeSourcePageNum, DofeSource, ClaimTypeSource,
--			   CASE WHEN ClaimTypeSource = 1 THEN 'Silent' WHEN ClaimTypeSource <> 1 THEN Doc.DocumentTitle END AS CTSourceTitle, CASE WHEN DofeSource = 1 THEN 'Silent' WHEN DofeSource <> 1 THEN Do.DocumentTitle END AS DOFESourceTitle, Doc.DocumentURL AS CTSourceURL, Do.DocumentURL AS DOFESourceURL,
--			   LEFT(COALESCE(Firms, 'Unassigned.'), LEN(COALESCE(Firms, 'Unassigned.'))-1) AS Firms
--	FROM dbo.tblClaimantPersonalInfo AS CPI 
--	LEFT JOIN dbo.tblClaimType AS C ON CPI.ClaimTypeID = C.ClaimTypeID
--	LEFT JOIN tblDocuments d ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
--	LEFT JOIN tblTravelersBill tb ON d.TravelersBillID = tb.TravelersBillID
--	LEFT JOIN (SELECT * FROM @TravelersComparison WHERE coalesce(ConfirmationStatusID, 0) >= COALESCE(@ConfirmationStatusID, 0) AND coalesce(ConfirmationStatusID,0) <= COALESCE(@ConfirmationStatusID, @MaxConfirmationStatusID)) tc ON cpi.ClaimantPersonalInfoID = tc.ClaimantPersonalInfoID
--	LEFT JOIN tblGender G ON CPI.GenderID = G.GenderID
--	INNER JOIN tblConfirmationStatus cs ON tc.ConfirmationStatusID = cs.ConfirmationStatusID
--	LEFT JOIN tblSuffix Su ON CPI.SuffixID = Su.SuffixID
--	LEFT JOIN tblDocuments Doc ON CPI.ClaimTypeSource = Doc.DocumentID
--	LEFT JOIN tblDocuments Do ON CPI.DofeSource = Do.DocumentID
--	LEFT JOIN @DefenseCounsels dc ON cpi.ClaimantPersonalInfoID = dc.ClaimantPersonalInfoID
--	LEFT JOIN tblClaimantLawsuit cl ON CPI.ClaimantPersonalInfoID = cl.ClaimantPersonalInfoID
--	LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
--	WHERE ((CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' end) like '%'+coalesce(@ClaimantName, '')+'%')
--			AND  coalesce(cpi.ClaimTypeID, 0) >= COALESCE(@ClaimTypeID, 0) AND coalesce(cpi.ClaimTypeID, 0) <= COALESCE(@ClaimTypeID, @MaxClaimTypeID)
--			AND COALESCE(l.FirmID, 0) >= COALESCE(@FirmID, 0) AND COALESCE(l.FirmID, 0) <= COALESCE(@FirmID, @MaxFirmID)
			
--	GROUP BY CPI.ClaimantPersonalInfoID,  CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
--CASE WHEN FirstName IS NULL 
--		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END, 
--			  CASE WHEN ClaimType IS NULL THEN '' ELSE ClaimType END, cpi.DOFE, Notes, 
--			   ConfirmationStatus, FirstName, MiddleName, LastName, CPI.ClaimTypeID, Gender, BirthDate, DeceasedDate, SSN, 
--			   CPI.SuffixID, Suffix, DofeSourcePageNum, ClaimTypeSourcePageNum, DofeSource, ClaimTypeSource, Doc.DocumentURL , 
--			   Do.DocumentURL, Doc.DocumentTitle, Do.DocumentTitle, Firms
			   
--	ORDER BY ClaimantName
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantSearch] TO [power_user]
GO
