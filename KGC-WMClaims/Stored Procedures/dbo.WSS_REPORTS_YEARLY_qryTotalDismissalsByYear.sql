SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_YEARLY_qryTotalDismissalsByYear] 
--(@YearID   int,
--                                                                @MatterID int,
--                                                                @MonthID  int) 

(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null)  

AS
SET NOCOUNT ON
	
----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

--DECLARE @year int
--DECLARE @Month int
--SET @year = @YearID
--SET @Month = @MonthID

SELECT COUNT(cl.ClaimLawsuitID) AS 'Count'
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT  JOIN tblState s                         ON l.StateID = s.StateID
WHERE  StatusDate BETWEEN COALESCE(@StartDate,'1/1/1900')
		AND COALESCE(@EndDate, '1/1/2100')
	AND ClaimLawsuitStatusID in (15,16,17,36,27,19) 
	AND IsPrimaryForClaim = 1 
	AND IsPrimaryStatus = 1
	--AND IsCurrentClaim =1
	AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryTotalDismissalsByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryTotalDismissalsByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryTotalDismissalsByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryTotalDismissalsByYear] TO [power_user]
GO
