SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryClaimSequenceList]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT ClaimSequenceID, ClaimSequence
FROM tblClaimSequence WHERE ClaimSequenceID IN (1, 5)
UNION
SELECT NULL, ''
ORDER BY ClaimSequence
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryClaimSequenceList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryClaimSequenceList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryClaimSequenceList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryClaimSequenceList] TO [power_user]
GO
