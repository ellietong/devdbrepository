SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryOutstandingAccountsReceivable]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----Get over-/under-/un-paid Travelers Bills
	--DECLARE @BillsWithIssues TABLE (TravelersBillID int)
	--INSERT INTO @BillsWithIssues
	--SELECT TravelersBillID
	--FROM tblAccountsReceivable ar
	--LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	--GROUP BY TravelersBillID
	----HAVING SUM(COALESCE(arp.Amount, 0)) <> SUM(COALESCE(ar.Amount, 0))
	--ORDER BY TravelersBillID

	DECLARE @Payments TABLE (AccountsReceivableID int, Amount money, Paid money)
	INSERT INTO @Payments
	SELECT ar.AccountsReceivableID, ar.Amount, SUM(COALESCE(arp.Amount,0))
	FROM tblAccountsReceivable ar
	LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	GROUP BY ar.AccountsReceivableID, ar.Amount
	
			DECLARE @Payments2 TABLE (AccountsReceivableID int, Amount money, Paid money)
	INSERT INTO @Payments2
	SELECT ar.AccountsReceivableID, ar.Amount, SUM(COALESCE(arp.Amount,0))
	FROM tblAccountsReceivable ar
	LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	GROUP BY ar.AccountsReceivableID, ar.Amount
	
	SELECT	ar.TravelersBillID,
			CarrierName,
			PolicyNum,
			PStartDate, 
			PEndDate,
			SUM(COALESCE(pay.Amount,0)) AS Amount,
			SUM(COALESCE(pay.Paid, 0)) AS AmountPaid,
			SUM(COALESCE(pay.Amount,0)) - SUM(COALESCE(pay.Paid, 0)) AS Outstanding
	FROM @Payments pay
		INNER JOIN tblAccountsReceivable ar ON pay.AccountsReceivableID = ar.AccountsReceivableID
		LEFT JOIN tblTravelersBill tb			ON tb.TravelersBillID = ar.TravelersBillID
		--LEFT JOIN tblAccountsReceivablePayment arp	ON ar.AccountsReceivableID = arp.AccountsReceivableID
		LEFT JOIN tblPolicy p						ON ar.PolicyID = p.PolicyID
		LEFT JOIN tblPolicyDates pd				ON p.PolicyID = pd.PolicyID
		LEFT JOIN tblCarrier c						ON p.CarrierID = c.CarrierID
		LEFT JOIN tblPolicyLimits pl				ON pl.PolicyID = p.PolicyID
		--LEFT JOIN @BillsWithIssues bwi ON tb.TravelersBillID = bwi.TravelersBillID
	WHERE PayeeTypeID <> 2 AND tb.TravelersBillID <> 0 AND tb.TravelersBillID NOT IN (
	
	SELECT distinct ar.TravelersBillID
	FROM @Payments2 pay
		INNER JOIN tblAccountsReceivable ar ON pay.AccountsReceivableID = ar.AccountsReceivableID
		LEFT JOIN tblTravelersBill tb			ON tb.TravelersBillID = ar.TravelersBillID
		--LEFT JOIN tblAccountsReceivablePayment arp	ON ar.AccountsReceivableID = arp.AccountsReceivableID
		LEFT JOIN tblPolicy p						ON ar.PolicyID = p.PolicyID
		LEFT JOIN tblPolicyDates pd				ON p.PolicyID = pd.PolicyID
		LEFT JOIN tblCarrier c						ON p.CarrierID = c.CarrierID
		LEFT JOIN tblPolicyLimits pl				ON pl.PolicyID = p.PolicyID
		--LEFT JOIN @BillsWithIssues bwi ON tb.TravelersBillID = bwi.TravelersBillID
	WHERE PayeeTypeID <> 2	AND tb.TravelersBillID <> 0
	GROUP BY
			pay.AccountsReceivableID,
			ar.TravelersBillID,
			CarrierName,
			PolicyNum,
			PStartDate, 
			PEndDate	
	)
	GROUP BY
			pay.AccountsReceivableID,
			ar.TravelersBillID,
			CarrierName,
			PolicyNum,
			PStartDate, 
			PEndDate
	ORDER BY TravelersBillID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingAccountsReceivable] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingAccountsReceivable] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryOutstandingAccountsReceivable] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryOutstandingAccountsReceivable] TO [power_user]
GO
