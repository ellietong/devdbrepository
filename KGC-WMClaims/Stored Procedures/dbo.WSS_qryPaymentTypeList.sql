SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryPaymentTypeList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PaymentTypeID, PaymentType 
	FROM tblPaymentType
	UNION
	SELECT NULL, ''
	ORDER BY PaymentTypeID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryPaymentTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryPaymentTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryPaymentTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryPaymentTypeList] TO [power_user]
GO
