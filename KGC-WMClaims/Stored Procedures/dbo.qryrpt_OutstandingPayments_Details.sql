SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryrpt_OutstandingPayments_Details]
AS
--BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	SET NOCOUNT ON;

	----Get over-/under-/un-paid Travelers Bills
	--DECLARE @BillsWithIssues TABLE (TravelersBillID int)
	--INSERT INTO @BillsWithIssues
	--SELECT TravelersBillID
	--FROM tblAccountsReceivable ar
	--LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	--GROUP BY TravelersBillID
	----HAVING SUM(COALESCE(arp.Amount, 0)) <> SUM(COALESCE(ar.Amount, 0))
	--ORDER BY TravelersBillID

	DECLARE @PaymentDueDate DATE
	SET @PaymentDueDate = '4/16/2013'

	DECLARE @Payments TABLE (AccountsReceivableID int, Amount money, Paid money)
	INSERT INTO @Payments
	SELECT ar.AccountsReceivableID, ar.Amount, SUM(COALESCE(arp.Amount,0))
	FROM tblAccountsReceivable ar
	LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	GROUP BY ar.AccountsReceivableID, ar.Amount
	
	--DECLARE @Payments2 TABLE (AccountsReceivableID int, Amount money, Paid money)
	--INSERT INTO @Payments2
	--SELECT ar.AccountsReceivableID, ar.Amount, SUM(COALESCE(arp.Amount,0))
	--FROM tblAccountsReceivable ar
	--LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	--GROUP BY ar.AccountsReceivableID, ar.Amount

	SELECT	CarrierName AS Carrier,
			Matter,
			DCA.DefenseCounsel AS Firm,
			tb.TravelersBillID,
			tb.ClaimantName,
			tb.AllocationName,
			tb.AllocationType,
			tb.PaymentType,
			tb.BillDate,
			tb.PaymentDueDate,
			--PolicyNum,
			--PStartDate, 
			--PEndDate,
			SUM(COALESCE(pay.Amount,0)) AS Amount,
			SUM(COALESCE(pay.Paid, 0)) AS AmountPaid,
			SUM(COALESCE(pay.Amount,0)) - SUM(COALESCE(pay.Paid, 0)) AS Outstanding
	FROM @Payments pay
		INNER JOIN tblAccountsReceivable ar ON pay.AccountsReceivableID = ar.AccountsReceivableID
		LEFT JOIN tblTravelersBill tb			ON tb.TravelersBillID = ar.TravelersBillID
		--LEFT JOIN tblAccountsReceivablePayment arp	ON ar.AccountsReceivableID = arp.AccountsReceivableID
		LEFT JOIN tblPolicy p						ON ar.PolicyID = p.PolicyID
		LEFT JOIN tblPolicyDates pd				ON p.PolicyID = pd.PolicyID
		LEFT JOIN tblCarrier c						ON p.CarrierID = c.CarrierID
		LEFT JOIN tblPolicyLimits pl				ON pl.PolicyID = p.PolicyID
		LEFT JOIN tblMatter M					ON tb.MatterID = M.MatterID
		LEFT JOIN tblDefenseCounselAddress DCA	ON DCA.DefenseCounselAddressID = tb.DefenseCounselAddressID
		--LEFT JOIN @BillsWithIssues bwi ON tb.TravelersBillID = bwi.TravelersBillID
	WHERE PayeeTypeID <> 2 AND tb.TravelersBillID <> 0 AND PaymentDueDate <= @PaymentDueDate --AND tb.TravelersBillID NOT IN (
	
	--SELECT distinct ar.TravelersBillID
	--FROM @Payments2 pay
	--	INNER JOIN tblAccountsReceivable ar ON pay.AccountsReceivableID = ar.AccountsReceivableID
	--	LEFT JOIN tblTravelersBill tb			ON tb.TravelersBillID = ar.TravelersBillID
	--	--LEFT JOIN tblAccountsReceivablePayment arp	ON ar.AccountsReceivableID = arp.AccountsReceivableID
	--	LEFT JOIN tblPolicy p						ON ar.PolicyID = p.PolicyID
	--	LEFT JOIN tblPolicyDates pd				ON p.PolicyID = pd.PolicyID
	--	LEFT JOIN tblCarrier c						ON p.CarrierID = c.CarrierID
	--	LEFT JOIN tblPolicyLimits pl				ON pl.PolicyID = p.PolicyID
	--	--LEFT JOIN @BillsWithIssues bwi ON tb.TravelersBillID = bwi.TravelersBillID
	--WHERE PayeeTypeID <> 2	AND tb.TravelersBillID <> 0
	--GROUP BY
	--		pay.AccountsReceivableID,
	--		ar.TravelersBillID,
	--		CarrierName,
	--		PolicyNum,
	--		PStartDate, 
	--		PEndDate	
	--)
	GROUP BY
			CarrierName,
			DCA.DefenseCounsel,
			Matter,
			tb.TravelersBillID,
			tb.ClaimantName,
			tb.AllocationName,
			tb.AllocationType,
			tb.PaymentType,
			tb.BillDate,
			tb.PaymentDueDate
	ORDER BY --TravelersBillID
	CarrierName, DCA.DefenseCounsel, Matter
--END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrpt_OutstandingPayments_Details] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrpt_OutstandingPayments_Details] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrpt_OutstandingPayments_Details] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrpt_OutstandingPayments_Details] TO [power_user]
GO
