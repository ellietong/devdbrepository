SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_QC_qryUnconfirmedTravelersBills]
--(@TravelersBillID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Create Table #AccountsReceivable  (policyID int, TravelersBillID int, Amount money)
	Insert into #AccountsReceivable
	Select PolicyID, ar.TravelersBillID, SUM(Amount)
	From tblAccountsReceivable ar
	inner join tblTravelersBill tb on tb.TravelersBillID = ar.TravelersBillID
	where coalesce(tb.AllocationQCConfirmation,0) <> 1 and billstatusid <> 2
	Group By PolicyID, ar.TravelersBillID

	SELECT occurrenceID, LondonPolicyID, SUM(COALESCE(RespreadIndemnity, 0) + COALESCE(RespreadDefense, 0) + COALESCE(RespreadDefenseOutsideLimits,0)) AS LondonRespread
	Into #LondonRespread
	FROM m_tblAllocationResultsLondonRespread GROUP BY OccurrenceID, LondonPolicyID

	
	SELECT tb.TravelersBillID, 
	CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
		CASE WHEN FirstName IS NULL 
		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName,
	--Matter, 
	COALESCE(tb.InvoiceNumber, 'N/A') AS InvoiceNumber, 
	PaymentType, 
	SUM(e.IndemnityAllocation + e.DefenseAllocation + e.DefenseOutsideLimitsAllocation - COALESCE(LondonRespread,0)) AS KCICTotal,  SUM(ar.Amount) AS TravelersTotal,
	SUM(e.IndemnityAllocation + e.DefenseAllocation + e.DefenseOutsideLimitsAllocation) - SUM(COALESCE(LondonREspread,0)) -  SUM(ar.Amount) AS [Difference]
	FROM  tblTravelersBill  tb
	INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
	inner join tblMultiClaimantDocument mcd on mcd.DocumentID=d.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	--INNER JOIN tblClaim c on cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	--INNER JOIN tblMatter m on c.MatterID = m.MatterID and m.MatterID=tb.MatterID
	--INNER JOIN tblClaimType ct ON tb.ClaimTypeID = ct.ClaimTypeID	
	INNER JOIN tblOccurrence o ON tb.TravelersBillID = o.TravelersBillID /********** Need to address the issue of TBills changing since this pulling from a different database*/
	INNER JOIN tblExhaustion e ON o.OccurrenceID = e.OccurrenceID
	INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
	INNER JOIN #AccountsReceivable ar ON tb.TravelersBillID = ar.TravelersBillID AND ar.PolicyID = pd.PolicyID
	LEFT JOIN tblSuffix s ON cpi.SuffixID = s.SuffixID
	LEFT JOIN  #LondonRespread lr  ON e.OccurrenceID = lr.OccurrenceID AND pd.PolicyID  = lr.LondonPolicyID
	WHERE coalesce(tb.AllocationQCConfirmation,0) <> 1 and BillStatusID <> 2
	GROUP BY	tb.TravelersBillID,
				CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
		CASE WHEN FirstName IS NULL 
		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END,
				--Matter, 
				COALESCE(tb.InvoiceNumber, 'N/A'), 
				PaymentType
	ORDER BY CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
		CASE WHEN FirstName IS NULL 
		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END


DROP TABLE #LondonRespread 
DROP TABLE #AccountsReceivable
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_QC_qryUnconfirmedTravelersBills] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_QC_qryUnconfirmedTravelersBills] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_QC_qryUnconfirmedTravelersBills] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_QC_qryUnconfirmedTravelersBills] TO [power_user]
GO
