SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryJurisdictionList]
AS
SET NOCOUNT ON

SELECT JurisdictionID, 
	Jurisdiction 
FROM tblJurisdiction
UNION
SELECT NULL, ''
ORDER BY Jurisdiction

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryJurisdictionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryJurisdictionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryJurisdictionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryJurisdictionList] TO [power_user]
GO
