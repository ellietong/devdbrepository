SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryMatterType_Filtered]
@ClaimantPersonalInfoID int
	-- Add the parameters for the stored procedure here

AS	
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	


    -- Insert statements for procedure here
SELECT C.MatterID, Matter FROM 
tblClaim C INNER JOIN tblMatter M ON C.MatterID = M.MatterID
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
ORDER BY Matter
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryMatterType_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryMatterType_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryMatterType_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryMatterType_Filtered] TO [power_user]
GO
