SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryLondonShadowAllocation]
AS
BEGIN
	SET NOCOUNT ON;



	SELECT CASE WHEN InsuranceProgramID =1 THEN 'Schedule A1'
				WHEN InsuranceProgramID in (2,5,6,7) THEN 'Schedule A2'
				WHEN InsuranceProgramID in (3) THEN 'Schedule A3' end as Schedule,
				PolicyNum,
				pl.PerOccLimit, coalesce(sum(e.indemnityallocation), 0) as Exhaustion,
				pl.PerOccLimit- coalesce(sum(e.indemnityallocation), 0) AS RemainingLimit
			
	FROM tblPolicy p
	INNER JOIN tblPolicyDates pd				ON p.PolicyID = pd.PolicyID
	INNER JOIN tblCarrier c						ON p.CarrierID = c.CarrierID
	INNER JOIN tblPolicyLimits pl				ON pl.PolicyID = p.PolicyID
	LEFT JOIN tblExhaustion E					ON E.PolicyDateID = pd.PolicyDateID
	WHERE p.PolicyID not in (280,281) and c.CarrierID =1002
	Group by PolicyNum, PStartDate, InsuranceProgramID, PerOccLimit
	order by schedule, PStartDate
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryLondonShadowAllocation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryLondonShadowAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryLondonShadowAllocation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryLondonShadowAllocation] TO [power_user]
GO
