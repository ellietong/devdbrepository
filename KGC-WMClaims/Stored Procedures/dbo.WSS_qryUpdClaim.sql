SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdClaim]
	(@ClaimantLawsuitID bigint, @LawsuitID bigint,
	@DocketNumber nvarchar(100),
	@CaseCaption nvarchar(1000),
	@LeadPlaintiffLastName nvarchar(100),
	@LeadPlaintiffFirstName nvarchar(100),
	@JurisdictionID int, 
	@StateID int,
	@FileDate datetime,
	@ServiceDate datetime,
	@StatusDate datetime,
	@LawsuitStatusID int,
	@SettlementDate datetime,
	@SettlementAmount smallmoney,
	@FirmID int, 
	@Emailaddress nvarchar(250)) 
	
	
AS
--BEGIN
	
--	UPDATE tblLawsuit
--	SET DocketNumber = @DocketNumber,
--		CaseCaption = @CaseCaption,
--		LeadPlaintiffFirstName = @LeadPlaintiffLastName,
--		LeadPlaintiffLastName = @LeadPlaintiffFirstName,
--		JurisdictionID = @JurisdictionID,
--		FileDate = @FileDate,
--		ServiceDate = @ServiceDate,
--		StateID = @StateID,
--		FirmID = @FirmID,
--		SPUsername = @Emailaddress
--	WHERE LawsuitID = @LawsuitID
	
--	UPDATE tblClaimantLawsuit
--	SET SettlementAmount = @SettlementAmount,
--		SettlementDate = @SettlementDate,
--		StatusDate = @StatusDate,
--		LawsuitStatusID = @LawsuitStatusID,
--		SPUsername = @Emailaddress
--	WHERE ClaimantLawsuitID = @ClaimantLawsuitID		
--END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaim] TO [power_user]
GO
