SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyOverduePayments]

AS
--BEGIN
--/*Query run daily to send an alert email to send a summary of the new tenders that have been created in the past week. - BEH*/

----Declare and set email inputs.
--Declare @Subject nvarchar(100)
--Declare @LogoHeader nvarchar(2000)

--SET @Subject =    'Waste Management: Overdue Payments Summary ' + CONVERT(nvarchar, GETDATE(), 107)
----Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
----KCIC logo is temporarily being pulled from D. Snyder's Google site    

--SET @LogoHeader = '<FONT FACE = "Calibri"> 
--<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--DECLARE @AgingAccountsReceivable TABLE (CarrierName nvarchar(250), AmountCurrent money, Amount30 money, Amount60 money, Amount90 money, 
--		Amount120 money, AmountLong money, AmountOverdue money, AmountTotal money, Count30 money, Count60 money,
--		Count90 money, Count120 money, CountLong money, CountTotal money, Remainder int)

--INSERT INTO @AgingAccountsReceivable
--SELECT COALESCE(CarrierName, 'Resco'), --Non-pivoted column
--	--columns to pivot
--	SUM(COALESCE(AmountCurrent, 0)) AS AmountCurrent,
--    SUM(COALESCE(Amount30, 0)) AS Amount30,
--    SUM(COALESCE(Amount60,0)) AS Amount60,
--    SUM(COALESCE(Amount90, 0)) AS Amount90,
--    SUM(COALESCE(Amount120, 0)) AS Amount120,
--    SUM(COALESCE(AmountLong, 0)) AS AmountLong,
--    SUM(COALESCE(Amount30,0) + COALESCE(Amount60,0) + COALESCE(Amount90,0) + COALESCE(Amount120,0) + COALESCE(AmountLong,0)) AS AmountOverdue,
--    SUM(COALESCE(Amount30,0) + COALESCE(Amount60,0) + COALESCE(Amount90,0) + COALESCE(Amount120,0) + COALESCE(AmountLong,0) +COALESCE(AmountCurrent, 0)) AS AmountTotal,
--    SUM(CASE WHEN COALESCE(Amount30, 0) > 0 THEN Count ELSE 0 END) AS Count30,
--    SUM(CASE WHEN COALESCE(Amount60, 0) > 0 THEN Count ELSE 0 END) AS Count60,
--    SUM(CASE WHEN COALESCE(Amount90, 0) > 0 THEN Count ELSE 0 END) AS Count90,
--    SUM(CASE WHEN COALESCE(Amount120, 0) > 0 THEN Count ELSE 0 END) AS Count120,
--    SUM(CASE WHEN COALESCE(AmountLong, 0) > 0 THEN Count ELSE 0 END) AS CountLong,
--    SUM(Count) AS CountTotal, 
--     ROW_NUMBER() OVER (ORDER BY COALESCE(CarrierName, 'Resco'))% 2 AS Remainder
--FROM
--	--where most of the magic happens
--    (SELECT CarrierGroupName AS CarrierName, 
--			SUM(COALESCE(ar.Amount,0)-COALESCE(arp.Amount, 0)) as Amount, 
--			CASE	WHEN GETDATE() - PaymentDueDate <= 0  THEN 'AmountCurrent'
--					WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 30 THEN 'Amount30'
--					WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 60 THEN 'Amount60'
--					WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 90 THEN 'Amount90'
--					WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 120 THEN 'Amount120'
--					WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate > 120 THEN 'AmountLong' END AS AmountColumn,
--			COUNT(DISTINCT ar.TravelersBillID) AS Count
--FROM tblAccountsReceivable ar
--	LEFT JOIN tblAccountsReceivablePayment arp	ON ar.AccountsReceivableID = arp.AccountsReceivableID
--	LEFT JOIN tblTravelersBill tb ON ar.TravelersBillID = tb.TravelersBillID
--	LEFT JOIN tblPolicy p ON ar.PolicyID = p.PolicyID
--	LEFT JOIN tblCarrier c ON p.CarrierID = c.CarrierID
--	LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID
--WHERE PaymentDueDate IS NOT NULL AND PayeeTypeID <> 2
--GROUP BY CarrierGroupName, 
--	CASE	WHEN GETDATE() - PaymentDueDate <= 0  THEN 'AmountCurrent'
--			WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 30 THEN 'Amount30'
--			WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 60 THEN 'Amount60'
--			WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 90 THEN 'Amount90'
--			WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate <= 120 THEN 'Amount120'
--			WHEN GETDATE() - PaymentDueDate > 0 AND GETDATE() - PaymentDueDate > 120 THEN 'AmountLong' END
--HAVING SUM(COALESCE(arp.Amount, 0)) <> SUM(COALESCE(ar.Amount, 0)))

--    AS counts

--PIVOT

--(
--	SUM(Amount)

--FOR
--AmountColumn

--    IN (AmountCurrent, Amount30, Amount60, Amount90, Amount120, AmountLong)

--) AS Pivoted
--GROUP BY CarrierName


--DECLARE @5DayPayments TABLE (ID int NOT NULL IDENTITY(1,1) PRIMARY KEY, CarrierName nvarchar(250), DefenseCounsel nvarchar(500), ClaimantName nvarchar(300), ClaimType nvarchar(250), InvoiceNumber nvarchar(250), AmountBilled nvarchar(150), DueDate nvarchar(250), Owed money, Remainder int)
----DECLARE @5DayPayment TABLE  (ID int NOT NULL IDENTITY(1,1) PRIMARY KEY, CarrierName nvarchar(250), DefenseCounsel nvarchar(500), ClaimantName nvarchar(300), ClaimType nvarchar(250), InvoiceNumber nvarchar(250), AmountBilled nvarchar(150), DueDate nvarchar(250), Owed money) --, Remainder int)
--DECLARE @Payments TABLE (AccountsReceivableID int, Amount money, Paid money, PayeeTypeID nvarchar(250))
--INSERT INTO @Payments
--SELECT ar.AccountsReceivableID, ar.Amount, SUM(COALESCE(arp.Amount,0)), PayeeTypeID
--FROM tblAccountsReceivable ar
--LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
--GROUP BY ar.AccountsReceivableID, ar.Amount, PayeeTypeID

--INSERT INTO @5DayPayments
--SELECT COALESCE(CarrierGroupName, 'Resco') AS CarrierName, Firm as Payee, CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
--		CASE WHEN FirstName IS NULL 
--		THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, ClaimType,
--		CASE WHEN (SettlementAmount IS NULL OR SettlementAmount = 0) AND InvoiceNumber IS NOT NULL THEN InvoiceNumber ELSE 'Settlement' END,
--		SUM(ar.Amount) AS 'Amount Billed', CONVERT(nvarchar(100),(PaymentDueDate), 101), 
--		SUM(ar.Amount)  - SUM(COALESCE(pay.Paid, 0)) AS Outstanding, 0
--FROM tblAccountsReceivable ar 
--LEFT JOIN tblTravelersBill tb ON tb.TravelersBillID = ar.TravelersBillID
--INNER JOIN tblFirm F ON tb.FirmID = f.FirmID
--INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
--INNER JOIN tblClaimantPersonalInfo cpi ON d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
--LEFT JOIN tblSuffix s ON cpi.SuffixID = s.SuffixID
--LEFT JOIN tblPolicy p ON ar.PolicyID = P.PolicyID
--LEFT JOIN tblCarrier c ON p.CarrierID = c.CarrierID
--LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID
--LEFT JOIN tblPayeeType pt ON ar.PayeeTypeID = pt.PayeeTypeID
--INNER JOIN tblClaimType ct ON cpi.ClaimTypeID = ct.ClaimTypeID
--LEFT JOIN @Payments pay ON pay.AccountsReceivableID = ar.AccountsReceivableID
--WHERE PaymentDueDate < GETDATE() -- PaymentDueDate <= GETDATE()+5 AND PaymentDueDate >= GETDATE()+5
--GROUP BY COALESCE(CarrierGroupName, 'Resco'), Firm, ClaimType, PayeeType, LastName, FirstName, MiddleName, cpi.SuffixID, Suffix, InvoiceNumber, SettlementAmount, InvoicedTotal, PaymentDueDate
--HAVING SUM(ar.Amount)  - SUM(COALESCE(pay.Paid, 0)) >0
--ORDER BY COALESCE(CarrierGroupName, 'Resco'), ClaimType, PaymentDueDate, ClaimantName, InvoiceNumber
----SELECT GETDATE()+5
----SELECT PaymentDueDate from tblTravelersBill

----Calculate the remainer for every-other row highlighting
--UPDATE @5DayPayments
--SET Remainder = fdp.Remainder 
--FROM (SELECT ID, (ID % 2) AS Remainder FROM @5DayPayments) fdp
--INNER JOIN @5DayPayments ofdp ON fdp.ID = ofdp.ID
--WHERE ofdp.ID = fdp.ID

--DECLARE @CarrierList TABLE (CarrierName nvarchar(250))
--INSERT INTO @CarrierList
--SELECT DISTINCT CarrierName 
--FROM @5DayPayments

--DECLARE @FirmList TABLE (DefenseCounsel nvarchar(250))
--INSERT INTO @FirmList
--SELECT DISTINCT DefenseCounsel 
--FROM @5DayPayments

--DECLARE @OwedtoPayee TABLE (CarrierName nvarchar(250), DefenseCounsel nvarchar(500), OwedToPayee money)	
--INSERT INTO @OwedtoPayee
--	SELECT CarrierName, DefenseCounsel, Sum(Owed) AS 'OwedtoPayee'
--	FROM (SELECT * FROM @5DayPayments) tblp
--	GROUP BY CarrierName, DefenseCounsel
--	ORDER BY CarrierName, DefenseCounsel
----SELECT * FROM @OwedtoPayee	
	
--DECLARE @OwedByCarrier TABLE (CarrierName nvarchar(250), OwedByCarrier money)	
--INSERT INTO @OwedByCarrier
--SELECT CarrierName, SUM(OwedToPayee)
--FROM (SELECT * FROM @OwedtoPayee) tblotp
--GROUP BY CarrierName
--ORDER BY CarrierName
----SELECT * FROM @OwedByCarrier

----INSERT INTO @5DayPayments
----SELECT *, ROW_NUMBER() OVER (ORDER BY ID)% 2 AS Remainder FROM @5DayPayment
----ORDER BY ClaimType, DueDate, ClaimantName, InvoiceNumber


--DECLARE @Remainder TABLE (arID int, RowCounter int, Remainder int)
--INSERT INTO @Remainder
--SELECT ID,
--    ROW_NUMBER() OVER (ORDER BY ID) AS Row_Counter,  ROW_NUMBER() OVER (ORDER BY ID)% 2 AS Remainder    
--FROM @5DayPayments
----SELECT Remainder FROM @Remainder


----DECLARE @OverDueByPIR TABLE (ID int, Firm nvarchar(250), CNA money, INA money, London money, Resco money, Travelers money, Total money, Remainder int)
--DECLARE @OverDueByPI TABLE (ID int NOT NULL IDENTITY(1,1) PRIMARY KEY, Firm nvarchar(250), CNA money, INA money, London money, Resco money, Travelers money, Total money, Remainder int)
--DECLARE @AggregatedPayments TABLE (TravelersBillID bigint, CarrierGroupID int, TotalPayments money)

--INSERT INTO @AggregatedPayments
--SELECT TravelersBillID, COALESCE(CarrierGroupID, 0), SUM(COALESCE(arp.Amount, 0))
--FROM tblAccountsReceivable ar
--LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
--LEFT JOIN tblPolicy p ON ar.PolicyID = p.PolicyID
--LEFT JOIN tblCarrier c ON p.CarrierID = c.CarrierID
--WHERE PayeeTypeID <> 2
--GROUP BY TravelersBillID, CarrierGroupID

--DECLARE @AggregatedReceivables TABLE (TravelersBillID bigint, CarrierGroupID int, TotalReceivable money)

--INSERT INTO @AggregatedReceivables
--SELECT TravelersBillID, COALESCE(CarrierGroupID, 0), SUM(COALESCE(ar.Amount, 0))
--FROM tblAccountsReceivable ar
--LEFT JOIN tblPolicy p ON ar.PolicyID = p.PolicyID
--LEFT JOIN tblCarrier c ON p.CarrierID = c.CarrierID
--WHERE PayeeTypeID <> 2
--GROUP BY TravelersBillID, CarrierGroupID

--INSERT INTO @OverDueByPI
--SELECT COALESCE(Firm, 'Unlinked Settlements'), 
--COALESCE([CNA Insurance Companies], 0) AS [CNA Insurance Companies], 
--COALESCE([ACE INA Group], 0) AS [ACE INA Group],
--COALESCE([London Market], 0) AS [London Market], 
--COALESCE([Resco], 0) AS [Resco], 
--COALESCE([The Travelers Companies, Inc.], 0) AS [The Travelers Companies],
--COALESCE([CNA Insurance Companies], 0)+ COALESCE([ACE INA Group], 0)+COALESCE([London Market], 0)+COALESCE([Resco], 0)+COALESCE([The Travelers Companies, Inc.], 0) AS Total,
--0
--FROM
--(SELECT Firm, 
--                        COALESCE(SUM(COALESCE(ar.TotalReceivable,0)-COALESCE(ap.TotalPayments, 0)), 0) as Amount, coalesce(CarrierGroupName, 'Resco') as CarrierGroupName
--FROM @AggregatedPayments ap
--LEFT JOIN @AggregatedReceivables ar ON ap.CarrierGroupID = ar.CarrierGroupID AND ap.TravelersBillID = ar.TravelersBillID
--LEFT JOIN tblTravelersBill tb ON ar.TravelersBillID = tb.TravelersBillID
--LEFT JOIN tblFirm f ON tb.FirmID = f.FirmID
--LEFT JOIN tblCarrierGroup cg ON ar.CarrierGroupID = cg.CarrierGroupID
        
--WHERE PaymentDueDate < GETDATE() --AND PayeeTypeID <> '2'
--GROUP BY CarrierGroupName, Firm)

--    AS carriers

--PIVOT

--(
--        SUM(Amount)

--FOR
--CarrierGroupName

--    IN ([CNA Insurance Companies], [ACE INA Group], [London Market], [Resco], [The Travelers Companies, Inc.])

--) AS Pivoted

----Update the remainder based on the PK
--UPDATE @OverDueByPI
--SET Remainder = remainder.Remainder 
--FROM (SELECT ID, (ID % 2) AS Remainder FROM @OverDueByPI) remainder
--INNER JOIN @OverDueByPI ODBPI ON remainder.ID = ODBPI.ID
--WHERE ODBPI.ID = remainder.ID


----Declare and populate the body of the email for each tender type
--DECLARE @BodyMessage nvarchar(MAX)
----Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
----TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
--SET @BodyMessage = @LogoHeader + 
--'<STYLE TYPE="text/css">
--<!--
--TABLE {border-collapse: collapse;
--      border: 1px solid #38160C;
--      color: #F6ECF0;}
--TD    {font-family: Calibri; 
--      font-size: 9pt; 
--      text-align: center;
--      border: 1px solid transparent;
--      /*Added*/
--      border-top: 1px solid rgb(0,121,193);
--      padding: .3em;
--      color: #000000}
--TH    {font-family: Calibri; 
--		border: 1px solid #ffffff;
--      padding: .3em;
--      color: #F6ECF0;
--      font-size: 9pt;
--      background: rgb(0,121,193);}
--UL {margin:0; 
--	padding:0;}	 
--.Aging TH    { border: none;
--      padding: .3em;
--      color: #F6ECF0;
--      background: rgb(0,121,193);}	
--.FOOT    {/*border-top: 2px solid #FDBB30;*/
--      padding: .3em;
--      color: #F6ECF0;
--       border: none;}    
--.Footer     {font-family: Calibri; 
--      font-size: 10pt; 
--      text-align: center;
--      border: 1px solid transparent;
--      border-top: 2px solid #FDBB30;
--      padding: .3em;
--      color: #000000;
--      font-weight:bold;}   
--.Even {background-color: #D6E8FF;}              
    
--   .mainTitle
--   {
--	font-weight: bold;
--	font-size: large;
--	padding-bottom: .25em;
--	margin-bottom: .25em;
--   }
--   .subTitle
--   {
--    font-weight: bold;
--	font-size: medium;
--	padding: 0; margin: 0;
--   }
----->
--</STYLE>'

--DECLARE @CurrentTotal nvarchar(250)
--SET @CurrentTotal = (SELECT CONVERT(varchar(12), CAST( SUM(AmountCurrent)  as MONEY),1) FROM @AgingAccountsReceivable)
----SET @CurrentTotal = (SELECT SUM(CAST(AmountCurrent AS money)) FROM @AgingAccountsReceivable)
----CONVERT(money, CurrentTotal, 1)
--DECLARE @30Total nvarchar(250)
--SET @30Total = (SELECT CONVERT(varchar(12), CAST( SUM(Amount30)  as MONEY),1) FROM @AgingAccountsReceivable)
--DECLARE @60Total nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Amount60)  as MONEY),1) FROM @AgingAccountsReceivable)
--DECLARE @90Total nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Amount90)  as MONEY),1) FROM @AgingAccountsReceivable)
--DECLARE @120Total nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Amount120)  as MONEY),1) FROM @AgingAccountsReceivable)
--DECLARE @LongTotal nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(AmountLong)  as MONEY),1) FROM @AgingAccountsReceivable)
--DECLARE @TotalOverdue nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(AmountOverdue)  as MONEY),1) FROM @AgingAccountsReceivable)
--DECLARE @AllTotal nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(AmountTotal)  as MONEY),1) FROM @AgingAccountsReceivable)
--IF (Select COUNT(CarrierName) from @5DayPayments) > 0 
--BEGIN
--SET @BodyMessage = @BodyMessage + '<P class="mainTitle">Aging Accounts Receivable</P>'
--SET @BodyMessage = @BodyMessage + '<P class="subTitle">Non-Interim Costs Owed to Lawfirms</P>'
--SET @BodyMessage = @BodyMessage + N'<table class="Aging"><tbody>' + 
--		N'<tr border-style:none><th></th><th></th><th colspan=6> Days Overdue</th><th></th></tr>' +
--		N'<tr><th>Carrier</th><th>Current</th><th>0-30</th><th>30-60</th><th>60-90</th><th>90-120</th><th> >120 </th> <th>Total <br/>Overdue</th><th>Total <br/>Outstanding</th></tr>'  
	
--			DECLARE @Carrier nvarchar(250), @AmountCurrent money, @Amount30 money, @Amount60 money, @Amount90 money, 
--					@Amount120 money, @AmountLong money, @AmountOverdue money, @AmountTotal money, @RemainderVal int

--						DECLARE AAR_curs CURSOR FOR
--			            SELECT 	CarrierName, AmountCurrent, Amount30, Amount60, Amount90, Amount120, AmountLong, AmountOverdue, AmountTotal, Remainder				            
--						FROM @AgingAccountsReceivable
						
--						OPEN AAR_curs
--						FETCH NEXT FROM AAR_curs INTO @Carrier, @AmountCurrent, @Amount30, @Amount60, @Amount90, @Amount120, @AmountLong, @AmountOverdue, @AmountTotal, @RemainderVal

						
--						--IF @LastCarrierName <> @CarrierName OR @@FETCH_STATUS = 0
--						While @@FETCH_STATUS = 0
--						BEGIN
--								IF @RemainderVal = 0 BEGIN
--								SET @BodyMessage = @BodyMessage + '<tr class="Even">'
--								END
--								ELSE BEGIN
--								SET @BodyMessage = @BodyMessage + '<tr>'
--								END						
--						    	--SET @BodyMessage = @BodyMessage + '<tr>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: left">' + @Carrier + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @AmountCurrent,1)	 + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @Amount30,1) + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @Amount60,1) + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @Amount90,1) + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @Amount120,1) + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @AmountLong,1) + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @AmountOverdue,1) + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @AmountTotal,1) + '</td></tr></tbody>'
															
								
--						FETCH NEXT FROM AAR_curs INTO @Carrier, @AmountCurrent, @Amount30, @Amount60, @Amount90, @Amount120, @AmountLong, @AmountOverdue, @AmountTotal, @RemainderVal

--						END
--						CLOSE AAR_curs
--						DEALLOCATE AAR_curs	
								
--								--SET @BodyMessage = @BodyMessage + '<tfoot class="Foot">'
--								SET @BodyMessage = @BodyMessage + '<tr class="Foot">'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer">' + 'Totals' + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @CurrentTotal + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @30Total + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @60Total + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @90Total + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @120Total + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @LongTotal + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @TotalOverdue + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @AllTotal + '</td>'
--								SET @BodyMessage = @BodyMessage + '</tr>'							
		   
            			
--SET @BodyMessage = @BodyMessage + N'</table>' +  N'<br>'    								
--								--SET @BodyMessage = @BodyMessage + N'<table border=0 cellspacing=0 cellpadding=0>' + 
---- N''--<tr><th>Carrier</th><th>Payee</th>'
--								--SET @BodyMessage = @BodyMessage + '<tr>'
--								--SET @BodyMessage = @BodyMessage + N'<table border=0 cellspacing=0 cellpadding=0>' + 
--								--	N'<tr><th>Claim Type</th><th>Claimant Name</th><th>Invoice Number</th><th>Amount Owed</th></th><th>Due Date</th>'

--END

--SET @BodyMessage = @BodyMessage + '<P class="mainTitle">Aging Accounts Receivable</P>'
--SET @BodyMessage = @BodyMessage + '<P class="subTitle">Interim Costs Owed to Resco</P>'
--SET @BodyMessage = @BodyMessage + N'<table class="Aging"><tbody>' + 
--		N'<tr border-style:none><th></th><th></th><th colspan=6> Days Overdue</th><th></th></tr>' +
--		N'<tr><th>Carrier</th><th>Current</th><th>0-30</th><th>30-60</th><th>60-90</th><th>90-120</th><th> >120 </th> <th>Total <br/>Overdue</th><th>Total <br/>Outstanding</th></tr>'  
----New interim costs owed to resco go here
--SET @BodyMessage = @BodyMessage + N'</table>' +  N'<br>'    


--SET @BodyMessage = @BodyMessage + '<P class="mainTitle">Aging Accounts Receivable</P>'
--SET @BodyMessage = @BodyMessage + '<P class="subTitle">Non-Interim Costs Owed to Resco</P>'
--SET @BodyMessage = @BodyMessage + N'<table class="Aging"><tbody>' + 
--		N'<tr border-style:none><th></th><th></th><th colspan=6> Days Overdue</th><th></th></tr>' +
--		N'<tr><th>Carrier</th><th>Current</th><th>0-30</th><th>30-60</th><th>60-90</th><th>90-120</th><th> >120 </th> <th>Total <br/>Overdue</th><th>Total <br/>Outstanding</th></tr>'  
----New non-interim costs owed to resco go here (ie: London)
--SET @BodyMessage = @BodyMessage + N'</table>' +  N'<br>'   


--DECLARE @CNAT nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(CNA)  as MONEY),1) FROM @OverDueByPI)
--DECLARE @ACET nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(INA)  as MONEY),1) FROM @OverDueByPI)
--DECLARE @LondonT nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(London)  as MONEY),1) FROM @OverDueByPI)
--DECLARE @RescoT nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Resco)  as MONEY),1) FROM @OverDueByPI)
--DECLARE @TravelersT nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Travelers)  as MONEY),1) FROM @OverDueByPI)
--DECLARE @GrandTotal nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Total)  as MONEY),1) FROM @OverDueByPI)
----DECLARE @AllTotal nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(AmountTotal)  as MONEY),1) FROM @AgingAccountsReceivable2)
--IF (Select COUNT(Firm) from @OverDueByPI) > 0 
--BEGIN
--SET @BodyMessage = @BodyMessage + '<P class="mainTitle">Overdue Amounts by Payee and Carrier</P>'
--SET @BodyMessage = @BodyMessage + N'<table class="Aging"><tbody>' + 
--		--N'<tr border-style:none><th></th><th></th><th colspan=5> Days Overdue</th><th></th></tr>' +
--		N'<tr><th>Firm</th><th>INA</th><th>CNA</th><th>London</th><th>Resco</th><th>Travelers</th><th style="text-align: right;">Total</th></tr>'  
	
--			DECLARE @Firm nvarchar(250), @CNA money, @ACE money, @London money, @Resco money, 
--					@Travelers money, @PayeeTotal money, @RemainderValPC int

--						DECLARE OPC_curs CURSOR FOR
--			            SELECT 	Firm, CNA, INA, London, Resco, Travelers, Total, Remainder				            
--						FROM  @OverDueByPI
						
--						OPEN OPC_curs
--						FETCH NEXT FROM OPC_curs INTO @Firm, @CNA, @ACE, @London, @Resco, @Travelers, @PayeeTotal, @RemainderValPC

						
--						--IF @LastCarrierName <> @CarrierName OR @@FETCH_STATUS = 0
--						While @@FETCH_STATUS = 0
--						BEGIN
--								IF @RemainderValPC = 0 BEGIN
--								SET @BodyMessage = @BodyMessage + '<tr class="Even">'
--								END
--								ELSE BEGIN
--								SET @BodyMessage = @BodyMessage + '<tr>'
--								END						
--						    	--SET @BodyMessage = @BodyMessage + '<tr>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: left">' + @Firm + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @ACE,1) + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @CNA,1)	 + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @London,1) + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @Resco,1) + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @Travelers,1) + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td style="text-align: right; font-weight: bold;">' + '$' + CONVERT(varchar(12), @PayeeTotal,1) + '</td></tr></tbody>'
															
								
--						FETCH NEXT FROM OPC_curs INTO @Firm, @CNA, @ACE, @London, @Resco, @Travelers, @PayeeTotal, @RemainderValPC

--						END
--						CLOSE OPC_curs
--						DEALLOCATE OPC_curs	
								
--								--SET @BodyMessage = @BodyMessage + '<tfoot class="Foot">'
--								SET @BodyMessage = @BodyMessage + '<tr class="Foot">'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer">' + 'Totals' + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @ACET + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @CNAT + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @LondonT + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @RescoT + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @TravelersT + '</td>'
--								SET @BodyMessage = @BodyMessage + '<td class="Footer" style="text-align: right">' + '$' + @GrandTotal + '</td>'
--								SET @BodyMessage = @BodyMessage + '</tr>'							
		   
            			
--SET @BodyMessage = @BodyMessage + N'</table>' +  N'<br>'    								
--								--SET @BodyMessage = @BodyMessage + N'<table border=0 cellspacing=0 cellpadding=0>' + 
---- N''--<tr><th>Carrier</th><th>Payee</th>'
--								--SET @BodyMessage = @BodyMessage + '<tr>'
--								--SET @BodyMessage = @BodyMessage + N'<table border=0 cellspacing=0 cellpadding=0>' + 
--								--	N'<tr><th>Claim Type</th><th>Claimant Name</th><th>Invoice Number</th><th>Amount Owed</th></th><th>Due Date</th>'

--END


--DECLARE @LastCarrierName nvarchar(250) = ''
--DECLARE @LastPayee nvarchar(250) = ''

--IF (Select COUNT(CarrierName) from @5DayPayments) > 0 
--BEGIN
--SET @BodyMessage = @BodyMessage + '<P class="mainTitle">Details of Overdue Amounts</P>'


--            --create a cursor do loop through invoices and print appropriate HTML
--            DECLARE @CarrierName nvarchar(250), @DefenseCounsel nvarchar(500), @ClaimantName nvarchar(300), @ClaimType nvarchar(250), @InvoiceNumber nvarchar(250), 
--						@Payee nvarchar(100), @AmountBilled money, @DueDate nvarchar(150), @Owed money, @RemainderValue nvarchar(3), @CarrierNameOTP nvarchar(250), @DefenseCounselOTP nvarchar(500), @CarrierNameTO nvarchar(250), @OwedToPayeeOTP money, @OwedByCarrierTO money--, @LastCarrierName nvarchar(250), @LastPayee nvarchar(250)
			
 
--            DECLARE Carrier_curs CURSOR FOR
--            SELECT DISTINCT CarrierName           
--            FROM @5DayPayments DP
--				SET @BodyMessage = @BodyMessage + N'<table border=0 cellspacing=0 cellpadding=0>'
--				SET @BodyMessage = @BodyMessage + N'<tr><th>Carrier</th><th>Payee</th><th>Claim Type</th><th>Claimant Name</th><th>Invoice Number</th><th>Amount Owed</th></th><th>Due Date</th>'
				
--            OPEN Carrier_curs
--            FETCH NEXT FROM Carrier_curs INTO @CarrierName

			
--			WHILE @@FETCH_STATUS = 0
--            --While @@FETCH_STATUS = 0
--            BEGIN
--				--Write out the carrier name, if necessary
--				IF @LastCarrierName <> @CarrierName
--				BEGIN
--					SET @BodyMessage = @BodyMessage + '<tr>'
--					SET @BodyMessage = @BodyMessage + '<td style="text-align: left; font-weight:bold; background-color: #E36F1E; color: #ffffff"colspan="6">' + @CarrierName + '</td>'
--					SET @BodyMessage = @BodyMessage + '<td style="text-align: left; background-color: #E36F1E" colspan="1"></td>'
--					SET @BodyMessage = @BodyMessage + '</tr>' 
					
--				END
								
--									DECLARE Firm_curs CURSOR FOR
--						            SELECT DISTINCT DefenseCounsel					            
--									FROM @5DayPayments DP
--									WHERE CarrierName = @CarrierName

--									OPEN Firm_curs
--									FETCH NEXT FROM Firm_curs INTO @DefenseCounsel
	
									

--									While @@FETCH_STATUS = 0
--									BEGIN
--									    IF @LastPayee <> @DefenseCounsel OR @CarrierName <> @LastCarrierName
--										BEGIN

--												SET @BodyMessage = @BodyMessage + '<tr>' 
												
--												SET @BodyMessage = @BodyMessage + '<td style="text-align: left; background-color: #D8D8D8;colspan="1""> </td>'
--												SET @BodyMessage = @BodyMessage + '<td colspan="6" style="text-align: left; font-weight:bold; background-color: #D8D8D8">' + @DefenseCounsel + '</td>'

--												SET @BodyMessage = @BodyMessage + '</tr>' 
											
--										END

									
--															DECLARE Info_curs CURSOR FOR
						            
--															SELECT ClaimType, ClaimantName, InvoiceNumber, Owed, DueDate, Remainder					            
--															FROM @5DayPayments DP
--															WHERE CarrierName = @CarrierName AND DefenseCounsel = @DefenseCounsel
--															--WHERE EXISTS (SELECT DISTINCT DefenseCounsel FROM @FirmList)

--															OPEN Info_curs
--															FETCH NEXT FROM Info_curs INTO @ClaimType, @ClaimantName, @InvoiceNumber, @AmountBilled, @DueDate, @RemainderValue

--															While @@FETCH_STATUS = 0
									

--															BEGIN

--																IF @RemainderValue = 1 BEGIN
--																SET @BodyMessage = @BodyMessage + '<tr class="Even">'
--																END
--																ELSE BEGIN
--																SET @BodyMessage = @BodyMessage + '<tr>'
--																END
--																--SET @BodyMessage = @BodyMessage + '<tr>'
--																--SET @BodyMessage = @BodyMessage + '<td>' + '&nbsp;' + '</td>'
--																SET @BodyMessage = @BodyMessage + '<td> </td>'
--																SET @BodyMessage = @BodyMessage + '<td> </td>'
--																SET @BodyMessage = @BodyMessage + '<td style="text-align: left">' + @ClaimType + '</td>'--</tr>'
--																SET @BodyMessage = @BodyMessage + '<td style="text-align: left">' + @ClaimantName + '</td>'
--																SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + @InvoiceNumber + '</td>'
--																SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + '$' + CONVERT(varchar(12), @AmountBilled,1) + '</td>'
--																SET @BodyMessage = @BodyMessage + '<td style="text-align: right">' + @DueDate + '</td></tr>'
																
--																	--SET @BodyMessage = @BodyMessage + '</td>'
--																	--SET @BodyMessage = @BodyMessage + '</tr>'															
															
--																IF(@LastPayee = '' OR @LastPayee <> @DefenseCounsel)
--																	SET @LastPayee = @DefenseCounsel
--																FETCH NEXT FROM Info_curs INTO @ClaimType, @ClaimantName, @InvoiceNumber, @AmountBilled, @DueDate, @RemainderValue

--															END
--															CLOSE Info_curs
--															DEALLOCATE Info_curs

								
--																						DECLARE OwedtoPayee_curs CURSOR FOR

--																						SELECT CarrierName, DefenseCounsel, OwedToPayee				            
--																						FROM @OwedtoPayee DP
--																						WHERE CarrierName = @CarrierName AND DefenseCounsel = @DefenseCounsel
--																						OPEN OwedtoPayee_curs
--																						FETCH NEXT FROM OwedtoPayee_curs INTO @CarrierName, @DefenseCounsel, @OwedToPayeeOTP

--																						While @@FETCH_STATUS = 0


--																						BEGIN
																						
--																								SET @BodyMessage = @BodyMessage + '<tr>'
--																								SET @BodyMessage = @BodyMessage + '<td colspan="1" style="font-weight:bold; text-align: right; font-weight:bold; background-color: #D8D8D8""/><td colspan="5" style="text-align: left; font-weight:bold; background-color: #D8D8D8">' + 'Total Owed by &nbsp;' + @CarrierName + '&nbsp;to &nbsp;' + @DefenseCounsel + '</td>'
--																								SET @BodyMessage = @BodyMessage + '<td colspan="1" style="text-align: right; font-weight:bold; background-color: #D8D8D8">' + '$' + CONVERT(varchar(12), @OwedToPayeeOTP,1) + '</td>'
--																								SET @BodyMessage = @BodyMessage + '</tr>'
																								
--																						FETCH NEXT FROM OwedtoPayee_curs INTO @CarrierName, @DefenseCounsel, @OwedToPayeeOTP


--																						END
--																						CLOSE OwedtoPayee_curs
--																						DEALLOCATE OwedtoPayee_curs
								
								
--						FETCH NEXT FROM Firm_curs INTO @DefenseCounsel
--						END
--						CLOSE Firm_curs
--						DEALLOCATE Firm_curs
--												DECLARE OwedByCarrier_curs CURSOR FOR

--												SELECT CarrierName, OwedByCarrier				            
--												FROM @OwedByCarrier DP
--												WHERE CarrierName = @CarrierName


--												OPEN OwedByCarrier_curs
--												FETCH NEXT FROM OwedByCarrier_curs INTO @CarrierName, @OwedByCarrierTO

--												While @@FETCH_STATUS = 0


--												BEGIN

--														SET @BodyMessage = @BodyMessage + '<tr>'
--														SET @BodyMessage = @BodyMessage + '<td colspan="6" style="font-weight:bold; text-align: left; font-weight:bold; background-color: #989898; color: #ffffff"">' + 'Total Owed by &nbsp;' + @CarrierName +'</td>'
--														SET @BodyMessage = @BodyMessage + '<td colspan="1" style="font-weight:bold; text-align: right; font-weight:bold; background-color: #989898;color: #ffffff"">' + '$' + CONVERT(varchar(12), @OwedByCarrierTO,1) + '</td>'
--														SET @BodyMessage = @BodyMessage + '</tr>'
														
--												FETCH NEXT FROM OwedByCarrier_curs INTO @CarrierName, @OwedToPayeeOTP


--												END
--												CLOSE OwedByCarrier_curs
--												DEALLOCATE OwedByCarrier_curs
						
--            SET @BodyMessage = @BodyMessage + '</tr>'
--            IF(@LastCarrierName = '' OR @LastCarrierName <> @CarrierName)
--				SET @LastCarrierName = @CarrierName
--            FETCH NEXT FROM Carrier_curs INTO @CarrierName

--            END
--            CLOSE Carrier_curs
--            DEALLOCATE Carrier_curs
            
						
--SET @BodyMessage = @BodyMessage + N'</table>' +  N'<br>'    

----Undo the HTML stripping from the xml path function above so embedded tags work
--SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')
--END


------Link for document library
----SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

----KCIC address and disclaimer
--SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
--1100 New York Ave., NW, Suite 650 East<br>
--Washington, DC 20005<br>
--(t) 202-772-2300<br>
--(f) 202-772-2333<br><br>

--This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'



--PRINT @BodyMessage

--EXEC msdb.dbo.sp_send_dbmail
----The wasteManagementProfile sends from @kcic.com
--@profile_name='Admin',
--@recipients = 'crookc@kcic.com',--'owenss@kcic.com', --
----@copy_recipients = '',
----@blind_copy_recipients = '',
--@subject = @subject,
--@body = @BodyMessage,
--@body_format = 'html'

--END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyOverduePayments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyOverduePayments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyOverduePayments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyOverduePayments] TO [power_user]
GO
