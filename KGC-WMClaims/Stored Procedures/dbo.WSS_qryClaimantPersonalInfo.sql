SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantPersonalInfo]
	-- Add the parameters for the stored procedure here
@ClaimantPersonalInfoID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--SELECT DISTINCT CPI.ClaimantPersonalInfoID AS ClaimantPersonalInfoID, CPI.LastName + '' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
--CASE WHEN FirstName IS NULL 
--		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
--		  CASE WHEN ClaimType IS NULL THEN '' ELSE ClaimType END AS ClaimType, cpi.DOFE, ActualDOFE, Notes, CASE WHEN SSN IS NULL THEN '' ELSE SSN END AS FullSSN,
--		  FirstName, MiddleName, LastName, CPI.ClaimTypeID, Gender, CPI.GenderID, BirthDate, DeceasedDate, CASE WHEN SSN IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SSN, 4) END AS SSN, CPI.SuffixID, Suffix, InsurerNotification, TravelersResponse
--FROM dbo.tblClaimantPersonalInfo AS CPI 
--LEFT JOIN tblClaimantLawsuit CL ON CPI.ClaimantPersonalInfoID = CL.ClaimantPersonalInfoID
--LEFT JOIN dbo.tblClaimType AS C ON CPI.ClaimTypeID = C.ClaimTypeID
--LEFT JOIN tblGender G ON CPI.GenderID = G.GenderID
--LEFT JOIN tblSuffix Su ON CPI.SuffixID = Su.SuffixID
--WHERE CPI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantPersonalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantPersonalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantPersonalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantPersonalInfo] TO [power_user]
GO
