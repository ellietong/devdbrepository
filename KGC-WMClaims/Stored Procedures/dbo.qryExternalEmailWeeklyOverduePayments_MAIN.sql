SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyOverduePayments_MAIN] 
 
AS
BEGIN
/*Query run daily to send an alert email to send a summary of the new tenders that have been created in the past week. - BEH*/
 
--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)
 
SET @Subject =    'Waste Management: Overdue Payments Summary ' + CONVERT(nvarchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    
 
SET @LogoHeader = '<FONT FACE = "Calibri"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
 
--Declare and populate the body of the email for each tender type
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      /*color: #F6ECF0;*/}
TD    {
		font-family: Calibri; 
		font-size: 9pt; 
		text-align: center;
		border: 1px solid transparent;
      /*Added*/
		border-top: 1px solid rgb(0,121,193);
		padding: .3em;
	}
      
TH    {
		font-family: Calibri; 
		border: 1px solid #ffffff;
	    padding: .3em;
		color: white;//#F6ECF0;
		font-size: 9pt;
		background: rgb(0,121,193);
      }
UL {
		margin:0; 
		padding:0;
	}	 
	
.Aging TH    { 
		border: none;
		padding: .3em;
		color: #F6ECF0;
		background: rgb(0,121,193);
	}	
.FOOT    {
	/*border-top: 2px solid #FDBB30;*/
    padding: .3em;
    color: #F6ECF0;
    border: none;
    }    
.Footer     {
		font-family: Calibri; 
		font-size: 10pt; 
		text-align: center;
		border: 1px solid transparent;
		border-top: 2px solid #FDBB30;
		padding: .3em;
		color: #000000;
		font-weight:bold;
	}   
.Even {
		background-color: #D6E8FF;
	}              
 
   .mainTitle
   {
	font-weight: bold;
	font-size: large;
	padding-bottom: .25em;
	margin-bottom: .25em;
   }
   
   .subTitle
   {
		font-weight: bold;
		font-size: medium;
		padding: 0; margin: 0;
   }
   
   .right{
		text-align: right;
   }
   
   .left{
		text-align: left;
   }
   
   .carrierTotalRight{
		font-weight:bold; 
		font-weight:bold; 
		background-color: #989898;
		color: #ffffff";
		text-align: right;
   }
   .carrierTotalLeft{
		font-weight:bold; 
		font-weight:bold; 
		background-color: #989898;
		color: #000000";
		text-align: left;
   }
   .payeeTotalRight
   {
		text-align: right; 
		font-weight:bold; 
		background-color: #D8D8D8
   }
   .payeeTotalLeft
   {
		text-align: left; 
		font-weight:bold; 
		background-color: #D8D8D8
   }
   .carrierGrouping{
		text-align: left;
		font-weight:bold; 
		background-color: #E36F1E; 
		color: #fff"
   }
   .carrierGroupingRight{
		text-align: right;
		font-weight:bold; 
		background-color: #E36F1E; 
		color: #fff";
	
   }
--->
</STYLE>'
 
--Following section of code uses stored procs with OUTPUT parameters to append HTML to the message
--It logically separates the email into sections so it's easier to digest / maintain
 
--Aging Accounts Receivable - Non-Interim Costs Owed to Lawfirms
EXEC	[dbo].[qryExternalEmailWeeklyOverduePayments_NonInterimOwedToDefenseCounsels]
		@BodyMessage = @BodyMessage OUTPUT
 
--Aging Accounts Receivable - Interim Costs Owed to Resco
EXEC	[dbo].[qryExternalEmailWeeklyOverduePayments_InterimOwedToResco]
		@BodyMessage = @BodyMessage OUTPUT
 
--Aging Accounts Receivable - Non-Interim Costs Owed to Resco
EXEC	[dbo].[qryExternalEmailWeeklyOverduePayments_NonInterimOwedToResco]
		@BodyMessage = @BodyMessage OUTPUT
 
--Overdue Amounts by Payee By Carrier
EXEC	[dbo].[qryExternalEmailWeeklyOverduePayments_OverdueByPayeeByCarrier]
		@BodyMessage = @BodyMessage OUTPUT
 
--Details of Overdue Amounts
--EXEC	[dbo].[qryExternalEmailWeeklyOverduePayments_OverdueDetails]
--		@BodyMessage = @BodyMessage OUTPUT
 
 
--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')
 
 
----Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'
 
--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>
 
This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'
 
 
 
--PRINT @BodyMessage
 
EXEC msdb.dbo.sp_send_dbmail
--The wasteManagementProfile sends from @kcic.com
@profile_name='Claims',
@recipients = 'NShoebot@wm.com; rescocip@kcic.com', --'crookc@kcic.com',--'owenss@kcic.com', --
@copy_recipients = '',
@blind_copy_recipients = '',--'crookc@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
 
--@profile_name='Claims',
--@recipients = 'hubbardb@kcic.com', --'crookc@kcic.com',--'owenss@kcic.com', --
--@copy_recipients = '',
--@blind_copy_recipients = '',
--@subject = @subject,
--@body = @BodyMessage,
--@body_format = 'html'
 
END
 

GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyOverduePayments_MAIN] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyOverduePayments_MAIN] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyOverduePayments_MAIN] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyOverduePayments_MAIN] TO [power_user]
GO
