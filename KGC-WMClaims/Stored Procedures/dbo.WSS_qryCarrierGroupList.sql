SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryCarrierGroupList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT		CASE	WHEN c.CarrierGroupID IS NULL AND CarrierName = 'Producer' THEN 10000 
								WHEN c.CarrierGroupID IS NULL AND CarrierName = 'Shadow Carrier' THEN 10001
								ELSE COALESCE(c.CarrierGroupID, 0)END AS CarrierGroupID,
						COALESCE(CarrierGroupName, CarrierName) AS CarrierGroupName
	FROM tblPolicy p 
	INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
	LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID
	WHERE CarrierName <> 'Shadow Carrier'
	UNION
	SELECT NULL, ''
	ORDER BY COALESCE(CarrierGroupName, CarrierName)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryCarrierGroupList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryCarrierGroupList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryCarrierGroupList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryCarrierGroupList] TO [power_user]
GO
