SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIM_qryDefenseCounselList] (@emailaddress nvarchar(1000))
AS

DECLARE @firmID int
SET @firmID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)
BEGIN
	
DECLARE @emaildomain nvarchar(1000)
	
SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress), LEN(@emailaddress)-CHARINDEX('@', @emailaddress)+1)
	
IF @emaildomain = '@kcicllc.com' OR @emailDomain='@kcic.com' OR @firmID = 48 OR @emaildomain = '@wm.com'
	
BEGIN
	SELECT DISTINCT DefenseCounselAddressID as FirmID, DefenseCounsel as Firm 
	FROM tblDefenseCounselAddress 
	ORDER BY DefenseCounsel
END
ELSE
BEGIN
	SELECT DISTINCT DefenseCounselAddressID AS FirmID, DefenseCounsel as Firm 
	FROM vtblPerson 
	INNER JOIN tblDefenseCounselAddress ON vtblPerson.FirmAddressID = tblDefenseCounselAddress.DefenseCounselAddressID
	WHERE Email = @EmailAddress	
END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDefenseCounselList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDefenseCounselList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryDefenseCounselList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryDefenseCounselList] TO [power_user]
GO
