SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] (@SetPymtID INT, @DocumentID int, @DocCategoryID int, @PageNums nvarchar(100), @ClaimantPersonalInfoID bigint = NULL, @Emailaddress nvarchar(250), @ClaimID int = null)
as

--DECLARE @SetPymtID int
--SET @SetPymtID = NULL
--DECLARE @EmailAddress nvarchar(100)
--set @EmailAddress = 'hubbardb@kcicllc.com' 
--DECLARE @DocumentID int
--DECLARE @DocCategoryID int
--DECLARE @PageNums nvarchar(100)

--DECLARE @ClaimantPersonalInfoID bigint
--SET @ClaimantPersonalInfoID = 70233

DECLARE @ClaimLawsuitID int
SET @ClaimLawsuitID = null

IF(@SetPymtID IS NULL AND @ClaimantPersonalInfoID IS NOT NULL)
BEGIN
	--if there is no record for this claim in tblSettlePaymentDocumentation then create one
	if (select COUNT(setpymtid) 
		from tblSettlePaymentDocumentation 
		where ClaimLawsuitID IN (SELECT ClaimLawsuitID 
								FROM tblClaimLawsuit CL 
								INNER JOIN tblClaim C ON cl.ClaimID = c.ClaimID 
								WHERE ClaimantPersonalInfoID  = @ClaimantPersonalInfoID 
								AND CL.ClaimID = @ClaimID 
								AND COALESCE(IsPrimaryForClaim, 0) = 1 
								AND COALESCE(IsCurrentClaim, 0) = 1)) = 0 
	BEGIN
		SET @ClaimLawsuitID = (SELECT cl.ClaimLawsuitID 
									FROM tblClaimLawsuit CL 
									INNER JOIN tblClaim C ON cl.ClaimID = c.ClaimID 
									LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
									WHERE ClaimantPersonalInfoID  = @ClaimantPersonalInfoID 
									AND CL.ClaimID = @ClaimID
									AND COALESCE(IsPrimaryForClaim, 0) = 1 
									AND COALESCE(IsCurrentClaim, 0) = 1)
									
		--insert claim lawsuit ID for settled record
		IF(@ClaimLawsuitID IS NOT NULL)
		BEGIN
			Insert into tblSettlePaymentDocumentation (ClaimLawsuitID, SPUsername)
			SELECT @ClaimLawsuitID, @Emailaddress
			SET @SetPymtID = @@IDENTITY
		END
	END
	ELSE IF (select COUNT(setpymtid) 
		from tblSettlePaymentDocumentation 
		where ClaimLawsuitID IN (SELECT ClaimLawsuitID 
								FROM tblClaimLawsuit CL 
								INNER JOIN tblClaim C ON cl.ClaimID = c.ClaimID 
								WHERE ClaimantPersonalInfoID  = @ClaimantPersonalInfoID 
								AND CL.ClaimID = @ClaimID
								AND COALESCE(IsPrimaryForClaim, 0) = 1 
								AND COALESCE(IsCurrentClaim, 0) = 1)) = 1
	BEGIN
	SET @SetPymtID = (select setpymtid
		from tblSettlePaymentDocumentation 
		where ClaimLawsuitID IN (SELECT ClaimLawsuitID 
								FROM tblClaimLawsuit CL 
								INNER JOIN tblClaim C ON cl.ClaimID = c.ClaimID 
								WHERE ClaimantPersonalInfoID  = @ClaimantPersonalInfoID 
								AND CL.ClaimID = @ClaimID
								AND COALESCE(IsPrimaryForClaim, 0) = 1 
								AND COALESCE(IsCurrentClaim, 0) = 1))
	END													 
END
IF @SetPymtID IS NOT NULL
BEGIN
	INSERT INTO tblSettleDocs (SetPymtID, DocumentID, DocCategoryID, PageNums, Spusername)
	SELECT @SetPymtID, @DocumentID, @DocCategoryID, @PageNums, @Emailaddress
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryInsSettleDocs] TO [power_user]
GO
