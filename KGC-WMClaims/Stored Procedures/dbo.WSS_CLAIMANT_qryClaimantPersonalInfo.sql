SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCedure [dbo].[WSS_CLAIMANT_qryClaimantPersonalInfo] (@ClaimantPersonalInfoID bigint = NULL, 
															  @emailaddress			  nvarchar(250))
AS

SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @emailaddress nvarchar(250)
--DECLARE @ClaimantPersonalInfoID bigint
--SET @emailaddress = 'marykay@wilkinsonlaw.com'
--SET @ClaimantPersonalInfoID = 16729

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE cpi.ClaimantPersonalInfoID = ' + CONVERT(nvarchar(20),@ClaimantPersonalInfoID)
						  
						   
						   


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' '
ELSE
BEGIN
	SET @JoinString = ' LEFT  JOIN tblClaim c					ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						LEFT  JOIN tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
                        LEFT  JOIN tblLawsuit l					ON cl.LawsuitID = l.LawsuitID
						LEFT JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID and dj.matterid = c.matterid '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT cpi.ClaimantPersonalInfoID, FirstName, MiddleName, LastName, cpi.SuffixID, Suffix, 
		cpi.LastName + CASE WHEN CPI.SuffixID IS NULL THEN '''' ELSE +'' '' + Suffix END + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + 
			CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' END AS ClaimantName, 
        CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN,
        Gender, 
        COALESCE(cpi.GenderID, 3) AS GenderID, 
        COALESCE(cpi.MaritalStatusID, 1) AS MaritalStatusID, 
        MaritalStatus, 
        COALESCE(nullif(COALESCE(Address1 + CHAR(13) + CHAR(10), '''') + COALESCE(Address2 + CHAR(13) + CHAR(10),'''') + 
			COALESCE(City + '', '','''') + COALESCE(s.State + '' '','''') + COALESCE(CONVERT(varchar,Zip),'''') + 
			COALESCE(''-''+CONVERT(varchar,mailzip),''''),''''),'''') as CompleteAddress, 
        BirthDate, 
        DeceasedDate, 
		City, 
		s.StateID, 
		Zip, 
		MailZip, 
		Address1, 
		Address2, 
		CASE WHEN IsSmoker = 1 THEN ''YES'' ELSE ''NO'' END AS IsSmoker, 
		SmokerStart,
		SmokerEnd, 
		PacksPerDay, 
		CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE SocialSecurityNumber END AS FullSSN,
		Notes
    FROM tblClaimantPersonalInfo cpi 
		 LEFT JOIN tblGender g ON g.GenderID = cpi.GenderID
         LEFT JOIN tblMaritalStatus ms ON ms.MaritalStatusID = cpi.MaritalStatusID
         LEFT JOIN tblState s ON cpi.StateID = s.StateID
         LEFT JOIN tblSuffix Su ON CPI.SuffixID = Su.SuffixID ' +
    	 @JoinString +
    @WhereString + '
    OPTION (RECOMPILE)'

PRINT @QueryString
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantPersonalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantPersonalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantPersonalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantPersonalInfo] TO [power_user]
GO
