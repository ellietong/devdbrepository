SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyInsurerNotification_RustFurnace_WAPC]
(@BodyMessage nvarchar(MAX) output)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT
--BEGIN	


set @BodyMessage = @BodyMessage + '
		<p style="font-family: Calibri; font-size: 10pt; text-align: left; ">
			WHEELABRATOR AIR POLLUTION CONTROL, INC. INSURERS<br/>
			RUST FURNACE COMPANY INSURERS<br/>
			
		</p>
		<p style="font-family: Calibri; font-size: 10pt; font-weight: bold; text-align: left; border: 1px solid transparent; " colspan="9">	
			<u>NOTIFICATION OF CLAIM</u><br/>
			
		<p/>
		<p style="font-family: Calibri; font-size: 10pt; font-weight: bold; text-align: left; border: 1px solid transparent; " colspan="9">
			RE: Notice of Complaint Received - See Table Below
		<p/>	
<p style="font-family: Calibri; font-size: 10pt; text-align: left; border: 1px solid transparent; " colspan="9">			
	Dear Insurers:<br/>
	<br>
		   We have received a complaint in the state of West Virginia in the above-styled 
	case. 

		   We, the insured, give you formal notice of this claim against it and require:

		   1.	Acknowledgment of your receipt of this letter;<br/>
		   2.	Identification of your file number (if determined) and designation of the<br/>
				individual at your company who will be handling the matter;
		   3.	Your immediate and unequivocal participation in defense of this matter;<br/>
		   4.	Indication that you will indemnify your insured if obligated to do so under<br/>
				the insurance policies.<br/>
		<br/>
		  The liabilities attributed to Wheelabrator Air Pollution Control, Inc. and its<br/>
		  predecessors and successors, including but not limited to Rust Furnace Company, are<br/>
		  held by Waste Management, Inc., thus requiring Waste Management, Inc.’s tender of<br/>
		  these matters to you for defense and indemnity.  The corporate history on these claims<br/>
		  has previously been provided to you and will not be reiterated here, unless specifically<br/>
		  requested.<br/>
		  <br/>	

		  We, the insured, believe that this matter can, and should be resolved with full<br/>
		  participation of its insurers and look to you for prompt and unequivocal assumption of<br/>
		  defense and indemnity.<br/>
		  <br/>
											Sincerely,<br/>
											<br/>

											WHEELABRATOR AIR POLLUTION<br/>
											CONTROL, INC.<br/>
											<br/>
											<br/>
											_____________________________
											<br/>
											Ashley Harper<br/>
											Waste Management, Inc.<br/>
											Litigation Counsel<br/>		  

	<br/>
	
 </p>'	
--END	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_RustFurnace_WAPC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_RustFurnace_WAPC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_RustFurnace_WAPC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_RustFurnace_WAPC] TO [power_user]
GO
