SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryINAShadowAllocation]
AS
BEGIN
	SET NOCOUNT ON;



	SELECT PolicyNum, 3500000 AS KelloggLimit, 
	coalesce(sum(case when o.CorporateHistoryID = 1 then E.DefenseAllocation else 0 end),0) + coalesce(sum(case when o.CorporateHistoryID = 1 then E.IndemnityAllocation else 0 end),0) as KelloggExhaustion,
	3500000 - (coalesce(sum(case when o.CorporateHistoryID = 1 then E.DefenseAllocation else 0 end),0) +coalesce( sum(case when o.CorporateHistoryID = 1 then E.IndemnityAllocation else 0 end),0)) as RemainingKellogg,
	5000000 as CombinedLimit,
	coalesce(SUM(e.defenseallocation),0) + coalesce(SUM(e.indemnityallocation),0) as CombinedExhaustion,
	5000000 - 	(coalesce(SUM(e.defenseallocation),0) + coalesce(SUM(e.indemnityallocation),0)) as RemainingCombined
			
	FROM tblPolicy p
	INNER JOIN tblPolicyDates pd				ON p.PolicyID = pd.PolicyID
	INNER JOIN tblCarrier c						ON p.CarrierID = c.CarrierID
	INNER JOIN tblPolicyLimits pl				ON pl.PolicyID = p.PolicyID
	LEFT JOIN tblExhaustion E					ON E.PolicyDateID = pd.PolicyDateID
	left join tblExhaustionDate ed				on ed.ExhaustionDateID = e.ExhaustionDateID
	LEFT JOIN tblOccurrence O					ON O.OccurrenceID = E.OccurrenceID
	WHERE p.PolicyID in (280,281) and ExhaustionTypeID = 2
	Group by PolicyNum, PStartDate
	order by PStartDate
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryINAShadowAllocation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryINAShadowAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryINAShadowAllocation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryINAShadowAllocation] TO [power_user]
GO
