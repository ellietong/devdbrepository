SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryClaimantsMissingWMDocs] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	SET NOCOUNT ON;

	--Test vars
	
	DECLARE @TravelersComparison TABLE (ClaimantPersonalInfoID bigint, ConfirmationStatusID int)
	DECLARE @MaxConfirmationStatusID int = (SELECT MAX(ConfirmationStatusID) FROM tblConfirmationStatus)
	DECLARE @MaxMatterID int = (SELECT MAX(MatterID) FROM tblMatter)
	
	INSERT INTO @TravelersComparison	
	SELECT cpi.ClaimantPersonalInfoID, 
		CASE WHEN SUM(CASE WHEN cl.DOFE is null OR c.MatterID is null THEN 1 ELSE 0 END) > 0 THEN 3 ELSE 0 END
	FROM tblClaimantPersonalInfo cpi
		INNER JOIN tblClaim C ON cpi.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit CL ON CL.ClaimID = C.ClaimID 
		LEFT JOIN (SELECT * FROm tblDocuments WHERE ClaimantDocumentTypeID NOT IN (48, 49, 40)) wmd ON cpi.ClaimantPersonalInfoID = wmd.ClaimantPersonalInfoID
	GROUP BY cpi.ClaimantPersonalInfoID


	SELECT	DISTINCT CPI.ClaimantPersonalInfoID AS ClaimantPersonalInfoID, 
			COUNT(DISTINCT(CPI.ClaimantPersonalInfoID)) As Count,
			CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  + CASE WHEN FirstName IS NULL 
				THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, 
			CASE WHEN Matter IS NULL THEN '' ELSE Matter END AS Matter,
			CL.DOFE,
			SUM(COALESCE(FinalTotal, 0) + COALESCE(TB.SettlementAmount, 0)) as TotalCosts, 
			Notes, 
			ConfirmationStatus, 
			FirstName, 
			MiddleName, 
			LastName, 
			C.MatterID, 
			Gender, 
			BirthDate, 
			DeceasedDate, 
			SocialSecurityNumber, 
			CPI.SuffixID AS SuffixID,
			Suffix
	FROM dbo.tblClaimantPersonalInfo cpi
		INNER JOIN tblClaim C ON C.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit CL ON C.ClaimID = CL.ClaimID
		LEFT JOIN dbo.tblMatter m		ON m.MatterID = C.MatterID
		LEFT JOIN tblDocuments d			ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
		LEFT JOIN tblTravelersBill tb		ON d.TravelersBillID = tb.TravelersBillID
		LEFT JOIN @TravelersComparison tc	ON cpi.ClaimantPersonalInfoID = tc.ClaimantPersonalInfoID
		LEFT JOIN tblGender g				ON CPI.GenderID = G.GenderID
		INNER JOIN tblConfirmationStatus cs ON tc.ConfirmationStatusID = cs.ConfirmationStatusID
		LEFT JOIN tblSuffix su				ON CPI.SuffixID = Su.SuffixID
	GROUP BY	
		CPI.ClaimantPersonalInfoID,  
		CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  + CASE WHEN FirstName IS NULL 
				THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END, 
		CASE WHEN Matter IS NULL THEN '' ELSE Matter END,
		CL.DOFE,
		Notes, 
		ConfirmationStatus,
	    FirstName,
	    MiddleName,
	    LastName,
	    C.MatterID, 
	    Gender, 
	    BirthDate, 
	    DeceasedDate, 
	    SocialSecurityNumber, 
	    CPI.SuffixID, 
	    Suffix 
	ORDER BY ClaimantName

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryClaimantsMissingWMDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryClaimantsMissingWMDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryClaimantsMissingWMDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryClaimantsMissingWMDocs] TO [power_user]
GO
