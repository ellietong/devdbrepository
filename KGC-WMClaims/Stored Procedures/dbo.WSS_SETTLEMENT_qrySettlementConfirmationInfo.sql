SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qrySettlementConfirmationInfo]
	-- Add the parameters for the stored procedure here
@ClaimLawsuitID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT DISTINCT CL.ClaimLawsuitID, CPI.ClaimantPersonalInfoID, CASE WHEN Suffix IS NULL THEN CPI.LastName + ', ' ELSE CPI.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
			  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, FirstName, MiddleName, LastName, Suffix, CPI.SuffixID, DocketNumber, CASE WHEN SettlementAmount IS NOT NULL THEN CONVERT( nvarchar(250), SettlementAmount, 1) ELSE '' END AS SettlementAmount,
			   CONVERT(nvarchar(100), SettlementDate, 101) AS SettlementDate, PayToCompany, CONVERT(nvarchar(100), SettlementDueDate, 101) AS SettlementDueDate, SettlementNotes, CS.SPUsername, PCA.PlaintiffCounselTaxIDNum AS NationalTaxID, 
			   pca.PlaintiffCounsel AS PlaintiffCounsel, CONVERT(nvarchar(100), GETDATE(), 101) AS NotificationDate, 
			  CASE WHEN LeadPlaintiffFirstName IS NOT NULL THEN LeadPlaintiffFirstName + ' ' + LeadPlaintiffLastName ELSE LeadPlaintiffLastName END AS LeadPlaintiff, LeadPlaintiffFirstName, LeadPlaintiffLastName, l.PlaintiffCounselAddressID,
			  CASE WHEN SettlementDueDate IS NOT NULL THEN 'To be paid ' + datename(dw,SettlementDueDate) +', ' + DATENAME(month, SettlementDueDate) + ' ' + CONVERT(nvarchar(10),DATEPART(day, SettlementDueDate)) + ', ' + CONVERT(nvarchar(20), DATEPART(year, SettlementDueDate)) ELSE '' END AS LongDate, cs.SPUsername as CreatedBy
			   --, PaymentTermsID --, CASE WHEN LeadPlaintiffFirstName IS NOT NULL THEN LeadPlaintiffLastName + ', ' + LeadPlaintiffFirstName ELSE LeadPlaintiffLastName END AS LeadPlaintiff			 

FROM tblClaimLawsuit CL
	LEFT JOIN tblClaimantSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblClaim C ON CL.ClaimID = C.ClaimID
	INNER JOIN tblClaimantPersONalInfo CPI ON c.ClaimantPersONalInfoID = CPI.ClaimantPersONalInfoID
	LEFT JOIN tblClaimantSuffix CLS ON  CPI.SuffixID =CLS.SuffixID 
	INNER JOIN tblLawsuit L ON CL.LawsuitID = L.LawsuitID
	LEFT JOIN tblPlaintiffCounselAddress PCA ON PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID

    
--SELECT DISTINCT CS.ClaimLawsuitID, CPI.ClaimantPersonalInfoID, CASE WHEN Suffix IS NULL THEN CPI.LastName + ', ' ELSE CPI.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
--						  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, FirstName, MiddleName, LastName, Suffix, CPI.SuffixID, NCCReferenceID, CASE WHEN SettlementAmount IS NOT NULL THEN CONVERT( nvarchar(250), SettlementAmount, 1) ELSE '' END AS SettlementAmount, CONVERT(nvarchar(100), SettlementDate, 101) AS SettlementDate, PayToCompany, CONVERT(nvarchar(100), SettlementDueDate, 101) AS SettlementDueDate, SettlementNotes, CS.SPUsername, PCA.PlaintiffCounselTaxIDNum AS NationalTaxID, pca.PlaintiffCounsel AS PlaintiffCounsel, NPCA.PlaintiffCounsel AS NationalPlaintiffCounsel, CONVERT(nvarchar(100), GETDATE(), 101) AS NotificationDate, 
--						  CASE WHEN LeadPlaintiffFirstName IS NOT NULL THEN LeadPlaintiffFirstName + ' ' + LeadPlaintiffLastName ELSE LeadPlaintiffLastName END AS LeadPlaintiff, LeadPlaintiffFirstName, LeadPlaintiffLastName, l.PlaintiffCounselAddressID, l.NationalPlaintiffCounselAddressID,
--						  CASE WHEN SettlementDueDate IS NOT NULL THEN 'To be paid ' + datename(dw,SettlementDueDate) +', ' + DATENAME(month, SettlementDueDate) + ' ' + CONVERT(nvarchar(10),DATEPART(day, SettlementDueDate)) + ', ' + CONVERT(nvarchar(20), DATEPART(year, SettlementDueDate)) ELSE '' END AS LongDate
--						   --, PaymentTermsID --, CASE WHEN LeadPlaintiffFirstName IS NOT NULL THEN LeadPlaintiffLastName + ', ' + LeadPlaintiffFirstName ELSE LeadPlaintiffLastName END AS LeadPlaintiff			 
--FROM tblClaimantSettlement CS
--	INNER JOIN tblClaimLawsuit CL ON CS.ClaimLawsuitID = CL.ClaimLawsuitID
--	INNER JOIN tblClaim C ON CL.ClaimID = C.ClaimID
--	INNER JOIN tblClaimantPersONalInfo CPI ON c.ClaimantPersONalInfoID = CPI.ClaimantPersONalInfoID
--	LEFT JOIN tblClaimantSuffix CLS ON  CPI.SuffixID =CLS.SuffixID 
--	INNER JOIN tblLawsuit L ON CL.LawsuitID = L.LawsuitID
--	LEFT JOIN tblPlaintiffCounselAddress PCA ON PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
--	LEFT JOIN tblPlaintiffCounselAddress_Local PCAL ON PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
--	LEFT JOIN tblPlaintiffCounselAddress NPCA ON NPCA.PlaintiffCounselAddressID = L.NationalPlaintiffCounselAddressID
--WHERE CS.ClaimLawsuitID = @ClaimLawsuitID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementConfirmationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementConfirmationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementConfirmationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementConfirmationInfo] TO [power_user]
GO
