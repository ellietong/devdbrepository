SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdOccupationInfo]
(@ClaimantEmploymentID int, @Occupation nvarchar(100), @EmpStartDate datetime, @EmpEndDate datetime, @Jobsite nvarchar(4000), @Notes nvarchar (1000), @Emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE tblClaimantEmployment
    SET OccupationID = UPPER(@Occupation),
		EmpStartDate = @EmpStartDate,
		EmpEndDate = @EmpEndDate,
		Jobsite = UPPER(@Jobsite),
		Comments = UPPER(@Notes),
		SPUsername = @Emailaddress
	WHERE ClaimantEmploymentID = @ClaimantEmploymentID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdOccupationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdOccupationInfo] TO [power_user]
GO
