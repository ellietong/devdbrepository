SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_ByMatterTotals] (@StartDate datetime, 
                                                                @EndDate datetime,
                                                                @MatterID int)
AS
SET NOCOUNT ON

--DECLARE @MatterID	int
--DECLARE @startdate	datetime
--DECLARE @enddate	datetime
--SET @MatterID	=	 null
--SET @startdate	=	'6/1/2013'
--SET @enddate	=	'6/30/2013'



----Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @DiseaseCount TABLE (ClaimID int, Matter nvarchar(100), MatterID int, YearFiled nvarchar(50), MoYearFiled nvarchar(50), MonthID int, MesoCount int, LungCancerCount int, 
OtherCancerCount int, NonMaligCount int, SilicosisCount int, UnknownCount int, unknowncount1 int, [state] nvarchar(100), stateid int)
INSERT INTO @DiseaseCount
SELECT C.ClaimID, 
	Matter, 
	C.MatterID, 
	Coalesce(year(fileDate), year(receiveddate)) as YearFiled, 
	coalesce(DateName(month,filedate), DateName(month,receiveddate)) as MonthFiled,
	coalesce(month(filedate), month(receiveddate)) as MonthID,
	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) = 1 THEN 1 ELSE 0 END AS MesoCount,
	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) = 2 THEN 1 ELSE 0 END AS LungCancerCount,
	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) = 3 THEN 1 ELSE 0 END AS OtherCancerCount,
	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) IN (4, 8) THEN 1 ELSE 0 END AS NonMaligCount,
	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) = 7 Then 1 Else 0 End as SilicosisCount,
	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) in (5,6) THEN 1 ELSE 0 END AS UnknownCount,
	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) is NULL THEN 1 ELSE 0 END AS UnknownCount1, 
	COALESCE(FullNameState, 'Unknown') AS State,
	l.StateID
FROM tblClaim C 
	LEFT Join (SELECT *
			   FROM tblClaimDisease CD 
			   WHERE PrimaryDisease = 1) CD			ON CD.ClaimID=C.ClaimID
	LEFT JOIN tblMatter M							ON c.MatterID = M.MatterID
	LEFT JOIN tblDisease D							ON CD.DiseaseID=D.DiseaseID
	LEFT JOIN tblClaimLawsuit CL					ON C.ClaimID=CL.ClaimID
	LEFT JOIN tblLawsuit L							ON CL.LawsuitID=L.LawsuitID
	LEFT JOIN tblState s							ON l.StateID = s.StateID
	LEFT JOIN tblClaimLawsuitStatusTracking cst		ON cst.ClaimLawsuitID = cl.ClaimLawsuitID
WHERE IsPrimaryForClaim = 1 
	AND C.MatterID >= COALESCE(@MatterID,0) 
	AND C.MatterID <= COALESCE(NULLIF(@MatterID,0), @maxMatterID) 
	AND ServiceDate BETWEEN COALESCE(@StartDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) AND COALESCE(@EndDate, GETDATE())  
	AND ClaimLawsuitStatusID = 22
ORDER BY COALESCE(YEAR(fileDate), YEAR(receiveddate))

SELECT Matter, 
	MatterID, 
	COUNT(ClaimID) AS NewFilings, 
	@StartDate as StartDate, 
	@EndDate as EndDate,
	SUM(MesoCount) AS MesoFilings, 
	SUM(LungCancerCount) AS LCFilings, 
	SUM(OtherCancerCount) AS OCFilings,
	SUM(NonMaligCount) AS NMFILINGS, 
	SUM(SilicosisCount) AS SilicosisFilings,
	SUM(UnknownCount+unknowncount1) AS UnknownFilings, 
	COALESCE([state], 'Unknown') AS State,
	stateid,
	SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + SilicosisCount + Unknowncount1) AS TotalCount
FROM @DiseaseCount DC
GROUP BY Matter, 
	MatterID, 
	[state], 
	stateid
ORDER BY Matter

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_ByMatterTotals] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_ByMatterTotals] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_ByMatterTotals] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilings_ByMatterTotals] TO [power_user]
GO
