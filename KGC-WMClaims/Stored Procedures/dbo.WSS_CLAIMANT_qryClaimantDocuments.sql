SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROC [dbo].[WSS_CLAIMANT_qryClaimantDocuments] (@ClaimantPersonalInfoID bigint = null, 
													  @emailaddress           nvarchar(250))
AS
 
SET NOCOUNT ON
 
----The following set of variables can be used for testing.
--DECLARE @emailaddress nvarchar(250)
--DECLARE @ClaimantPersonalInfoID bigint
--SET @ClaimantpersonalinfoID = 249492
--SET @emailaddress = 'tfalcone@swartzcampbell.com'
 
--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
IF object_id('tempdb..#Locked') IS NOT NULL
BEGIN
   DROP TABLE #Locked
END
CREATE TABLE #Locked (DocumentID bigint, Locked bit)
INSERT INTO #Locked
SELECT	mcd.DocumentID, 
		CASE WHEN (COUNT(c.ClaimID)+ COUNT(ps.PaymentSettlementID)) > 0 THEN 1 ELSE 0 END  
FROM tblClaim c
	INNER JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblClaimantSettlement cs				ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	LEFT JOIN tblPaymentSettlement ps				ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
	INNER JOIN tblLawsuit l							ON cl.LawsuitID	 = l.LawsuitID
	INNER JOIN tblMultiClaimantDocument mcd			ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuitStatusTracking clst	ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	WHERE COALESCE(c.IsCurrentClaim,0) = 1 
		AND COALESCE(IsPrimaryForClaim, 0) = 1 
		AND ClaimLawsuitStatusID IN (15,16,17)
		AND c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	GROUP BY mcd.DocumentID
 
 
DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)
 
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE mcd.ClaimantPersonalInfoID = ' + CONVERT(nvarchar(20),@ClaimantPersonalInfoID) + ' AND dg.DocumentGroupID <> 5 AND d.DocumentID NOT IN (SELECT DISTINCT COALESCE(InvoiceDocumentID, 0) 
																																								   FROM tblTravelersBill tb
																																									   INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
																																									   INNER JOIN tblMultiClaimantDocument mcd on d.DocumentID = mcd.DocumentID
																																								   WHERE mcd.ClaimantPersonalInfoID =' + CONVERT(nvarchar(20),@ClaimantPersonalInfoID) + ')' --exclude bills & invoices (linked to claimant's bills)
 
 
--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' '
ELSE
BEGIN
	SET @JoinString = ' LEFT JOIN tblClaim c2					ON c2.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						LEFT JOIN tblClaimLawsuit cl2			ON c2.ClaimID = cl2.ClaimID
                        LEFT JOIN tblLawsuit l					ON cl2.LawsuitID = l.LawsuitID
						LEFT JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID and dj.matterid = c2.matterid '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END
IF @DefenseCounselAddressID in (39,72)
BEGIN
	SET @WhereString = @WhereString + ' AND d.firmid in (39,72) '
END
 
--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
 
	'SELECT DISTINCT d.DocumentID, 
		d.DocumentTitle, 
		COALESCE(C.MatterID, D.MatterID) as MatterID, 
		COALESCE(m.Matter + COALESCE('' - '' + dc.DefendantCompany, ''''), mt.Matter + COALESCE('' - '' + dct.DefendantCompany, '''')) AS Matter, 
		''/clients/wm/cip/Pages/DocumentProfile.aspx?ItemId='' + CONVERT(nvarchar(100), d.DocumentID) + ''&source='' + ''/clients/wm/cip/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), @ClaimantPersonalInfoID) + ''' AS DocumentURL,
		cdt.ClaimantDocumentType, 
		COALESCE(f.DefenseCounsel, ''Historical'') AS Firm, 
		d.AcceptedBy, 
		d.DateAccepted, 
		d.DateCreated, 
		mcd.ClaimantPersonalInfoID, 
		CreatedBy,
		COALESCE(Locked, 0) AS Locked,
		Comments,
		AmazonKey,
		CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical  
	FROM tblMultiClaimantDocument mcd 
		INNER JOIN tblDocuments d				ON mcd.DocumentID = d.documentID
		INNER JOIN tblClaimantPersonalInfo cpi	ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		LEFT JOIN tblComplaintTrackingLawsuit ctl on ctl.ComplaintTrackingID = d.ComplaintID
		LEFT JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = ctl.ClaimLawsuitID
		LEFT JOIN tblClaim c on c.ClaimID = cl.ClaimID
		LEFT JOIN tblallegation a on a.claimlawsuitid = ctl.claimlawsuitID
		INNER JOIN tblClaimantDocumentType cdt	ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
		INNER JOIN tblDocumentGroup dg			ON dg.DocumentGroupID = cdt.DocumentGroupID
		LEFT JOIN tblMatter m					ON m.MatterID = d.MatterID
		LEFT JOIN tblMatter mt					ON mt.MatterID = c.MatterID
		LEFT JOIN  tbldefensecounseladdress f	ON f.defensecounseladdressid = d.firmid 
		LEFT JOIN tblDefendantCompany dc		ON d.DefendantCompanyID = dc.DefendantCompanyID
		LEFT JOIN tblDefendantCompany dct		ON a.DefendantCompanyID = dct.DefendantCompanyID
		LEFT JOIN #Locked lock					ON mcd.DocumentID = lock.DocumentID' +
		@JoinString +
   @WhereString + '
   ORDER BY DateCreated DESC
   OPTION (RECOMPILE)'
 
PRINT @QueryString
 
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString
 
 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryClaimantDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryClaimantDocuments] TO [power_user]
GO
