SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryUpdOccupationInfo] (@ClaimantEmploymentID int, 
															 @OccupationID int, 
															 @EmpStartDate datetime, 
															 @EmpEndDate datetime, 
															 @Username nvarchar(250), 
															 @Jobsite nvarchar(4000), 
															 @JobsiteCity nvarchar(250), 
															 @JobsiteStateID int,
															 @IsSecondaryExposure bit)
AS
SET NOCOUNT ON

UPDATE tblClaimantEmployment
    SET OccupationID		= @OccupationID,
		EmpStartDate		= @EmpStartDate,
		EmpEndDate			= @EmpEndDate,
		SPUsername			= @Username,
		Jobsite				= UPPER(@Jobsite),
		JobsiteCity			= UPPER(@JobsiteCity),
		JobsiteStateID		= @JobsiteStateID,
		IsSecondaryExposure = @IsSecondaryExposure
	WHERE ClaimantEmploymentID = @ClaimantEmploymentID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryUpdOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryUpdOccupationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryUpdOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryUpdOccupationInfo] TO [power_user]
GO
