SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_PAYMENTS_qryClaimantList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName
	FROM tblClaimantPersonalInfo cpi
	INNER JOIN tblMultiClaimantDocument mcd on cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	INNER JOIN tblDocuments d ON mcd.DocumentID = d.DocumentID
	INNER JOIN tblTravelersBill tb  ON d.TravelersBillID = tb.TravelersBillID
	WHERE d.TravelersBillID IN 
		(SELECT TravelersBillID FROM
		tblAccountsReceivable ar
		LEFT JOIN (SELECT AccountsReceivableID, SUM(Amount) AS PaidAmount FROM tblAccountsReceivablePayment GROUP BY AccountsReceivableID) arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
		WHERE ar.Amount - COALESCE(arp.PaidAmount, 0) <> 0 ) --Claimants who have accounts receivable records that are not full paid
	UNION
	SELECT NULL
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_PAYMENTS_qryClaimantList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_PAYMENTS_qryClaimantList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_PAYMENTS_qryClaimantList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_PAYMENTS_qryClaimantList] TO [power_user]
GO
