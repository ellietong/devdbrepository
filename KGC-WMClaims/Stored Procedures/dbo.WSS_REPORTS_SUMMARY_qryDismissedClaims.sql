SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_SUMMARY_qryDismissedClaims]
	(@MatterID int,
	 @StartDate datetime,
	 @EndDate datetime)
AS
SET NOCOUNT ON

/*---Debugging---*/
--DECLARE @StartDate	DATETIME
--DECLARE @EndDate	DATETIME
--DECLARE @Matterid	INT

--SET @StartDate	= NULL--'1/1/2000'
--SET @EndDate	= NULL--'9/30/2012'
--SET @Matterid	= NULL

--Find Max Matter ID
DECLARE @maxMatterID INT
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @DiseaseCount TABLE
	(Matter NVARCHAR(100),
	 MatterID INT,
	 claimlawsuitstatus NVARCHAR (500),
	 DateDismissed DATETIME,
	 MesoCount INT,
	 LungCancerCount INT,
	 OtherCancerCount INT,
	 NonMaligCount INT,
	 UnknownCount INT,
	 unknowncount1 int)

INSERT INTO @DiseaseCount

SELECT Matter, 
	C.MatterID,  
	css.claimlawsuitstatus,
	COALESCE(CST.StatusDate,GETDATE()) AS DateDismissed,
	Case when DMeso.HighLevelDiseaseID = 1		Then 1 Else 0 End AS MesoCount,
	Case when DLC.HighLevelDiseaseID = 2		Then 1 Else 0 End AS LungCancerCount,
	Case when DOC.HighLevelDiseaseID = 3		Then 1 Else 0 End AS OtherCancerCount,
	Case when DNM.HighLevelDiseaseID in (4,7,8)	Then 1 Else 0 End AS NonMaligCount,
	Case when DUnk.HighLevelDiseaseID in (5,6)	Then 1 Else 0 End AS UnknownCount,
	Case when DUnk.HighLevelDiseaseID is NULL	Then 1 Else 0 End AS UnknownCount1
FROM tblClaim C	
	LEFT Join (Select * 
			   From tblClaimDisease CD 
			   Where PrimaryDisease = 1) CD		ON CD.ClaimID=C.ClaimID
	LEFT JOIN tblMatter M						ON c.MatterID = M.MatterID
	LEFT JOIN tblClaimSequence csq				ON c.ClaimSequenceID = csq.ClaimSequenceID
	Left Join tblDisease DMeso					ON CD.DiseaseID=DMeso.DiseaseID
	Left Join tblDisease DLC					ON CD.DiseaseID=DLC.DiseaseID
	Left Join tblDisease DOC					ON CD.DiseaseID=DOC.DiseaseID
	Left Join tblDisease DNM					ON CD.DiseaseID=DNM.DiseaseID
	Left Join tblDisease DUnk					ON CD.DiseaseID=DUnk.DiseaseID
	Left Join tblClaimLawsuit CL				ON C.ClaimID=CL.ClaimID
	Left Join tblLawsuit L						ON CL.LawsuitID=L.LawsuitID
	left join tblClaimantSettlement cs			ON cs.ClaimLawsuitID = CL.ClaimLawsuitID
	left join tblClaimLawsuitStatusTracking cst ON cst.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblClaimLawsuitStatus css			ON css.ClaimLawsuitStatusID = cst.ClaimLawsuitStatusID
WHERE  COALESCE(IsPrimaryStatus, 0) = 1
	AND cst.ClaimLawsuitStatusID not in (22, 23, 24, 25, 29, 39)
	AND C.MatterID >= COALESCE(@MatterID, 0) 
	AND C.MatterID <= COALESCE(NULLIF(@MatterID,0), @maxMatterID) 
	--AND COALESCE(CST.StatusDate,GETDATE()) BETWEEN COALESCE(@StartDate, '1/1/1900') AND COALESCE(@EndDate, GETDATE())
	AND COALESCE(CST.StatusDate,GETDATE()) <= COALESCE(@EndDate, GETDATE())
	AND COALESCE(CST.StatusDate,'1/1/1900') >= COALESCE(@StartDate, '1/1/1900')
	--and ProcessedDate BETWEEN COALESCE(@StartDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) AND COALESCE(@EndDate, GETDATE())
ORDER BY  Year(cst.StatusDate)

--select * from @DiseaseCount

Select Matter, 
	MatterID,  
	Sum(MesoCount)					as MesoCount,
	Sum(LungCancerCount)			as LungCancerCount, 
	Sum(OtherCancerCount)			as OtherCancerCount,
	Sum(NonMaligCount)				as NonMaligCount, 
	Sum(UnknownCount+unknowncount1) as UnknownCount, 
	COALESCE(@StartDate, '1/1/1900')						as StartDate, 
	COALESCE(@EndDate, GETDATE())						as EndDate,
	SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + unknowncount1) AS MonthTotal-- , 
FROM @DiseaseCount
GROUP BY Matter, MatterID
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryDismissedClaims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryDismissedClaims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryDismissedClaims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryDismissedClaims] TO [power_user]
GO
