SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryDocumentSearch] (@ClaimantName			nvarchar(max), 
													  @DocumentTitle		nvarchar(max), 
													  @DocumentTypeID		int,
													  @FirmID				int,
													  @CreatedBy			nvarchar(250), 
													  @DateCreatedStart		datetime, 
													  @DateCreatedEnd		datetime, 
													  @IncludeHistorical	bit,
													  @ClaimantIDs			integer_list_tbltype READONLY,
													  @emailaddress			nvarchar(250))
AS
SET NOCOUNT ON
 
----The following set of variables can be used for testing.
--DECLARE @ClaimantName      nvarchar(max)
--DECLARE @DocumentTitle     nvarchar(max)
--DECLARE @DocumentTypeID    int
--DECLARE @FirmID            int
--DECLARE @CreatedBy         nvarchar(250)
--DECLARE @DateCreatedStart  datetime
--DECLARE @DateCreatedEnd    datetime
--DECLARE @IncludeHistorical bit
--DECLARE @ClaimantIDs       TABLE (n int) 
--DECLARE @emailaddress      nvarchar(250)
--SET @ClaimantName      = 'lower'
--SET @DocumentTitle     = NULL
--SET @DocumentTypeID    = 8
--SET @FirmID            = NULL
--SET @CreatedBy         = NULL
--SET @DateCreatedStart  = NULL
--SET @DateCreatedEnd    = NULL
--SET @IncludeHistorical = 0
----SET @ClaimantIDs     = NULL
--SET @emailaddress      = 'snyderd@kcicllc.com'
 
DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)
 
IF object_id('tempdb..#AccessibleClaimants') IS NOT NULL DROP TABLE #AccessibleClaimants
CREATE TABLE #AccessibleClaimants (DocumentID int, ClaimantName nvarchar(max))
 
IF object_id('tempdb..#DocsToConsider') IS NOT NULL DROP TABLE #DocsToConsider
CREATE TABLE #DocsToConsider (DocumentID int, DocumentTitle nvarchar(4000), DocumentURL nvarchar(4000), ClaimantDocumentType nvarchar(100), FirmID int, 
							  Firm nvarchar(250), CreatedBy nvarchar(250), DateCreated datetime, AcceptedBy nvarchar(250), DateAccepted datetime, AmazonKey nvarchar(4000), isHistorical bit)
 
--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
IF object_id('tempdb..#ClaimantIDs') IS NOT NULL DROP TABLE #ClaimantIDs 
CREATE TABLE #ClaimantIDs (n int)
INSERT INTO #ClaimantIDs
	SELECT n FROM @ClaimantIDs
	
--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @SelectStringAccessibleClaimants nvarchar(max)
DECLARE @GroupByAccessibleClaimants nvarchar(max)
DECLARE @SelectStringDocsToConsider nvarchar(max)
DECLARE @JoinString nvarchar(max)
DECLARE @WhereStringClaimants nvarchar(max)
DECLARE @WhereStringClaimantName nvarchar(max)
DECLARE @WhereStringDocuments nvarchar(max)
DECLARE @QueryString nvarchar(max)
 
SET @SelectStringAccessibleClaimants = 'INSERT INTO #AccessibleClaimants
											SELECT mcd.DocumentID, 
												''<a target="_blank" href="/clients/wm/cip/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + ''">'' + (COALESCE(LastName, '''') + '', '' + COALESCE(FirstName, '''') + '' ('' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + '')'') + ''</a>''
											FROM tblMultiClaimantDocument mcd
												INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID '
 
SET @GroupByAccessibleClaimants =	   'GROUP BY mcd.DocumentID, 
											''<a target="_blank" href="/clients/wm/cip/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID='' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + ''">'' + (COALESCE(LastName, '''') + '', '' + COALESCE(FirstName, '''') + '' ('' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '''') + '')'') + ''</a>'',
											LastName, FirstName	
										ORDER BY LastName, FirstName '
 
SET @SelectStringDocsToConsider =	   'INSERT INTO #DocsToConsider
											SELECT DISTINCT DocumentID,
												DocumentTitle,
												DocumentURL,
												ClaimantDocumentType,
												d.FirmID,
												DefenseCounsel,
												CreatedBy,
												DateCreated,
												AcceptedBy,
												DateAccepted,
												REPLACE(AmazonKey, '''''''', ''\'''''') AS AmazonKey,
												CASE WHEN d.FirmID IS NULL THEN 1 ELSE 0 END AS IsHistorical	
											FROM tblDocuments d 
												INNER JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
												LEFT JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselAddressID = d.FirmID'
 
SET @JoinString = ' '
SET @WhereStringClaimantName = ' '
SET @WhereStringClaimants = ' WHERE 1=1 '
SET @WhereStringDocuments = ' WHERE 1=1 '
 
IF @ClaimantName	  IS NOT NULL           BEGIN SET @WhereStringClaimantName = @WhereStringClaimantName + ' AND ClaimantNames like ''%'' + @ClaimantNameParam + ''%'' ' END
IF (SELECT COUNT(n) FROM #ClaimantIDs) <> 0 BEGIN SET @WhereStringClaimants = @WhereStringClaimants + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)' END
 
IF @DocumentTitle	  IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND DocumentTitle LIKE ''%'' + @DocumentTitleParam + ''%''' END
IF @DocumentTypeID	  IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND d.ClaimantDocumentTypeID = ' + CONVERT(nvarchar(20), @DocumentTypeID) END
IF @FirmID            IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND d.FirmID = ' + CONVERT(nvarchar(20),@FirmID) END
IF @CreatedBy		  IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND d.CreatedBy LIKE ''%'' + @CreatedByParam + ''%''' END
IF @DateCreatedStart  IS NOT NULL 
   OR @DateCreatedEnd IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND COALESCE(d.DateCreated, ''1/1/1900'') >= ''' + CONVERT(nvarchar,COALESCE(@DateCreatedStart, '1/1/1900'))+''' AND COALESCE(d.DateCreated, CONVERT(date, GETDATE())) <= ''' + CONVERT(nvarchar,COALESCE(@DateCreatedEnd,CONVERT(date, GETDATE())))+'''' END
--IF @IncludeHistorical IS NOT NULL BEGIN SET @WhereStringDocuments = @WhereStringDocuments + ' AND CASE WHEN HistoricalDocumentID IS NULL THEN 0 ELSE 1 END <= ' + CONVERT(nvarchar(20),@IncludeHistorical) END
 
IF((RIGHT(@emailaddress, 11) <> 'kcicllc.com') AND (RIGHT(@emailaddress, 8) <> 'kcic.com') AND (RIGHT(@emailaddress, 6) <> 'wm.com'))
BEGIN
	SET @WhereStringClaimants = @WhereStringClaimants + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
	SET @JoinString = ' INNER JOIN tblClaim c                  ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
						INNER JOIN tblClaimLawsuit cl          ON c.ClaimID = cl.ClaimID
						INNER JOIN tblLawsuit l                ON cl.LawsuitID = l.LawsuitID
						LEFT JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID and dj.matterid = c.matterid '
	SET @QueryString = @SelectStringAccessibleClaimants + 
					   @JoinString + 
					   @WhereStringClaimants + 
					   @GroupByAccessibleClaimants +
					   @SelectStringDocsToConsider +
					   @WhereStringDocuments +
					   ' AND DocumentID IN(SELECT DocumentID FROM #AccessibleClaimants) '			   
END
ELSE
BEGIN
	SET @QueryString = @SelectStringDocsToConsider +
					   @WhereStringDocuments +
					   @SelectStringAccessibleClaimants +  
					   @WhereStringClaimants + 
					   ' AND DocumentID IN(SELECT DocumentID FROM #DocsToConsider) ' +
					   @GroupByAccessibleClaimants			   	
END
IF @DefenseCounselAddressID in (39,72)
BEGIN
	SET @WhereStringDocuments = @WhereStringDocuments + ' AND d.firmid in (39,72) '
END

 
--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
SET @QueryString = @QueryString + ' 
	SELECT DISTINCT DocumentID, 
		REPLACE(AmazonKey, '''''''', ''\'''''') AS AmazonKey,
		''/clients/wm/cip/Pages/DocumentProfile.aspx?ItemId='' + CONVERT(nvarchar(100), d.DocumentID) + ''&source='' + ''/clients/wm/cip/Pages/Search.aspx'' AS DocumentURL, 	
		DocumentTitle,
		ClaimantDocumentType AS DocumentType,
		Firm,
		COALESCE(ClaimantNames, '''') AS Claimants,
		DateCreated,
		CreatedBy  + '' /</br>'' + CONVERT(nvarchar,CONVERT(DATE, DateCreated)) AS Created,
		AcceptedBy + '' /</br>'' + CONVERT(nvarchar,CONVERT(DATE, DateAccepted)) AS Accepted,
		isHistorical
	FROM #DocsToConsider d
		CROSS APPLY (SELECT ClaimantName + ''</br> ''
					 FROM #AccessibleClaimants ac
					 WHERE ac.DocumentID = d.DocumentID
					 FOR XML PATH('''') ) Q ( ClaimantNames )
	WHERE DocumentID IN(SELECT DocumentID FROM #AccessibleClaimants) ' +
		@WhereStringClaimantName + '
	ORDER BY DateCreated DESC
	OPTION (RECOMPILE)'
	
print @Querystring	
 
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@ClaimantNameParam nvarchar(max), @DocumentTitleParam nvarchar(max), @CreatedByParam nvarchar(250)', @ClaimantNameParam = @ClaimantName, @DocumentTitleParam = @DocumentTitle, @CreatedByParam = @CreatedBy
 

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SEARCH_qryDocumentSearch] TO [power_user]
GO
