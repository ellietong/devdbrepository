SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_DOCUMENT_qryGetAmazonKeyByDocID] 
	-- Add the parameters for the stored procedure here
@DocID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT AmazonKey
	FROM tblDocuments
	WHERE DocumentID = @DocID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryGetAmazonKeyByDocID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryGetAmazonKeyByDocID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryGetAmazonKeyByDocID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryGetAmazonKeyByDocID] TO [power_user]
GO
