SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryFirmList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     --Insert statements for procedure here
	SELECT DefenseCounselAddressID AS FirmID, DefenseCounsel AS Firm FROM tblDefenseCounselAddress
	UNION
	SELECT NULL, ''
	ORDER BY DefenseCounsel
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryFirmList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryFirmList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryFirmList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryFirmList] TO [power_user]
GO
