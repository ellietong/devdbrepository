SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryDismissalsByState] 
	(@StartDate datetime,
	 @EndDate datetime)
AS
BEGIN
	SET NOCOUNT ON;
	--Test Variables
	--DECLARE @StartDate datetime = '1/1/1900'
	--DECLARE @EndDate datetime = GETDATE()
	
	--SELECT 
	--	COALESCE(FullNameState, 'Unknown') AS FullNameState, 
	--	COUNT(DISTINCT(cl.ClaimantLawsuitID)) AS [Count]
	--FROM tblClaimantLawsuit cl
	--	LEFT JOIN tblLawsuit l				ON cl.LawsuitID=l.LawsuitID
	--	LEFT JOIN tblLawsuitStatus clst	ON cl.LawsuitStatusID=clst.LawsuitStatusID
	--	LEFT JOIN tblState s				ON l.StateID=s.StateID
	--WHERE	COALESCE(statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
	--		AND LawsuitStatusGroupID =4
	--GROUP BY FullNameState
	--ORDER BY COUNT(DISTINCT(cl.ClaimantLawsuitID)) DESC



END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByState] TO [power_user]
GO
