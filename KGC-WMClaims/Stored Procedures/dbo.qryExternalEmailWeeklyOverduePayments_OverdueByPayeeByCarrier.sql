SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyOverduePayments_OverdueByPayeeByCarrier]
(@BodyMessage nvarchar(MAX) output)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @OverDueByPI TABLE (ID int NOT NULL IDENTITY(1,1) PRIMARY KEY, Firm nvarchar(250), CNA money, INA money, London money, Resco money, Travelers money, Total money, Remainder int)
	DECLARE @AggregatedPayments TABLE (TravelersBillID bigint, CarrierGroupID int, TotalPayments money, PayeeTypeID int)

	INSERT INTO @AggregatedPayments
	SELECT TravelersBillID, COALESCE(CarrierGroupID, 0), SUM(COALESCE(arp.Amount, 0)), PayeeTypeID
	FROM tblAccountsReceivable ar
	LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	LEFT JOIN tblPolicy p ON ar.PolicyID = p.PolicyID
	LEFT JOIN tblCarrier c ON p.CarrierID = c.CarrierID
	--WHERE PayeeTypeID =1
	GROUP BY TravelersBillID, CarrierGroupID, PayeeTypeID

	DECLARE @AggregatedReceivables TABLE (TravelersBillID bigint, CarrierGroupID int, TotalReceivable money, PayeeTypeID int, RESCODUEDATE DATE)

	INSERT INTO @AggregatedReceivables
	SELECT TravelersBillID, COALESCE(CarrierGroupID, 0), SUM(COALESCE(ar.Amount, 0)), PayeeTypeID, RescoDueDate
	FROM tblAccountsReceivable ar
	LEFT JOIN tblPolicy p ON ar.PolicyID = p.PolicyID
	LEFT JOIN tblCarrier c ON p.CarrierID = c.CarrierID
	--WHERE PayeeTypeID =1
	GROUP BY TravelersBillID, CarrierGroupID, PayeeTypeID, RescoDueDate

	INSERT INTO @OverDueByPI
	SELECT COALESCE(Firm, 'Unlinked Settlements'), 
	COALESCE([CNA Insurance Companies], 0) AS [CNA Insurance Companies], 
	COALESCE([ACE INA Group], 0) AS [ACE INA Group],
	COALESCE([London Market], 0) AS [London Market], 
	COALESCE([Resco], 0) AS [Resco], 
	COALESCE([The Travelers Companies, Inc.], 0) AS [The Travelers Companies],
	COALESCE([CNA Insurance Companies], 0)+ COALESCE([ACE INA Group], 0)+COALESCE([London Market], 0)+COALESCE([Resco], 0)+COALESCE([The Travelers Companies, Inc.], 0) AS Total,
	0
	FROM
	(SELECT CASE WHEN ap.PayeeTypeID = 2 THEN 'Resco (Non-Interim Costs)' WHEN ap.PayeeTypeID = 3 THEN 'Resco (Interim Costs)' ELSE f.DefenseCounsel END AS Firm, 
							COALESCE(SUM(COALESCE(ar.TotalReceivable,0)-COALESCE(ap.TotalPayments, 0)), 0) as Amount, coalesce(CarrierGroupName, 'Resco') as CarrierGroupName
	FROM @AggregatedPayments ap
	LEFT JOIN @AggregatedReceivables ar ON ap.CarrierGroupID = ar.CarrierGroupID AND ap.TravelersBillID = ar.TravelersBillID AND ap.PayeeTypeID = ar.PayeeTypeID
	LEFT JOIN tblTravelersBill tb ON ar.TravelersBillID = tb.TravelersBillID
	LEFT JOIN tblDefenseCounselAddress f ON tb.DefenseCounselAddressID = f.DefenseCounselAddressID
	LEFT JOIN tblCarrierGroup cg ON ar.CarrierGroupID = cg.CarrierGroupID
	        
	WHERE CASE WHEN AP.PayeeTypeID = 2 THEN RESCODUEDATE ELSE PaymentDueDate END < GETDATE() AND AllocationQCConfirmation = 1 AND BillStatusID <> 2 --AND PayeeTypeID <> '2'
	GROUP BY CarrierGroupName, CASE WHEN ap.PayeeTypeID = 2 THEN 'Resco (Non-Interim Costs)' WHEN ap.PayeeTypeID = 3 THEN 'Resco (Interim Costs)' ELSE f.DefenseCounsel END)

		AS carriers

	PIVOT

	(
			SUM(Amount)

	FOR
	CarrierGroupName

		IN ([CNA Insurance Companies], [ACE INA Group], [London Market], [Resco], [The Travelers Companies, Inc.])

	) AS Pivoted
	ORDER BY Firm

	--Update the remainder based on the PK
	UPDATE @OverDueByPI
	SET Remainder = remainder.Remainder 
	FROM (SELECT ID, (ID % 2) AS Remainder FROM @OverDueByPI) remainder
	INNER JOIN @OverDueByPI ODBPI ON remainder.ID = ODBPI.ID
	WHERE ODBPI.ID = remainder.ID

						
	
	DECLARE @CNAT nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(CNA)  as MONEY),1) FROM @OverDueByPI)
	DECLARE @ACET nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(INA)  as MONEY),1) FROM @OverDueByPI)
	DECLARE @LondonT nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(London)  as MONEY),1) FROM @OverDueByPI)
	DECLARE @RescoT nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Resco)  as MONEY),1) FROM @OverDueByPI)
	DECLARE @TravelersT nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Travelers)  as MONEY),1) FROM @OverDueByPI)
	DECLARE @GrandTotal nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Total)  as MONEY),1) FROM @OverDueByPI)
	--DECLARE @AllTotal nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(AmountTotal)  as MONEY),1) FROM @AgingAccountsReceivable2)
	IF (Select COUNT(Firm) from @OverDueByPI) > 0 
	BEGIN
	SET @BodyMessage = @BodyMessage + '<P style="font-weight: bold; font-size: large; padding-bottom: .25em; margin-bottom: .25em;">Overdue Amounts by Payee and Carrier</P>'
	SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C;"><tbody>' + 
			--N'<tr border-style:none><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); "></th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); "></th><th colspan=5> Days Overdue</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); "></th></tr>' +
			N'<tr><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); text-align: left">Firm</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); text-align: right">INA</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); text-align: right">CNA</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); text-align: right">London</th>'+
				'<th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); text-align: right">Resco</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); text-align: right">Travelers</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); text-align: right">Total</th></tr>'  
		
				DECLARE @Firm nvarchar(250), @CNA money, @ACE money, @London money, @Resco money, 
						@Travelers money, @PayeeTotal money, @RemainderValPC int

							DECLARE OPC_curs CURSOR FOR
							SELECT 	Firm, CNA, INA, London, Resco, Travelers, Total, Remainder				            
							FROM  @OverDueByPI
							
							OPEN OPC_curs
							FETCH NEXT FROM OPC_curs INTO @Firm, @CNA, @ACE, @London, @Resco, @Travelers, @PayeeTotal, @RemainderValPC

							
							--IF @LastCarrierName <> @CarrierName OR @@FETCH_STATUS = 0
							While @@FETCH_STATUS = 0
							BEGIN
									IF @RemainderValPC = 0 BEGIN
									SET @BodyMessage = @BodyMessage + '<tr style="background-color: #D6E8FF; ">'
									END
									ELSE BEGIN
									SET @BodyMessage = @BodyMessage + '<tr>'
									END						
						    		--SET @BodyMessage = @BodyMessage + '<tr>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: left; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + @Firm + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @ACE,1) + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @CNA,1)	 + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @London,1) + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @Resco,1) + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @Travelers,1) + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; font-weight: bold; border: 1px solid transparent">' + '$' + CONVERT(varchar(12), @PayeeTotal,1) + '</td></tr></tbody>'
																
									
							FETCH NEXT FROM OPC_curs INTO @Firm, @CNA, @ACE, @London, @Resco, @Travelers, @PayeeTotal, @RemainderValPC

							END
							CLOSE OPC_curs
							DEALLOCATE OPC_curs	
									
									--SET @BodyMessage = @BodyMessage + '<tfoot class="Foot">'
									SET @BodyMessage = @BodyMessage + '<tr style="padding: .3em; color: #F6ECF0; border: none;">'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: center; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + 'Totals' + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @ACET + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @CNAT + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @LondonT + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @RescoT + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @TravelersT + '</td>'
									SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @GrandTotal + '</td>'
									SET @BodyMessage = @BodyMessage + '</tr>'							
			   
	            			
	SET @BodyMessage = @BodyMessage + N'</table>' +  N'<br>'    								
									--SET @BodyMessage = @BodyMessage + N'<table border=0 cellspacing=0 cellpadding=0>' + 
	-- N''--<tr><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Carrier</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Payee</th>'
									--SET @BodyMessage = @BodyMessage + '<tr>'
									--SET @BodyMessage = @BodyMessage + N'<table border=0 cellspacing=0 cellpadding=0>' + 
									--	N'<tr><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Claim Type</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Claimant Name</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Invoice Number</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Amount Owed</th></th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Due Date</th>'

	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyOverduePayments_OverdueByPayeeByCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyOverduePayments_OverdueByPayeeByCarrier] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyOverduePayments_OverdueByPayeeByCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyOverduePayments_OverdueByPayeeByCarrier] TO [power_user]
GO
