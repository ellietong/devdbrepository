SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryOccupationList]
AS
BEGIN
	SELECT OccupationID, Occupation 
	FROM [KGA-ClaimsAdmin].[dbo].tblOccupation
	UNION
	SELECT NULL, ''
	ORDER BY Occupation
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOccupationList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOccupationList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOccupationList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOccupationList] TO [power_user]
GO
