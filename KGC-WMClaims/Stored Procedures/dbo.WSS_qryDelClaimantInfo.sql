SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDelClaimantInfo]
	-- Add the parameters for the stored procedure here
	(@ClaimantPersonalInfoID int, @Emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
----Delete Claimant Related Party Information
--    UPDATE tblClaimantRelatedParty
--    SET SPUsername = @Emailaddress
--    WHERE ClaimantRelatedPartyID IN (SELECT ClaimantRelatedPartyID from tblClaimantRelatedParty where ClaimantPersonalInfoID = @ClaimantPersonalInfoID)
    
--	DELETE FROM tblClaimantRelatedParty WHERE ClaimantRelatedPartyID IN (SELECT ClaimantRelatedPartyID from tblClaimantRelatedParty where ClaimantPersonalInfoID = @ClaimantPersonalInfoID)

----Delete Claimant Disease Information
--	UPDATE tblClaimLawsuitDisease
--	SET SPUsername = @Emailaddress
--	WHERE ClaimantLawsuitID IN (SELECT ClaimantLawsuitID FROM tblClaimantLawsuit WHERE ClaimantPersonalInfoID  = @ClaimantPersonalInfoID)

--	DELETE FROM tblClaimLawsuitDisease WHERE ClaimantLawsuitID IN (SELECT ClaimantLawsuitID FROM tblClaimantLawsuit WHERE ClaimantPersonalInfoID  = @ClaimantPersonalInfoID)
	
----Delete Claimant Employment Information
--	UPDATE tblClaimantEmployment
--	SET SPUsername = @Emailaddress
--	WHERE ClaimantEmploymentID IN (SELECT ClaimantEmploymentID from tblClaimantEmployment where ClaimantPersonalInfoID = @ClaimantPersonalInfoID)

--	DELETE FROM tblClaimantEmployment WHERE ClaimantEmploymentID IN (SELECT ClaimantEmploymentID from tblClaimantEmployment where ClaimantPersonalInfoID = @ClaimantPersonalInfoID)
	
		
----Delete Claimant Personal Information		
--	UPDATE tblClaimantPersonalInfo
--	SET SPUsername = @Emailaddress
--	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
--	DELETE FROM tblClaimantPersonalInfo WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelClaimantInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelClaimantInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelClaimantInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelClaimantInfo] TO [power_user]
GO
