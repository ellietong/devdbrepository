SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByClaimType] 
	(@StartDate datetime,
	 @EndDate datetime)  
AS
BEGIN
	SET NOCOUNT ON;
	----Test Variables
	----DECLARE @StartDate datetime = '1/1/1900'
	----DECLARE @EndDate datetime = GETDATE()

	--SELECT ClaimType, COUNT(distinct(cpi.ClaimantPersonalInfoID)) AS Count
	--	FROM tblClaimantPersonalInfo cpi
	--	LEFT JOIN tblClaimantLawsuit cl			ON cpi.ClaimantPersonalInfoID=cl.ClaimantPersonalInfoID
	--	LEFT JOIN tblLawsuit l						ON cl.LawsuitID=l.LawsuitID
	--	LEFT JOIN (SELECT * 
	--				FROM tblLawsuitStatus 
	--				WHERE LawsuitStatusID = 6) clst ON cl.LawsuitStatusID=clst.LawsuitStatusID
	--	LEFT JOIN tblState s						ON l.StateID=s.StateID
	--	LEFT JOIN tblClaimType ct					ON ct.ClaimTypeID = cpi.ClaimTypeID
	--WHERE COALESCE(ServiceDate,FileDate,Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
	--GROUP BY ClaimType
	--ORDER BY ClaimType

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByClaimType] TO [power_user]
GO
