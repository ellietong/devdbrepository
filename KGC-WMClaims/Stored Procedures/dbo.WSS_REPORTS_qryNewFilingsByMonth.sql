SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryNewFilingsByMonth] 
	(@StartDate datetime,
	 @EndDate datetime)  
AS
BEGIN
	SET NOCOUNT ON;
	--Test Variables
	--DECLARE @StartDate datetime = '1/1/1900'
	--DECLARE @EndDate datetime = GETDATE()

	--SELECT 
	--	DATENAME("mm", coalesce(ServiceDate,FileDate,Statusdate)) + ' ' + CONVERT(nvarchar, YEAR(COALESCE(ServiceDate,FileDate,Statusdate))) AS [MonthName], 
	--	COUNT(DISTINCT(cpi.ClaimantPersonalInfoID)) AS MonthCount, 
	--	MONTH(COALESCE(ServiceDate,FileDate,Statusdate)) As MonthID, 
	--	YEAR(COALESCE(ServiceDate,FileDate,Statusdate)) AS [Year]
	--FROM tblClaimantPersonalInfo cpi 
	--	LEFT JOIN tblClaimantLawsuit cl on cl.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	--	LEFT JOIN tblLawsuit l						ON cl.LawsuitID=l.LawsuitID
	--	LEFT JOIN (SELECT * 
	--				FROM tblLawsuitStatus 
	--				WHERE LawsuitStatusID=6) clst	ON cl.LawsuitStatusID=clst.LawsuitStatusID
	--	LEFT JOIN tblState s						ON l.StateID=s.StateID
	--WHERE	COALESCE(ServiceDate,FileDate,Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
	--GROUP BY DATENAME("mm", COALESCE(ServiceDate,FileDate,Statusdate)) + ' ' +  CONVERT(nvarchar, YEAR(COALESCE(ServiceDate,FileDate,Statusdate))) , MONTH(coalesce(ServiceDate,FileDate,Statusdate)),YEAR(coalesce(ServiceDate,FileDate,Statusdate))
	--ORDER BY YEAR(COALESCE(ServiceDate,FileDate,Statusdate)), MONTH(COALESCE(ServiceDate,FileDate,Statusdate))

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryNewFilingsByMonth] TO [power_user]
GO
