SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryShadowAllocation]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PreviousExhaustionShadow TABLE (PolicyDateID int, Exhaustion money)
	INSERT INTO @PreviousExhaustionShadow
	SELECT	PolicyDateID, 
			SUM(IndemnityAllocation+DefenseAllocation) as Exhaustion
	FROM tblExhaustion e
		INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID
	WHERE ExhaustionTypeID = 2
	GROUP BY	PolicyDateID, 
				ExhaustionTypeID

	DECLARE @PreviousExhaustionTravelers TABLE (PolicyID int, Exhaustion money)
	INSERT INTO @PreviousExhaustionTravelers
	SELECT	ar.PolicyID, 
			Sum(Case	When PaymentType like 'Defense' and ProvisionValueID in (1,2) Then Amount
						When PaymentType like 'Indemnity' Then Amount End) As Amount
	FROM tblTravelersBill tb
		INNER JOIN tblAccountsReceivable ar on tb.TravelersBillID=ar.TravelersBillID
		INNER JOIN tblPolicy p on ar.PolicyID = p.PolicyID
		INNER JOIN (Select ProvisionLabelID, ProvisionValueID, PolicyID From tblprovision where provisionlabelID = 1) pro on p.PolicyID =pro.policyID
	GROUP BY ar.PolicyID


	SELECT CASE WHEN InsuranceProgramID =1 THEN 'Schedule A1'
				WHEN InsuranceProgramID in (2,5,6,7) THEN 'Schedule A2'
				WHEN InsuranceProgramID in (3) THEN 'Schedule A3'
				WHEN InsuranceProgramID in (4) THEN 'Schedule A4' END AS Schedule,
		CarrierName, 
		PolicyNum, 
		PStartDate, 
		PEndDate, 
		PerOccLimit as AvailableLimit, 
		Coalesce(pet.Exhaustion,0) as ExhaustionTravelers, 
		PerOccLimit-Coalesce(pet.Exhaustion,0) as RemainingLimitTravelers, 
		Coalesce(pes.Exhaustion,0) as ExhaustionShadow,
		PerOccLimit-Coalesce(pes.Exhaustion,0) as RemainingLimitShadow
	FROM tblPolicy p
	INNER JOIN tblPolicyDates pd				ON p.PolicyID = pd.PolicyID
	INNER JOIN tblCarrier c						ON p.CarrierID = c.CarrierID
	INNER JOIN tblPolicyLimits pl				ON pl.PolicyID = p.PolicyID
	LEFT JOIN @PreviousExhaustionTravelers pet	ON pet.PolicyID = p.PolicyID
	LEFT JOIN @PreviousExhaustionShadow pes		ON pes.PolicyDateID = pd.PolicyDateID
	WHERE p.CarrierID = 1002
	Order By	Schedule, 
				PStartDate, 
				AttachmentPoint

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryShadowAllocation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryShadowAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryShadowAllocation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryShadowAllocation] TO [power_user]
GO
