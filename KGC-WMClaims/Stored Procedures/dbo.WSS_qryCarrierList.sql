SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryCarrierList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT p.CarrierID, CarrierName FROM tblPolicy p INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
	UNION
	SELECT NULL, ''
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryCarrierList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryCarrierList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryCarrierList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryCarrierList] TO [power_user]
GO
