SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryUpdClaim] (@ClaimDiseaseID			bigint, 
											   @DiseaseID				int, 
											   @ActiveTrialDate			datetime, 
											   @LawsuitID				bigint,
											   @Username				nvarchar(250),
											   @ClaimID					bigint,
											   @DiseaseDiagnosisDate	datetime,
											   @DocketNumber			nvarchar(250),
											   @IndividualDocketNumber	nvarchar(250) = NULL,
											   @CaseCaption				nvarchar(max),
											   @LeadPlaintiffFirstName nvarchar(250),
											   @LeadPlaintiffLastName	nvarchar(250),
											   @JurisdictionID			int,
											   @StateID					int,
											   @AllegationTypeID		int = NULL,
											   @PlaintiffCounselAddressID int,											   
											   @FileDate				datetime,
											   @ServiceDate				datetime,
											   @ReceivedDate				datetime,
											   @DefenseCounselAddressID int,
											   @chkPrimary				bit = 0,
											   @chkIsMDL				int = NULL, 
											   @chkIs3rdPartyComplaint	bit = 0, 
											   @TravelersResponse		datetime = NULL,
											   @InsurerNotification		datetime = NULL,
											   @DOFE					datetime = NULL,
											   @DOFESource				int = NULL,
											   @DOFESourcePageNum 		int = NULL,
											   @ClaimLawsuitID			int,
											   @DefendantCompanyID		int = NULL)


AS

UPDATE tblLawsuit
	SET SPUsername = COALESCE(@Username, SPUsername),
		DocketNumber = COALESCE(@DocketNumber, DocketNumber),
		CaseCaption = COALESCE(@CaseCaption, CaseCaption),
		LeadPlaintiffFirstName = COALESCE(@LeadPlaintiffFirstName, LeadPlaintiffFirstName),
		LeadPlaintiffLastName = COALESCE(@LeadPlaintiffLastName, LeadPlaintiffLastName),
		JurisdictionID = COALESCE(@JurisdictionID, JurisdictionID),
		StateID = COALESCE(@StateID, StateID),
		PlaintiffCounselAddressID = COALESCE(@PlaintiffCounselAddressID, PlaintiffCounselAddressID),
		FileDate = COALESCE(@FileDate, FileDate),
		IsThirdPartyComplaint = @chkIs3rdPartyComplaint,
		DefenseCounselAddressID = COALESCE(@DefenseCounselAddressID, DefenseCounselAddressID)	
	WHERE LawsuitID = @LawsuitID

IF @chkPrimary <> 0
BEGIN
	UPDATE tblClaimLawsuit
	SET IsPrimaryForClaim = 0
	WHERE ClaimLawsuitID = @ClaimLawsuitID
END
	
	UPDATE tblClaimLawsuit
	SET ActiveTrialDate = COALESCE(@ActiveTrialDate, NULL),
		ServiceDate = @ServiceDate,
		ReceivedDate = @ReceivedDate,
		SPUsername = COALESCE(@Username, SPUsername),
		IsPrimaryForClaim = COALESCE(@chkPrimary, IsPrimaryForClaim),
		TravelersResponse = @TravelersResponse,
		InsurerNotification = @InsurerNotification, 
		IsMDL = @chkIsMDL,
		DOFE = @DOFE, 
		DOFESource = COALESCE(@DOFESource, DOFESource),
		DOFESourcePageNum = COALESCE(@DOFESourcePageNum, ''),
		IndividualDocketNumber = @IndividualDocketNumber
	WHERE ClaimLawsuitID =  @ClaimLawsuitID


--UPDATE DEFENDANT COMPANY
IF(SELECT AllegationID FROM tblAllegation WHERE ClaimLawsuitID = @ClaimLawsuitID) IS NOT NULL
BEGIN
	UPDATE tblAllegation
	SET DefendantCompanyID = @DefendantCompanyID	
	where ClaimLawsuitID = @ClaimLawsuitID
END
--ONLY INSERT IF DefendantCompanyID IS NOT NULL
ELSE IF((SELECT AllegationID FROM tblAllegation WHERE ClaimLawsuitID = @ClaimLawsuitID) IS NULL) AND @DefendantCompanyID IS NOT NULL
BEGIN
	INSERT INTO tblAllegation (ClaimLawsuitID, DefendantCompanyID)
	SELECT @ClaimLawsuitID, @DefendantCompanyID
END


SELECT * FROM tblClaimLawsuit
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaim] TO [power_user]
GO
