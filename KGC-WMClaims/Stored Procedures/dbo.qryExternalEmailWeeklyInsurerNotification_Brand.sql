SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyInsurerNotification_Brand]
(@BodyMessage nvarchar(MAX) output)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT
--BEGIN	


set @BodyMessage = @BodyMessage + '
		<p style="font-family: Calibri; font-size: 10pt; text-align: left; ">
			BRAND INSURERS<br/>
			
		</p>
		<p style="font-family: Calibri; font-size: 10pt; font-weight: bold; text-align: left; border: 1px solid transparent; " colspan="9">	
			<u>NOTIFICATION OF CLAIM</u><br/>
			
		<p/>
		<p style="font-family: Calibri; font-size: 10pt; font-weight: bold; text-align: left; border: 1px solid transparent; " colspan="9">
			Re:	“See Table Below”
		<p/>	
<p style="font-family: Calibri; font-size: 10pt; text-align: left; border: 1px solid transparent; " colspan="9">			
	Dear Insurers:<br/>
	<br/>
	
		We have received complaints in the states of Illinois, West Virginia, Pennsylvania, <br/>
		New Jersey, Missouri and Indiana in the above-styled cases.<br/>
		<br/>

		We give you formal notice of these claims against us and require:<br/>
		<br/>

		1.	Acknowledgment of your receipt of this letter;<br/>
		<br/>
		2.	Identification of your file number and designation of the individual at your<br/>
			company who will be handling these matters;<br/>
		<br/>	

		3.	Your immediate and unequivocal participation in defense of these matters;<br/>
		<br/>

		4.	Indication that you will indemnify us, the insured, if obligated to do so under<br/>
			the insurance policies.<br/>
		<br/>	

		The liabilities attributed to Brand Insulations, Inc. and its successors, are held by<br/>
	Waste Management, Inc., thus requiring Waste Management, Inc.’s tender of these matters<br/>
	to you for defense and indemnity.  The corporate history on these claims has previously been<br/>
	provided to you and will not be reiterated here, unless specifically requested.<br/>



		We, the insured, believe that these matters can, and should be resolved with full<br/>
	participation of its insurers, and look to you for prompt and unequivocal assumption of<br/>
	defense and indemnity.<br/>  
	<br/>
											Sincerely,<br/>
											<br/>

											BRAND INSULATIONS, INC. <br/>
											<br/>
											<br/>
											_____________________________
											<br/>
											Ashley Harper<br/>
											Waste Management, Inc.<br/>
											Litigation Counsel<br/>

	
 </p>'	
--END	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Brand] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Brand] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Brand] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Brand] TO [power_user]
GO
