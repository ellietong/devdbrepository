SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDelTravelersDocument] 
	-- Add the parameters for the stored procedure here
	(@DocumentID int, @InvoiceDocumentID int, @Emailaddress nvarchar(250))
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--InvoiceDocumentID parameter unused.  Invoice not deleted if bill is deleted
    
    
    UPDATE tblAccountsReceivable
    SET SPUsername = @Emailaddress
    WHERE TravelersBillID IN (SELECT TravelersBillID FROM tblDocuments where DocumentID = @DocumentID)
    
    UPDATE tblTravelersBill
    SET SPUsername = @Emailaddress
    WHERE TravelersBillID IN (SELECT TravelersBillID FROM tblDocuments where DocumentID = @DocumentID)
    
    UPDATE tblDocuments 
    SET ModifiedBy = @Emailaddress
    WHERE DocumentID = @DocumentID
    
    UPDATE tblMultiClaimantDocument 
    SET SPUsername = @Emailaddress
    WHERE DocumentID = @DocumentID
    
    DELETE FROM tblAccountsReceivable
    WHERE TravelersBillID IN (SELECT TravelersBillID FROM tblDocuments WHERE DocumentID = @DocumentID)
    
    DELETE FROM tblTravelersBill WHERE TravelersBillID IN (SELECT TravelersBillID FROM tblDocuments WHERE DocumentID = @DocumentID)

	DELETE FROM tblMultiClaimantDocument WHERE DocumentID = @DocumentID

	DELETE FROM tblDocuments WHERE DocumentID = @DocumentID
	

	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelTravelersDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelTravelersDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelTravelersDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelTravelersDocument] TO [power_user]
GO
