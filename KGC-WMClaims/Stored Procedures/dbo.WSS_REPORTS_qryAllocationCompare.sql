SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryAllocationCompare]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--SELECT tb.TravelersBillID, ar.PolicyID AS KCICPolicyID, ar.Allocation AS KCICAllocation,
	--acctrec.PolicyID AS TravelersPolicyID, acctrec.Amount AS TravelersAllocation,
	--ar.Allocation - acctrec.Amount AS [Difference]
	----SELECT * 
	--FROM tblAllocationResults ar
	--INNER JOIN tblOccurrence o ON ar.OccurrenceID = o.OccurrenceID
	--INNER JOIN tblTravelersBill tb ON o.TravelersBillID = tb.TravelersBillID
	--LEFT JOIN tblExhaustion e ON o.OccurrenceID = e.OccurrenceID
	--LEFT JOIN tblAccountsReceivable acctrec ON acctrec.TravelersBillID = tb.TravelersBillID AND acctrec.PolicyID = ar.PolicyID
	----WHERE tb.AllocationQCStatusID = 1

	--ORDER BY tb.TravelersBillID, KCICPolicyID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryAllocationCompare] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryAllocationCompare] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryAllocationCompare] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryAllocationCompare] TO [power_user]
GO
