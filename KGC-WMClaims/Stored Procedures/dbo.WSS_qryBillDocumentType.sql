SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[WSS_qryBillDocumentType]

AS
SET NOCOUNT ON
SELECT ClaimantDocumentTypeID, ClaimantDocumentType FROM tblClaimantDocumentType
WHERE ClaimantDocumentTypeID IN (48, 49)
UNION
SELECT NULL, '<< Document Type >>'
ORDER BY ClaimantDocumentType
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryBillDocumentType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryBillDocumentType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryBillDocumentType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryBillDocumentType] TO [power_user]
GO
