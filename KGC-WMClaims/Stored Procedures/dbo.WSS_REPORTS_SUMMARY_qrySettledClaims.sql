SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_REPORTS_SUMMARY_qrySettledClaims] (	@SettlementDateStart  NVARCHAR(250),
																		@SettlementDateEnd  NVARCHAR(250),
																		@MatterID INT) 

AS
SET NOCOUNT ON

------Find Max Matter ID
DECLARE @maxMatterID INT
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

 --Declare @SettlementDateStart datetime
 --Declare @SettlementDateEnd datetime
 --declare @matterid INT
 --Set  @SettlementDateStart  = '1/1/1900'
 --Set @SettlementDateEnd = '9/30/2012'
 --set @matterid = NULL
 
DECLARE @SettlementSUMmary TABLE (ClaimNumber INT, 
							
				SettlementAmount MONEY,
				SettlementAverage MONEY,
				
				Meso_Count INT,
				Meso_SettlementAmount MONEY,
				Meso_Avg MONEY,
				LungCancer_Count INT,
				LungCancer_SettlementAmount MONEY,
				LungCancer_Avg MONEY,
				OtherCancer_Count INT,
				OtherCancer_SettlementAmount MONEY,
				OtherCancer_Avg MONEY,
				NonMalig_Count INT,
				NonMalig_SettlementAmount MONEY,
				NonMalig_Avg MONEY,
				Other_Count INT,
				Other_SettlementAmount MONEY,

				NoDisease_Count INT,
				NoDisease_SettlementAmount MONEY,

				Null_Count INT,
				Null_SettlementAmount MONEY,
				Matter NVARCHAR(200))


INSERT INTO @SettlementSUMmary 
Select COUNT(DISTINCT(c.ClaimID)) AS ClaimNumber,
		COALESCE(SUM(SettlementAmount),0) AS SettlementAmount,
	COALESCE(SUM(SettlementAmount),0)/COUNT(c.ClaimID) AS SettlementAverage, 
	
	
SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN 1 ELSE 0 END) AS Meso_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN SettlementAmount ELSE 0 END) AS Meso_SettlementAmount,	
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN 1 ELSE 0 END) = 0 THEN 0 
		ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = (1) THEN SettlementAmount ELSE 0 END)/ 
			SUM(CASE WHEN HLD.HighLevelDiseaseID = 1 THEN 1 ELSE 0 END) END  AS Meso_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseaseID = 2 THEN 1 ELSE 0 END) AS LungCancer_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseaseID = 2 THEN SettlementAmount ELSE 0 END) AS LungCancer_SettlementAmount,	
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = (2) THEN 1 ELSE 0 END) = 0 THEN 0 
		ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = (2) THEN SettlementAmount ELSE 0 END)/ 
			SUM(CASE WHEN HLD.HighLevelDiseaseID = 2 THEN 1 ELSE 0 END) END  AS LungCancer_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN 1 ELSE 0 END) AS OtherCancer_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN SettlementAmount ELSE 0 END) AS OtherCancer_SettlementAmount,	
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN 1 ELSE 0 END) = 0 THEN 0 
		ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN SettlementAmount ELSE 0 END)/ 
			SUM(CASE WHEN HLD.HighLevelDiseaseID = 3 THEN 1 ELSE 0 END) END  AS OtherCancer_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseaseID in (4,7,8) THEN 1 ELSE 0 END) AS NonMalig_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseaseID in (4,7,8) THEN SettlementAmount ELSE 0 END) AS NonMalig_SettlementAmount,	
	CASE WHEN SUM(CASE WHEN HLD.HighLevelDiseaseID in (4,7,8) THEN 1 ELSE 0 END) = 0 THEN 0 
		ELSE SUM(CASE WHEN HLD.HighLevelDiseaseID in (4,7,8)THEN SettlementAmount ELSE 0 END)/ 
			SUM(CASE WHEN HLD.HighLevelDiseaseID in (4,7,8) THEN 1 ELSE 0 END) END  AS NonMalig_Avg,
			
	SUM(CASE WHEN HLD.HighLevelDiseaseID IN (5, 6) THEN 1 ELSE 0 END) AS Other_Count,	
	SUM(CASE WHEN HLD.HighLevelDiseaseID IN (5, 6) THEN SettlementAmount ELSE 0 END) AS Other_SettlementAmount,
			
	0 AS NoDisease_Count,
	0 AS NoDisease_SettlementAmount,		
	--SUM(CASE WHEN HLD.HighLevelDiseaseID = 6 THEN 1 ELSE 0 END) AS NoDisease_Count,	
	--SUM(CASE WHEN HLD.HighLevelDiseaseID = 6 THEN SettlementAmount ELSE 0 END) AS NoDisease_SettlementAmount,
			
	SUM(CASE WHEN HLD.HighLevelDiseaseID is null THEN 1 ELSE 0 END) AS Null_Count,				
	SUM(CASE WHEN HLD.HighLevelDiseaseID  is null THEN SettlementAmount ELSE 0 END) AS Null_SettlementAmount,
	UPPER(Matter) AS Matter
	
From tblClaimantPersonalInfo i 
	Left Join tblClaim c on i.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
	LEFT JOIN tblMatter M on c.MatterID = m.MatterID
	Left Join tblClaimLawsuit l on l.claimID = c.claimID
	Left Join tblLawsuit law on l.lawsuitid = law.lawsuitid
	Left Join tblCLaimantSettlement s on s.claimlawsuitID = l.claimlawsuitID
	Left Join tblDisease D on CD.DiseaseID = D.DiseaseID
	left join tblHighLevelDisease HLD on HLD.HighLevelDiseaseID=D.HighLevelDiseaseID
	Left join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = l.ClaimLawsuitID
	LEFT JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = law.PlaintiffCounselAddressID
--left join tblclaimlawsuitstatusgroup csg on csg.claimlawsuitstatusid = cst.ClaimLawsuitStatusID

Where  
	COALESCE(CST.StatusDate,GETDATE()) <= COALESCE(@SettlementDateEnd, GETDATE())
	AND COALESCE(CST.StatusDate,'1/1/1900') >= COALESCE(@SettlementDateStart, '1/1/1900')
	
	--StatusDate >= COALESCE(@SettlementDateStart, '1/1/'+CONVERT(NVARCHAR(4), YEAR(GETDATE()))) and StatusDate <= COALESCE(@SettlementDateEnd, GETDATE())
	AND C.MatterID >= COALESCE(@MatterID,0) AND C.MatterID <= COALESCE(NULLIF(@MatterID,0),@maxMatterID) 
	and ClaimLawsuitStatusID in (23,24,25)
	 --and COALESCE(cst.IsPrimaryStatus,0) = 1
Group By Matter


Select 
--SUM(COALESCE(ClaimNumber,0)) AS ClaimNumber, 
	SUM(COALESCE(Meso_Count,0)+COALESCE(LungCancer_Count,0)+COALESCE(OtherCancer_Count,0)+COALESCE(NonMalig_Count,0)+COALESCE(Other_Count,0)+ COALESCE(Null_Count,0)) AS ClaimNumber,
	--COALESCE(resolveddate, 0) AS resolvedDate,
	SUM(COALESCE(SettlementAmount,0)) AS SettlementAmount,
	SUM(COALESCE(SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Claimnumber, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Claimnumber, 0)) END AS SettlementAverage,
	--COALESCE(SettlementAverage,0) AS SettlementAverage,
	SUM(COALESCE(Meso_Count,0)) AS Meso_Count,
	SUM(COALESCE(Meso_SettlementAmount,0)) AS Meso_SettlementAmount,
	--COALESCE(Meso_Avg,0) AS Meso_Avg,
	SUM(COALESCE(Meso_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Meso_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Meso_Count, 0)) END AS Meso_Avg,
	SUM(COALESCE(LungCancer_Count,0)) AS LungCancer_Count,
	SUM(COALESCE(LungCancer_SettlementAmount,0)) AS LungCancer_SettlementAmount,
	--COALESCE(LungCancer_Avg,0) AS LungCancer_Avg,
	SUM(COALESCE(LungCancer_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(LungCancer_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(LungCancer_Count, 0)) END AS LungCancer_Avg,
	SUM(COALESCE(OtherCancer_Count,0)) AS OtherCancer_Count,
	SUM(COALESCE(OtherCancer_SettlementAmount,0)) AS OtherCancer_SettlementAmount,
	--COALESCE(OtherCancer_Avg,0) AS OtherCancer_Avg,
	SUM(COALESCE(OtherCancer_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(OtherCancer_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(OtherCancer_Count, 0)) END AS Othercancer_Avg,
	SUM(COALESCE(NonMalig_Count,0))AS NonMalig_Count,
	SUM(COALESCE(NonMalig_SettlementAmount,0))AS NonMalig_SettlementAmount,
	--COALESCE(NonMalig_Avg,0) AS NonMalig_Avg,
	SUM(COALESCE(NonMalig_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(NonMalig_Count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(NonMalig_Count, 0)) END AS NonMalig_Avg,
	SUM(COALESCE(Other_Count,0)+ COALESCE(Null_Count,0)) AS Unknown_Count,
	SUM(COALESCE(Other_SettlementAmount,0)+COALESCE(Null_SettlementAmount,0)) AS Unknown_SettlementAmount,
 	SUM(COALESCE(Other_SettlementAmount,0)+COALESCE(Null_SettlementAmount,0)) / CASE WHEN SUM(COALESCE(Other_Count, 0)+COALESCE(null_count, 0)) = 0 THEN 1 ELSE SUM(COALESCE(Other_Count, 0)+COALESCE(null_count, 0)) END AS Unknown_Avg,
 	Matter--,
 	--resolveddate AS MoYearSettled
From @SettlementSUMmary 
GROUP BY Matter

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qrySettledClaims] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qrySettledClaims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qrySettledClaims] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qrySettledClaims] TO [power_user]
GO
