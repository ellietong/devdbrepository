SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_PENDING_qryPendingClaims_Active] 
(@MatterID int = NULL,
 @SelectCriterion nvarchar(50)) 

AS
SET NOCOUNT ON

--/*---Debugging---*/
--DECLARE @MatterID INT
--DECLARE @SelectCriterion NVARCHAR(50)

--SET @MatterID = NULL
----SET @SelectCriterion = 'TopTenPlaintiffCounsel'
----SET @SelectCriterion = 'TopTenStates'
----SET @SelectCriterion = 'Diseases'
--SET @SelectCriterion = 'Total'
--/*---End Debugging---*/

DECLARE @MaxMatterID INT
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @RawComplaints TABLE (	ClaimID		INT,
								StatusDate	DATETIME,
								ClaimLawsuitStatus NVARCHAR(50),
								DiseaseID	INT,
								DiseaseID_Alt INT,
								HighLevelDisease NVARCHAR(100),
								State NVARCHAR(100),
								PlaintiffCounsel NVARCHAR(200)
							  )
INSERT	INTO @RawComplaints
	SELECT  c.ClaimID,
			MAX(StatusDate) AS MaxStatusDate,
			ClaimLawsuitStatus,
			d.DiseaseID,
			da.DiseaseID_Alt,
			COALESCE(hld.HighLevelDisease,'Unknown') AS HighLevelDisease,
			CASE	WHEN L.StateID=45 AND L.JurisdictionID IN (386,387)
						THEN 'Illinois (Madison)'
					WHEN L.StateID=45 AND L.JurisdictionID NOT IN (386,387)
						THEN 'Illinois (Other)'
				ELSE COALESCE(S.FullNameState, 'Unknown')
			END AS 'State',
			COALESCE(PlaintiffCounsel, 'Unknown') AS PlaintiffCounsel
	FROM	tblClaim c 
		INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblClaimLawsuitStatusTracking	clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls				ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN (select * From tblClaimDisease where primarydisease = 1) cd					ON c.ClaimID = cd.ClaimID
		LEFT JOIN tblDisease d							ON cd.DiseaseID = d.DiseaseID
		LEFT JOIN tblDisease_ALT da						ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT JOIN tblHighLevelDisease hld				ON d.HighLevelDiseaseID = hld.HighLevelDiseaseID OR da.HighLevelDiseaseID = hld.HighLevelDiseaseID
		LEFT JOIN tblLawsuit l							ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN tblState s							ON l.StateID = s.StateID
		LEFT JOIN tblPlaintiffCounselAddress pca		ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE IsPrimaryStatus = 1
		AND clst.ClaimLawsuitStatusID = 22
		AND COALESCE(cd.PrimaryDisease, 1) = 1
		AND cl.IsPrimaryForClaim = 1
		AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(@MatterID, @MaxMatterID)
	
	GROUP BY c.ClaimID,
			 ClaimLawsuitStatus,
			 d.DiseaseID,
			 da.DiseaseID_Alt,
			 hld.HighLevelDisease,
			 (CASE	WHEN L.StateID=45 AND L.JurisdictionID IN (386,387)
						THEN 'Illinois (Madison)'
					WHEN L.StateID=45 AND L.JurisdictionID NOT IN (386,387)
						THEN 'Illinois (Other)'
				ELSE COALESCE(S.FullNameState, 'Unknown')
			END),
			COALESCE(PlaintiffCounsel, 'Unknown')
	ORDER BY c.ClaimID

--SELECT * FROM @RawComplaints

IF @SelectCriterion = 'TopTenPlaintiffCounsel'
	BEGIN 
		SELECT TOP 10 PlaintiffCounsel,
			COUNT(DISTINCT(ClaimID)) AS 'Count'
		FROM @RawComplaints
		GROUP BY PlaintiffCounsel
		ORDER BY
			COUNT(DISTINCT(ClaimID)) DESC,
			PlaintiffCounsel
	END
ELSE IF @SelectCriterion = 'TopTenStates'
	BEGIN
		SELECT TOP 10 State,
			COUNT(DISTINCT(ClaimID)) AS 'Count'
		FROM @RawComplaints
		GROUP BY State
		ORDER BY COUNT(DISTINCT(ClaimID)) DESC
	END
ELSE IF (@SelectCriterion = 'Diseases')
	BEGIN
		SELECT HighLevelDisease,
			COUNT(DISTINCT(ClaimID)) AS 'Count'
		FROM @RawComplaints
		GROUP BY HighLevelDisease
		ORDER BY COUNT(DISTINCT(ClaimID)) DESC
	END
ELSE IF (@SelectCriterion = 'Total')
	BEGIN
		SELECT COUNT(DISTINCT(ClaimID)) AS 'Total'
	FROM @RawComplaints
	END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryPendingClaims_Active] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryPendingClaims_Active] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryPendingClaims_Active] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryPendingClaims_Active] TO [power_user]
GO
