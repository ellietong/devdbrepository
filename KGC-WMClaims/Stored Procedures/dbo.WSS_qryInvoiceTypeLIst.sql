SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInvoiceTypeLIst]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimantDocumentTypeID, ClaimantDocumentType FROM tblClaimantDocumentType WHERE ClaimantDocumentTypeID IN (48,49)
	UNION
	SELECT NULL, ''
	ORDER BY ClaimantDocumentType
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInvoiceTypeLIst] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInvoiceTypeLIst] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInvoiceTypeLIst] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInvoiceTypeLIst] TO [power_user]
GO
