SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_REPORTS_qrySettlementsByClaimType] 
	(@StartDate datetime,
	 @EndDate datetime)  
AS
BEGIN
	SET NOCOUNT ON;
	--Test Variables
	--DECLARE @StartDate datetime = '1/1/1900'
	--DECLARE @EndDate datetime = GETDATE()

	--SELECT 
	--	ClaimType, 
	--	COUNT(distinct(cl.ClaimantLawsuitID)) as Count, 
	--	SUM(COALESCE(SettlementAmount, 0)) as IndemnityPaid, 
	--	SUM(COALESCE(SettlementAmount, 0))/COUNT(distinct(cl.ClaimantLawsuitID)) as AverageIndemnity
	--FROM tblClaimantPersonalInfo cpi
	--	LEFT JOIN tblClaimantLawsuit cl	ON cpi.ClaimantPersonalInfoID=cl.ClaimantPersonalInfoID
	--	LEFT JOIN tblLawsuit l				ON cl.LawsuitID=l.LawsuitID
	--	LEFT JOIN tblLawsuitStatus clst	ON cl.LawsuitStatusID=clst.LawsuitStatusID
	--	LEFT JOIN tblState s				ON l.StateID=s.StateID
	--	LEFT JOIN tblClaimType ct			ON cpi.ClaimTypeID = ct.ClaimTypeID
	--WHERE	COALESCE(Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
	--		AND LawsuitStatusGroupID =1
	--GROUP BY ClaimType
	--ORDER BY ClaimType
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementsByClaimType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementsByClaimType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementsByClaimType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementsByClaimType] TO [power_user]
GO
