SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryScheduleList]
AS
BEGIN
	SET NOCOUNT ON;
	Select DISTINCT Case When InsuranceProgramID =1 Then 'Schedule A1'
				When InsuranceProgramID in (2,5,6,7) Then 'Schedule A2'
				When InsuranceProgramID in (3) Then 'Schedule A3'
				When InsuranceProgramID in (4) Then 'Schedule A4' End As Schedule
	From tblPolicy p
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryScheduleList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryScheduleList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryScheduleList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryScheduleList] TO [power_user]
GO
