SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDelOccupationInfo](@ClaimantEmploymentID int, @Emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--UPDATE tblClaimantEmployment
	--SET SPUsername = @Emailaddress
	--WHERE ClaimantEmploymentID = @ClaimantEmploymentID
UPDATE tblClaimantEmployment
SET SPUsername = @Emailaddress
WHERE ClaimantEmploymentID = @ClaimantEmploymentID
    -- Insert statements for procedure here
	DELETE FROM tblClaimantEmployment WHERE ClaimantEmploymentID = @ClaimantEmploymentID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelOccupationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelOccupationInfo] TO [power_user]
GO
