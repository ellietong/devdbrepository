SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyOverduePayments_OverdueDetails]
(@BodyMessage nvarchar(MAX) output)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @5DayPayments TABLE (ID int NOT NULL IDENTITY(1,1) PRIMARY KEY, CarrierName nvarchar(250), DefenseCounsel nvarchar(500), ClaimantName nvarchar(300), Matter nvarchar(250), InvoiceNumber nvarchar(250), AmountBilled nvarchar(150), DueDate nvarchar(250), Owed money, Remainder int)
	DECLARE @Payments TABLE (AccountsReceivableID int, Amount money, Paid money, PayeeTypeID nvarchar(250))
	INSERT INTO @Payments
	SELECT ar.AccountsReceivableID, ar.Amount, SUM(COALESCE(arp.Amount,0)), PayeeTypeID
	FROM tblAccountsReceivable ar
	LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	GROUP BY ar.AccountsReceivableID, ar.Amount, PayeeTypeID

	INSERT INTO @5DayPayments
	SELECT COALESCE(CarrierGroupName, 'Resco') AS CarrierName, 
			CASE WHEN pay.PayeeTypeID = 2 THEN 'Resco (Non-Interim Costs)' WHEN pay.PayeeTypeID = 3 THEN 'Resco (Interim Costs)' ELSE f.DefenseCounsel END AS Payee,
			CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  + CASE WHEN FirstName IS NULL 
									THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName,
			Matter,
			CASE WHEN (tb.SettlementAmount IS NULL OR tb.SettlementAmount = 0) AND tb.InvoiceNumber IS NOT NULL THEN tb.InvoiceNumber ELSE 'Settlement' END,
			SUM(ar.Amount) AS 'Amount Billed', 
			CASE WHEN pay.PayeeTypeID = 2 THEN CONVERT(nvarchar(100),(RescoDueDate), 101) ELSE CONVERT(nvarchar(100),(PaymentDueDate), 101) END, 
			SUM(ar.Amount)  - SUM(COALESCE(pay.Paid, 0)) AS Outstanding, 0
	FROM tblAccountsReceivable ar 
		LEFT JOIN	tblTravelersBill		tb	ON tb.TravelersBillID = ar.TravelersBillID
		/*INNER*/LEFT JOIN	tblDefenseCounselAddress					F	ON f.DefenseCounselAddressID = tb.DefenseCounselAddressID
		INNER JOIN	tblDocuments			d	ON tb.TravelersBillID = d.TravelersBillID
		INNER JOIN	tblMultiClaimantDocument mcd ON mcd.DocumentID = D.DocumentID
		INNER JOIN	tblClaimantPersonalInfo	cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN	tblSuffix				s	ON cpi.SuffixID = s.SuffixID
		LEFT JOIN	tblPolicy				p	ON ar.PolicyID = P.PolicyID
		LEFT JOIN	tblCarrier				c	ON p.CarrierID = c.CarrierID
		LEFT JOIN	tblCarrierGroup			cg	ON c.CarrierGroupID = cg.CarrierGroupID
		LEFT JOIN	tblPayeeType			pt	ON ar.PayeeTypeID = pt.PayeeTypeID
		--INNER JOIN tblClaim CL					ON cpi.ClaimantPersonalInfoID = cl.ClaimantPersonalInfoID
		INNER JOIN	tblMatter			ct	ON tb.MatterID = ct.MatterID
		LEFT JOIN	@Payments				pay ON pay.AccountsReceivableID = ar.AccountsReceivableID
	WHERE CASE WHEN pay.PayeeTypeID = 2 THEN RescoDueDate ELSE PaymentDueDate END < GETDATE()AND AllocationQCConfirmation = 1 AND BillStatusID <> 2 -- PaymentDueDate <= GETDATE()+5 AND PaymentDueDate >= GETDATE()+5
	GROUP BY	COALESCE(CarrierGroupName, 'Resco'), 
				CASE WHEN pay.PayeeTypeID = 2 THEN 'Resco (Non-Interim Costs)' WHEN pay.PayeeTypeID = 3 THEN 'Resco (Interim Costs)' ELSE f.DefenseCounsel END, 
				Matter, 
				PayeeType, 
				LastName, 
				FirstName, 
				MiddleName, 
				cpi.SuffixID, 
				Suffix, 
				tb.InvoiceNumber, 
				tb.SettlementAmount, 
				InvoicedTotal, 
				CASE WHEN pay.PayeeTypeID = 2 THEN CONVERT(nvarchar(100),(RescoDueDate), 101) ELSE CONVERT(nvarchar(100),(PaymentDueDate), 101) END
	HAVING SUM(ar.Amount)  - SUM(COALESCE(pay.Paid, 0)) > 0
	ORDER BY	COALESCE(CarrierGroupName, 'Resco'), 
				Matter, 
				CASE WHEN pay.PayeeTypeID = 2 THEN CONVERT(nvarchar(100),(RescoDueDate), 101) ELSE CONVERT(nvarchar(100),(PaymentDueDate), 101) END, 
				ClaimantName, 
				tb.InvoiceNumber

	--Calculate the remainer for every-other row highlighting
	UPDATE @5DayPayments
	SET Remainder = fdp.Remainder 
	FROM (SELECT ID, (ID % 2) AS Remainder FROM @5DayPayments) fdp
	INNER JOIN @5DayPayments ofdp ON fdp.ID = ofdp.ID
	WHERE ofdp.ID = fdp.ID

	DECLARE @CarrierList TABLE (CarrierName nvarchar(250))
	INSERT INTO @CarrierList
	SELECT DISTINCT CarrierName 
	FROM @5DayPayments

	DECLARE @DefenseCounselList TABLE (DefenseCounsel nvarchar(250))
	INSERT INTO @DefenseCounselList
	SELECT DISTINCT DefenseCounsel 
	FROM @5DayPayments

	DECLARE @OwedtoPayee TABLE (CarrierName nvarchar(250), DefenseCounsel nvarchar(500), OwedToPayee money)	
	INSERT INTO @OwedtoPayee
		SELECT CarrierName, DefenseCounsel, Sum(Owed) AS 'OwedtoPayee'
		FROM (SELECT * FROM @5DayPayments) tblp
		GROUP BY CarrierName, DefenseCounsel
		ORDER BY CarrierName, DefenseCounsel

	DECLARE @OwedByCarrier TABLE (CarrierName nvarchar(250), OwedByCarrier money)	
	INSERT INTO @OwedByCarrier
	SELECT CarrierName, SUM(OwedToPayee)
	FROM (SELECT * FROM @OwedtoPayee) tblotp
	GROUP BY CarrierName
	ORDER BY CarrierName

	DECLARE @Remainder TABLE (arID int, RowCounter int, Remainder int)
	INSERT INTO @Remainder
	SELECT ID,
		ROW_NUMBER() OVER (ORDER BY ID) AS Row_Counter,  ROW_NUMBER() OVER (ORDER BY ID)% 2 AS Remainder    
	FROM @5DayPayments

	
	DECLARE @LastCarrierName nvarchar(250) = ''
	DECLARE @LastPayee nvarchar(250) = ''

	IF (Select COUNT(CarrierName) from @5DayPayments) > 0 
	BEGIN
	SET @BodyMessage = @BodyMessage + '<P style="font-weight: bold; font-size: large; padding-bottom: .25em; margin-bottom: .25em;">Details of Overdue Amounts</P>'


				--create a cursor do loop through invoices and print appropriate HTML
				DECLARE @CarrierName nvarchar(250), @DefenseCounsel nvarchar(500), @ClaimantName nvarchar(300), @Matter nvarchar(250), @InvoiceNumber nvarchar(250), 
							@Payee nvarchar(100), @AmountBilled money, @DueDate nvarchar(150), @Owed money, @RemainderValue nvarchar(3), @CarrierNameOTP nvarchar(250), @DefenseCounselOTP nvarchar(500), @CarrierNameTO nvarchar(250), @OwedToPayeeOTP money, @OwedByCarrierTO money--, @LastCarrierName nvarchar(250), @LastPayee nvarchar(250)
				
	 
				DECLARE Carrier_curs CURSOR FOR
				SELECT DISTINCT CarrierName           
				FROM @5DayPayments DP
					SET @BodyMessage = @BodyMessage + N'<table border=0 cellspacing=0 cellpadding=0>'
					SET @BodyMessage = @BodyMessage + N'<tr><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Carrier</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Payee</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Claim Type</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Claimant Name</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Invoice Number</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Amount Owed</th></th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Due Date</th>'
					
				OPEN Carrier_curs
				FETCH NEXT FROM Carrier_curs INTO @CarrierName

				
				WHILE @@FETCH_STATUS = 0
				--While @@FETCH_STATUS = 0
				BEGIN
					--Write out the carrier name, if necessary
					IF @LastCarrierName <> @CarrierName
					BEGIN
						SET @BodyMessage = @BodyMessage + '<tr>'
						SET @BodyMessage = @BodyMessage + '<td style="text-align: left; font-weight:bold; background-color: #E36F1E; color: #ffffff; color: white;" colspan="6">' + @CarrierName + '</td>'
						SET @BodyMessage = @BodyMessage + '<td style="text-align: left; font-weight:bold; background-color: #E36F1E; color: #ffffff;" colspan="1"></td>'
						SET @BodyMessage = @BodyMessage + '</tr>' 
						
					END
									
										DECLARE DefenseCounsel_curs CURSOR FOR
										SELECT DISTINCT DefenseCounsel					            
										FROM @5DayPayments DP
										WHERE CarrierName = @CarrierName

										OPEN DefenseCounsel_curs
										FETCH NEXT FROM DefenseCounsel_curs INTO @DefenseCounsel
		
										

										While @@FETCH_STATUS = 0
										BEGIN
											IF @LastPayee <> @DefenseCounsel OR @CarrierName <> @LastCarrierName
											BEGIN

													SET @BodyMessage = @BodyMessage + '<tr>' 
													
													SET @BodyMessage = @BodyMessage + '<td style="text-align: left; background-color: #D8D8D8;" colspan="1"> </td>'
													SET @BodyMessage = @BodyMessage + '<td colspan="6" style="text-align: left; font-weight:bold; background-color: #D8D8D8">' + @DefenseCounsel + '</td>'

													SET @BodyMessage = @BodyMessage + '</tr>' 
												
											END

										
																DECLARE Info_curs CURSOR FOR
							            
																SELECT Matter, ClaimantName, InvoiceNumber, Owed, DueDate, Remainder					            
																FROM @5DayPayments DP
																WHERE CarrierName = @CarrierName AND DefenseCounsel = @DefenseCounsel
																--WHERE EXISTS (SELECT DISTINCT DefenseCounsel FROM @DefenseCounselList)

																OPEN Info_curs
																FETCH NEXT FROM Info_curs INTO @Matter, @ClaimantName, @InvoiceNumber, @AmountBilled, @DueDate, @RemainderValue

																While @@FETCH_STATUS = 0
										

																BEGIN

																	IF @RemainderValue = 1 BEGIN
																	SET @BodyMessage = @BodyMessage + '<tr style="background-color: #D6E8FF; ">'
																	END
																	ELSE BEGIN
																	SET @BodyMessage = @BodyMessage + '<tr>'
																	END
																	--SET @BodyMessage = @BodyMessage + '<tr>'
																	--SET @BodyMessage = @BodyMessage + '<td>' + '&nbsp;' + '</td>'
																	SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: center; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;"> </td>'
																	SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: center; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;"> </td>'
																	SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: left; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + @Matter + '</td>'--</tr>'
																	SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: left; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + @ClaimantName + '</td>'
																	SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + @InvoiceNumber + '</td>'
																	SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @AmountBilled,1) + '</td>'
																	SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + @DueDate + '</td></tr>'
																	
																		--SET @BodyMessage = @BodyMessage + '</td>'
																		--SET @BodyMessage = @BodyMessage + '</tr>'															
																
																	IF(@LastPayee = '' OR @LastPayee <> @DefenseCounsel)
																		SET @LastPayee = @DefenseCounsel
																	FETCH NEXT FROM Info_curs INTO @Matter, @ClaimantName, @InvoiceNumber, @AmountBilled, @DueDate, @RemainderValue

																END
																CLOSE Info_curs
																DEALLOCATE Info_curs

									
																							DECLARE OwedtoPayee_curs CURSOR FOR

																							SELECT CarrierName, DefenseCounsel, OwedToPayee				            
																							FROM @OwedtoPayee DP
																							WHERE CarrierName = @CarrierName AND DefenseCounsel = @DefenseCounsel
																							OPEN OwedtoPayee_curs
																							FETCH NEXT FROM OwedtoPayee_curs INTO @CarrierName, @DefenseCounsel, @OwedToPayeeOTP

																							While @@FETCH_STATUS = 0


																							BEGIN
																							
																									SET @BodyMessage = @BodyMessage + '<tr>'
																									SET @BodyMessage = @BodyMessage + '<td colspan="1" style="text-align: right; font-weight:bold; background-color: #D8D8D8"/>' 
																									SET @BodyMessage = @BodyMessage + '<td colspan="4" style="text-align: left; font-weight:bold; background-color: #D8D8D8">' + 'Total Owed by &nbsp;' + @CarrierName + '&nbsp;to &nbsp;' + @DefenseCounsel + '</td>'
																									SET @BodyMessage = @BodyMessage + '<td colspan="1" style="text-align: right; font-weight:bold; background-color: #D8D8D8">' + '$' + CONVERT(varchar(12), @OwedToPayeeOTP,1) + '</td>'
																									SET @BodyMessage = @BodyMessage + '<td colspan="1" style="text-align: right; font-weight:bold; background-color: #D8D8D8"></td>'
																									SET @BodyMessage = @BodyMessage + '</tr>'
																									
																							FETCH NEXT FROM OwedtoPayee_curs INTO @CarrierName, @DefenseCounsel, @OwedToPayeeOTP


																							END
																							CLOSE OwedtoPayee_curs
																							DEALLOCATE OwedtoPayee_curs
									
									
							FETCH NEXT FROM DefenseCounsel_curs INTO @DefenseCounsel
							END
							CLOSE DefenseCounsel_curs
							DEALLOCATE DefenseCounsel_curs
													DECLARE OwedByCarrier_curs CURSOR FOR

													SELECT CarrierName, OwedByCarrier				            
													FROM @OwedByCarrier DP
													WHERE CarrierName = @CarrierName


													OPEN OwedByCarrier_curs
													FETCH NEXT FROM OwedByCarrier_curs INTO @CarrierName, @OwedByCarrierTO

													While @@FETCH_STATUS = 0


													BEGIN

															SET @BodyMessage = @BodyMessage + '<tr>'
															SET @BodyMessage = @BodyMessage + '<td colspan="5" style="text-align: left; font-weight:bold; background-color: #E36F1E; color: white;" >' + 'Total Owed by &nbsp;' + @CarrierName +'</td>'
															SET @BodyMessage = @BodyMessage + '<td colspan="1" style="text-align: right; font-weight:bold; background-color: #E36F1E; color: white;" >' + '$' + CONVERT(varchar(12), @OwedByCarrierTO,1) + '</td>'
															SET @BodyMessage = @BodyMessage + '<td style="text-align: right; font-weight:bold; background-color: #E36F1E; color: #ffffff";"></td>'
															SET @BodyMessage = @BodyMessage + '</tr>'
															
													FETCH NEXT FROM OwedByCarrier_curs INTO @CarrierName, @OwedToPayeeOTP


													END
													CLOSE OwedByCarrier_curs
													DEALLOCATE OwedByCarrier_curs
							
				SET @BodyMessage = @BodyMessage + '</tr>'
				IF(@LastCarrierName = '' OR @LastCarrierName <> @CarrierName)
					SET @LastCarrierName = @CarrierName
				FETCH NEXT FROM Carrier_curs INTO @CarrierName

				END
				CLOSE Carrier_curs
				DEALLOCATE Carrier_curs
	            
							
	SET @BodyMessage = @BodyMessage + N'</table>' +  N'<br>'    
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyOverduePayments_OverdueDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyOverduePayments_OverdueDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyOverduePayments_OverdueDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyOverduePayments_OverdueDetails] TO [power_user]
GO
