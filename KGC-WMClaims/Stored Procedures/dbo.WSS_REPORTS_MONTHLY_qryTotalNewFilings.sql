SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_REPORTS_MONTHLY_qryTotalNewFilings] (@MonthID  int, 
                                                                @YearID   int, 
                                                                @MatterID int) 
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

--SELECT COUNT(distinct(cl.ClaimLawsuitID)) AS 'Count'
--FROM tblClaim c 
--	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
--	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
--    LEFT JOIN (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
--    INNER JOIN tblPlaintiffCounselAddress_ALT pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
--	INNER JOIN tblPlaintiffCounsel_ALT pc         ON pc.PlaintiffCounselID = pca.PlaintiffCounselID
--WHERE  MONTH(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) = @Month 
--	and YEAR(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) = @Year 
--	AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
--ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC



SELECT COUNT(distinct(cl.ClaimLawsuitID)) AS 'Count'
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
	INNER JOIN tblLawsuit l on cl.LawsuitID=l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
	INNER JOIN tblPlaintiffCounsel pc on pc.PlaintiffCounselID=pca.PlaintiffCounselID
WHERE month(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate))=@Month 
	AND year(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate))=@Year 
	--and IsPrimaryStatus=1 and IsPrimaryForClaim = 1
	AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
	AND ClaimLawsuitStatusID=22
ORDER BY COUNT(distinct(cl.ClaimLawsuitID)) DESC
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qryTotalNewFilings] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qryTotalNewFilings] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_MONTHLY_qryTotalNewFilings] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_MONTHLY_qryTotalNewFilings] TO [power_user]
GO
