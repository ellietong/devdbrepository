SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGender]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT NULL AS GenderID, '' AS Gender
UNION
SELECT GenderID, Gender
FROM tblGender
ORDER BY Gender DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGender] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGender] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGender] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGender] TO [power_user]
GO
