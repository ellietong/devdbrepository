SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument]
(@ClaimantPersonalInfoID bigint, @DocumentID bigint)
AS
SET NOCOUNT ON


--DECLARE @ClaimantDocumentTypeID int =  (SELECT ClaimantDocumentTypeID FROM tblDocuments WHERE DocumentID = @DocumentID)

--IF @ClaimantDocumentTypeID IN (1, 22)
--BEGIN

INSERT INTO tblMultiClaimantDocument (ClaimantPersonalInfoID, DocumentID, QCDate)
	VALUES(@ClaimantPersonalInfoID, @DocumentID, GETDATE())

	

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [power_user]
GO
