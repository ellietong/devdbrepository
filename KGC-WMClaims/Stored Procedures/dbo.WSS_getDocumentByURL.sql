SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_getDocumentByURL]
	@URL nvarchar(4000)
AS
BEGIN
	SELECT COUNT(*) FROM tblDocuments WHERE DocumentURL = @URL
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_getDocumentByURL] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_getDocumentByURL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_getDocumentByURL] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_getDocumentByURL] TO [power_user]
GO
