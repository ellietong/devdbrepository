SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_COMPLAINTS_qryRescoComplaints] (@StartDate     datetime = '1/1/1900',
															@EndDate      datetime = '1/1/2100',
															@StateID      int      = NULL,
															@MatterID     int      = NULL,
															@emailaddress nvarchar (250) = NULL)
AS

set nocount on

BEGIN

--DECLARE @StartDate datetime
--DECLARE @EndDate datetime
--DECLARE @StateID int = null
--DECLARE @MatterID int= null

--SET @StartDate='1/1/1900'
--SET @EndDate='1/1/2100'

DECLARE @UserMatters TABLE (MatterID int)
IF @emailaddress IS NULL
BEGIN
	INSERT INTO @UserMatters
		SELECT MatterID
		FROM tblMatter
		WHERE MatterID IN(1,2,4,5,8)
END
ELSE
BEGIN
	IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	BEGIN
		INSERT INTO @UserMatters
			SELECT MatterID
			FROM tblMatter
			WHERE MatterID IN(1,2,4,5,8)
	END
	ELSE
	BEGIN
		DECLARE @UserPersonID int = (SELECT PersonID FROM vtblPerson WHERE email = @emailaddress)

		INSERT INTO @UserMatters
			SELECT MatterID
			FROM tblMatterInsurersToNotify
			WHERE PersonID = @UserPersonID
	END
END

DECLARE @MaxMatterID INT
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @MaxStateID INT
SET @MaxStateID = (SELECT MAX(StateID) FROM tblState)

SELECT DISTINCT d.DocumentID,
	'https://www.kcicextranet.com/clients/wm/cip/pages/DocumentProfile.aspx?ItemId='+COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') AS DocumentLink,
	COALESCE(d.DocumentTitle,'') as DocumentTitle,
	cpi.LastName,
	cpi.FirstName,
	cpi.ClaimantPersonalInfoID,
	COALESCE(cpi.LastName, '') + ', ' +  COALESCE(cpi.FirstName, '') as ClaimantFullName,
	max(ServiceDate) as ServiceDate,
	D.MatterID,
	m.Matter,
	dc.DefendantCompany AS NamedDefendant,
	State,
	d.DateCreated,
	d.InsurerNotificationDate AS DateNoticed,
	AmazonKey
	--max(dcn.DateNoticed) AS DateNoticed--,
	--@MatterID AS SelectedMatter

FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	INNER join tblComplaintTrackingLawsuit ctl ON d.ComplaintID = ctl.ComplaintTrackingID
	INNER join tblclaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID 
	INNER join tblclaimlawsuit cl on cl.claimid = c.claimid and ctl.ClaimLawsuitID = cl.ClaimLawsuitID
	INNER join tblAllegation a on cl.ClaimLawsuitID=a.ClaimLawsuitID 
	INNER join tblDefendantCompany dc on dc.DefendantCompanyID=a.DefendantCompanyID
	INNER join tbllawsuit l on l.lawsuitid =cl.lawsuitid
	INNER join tblState s on s.StateID = l.StateID
	INNER join tblmatter m on m.matterid = c.matterid
WHERE COALESCE(ServiceDate,'1/1/1900') between @StartDate AND @EndDate
	AND d.ClaimantDocumentTypeID IN (8)
	AND m.MatterID >= COALESCE(@MatterID, 0) AND m.MatterID <= COALESCE(@MatterID, @MaxMatterID)
	AND m.MatterID IN(SELECT um.MatterID FROM @UserMatters um)
	AND l.StateID >= COALESCE(@StateID, 0) AND l.StateID <= COALESCE(@StateID, @MaxStateID)
	AND InsurerNotificationDate is not null
GROUP BY
	d.documentID,
	d.DocumentTitle,
	LastName,
	FirstName,
	cpi.ClaimantPersonalInfoID,
	D.MatterID,
	m.Matter,
	dc.DefendantCompany,
	State,
	d.DateCreated,
	d.InsurerNotificationDate,
	AmazonKey

	
union all

SELECT DISTINCT d.DocumentID,
	'https://www.kcicextranet.com/clients/wm/cip/pages/DocumentProfile.aspx?ItemId='+COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') AS DocumentLink,
	COALESCE(d.DocumentTitle,'') as DocumentTitle,
	cpi.LastName,
	cpi.FirstName,
	cpi.ClaimantPersonalInfoID,
	COALESCE(cpi.LastName, '') + ', ' +  COALESCE(cpi.FirstName, '') as ClaimantFullName,
	max(ServiceDate) as ServiceDate,
	D.MatterID,
	m.Matter,
	dc.DefendantCompany AS NamedDefendant,
	State,
	d.DateCreated,
	d.InsurerNotificationDate AS DateNoticed,
	AmazonKey
	--max(dcn.DateNoticed) AS DateNoticed--,
	--@MatterID AS SelectedMatter

FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	INNER join tblmatter m on m.matterid = d.matterid
	LEFT JOIN  tblDefendantCompany dc on dc.DefendantCompanyID=d.DefendantCompanyID
	INNER join tblclaim c on c.matterid = m.matterid and c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER join tblclaimlawsuit cl on cl.claimid = c.claimid  
	LEFT join tblAllegation a on cl.ClaimLawsuitID=a.ClaimLawsuitID 
	INNER join tbllawsuit l on l.lawsuitid =cl.lawsuitid
	INNER join tblState s on s.StateID = l.StateID
WHERE COALESCE(ServiceDate,'1/1/1900') between @StartDate AND @EndDate
	AND d.ClaimantDocumentTypeID IN (8)
	AND m.MatterID >= COALESCE(@MatterID, 0) AND m.MatterID <= COALESCE(@MatterID, @MaxMatterID)
	AND m.MatterID IN(SELECT um.MatterID FROM @UserMatters um)
	AND l.StateID >= COALESCE(@StateID, 0) AND l.StateID <= COALESCE(@StateID, @MaxStateID)
	and coalesce(a.defendantcompanyid, 9999) = coalesce(d.defendantcompanyid, 9999)
	and d.datecreated between '6/15/2013' and '7/2/2013'
	AND InsurerNotificationDate is not null
GROUP BY
	d.documentID,
	d.DocumentTitle,
	LastName,
	FirstName,
	cpi.ClaimantPersonalInfoID,
	D.MatterID,
	m.Matter,
	dc.DefendantCompany,
	State,
	d.DateCreated,
	d.InsurerNotificationDate,
	AmazonKey

Union all

SELECT DISTINCT d.DocumentID,
	'https://www.kcicextranet.com/clients/wm/cip/pages/DocumentProfile.aspx?ItemId='+COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') AS DocumentLink,
	COALESCE(d.DocumentTitle,'') as DocumentTitle,
	cpi.LastName,
	cpi.FirstName,
	cpi.ClaimantPersonalInfoID,
	COALESCE(cpi.LastName, '') + ', ' +  COALESCE(cpi.FirstName, '') as ClaimantFullName,
	max(ServiceDate) as ServiceDate,
	D.MatterID,
	m.Matter,
	dc.DefendantCompany AS NamedDefendant,
	State,
	d.DateCreated,
	d.InsurerNotificationDate AS DateNoticed,
	AmazonKey
	--max(dcn.DateNoticed) AS DateNoticed--,
	--@MatterID AS SelectedMatter

FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	INNER join tblmatter m on m.matterid = d.matterid
	INNER join tblclaim c on c.matterid = m.matterid and c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER join tblclaimlawsuit cl on cl.claimid = c.claimid  
	LEFT join tblAllegation a on cl.ClaimLawsuitID=a.ClaimLawsuitID 
	LEFT JOIN  tblDefendantCompany dc on dc.DefendantCompanyID=a.DefendantCompanyID
	INNER join tbllawsuit l on l.lawsuitid =cl.lawsuitid
	INNER join tblState s on s.StateID = l.StateID
WHERE COALESCE(ServiceDate,'1/1/1900') between @StartDate AND @EndDate
	AND d.ClaimantDocumentTypeID IN (8)
	AND m.MatterID >= COALESCE(@MatterID, 0) AND m.MatterID <= COALESCE(@MatterID, @MaxMatterID)
	AND m.MatterID IN(SELECT um.MatterID FROM @UserMatters um)
	AND l.StateID >= COALESCE(@StateID, 0) AND l.StateID <= COALESCE(@StateID, @MaxStateID)
	and coalesce(a.defendantcompanyid, 9999) = coalesce(d.defendantcompanyid, 9999)
	and d.datecreated between '6/15/2013' and '7/2/2013'
	AND InsurerNotificationDate is not null
	and d.documentid in (24440, 24444, 24439)
GROUP BY
	d.documentID,
	d.DocumentTitle,
	LastName,
	FirstName,
	cpi.ClaimantPersonalInfoID,
	D.MatterID,
	m.Matter,
	dc.DefendantCompany,
	State,
	d.DateCreated,
	d.InsurerNotificationDate,
	AmazonKey

union all
SELECT DISTINCT d.DocumentID,
	'https://www.kcicextranet.com/clients/wm/cip/pages/DocumentProfile.aspx?ItemId='+COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') AS DocumentLink,
	COALESCE(d.DocumentTitle,'') as DocumentTitle,
	cpi.LastName,
	cpi.FirstName,
	cpi.ClaimantPersonalInfoID,
	COALESCE(cpi.LastName, '') + ', ' +  COALESCE(cpi.FirstName, '') as ClaimantFullName,
	max(ServiceDate) as ServiceDate,
	D.MatterID,
	m.Matter,
	dc.DefendantCompany AS NamedDefendant,
	State,
	d.DateCreated,
	d.InsurerNotificationDate AS DateNoticed,
	AmazonKey
	--max(dcn.DateNoticed) AS DateNoticed--,
	--@MatterID AS SelectedMatter

FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	INNER join tblmatter m on m.matterid = d.matterid
	INNER join tblclaim c on c.matterid = m.matterid and c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER join tblclaimlawsuit cl on cl.claimid = c.claimid  
	LEFT join tblAllegation a on cl.ClaimLawsuitID=a.ClaimLawsuitID 
	LEFT JOIN  tblDefendantCompany dc on dc.DefendantCompanyID=a.DefendantCompanyID
	INNER join tbllawsuit l on l.lawsuitid =cl.lawsuitid
	INNER join tblState s on s.StateID = l.StateID
WHERE 
	COALESCE(ServiceDate,'1/1/1900') between @StartDate AND @EndDate
	AND d.ClaimantDocumentTypeID IN (8)
	AND m.MatterID >= COALESCE(@MatterID, 0) AND m.MatterID <= COALESCE(@MatterID, @MaxMatterID)
	AND m.MatterID IN(SELECT um.MatterID FROM @UserMatters um)
	AND l.StateID >= COALESCE(@StateID, 0) AND l.StateID <= COALESCE(@StateID, @MaxStateID)
	and d.datecreated between '6/15/2013' and '7/2/2013'
	AND InsurerNotificationDate is not null
	and d.documentid in (24440, 24444, 24439)
GROUP BY
	d.documentID,
	d.DocumentTitle,
	LastName,
	FirstName,
	cpi.ClaimantPersonalInfoID,
	D.MatterID,
	m.Matter,
	dc.DefendantCompany,
	State,
	d.DateCreated,
	d.InsurerNotificationDate,
	AmazonKey


ORDER BY
	insurernotificationdate desc, servicedate desc
	
		
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_COMPLAINTS_qryRescoComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_COMPLAINTS_qryRescoComplaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_COMPLAINTS_qryRescoComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_COMPLAINTS_qryRescoComplaints] TO [power_user]
GO
