SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryClaimantSearch] (@ClaimantID   int, 
												 @emailaddress nvarchar(250))
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantID   int
--DECLARE @emailaddress nvarchar(250)
--SET @ClaimantID   = 189026
--SET @emailaddress = 'RAlbee@bakerlaw.com'

DECLARE @FirmAddressID int
SET @FirmAddressID = (SELECT FirmAddressID 
					  FROM vtblPerson 
					  WHERE email = @emailaddress)

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE cpi.ClaimantPersonalInfoID = ' + CONVERT(nvarchar(20),@ClaimantID)

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
BEGIN
	SET @JoinString = ' LEFT JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
						LEFT JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.matterID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@FirmAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@FirmAddressID,0)) + ')'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = '
	SELECT DISTINCT cpi.ClaimantPersonalInfoID AS ClaimantID,
		cpi.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '' '' ELSE MiddleName + '' '' END AS ClaimantName
	FROM tblClaimantPersonalInfo cpi 
		INNER JOIN tblClaim c									 ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT  JOIN tblClaimLawsuit cl							 ON c.ClaimID = cl.ClaimID
		LEFT  JOIN tblLawsuit l									 ON cl.LawsuitID = L.LawsuitID ' +
		@JoinString +
	@WhereString + '
	ORDER BY ClaimantName
	OPTION (RECOMPILE)'
	
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantSearch] TO [power_user]
GO
