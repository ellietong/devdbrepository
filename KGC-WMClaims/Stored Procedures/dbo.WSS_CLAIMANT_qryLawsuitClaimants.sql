SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryLawsuitClaimants] (@LawsuitID bigint)
AS

SET NOCOUNT ON

SELECT cpi.ClaimantPersonalInfoID AS ClaimantID, 
	cpi.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + 
		CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi	ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l					ON cl.LawsuitID = l.LawsuitID
WHERE cl.LawsuitID = @LawsuitID
ORDER BY LastName, FirstName

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryLawsuitClaimants] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryLawsuitClaimants] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryLawsuitClaimants] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryLawsuitClaimants] TO [power_user]
GO
