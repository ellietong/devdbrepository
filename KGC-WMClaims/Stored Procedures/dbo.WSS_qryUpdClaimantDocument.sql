SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdClaimantDocument]
	-- Add the parameters for the stored procedure here
	(@DocumentID int, @DocumentTypeID int, @Notes nvarchar (1000), @Emailaddress nvarchar(250))  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 --   -- Insert statements for procedure here
	--UPDATE tblDocuments
	--SET DocumentTypeID = COALESCE(@DocumentTypeID, DocumentTypeID),
	--	Comments = @Notes, ModifiedBy = @Emailaddress
	--WHERE DocumentID = @DocumentID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantDocument] TO [power_user]
GO
