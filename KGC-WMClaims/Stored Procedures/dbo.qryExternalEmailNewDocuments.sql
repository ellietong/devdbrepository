SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[qryExternalEmailNewDocuments]
AS

SET NOCOUNT ON

BEGIN

/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH

1-28-12 - CAS modified to query data QC'ed between yesterday and current day at 4 PM so that query goes off those time stamps and not the current system date
which could cause some notifications to be lost.
*/

--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

Declare @Today4PM datetime
Declare @Yesterday4PM datetime
SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 20:00:00.000')
SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 20:00:00.000')
DECLARE @MatterID int --= 2

SET @Subject =
'Waste Management: New Documents Uploaded by Waste Management ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


/*SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://admin.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>*/ --WM Logo


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (DocID int, docTitle nvarchar (500), FirmAddressID int, lName nvarchar (50),
fName nvarchar (50), ClaimantPersonalInfoID bigint, DocumentType nvarchar (500) /*, ServiceDate date, MatterID int, State nvarchar(100), Jurisdiction nvarchar(150)*/)

INSERT INTO @NewComplaintsTable
	SELECT DISTINCT
		d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/wm/cip/pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		d.FirmID,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		cdt.ClaimantDocumentType --, max(ServiceDate), D.MatterID, State--, Jurisdiction
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	INNER JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
	--left join tblclaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID and d.MatterID = c.MatterID
	--left join tblclaimlawsuit cl on cl.claimid = c.claimid	
	--left join tbllawsuit l on l.lawsuitid =cl.lawsuitid
	--left join tblState s on s.StateID = l.StateID
	--left join tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
	WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
	AND d.ClaimantDocumentTypeID NOT IN (8, 48, 49, 29, 40, 42)
	AND (RIGHT(CreatedBy, 6) = 'wm.com'
	OR RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	AND AcceptedBy IS NULL
	GROUP BY	d.documentID,
				d.DocumentTitle,
				d.FirmID,
				LastName,
				FirstName,
				cpi.ClaimantPersonalInfoID,
				ClaimantDocumentType --, D.MatterID, State



DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar (max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar (max), @ClaimantID bigint, @ClaimantList nvarchar (max)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for SELECT
	DocID,
	COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
	claimantpersonalInfoID
FROM @NewComplaintsTable
--UNION
--SELECT DocID, COALESCE(Lname, '') + ', ' +  COALESCE(Fname, ''), claimantpersonalInfoID FROM @NewDocumentsTable
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
WHILE @@FETCH_STATUS = 0
BEGIN

IF (LEN (@ClaimantList) = 0 OR @ClaimantList IS NULL)
BEGIN

SET @ClaimantList = '<ul style="margin:0; padding:0;"> <a href="https://www.kcicextranet.com/clients/wm/cip/pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + @Claimant + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
END
ELSE
BEGIN

SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul style="margin:0; padding:0;">  <a href="https://www.kcicextranet.com/clients/wm/cip/pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + COALESCE(@Claimant, '') + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
PRINT @@FETCH_STATUS
IF @DocID < > @LastDocID OR @@FETCH_STATUS < > 0
BEGIN

INSERT INTO @ClaimantPerDoc

	SELECT
		@LastDocID,
		@ClaimantList

SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints



--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

DECLARE @firmaddressid nvarchar (10), @firmURL nvarchar (100)

DECLARE firm_Curs CURSOR FOR SELECT
	nct.FirmAddressID,
	'https://www.kcicextranet.com/clients/wm/cip/pages/Documents.aspx'
FROM @NewComplaintsTable nct
GROUP BY nct.FirmAddressID



--DECLARE @Complaint_MatterID int, @Complaint_Matter nvarchar(500)

--DECLARE @MatterList TABLE (Matter nvarchar(500))
--INSERT INTO @MatterList
--SELECT DISTINCT Matter
--FROM @NewComplaintsTable nct
--INNER JOIN tblMatter M ON nct.MatterID = M.MatterID

--DECLARE @LastMatter nvarchar(500) = '' 


OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @firmaddressid, @firmURL
WHILE @@FETCH_STATUS = 0
BEGIN

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = /*@LogoHeader*/ +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'

--Set table column headings and populate the cells with the complaints from @NewComplaintsTable

SET @BodyMessage = @BodyMessage + '<H4>The following documents have been uploaded in the last 24 hours: </H4>'

--DECLARE Matter_curs CURSOR FOR
--SELECT DISTINCT nct.MatterID, Matter
--FROM @NewComplaintsTable nct
--INNER JOIN tblMatter M ON nct.MatterID = M.MatterID
--where nct.firmaddressid = @FirmAddressID		
--OPEN Matter_curs 
--FETCH NEXT FROM Matter_curs INTO @Complaint_MatterID, @Complaint_Matter

--WHILE @@FETCH_STATUS = 0
--BEGIN 

IF (SELECT
	COUNT(docTitle)
FROM @NewComplaintsTable
WHERE FirmAddressID = @firmaddressid) > 0
begin



--SET @BodyMessage = @BodyMessage + '<H4>' + @Complaint_Matter +  '</H4>'
--SET @BodyMessage = @BodyMessage + '<H4>The following complaints have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + --SET @BodyMessage = @BodyMessage + N'<table>' + 

N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants (Claimant ID) </th>' +






COALESCE(CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		DocumentType)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		docTitle)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		cl.ClaimantList)
	AS td,
	'' --,
--@TDStyle AS 'td/@style', (SELECT max(Jurisdiction)) as td, ''
FROM @NewComplaintsTable C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
WHERE c.FirmaddressID = (SELECT
	@firmaddressid) --AND c.MatterID = (select @Complaint_MatterID)
GROUP BY	docTitle,
			cl.ClaimantList,
			documenttype

FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)), '') +

N'</table>' + N'<br/>'






end
--ELSE
--begin
--Set @BodyMessage = @BodyMessage + '<H4>No Complaints have been uploaded in the last 24 hours. </H4>'
--end
--            FETCH NEXT FROM Matter_curs INTO @Complaint_MatterID, @Complaint_Matter
--                        END
--            CLOSE Matter_curs
--            DEALLOCATE Matter_curs




--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

--Service Date Assumption 

--IF (Select COUNT(docTitle) from @NewComplaintsTable where FirmAddressID = @FirmAddressID) >0 
--begin
--SET @BodyMessage = @BodyMessage + '<br>*Note: Date served is the latest service date for claimants'' lawsuits.<br><br>'
--end

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'


DECLARE @RecipientList nvarchar (500)
SET @RecipientList = ''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar (500)
DECLARE contact_curs CURSOR
LOCAL FORWARD_ONLY STATIC READ_ONLY
FOR SELECT
	email
FROM vtblPerson p
WHERE FirmAddressID = @FirmAddressID

AND complaintContact = 1


OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS = 0
BEGIN

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

PRINT @BodyMessage


--QC Recipient List
--Set @Bodymessage = @BodyMessage + 'Recipients' + @RecipientList
--PRINT @RecipientList
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail --The wasteManagementProfile sends from @kcic.com
@profile_name = 'Claims',
@recipients = @RecipientList,
@copy_recipients = 'nshoebot@wm.com; ejespers@wm.com',
@blind_copy_recipients = 'hubbardb@kcic.com; claims@kcic.com; wm@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

UPDATE [KGA-ClaimsAdmin].dbo.tblComplaintTracking
SET ExternalEmailDate = GETDATE()
WHERE ComplaintID IN (SELECT
	ComplaintID
FROM @NewComplaintsTable)

--Move to the next firm to create a new email
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmurl
END
CLOSE firm_curs
DEALLOCATE firm_curs


END




GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewDocuments] TO [power_user]
GO
