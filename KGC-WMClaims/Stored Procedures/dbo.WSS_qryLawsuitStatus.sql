SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryLawsuitStatus] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClaimLawsuitStatusID, ClaimLawsuitStatus FROM tblClaimLawsuitStatus
	UNION
	SELECT NULL, ''
	ORDER BY ClaimLawsuitStatus
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryLawsuitStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryLawsuitStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryLawsuitStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryLawsuitStatus] TO [power_user]
GO
