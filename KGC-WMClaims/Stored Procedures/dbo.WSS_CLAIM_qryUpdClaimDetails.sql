SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIM_qryUpdClaimDetails]
	-- Add the parameters for the stored procedure here
	(@ClaimID int, @MatterID int, @EmailAddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



UPDATE tblClaim 
SET MatterID = @MatterID,
	SPUsername = @EmailAddress
WHERE ClaimID = @ClaimID	
	

END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaimDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaimDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryUpdClaimDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryUpdClaimDetails] TO [power_user]
GO
