SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantNameByID]
(@ClaimantPersonalInfoID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT COALESCE(cpi.LastName + ', ', '') + COALESCE(cpi.FirstName + ' ', '') + COALESCE(MiddleName, '') AS Claimant,
		cpi.ClaimantPersonalInfoID, FirstName, LastName, MiddleName
	FROM tblClaimantPersonalInfo cpi
	WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantNameByID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantNameByID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantNameByID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantNameByID] TO [power_user]
GO
