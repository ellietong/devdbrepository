SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryOutstandingAccountsReceivable_CarrierGroup]
(@CarrierGroupID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--DECLARE @CarrierGroupID int = 10000

	
    
    DECLARE @strCommand nvarchar(MAX)
    SET @strCommand = 'SELECT ar.AccountsReceivableID, ar.Amount AS ReceivableAmount, ar.TravelersBillID, ar.PolicyID, ar.Amount - COALESCE(arp.PaidAmount, 0) AS Remaining, 
        p.PolicyNum, CASE WHEN d.DocumentTypeID = 3 THEN tb.InvoiceNumber ELSE ''N\A'' END AS InvoiceNumber, CPI.LastName + '', '' + CASE WHEN FirstName IS NULL 
		  THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '' '' ELSE MiddleName + '' '' END AS ClaimantName, FirstName, MiddleName, LastName, c.CarrierName, f.Firm AS FirmName, tb.FirmID, d.ClaimantPersonalInfoID, ClaimantDocumentType,
		  ClaimType
    FROM tblAccountsReceivable ar
    INNER JOIN tblPolicy p ON p.PolicyID = ar.PolicyID
    INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
    INNER JOIN tblTravelersBill tb ON ar.TravelersBillID = tb.TravelersBillID
    INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
    INNER JOIN tblClaimantPersonalInfo CPI on d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
    INNER JOIN tblDocumentType dt ON d.DocumentTypeID = dt.ClaimantdocumentTypeID
    INNER JOIN tblClaimType ct ON tb.ClaimTypeID = ct.ClaimTypeID
    LEFT JOIN tblFirm f ON tb.FirmID = f.FirmID
    LEFT JOIN (SELECT AccountsReceivableID, SUM(Amount) AS PaidAmount FROM tblAccountsReceivablePayment GROUP BY AccountsReceivableID) arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
    WHERE ar.Amount - COALESCE(arp.PaidAmount, 0) <> 0 
    and lastname like ''%hadley%'''
    
    
    IF(@CarrierGroupID IS NOT NULL) SET @strCommand = @strCommand + 'AND CASE	WHEN c.CarrierGroupID IS NULL AND c.CarrierName = ''Producer'' THEN 10000 
																				WHEN c.CarrierGroupID IS NULL AND c.CarrierName = ''Shadow Carrier'' THEN 10001 ELSE c.CarrierGroupID END = @CarrierGroupIDParam ' 
    SET @strCommand = @strCommand + char(10) + char(13) + 'ORDER BY LastName, FirstName, MiddleName'
    
    EXEC sp_executesql @strCommand, N'@CarrierGroupIDParam int', @CarrierGroupIDParam = @CarrierGroupID
    
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOutstandingAccountsReceivable_CarrierGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOutstandingAccountsReceivable_CarrierGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryOutstandingAccountsReceivable_CarrierGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryOutstandingAccountsReceivable_CarrierGroup] TO [power_user]
GO
