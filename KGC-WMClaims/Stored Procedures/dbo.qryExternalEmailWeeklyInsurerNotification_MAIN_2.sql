SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyInsurerNotification_MAIN_2]
AS

SET NOCOUNT ON

BEGIN


--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

Declare @Today4PM datetime
Declare @Yesterday4PM datetime
SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')
SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')


SET @Subject =
'RESCO CIP: New Complaints Uploaded ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


/*SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://admin.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>*/ --WM Logo


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (DocID int, docTitle nvarchar (500), FirmAddressID int, lName nvarchar (50),
fName nvarchar (50), ClaimantPersonalInfoID bigint, ServiceDate date, MatterID int)

INSERT INTO @NewComplaintsTable
	SELECT DISTINCT
		d.DocumentID,
		'<a href="admin.kcicextranetdev.com/clients/wm/cip/pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		/*l.DefenseCOunselAddressID*/
		2,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		MAX(ServiceDate),
		D.MatterID
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
	WHERE mcd.QCDate BETWEEN GETDATE() - 7 AND GETDATE()
	AND d.ClaimantDocumentTypeID IN (1, 22)
	/*AND RIGHT(CreatedBy, 11) = 'kcicllc.com' -- Don't Need
and coalesce(Accepted,0) <> 1*/ --May Need 
	/*AND FirmID IS NOT NULL*/ /*NEED*/
	/*AND IsPrimaryForClaim = 1
AND IsCurrentClaim = 1*/
	GROUP BY	d.documentID,
				d.DocumentTitle,
				DefenseCOunselAddressID,
				LastName,
				FirstName,
				cpi.ClaimantPersonalInfoID,
				D.MatterID


--DECLARE @NewDocumentsTable TABLE (DocID int, docTitle nvarchar(500), FirmAddressID int, lName nvarchar(50), 
--fName nvarchar(50), DocType nvarchar(50), ClaimantPersonalInfoID bigint)
--INSERT INTO @NewDocumentsTable
--SELECT d.DocumentID, '<a href="admin.kcicextranet.com/clients/wm/cip/pages/DocumentProfile.aspx?ItemId='+ COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle, 
--l.DefenseCOunselAddressID, cpi.LastName, cpi.FirstName, ClaimantDocumentType, cpi.claimantpersonalinfoid
--FROM tblDocuments d
--INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
--INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
--left join tblClaimantDocumentType ct on ct.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
--left join tblclaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
--left join tblclaimlawsuit cl on cl.claimid = c.claimid
--left join tbllawsuit l on l.lawsuitid =cl.lawsuitid
--WHERE datecreated between @Yesterday4PM AND @Today4PM
--AND d.ClaimantDocumentTypeID IN (7,11,23,25,6,28)
--and RIGHT(CreatedBy, 11) = 'kcicllc.com'
--and coalesce(Accepted,0) <> 1 
--AND FirmID IS NOT NULL
--AND IsPrimaryForClaim = 1
--AND IsCurrentClaim = 1	
--GROUP BY d.DocumentID,  d.DocumentTitle,
--DefenseCOunselAddressID, LastName, FirstName, ClaimantDocumentType, cpi.claimantpersonalinfoid

DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar (max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar (max), @ClaimantID bigint, @ClaimantList nvarchar (max)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for SELECT
	DocID,
	COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
	claimantpersonalInfoID
FROM @NewComplaintsTable
--UNION
--SELECT DocID, COALESCE(Lname, '') + ', ' +  COALESCE(Fname, ''), claimantpersonalInfoID FROM @NewDocumentsTable
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
WHILE @@FETCH_STATUS = 0
BEGIN

IF (LEN (@ClaimantList) = 0 OR @ClaimantList IS NULL)
BEGIN

SET @ClaimantList = '<ul> <a href="admin.kcicextranetdev.com/clients/wm/cip/pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + @Claimant + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
END
ELSE
BEGIN

SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="admin.kcicextranetdev.com/clients/wm/cip/pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + COALESCE(@Claimant, '') + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
PRINT @@FETCH_STATUS
IF @DocID < > @LastDocID OR @@FETCH_STATUS < > 0
BEGIN

INSERT INTO @ClaimantPerDoc

	SELECT
		@LastDocID,
		@ClaimantList

SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints



--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

DECLARE @Complaint_MatterID int, @Complaint_Matter nvarchar (500)

DECLARE matter_Curs CURSOR FOR SELECT
	nct.matterID,
	Matter
FROM @NewComplaintsTable nct
INNER JOIN tblMatter M ON nct.MatterID = M.MatterID
GROUP BY	nct.matterID,
			matter
--Union
--Select doc.FirmAddressID, 
--'admin.kcicextranet.com/clients/wm/cip/pages/Documents.aspx'
--from @NewDocumentsTable doc
--group by doc.FirmAddressID


OPEN matter_Curs
FETCH NEXT FROM matter_Curs INTO @Complaint_MatterID, @Complaint_Matter
WHILE @@FETCH_STATUS = 0
BEGIN

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = /*@LogoHeader*/ +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'



--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

IF (SELECT
	COUNT(docTitle)
FROM @NewComplaintsTable
WHERE MatterID = @Complaint_MatterID) > 0
begin

IF @Complaint_MatterID = (1)
BEGIN
EXEC [dbo].[qryExternalEmailWeeklyInsurerNotification_Swindell] @BodyMessage = @BodyMessage OUTPUT
END
ELSE IF @Complaint_MatterID = (2)
BEGIN
EXEC [dbo].[qryExternalEmailWeeklyInsurerNotification_Kellogg] @BodyMessage = @BodyMessage OUTPUT
END
ELSE IF @Complaint_MatterID = (4)
BEGIN
EXEC [dbo].[qryExternalEmailWeeklyInsurerNotification_Pullman] @BodyMessage = @BodyMessage OUTPUT
END
ELSE IF @Complaint_MatterID = (5)
BEGIN
EXEC [dbo].[qryExternalEmailWeeklyInsurerNotification_Brand] @BodyMessage = @BodyMessage OUTPUT
END
ELSE IF @Complaint_MatterID = (6)
BEGIN
EXEC [dbo].[qryExternalEmailWeeklyInsurerNotification_RustFurnace_WAPC] @BodyMessage = @BodyMessage OUTPUT
END
ELSE IF @Complaint_MatterID = (7)
BEGIN
EXEC [dbo].[qryExternalEmailWeeklyInsurerNotification_WFI] @BodyMessage = @BodyMessage OUTPUT
END
ELSE IF @Complaint_MatterID = (8)
BEGIN
EXEC [dbo].[qryExternalEmailWeeklyInsurerNotification_Treco_Rust] @BodyMessage = @BodyMessage OUTPUT
END


--EXEC	[dbo].[qryExternalEmailNewComplaints_BRAND]
--		@BodyMessage = @BodyMessage OUTPUT		

SET @BodyMessage = @BodyMessage + '<H4>The following complaints have been uploaded in the previous week for' + '<H4>' + @Complaint_Matter + '</H4>' + ':'
SET @BodyMessage = @BodyMessage + N'<table>' +


N'<tr><th>Document Title</th><th>Linked Claimants (ClaimantID) </th><th>Date Served*</th>' +


CAST((SELECT
	td = docTitle,
	'',
	td = cl.ClaimantList,
	'',
	td = MAX(ServiceDate),
	''
FROM @NewComplaintsTable C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
WHERE c.MatterID = (SELECT
	@Complaint_MatterID)
GROUP BY	docTitle,
			cl.ClaimantList

FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +

N'</table>' + N'<br/>'

end
ELSE
begin
SET @BodyMessage = @BodyMessage + '<H4>No Complaints have been uploaded in the previous week. </H4>'
end
--IF (Select COUNT(docTitle) from @NewDocumentsTable where FirmAddressID = @FirmAddressID) >0 
--begin	

--Set @BodyMessage = @BodyMessage + '<H4>The following documents have been uploaded in the last 24 hours: </H4>'

--SET @BodyMessage = @BodyMessage + N'<table>' + N'<tr><th>Document Title</th><th>Linked Claimants (ClaimantID)</th><th>Document Type</th>' +

--CAST ( (
--	SELECT  td = docTitle, '', td = cl.ClaimantList, '', td = DocType, ''
--	FROM @NewDocumentsTable d
--	INNER JOIN @ClaimantPerDoc cl ON d.DocID = cl.DocID
--	WHERE FirmAddressID = (select @firmaddressid)
--	GROUP BY docTitle, cl.ClaimantList, DocType
--	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
--	N'</table>' +  N'<br/>'	
--end
--ELSE
--begin
--Set @BodyMessage = @BodyMessage + '<H4>No other new documents have been uploaded in the last 24 hours. </H4>'
--end

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

--Service Date Assumption 

IF (SELECT
	COUNT(docTitle)
FROM @NewComplaintsTable
WHERE MatterID = @Complaint_MatterID) > 0
begin
SET @BodyMessage = @BodyMessage + '<br>*Note: Date served is the latest service date for claimants'' lawsuits.<br><br>'
end

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'


DECLARE @RecipientList nvarchar (500)
SET @RecipientList = ''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar (500)
DECLARE contact_curs CURSOR
LOCAL FORWARD_ONLY STATIC READ_ONLY
FOR --SELECT email 
--FROM vtblPerson p
--WHERE FirmAddressID = @FirmAddressID

--AND complaintContact = 1
SELECT
	''


OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS = 0
BEGIN

--SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

PRINT @BodyMessage

----QC Recipient List
----Set @Bodymessage = @BodyMessage + 'Recipients' + @RecipientList
----PRINT @RecipientList
----Execute stored procedure to send the email to that firm
--EXEC msdb.dbo.sp_send_dbmail
----The wasteManagementProfile sends from @kcic.com
--@profile_name='Claims',
--@recipients = 'owenss@kcic.com', --'crookc@kcic.com',--'owenss@kcic.com', --
--@copy_recipients = '',
--@blind_copy_recipients = '',
--@subject = @subject,
--@body = @BodyMessage,
--@body_format = 'html'

--Move to the next firm to create a new email
FETCH NEXT FROM matter_Curs INTO @Complaint_MatterID, @Complaint_Matter
END
CLOSE matter_Curs
DEALLOCATE matter_Curs


END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_MAIN_2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_MAIN_2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_MAIN_2] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_MAIN_2] TO [power_user]
GO
