SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[WSS_qryDocumentType]

AS
SET NOCOUNT ON
SELECT ClaimantDocumentTypeID, ClaimantDocumentType FROM tblClaimantDocumentType
where ClaimantDocumentTypeID not in (48, 49, 29)/**exclude travelers bills and payment types**/
UNION
SELECT NULL, '<< Document Type >>'
ORDER BY ClaimantDocumentType
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDocumentType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDocumentType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDocumentType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDocumentType] TO [power_user]
GO
