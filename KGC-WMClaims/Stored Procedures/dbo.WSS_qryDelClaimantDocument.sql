SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDelClaimantDocument]
(@DocumentID int, @Emailaddress nvarchar(250))
AS
BEGIN
	SET NOCOUNT ON;

		--Select the file path
		SELECT DocumentURL FROM tblDocuments WHERE DocumentID = @DocumentID
		
		--Update the ModifiedBy field in tblDocuments prior to delete for audit log purposes
		UPDATE tblDocuments
			SET ModifiedBy = @emailaddress
		WHERE DocumentID = @DocumentID
		
		--Finally, do the delete
		DELETE FROM tblDocuments WHERE DocumentID = @DocumentID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelClaimantDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelClaimantDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelClaimantDocument] TO [power_user]
GO
