SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyOverduePayments_NonInterimOwedToResco]
(@BodyMessage nvarchar(MAX) output)
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @Payments TABLE (AccountsReceivableID int, Paid money)
	INSERT INTO @Payments
	SELECT ar.AccountsReceivableID,  SUM(COALESCE(arp.Amount,0))
	FROM tblAccountsReceivable ar
	INNER JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	GROUP BY ar.AccountsReceivableID	


	DECLARE @AgingAccountsReceivable TABLE (CarrierName nvarchar(250), AmountCurrent money, Amount30 money, Amount60 money, Amount90 money, 
			Amount120 money, AmountLong money, AmountOverdue money, AmountTotal money, Count30 money, Count60 money,
			Count90 money, Count120 money, CountLong money, CountTotal money, Remainder int)

	INSERT INTO @AgingAccountsReceivable
	SELECT COALESCE(CarrierName, 'Resco'), --Non-pivoted column
		--columns to pivot
		SUM(COALESCE(AmountCurrent, 0)) AS AmountCurrent,
		SUM(COALESCE(Amount30, 0)) AS Amount30,
		SUM(COALESCE(Amount60,0)) AS Amount60,
		SUM(COALESCE(Amount90, 0)) AS Amount90,
		SUM(COALESCE(Amount120, 0)) AS Amount120,
		SUM(COALESCE(AmountLong, 0)) AS AmountLong,
		SUM(COALESCE(Amount30,0) + COALESCE(Amount60,0) + COALESCE(Amount90,0) + COALESCE(Amount120,0) + COALESCE(AmountLong,0)) AS AmountOverdue,
		SUM(COALESCE(Amount30,0) + COALESCE(Amount60,0) + COALESCE(Amount90,0) + COALESCE(Amount120,0) + COALESCE(AmountLong,0) +COALESCE(AmountCurrent, 0)) AS AmountTotal,
		SUM(CASE WHEN COALESCE(Amount30, 0) > 0 THEN Count ELSE 0 END) AS Count30,
		SUM(CASE WHEN COALESCE(Amount60, 0) > 0 THEN Count ELSE 0 END) AS Count60,
		SUM(CASE WHEN COALESCE(Amount90, 0) > 0 THEN Count ELSE 0 END) AS Count90,
		SUM(CASE WHEN COALESCE(Amount120, 0) > 0 THEN Count ELSE 0 END) AS Count120,
		SUM(CASE WHEN COALESCE(AmountLong, 0) > 0 THEN Count ELSE 0 END) AS CountLong,
		SUM(Count) AS CountTotal, 
		 ROW_NUMBER() OVER (ORDER BY COALESCE(CarrierName, 'Resco'))% 2 AS Remainder
	FROM
		--where most of the magic happens
		(SELECT CarrierGroupName AS CarrierName, 
				SUM(COALESCE(ar.Amount,0)-COALESCE(arp.Paid, 0)) as Amount, 
				CASE	WHEN GETDATE() - RescoDueDate <= 0  THEN 'AmountCurrent'
						WHEN GETDATE() - RescoDueDate > 0 AND GETDATE() - RescoDueDate <= 30 THEN 'Amount30'
						WHEN GETDATE() - RescoDueDate > 0 AND GETDATE() - RescoDueDate <= 60 THEN 'Amount60'
						WHEN GETDATE() - RescoDueDate > 0 AND GETDATE() - RescoDueDate <= 90 THEN 'Amount90'
						WHEN GETDATE() - RescoDueDate > 0 AND GETDATE() - RescoDueDate <= 120 THEN 'Amount120'
						WHEN GETDATE() - RescoDueDate > 0 AND GETDATE() - RescoDueDate > 120 THEN 'AmountLong' END AS AmountColumn,
				COUNT(DISTINCT ar.TravelersBillID) AS Count
	FROM tblAccountsReceivable ar
		LEFT JOIN @Payments arp	ON ar.AccountsReceivableID = arp.AccountsReceivableID
		LEFT JOIN tblTravelersBill tb ON ar.TravelersBillID = tb.TravelersBillID
		LEFT JOIN tblPolicy p ON ar.PolicyID = p.PolicyID
		LEFT JOIN tblCarrier c ON p.CarrierID = c.CarrierID
		LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID
	WHERE PaymentDueDate IS NOT NULL AND ar.PayeeTypeID = 2 AND AllocationQCConfirmation = 1 AND BillStatusID <> 2
	GROUP BY CarrierGroupName, 
		CASE	WHEN GETDATE() - RescoDueDate <= 0  THEN 'AmountCurrent'
				WHEN GETDATE() - RescoDueDate > 0 AND GETDATE() - RescoDueDate <= 30 THEN 'Amount30'
				WHEN GETDATE() - RescoDueDate > 0 AND GETDATE() - RescoDueDate <= 60 THEN 'Amount60'
				WHEN GETDATE() - RescoDueDate > 0 AND GETDATE() - RescoDueDate <= 90 THEN 'Amount90'
				WHEN GETDATE() - RescoDueDate > 0 AND GETDATE() - RescoDueDate <= 120 THEN 'Amount120'
				WHEN GETDATE() - RescoDueDate > 0 AND GETDATE() - RescoDueDate > 120 THEN 'AmountLong' END
	HAVING SUM(COALESCE(arp.PAID, 0)) <> SUM(COALESCE(ar.Amount, 0)))

		AS counts

	PIVOT

	(
		SUM(Amount)

	FOR
	AmountColumn

		IN (AmountCurrent, Amount30, Amount60, Amount90, Amount120, AmountLong)

	) AS Pivoted
	GROUP BY CarrierName


	--Calculate totals
	DECLARE @CurrentTotal nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(AmountCurrent)  as MONEY),1) FROM @AgingAccountsReceivable)
	DECLARE @30Total nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Amount30)  as MONEY),1) FROM @AgingAccountsReceivable)
	DECLARE @60Total nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Amount60)  as MONEY),1) FROM @AgingAccountsReceivable)
	DECLARE @90Total nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Amount90)  as MONEY),1) FROM @AgingAccountsReceivable)
	DECLARE @120Total nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(Amount120)  as MONEY),1) FROM @AgingAccountsReceivable)
	DECLARE @LongTotal nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(AmountLong)  as MONEY),1) FROM @AgingAccountsReceivable)
	DECLARE @TotalOverdue nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(AmountOverdue)  as MONEY),1) FROM @AgingAccountsReceivable)
	DECLARE @AllTotal nvarchar(250) = (SELECT CONVERT(varchar(12), CAST( SUM(AmountTotal)  as MONEY),1) FROM @AgingAccountsReceivable)
	
	--Create HTML to return to main email query
	SET @BodyMessage = @BodyMessage + '<P style="font-weight: bold; font-size: medium; padding: 0; margin: 0;">Non-Interim Amounts Owed to Resco</P>'
	SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C;"><tbody>' + 
		N'<tr border-style:none><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); "></th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); "></th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193);" colspan=6> Days Overdue</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); "></th></tr>' +
		N'<tr><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Carrier</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Current</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">0-30</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">30-60</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">60-90</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">90-120</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); "> >120 </th> <th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Total <br/>Overdue</th><th style="font-family: Calibri;  border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; font-size: 9pt; background: rgb(0,121,193); ">Total <br/>Outstanding</th></tr>'  
	
	--Check if there are any rows in the table, otherwise print a message indicating there are no amounts owed to resco.
	IF(SELECT COUNT(*) FROM @AgingAccountsReceivable) = 0
	BEGIN
		SET @BodyMessage = @BodyMessage + '<tr><td style="font-family: Calibri; font-size: 9pt; text-align: center; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;" colspan="9">There are no amounts currently owed to Resco.</td></tr>'
	END
	ELSE
	BEGIN
		DECLARE @Carrier nvarchar(250), @AmountCurrent money, @Amount30 money, @Amount60 money, @Amount90 money, 
				@Amount120 money, @AmountLong money, @AmountOverdue money, @AmountTotal money, @RemainderVal int

		DECLARE AAR_curs CURSOR FOR
		SELECT 	CarrierName, AmountCurrent, Amount30, Amount60, Amount90, Amount120, AmountLong, AmountOverdue, AmountTotal, Remainder				            
		FROM @AgingAccountsReceivable
		
		OPEN AAR_curs
		FETCH NEXT FROM AAR_curs INTO @Carrier, @AmountCurrent, @Amount30, @Amount60, @Amount90, @Amount120, @AmountLong, @AmountOverdue, @AmountTotal, @RemainderVal

		
		--IF @LastCarrierName <> @CarrierName OR @@FETCH_STATUS = 0
		While @@FETCH_STATUS = 0
		BEGIN
				IF @RemainderVal = 0 BEGIN
				SET @BodyMessage = @BodyMessage + '<tr style="background-color: #D6E8FF; ">'
				END
				ELSE BEGIN
				SET @BodyMessage = @BodyMessage + '<tr>'
				END						
				--SET @BodyMessage = @BodyMessage + '<tr>'
				SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: left; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + @Carrier + '</td>'
				SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @AmountCurrent,1)	 + '</td>'
				SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @Amount30,1) + '</td>'
				SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @Amount60,1) + '</td>'
				SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @Amount90,1) + '</td>'
				SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @Amount120,1) + '</td>'
				SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @AmountLong,1) + '</td>'
				SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @AmountOverdue,1) + '</td>'
				SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 9pt; text-align: right; border: 1px solid transparent; border-top: 1px solid rgb(0,121,193); padding: .3em;">' + '$' + CONVERT(varchar(12), @AmountTotal,1) + '</td></tr></tbody>'
											
				
		FETCH NEXT FROM AAR_curs INTO @Carrier, @AmountCurrent, @Amount30, @Amount60, @Amount90, @Amount120, @AmountLong, @AmountOverdue, @AmountTotal, @RemainderVal

		END
		CLOSE AAR_curs
		DEALLOCATE AAR_curs	
				
		--SET @BodyMessage = @BodyMessage + '<tfoot class="Foot">'
		SET @BodyMessage = @BodyMessage + '<tr style="padding: .3em; color: #F6ECF0; border: none;">'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: center; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + 'Totals' + '</td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @CurrentTotal + '</td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @30Total + '</td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @60Total + '</td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @90Total + '</td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @120Total + '</td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @LongTotal + '</td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @TotalOverdue + '</td>'
		SET @BodyMessage = @BodyMessage + '<td style="font-family: Calibri; font-size: 10pt;  text-align: right; border: 1px solid transparent; border-top: 2px solid #FDBB30; padding: .3em; color: #000000; font-weight:bold;">' + '$' + @AllTotal + '</td>'
		SET @BodyMessage = @BodyMessage + '</tr>'							
	END
	
	    			
	SET @BodyMessage = @BodyMessage + N'</table>' +  N'<br>'    								
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyOverduePayments_NonInterimOwedToResco] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyOverduePayments_NonInterimOwedToResco] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyOverduePayments_NonInterimOwedToResco] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyOverduePayments_NonInterimOwedToResco] TO [power_user]
GO
