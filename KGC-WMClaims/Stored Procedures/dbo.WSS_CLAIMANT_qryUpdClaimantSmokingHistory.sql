SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryUpdClaimantSmokingHistory]
(@ClaimantSmokingHistoryID int, @SmokingStart date, @SmokingEnd date, @PacksPerDay float, @SPUsername nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblClaimantSmokingHistory
	SET SmokingStart = @SmokingStart,
		SmokingEnd = @SmokingEnd,
		PacksPerDay = @PacksPerDay,
		SPUsername = @SPUsername
	WHERE ClaimantSmokingHistoryID = @ClaimantSmokingHistoryID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantSmokingHistory] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantSmokingHistory] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantSmokingHistory] TO [power_user]
GO
