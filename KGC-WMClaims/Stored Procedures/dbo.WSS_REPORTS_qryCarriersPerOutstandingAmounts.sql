SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[WSS_REPORTS_qryCarriersPerOutstandingAmounts]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	--DECLARE @Payments TABLE (AccountsReceivableID int, Amount money, Paid money, RescoDueDate date, CarrierBilled nvarchar(500))
	--INSERT INTO @Payments
	--SELECT ar.AccountsReceivableID, ar.Amount, SUM(COALESCE(arp.Amount,0)), RescoDueDate, CarrierName
	--FROM tblAccountsReceivable ar
	--	LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
	--	INNER JOIN tblPolicy p on ar.PolicyID = p.PolicyID
	--	INNER JOIN tblCarrier c on p.CarrierID = c.CarrierID
	--WHERE PayeeTypeID = 2
	--GROUP BY ar.AccountsReceivableID, ar.Amount, RescoDueDate, CarrierName

	--DECLARE @BillsWithIssues TABLE (TravelersBillID int, TotalAmount money, TotalPaid money, TotalDifference money, RescoDueDate date, CarrierBilled nvarchar(500))
	--INSERT INTO @BillsWithIssues
	--SELECT TravelersBillID, SUM(COALESCE(pay.Amount, 0)), SUM(COALESCE(pay.Paid, 0)), SUM(COALESCE(pay.Amount, 0)) - SUM(COALESCE(pay.Paid, 0)), pay.RescoDueDate, CarrierBilled
	--FROM @Payments pay
	--	INNER JOIN tblAccountsReceivable ar ON pay.AccountsReceivableID = ar.AccountsReceivableID
	--GROUP BY TravelersBillID, pay.RescoDueDate, CarrierBilled
	--HAVING SUM(COALESCE(pay.Paid, 0)) <> SUM(COALESCE(pay.Amount, 0))
	--ORDER BY TravelersBillID

	--SELECT DISTINCT	CarrierBilled
	--FROM tblTravelersBill tb
	--	INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
	--	INNER JOIN tblClaimantPersonalInfo cpi ON d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	--	LEFT JOIN tblSuffix s ON cpi.SuffixID = s.SuffixID
	--	INNER JOIN @BillsWithIssues bwi ON tb.TravelersBillID = bwi.TravelersBillID
	--	INNER JOIN tblClaimType ct on tb.ClaimTypeID = ct.ClaimTypeID
	--	INNER JOIN tblFirm f on tb.FirmID = f.FirmID
	
		DECLARE @Payments TABLE (AccountsReceivableID int, Amount money, Paid money, RescoDueDate date, CarrierBilled nvarchar(500))
	INSERT INTO @Payments
	SELECT ar.AccountsReceivableID, ar.Amount, SUM(COALESCE(arp.Amount,0)), RescoDueDate, CarrierName
	FROM tblAccountsReceivable ar
		LEFT JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
		INNER JOIN tblPolicy p on ar.PolicyID = p.PolicyID
		INNER JOIN tblCarrier c on p.CarrierID = c.CarrierID
	WHERE PayeeTypeID = 2
	GROUP BY ar.AccountsReceivableID, ar.Amount, RescoDueDate, CarrierName

	DECLARE @BillsWithIssues TABLE (TravelersBillID int, TotalAmount money, TotalPaid money, TotalDifference money, RescoDueDate date, CarrierBilled nvarchar(500))
	INSERT INTO @BillsWithIssues
	SELECT TravelersBillID, SUM(COALESCE(pay.Amount, 0)), SUM(COALESCE(pay.Paid, 0)), SUM(COALESCE(pay.Amount, 0)) - SUM(COALESCE(pay.Paid, 0)), pay.RescoDueDate, CarrierBilled
	FROM @Payments pay
		INNER JOIN tblAccountsReceivable ar ON pay.AccountsReceivableID = ar.AccountsReceivableID
	GROUP BY TravelersBillID, pay.RescoDueDate, CarrierBilled
	HAVING SUM(COALESCE(pay.Paid, 0)) <> SUM(COALESCE(pay.Amount, 0))
	ORDER BY TravelersBillID

	SELECT Carrierbilled, SUM(TotalAmount) AS 'TotalOutstandingAmount'
	FROM(
	SELECT CarrierBilled, TotalAmount
	FROM (
	SELECT 	d.TravelersBillID, CASE WHEN (SettlementAmount IS NULL OR SettlementAmount = 0) AND InvoiceNumber IS NOT NULL THEN 'Invoice: ' + InvoiceNumber ELSE 'Settlement' END AS BillType,
		CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
		CASE WHEN FirstName IS NULL 
		THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName,
		TotalAmount, RescoDueDate, PaymentDueDate, CarrierBilled, ClaimType, Firm AS Payee
	FROM tblTravelersBill tb
		INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
		INNER JOIN tblClaimantPersonalInfo cpi ON d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblSuffix s ON cpi.SuffixID = s.SuffixID
		INNER JOIN @BillsWithIssues bwi ON tb.TravelersBillID = bwi.TravelersBillID
		INNER JOIN tblClaimType ct on tb.ClaimTypeID = ct.ClaimTypeID
		INNER JOIN tblFirm f on tb.FirmID = f.FirmID) tbl
		GROUP BY CarrierBilled, TotalAmount) tbl2
		GROUP BY CarrierBilled
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryCarriersPerOutstandingAmounts] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryCarriersPerOutstandingAmounts] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryCarriersPerOutstandingAmounts] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryCarriersPerOutstandingAmounts] TO [power_user]
GO
