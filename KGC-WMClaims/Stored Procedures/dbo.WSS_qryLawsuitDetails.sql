SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryLawsuitDetails]
	-- Add the parameters for the stored procedure here
--@LawsuitID int
@ClaimantLawsuitID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--    -- Insert statements for procedure here
--SELECT  DISTINCT   CL.LawsuitID, CL.ClaimantLawsuitID, l.stateid,  S.State, J.Jurisdiction, 
--                   cL.LawsuitStatusID, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, L.DocketNumber, L.CaseCaption, 
--                   L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,
--					JOBSITE, StatusDate, SettlementDate, CLD.Notes, LawsuitStatus, LeadPlaintiffFirstName, LeadPlaintiffLastName, L.JurisdictionID, CL.LawsuitID
--FROM			  dbo.tblClaimantPersonalInfo AS CPI 
--				  INNER JOIN dbo.tblClaimantLawsuit AS CL ON CPI.ClaimantPersonalInfoID = CL.ClaimantPersonalInfoID 
--				  INNER JOIN dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID 
--				  LEFT JOIN dbo.tblState AS S ON S.StateID = L.StateID 
--                  LEFt JOIN dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID  
--                  left join tblClaimLawsuitDisease as ClD on ClD.ClaimantLawsuitID = cl.ClaimantLawsuitID
--				  left join tblDisease ddis on ddis.DiseaseID = ClD.DiseaseID
				
--				  left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID --OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
--				  LEFT JOIN tblClaimantEmployment CE ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID   
--                  LEFT JOIN dbo.tblLawsuitStatus AS ls ON cl.LawsuitStatusID = ls.LawsuitStatusID 
                      
            
--WHERE CL.ClaimantLawsuitID = @ClaimantLawsuitID
----WHERE Cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
----WHERE L.LawsuitID = @LawsuitID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryLawsuitDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryLawsuitDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryLawsuitDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryLawsuitDetails] TO [power_user]
GO
