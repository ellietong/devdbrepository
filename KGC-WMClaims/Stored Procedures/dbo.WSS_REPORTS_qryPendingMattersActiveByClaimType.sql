SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryPendingMattersActiveByClaimType] 
	(@StartDate datetime,
	 @EndDate datetime)	
AS
BEGIN
	SET NOCOUNT ON;
	--Test Variables
	--DECLARE @StartDate datetime = '1/1/1900'
	--DECLARE @EndDate datetime = GETDATE()
	
	--SELECT
	--	ClaimType, 
	--	COUNT(distinct(cl.ClaimantLawsuitID)) As Count
	--FROM tblClaimantPersonalInfo cpi
	--	LEFT JOIN tblClaimantLawsuit cl	ON cpi.ClaimantPersonalInfoID=cl.ClaimantPersonalInfoID
	--	LEFT JOIN tblLawsuit l				ON cl.LawsuitID=l.LawsuitID
	--	LEFT JOIN tblLawsuitStatus ls		ON cl.LawsuitStatusID=ls.LawsuitStatusID
	--	LEFT JOIN tblState s				ON l.StateID=s.StateID
	--	LEFT JOIN tblClaimType ct			ON ct.ClaimTypeID = cpi.ClaimTypeID
	--WHERE COALESCE(filedate, servicedate, statusdate, '1/1/1900') <= @EndDate
	--	AND		(ls.LawsuitStatusID = 6 
	--				OR (ls.LawsuitStatusID not in (6, 11) and statusdate >= @EndDate))
	--GROUP BY ClaimType
	--ORDER BY ClaimType

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryPendingMattersActiveByClaimType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryPendingMattersActiveByClaimType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryPendingMattersActiveByClaimType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryPendingMattersActiveByClaimType] TO [power_user]
GO
