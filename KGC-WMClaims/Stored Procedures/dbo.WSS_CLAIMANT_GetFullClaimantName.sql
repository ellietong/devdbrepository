SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_GetFullClaimantName]
	-- Add the parameters for the stored procedure here
	@ClaimantID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + '' END + CASE WHEN MiddleName IS NULL THEN '' ELSE + ' ' + MiddleName END + ' ' + LastName AS FullClaimantName
	FROM tblClaimantPersonalInfo
	WHERE ClaimantPersonalInfoID = @ClaimantID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_GetFullClaimantName] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_GetFullClaimantName] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_GetFullClaimantName] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_GetFullClaimantName] TO [power_user]
GO
