SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryClaimantsMissingTravelersDocs] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @TravelersComparison TABLE (ClaimantPersonalInfoID bigint, ConfirmationStatusID int)
	DECLARE @MaxConfirmationStatusID int = (SELECT MAX(ConfirmationStatusID) FROM tblConfirmationStatus)
	DECLARE @MaxClaimTypeID int = (SELECT MAX(MatterID) FROM tblMatter)
	
	INSERT INTO @TravelersComparison	
	SELECT 
		cpi.ClaimantPersonalInfoID, 
		CASE WHEN SUM(CASE WHEN COALESCE(d.TravelersBillID, 0)=0  THEN 1 ELSE 0 END) > 0 THEN 2 ELSE 0 END --No travelers docs
	FROM tblClaimantPersonalInfo cpi 
		LEFT JOIN (SELECT * FROM tblDocuments WHERE ClaimantDocumentTypeID IN (48, 49))d	ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
		LEFT JOIN (SELECT * FROM tblTravelersBill WHERE BillStatusID = 1) tb	ON d.TravelersBillID = tb.TravelersBillID--only current bills
	GROUP BY cpi.ClaimantPersonalInfoID

	SELECT DISTINCT 
		CPI.ClaimantPersonalInfoID AS ClaimantPersonalInfoID, 
		COUNT(DISTINCT(CPI.ClaimantPersonalInfoID)) As Count, 
		CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +CASE WHEN FirstName IS NULL 
		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, 
		CASE WHEN Matter IS NULL THEN '' ELSE Matter END AS Matter, 
		Cl.DOFE, 
		SUM(COALESCE(FinalTotal, 0) + COALESCE(cset.SettlementAmount, 0)) as TotalCosts, 
		Notes, 
		ConfirmationStatus,
		FirstName, 
		MiddleName, 
		LastName, 
		M.MatterID, 
		Gender, 
		BirthDate, 
		DeceasedDate, 
		SocialSecurityNumber, 
		CPI.SuffixID AS SuffixID, 
		Suffix
	FROM dbo.tblClaimantPersonalInfo cpi
		INNER JOIN tblClaim C on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblMatter M		ON C.MatterID = M.MatterID
		LEFT JOIN tblDocuments d			ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
		LEFT JOIN tblTravelersBill tb		ON d.TravelersBillID = tb.TravelersBillID
		LEFT JOIN @TravelersComparison tc	ON cpi.ClaimantPersonalInfoID = tc.ClaimantPersonalInfoID
		LEFT JOIN tblGender g				ON CPI.GenderID = G.GenderID
		INNER JOIN tblConfirmationStatus cs ON tc.ConfirmationStatusID = cs.ConfirmationStatusID
		LEFT JOIN tblSuffix su				ON CPI.SuffixID = Su.SuffixID
		LEFT JOIN tblClaimLawsuit cl ON C.ClaimID = cl.ClaimID
		LEFT JOIN tblClaimantSettlement cset ON cl.ClaimLawsuitID = cset.ClaimLawsuitID
	GROUP BY 
		CPI.ClaimantPersonalInfoID,  
		CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  + CASE WHEN FirstName IS NULL 
			  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END, 
		CASE WHEN Matter IS NULL THEN '' ELSE Matter END, 
		Cl.DOFE, 
		Notes, 
		ConfirmationStatus, 
		FirstName, 
		MiddleName, 
		LastName, 
		M.MatterID, 
		Gender, 
		BirthDate, 
		DeceasedDate, 
		SocialSecurityNumber, 
		CPI.SuffixID, 
		Suffix 
	ORDER BY ClaimantName

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryClaimantsMissingTravelersDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryClaimantsMissingTravelersDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryClaimantsMissingTravelersDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryClaimantsMissingTravelersDocs] TO [power_user]
GO
