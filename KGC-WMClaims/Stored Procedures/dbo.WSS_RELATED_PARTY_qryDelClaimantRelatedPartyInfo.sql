SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RELATED_PARTY_qryDelClaimantRelatedPartyInfo]
(@ClaimantRelatedPartyID int, @Emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--UPDATE tblClaimantRelatedParty
	--SET SPUserName = @emailaddress
	--WHERE ClaimantRelatedPartyID = @ClaimantRelatedPartyID
    -- Insert statements for procedure here
    UPDATE tblClaimantRelatedParty
    SET SPUsername = @Emailaddress
    WHERE ClaimantRelatedPartyID = @ClaimantRelatedPartyID
    
	DELETE FROM tblClaimantRelatedParty
	WHERE ClaimantRelatedPartyID = @ClaimantRelatedPartyID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryDelClaimantRelatedPartyInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryDelClaimantRelatedPartyInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryDelClaimantRelatedPartyInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryDelClaimantRelatedPartyInfo] TO [power_user]
GO
