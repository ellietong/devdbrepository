SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_MATTER_qryRescoMatterList] (@emailaddress nvarchar(250) = NULL)
AS
SET NOCOUNT ON

IF @emailaddress IS NULL
BEGIN 
	SELECT MatterID, 
		Matter 
	FROM tblMatter
	WHERE MatterID IN(1,2,4,5,8)
	UNION
	SELECT NULL, ''
	ORDER BY Matter
END
ELSE
BEGIN
	IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	BEGIN
			SELECT MatterID, 
				Matter 
			FROM tblMatter
			WHERE MatterID IN(1,2,4,5,8)
			UNION
			SELECT NULL, ''
			ORDER BY Matter
	END 
	ELSE
	BEGIN
		DECLARE @UserPersonID int = (SELECT PersonID FROM vtblPerson WHERE email = @emailaddress)

		SELECT MatterID, 
			Matter 
		FROM tblMatter
		WHERE MatterID IN(SELECT mitn.MatterID FROM tblMatterInsurersToNotify mitn WHERE PersonID = @UserPersonID)
		UNION
		SELECT NULL, ''
		ORDER BY Matter
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_MATTER_qryRescoMatterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_MATTER_qryRescoMatterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_MATTER_qryRescoMatterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_MATTER_qryRescoMatterList] TO [power_user]
GO
