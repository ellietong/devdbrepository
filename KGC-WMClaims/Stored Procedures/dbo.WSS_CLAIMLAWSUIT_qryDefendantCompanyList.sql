SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_CLAIMLAWSUIT_qryDefendantCompanyList]
(@MatterID int = null)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--DECLARE @MaxDefendantCompanyID int = (SELECT MAX(DefendantCompanyID) FROM tblDefendantCompany)
	DECLARE @MaxMatterID int = (SELECT MAX(MatterID) FROM tblMatter)

    SELECT DefendantCompanyID, DefendantCompany + ' (' + Matter + ')' AS DefendantCompany, dc.MatterID, Matter
    FROM tblDefendantCompany dc
    INNER JOIN tblMatter m ON dc.MatterID = m.MatterID
    WHERE dc.MatterID >= COALESCE(@MatterID, 0) AND dc.MatterID <= COALESCE(@MatterID, @MaxMatterID)
	UNION
	SELECT NULL, NULL, NULL, NULL
	ORDER BY Matter, DefendantCompany
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMLAWSUIT_qryDefendantCompanyList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMLAWSUIT_qryDefendantCompanyList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMLAWSUIT_qryDefendantCompanyList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMLAWSUIT_qryDefendantCompanyList] TO [power_user]
GO
