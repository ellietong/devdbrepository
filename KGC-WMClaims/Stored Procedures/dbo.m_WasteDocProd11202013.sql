SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[m_WasteDocProd11202013]
AS

SELECT DISTINCT d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/wm/Pages/Insurer-Complaint-Notification.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		ServiceDate,
		c.MatterID,
		[State], DateCreated,
		DocumentTitle,
		AmazonKey
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument    mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo     cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		INNER JOIN tblComplaintTrackingLawsuit ctl ON d.ComplaintID = ctl.ComplaintTrackingID
		INNER JOIN tblClaim                    c   ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit             cl  ON cl.ClaimID = c.ClaimID AND ctl.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblAllegation               a   ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblDefendantCompany         dc  ON dc.DefendantCompanyID = a.DefendantCompanyID
		INNER JOIN tbllawsuit                  l   ON l.LawsuitID = cl.LawsuitID
		INNER JOIN tblState                    s   ON s.StateID = l.StateID
	WHERE d.ClaimantDocumentTypeID IN (8)
		AND c.MatterID =8
		and d.datecreated <= '11/16/2013'
	GROUP BY d.documentID,
		d.DocumentTitle,
		LastName,
		FirstName,
		cpi.ClaimantPersonalInfoID,
		ServiceDate,
		c.MatterID,
		[State], datecreated,
		DocumentTitle,
		AmazonKey

 union
	select DISTINCT d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/wm/Pages/Insurer-Complaint-Notification.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		ServiceDate,
		c.MatterID,
		[State], DateCreated,
		DocumentTitle,
		AmazonKey
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument    mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo     cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		INNER JOIN tblClaim                    c   ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID and c.matterid = d.matterid
		INNER JOIN tblClaimLawsuit             cl  ON cl.ClaimID = c.ClaimID 
		INNER JOIN tblAllegation               a   ON cl.ClaimLawsuitID = a.ClaimLawsuitID and coalesce(a.DefendantCompanyID, '') = coalesce(d.DefendantCompanyID,'')
		INNER JOIN tblDefendantCompany         dc  ON dc.DefendantCompanyID = a.DefendantCompanyID
		INNER JOIN tbllawsuit                  l   ON l.LawsuitID = cl.LawsuitID
		INNER JOIN tblState                    s   ON s.StateID = l.StateID
WHERE d.ClaimantDocumentTypeID IN (8)
		AND c.MatterID =8
		and datecreated > '6/7/2013'
		and d.datecreated <= '11/16/2013'
	GROUP BY d.documentID,
		d.DocumentTitle,
		LastName,
		FirstName,
		cpi.ClaimantPersonalInfoID,
		ServiceDate,
		c.MatterID,
		[State], datecreated,
		DocumentTitle,
		AmazonKey
	ORDER BY ServiceDate
GO
GRANT VIEW DEFINITION ON  [dbo].[m_WasteDocProd11202013] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_WasteDocProd11202013] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_WasteDocProd11202013] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_WasteDocProd11202013] TO [power_user]
GO
