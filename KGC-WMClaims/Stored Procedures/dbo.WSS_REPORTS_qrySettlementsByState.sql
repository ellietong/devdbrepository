SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_qrySettlementsByState] 
	-- Add the parameters for the stored procedure here
	(@StartDate datetime, @EndDate datetime)  
AS
BEGIN
	SET NOCOUNT ON;
	--Test Variables
	--DECLARE @StartDate datetime = '1/1/1900'
	--DECLARE @EndDate datetime = GETDATE()

	--SELECT 
	--	COALESCE(FullNameState, 'Unknown') AS FullNameState, 
	--	COUNT(DISTINCT(cl.ClaimantLawsuitID)) as Count, 
	--	SUM(COALESCE(SettlementAmount, 0)) as IndemnityPaid, 
	--	SUM(COALESCE(SettlementAmount, 0))/COUNT(DISTINCT(cl.ClaimantLawsuitID)) as AverageIndemnity
	--FROM tblClaimantPersonalInfo cpi
	--	INNER JOIN tblClaimantLawsuit cl	ON cpi.ClaimantPersonalInfoID=cl.ClaimantPersonalInfoID
	--	INNER JOIN tblLawsuit l				ON cl.LawsuitID=l.LawsuitID
	--	LEFT JOIN tblLawsuitStatus clst	ON cl.LawsuitStatusID=clst.LawsuitStatusID
	--	LEFT JOIN tblState s				ON l.StateID=s.StateID
	--	LEFT JOIN tblClaimType ct			ON ct.ClaimTypeID = cpi.ClaimTypeID
	--WHERE	COALESCE(Statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
	--		AND LawsuitStatusGroupID = 1
	--GROUP BY FullNameState
	--ORDER BY COUNT(DISTINCT(cl.ClaimantLawsuitID)) DESC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementsByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementsByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qrySettlementsByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qrySettlementsByState] TO [power_user]
GO
