SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryDismissalTypeList]
AS
SET NOCOUNT ON

SELECT ClaimLawsuitStatusID, 
	ClaimLawsuitStatus 
FROM tblClaimLawsuitStatus 
WHERE ClaimLawsuitStatusID IN (15, 16, 27, 42)
UNION
SELECT NULL, '-- Dismissal Type --'
ORDER BY ClaimLawsuitStatus





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDismissalTypeList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDismissalTypeList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDismissalTypeList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDismissalTypeList] TO [power_user]
GO
