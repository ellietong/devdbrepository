SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyInsurerNotification_Swindell]
(@BodyMessage nvarchar(MAX) output)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT
--BEGIN	


set @BodyMessage = @BodyMessage + '
		<p style="font-family: Calibri; font-size: 10pt; text-align: left; ">
			RESCO INSURERS<br/>
			(See Attached List)<br/>
			
		</p>
		<p style="font-family: Calibri; font-size: 10pt; font-weight: bold; text-align: left; border: 1px solid transparent; " colspan="9">	
			<u>NOTIFICATION OF CLAIM</u><br/>
			
		<p/>
		<p style="font-family: Calibri; font-size: 10pt; font-weight: bold; text-align: left; border: 1px solid transparent; " colspan="9">
			RE: Notice of Receipt of Asbestos-Related Swindell Claims - See Table Below
		<p/>	
<p style="font-family: Calibri; font-size: 10pt; text-align: left; border: 1px solid transparent; " colspan="9">			
	Dear Participating Insurer:<br/>
	<br>
		Pursuant to Schedule N, paragraph D of the Coverage in Place Agreement executed by Resco Holdings<br/> 
	LLC (“Resco”), Travelers Indemnity, Travelers Casualty, Continental Insurance, Insurance Company<br/> 
	of North America, ACE American Insurance Company, Century Indemnity, National Union, TIG Insurance,<br/> 
	and the London Market Insurers, with an Effective Date of October 13, 2011, below please find the<br/> 
	Asbestos-Related Swindell Claim(s) asserted against Resco.
	<br/>
										Sincerely,<br/>
											<br/>
											<br/>
										Ashley A.  Harper<br/>
										<br/>
											
 </p>'	
--END	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Swindell] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Swindell] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Swindell] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_Swindell] TO [power_user]
GO
