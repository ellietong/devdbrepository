SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantDocuments] 
	-- Add the parameters for the stored procedure here
	@ClaimantPersonalInfoID bigint
AS
--BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	----SET NOCOUNT ON;
--	--DECLARE @ClaimantPersonalInfoID bigint = 9
--    -- Insert statements for procedure here
--	SELECT  DISTINCT   DocumentID, DocumentTitle, DocumentTypeID, ClaimantDocumentType, Comments AS Notes, DocumentURL
--	FROM tblDocuments D          
--		 LEFT JOIN tblClaimantPersonalInfo AS CPI ON CPI.ClaimantPersonalInfoID = D.ClaimantPersonalInfoID
--		 INNER JOIN tblDocumentType AS DT ON DT.ClaimantDocumentTypeID = D.DocumentTypeID  
--	WHERE D.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND D.DocumentTypeID NOT IN (3,4)
--	AND d.DocumentID NOT IN (SELECT DISTINCT COALESCE(InvoiceDocumentID, 0) FROM tblTravelersBill) --exclude bills & invoices
--	UNION
--	SELECT d1.DocumentID, d1.DocumentTitle, d1.DocumentTypeID, dt.ClaimantDocumentType + coalesce(' (' + tb.PaymentType + ')',''), d1.Comments AS Notes, d1.DocumentURL
--	FROM tblDocuments d
--	inner join tblTravelersBill tb on tb.TravelersBillID = d.TravelersBillID
--	INNER JOIN tblAccountsReceivable ar ON tb.TravelersBillID =ar.TravelersBillID
--	INNER JOIN tblAccountsReceivablePayment arp ON ar.AccountsReceivableID = arp.AccountsReceivableID
--	INNER JOIN tblPayment p ON arp.PaymentID = p.PaymentID
--	INNER JOIN tblDocuments d1 ON p.DocumentID = d1.DocumentID
--	INNER JOIN tblDocumentType AS DT ON DT.ClaimantDocumentTypeID = D1.DocumentTypeID
--	WHERE d.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
--	GROUP BY  d1.DocumentID, d1.DocumentTitle, d1.DocumentTypeID, ClaimantDocumentType, d1.Comments, d1.DocumentURL, tb.PaymentType
--END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantDocuments] TO [power_user]
GO
