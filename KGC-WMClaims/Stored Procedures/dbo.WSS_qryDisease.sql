SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDisease]
AS
BEGIN

	SELECT DiseaseID, Disease + ' (ICD9: ' + COALESCE(CONVERT(nvarchar(10),ICD9),'N/A') + '), ' + HighLevelDisease AS Disease, '1' as orderflag
	FROM tblDisease dn
	LEFT JOIN tblHighLevelDisease hld ON dn.HighLevelDiseaseID = hld.HighLevelDiseaseID --WHERE ICD9 IS NOT Null
	UNION
	SELECT dn.DiseaseID_Alt AS DiseaseID, Disease + ' (ICD9: ' + COALESCE(CONVERT(nvarchar(10),ICD9),'N/A') + '), ' + HighLevelDisease AS Disease, '2' as orderflag
	FROM tblDisease_ALT dn
	LEFT JOIN tblHighLevelDisease hld ON dn.HighLevelDiseaseID = hld.HighLevelDiseaseID --WHERE ICD9 IS NULL --or dn.diseaseid_alt = 20
	UNION
	SELECT NULL, '', 0
	ORDER BY orderflag, disease
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDisease] TO [power_user]
GO
