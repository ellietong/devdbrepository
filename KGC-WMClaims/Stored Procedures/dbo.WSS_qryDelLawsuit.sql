SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryDelLawsuit]
	-- Add the parameters for the stored procedure here
(@ClaimantLawsuitID int, @Emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE tblClaimLawsuit
SET SPUsername = @Emailaddress
WHERE ClaimLawsuitID = @ClaimantLawsuitID

UPDATE tblClaimLawsuitStatusTracking
SET SPUsername = @Emailaddress
WHERE ClaimLawsuitID = @ClaimantLawsuitID

DELETE FROM tblClaimLawsuitStatusTracking WHERE ClaimLawsuitID = @ClaimantLawsuitID

DELETE FROM tblClaimLawsuit WHERE ClaimLawsuitID = @ClaimantLawsuitID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelLawsuit] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelLawsuit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryDelLawsuit] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryDelLawsuit] TO [power_user]
GO
