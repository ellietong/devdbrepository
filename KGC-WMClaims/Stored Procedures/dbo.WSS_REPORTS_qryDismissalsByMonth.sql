SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_REPORTS_qryDismissalsByMonth] 
	(@StartDate datetime,
	 @EndDate datetime) 
AS
BEGIN
	SET NOCOUNT ON;
	--Test Variables
	--DECLARE @StartDate datetime = '1/1/1900'
	--DECLARE @EndDate datetime = GETDATE()

	--SELECT 
	--	COALESCE(DATENAME("mm", statusdate) + ' ' + CONVERT(nvarchar, YEAR(statusdate)), 'Unknown') AS [MonthName],
	--	COUNT(distinct(cl.ClaimantLawsuitID)) AS [Count], 
	--	MONTH(statusdate) As MonthID
	--FROM tblClaimantLawsuit cl
	--	LEFT JOIN tblLawsuit l				ON cl.LawsuitID=l.LawsuitID
	--	LEFT JOIN tblLawsuitStatus clst	ON cl.LawsuitStatusID=clst.LawsuitStatusID
	--	LEFT JOIN tblState s				ON l.StateID=s.StateID
	--WHERE COALESCE(statusdate, statusdate, '1/1/1900') BETWEEN @StartDate AND @EndDate
	--	AND LawsuitStatusGroupID =4
	--GROUP BY DATENAME("mm", statusdate) + ' ' + CONVERT(nvarchar, YEAR(statusdate)), MONTH(statusdate), YEAR(statusdate)
	--ORDER BY YEAR(statusdate), MONTH(statusdate)
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDismissalsByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDismissalsByMonth] TO [power_user]
GO
