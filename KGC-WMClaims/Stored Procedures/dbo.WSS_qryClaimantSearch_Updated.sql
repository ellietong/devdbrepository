SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryClaimantSearch_Updated] 
	-- Add the parameters for the stored procedure here
	(@ClaimantName nvarchar(max), @MatterID int, @ConfirmationStatusID int, @FirmID int) --@ConfirmationStatusID int
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--DECLARE @ClaimantName nvarchar(max) = ''
	--DECLARE @MatterID int = null
	--DECLARE @ConfirmationStatusID int = null 
	--DECLARE @FirmID int = null
	
	
DECLARE @ColumnListComma nvarchar(MAX)
DECLARE @ColumnListPlus nvarchar(MAX)



SET @ColumnListComma = (
SELECT LEFT(FirmList, LEN(FirmList)-1) FROM (
SELECT  DISTINCT
            '[' + REPLACE(DefenseCounsel, '&', 'AND') + ']' + ', ' AS [text()] 
        FROM 
            tblDefenseCounselAddress
        FOR XML PATH('')  ) fl (FirmList))
        
SET @ColumnListPlus = (
SELECT LEFT(FirmList, LEN(FirmList)-1) FROM (
SELECT DISTINCT
            'CASE WHEN COALESCE([' + REPLACE(DefenseCounsel, '&', 'AND') + '], 0) = 1 THEN ''' + REPLACE(DefenseCounsel, '''', '"') + ', '' ELSE '''' END + ' AS [text()] 
        FROM 
            tblDefenseCounselAddress
        FOR XML PATH('')  ) fl (FirmList))
        
        
DECLARE @Command nvarchar(MAX)

SET @Command = '
SELECT ClaimantPersonalInfoID, ' + @ColumnListPlus + ' FROM
(SELECT DISTINCT ClaimantPersonalInfoID, REPLACE(d.DefenseCounsel, ''&'', ''AND'') AS DefenseCounsel , COUNT(DISTINCT l.DefenseCounselAddressID) AS FirmCount FROM tblClaimLawsuit cl
LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
LEFT JOIN tblClaim c on cl.claimid = c.claimid
--LEFT JOIN tblclaimantpersonalinfo cpi on c.claimantpersonalinfoid = cpi.claimantpersonalinfoid
INNER JOIN (SELECT * FROM tblDefenseCounselAddress) d ON l.DefenseCounselAddressID = d.DefenseCounselAddressID
GROUP BY ClaimantPersonalInfoID, DefenseCounsel
--HAVING COUNT(DISTINCT l.DefenseCounselAddressID) > 0
)
    AS dlist PIVOT

(

    Sum(FirmCount)
    
    
FOR

DefenseCounsel
IN (' + @ColumnListComma + ')

) AS piv
'

	
	DECLARE @TravelersComparison TABLE (ClaimantPersonalInfoID bigint, ConfirmationStatusID int)
	DECLARE @MaxConfirmationStatusID int = (SELECT MAX(ConfirmationStatusID) FROM tblConfirmationStatus)
	DECLARE @MaxMatterID int = (SELECT MAX(MatterID) FROM tblMatter)
	DECLARE @MaxFirmID int = (SELECT MAX(DefenseCounselAddressID) FROM tblDefenseCounselAddress)

	
	INSERT INTO @TravelersComparison	
SELECT cpi.ClaimantPersonalInfoID, CASE WHEN SUM(CASE WHEN COALESCE(d.TravelersBillID, 0)=0  THEN 1 ELSE 0 END) > 0 THEN 2 --No travelers docs
		WHEN SUM(CASE WHEN tb.DOFE <> cl.DOFE THEN 1 ELSE 0 END) > 0 THEN 5 --DOFE mismatch
		WHEN SUM(CASE WHEN tb.MatterID <> c.MatterID THEN 1 ELSE 0 END) > 0 THEN 4 ELSE 1 END --Claim Type Mismatch
	FROM tblClaimantPersonalInfo cpi 
	LEFT JOIN dbo.tblClaim c				 ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl			 ON c.ClaimID = cl.ClaimID
	LEFT JOIN (SELECT * FROM tblDocuments WHERE ClaimantDocumentTypeID IN (17, 18))d ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
	LEFT JOIN (SELECT * FROM tblTravelersBill WHERE BillStatusID = 1) tb ON d.TravelersBillID = tb.TravelersBillID--only current bills
GROUP BY cpi.ClaimantPersonalInfoID

DECLARE @DefenseCounsels TABLE (ClaimantPersonalInfoID bigint, Firms nvarchar(MAX))
INSERT INTO @DefenseCounsels
EXEC (@Command)

	SELECT DISTINCT CPI.ClaimantPersonalInfoID AS ClaimantPersonalInfoID,  CPI.LastName  + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE ' ' + Suffix END + ', '  +
CASE WHEN FirstName IS NULL 
		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, 
			  CASE WHEN Matter IS NULL THEN '' ELSE Matter END AS Matter, cl.DOFE, SUM(COALESCE(FinalTotal, 0) + COALESCE(tb.SettlementAmount, 0)) as TotalCosts, Notes, 
			  ConfirmationStatus, FirstName, MiddleName, LastName, c.MatterID, Gender, BirthDate, DeceasedDate, SocialSecurityNumber, CPI.SuffixID AS SuffixID, Suffix, DofeSourcePageNum, DofeSource, 
			   CASE WHEN DofeSource = 1 THEN 'Silent' WHEN DofeSource <> 1 THEN Do.DocumentTitle END AS DOFESourceTitle, Do.DocumentURL AS DOFESourceURL,
			   LEFT(COALESCE(Firms, 'Unassigned.'), LEN(COALESCE(Firms, 'Unassigned.'))-1) AS Firms, '' AS CTSourceURL
	FROM dbo.tblClaimantPersonalInfo AS CPI
	/*INNER*/LEFT JOIN dbo.tblClaim c				 ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl			 ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l					 ON cl.LawsuitID = l.LawsuitID 
		LEFT JOIN tblMatter m					 ON c.MatterID = m.MatterID
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		LEFT JOIN tblState s					 ON l.StateID = s.StateID
		LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls		 ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	--LEFT JOIN dbo.tblMatter AS C ON CPI.ClaimTypeID = C.ClaimTypeID
	LEFT JOIN tblMultiClaimantDocument mcd ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblDocuments d on mcd.DocumentID = d.DocumentID
	LEFT JOIN tblTravelersBill tb ON d.TravelersBillID = tb.TravelersBillID
	LEFT JOIN (SELECT * FROM @TravelersComparison WHERE coalesce(ConfirmationStatusID, 0) >= COALESCE(@ConfirmationStatusID, 0) AND coalesce(ConfirmationStatusID,0) <= COALESCE(@ConfirmationStatusID, @MaxConfirmationStatusID)) tc ON cpi.ClaimantPersonalInfoID = tc.ClaimantPersonalInfoID
	LEFT JOIN tblGender G ON CPI.GenderID = G.GenderID
	/*INNER*/ LEFT JOIN tblConfirmationStatus cs ON tc.ConfirmationStatusID = cs.ConfirmationStatusID
	LEFT JOIN tblSuffix Su ON CPI.SuffixID = Su.SuffixID
	--LEFT JOIN tblDocuments Doc ON CPI.ClaimTypeSource = Doc.DocumentID
	LEFT JOIN tblDocuments Do ON cl.DOFESource = Do.DocumentID
	LEFT JOIN @DefenseCounsels dc ON cpi.ClaimantPersonalInfoID = dc.ClaimantPersonalInfoID
	--LEFT JOIN tblClaimantLawsuit cl ON CPI.ClaimantPersonalInfoID = cl.ClaimantPersonalInfoID
	--LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	WHERE ((CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' end) like '%'+coalesce(@ClaimantName, '')+'%')
			AND  coalesce(c.MatterID, 0) >= COALESCE(@MatterID, 0) AND coalesce(c.MAtterID, 0) <= COALESCE(@MatterID, @MaxMatterID)
			AND COALESCE(l.DefenseCounselAddressID, 0) >= COALESCE(@FirmID, 0) AND COALESCE(l.DefenseCounselAddressID, 0) <= COALESCE(@FirmID, @MaxFirmID)
			
	GROUP BY CPI.ClaimantPersonalInfoID,  CPI.LastName + ' ' + CASE WHEN CPI.SuffixID IS NULL THEN '' ELSE Suffix END + ', '  +
CASE WHEN FirstName IS NULL 
		  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END, 
			  CASE WHEN Matter IS NULL THEN '' ELSE Matter END, cl.DOFE, Notes, 
			   ConfirmationStatus, FirstName, MiddleName, LastName, c.MatterID, Gender, BirthDate, DeceasedDate, SocialSecurityNumber, 
			   CPI.SuffixID, Suffix, DofeSourcePageNum, DofeSource, Do.DocumentURL, Do.DocumentTitle, Firms
			   
	ORDER BY ClaimantName
END


--select * from tblClaim
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantSearch_Updated] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantSearch_Updated] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryClaimantSearch_Updated] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryClaimantSearch_Updated] TO [power_user]
GO
