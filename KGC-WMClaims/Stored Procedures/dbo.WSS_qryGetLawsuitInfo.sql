SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetLawsuitInfo] 
	-- Add the parameters for the stored procedure here
	@LawsuitID bigint = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT DocketNumber, CaseCaption, Jurisdiction, State, ServiceDate, FileDate
FROM tblLawsuit L
LEFT JOIN tblState S ON L.StateID = S.StateID
LEFT JOIN tblJurisdiction J ON L.JurisdictionID = J.JurisdictionID
WHERE LawsuitID = @LawsuitID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetLawsuitInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetLawsuitInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetLawsuitInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetLawsuitInfo] TO [power_user]
GO
