SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] (@DocumentID bigint, 
															@DocCategoryID int, 
															@PageNums nvarchar(20), 
															@SetPymtID bigint = NULL)
AS
SET NOCOUNT ON

----DECLARE @ClaimantPersonalInfoID int
----DECLARE @DocumentID int
----DECLARE @DocCategoryID int
----DECLARE @PageNums varchar
----SET @ClaimantPersonalInfoID = 91864
----SET @DocumentID             = 973
----SET @DocCategoryID          = 4 
----SET @PageNums               = '1'


DECLARE @ClaimantPersonalInfoID int
DECLARE @MatterID int

--/**Declare cursor to attach documentation to each claimant the document is attached to**/
DECLARE Cur_Claimants CURSOR Local Fast_Forward FOR
	SELECT mcd.ClaimantpersonalinfoID, MatterID 
	FROM tblmulticlaimantdocument mcd
		inner join tbldocuments d on d.documentid =  mcd.documentid
	WHERE mcd.documentid = @DocumentID

OPEN Cur_Claimants
FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID, @MatterID

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @ClaimLawsuitID int
	SET @ClaimLawsuitID = (SELECT ClaimlawsuitID 
						   FROM tblClaimantPersonalInfo cpi 
						       INNER JOIN tblClaim c		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID 
						       INNER JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID 
						   WHERE c.IsCurrentClaim = 1 
						       AND cl.IsPrimaryForClaim = 1 
						       AND cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
						       AND c.MatterID = @MatterID)

	DECLARE @NewSettlePaymentDocumentationID int

	--if there is no record for this claim in tblSettlePaymentDocumentation then create one
	IF (SELECT COUNT(SetPymtID) FROM tblSettlePaymentDocumentation WHERE ClaimLawsuitID = @ClaimLawsuitID) = 0 
	BEGIN
		INSERT INTO tblSettlePaymentDocumentation (ClaimLawsuitID)
			SELECT @ClaimLawsuitID
		SET @NewSettlePaymentDocumentationID = (SELECT MAX(SetPymtID) 
												FROM tblSettlePaymentDocumentation)
	END
	ELSE
	BEGIN
		SET @NewSettlePaymentDocumentationID = (SELECT SetPymtID 
												FROM tblSettlePaymentDocumentation 
												WHERE ClaimLawsuitID = @ClaimLawsuitID)
	END

	--insert settlementdocumentation recordusing settlement payment information ID
	INSERT INTO tblSettleDocs (DocumentID, DocCategoryID, PageNums, SetPymtID)
		SELECT @DocumentID, @DocCategoryID, @PageNums, @NewSettlePaymentDocumentationID
		
	--update the settlementdocumentation record to include this document
	-- 1 Claimant Identified
	-- 2 Diagnosing Medicals
	-- 3 Death Certificate
	-- 4 Estate Paperwork
	-- 5 Product Identification
	-- 6 Emloyment/Exposure History
	-- 7 MMSEA Settlement Form (Not in IUNA)
	-- 8 Medicare Plaintiff
	-- 9 Medicare Plaintiff Counsel
	--10 Settlement Confirmation Letter
	IF @DocCategoryID = 1 BEGIN
		UPDATE tblSettlePaymentDocumentation 
			SET ClaimantIDed = 1 
			WHERE SetPymtID = @NewSettlePaymentDocumentationID END
	IF @DocCategoryID = 2 BEGIN
		UPDATE tblSettlePaymentDocumentation 
			SET DxMedicals = 1 
			WHERE SetPymtID = @NewSettlePaymentDocumentationID END
	IF @DocCategoryID = 3 BEGIN
		UPDATE tblSettlePaymentDocumentation 
			SET DODCert = 1 
			WHERE SetPymtID = @NewSettlePaymentDocumentationID END
	IF @DocCategoryID = 4 BEGIN
		UPDATE tblSettlePaymentDocumentation 
			SET EstatePapers = 1 
			WHERE SetPymtID = @NewSettlePaymentDocumentationID END
	IF @DocCategoryID = 5 BEGIN
		UPDATE tblSettlePaymentDocumentation 
			SET ProductID = 1 
			WHERE SetPymtID = @NewSettlePaymentDocumentationID END
	IF @DocCategoryID = 6 BEGIN
		UPDATE tblSettlePaymentDocumentation 
			SET EmployExpoHist = 1 
			WHERE SetPymtID = @NewSettlePaymentDocumentationID END
	--IF @DocCategoryID = 7 BEGIN
	--	UPDATE tblSettlePaymentDocumentation 
	--		SET MMSEA = 1 
	--		WHERE SetPymtID = @NewSettlePaymentDocumentationID END
	--IF @DocCategoryID = 8 BEGIN
	--	UPDATE tblSettlePaymentDocumentation 
	--		SET MedicarePlaintiff = 1 
	--		WHERE SetPymtID = @NewSettlePaymentDocumentationID END
	--IF @DocCategoryID = 9 BEGIN
	--	UPDATE tblSettlePaymentDocumentation 
	--		SET MedicarePlaintiffCounsel = 1 
	--		WHERE SetPymtID = @NewSettlePaymentDocumentationID END
	IF @DocCategoryID = 10 BEGIN
		UPDATE tblSettlePaymentDocumentation 
			SET SettlementConfirmation = 1 
			WHERE SetPymtID = @NewSettlePaymentDocumentationID END

	FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID, @MatterID
END

CLOSE Cur_Claimants
DEALLOCATE Cur_Claimants

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] TO [power_user]
GO
