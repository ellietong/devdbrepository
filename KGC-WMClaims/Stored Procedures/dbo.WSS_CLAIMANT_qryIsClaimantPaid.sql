SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] (@ClaimantPersonalInfoID int)
AS

SET NOCOUNT ON

SELECT COUNT(DISTINCT paymentSettlementID) 
FROM tblPaymentSettlement ps
	LEFT JOIN tblClaimantSettlement cs	ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
	LEFT JOIN tblClaimLawsuit cl		ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaim c				ON cl.ClaimID = c.ClaimID
WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
GROUP BY c.ClaimantPersonalInfoID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryIsClaimantPaid] TO [power_user]
GO
