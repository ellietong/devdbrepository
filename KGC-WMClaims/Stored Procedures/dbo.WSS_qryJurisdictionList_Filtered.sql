SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryJurisdictionList_Filtered]
(@StateID int = null)
--(@prefixText nvarchar(MAX), @count int = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT JurisdictionID, Jurisdiction FROM tblJurisdiction
	--UNION
	--SELECT NULL, ''
	--ORDER BY Jurisdiction, JurisdictionID
	
IF @StateID IS NULL
BEGIN	
	SELECT DJ.JurisdictionID, Jurisdiction FROM tblDefenseJurisdiction DJ
	INNER JOIN	tblJurisdiction J ON DJ.JurisdictionID = J.JurisdictionID
	WHERE StateID = @StateID
	--UNION
	--SELECT NULL, '--Select a State--'
	ORDER BY Jurisdiction, JurisdictionID	
END
ELSE
BEGIN
	SELECT DJ.JurisdictionID, Jurisdiction FROM tblDefenseJurisdiction DJ
	INNER JOIN	tblJurisdiction J ON DJ.JurisdictionID = J.JurisdictionID
	WHERE StateID = @StateID
	UNION
	SELECT NULL, ''
	ORDER BY Jurisdiction, JurisdictionID
END	
	
	--SET NOCOUNT ON;

 --   -- Insert statements for procedure here
 --   SET ROWCOUNT @count
	--SELECT DJ.JurisdictionID, COALESCE(Jurisdiction, '') + ' (' + State + ')' AS Jurisdiction 
	--FROM tblDefenseJurisdiction DJ
	--INNER JOIN tblState S ON DJ.StateID = S.StateID
	--INNER JOIN tblJurisdiction  WITH (READCOMMITTED)  
	--ON DJ.JurisdictionID = tblJurisdiction.JurisdictionID
	----ON P.CarrierID = tblCarrier.CarrierID  
	--WHERE DJ.StateID = @prefixText
	--GROUP BY Jurisdiction, [State], DJ.StateID, DJ.JurisdictionID
	--ORDER BY Jurisdiction
	--SET ROWCOUNT 0	
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryJurisdictionList_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryJurisdictionList_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryJurisdictionList_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryJurisdictionList_Filtered] TO [power_user]
GO
