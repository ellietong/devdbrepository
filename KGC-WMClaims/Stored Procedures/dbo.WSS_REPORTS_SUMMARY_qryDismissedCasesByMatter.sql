SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[WSS_REPORTS_SUMMARY_qryDismissedCasesByMatter]
(@MatterID int,@BeginDate datetime, @EndDate datetime)
As
Set NoCount On

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @DiseaseCount Table (Matter nvarchar(100), MatterID int, claimlawsuitstatus nvarchar (500),
	MoYearDismissed datetime, YearDismissed datetime,MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int, unknowncount1 int)
INSERT INTO @DiseaseCount

SELECT Matter, 
	C.MatterID,  
	css.claimlawsuitstatus,
	convert(datetime,(convert(nvarchar,Month(cst.statusDate))+'/1/'+ convert(nvarchar,Year(cst.StatusDate)))) AS MoYearDismissed,
	convert(nvarchar,Year(cst.StatusDate)) as YearDismissed,
	Case when DMeso.HighLevelDiseaseID = 1 Then 1 Else 0 End AS MesoCount,
	Case when DLC.HighLevelDiseaseID = 2 Then 1 Else 0 End AS LungCancerCount,
	Case when DOC.HighLevelDiseaseID = 3 Then 1 Else 0 End AS OtherCancerCount,
	Case when DNM.HighLevelDiseaseID in (4,7) Then 1 Else 0 End AS NonMaligCount,
	Case when DUnk.HighLevelDiseaseID in (5,6) Then 1 Else 0 End AS UnknownCount,
	Case when DUnk.HighLevelDiseaseID is NULL Then 1 Else 0 End AS UnknownCount1
FROM tblClaim C	
	LEFT Join (Select * 
			   From tblClaimDisease CD 
			   Where PrimaryDisease = 1) CD		ON CD.ClaimID=C.ClaimID
	LEFT JOIN tblMatter M						ON c.MatterID = M.MatterID
	LEFT JOIN tblClaimSequence csq				ON c.ClaimSequenceID = csq.ClaimSequenceID
	Left Join tblDisease DMeso					ON CD.DiseaseID=DMeso.DiseaseID
	Left Join tblDisease DLC					ON CD.DiseaseID=DLC.DiseaseID
	Left Join tblDisease DOC					ON CD.DiseaseID=DOC.DiseaseID
	Left Join tblDisease DNM					ON CD.DiseaseID=DNM.DiseaseID
	Left Join tblDisease DUnk					ON CD.DiseaseID=DUnk.DiseaseID
	Left Join tblClaimLawsuit CL				ON C.ClaimID=CL.ClaimID
	Left Join tblLawsuit L						ON CL.LawsuitID=L.LawsuitID
	left join tblClaimantSettlement cs			ON cs.ClaimLawsuitID = CL.ClaimLawsuitID
	left join tblClaimLawsuitStatusTracking cst ON cst.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblClaimLawsuitStatus css			ON css.ClaimLawsuitStatusID = cst.ClaimLawsuitStatusID
WHERE   COALESCE(IsPrimaryStatus, 0) = 1
	AND cst.ClaimLawsuitStatusID in (15,16,17,18,19,20,21,26,27,30,34,35,37,38,41,42)
	AND C.MatterID >= COALESCE(@MatterID, 0) 
	AND C.MatterID <= COALESCE(NULLIF(@MatterID,0), @maxMatterID) 
	and ProcessedDate BETWEEN COALESCE(@BeginDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) AND COALESCE(@EndDate, GETDATE())
ORDER BY  Year(cst.StatusDate)

Select Matter, 
	MatterID,  
	Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, 
	Sum(OtherCancerCount) as OtherCancerCount,
	Sum(NonMaligCount) as NonMaligCount, 
	Sum(UnknownCount+unknowncount1) as UnknownCount, 
	@BeginDate as StartDate, 
	@EndDate as EndDate,
	MoYearDismissed, 		
	YearDismissed, 
	DATENAME(month, 
	MoYearDismissed) AS Month, 
	DATENAME(year, MoYearDismissed) As Year,
	SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + unknowncount1) AS MonthTotal , 
	DATENAME(month, MoYearDismissed) + ' ' + DATENAME(year, MoYearDismissed) AS MonthYear
FROM @DiseaseCount
GROUP BY Matter, MatterID,  YearDismissed,MoYearDismissed

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryDismissedCasesByMatter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryDismissedCasesByMatter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryDismissedCasesByMatter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryDismissedCasesByMatter] TO [power_user]
GO
