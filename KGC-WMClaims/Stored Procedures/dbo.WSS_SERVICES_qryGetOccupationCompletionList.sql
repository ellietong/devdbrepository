SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SERVICES_qryGetOccupationCompletionList]
(@prefixText nvarchar(MAX), @count int = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SET ROWCOUNT @count
	SELECT DISTINCT TOP 5 Occupation FROM tblOccupation WHERE Occupation LIKE @prefixText + '%'
	SET ROWCOUNT 0
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SERVICES_qryGetOccupationCompletionList] TO [power_user]
GO
