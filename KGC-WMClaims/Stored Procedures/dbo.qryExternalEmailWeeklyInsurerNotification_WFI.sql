SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalEmailWeeklyInsurerNotification_WFI]
(@BodyMessage nvarchar(MAX) output)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT
--BEGIN	


set @BodyMessage = @BodyMessage + '
		<p style="font-family: Calibri; font-size: 10pt; text-align: left; ">
			WHEELABRATOR-FRYE, INC. INSURERS<br/>
			
		</p>
		<p style="font-family: Calibri; font-size: 10pt; font-weight: bold; text-align: left; border: 1px solid transparent; " colspan="9">	
			<u>NOTIFICATION OF CLAIM</u><br/>
			
		<p/>
		<p style="font-family: Calibri; font-size: 10pt; font-weight: bold; text-align: left; border: 1px solid transparent; " colspan="9">
			RE: See the Table Below
		<p/>	
<p style="font-family: Calibri; font-size: 10pt; text-align: left; border: 1px solid transparent; " colspan="9">			
		Dear Insurers:<br/>
<br/>
			 We have received a complaint in the state of Pennsylvania in the above-styled<br/>
			 case.<br/> 
<br/>
			 We give you formal notice of these claims against us and require:<br/>
<br/>
			 1.  Acknowledgment of your receipt of this letter;<br/>
<br/>
			 2.  Identification of your file number and designation of the individual at your<br/>
			     company who will be handling these matters;<br/>
<br/>
			 3.  Your immediate and unequivocal participation in defense of these matters;<br/>
<br/>
			 4.  Indication that you will indemnify us, the insured, if obligated to do so<br/>
				 under the insurance policies.<br/>
<br/>
			The liabilities attributed to Wheelabrator-Frye may be held by Waste<br/>
		Management, Inc. or one of its subsidiaries to whom your company owes a duty to<br/>
		defend and indemnify.  Thus, Waste Management, Inc. tenders these matters to you for<br/>
		defense and indemnity.  The corporate history on these claims has either previously<br/>
		been provided to you or will be provided upon request.<br/>
<br/>
			We, the insured, believe that this matter can, and should be resolved with full<br/>
		participation of its insurers, and looks to you for prompt and unequivocal assumption of<br/>
		defense and indemnity.<br/>
<br/>
											Sincerely,<br/>
											<br/>

											WHEELABRATOR-FRYE, INC.<br/>
											<br/>
											<br/>
											_____________________________
											<br/>
											Ashley Harper<br/>
											Waste Management, Inc.<br/>
											Litigation Counsel<br/>	

	
 </p>'	
--END	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_WFI] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_WFI] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_WFI] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailWeeklyInsurerNotification_WFI] TO [power_user]
GO
