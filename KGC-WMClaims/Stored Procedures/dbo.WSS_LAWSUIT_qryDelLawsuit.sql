SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryDelLawsuit]
	-- Add the parameters for the stored procedure here
(@ClaimID int, @LawsuitID int =  null, @Emailaddress nvarchar(250), @ClaimDiseaseID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @ClaimLawsuitID int = (SELECT ClaimLawsuitID FROM tblClaimLawsuit WHERE LawsuitID = @LawsuitID AND ClaimID = @ClaimID)

UPDATE tblClaimLawsuit
SET SPUsername = @Emailaddress
WHERE ClaimLawsuitID = @ClaimLawsuitID

UPDATE tblClaimLawsuitStatusTracking
SET SPUsername = @Emailaddress
WHERE ClaimLawsuitID = @ClaimLawsuitID

DELETE FROM tblClaimLawsuitStatusTracking WHERE ClaimLawsuitID = @ClaimLawsuitID

DELETE FROM tblClaimLawsuit WHERE ClaimLawsuitID = @ClaimLawsuitID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryDelLawsuit] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryDelLawsuit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryDelLawsuit] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryDelLawsuit] TO [power_user]
GO
