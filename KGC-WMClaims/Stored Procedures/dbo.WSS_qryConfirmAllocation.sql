SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryConfirmAllocation]
(@TravelersBillID int, @Emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE tblTravelersBill
	SET AllocationQCConfirmation = 1--,
		--SPUsername = @Emailaddress
	WHERE TravelersBillID = @TravelersBillID
	
	SELECT @@ROWCOUNT

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryConfirmAllocation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryConfirmAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryConfirmAllocation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryConfirmAllocation] TO [power_user]
GO
