SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByPlaintiffsCounselByYear] 
--(@MatterID int = NULL, 
--                                                                              @YearID   int, 
--                                                                              @MonthID  int) 
                                                                              
	(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null)
                                                                              
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--DECLARE @year int
--DECLARE @Month int
--SET @year = @YearID
--SET @Month= @MonthID

SELECT pca.PlaintiffCounsel, 
	COUNT(DISTINCT(cl.ClaimLawsuitID)) AS Count
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
    LEFT JOIN (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
    INNER JOIN tblPlaintiffCounselAddress pca     ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
WHERE  COALESCE(ServiceDate,FileDate,ReceivedDate,Statusdate)between COALESCE(@StartDate,'1/1/1900')
		and COALESCE(@EndDate, '1/1/2100')
		--and IsPrimaryForClaim = 1 
		--and IsPrimaryStatus=1
	AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
	AND ClaimLawsuitStatusID = 8 
GROUP BY pca.PlaintiffCounsel
ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC, pca.PlaintiffCounsel


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByPlaintiffsCounselByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByPlaintiffsCounselByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByPlaintiffsCounselByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByPlaintiffsCounselByYear] TO [power_user]
GO
