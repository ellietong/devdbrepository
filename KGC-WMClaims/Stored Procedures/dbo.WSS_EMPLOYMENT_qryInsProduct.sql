SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_EMPLOYMENT_qryInsProduct] (@ProductID			bigint, 
													  @ClaimantEmploymentID bigint, 
													  @StartDate			datetime, 
													  @EndDate				datetime, 
													  @Emailaddress			nvarchar(100))
AS
SET NOCOUNT ON

INSERT INTO tblClaimantProduct (ProductID, ClaimantEmploymentID, StartDate, EndDate, SPUsername)
VALUES (@ProductID, @ClaimantEmploymentID, @StartDate, @EndDate, @Emailaddress)

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_EMPLOYMENT_qryInsProduct] TO [power_user]
GO
