SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryUpdSettleDocs] (@SettleDocID int, 
													@DocumentID int, 
													@DocCategoryID int, 
													@PageNums nvarchar(100), 
													@EmailAddress nvarchar(250))
AS
SET NOCOUNT ON

--DECLARE @SettlePaymentID int
--DECLARE @EmailAddress nvarchar(100)
--SET @SettlePaymentID = 23
--SET @EmailAddress = 'hubbardb@kcicllc.com' 

UPDATE tblSettleDocs
	SET DocumentID = @DocumentID, 
		DocCategoryID = @DocCategoryID, 
		PageNums=@PageNums, 
		SPUsername = @EmailAddress
	WHERE SettleDocID = @SettleDocID


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettleDocs] TO [power_user]
GO
