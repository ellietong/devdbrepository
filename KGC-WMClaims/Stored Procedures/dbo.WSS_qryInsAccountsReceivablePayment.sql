SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInsAccountsReceivablePayment](@AccountsReceivableID int, @PaymentID int, @Amount money, @Emailaddress nvarchar(250))
AS
BEGIN
	INSERT INTO tblAccountsReceivablePayment (AccountsReceivableID, PaymentID, Amount, SPUsername)
	VALUES (@AccountsReceivableID, @PaymentID, @Amount, @Emailaddress)
	

DECLARE @ARCGID int
SET @ARCGID = (SELECT C.CarrierGroupID
FROM tblAccountsReceivable AR 
INNER JOIN tblPolicy P ON AR.PolicyID = P.PolicyID
INNER JOIN tblCarrier C ON P.CarrierID = C.CarrierID
INNER JOIN tblCarrierGroup CG ON C.CarrierGroupID = CG.CarrierGroupID
WHERE AccountsReceivableID = @AccountsReceivableID)

DECLARE @PCGID int
SET @PCGID = (SELECT P.CarrierGroupID FROM tblPayment P
WHERE PaymentID = @PaymentID)



IF @ARCGID <> @PCGID
BEGIN
	INSERT INTO tblAccountsReceivable (PolicyID, Amount, TravelersBillID, PayeeTypeID, RescoDueDate, SPUsername)
	SELECT	PolicyID, 
			Amount, 
			TravelersBillID, 
			'2',
			CASE WHEN @ARCGID = 18 THEN GETDATE()+33 ELSE (SELECT DatePaid FROM tblPayment WHERE PaymentID = @PaymentID) END,
			@Emailaddress
	FROM tblAccountsReceivable ar
	WHERE AccountsReceivableID = @AccountsReceivableID
	AND PayeeTypeID = 1
END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsAccountsReceivablePayment] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsAccountsReceivablePayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsAccountsReceivablePayment] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsAccountsReceivablePayment] TO [power_user]
GO
