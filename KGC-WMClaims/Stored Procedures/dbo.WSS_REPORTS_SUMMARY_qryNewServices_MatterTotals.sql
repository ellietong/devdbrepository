SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_REPORTS_SUMMARY_qryNewServices_MatterTotals] (@StartDate datetime, 
                                                                   @EndDate datetime, 
                                                                   @MatterID int)
AS
SET NOCOUNT ON

/*Modified July 2013 -- DRE*/

/*---Debugging---*/
--DECLARE @StartDate	DATETIME
--DECLARE @EndDate	DATETIME
--DECLARE @Matterid	INT

--SET @StartDate	= '1/1/2000'
--SET @EndDate	= '7/17/2013'
--SET @Matterid	= NULL

-- Find Max ID Values
DECLARE @maxMatterID INT
SELECT	@maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @DiseaseCount TABLE (	Matter NVARCHAR(100),
								MatterID INT,
								State NVARCHAR(150),
								ClaimID INT,
								ReportingDate DATETIME,
								MesoCount INT,
								LungCancerCount INT,
								OtherCancerCount INT,
								NonMaligCount INT,
								UnknownCount INT,
								NullCount INT
							)
INSERT INTO @DiseaseCount
	SELECT Matter, 
		C.MatterID,
			CASE	WHEN L.StateID=45 AND L.JurisdictionID IN (386,387)
						THEN 'Illinois (Madison)'
					WHEN L.StateID=45 AND L.JurisdictionID NOT IN (386,387)
						THEN 'Illinois (Other)'
			ELSE COALESCE(S.FullNameState, 'Unknown')
		END AS State,
		C.ClaimID, 
		--MIN(Coalesce(CL.ServiceDate, L.FileDate, CL.ReceivedDate, '1/1/1900')) AS ReportingDate,
		min(CL.ServiceDate) AS ReportingDate,
		CASE WHEN DMeso.HighLevelDiseaseID = 1		THEN 1 ELSE 0 END AS MesoCount,
		CASE WHEN DLC.HighLevelDiseaseID = 2		THEN 1 ELSE 0 END AS LungCancerCount,
		CASE WHEN DOC.HighLevelDiseaseID = 3		THEN 1 ELSE 0 END AS OtherCancerCount,
		CASE WHEN DNM.HighLevelDiseaseID IN (4, 7, 8)	THEN 1 ELSE 0 END AS NonMaligCount,
		CASE WHEN DUnk.HighLevelDiseaseID IN (5, 6) THEN 1 ELSE 0 END AS UnknownCount,
		CASE WHEN DUnk.HighLevelDiseaseID is NULL	THEN 1 ELSE 0 END AS NullCount
	FROM tblClaim C 
		LEFT JOIN (SELECT * 
				   FROM tblClaimDisease CD 
				   WHERE PrimaryDisease = 1) CD		ON CD.ClaimID = C.ClaimID
		LEFT JOIN tblMatter M						ON c.MatterID = M.MatterID
		LEFT JOIN tblDisease DMeso					ON CD.DiseaseID = DMeso.DiseaseID
		LEFT JOIN tblDisease DLC					ON CD.DiseaseID = DLC.DiseaseID
		LEFT JOIN tblDisease DOC					ON CD.DiseaseID = DOC.DiseaseID
		LEFT JOIN tblDisease DNM					ON CD.DiseaseID = DNM.DiseaseID
		LEFT JOIN tblDisease DSC					ON CD.DiseaseID = DSC.DiseaseID
		LEFT JOIN tblDisease DND					ON CD.DiseaseID = DND.DiseaseID
		LEFT JOIN tblDisease DUnk					ON CD.DiseaseID = DUnk.DiseaseID
		LEFT JOIN tblClaimLawsuit CL				ON C.ClaimID = CL.ClaimID
		LEFT JOIN tblLawsuit L						ON CL.LawsuitID = L.LawsuitID
	LEFT JOIN tblState S						ON L.StateID = S.StateID
	WHERE  C.MatterID >= COALESCE(@MatterID, 0) 
		AND C.MatterID <= COALESCE(NULLIF(@MatterID,0), @maxMatterID)
	GROUP BY 
		C.MatterID,
		C.ClaimID,
		Matter,
				(CASE	WHEN L.StateID=45 AND L.JurisdictionID IN (386,387)
							THEN 'Illinois (Madison)'
						WHEN L.StateID=45 AND L.JurisdictionID NOT IN (386,387)
							THEN 'Illinois (Other)'
			ELSE COALESCE(S.FullNameState, 'Unknown') END),
		--CL.ServiceDate,
		DMeso.HighLevelDiseaseID,
		DLC.HighLevelDiseaseID,
		DOC.HighLevelDiseaseID,
		DNM.HighLevelDiseaseID,
		DUnk.HighLevelDiseaseID

	/*SELECT * FROM @DiseaseCount
	WHERE ReportingDate BETWEEN COALESCE(@StartDate, '1/1/1900') AND COALESCE(@EndDate, GETDATE())
	ORDER BY ClaimID*/

SELECT	Matter, 
		MatterID, 
		SUM(MesoCount) AS MesoServices,
		SUM(LungCancerCount) AS LCServices, 
		SUM(OtherCancerCount) AS OCServices,
		SUM(NonMaligCount) AS NMServices, 
		SUM(UnknownCount+NullCount) AS UnknownServices, 
		SUM(UnknownCount+NullCount+MesoCount+LungCancerCount+OtherCancerCount+NonMaligCount) AS TotalCount
FROM @DiseaseCount
WHERE --ReportingDate BETWEEN COALESCE(@StartDate, '1/1/1900') AND COALESCE(@EndDate, GETDATE())
	COALESCE(ReportingDate,GETDATE()) <= COALESCE(@EndDate, GETDATE())
	AND COALESCE(ReportingDate,'1/1/1900') >= COALESCE(@StartDate, '1/1/1900')
GROUP BY Matter, 
		 MatterID
ORDER BY Matter
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewServices_MatterTotals] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewServices_MatterTotals] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewServices_MatterTotals] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewServices_MatterTotals] TO [power_user]
GO
