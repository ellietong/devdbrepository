SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] 
--(@MatterID int = NULL, 
--                                                                                    @YearID   int, 
--                                                                                    @MonthID  int) 
                                                                                    
	(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null)                                                                                    
                                                                                    
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      AND all original StatusDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--DECLARE @year int
--DECLARE @month int
--SET @year = @YearID
--SET @month = @MonthID

SELECT TOP 10 pca.PlaintiffCounsel, 
	COUNT(DISTINCT(cl.ClaimLawsuitID)) AS 'Count'
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca     ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
WHERE COALESCE(Statusdate,ProcessedDate)BETWEEN COALESCE(@StartDate,'1/1/1900')
	AND COALESCE(@EndDate, '1/1/2100') 
	AND ClaimLawsuitStatusID IN (1, 2, 3, 13) 
	AND IsPrimaryForClaim = 1 
	AND IsPrimaryStatus = 1
	AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
GROUP BY pca.PlaintiffCounsel
ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC, pca.PlaintiffCounsel


select * from tblClaimLawsuitStatus


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [power_user]
GO
