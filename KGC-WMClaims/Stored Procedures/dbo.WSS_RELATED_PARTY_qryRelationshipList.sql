SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_RELATED_PARTY_qryRelationshipList]
AS
SET NOCOUNT ON

SELECT RelationshipID, Relationship
FROM tblRelationship
UNION
SELECT NULL, ''	 
ORDER BY Relationship

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryRelationshipList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryRelationshipList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryRelationshipList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryRelationshipList] TO [power_user]
GO
