SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmail_InsurerNotification_ByMatter]
AS

SET NOCOUNT ON

BEGIN

--STEP 1: Collect the relevant matters to be emailed, and the body text of each email from tblMatter
DECLARE @EmailInformation TABLE (EmailMatter      nvarchar (20), 
								 EmailMatterID    int, 
								 EmailBoilerplate nvarchar (max), 
								 EmailContactsCC    nvarchar (max),
								 EmailContactsTo    nvarchar (max))
INSERT INTO @EmailInformation
	SELECT Matter AS EmailMatter, m.MatterID AS EmailMatterID, InsurerNotificationText + '<br/><br/>' AS EmailBoilerplate, NULL AS EmailContactsTo, NULL AS EmailContactsCC
	FROM tblMatter m
		--INNER JOIN tblMatterInsurersToNotify mitn ON m.MatterID = mitn.MatterID
		--INNER JOIN [ShareSQL].[FBSecurity].[dbo].tblPerson p ON mitn.PersonID = p.PersonID
	WHERE InsurerNotificationText IS NOT NULL

--STEP 2: Iterate through each matter to create a semi-colon delineated list of the contacts, per tblMatterInsurersToNotify, by matter from Step 1
DECLARE @MatterID       int
DECLARE @MatterContactsTo nvarchar (max) = ''
DECLARE @MatterContactsCC nvarchar (max) = ''
DECLARE @Contact        nvarchar (500)
DECLARE @RecipientTypeID int
DECLARE @EmailToField nvarchar(MAX)
DECLARE @EmailCCField nvarchar(MAX)

DECLARE matter_curs CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY FOR 
	SELECT EmailMatterID
	FROM @EmailInformation

OPEN matter_curs
FETCH NEXT FROM matter_curs INTO @MatterID

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE contact_curs CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY FOR 
		SELECT Email,  RecipientTypeID
		FROM tblMatterInsurersToNotify mitn
			INNER JOIN vtblPerson p ON mitn.PersonID = p.PersonID
		WHERE MatterID = @MatterID

	OPEN contact_curs
	FETCH NEXT FROM contact_curs INTO @contact, @RecipientTypeID

	WHILE @@FETCH_STATUS = 0
	BEGIN
	IF @RecipientTypeID=1
		SET @MatterContactsTo = @MatterContactsTo + @Contact + '; '
	ELSE
	    SET @MatterContactsCC = @MatterContactsCC + @Contact + '; '
		FETCH NEXT FROM contact_curs INTO @contact, @RecipientTypeID
	END

	UPDATE @EmailInformation
		SET EmailContactsTo = @MatterContactsTo, EmailContactsCC = @MatterContactsCC
		WHERE EmailMatterID = @MatterID
		
		
	CLOSE contact_curs
	DEALLOCATE contact_curs

	SET @MatterContactsTo = ''
	SET @MatterContactsCC = ''
	FETCH NEXT FROM matter_curs INTO @MatterID
END

CLOSE matter_curs
DEALLOCATE matter_curs

--STEP 3: Collect a summary of all complaints newly created in the past week for the matters collected in Step 1
DECLARE @LastWeek4PM datetime = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 7)) + ' 16:00:00.000')
DECLARE @Today4PM    datetime = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE()    )) + ' 16:00:00.000')

DECLARE @NewComplaintsTable TABLE (DocID                  int, 
								   docTitle               nvarchar (500), 
								   lName                  nvarchar (50),
								   fName                  nvarchar (50), 
								   ClaimantPersonalInfoID bigint, 
								   ServiceDate            date, 
								   MatterID               int, 
								   [State]                nvarchar (100))
INSERT INTO @NewComplaintsTable
	SELECT DISTINCT d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/wm/Pages/Insurer-Complaint-Notification.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		ServiceDate,
		c.MatterID,
		[State]
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument    mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo     cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		INNER JOIN tblComplaintTrackingLawsuit ctl ON d.ComplaintID = ctl.ComplaintTrackingID
		INNER JOIN tblClaim                    c   ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit             cl  ON cl.ClaimID = c.ClaimID AND ctl.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblAllegation               a   ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		INNER JOIN tblDefendantCompany         dc  ON dc.DefendantCompanyID = a.DefendantCompanyID
		INNER JOIN tbllawsuit                  l   ON l.LawsuitID = cl.LawsuitID
		INNER JOIN tblState                    s   ON s.StateID = l.StateID
	WHERE d.DateCreated BETWEEN @LastWeek4PM AND @Today4PM 
		AND d.ClaimantDocumentTypeID IN (8)
		AND c.MatterID IN (SELECT EmailMatterID FROM @EmailInformation)
	GROUP BY d.documentID,
		d.DocumentTitle,
		LastName,
		FirstName,
		cpi.ClaimantPersonalInfoID,
		ServiceDate,
		c.MatterID,
		[State]


	ORDER BY ServiceDate

--STEP 4: Build a list of claimants related to each of the new complaints collected in Step 3
DECLARE @DocID          int,
		@LastDocID      int,
		@Claimant       nvarchar (max),
		@ClaimantID     bigint,
		@ClaimantList   nvarchar (max)
DECLARE @ClaimantPerDoc TABLE (DocID        int, 
							   ClaimantList nvarchar (max))

DECLARE Cur_Complaints CURSOR Local Fast_Forward FOR 
	SELECT DocID,
		COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
		ClaimantPersonalInfoID
	FROM @NewComplaintsTable

OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID

WHILE @@FETCH_STATUS = 0
BEGIN
	IF (LEN (@ClaimantList) = 0 OR @ClaimantList IS NULL)
	BEGIN
		SET @ClaimantList = '<ul style="margin:0; padding:0;"> ' + @Claimant + '</ul>'
	END
	ELSE
	BEGIN
		SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul style="margin:0; padding:0;"> ' + COALESCE(@Claimant, '') + '</ul>'
	END

	SET @LastDocID = @DocID
	FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID

	IF @DocID < > @LastDocID OR @@FETCH_STATUS < > 0
	BEGIN
		INSERT INTO @ClaimantPerDoc
			SELECT @LastDocID, @ClaimantList

		SET @ClaimantList = NULL
	END
END

CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints

--STEP 5: Prepare the HTML formatting and send the actual emails, using the body text from Step 1, recipients from Step 2, and the complaint information from Steps 3 and 4
DECLARE @TDStyle          nvarchar (max) = 'font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'
DECLARE @EmailMatter      nvarchar (20), 
		@EmailMatterID    int, 
		@EmailBoilerplate nvarchar (max), 
		@EmailContactsTo    nvarchar (max),
		@EmailContactsCC   nvarchar (max),
		@Subject          nvarchar (200),
		@BodyMessage      nvarchar (MAX)
		
DECLARE Email_Curs CURSOR FOR 
	SELECT EmailMatter,
		EmailMatterID,
		EmailBoilerplate,
		ei.EmailContactsTo,
		ei.EmailContactsCC
	FROM @EmailInformation ei

OPEN Email_Curs
FETCH NEXT FROM Email_Curs INTO @EmailMatter, @EmailMatterID, @EmailBoilerplate, @EmailContactsTo, @EmailContactsCC

WHILE @@FETCH_STATUS = 0
BEGIN
	IF (SELECT COUNT(docTitle) FROM @NewComplaintsTable	WHERE MatterID = @EmailMatterID) > 0
	BEGIN
		SET @BodyMessage = ''
		SET @BodyMessage = @BodyMessage + @EmailBoilerplate
		SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">'
			+ N'<tr>
				<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th>
				<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants</th>
				<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Served</th>
				<th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">State</th>'
			+ COALESCE(CAST((SELECT @TDStyle AS 'td/@style', (SELECT docTitle)         AS td, '',
									@TDStyle AS 'td/@style', (SELECT cl.ClaimantList)  AS td, '',
									@TDStyle AS 'td/@style', (SELECT MAX(ServiceDate)) AS td, '',
									@TDStyle AS 'td/@style', (SELECT [State])          AS td, ''
							 FROM @NewComplaintsTable C
							     INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
							 WHERE c.MatterID = (SELECT @EmailMatterID)
							 GROUP BY docTitle,
								 cl.ClaimantList,
								 [State]
							 FOR xml PATH ('tr'), TYPE) AS nvarchar(MAX)), '')
			+ N'</table>'
			+ N'<br/>'

		SET @Subject = case when @EmailMatterID in (1,2,3) then 'Resco ' else '' end + @EmailMatter + ' Notification of New Claim ' + CONVERT(varchar, GETDATE(), 107)
		SET @BodyMessage = @BodyMessage + '<P>Click <A Href = ' + (SELECT 'https://www.kcicextranet.com/clients/wm/Pages/Insurer-Complaint-Notification.aspx')
			+ '>here</A> to view all complaints noticed to date.</p> '

		--Undo the HTML stripping from the xml path function above so embedded tags work
		SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')
		SET @EmailContactsCC = @EmailContactsCC + ';nshoebot@wm.com; AHarper@wm.com; DMiller@tmlpa.com'

		IF(@EmailMatterID = 8) -- If the user is from Rust, attach the PDF
		BEGIN
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = 'Claims',
			--@recipients = 'hubbardb@kcic.com',
			@recipients = @EmailContactsTo ,
			@copy_recipients = @EmailContactsCC ,
			@blind_copy_recipients = 'hubbardb@kcic.com; maloneyd@kcic.com; barnabyj@kcic.com; wm@kcic.com; lislev@kcic.com',
			@subject = @Subject,
			@body = @BodyMessage,
			@body_format = 'html',
			@file_attachments = 'C:\WMFILE_DONOTDELETE\SchedulesA&B-RustEngineeringCompany_PolicyInformation.PDF'
		END
		ELSE -- All other users get the email without the PDF.
		BEGIN
		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = 'Claims',
			--@recipients = 'hubbardb@kcic.com',
			@recipients = @EmailContactsTo ,
			@copy_recipients = @EmailContactsCC,
			@blind_copy_recipients = 'hubbardb@kcic.com; maloneyd@kcic.com; barnabyj@kcic.com; wm@kcic.com; lislev@kcic.com',
			@subject = @Subject,
			@body = @BodyMessage,
			@body_format = 'html'
		END


		UPDATE d
			SET InsurerNotificationDate = GETDATE()
			FROM tblDocuments d
				INNER JOIN (SELECT DocID 
							FROM @NewComplaintsTable c 
							WHERE c.MatterID = @EmailMatterID) AS nct ON d.DocumentID = nct.DocID	
	END

	FETCH NEXT FROM Email_Curs INTO @EmailMatter, @EmailMatterID, @EmailBoilerplate, @EmailContactsTo, @EmailContactsCC
END

CLOSE Email_Curs
DEALLOCATE Email_Curs

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmail_InsurerNotification_ByMatter] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmail_InsurerNotification_ByMatter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmail_InsurerNotification_ByMatter] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmail_InsurerNotification_ByMatter] TO [power_user]
GO
