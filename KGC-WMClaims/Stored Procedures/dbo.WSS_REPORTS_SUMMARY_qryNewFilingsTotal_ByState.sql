SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_REPORTS_SUMMARY_qryNewFilingsTotal_ByState] (@StartDate		datetime, 
                                                                @EndDate		datetime,
															    @MatterID		int)
AS
SET NOCOUNT ON

--Declare @matterid	int
--declare @startdate	datetime
--declare @enddate	datetime
--declare @stateid	int

--set @startdate =	'1/1/2012'
--set @enddate =	'9/30/2012'
--set @matterid =	9




DECLARE @DiseaseCount TABLE (Matter nvarchar(100), MatterID int, State nvarchar(150), StateID int, YearFiled datetime, monthFiledID int, monthFiledName nvarchar(100), statusdate datetime, MesoCount int, LungCancerCount int, OtherCancerCount int, 
							 NonMaligCount int,  UnknownCount int, UnknownCount1 int)
INSERT INTO @DiseaseCount
SELECT Matter, 
	C.MatterID, 
	
	FullNameState as state,  
	l.StateID,
	CASE WHEN Coalesce(new_filing_date, FileDate, receiveddate) is null then  '1/1/1900' ELSE CONVERT(datetime,('1/1/'+convert(nvarchar,year(Coalesce(new_filing_date, FileDate, receiveddate)))))END AS YearFiled, 
	MONTH(COALESCE(new_filing_date, FileDate, receiveddate, '1/1/1900')), 
	Datename(month, COALESCE(new_filing_date, FileDate, receiveddate, '1/1/1900')),
	COALESCE(statusdate,'1/1/1900'),
	Case when DMeso.HighLevelDiseaseID = 1 Then 1 Else 0 End AS MesoCount,
	Case when DLC.HighLevelDiseaseID = 2 Then 1 Else 0 End as LungCancerCount,
	Case when DOC.HighLevelDiseaseID = 3 Then 1 Else 0 End as OtherCancerCount,
	Case when DNM.HighLevelDiseaseID IN (4, 7) Then 1 Else 0 End as NonMaligCount,
	Case when DUnk.HighLevelDiseaseID IN (5, 6) Then 1 Else 0 End AS UnknownCount,
	Case when DUnk.HighLevelDiseaseID is NULL Then 1 Else 0 End AS UnknownCount1
FROM tblClaim C 
	LEFT JOIN (SELECT * 
			   FROM tblClaimDisease CD 
			   WHERE PrimaryDisease = 1) CD		ON CD.ClaimID = C.ClaimID
	LEFT JOIN tblMatter M						ON c.MatterID = M.MatterID
	LEFT JOIN tblDisease DMeso					ON CD.DiseaseID = DMeso.DiseaseID
	LEFT JOIN tblDisease DLC					ON CD.DiseaseID = DLC.DiseaseID
	LEFT JOIN tblDisease DOC					ON CD.DiseaseID = DOC.DiseaseID
	LEFT JOIN tblDisease DNM					ON CD.DiseaseID = DNM.DiseaseID
	LEFT JOIN tblDisease DSC					ON CD.DiseaseID = DSC.DiseaseID
	LEFT JOIN tblDisease DND					ON CD.DiseaseID = DND.DiseaseID
	LEFT JOIN tblDisease DUnk					ON CD.DiseaseID = DUnk.DiseaseID
	LEFT JOIN tblClaimLawsuit CL				ON C.ClaimID = CL.ClaimID
	LEFT JOIN tblLawsuit L						ON CL.LawsuitID = L.LawsuitID
	LEFT JOIN tblState S						ON L.StateID = S.StateID
	LEFT JOIN FINAL_CLAIM_NF_DATE N				ON N.CLM_KEY = C.CLM_KEY
	LEFT JOIN tblClaimLawsuitStatusTracking cst ON cst.ClaimLawsuitID = cl.ClaimLawsuitID
WHERE  /*s.StateID >= COALESCE(@StateID, 0) 
	AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID)*/ 
	--AND IsPrimaryForClaim = 1 
	/*AND*/coalesce(cst.IsPrimaryStatus, 0) = 1
	AND 	C.MatterID = COALESCE(@MatterID, 0) 
	--AND (cst.ClaimLawsuitStatusID IN (7, 8)  
	/*AND coalesce(processeddate, '1/1/1900') >= @StartDate
	AND coalesce(processeddate, '1/1/1900') <= @endDate*/--)
	
 SELECT Matter, 
	MatterID, 
	State,
	Sum(MesoCount) AS MesoFilings,
	Sum(LungCancerCount) AS LCFilings, 
	Sum(OtherCancerCount) AS OCFilings,
	Sum(NonMaligCount) AS NMFilings, 
	Sum(UnknownCount+UnknownCount1) AS UnknownFilings, 
	Sum(UnknownCount+UnknownCount1+MesoCount+LungCancerCount+OtherCancerCount+NonMaligCount) AS TotalCount
FROM @DiseaseCount
WHERE coalesce(yearfiled, '1/1/1900') BETWEEN COALESCE(@StartDate, '1/1/1900') AND COALESCE(@EndDate, GETDATE())
GROUP BY Matter, 
	MatterID, State
ORDER BY State

















------Find Max Matter ID
--DECLARE @maxMatterID int
--SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--DECLARE @DiseaseCount TABLE (ClaimID int, Matter nvarchar(100), MatterID int, State nvarchar(100), StateID int, MesoCount int, LungCancerCount int, 
--OtherCancerCount int, NonMaligCount int, SilicosisCount int, UnknownCount int, unknowncount1 int)
--INSERT INTO @DiseaseCount
--SELECT C.ClaimID, 
--	Matter, 
--	C.MatterID, 
--	COALESCE(s.FullNameState, 'Unknown') AS State,
--	CASE WHEN s.FullNameState IS NULL OR s.FullNameState = 'Unknown' THEN 0 ELSE s.StateID END AS StateID,
--	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) = 1 THEN 1 ELSE 0 END AS MesoCount,
--	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) = 2 THEN 1 ELSE 0 END AS LungCancerCount,
--	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) = 3 THEN 1 ELSE 0 END AS OtherCancerCount,
--	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) IN (4, 8) THEN 1 ELSE 0 END AS NonMaligCount,
--	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) = 7 Then 1 Else 0 End as SilicosisCount,
--	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) in (5,6) THEN 1 ELSE 0 END AS UnknownCount,
--	CASE WHEN COALESCE(D.HighLevelDiseaseID, 0) is NULL THEN 1 ELSE 0 END AS UnknownCount1 
--	--FullNameState
--FROM tblClaim C						
--	LEFT JOIN (SELECT *
--			   FROM tblClaimDisease CD 
--			   WHERE PrimaryDisease = 1) CD		ON CD.ClaimID = C.ClaimID
--	LEFT JOIN tblMatter M						ON c.MatterID = M.MatterID
--	LEFT JOIN FINAL_CLAIM_NF_DATE n				ON n.CLM_KEY = c.CLM_KEY
--	LEFT JOIN tblDisease D						ON CD.DiseaseID = D.DiseaseID
--	LEFT JOIN tblClaimLawsuit CL				ON C.ClaimID = CL.ClaimID
--	LEFT JOIN tblLawsuit L						ON CL.LawsuitID = L.LawsuitID
--	LEFT JOIN tblState s						ON l.StateID = s.StateID
--	LEFT JOIN tblClaimLawsuitStatusTracking cst ON cst.ClaimLawsuitID = cl.ClaimLawsuitID
--WHERE   IsPrimaryForClaim = 1 
--	--AND C.MatterID >= COALESCE(@MatterID,0) 
--	--AND C.MatterID <= COALESCE(NULLIF(@MatterID,0),@maxMatterID) 
--	--AND processeddate BETWEEN COALESCE(@StartDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) and COALESCE(@EndDate, GETDATE())
--	--AND l.StateID = @StateID
--	--AND FileDate BETWEEN @startdate AND @enddate
--	AND C.MatterID = @MatterID
--ORDER BY COALESCE(YEAR(new_filing_date), YEAR(fileDate), YEAR(receiveddate))

--Select Matter, 
--	MatterID, 
--	State,
--	StateID,
--	COUNT(ClaimID) AS NewFilings, 
--	@StartDate AS StartDate, 
--	@EndDate AS EndDate,
--	SUM(MesoCount) AS MesoFilings, 
--	SUM(LungCancerCount) AS LCFilings, 
--	SUM(OtherCancerCount) AS OCFilings,
--	SUM(NonMaligCount) AS NMFILINGS, 
--	SUM(SilicosisCount) AS SilicosisFilings,
--	SUM(UnknownCount+unknowncount1) AS UnknownFilings, 
--	--[state],
--	SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + SilicosisCount + Unknowncount1) AS TotalCount
--FROM @DiseaseCount DC
--GROUP BY Matter, 
--	MatterID, 
--	State, StateID
--	--[state]
--ORDER BY State
--RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilingsTotal_ByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilingsTotal_ByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilingsTotal_ByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_SUMMARY_qryNewFilingsTotal_ByState] TO [power_user]
GO
