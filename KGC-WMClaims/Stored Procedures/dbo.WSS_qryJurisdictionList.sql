SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryJurisdictionList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--SELECT DJ.JurisdictionID, Jurisdiction FROM tblDefenseJurisdiction DJ
	--INNER JOIN tblJurisdiction J ON DJ.JurisdictionID = J.JurisdictionID
	--UNION
	--SELECT NULL, ''
	--UNION 
	--SELECT JurisdictionID, Jurisdiction FROM tblJurisdiction
	--WHERE JurisdictionID = 5
	--ORDER BY Jurisdiction, JurisdictionID	
	
	SELECT DJ.JurisdictionID, Jurisdiction 
	FROM tblDefenseJurisdiction DJ
		INNER JOIN tblJurisdiction J ON DJ.JurisdictionID = J.JurisdictionID
	UNION
	SELECT NULL, ''
	UNION 
	SELECT JurisdictionID, Jurisdiction FROM tblJurisdiction
	WHERE JurisdictionID  NOT IN 
		(
		SELECT DJ.JurisdictionID FROM tblDefenseJurisdiction DJ
		INNER JOIN tblJurisdiction J ON DJ.JurisdictionID = J.JurisdictionID
		)
	ORDER BY Jurisdiction, JurisdictionID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryJurisdictionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryJurisdictionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryJurisdictionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryJurisdictionList] TO [power_user]
GO
