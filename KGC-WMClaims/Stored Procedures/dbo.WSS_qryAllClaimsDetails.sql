SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryAllClaimsDetails]
(@ClaimantLawsuitID bigint = null, @ClaimantPersonalInfoID bigint) --, @emailaddress nvarchar(250)
AS
--BEGIN


--SELECT  DISTINCT   CL.ClaimantLawsuitID, l.stateid,  S.State AS State, J.Jurisdiction, 
--                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
--                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SSN IS NULL 
--                      THEN '' ELSE 'XXX-XX-' + RIGHT(SSN, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
--                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
--                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
--                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
--                      CL.LawsuitStatusID, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, L.DocketNumber, L.CaseCaption, 
--                      L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName, CPI.ClaimantPersonalInfoID,
--					JOBSITE, cl.LawsuitID, StatusDate, SettlementDate,  LawsuitStatus, LeadPlaintiffFirstName, LeadPlaintiffLastName, L.JurisdictionID, CL.LawsuitID, SettlementAmount, SettlementDate,
--					l.FirmID, Firm
--FROM			  dbo.tblClaimantPersonalInfo AS CPI 
--				  INNER JOIN dbo.tblClaimantLawsuit AS CL ON CPI.ClaimantPersonalInfoID = CL.ClaimantPersonalInfoID 
--				  INNER JOIN dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID 
--				  LEFT JOIN dbo.tblState AS S ON S.StateID = L.StateID 
--                  LEFt JOIN dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID  
--                  left join tblClaimLawsuitDisease as ClD on ClD.ClaimantLawsuitID = cl.ClaimantLawsuitID
--				  left join tblDisease ddis on ddis.DiseaseID = ClD.DiseaseID
--				  left join tblFirm f ON L.FirmID = f.FirmID
--				  left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID --OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
--				  LEFT JOIN tblClaimantEmployment CE ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID   
--                  LEFT JOIN dbo.tblLawsuitStatus AS ls ON CL.LawsuitStatusID = ls.LawsuitStatusID 
                      
            

--WHERE Cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND CL.ClaimantLawsuitID = @ClaimantLawsuitID

--END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAllClaimsDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAllClaimsDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryAllClaimsDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryAllClaimsDetails] TO [power_user]
GO
