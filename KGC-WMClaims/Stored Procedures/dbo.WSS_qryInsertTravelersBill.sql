SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryInsertTravelersBill]
(@BillDate nvarchar(500),
@ClaimantName nvarchar(500),
@AllocationType nvarchar(500),
@DOFE  nvarchar(500),
@PaymentType nvarchar(500),
@SettlementAmount nvarchar(500),
@FirmName nvarchar(500),
@InvoiceNumber nvarchar(500),
@InvoiceDate nvarchar(500),
@InvoicedFees nvarchar(500),
@InvoicedExpenses nvarchar(500),
@InvoicedTotal nvarchar(500),
@AdjustmentFees nvarchar(500),
@AdjustmentExpenses nvarchar(500),
@FinalFees nvarchar(500),
@FinalExpenses nvarchar(500),
@FinalTotal nvarchar(500), @Type nvarchar(500),
@FirmID int = NULL,
@PaymentDueDate nvarchar(500),
@Emailaddress nvarchar(250),
@ClaimLawsuitID int)
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO tblTravelersBill (
	   [BillDate]
      ,[ClaimantName]
      ,[AllocationName]
      ,[AllocationType]
      ,[MatterID]
      ,[DOFE]
      ,[PaymentType]
      ,[SettlementAmount]
      ,[DefenseCounsel]
      ,[InvoiceNumber]
      ,[InvoiceDate]
      ,[InvoicedFees]
      ,[InvoicedExpenses]
      ,[InvoicedTotal]
      ,[AdjustmentFees]
      ,[AdjustmentExpenses]
      ,[FinalFees]
      ,[FinalExpenses]
      ,[FinalTotal]
      ,DefenseCounselAddressID
      ,PaymentDueDate
      ,SPUsername
      ,ClaimLawsuitID) 
	VALUES(
	CONVERT(date, @BillDate, 101),
	@ClaimantName,
	@AllocationType,
	@Type,
	dbo.fnGetClaimTypeID(@AllocationType, @Type), -- MatterID
	CONVERT(date, @DOFE, 101),
	@PaymentType,
	CONVERT(money, @SettlementAmount),
	@FirmName,
	@InvoiceNumber,
	@InvoiceDate,--CASE WHEN @InvoiceDate IS NOT NULL AND LEN(@InvoiceDate) > 0 THEN CONVERT(date, @InvoiceDate, 101) ELSE NULL END,
	CONVERT(money, @InvoicedFees),
	CONVERT(money, @InvoicedExpenses),
	CONVERT(money, @InvoicedTotal),
	CONVERT(money, @AdjustmentFees),
	CONVERT(money, @AdjustmentExpenses),
	CONVERT(money, @FinalFees),
	CONVERT(money, @FinalExpenses),
	CONVERT(money, @FinalTotal),
	@FirmID,
	CONVERT(date,@PaymentDueDate, 101),
	@Emailaddress,
	@ClaimLawsuitID)
	
	SELECT MAX(TravelersBillID) FROM tblTravelersBill

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsertTravelersBill] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsertTravelersBill] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryInsertTravelersBill] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryInsertTravelersBill] TO [power_user]
GO
