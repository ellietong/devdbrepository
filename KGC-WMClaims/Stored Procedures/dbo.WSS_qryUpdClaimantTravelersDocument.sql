SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryUpdClaimantTravelersDocument]
	-- Add the parameters for the stored procedure here
														(@DocumentID int, 
														 @DocumentTypeID int = null, 
														 @MatterID int = null, 
														 @DOFE date = null,
														 @InvoiceNumber nvarchar(200) = null, 
														 @BillStatusID int = null, 
														 @InvoiceDocumentID int = null,
														 @AllocatedInvoiceAmountQC bit = 0, 
														 @AllocationQCConfirmation bit = 0, 
														 @PaymentDueDate date = null,
														 @InvoiceAmazonKey nvarchar(4000) = null, 
														 @Emailaddress nvarchar(250))  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblDocuments
		SET ClaimantDocumentTypeID = COALESCE(@DocumentTypeID, ClaimantDocumentTypeID)
	WHERE DocumentID = @DocumentID
	
	UPDATE tblTravelersBill 
		SET MatterID = COALESCE(@MatterID, tb.MatterID),
			DOFE = COALESCE(@DOFE, DOFE),
			InvoiceNumber = COALESCE(@InvoiceNumber, tb.InvoiceNumber),
			BillStatusID = COALESCE(@BillStatusID, BillStatusID),
			InvoiceDocumentID = @InvoiceDocumentID,
			AllocatedInvoiceAmountQC = @AllocatedInvoiceAmountQC,
			AllocationQCConfirmation = @AllocationQCConfirmation,
			PaymentDueDate = @PaymentDueDate,
			SPUsername = @Emailaddress
	FROM tblTravelersBill tb
		INNER JOIN tblDocuments d ON tb.TravelersBillID = d.TravelersBillID
	WHERE d.DocumentID = @DocumentID

	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantTravelersDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantTravelersDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryUpdClaimantTravelersDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryUpdClaimantTravelersDocument] TO [power_user]
GO
