SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[vtblFirmsAndVendors] as 
select firmid, firm from vtblFirm
union
select vendorfirmid, vendorfirm from m_tblVendorFirm
GO
GRANT SELECT ON  [dbo].[vtblFirmsAndVendors] TO [base_user]
GO
