SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vExhaustion]
AS

SELECT ExhaustionID, ex.ExhaustionDateID, exd.ExhaustionTypeID, ExhaustionType, ExhaustionDate, AllocationID, 
[Description], OccurrenceID, PolicyDateID, IndemnityAllocation, DefenseAllocation, DefenseOutsideLimitsAllocation, 
SIRIndemnityAllocation, SIRDefenseAllocation, LedgerID
FROM tblExhaustion EX
LEFT JOIN tblExhaustionDate EXD ON EXD.ExhaustionDateID = EX.ExhaustionDateID
LEFT JOIN tblExhaustionType EXT ON EXT.ExhaustionTypeID = EXD.ExhaustionTypeID
GO
GRANT SELECT ON  [dbo].[vExhaustion] TO [base_user]
GO
