SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[tblGender]
AS
SELECT     GenderID, Gender
FROM         [KGA-ClaimsAdmin].dbo.tblGender AS tblGender_1

GO
GRANT SELECT ON  [dbo].[tblGender] TO [base_user]
GRANT SELECT ON  [dbo].[tblGender] TO [Claims_User]
GO
