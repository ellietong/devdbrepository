SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblRelationship]
AS
SELECT     RelationshipID, Relationship
FROM         [KGA-ClaimsAdmin].dbo.tblRelationship AS tblRelationship_1


GO
GRANT SELECT ON  [dbo].[tblRelationship] TO [base_user]
GRANT SELECT ON  [dbo].[tblRelationship] TO [Claims_User]
GO
