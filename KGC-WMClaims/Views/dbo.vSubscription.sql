SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vSubscription]
AS
SELECT     SSC.PolicyID, SSC.SubscriptionSectionCoverageID, SSC.SubscriptionSectionID, SS.SubscriptionSection, SSC.SubscriptionSectionPercentage, 
                      SC.SubscriptionCoverageID, SC.SubscriberID, SUB.SubscriptionCompany, SUB.SubscriptionCompanySolvency, 
                      SUB.SubscriptionCompanySolvencyStatus, SUB.SolvencySource, SUB.AMBestNo, SUB.DateUpdated, SUB.Initials, SUB.ResearchLink, 
                      SUB.WellingtonSignatory, SUB.SubscriptionCompanyNote, SUB.CarrierGroupID, CG.CarrierGroupName, CG.CarrierGroupNote, 
                      SC.SubscriptionCompanyPercent, SC.StartDate, SC.EndDate, SC.AcctNum, SC.UWReference, SC.SubscriptionNotes, SC.AgencyID, SA.AgencyName, 
                      SC.AgencyPercentOfSection, SC.GroupWithinAgencyPercent, dbo.fnSubscriptionDateProportionActual(SSC.PolicyID, SC.StartDate, SC.EndDate) 
                      AS SubscriptionDatePercentActual, dbo.fnSubscriptionDateProportionAllocation(SSC.PolicyID, SC.StartDate, SC.EndDate) 
                      AS SubscriptionDatePercentAllocation, COALESCE (SC.SubscriptionCompanyPercent, 1.000) * COALESCE (SC.AgencyPercentOfSection, 1.000) 
                      * COALESCE (SC.GroupWithinAgencyPercent, 1.000) * COALESCE (dbo.fnSubscriptionDateProportionActual(SSC.PolicyID, SC.StartDate, SC.EndDate), 
                      1.000) AS SubscriptionActualPercentOfPolicy, COALESCE (SC.SubscriptionCompanyPercent, 1.000) * COALESCE (SC.AgencyPercentOfSection, 1.000) 
                      * COALESCE (SC.GroupWithinAgencyPercent, 1.000) * COALESCE (dbo.fnSubscriptionDateProportionAllocation(SSC.PolicyID, SC.StartDate, 
                      SC.EndDate), 1.000) AS SubscriptionAllocationPercentOfPolicy, SUB.SubscriptionSort
FROM         dbo.tblSubscriptionSectionCoverage AS SSC LEFT OUTER JOIN
                      dbo.tblSubscriptionCoverage AS SC ON SSC.SubscriptionSectionCoverageID = SC.SubscriptionSectionCoverageID LEFT OUTER JOIN
                      dbo.tblSubscriptionSection AS SS ON SSC.SubscriptionSectionID = SS.SubscriptionSectionID LEFT OUTER JOIN
                      dbo.tblSubscriberCompany AS SUB ON SC.SubscriberID = SUB.SubscriberID LEFT OUTER JOIN
                      dbo.tblCarrierGroup AS CG ON SUB.CarrierGroupID = CG.CarrierGroupID LEFT OUTER JOIN
                      dbo.tblSubscriberAgency AS SA ON SC.AgencyID = SA.AgencyID
GO
GRANT SELECT ON  [dbo].[vSubscription] TO [base_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SSC"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 117
               Right = 271
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SC"
            Begin Extent = 
               Top = 120
               Left = 38
               Bottom = 231
               Right = 271
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SS"
            Begin Extent = 
               Top = 234
               Left = 38
               Bottom = 315
               Right = 224
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SUB"
            Begin Extent = 
               Top = 318
               Left = 38
               Bottom = 429
               Right = 297
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CG"
            Begin Extent = 
               Top = 432
               Left = 38
               Bottom = 528
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SA"
            Begin Extent = 
               Top = 6
               Left = 309
               Bottom = 87
               Right = 462
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
 ', 'SCHEMA', N'dbo', 'VIEW', N'vSubscription', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'        Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSubscription', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSubscription', NULL, NULL
GO
