SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[tblCase]
AS
SELECT     CaseID, CaseName, CaseDisplayName, Active, ConnectionStringKey, Bucket, DocumentFolder, Email
FROM         [Ligado-ExtranetSecurity].dbo.tblcase AS c
WHERE     (CaseID IN (3, 22, 26, 29, 30, 35, 36, 37, 38, 40, 41, 45, 46))

GO
