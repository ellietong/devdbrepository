SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vProvisions]
AS

SELECT PR.PolicyID, ProvisionID, pr.ProvisionLabelID, Provision, pr.ProvisionValueID, ProvisionValue
FROM tblProvision PR
LEFT JOIN tblProvisionLabel PL ON PL.ProvisionLabelID = PR.ProvisionLabelID
LEFT JOIN tblProvisionValue PV ON PV.ProvisionValueID = PR.ProvisionValueID
GO
GRANT SELECT ON  [dbo].[vProvisions] TO [base_user]
GO
