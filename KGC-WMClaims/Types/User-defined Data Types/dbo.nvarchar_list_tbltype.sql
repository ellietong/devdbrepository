CREATE TYPE [dbo].[nvarchar_list_tbltype] AS TABLE
(
[id] [int] NOT NULL,
[n] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
