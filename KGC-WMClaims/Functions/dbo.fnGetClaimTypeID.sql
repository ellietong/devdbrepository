SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnGetClaimTypeID]
(
	@AllocationType nvarchar(max), @AllocationName nvarchar(max)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @RetVal int
	
	--DECLARE @AllocationType nvarchar(MAX) = 'Presumed Operations and Products/Completed Operations'
	--DECLARE @AllocationName nvarchar(MAX) = 'Cost Share Allocation: Swindell'
	
	SET @RetVal = (SELECT MatterID FROM tblMatter WHERE FullMatterName LIKE '%' + REPLACE(@AllocationName, 'Cost Share Allocation: ', '') + '%' + REPLACE(@AllocationType, 'Presumed Operations and Products/', '') + '%')
	
	RETURN @RetVal

END
GO
