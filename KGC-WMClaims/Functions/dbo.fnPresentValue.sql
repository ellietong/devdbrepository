SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    FUNCTION [dbo].[fnPresentValue] 
(@Amount money, @Rate decimal(18,15), @PayDate datetime, 
@TodaysDate datetime, @PastInterest bit)
RETURNS Decimal(38, 15)
AS
BEGIN

	DECLARE @DaysToPV decimal (38,15)
	DECLARE @PV decimal(38,15)
	DECLARE @Amount1 decimal (38,15)
	SET @Amount1 = CAST(@amount as  decimal (38,15))
	---------------------------------------------------------------
	--Determine if the equation will need to calculate interest.
	---------------------------------------------------------------
	IF @PastInterest = 1
	--Calculate Interest 
	--DateDiff will present a negative number if the PayDate occurred prior to TodaysDate.
		SET @DaysToPV = DATEDIFF(d, @TodaysDate, @PayDate)
	ELSE
	--Don't Calculate Interest
		IF @PayDate < @TodaysDate
			SET @DaysToPV = 0
		ELSE
			SET @DaysToPV=DATEDIFF(d, @TodaysDate, @PayDate)
	---------------------------------------------------------------
	--Run the Equation and Determine the PV Value.
	---------------------------------------------------------------
	IF @Rate = 0 or @rate = -1
		SET @PV = @Amount1
	Else
		SET @PV = @Amount1/(power((1+(cast(@rate as decimal(18,15))/365)), @DaysToPV))
	RETURN @PV
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnPresentValue] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnPresentValue] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnPresentValue] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnPresentValue] TO [power_user]
GO
