SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnGetTenderBlock]
(
	@MaterialID int, @StartDate date
)
RETURNS int
AS
BEGIN
	--DECLARE @MaterialID int
	--SET @MaterialID = 3

	--DECLARE @StartDate datetime
	--SET @StartDate = '4/1/1983'
	
	DECLARE @RetVal int

	IF(@StartDate = '4/1/1983' OR @StartDate IS NULL)
	BEGIN
		--hardcoded for 1983 case -- no specific date but year = 1983
		SET @RetVal = 4
	END
	ELSE
	BEGIN
		SET @RetVal = (SELECT TOP 1 TenderBlockID FROM tblTenderRules tr
				WHERE @StartDate BETWEEN tr.StartDate AND tr.EndDate 	
				AND  AllowAsbestos = (SELECT ContainsAsbestos FROM tblMaterial WHERE MaterialID = @MaterialID))
	END
	
	RETURN @RetVal
END
GO
