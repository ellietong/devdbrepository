CREATE TABLE [dbo].[tblClaimantSuffix]
(
[SuffixID] [int] NOT NULL IDENTITY(1, 1),
[Suffix] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSuffix] ADD CONSTRAINT [PK_mBtblClaimantSuffix] PRIMARY KEY CLUSTERED  ([SuffixID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimantSuffix] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSuffix] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimantSuffix] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantSuffix] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantSuffix] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantSuffix] TO [Claims_User]
GO
