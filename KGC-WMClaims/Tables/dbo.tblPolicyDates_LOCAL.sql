CREATE TABLE [dbo].[tblPolicyDates_LOCAL]
(
[PolicyDateID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NOT NULL,
[PStartDate] [datetime] NOT NULL,
[PEndDate] [datetime] NOT NULL,
[ActualPStartDate] [datetime] NOT NULL,
[ActualPEndDate] [datetime] NOT NULL,
[AnnualPeriod] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyDates_LOCAL] ADD CONSTRAINT [PK_tblPolicyDates] PRIMARY KEY CLUSTERED  ([PolicyDateID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPolicyDates_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyDates_LOCAL] TO [Claims_User]
GO
