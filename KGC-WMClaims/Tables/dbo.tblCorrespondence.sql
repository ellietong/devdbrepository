CREATE TABLE [dbo].[tblCorrespondence]
(
[CorrespondenceID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceDate] [datetime] NULL,
[CorrespondenceTypeID] [int] NULL,
[CorrespondenceModeID] [int] NULL,
[Notes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attachments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConfirmReceipt] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorrespondence] ADD CONSTRAINT [PK_tblCorrespondence] PRIMARY KEY CLUSTERED  ([CorrespondenceID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCorrespondence] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorrespondence] TO [Claims_User]
GO
