CREATE TABLE [dbo].[tblRustInsurerIndemnityCap]
(
[RustInsurerIndemnityCapID] [int] NOT NULL IDENTITY(1, 1),
[Insurer] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndemnityCap] [money] NULL,
[OutsideImpairment] [money] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRustInsurerIndemnityCap] ADD CONSTRAINT [PK_tblRustInsurerIndemnityCap] PRIMARY KEY CLUSTERED  ([RustInsurerIndemnityCapID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblRustInsurerIndemnityCap] TO [base_user]
GRANT INSERT ON  [dbo].[tblRustInsurerIndemnityCap] TO [base_user]
GRANT DELETE ON  [dbo].[tblRustInsurerIndemnityCap] TO [base_user]
GRANT UPDATE ON  [dbo].[tblRustInsurerIndemnityCap] TO [base_user]
GRANT SELECT ON  [dbo].[tblRustInsurerIndemnityCap] TO [power_user]
GRANT INSERT ON  [dbo].[tblRustInsurerIndemnityCap] TO [power_user]
GRANT DELETE ON  [dbo].[tblRustInsurerIndemnityCap] TO [power_user]
GRANT UPDATE ON  [dbo].[tblRustInsurerIndemnityCap] TO [power_user]
GO
