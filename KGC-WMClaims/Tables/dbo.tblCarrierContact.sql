CREATE TABLE [dbo].[tblCarrierContact]
(
[CarrierContactID] [int] NOT NULL IDENTITY(1, 1),
[CarrierID] [int] NOT NULL,
[ContactID] [int] NOT NULL,
[CurrentContact] [bit] NOT NULL CONSTRAINT [DF_tblCarrierContact_CurrentContact] DEFAULT ((0)),
[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCarrierContact] ADD CONSTRAINT [PK_tblCarrierContact] PRIMARY KEY CLUSTERED  ([CarrierContactID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCarrierContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblCarrierContact] TO [Claims_User]
GO
