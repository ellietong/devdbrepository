CREATE TABLE [dbo].[tblSavedState]
(
[SavedStateSearchID] [bigint] NOT NULL IDENTITY(1, 1),
[StateID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedState] ADD CONSTRAINT [PK_tblSavedStateSearch] PRIMARY KEY CLUSTERED  ([SavedStateSearchID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSavedState] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedState] TO [Claims_User]
GO
