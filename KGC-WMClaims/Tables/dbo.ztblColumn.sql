CREATE TABLE [dbo].[ztblColumn]
(
[ColumnID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[ColumnStartDate] [datetime] NULL,
[ColumnEndDate] [datetime] NULL,
[ColumnDays] [int] NULL,
[ColumnPerOcc] [money] NULL,
[ColumnCumAllocation] [money] NULL,
[InsuranceProgramID] [int] NULL,
[VerticalUncollapse] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblColumn] ADD CONSTRAINT [PK_ztblColumn] PRIMARY KEY NONCLUSTERED  ([ColumnID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ztblColumn] TO [base_user]
GRANT SELECT ON  [dbo].[ztblColumn] TO [Claims_User]
GO
