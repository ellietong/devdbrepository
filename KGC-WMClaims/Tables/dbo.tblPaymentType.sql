CREATE TABLE [dbo].[tblPaymentType]
(
[PaymentTypeID] [int] NOT NULL IDENTITY(1, 1),
[PaymentType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPaymentType] ADD CONSTRAINT [PK_tblPaymentType] PRIMARY KEY CLUSTERED  ([PaymentTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPaymentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblPaymentType] TO [Claims_User]
GO
