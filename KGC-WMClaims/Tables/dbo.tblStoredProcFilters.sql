CREATE TABLE [dbo].[tblStoredProcFilters]
(
[StoredProcID] [int] NOT NULL,
[FilterID] [int] NULL,
[ID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblStoredProcFilters] ADD CONSTRAINT [PK_TblStoredProcFilters] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblStoredProcFilters] TO [base_user]
GRANT SELECT ON  [dbo].[tblStoredProcFilters] TO [Claims_User]
GO
