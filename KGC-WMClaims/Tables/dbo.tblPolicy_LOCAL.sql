CREATE TABLE [dbo].[tblPolicy_LOCAL]
(
[PolicyID] [int] NOT NULL IDENTITY(1, 1),
[MinPStartDate] [datetime] NULL,
[CarrierID] [int] NOT NULL,
[NamedInsuredID] [int] NOT NULL,
[InsuranceProgramID] [int] NOT NULL,
[PolicyNum] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PolicyNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyReviewed] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyPDF] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitialDataEntry] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitialDataEntryDate] [datetime] NULL,
[PolicyReview] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyReviewDate] [datetime] NULL,
[PolPicPDFLink] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoNotReport] [bit] NOT NULL CONSTRAINT [DF_tblPolicy_DoNotReport] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicy_LOCAL] ADD CONSTRAINT [PK_tblPolicy] PRIMARY KEY CLUSTERED  ([PolicyID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPolicy_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicy_LOCAL] TO [Claims_User]
GO
