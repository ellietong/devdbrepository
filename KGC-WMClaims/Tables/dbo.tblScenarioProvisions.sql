CREATE TABLE [dbo].[tblScenarioProvisions]
(
[ScenarioProvisionID] [int] NOT NULL IDENTITY(1, 1),
[ScenarioID] [int] NOT NULL,
[ProvisionLabelID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblScenarioProvisions] ADD CONSTRAINT [PK_tblScenarioProvision] PRIMARY KEY CLUSTERED  ([ScenarioProvisionID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblScenarioProvisions] TO [base_user]
GRANT SELECT ON  [dbo].[tblScenarioProvisions] TO [Claims_User]
GO
