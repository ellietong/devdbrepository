CREATE TABLE [dbo].[tblSubscriptionSection_LOCAL]
(
[SubscriptionSectionID] [int] NOT NULL IDENTITY(1, 1),
[SubscriptionSection] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriptionSection_LOCAL] ADD CONSTRAINT [PK_tblSubscriptionSection] PRIMARY KEY CLUSTERED  ([SubscriptionSectionID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSubscriptionSection_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriptionSection_LOCAL] TO [Claims_User]
GO
