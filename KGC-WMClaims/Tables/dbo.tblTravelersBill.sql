CREATE TABLE [dbo].[tblTravelersBill]
(
[TravelersBillID] [int] NOT NULL IDENTITY(1, 1),
[BillDate] [date] NULL,
[ClaimantName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllocationName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllocationType] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimTypeID] [int] NULL,
[DOFE] [date] NULL,
[PaymentType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementAmount] [money] NULL,
[FirmName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoicedFees] [money] NULL,
[InvoicedExpenses] [money] NULL,
[InvoicedTotal] [money] NULL,
[AdjustmentFees] [money] NULL,
[AdjustmentExpenses] [money] NULL,
[FinalFees] [money] NULL,
[FinalExpenses] [money] NULL,
[FinalTotal] [money] NULL,
[BillStatusID] [int] NOT NULL CONSTRAINT [DF_tblTravelersBill_BillStatusID] DEFAULT ((1)),
[FirmID] [int] NULL,
[InvoiceDocumentID] [int] NULL,
[AllocationQCStatusID] [int] NOT NULL CONSTRAINT [DF_tblTravelersBill_AllocatedInvoiceAmountQC] DEFAULT ((1)),
[AllocatedInvoiceAmountQC] [bit] NULL CONSTRAINT [DF_tblTravelersBill_AllocatedInvoiceAmountQC_1] DEFAULT ((0)),
[PaymentDueDate] [date] NULL CONSTRAINT [DF_tblTravelersBill_PaymentDueDate] DEFAULT ('1/1/2020'),
[AllocationQCConfirmation] [bit] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TravelersBillID_Original] [int] NULL,
[MatterID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[DefenseCounsel] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [bigint] NULL,
[mReportedBilldate] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTravelersBill] ADD CONSTRAINT [PK_tblTravelersBill] PRIMARY KEY CLUSTERED  ([TravelersBillID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_142835_142834_tblTravelersBill] ON [dbo].[tblTravelersBill] ([AllocationQCConfirmation], [BillStatusID]) INCLUDE ([BillDate], [mReportedBilldate], [TravelersBillID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblTravelersBill] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTravelersBill] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblTravelersBill] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblTravelersBill] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblTravelersBill] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblTravelersBill] TO [Claims_User]
GO
