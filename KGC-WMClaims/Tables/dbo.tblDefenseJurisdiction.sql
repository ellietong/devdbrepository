CREATE TABLE [dbo].[tblDefenseJurisdiction]
(
[DefenseJurisdictionID] [int] NOT NULL IDENTITY(1, 1),
[JurisdictionID] [int] NOT NULL,
[DefenseCounselAddressID] [int] NULL,
[CourtTypeID] [int] NULL,
[MatterID] [int] NULL,
[StateID] [int] NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_tblDefenseJurisdiction_IsActive] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseJurisdiction] ADD CONSTRAINT [PK_tblDefenseJurisdiction] PRIMARY KEY CLUSTERED  ([DefenseJurisdictionID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GO
