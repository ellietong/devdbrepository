CREATE TABLE [dbo].[tblMiscCashActivityType]
(
[CashActivityTypeID] [int] NOT NULL IDENTITY(1, 1),
[CashActivityType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMiscCashActivityType] ADD CONSTRAINT [PK_tblMiscCashActivityType] PRIMARY KEY CLUSTERED  ([CashActivityTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblMiscCashActivityType] TO [base_user]
GRANT SELECT ON  [dbo].[tblMiscCashActivityType] TO [Claims_User]
GO
