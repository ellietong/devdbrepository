CREATE TABLE [dbo].[tblCarrier_ALT]
(
[CarrierID] [int] NOT NULL IDENTITY(99999, 1),
[CarrierName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CarrierNickname] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierCurrentName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierSolvency] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierSolvencyStatus] [int] NULL,
[Signatory] [bit] NULL,
[CarrierGroupID] [int] NULL,
[CarrierNote] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SolvencySource] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateUpdated] [smalldatetime] NULL,
[DateInsolvent] [smalldatetime] NULL,
[CarrierSolvencyDetail] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AMBestNo] [int] NULL,
[PrincipalPlaceBusinessCity] [int] NULL,
[PrincipalPlaceBusinessState] [int] NULL,
[PrincipalPlaceBusinessCountry] [int] NULL,
[PolicyHolder] [bit] NULL,
[GraceIgnore] [bit] NULL,
[Expr1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCarrier_ALT] ADD CONSTRAINT [PK_tblCarrier_ALT] PRIMARY KEY CLUSTERED  ([CarrierID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCarrier_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblCarrier_ALT] TO [Claims_User]
GO
