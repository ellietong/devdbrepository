CREATE TABLE [dbo].[tblTaskCategoryLookup]
(
[TaskCategoryID] [int] NOT NULL IDENTITY(1, 1),
[TaskCategory] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTaskCategoryLookup] ADD CONSTRAINT [PK_tblTaskCategoryLookup] PRIMARY KEY CLUSTERED  ([TaskCategoryID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblTaskCategoryLookup] TO [base_user]
GRANT SELECT ON  [dbo].[tblTaskCategoryLookup] TO [Claims_User]
GO
