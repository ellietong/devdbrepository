CREATE TABLE [dbo].[tblSuffix]
(
[SuffixID] [int] NOT NULL IDENTITY(1, 1),
[Suffix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSuffix] ADD CONSTRAINT [PK_tblSuffix] PRIMARY KEY CLUSTERED  ([SuffixID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSuffix] TO [base_user]
GRANT SELECT ON  [dbo].[tblSuffix] TO [Claims_User]
GO
