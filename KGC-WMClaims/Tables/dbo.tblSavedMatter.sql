CREATE TABLE [dbo].[tblSavedMatter]
(
[SavedMatterID] [bigint] NOT NULL IDENTITY(1, 1),
[MatterID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedMatter] ADD CONSTRAINT [PK_tblSavedMatter] PRIMARY KEY CLUSTERED  ([SavedMatterID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSavedMatter] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedMatter] TO [Claims_User]
GO
