CREATE TABLE [dbo].[tblCorpHistoryPurchaseType]
(
[PurchaseTypeID] [int] NOT NULL IDENTITY(1, 1),
[PurchaseType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorpHistoryPurchaseType] ADD CONSTRAINT [PK_mEtblCorpHistoryPurchaseType] PRIMARY KEY CLUSTERED  ([PurchaseTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCorpHistoryPurchaseType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorpHistoryPurchaseType] TO [Claims_User]
GO
