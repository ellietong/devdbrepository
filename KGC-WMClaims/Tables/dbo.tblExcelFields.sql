CREATE TABLE [dbo].[tblExcelFields]
(
[ExcelFieldID] [int] NOT NULL IDENTITY(1, 1),
[FieldName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column] [int] NULL,
[Row] [int] NULL,
[FileTypeID] [int] NULL,
[Sheet] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExcelFields] ADD CONSTRAINT [PK_tblExcelFields] PRIMARY KEY CLUSTERED  ([ExcelFieldID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblExcelFields] TO [base_user]
GRANT SELECT ON  [dbo].[tblExcelFields] TO [Claims_User]
GO
