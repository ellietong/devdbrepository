CREATE TABLE [dbo].[tblDiscountProvisionByOccType]
(
[DiscountProvisionID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionID] [int] NULL,
[ProvisionValueID] [int] NULL,
[ProvisionIDProvisionValueID] [int] NULL,
[ProvisionOccTypeDiscount] [money] NULL,
[OccurrenceTypeID] [int] NULL,
[discountid] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountProvisionByOccType] ADD CONSTRAINT [PK_tblDiscountProvisionByOccType] PRIMARY KEY CLUSTERED  ([DiscountProvisionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountProvisionByOccType] ADD CONSTRAINT [FK_tblDiscountProvisionByOccType_tblOccurrenceType] FOREIGN KEY ([OccurrenceTypeID]) REFERENCES [dbo].[tblOccurrenceType] ([OccurrenceTypeID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblDiscountProvisionByOccType] ADD CONSTRAINT [FK_tblDiscountProvisionByOccType_tblProvisionValue] FOREIGN KEY ([ProvisionValueID]) REFERENCES [dbo].[tblProvisionValue_LOCAL] ([ProvisionValueID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblDiscountProvisionByOccType] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiscountProvisionByOccType] TO [Claims_User]
GO
