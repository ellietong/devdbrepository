CREATE TABLE [dbo].[tblAllocationOccurrenceGroupInclude]
(
[OccurrenceGroupIncludeID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[OccurrenceGroupID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationOccurrenceGroupInclude] ADD CONSTRAINT [PK_tblAllocationOccurrenceGroupInclude] PRIMARY KEY CLUSTERED  ([OccurrenceGroupIncludeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationOccurrenceGroupInclude] ADD CONSTRAINT [FK_tblAllocationOccurrenceGroupInclude_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceGroupInclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceGroupInclude] TO [Claims_User]
GO
