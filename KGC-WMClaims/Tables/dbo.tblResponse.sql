CREATE TABLE [dbo].[tblResponse]
(
[ResponseID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NULL,
[NoticeResponseID] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblResponse] ADD CONSTRAINT [PK_tblResponse] PRIMARY KEY CLUSTERED  ([ResponseID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblResponse] ADD CONSTRAINT [FK_tblResponse_tblCorrespondence] FOREIGN KEY ([CorrespondenceID]) REFERENCES [dbo].[tblCorrespondence] ([CorrespondenceID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblResponse] TO [base_user]
GRANT SELECT ON  [dbo].[tblResponse] TO [Claims_User]
GO
