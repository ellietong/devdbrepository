CREATE TABLE [dbo].[tblClaimantDocumentType]
(
[ClaimantDocumentTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClaimantDocumentType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentGroupID] [int] NULL,
[ClaimantDocumentTypeID_Original] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantDocumentType] ADD CONSTRAINT [PK_mBtblClaimantDocumentType] PRIMARY KEY CLUSTERED  ([ClaimantDocumentTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimantDocumentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantDocumentType] TO [Claims_User]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Orientation', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1000000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'ClaimantDocumentType'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'ClaimantDocumentType'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'ClaimantDocumentType'
GO
DECLARE @xp int
SELECT @xp=4200
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'ClaimantDocumentType'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'ClaimantDocumentType'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'ClaimantDocumentTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'ClaimantDocumentTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'ClaimantDocumentTypeID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'ClaimantDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'ClaimantDocumentTypeID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'DocumentGroupID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'DocumentGroupID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'DocumentGroupID'
GO
DECLARE @xp int
SELECT @xp=2145
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'DocumentGroupID'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantDocumentType', 'COLUMN', N'DocumentGroupID'
GO
