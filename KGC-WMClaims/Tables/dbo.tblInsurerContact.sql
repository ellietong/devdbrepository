CREATE TABLE [dbo].[tblInsurerContact]
(
[MatterID] [int] NULL,
[CarrierID] [int] NULL,
[FirstName] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblInsurerContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblInsurerContact] TO [Claims_User]
GO
