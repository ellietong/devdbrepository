CREATE TABLE [dbo].[tblRecipientType]
(
[RecipientTypeID] [int] NOT NULL IDENTITY(1, 1),
[RecipientType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRecipientType] ADD CONSTRAINT [PK_tblRecipientType] PRIMARY KEY CLUSTERED  ([RecipientTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblRecipientType] TO [base_user]
GRANT SELECT ON  [dbo].[tblRecipientType] TO [Claims_User]
GO
