CREATE TABLE [dbo].[tblCarrierGroup_ALT]
(
[CarrierGroupID] [int] NOT NULL IDENTITY(1, 1),
[CarrierGroupName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarrierGroupNote] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCarrierGroup_ALT] ADD CONSTRAINT [PK_tblCarrierGroup] PRIMARY KEY CLUSTERED  ([CarrierGroupID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCarrierGroup_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblCarrierGroup_ALT] TO [Claims_User]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderByOn', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2147483647
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_ColumnHidden', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', 'COLUMN', N'CarrierGroupID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', 'COLUMN', N'CarrierGroupID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', 'COLUMN', N'CarrierGroupID'
GO
EXEC sp_addextendedproperty N'MS_ColumnHidden', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', 'COLUMN', N'CarrierGroupName'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', 'COLUMN', N'CarrierGroupName'
GO
DECLARE @xp int
SELECT @xp=3105
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', 'COLUMN', N'CarrierGroupName'
GO
EXEC sp_addextendedproperty N'MS_ColumnHidden', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', 'COLUMN', N'CarrierGroupNote'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', 'COLUMN', N'CarrierGroupNote'
GO
DECLARE @xp int
SELECT @xp=18585
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblCarrierGroup_ALT', 'COLUMN', N'CarrierGroupNote'
GO
