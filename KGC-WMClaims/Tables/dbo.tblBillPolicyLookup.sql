CREATE TABLE [dbo].[tblBillPolicyLookup]
(
[BillPolicyLookupID] [int] NOT NULL IDENTITY(1, 1),
[Company] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateRange] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyID] [int] NULL,
[Type] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Program] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillPolicyLookupID_Original] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillPolicyLookup] ADD CONSTRAINT [PK_tblBillPolicyLookup] PRIMARY KEY CLUSTERED  ([BillPolicyLookupID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblBillPolicyLookup] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillPolicyLookup] TO [Claims_User]
GO
