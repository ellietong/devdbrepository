CREATE TABLE [dbo].[tblProvisionGenericLanguage]
(
[ProvisionGenericLanguageID] [int] NOT NULL IDENTITY(1, 1),
[GenericLanguageID] [int] NULL,
[ProvisionLabelID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionGenericLanguage] ADD CONSTRAINT [PK_tblProvisionGenericLanguage] PRIMARY KEY CLUSTERED  ([ProvisionGenericLanguageID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblProvisionGenericLanguage] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionGenericLanguage] TO [Claims_User]
GO
