CREATE TABLE [dbo].[tblPV]
(
[PVID] [int] NOT NULL IDENTITY(1, 1),
[Method] [int] NULL,
[PVRate] [real] NULL,
[DateToPresentValue] [datetime] NULL,
[YieldCurveID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPV] ADD CONSTRAINT [PK_tblPV] PRIMARY KEY CLUSTERED  ([PVID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPV] TO [base_user]
GRANT SELECT ON  [dbo].[tblPV] TO [Claims_User]
GO
