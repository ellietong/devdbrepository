CREATE TABLE [dbo].[tblSupportingDocument]
(
[SupportingDocumentID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NOT NULL,
[SupportingDocumentTypeID] [int] NULL,
[Description] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InternalLink] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSupportingDocument] ADD CONSTRAINT [PK_tblSupportingDocument] PRIMARY KEY CLUSTERED  ([SupportingDocumentID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSupportingDocument] TO [base_user]
GRANT SELECT ON  [dbo].[tblSupportingDocument] TO [Claims_User]
GO
