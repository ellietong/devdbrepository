CREATE TABLE [dbo].[tblProvisionValue_LOCAL]
(
[ProvisionValueID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionLabelID] [int] NOT NULL,
[ProvisionValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionValue_LOCAL] ADD CONSTRAINT [PK__tblProvisionValu__24927208] PRIMARY KEY CLUSTERED  ([ProvisionValueID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionValue_LOCAL] ADD CONSTRAINT [FK_tblProvisionValue_tblProvisionLabel] FOREIGN KEY ([ProvisionLabelID]) REFERENCES [dbo].[tblProvisionLabel_LOCAL] ([ProvisionLabelID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblProvisionValue_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionValue_LOCAL] TO [Claims_User]
GO
