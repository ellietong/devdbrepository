CREATE TABLE [dbo].[tblClaim]
(
[ClaimID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[IsCurrentClaim] [bit] NULL,
[ClaimSequenceID] [int] NULL,
[FullyDocumented] [bit] NULL,
[CauseOfInjuryID] [int] NULL,
[MatterID] [int] NULL,
[CLM_KEY] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILODoctorID] [int] NULL,
[ILOProfusion] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILODate] [datetime] NULL,
[PFTDoctorID] [int] NULL,
[PFTDate] [datetime] NULL,
[PFTTypeID] [int] NULL,
[PFTFVC_Actual] [float] NULL,
[PFTFVC_Predicted] [float] NULL,
[PFTFEV1_Actual] [float] NULL,
[PFTFEV1_Predicted] [float] NULL,
[PFTFEV1AndFVC_Actual] [float] NULL,
[PFTFEV1AndFVC_Predicted] [float] NULL,
[PFTTLC_Actual] [float] NULL,
[PFTTLC_Predicted] [float] NULL,
[PFTDLCO_Actual] [float] NULL,
[PFTDLCO_Predicted] [float] NULL,
[PFTTV] [float] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReferenceID] [int] NULL,
[ReferenceNote] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [PK_mBtblClaim] PRIMARY KEY CLUSTERED  ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_641136_641135_tblClaim] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_643734_643733_tblClaim] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID]) INCLUDE ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_705303_705302_tblClaim] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID]) INCLUDE ([ClaimID], [MatterID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_642531_642530_tblClaim] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID], [IsCurrentClaim], [MatterID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_642529_642528_tblClaim] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID], [IsCurrentClaim], [MatterID]) INCLUDE ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_802823_802822_tblClaim] ON [dbo].[tblClaim] ([MatterID]) INCLUDE ([ClaimantPersonalInfoID], [ClaimID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaim] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaim] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaim] TO [DeleteClaims]
GO
