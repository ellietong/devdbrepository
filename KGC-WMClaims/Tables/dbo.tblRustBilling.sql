CREATE TABLE [dbo].[tblRustBilling]
(
[RustBillingID] [int] NOT NULL IDENTITY(1, 1),
[ClaimID] [int] NULL,
[BillTypeID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[InvoicePeriod] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [date] NULL,
[Fees] [money] NULL,
[Expenses] [money] NULL,
[TotalInvoiced] [money] NULL,
[Adjustment] [money] NULL,
[FinalBilled] [money] NULL,
[DateBilled] [date] NULL,
[HartfordShare] [money] NULL,
[TravelersShare] [money] NULL,
[ResoluteShare] [money] NULL,
[WMShare] [money] NULL,
[NationwideShare] [money] NULL,
[HartfordPaid] [money] NULL,
[TravelersPaid] [money] NULL,
[ResolutePaid] [money] NULL,
[WMPaid] [money] NULL,
[NationwidePaid] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRustBilling] ADD CONSTRAINT [PK_tblRustBilling] PRIMARY KEY CLUSTERED  ([RustBillingID]) ON [PRIMARY]
GO
GRANT ALTER ON  [dbo].[tblRustBilling] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblRustBilling] TO [base_user]
GRANT SELECT ON  [dbo].[tblRustBilling] TO [base_user]
GRANT INSERT ON  [dbo].[tblRustBilling] TO [base_user]
GRANT DELETE ON  [dbo].[tblRustBilling] TO [base_user]
GRANT UPDATE ON  [dbo].[tblRustBilling] TO [base_user]
GRANT ALTER ON  [dbo].[tblRustBilling] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblRustBilling] TO [power_user]
GRANT INSERT ON  [dbo].[tblRustBilling] TO [power_user]
GRANT DELETE ON  [dbo].[tblRustBilling] TO [power_user]
GRANT UPDATE ON  [dbo].[tblRustBilling] TO [power_user]
GO
