CREATE TABLE [dbo].[tblBillStatus]
(
[BillStatusID] [int] NOT NULL IDENTITY(1, 1),
[Status] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillStatus] ADD CONSTRAINT [PK_tblBillStatus] PRIMARY KEY CLUSTERED  ([BillStatusID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblBillStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillStatus] TO [Claims_User]
GO
