CREATE TABLE [dbo].[tblDamageOLD]
(
[DamageID] [int] NOT NULL IDENTITY(1, 1),
[Damage] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDamageOLD] ADD CONSTRAINT [PK_mBtblDamage] PRIMARY KEY CLUSTERED  ([DamageID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDamageOLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblDamageOLD] TO [Claims_User]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDamageOLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDamageOLD', 'COLUMN', N'Damage'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDamageOLD', 'COLUMN', N'Damage'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDamageOLD', 'COLUMN', N'Damage'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDamageOLD', 'COLUMN', N'DamageID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDamageOLD', 'COLUMN', N'DamageID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDamageOLD', 'COLUMN', N'DamageID'
GO
