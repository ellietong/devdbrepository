CREATE TABLE [dbo].[tblAllocation_import]
(
[Cost Share Allocation: Kellogg] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F2] [datetime] NULL,
[F3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[F6] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblAllocation_import] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocation_import] TO [Claims_User]
GO
