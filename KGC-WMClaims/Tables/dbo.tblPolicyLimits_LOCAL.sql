CREATE TABLE [dbo].[tblPolicyLimits_LOCAL]
(
[LimitID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NULL,
[LimitTypeID] [int] NULL,
[AttachmentPoint] [money] NULL CONSTRAINT [DF_tblPolicyLimits_AttachmentPoint] DEFAULT ((0)),
[PerOccLimit] [money] NULL CONSTRAINT [DF_tblPolicyLimits_PerOccLimit] DEFAULT ((0)),
[LayerPerOccLimit] [money] NULL CONSTRAINT [DF_tblPolicyLimits_LayerPerOccLimit] DEFAULT ((0)),
[SIR] [money] NULL CONSTRAINT [DF_tblPolicyLimits_SIR] DEFAULT ((0)),
[SIRAggregateLimit] [money] NULL CONSTRAINT [DF_tblPolicyLimits_SIRAggregateLimit] DEFAULT ((0)),
[AggregateLimit] [money] NULL CONSTRAINT [DF_tblPolicyLimits_AggregateLimit] DEFAULT ((0)),
[PreviouslyExhaustedAggregate] [money] NULL CONSTRAINT [DF_tblPolicyLimits_PreviouslyExhaustedAggregate] DEFAULT ((0)),
[Layer] [tinyint] NULL CONSTRAINT [DF_tblPolicyLimits_Layer] DEFAULT ((0)),
[LimitNote] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyLimits_LOCAL] ADD CONSTRAINT [PK_tblPolicyLimits] PRIMARY KEY CLUSTERED  ([LimitID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyLimits_LOCAL] ADD CONSTRAINT [FK_tblPolicyLimits_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy_LOCAL] ([PolicyID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblPolicyLimits_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyLimits_LOCAL] TO [Claims_User]
GO
