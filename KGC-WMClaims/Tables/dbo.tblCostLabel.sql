CREATE TABLE [dbo].[tblCostLabel]
(
[CostID] [int] NOT NULL IDENTITY(1, 1),
[CostDescription] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCostLabel] ADD CONSTRAINT [PK_tblCostLabel] PRIMARY KEY NONCLUSTERED  ([CostID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCostLabel] TO [base_user]
GRANT SELECT ON  [dbo].[tblCostLabel] TO [Claims_User]
GO
