CREATE TABLE [dbo].[tblSubscriberPolicyType]
(
[PolicyTypeID] [int] NOT NULL IDENTITY(1, 1),
[PolicyType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriberPolicyType] ADD CONSTRAINT [PK_tblSubscriberPolicyType] PRIMARY KEY CLUSTERED  ([PolicyTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSubscriberPolicyType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriberPolicyType] TO [Claims_User]
GO
