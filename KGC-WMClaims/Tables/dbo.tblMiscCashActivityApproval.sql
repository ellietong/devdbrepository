CREATE TABLE [dbo].[tblMiscCashActivityApproval]
(
[MiscCashActivityApprovalID] [bigint] NOT NULL IDENTITY(1, 1),
[MiscCashActivityID] [bigint] NULL,
[IsApproved] [bit] NULL,
[ApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMiscCashActivityApproval] ADD CONSTRAINT [PK_tblMiscCashActivityApproval] PRIMARY KEY CLUSTERED  ([MiscCashActivityApprovalID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblMiscCashActivityApproval] TO [base_user]
GRANT SELECT ON  [dbo].[tblMiscCashActivityApproval] TO [Claims_User]
GO
