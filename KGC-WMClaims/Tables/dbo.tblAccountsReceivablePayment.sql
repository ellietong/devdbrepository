CREATE TABLE [dbo].[tblAccountsReceivablePayment]
(
[AccountsReceivablePaymentID] [int] NOT NULL IDENTITY(1, 1),
[AccountsReceivableID] [int] NULL,
[PaymentID] [int] NULL,
[Amount] [money] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAccountsReceivablePayment] ADD CONSTRAINT [PK_tblAccountsReceivablePayment] PRIMARY KEY CLUSTERED  ([AccountsReceivablePaymentID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblAccountsReceivablePayment] TO [base_user]
GRANT SELECT ON  [dbo].[tblAccountsReceivablePayment] TO [Claims_User]
GO
