CREATE TABLE [dbo].[tblExhaustionType_LOCAL]
(
[ExhaustionTypeID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustionType_LOCAL] ADD CONSTRAINT [PK_tblExhaustionType] PRIMARY KEY CLUSTERED  ([ExhaustionTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblExhaustionType_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustionType_LOCAL] TO [Claims_User]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType_LOCAL', 'COLUMN', N'ExhaustionType'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType_LOCAL', 'COLUMN', N'ExhaustionType'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType_LOCAL', 'COLUMN', N'ExhaustionType'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType_LOCAL', 'COLUMN', N'ExhaustionTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType_LOCAL', 'COLUMN', N'ExhaustionTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionType_LOCAL', 'COLUMN', N'ExhaustionTypeID'
GO
