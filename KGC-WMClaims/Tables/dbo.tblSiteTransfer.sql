CREATE TABLE [dbo].[tblSiteTransfer]
(
[TransferID] [int] NOT NULL IDENTITY(1, 1),
[SiteID] [int] NULL,
[CorpHistoryID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSiteTransfer] ADD CONSTRAINT [PK_mEtblSiteTransfer] PRIMARY KEY CLUSTERED  ([TransferID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSiteTransfer] ADD CONSTRAINT [FK_tblSiteTransfer_tblEnviroSitesDetail] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[tblEnviroSitesDetail] ([SiteID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblSiteTransfer] TO [base_user]
GRANT SELECT ON  [dbo].[tblSiteTransfer] TO [Claims_User]
GO
