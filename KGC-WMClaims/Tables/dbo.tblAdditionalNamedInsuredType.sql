CREATE TABLE [dbo].[tblAdditionalNamedInsuredType]
(
[AdditionalNamedInsTypeID] [int] NOT NULL IDENTITY(1, 1),
[AdditonalNamedInsType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAdditionalNamedInsuredType] ADD CONSTRAINT [PK_tblAdditionalNamedInsuredType] PRIMARY KEY CLUSTERED  ([AdditionalNamedInsTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblAdditionalNamedInsuredType] TO [base_user]
GRANT SELECT ON  [dbo].[tblAdditionalNamedInsuredType] TO [Claims_User]
GO
