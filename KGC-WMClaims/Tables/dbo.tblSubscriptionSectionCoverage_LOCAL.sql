CREATE TABLE [dbo].[tblSubscriptionSectionCoverage_LOCAL]
(
[SubscriptionSectionCoverageID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NOT NULL,
[SubscriptionSectionID] [int] NOT NULL,
[SubscriptionSectionPercentage] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriptionSectionCoverage_LOCAL] ADD CONSTRAINT [PK_tblSubscriptionSectionCoverage] PRIMARY KEY CLUSTERED  ([SubscriptionSectionCoverageID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSubscriptionSectionCoverage_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriptionSectionCoverage_LOCAL] TO [Claims_User]
GO
