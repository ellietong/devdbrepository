CREATE TABLE [dbo].[tblAllocationProvisionToExclude]
(
[ProvisionExcludeID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[ProvisionLabelID] [int] NULL,
[ProvisionValueID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationProvisionToExclude] ADD CONSTRAINT [PK_tblAllocationProvisionToExclude] PRIMARY KEY NONCLUSTERED  ([ProvisionExcludeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationProvisionToExclude] ADD CONSTRAINT [FK_tblAllocationProvisionToExclude_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblAllocationProvisionToExclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationProvisionToExclude] TO [Claims_User]
GO
