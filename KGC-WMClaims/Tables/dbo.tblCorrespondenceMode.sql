CREATE TABLE [dbo].[tblCorrespondenceMode]
(
[CorrespondenceModeID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceMode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorrespondenceMode] ADD CONSTRAINT [PK_tblCorrespondenceMode] PRIMARY KEY CLUSTERED  ([CorrespondenceModeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCorrespondenceMode] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorrespondenceMode] TO [Claims_User]
GO
