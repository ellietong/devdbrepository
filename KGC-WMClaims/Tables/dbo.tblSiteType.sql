CREATE TABLE [dbo].[tblSiteType]
(
[SiteTypeID] [int] NOT NULL IDENTITY(1, 1),
[SiteType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSiteType] ADD CONSTRAINT [PK_tblSiteType] PRIMARY KEY CLUSTERED  ([SiteTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSiteType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSiteType] TO [Claims_User]
GO
