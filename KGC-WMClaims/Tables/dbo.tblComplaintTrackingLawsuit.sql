CREATE TABLE [dbo].[tblComplaintTrackingLawsuit]
(
[ComplaintTrackingLawsuitID] [int] NOT NULL IDENTITY(1, 1),
[ComplaintTrackingID] [int] NULL,
[LawsuitID] [int] NULL,
[ClaimLawsuitID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblComplaintTrackingLawsuit] ADD CONSTRAINT [PK_tblComplaintTrackingLawsuit] PRIMARY KEY CLUSTERED  ([ComplaintTrackingLawsuitID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblComplaintTrackingLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblComplaintTrackingLawsuit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblComplaintTrackingLawsuit] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblComplaintTrackingLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblComplaintTrackingLawsuit] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblComplaintTrackingLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblComplaintTrackingLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblComplaintTrackingLawsuit] TO [DeleteClaims]
GO
