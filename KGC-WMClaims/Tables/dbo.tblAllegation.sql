CREATE TABLE [dbo].[tblAllegation]
(
[AllegationID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[DefendantCompanyID] [int] NULL,
[AllegationTypeID] [int] NULL,
[DamageID] [int] NULL,
[MaterialID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegation] ADD CONSTRAINT [PK__tblAllegation1__414EAC47] PRIMARY KEY CLUSTERED  ([AllegationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegation] ADD CONSTRAINT [FK_tblAllegation_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblAllegation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblAllegation] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblAllegation] TO [DeleteClaims]
GO
