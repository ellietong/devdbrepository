CREATE TABLE [dbo].[ztblPolicyDefense]
(
[PolicyDefenseID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NOT NULL,
[AllocationID] [int] NOT NULL,
[ScenarioID] [int] NOT NULL,
[Provision1] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblPolicyDefense] ADD CONSTRAINT [PK_ztblPolicyDefense] PRIMARY KEY CLUSTERED  ([PolicyDefenseID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ztblPolicyDefense] TO [base_user]
GRANT SELECT ON  [dbo].[ztblPolicyDefense] TO [Claims_User]
GO
