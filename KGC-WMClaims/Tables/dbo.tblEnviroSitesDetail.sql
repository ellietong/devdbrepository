CREATE TABLE [dbo].[tblEnviroSitesDetail]
(
[SiteID] [int] NOT NULL IDENTITY(1, 1),
[SiteName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteAddress] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteCity] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[ZipCode] [bigint] NULL,
[CountryID] [int] NULL,
[OwnBegDate] [datetime] NULL,
[OwnEndDate] [datetime] NULL,
[OpBegDate] [datetime] NULL,
[OpEndDate] [datetime] NULL,
[ContaminationDate] [datetime] NULL,
[TriggerStartDate] [datetime] NULL,
[TriggerEndDate] [datetime] NULL,
[SiteActivityID] [int] NULL,
[SiteTypeID] [int] NULL,
[StatusID] [int] NULL,
[SiteNotes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcludedSite] [bit] NULL,
[ExcludedSiteReason] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CorporateHistoryID] [int] NULL,
[Division] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LiabilityShare] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEnviroSitesDetail] ADD CONSTRAINT [PK_mEtblEnviroSitesDetail] PRIMARY KEY CLUSTERED  ([SiteID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblEnviroSitesDetail] TO [base_user]
GRANT SELECT ON  [dbo].[tblEnviroSitesDetail] TO [Claims_User]
GO
