CREATE TABLE [dbo].[dtproperties]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[objectid] [int] NULL,
[property] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[value] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[uvalue] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[lvalue] [image] NULL,
[version] [int] NOT NULL CONSTRAINT [DF__dtpropert__versi__188D592D] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[dtproperties] ADD CONSTRAINT [pk_dtproperties] PRIMARY KEY CLUSTERED  ([id], [property]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[dtproperties] TO [Claims_User]
GRANT REFERENCES ON  [dbo].[dtproperties] TO [public]
GRANT SELECT ON  [dbo].[dtproperties] TO [public]
GRANT INSERT ON  [dbo].[dtproperties] TO [public]
GRANT DELETE ON  [dbo].[dtproperties] TO [public]
GRANT UPDATE ON  [dbo].[dtproperties] TO [public]
GO
