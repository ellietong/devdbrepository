CREATE TABLE [dbo].[tblMatter]
(
[MatterID] [int] NOT NULL IDENTITY(1, 1),
[Matter] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullMatterName] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsurerNotificationText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMatter] ADD CONSTRAINT [PK_tblMatter] PRIMARY KEY CLUSTERED  ([MatterID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblMatter] TO [base_user]
GRANT INSERT ON  [dbo].[tblMatter] TO [base_user]
GRANT UPDATE ON  [dbo].[tblMatter] TO [base_user]
GRANT SELECT ON  [dbo].[tblMatter] TO [Claims_User]
GO
