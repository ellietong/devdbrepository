CREATE TABLE [dbo].[tblDefenseInvoice]
(
[DefenseInvoiceID] [bigint] NOT NULL IDENTITY(1, 1),
[DocumentID] [bigint] NULL,
[StatusID] [int] NULL,
[FirmID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[VendorFirmID] [int] NULL,
[InvoiceNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubmitDate] [datetime] NULL,
[DatePayable] [datetime] NULL,
[InvoiceLink] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateApproved] [datetime] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[InvoiceNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseInvoice] ADD CONSTRAINT [PK_tblDefenseInvoice_New] PRIMARY KEY CLUSTERED  ([DefenseInvoiceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseInvoice] ADD CONSTRAINT [FK_tblDefenseInvoice_New_tblDefenseInvoice_New] FOREIGN KEY ([DefenseInvoiceID]) REFERENCES [dbo].[tblDefenseInvoice] ([DefenseInvoiceID])
GO
ALTER TABLE [dbo].[tblDefenseInvoice] ADD CONSTRAINT [FK_tblDefenseInvoice_tblDocuments] FOREIGN KEY ([DocumentID]) REFERENCES [dbo].[tblDocuments] ([DocumentID])
GO
GRANT SELECT ON  [dbo].[tblDefenseInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoice] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefenseInvoice] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseInvoice] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseInvoice] TO [Claims_User]
GO
