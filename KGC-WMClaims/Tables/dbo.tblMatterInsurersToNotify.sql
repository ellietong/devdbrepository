CREATE TABLE [dbo].[tblMatterInsurersToNotify]
(
[MatterInsurersToNotifyID] [int] NOT NULL IDENTITY(1, 1),
[MatterID] [int] NULL,
[PersonID] [int] NULL,
[RecipientTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMatterInsurersToNotify] ADD CONSTRAINT [PK_tblMatterInsurersToNotify] PRIMARY KEY CLUSTERED  ([MatterInsurersToNotifyID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblMatterInsurersToNotify] TO [base_user]
GRANT SELECT ON  [dbo].[tblMatterInsurersToNotify] TO [base_user]
GRANT INSERT ON  [dbo].[tblMatterInsurersToNotify] TO [base_user]
GRANT UPDATE ON  [dbo].[tblMatterInsurersToNotify] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMatterInsurersToNotify] TO [power_user]
GRANT SELECT ON  [dbo].[tblMatterInsurersToNotify] TO [power_user]
GO
