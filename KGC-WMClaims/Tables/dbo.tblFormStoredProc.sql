CREATE TABLE [dbo].[tblFormStoredProc]
(
[FormStoredProcID] [int] NOT NULL IDENTITY(1, 1),
[FormID] [int] NULL,
[StoredProcID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFormStoredProc] ADD CONSTRAINT [PK_tblFormStoredProc] PRIMARY KEY CLUSTERED  ([FormStoredProcID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblFormStoredProc] TO [base_user]
GRANT SELECT ON  [dbo].[tblFormStoredProc] TO [Claims_User]
GO
