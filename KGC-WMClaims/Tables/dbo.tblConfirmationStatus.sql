CREATE TABLE [dbo].[tblConfirmationStatus]
(
[ConfirmationStatusID] [int] NOT NULL IDENTITY(1, 1),
[ConfirmationStatus] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblConfirmationStatus] ADD CONSTRAINT [PK_tblConfirmationStatus] PRIMARY KEY CLUSTERED  ([ConfirmationStatusID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblConfirmationStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblConfirmationStatus] TO [Claims_User]
GO
