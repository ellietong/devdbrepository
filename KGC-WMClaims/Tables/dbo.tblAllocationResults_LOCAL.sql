CREATE TABLE [dbo].[tblAllocationResults_LOCAL]
(
[AllocationResultID] [bigint] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[PolicyID] [int] NULL,
[PolicyDateID] [bigint] NULL,
[PStartSplit] [datetime] NULL,
[PEndSplit] [datetime] NULL,
[AnnualPeriodSplit] [int] NULL,
[Allocation] [float] NULL,
[AllocationIndemnity] [float] NULL,
[AllocationDefense] [float] NULL,
[AllocationDefenseOutsideLimits] [float] NULL,
[OccurrenceID] [bigint] NULL,
[TriggerStartDateUsed] [datetime] NULL,
[TriggerEndDateUsed] [datetime] NULL,
[SIRAllocated] [float] NULL,
[SIRAllocatedIndemnity] [float] NULL,
[SIRAllocatedDefense] [float] NULL,
[SIRDiscount] [float] NULL,
[DateTime] [datetime] NULL,
[PV] [decimal] (38, 15) NULL,
[DateToPresentValue] [datetime] NULL,
[PVRate] [decimal] (18, 15) NULL,
[YieldCurveID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResults_LOCAL] ADD CONSTRAINT [FK_tblAllocationResults_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblAllocationResults_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResults_LOCAL] TO [Claims_User]
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResults_LOCAL', 'COLUMN', N'AllocationResultID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResults_LOCAL', 'COLUMN', N'OccurrenceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResults_LOCAL', 'COLUMN', N'OccurrenceID'
GO
