CREATE TABLE [dbo].[tblDefenseInvoiceStatus]
(
[StatusID] [int] NOT NULL IDENTITY(1, 1),
[Status] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseInvoiceStatus] ADD CONSTRAINT [PK__tblDefen__C8EE20435ED4ED8D] PRIMARY KEY CLUSTERED  ([StatusID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDefenseInvoiceStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoiceStatus] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefenseInvoiceStatus] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseInvoiceStatus] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseInvoiceStatus] TO [Claims_User]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', 'COLUMN', N'Status'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', 'COLUMN', N'Status'
GO
DECLARE @xp int
SELECT @xp=3525
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', 'COLUMN', N'Status'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', 'COLUMN', N'StatusID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', 'COLUMN', N'StatusID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefenseInvoiceStatus', 'COLUMN', N'StatusID'
GO
