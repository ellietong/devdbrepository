CREATE TABLE [dbo].[tblStatus]
(
[StatusID] [int] NOT NULL IDENTITY(1, 1),
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblStatus] ADD CONSTRAINT [PK_mEtblStatus] PRIMARY KEY CLUSTERED  ([StatusID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblStatus] TO [Claims_User]
GO
