CREATE TABLE [dbo].[tblDefenseCounselState]
(
[DefenseCounselStateID] [int] NOT NULL IDENTITY(1, 1),
[DefenseCounselAddressID] [int] NOT NULL,
[StateID] [int] NOT NULL,
[ResponsibleDefenseCounselAddressID] [int] NOT NULL,
[IsActiveCounsel] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseCounselState] ADD CONSTRAINT [PK_mBtblDefenseCounselState] PRIMARY KEY CLUSTERED  ([DefenseCounselStateID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDefenseCounselState] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselState] TO [Claims_User]
GO
