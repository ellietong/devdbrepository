CREATE TABLE [dbo].[tblClaimantDocketNumbers]
(
[ClaimantDocketNumberID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NOT NULL,
[DocketNumbers] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimantDocketNumbers] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantDocketNumbers] TO [Claims_User]
GO
