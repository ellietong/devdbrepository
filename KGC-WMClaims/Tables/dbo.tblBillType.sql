CREATE TABLE [dbo].[tblBillType]
(
[BillTypeID] [int] NOT NULL IDENTITY(1, 1),
[BillType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillType] ADD CONSTRAINT [PK_tblBillType] PRIMARY KEY CLUSTERED  ([BillTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblBillType] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillType] TO [power_user]
GO
