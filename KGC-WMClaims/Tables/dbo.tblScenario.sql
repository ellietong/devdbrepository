CREATE TABLE [dbo].[tblScenario]
(
[ScenarioID] [int] NOT NULL IDENTITY(1, 1),
[Scenario] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [datetime] NULL,
[ScenarioNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblScenario] ADD CONSTRAINT [PK_tblScenario] PRIMARY KEY CLUSTERED  ([ScenarioID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblScenario] TO [base_user]
GRANT SELECT ON  [dbo].[tblScenario] TO [Claims_User]
GO
