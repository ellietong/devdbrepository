CREATE TABLE [dbo].[tblRelationship_LOCAL]
(
[RelationshipID] [int] NOT NULL IDENTITY(1, 1),
[Relationship] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRelationship_LOCAL] ADD CONSTRAINT [PK_mBtblRelationship] PRIMARY KEY CLUSTERED  ([RelationshipID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblRelationship_LOCAL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblRelationship_LOCAL] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblRelationship_LOCAL] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblRelationship_LOCAL] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblRelationship_LOCAL] TO [Claims_User]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship_LOCAL', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship_LOCAL', 'COLUMN', N'Relationship'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship_LOCAL', 'COLUMN', N'Relationship'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship_LOCAL', 'COLUMN', N'Relationship'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship_LOCAL', 'COLUMN', N'RelationshipID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship_LOCAL', 'COLUMN', N'RelationshipID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblRelationship_LOCAL', 'COLUMN', N'RelationshipID'
GO
