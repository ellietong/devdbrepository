CREATE TABLE [dbo].[tblSubscriptionCoverage_LOCAL]
(
[SubscriptionCoverageID] [int] NOT NULL IDENTITY(1, 1),
[SubscriberID] [int] NOT NULL,
[SubscriptionSectionCoverageID] [int] NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[AgencyPercentOfSection] [float] NULL,
[GroupWithinAgencyPercent] [float] NULL,
[SubscriptionCompanyPercent] [float] NULL,
[AgencyID] [int] NULL,
[UWReference] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubscriptionNotes] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcctNum] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriptionCoverage_LOCAL] ADD CONSTRAINT [PK_tblSubscriptionCompanyCoverage] PRIMARY KEY CLUSTERED  ([SubscriptionCoverageID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSubscriptionCoverage_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriptionCoverage_LOCAL] TO [Claims_User]
GO
