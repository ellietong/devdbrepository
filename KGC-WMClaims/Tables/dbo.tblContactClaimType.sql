CREATE TABLE [dbo].[tblContactClaimType]
(
[ContactClaimTypeID] [int] NOT NULL IDENTITY(1, 1),
[ContactClaimType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblContactClaimType] ADD CONSTRAINT [PK_tblContactClaimType] PRIMARY KEY CLUSTERED  ([ContactClaimTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblContactClaimType] TO [base_user]
GRANT SELECT ON  [dbo].[tblContactClaimType] TO [Claims_User]
GO
