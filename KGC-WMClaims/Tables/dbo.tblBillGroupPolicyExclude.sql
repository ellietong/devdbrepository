CREATE TABLE [dbo].[tblBillGroupPolicyExclude]
(
[BillGroupPolicyExcludeID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupID] [int] NOT NULL,
[PolicyID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillGroupPolicyExclude] ADD CONSTRAINT [PK_tblBillGroupPolicyExclude] PRIMARY KEY CLUSTERED  ([BillGroupPolicyExcludeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblBillGroupPolicyExclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillGroupPolicyExclude] TO [Claims_User]
GO
