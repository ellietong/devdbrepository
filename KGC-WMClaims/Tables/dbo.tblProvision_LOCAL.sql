CREATE TABLE [dbo].[tblProvision_LOCAL]
(
[ProvisionID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionLabelID] [int] NULL,
[PolicyID] [int] NULL,
[ProvisionValueID] [int] NULL,
[ProvisionNotes] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvision_LOCAL] ADD CONSTRAINT [PK_dropTemptblProvision] PRIMARY KEY CLUSTERED  ([ProvisionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvision_LOCAL] ADD CONSTRAINT [FK_tblProvision_tblPolicy] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[tblPolicy_LOCAL] ([PolicyID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblProvision_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvision_LOCAL] TO [Claims_User]
GO
