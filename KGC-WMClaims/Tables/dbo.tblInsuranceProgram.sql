CREATE TABLE [dbo].[tblInsuranceProgram]
(
[InsuranceProgramID] [int] NOT NULL IDENTITY(1, 1),
[InsuranceProgram] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblInsuranceProgram] ADD CONSTRAINT [PK_tblInsuranceProgram] PRIMARY KEY CLUSTERED  ([InsuranceProgramID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblInsuranceProgram] TO [base_user]
GRANT SELECT ON  [dbo].[tblInsuranceProgram] TO [Claims_User]
GO
