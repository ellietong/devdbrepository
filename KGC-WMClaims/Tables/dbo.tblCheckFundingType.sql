CREATE TABLE [dbo].[tblCheckFundingType]
(
[FundingTypeID] [int] NOT NULL IDENTITY(1, 1),
[FundingType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCheckFundingType] ADD CONSTRAINT [PK_tblCheckFundingType] PRIMARY KEY CLUSTERED  ([FundingTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCheckFundingType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCheckFundingType] TO [Claims_User]
GO
