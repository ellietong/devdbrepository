CREATE TABLE [dbo].[tblCorpHistorySource]
(
[CorpSourceID] [int] NOT NULL IDENTITY(1, 1),
[CorpHistorySource] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorpHistorySource] ADD CONSTRAINT [PK_tblCorpHistorySource] PRIMARY KEY CLUSTERED  ([CorpSourceID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCorpHistorySource] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorpHistorySource] TO [Claims_User]
GO
