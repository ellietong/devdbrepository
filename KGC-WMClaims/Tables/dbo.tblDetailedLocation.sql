CREATE TABLE [dbo].[tblDetailedLocation]
(
[DetailedLocationID] [int] NOT NULL IDENTITY(1, 1),
[DetailedLocation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDetailedLocation] ADD CONSTRAINT [PK_mtblLangType] PRIMARY KEY CLUSTERED  ([DetailedLocationID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDetailedLocation] TO [base_user]
GRANT SELECT ON  [dbo].[tblDetailedLocation] TO [Claims_User]
GO
