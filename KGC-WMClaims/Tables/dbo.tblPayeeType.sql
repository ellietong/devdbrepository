CREATE TABLE [dbo].[tblPayeeType]
(
[PayeeTypeID] [int] NOT NULL IDENTITY(1, 1),
[PayeeType] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPayeeType] ADD CONSTRAINT [PK_tblPayeeType] PRIMARY KEY CLUSTERED  ([PayeeTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPayeeType] TO [base_user]
GRANT SELECT ON  [dbo].[tblPayeeType] TO [Claims_User]
GO
