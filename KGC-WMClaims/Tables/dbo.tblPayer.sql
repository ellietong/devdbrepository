CREATE TABLE [dbo].[tblPayer]
(
[PayerID] [int] NOT NULL IDENTITY(1, 1),
[Payer] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPayer] ADD CONSTRAINT [PK_mEtblPayer] PRIMARY KEY CLUSTERED  ([PayerID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPayer] TO [base_user]
GRANT SELECT ON  [dbo].[tblPayer] TO [Claims_User]
GO
