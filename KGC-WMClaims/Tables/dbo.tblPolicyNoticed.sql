CREATE TABLE [dbo].[tblPolicyNoticed]
(
[PolicyNoticedID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NOT NULL,
[PolicyID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyNoticed] ADD CONSTRAINT [PK_tblPolicyNoticed] PRIMARY KEY CLUSTERED  ([PolicyNoticedID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyNoticed] ADD CONSTRAINT [FK_tblPolicyNoticed_tblCorrespondence] FOREIGN KEY ([CorrespondenceID]) REFERENCES [dbo].[tblCorrespondence] ([CorrespondenceID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblPolicyNoticed] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyNoticed] TO [Claims_User]
GO
