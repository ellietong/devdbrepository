CREATE TABLE [dbo].[FINAL_CLAIM_NF_DATE]
(
[CLM_KEY] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NEW_FILING_DATE] [datetime] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[FINAL_CLAIM_NF_DATE] TO [base_user]
GRANT SELECT ON  [dbo].[FINAL_CLAIM_NF_DATE] TO [Claims_User]
GO
