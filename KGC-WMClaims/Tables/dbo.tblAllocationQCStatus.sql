CREATE TABLE [dbo].[tblAllocationQCStatus]
(
[AllocationQCStatusID] [int] NOT NULL,
[AllocationQCStatus] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblAllocationQCStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationQCStatus] TO [Claims_User]
GO
