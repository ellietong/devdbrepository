CREATE TABLE [dbo].[tblClaimDisease]
(
[ClaimDiseaseID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID] [bigint] NULL,
[DiseaseID] [int] NULL,
[PrimaryDisease] [bit] NULL,
[DiseaseDiagnosisDate] [datetime] NULL,
[DoctorID] [int] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiseaseID_OLD] [int] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimDisease] ADD CONSTRAINT [PK_tblClaimDisease] PRIMARY KEY CLUSTERED  ([ClaimDiseaseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_3281_3280_tblClaimDisease] ON [dbo].[tblClaimDisease] ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_3277_3276_tblClaimDisease] ON [dbo].[tblClaimDisease] ([ClaimID], [PrimaryDisease]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [DeleteClaims]
GO
