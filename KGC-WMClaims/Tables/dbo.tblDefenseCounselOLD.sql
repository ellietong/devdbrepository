CREATE TABLE [dbo].[tblDefenseCounselOLD]
(
[DefenseCounselID] [int] NOT NULL IDENTITY(1, 1),
[OldDefenseCounsel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDefenseCounselOLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselOLD] TO [Claims_User]
GO
