CREATE TABLE [dbo].[tblLegalCapacity_ALT]
(
[LegalCapacityID] [int] NULL,
[LegalCapacity] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicareRelationshipID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblLegalCapacity_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblLegalCapacity_ALT] TO [Claims_User]
GO
