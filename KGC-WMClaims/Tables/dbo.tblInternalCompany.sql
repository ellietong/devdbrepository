CREATE TABLE [dbo].[tblInternalCompany]
(
[InternalCompanyID] [int] NOT NULL IDENTITY(1, 1),
[InternalCompany] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblInternalCompany] ADD CONSTRAINT [PK_tblInternalCompany] PRIMARY KEY CLUSTERED  ([InternalCompanyID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblInternalCompany] TO [base_user]
GRANT SELECT ON  [dbo].[tblInternalCompany] TO [Claims_User]
GO
