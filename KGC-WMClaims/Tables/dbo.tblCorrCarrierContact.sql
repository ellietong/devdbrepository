CREATE TABLE [dbo].[tblCorrCarrierContact]
(
[CorrCarrierContactID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NOT NULL,
[CarrierContactID] [int] NOT NULL,
[RecipientTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorrCarrierContact] ADD CONSTRAINT [PK_tblCorrCarrierContact] PRIMARY KEY CLUSTERED  ([CorrCarrierContactID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCorrCarrierContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorrCarrierContact] TO [Claims_User]
GO
