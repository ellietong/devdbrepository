CREATE TABLE [dbo].[tblResolutionType]
(
[ResolutionTypeID] [int] NOT NULL IDENTITY(1, 1),
[ResolutionType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblResolutionType] ADD CONSTRAINT [PK_mBtblResolutionType] PRIMARY KEY CLUSTERED  ([ResolutionTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblResolutionType] TO [base_user]
GRANT SELECT ON  [dbo].[tblResolutionType] TO [Claims_User]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblResolutionType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblResolutionType', 'COLUMN', N'ResolutionType'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblResolutionType', 'COLUMN', N'ResolutionType'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblResolutionType', 'COLUMN', N'ResolutionType'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblResolutionType', 'COLUMN', N'ResolutionTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblResolutionType', 'COLUMN', N'ResolutionTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblResolutionType', 'COLUMN', N'ResolutionTypeID'
GO
