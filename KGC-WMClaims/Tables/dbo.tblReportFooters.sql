CREATE TABLE [dbo].[tblReportFooters]
(
[ReportFooterID] [int] NOT NULL IDENTITY(1, 1),
[ReportFooterType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportFooter] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblReportFooters] ADD CONSTRAINT [PK_tblReportFooters] PRIMARY KEY CLUSTERED  ([ReportFooterID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblReportFooters] TO [base_user]
GRANT SELECT ON  [dbo].[tblReportFooters] TO [Claims_User]
GO
