CREATE TABLE [dbo].[tblClaimantDocumentTypeGroup]
(
[ClaimantDocumentTypeGroupID] [bigint] NOT NULL,
[ClaimantDocumentTypeID] [int] NOT NULL,
[ClaimantDocumentGroupID] [int] NOT NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimantDocumentTypeGroup] TO [Claims_User]
GO
