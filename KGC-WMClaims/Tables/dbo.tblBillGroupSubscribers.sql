CREATE TABLE [dbo].[tblBillGroupSubscribers]
(
[BillGroupSubscriberID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupID] [int] NOT NULL,
[SubscriberID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillGroupSubscribers] ADD CONSTRAINT [PK_tblBillGroupSubscribers] PRIMARY KEY CLUSTERED  ([BillGroupSubscriberID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblBillGroupSubscribers] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillGroupSubscribers] TO [Claims_User]
GO
