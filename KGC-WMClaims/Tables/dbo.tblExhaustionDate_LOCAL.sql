CREATE TABLE [dbo].[tblExhaustionDate_LOCAL]
(
[ExhaustionDateID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionTypeID] [int] NOT NULL,
[ExhaustionDate] [datetime] NOT NULL,
[AllocationID] [int] NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConsumptionTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustionDate_LOCAL] ADD CONSTRAINT [PK_tblExhaustionDate] PRIMARY KEY CLUSTERED  ([ExhaustionDateID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustionDate_LOCAL] ADD CONSTRAINT [FK_tblExhaustionDate_tblExhaustionType] FOREIGN KEY ([ExhaustionTypeID]) REFERENCES [dbo].[tblExhaustionType_LOCAL] ([ExhaustionTypeID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblExhaustionDate_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustionDate_LOCAL] TO [Claims_User]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionDate_LOCAL', 'COLUMN', N'ExhaustionTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionDate_LOCAL', 'COLUMN', N'ExhaustionTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblExhaustionDate_LOCAL', 'COLUMN', N'ExhaustionTypeID'
GO
