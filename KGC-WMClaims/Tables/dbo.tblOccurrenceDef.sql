CREATE TABLE [dbo].[tblOccurrenceDef]
(
[OccurrenceDefID] [int] NOT NULL,
[OccurrenceDef] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccurrenceDef] ADD CONSTRAINT [PK_tblOccurrenceDef] PRIMARY KEY CLUSTERED  ([OccurrenceDefID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblOccurrenceDef] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrenceDef] TO [Claims_User]
GO
