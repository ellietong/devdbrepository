CREATE TABLE [dbo].[tblProductName]
(
[ProductNameID] [int] NOT NULL IDENTITY(1, 1),
[ProductName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProductName] ADD CONSTRAINT [PK_tblClaimantProductName] PRIMARY KEY CLUSTERED  ([ProductNameID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblProductName] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblProductName] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblProductName] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblProductName] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblProductName] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblProductName] TO [Claims_User]
GO
