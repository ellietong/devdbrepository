CREATE TABLE [dbo].[tblSupportingDocumentType]
(
[SupportingDocumentTypeID] [int] NOT NULL IDENTITY(1, 1),
[SupportingDocumentType] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSupportingDocumentType] ADD CONSTRAINT [PK_tblSupportingDocumentType] PRIMARY KEY CLUSTERED  ([SupportingDocumentTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSupportingDocumentType] TO [base_user]
GRANT SELECT ON  [dbo].[tblSupportingDocumentType] TO [Claims_User]
GO
