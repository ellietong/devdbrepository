CREATE TABLE [dbo].[tblAllocationSIR]
(
[SIRID] [int] NOT NULL IDENTITY(1, 1),
[SIRTreatment] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hide] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationSIR] ADD CONSTRAINT [PK_tblAllocationSIR] PRIMARY KEY NONCLUSTERED  ([SIRID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblAllocationSIR] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationSIR] TO [Claims_User]
GO
