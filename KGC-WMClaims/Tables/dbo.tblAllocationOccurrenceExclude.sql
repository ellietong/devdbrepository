CREATE TABLE [dbo].[tblAllocationOccurrenceExclude]
(
[OccurrenceExcludeID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[OccurrenceID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationOccurrenceExclude] ADD CONSTRAINT [PK_tblAllocationOccurrenceExclude] PRIMARY KEY NONCLUSTERED  ([OccurrenceExcludeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationOccurrenceExclude] ADD CONSTRAINT [FK_tblAllocationOccurrenceExclude_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation] ([AllocationID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceExclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceExclude] TO [Claims_User]
GO
