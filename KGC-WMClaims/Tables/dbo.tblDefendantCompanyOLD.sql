CREATE TABLE [dbo].[tblDefendantCompanyOLD]
(
[DefendantCompanyID] [int] NOT NULL IDENTITY(1, 1),
[DefendantCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MatterID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefendantCompanyOLD] ADD CONSTRAINT [PK_mBtblDefendantCompany] PRIMARY KEY CLUSTERED  ([DefendantCompanyID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDefendantCompanyOLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefendantCompanyOLD] TO [Claims_User]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompanyOLD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompanyOLD', 'COLUMN', N'DefendantCompany'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompanyOLD', 'COLUMN', N'DefendantCompany'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompanyOLD', 'COLUMN', N'DefendantCompany'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompanyOLD', 'COLUMN', N'DefendantCompanyID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompanyOLD', 'COLUMN', N'DefendantCompanyID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDefendantCompanyOLD', 'COLUMN', N'DefendantCompanyID'
GO
