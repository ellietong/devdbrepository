CREATE TABLE [dbo].[tblCostType]
(
[CostTypeID] [int] NOT NULL IDENTITY(1, 1),
[CostType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcludeCostType] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCostType] ADD CONSTRAINT [PK_mEtblCostType] PRIMARY KEY CLUSTERED  ([CostTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCostType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCostType] TO [Claims_User]
GO
