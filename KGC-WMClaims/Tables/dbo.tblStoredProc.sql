CREATE TABLE [dbo].[tblStoredProc]
(
[StoredProcID] [int] NOT NULL IDENTITY(1, 1),
[StoredProcedure] [char] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblStoredProc] ADD CONSTRAINT [PK_TblStoredProc] PRIMARY KEY CLUSTERED  ([StoredProcID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblStoredProc] TO [base_user]
GRANT SELECT ON  [dbo].[tblStoredProc] TO [Claims_User]
GO
