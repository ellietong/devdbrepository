CREATE TABLE [dbo].[tblCombination]
(
[ProvisionComboID] [int] NOT NULL IDENTITY(1, 1),
[Combination] [int] NOT NULL,
[ProvisionLabelID] [int] NOT NULL,
[ProvisionValueID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCombination] ADD CONSTRAINT [PK_tblCombination] PRIMARY KEY CLUSTERED  ([ProvisionComboID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCombination] TO [base_user]
GRANT SELECT ON  [dbo].[tblCombination] TO [Claims_User]
GO
