CREATE TABLE [dbo].[tblCorrespondenceNoticeDefense]
(
[CorrespondenceNoticeDefenseID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NULL,
[NoticeDefenseID] [int] NULL,
[CorrespondenceNoticeDefense] [nvarchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorrespondenceNoticeDefense] ADD CONSTRAINT [PK_mtblCorrespondenceNoticeDefense] PRIMARY KEY CLUSTERED  ([CorrespondenceNoticeDefenseID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCorrespondenceNoticeDefense] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorrespondenceNoticeDefense] TO [Claims_User]
GO
