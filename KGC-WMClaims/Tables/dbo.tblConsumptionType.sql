CREATE TABLE [dbo].[tblConsumptionType]
(
[ConsumptionTypeID] [int] NOT NULL IDENTITY(1, 1),
[ConsumptionType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblConsumptionType] ADD CONSTRAINT [PK_tblConsumptionType] PRIMARY KEY CLUSTERED  ([ConsumptionTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblConsumptionType] TO [base_user]
GRANT SELECT ON  [dbo].[tblConsumptionType] TO [Claims_User]
GO
