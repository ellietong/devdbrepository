CREATE TABLE [dbo].[tblAccountsReceivable]
(
[AccountsReceivableID] [bigint] NOT NULL IDENTITY(1, 1),
[Amount] [money] NULL,
[TravelersBillID] [int] NULL,
[PolicyID] [int] NULL,
[RescoDueDate] [date] NULL,
[PayeeTypeID] [int] NULL CONSTRAINT [DF_tblAccountsReceivable_PayeeTypeID] DEFAULT ((1)),
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountsReceivableID_Original] [bigint] NULL,
[NotificationDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAccountsReceivable] ADD CONSTRAINT [PK_tblAccountsReceivable] PRIMARY KEY CLUSTERED  ([AccountsReceivableID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_45217_45216_tblAccountsReceivable] ON [dbo].[tblAccountsReceivable] ([PolicyID], [PayeeTypeID]) INCLUDE ([Amount], [TravelersBillID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblAccountsReceivable] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAccountsReceivable] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblAccountsReceivable] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblAccountsReceivable] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblAccountsReceivable] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblAccountsReceivable] TO [Claims_User]
GO
