CREATE TABLE [dbo].[tblClaimLawsuit]
(
[ClaimLawsuitID] [bigint] NOT NULL IDENTITY(1, 1),
[LawsuitID] [bigint] NULL,
[IndividualDocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimaryForClaim] [bit] NULL,
[IsMDL] [bit] NULL CONSTRAINT [DF_tblClaimLawsuit_IsMDL] DEFAULT ((0)),
[CLM_LWST_KEY] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [bigint] NULL,
[ActiveTrialDate] [date] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServiceDate] [datetime] NULL,
[ReceivedDate] [datetime] NULL,
[InsurerNotification] [datetime] NULL,
[TravelersResponse] [datetime] NULL,
[DOFESource] [int] NULL,
[DOFESourcePageNum] [int] NULL,
[DOFE] [datetime] NULL,
[SettlementConference] [date] NULL,
[ReferenceID] [int] NULL,
[ReferenceNote] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [PK_mBtblClaimLawsuitNEW] PRIMARY KEY CLUSTERED  ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_622316_622315_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_735773_735772_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([ClaimID]) INCLUDE ([ClaimLawsuitID], [DOFE]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_644165_644164_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([ClaimID]) INCLUDE ([ActiveTrialDate], [ClaimLawsuitID], [DOFE], [DOFESource], [DOFESourcePageNum], [IndividualDocketNumber], [InsurerNotification], [IsMDL], [IsPrimaryForClaim], [LawsuitID], [ReceivedDate], [ServiceDate], [TravelersResponse]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_615539_615538_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([ClaimID]) INCLUDE ([LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_643716_643715_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([ClaimID]) INCLUDE ([LawsuitID], [ServiceDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_642533_642532_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([IsPrimaryForClaim]) INCLUDE ([ClaimID], [ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_661988_661987_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([IsPrimaryForClaim]) INCLUDE ([ClaimID], [ClaimLawsuitID], [InsurerNotification], [IsMDL], [LawsuitID], [ReceivedDate], [ServiceDate], [TravelersResponse]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_642527_642526_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([IsPrimaryForClaim], [ClaimID]) INCLUDE ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_622309_622308_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([IsPrimaryForClaim], [ClaimID]) INCLUDE ([ClaimLawsuitID], [InsurerNotification], [IsMDL], [LawsuitID], [ReceivedDate], [ServiceDate], [TravelersResponse]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_794604_794603_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([LawsuitID]) INCLUDE ([ClaimID], [ClaimLawsuitID], [IsMDL]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [FK_tblClaimLawsuit_tblClaim] FOREIGN KEY ([ClaimID]) REFERENCES [dbo].[tblClaim] ([ClaimID])
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [FK_tblClaimLawsuit_tblLawsuit] FOREIGN KEY ([LawsuitID]) REFERENCES [dbo].[tblLawsuit] ([LawsuitID])
GO
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuit] TO [DeleteClaims]
GO
