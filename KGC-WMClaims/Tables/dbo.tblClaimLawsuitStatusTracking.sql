CREATE TABLE [dbo].[tblClaimLawsuitStatusTracking]
(
[ClaimLawsuitStatusTrackingID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[ClaimLawsuitStatusID] [int] NULL,
[StatusDate] [datetime] NULL,
[StatusNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimaryStatus] [bit] NULL,
[ProcessedDate] [datetime] NULL,
[ClaimLawsuitStatusIDold] [int] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [PK_tblClaimLawsuitStatusTracking] PRIMARY KEY CLUSTERED  ([ClaimLawsuitStatusTrackingID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_3254_3253_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitID], [ClaimLawsuitStatusID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_3279_3278_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitID], [IsPrimaryStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_3264_3263_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitID], [IsPrimaryStatus]) INCLUDE ([ClaimLawsuitStatusID], [StatusDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_3259_3258_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitStatusID]) INCLUDE ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_3271_3270_tblClaimLawsuitStatusTracking] ON [dbo].[tblClaimLawsuitStatusTracking] ([IsPrimaryStatus]) INCLUDE ([ClaimLawsuitID], [ClaimLawsuitStatusID], [StatusDate]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [DeleteClaims]
GO
