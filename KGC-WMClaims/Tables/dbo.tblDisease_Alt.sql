CREATE TABLE [dbo].[tblDisease_Alt]
(
[DiseaseID_Alt] [int] NOT NULL IDENTITY(1, 1),
[ICD9] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISEASE] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ICD9Desc] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighLevelDiseaseID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDisease_Alt] ADD CONSTRAINT [PK_tblDisease_Alt] PRIMARY KEY CLUSTERED  ([DiseaseID_Alt]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDisease_Alt] TO [base_user]
GRANT SELECT ON  [dbo].[tblDisease_Alt] TO [Claims_User]
GO
