CREATE TABLE [dbo].[tblExhaustion_LOCAL]
(
[ExhaustionID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionDateID] [int] NOT NULL,
[OccurrenceID] [bigint] NULL,
[PolicyDateID] [int] NOT NULL,
[IndemnityAllocation] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_IndemnityAllocation] DEFAULT ((0)),
[DefenseAllocation] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_DefenseAllocation] DEFAULT ((0)),
[DefenseOutsideLimitsAllocation] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_DefenseOutsideLimitsAllocation] DEFAULT ((0)),
[SIRIndemnityAllocation] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_SIRIndemnityAllocation] DEFAULT ((0)),
[SIRDefenseAllocation] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_SIRDefenseAllocation] DEFAULT ((0)),
[LedgerID] [int] NULL,
[OutsideConsumption] [float] NOT NULL CONSTRAINT [DF_tblExhaustion_OutsideConsumption] DEFAULT ((0)),
[ConsumptionType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustion_LOCAL] ADD CONSTRAINT [PK_tblExhaustion] PRIMARY KEY CLUSTERED  ([ExhaustionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustion_LOCAL] ADD CONSTRAINT [FK_tblExhaustion_tblPolicyDates] FOREIGN KEY ([PolicyDateID]) REFERENCES [dbo].[tblPolicyDates_LOCAL] ([PolicyDateID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblExhaustion_LOCAL] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustion_LOCAL] TO [Claims_User]
GO
