CREATE TABLE [dbo].[tblLawsuit]
(
[LawsuitID] [bigint] NOT NULL IDENTITY(1, 1),
[CaseCaption] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[JurisdictionID] [int] NULL,
[StateID] [int] NULL,
[CourtTypeID] [int] NULL,
[LeadPlaintiffFirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffLastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveTrialDate_OLD] [datetime] NULL,
[DefenseCounselAddressID] [int] NULL,
[PlaintiffCounselAddressID] [int] NULL,
[PrimaryNationalPlaintiffCounselID] [int] NULL,
[IsThirdPartyComplaint] [bit] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawsuitID_Original] [bigint] NULL,
[ReferenceID] [int] NULL,
[LawsuitNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReferenceNote] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLawsuit] ADD CONSTRAINT [PK__tblLawsuit1__5A1A5A11] PRIMARY KEY CLUSTERED  ([LawsuitID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblLawsuit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblLawsuit] TO [DeleteClaims]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'JurisdictionID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'JurisdictionID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'StateID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'StateID'
GO
