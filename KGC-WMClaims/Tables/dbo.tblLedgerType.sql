CREATE TABLE [dbo].[tblLedgerType]
(
[LedgerTypeID] [int] NOT NULL IDENTITY(1, 1),
[LedgerType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLedgerType] ADD CONSTRAINT [PK_tblLedgerType] PRIMARY KEY NONCLUSTERED  ([LedgerTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblLedgerType] TO [base_user]
GRANT SELECT ON  [dbo].[tblLedgerType] TO [Claims_User]
GO
