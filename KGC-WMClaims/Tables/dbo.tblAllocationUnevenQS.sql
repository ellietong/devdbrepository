CREATE TABLE [dbo].[tblAllocationUnevenQS]
(
[UnevenQSID] [int] NOT NULL IDENTITY(1, 1),
[UnevenQSType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnevenQSDescription] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationUnevenQS] ADD CONSTRAINT [PK_tblAllocationUnevenQS] PRIMARY KEY CLUSTERED  ([UnevenQSID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblAllocationUnevenQS] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationUnevenQS] TO [Claims_User]
GO
