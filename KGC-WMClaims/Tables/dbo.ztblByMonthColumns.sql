CREATE TABLE [dbo].[ztblByMonthColumns]
(
[MonthColumnID] [int] NOT NULL IDENTITY(1, 1),
[ColumnID] [int] NULL,
[ColumnStartDate] [datetime] NULL,
[ColumnEndDate] [datetime] NULL,
[ColumnRemaining] [money] NULL,
[TriggeredDays] [int] NULL,
[TotalMonths] [int] NULL,
[MinRemaining] [money] NULL,
[InsuranceProgramID] [int] NULL,
[OccurrenceID] [bigint] NULL,
[AllocationID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblByMonthColumns] ADD CONSTRAINT [PK_ztblByMonthColumns] PRIMARY KEY CLUSTERED  ([MonthColumnID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ztblByMonthColumns] TO [base_user]
GRANT SELECT ON  [dbo].[ztblByMonthColumns] TO [Claims_User]
GO
