CREATE TABLE [dbo].[tblDocuments]
(
[DocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantDocumentTypeID] [int] NOT NULL,
[LawsuitID] [bigint] NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[Comments] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentURL] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentTitle] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPDocId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [datetime] NOT NULL,
[TravelersBillID] [bigint] NULL,
[SettlementGroupID] [bigint] NULL,
[SettlementDate] [datetime] NULL,
[SettlementAmount] [money] NULL,
[InvoiceNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatePayable] [datetime] NULL,
[FeeAmount] [money] NULL,
[DateOfFiling] [datetime] NULL,
[DateOfDeath] [datetime] NULL,
[ModifiedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateModified] [datetime] NOT NULL,
[Accepted] [bit] NULL,
[AcceptedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateAccepted] [datetime] NULL,
[DisbursementAmount] [money] NULL,
[Adjustment] [money] NULL,
[AdjustmentNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusID] [int] NULL,
[ApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateApproved] [datetime] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[FirmID] [int] NULL,
[DocumentSourceID] [int] NULL,
[VendorFirmID] [int] NULL CONSTRAINT [DF_tblDocuments_VendorFirmID] DEFAULT ((0)),
[ClaimLawsuitStatusID] [int] NULL,
[DismissalDate] [date] NULL,
[DismissalProcessDate] [date] NULL,
[MatterID] [int] NULL,
[HistoricalDocumentID] [bigint] NULL,
[FirmID_Old] [int] NULL,
[VendorFirmID_old] [int] NULL,
[DefendantCompanyID] [int] NULL,
[AmazonKey] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComplaintID] [int] NULL,
[ForQC] [bit] NULL,
[InsurerNotificationDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDocuments] ADD CONSTRAINT [PK_mBtblDocuments] PRIMARY KEY CLUSTERED  ([DocumentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_366099_366098_tblDocuments] ON [dbo].[tblDocuments] ([ClaimantDocumentTypeID]) INCLUDE ([DocumentID], [DocumentTitle]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_629214_629213_tblDocuments] ON [dbo].[tblDocuments] ([TravelersBillID]) INCLUDE ([AmazonKey], [ClaimantDocumentTypeID], [DocumentID], [DocumentTitle]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_381341_381340_tblDocuments] ON [dbo].[tblDocuments] ([TravelersBillID]) INCLUDE ([ClaimantDocumentTypeID], [DocumentID], [DocumentTitle], [DocumentURL]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblDocuments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDocuments] TO [Claims_User]
GO
EXEC sp_addextendedproperty N'MS_Orientation', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantDocumentTypeID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'ClaimantPersonalInfoID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'CreatedBy'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DateCreated'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'DocumentURL'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'LawsuitID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'SettlementGroupID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'SettlementGroupID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblDocuments', 'COLUMN', N'SettlementGroupID'
GO
