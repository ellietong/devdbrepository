CREATE TABLE [dbo].[tblDocumentSource]
(
[DocumentSourceID] [int] NOT NULL IDENTITY(1, 1),
[DocumentSource] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDocumentSource] ADD CONSTRAINT [PK_tblDocumentSource] PRIMARY KEY CLUSTERED  ([DocumentSourceID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDocumentSource] TO [base_user]
GRANT SELECT ON  [dbo].[tblDocumentSource] TO [Claims_User]
GO
