CREATE TABLE [dbo].[tblMonth]
(
[MonthID] [int] NOT NULL IDENTITY(1, 1),
[MonthName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMonth] ADD CONSTRAINT [PK_mtblMonth] PRIMARY KEY CLUSTERED  ([MonthID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblMonth] TO [base_user]
GRANT SELECT ON  [dbo].[tblMonth] TO [Claims_User]
GO
