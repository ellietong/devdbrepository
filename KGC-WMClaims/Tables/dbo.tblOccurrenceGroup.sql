CREATE TABLE [dbo].[tblOccurrenceGroup]
(
[OccurrenceGroupID] [int] NOT NULL IDENTITY(1, 1),
[OccurrenceGroup] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccGrpTriggerStart] [datetime] NULL,
[OccGrpDiscount] [real] NULL,
[SiteID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccurrenceGroup] ADD CONSTRAINT [PK_tblOccurrenceGroup] PRIMARY KEY CLUSTERED  ([OccurrenceGroupID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblOccurrenceGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrenceGroup] TO [Claims_User]
GO
