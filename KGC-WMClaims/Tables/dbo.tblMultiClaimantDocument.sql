CREATE TABLE [dbo].[tblMultiClaimantDocument]
(
[MultiClaimantDocumentID] [bigint] NOT NULL IDENTITY(1, 1),
[DocumentID] [bigint] NOT NULL,
[ClaimantPersonalInfoID] [bigint] NOT NULL,
[HistoricalDocumentLink] [bigint] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [bigint] NULL,
[QCBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCDate] [datetime] NULL,
[DocumentID_Original] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMultiClaimantDocument] ADD CONSTRAINT [PK_tblMultiClaimantDocument] PRIMARY KEY CLUSTERED  ([MultiClaimantDocumentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_352596_352595_tblMultiClaimantDocument] ON [dbo].[tblMultiClaimantDocument] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_351051_351050_tblMultiClaimantDocument] ON [dbo].[tblMultiClaimantDocument] ([DocumentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMultiClaimantDocument] WITH NOCHECK ADD CONSTRAINT [FK_tblMultiClaimantDocument_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID])
GO
ALTER TABLE [dbo].[tblMultiClaimantDocument] WITH NOCHECK ADD CONSTRAINT [FK_tblMultiClaimantDocument_tblDocuments] FOREIGN KEY ([DocumentID]) REFERENCES [dbo].[tblDocuments] ([DocumentID])
GO
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblMultiClaimantDocument] TO [Claims_User]
GO
