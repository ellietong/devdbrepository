CREATE TABLE [dbo].[tblCheckApproval]
(
[CheckApprovalID] [bigint] NOT NULL IDENTITY(1, 1),
[CheckID] [bigint] NULL,
[IsApproved] [bit] NULL,
[ApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovedDate] [datetime] NULL,
[IsCheckCleared] [bit] NULL,
[CheckClearedDate] [date] NULL,
[VoidDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCheckApproval] ADD CONSTRAINT [PK_tblCheckApproval] PRIMARY KEY CLUSTERED  ([CheckApprovalID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCheckApproval] TO [base_user]
GRANT SELECT ON  [dbo].[tblCheckApproval] TO [Claims_User]
GO
