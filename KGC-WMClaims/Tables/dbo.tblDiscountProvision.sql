CREATE TABLE [dbo].[tblDiscountProvision]
(
[DiscountProvisionID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionID] [int] NULL,
[ProvisionValueID] [int] NULL,
[ProvisionIDProvisionValueID] [int] NULL,
[ProvisionDiscount] [money] NULL,
[discountid] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountProvision] ADD CONSTRAINT [PK_tblDiscountProvision] PRIMARY KEY CLUSTERED  ([DiscountProvisionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountProvision] ADD CONSTRAINT [FK_tblDiscountProvision_tblProvisionValue] FOREIGN KEY ([ProvisionValueID]) REFERENCES [dbo].[tblProvisionValue_LOCAL] ([ProvisionValueID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblDiscountProvision] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiscountProvision] TO [Claims_User]
GO
