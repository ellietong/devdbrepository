CREATE TABLE [dbo].[tblAcquiredSold]
(
[AcqSoldID] [int] NOT NULL IDENTITY(1, 1),
[AcqSold] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAcquiredSold] ADD CONSTRAINT [PK_tblAcquiredSold] PRIMARY KEY CLUSTERED  ([AcqSoldID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblAcquiredSold] TO [base_user]
GRANT SELECT ON  [dbo].[tblAcquiredSold] TO [Claims_User]
GO
