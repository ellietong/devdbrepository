CREATE TABLE [dbo].[tblNamedInsured]
(
[NamedInsuredID] [int] NOT NULL IDENTITY(1, 1),
[NamedInsured] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblNamedInsured] ADD CONSTRAINT [PK_tblNamedInsured] PRIMARY KEY NONCLUSTERED  ([NamedInsuredID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblNamedInsured] TO [base_user]
GRANT SELECT ON  [dbo].[tblNamedInsured] TO [Claims_User]
GO
