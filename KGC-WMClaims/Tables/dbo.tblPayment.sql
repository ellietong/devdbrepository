CREATE TABLE [dbo].[tblPayment]
(
[PaymentID] [int] NOT NULL IDENTITY(1, 1),
[DatePaid] [datetime] NULL,
[DateEntered] [datetime] NULL CONSTRAINT [DF_tblPayment_DateEntered] DEFAULT (getdate()),
[Amount] [money] NULL,
[DocumentID] [int] NULL,
[PaymentTypeID] [int] NULL,
[CarrierGroupID] [int] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentID_Original] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPayment] ADD CONSTRAINT [PK_tblPayment] PRIMARY KEY CLUSTERED  ([PaymentID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPayment] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblPayment] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblPayment] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblPayment] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblPayment] TO [Claims_User]
GO
