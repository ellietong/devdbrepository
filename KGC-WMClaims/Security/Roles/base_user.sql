CREATE ROLE [base_user]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'base_user', N'barnabyj'
GO
EXEC sp_addrolemember N'base_user', N'blairo'
GO
EXEC sp_addrolemember N'base_user', N'casillaw'
GO
EXEC sp_addrolemember N'base_user', N'chantosao'
GO
EXEC sp_addrolemember N'base_user', N'chaudoinr'
GO
EXEC sp_addrolemember N'base_user', N'colek'
GO
EXEC sp_addrolemember N'base_user', N'cornellt'
GO
EXEC sp_addrolemember N'base_user', N'crookc'
GO
EXEC sp_addrolemember N'base_user', N'donzer'
GO
EXEC sp_addrolemember N'base_user', N'eastlacka'
GO
EXEC sp_addrolemember N'base_user', N'gemouless'
GO
EXEC sp_addrolemember N'base_user', N'gibbonse'
GO
EXEC sp_addrolemember N'base_user', N'hubbardb'
GO
EXEC sp_addrolemember N'base_user', N'hughesr'
GO
EXEC sp_addrolemember N'base_user', N'husainm'
GO
EXEC sp_addrolemember N'base_user', N'KCICLLC\carthyl'
GO
EXEC sp_addrolemember N'base_user', N'KCICLLC\hashemik'
GO
EXEC sp_addrolemember N'base_user', N'KCICLLC\jacobsb'
GO
EXEC sp_addrolemember N'base_user', N'KCICLLC\kanga'
GO
EXEC sp_addrolemember N'base_user', N'KCICLLC\kennedyl'
GO
EXEC sp_addrolemember N'base_user', N'KCICLLC\lislev'
GO
EXEC sp_addrolemember N'base_user', N'KCICLLC\marshc'
GO
EXEC sp_addrolemember N'base_user', N'KCICLLC\ohaverk'
GO
EXEC sp_addrolemember N'base_user', N'KCICLLC\pikeg'
GO
EXEC sp_addrolemember N'base_user', N'khanm'
GO
EXEC sp_addrolemember N'base_user', N'labellj'
GO
EXEC sp_addrolemember N'base_user', N'madlp'
GO
EXEC sp_addrolemember N'base_user', N'maloneyd'
GO
EXEC sp_addrolemember N'base_user', N'mollardj'
GO
EXEC sp_addrolemember N'base_user', N'monahanc'
GO
EXEC sp_addrolemember N'base_user', N'nguyenn'
GO
EXEC sp_addrolemember N'base_user', N'onyiukeo'
GO
EXEC sp_addrolemember N'base_user', N'owenss'
GO
EXEC sp_addrolemember N'base_user', N'petryszaka'
GO
EXEC sp_addrolemember N'base_user', N'potterm'
GO
EXEC sp_addrolemember N'base_user', N'prossc'
GO
EXEC sp_addrolemember N'base_user', N'schultzea'
GO
EXEC sp_addrolemember N'base_user', N'shockleym'
GO
EXEC sp_addrolemember N'base_user', N'tonge'
GO
EXEC sp_addrolemember N'base_user', N'wongc'
GO
EXEC sp_addrolemember N'base_user', N'youngc'
GO
EXEC sp_addrolemember N'base_user', N'zongc'
GO
GRANT CREATE PROCEDURE TO [base_user]
GRANT CREATE VIEW TO [base_user]
