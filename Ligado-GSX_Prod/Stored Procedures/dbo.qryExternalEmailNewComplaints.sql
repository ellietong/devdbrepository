SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailNewComplaints]
AS

	SET NOCOUNT ON

	BEGIN


		/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
		a new complaint is uploaded that day. - BEH
		 
		1-28-12 - CAS modified to query data QC'ed between yesterday and current day at 4 PM so that query goes off those time stamps and not the current system date
		which could cause some notifications to be lost.
		*/

		--Declare and set email inputs.
		DECLARE @Subject nvarchar(100)
		DECLARE @LogoHeader nvarchar(2000)

		DECLARE @Today4PM datetime
		DECLARE @Yesterday4PM datetime
		SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')
		SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')


		SET @Subject =
		'GSX: New Complaints Uploaded ' + CONVERT(varchar, GETDATE(), 107)
		--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
		--KCIC logo is temporarily being pulled from D. Snyder's Google site	


		SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


		--Create a temp table for Complaints which have been uploaded in the past day.
		--DocTitle and FirmAbb are retreived by parsing the documentURL.
		DECLARE @NewComplaintsTable TABLE (
			DocID int,
			docTitle nvarchar(500),
			FirmAddressID int,
			lName nvarchar(50),
			fName nvarchar(50),
			ClaimantPersonalInfoID bigint,
			ServiceDate date,
			ComplaintID bigint,
			Matter nvarchar(50),
			Jurisdiction nvarchar(100)
		)

		INSERT INTO @NewComplaintsTable
			SELECT DISTINCT
				COALESCE(d.DocumentID, 0),
				COALESCE(d.DocumentTitle, ''),
				COALESCE(cl.DefenseCounselAddressID, 0),
				COALESCE(cpi.LastName, ''),
				COALESCE(cpi.FirstName, ''),
				COALESCE(cpi.ClaimantPersonalInfoID, 0),
				MAX(COALESCE(a.ServiceDate, '1/1/1900')),
				COALESCE(d.ComplaintID, 0),
				COALESCE(Matter, ''),
				COALESCE(Jurisdiction,'')
			FROM tblDocuments d
			INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		INNER JOIN tblclaimlawsuit cl ON cl.ClaimLawsuitID = mcd.ClaimLawsuitID
		LEFT JOIN tblclaim c ON c.ClaimID = cl.ClaimID
		LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.LawsuitID
		LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
		LEFT JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselAddressID = cl.DefenseCounselAddressID
		LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
			WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
			AND ClaimantDocumentTypeID IN (8, 4, 33)
			AND RIGHT(CreatedBy, 8) = 'kcic.com'
			--AND COALESCE(Accepted, 0) <> 1
			AND IsPrimaryForClaim = 1
			AND IsCurrentClaim = 1
			GROUP BY	COALESCE(d.DocumentID, 0),
				COALESCE(d.DocumentTitle, ''),
				COALESCE(cl.DefenseCounselAddressID, 0),
				COALESCE(cpi.LastName, ''),
				COALESCE(cpi.FirstName, ''),
				COALESCE(cpi.ClaimantPersonalInfoID, 0),
				COALESCE(d.ComplaintID, 0),
				COALESCE(Matter, ''),
				COALESCE(Jurisdiction,'')


		DECLARE @NewDocumentsTable TABLE (
			DocID int,
			docTitle nvarchar(500),
			FirmAddressID int,
			lName nvarchar(50),
			fName nvarchar(50),
			DocType nvarchar(50),
			ClaimantPersonalInfoID bigint,
			ComplaintID bigint,
			Matter nvarchar(50),
			Jurisdiction nvarchar(100)
		)
		INSERT INTO @NewDocumentsTable
			SELECT
				d.DocumentID,
				COALESCE(d.DocumentTitle, ''),
				cl.DefenseCOunselAddressID,
				cpi.LastName,
				cpi.FirstName,
				ClaimantDocumentType,
				cpi.claimantpersonalinfoid,
				d.ComplaintID,
				COALESCE(Matter, ''),
				COALESCE(Jurisdiction,'')

			FROM tblDocuments d
			INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		INNER JOIN tblclaimlawsuit cl ON cl.ClaimLawsuitID = mcd.ClaimLawsuitID
		LEFT JOIN tblclaim c ON c.ClaimID = cl.ClaimID
		LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.LawsuitID
		LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
		LEFT JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselAddressID = cl.DefenseCounselAddressID
		LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		LEFT JOIN tblClaimantDocumentType cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID
		LEFT JOIN tblJurisdiction j on j.JurisdictionID = l.JurisdictionID
			WHERE datecreated BETWEEN @Yesterday4PM AND @Today4PM
			AND d.ClaimantDocumentTypeID NOT IN (8, 4, 33, 40, 41, 42)
			AND RIGHT(CreatedBy, 8) = 'kcic.com'
			AND COALESCE(Accepted, 0) <> 1
			AND IsPrimaryForClaim = 1
			AND IsCurrentClaim = 1
			GROUP BY	d.DocumentID,
						d.DocumentTitle,
						cl.DefenseCOunselAddressID,
						LastName,
						FirstName,
						ClaimantDocumentType,
						cpi.claimantpersonalinfoid,
						d.ComplaintID,
						COALESCE(Matter, ''),
						COALESCE(Jurisdiction,'')

		DECLARE @ClaimantPerDoc TABLE (
			DocID int,
			ClaimantList nvarchar(max)
		)
		DECLARE	@DocID int,
				@LastDocID int,
				@Claimant nvarchar(max),
				@ClaimantID bigint,
				@ClaimantList nvarchar(max)

		DECLARE Cur_Complaints CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			DocID,
			COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
			claimantpersonalInfoID
		FROM @NewComplaintsTable
		UNION
		SELECT
			DocID,
			COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
			claimantpersonalInfoID
		FROM @NewDocumentsTable
		OPEN Cur_Complaints
		FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			IF (LEN(@ClaimantList) = 0
				OR @ClaimantList IS NULL)
			BEGIN

				SET @ClaimantList = '<ul> <a href="https://ligado.kcic.com/Claims/ClaimantDetails?ClientSiteID=19&ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + @Claimant + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
			END
			ELSE
			BEGIN

				SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="https://ligado.kcic.com/Claims/ClaimantDetails?ClientSiteID=19&ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + COALESCE(@Claimant, '') + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'

			END
			SET @LastDocID = @DocID
			FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
			PRINT @@FETCH_STATUS
			IF @DocID <> @LastDocID
				OR @@FETCH_STATUS <> 0
			BEGIN

				INSERT INTO @ClaimantPerDoc

					SELECT
						@LastDocID,
						@ClaimantList

				SET @ClaimantList = NULL
			END
		END
		CLOSE Cur_Complaints
		DEALLOCATE Cur_Complaints



		--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

		DECLARE	@firmaddressid nvarchar(10),
				@firmURL nvarchar(100)

		DECLARE firm_Curs CURSOR FOR
		SELECT
			nct.FirmAddressID,
			'https://ligado.kcic.com/Claims/Dashboard?ClientSiteID=19'
		FROM @NewComplaintsTable nct
		GROUP BY nct.FirmAddressID
		UNION
		SELECT
			doc.FirmAddressID,
			'https://ligado.kcic.com/Claims/Dashboard?ClientSiteID=19'
		FROM @NewDocumentsTable doc
		GROUP BY doc.FirmAddressID


		OPEN firm_curs
		FETCH NEXT FROM firm_curs INTO @firmaddressid, @firmURL
		WHILE @@FETCH_STATUS = 0
		BEGIN

			--Declare and populate the body of the email for individual firms
			DECLARE @BodyMessage nvarchar(max)
			--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
			--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
			SET @BodyMessage = @LogoHeader +
			'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	
 
 
--->
</STYLE>'



			--Set table column headings and populate the cells

			IF (SELECT
					COUNT(docTitle)
				FROM @NewComplaintsTable
				WHERE FirmAddressID = @firmAddressid)
				> 0
			BEGIN
				SET @BodyMessage = @BodyMessage + '<H4>The following complaints have been uploaded in the last 24 hours: </H4>'
				SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +


				N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants (ClaimantID) </th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Served*</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jurisdiction</th>' +


				CAST((SELECT
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + docTitle + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + cl.ClaimantList + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + C.Matter + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + CAST(MAX(ServiceDate) AS varchar(max)) + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + C.Jurisdiction + ' </td>',
					''
				FROM @NewComplaintsTable C
				INNER JOIN @ClaimantPerDoc cl ON C.DocID = cl.DocID
				WHERE C.FirmaddressID = (SELECT
					@firmaddressid)
				GROUP BY	docTitle,
							cl.ClaimantList,
							C.Matter,
							c.Jurisdiction

				FOR xml PATH ('tr'), TYPE)
				AS nvarchar(max)) +

				N'</table>' + N'<br/>'

			END
			ELSE
			BEGIN
				SET @BodyMessage = @BodyMessage + '<H4>No Complaints have been uploaded in the last 24 hours. </H4>'
			END
			IF (SELECT
					COUNT(docTitle)
				FROM @NewDocumentsTable
				WHERE FirmAddressID = @FirmAddressID)
				> 0
			BEGIN

				SET @BodyMessage = @BodyMessage + '<H4>The following documents have been uploaded in the last 24 hours: </H4>'

				SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +


				N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants (ClaimantID) </th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jurisdiction</th>' +

				CAST((SELECT
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + docTitle + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + cl.ClaimantList + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + d.Matter + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + CAST(MAX(DocType) AS varchar(max)) + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + d.Jurisdiction + ' </td>',
					''

				FROM @NewDocumentsTable d
				INNER JOIN @ClaimantPerDoc cl ON d.DocID = cl.DocID
				WHERE FirmAddressID = (SELECT
					@firmaddressid)
				GROUP BY	docTitle,
							cl.ClaimantList,
							DocType,
							d.Matter,
							d.Jurisdiction
				FOR xml PATH ('tr'), TYPE)
				AS nvarchar(max)) +
				N'</table>' + N'<br/>'
			END
			ELSE
			BEGIN
				SET @BodyMessage = @BodyMessage + '<H4>No other new documents have been uploaded in the last 24 hours. </H4>'
			END

			--Undo the HTML stripping from the xml path function above so embedded tags work
			SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

			--Link for document library
			SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT
				@firmURL)
			+ '>here</A> to access the document(s).</b><br><br>'

			--Service Date Assumption 

			IF (SELECT
					COUNT(docTitle)
				FROM @NewComplaintsTable
				WHERE FirmAddressID = @FirmAddressID)
				> 0
			BEGIN
				SET @BodyMessage = @BodyMessage + '<br>*Note: Date served is the latest service date for claimants'' lawsuits.<br><br>'
			END

			--KCIC address and disclaimer
			SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


			DECLARE @RecipientList nvarchar(500)
			SET @RecipientList = ''
			DECLARE @CCRecipientList nvarchar(500)
			SET @CCRecipientList = ''

			DECLARE @Contact nvarchar(500)
			DECLARE contact_curs CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY FOR
			SELECT
			DISTINCT
				email
			FROM vEmailRecipients e
			WHERE FirmAddressID = @FirmAddressID
			AND e.RoleID = 55


			OPEN contact_curs
			FETCH NEXT FROM contact_curs INTO @contact
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @RecipientList = @RecipientList + @Contact + '; '
				FETCH NEXT FROM contact_curs INTO @contact
			END
			CLOSE contact_curs
			DEALLOCATE contact_curs



			--QC Recipient List
			--Set @Bodymessage = @BodyMessage + '****Recipients:' + @RecipientList
			PRINT @RecipientList
			--Execute stored procedure to send the email to that firm
			EXEC msdb.dbo.sp_send_dbmail --The Profile_Name sends from [client]@kcic.com email address
			@profile_name = 'Claims',
			--@recipients = 'hagep@kcic.com;cornellt@kcic.com',--;shockleym@kcic.com;cornellt@kcic.com;youngc@kcic.com',
			@recipients = @RecipientList,
			@blind_copy_recipients = 'gsx@kcic.com',
			@subject = @subject,
			@body = @BodyMessage,
			@body_format = 'html'


			UPDATE [KGA-ClaimsAdmin].dbo.tblComplaintTracking
			SET	ExternalEmailDate = GETDATE(),
				ExternalRecipient = LEFT(@RecipientList, 30)
			WHERE ((ComplaintID IN (SELECT
				ComplaintID
			FROM @NewComplaintsTable
			WHERE FirmAddressID = @firmaddressid
			AND ComplaintID IS NOT NULL)
			OR ComplaintID IN (SELECT
				ComplaintID
			FROM @NewDocumentsTable
			WHERE FirmAddressID = @firmaddressid
			AND ComplaintID IS NOT NULL)
			)) AND ExternalEmailDate is NULL 

			--Move to the next firm to create a new email
			FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmurl
		END
		CLOSE firm_curs
		DEALLOCATE firm_curs


	END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints] TO [power_user]
GO
