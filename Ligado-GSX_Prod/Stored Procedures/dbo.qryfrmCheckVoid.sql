SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCedure [dbo].[qryfrmCheckVoid] (@CheckID int)
as
Set NoCount On

Update tblCheck Set SourceAmount = 0, CheckFundingSourceID = 3
Where CheckID = @CheckID

Update tblPaymentDefense Set PaymentAmount = 0
Where CheckID = @CheckID

Update tblPaymentDefense Set IsFullyPaid = 0
Where DefenseInvoiceID in (Select DefenseInvoiceID From tblPaymentDefense where CheckID = @CheckID)

Update tblPaymentSettlement Set PaymentAmount = 0
Where CheckID = @CheckID

Update tblPaymentSettlement Set IsFullyPaid = 0
Where ClaimSettlementID in (Select ClaimSettlementID From tblPaymentSettlement where CheckID = @CheckID)


Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckVoid] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckVoid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckVoid] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheckVoid] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckVoid] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheckVoid] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckVoid] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckVoid] TO [power_user]
GO
