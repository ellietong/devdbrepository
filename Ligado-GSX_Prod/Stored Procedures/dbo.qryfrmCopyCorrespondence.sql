SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE procedure [dbo].[qryfrmCopyCorrespondence] (@correspondenceID int) as

set nocount on

--DECLARE @correspondenceid int
--SET @correspondenceid = 1

declare @newcorrespondenceid int

--Copy Correspondence Record
insert into tblCorrespondence (CorrespondenceDate, CorrespondenceTypeID, CorrespondenceModeID, Notes, 
	Attachments, ConfirmReceipt) 
select CorrespondenceDate, CorrespondenceTypeID, CorrespondenceModeID, Notes, 
	Attachments, ConfirmReceipt
	from tblCorrespondence where correspondenceid = @correspondenceid

--Determine new correspondence id
set @newcorrespondenceid = (select max(correspondenceid) from tblCorrespondence)

--Copy Correspondence contacts
insert into tblcorrCarriercontact (correspondenceid, CarrierContactID, RecipientTypeID) 
select @newcorrespondenceid, CarrierContactID, RecipientTypeID
	from tblcorrCarriercontact where correspondenceid = @correspondenceid
	
--Copy Internal contacts
insert into tblCorrInternalContact (correspondenceid, InternalContactID, RecipientTypeID) 
select @newcorrespondenceid, InternalContactID, RecipientTypeID
from tblCorrInternalContact where correspondenceid = @correspondenceid

--Copy Notice Defense
insert into tblcorrespondencenoticedefense (correspondenceid, NoticeDefenseID, CorrespondenceNoticeDefense) 
select @newcorrespondenceid, NoticeDefenseID, CorrespondenceNoticeDefense
	from tblcorrespondencenoticedefense where correspondenceid = @correspondenceid

--Copy Original Notice Letter
insert into tblresponse (CorrespondenceID, NoticeResponseID) 
select @newcorrespondenceid, NoticeResponseID from tblResponse where CorrespondenceID = @correspondenceid

--Copy Follow up for notice
insert into tblfollowup (CorrespondenceID, FollowUp, TaskCategoryID, InternalContactID, 
	[PartofSitePacket?], Completed, DateCompleted, FollowUpNotes, CompletedContactID) 
select @newcorrespondenceid, FollowUp, TaskCategoryID, InternalContactID, 
	[PartofSitePacket?], Completed, DateCompleted, FollowUpNotes, CompletedContactID 
from tblFollowUp where CorrespondenceID = @correspondenceid

--Copy Sites Noticed
insert into tblsitenoticed (CorrespondenceID, SiteID) 
select @newcorrespondenceid, SiteID 
from tblSiteNoticed where CorrespondenceID=@correspondenceid

--Copy Policies Noticed
insert into tblpolicynoticed (CorrespondenceID, PolicyID) 
select @newcorrespondenceid, PolicyID
from tblPolicyNoticed where CorrespondenceID=@correspondenceid

--Copy Supporting Documents
insert into tblSupportingDocument (CorrespondenceID, SupportingDocumentTypeID, Description, InternalLink) 
select @newcorrespondenceid, SupportingDocumentTypeID, Description, InternalLink
from tblSupportingDocument where CorrespondenceID=@correspondenceid

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyCorrespondence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyCorrespondence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyCorrespondence] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCopyCorrespondence] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyCorrespondence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyCorrespondence] TO [power_user]
GO
