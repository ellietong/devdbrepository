SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qrysrptCheckDetailSettlement] (@CheckID int)

AS
Set NoCount On


Declare @SGTotals table (SettlementGroupID int, CountOfTotalSettlements int)
Insert Into @SGTotals
Select SettlementGroupID, COUNT(*)
From tblClaimSettlement cs
Group By SettlementGroupID

Select CheckID, cs.SettlementGroupID as SettlementGroup, COUNT(*) as CountOfSettlements, CountOfTotalSettlements, SUM(SettlementAmount) as SettlementAmount, Sum(PaymentAmount) as PaymentAmount, 
Case When MiddleName IS Null then FirstName + ' ' + LastName ELSE
		FirstName + ' ' + MiddleName + ' ' + LastName end as ClaimantName, c.ClaimantPersonalInfoID AS ClaimantID, SocialSecurityNumber AS SSN, SettlementDate AS SettleDate, IsFullyPaid AS FullyPaid
from tblPaymentSettlement ps
Inner Join tblClaimSettlement cs on cs.ClaimSettlementID = ps.ClaimSettlementID
Inner Join tblClaimSettlementGroup sg on sg.SettlementGroupID = cs.SettlementGroupID
Inner Join @SGTotals sgt on sgt.SettlementGroupID=cs.SettlementGroupID
inner join tblClaimLawsuit cl on cl.ClaimLawsuitID=cs.ClaimLawsuitID
inner join tblClaim c on c.ClaimID=cl.ClaimID
inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID=c.ClaimantPersonalInfoID
Where CheckID = @CheckID
Group By CheckID, cs.SettlementGroupID, CountOfTotalSettlements, Case When MiddleName IS Null then FirstName + ' ' + LastName ELSE
		FirstName + ' ' + MiddleName + ' ' + LastName end, c.ClaimantPersonalInfoID, SocialSecurityNumber, SettlementDate, IsFullyPaid



Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailSettlement] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailSettlement] TO [power_user]
GO
