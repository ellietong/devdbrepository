SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[qryfrmCopyPolicy]
(@policyID int)
AS

SET NOCOUNT ON

--DECLARE @PolicyID int
--SET @policyID = 1

DECLARE @newPolicyID int

--Copy Policy Record 
INSERT INTO tblPolicy (CarrierID, DoNotReport, InitialDataEntry, InitialDataEntryDate, InsuranceProgramID, MinPStartDate, NamedInsuredID, PolicyNotes, PolicyNum, PolicyPDF, PolicyReview, PolicyReviewDate, PolicyReviewed, PolPicPDFLink)
	SELECT CarrierID, DoNotReport, InitialDataEntry, InitialDataEntryDate, InsuranceProgramID, MinPStartDate, NamedInsuredID, PolicyNotes, PolicyNum, PolicyPDF, PolicyReview, PolicyReviewDate, PolicyReviewed, PolPicPDFLink
	FROM tblpolicy
	WHERE PolicyID = @policyID

--Determine new policy id
SET @newPolicyID = (SELECT MAX(PolicyID) FROM tblPolicy)

--Copy Policy Dates
INSERT INTO tblPolicyDates (ActualPStartDate, ActualPEndDate, AnnualPeriod, PEndDate, PolicyID, PStartDate)
	SELECT ActualPStartDate, ActualPEndDate, AnnualPeriod, PEndDate, @newPolicyID, PStartDate
	FROM tblPolicyDates
	WHERE PolicyID = @policyID

--Copy Broker Policy 
INSERT INTO tblBrokerPolicy (BrokerID, BrokerRefID, BrokerTypeID, PolicyBrokerNotes, PolicyID)
	SELECT BrokerID, BrokerRefID, BrokerTypeID, PolicyBrokerNotes, @newPolicyID
	FROM tblBrokerPolicy
	WHERE PolicyID = @policyID

--Copy Policy Limits
INSERT INTO tblPolicyLimits (AggregateLimit, AttachmentPoint, Layer, LayerPerOccLimit, LimitNote, LimitTypeID, PerOccLimit, PolicyID, PreviouslyExhaustedAggregate, SIR, SIRAggregateLimit)
	SELECT AggregateLimit, AttachmentPoint, Layer, LayerPerOccLimit, LimitNote, LimitTypeID, PerOccLimit, @newPolicyID, PreviouslyExhaustedAggregate, SIR, SIRAggregateLimit
	FROM tblPolicyLimits
	WHERE PolicyID = @policyID

--Copy Policy Additional Named Insured
INSERT INTO tblAdditionalNamedInsured (AdditionalNamedInsTypeID, Bates, EffectiveDate, NamedInsuredID, PolicyID)
	SELECT AdditionalNamedInsTypeID, Bates, EffectiveDate, NamedInsuredID, @newPolicyID
	FROM tblAdditionalNamedInsured
	WHERE PolicyID = @policyID
	
--Copy Policy Issues
INSERT INTO tblIssues (IssueDate, IssueDescription, IssueTypeID, MngrApproval, MngrReviewDate, MngrReviewNeeded, PolicyID, Resolution, Resolved, ResolvedBy)
	SELECT IssueDate, IssueDescription, IssueTypeID, MngrApproval, MngrReviewDate, MngrReviewNeeded, @newPolicyID, Resolution, Resolved, ResolvedBy
	FROM tblIssues
	WHERE PolicyID = @policyID
	
--Copy Policy Provisions
INSERT INTO tblProvision (PolicyID, ProvisionLabelID, ProvisionNotes, ProvisionValueID)
	SELECT @newPolicyID, ProvisionLabelID, ProvisionNotes, ProvisionValueID
	FROM tblProvision
	WHERE PolicyID = @policyID


--Copy Policy Exact Language and Associated Provisions
DECLARE @policyExactLanguageIDs TABLE(ExactLanguageID int)
DECLARE @languageCount int
DECLARE @languageCounter int
DECLARE @curExactLanguageID int
DECLARE @newExactLanguageID int

INSERT INTO @policyExactLanguageIDs (ExactLanguageID)
	SELECT ExactLanguageID
	FROM tblExactLanguage
	WHERE PolicyID = @policyID
	ORDER BY ExactLanguageID

SET @languageCounter = 0
SET @languageCount = (SELECT COALESCE(COUNT(ExactLanguageID),0) FROM @policyExactLanguageIDs)

WHILE @languageCounter < @languageCount BEGIN
	SET @curExactLanguageID = (SELECT MIN(ExactLanguageID) FROM @policyExactLanguageIDs)
	
	--Copy Policy Exact Language
	INSERT INTO tblExactLanguage (BatesEnd, BatesStart, DetailedLocationID, DoNotShow, EffectiveDate, EndorsementNum, ExactLanguage, FormID, GenericLanguageID, LanguageNotes, LocationID, PolicyID, Sort)
		SELECT BatesEnd, BatesStart, DetailedLocationID, DoNotShow, EffectiveDate, EndorsementNum, ExactLanguage, FormID, GenericLanguageID, LanguageNotes, LocationID, @newPolicyID, Sort
		FROM tblExactLanguage
		WHERE ExactLanguageID = @curExactLanguageID
		
	SET @newExactLanguageID = (SELECT MAX(ExactLanguageID) FROM tblExactLanguage)

	--Copy Policy Provisions by Exact Language
	INSERT INTO tblProvisionLanguage(ExactLanguageID, ProvisionLabelID)
		SELECT @newExactLanguageID, ProvisionLabelID
		FROM tblProvisionLanguage
		WHERE ExactLanguageID = @curExactLanguageID

	DELETE FROM @policyExactLanguageIDs WHERE ExactLanguageID = @curExactLanguageID
	SET @languageCounter = @languageCounter + 1
END


--Copy Policy Subscription Coverage
DECLARE @policySectionCoverageIDs TABLE(SubscriptionSectionCoverageID int)
DECLARE @sectionCount int
DECLARE @sectionCounter int
DECLARE @curSectionCoverageID int
DECLARE @newSectionCoverageID int

INSERT INTO @policySectionCoverageIDs (SubscriptionSectionCoverageID)
	SELECT SubscriptionSectionCoverageID
	FROM tblSubscriptionSectionCoverage
	WHERE PolicyID = @policyID
	ORDER BY SubscriptionSectionCoverageID

SET @sectionCounter = 0
SET @sectionCount = (SELECT COALESCE(COUNT(SubscriptionSectionCoverageID),0) FROM @policySectionCoverageIDs)

WHILE @sectionCounter < @sectionCount BEGIN
	SET @curSectionCoverageID = (SELECT MIN(SubscriptionSectionCoverageID) FROM @policySectionCoverageIDs)
	
	--Copy Policy Subscription Section
	INSERT INTO tblSubscriptionSectionCoverage (PolicyID, SubscriptionSectionID, SubscriptionSectionPercentage)
		SELECT @newPolicyID, SubscriptionSectionID, SubscriptionSectionPercentage
		FROM tblSubscriptionSectionCoverage
		WHERE SubscriptionSectionCoverageID = @curSectionCoverageID
		
	SET @newSectionCoverageID = (SELECT MAX(SubscriptionSectionCoverageID) FROM tblSubscriptionSectionCoverage)

	--Copy Policy Subscription Coverage by Subscription Section
	INSERT INTO tblSubscriptionCoverage(AcctNum, AgencyID, AgencyPercentOfSection, EndDate, GroupWithinAgencyPercent, StartDate, SubscriberID, SubscriptionCompanyPercent, SubscriptionNotes, SubscriptionSectionCoverageID, UWReference)
		SELECT AcctNum, AgencyID, AgencyPercentOfSection, EndDate, GroupWithinAgencyPercent, StartDate, SubscriberID, SubscriptionCompanyPercent, SubscriptionNotes, @newSectionCoverageID, UWReference
		FROM tblSubscriptionCoverage
		WHERE SubscriptionSectionCoverageID = @curSectionCoverageID

	DELETE FROM @policySectionCoverageIDs WHERE SubscriptionSectionCoverageID = @curSectionCoverageID
	SET @sectionCounter = @sectionCounter + 1
END

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyPolicy] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCopyPolicy] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyPolicy] TO [power_user]
GO
