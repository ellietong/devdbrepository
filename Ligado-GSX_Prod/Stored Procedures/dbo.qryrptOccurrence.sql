SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  Procedure [dbo].[qryrptOccurrence]
(@CorporateProfileID INT = NULL, @InClaim int, @selectedOccTypes nvarchar(1000) = NULL, @selectedOccGroups nvarchar(1000) = NULL) 
AS

SET NOCOUNT ON

--DECLARE @CorporateProfileID int
--SET @CorporateProfileID = 0
--DECLARE @InClaim bit
--SET @InClaim = 1

DECLARE @maxCorporateProfileID int
SELECT @maxCorporateProfileID = MAX(CorporateHistoryID) FROM vOccurrence

--Declare backup strings of the passed in lists to be used in the ultimate where clauses below
DECLARE @selectedProvisionsList nvarchar(1000)
SET @selectedProvisionsList = @selectedOccTypes
DECLARE @selectedProvisionValuesList nvarchar(1000)
SET @selectedProvisionValuesList = @selectedOccGroups

--Declare the tables to be used for the separated provision and provision value IDs
DECLARE @selectedOccTypesTable TABLE(OccTypeID int)
DECLARE @selectedOccGroupsTable TABLE(OccGroupID int)

--Prepare flags if there is only a zero in the table (and thus there should be no filter)
DECLARE @flagOccTypeFilter int
DECLARE @flagOccGroupFilter int
SET @flagOccTypeFilter = 0
SET @flagOccGroupFilter = 0
--add params for occType and occGroup
--add parsing of params into temp tables
-- add where for vars in temp tables
--Parse out occtypes	
WHILE LEN(@selectedOccTypes)>0 BEGIN
	IF CHARINDEX(',',@selectedOccTypes) = 0 BEGIN
		INSERT INTO @selectedOccTypesTable
			SELECT CONVERT(int, @selectedOcctypes)
		IF CONVERT(int,@selectedOccTypes) <> 0
			SET @flagOccTypeFilter = 1
		SET @selectedOcctypes = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedOccTypesTable
			SELECT CONVERT(int, LEFT(@selectedOccTypes, CHARINDEX(',',@selectedOccTypes)-1))
		SET @selectedOccTypes = RIGHT(@selectedOccTypes,LEN(@selectedOccTypes)-CHARINDEX(',',@selectedOccTypes))
		SET @flagOccTypeFilter = 1
	END
END
----Parse out occgroups	
WHILE LEN(@selectedOccGroups)>0 BEGIN
	IF CHARINDEX(',',@selectedOccGroups) = 0 BEGIN
		INSERT INTO @selectedOccGroupsTable
			SELECT CONVERT(int, @selectedOccGroups)
		IF CONVERT(int,@selectedOccGroups) <> 0
			SET @flagOccGroupFilter = 1
		SET @selectedOccGroups = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedoccgroupsTable
			SELECT CONVERT(int, LEFT(@selectedoccgroups, CHARINDEX(',',@selectedOccGroups)-1))
		SET @selectedOccGroups = RIGHT(@selectedOccGroups,LEN(@selectedOccGroups)-CHARINDEX(',',@selectedOccGroups))
		SET @flagOccGroupFilter = 1
	END
END

IF @InClaim = -1
BEGIN
	IF @flagOccTypeFilter = 1 AND @flagOccGroupFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, OccurrenceGroup, CorporateHistoryID, OccurrenceTypeID,
			TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, 
			Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceType, CorporateHistoryProfile
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
			AND OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
			AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
	END
	ELSE IF @flagOccTypeFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, OccurrenceGroup, CorporateHistoryID, OccurrenceTypeID,
		TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, 
		Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceType, CorporateHistoryProfile
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
			AND OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
	END
	ELSE IF @flagOccGroupFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, OccurrenceGroup, CorporateHistoryID, OccurrenceTypeID,
		TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, 
		Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceType, CorporateHistoryProfile
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
			AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
	END
	ELSE
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, OccurrenceGroup, CorporateHistoryID, OccurrenceTypeID,
		TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, 
		Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceType, CorporateHistoryProfile
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
	END
END
ELSE 
BEGIN
	IF @flagOccTypeFilter = 1 AND @flagOccGroupFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, OccurrenceGroup, CorporateHistoryID, OccurrenceTypeID,
			TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, 
			Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceType, CorporateHistoryProfile
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
			AND OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
			AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
			AND InClaim = @InClaim
	END
	ELSE IF @flagOccTypeFilter = 1
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, OccurrenceGroup, CorporateHistoryID, OccurrenceTypeID,
			TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, 
			Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceType, CorporateHistoryProfile
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
			AND OccurrenceTypeID IN (SELECT * FROM @selectedOccTypesTable)
			AND InClaim = @InClaim
	END
	ELSE IF @flagOccGroupFilter = 1
	BEGIN	
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, OccurrenceGroup, CorporateHistoryID, OccurrenceTypeID,
			TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, 
			Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceType, CorporateHistoryProfile
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
			AND OccurrenceGroupID IN (SELECT * FROM @selectedOccGroupsTable)
			AND InClaim = @InClaim
	END
	ELSE
	BEGIN
		SELECT OccurrenceID, AggregateApplies, ExpectedPayDate, InClaim, OccurrenceName, OccurrenceAddress, OccurrenceCity, OccurrenceStateID, OccurrenceGroupID, OccurrenceGroup, CorporateHistoryID, OccurrenceTypeID,
			TriggerStart1, TriggerStart1Explanation, TriggerEnd1, TriggerEnd1Explanation, TriggerStart2, TriggerEnd2, TriggerStart3, TriggerEnd3, TriggerStart4, TriggerEnd4, Cost1, Cost1Explanation, 
			Cost2, Cost2Explanation, Cost3, Cost3Explanation, Cost4, Cost5, Cost6, Cost7, Cost8, Cost9, Cost10, Cost11, Cost12, OccurrenceNote, OccurrenceType, CorporateHistoryProfile
		FROM vOccurrence
		WHERE CorporateHistoryID >= COALESCE(@CorporateProfileID, 0) AND CorporateHistoryID <= COALESCE(NULLIF(@CorporateProfileID, 0), @maxCorporateProfileID)
			AND InClaim = @InClaim
	END
END

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptOccurrence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrence] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptOccurrence] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptOccurrence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptOccurrence] TO [power_user]
GO
