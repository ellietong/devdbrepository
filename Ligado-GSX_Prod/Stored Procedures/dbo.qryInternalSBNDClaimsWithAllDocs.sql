SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROC [dbo].[qryInternalSBNDClaimsWithAllDocs]
AS
BEGIN
 
--Query run daily to send an internal email listing any SBND claims that contain full documentationand data for settled status
 
--Declare and set email inputs.
DECLARE @Subject nvarchar(100) = 'GSX: SBND Claims with Full Data/Documentation'   
DECLARE @TDStyle nvarchar(max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'


--Section 1: ClaimLawsuits who are SBND and are fully documented (and were not yet marked as fully documented)
DECLARE @BodyMessage1 nvarchar(max)
SET @BodyMessage1 = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>The following claimants are now fully documented and have to changed to the S1-B status: </H4>
<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'
 
DECLARE @ApplicableClaimLawsuits TABLE (ClaimLawsuitID int, ClaimID int)
INSERT INTO @ApplicableClaimLawsuits 
	SELECT clst.ClaimLawsuitID, ClaimID
	FROM tblClaimLawsuitStatusTracking            clst
		INNER JOIN tblClaimLawsuit                cl   ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN vClaimLawsuitIsFullyDocumented clfd ON cl.ClaimLawsuitID = clfd.ClaimLawsuitID
	WHERE ClaimLawsuitStatusID   = 25
		AND IsPrimaryStatus      =  1
		AND clfd.FullyDocumented =  1
	GROUP BY clst.ClaimLawsuitID, ClaimID
	ORDER BY clst.ClaimLawsuitID, ClaimID

UPDATE tblClaimLawsuitStatusTracking
	SET IsPrimaryStatus = 0
	WHERE ClaimLawsuitID IN(SELECT ClaimLawsuitID FROM @ApplicableClaimLawsuits)

INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, IsPrimaryStatus, statusdate, ProcessedDate)
	SELECT a.ClaimLawsuitID, 39 AS ClaimLawsuitStatusID, 1 AS IsPrimaryStatus, settlementdate as statusdate, GETDATE() AS processeddate
	FROM @ApplicableClaimLawsuits     a
		INNER JOIN tblClaimSettlement cs ON cs.claimlawsuitid = a.claimlawsuitid

DECLARE @ClaimantsDocumented TABLE(Claimant nvarchar(max), ClaimLawsuitID nvarchar(50), Matter nvarchar(50),state nvarchar(3), DocStatus nvarchar (50), SettlementDueDate date)
INSERT INTO @ClaimantsDocumented
	SELECT LastName+', '+FirstName+' ('+'<a href="https://ligado.kcic.com/Claims/ClaimantDetails?ClientSiteID=19&ClaimantPersonalInfoID=' +coalesce(CONVERT(nvarchar(50),c.ClaimantPersonalInfoID), '')+'">'+ coalesce(CONVERT(nvarchar(50), c.ClaimantPersonalInfoID), '')+'</a>'+')' AS Claimant, 
		CONVERT(nvarchar(50), acl.ClaimLawsuitID) AS ClaimLawsuitID, Matter, state, 'Claimant Fully Documented' AS DocStatus, coalesce(settlementduedate, '1/1/1900') 
	FROM @ApplicableClaimLawsuits          acl
		INNER JOIN tblClaim                c   ON acl.ClaimID = c.ClaimID
		INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblMatter               m   ON c.MatterID = m.MatterID
		INNER JOIN tblClaimLawsuit		   CL  ON cl.claimid = c.claimid
		INNER JOIN tblLawsuit				L  ON l.lawsuitid =cl.lawsuitid
		INNER JOIN tblstate					S  ON s.stateid = l.stateid
		INNER JOIN tblclaimsettlement		CS ON cs.claimlawsuitid = cl.claimlawsuitid
	GROUP BY LastName+', '+FirstName+' ('+'<a href="https://ligado.kcic.com/Claims/ClaimantDetails?ClientSiteID=19&ClaimantPersonalInfoID=' +coalesce(CONVERT(nvarchar(50),c.ClaimantPersonalInfoID), '')+'">'+ coalesce(CONVERT(nvarchar(50),
	 c.ClaimantPersonalInfoID), '')+'</a>'+')', CONVERT(nvarchar(50), acl.ClaimLawsuitID), Matter, state, coalesce(settlementduedate, '1/1/1900') 

SET @BodyMessage1 = @BodyMessage1 + 
    N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
    --Set column headings and populate cells from the cursor
    N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">ClaimLawsuitID</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">State</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Documentation Status</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Settlement Due Date</th>' +
    CAST ( (
          SELECT	
			@TDStyle AS 'td/@style', (SELECT Claimant      ) as td, '',
			@TDStyle AS 'td/@style', (SELECT ClaimLawsuitID) as td, '',
			@TDStyle AS 'td/@style', (SELECT Matter        ) as td, '',
			@TDStyle AS 'td/@style', (SELECT State        )  as td, '',
			@TDStyle AS 'td/@style', (SELECT DocStatus     ) as td, '',
			@TDStyle AS 'td/@style', (SELECT SettlementDueDate     ) as td, ''
          FROM @ClaimantsDocumented C
          ORDER BY Claimant
          FOR XML PATH( 'tr' ), type) AS NVARCHAR(MAX) ) + 
    N'</table>' +  N'<br/>' 


--Section 2: ClaimLawsuits who are S1-B status and have a dismissal loaded or claimlawsuits in state (PA, MA, DE) WHERE THERE WILL NOT BE A FINAL DISMISSAL ORDER SO WE CHANGE TO S2 ONCE PAYMENT IS ISSUED
DECLARE @BodyMessage2 nvarchar(max)
SET @BodyMessage2 = '
<H4>The following claimants are S1-B status and have a dismissal loaded or are MA, PA, DE settlements that have been billed/paid: </H4>
<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

DECLARE @S1BClaimantsWithDismissals TABLE (Claimant nvarchar(max), ClaimLawsuitID nvarchar(50), Matter nvarchar(50), SettlementAmount nvarchar(50), SettlementDate date, state nvarchar(3))
INSERT INTO @S1BClaimantsWithDismissals 
SELECT LastName+', '+FirstName+' ('+'<a href="https://ligado.kcic.com/Claims/ClaimantDetails?ClientSiteID=19&ClaimantPersonalInfoID=' +coalesce(CONVERT(nvarchar(50),c.ClaimantPersonalInfoID), '')+'">'+ coalesce(CONVERT(nvarchar(50), c.ClaimantPersonalInfoID), '')+'</a>'+')' AS Claimant, 
	CONVERT(nvarchar(50),clst.ClaimLawsuitID) AS ClaimLawsuitID, Matter, '$' + CONVERT(nvarchar(50),SettlementAmount) AS SettlementAmount, CONVERT(date, SettlementDate) AS SettlementDate, State
FROM tblClaimLawsuitStatusTracking     clst
	INNER JOIN tblClaimLawsuit         cl   ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblClaim                c    ON c.claimid = cl.claimid
	INNER JOIN tblMatter               m    ON m.matterid = c.matterid
	INNER JOIN tblClaimantPersonalInfo cpi  ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	INNER JOIN tblClaimSettlement      cs   ON cs.claimlawsuitid = cl.claimlawsuitid
	INNER JOIN tblLawsuit				l	ON l.lawsuitid = cl.LawsuitID
	INNER JOIN tblState					S	ON s.stateid = l.stateid
WHERE IsPrimaryStatus = 1 
	AND (ClaimLawsuitStatusID in (39) or (claimlawsuitstatusid = 25 and processeddate<'3/31/2014'))
	AND (cl.ClaimLawsuitID    IN (SELECT ClaimLawsuitID       
								 FROM tblClaimLawsuitStatusTracking 
								 WHERE IsPrimaryStatus <> 1
									   AND ClaimLawsuitStatusID IN (SELECT ClaimLawsuitStatusID 
																	FROM tblClaimLawsuitStatus         
																	WHERE ClaimLawsuitStatus LIKE '%DM2%'))
		OR
		----PA, MA, DE ARE STATES WHERE THERE WILL NOT BE A FINAL DISMISSAL ORDER SO WE CHANGE TO S2 ONCE PAYMENT IS ISSUED--------------------
		 cl.claimlawsuitid in (select cl.claimlawsuitid
							  from tblclaimsettlement cs
									inner join tblclaimlawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
									inner join tblPaymentSettlement ps on ps.ClaimSettlementID = cs.ClaimSettlementID
									inner join tbllawsuit l on l.lawsuitid = cl.lawsuitid
									inner join tblstate s on s.stateid = l.stateid
									inner join tblcheck ch on ch.checkid = ps.checkid
									inner join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
							   where state in ('ma', 'de', 'pa') and IsPrimaryStatus =1 and clst.claimlawsuitstatusid <>23
									and cl.ClaimLawsuitID in (select claimid from tbloccurrence where occurrencetypeid = 1)
									AND IsFullyPaid = 1))
														


SET @BodyMessage2 = @BodyMessage2 + 
    N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + 
    --Set column headings and populate cells from the cursor
    N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">ClaimLawsuitID</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Settlement Amount</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Settlement Date</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">State</th>' +
    CAST ( (
          SELECT	
			@TDStyle AS 'td/@style', (SELECT Claimant        ) as td, '',
			@TDStyle AS 'td/@style', (SELECT ClaimLawsuitID  ) as td, '',
			@TDStyle AS 'td/@style', (SELECT Matter          ) as td, '',
			@TDStyle AS 'td/@style', (SELECT SettlementAmount) as td, '',
			@TDStyle AS 'td/@style', (SELECT SettlementDate  ) as td, '',
			@TDStyle AS 'td/@style', (SELECT State  ) as td, ''
          FROM @S1BClaimantsWithDismissals C
          ORDER BY Claimant
          FOR XML PATH( 'tr' ), type) AS NVARCHAR(MAX) ) + 
    N'</table>' +  N'<br/>' 

--Combine the sections, undo the HTML stripping from the xml path function above so embedded tags work, and add the footer
DECLARE @BodyMessage nvarchar(max)
SET @BodyMessage = COALESCE(@BodyMessage1,'') + COALESCE(@BodyMessage2,'')
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'
 
--Check if the email needs to be sent, and send if anything exists in any section
IF (SELECT COUNT(DISTINCT Claimant) FROM @ClaimantsDocumented) <> 0 OR (SELECT COUNT(DISTINCT Claimant) FROM @S1BClaimantsWithDismissals) <> 0 
BEGIN
	--Send email
	EXEC msdb.dbo.sp_send_dbmail
	@profile_name='Claims',
	--@recipients = 'shockleym@kcic.com', 
	@recipients = 'gsx@kcic.com',
	@copy_recipients = 'shockleym@kcic.com; lynette.jones@spx.com', 
	@subject= @subject,
	@body= @BodyMessage,
	@body_format = 'html'
END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalSBNDClaimsWithAllDocs] TO [power_user]
GO
