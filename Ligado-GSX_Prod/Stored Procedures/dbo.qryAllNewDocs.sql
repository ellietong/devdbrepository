SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryAllNewDocs]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT * FROM tblDocuments WHERE DetailDocumentTypeID NOT IN (40, 41, 42)

END


GO
GRANT VIEW DEFINITION ON  [dbo].[qryAllNewDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryAllNewDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryAllNewDocs] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryAllNewDocs] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryAllNewDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryAllNewDocs] TO [power_user]
GO
