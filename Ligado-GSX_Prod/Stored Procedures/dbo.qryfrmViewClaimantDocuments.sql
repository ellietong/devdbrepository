SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmViewClaimantDocuments] (@ClaimantPersonalInfoID int)
as

Select cd.ClaimantDocumentID, 
	hmcd.ClaimantPersonalInfoID,
	case when DateTimeEntered is NULL then ClaimantDocumentType+ ': ' + DocumentTitle else ClaimantDocumentType + ': ' +DocumentTitle+ ': ' + convert(nvarchar,month(DateTimeEntered))+'/'+convert(nvarchar,day(DateTimeEntered))+'/'+convert(nvarchar,Year(DateTimeEntered)) end AS DocumentTypeWithDate
	, 'https://admin.kcicextranet.com' + DocumentURL as DocumentLink
From tblClaimantDocuments cd 
Inner Join tblClaimantDOcumentType ct on cd.claimantdocumentTypeID = ct.ClaimantDocumentTypeID
Inner join tblHistoricalMultiClaimantDocuments hmcd on hmcd.ClaimantDocumentID = cd.ClaimantDocumentID
Where hmcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID

UNION

Select d.DocumentID AS ClaimantDocumentID,
	mcd.ClaimantPersonalInfoID,
	case when DateCreated is NULL then ClaimantDocumentType + ': ' + DocumentTitle else
	ClaimantDocumentType + ': ' + DocumentTitle + ': ' + convert(nvarchar,month(DateCreated))+'/'+convert(nvarchar,day(DateCreated))+
	'/'+convert(nvarchar,Year(DateCreated)) end AS DocumentTypeWithDate,
	'https://admin.kcicextranet.com' + DocumentURL as DocumentLink
FROM tblDocuments d
Inner Join tblClaimantDocumentType ct on d.DetailDocumentTypeID = ct.ClaimantDocumentTypeID
inner join tblMultiClaimantDocument mcd on mcd.DocumentID = d.DocumentID
WHERE DetailDocumentTypeID NOT IN (40,41) AND mcd.ClaimantPersonalInfoID = @CLaimantPersonalInfoID 

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewClaimantDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewClaimantDocuments] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmViewClaimantDocuments] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewClaimantDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewClaimantDocuments] TO [power_user]
GO
