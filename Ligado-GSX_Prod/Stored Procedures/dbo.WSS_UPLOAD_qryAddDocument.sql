SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryAddDocument] (@DetailDocumentTypeID		int,					--aka ClaimantDocumentTypeID
											  @ClaimantPersonalInfoID	bigint = NULL,
											  @CreatedBy				nvarchar(250),
											  @DateCreated				datetime,
											  @DocumentURL				nvarchar(4000),
											  @DocumentTitle			nvarchar(4000),
											  @SPDocId					nvarchar(50),	
											  @Comments					nvarchar(4000) = NULL,	--comments are optional	
											  @FirmID					int = NULL,
											  @DocumentSourceID			int,
											  --These are null unless the document is an Invoice document
											  @InvoiceNumber			nvarchar(50) = NULL,
											  @Period					nvarchar(50) = NULL,
											  @DatePayable				datetime = NULL,
											  @FeeAmount				money = NULL,
											  @DisbursementAmount		money = NULL,
    										  @Adjustment				money = NULL,
    										  @AdjustmentNotes			nvarchar(4000) = NULL,
    										  @StatusID					int = NULL,
    										  @MatterID					int = NULL,
    										  @ExpertFirmID				int = NULL,
    										  @PrivateComments			nvarchar(4000) = NULL,
    										  @Approved					bit = NULL,
    										  @ApprovedBy				nvarchar(250) = NULL,
    										  @DateApproved				datetime = NULL)

AS

DECLARE @MaxDocID int
SET @MaxDocID = (SELECT IDENT_CURRENT('tblDocuments')+IDENT_INCR('tblDocuments'))

IF(RIGHT(@CreatedBy, 11) = 'kcicllc.com') OR (@DetailDocumentTypeID IN (40, 41, 42))
BEGIN
INSERT INTO tblDocuments (DetailDocumentTypeID,	
						  ClaimantPersonalInfoID,
						  Comments,
						  DocumentURL,
						  DocumentTitle,
						  SPDocId,
						  CreatedBy,
						  DateCreated,
						  InvoiceNumber,
						  Period,
						  DatePayable,
						  FeeAmount,
						  DisbursementAmount,
						  Adjustment,
						  AdjustmentNotes,
						  StatusID,	
						  ModifiedBy,
						  DateModified,
						  FirmID,
						  DocumentSourceID,
						  MatterID,
						  ExpertFirmID,
						  PrivateComments,
						  ApprovedBy,
						  DateApproved)
	VALUES (@DetailDocumentTypeID,
			@ClaimantPersonalInfoID,
			@Comments,
			REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
			@DocumentTitle,
			@SPDocId,
			@CreatedBy,
			@DateCreated,
			@InvoiceNumber,
			@Period,
			@DatePayable,
			@FeeAmount,
			@DisbursementAmount,
			@Adjustment,
			@AdjustmentNotes,
			@StatusID,	
			@CreatedBy,
			@DateCreated,	
			@FirmID,
			@DocumentSourceID,
			@MatterID,
			@ExpertFirmID,
			@PrivateComments,
			@ApprovedBy,
			@DateApproved)

SELECT @MaxDocID
END

ELSE
BEGIN
INSERT INTO tblDocuments (DetailDocumentTypeID,	
						  ClaimantPersonalInfoID,
						  Comments,
						  DocumentURL,
						  DocumentTitle,
						  SPDocId,
						  CreatedBy,
						  DateCreated,
						  InvoiceNumber,
						  Period,
						  DatePayable,
						  FeeAmount,
						  DisbursementAmount,
						  Adjustment,
						  AdjustmentNotes,
						  StatusID,	
						  ModifiedBy,
						  DateModified,
						  FirmID,
						  DocumentSourceID,
						  MatterID,
						  ExpertFirmID,
						  PrivateComments,
						  ApprovedBy,
						  DateApproved,
						  Accepted,
						  AcceptedBy,
						  DateAccepted)
	VALUES (@DetailDocumentTypeID,
			@ClaimantPersonalInfoID,
			@Comments,
			REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
			@DocumentTitle,
			@SPDocId,
			@CreatedBy,
			@DateCreated,
			@InvoiceNumber,
			@Period,
			@DatePayable,
			@FeeAmount,
			@DisbursementAmount,
			@Adjustment,
			@AdjustmentNotes,
			@StatusID,	
			@CreatedBy,
			@DateCreated,	
			@FirmID,
			@DocumentSourceID,
			@MatterID,
			@ExpertFirmID,
			@PrivateComments,
			@ApprovedBy,
			@DateApproved,
			1,
			'dana@kcicllc.com',
			GETDATE())
SELECT @MaxDocID
END			
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddDocument] TO [power_user]
GO
