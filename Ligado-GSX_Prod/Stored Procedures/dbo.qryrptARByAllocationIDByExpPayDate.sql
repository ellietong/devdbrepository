SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByAllocationIDByExpPayDate]
(@AllocationID INT = Null)
As
SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 3

DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM tblAllocation

DECLARE @Policy TABLE (policyid int, carrierid int)
INSERT INTO @Policy

SELECT policyid, carrierid
FROM vPolicy
GROUP BY policyid, carrierid

SELECT ar.AllocationID, a.AllocationDescription, Year(o.ExpectedPayDate) AS ExpPayYear, SUM(ar.Allocation) AS SumOfAllocation, SUM(ar.AllocationIndemnity) AS SumofAllocationIndemnity,
	SUM(ar.AllocationDefense) as SumofAllocationDefense, SUM(ar.AllocationDefenseOutsideLimits) AS SumofAllocationDefenseOutsideLimits, MAX(ar.DateTime) AS MaxOfDateTime
FROM @Policy p 
INNER JOIN vAllocationResults ar ON p.PolicyID = ar.PolicyID 
INNER JOIN vOccurrence o ON o.OccurrenceID = ar.OccurrenceID 
INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID
WHERE a.AllocationID >= COALESCE(@allocationid, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationid, 0), @maxAllocationID)
GROUP BY ar.AllocationID, a.AllocationDescription, Year(o.ExpectedPayDate)

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAllocationIDByExpPayDate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByAllocationIDByExpPayDate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAllocationIDByExpPayDate] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptARByAllocationIDByExpPayDate] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByAllocationIDByExpPayDate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByAllocationIDByExpPayDate] TO [power_user]
GO
