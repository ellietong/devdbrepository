SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersInactiveByPlaintiffsCounsel] (@MatterID int = NULL, 
																						   @MonthID  int, 
																						   @YearID   int)
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MatterID int
--DECLARE @MonthID int
--DECLARE @YearID int
--SET @MatterID = NULL
--SET @MonthID  = NULL
--SET @YearID   = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with ProcessedDate, 
--      and all original ProcessedDate references have been replaced with ProcessedDate.

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

DECLARE @Month int
DECLARE @Year int
DECLARE @PendingDate date
SET @Month = @MonthID
SET @Year = @YearID
SET @PendingDate = DATEADD(MONTH,1,CONVERT(date,CONVERT(varchar,@Month)+'/1/'+CONVERT(varchar,@Year)))

--Find all claims with a status change post pending date(2a,2b)
DECLARE @FindClaimantsWithPostPendingDateStatusChange TABLE (Claimant int, 
															 ClaimLawsuitID int)
INSERT INTO @FindClaimantsWithPostPendingDateStatusChange
	SELECT c.ClaimantPersonalInfoID AS Claimant, 
		cl.ClaimLawsuitID
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl				  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l						  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	WHERE ProcessedDate >= @PendingDate
		AND ClaimLawsuitStatusID NOT IN (21,29) 
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
		AND IsCurrentClaim = 1
		AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
	GROUP BY c.ClaimantPersonalInfoID, cl.ClaimLawsuitID

--For all claims with a status change post pending AND a stausdate before pending date: find the max status date before pending date (2a)
DECLARE @FindMaxProcessedDateBeforePendingDate TABLE (ClaimLawsuitID2 int, 
												   MaxDate		   datetime)
INSERT INTO @FindMaxProcessedDateBeforePendingDate
	SELECT cl.ClaimLawsuitID AS ClaimLawsuitID2,
		MAX(ProcessedDate) AS MaxDate
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl								ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimLawsuitStatusTracking clst				ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN @FindClaimantsWithPostPendingDateStatusChange ft ON ft.claimlawsuitID = cl.ClaimLawsuitID
	WHERE ProcessedDate < @PendingDate
	GROUP BY cl.ClaimLawsuitID

--For claims with a status change post pending AND a ProcessedDate before pending date: find only those claims with a max open status before pending date (2a)
DECLARE @CheckForOnlyOpenStatusBeforePendingDate TABLE (ClaimLawsuitID3 int)
INSERT INTO @CheckForOnlyOpenStatusBeforePendingDate
	SELECT cl.ClaimLawsuitID AS ClaimLawsuitID3
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl					  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l							  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst	  ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimLawsuitStatus cls			  ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		INNER JOIN @FindMaxProcessedDateBeforePendingDate fm ON fm.ClaimLawsuitID2 = clst.ClaimLawsuitID AND clst.ProcessedDate = fm.MaxDate
	WHERE clst.ClaimLawsuitStatusID IN (21,29)

--Check to see the status count of all claims with a status change post pending date (2b)
DECLARE @CheckForStatuscount TABLE (ClaimLawsuitID4 int, 
									StatusCount     int)
INSERT INTO @CheckForStatuscount
	SELECT cl.ClaimLawsuitID AS ClaimLawsuitID4, 
		COUNT(ProcessedDate) AS StatusCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl								 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimLawsuitStatusTracking clst				 ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN @FindClaimantsWithPostPendingDateStatusChange fcw ON fcw.claimlawsuitID = cl.ClaimLawsuitID
	GROUP BY cl.ClaimLawsuitID

--Union table to combine main components of Pending Count
DECLARE @CombinedTable TABLE (HighLevelDisease nvarchar(100), 
							  PlaintiffCounsel nvarchar(200), 
							  DiseaseCount int)
INSERT INTO @CombinedTable
	--Counts All Claims that have their status date before the pending date and are CURRENTLY still open (1)
	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		PlaintiffCounsel,
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS DiseaseCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl				  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l						  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimDisease cd				  ON c.ClaimID = cd.ClaimID
		INNER JOIN tblPlaintiffCounselAddress pca	  ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		LEFT  JOIN tblDisease d						  ON d.DiseaseID = cd.DiseaseID
		LEFT  JOIN tblDisease_ALT da				  ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT  JOIN tblHighLevelDisease hld			  ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
		LEFT  JOIN tblState s						  ON s.StateID = l.StateID
	WHERE (ProcessedDate < @PendingDate OR ProcessedDate IS NULL)
		AND ClaimLawsuitStatusID IN (21, 29)
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
		AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
		AND cd.PrimaryDisease = 1
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID, PlaintiffCounsel
	UNION ALL
	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		PlaintiffCounsel,
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS DiseaseCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl						   ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l								   ON cl.LawsuitID = l.LawsuitID
		INNER JOIN @CheckForOnlyOpenStatusBeforePendingDate cf ON cf.ClaimLawsuitID3 = cl.ClaimLawsuitID
		INNER JOIN tblClaimDisease cd						   ON c.ClaimID = cd.ClaimID
		INNER JOIN tblPlaintiffCounselAddress pca			   ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		LEFT  JOIN tblDisease d								   ON d.DiseaseID = cd.DiseaseID
		LEFT  JOIN tblDisease_ALT da						   ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT  JOIN tblHighLevelDisease hld					   ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
		LEFT  JOIN tblState s								   ON s.StateID = l.StateID
	WHERE cd.PrimaryDisease = 1
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID, PlaintiffCounsel

--Groups all results into one table
SELECT TOP 10 ct.PlaintiffCounsel, 
	SUM(ct.DiseaseCount) AS 'Count'
FROM @CombinedTable ct
	LEFT JOIN tblHighLevelDisease hld ON hld.HighLevelDisease=ct.HighLevelDisease
GROUP BY ct.PlaintiffCounsel
ORDER BY SUM(ct.DiseaseCount) DESC, ct.PlaintiffCounsel

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersInactiveByPlaintiffsCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersInactiveByPlaintiffsCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersInactiveByPlaintiffsCounsel] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersInactiveByPlaintiffsCounsel] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersInactiveByPlaintiffsCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryTopTenPendingMattersInactiveByPlaintiffsCounsel] TO [power_user]
GO
