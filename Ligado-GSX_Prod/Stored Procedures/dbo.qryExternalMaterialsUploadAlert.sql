SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---- =============================================
---- Author:            <Author,,Name>
---- Create date: <Create Date,,>
---- Description: <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalMaterialsUploadAlert] 
AS
BEGIN

/*Query run daily to send a separate alert email to the designated Invoice Contacts for each firm when
an invoice has been accepted or rejected. - BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @Header nvarchar(2000)

Declare @Today4PM datetime
Declare @Yesterday4PM datetime
SET @Yesterday4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-1))+' 16:00:00.000')
SET @Today4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 16:00:00.000')

SET @Subject =    'Dana Companies, LLC: Materials Upload ' + convert(varchar, getdate(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    
SET @Header = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>
<H4>The following materials have been uploaded in the last 24 hours:</H4>'

--Create a temp table for any documents uploaded in last 24 hours, organized by claimant
DECLARE @Documents TABLE (DocumentID bigint, DocumentTitle nvarchar(MAX), DateCreated datetime, 
            ClaimantDocumentType nvarchar(MAX), ClaimantName nvarchar(MAX), ClaimantPersonalInfoID bigint, CreatedBy nvarchar(MAX), Firm nvarchar(MAX))
INSERT INTO @Documents
SELECT d.DocumentID, DocumentTitle, DateCreated, ClaimantDocumentType, 
		CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
        THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, mcd.ClaimantPersonalInfoID,
        CreatedBy, DefenseCounsel
FROM tblDocuments d
      LEFT JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
      LEFT JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
      LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
      LEFT JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
WHERE DateCreated BETWEEN GETDATE()-1 and GETDATE() 
      AND (detailDocumentTypeID IN (2, 4, 13, 12, 14, 15, 18, 19, 20, 24, 35, 38, 44, 45, 46, 47, 48, 49, 51))
      AND RIGHT(CreatedBy, 11) <> 'kcicllc.com'
UNION
SELECT d.DocumentID, DocumentTitle, DateCreated, ClaimantDocumentType, 
		CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
        THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, mcd.ClaimantPersonalInfoID,
        CreatedBy, DefenseCounsel
FROM tblDocuments d
      INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
      INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
      INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
      LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
      LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
      LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
      INNER JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
WHERE mcd.QCDate BETWEEN @Yesterday4PM and @Today4PM
	AND detailDocumentTypeID IN (8,4,33)
	AND RIGHT(CreatedBy, 11) = 'kcicllc.com'
	AND FirmID IS NOT NULL
	AND IsPrimaryForClaim = 1
	AND IsCurrentClaim = 1	
	AND l.StateID = 31
ORDER BY mcd.ClaimantPersonalInfoID



--print CSS styling and beginning of table (headers, etc)
DECLARE @BodyMessage nvarchar(max)
SET @BodyMessage = @Header
SET @BodyMessage = @BodyMessage 
--'<STYLE TYPE="text/css">
--<!--
--TABLE {border-collapse: collapse; border: 1px solid rgb(0,121,193); color: #ffffff; width: 800px;}
      
--TD    {font-family: Goudy Old Style; font-size: 10pt; text-align: left; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000}

--.subHeader{
--	  border: 1px solid rgb(0,121,193);
--      padding: .1em;
--      color: #ffffff;
--      background: rgb(163, 25, 132);
--      font-weight: bolder;
      
--}
--.subHeader b{
--	font-weight: bold;
--}
--TH    {border: 1px solid rgb(0,121,193); padding: .3em; color: #F6ECF0; background: rgb(0,121,193); v-align: center; text-align: left;
--      }
--TH A {
--	font-weight: bold; color: white;

--}
--#extra {
--      text-align: left;
--      padding: .3em 0 .3em 0;
--      margin-left: 5px;
--      margin-right: 5px;
--      }
----->
--</STYLE>'


DECLARE @DocumentID bigint, @DocumentTitle nvarchar(MAX), @ClaimantDocumentType nvarchar(MAX), @ClaimantName nvarchar(MAX),
		@ClaimantPersonalInfoID bigint, @CreatedBy nvarchar(MAX), @Firm nvarchar(MAX), @LastClaimantPersonalInfoID bigint
		
--initialize lastCPIID
SET @LastClaimantPersonalInfoID = 0

--Declare a cursor to loop through claimant documents
DECLARE doc_Curs CURSOR FOR
SELECT DocumentID, DocumentTitle, ClaimantDocumentType, COALESCE(ClaimantName, 'ERROR: No Claimant'), COALESCE(ClaimantPersonalInfoID, 99999999),
		CreatedBy, Firm
FROM @Documents
ORDER BY ClaimantName, ClaimantDocumentType

OPEN doc_Curs
FETCH NEXT FROM doc_Curs INTO @DocumentID, @DocumentTitle, @ClaimantDocumentType, @ClaimantName, @ClaimantPersonalInfoID, @CreatedBy, @Firm

While @@FETCH_STATUS = 0

BEGIN
	IF(@ClaimantPersonalInfoID <> @LastClaimantPersonalInfoID)
	BEGIN
		IF(@LastClaimantPersonalInfoID <> 0)
			SET @BodyMessage = @BodyMessage + N'</table>' +  N'<br>' 
		--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
		SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid rgb(0,121,193); color: #ffffff; width: 800px;">' + 
					N'<tr><th colspan="4"><b><a href="https://www.kcicextranet.com/clients/dana/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(MAX), @ClaimantPersonalInfoID) + '">'  + @ClaimantName + '&nbsp;(' + CONVERT(nvarchar(MAX), @ClaimantPersonalInfoID) + ')</a></b></th></tr>' + 
					'<tr><td style="border: 1px solid rgb(0,121,193); padding: .3em; color: #F6ECF0; background: rgb(0,121,193); v-align: center; text-align: middle;" class="subHeader"><b>Title</b></td><td style="border: 1px solid rgb(0,121,193); padding: .3em; color: #F6ECF0; background: rgb(0,121,193); v-align: center; text-align: middle;" class="subHeader"class="subHeader"><b>Type</b></td><td style="border: 1px solid rgb(0,121,193); padding: .3em; color: #F6ECF0; background: rgb(0,121,193); v-align: center; text-align: middle;" class="subHeader" class="subHeader"><b>Firm</b></td><td style="border: 1px solid rgb(0,121,193); padding: .3em; color: #F6ECF0; background: rgb(0,121,193); v-align: center; text-align: middle;" class="subHeader" class="subHeader"><b>Created By</b></td></tr>'
			--SET @BodyMessage = @BodyMessage + '<tr><td colspan="2"><b><a href="https://admin.kcicllc.com/cp/dana/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(MAX), @ClaimantPersonalInfoID) + '">'  + @ClaimantName + '</a></b></td></tr>' + 
			--			
	END		
    --start the table row
    SET @BodyMessage = @BodyMessage + '<tr>'
    SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: left; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000"><a href="https://www.kcicextranet.com/clients/dana/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(MAX), @DocumentID) + '">'  + @DocumentTitle +  '</a></td>'
    SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: left; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">' + @ClaimantDocumentType + '</td>'
    SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: left; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">' + @Firm + '</td>'
    SET @BodyMessage = @BodyMessage + '<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: left; border: 1px solid rgb(0,121,193); border-style: solid; padding: .3em; color: #000000">' + @CreatedBy + '</td></tr>'
    
    SET @LastClaimantPersonalInfoID = @ClaimantPersonalInfoID
		


FETCH NEXT FROM doc_Curs INTO @DocumentID, @DocumentTitle, @ClaimantDocumentType, @ClaimantName, @ClaimantPersonalInfoID, @CreatedBy, @Firm
END
CLOSE doc_Curs
DEALLOCATE doc_Curs

--final table closing tag
SET @BodyMessage = @BodyMessage + N'</table>' +  N'<br>' 



SET @BodyMessage = @BodyMessage+ N'<br><br>KCIC, LLC<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--only send if there are documents...
	IF (SELECT COUNT(*) FROM @Documents) > 0
	BEGIN
		--Execute stored procedure to send email
		EXEC msdb.dbo.sp_send_dbmail
		--The Demo ClientProfile sends from Demo Client@kcicllc.com
		@profile_name='DanaProfile',
		@recipients = 'sreich@dehay.com; dmyles@dehay.com',--@RecipientList,
		@copy_recipients = 'dana@kcicllc.com',
		--@recipients = 'snyderd@kcicllc.com',
		@subject= @subject,
		@body= @BodyMessage,
		@body_format = 'html'
		--print @BodyMessage
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalMaterialsUploadAlert] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalMaterialsUploadAlert] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalMaterialsUploadAlert] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryExternalMaterialsUploadAlert] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalMaterialsUploadAlert] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalMaterialsUploadAlert] TO [power_user]
GO
