SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Proc [dbo].[qrysfrmClaimsForDismissal2]
(@DocumentID bigint)

As
Set NoCount On

select LastName + coalesce(', ' + FirstName + ' ' + MiddleName,', ' + FirstName ,', ' +  MiddleName, '') as ClaimantName,
 SocialSecurityNumber, IndividualDocketNumber, CaseCaption, DocketNumber, clst.StatusNotes, 
 State as FullNameState, cl.ClaimLawsuitID,clst.ClaimLawsuitStatusTrackingID,  ProcessedDate, StatusDate, ClaimLawsuitStatus, clst.ClaimLawsuitStatusID
  from tblClaimantPersonalInfo cpi
  inner join tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
  inner join tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
  inner join tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
 Left join (select * from tblClaimLawsuitStatusTracking where IsPrimaryStatus =1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
 LEFT join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
  inner join tblLawsuit l on l.LawsuitID=cl.LawsuitID
  inner join tblState s on s.StateID=l.StateID
  
  where DocumentID = @DocumentID
  
  Group By DocketNumber, IndividualDocketNumber,cpi.ClaimantPersonalInfoID,LastName, MiddleName, FirstName, CaseCaption, DocketNumber, clst.StatusNotes,
 State, cl.ClaimLawsuitID, SocialSecurityNumber,clst.ClaimLawsuitStatusTrackingID, ProcessedDate, ClaimLawsuitStatus, StatusDate, clst.ClaimLawsuitStatusID
  --Order By ClaimantName, cpi.ClaimantPersonalInfoID, DocketNumber
  
RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForDismissal2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForDismissal2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForDismissal2] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForDismissal2] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForDismissal2] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForDismissal2] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForDismissal2] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForDismissal2] TO [power_user]
GO
