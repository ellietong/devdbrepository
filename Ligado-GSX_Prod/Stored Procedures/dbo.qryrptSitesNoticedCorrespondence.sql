SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryrptSitesNoticedCorrespondence]
(@CorrespondenceID int)
AS
SET NOCOUNT ON
SELECT sn.SiteNoticedID, sn.CorrespondenceID, sn.SiteID, s.SiteName, i.InsuranceProgram, s.SiteName + coalesce(' (' + i.InsuranceProgram + ')', '') AS SiteNameInfo
FROM   tblSiteNoticed sn INNER JOIN
      tblEnviroSitesDetail s ON sn.SiteID = s.SiteID
      left join tblCorpHistInsProgram ci on ci.CorporateHistoryID = s.CorporateHistoryID
	 INNER JOIN tblInsuranceProgram i ON ci.InsuranceProgramID = i.InsuranceProgramID
WHERE @CorrespondenceID = sn.CorrespondenceID
ORDER BY s.SiteName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSitesNoticedCorrespondence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSitesNoticedCorrespondence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSitesNoticedCorrespondence] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSitesNoticedCorrespondence] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSitesNoticedCorrespondence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSitesNoticedCorrespondence] TO [power_user]
GO
