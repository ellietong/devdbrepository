SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE   Procedure [dbo].[qryALLCreateOccurrenceRecordSet]
(@previousAllocationID int, @considerPreviousAllocation bit = 0, @strSortOrder varchar(500),
@triggerStartField nvarchar(250), @triggerEndField nvarchar(250), @defenseCosts nvarchar(250), @costFields nvarchar(250), @strWhere nvarchar(2000))
AS
SET NOCOUNT ON

--DECLARE @previousAllocationID       INT            = 0
--DECLARE @considerPreviousAllocation BIT            = 0
--DECLARE @strSortOrder               VARCHAR(500)   = 'OccurrenceID'
--DECLARE @triggerStartField          NVARCHAR(250)  = 'TriggerStart2'
--DECLARE @triggerEndField            NVARCHAR(250)  = 'TriggerEnd2'
--DECLARE @defenseCosts               NVARCHAR(250)  = 'Cost3'
--DECLARE @costFields                 NVARCHAR(250)  = 'Cost1'
--DECLARE @strWhere                   NVARCHAR(2000) = '1=1'

DECLARE @strSQL nvarchar(4000)
SET @strSQL = 'SELECT o.originaloccurrenceID, o.OccurrenceID, OccurrenceName, o.OccurrenceTypeID, AggregateApplies, 
			case when day(' + @triggerStartField +') <=15 then DateFromPArts(year(' + @triggerStartField +'), month(' + @triggerStartField +'), 1) else case when month(' + @triggerStartField +') = 12 then DateFromPArts(year(' + @triggerStartField +'), 1, 1) else DateFromPArts(year(' + @triggerStartField +'), month(' + @triggerStartField +')+1, 1) end end AS TriggerStart, 
			' + @triggerEndField + ' AS TriggerEnd, (' + @defenseCosts + ') AS DefenseCost, CASE WHEN ' + @costFields +
	' = 0 THEN 1 ELSE (' + @defenseCosts + ')/(' + @costFields + ') END AS DefenseRatio, CorporateHistoryID, o.OccurrenceGroupID, DefenseType, '
-- If considering previous exhaustion
IF @considerPreviousAllocation = 1
BEGIN
	SET @strSQL = @strSQL + '(' + @costFields + ') - COALESCE(a.AllocationTot, 0) AS TotalCost ' +
		'FROM tblOccurrence o INNER JOIN tblOccurrenceGroup og ON o.OccurrenceGroupID = og.OccurrenceGroupID ' +
		'LEFT JOIN (SELECT AllocationID, OccurrenceID, Sum(Allocation) AS AllocationTot, Sum(PV) AS PVTot FROM tblAllocationResults ' +
		'WHERE AllocationID = ' + CONVERT(nvarchar(50), @previousAllocationID) + ' GROUP BY AllocationID, OccurrenceID) a ON ' +
		'o.OccurrenceID = a.OccurrenceID WHERE a.AllocationID = ' + CONVERT(nvarchar(50), @previousAllocationID) + ' OR a.AllocationID IS NULL '
	IF @strWhere IS NOT NULL OR @strWhere <> ''
		SET @strSQL = @strSQL + ' AND ' + @strWhere
END
ELSE
BEGIN
	SET @strSQL = @strSQL + '(' + @costFields + ') AS TotalCost FROM tblOccurrence o INNER JOIN tblOccurrenceGroup og ON o.OccurrenceGroupID = og.OccurrenceGroupID '+
		' INNER JOIN tblOccurrenceType ot ON ot.OccurrenceTypeID = o.OccurrenceTypeID '
	IF @strWhere IS NOT NULL OR @strWhere <> ''
		SET @strSQL = @strSQL + ' WHERE ' + @strWhere
END
SET @strSQL = @strSQL + ' ORDER BY ' + @strSortOrder
EXECUTE sp_executesql @strSQL
--SELECT @strSQL
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLCreateOccurrenceRecordSet] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLCreateOccurrenceRecordSet] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLCreateOccurrenceRecordSet] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryALLCreateOccurrenceRecordSet] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryALLCreateOccurrenceRecordSet] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLCreateOccurrenceRecordSet] TO [power_user]
GO
