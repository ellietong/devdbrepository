SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryDefenseCreateNewScenarioCombinations]
(@Scenario int = Null)
AS
SET NOCOUNT ON


----Will be input scenario
--DECLARE @Scenario int
--SET @Scenario = 2

--Create temp table
CREATE TABLE #tempPolicyCombos(PolicyID int)

--Create temp table
CREATE TABLE #tempCurrentCombos(ComboID int)

--Create string to know what the columns of your pivot table should be
DECLARE @pivotColumns nvarchar(2000)
SET @pivotColumns = ''
SELECT @pivotColumns =   @pivotColumns + '[' + cast(ProvisionLabelID AS nvarchar(10)) + '],'
FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @pivotColumns = LEFT(@pivotColumns, LEN(@pivotColumns) - 1)

--Create string to know what the columns of your  temp table should be 
--(same columns as @pivotColums just different format for how it will be used)
DECLARE @alterColumns nvarchar(2000)
SET @alterColumns = ''
 SELECT @alterColumns = @alterColumns + '[' + cast(ProvisionLabelID AS varchar) + '] int,'
 FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @alterColumns = LEFT(@alterColumns, LEN(@alterColumns) - 1)

--Create a string to see if any of the provisions are not coded for a particular policy
--(same columns as @pivotColums just different format for how it will be used)
DECLARE @SumofColumns nvarchar(2000)
SET @SumofColumns = ''
 SELECT @SumofColumns = @SumofColumns + '[' + cast(ProvisionLabelID as varchar) + '] +'
 FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @SumofColumns = LEFT(@SumofColumns, LEN(@SumofColumns) - 1)

--Find number of provisions this scenario is using -- this is used along with finding out what they actually are
--to see if the combination already exists.  (Count is necessary because a combination could use the 3 provisions,
--but also have a 4th and 5th for example so we would not want to use these combinations)
DECLARE @ProvisionCount int
SELECT @ProvisionCount= COUNT(provisionlabelid) 
FROM tblScenarioProvisions WHERE ScenarioID = @Scenario


DECLARE @sql nvarchar(4000)
--Update temp table for policy data to have appropriate columns
SET @sql = 'ALTER TABLE #tempPolicyCombos
ADD ' + @alterColumns
EXEC(@sql)
--perform transform on Policy Provision data and insert it into temp table
SET @sql = 'INSERT #tempPolicyCombos
SELECT  * 
FROM
(SELECT policyid, provisionlabelid, provisionvalueid
FROM tblProvision) datatable
pivot
(MIN(provisionvalueid) for provisionlabelid in (' + @pivotColumns + ')) AS pvt'
EXEC (@sql)


--Update temp table for current combination data to have appropriate columns
SET @sql = 'ALTER TABLE #tempCurrentCombos
ADD ' + @alterColumns
EXEC(@sql)
--perform transform on current combination data and insert it into temp table  
SET @sql = 'INSERT #tempCurrentCombos
select * 
from
(select combination, provisionlabelid, provisionvalueid
from tblcombination where combination in (select combination from tblcombination group by combination
having COUNT(provisionlabelid) = ' +CONVERT(nvarchar(50), @ProvisionCount) + ' )) datatable
pivot
(min(provisionvalueid) for provisionlabelid in ('+@pivotColumns+ ')) pivottable
WHERE (' + @SumofColumns + ') is not null'
EXEC (@sql)



--insert into temp table those coded combos that are not already combos
CREATE TABLE #tempNewCombos(comboID int)
SET @sql = 'ALTER TABLE #tempNewCombos
ADD  ' + @alterColumns 
EXEC(@sql)

SET @sql = 'INSERT #tempNewCombos
SELECT 0,' + @pivotColumns + ' 
from #tempPolicyCombos
WHERE (' + @SumofColumns + ') is not null
EXCEPT
SELECT 0, ' + @pivotColumns + ' 
from #tempCurrentCombos   

group by ' + @pivotColumns
EXEC(@sql)



--update combination number to increment, starting at the max currently existing
DECLARE @counter int
SET @counter = (select coalesce(MAX(Combination),0) from tblcombination)
UPDATE #tempNewCombos
SET @counter = comboid = @counter + 1




--unpivot new combos and insert into tblcombination
SET @sql = 'insert into tblcombination
SELECT	ComboID,
	ProvisionLabelID,
	ProvisionValueID
FROM	(
		SELECT	*
		FROM	#tempNewCombos
		) AS p
UNPIVOT	(
		provisionvalueID 
		FOR ProvisionLabelID IN (' + @pivotColumns + ')
	) AS unpvt'

EXEC (@sql)

--insert new combinations into tblCombinationTreatment
INSERT INTO tblcombinationtreatment (scenarioid, combination)
SELECT @Scenario, comboid
FROM #tempNewCombos

--insert existing combinations into tblCombinationTreatment that did not exist for this particular scenario
INSERT INTO tblcombinationtreatment (ScenarioID, Combination)
SELECT @Scenario, comboid
FROM #tempCurrentCombos 
where ComboID not in (select combination from tblcombinationtreatment where scenarioid = @Scenario)


DROP TABLE #tempPolicyCombos
DROP TABLE #tempNewCombos
DROP TABLE #tempCurrentCombos

GO
GRANT VIEW DEFINITION ON  [dbo].[qryDefenseCreateNewScenarioCombinations] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryDefenseCreateNewScenarioCombinations] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryDefenseCreateNewScenarioCombinations] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryDefenseCreateNewScenarioCombinations] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryDefenseCreateNewScenarioCombinations] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryDefenseCreateNewScenarioCombinations] TO [power_user]
GO
