SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptCashReconciliation] (@CurrentStatement MONEY,
													@LastStatement MONEY,
													@CurrentCashActivity MONEY,
													@BalanceStartDate datetime, @BalanceEndDate datetime)

As
Set NoCount On

/*
10/14/11 - CAS - removed references to MatterID and added funding type ID 3 to defesne payments.
10/17/11 - CAS - add to misc cash the non-check items outstanding.
*/

--Declare @BalanceStartDate datetime, @BalanceEndDate datetime, @CurrentStatement MONEY, @LastStatement MONEY, @CurrentCashActivity MONEY
--Set @BalanceStartDate = '9/1/2014'
--Set @BalanceEndDate = '9/30/2014'
--SET @CurrentStatement = 1367620.89
--SET @LastStatement = 734760.72
--SET @CurrentCashActivity = 1168694.54


Declare @Detail table (FundingType nvarchar(250), FundingSource nvarchar(250), SourceDate datetime, SourceNumber nvarchar(250), 
Indemnity money, Defense money, MiscFees money, CashReceipts money, Comment nvarchar(Max), CheckStatus DATETIME, SectionHeader NVARCHAR(250),
AmountReconciled MONEY, AmountOutstanding MONEY)
Insert Into @Detail

--Reconciliation Data
--Defense
Select FundingType, FundingSource, SourceDate, SourceNumber, 0, SourceAmount*-1 as SourceAmount, 0, 0, coalesce(v.vendorfirm, dif.defensecounsel), CheckClearedDate, 'Reconciliation Detail', Null, Null
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentDefense pd on pd.CheckID = c.CheckID
	Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
	left Join tblDefenseCounselAddress dif on dif.DefenseCounselAddressID = di.DefenseCounselAddressID
	left join tblVendorFirm V on v.VendorFirmID = di.VendorFirmID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.CheckFundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.CheckFundingTypeID
Where (CheckClearedDate >= @BalanceStartDate and coalesce(CheckClearedDate, VoidDate) <= @BalanceEndDate)  AND c.CheckFundingTypeID IN (1,3) and c.CheckFundingSourceID not in (4,5)
Group by FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, coalesce(v.vendorfirm, dif.defensecounsel), CheckClearedDate

Union ALL

--Settlement
Select  FundingType, FundingSource, SourceDate, SourceNumber, sum(PaymentAmount)*-1 as PaymentAmount, 0, 0, 0, PlaintiffCounsel, CheckClearedDate, 'Reconciliation Detail', Null, Null
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
	Inner Join tblClaimSettlement cs on ps.ClaimSettlementID = cs.ClaimSettlementID
	Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.CheckFundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.CheckFundingTypeID
Where (CheckClearedDate >= @BalanceStartDate and CheckClearedDate <= @BalanceEndDate) AND  c.CheckFundingTypeID = 2 and c.CheckFundingSourceID not in (4,5)
Group by  FundingType, FundingSource, SourceDate, SourceNumber, PlaintiffCounsel, CheckClearedDate

Union ALL
--Misc Fees
Select CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, CashActivityAmount, 0, CashActivityNotes, CashActivityDate, 'Reconciliation Detail', Null, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
Where (CashActivityDate >= @BalanceStartDate and CashActivityDate <= @BalanceEndDate) AND CashActivityAmount < 0

Union ALL
-- cash receipts
Select CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, 0, CashActivityAmount, CashActivityNotes, CashActivityDate, 'Reconciliation Detail', Null, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Where (CashActivityDate >= @BalanceStartDate and CashActivityDate <= @BalanceEndDate) AND CashActivityAmount > 0

union all

--Outstanding Detail Defense and Indemnity and Misc Act
--Defense
Select FundingType, FundingSource, SourceDate, SourceNumber, 0, PaymentAmount*-1 as SourceAmount,
 0, 0, coalesce(v.vendorfirm, dif.defensecounsel), CheckClearedDate, 'Outstanding Detail', Null, Null
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentDefense pd on pd.CheckID = c.CheckID
	Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
	left Join tblDefenseCounselAddress dif on dif.DefenseCounselAddressID = di.DefenseCounselAddressID
	left join tblVendorFirm V on v.VendorFirmID = di.VendorFirmID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.CheckFundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.CheckFundingTypeID
Where (CheckClearedDate is null or CheckClearedDate = ''or CheckClearedDate >@BalanceEndDate) 
	AND  c.CheckFundingTypeID IN (1,3) and c.CheckFundingSourceID not in (3,4,5)
	--AND c.SourceDate <=@BalanceEndDate
Group by FundingType, FundingSource, SourceDate, SourceNumber, PaymentAmount*-1, coalesce(v.vendorfirm, dif.defensecounsel), CheckClearedDate

Union ALL

--Settlement
Select FundingType, FundingSource, SourceDate, SourceNumber, sum(PaymentAmount*-1) as SourceAmount, 0, 0, 0, PlaintiffCounsel, CheckClearedDate, 'Outstanding Detail', Null, Null
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
	Inner Join tblClaimSettlement cs on ps.ClaimSettlementID = cs.ClaimSettlementID
	Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.CheckFundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.CheckFundingTypeID
Where (CheckClearedDate is null or CheckClearedDate = '' or CheckClearedDate >@BalanceEndDate ) 
AND c.CheckFundingTypeID = 2 and c.CheckFundingSourceID not in (3,4,5)
--AND c.SourceDate <=@BalanceEndDate
Group by FundingType, FundingSource, SourceDate, SourceNumber, PlaintiffCounsel, CheckClearedDate

Union All
-- Misc Activity 
Select CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, CashActivityAmount,0, CashActivityNotes, CashActivityDate, 'Outstanding Detail', Null, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Where (CashActivityDate > @BalanceEndDate) AND CashActivityAmount <> 0


UPDATE @Detail
SET AmountReconciled = (SELECT SUM(Indemnity + Defense + MiscFees + CashReceipts) from @Detail where SectionHeader = 'Reconciliation Detail'), 
AmountOutstanding = (SELECT SUM(Indemnity + Defense + MiscFees + CashReceipts) from @Detail where SectionHeader = 'Outstanding Detail')

Select *, @BalanceStartDate as BalanceStartDate, @BalanceEndDate as BalanceEndDate, @CurrentStatement AS CurrentStatement, @LastStatement AS LastStatement, @CurrentCashActivity AS CurrentCashActivity
from @Detail Order By SourceDate, SourceNumber

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashReconciliation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashReconciliation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashReconciliation] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryrptCashReconciliation] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashReconciliation] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCashReconciliation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashReconciliation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashReconciliation] TO [power_user]
GO
