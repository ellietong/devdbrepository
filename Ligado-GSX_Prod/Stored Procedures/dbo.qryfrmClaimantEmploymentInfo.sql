SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantEmploymentInfo] (@ClaimLawsuitID int)
as

Select *
From tblClaimLawsuitEmployment
Where ClaimLawsuitID = @ClaimLawsuitID
Order By EmpStartDate, ClaimLawsuitEmploymentID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantEmploymentInfo] TO [power_user]
GO
