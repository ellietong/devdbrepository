SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE Proc [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] (@ExhaustionDateID int)
As
SET NOCOUNT ON

--Declare @ExhaustionDateID int
--Set @ExhaustionDateID = 41

--select * from tblExhaustionDate


DECLARE @CarrierAllocation Table (OccurrenceID int, AllocatedAmount float,CarrierGroupID int ,CarrierGroupName Nvarchar (100))
Insert into @CarrierAllocation
SELECT OC.OccurrenceID, sum(e.DefenseAllocation) + Sum(e.DefenseOutsideLimitsAllocation) as AllocatedAmount, coalesce(cg.CarrierGroupID,''),coalesce(CarrierGroupName,'')
from tblExhaustion E
Join tblOccurrence OC on OC.OccurrenceID = E.OccurrenceID
inner join tblPolicyDates pd on pd.PolicyDateID = e.PolicyDateID
Join tblPolicy P on p.PolicyID = pd.PolicyID
Join tblCarrier C on c.CarrierID = p.CarrierID
left join tblcarriergroup cg on cg.CarrierGroupID = c.CarrierGroupID
where e.ExhaustionDateID = @ExhaustionDateID 
Group by OC.OccurrenceID, cg.CarrierGroupID,CarrierGroupName

--Select * from @CarrierAllocation

Declare @Firm Table (VendorFirmID bigint, FirmName varchar (100))
Insert into @Firm
Select FirmID,FirmDisplayName from vVendorFirm 
Union
Select DefenseCounselID,DefenseCounsel From tblDefenseCounsel


Select di.defenseinvoiceid, 
vf.FirmName as Firm, 
dic.ClaimLawsuitID as ClaimantLawsuitID, 
lastname+', '+firstname as ClaimantName, 
'XXX-XX-'+right(coalesce(socialsecuritynumber,''),4) as socialsecuritynumber, 
case when s.state = 'ZZ' Then '' Else s.state End As State, 
jurisdiction, 
ltrim(rtrim(dic.invoicenumber)) as invoicenumber,
invoicedate, 
Sum(case when CarrierGroupID = 13 Then AllocatedAmount Else 0 End) as Hartford, 
Sum(case when CarrierGroupID = 30 Then AllocatedAmount Else 0 End) as AllState,
Sum(case when CarrierGroupID = 31 Then AllocatedAmount Else 0 End) as ACE,
Sum(case when CarrierGroupID = 47 Then AllocatedAmount Else 0 End) as Liberty,
round(Sum(AllocatedAmount),2) as TotalAllocation,
Coalesce(AdjustedTotal,coalesce(costs,0)+coalesce(fees,0)-coalesce(adjustments,0)) as AdjustedTotal, 
o.TriggerStart1 as DOFE,
Case when cdt.ClaimantDocumentType = 'INVOICE' Then 'Invoice' When cdt.ClaimantDocumentType = 'VENDOR INVOICE' Then 'Expert Invoice' End As ClaimantDocumentType,
Fees,
Costs,
Coalesce(Adjustments,0) As Adjustments,
Coalesce(AdjustedFee,0) AS AdjustedFee,
Coalesce(AdjustedDisbursement,0) As AdjustedCost,
(dic.Fees + dic.costs) As InvoiceTotal,
Matter,
AdjustmentNotes,
OccurrenceGroup,
vf.VendorFirmID as FirmID
from tbldefenseinvoice di
inner join tbldefinvclaimantdetail dic on dic.DefenseInvoiceID = di.DefenseInvoiceID
left join tblclaimlawsuit cl on cl.ClaimLawsuitID = dic.ClaimLawsuitID
left join tblclaim c on c.claimid = cl.ClaimID
left join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
left join tbllawsuit l on l.lawsuitid = cl.lawsuitid
left join tbljurisdiction j on j.jurisdictionid = l.jurisdictionid
left join tblstate s on s.stateid = l.stateid
left Join @firm vf on vf.VendorFirmID = di.VendorFirmID
left join tblOccurrence o on dic.InvoiceDetailID = o.OccurrenceNote
inner join tbloccurrencegroup og on og.OccurrenceGroupID=o.OccurrenceGroupID
inner join @carrierallocation ca on ca.occurrenceid = o.occurrenceid
left join tblDocuments d on d.DocumentID = di.DocumentID
left join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
left join tblMatter m on m.MatterID = c.MatterID
group by di.defenseinvoiceid, 
vf.FirmName, 
dic.ClaimLawsuitID 
, lastname+', '+firstname , 
'XXX-XX-'+right(coalesce(socialsecuritynumber,''),4) , 
s.state, 
jurisdiction, 
ltrim(rtrim(dic.invoicenumber)),
invoicedate,  
Coalesce(AdjustedTotal,coalesce(costs,0)+coalesce(fees,0)-coalesce(adjustments,0)),
o.TriggerStart1,
Case when cdt.ClaimantDocumentType = 'INVOICE' Then 'Invoice' When cdt.ClaimantDocumentType = 'VENDOR INVOICE' Then 'Expert Invoice' End,
Fees,
Costs,
Coalesce(Adjustments,0),
Coalesce(AdjustedFee,0),
Coalesce(AdjustedDisbursement,0),
(dic.Fees + dic.costs),
Matter,
AdjustmentNotes,
OccurrenceGroup,
vf.VendorFirmID











GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [power_user]
GO
