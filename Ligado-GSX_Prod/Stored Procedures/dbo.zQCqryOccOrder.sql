SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[zQCqryOccOrder]
(@allocationID  int)
As

SET NOCOUNT ON

SELECT a.AllocationID, o.ExpectedPayDate, o.OccurrenceTypeID, o.TriggerStart1, o.OccurrenceID, o.OccurrenceName, MIN(ar.DateTime) AS MinOfDateTime, MAX(ar.DateTime) AS MaxOfDateTime
FROM tblOccurrence o INNER JOIN (tblAllocation a INNER JOIN tblAllocationResults ar ON a.AllocationID = ar.AllocationID) ON o.OccurrenceID = ar.OccurrenceID
WHERE a.AllocationID = @AllocationID
GROUP BY a.AllocationID, o.ExpectedPayDate, o.OccurrenceTypeID, o.TriggerStart1, o.OccurrenceID, o.OccurrenceName
ORDER BY o.ExpectedPayDate, o.OccurrenceTypeID DESC, o.TriggerStart1, o.OccurrenceID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[zQCqryOccOrder] TO [base_user]
GRANT EXECUTE ON  [dbo].[zQCqryOccOrder] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryOccOrder] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[zQCqryOccOrder] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[zQCqryOccOrder] TO [power_user]
GRANT EXECUTE ON  [dbo].[zQCqryOccOrder] TO [power_user]
GO
