SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmCaseInfo]
As

SET NOCOUNT ON

SELECT CaseInfoID, CaseName, FilePathPolicies, FilePathExternalFiles, FilePathChecks,
	FilePathClaimantDocs, FilePathCorrespondence, FilePathEnviroInvoice
FROM zCaseInfo

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCaseInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCaseInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCaseInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCaseInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCaseInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCaseInfo] TO [power_user]
GO
