SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCedure [dbo].[qryfrmPaymentSettlement] (@PlaintiffCounselAddressID int, @MatterID int)
AS
SET NOCOUNT ON

Select cs.ClaimSettlementID, PlaintiffCounselAddressID, LastName + ', ' + Coalesce(FirstName,'') + ' ' + Coalesce(MiddleName, '') as Claimant, 
DocketNumber, SettlementAmount, SettlementAmount-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount, c.MatterID--, cs.SettlementGroupID
From tblClaim c
INNER JOIN tblClaimSequence csq ON c.ClaimSequenceID = csq.ClaimSequenceID
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
INNER JOIN tblClaimSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
INNER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID 
LEFT JOIN tblClaimSettlementGroup s ON cs.SettlementGroupID = s.SettlementGroupID
--LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID
LEFT JOIN tblClaimSettlementDocumentation sd on sd.ClaimLawsuitID = cl.ClaimLawsuitID

LEFT JOIN (Select ClaimSettlementID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentSettlement ps on ca.CheckID = ps.CheckID)
a on a.ClaimSettlementID = cs.ClaimSettlementID

Where IsPrimaryStatus = 1
AND COALESCE(IsCurrentClaim,0) = 1
AND c.MatterID = @MatterID
AND l.PlaintiffCounselAddressID = @PlaintiffCounselAddressID
AND csq.ClaimSequenceID not in (4,2,3,11)
AND ((COALESCE(sd.SettlementConfirmation,0) + COALESCE(sd.ClaimantIDed,0) + COALESCE(sd.DxMedicals,0) + COALESCE(sd.DODCert,0) + 
COALESCE(sd.EstatePapers,0) + COALESCE(sd.ProductID,0) + COALESCE(sd.EmployExpoHist,0) >= 7) 
OR (DeceasedDate IS NULL AND (COALESCE(sd.SettlementConfirmation,0) + COALESCE(sd.ClaimantIDed,0) + COALESCE(sd.DxMedicals,0) + 
COALESCE(sd.ProductID,0) + COALESCE(sd.EmployExpoHist,0) >= 5)))

Group By cs.ClaimSettlementID, PlaintiffCounselAddressID, LastName + ', ' + Coalesce(FirstName,'') + ' ' + Coalesce(MiddleName, ''), 
DocketNumber, SettlementAmount, MatterID--, cs.SettlementGroupID
Having SettlementAmount-SUM(Coalesce(PaymentAmount,0)) <> 0

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentSettlement] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentSettlement] TO [power_user]
GO
