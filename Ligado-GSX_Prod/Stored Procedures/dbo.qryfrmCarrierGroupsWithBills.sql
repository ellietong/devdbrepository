SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmCarrierGroupsWithBills]
AS

SELECT CarrierGroupID, CarrierGroupName 
FROM tblCarrierGroup 
WHERE CarrierGroupID IN(SELECT CarrierGroupID
						FROM dbo.tblExhaustionDate ed
							INNER JOIN dbo.tblExhaustion e ON e.ExhaustionDateID = ed.ExhaustionDateID
							INNER JOIN dbo.tblPolicyDates pd ON pd.PolicyDateID = e.PolicyDateID
							INNER JOIN dbo.tblPolicy p ON p.PolicyID = pd.PolicyID
							INNER JOIN dbo.tblCarrier c ON c.CarrierID = p.CarrierID)
UNION 
SELECT 0, '<<Select Carrier Group>>' 
FROM tblCarrierGroup 
ORDER BY CarrierGroupName

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierGroupsWithBills] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierGroupsWithBills] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierGroupsWithBills] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierGroupsWithBills] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierGroupsWithBills] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierGroupsWithBills] TO [power_user]
GO
