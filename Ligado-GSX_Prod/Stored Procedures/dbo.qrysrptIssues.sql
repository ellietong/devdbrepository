SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qrysrptIssues]
(@policyid int = null)
AS

SET NOCOUNT ON

SELECT *
FROM tblIssues
WHERE PolicyID = @policyid 

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptIssues] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptIssues] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptIssues] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysrptIssues] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptIssues] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptIssues] TO [power_user]
GO
