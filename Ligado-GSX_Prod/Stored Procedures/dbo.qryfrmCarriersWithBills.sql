SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryfrmCarriersWithBills] (@CarrierGroupID INT)
AS

--DECLARE @CarrierGroupID INT = 13

SELECT CarrierID, CarrierName 
FROM tblCarrier 
WHERE CarrierID IN(SELECT CarrierID
				   FROM dbo.tblExhaustionDate ed
					   INNER JOIN dbo.tblExhaustion e ON e.ExhaustionDateID = ed.ExhaustionDateID
					   INNER JOIN dbo.tblPolicyDates pd ON pd.PolicyDateID = e.PolicyDateID
					   INNER JOIN dbo.tblPolicy p ON p.PolicyID = pd.PolicyID)
	AND CarrierGroupID = @CarrierGroupID

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarriersWithBills] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarriersWithBills] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarriersWithBills] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCarriersWithBills] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarriersWithBills] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarriersWithBills] TO [power_user]
GO
