SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_qryGetZipKey](@ZipQueueID uniqueidentifier)
AS
BEGIN
	--DECLARE @ZipQueueID uniqueidentifier = 'CC4729FE-FDB9-463B-B95F-070FD9C9F543'
	SELECT ZipKey
	FROM tblZipQueue
	WHERE ZipQueueID = @ZipQueueID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetZipKey] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetZipKey] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetZipKey] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_qryGetZipKey] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_qryGetZipKey] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_qryGetZipKey] TO [power_user]
GO
