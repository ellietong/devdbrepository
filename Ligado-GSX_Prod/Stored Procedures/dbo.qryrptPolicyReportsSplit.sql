SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


CREATE  Procedure [dbo].[qryrptPolicyReportsSplit] 
(@insuranceProgramID int = NULL, @namedinsuredid int = null, @limitTypeID int = NULL, @carrierID int = NULL, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL, 
@lowLayer int = NULL, @highLayer int = NULL, @lowAttach nvarchar(50) = NULL, @highAttach nvarchar(50) = NULL, @startDate int = NULL, @endDate int = NULL)

AS
SET NOCOUNT ON

--DECLARE @insuranceProgramID int 
--DECLARE @limitTypeID int
--DECLARE @carrierID int 
--DECLARE @carrierGroupID int 
--DECLARE @carrierSolvency varchar(50) 
--DECLARE @namedInsuredID int 
--DECLARE @lowLayer int 
--DECLARE @highLayer int 
--DECLARE @lowAttach nvarchar(50) 
--DECLARE @highAttach nvarchar(50) 
--DECLARE @provisionLabelID int 
--DECLARE @provisionValueID int 
--DECLARE @startDate int
--DECLARE @endDate int
--SET @insuranceProgramID = null
--SET @limitTypeID = null
--SET @carrierID = null
--SET @carrierGroupID = null
--SET @carrierSolvency = null
--SET @namedInsuredID = null
--SET @lowLayer = null
--SET @highLayer = null
--SET @lowAttach = null
--SET @highAttach = null
--SET @provisionLabelID = null
--SET @provisionValueID = null
--SET @startdate = 1970
--SET @enddate = 1980

DECLARE @maxInsuranceProgram int
DECLARE @maxNamedInsured int
DECLARE @maxLimitTypeID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)
DECLARE @maxLayer int
DECLARE @maxAttach money
DECLARE @maxEndDate int

-- Find max ID values
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM vPolicy
SELECT @maxNamedInsured = MAX(NamedInsuredID) FROM vPolicy
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxLayer = MAX(Layer) FROM vPolicy
SELECT @maxAttach = MAX(AttachmentPoint) FROM vPolicy
SELECT @maxEndDate = MAX(AnnualPeriod) FROM vPolicy
IF @carrierGroupID = 0 SET @carrierGroupID = NULL

SELECT PolicyID, MinPStartDate, CarrierID, InsuranceProgramID, NamedInsuredID, PolicyNum, PolicyNotes, PolicyPDF, InitialDataEntry, InitialDataEntryDate, PolicyReview, 
	PolicyReviewDate, NamedInsured, InsuranceProgram, AnnualPeriod AS SplitAnnualPeriod, ActualPStartDate AS SplitStart, ActualPEndDate AS SplitEnd, CarrierName, CarrierSolvency, 
	CarrierGroupName, CarrierGroupID, LimitTypeID, AttachmentPoint, PerOccLimit, Layer, LayerPerOccLimit, AggregateLimit, SIR
FROM vPolicy pinfo
WHERE InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND
	NamedInsuredID >= COALESCE(@namedinsuredid, 0) AND NamedInsuredID <= COALESCE(NULLIF(@namedinsuredID, 0), @maxNamedInsured) AND
	LimitTypeID >= COALESCE(@limitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
	CarrierID >= COALESCE(@carrierID, 0) AND CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
	(coalesce(pinfo.CarrierGroupID, 0) >= COALESCE(@carrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID, 0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(pinfo.CarrierGroupID, 0) = @carrierGroupID) AND
	CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND Layer BETWEEN COALESCE(@lowLayer, 0) AND COALESCE(@highLayer, @maxLayer) AND
	AttachmentPoint BETWEEN COALESCE(CONVERT(money, @lowAttach), 0) AND COALESCE(CONVERT(money, @highAttach), @maxAttach)
	AND AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
	and DoNotReport <>1
ORDER BY InsuranceProgram, AnnualPeriod, ActualPStartDate

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplit] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplit] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplit] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplit] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplit] TO [power_user]
GO
