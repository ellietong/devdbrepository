SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[qryrptEnviroSitesDetail] (@CorporateHistoryID int, @SiteID int, @SiteActivityID int, @SiteTypeID int)
As 
set nocount on

--DECLARE @CorporateHistoryID int 
--DECLARE @SiteID int
--DECLARE @SiteActivityID int
--DECLARE @SiteTypeID int

--SET @CorporateHistoryID =0 
--SET @SiteID =0
--SET @SiteActivityID =0
--SET @SiteTypeID =0

DECLARE @MaxCorporateHistoryID int 
DECLARE @MaxSiteID int
DECLARE @MaxSiteActivityID int
DECLARE @MaxSiteTypeID int

SET @MaxCorporateHistoryID = (Select MAX(corporatehistoryid) from tblCorporateHistoryProfile)
SET @MaxSiteID = (Select MAX(siteid) from tblEnviroSitesDetail)
set @MaxSiteActivityID = (select MAX(siteactivityid) from tblSiteActivity)
set @MaxSiteTypeID= (select MAX(sitetypeid) from tblSiteType)


SELECT sd.*, st.FullNameState AS 'State', co.Country, CorporateHistoryProfile, Status, SiteActivity,SiteType

FROM tblEnviroSitesDetail sd
LEFT join tblstate st on sd.stateid=st.stateid
LEFT join tblCountry co on sd.countryid=co.countryid
LEFT join tblCorporateHistoryProfile chp on sd.CorporateHistoryID = chp.CorporateHistoryID
LEFT join tblStatus s on s.StatusID=sd.StatusID
LEFT join tblSiteActivity sa on sa.SiteActivityID=sd.SiteActivityID
left join tblSiteType sty on sty.SiteTypeID=sd.SiteTypeID

WHERE
coalesce(sd.CorporateHistoryID,0) >= COALESCE(@CorporateHistoryID, 0) 
AND coalesce(SD.CorporateHistoryID,0) <= COALESCE(NULLIF(@CorporateHistoryID, 0), @maxCorporateHistoryID)
AND sd.SiteID >= coalesce(@SiteID,0) 
AND sd.SiteID<= coalesce(nullif(@SiteID, 0), @maxsiteid)
AND coalesce(sa.SiteActivityID,0) >= coalesce(@SiteActivityID,0)
AND coalesce(sa.SiteActivityID,0)<= coalesce(nullif(@SiteActivityID,0), @maxsiteactivityid)
AND coalesce(sty.sitetypeid,0) >= coalesce(@SiteTypeID,0)
AND coalesce(sty.SiteTypeID,0) <= coalesce(nullif(@SiteTypeID,0), @maxSitetypeid)


return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptEnviroSitesDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptEnviroSitesDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptEnviroSitesDetail] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptEnviroSitesDetail] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptEnviroSitesDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptEnviroSitesDetail] TO [power_user]
GO
