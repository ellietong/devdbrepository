SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmUnionCarrierGroupName]
AS
SET NOCOUNT ON

SELECT CarrierGroupID, CarrierGroupName
FROM tblCarrierGroup

UNION 

SELECT 0, '<<All Carrier Groups>>' AS CarrierGroupName 
FROM tblCarrier
ORDER BY CarrierGroupName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionCarrierGroupName] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionCarrierGroupName] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionCarrierGroupName] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmUnionCarrierGroupName] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUnionCarrierGroupName] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUnionCarrierGroupName] TO [power_user]
GO
