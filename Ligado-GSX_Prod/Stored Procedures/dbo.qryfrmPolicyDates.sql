SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmPolicyDates]
(@policyID int = NULL)
AS
SET NOCOUNT ON

SELECT *
FROM tblPolicyDates 
WHERE PolicyID = @policyID
ORDER BY PStartDate

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyDates] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyDates] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyDates] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyDates] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyDates] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyDates] TO [power_user]
GO
