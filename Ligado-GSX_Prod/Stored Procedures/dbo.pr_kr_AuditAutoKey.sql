SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pr_kr_AuditAutoKey] (
  @TableName varchar(30) = null,
  @KeyValue int OUTPUT)
AS
BEGIN
  set nocount on
  SELECT @KeyValue = 0
  IF @TableName IS NULL
  BEGIN
    RAISERROR('pr_kr_AuditAutoKey requires @TableName parameter', 16, 1 )
    RETURN (1)
  END

  BEGIN TRANSACTION
  UPDATE AuditLogAutoKeys SET KeyValue = KeyValue + 1
    WHERE TableName = @TableName
  IF @@ROWCOUNT = 0
    INSERT INTO AuditLogAutoKeys (TableName, KeyValue) VALUES (@TableName, 1)
  SELECT @KeyValue = KeyValue FROM AuditLogAutoKeys
    WHERE TableName = @TableName
  COMMIT TRANSACTION
END
GO
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditAutoKey] TO [base_user]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditAutoKey] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditAutoKey] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditAutoKey] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditAutoKey] TO [omniaudit_admin]
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditAutoKey] TO [power_user]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditAutoKey] TO [power_user]
GO
