SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmCourtType]
as
Select CourtTypeID, CourtType
From tblCourtType
Order By CourtType

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCourtType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCourtType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCourtType] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCourtType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCourtType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCourtType] TO [power_user]
GO
