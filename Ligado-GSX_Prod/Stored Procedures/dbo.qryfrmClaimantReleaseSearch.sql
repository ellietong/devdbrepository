SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmClaimantReleaseSearch]
as


Select cpi.ClaimantPersonalInfoID, cpi.NavLegacyID as NavigantID, cpi.FirstName, cpi.MiddleName, cpi.LastName, cpi.SocialSecurityNumber, coalesce(d.Disease, da.disease) as Disease, 
ClaimLawsuitStatus, DocketNumber, Jurisdiction, s.State, l.NavPlaintiffCounsel as PlaintiffCounsel, cs.SettlementDate, cs.SettlementAmount, cs.PaidDate, sg.SettlementGroup, cr.ClaimantReleaseID
From tblClaimantPersonalInfo cpi
Inner Join tblClaim c on cpi.ClaimantPersonalInfoID=c.ClaimantPersonalInfoID
Inner Join tblClaimDisease cd on c.ClaimID=cd.ClaimID
Left Join tblDisease d on cd.DiseaseID=d.DiseaseID
Left Join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
Inner Join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
Inner Join tblClaimLawsuitStatusTracking clt on cl.ClaimLawsuitID=clt.ClaimLawsuitID
Inner Join tblClaimLawsuitStatus cls on clt.ClaimLawsuitStatusID=cls.ClaimLawsuitStatusID
Inner Join tblLawsuit l on cl.LawsuitID=l.LawsuitID
Inner Join tblJurisdiction j on l.JurisdictionID=j.JurisdictionID
Inner Join tblState s on l.StateID=s.StateID
Inner Join tblClaimantSettlement cs on cl.ClaimLawsuitID=cs.ClaimLawsuitID
Inner Join tblSettlementGroup sg on cs.SettlementGroupID=sg.SettlementGroupID
LEFT Join tblClaimantRelease cr on cs.ClaimantSettlementID=cr.ClaimantSettlementID
Where clt.IsPrimaryStatus = 1

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantReleaseSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantReleaseSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantReleaseSearch] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantReleaseSearch] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantReleaseSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantReleaseSearch] TO [power_user]
GO
