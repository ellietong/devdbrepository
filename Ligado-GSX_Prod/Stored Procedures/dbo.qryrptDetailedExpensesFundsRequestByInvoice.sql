SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptDetailedExpensesFundsRequestByInvoice]

As

SET NOCOUNT ON


Declare @FundingInvoices table(DefenseCounselID bigint, Firm nvarchar(250), LegalBillID nvarchar(250),InvoiceNumber nvarchar(250), Period date, InvoiceTotal money, SubmittedDate date)
Insert into @FundingInvoices

--LB Non-Appeal Invoices
Select	D.DefenseCounselID as DefenseCounselID,
		D.DefenseCounsel as Firm,
		i.LegalBillID,
		i.OriginalInvoiceNumber as InvoiceNumber,
		DATENAME("mm", i.Period) + ' ' + DATENAME("yy", i.Period) as Period,
		SUM(AdjustedTotal) as InvoiceTotal,
		MAX(SubmittedDate) as SubmittedDate
From m_tblLegalBillInvoices i
	Left Join tblDefenseCounselAddress da on i.DefenseCounselAddressID=da.DefenseCounselAddressID
	Left Join tblDefenseCounsel d on da.DefenseCounselID=d.DefenseCounselID
Where	SubmittedForPayment is not null AND 
		FundsRequestDate is null AND
		AppealFlag = 0 AND
		AdjustedTotal != 0
Group By D.DefenseCounselID, d.DefenseCounsel, i.LegalBillID,i.OriginalInvoiceNumber, DATENAME("mm", i.Period) + ' ' + DATENAME("yy", i.Period)


UNION

--Appeal LB Invoices
Select	D.DefenseCounselID as DefenseCounselID,
		D.DefenseCounsel as Firm,
		i.LegalBillID,
		i.OriginalInvoiceNumber as InvoiceNumber,
		DATENAME("mm", i.Period) + ' ' + DATENAME("yy", i.Period) as Period,
		SUM(AppealTotal) as InvoiceTotal,
		MAX(SubmittedDate) as SubmittedDate
From m_tblLegalBillInvoices i
	Left Join tblDefenseCounselAddress da on i.DefenseCounselAddressID=da.DefenseCounselAddressID
	Left Join tblDefenseCounsel d on da.DefenseCounselID=d.DefenseCounselID
Where	SubmittedForPayment is not null AND 
		FundsRequestDate is null AND
		AppealFlag = 1 AND
		AppealTotal != 0
Group By D.DefenseCounselID, d.DefenseCounsel, i.LegalBillID, i.OriginalInvoiceNumber, DATENAME("mm", i.Period) + ' ' + DATENAME("yy", i.Period)


UNION

--Expert and General Invoices
Select	DefenseCounselID, 
		Coalesce(ExpertFirm, DefenseCounsel) as Firm, 
		'0' as LegalBillID,
		di.InvoiceNumber,
		Period,
		SUM(InvoiceTotal) as InvoiceTotal,
		MAX(SubmitDate) as SubmittedDate
from tblDefenseInvoice di 
left Join m_tblExpertFirm ef on ef.ExpertFirmID = di.ExpertFirmID
left join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
Where (FundsRequestDate is null and SubmitDate is not null and StatusID in (1,2)) --Need to limit out KCIC invoices
Group By DefenseCounselID, Coalesce(ExpertFirm, DefenseCounsel), di.InvoiceNumber, Period




Select f.DefenseCounselID, f.Firm, f.Period, f.LegalBillID, f.InvoiceNumber, max(f.SubmittedDate) as SubmittedDate, sum(f.InvoiceTotal) as InvoiceTotal
From @FundingInvoices f
Group BY f.DefenseCounselID, f.Firm, f.LegalBillID, f.InvoiceNumber, f.Period
Order By f.Firm, f.Period, f.InvoiceNumber


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailedExpensesFundsRequestByInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailedExpensesFundsRequestByInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailedExpensesFundsRequestByInvoice] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptDetailedExpensesFundsRequestByInvoice] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDetailedExpensesFundsRequestByInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDetailedExpensesFundsRequestByInvoice] TO [power_user]
GO
