
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[qryfrmBills] (@CarrierGroupID INT, @BillList NVARCHAR(250) = NULL)
AS

--exec dbo.[qryfrmBills]
--@CarrierGroupID = 32
--,@BillList = '160,161'

----Variables for testing----
--DECLARE @CarrierGroupID int = 32
--DECLARE @BillList NVARCHAR(250) = '160,161'

DECLARE @SQL NVARCHAR(2000)

SET @SQL = 'SELECT 
				ed.ExhaustionDateID,
				cast(ed.ExhaustionDateID as nvarchar(10)) + '' - '' + [Description] AS ''Bill Name'', 
				OccurrenceName AS ''Occurrence Name'', 
				ClaimID AS ''Claim Lawsuit ID'', 
				SUM(COALESCE(IndemnityAllocation,0) + COALESCE(SIRIndemnityAllocation,0) + COALESCE(DefenseAllocation,0) + COALESCE(DefenseOutsideLimitsAllocation,0)) AS ''Total Outstanding Bill Amount''
			FROM dbo.tblExhaustion e
				INNER JOIN dbo.tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID
				INNER JOIN dbo.tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
				INNER JOIN dbo.tblPolicy p ON p.PolicyID = pd.PolicyID
				INNER JOIN dbo.tblCarrier c ON p.CarrierID = c.CarrierID
				INNER JOIN dbo.tblOccurrence o ON e.OccurrenceID = o.OccurrenceID
				LEFT JOIN  dbo.tblAccountsReceivablePayment arp ON arp.ExhaustionID = e.ExhaustionID
			WHERE e.ExhaustionDateID IN(' + @BillList + ')
				AND CarrierGroupID = ' + CONVERT(NVARCHAR(10),@CarrierGroupID) + '
				AND arp.ExhaustionID is null				
			GROUP BY ed.ExhaustionDateID,[Description], ExhaustionDate, ClaimID, OccurrenceName, c.CarrierID
			ORDER BY ExhaustionDate'

EXECUTE (@SQL)


GO

GRANT VIEW DEFINITION ON  [dbo].[qryfrmBills] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmBills] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBills] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmBills] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBills] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmBills] TO [power_user]
GO
