SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE Procedure [dbo].[qryrptAllocationByClaimDetailsByCarrierGroup] (@ExhaustionDateID int, @LimitTypeID int, @matterid int = NULL, @CarrierGroupID int = Null)
As
Set NoCount on



----------------------------------------------------------------------------------------------------------
--Declare @ExhaustionDateID int
--Set @ExhaustionDateID = 11
 
--Declare @LimitTypeID int
--Set @LimitTypeID=1

--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = 13

--DECLARE @matterid int 
--SET @matterid = 1
------------------------------------------------------------------------------------------------------------
DECLARE @maxCarrierGroupID int
SELECT @maxCarrierGroupID = MAX(CarrierID) FROM vPolicy

Declare @maxMatterID int
Select @maxmatterid = max(matterid) from tblmatter

Declare @FirstPolicyStartDate table (occurrenceid int, FirstPolicyStartDate date)
insert into @FirstPolicyStartDate
Select occurrenceid, min(pstartdate) 
from tblpolicydates d inner join tblexhaustion e on e.policydateid = d.policydateid
where exhaustiondateid = @ExhaustionDateID and d.policydateid not in (3098, 3099) -- exclude producer policies
group by occurrenceid




Select '' as AllocationDescription, 
	e.PolicyDateID,
	o.ClaimID, 
	triggerstart1 as EarliestExposureDate, 
	triggerend1 as DiagnosisDate, 
	o.OccurrenceID, 
	OccurrenceGroup, 
	OccurrenceName, 
	ot.OccurrenceTypeID, 
	ot.occurrencetype,
	convert(money,(O.Cost1 + O.Cost3)) as CostAllocated, 
	PolicyNum,
	Case when p.carrierid = 10 then 'Producer' else CarrierName end as carriername,
	Case when p.policydateid = 3098 then triggerstart1 
		 when p.policydateid = 3099 then Null
		 else Min(PStartDate) end as MinStartDate, 
	Case when p.policydateid = 3098 then min(FirstPolicyStartDate)
		 when p.policydateid = 3099 then Null
		 else Max(PEndDate) end as MaxEndDate, 
	coalesce(Layer,0)+1 as Layer, 
	SUM(IndemnityAllocation) as AllocationIndemnity, 
	SUM(DefenseAllocation) as AllocationDefense, 
	SUM(SIRIndemnityAllocation+SIRDefenseAllocation) as SIRAllocated, 
	0 as Adjustment, 
	SUM(DefenseOutsideLimitsAllocation) as AllocationDefenseOutsideLimits,
	Matter, 
	cl.ClaimLawsuitID, 
	SettlementDate,
	ed.Description,
	p.CarrierGroupName
From tblExhaustion e
	INNER JOIN vPolicy p ON e.PolicyDateID = p.PolicyDateID
	Inner Join tblOccurrence O on e.OccurrenceID=O.OccurrenceID
	Inner Join tblOccurrenceGroup OG on OG.OccurrenceGroupID=O.OccurrenceGroupID
	Left Join tblOccurrenceType ot on o.OccurrenceTypeID = ot.OccurrenceTypeID
	left join tblclaimsettlement cs on cs.ClaimLawsuitID = o.ClaimID
	left join tblclaimlawsuit cl on cl.claimlawsuitid = o.claimid
	left join tblclaim c on c.claimid = cl.claimid
	left join tblmatter m on m.matterid = c.matterid
	Left join @FirstPolicyStartDate f on f.occurrenceid = e.OccurrenceID
	Left Join tblExhaustionDate ed on ed.ExhaustionDateID = e.ExhaustionDateID
Where coalesce(p.LimitTypeID,1) = @LimitTypeID
	 AND e.ExhaustionDateID = @ExhaustionDateID
	 --AND p.CarrierID in (Coalesce(@carrierID1,0),Coalesce(@carrierID2,0),Coalesce(@carrierID3,0),Coalesce(@carrierID4,0),Coalesce(@carrierID5,0))
	 AND CarrierGroupID >= COALESCE(@CarrierGroupID, 0) AND CarrierGroupID <= COALESCE(NULLIF(@CarrierGroupID, 0), @maxCarrierGroupID)
	 AND (c.MatterID >= COALESCE(@matterid, 0) AND c.Matterid <= COALESCE(NULLIF(@matterid, 0), @maxMatterID))
GROUP BY o.ClaimID, triggerstart1, triggerend1, o.OccurrenceID, OccurrenceGroup, 
	OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
	O.Cost1 + O.Cost3, PolicyNum,
	Case when p.carrierid = 10 then 'Producer' else CarrierName end, 
	Layer, matter, e.PolicyDateID, cl.ClaimLawsuitID,
	p.PolicyDateID, settlementdate,ed.Description,p.CarrierGroupName
	
Order by matter
	
RETURN










GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetailsByCarrierGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetailsByCarrierGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetailsByCarrierGroup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetailsByCarrierGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetailsByCarrierGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetailsByCarrierGroup] TO [power_user]
GO
