SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptDefenseClaimList_ByCarrier_Sola] (@ExhaustionDateID INT)
AS
SET NOCOUNT ON

--Declare @ExhaustionDateID int
--Set @ExhaustionDateID = 28




--select * from tblexhaustiondate

--Pull allocation amounts for requested Carrier
DECLARE @CarrierAllocation TABLE (OccurrenceID INT, AllocatedAmount FLOAT,CarrierGroupID INT ,CarrierGroupName NVARCHAR (100),carrierID INT)
INSERT INTO @CarrierAllocation
SELECT OC.OccurrenceID, SUM(e.DefenseAllocation) + SUM(e.DefenseOutsideLimitsAllocation) AS AllocatedAmount, cg.CarrierGroupID ,CarrierGroupName, c.carrierID
FROM 
tblExhaustion E
JOIN tblOccurrence OC ON OC.OccurrenceID = E.OccurrenceID
INNER JOIN tblPolicyDates pd ON pd.PolicyDateID = e.PolicyDateID
JOIN tblPolicy P ON p.PolicyID = pd.PolicyID
JOIN tblCarrier C ON c.CarrierID = p.CarrierID
INNER JOIN tblcarriergroup cg ON cg.CarrierGroupID = c.CarrierGroupID
WHERE 
e.ExhaustionDateID = @ExhaustionDateID 
GROUP BY OC.OccurrenceID, cg.CarrierGroupID,CarrierGroupName,c.CarrierID

--Select * from @CarrierAllocation

--select Sum(AllocatedAmount) from @CarrierAllocation
--select * from tbloccurrence
--select * from tblexhaustion
--Select * from tbldefinvclaimantdetail
--select * from vtblFirm
--select * from tblDefenseInvoice

--select * from tblPolicyDates pd 
--inner join tblPolicy p on p.PolicyID = pd.PolicyID
--inner join tblcarrier c on c.CarrierID = p.CarrierID
--inner join tblCarrierGroup cg on cg.CarrierGroupID = c.CarrierGroupID
--Where pd.PolicyDateID in (1737,1757) 


SELECT di.defenseinvoiceid, 
COALESCE(vf.VendorFirm,dc.defensecounsel) AS Firm, 
dic.ClaimLawsuitID AS ClaimantLawsuitID, 
lastname+', '+firstname AS ClaimantName, 
'XXX-XX-'+RIGHT(COALESCE(socialsecuritynumber,''),4) AS socialsecuritynumber, 
CASE WHEN s.state = 'ZZ' THEN '' ELSE s.state END AS State, 
jurisdiction, 
LTRIM(RTRIM(dic.invoicenumber)) AS invoicenumber, 
SUM(CASE WHEN CarrierID = 14 THEN AllocatedAmount ELSE 0 END) AS Hartford_Sola, 
SUM(CASE WHEN CarrierID IN (88,459) THEN AllocatedAmount ELSE 0 END) AS Hartford_GSX, 
SUM(CASE WHEN CarrierGroupID = 30 THEN AllocatedAmount ELSE 0 END) AS AllState,
SUM(CASE WHEN CarrierGroupID = 31 THEN AllocatedAmount ELSE 0 END) AS ACE,
SUM(CASE WHEN CarrierGroupID = 86 THEN AllocatedAmount ELSE 0 END) AS USFG,
AdjustedTotal AS AdjustedTotal, 
o.TriggerStart1 AS DOFE,
cost1,
OccurrenceTypeID,
cdt.ClaimantDocumentType,
(dic.Fees + dic.costs) AS InvoiceTotal,
Matter
FROM tbldefenseinvoice di
INNER JOIN tbldefinvclaimantdetail dic ON dic.DefenseInvoiceID = di.DefenseInvoiceID
LEFT JOIN tblOccurrence o ON dic.InvoiceDetailID = o.OccurrenceNote
INNER JOIN @carrierallocation ca ON ca.occurrenceid = o.occurrenceid
LEFT JOIN tblclaimlawsuit cl ON cl.ClaimLawsuitID = dic.ClaimLawsuitID
LEFT JOIN tblclaim c ON c.claimid = cl.ClaimID
LEFT JOIN tblmatter m ON m.MatterID = c.MatterID
LEFT JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
LEFT JOIN tbljurisdiction j ON j.jurisdictionid = l.jurisdictionid
LEFT JOIN tblstate s ON s.stateid = l.stateid
LEFT JOIN tblVendorFirm vf ON vf.VendorFirmID=di.VendorFirmID
LEFT JOIN tblDefenseCounsel dc ON dc.DefenseCounselID=di.VendorFirmID
LEFT JOIN tblDocuments d ON d.DocumentID = di.DocumentID
LEFT JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
GROUP BY di.defenseinvoiceid, 
COALESCE(vf.VendorFirm,dc.defensecounsel), 
dic.ClaimLawsuitID , 
lastname+', '+firstname , 
'XXX-XX-'+RIGHT(COALESCE(socialsecuritynumber,''),4) , 
s.state, 
jurisdiction, 
LTRIM(RTRIM(dic.invoicenumber)) , 
adjustedTotal,
o.TriggerStart1,
cost1,
OccurrenceTypeID,
cdt.ClaimantDocumentType,
(dic.Fees + dic.costs),
Matter
 HAVING SUM(fees+costs-adjustments)<>0
--compute sum(case when cost1<0 then (fees+costs-adjustments)*1 else (fees+costs-adjustments) end), sum(Coalesce(convert(money,allocatedamount), 0))








GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Sola] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Sola] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Sola] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Sola] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Sola] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Sola] TO [power_user]
GO
