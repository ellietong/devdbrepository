SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailSettlementsPaid]
AS

	SET NOCOUNT ON

	BEGIN

	--Email notification to let GSX know when a settled case has been fully reimbursed by insurers

		--Declare and set email inputs.
		DECLARE @Subject nvarchar(100)
		DECLARE @LogoHeader nvarchar(2000)

		DECLARE @Today4PM datetime
		DECLARE @Yesterday4PM datetime
		SET @Yesterday4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')
		SET @Today4PM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')


		SET @Subject =
		'GSX: Settlements Paid In Full ' + CONVERT(varchar, GETDATE(), 107)
		--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
		--KCIC logo is temporarily being pulled from D. Snyder's Google site	


		SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


		--Create a temp table for Complaints which have been uploaded in the past day.
		--DocTitle and FirmAbb are retreived by parsing the documentURL.
		DECLARE @NewPaidSettlements TABLE (
			ClaimantPersonalInfoID bigint,
			claimlawsuitid bigint,
			ClaimantName nvarchar(50),
			settlementdate date,
			SettlementAmount money,
			TotalReimbursement money
		)

		INSERT INTO @NewPaidSettlements
			SELECT DISTINCT
			cpi.ClaimantPersonalInfoID,
			Cl.claimLawsuitID,
			Lastname + ', ' + COALESCE(firstname, '') +' ('+CONVERT(nvarchar,cpi.ClaimantPersonalInfoID)+')' as ClaimantName,
			settlementdate,
			SettlementAmount,
			round(SUM(arp.Amount),2) AS TotalReimbursement
		FROM tblclaimsettlement cs
		INNER JOIN tblclaimlawsuit cl ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		INNER JOIN tblclaim c ON c.claimid = cl.claimid
		INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblmatter m ON m.matterid = c.matterid
		INNER JOIN tbloccurrence o ON o.claimid = cl.ClaimLawsuitID
		INNER JOIN tblexhaustion e ON e.occurrenceid = o.occurrenceid
		INNER JOIN tblAccountsReceivablePayment arp ON arp.ExhaustionID = e.exhaustionid
		WHERE cs.PaymentApprovedBy is null and cs.PaymentApprovedDate is null and OccurrenceTypeID=1
		GROUP BY	cpi.ClaimantPersonalInfoID,
					Cl.claimLawsuitID,
					Lastname + ', ' + COALESCE(firstname, ''),
					settlementdate,
					SettlementAmount
		HAVING settlementamount <= round(SUM(arp.Amount),2)


			--Declare and populate the body of the email for individual firms
			DECLARE @BodyMessage nvarchar(max)
			--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
			--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
			SET @BodyMessage = @LogoHeader +
			'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	
 
 
--->
</STYLE>'



			--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

			IF (SELECT
					COUNT(ClaimantPersonalInfoID)
				FROM @NewPaidSettlements)
				> 0
			BEGIN
				SET @BodyMessage = @BodyMessage + '<H4>The following settlements have been paid in full in the last 24 hours: </H4>'
				SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +


				N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant (ClaimantID)</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Settlement Date </th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Settlement Amount</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Total Reimbursement</th>' +


				CAST((SELECT
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + ClaimantName + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + CAST(Settlementdate AS varchar(max))  + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + '$'+ Convert(varchar,CAST(C.SettlementAmount AS money),1)  + ' </td>',
					'',
					'<td style = "font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + '$'+ Convert(varchar,CAST(TotalReimbursement AS money),1) + ' </td>',
					''
				FROM @NewPaidSettlements C
								

				FOR xml PATH ('tr'), TYPE)
				AS nvarchar(max)) +

				N'</table>' + N'<br/>'

			END
			ELSE
			BEGIN
				SET @BodyMessage = @BodyMessage + '<H4>No settlements have been paid in the last 24 hours. </H4>'
			END
			

			--Undo the HTML stripping from the xml path function above so embedded tags work
			SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

			--Link for document library
			--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT
			--	@firmURL)
			--+ '>here</A> to access the document(s).</b><br><br>'

				
			--KCIC address and disclaimer
			SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


			DECLARE @RecipientList nvarchar(500)
			SET @RecipientList = ''
			DECLARE @CCRecipientList nvarchar(500)
			SET @CCRecipientList = ''
			DECLARE @Contact nvarchar(500)
			DECLARE contact_curs CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY FOR
			
			SELECT
			DISTINCT
				email
			FROM vEmailRecipients e
			WHERE e.RoleID = 103


			OPEN contact_curs
			FETCH NEXT FROM contact_curs INTO @contact
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @RecipientList = @RecipientList + @Contact + '; '
				FETCH NEXT FROM contact_curs INTO @contact
			END
			CLOSE contact_curs
			DEALLOCATE contact_curs


			IF (SELECT
					COUNT(ClaimantPersonalInfoID)
				FROM @NewPaidSettlements)
				> 0
			BEGIN
			--QC Recipient List
			--Set @Bodymessage = @BodyMessage + '****Recipients:' + @RecipientList
			PRINT @RecipientList
			--Execute stored procedure to send the email to that firm
			EXEC msdb.dbo.sp_send_dbmail --The Profile_Name sends from [client]@kcic.com email address
			@profile_name = 'Claims',
			--@recipients = 'hagep@kcic.com;shockleym@kcic.com',--;cornellt@kcic.com;youngc@kcic.com',
			@recipients = @RecipientList,
			@blind_copy_recipients = 'shockleym@kcic.com;gsx@kcic.com',
			@subject = @subject,
			@body = @BodyMessage,
			@body_format = 'html'
			END
					

	END



GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailSettlementsPaid] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailSettlementsPaid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailSettlementsPaid] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryExternalEmailSettlementsPaid] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailSettlementsPaid] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailSettlementsPaid] TO [power_user]
GO
