SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryInternalEmailInsurerPayments] 
AS
SET NOCOUNT ON
 
BEGIN

 
--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)
 
Declare @Today10AM datetime
Declare @Yesterday10AM datetime
SET @Yesterday10AM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 10:00:00.000')
SET @Today10AM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 10:00:00.000')
DECLARE @SendEmail bit = 0

SET @Subject = 'GSX Additional Insurer Payments ' + CONVERT(varchar, GETDATE(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(max)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
	<!--
	TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
	TD	
	{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
		
	}
	TH	
	{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
	UL {margin:0; 
		
	padding:0;}	
 
 
	--->
	</STYLE>'



SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
--CSS
DECLARE @MainHeaderStyle nvarchar(max) = 'background-color: #0079C1; font-weight: bold; color: white; padding: 4px;'
DECLARE @tdStyle nvarchar(max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'

if object_id('tempdb..#PaidToDate') is not null drop table #PaidToDate
CREATE TABLE #PaidToDate (claimsettlementID int, PaidAmount money, IsFullyPaid int)
INSERT INTO #PaidToDate
	SELECT claimsettlementid,
		SUM(paymentamount),
		SUM(CONVERT(int, isfullypaid))
	FROM tblpaymentsettlement
	GROUP BY ClaimSettlementID

if object_id('tempdb..#OccCounts') is not null drop table #OccCounts
CREATE TABLE #OccCounts (OccCount INT)
INSERT INTO #OccCounts
	SELECT COUNT(occurrencename) AS OccCount
	FROM tblclaimsettlement cs
		INNER JOIN (select * from tbloccurrence where OccurrenceTypeID=1 ) o on o.claimid = cs.claimlawsuitid
		INNER JOIN tblexhaustion e on e.occurrenceid = o.occurrenceid
		INNER JOIN tblaccountsreceivablepayment arp on arp.exhaustionid = e.exhaustionid
		LEFT  JOIN #PaidToDate p on p.claimsettlementID = cs.ClaimSettlementID
	WHERE IsFullyPaid = 0
	GROUP BY occurrencename, settlementamount, p.PaidAmount, IsFullyPaid, PaymentApprovedBy, PaymentApprovedDate
	HAVING ROUND(SUM(arp.amount),0) > ROUND(COALESCE(p.PaidAmount,0),0)

IF (SELECT SUM(OccCount) FROM #OccCounts) > 0 
BEGIN
SET @SendEmail = 1


--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
SET @BodyMessage = @BodyMessage + '<H4>The following partially paid settlement(s) have received additional insurer reimbursement(s).<br/><br/>'-- +


--PRINT TABLE HEADERS
SET @BodyMessage = @BodyMessage + '
	<table>
		<thead>
			<tr>
				<th style="' + @MainHeaderStyle + '">Occurrence</th>
				<th style="' + @MainHeaderStyle + '">Settlement Amount</th>
				<th style="' + @MainHeaderStyle + '">Insurer Reimbursement</th>
				<th style="' + @MainHeaderStyle + '">Amount Paid By KCIC</th>
				<th style="' + @MainHeaderStyle + '">Payment Status</th>
				<th style="' + @MainHeaderStyle + '">Payment Approved Date</th>
				<th style="' + @MainHeaderStyle + '">Payment Approved By</th>
				
			</tr>
		</thead>
	'
--PRINT TABLE OF CLAIMS
SET @BodyMessage = @BodyMessage +

COALESCE((CAST((SELECT
	
	[td/@style] = @tdStyle,
	td = COALESCE(UPPER(occurrencename), 'Unknown'),
	'',
	[td/@style] = @tdStyle,
	td = '$' + CONVERT(nvarchar, CONVERT(money, cs.SettlementAmount)),
	'',
	[td/@style] = @tdStyle,
	td = '$' + CONVERT(nvarchar, CONVERT(money, sum(arp.amount))),
	'',
	[td/@style] = @tdStyle,
	td = '$' + CONVERT(nvarchar, CONVERT(money, coalesce(p.PaidAmount,0))),
	'',
	[td/@style] = @tdStyle,
	td = case when IsFullyPaid = 1 then 'Fully Paid' 
			  when isfullypaid = 0 then 'Partially Paid'
			  when IsFullyPaid is null then 'No Payments Made' End,
	'',
	[td/@style] = @tdStyle,
	td = CONVERT(nvarchar, PaymentApprovedDate, 101),
	'',
	[td/@style] = @tdStyle,
	td = CONVERT(nvarchar, cs.PaymentApprovedBy),
	''
FROM tblclaimsettlement cs
INNER JOIN (select * from tbloccurrence where OccurrenceTypeID=1) o ON o.claimid = cs.claimlawsuitid
INNER JOIN tblexhaustion e ON e.occurrenceid = o.occurrenceid
INNER JOIN tblaccountsreceivablepayment arp ON arp.exhaustionid = e.exhaustionid
LEFT JOIN #PaidToDate p ON p.claimsettlementID = cs.ClaimSettlementID
WHERE IsFullyPaid = 0
GROUP BY	occurrencename,
			settlementamount,
			p.PaidAmount,
			IsFullyPaid,
			PaymentApprovedBy,
			PaymentApprovedDate
HAVING ROUND(SUM(arp.amount), 0) > ROUND(COALESCE(p.PaidAmount, 0), 0)


FOR xml PATH ('tr'), TYPE)
AS nvarchar(max))), '')

--CLOSE TABLE
SET @BodyMessage = @BodyMessage + '</table><br>'
END



--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
	733 10th Street, NW, Suite 3001<br>
	Washington, DC 20001<br>
	(t) 202-772-2300<br>
	(f) 202-772-2333'


DECLARE @RecipientList nvarchar(500)


----QC Recipient List
--Set @Bodymessage = @BodyMessage + 'Recipients' + @RecipientList
--PRINT @RecipientList

--PRINT @SendEmail

IF (@SendEmail = 1) BEGIN
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The Profile_Name sends from [client]@kcicllc.com email address
@profile_name = 'Claims',
@recipients = 'shockleym@kcic.com;gsx@kcic.com',
--@recipients = 'hagep@kcic.com;shockleym@kcic.com',
--@copy_recipients = '',
--@blind_copy_recipients = 'gsx@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailInsurerPayments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailInsurerPayments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailInsurerPayments] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryInternalEmailInsurerPayments] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailInsurerPayments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailInsurerPayments] TO [power_user]
GO
