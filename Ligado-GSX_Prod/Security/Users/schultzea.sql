IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'KCICLLC\schultzea')
CREATE LOGIN [KCICLLC\schultzea] FROM WINDOWS
GO
CREATE USER [schultzea] FOR LOGIN [KCICLLC\schultzea]
GO
GRANT CREATE PROCEDURE TO [schultzea]
GRANT CREATE TABLE TO [schultzea]
GRANT CREATE VIEW TO [schultzea]
