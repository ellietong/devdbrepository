IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'KCICLLC\petryszaka')
CREATE LOGIN [KCICLLC\petryszaka] FROM WINDOWS
GO
CREATE USER [petryszaka] FOR LOGIN [KCICLLC\petryszaka]
GO
GRANT CREATE FUNCTION TO [petryszaka]
GRANT CREATE PROCEDURE TO [petryszaka]
GRANT CREATE TABLE TO [petryszaka]
GRANT CREATE VIEW TO [petryszaka]
