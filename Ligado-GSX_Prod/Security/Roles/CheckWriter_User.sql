CREATE ROLE [CheckWriter_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'CheckWriter_User', N'carthyl'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'casillaw'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'eastlacka'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'hughesr'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'husainm'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'jacobsb'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'kennedyl'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'madlp'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'mollardj'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'pikeg'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'schultzea'
GO
EXEC sp_addrolemember N'CheckWriter_User', N'zongc'
GO
