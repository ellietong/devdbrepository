CREATE ROLE [base_user]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'base_user', N'carthyl'
GO
EXEC sp_addrolemember N'base_user', N'casillaw'
GO
EXEC sp_addrolemember N'base_user', N'chaudoinr'
GO
EXEC sp_addrolemember N'base_user', N'cornellt'
GO
EXEC sp_addrolemember N'base_user', N'crookc'
GO
EXEC sp_addrolemember N'base_user', N'eastlacka'
GO
EXEC sp_addrolemember N'base_user', N'hankee'
GO
EXEC sp_addrolemember N'base_user', N'heczkom'
GO
EXEC sp_addrolemember N'base_user', N'hubbardb'
GO
EXEC sp_addrolemember N'base_user', N'hughesr'
GO
EXEC sp_addrolemember N'base_user', N'husainm'
GO
EXEC sp_addrolemember N'base_user', N'jacksonc'
GO
EXEC sp_addrolemember N'base_user', N'jacobsb'
GO
EXEC sp_addrolemember N'base_user', N'kanga'
GO
EXEC sp_addrolemember N'base_user', N'kellyb'
GO
EXEC sp_addrolemember N'base_user', N'kennedyl'
GO
EXEC sp_addrolemember N'base_user', N'khanm'
GO
EXEC sp_addrolemember N'base_user', N'LigadoMonitoring'
GO
EXEC sp_addrolemember N'base_user', N'madlp'
GO
EXEC sp_addrolemember N'base_user', N'marshalla'
GO
EXEC sp_addrolemember N'base_user', N'mollardj'
GO
EXEC sp_addrolemember N'base_user', N'monahanc'
GO
EXEC sp_addrolemember N'base_user', N'nguyenn'
GO
EXEC sp_addrolemember N'base_user', N'onyiukeo'
GO
EXEC sp_addrolemember N'base_user', N'owenss'
GO
EXEC sp_addrolemember N'base_user', N'petryszaka'
GO
EXEC sp_addrolemember N'base_user', N'pikeg'
GO
EXEC sp_addrolemember N'base_user', N'potterm'
GO
EXEC sp_addrolemember N'base_user', N'schultzea'
GO
EXEC sp_addrolemember N'base_user', N'scottc'
GO
EXEC sp_addrolemember N'base_user', N'shockleym'
GO
EXEC sp_addrolemember N'base_user', N'tonge'
GO
EXEC sp_addrolemember N'base_user', N'wongc'
GO
EXEC sp_addrolemember N'base_user', N'youngc'
GO
EXEC sp_addrolemember N'base_user', N'zongc'
GO
GRANT CREATE PROCEDURE TO [base_user]
GRANT CREATE VIEW TO [base_user]
