CREATE ROLE [Claims_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Claims_User', N'carthyl'
GO
EXEC sp_addrolemember N'Claims_User', N'casillaw'
GO
EXEC sp_addrolemember N'Claims_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'Claims_User', N'cornellt'
GO
EXEC sp_addrolemember N'Claims_User', N'eastlacka'
GO
EXEC sp_addrolemember N'Claims_User', N'heczkom'
GO
EXEC sp_addrolemember N'Claims_User', N'hughesr'
GO
EXEC sp_addrolemember N'Claims_User', N'husainm'
GO
EXEC sp_addrolemember N'Claims_User', N'jacobsb'
GO
EXEC sp_addrolemember N'Claims_User', N'kanga'
GO
EXEC sp_addrolemember N'Claims_User', N'kennedyl'
GO
EXEC sp_addrolemember N'Claims_User', N'khanm'
GO
EXEC sp_addrolemember N'Claims_User', N'madlp'
GO
EXEC sp_addrolemember N'Claims_User', N'mollardj'
GO
EXEC sp_addrolemember N'Claims_User', N'monahanc'
GO
EXEC sp_addrolemember N'Claims_User', N'nguyenn'
GO
EXEC sp_addrolemember N'Claims_User', N'owenss'
GO
EXEC sp_addrolemember N'Claims_User', N'pikeg'
GO
EXEC sp_addrolemember N'Claims_User', N'potterm'
GO
EXEC sp_addrolemember N'Claims_User', N'schultzea'
GO
EXEC sp_addrolemember N'Claims_User', N'shockleym'
GO
EXEC sp_addrolemember N'Claims_User', N'zongc'
GO
