CREATE ROLE [Exhaustion_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Exhaustion_User', N'carthyl'
GO
EXEC sp_addrolemember N'Exhaustion_User', N'cornellt'
GO
EXEC sp_addrolemember N'Exhaustion_User', N'madlp'
GO
