SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fn_GetClientID]()
RETURNS int
AS
BEGIN
    RETURN 15
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetClientID] TO [base_user]
GRANT EXECUTE ON  [dbo].[fn_GetClientID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fn_GetClientID] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[fn_GetClientID] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[fn_GetClientID] TO [power_user]
GRANT EXECUTE ON  [dbo].[fn_GetClientID] TO [power_user]
GO
