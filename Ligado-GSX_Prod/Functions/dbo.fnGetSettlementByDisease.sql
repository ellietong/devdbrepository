SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fnGetSettlementByDisease] 
(	
	@s1 Date, @s2 Date

)
RETURNS TABLE 
AS
RETURN 
(
	select HighLevelDisease, count(ClaimLawsuitID) as [Count],Sum(IndemnityPaid) as [Indemnity Paid],Sum(AverageIndemnity) as [Avg. Indemnity paid per Claimant]
	from vReport_Settlements 
	where SettlementDate >= @s1 and SettlementDate <= @s2
	group by HighLevelDisease
)
GO
