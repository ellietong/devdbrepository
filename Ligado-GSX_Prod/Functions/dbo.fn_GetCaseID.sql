SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[fn_GetCaseID]()
RETURNS int
AS
BEGIN
    RETURN 45
END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetCaseID] TO [base_user]
GRANT EXECUTE ON  [dbo].[fn_GetCaseID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fn_GetCaseID] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[fn_GetCaseID] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[fn_GetCaseID] TO [power_user]
GRANT EXECUTE ON  [dbo].[fn_GetCaseID] TO [power_user]
GO
