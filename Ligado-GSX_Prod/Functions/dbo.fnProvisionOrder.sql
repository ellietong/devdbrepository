SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE FUNCTION [dbo].[fnProvisionOrder]
--(@keyName varchar(50), @keyValue varchar(50), 
--@fieldNameToGet varchar(50), @insuranceProgramID int)
(@ProvisionLabelID int)
RETURNS int
AS
BEGIN
-- declare temporary table
DECLARE @PLID table (RowIndex int IDENTITY, 
	ProvisionLabelID int)
-- declare temporary integer variable

INSERT INTO @PLID (ProvisionLabelID)
	SELECT ProvisionLabelID
	FROM tblProvisionLabel
	ORDER BY Provision ASC

RETURN (SELECT RowIndex FROM @PLID WHERE ProvisionLabelID = @provisionlabelID)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnProvisionOrder] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnProvisionOrder] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnProvisionOrder] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnProvisionOrder] TO [power_user]
GO
