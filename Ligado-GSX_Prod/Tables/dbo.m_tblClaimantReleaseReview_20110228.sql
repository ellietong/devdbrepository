CREATE TABLE [dbo].[m_tblClaimantReleaseReview_20110228]
(
[ClaimantReleaseID] [bigint] NOT NULL,
[ClaimantSettlementID] [bigint] NOT NULL,
[ReleaseSignedDate] [datetime] NULL,
[ReleaseSentDate] [datetime] NULL,
[ReleaseReceivedDate] [datetime] NULL,
[ReleaseTypeID] [int] NULL,
[NavClaimResolutionKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavReleaseType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HardcopyRelease] [bit] NULL,
[InformationUpdated] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblClaimantReleaseReview_20110228] ADD CONSTRAINT [PK_m_tblClaimantReleaseReview_20110228] PRIMARY KEY CLUSTERED  ([ClaimantReleaseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblClaimantReleaseReview_20110228] TO [power_user]
GO
