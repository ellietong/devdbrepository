CREATE TABLE [dbo].[AuditLogDetail]
(
[AuditLogID] [int] NOT NULL,
[RowKey] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnID] [int] NOT NULL,
[Status] [tinyint] NOT NULL CONSTRAINT [DF_AuditLogDetail_Status] DEFAULT ((0)),
[OldValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trg_kr_AuditLogDetail_Delete]
  ON [dbo].[AuditLogDetail] FOR DELETE
AS
BEGIN
  DELETE FROM AuditLogText FROM Deleted D
    WHERE D.Status = 1 AND
          D.AuditLogID = AuditLogText.AuditLogID AND
          D.RowKey = AuditLogText.RowKey AND
          D.ColumnID = AuditLogText.ColumnID

  DELETE FROM AuditLogImage FROM Deleted D
    WHERE D.Status = 2 AND
          D.AuditLogID = AuditLogImage.AuditLogID AND
          D.RowKey = AuditLogImage.RowKey AND
          D.ColumnID = AuditLogImage.ColumnID

  DELETE FROM AuditLogNText FROM Deleted D
    WHERE D.Status = 4 AND
          D.AuditLogID = AuditLogNText.AuditLogID AND
          D.RowKey = AuditLogNText.RowKey AND
          D.ColumnID = AuditLogNText.ColumnID
END
GO
ALTER TABLE [dbo].[AuditLogDetail] ADD CONSTRAINT [PK_AuditLogDetail] PRIMARY KEY NONCLUSTERED  ([AuditLogID], [RowKey], [ColumnID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditLogDetail] TO [base_user]
GRANT SELECT ON  [dbo].[AuditLogDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditLogDetail] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditLogDetail] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditLogDetail] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditLogDetail] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditLogDetail] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditLogDetail] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLogDetail] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditLogDetail] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditLogDetail] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditLogDetail] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLogDetail] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditLogDetail] TO [power_user]
GRANT SELECT ON  [dbo].[AuditLogDetail] TO [power_user]
GRANT INSERT ON  [dbo].[AuditLogDetail] TO [power_user]
GRANT DELETE ON  [dbo].[AuditLogDetail] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditLogDetail] TO [power_user]
GO
