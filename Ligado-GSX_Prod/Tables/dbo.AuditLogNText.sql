CREATE TABLE [dbo].[AuditLogNText]
(
[AuditLogID] [int] NOT NULL,
[RowKey] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnID] [int] NOT NULL,
[Value] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditLogNText] ADD CONSTRAINT [PK_AuditLogNText] PRIMARY KEY NONCLUSTERED  ([AuditLogID], [RowKey], [ColumnID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditLogNText] TO [base_user]
GRANT SELECT ON  [dbo].[AuditLogNText] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditLogNText] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditLogNText] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditLogNText] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditLogNText] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditLogNText] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditLogNText] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLogNText] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditLogNText] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditLogNText] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditLogNText] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLogNText] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditLogNText] TO [power_user]
GRANT SELECT ON  [dbo].[AuditLogNText] TO [power_user]
GRANT INSERT ON  [dbo].[AuditLogNText] TO [power_user]
GRANT DELETE ON  [dbo].[AuditLogNText] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditLogNText] TO [power_user]
GO
