CREATE TABLE [dbo].[tblClaimLawsuit]
(
[ClaimLawsuitID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID] [bigint] NULL,
[LawsuitID] [bigint] NULL,
[IndividualDocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavIsPrimaryForClaim] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsMDL] [bit] NULL CONSTRAINT [DF_tblClaimLawsuit_IsMDL] DEFAULT ((0)),
[NavClaimLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavStatus] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavStatusDate] [datetime] NULL,
[IsPrimaryForClaim] [bit] NULL,
[AdminSettlement] [bit] NULL,
[ActiveTrialDate] [datetime] NULL,
[TrialGroupID] [bigint] NULL,
[ComplaintID] [bigint] NULL,
[SPUsername] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [bigint] NULL,
[ClaimLawsuitTypeID] [int] NULL,
[BankruptcyStayID] [int] NULL,
[ClaimLawsuitNotes] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DemandAmount] [money] NULL,
[DemandDate] [datetime] NULL,
[PresentedYear] [int] NULL,
[TrialRiskID] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE TRIGGER [dbo].[del_CLDocketNumbers]
   ON  [dbo].[tblClaimLawsuit]
   AFTER DELETE
AS 
BEGIN
      SET NOCOUNT ON;
      
       
           
DECLARE @CPI bigint
DECLARE @DocketNumStr nvarchar(4000)
DECLARE @addDocket nvarchar(4000)
BEGIN

      
      --Declare Cursor
      DECLARE Cur_CPI CURSOR Local Fast_Forward for
      SELECT DISTINCT ClaimantPersonalInfoID FROM deleted INNER JOIN tblClaim c ON c.ClaimID = deleted.ClaimID
      --Open CPI Cursor
      OPEN Cur_CPI

      --Fetch Data
      FETCH NEXT FROM Cur_CPI INTO @CPI

      WHILE @@Fetch_Status = 0
      BEGIN
                        SET @DocketNumStr = ''
                        DECLARE Cur_Docket CURSOR Local Fast_Forward for
                        SELECT L.DocketNumber FROM tblLawsuit l 
                        INNER JOIN tblClaimLawsuit cl ON cl.LawsuitID = l.LawsuitID
                        INNER JOIN tblClaim c ON C.ClaimID = cl.ClaimID WHERE ClaimantPersonalInfoID = @CPI ORDER BY L.DocketNumber ASC

                        --Open Docket Cursor
                        OPEN Cur_Docket

                        --Fetch Data
                        FETCH NEXT FROM Cur_Docket INTO @addDocket

                        WHILE @@Fetch_Status = 0
                        BEGIN
                              --assemble the string
                              if LEN(@DocketNumStr) = 0 SET @DocketNumStr = @addDocket
                              else SET @DocketNumStr =  @DocketNumStr  + ', ' +  @addDocket
                              
                        
            
                              FETCH NEXT FROM Cur_Docket INTO @addDocket
                              
                        END
                        
                        CLOSE Cur_Docket 
                        DEALLOCATE Cur_Docket
                        --insert the string into a temp table with CPI
                        UPDATE tblClaimantDocketNumbers SET DocketNumbers = @DocketNumStr WHERE ClaimantPersonalInfoID = @CPI 
                        
      FETCH NEXT FROM Cur_CPI INTO @CPI
      
      END

CLOSE Cur_CPI 
DEALLOCATE Cur_CPI 
            
END

    -- Insert statements for trigger here

END
GO
DISABLE TRIGGER [dbo].[del_CLDocketNumbers] ON [dbo].[tblClaimLawsuit]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE TRIGGER [dbo].[ins_CLDocketNumbers]
   ON  [dbo].[tblClaimLawsuit]
   AFTER INSERT
AS 
BEGIN
      SET NOCOUNT ON;
      
       
           
DECLARE @CPI bigint
DECLARE @DocketNumStr nvarchar(4000)
DECLARE @addDocket nvarchar(4000)
BEGIN

      
      --Declare Cursor
      DECLARE Cur_CPI CURSOR Local Fast_Forward for
      SELECT DISTINCT ClaimantPersonalInfoID FROM inserted INNER JOIN tblClaim c ON c.ClaimID = inserted.ClaimID
      --Open CPI Cursor
      OPEN Cur_CPI

      --Fetch Data
      FETCH NEXT FROM Cur_CPI INTO @CPI

      WHILE @@Fetch_Status = 0
      BEGIN
                        SET @DocketNumStr = ''
                        DECLARE Cur_Docket CURSOR Local Fast_Forward for
                        SELECT L.DocketNumber FROM tblLawsuit l 
                        INNER JOIN tblClaimLawsuit cl ON cl.LawsuitID = l.LawsuitID
                        INNER JOIN tblClaim c ON C.ClaimID = cl.ClaimID WHERE ClaimantPersonalInfoID = @CPI ORDER BY L.DocketNumber ASC

                        --Open Docket Cursor
                        OPEN Cur_Docket

                        --Fetch Data
                        FETCH NEXT FROM Cur_Docket INTO @addDocket

                        WHILE @@Fetch_Status = 0
                        BEGIN
                              --assemble the string
                              if LEN(@DocketNumStr) = 0 SET @DocketNumStr = @addDocket
                              else SET @DocketNumStr =  @DocketNumStr  + ', ' +  @addDocket
                              
                        
            
                              FETCH NEXT FROM Cur_Docket INTO @addDocket
                              
                        END
                        
                        CLOSE Cur_Docket 
                        DEALLOCATE Cur_Docket
                        --insert the string into a temp table with CPI
                        UPDATE tblClaimantDocketNumbers SET DocketNumbers = @DocketNumStr WHERE ClaimantPersonalInfoID = @CPI 
                        
      FETCH NEXT FROM Cur_CPI INTO @CPI
      
      END

CLOSE Cur_CPI 
DEALLOCATE Cur_CPI 
            
END

END
GO
DISABLE TRIGGER [dbo].[ins_CLDocketNumbers] ON [dbo].[tblClaimLawsuit]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblClaimLawsuit_Del] ON [dbo].[tblClaimLawsuit] FOR DELETE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:52 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int
  SELECT @RowsAffected = COUNT(*) FROM DELETED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    -- Populate audit detail records...
    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      149,
      0,
      CONVERT( NVARCHAR( 34 ), [ActiveTrialDate], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      150,
      0,
      CONVERT(NVARCHAR, [AdminSettlement])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      399,
      0,
      CONVERT(NVARCHAR, [BankruptcyStayID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      151,
      0,
      CONVERT(NVARCHAR, [ClaimID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      152,
      0,
      CONVERT(NVARCHAR, [ClaimLawsuitID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      400,
      0,
      CONVERT( NVARCHAR(max), [ClaimLawsuitNotes] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      401,
      0,
      CONVERT(NVARCHAR, [ClaimLawsuitTypeID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      153,
      0,
      CONVERT(NVARCHAR, [ComplaintID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      154,
      0,
      CONVERT(NVARCHAR, [DefenseCounselAddressID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      402,
      0,
      CONVERT( NVARCHAR( 30 ), [DemandAmount], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      403,
      0,
      CONVERT( NVARCHAR( 34 ), [DemandDate], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      155,
      0,
      CONVERT( NVARCHAR(max), [IndividualDocketNumber] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      156,
      0,
      CONVERT(NVARCHAR, [IsMDL])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      157,
      0,
      CONVERT(NVARCHAR, [IsPrimaryForClaim])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      158,
      0,
      CONVERT(NVARCHAR, [LawsuitID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      159,
      0,
      CONVERT( NVARCHAR(max), [NavClaimKey] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      160,
      0,
      CONVERT( NVARCHAR(max), [NavClaimLawsuitKey] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      161,
      0,
      CONVERT( NVARCHAR(max), [NavIsPrimaryForClaim] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      162,
      0,
      CONVERT( NVARCHAR(max), [NavLawsuitKey] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      163,
      0,
      CONVERT( NVARCHAR(max), [NavStatus] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      164,
      0,
      CONVERT( NVARCHAR( 34 ), [NavStatusDate], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      404,
      0,
      CONVERT(NVARCHAR, [PresentedYear])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      165,
      8,
      CONVERT( NVARCHAR(max), [SPUsername] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      166,
      0,
      CONVERT(NVARCHAR, [TrialGroupID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitID=' + CONVERT(VARCHAR, [ClaimLawsuitID]),
      405,
      0,
      CONVERT(NVARCHAR, [TrialRiskID])
    FROM
      DELETED

    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 11, @RowsAffected, 'D', @AuditedUser
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblClaimLawsuit_Del]', 'last', 'delete', null
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblClaimLawsuit_Upd] ON [dbo].[tblClaimLawsuit] FOR UPDATE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:52 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int, @ChangeCount INT, @CaptureCount INT
  SELECT @RowsAffected = COUNT(*) FROM INSERTED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    SELECT @CaptureCount = 0

    -- Test for a key update...
    SELECT @ChangeCount = COUNT(*)
    FROM
      INSERTED i
      FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )

    -- Populate audit detail records...
    IF( UPDATE( [ActiveTrialDate] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        149,
        0,
        CONVERT( NVARCHAR( 34 ), d.[ActiveTrialDate], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[ActiveTrialDate], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[ActiveTrialDate] <> i.[ActiveTrialDate] ) OR
        ( d.[ActiveTrialDate] IS NULL AND i.[ActiveTrialDate] IS NOT NULL ) OR
        ( i.[ActiveTrialDate] IS NULL AND d.[ActiveTrialDate] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [AdminSettlement] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        150,
        0,
        CONVERT(NVARCHAR, d.[AdminSettlement]),
        CONVERT(NVARCHAR, i.[AdminSettlement])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[AdminSettlement] <> i.[AdminSettlement] ) OR
        ( d.[AdminSettlement] IS NULL AND i.[AdminSettlement] IS NOT NULL ) OR
        ( i.[AdminSettlement] IS NULL AND d.[AdminSettlement] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [BankruptcyStayID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        399,
        0,
        CONVERT(NVARCHAR, d.[BankruptcyStayID]),
        CONVERT(NVARCHAR, i.[BankruptcyStayID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[BankruptcyStayID] <> i.[BankruptcyStayID] ) OR
        ( d.[BankruptcyStayID] IS NULL AND i.[BankruptcyStayID] IS NOT NULL ) OR
        ( i.[BankruptcyStayID] IS NULL AND d.[BankruptcyStayID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [ClaimID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        151,
        0,
        CONVERT(NVARCHAR, d.[ClaimID]),
        CONVERT(NVARCHAR, i.[ClaimID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[ClaimID] <> i.[ClaimID] ) OR
        ( d.[ClaimID] IS NULL AND i.[ClaimID] IS NOT NULL ) OR
        ( i.[ClaimID] IS NULL AND d.[ClaimID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [ClaimLawsuitID] ) )
    BEGIN
      -- Audit key column updates...
      IF( @ChangeCount > @RowsAffected )
      BEGIN
        INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
        SELECT
          @AuditID,
          'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
          152,
          0,
          CONVERT(NVARCHAR, d.[ClaimLawsuitID]),
          CONVERT(NVARCHAR, i.[ClaimLawsuitID])
        FROM
          INSERTED i
          FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
        WHERE
          ( d.[ClaimLawsuitID] <> i.[ClaimLawsuitID] ) OR
          ( d.[ClaimLawsuitID] IS NULL AND i.[ClaimLawsuitID] IS NOT NULL ) OR
          ( i.[ClaimLawsuitID] IS NULL AND d.[ClaimLawsuitID] IS NOT NULL )
        SELECT @CaptureCount = @CaptureCount + @@RowCount
      END
    END

    IF( UPDATE( [ClaimLawsuitNotes] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        400,
        0,
        CONVERT( NVARCHAR(max), d.[ClaimLawsuitNotes] ),
        CONVERT( NVARCHAR(max), i.[ClaimLawsuitNotes] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[ClaimLawsuitNotes] <> i.[ClaimLawsuitNotes] ) OR
        ( d.[ClaimLawsuitNotes] IS NULL AND i.[ClaimLawsuitNotes] IS NOT NULL ) OR
        ( i.[ClaimLawsuitNotes] IS NULL AND d.[ClaimLawsuitNotes] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [ClaimLawsuitTypeID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        401,
        0,
        CONVERT(NVARCHAR, d.[ClaimLawsuitTypeID]),
        CONVERT(NVARCHAR, i.[ClaimLawsuitTypeID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[ClaimLawsuitTypeID] <> i.[ClaimLawsuitTypeID] ) OR
        ( d.[ClaimLawsuitTypeID] IS NULL AND i.[ClaimLawsuitTypeID] IS NOT NULL ) OR
        ( i.[ClaimLawsuitTypeID] IS NULL AND d.[ClaimLawsuitTypeID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [ComplaintID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        153,
        0,
        CONVERT(NVARCHAR, d.[ComplaintID]),
        CONVERT(NVARCHAR, i.[ComplaintID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[ComplaintID] <> i.[ComplaintID] ) OR
        ( d.[ComplaintID] IS NULL AND i.[ComplaintID] IS NOT NULL ) OR
        ( i.[ComplaintID] IS NULL AND d.[ComplaintID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [DefenseCounselAddressID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        154,
        0,
        CONVERT(NVARCHAR, d.[DefenseCounselAddressID]),
        CONVERT(NVARCHAR, i.[DefenseCounselAddressID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[DefenseCounselAddressID] <> i.[DefenseCounselAddressID] ) OR
        ( d.[DefenseCounselAddressID] IS NULL AND i.[DefenseCounselAddressID] IS NOT NULL ) OR
        ( i.[DefenseCounselAddressID] IS NULL AND d.[DefenseCounselAddressID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [DemandAmount] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        402,
        0,
        CONVERT( NVARCHAR( 30 ), d.[DemandAmount], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[DemandAmount], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[DemandAmount] <> i.[DemandAmount] ) OR
        ( d.[DemandAmount] IS NULL AND i.[DemandAmount] IS NOT NULL ) OR
        ( i.[DemandAmount] IS NULL AND d.[DemandAmount] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [DemandDate] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        403,
        0,
        CONVERT( NVARCHAR( 34 ), d.[DemandDate], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[DemandDate], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[DemandDate] <> i.[DemandDate] ) OR
        ( d.[DemandDate] IS NULL AND i.[DemandDate] IS NOT NULL ) OR
        ( i.[DemandDate] IS NULL AND d.[DemandDate] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [IndividualDocketNumber] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        155,
        0,
        CONVERT( NVARCHAR(max), d.[IndividualDocketNumber] ),
        CONVERT( NVARCHAR(max), i.[IndividualDocketNumber] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[IndividualDocketNumber] <> i.[IndividualDocketNumber] ) OR
        ( d.[IndividualDocketNumber] IS NULL AND i.[IndividualDocketNumber] IS NOT NULL ) OR
        ( i.[IndividualDocketNumber] IS NULL AND d.[IndividualDocketNumber] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [IsMDL] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        156,
        0,
        CONVERT(NVARCHAR, d.[IsMDL]),
        CONVERT(NVARCHAR, i.[IsMDL])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[IsMDL] <> i.[IsMDL] ) OR
        ( d.[IsMDL] IS NULL AND i.[IsMDL] IS NOT NULL ) OR
        ( i.[IsMDL] IS NULL AND d.[IsMDL] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [IsPrimaryForClaim] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        157,
        0,
        CONVERT(NVARCHAR, d.[IsPrimaryForClaim]),
        CONVERT(NVARCHAR, i.[IsPrimaryForClaim])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[IsPrimaryForClaim] <> i.[IsPrimaryForClaim] ) OR
        ( d.[IsPrimaryForClaim] IS NULL AND i.[IsPrimaryForClaim] IS NOT NULL ) OR
        ( i.[IsPrimaryForClaim] IS NULL AND d.[IsPrimaryForClaim] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [LawsuitID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        158,
        0,
        CONVERT(NVARCHAR, d.[LawsuitID]),
        CONVERT(NVARCHAR, i.[LawsuitID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[LawsuitID] <> i.[LawsuitID] ) OR
        ( d.[LawsuitID] IS NULL AND i.[LawsuitID] IS NOT NULL ) OR
        ( i.[LawsuitID] IS NULL AND d.[LawsuitID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [NavClaimKey] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        159,
        0,
        CONVERT( NVARCHAR(max), d.[NavClaimKey] ),
        CONVERT( NVARCHAR(max), i.[NavClaimKey] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[NavClaimKey] <> i.[NavClaimKey] ) OR
        ( d.[NavClaimKey] IS NULL AND i.[NavClaimKey] IS NOT NULL ) OR
        ( i.[NavClaimKey] IS NULL AND d.[NavClaimKey] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [NavClaimLawsuitKey] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        160,
        0,
        CONVERT( NVARCHAR(max), d.[NavClaimLawsuitKey] ),
        CONVERT( NVARCHAR(max), i.[NavClaimLawsuitKey] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[NavClaimLawsuitKey] <> i.[NavClaimLawsuitKey] ) OR
        ( d.[NavClaimLawsuitKey] IS NULL AND i.[NavClaimLawsuitKey] IS NOT NULL ) OR
        ( i.[NavClaimLawsuitKey] IS NULL AND d.[NavClaimLawsuitKey] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [NavIsPrimaryForClaim] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        161,
        0,
        CONVERT( NVARCHAR(max), d.[NavIsPrimaryForClaim] ),
        CONVERT( NVARCHAR(max), i.[NavIsPrimaryForClaim] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[NavIsPrimaryForClaim] <> i.[NavIsPrimaryForClaim] ) OR
        ( d.[NavIsPrimaryForClaim] IS NULL AND i.[NavIsPrimaryForClaim] IS NOT NULL ) OR
        ( i.[NavIsPrimaryForClaim] IS NULL AND d.[NavIsPrimaryForClaim] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [NavLawsuitKey] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        162,
        0,
        CONVERT( NVARCHAR(max), d.[NavLawsuitKey] ),
        CONVERT( NVARCHAR(max), i.[NavLawsuitKey] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[NavLawsuitKey] <> i.[NavLawsuitKey] ) OR
        ( d.[NavLawsuitKey] IS NULL AND i.[NavLawsuitKey] IS NOT NULL ) OR
        ( i.[NavLawsuitKey] IS NULL AND d.[NavLawsuitKey] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [NavStatus] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        163,
        0,
        CONVERT( NVARCHAR(max), d.[NavStatus] ),
        CONVERT( NVARCHAR(max), i.[NavStatus] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[NavStatus] <> i.[NavStatus] ) OR
        ( d.[NavStatus] IS NULL AND i.[NavStatus] IS NOT NULL ) OR
        ( i.[NavStatus] IS NULL AND d.[NavStatus] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [NavStatusDate] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        164,
        0,
        CONVERT( NVARCHAR( 34 ), d.[NavStatusDate], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[NavStatusDate], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[NavStatusDate] <> i.[NavStatusDate] ) OR
        ( d.[NavStatusDate] IS NULL AND i.[NavStatusDate] IS NOT NULL ) OR
        ( i.[NavStatusDate] IS NULL AND d.[NavStatusDate] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [PresentedYear] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        404,
        0,
        CONVERT(NVARCHAR, d.[PresentedYear]),
        CONVERT(NVARCHAR, i.[PresentedYear])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[PresentedYear] <> i.[PresentedYear] ) OR
        ( d.[PresentedYear] IS NULL AND i.[PresentedYear] IS NOT NULL ) OR
        ( i.[PresentedYear] IS NULL AND d.[PresentedYear] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [SPUsername] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        165,
        0,
        CONVERT( NVARCHAR(max), d.[SPUsername] ),
        CONVERT( NVARCHAR(max), i.[SPUsername] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[SPUsername] <> i.[SPUsername] ) OR
        ( d.[SPUsername] IS NULL AND i.[SPUsername] IS NOT NULL ) OR
        ( i.[SPUsername] IS NULL AND d.[SPUsername] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [TrialGroupID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        166,
        0,
        CONVERT(NVARCHAR, d.[TrialGroupID]),
        CONVERT(NVARCHAR, i.[TrialGroupID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[TrialGroupID] <> i.[TrialGroupID] ) OR
        ( d.[TrialGroupID] IS NULL AND i.[TrialGroupID] IS NOT NULL ) OR
        ( i.[TrialGroupID] IS NULL AND d.[TrialGroupID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [TrialRiskID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        405,
        0,
        CONVERT(NVARCHAR, d.[TrialRiskID]),
        CONVERT(NVARCHAR, i.[TrialRiskID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
          ( d.[TrialRiskID] <> i.[TrialRiskID] ) OR
        ( d.[TrialRiskID] IS NULL AND i.[TrialRiskID] IS NOT NULL ) OR
        ( i.[TrialRiskID] IS NULL AND d.[TrialRiskID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    -- Handle capture values that were not already caught by auditing
    IF( @CaptureCount > 0 )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitID], d.[ClaimLawsuitID] )),
        165,
        8,
        CONVERT( NVARCHAR(max), i.[SPUsername] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitID] = i.[ClaimLawsuitID] )
      WHERE
        ( d.[SPUsername] = i.[SPUsername] ) OR
        ( ( d.[SPUsername] IS NULL ) AND ( i.[SPUsername] IS NULL ) )
    END

    IF( @CaptureCount > 0 )
    begin
    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 11, @RowsAffected, 'U', @AuditedUser
    end
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblClaimLawsuit_Upd]', 'last', 'update', null
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE TRIGGER [dbo].[upd_CLDocketNumbers]
   ON  [dbo].[tblClaimLawsuit]
   AFTER UPDATE
AS 
BEGIN
      SET NOCOUNT ON;
      
       
           
DECLARE @CPI bigint
DECLARE @DocketNumStr nvarchar(4000)
DECLARE @addDocket nvarchar(4000)
BEGIN

      
      --Declare Cursor
      DECLARE Cur_CPI CURSOR Local Fast_Forward for
      SELECT DISTINCT ClaimantPersonalInfoID FROM inserted INNER JOIN deleted ON inserted.ClaimLawsuitID = deleted.ClaimLawsuitID INNER JOIN tblClaim c ON c.ClaimID = inserted.ClaimID or c.claimID = deleted.claimid
      --Open CPI Cursor
      OPEN Cur_CPI

      --Fetch Data
      FETCH NEXT FROM Cur_CPI INTO @CPI

      WHILE @@Fetch_Status = 0
      BEGIN
                        SET @DocketNumStr = ''
                        DECLARE Cur_Docket CURSOR Local Fast_Forward for
                        SELECT L.DocketNumber FROM tblLawsuit l 
                        INNER JOIN tblClaimLawsuit cl ON cl.LawsuitID = l.LawsuitID
                        INNER JOIN tblClaim c ON C.ClaimID = cl.ClaimID WHERE ClaimantPersonalInfoID = @CPI ORDER BY L.DocketNumber ASC

                        --Open Docket Cursor
                        OPEN Cur_Docket

                        --Fetch Data
                        FETCH NEXT FROM Cur_Docket INTO @addDocket

                        WHILE @@Fetch_Status = 0
                        BEGIN
                              --assemble the string
                              if LEN(@DocketNumStr) = 0 SET @DocketNumStr = @addDocket
                              else SET @DocketNumStr =  @DocketNumStr  + ', ' +  @addDocket
                              
                        
            
                              FETCH NEXT FROM Cur_Docket INTO @addDocket
                              
                        END
                        
                        CLOSE Cur_Docket 
                        DEALLOCATE Cur_Docket
                        --insert the string into a temp table with CPI
                        UPDATE tblClaimantDocketNumbers SET DocketNumbers = @DocketNumStr WHERE ClaimantPersonalInfoID = @CPI 
                        
      FETCH NEXT FROM Cur_CPI INTO @CPI
      
      END

CLOSE Cur_CPI 
DEALLOCATE Cur_CPI 
            
END

    -- Insert statements for trigger here

END
GO
DISABLE TRIGGER [dbo].[upd_CLDocketNumbers] ON [dbo].[tblClaimLawsuit]
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [PK_mBtblClaimLawsuitNEW] PRIMARY KEY CLUSTERED  ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimLawsuit_tblClaim] ON [dbo].[tblClaimLawsuit] ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuit_ClaimID_IsMDL] ON [dbo].[tblClaimLawsuit] ([ClaimID], [IsMDL]) INCLUDE ([ClaimLawsuitID], [IsPrimaryForClaim], [LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuit_IsMDL] ON [dbo].[tblClaimLawsuit] ([IsMDL]) INCLUDE ([ClaimID], [ClaimLawsuitID], [IsPrimaryForClaim], [LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_13865_13864_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([IsPrimaryForClaim]) INCLUDE ([ClaimID], [ClaimLawsuitID], [LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimLawsuit_tblLawsuit] ON [dbo].[tblClaimLawsuit] ([LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_LawsuitID_ClaimLawsuitID_ClaimID_IsPrimaryForClaim] ON [dbo].[tblClaimLawsuit] ([LawsuitID]) INCLUDE ([ClaimID], [ClaimLawsuitID], [NavIsPrimaryForClaim]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [FK_tblClaimLawsuit_tblClaim] FOREIGN KEY ([ClaimID]) REFERENCES [dbo].[tblClaim] ([ClaimID])
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [FK_tblClaimLawsuit_tblLawsuit] FOREIGN KEY ([LawsuitID]) REFERENCES [dbo].[tblLawsuit] ([LawsuitID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuit] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuit] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuit] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuit] TO [power_user]
GO
