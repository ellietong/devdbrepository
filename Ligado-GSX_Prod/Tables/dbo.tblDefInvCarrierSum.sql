CREATE TABLE [dbo].[tblDefInvCarrierSum]
(
[CarrierName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledAmount] [money] NULL,
[AmountReceived] [money] NULL,
[DatePaid] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvCarrierSum] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefInvCarrierSum] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvCarrierSum] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDefInvCarrierSum] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvCarrierSum] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefInvCarrierSum] TO [power_user]
GO
