CREATE TABLE [dbo].[Temp_DefenseExpense]
(
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Percentage] [float] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[Temp_DefenseExpense] TO [base_user]
GRANT SELECT ON  [dbo].[Temp_DefenseExpense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Temp_DefenseExpense] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Temp_DefenseExpense] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Temp_DefenseExpense] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Temp_DefenseExpense] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Temp_DefenseExpense] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[Temp_DefenseExpense] TO [power_user]
GRANT SELECT ON  [dbo].[Temp_DefenseExpense] TO [power_user]
GRANT INSERT ON  [dbo].[Temp_DefenseExpense] TO [power_user]
GRANT DELETE ON  [dbo].[Temp_DefenseExpense] TO [power_user]
GRANT UPDATE ON  [dbo].[Temp_DefenseExpense] TO [power_user]
GO
