CREATE TABLE [dbo].[TempSterlingInvoiceUpload]
(
[TempSterlingInvoiceUploadID] [int] NOT NULL IDENTITY(1, 1),
[UniqueInvoiceID] [int] NULL,
[Period Date] [date] NULL,
[EmailDate] [datetime] NULL,
[Account] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Firm] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [bigint] NULL,
[Claimant Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice Date] [date] NULL,
[Month of Services Performed] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date Received by Sterling] [datetime] NULL,
[Amount of Original Invoice Fees] [money] NULL,
[Amount of Original Invoice Expenses] [money] NULL,
[Total Amount of Original Invoice] [money] NULL,
[Date Invoice Review Complete and Sent for Payment] [datetime] NULL,
[Billing Issues Identified in Invoices] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Total Amount of Fee Deductions] [money] NULL,
[Total Amount of Expense Deductions] [money] NULL,
[Total Amount of Deductions] [money] NULL,
[Total Amount of Invoice Following SAG Review] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TempSterlingInvoiceUpload] ADD CONSTRAINT [PK_TempSterlingInvoiceUpload] PRIMARY KEY CLUSTERED  ([TempSterlingInvoiceUploadID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[TempSterlingInvoiceUpload] TO [base_user]
GRANT SELECT ON  [dbo].[TempSterlingInvoiceUpload] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TempSterlingInvoiceUpload] TO [Claims_User]
GRANT SELECT ON  [dbo].[TempSterlingInvoiceUpload] TO [Claims_User]
GRANT INSERT ON  [dbo].[TempSterlingInvoiceUpload] TO [Claims_User]
GRANT DELETE ON  [dbo].[TempSterlingInvoiceUpload] TO [Claims_User]
GRANT UPDATE ON  [dbo].[TempSterlingInvoiceUpload] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[TempSterlingInvoiceUpload] TO [Ligado_User]
GRANT SELECT ON  [dbo].[TempSterlingInvoiceUpload] TO [Ligado_User]
GRANT INSERT ON  [dbo].[TempSterlingInvoiceUpload] TO [Ligado_User]
GRANT DELETE ON  [dbo].[TempSterlingInvoiceUpload] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[TempSterlingInvoiceUpload] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[TempSterlingInvoiceUpload] TO [power_user]
GRANT SELECT ON  [dbo].[TempSterlingInvoiceUpload] TO [power_user]
GRANT INSERT ON  [dbo].[TempSterlingInvoiceUpload] TO [power_user]
GRANT DELETE ON  [dbo].[TempSterlingInvoiceUpload] TO [power_user]
GRANT UPDATE ON  [dbo].[TempSterlingInvoiceUpload] TO [power_user]
GO
