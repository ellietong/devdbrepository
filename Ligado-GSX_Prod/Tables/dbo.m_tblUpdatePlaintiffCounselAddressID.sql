CREATE TABLE [dbo].[m_tblUpdatePlaintiffCounselAddressID]
(
[OldPlaintiffCounselAddressID] [int] NULL,
[NewPlaintiffCounselAddressID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblUpdatePlaintiffCounselAddressID] TO [power_user]
GO
