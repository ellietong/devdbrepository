CREATE TABLE [dbo].[tblDefenseCounselState]
(
[DefenseCounselStateID] [int] NOT NULL IDENTITY(1, 1),
[DefenseCounselAddressID] [int] NOT NULL,
[StateID] [int] NOT NULL,
[ResponsibleDefenseCounselAddressID] [int] NOT NULL,
[IsActiveCounsel] [bit] NOT NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblDefenseCounselState_Del] ON [dbo].[tblDefenseCounselState] FOR DELETE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:53 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int
  SELECT @RowsAffected = COUNT(*) FROM DELETED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    -- Populate audit detail records...
    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'DefenseCounselStateID=' + CONVERT(VARCHAR, [DefenseCounselStateID]),
      530,
      0,
      CONVERT(NVARCHAR, [DefenseCounselAddressID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'DefenseCounselStateID=' + CONVERT(VARCHAR, [DefenseCounselStateID]),
      531,
      0,
      CONVERT(NVARCHAR, [DefenseCounselStateID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'DefenseCounselStateID=' + CONVERT(VARCHAR, [DefenseCounselStateID]),
      532,
      0,
      CONVERT(NVARCHAR, [IsActiveCounsel])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'DefenseCounselStateID=' + CONVERT(VARCHAR, [DefenseCounselStateID]),
      533,
      0,
      CONVERT(NVARCHAR, [ResponsibleDefenseCounselAddressID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'DefenseCounselStateID=' + CONVERT(VARCHAR, [DefenseCounselStateID]),
      534,
      0,
      CONVERT(NVARCHAR, [StateID])
    FROM
      DELETED

    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 40, @RowsAffected, 'D', @AuditedUser
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblDefenseCounselState_Del]', 'last', 'delete', null
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblDefenseCounselState_Upd] ON [dbo].[tblDefenseCounselState] FOR UPDATE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:53 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int, @ChangeCount INT, @CaptureCount INT
  SELECT @RowsAffected = COUNT(*) FROM INSERTED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    SELECT @CaptureCount = 0

    -- Test for a key update...
    SELECT @ChangeCount = COUNT(*)
    FROM
      INSERTED i
      FULL OUTER JOIN DELETED d ON ( d.[DefenseCounselStateID] = i.[DefenseCounselStateID] )

    -- Populate audit detail records...
    IF( UPDATE( [DefenseCounselAddressID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'DefenseCounselStateID=' + CONVERT(VARCHAR, ISNULL( i.[DefenseCounselStateID], d.[DefenseCounselStateID] )),
        530,
        0,
        CONVERT(NVARCHAR, d.[DefenseCounselAddressID]),
        CONVERT(NVARCHAR, i.[DefenseCounselAddressID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[DefenseCounselStateID] = i.[DefenseCounselStateID] )
      WHERE
          ( d.[DefenseCounselAddressID] <> i.[DefenseCounselAddressID] ) OR
        ( d.[DefenseCounselAddressID] IS NULL AND i.[DefenseCounselAddressID] IS NOT NULL ) OR
        ( i.[DefenseCounselAddressID] IS NULL AND d.[DefenseCounselAddressID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [DefenseCounselStateID] ) )
    BEGIN
      -- Audit key column updates...
      IF( @ChangeCount > @RowsAffected )
      BEGIN
        INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
        SELECT
          @AuditID,
          'DefenseCounselStateID=' + CONVERT(VARCHAR, ISNULL( i.[DefenseCounselStateID], d.[DefenseCounselStateID] )),
          531,
          0,
          CONVERT(NVARCHAR, d.[DefenseCounselStateID]),
          CONVERT(NVARCHAR, i.[DefenseCounselStateID])
        FROM
          INSERTED i
          FULL OUTER JOIN DELETED d ON ( d.[DefenseCounselStateID] = i.[DefenseCounselStateID] )
        WHERE
          ( d.[DefenseCounselStateID] <> i.[DefenseCounselStateID] ) OR
          ( d.[DefenseCounselStateID] IS NULL AND i.[DefenseCounselStateID] IS NOT NULL ) OR
          ( i.[DefenseCounselStateID] IS NULL AND d.[DefenseCounselStateID] IS NOT NULL )
        SELECT @CaptureCount = @CaptureCount + @@RowCount
      END
    END

    IF( UPDATE( [IsActiveCounsel] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'DefenseCounselStateID=' + CONVERT(VARCHAR, ISNULL( i.[DefenseCounselStateID], d.[DefenseCounselStateID] )),
        532,
        0,
        CONVERT(NVARCHAR, d.[IsActiveCounsel]),
        CONVERT(NVARCHAR, i.[IsActiveCounsel])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[DefenseCounselStateID] = i.[DefenseCounselStateID] )
      WHERE
          ( d.[IsActiveCounsel] <> i.[IsActiveCounsel] ) OR
        ( d.[IsActiveCounsel] IS NULL AND i.[IsActiveCounsel] IS NOT NULL ) OR
        ( i.[IsActiveCounsel] IS NULL AND d.[IsActiveCounsel] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [ResponsibleDefenseCounselAddressID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'DefenseCounselStateID=' + CONVERT(VARCHAR, ISNULL( i.[DefenseCounselStateID], d.[DefenseCounselStateID] )),
        533,
        0,
        CONVERT(NVARCHAR, d.[ResponsibleDefenseCounselAddressID]),
        CONVERT(NVARCHAR, i.[ResponsibleDefenseCounselAddressID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[DefenseCounselStateID] = i.[DefenseCounselStateID] )
      WHERE
          ( d.[ResponsibleDefenseCounselAddressID] <> i.[ResponsibleDefenseCounselAddressID] ) OR
        ( d.[ResponsibleDefenseCounselAddressID] IS NULL AND i.[ResponsibleDefenseCounselAddressID] IS NOT NULL ) OR
        ( i.[ResponsibleDefenseCounselAddressID] IS NULL AND d.[ResponsibleDefenseCounselAddressID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [StateID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'DefenseCounselStateID=' + CONVERT(VARCHAR, ISNULL( i.[DefenseCounselStateID], d.[DefenseCounselStateID] )),
        534,
        0,
        CONVERT(NVARCHAR, d.[StateID]),
        CONVERT(NVARCHAR, i.[StateID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[DefenseCounselStateID] = i.[DefenseCounselStateID] )
      WHERE
          ( d.[StateID] <> i.[StateID] ) OR
        ( d.[StateID] IS NULL AND i.[StateID] IS NOT NULL ) OR
        ( i.[StateID] IS NULL AND d.[StateID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( @CaptureCount > 0 )
    begin
    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 40, @RowsAffected, 'U', @AuditedUser
    end
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblDefenseCounselState_Upd]', 'last', 'update', null
GO
ALTER TABLE [dbo].[tblDefenseCounselState] ADD CONSTRAINT [PK_mBtblDefenseCounselState] PRIMARY KEY CLUSTERED  ([DefenseCounselStateID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselState] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselState] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselState] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseCounselState] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseCounselState] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselState] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDefenseCounselState] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblDefenseCounselState] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblDefenseCounselState] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblDefenseCounselState] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseCounselState] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseCounselState] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseCounselState] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefenseCounselState] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseCounselState] TO [power_user]
GO
