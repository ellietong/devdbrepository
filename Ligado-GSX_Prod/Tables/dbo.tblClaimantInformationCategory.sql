CREATE TABLE [dbo].[tblClaimantInformationCategory]
(
[ClaimantInfoCategoryID] [int] NOT NULL,
[ClaimantInfoCategory] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantInformationCategory] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantInformationCategory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantInformationCategory] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimantInformationCategory] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimantInformationCategory] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimantInformationCategory] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimantInformationCategory] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantInformationCategory] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantInformationCategory] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantInformationCategory] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantInformationCategory] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantInformationCategory] TO [power_user]
GO
