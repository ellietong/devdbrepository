CREATE TABLE [dbo].[tblPaymentLegalBill]
(
[LegalBillID] [bigint] NOT NULL IDENTITY(1, 1),
[CheckID] [bigint] NULL,
[InvoiceID] [bigint] NULL,
[PaymentAmount] [money] NULL,
[IsFullyPaid] [bit] NULL,
[PaymentInvoiceNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblPaymentLegalBill_Del] ON [dbo].[tblPaymentLegalBill] FOR DELETE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:53 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int
  SELECT @RowsAffected = COUNT(*) FROM DELETED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    -- Populate audit detail records...
    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'LegalBillID=' + CONVERT(VARCHAR, [LegalBillID]),
      577,
      0,
      CONVERT(NVARCHAR, [CheckID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'LegalBillID=' + CONVERT(VARCHAR, [LegalBillID]),
      578,
      0,
      CONVERT(NVARCHAR, [InvoiceID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'LegalBillID=' + CONVERT(VARCHAR, [LegalBillID]),
      579,
      0,
      CONVERT(NVARCHAR, [IsFullyPaid])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'LegalBillID=' + CONVERT(VARCHAR, [LegalBillID]),
      580,
      0,
      CONVERT(NVARCHAR, [LegalBillID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'LegalBillID=' + CONVERT(VARCHAR, [LegalBillID]),
      581,
      0,
      CONVERT( NVARCHAR( 30 ), [PaymentAmount], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'LegalBillID=' + CONVERT(VARCHAR, [LegalBillID]),
      582,
      0,
      CONVERT( NVARCHAR(max), [PaymentInvoiceNotes] )
    FROM
      DELETED

    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 48, @RowsAffected, 'D', @AuditedUser
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblPaymentLegalBill_Del]', 'last', 'delete', null
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblPaymentLegalBill_Upd] ON [dbo].[tblPaymentLegalBill] FOR UPDATE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:53 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int, @ChangeCount INT, @CaptureCount INT
  SELECT @RowsAffected = COUNT(*) FROM INSERTED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    SELECT @CaptureCount = 0

    -- Test for a key update...
    SELECT @ChangeCount = COUNT(*)
    FROM
      INSERTED i
      FULL OUTER JOIN DELETED d ON ( d.[LegalBillID] = i.[LegalBillID] )

    -- Populate audit detail records...
    IF( UPDATE( [CheckID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'LegalBillID=' + CONVERT(VARCHAR, ISNULL( i.[LegalBillID], d.[LegalBillID] )),
        577,
        0,
        CONVERT(NVARCHAR, d.[CheckID]),
        CONVERT(NVARCHAR, i.[CheckID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[LegalBillID] = i.[LegalBillID] )
      WHERE
          ( d.[CheckID] <> i.[CheckID] ) OR
        ( d.[CheckID] IS NULL AND i.[CheckID] IS NOT NULL ) OR
        ( i.[CheckID] IS NULL AND d.[CheckID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [InvoiceID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'LegalBillID=' + CONVERT(VARCHAR, ISNULL( i.[LegalBillID], d.[LegalBillID] )),
        578,
        0,
        CONVERT(NVARCHAR, d.[InvoiceID]),
        CONVERT(NVARCHAR, i.[InvoiceID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[LegalBillID] = i.[LegalBillID] )
      WHERE
          ( d.[InvoiceID] <> i.[InvoiceID] ) OR
        ( d.[InvoiceID] IS NULL AND i.[InvoiceID] IS NOT NULL ) OR
        ( i.[InvoiceID] IS NULL AND d.[InvoiceID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [IsFullyPaid] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'LegalBillID=' + CONVERT(VARCHAR, ISNULL( i.[LegalBillID], d.[LegalBillID] )),
        579,
        0,
        CONVERT(NVARCHAR, d.[IsFullyPaid]),
        CONVERT(NVARCHAR, i.[IsFullyPaid])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[LegalBillID] = i.[LegalBillID] )
      WHERE
          ( d.[IsFullyPaid] <> i.[IsFullyPaid] ) OR
        ( d.[IsFullyPaid] IS NULL AND i.[IsFullyPaid] IS NOT NULL ) OR
        ( i.[IsFullyPaid] IS NULL AND d.[IsFullyPaid] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [LegalBillID] ) )
    BEGIN
      -- Audit key column updates...
      IF( @ChangeCount > @RowsAffected )
      BEGIN
        INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
        SELECT
          @AuditID,
          'LegalBillID=' + CONVERT(VARCHAR, ISNULL( i.[LegalBillID], d.[LegalBillID] )),
          580,
          0,
          CONVERT(NVARCHAR, d.[LegalBillID]),
          CONVERT(NVARCHAR, i.[LegalBillID])
        FROM
          INSERTED i
          FULL OUTER JOIN DELETED d ON ( d.[LegalBillID] = i.[LegalBillID] )
        WHERE
          ( d.[LegalBillID] <> i.[LegalBillID] ) OR
          ( d.[LegalBillID] IS NULL AND i.[LegalBillID] IS NOT NULL ) OR
          ( i.[LegalBillID] IS NULL AND d.[LegalBillID] IS NOT NULL )
        SELECT @CaptureCount = @CaptureCount + @@RowCount
      END
    END

    IF( UPDATE( [PaymentAmount] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'LegalBillID=' + CONVERT(VARCHAR, ISNULL( i.[LegalBillID], d.[LegalBillID] )),
        581,
        0,
        CONVERT( NVARCHAR( 30 ), d.[PaymentAmount], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[PaymentAmount], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[LegalBillID] = i.[LegalBillID] )
      WHERE
          ( d.[PaymentAmount] <> i.[PaymentAmount] ) OR
        ( d.[PaymentAmount] IS NULL AND i.[PaymentAmount] IS NOT NULL ) OR
        ( i.[PaymentAmount] IS NULL AND d.[PaymentAmount] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [PaymentInvoiceNotes] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'LegalBillID=' + CONVERT(VARCHAR, ISNULL( i.[LegalBillID], d.[LegalBillID] )),
        582,
        0,
        CONVERT( NVARCHAR(max), d.[PaymentInvoiceNotes] ),
        CONVERT( NVARCHAR(max), i.[PaymentInvoiceNotes] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[LegalBillID] = i.[LegalBillID] )
      WHERE
          ( d.[PaymentInvoiceNotes] <> i.[PaymentInvoiceNotes] ) OR
        ( d.[PaymentInvoiceNotes] IS NULL AND i.[PaymentInvoiceNotes] IS NOT NULL ) OR
        ( i.[PaymentInvoiceNotes] IS NULL AND d.[PaymentInvoiceNotes] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( @CaptureCount > 0 )
    begin
    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 48, @RowsAffected, 'U', @AuditedUser
    end
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblPaymentLegalBill_Upd]', 'last', 'update', null
GO
ALTER TABLE [dbo].[tblPaymentLegalBill] ADD CONSTRAINT [PK_tblPaymentLegalBill] PRIMARY KEY CLUSTERED  ([LegalBillID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [base_user]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [CheckApproval_User]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [CheckWriter_User]
GRANT INSERT ON  [dbo].[tblPaymentLegalBill] TO [CheckWriter_User]
GRANT UPDATE ON  [dbo].[tblPaymentLegalBill] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPaymentLegalBill] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPaymentLegalBill] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPaymentLegalBill] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentLegalBill] TO [power_user]
GRANT SELECT ON  [dbo].[tblPaymentLegalBill] TO [power_user]
GRANT INSERT ON  [dbo].[tblPaymentLegalBill] TO [power_user]
GRANT DELETE ON  [dbo].[tblPaymentLegalBill] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPaymentLegalBill] TO [power_user]
GO
