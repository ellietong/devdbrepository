CREATE TABLE [dbo].[tblDiscountProvisionByOccType]
(
[DiscountProvisionID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionID] [int] NULL,
[ProvisionValueID] [int] NULL,
[ProvisionIDProvisionValueID] [int] NULL,
[ProvisionOccTypeDiscount] [money] NULL,
[OccurrenceTypeID] [int] NULL,
[discountid] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountProvisionByOccType] ADD CONSTRAINT [PK_tblDiscountProvisionByOccType] PRIMARY KEY CLUSTERED  ([DiscountProvisionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDiscountProvisionByOccType_tblOccurrenceType] ON [dbo].[tblDiscountProvisionByOccType] ([OccurrenceTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblDiscountProvisionByOccType_tblProvisionValue] ON [dbo].[tblDiscountProvisionByOccType] ([ProvisionValueID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountProvisionByOccType] ADD CONSTRAINT [FK_tblDiscountProvisionByOccType_tblOccurrenceType] FOREIGN KEY ([OccurrenceTypeID]) REFERENCES [dbo].[tblOccurrenceType] ([OccurrenceTypeID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblDiscountProvisionByOccType] ADD CONSTRAINT [FK_tblDiscountProvisionByOccType_tblProvisionValue] FOREIGN KEY ([ProvisionValueID]) REFERENCES [dbo].[tblProvisionValue] ([ProvisionValueID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDiscountProvisionByOccType] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiscountProvisionByOccType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDiscountProvisionByOccType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDiscountProvisionByOccType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblDiscountProvisionByOccType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblDiscountProvisionByOccType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblDiscountProvisionByOccType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDiscountProvisionByOccType] TO [power_user]
GRANT SELECT ON  [dbo].[tblDiscountProvisionByOccType] TO [power_user]
GRANT INSERT ON  [dbo].[tblDiscountProvisionByOccType] TO [power_user]
GRANT DELETE ON  [dbo].[tblDiscountProvisionByOccType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDiscountProvisionByOccType] TO [power_user]
GO
