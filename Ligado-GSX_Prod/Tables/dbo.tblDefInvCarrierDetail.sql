CREATE TABLE [dbo].[tblDefInvCarrierDetail]
(
[InvoiceNumber] [bigint] NULL,
[FullName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [bigint] NULL,
[CarrierName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledAmount] [money] NULL,
[AmountReceived] [money] NULL,
[DatePaid] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvCarrierDetail] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefInvCarrierDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvCarrierDetail] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDefInvCarrierDetail] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvCarrierDetail] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefInvCarrierDetail] TO [power_user]
GO
