CREATE TABLE [dbo].[tblClaimLawsuitSmokingHistory]
(
[ClaimLawsuitSmokingHistoryID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[SmokingStart] [date] NULL,
[SmokingEnd] [date] NULL,
[PacksPerDay] [float] NULL,
[Username] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblClaimLawsuitSmokingHistory_Del] ON [dbo].[tblClaimLawsuitSmokingHistory] FOR DELETE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:52 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int
  SELECT @RowsAffected = COUNT(*) FROM DELETED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    -- Populate audit detail records...
    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimLawsuitSmokingHistoryID]),
      627,
      0,
      CONVERT(NVARCHAR, [ClaimLawsuitID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimLawsuitSmokingHistoryID]),
      628,
      0,
      CONVERT(NVARCHAR, [ClaimLawsuitSmokingHistoryID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimLawsuitSmokingHistoryID]),
      629,
      0,
      CONVERT( NVARCHAR( 100 ), [PacksPerDay], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimLawsuitSmokingHistoryID]),
      630,
      0,
      CONVERT( NVARCHAR( 34 ), [SmokingEnd], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimLawsuitSmokingHistoryID]),
      631,
      0,
      CONVERT( NVARCHAR( 34 ), [SmokingStart], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimLawsuitSmokingHistoryID]),
      632,
      8,
      CONVERT( NVARCHAR(max), [Username] )
    FROM
      DELETED

    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 56, @RowsAffected, 'D', @AuditedUser
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblClaimLawsuitSmokingHistory_Del]', 'last', 'delete', null
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblClaimLawsuitSmokingHistory_Upd] ON [dbo].[tblClaimLawsuitSmokingHistory] FOR UPDATE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:52 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int, @ChangeCount INT, @CaptureCount INT
  SELECT @RowsAffected = COUNT(*) FROM INSERTED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    SELECT @CaptureCount = 0

    -- Test for a key update...
    SELECT @ChangeCount = COUNT(*)
    FROM
      INSERTED i
      FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitSmokingHistoryID] = i.[ClaimLawsuitSmokingHistoryID] )

    -- Populate audit detail records...
    IF( UPDATE( [ClaimLawsuitID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitSmokingHistoryID], d.[ClaimLawsuitSmokingHistoryID] )),
        627,
        0,
        CONVERT(NVARCHAR, d.[ClaimLawsuitID]),
        CONVERT(NVARCHAR, i.[ClaimLawsuitID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitSmokingHistoryID] = i.[ClaimLawsuitSmokingHistoryID] )
      WHERE
          ( d.[ClaimLawsuitID] <> i.[ClaimLawsuitID] ) OR
        ( d.[ClaimLawsuitID] IS NULL AND i.[ClaimLawsuitID] IS NOT NULL ) OR
        ( i.[ClaimLawsuitID] IS NULL AND d.[ClaimLawsuitID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [ClaimLawsuitSmokingHistoryID] ) )
    BEGIN
      -- Audit key column updates...
      IF( @ChangeCount > @RowsAffected )
      BEGIN
        INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
        SELECT
          @AuditID,
          'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitSmokingHistoryID], d.[ClaimLawsuitSmokingHistoryID] )),
          628,
          0,
          CONVERT(NVARCHAR, d.[ClaimLawsuitSmokingHistoryID]),
          CONVERT(NVARCHAR, i.[ClaimLawsuitSmokingHistoryID])
        FROM
          INSERTED i
          FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitSmokingHistoryID] = i.[ClaimLawsuitSmokingHistoryID] )
        WHERE
          ( d.[ClaimLawsuitSmokingHistoryID] <> i.[ClaimLawsuitSmokingHistoryID] ) OR
          ( d.[ClaimLawsuitSmokingHistoryID] IS NULL AND i.[ClaimLawsuitSmokingHistoryID] IS NOT NULL ) OR
          ( i.[ClaimLawsuitSmokingHistoryID] IS NULL AND d.[ClaimLawsuitSmokingHistoryID] IS NOT NULL )
        SELECT @CaptureCount = @CaptureCount + @@RowCount
      END
    END

    IF( UPDATE( [PacksPerDay] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitSmokingHistoryID], d.[ClaimLawsuitSmokingHistoryID] )),
        629,
        0,
        CONVERT( NVARCHAR( 100 ), d.[PacksPerDay], 2 ),
        CONVERT( NVARCHAR( 100 ), i.[PacksPerDay], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitSmokingHistoryID] = i.[ClaimLawsuitSmokingHistoryID] )
      WHERE
          ( d.[PacksPerDay] <> i.[PacksPerDay] ) OR
        ( d.[PacksPerDay] IS NULL AND i.[PacksPerDay] IS NOT NULL ) OR
        ( i.[PacksPerDay] IS NULL AND d.[PacksPerDay] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [SmokingEnd] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitSmokingHistoryID], d.[ClaimLawsuitSmokingHistoryID] )),
        630,
        0,
        CONVERT( NVARCHAR( 34 ), d.[SmokingEnd], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[SmokingEnd], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitSmokingHistoryID] = i.[ClaimLawsuitSmokingHistoryID] )
      WHERE
          ( d.[SmokingEnd] <> i.[SmokingEnd] ) OR
        ( d.[SmokingEnd] IS NULL AND i.[SmokingEnd] IS NOT NULL ) OR
        ( i.[SmokingEnd] IS NULL AND d.[SmokingEnd] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [SmokingStart] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitSmokingHistoryID], d.[ClaimLawsuitSmokingHistoryID] )),
        631,
        0,
        CONVERT( NVARCHAR( 34 ), d.[SmokingStart], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[SmokingStart], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitSmokingHistoryID] = i.[ClaimLawsuitSmokingHistoryID] )
      WHERE
          ( d.[SmokingStart] <> i.[SmokingStart] ) OR
        ( d.[SmokingStart] IS NULL AND i.[SmokingStart] IS NOT NULL ) OR
        ( i.[SmokingStart] IS NULL AND d.[SmokingStart] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [Username] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitSmokingHistoryID], d.[ClaimLawsuitSmokingHistoryID] )),
        632,
        0,
        CONVERT( NVARCHAR(max), d.[Username] ),
        CONVERT( NVARCHAR(max), i.[Username] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitSmokingHistoryID] = i.[ClaimLawsuitSmokingHistoryID] )
      WHERE
          ( d.[Username] <> i.[Username] ) OR
        ( d.[Username] IS NULL AND i.[Username] IS NOT NULL ) OR
        ( i.[Username] IS NULL AND d.[Username] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    -- Handle capture values that were not already caught by auditing
    IF( @CaptureCount > 0 )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, NewValue )
      SELECT
        @AuditID,
        'ClaimLawsuitSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimLawsuitSmokingHistoryID], d.[ClaimLawsuitSmokingHistoryID] )),
        632,
        8,
        CONVERT( NVARCHAR(max), i.[Username] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimLawsuitSmokingHistoryID] = i.[ClaimLawsuitSmokingHistoryID] )
      WHERE
        ( d.[Username] = i.[Username] ) OR
        ( ( d.[Username] IS NULL ) AND ( i.[Username] IS NULL ) )
    END

    IF( @CaptureCount > 0 )
    begin
    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 56, @RowsAffected, 'U', @AuditedUser
    end
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblClaimLawsuitSmokingHistory_Upd]', 'last', 'update', null
GO
ALTER TABLE [dbo].[tblClaimLawsuitSmokingHistory] ADD CONSTRAINT [PK_tblClaimLawsuitSmokingHistory] PRIMARY KEY CLUSTERED  ([ClaimLawsuitSmokingHistoryID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitSmokingHistory] TO [power_user]
GO
