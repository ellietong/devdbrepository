CREATE TABLE [dbo].[tblBundledClaim]
(
[Injured_Party_Id] [float] NULL,
[Claim_Sequence_Nbr] [float] NULL,
[Claim_Status_Cd] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claim_Status_Desc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First_Diagnosis_Date] [datetime] NULL,
[First_Exposure_Date] [datetime] NULL,
[Last_Exposure_Date] [datetime] NULL,
[Diagnosis Cd] [float] NULL,
[Diagnosis Desc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settlement_Amt] [float] NULL,
[Settlement_Date] [datetime] NULL,
[Release_Received_Date] [datetime] NULL,
[Release_Type] [float] NULL,
[Release_Type Desc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Input_Date] [datetime] NULL,
[Settlement_Group] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claim_QC_Flag] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Dynamic Category Cd] [float] NULL,
[Dynamic Category Desc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image Status Nbr] [float] NULL,
[Image Status Desc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Box Nbr] [float] NULL,
[Box Desc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Roll Nbr] [float] NULL,
[Frame Nbr] [float] NULL,
[DISR Input Date] [datetime] NULL,
[Release Status Cd] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Release Status Desc] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pfizer Transfer Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Void Letter Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Void Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_m_tblBundledClaim_m_tblBundledInjuredParty] ON [dbo].[tblBundledClaim] ([Injured_Party_Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBundledClaim] ADD CONSTRAINT [FK_m_tblBundledClaim_m_tblBundledInjuredParty] FOREIGN KEY ([Injured_Party_Id]) REFERENCES [dbo].[tblBundledInjuredParty] ([Injured_Party_Id])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBundledClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblBundledClaim] TO [base_user]
GRANT SELECT ON  [dbo].[tblBundledClaim] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblBundledClaim] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblBundledClaim] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBundledClaim] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBundledClaim] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBundledClaim] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBundledClaim] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBundledClaim] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBundledClaim] TO [power_user]
GRANT SELECT ON  [dbo].[tblBundledClaim] TO [power_user]
GRANT INSERT ON  [dbo].[tblBundledClaim] TO [power_user]
GRANT DELETE ON  [dbo].[tblBundledClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBundledClaim] TO [power_user]
GO
