CREATE TABLE [dbo].[AuditRelationships]
(
[TableID] [int] NOT NULL,
[RelationshipID] [int] NOT NULL IDENTITY(1, 1),
[TableName] [sys].[sysname] NOT NULL,
[JoinExpression] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditRelationships] ADD CONSTRAINT [PK_AuditRelationships] PRIMARY KEY CLUSTERED  ([RelationshipID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditRelationships] ADD CONSTRAINT [FK_AuditTables] FOREIGN KEY ([TableID]) REFERENCES [dbo].[AuditTables] ([TableID])
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditRelationships] TO [base_user]
GRANT SELECT ON  [dbo].[AuditRelationships] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditRelationships] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditRelationships] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditRelationships] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditRelationships] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditRelationships] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditRelationships] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditRelationships] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditRelationships] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditRelationships] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditRelationships] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditRelationships] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditRelationships] TO [power_user]
GRANT SELECT ON  [dbo].[AuditRelationships] TO [power_user]
GRANT INSERT ON  [dbo].[AuditRelationships] TO [power_user]
GRANT DELETE ON  [dbo].[AuditRelationships] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditRelationships] TO [power_user]
GO
