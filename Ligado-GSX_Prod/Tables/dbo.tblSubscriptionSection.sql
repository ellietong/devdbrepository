CREATE TABLE [dbo].[tblSubscriptionSection]
(
[SubscriptionSectionID] [int] NOT NULL IDENTITY(1, 1),
[SubscriptionSection] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSubscriptionSection] ADD CONSTRAINT [PK_tblSubscriptionSection] PRIMARY KEY CLUSTERED  ([SubscriptionSectionID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionSection] TO [base_user]
GRANT SELECT ON  [dbo].[tblSubscriptionSection] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionSection] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSubscriptionSection] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSubscriptionSection] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSubscriptionSection] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSubscriptionSection] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionSection] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblSubscriptionSection] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionSection] TO [power_user]
GRANT SELECT ON  [dbo].[tblSubscriptionSection] TO [power_user]
GRANT INSERT ON  [dbo].[tblSubscriptionSection] TO [power_user]
GRANT DELETE ON  [dbo].[tblSubscriptionSection] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSubscriptionSection] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSubscriptionSection] TO [Subscription_User]
GRANT SELECT ON  [dbo].[tblSubscriptionSection] TO [Subscription_User]
GRANT INSERT ON  [dbo].[tblSubscriptionSection] TO [Subscription_User]
GRANT DELETE ON  [dbo].[tblSubscriptionSection] TO [Subscription_User]
GRANT UPDATE ON  [dbo].[tblSubscriptionSection] TO [Subscription_User]
GO
