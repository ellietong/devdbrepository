CREATE TABLE [dbo].[tblPolicyDates]
(
[PolicyDateID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NOT NULL,
[PStartDate] [datetime] NOT NULL,
[PEndDate] [datetime] NOT NULL,
[ActualPStartDate] [datetime] NOT NULL,
[ActualPEndDate] [datetime] NOT NULL,
[AnnualPeriod] [int] NULL,
[OldPolicyID] [int] NULL,
[OldPolicyDateID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyDates] ADD CONSTRAINT [PK_tblPolicyDates] PRIMARY KEY CLUSTERED  ([PolicyDateID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDates] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyDates] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDates] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPolicyDates] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPolicyDates] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPolicyDates] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPolicyDates] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPolicyDates] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblPolicyDates] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblPolicyDates] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblPolicyDates] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDates] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicyDates] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicyDates] TO [power_user]
GRANT DELETE ON  [dbo].[tblPolicyDates] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicyDates] TO [power_user]
GO
