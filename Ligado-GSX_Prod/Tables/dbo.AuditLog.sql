CREATE TABLE [dbo].[AuditLog]
(
[AuditLogID] [int] NOT NULL,
[TableID] [int] NOT NULL,
[RowsAffected] [int] NOT NULL,
[Event] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostedDateTime] [datetime] NOT NULL CONSTRAINT [DF_AuditLog_DateTime] DEFAULT (getdate()),
[UserName] [sys].[sysname] NOT NULL CONSTRAINT [DF_AuditLog_UserName] DEFAULT (suser_sname()),
[HostName] [sys].[sysname] NOT NULL CONSTRAINT [DF_AuditLog_HostName] DEFAULT (host_name()),
[ApplicationName] [sys].[sysname] NOT NULL CONSTRAINT [DF_AuditLog_AppName] DEFAULT (app_name())
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trg_kr_AuditLog_Delete]
  ON [dbo].[AuditLog] FOR DELETE
AS
BEGIN
  DELETE FROM AuditLogDetail FROM Deleted D
    WHERE AuditLogDetail.AuditLogID = D.AuditLogID
END
GO
ALTER TABLE [dbo].[AuditLog] ADD CONSTRAINT [PK_AuditLog] PRIMARY KEY NONCLUSTERED  ([AuditLogID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AuditLog_DateTime] ON [dbo].[AuditLog] ([PostedDateTime]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_AuditLog_Table] ON [dbo].[AuditLog] ([TableID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditLog] TO [base_user]
GRANT SELECT ON  [dbo].[AuditLog] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditLog] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditLog] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditLog] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditLog] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditLog] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditLog] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLog] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditLog] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditLog] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditLog] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLog] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditLog] TO [power_user]
GRANT SELECT ON  [dbo].[AuditLog] TO [power_user]
GRANT INSERT ON  [dbo].[AuditLog] TO [power_user]
GRANT DELETE ON  [dbo].[AuditLog] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditLog] TO [power_user]
GO
