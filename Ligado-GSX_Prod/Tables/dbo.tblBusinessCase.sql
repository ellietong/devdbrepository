CREATE TABLE [dbo].[tblBusinessCase]
(
[BusinessCaseID] [int] NOT NULL IDENTITY(1, 1),
[BusinessCase] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBusinessCase] ADD CONSTRAINT [PK_tblBusinessCase] PRIMARY KEY CLUSTERED  ([BusinessCaseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBusinessCase] TO [base_user]
GRANT SELECT ON  [dbo].[tblBusinessCase] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBusinessCase] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBusinessCase] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBusinessCase] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBusinessCase] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBusinessCase] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBusinessCase] TO [power_user]
GRANT SELECT ON  [dbo].[tblBusinessCase] TO [power_user]
GRANT INSERT ON  [dbo].[tblBusinessCase] TO [power_user]
GRANT DELETE ON  [dbo].[tblBusinessCase] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBusinessCase] TO [power_user]
GO
