CREATE TABLE [dbo].[m_tblLegalBillInvoices]
(
[InvoiceID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[NavLegacyID] [bigint] NULL,
[MatterID] [int] NOT NULL,
[DefenseCounselAddressID] [int] NULL,
[LegalBillID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavFirmID] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Firm] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LBMatter] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LBInvoiceNumber] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalInvoiceNumber] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Period] [datetime] NULL,
[SubmittedDate] [datetime] NULL,
[DanaApprovalDate] [datetime] NULL,
[LBApprovalDate] [datetime] NULL,
[SubmittedForPayment] [datetime] NULL,
[FundsRequestDate] [datetime] NULL,
[StatusID] [int] NULL,
[OriginalTotal] [money] NULL,
[OriginalFee] [money] NULL,
[OriginalDisbursement] [money] NULL,
[AdjustedTotal] [money] NULL,
[AdjustedFee] [money] NULL,
[AdjustedDisbursement] [money] NULL,
[AppealTotal] [money] NULL,
[AppealFee] [money] NULL,
[AppealCost] [money] NULL,
[InvoiceNotes] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppealFlag] [bit] NULL,
[NavPaidInvoice] [bit] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_m_tblLegalBillInvoices_Del] ON [dbo].[m_tblLegalBillInvoices] FOR DELETE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:52 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int
  SELECT @RowsAffected = COUNT(*) FROM DELETED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    -- Populate audit detail records...
    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      429,
      0,
      CONVERT( NVARCHAR( 30 ), [AdjustedDisbursement], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      430,
      0,
      CONVERT( NVARCHAR( 30 ), [AdjustedFee], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      431,
      0,
      CONVERT( NVARCHAR( 30 ), [AdjustedTotal], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      432,
      0,
      CONVERT( NVARCHAR( 30 ), [AppealCost], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      433,
      0,
      CONVERT( NVARCHAR( 30 ), [AppealFee], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      434,
      0,
      CONVERT(NVARCHAR, [AppealFlag])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      435,
      0,
      CONVERT( NVARCHAR( 30 ), [AppealTotal], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      436,
      0,
      CONVERT(NVARCHAR, [ClaimantPersonalInfoID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      437,
      0,
      CONVERT( NVARCHAR( 34 ), [DanaApprovalDate], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      438,
      0,
      CONVERT(NVARCHAR, [DefenseCounselAddressID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      439,
      0,
      CONVERT( NVARCHAR(max), [Firm] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      440,
      0,
      CONVERT( NVARCHAR( 34 ), [FundsRequestDate], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      441,
      0,
      CONVERT(NVARCHAR, [InvoiceID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      442,
      0,
      CONVERT( NVARCHAR(max), [InvoiceNotes] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      443,
      0,
      CONVERT( NVARCHAR( 34 ), [LBApprovalDate], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      444,
      0,
      CONVERT( NVARCHAR(max), [LBInvoiceNumber] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      445,
      0,
      CONVERT( NVARCHAR(max), [LBMatter] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      446,
      0,
      CONVERT( NVARCHAR(max), [LegalBillID] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      447,
      0,
      CONVERT(NVARCHAR, [MatterID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      448,
      0,
      CONVERT( NVARCHAR(max), [NavFirmID] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      449,
      0,
      CONVERT(NVARCHAR, [NavLegacyID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      450,
      0,
      CONVERT(NVARCHAR, [NavPaidInvoice])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      451,
      0,
      CONVERT( NVARCHAR( 30 ), [OriginalDisbursement], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      452,
      0,
      CONVERT( NVARCHAR( 30 ), [OriginalFee], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      453,
      0,
      CONVERT( NVARCHAR(max), [OriginalInvoiceNumber] )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      454,
      0,
      CONVERT( NVARCHAR( 30 ), [OriginalTotal], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      455,
      0,
      CONVERT( NVARCHAR( 34 ), [Period], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      456,
      0,
      CONVERT(NVARCHAR, [StatusID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      457,
      0,
      CONVERT( NVARCHAR( 34 ), [SubmittedDate], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'InvoiceID=' + CONVERT(VARCHAR, [InvoiceID]),
      458,
      0,
      CONVERT( NVARCHAR( 34 ), [SubmittedForPayment], 121 )
    FROM
      DELETED

    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 31, @RowsAffected, 'D', @AuditedUser
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_m_tblLegalBillInvoices_Del]', 'last', 'delete', null
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_m_tblLegalBillInvoices_Upd] ON [dbo].[m_tblLegalBillInvoices] FOR UPDATE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:52 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int, @ChangeCount INT, @CaptureCount INT
  SELECT @RowsAffected = COUNT(*) FROM INSERTED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    SELECT @CaptureCount = 0

    -- Test for a key update...
    SELECT @ChangeCount = COUNT(*)
    FROM
      INSERTED i
      FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )

    -- Populate audit detail records...
    IF( UPDATE( [AdjustedDisbursement] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        429,
        0,
        CONVERT( NVARCHAR( 30 ), d.[AdjustedDisbursement], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[AdjustedDisbursement], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[AdjustedDisbursement] <> i.[AdjustedDisbursement] ) OR
        ( d.[AdjustedDisbursement] IS NULL AND i.[AdjustedDisbursement] IS NOT NULL ) OR
        ( i.[AdjustedDisbursement] IS NULL AND d.[AdjustedDisbursement] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [AdjustedFee] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        430,
        0,
        CONVERT( NVARCHAR( 30 ), d.[AdjustedFee], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[AdjustedFee], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[AdjustedFee] <> i.[AdjustedFee] ) OR
        ( d.[AdjustedFee] IS NULL AND i.[AdjustedFee] IS NOT NULL ) OR
        ( i.[AdjustedFee] IS NULL AND d.[AdjustedFee] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [AdjustedTotal] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        431,
        0,
        CONVERT( NVARCHAR( 30 ), d.[AdjustedTotal], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[AdjustedTotal], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[AdjustedTotal] <> i.[AdjustedTotal] ) OR
        ( d.[AdjustedTotal] IS NULL AND i.[AdjustedTotal] IS NOT NULL ) OR
        ( i.[AdjustedTotal] IS NULL AND d.[AdjustedTotal] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [AppealCost] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        432,
        0,
        CONVERT( NVARCHAR( 30 ), d.[AppealCost], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[AppealCost], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[AppealCost] <> i.[AppealCost] ) OR
        ( d.[AppealCost] IS NULL AND i.[AppealCost] IS NOT NULL ) OR
        ( i.[AppealCost] IS NULL AND d.[AppealCost] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [AppealFee] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        433,
        0,
        CONVERT( NVARCHAR( 30 ), d.[AppealFee], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[AppealFee], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[AppealFee] <> i.[AppealFee] ) OR
        ( d.[AppealFee] IS NULL AND i.[AppealFee] IS NOT NULL ) OR
        ( i.[AppealFee] IS NULL AND d.[AppealFee] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [AppealFlag] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        434,
        0,
        CONVERT(NVARCHAR, d.[AppealFlag]),
        CONVERT(NVARCHAR, i.[AppealFlag])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[AppealFlag] <> i.[AppealFlag] ) OR
        ( d.[AppealFlag] IS NULL AND i.[AppealFlag] IS NOT NULL ) OR
        ( i.[AppealFlag] IS NULL AND d.[AppealFlag] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [AppealTotal] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        435,
        0,
        CONVERT( NVARCHAR( 30 ), d.[AppealTotal], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[AppealTotal], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[AppealTotal] <> i.[AppealTotal] ) OR
        ( d.[AppealTotal] IS NULL AND i.[AppealTotal] IS NOT NULL ) OR
        ( i.[AppealTotal] IS NULL AND d.[AppealTotal] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [ClaimantPersonalInfoID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        436,
        0,
        CONVERT(NVARCHAR, d.[ClaimantPersonalInfoID]),
        CONVERT(NVARCHAR, i.[ClaimantPersonalInfoID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[ClaimantPersonalInfoID] <> i.[ClaimantPersonalInfoID] ) OR
        ( d.[ClaimantPersonalInfoID] IS NULL AND i.[ClaimantPersonalInfoID] IS NOT NULL ) OR
        ( i.[ClaimantPersonalInfoID] IS NULL AND d.[ClaimantPersonalInfoID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [DanaApprovalDate] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        437,
        0,
        CONVERT( NVARCHAR( 34 ), d.[DanaApprovalDate], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[DanaApprovalDate], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[DanaApprovalDate] <> i.[DanaApprovalDate] ) OR
        ( d.[DanaApprovalDate] IS NULL AND i.[DanaApprovalDate] IS NOT NULL ) OR
        ( i.[DanaApprovalDate] IS NULL AND d.[DanaApprovalDate] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [DefenseCounselAddressID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        438,
        0,
        CONVERT(NVARCHAR, d.[DefenseCounselAddressID]),
        CONVERT(NVARCHAR, i.[DefenseCounselAddressID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[DefenseCounselAddressID] <> i.[DefenseCounselAddressID] ) OR
        ( d.[DefenseCounselAddressID] IS NULL AND i.[DefenseCounselAddressID] IS NOT NULL ) OR
        ( i.[DefenseCounselAddressID] IS NULL AND d.[DefenseCounselAddressID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [Firm] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        439,
        0,
        CONVERT( NVARCHAR(max), d.[Firm] ),
        CONVERT( NVARCHAR(max), i.[Firm] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[Firm] <> i.[Firm] ) OR
        ( d.[Firm] IS NULL AND i.[Firm] IS NOT NULL ) OR
        ( i.[Firm] IS NULL AND d.[Firm] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [FundsRequestDate] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        440,
        0,
        CONVERT( NVARCHAR( 34 ), d.[FundsRequestDate], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[FundsRequestDate], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[FundsRequestDate] <> i.[FundsRequestDate] ) OR
        ( d.[FundsRequestDate] IS NULL AND i.[FundsRequestDate] IS NOT NULL ) OR
        ( i.[FundsRequestDate] IS NULL AND d.[FundsRequestDate] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [InvoiceID] ) )
    BEGIN
      -- Audit key column updates...
      IF( @ChangeCount > @RowsAffected )
      BEGIN
        INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
        SELECT
          @AuditID,
          'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
          441,
          0,
          CONVERT(NVARCHAR, d.[InvoiceID]),
          CONVERT(NVARCHAR, i.[InvoiceID])
        FROM
          INSERTED i
          FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
        WHERE
          ( d.[InvoiceID] <> i.[InvoiceID] ) OR
          ( d.[InvoiceID] IS NULL AND i.[InvoiceID] IS NOT NULL ) OR
          ( i.[InvoiceID] IS NULL AND d.[InvoiceID] IS NOT NULL )
        SELECT @CaptureCount = @CaptureCount + @@RowCount
      END
    END

    IF( UPDATE( [InvoiceNotes] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        442,
        0,
        CONVERT( NVARCHAR(max), d.[InvoiceNotes] ),
        CONVERT( NVARCHAR(max), i.[InvoiceNotes] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[InvoiceNotes] <> i.[InvoiceNotes] ) OR
        ( d.[InvoiceNotes] IS NULL AND i.[InvoiceNotes] IS NOT NULL ) OR
        ( i.[InvoiceNotes] IS NULL AND d.[InvoiceNotes] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [LBApprovalDate] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        443,
        0,
        CONVERT( NVARCHAR( 34 ), d.[LBApprovalDate], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[LBApprovalDate], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[LBApprovalDate] <> i.[LBApprovalDate] ) OR
        ( d.[LBApprovalDate] IS NULL AND i.[LBApprovalDate] IS NOT NULL ) OR
        ( i.[LBApprovalDate] IS NULL AND d.[LBApprovalDate] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [LBInvoiceNumber] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        444,
        0,
        CONVERT( NVARCHAR(max), d.[LBInvoiceNumber] ),
        CONVERT( NVARCHAR(max), i.[LBInvoiceNumber] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[LBInvoiceNumber] <> i.[LBInvoiceNumber] ) OR
        ( d.[LBInvoiceNumber] IS NULL AND i.[LBInvoiceNumber] IS NOT NULL ) OR
        ( i.[LBInvoiceNumber] IS NULL AND d.[LBInvoiceNumber] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [LBMatter] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        445,
        0,
        CONVERT( NVARCHAR(max), d.[LBMatter] ),
        CONVERT( NVARCHAR(max), i.[LBMatter] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[LBMatter] <> i.[LBMatter] ) OR
        ( d.[LBMatter] IS NULL AND i.[LBMatter] IS NOT NULL ) OR
        ( i.[LBMatter] IS NULL AND d.[LBMatter] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [LegalBillID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        446,
        0,
        CONVERT( NVARCHAR(max), d.[LegalBillID] ),
        CONVERT( NVARCHAR(max), i.[LegalBillID] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[LegalBillID] <> i.[LegalBillID] ) OR
        ( d.[LegalBillID] IS NULL AND i.[LegalBillID] IS NOT NULL ) OR
        ( i.[LegalBillID] IS NULL AND d.[LegalBillID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [MatterID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        447,
        0,
        CONVERT(NVARCHAR, d.[MatterID]),
        CONVERT(NVARCHAR, i.[MatterID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[MatterID] <> i.[MatterID] ) OR
        ( d.[MatterID] IS NULL AND i.[MatterID] IS NOT NULL ) OR
        ( i.[MatterID] IS NULL AND d.[MatterID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [NavFirmID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        448,
        0,
        CONVERT( NVARCHAR(max), d.[NavFirmID] ),
        CONVERT( NVARCHAR(max), i.[NavFirmID] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[NavFirmID] <> i.[NavFirmID] ) OR
        ( d.[NavFirmID] IS NULL AND i.[NavFirmID] IS NOT NULL ) OR
        ( i.[NavFirmID] IS NULL AND d.[NavFirmID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [NavLegacyID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        449,
        0,
        CONVERT(NVARCHAR, d.[NavLegacyID]),
        CONVERT(NVARCHAR, i.[NavLegacyID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[NavLegacyID] <> i.[NavLegacyID] ) OR
        ( d.[NavLegacyID] IS NULL AND i.[NavLegacyID] IS NOT NULL ) OR
        ( i.[NavLegacyID] IS NULL AND d.[NavLegacyID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [NavPaidInvoice] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        450,
        0,
        CONVERT(NVARCHAR, d.[NavPaidInvoice]),
        CONVERT(NVARCHAR, i.[NavPaidInvoice])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[NavPaidInvoice] <> i.[NavPaidInvoice] ) OR
        ( d.[NavPaidInvoice] IS NULL AND i.[NavPaidInvoice] IS NOT NULL ) OR
        ( i.[NavPaidInvoice] IS NULL AND d.[NavPaidInvoice] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [OriginalDisbursement] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        451,
        0,
        CONVERT( NVARCHAR( 30 ), d.[OriginalDisbursement], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[OriginalDisbursement], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[OriginalDisbursement] <> i.[OriginalDisbursement] ) OR
        ( d.[OriginalDisbursement] IS NULL AND i.[OriginalDisbursement] IS NOT NULL ) OR
        ( i.[OriginalDisbursement] IS NULL AND d.[OriginalDisbursement] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [OriginalFee] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        452,
        0,
        CONVERT( NVARCHAR( 30 ), d.[OriginalFee], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[OriginalFee], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[OriginalFee] <> i.[OriginalFee] ) OR
        ( d.[OriginalFee] IS NULL AND i.[OriginalFee] IS NOT NULL ) OR
        ( i.[OriginalFee] IS NULL AND d.[OriginalFee] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [OriginalInvoiceNumber] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        453,
        0,
        CONVERT( NVARCHAR(max), d.[OriginalInvoiceNumber] ),
        CONVERT( NVARCHAR(max), i.[OriginalInvoiceNumber] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[OriginalInvoiceNumber] <> i.[OriginalInvoiceNumber] ) OR
        ( d.[OriginalInvoiceNumber] IS NULL AND i.[OriginalInvoiceNumber] IS NOT NULL ) OR
        ( i.[OriginalInvoiceNumber] IS NULL AND d.[OriginalInvoiceNumber] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [OriginalTotal] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        454,
        0,
        CONVERT( NVARCHAR( 30 ), d.[OriginalTotal], 2 ),
        CONVERT( NVARCHAR( 30 ), i.[OriginalTotal], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[OriginalTotal] <> i.[OriginalTotal] ) OR
        ( d.[OriginalTotal] IS NULL AND i.[OriginalTotal] IS NOT NULL ) OR
        ( i.[OriginalTotal] IS NULL AND d.[OriginalTotal] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [Period] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        455,
        0,
        CONVERT( NVARCHAR( 34 ), d.[Period], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[Period], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[Period] <> i.[Period] ) OR
        ( d.[Period] IS NULL AND i.[Period] IS NOT NULL ) OR
        ( i.[Period] IS NULL AND d.[Period] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [StatusID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        456,
        0,
        CONVERT(NVARCHAR, d.[StatusID]),
        CONVERT(NVARCHAR, i.[StatusID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[StatusID] <> i.[StatusID] ) OR
        ( d.[StatusID] IS NULL AND i.[StatusID] IS NOT NULL ) OR
        ( i.[StatusID] IS NULL AND d.[StatusID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [SubmittedDate] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        457,
        0,
        CONVERT( NVARCHAR( 34 ), d.[SubmittedDate], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[SubmittedDate], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[SubmittedDate] <> i.[SubmittedDate] ) OR
        ( d.[SubmittedDate] IS NULL AND i.[SubmittedDate] IS NOT NULL ) OR
        ( i.[SubmittedDate] IS NULL AND d.[SubmittedDate] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [SubmittedForPayment] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'InvoiceID=' + CONVERT(VARCHAR, ISNULL( i.[InvoiceID], d.[InvoiceID] )),
        458,
        0,
        CONVERT( NVARCHAR( 34 ), d.[SubmittedForPayment], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[SubmittedForPayment], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[InvoiceID] = i.[InvoiceID] )
      WHERE
          ( d.[SubmittedForPayment] <> i.[SubmittedForPayment] ) OR
        ( d.[SubmittedForPayment] IS NULL AND i.[SubmittedForPayment] IS NOT NULL ) OR
        ( i.[SubmittedForPayment] IS NULL AND d.[SubmittedForPayment] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( @CaptureCount > 0 )
    begin
    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 31, @RowsAffected, 'U', @AuditedUser
    end
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_m_tblLegalBillInvoices_Upd]', 'last', 'update', null
GO
ALTER TABLE [dbo].[m_tblLegalBillInvoices] ADD CONSTRAINT [PK_m_tblLegalBillInvoices] PRIMARY KEY CLUSTERED  ([InvoiceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_m_tbleLegalBillInvoices_CPID] ON [dbo].[m_tblLegalBillInvoices] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblLegalBillInvoices_DefenseCounselAddressID_MatterID_StatusID] ON [dbo].[m_tblLegalBillInvoices] ([DefenseCounselAddressID], [MatterID], [StatusID]) INCLUDE ([AdjustedTotal], [AppealFlag], [AppealTotal], [DanaApprovalDate], [InvoiceID], [OriginalInvoiceNumber], [OriginalTotal], [Period], [SubmittedDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_m_tblLegalBillInvoices_DefenseCounselAddressID_StatusID] ON [dbo].[m_tblLegalBillInvoices] ([DefenseCounselAddressID], [StatusID]) INCLUDE ([AdjustedTotal], [AppealFlag], [AppealTotal], [DanaApprovalDate], [InvoiceID], [MatterID], [NavPaidInvoice], [OriginalInvoiceNumber], [OriginalTotal], [Period], [SubmittedDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_LegalBillInvoices_MatterID_StatusID] ON [dbo].[m_tblLegalBillInvoices] ([MatterID], [StatusID]) INCLUDE ([AdjustedTotal], [AppealFlag], [AppealTotal], [DanaApprovalDate], [DefenseCounselAddressID], [InvoiceID], [OriginalInvoiceNumber], [OriginalTotal], [Period], [SubmittedDate]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblLegalBillInvoices] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblLegalBillInvoices] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblLegalBillInvoices] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblLegalBillInvoices] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblLegalBillInvoices] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblLegalBillInvoices] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblLegalBillInvoices] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblLegalBillInvoices] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblLegalBillInvoices] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblLegalBillInvoices] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblLegalBillInvoices] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblLegalBillInvoices] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblLegalBillInvoices] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblLegalBillInvoices] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblLegalBillInvoices] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblLegalBillInvoices] TO [power_user]
GO
