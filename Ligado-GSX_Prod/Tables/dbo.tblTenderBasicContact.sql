CREATE TABLE [dbo].[tblTenderBasicContact]
(
[TenderBasicContactID] [bigint] NOT NULL IDENTITY(1, 1),
[DefenseCounselAddressID] [bigint] NULL,
[PlaintiffCounselAddressID] [bigint] NULL,
[VendorFirmID] [int] NULL,
[DefendantCompanyID] [int] NULL,
[BrandID] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblTenderBasicContact_Del] ON [dbo].[tblTenderBasicContact] FOR DELETE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:53 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int
  SELECT @RowsAffected = COUNT(*) FROM DELETED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    -- Populate audit detail records...
    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'TenderBasicContactID=' + CONVERT(VARCHAR, [TenderBasicContactID]),
      599,
      0,
      CONVERT(NVARCHAR, [BrandID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'TenderBasicContactID=' + CONVERT(VARCHAR, [TenderBasicContactID]),
      600,
      0,
      CONVERT(NVARCHAR, [DefendantCompanyID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'TenderBasicContactID=' + CONVERT(VARCHAR, [TenderBasicContactID]),
      601,
      0,
      CONVERT(NVARCHAR, [DefenseCounselAddressID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'TenderBasicContactID=' + CONVERT(VARCHAR, [TenderBasicContactID]),
      602,
      0,
      CONVERT(NVARCHAR, [PlaintiffCounselAddressID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'TenderBasicContactID=' + CONVERT(VARCHAR, [TenderBasicContactID]),
      603,
      0,
      CONVERT(NVARCHAR, [TenderBasicContactID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'TenderBasicContactID=' + CONVERT(VARCHAR, [TenderBasicContactID]),
      604,
      0,
      CONVERT(NVARCHAR, [VendorFirmID])
    FROM
      DELETED

    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 51, @RowsAffected, 'D', @AuditedUser
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblTenderBasicContact_Del]', 'last', 'delete', null
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblTenderBasicContact_Upd] ON [dbo].[tblTenderBasicContact] FOR UPDATE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\youngc on October 3, 2014 12:53 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int, @ChangeCount INT, @CaptureCount INT
  SELECT @RowsAffected = COUNT(*) FROM INSERTED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    SELECT @CaptureCount = 0

    -- Test for a key update...
    SELECT @ChangeCount = COUNT(*)
    FROM
      INSERTED i
      FULL OUTER JOIN DELETED d ON ( d.[TenderBasicContactID] = i.[TenderBasicContactID] )

    -- Populate audit detail records...
    IF( UPDATE( [BrandID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'TenderBasicContactID=' + CONVERT(VARCHAR, ISNULL( i.[TenderBasicContactID], d.[TenderBasicContactID] )),
        599,
        0,
        CONVERT(NVARCHAR, d.[BrandID]),
        CONVERT(NVARCHAR, i.[BrandID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[TenderBasicContactID] = i.[TenderBasicContactID] )
      WHERE
          ( d.[BrandID] <> i.[BrandID] ) OR
        ( d.[BrandID] IS NULL AND i.[BrandID] IS NOT NULL ) OR
        ( i.[BrandID] IS NULL AND d.[BrandID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [DefendantCompanyID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'TenderBasicContactID=' + CONVERT(VARCHAR, ISNULL( i.[TenderBasicContactID], d.[TenderBasicContactID] )),
        600,
        0,
        CONVERT(NVARCHAR, d.[DefendantCompanyID]),
        CONVERT(NVARCHAR, i.[DefendantCompanyID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[TenderBasicContactID] = i.[TenderBasicContactID] )
      WHERE
          ( d.[DefendantCompanyID] <> i.[DefendantCompanyID] ) OR
        ( d.[DefendantCompanyID] IS NULL AND i.[DefendantCompanyID] IS NOT NULL ) OR
        ( i.[DefendantCompanyID] IS NULL AND d.[DefendantCompanyID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [DefenseCounselAddressID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'TenderBasicContactID=' + CONVERT(VARCHAR, ISNULL( i.[TenderBasicContactID], d.[TenderBasicContactID] )),
        601,
        0,
        CONVERT(NVARCHAR, d.[DefenseCounselAddressID]),
        CONVERT(NVARCHAR, i.[DefenseCounselAddressID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[TenderBasicContactID] = i.[TenderBasicContactID] )
      WHERE
          ( d.[DefenseCounselAddressID] <> i.[DefenseCounselAddressID] ) OR
        ( d.[DefenseCounselAddressID] IS NULL AND i.[DefenseCounselAddressID] IS NOT NULL ) OR
        ( i.[DefenseCounselAddressID] IS NULL AND d.[DefenseCounselAddressID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [PlaintiffCounselAddressID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'TenderBasicContactID=' + CONVERT(VARCHAR, ISNULL( i.[TenderBasicContactID], d.[TenderBasicContactID] )),
        602,
        0,
        CONVERT(NVARCHAR, d.[PlaintiffCounselAddressID]),
        CONVERT(NVARCHAR, i.[PlaintiffCounselAddressID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[TenderBasicContactID] = i.[TenderBasicContactID] )
      WHERE
          ( d.[PlaintiffCounselAddressID] <> i.[PlaintiffCounselAddressID] ) OR
        ( d.[PlaintiffCounselAddressID] IS NULL AND i.[PlaintiffCounselAddressID] IS NOT NULL ) OR
        ( i.[PlaintiffCounselAddressID] IS NULL AND d.[PlaintiffCounselAddressID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [TenderBasicContactID] ) )
    BEGIN
      -- Audit key column updates...
      IF( @ChangeCount > @RowsAffected )
      BEGIN
        INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
        SELECT
          @AuditID,
          'TenderBasicContactID=' + CONVERT(VARCHAR, ISNULL( i.[TenderBasicContactID], d.[TenderBasicContactID] )),
          603,
          0,
          CONVERT(NVARCHAR, d.[TenderBasicContactID]),
          CONVERT(NVARCHAR, i.[TenderBasicContactID])
        FROM
          INSERTED i
          FULL OUTER JOIN DELETED d ON ( d.[TenderBasicContactID] = i.[TenderBasicContactID] )
        WHERE
          ( d.[TenderBasicContactID] <> i.[TenderBasicContactID] ) OR
          ( d.[TenderBasicContactID] IS NULL AND i.[TenderBasicContactID] IS NOT NULL ) OR
          ( i.[TenderBasicContactID] IS NULL AND d.[TenderBasicContactID] IS NOT NULL )
        SELECT @CaptureCount = @CaptureCount + @@RowCount
      END
    END

    IF( UPDATE( [VendorFirmID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'TenderBasicContactID=' + CONVERT(VARCHAR, ISNULL( i.[TenderBasicContactID], d.[TenderBasicContactID] )),
        604,
        0,
        CONVERT(NVARCHAR, d.[VendorFirmID]),
        CONVERT(NVARCHAR, i.[VendorFirmID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[TenderBasicContactID] = i.[TenderBasicContactID] )
      WHERE
          ( d.[VendorFirmID] <> i.[VendorFirmID] ) OR
        ( d.[VendorFirmID] IS NULL AND i.[VendorFirmID] IS NOT NULL ) OR
        ( i.[VendorFirmID] IS NULL AND d.[VendorFirmID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( @CaptureCount > 0 )
    begin
    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 51, @RowsAffected, 'U', @AuditedUser
    end
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblTenderBasicContact_Upd]', 'last', 'update', null
GO
ALTER TABLE [dbo].[tblTenderBasicContact] ADD CONSTRAINT [OneIDLookupPerRow_OthersNull] CHECK (([DefenseCounselAddressID] IS NOT NULL AND [PlaintiffCounselAddressID] IS NULL AND [VendorFirmID] IS NULL AND [DefendantCompanyID] IS NULL AND [BrandID] IS NULL OR [DefenseCounselAddressID] IS NULL AND [PlaintiffCounselAddressID] IS NOT NULL AND [VendorFirmID] IS NULL AND [DefendantCompanyID] IS NULL AND [BrandID] IS NULL OR [DefenseCounselAddressID] IS NULL AND [PlaintiffCounselAddressID] IS NULL AND [VendorFirmID] IS NOT NULL AND [DefendantCompanyID] IS NULL AND [BrandID] IS NULL OR [DefenseCounselAddressID] IS NULL AND [PlaintiffCounselAddressID] IS NULL AND [VendorFirmID] IS NULL AND [DefendantCompanyID] IS NOT NULL AND [BrandID] IS NULL OR [DefenseCounselAddressID] IS NULL AND [PlaintiffCounselAddressID] IS NULL AND [VendorFirmID] IS NULL AND [DefendantCompanyID] IS NULL AND [BrandID] IS NOT NULL))
GO
ALTER TABLE [dbo].[tblTenderBasicContact] ADD CONSTRAINT [PK_tblTenderBasicContact] PRIMARY KEY CLUSTERED  ([TenderBasicContactID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderBasicContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicContact] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblTenderBasicContact] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblTenderBasicContact] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblTenderBasicContact] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblTenderBasicContact] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderBasicContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderBasicContact] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderBasicContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderBasicContact] TO [power_user]
GO
