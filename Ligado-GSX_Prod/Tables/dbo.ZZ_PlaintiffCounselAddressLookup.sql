CREATE TABLE [dbo].[ZZ_PlaintiffCounselAddressLookup]
(
[PlaintiffCounsel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselAddressID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [base_user]
GRANT SELECT ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [power_user]
GRANT SELECT ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [power_user]
GRANT INSERT ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [power_user]
GRANT DELETE ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [power_user]
GRANT UPDATE ON  [dbo].[ZZ_PlaintiffCounselAddressLookup] TO [power_user]
GO
