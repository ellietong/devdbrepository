SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblClaimSettlementDocumentation]
AS
SELECT     csd.ClaimLawsuitID, SUM(CASE WHEN DocCategoryID = 1 THEN 1 ELSE 0 END) AS ClaimantIDed, SUM(CASE WHEN DocCategoryID = 2 THEN 1 ELSE 0 END) 
                      AS DxMedicals, SUM(CASE WHEN DocCategoryID = 3 THEN 1 ELSE 0 END) AS DODCert, SUM(CASE WHEN DocCategoryID = 4 THEN 1 ELSE 0 END) AS EstatePapers, 
                      SUM(CASE WHEN DocCategoryID = 5 THEN 1 ELSE 0 END) AS ProductID, SUM(CASE WHEN DocCategoryID = 6 THEN 1 ELSE 0 END) AS EmployExpoHist, 
                      SUM(CASE WHEN DocCategoryID = 7 THEN 1 ELSE 0 END) AS CompServ, SUM(CASE WHEN DocCategoryID IN (8, 12) THEN 1 ELSE 0 END) 
                      AS SettlementConfirmation, SUM(CASE WHEN DocCategoryID = 9 THEN 1 ELSE 0 END) AS MMSEA, SUM(CASE WHEN DocCategoryID = 10 THEN 1 ELSE 0 END) 
                      AS MedicarePlaintiff, SUM(CASE WHEN DocCategoryID = 11 THEN 1 ELSE 0 END) AS MedicarePlaintiffCounsel, 
                      SUM(CASE WHEN DocCategoryID = 13 THEN 1 ELSE 0 END) AS MedicareFormB, SUM(CASE WHEN DocCategoryID = 14 THEN 1 ELSE 0 END) AS Release, 
                      SUM(CASE WHEN DocCategoryID = 15 THEN 1 ELSE 0 END) AS MedicareReq, SUM(CASE WHEN DocCategoryID = 16 THEN 1 ELSE 0 END) AS SettlementAuth, 
                      SUM(CASE WHEN DocCategoryID = 17 THEN 1 ELSE 0 END) AS RelatedParty, SUM(CASE WHEN DocCategoryID = 18 THEN 1 ELSE 0 END) AS MedicareForms, 
                      SUM(CASE WHEN DocCategoryID = 19 THEN 1 ELSE 0 END) AS CheckRequest, SUM(CASE WHEN DocCategoryID = 20 THEN 1 ELSE 0 END) AS MedicareDocument,
					  SUM(CASE WHEN DocCategoryID = 21 THEN 1 ELSE 0 END) AS PreTrialReport,
					  SUM(CASE WHEN DocCategoryID = 22 THEN 1 ELSE 0 END) AS ExecutedRelease,
					  SUM(CASE WHEN DocCategoryID = 23 THEN 1 ELSE 0 END) AS AllstateMMSEAForm, 
                      cs.ReleaseRqstDate, cs.ReleaseReceivedDate, cs.ReleaseSentDate, rt.ReleaseType, cs.SettlePaymentComments, 1 AS Expr1, cs.SPUsername
FROM         dbo.tblClaimSettlementDocuments AS csd LEFT OUTER JOIN
                      dbo.tblClaimSettlement AS cs ON csd.ClaimLawsuitID = cs.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblReleaseType AS rt ON cs.ReleaseTypeID = rt.ReleaseTypeID
GROUP BY csd.ClaimLawsuitID, cs.ReleaseRqstDate, cs.ReleaseReceivedDate, cs.ReleaseSentDate, rt.ReleaseType, cs.SettlePaymentComments, cs.SPUsername


GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementDocumentation] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocumentation] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimSettlementDocumentation] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimSettlementDocumentation] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimSettlementDocumentation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementDocumentation] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocumentation] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimSettlementDocumentation] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimSettlementDocumentation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimSettlementDocumentation] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "csd"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 6
               Left = 255
               Bottom = 125
               Right = 504
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rt"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 204
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'tblClaimSettlementDocumentation', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'tblClaimSettlementDocumentation', NULL, NULL
GO
