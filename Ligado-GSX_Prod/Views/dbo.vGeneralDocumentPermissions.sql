SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vGeneralDocumentPermissions]
AS
--PersonID-level of permissions
SELECT gdp.PersonID, GoogleObjectID, CanAdd, CanEdit, CanDelete
FROM tblGeneralDocumentGroupMembership gdgm 
INNER JOIN tblGeneralDocumentPermissions gdp ON gdgm.GeneralDocumentGroupID = gdp.GeneralDocumentGroupID
WHERE gdp.PersonID IS NOT NULL

UNION

--PersonID-level of groups
SELECT gdgm.PersonID, GoogleObjectID, CanAdd, CanEdit, CanDelete
FROM tblGeneralDocumentGroupMembership gdgm 
INNER JOIN tblGeneralDocumentPermissions gdp ON gdgm.GeneralDocumentGroupID = gdp.GeneralDocumentGroupID
WHERE gdgm.PersonID IS NOT NULL

UNION

--FirmID level
SELECT p.PersonID, GoogleObjectID, CanAdd, CanEdit, CanDelete
FROM tblGeneralDocumentGroupMembership gdgm 
INNER JOIN tblGeneralDocumentPermissions gdp ON gdgm.GeneralDocumentGroupID = gdp.GeneralDocumentGroupID
INNER JOIN tblDefenseCounselAddress f ON gdgm.FirmID = f.DefenseCounselID
INNER JOIN vtblPerson p ON f.DefenseCounselID = p.FirmID

UNION

--FirmAddressID level
SELECT p.PersonID, GoogleObjectID, CanAdd, CanEdit, CanDelete
FROM tblGeneralDocumentGroupMembership gdgm 
INNER JOIN tblGeneralDocumentPermissions gdp ON gdgm.GeneralDocumentGroupID = gdp.GeneralDocumentGroupID
INNER JOIN tblDefenseCounselAddress f ON gdgm.FirmAddressID = f.DefenseCounselAddressID
INNER JOIN vtblPerson p ON f.DefenseCounselAddressID = p.FirmAddressID

UNION

--KCIC
SELECT 0, GoogleObjectID, 1, 1, 1
FROM tblGeneralDocumentPermissions gdp
GROUP BY gdp.GoogleObjectID

GO
GRANT VIEW DEFINITION ON  [dbo].[vGeneralDocumentPermissions] TO [base_user]
GRANT SELECT ON  [dbo].[vGeneralDocumentPermissions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vGeneralDocumentPermissions] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vGeneralDocumentPermissions] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vGeneralDocumentPermissions] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vGeneralDocumentPermissions] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vGeneralDocumentPermissions] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vGeneralDocumentPermissions] TO [power_user]
GRANT SELECT ON  [dbo].[vGeneralDocumentPermissions] TO [power_user]
GRANT INSERT ON  [dbo].[vGeneralDocumentPermissions] TO [power_user]
GRANT DELETE ON  [dbo].[vGeneralDocumentPermissions] TO [power_user]
GRANT UPDATE ON  [dbo].[vGeneralDocumentPermissions] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vGeneralDocumentPermissions', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vGeneralDocumentPermissions', NULL, NULL
GO
