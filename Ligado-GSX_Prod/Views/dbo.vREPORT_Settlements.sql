SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[vREPORT_Settlements]
AS
SELECT  DISTINCT      COALESCE (hld.HighLevelDisease, 'Unknown') AS HighLevelDisease, 
cl.ClaimLawsuitID, 
COALESCE(cs.SettlementAmount, 0) AS IndemnityPaid, 
COALESCE(cs.SettlementAmount, 0) AS AvgIndemnity,
pca.PlaintiffCounsel, 
s.FullNameState AS State, 
clst.ProcessedDate,
clst.StatusDate, 
cs.SettlementDate, 
c.MatterID,
CASE WHEN clst.StatusDate IS NULL THEN 'UNKNOWN' ELSE DATENAME("mm", coalesce(clst.StatusDate,'1/1/1900'))+' '+convert(nvarchar,year(coalesce(clst.StatusDate,'1/1/1900'))) END AS [Month],
cl.DefenseCounselAddressID,
dj.DefenseCounselAddressID AS DJDefenseCounselAddressID,
CONVERT(nvarchar, YEAR(COALESCE(clst.StatusDate, '1/1/1900'))) + RIGHT('0' + CONVERT(nvarchar, MONTH(coalesce(clst.StatusDate,'1/1/1900'))),2) AS PeriodSort,
ClaimLawsuitTypeID

FROM            dbo.tblClaim AS c INNER JOIN
                         dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                         dbo.tblClaimLawsuitStatusTracking AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID INNER JOIN
                         dbo.tblClaimDisease AS cd ON c.ClaimID = cd.ClaimID INNER JOIN
                         dbo.tblClaimSettlement AS cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblDisease AS d ON d.DiseaseID = cd.DiseaseID LEFT OUTER JOIN
                         dbo.tblDisease_ALT AS da ON cd.DiseaseID = da.DiseaseID_Alt LEFT OUTER JOIN
                         dbo.tblHighLevelDisease AS hld ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID INNER JOIN
                         dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                         dbo.tblState AS s ON l.StateID = s.StateID LEFT OUTER JOIN
                         dbo.tblPlaintiffCounselAddress AS pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
						 INNER JOIN		tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.MatterID
WHERE        (clst.ClaimLawsuitStatusID IN (23, 25, 39)) 
AND (cl.IsPrimaryForClaim = 1) 
AND (clst.IsPrimaryStatus = 1) 
AND (cd.PrimaryDisease = 1)






GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_Settlements] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_Settlements] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_Settlements] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_Settlements] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_Settlements] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_Settlements] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_Settlements] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_Settlements] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_Settlements] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_Settlements] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_Settlements] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_Settlements] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 286
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clst"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 303
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cd"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 257
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 664
               Right = 298
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 666
               Left = 38
               Bottom = 796
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "da"
            Begin Extent = 
               Top = 798
               Left = 38
               Bottom = 928
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         ', 'SCHEMA', N'dbo', 'VIEW', N'vREPORT_Settlements', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'End
         Begin Table = "hld"
            Begin Extent = 
               Top = 930
               Left = 38
               Bottom = 1026
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 1026
               Left = 38
               Bottom = 1156
               Right = 293
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 666
               Left = 284
               Bottom = 779
               Right = 470
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pca"
            Begin Extent = 
               Top = 1158
               Left = 38
               Bottom = 1288
               Right = 283
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vREPORT_Settlements', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vREPORT_Settlements', NULL, NULL
GO
