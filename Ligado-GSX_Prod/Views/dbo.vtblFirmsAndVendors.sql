SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vtblFirmsAndVendors]
AS
SELECT     firmid, firm
FROM         vtblFirm
UNION
SELECT     vendorfirmid, vendorfirm
FROM         tblVendorFirm

GO
GRANT VIEW DEFINITION ON  [dbo].[vtblFirmsAndVendors] TO [base_user]
GRANT SELECT ON  [dbo].[vtblFirmsAndVendors] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vtblFirmsAndVendors] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vtblFirmsAndVendors] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vtblFirmsAndVendors] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vtblFirmsAndVendors] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vtblFirmsAndVendors] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vtblFirmsAndVendors] TO [power_user]
GRANT SELECT ON  [dbo].[vtblFirmsAndVendors] TO [power_user]
GRANT INSERT ON  [dbo].[vtblFirmsAndVendors] TO [power_user]
GRANT DELETE ON  [dbo].[vtblFirmsAndVendors] TO [power_user]
GRANT UPDATE ON  [dbo].[vtblFirmsAndVendors] TO [power_user]
GO
