SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [dbo].[vMattersAndAliasesToMatterIDs] as
select Matter as Name, MatterID from tblMatter
                union 
            select MatterAlias as Name, MatterID from tblMatterAliases

GO
GRANT VIEW DEFINITION ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [base_user]
GRANT SELECT ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [power_user]
GRANT SELECT ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [power_user]
GRANT INSERT ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [power_user]
GRANT DELETE ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [power_user]
GRANT UPDATE ON  [dbo].[vMattersAndAliasesToMatterIDs] TO [power_user]
GO
