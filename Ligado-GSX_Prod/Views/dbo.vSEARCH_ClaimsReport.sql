
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vSEARCH_ClaimsReport]
AS
  SELECT MainQuery.ClaimLawsuitID
         , MainQuery.ClaimantPersonalInfoID
         , MainQuery.ActiveTrialDate
         , MainQuery.ClaimID
         , MainQuery.ClaimLawsuitStatus
         , MainQuery.Matter
         , MainQuery.ClaimSequence
         , MainQuery.LastName + ', '
           + MainQuery.FirstName                                                        AS FullName
         , MainQuery.BirthDate
         , MainQuery.DeceasedDate
         , ( CASE
               WHEN ssn4digit IS NOT NULL THEN 'XXX-XX-' + ssn4digit
               ELSE NULL
             END )                                                                      AS SocialSecurityNumber
         , MainQuery.Occupation
         , MainQuery.HighLevelDisease
         , MainQuery.DocketNumber
         , MainQuery.ServiceDate
         , COALESCE (MainQuery.FileDate, MainQuery.ReceivedDate)                        AS FileDate
         , MainQuery.State
         , MainQuery.Jurisdiction
         , MainQuery.ProcessedDate
         , MainQuery.StatusDate
         , MainQuery.ProductNotes
         , MainQuery.Startdate                                                          AS ProductExposureStart
         , MainQuery.SettlementDate
		 , MainQuery.SettlementPaidDate
         , MainQuery.SettlementAmount
         , MainQuery.sourcedate
         , MainQuery.PlaintiffCounsel
         , MainQuery.ClaimLawsuitStatusID
         , MainQuery.StateID
         , MainQuery.JurisdictionID
         , MainQuery.IsMDL
         , MainQuery.HighLevelDiseaseID
         , MainQuery.allegationtypeid
         , MainQuery.PlaintiffCounselID
         , MainQuery.MatterID
         , MainQuery.DefenseCounselAddressID                                            AS DefenseCounselAddressID
         , defCounselJurisdictionInfo.defCounselJurisdictionInfoDefenseCounselAddressID AS DefenseCounselAddressID2
         , CONVERT(INT, NULL)                                                           AS PersonID
         , CONVERT(INT, NULL)                                                           AS BrandID
         , CONVERT(NVARCHAR(50), NULL)                                                  AS Brand
         , CONVERT(INT, NULL)                                                           AS BankruptcyStayID
         , CONVERT(NVARCHAR(50), NULL)                                                  AS BankruptcyStay
         , CONVERT(INT, NULL)                                                           AS ClaimLawsuitTypeID
         , CONVERT(NVARCHAR(50), NULL)                                                  AS ClaimLawsuitType
         , CONVERT(NVARCHAR(100), NULL)                                                 AS JobsiteCity
         , CONVERT(INT, NULL)                                                           AS JobsiteStateID
         , CONVERT(NVARCHAR(1000), NULL)                                                AS Jobsite
         , CONVERT(INT, NULL)                                                           AS BusinessCaseID
         , CONVERT(NVARCHAR(50), NULL)                                                  AS BusinessCase
		 , MainQuery.ClaimLawsuitNotes
		 , MainQuery.TrialNotes -- FYI TrialNotes is literally the same as ClaimLawsuitNotes; it is here so that GSX can refer to it this way in their CSV exports.
		 , COALESCE(IsSecondaryExposure,'')			AS 			    IsSecondaryExposure
  FROM   (SELECT LawsuitLevelInfo.ClaimLawsuitID
                 , LawsuitLevelInfo.ActiveTrialDate
                 , LawsuitLevelInfo.ClaimID
                 , LawsuitLevelInfo.LawsuitID
                 , LawsuitLevelInfo.IsMDL
                 , LawsuitLevelInfo.ClaimLawsuitStatus
                 , LawsuitLevelInfo.ClaimLawsuitStatusID
                 , ClaimLevelInfo.Matter
                 , ClaimLevelInfo.ClaimSequence
                 , ClaimLevelInfo.ClaimantPersonalInfoID
                 , ClaimantLevelInfo.FirstName
                 , ClaimantLevelInfo.LastName
                 , ClaimantLevelInfo.BirthDate
                 , ClaimantLevelInfo.DeceasedDate
                 , ClaimantLevelInfo.SSN4Digit
                 , LawsuitLevelInfo.Occupation
                 , ClaimLevelInfo.HighLevelDisease
                 , LawsuitLevelInfo.DocketNumber
                 , LawsuitLevelInfo.ServiceDate
                 , LawsuitLevelInfo.FileDate
                 , LawsuitLevelInfo.ReceivedDate
                 , LawsuitLevelInfo.State
                 , LawsuitLevelInfo.Jurisdiction
                 , LawsuitLevelInfo.ProcessedDate
                 , LawsuitLevelInfo.StatusDate
                 , LawsuitLevelInfo.ProductNotes
                 , LawsuitLevelInfo.Startdate
                 , LawsuitLevelInfo.SettlementDate
				 , LawsuitLevelInfo.SettlementPaidDate
                 , LawsuitLevelInfo.SettlementAmount
                 , LawsuitLevelInfo.sourcedate
                 , LawsuitLevelInfo.PlaintiffCounsel
                 , LawsuitLevelInfo.StateID
                 , LawsuitLevelInfo.JurisdictionID
                 , ClaimLevelInfo.HighLevelDiseaseID
                 , LawsuitLevelInfo.PlaintiffCounselID
                 , ClaimLevelInfo.MatterID
                 , LawsuitLevelInfo.DefenseCounselAddressID
                 , LawsuitLevelInfo.allegationtypeid
				 , LawsuitLevelinfo.ClaimLawsuitNotes
				 , LawsuitLevelInfo.TrialNotes
				 , LawsuitLevelInfo.IsSecondaryExposure
          FROM   (SELECT CLInfo.ClaimLawsuitID
                         , CLInfo.ActiveTrialDate
                         , CLInfo.ClaimID
                         , CLInfo.LawsuitID
                         , CLInfo.IsMDL
                         , lawsuitstatusinfo.ClaimLawsuitStatus
                         , lawsuitstatusinfo.ClaimLawsuitStatusID
                         , lawsuitInfo.DocketNumber
                         , AllegationTypeInfo.allegationServiceDate AS ServiceDate
                         , lawsuitInfo.FileDate
                         , lawsuitInfo.ReceivedDate
                         , lawsuitInfo.State
                         , lawsuitInfo.StateID
                         , lawsuitInfo.Jurisdiction
                         , lawsuitInfo.JurisdictionID
                         , lawsuitstatusinfo.ProcessedDate
                         , lawsuitstatusinfo.StatusDate
                         , CLInfo.SettlementDate
						 , CLInfo.SETTLEMENTPAIDDATE
                         , CLInfo.SettlementAmount
                         , CLInfo.sourcedate
                         , lawsuitInfo.PlaintiffCounsel
                         , lawsuitInfo.PlaintiffCounselID
                         , CLInfo.DefenseCounselAddressID
                         , AllegationTypeInfo.allegationtypeid
                         , claimantEmployment.Occupation
                         , prodInfo.ProductNotes
                         , prodInfo.Startdate
						 , CLINFO.ClaimLawsuitNotes
						 , CLINFO.TrialNotes
						 ,claimantEmployment.IsSecondaryExposure
                  FROM   (SELECT dbo.TBLCLAIMLAWSUIT.ClaimLawsuitID
                                 , dbo.TBLCLAIMLAWSUIT.ActiveTrialDate
                                 , dbo.TBLCLAIMLAWSUIT.ClaimID
                                 , dbo.TBLCLAIMLAWSUIT.LawsuitID
                                 , dbo.TBLCLAIMLAWSUIT.IsMDL
                                 , settlementInfo.SettlementDate
								 , settlementInfo.SETTLEMENTPAIDDATE
                                 , settlementInfo.SettlementAmount
                                 , settlementInfo.sourcedate
                                 , dbo.TBLCLAIMLAWSUIT.DefenseCounselAddressID
								 , dbo.tblClaimLawsuit.ClaimLawsuitNotes
								 , dbo.tblClaimLawsuit.ClaimLawsuitNotes AS TrialNotes
                          FROM   dbo.TBLCLAIMLAWSUIT
                                 LEFT OUTER JOIN (SELECT CLAIMLAWSUITID      AS settlmentInfoClaimLawsuitid
                                                       , SETTLEMENTDATE
													   , SETTLEMENTPAIDDATE
                                                       , SETTLEMENTAMOUNT
                                                       , MATCHEDSOURCEDATE AS sourcedate
                                                FROM   (SELECT CLAIMSETTLEMENTID
                                                               , EARLIESTCHECKID
                                                               , CLAIMLAWSUITID
                                                               , SETTLEMENTDATE
															   , PAIDDATE AS SETTLEMENTPAIDDATE
                                                               , SETTLEMENTAMOUNT
                                                        FROM   TBLCLAIMSETTLEMENT
                                                               LEFT OUTER JOIN (SELECT CLAIMSETTLEMENTID AS SettlementPaymentInfoClaimSettlementID
                                                                                       , MIN(CHECKID)    AS EarliestCheckID
                                                                                FROM   TBLPAYMENTSETTLEMENT
                                                                                       LEFT OUTER JOIN (SELECT CHECKID AS checkInfoCheckID
                                                                                                        FROM   TBLCHECK) checkInfo
                                                                                                    ON ( TBLPAYMENTSETTLEMENT.CHECKID = checkInfo.CHECKINFOCHECKID )
                                                                                GROUP  BY CLAIMSETTLEMENTID) settlementPaymentInfo
                                                                            ON ( TBLCLAIMSETTLEMENT.CLAIMSETTLEMENTID = settlementPaymentInfo.SETTLEMENTPAYMENTINFOCLAIMSETTLEMENTID ))settlementAndCheckInfo
                                                  LEFT OUTER JOIN (SELECT CHECKID      AS matchedCheckID
                                                                          , SOURCEDATE AS MatchedSourceDate
                                                                   FROM   TBLCHECK)additionalCheckInfo
                                                               ON ( settlementAndCheckInfo.EARLIESTCHECKID = additionalCheckInfo.MATCHEDCHECKID )) AS settlementInfo
                                              ON dbo.TBLCLAIMLAWSUIT.ClaimLawsuitID = settlementInfo.settlmentInfoClaimLawsuitid) AS CLInfo
                         LEFT OUTER JOIN (SELECT ClaimLawsuitID          AS allegationTypeClaimLawsuitID
                                                 , MIN(AllegationTypeID) AS allegationtypeid
                                                 , MIN(ServiceDate)      AS allegationServiceDate
                                          FROM   dbo.TBLALLEGATION
                                          GROUP  BY ClaimLawsuitID) AS AllegationTypeInfo
                                      ON CLInfo.ClaimLawsuitID = AllegationTypeInfo.allegationTypeClaimLawsuitID
                         LEFT OUTER JOIN (SELECT empInfo.EmploymentClaimLawsuitID
                                                 , occInfo.Occupation
												 ,empInfo.IsSecondaryExposure
                                          FROM   (SELECT ClaimLawsuitID AS EmploymentClaimLawsuitID
                                                         , OccupationID
                                                         , ClaimLawsuitEmploymentID
														  ,IsSecondaryExposure
                                                  FROM   dbo.TBLCLAIMLAWSUITEMPLOYMENT
                                                  WHERE  ( IsPrimary = 1 )) AS empInfo
                                                 LEFT OUTER JOIN (SELECT OccupationID
                                                                         , Occupation
                                                                  FROM   dbo.TBLOCCUPATION) AS occInfo
                                                              ON empInfo.OccupationID = occInfo.OccupationID) AS claimantEmployment
                                      ON CLInfo.ClaimLawsuitID = claimantEmployment.EmploymentClaimLawsuitID
                         LEFT OUTER JOIN (SELECT MinDateQuery.ClaimLawsuitID AS ProductClaimLawsuitID
                                                 , MinDateQuery.MinStartDate AS Startdate
                                                 , prodnotesquery.ProductNotes
                                          FROM   (SELECT distinctProducts.ClaimLawsuitID
                                                         , MinStartDatesForProducts.MinStartDate
                                                  FROM   (SELECT DISTINCT ClaimLawsuitID
                                                          FROM   dbo.TBLCLAIMLAWSUITPRODUCT) AS distinctProducts
                                                         LEFT OUTER JOIN (SELECT ClaimLawsuitID   AS minQueryClaimLawsuitID
                                                                                 , MIN(StartDate) AS MinStartDate
                                                                          FROM   dbo.TBLCLAIMLAWSUITPRODUCT AS tblClaimLawsuitProduct_1
                                                                          GROUP  BY ClaimLawsuitID) AS MinStartDatesForProducts
                                                                      ON distinctProducts.ClaimLawsuitID = MinStartDatesForProducts.minQueryClaimLawsuitID) AS MinDateQuery
                                                 LEFT OUTER JOIN (SELECT ClaimLawsuitID AS prodnotesqueryClaimLawsuitid
                                                                         , ProductID          AS prodnotesqueryProductID
                                                                         , StartDate          AS prodnotesqueryStartDate
                                                                         , ProductNotes
                                                                  FROM   dbo.TBLCLAIMLAWSUITPRODUCT) AS prodnotesquery
                                                              ON MinDateQuery.ClaimLawsuitID = prodnotesquery.prodnotesqueryClaimLawsuitid
                                                                 AND MinDateQuery.MinStartDate = prodnotesquery.prodnotesqueryStartDate) AS prodInfo
                                      ON CLInfo.ClaimLawsuitID = prodInfo.ProductClaimLawsuitID
                         INNER JOIN (SELECT trackingInfo.ClaimLawsuitStatusID
                                            , trackingInfo.ClaimLawsuitID AS ClaimLawsuitID_fromTrackingInfo
                                            , statusLabelnfo.ClaimLawsuitStatus
                                            , trackingInfo.ProcessedDate
                                            , trackingInfo.StatusDate
                                     FROM   (SELECT ClaimLawsuitStatusID
                                                    , ClaimLawsuitID
                                                    , ProcessedDate
                                                    , StatusDate
                                             FROM   dbo.TBLCLAIMLAWSUITSTATUSTRACKING
                                             WHERE  ( IsPrimaryStatus = 1 )) AS trackingInfo
                                            INNER JOIN (SELECT ClaimLawsuitStatusID
                                                               , ClaimLawsuitStatus
                                                        FROM   dbo.TBLCLAIMLAWSUITSTATUS) AS statusLabelnfo
                                                    ON trackingInfo.ClaimLawsuitStatusID = statusLabelnfo.ClaimLawsuitStatusID) AS lawsuitstatusinfo
                                 ON CLInfo.ClaimLawsuitID = lawsuitstatusinfo.ClaimLawsuitID_fromTrackingInfo
                         INNER JOIN (SELECT dbo.TBLLAWSUIT.LawsuitID                 AS lawsuitinfolawsuitid
                                            , dbo.TBLLAWSUIT.DocketNumber
                                            , dbo.TBLLAWSUIT.ServiceDate
                                            , dbo.TBLLAWSUIT.FileDate
                                            , dbo.TBLLAWSUIT.ReceivedDate
                                            , stateInfo.State
                                            , dbo.TBLLAWSUIT.StateID
                                            , jurisdictionInfo.Jurisdiction
                                            , dbo.TBLLAWSUIT.JurisdictionID
                                            , plaintiffCounselInfo.PlaintiffCounsel
                                            , plaintiffCounselInfo.PlaintiffCounselID
                                            , defensecounselinfo.DefenseCounsel      AS LawsuitDefenseCounsel
                                            , dbo.TBLLAWSUIT.DefenseCounselAddressID AS LawsuitDefenseCounselAddressID
                                            , defensecounselinfo.DefenseCounselID    AS LawsuitDefenseCounselID
                                     FROM   dbo.TBLLAWSUIT
                                            LEFT OUTER JOIN (SELECT StateID AS stateInfoStateID
                                                                    , State
                                                             FROM   dbo.TBLSTATE) AS stateInfo
                                                         ON dbo.TBLLAWSUIT.StateID = stateInfo.stateInfoStateID
                                            LEFT OUTER JOIN (SELECT JurisdictionID AS jurisdictioninfoInfojurisdictionID
                                                                    , Jurisdiction
                                                             FROM   dbo.TBLJURISDICTION) AS jurisdictionInfo
                                                         ON dbo.TBLLAWSUIT.JurisdictionID = jurisdictionInfo.jurisdictioninfoInfojurisdictionID
                                            LEFT OUTER JOIN (SELECT PlaintiffCounselAddressID AS plaintiffCounselInfoAddressId
                                                                    , PlaintiffCounsel
                                                                    , PlaintiffCounselID
                                                             FROM   dbo.TBLPLAINTIFFCOUNSELADDRESS) AS plaintiffCounselInfo
                                                         ON dbo.TBLLAWSUIT.PlaintiffCounselAddressID = plaintiffCounselInfo.plaintiffCounselInfoAddressId
                                            LEFT OUTER JOIN (SELECT DefenseCounselAddressID AS defenseCounselInfoAddressID
                                                                    , DefenseCounsel
                                                                    , DefenseCounselID
                                                             FROM   dbo.TBLDEFENSECOUNSELADDRESS) AS defensecounselinfo
                                                         ON dbo.TBLLAWSUIT.DefenseCounselAddressID = defensecounselinfo.defenseCounselInfoAddressID) AS lawsuitInfo
                                 ON CLInfo.LawsuitID = lawsuitInfo.lawsuitinfolawsuitid) AS LawsuitLevelInfo
                 INNER JOIN (SELECT ClaimInfo.claimID_fromtblClaim
                                    , ClaimInfo.ClaimantPersonalInfoID
                                    , matterInfo.Matter
                                    , ClaimInfo.MatterID
                                    , ClaimSequenceInfo.ClaimSequence
                                    , ClaimDiseaseInfo.HighLevelDisease
                                    , ClaimDiseaseInfo.HighLevelDiseaseID

                             FROM   (SELECT ClaimID AS claimID_fromtblClaim
                                            , ClaimantPersonalInfoID
                                            , MatterID
                                            , ClaimSequenceID
                                     FROM   dbo.TBLCLAIM) AS ClaimInfo
                                    LEFT OUTER JOIN (SELECT MatterID AS matterID_fromtblMatter
                                                            , Matter
                                                     FROM   dbo.TBLMATTER) AS matterInfo
                                                 ON ClaimInfo.MatterID = matterInfo.matterID_fromtblMatter
                                    LEFT OUTER JOIN (SELECT ClaimSequenceID
                                                            , ClaimSequence
                                                     FROM   dbo.TBLCLAIMSEQUENCE) AS ClaimSequenceInfo
                                                 ON ClaimInfo.ClaimSequenceID = ClaimSequenceInfo.ClaimSequenceID
                                    LEFT OUTER JOIN (SELECT theClaimDiseaseInfo.ClaimID
                                                            , hlDiseaseInfo.HighLevelDisease
                                                            , diseaseInfo.HighLevelDiseaseID
                                                     FROM   (SELECT ClaimID
                                                                    , DiseaseID
                                                                    , PrimaryDisease
                                                             FROM   dbo.TBLCLAIMDISEASE) AS theClaimDiseaseInfo
                                                            LEFT OUTER JOIN (SELECT DiseaseID AS diseaseInfodiseaseid
                                                                                    , HighLevelDiseaseID
                                                                             FROM   dbo.TBLDISEASE) AS diseaseInfo
                                                                         ON theClaimDiseaseInfo.DiseaseID = diseaseInfo.diseaseInfodiseaseid
                                                            LEFT OUTER JOIN (SELECT HighLevelDiseaseID AS hldiseaseinfohldiseaseid
                                                                                    , HighLevelDisease
                                                                             FROM   dbo.TBLHIGHLEVELDISEASE) AS hlDiseaseInfo
                                                                         ON diseaseInfo.HighLevelDiseaseID = hlDiseaseInfo.hldiseaseinfohldiseaseid
                                                     WHERE  ( theClaimDiseaseInfo.PrimaryDisease = 1 )) AS ClaimDiseaseInfo
                                                 ON ClaimInfo.claimID_fromtblClaim = ClaimDiseaseInfo.ClaimID) AS ClaimLevelInfo
                         ON LawsuitLevelInfo.ClaimID = ClaimLevelInfo.claimID_fromtblClaim
                 INNER JOIN (SELECT ClaimInfo_1.ClaimID                  AS claimID_forclaimantinfo
                                    , ClaimInfo_1.ClaimantPersonalInfoID AS personalinfoid
                                    , ClaimantInfo.FirstName
                                    , ClaimantInfo.LastName
                                    , ClaimantInfo.BirthDate
                                    , ClaimantInfo.DeceasedDate
                                    , ClaimantInfo.SSN4Digit
                             FROM   (SELECT ClaimID
                                            , ClaimantPersonalInfoID
                                     FROM   dbo.TBLCLAIM AS tblClaim_1) AS ClaimInfo_1
                                    LEFT OUTER JOIN (SELECT ClaimantPersonalInfoID
                                                            , FirstName
                                                            , LastName
                                                            , BirthDate
                                                            , DeceasedDate
                                                            , RIGHT(SocialSecurityNumber, 4) AS SSN4Digit
                                                     FROM   dbo.TBLCLAIMANTPERSONALINFO) AS ClaimantInfo
                                                 ON ClaimInfo_1.ClaimantPersonalInfoID = ClaimantInfo.ClaimantPersonalInfoID) AS ClaimantLevelInfo
                         ON LawsuitLevelInfo.ClaimID = ClaimantLevelInfo.claimID_forclaimantinfo) AS MainQuery
         LEFT OUTER JOIN (SELECT DefenseCounselAddressID AS defCounselJurisdictionInfoDefenseCounselAddressID
                                 , StateID               AS defCounselJurisdictionInfoStateID
                                 , JurisdictionID        AS defCounselJurisdictionInfoJurisdictionId
                                 , MatterID              AS defCounselJurisdictionInfoMatterId
                          FROM   dbo.TBLDEFENSEJURISDICTION) AS defCounselJurisdictionInfo
                      ON MainQuery.StateID = defCounselJurisdictionInfo.defCounselJurisdictionInfoStateID
                         AND MainQuery.JurisdictionID = defCounselJurisdictionInfo.defCounselJurisdictionInfoJurisdictionId
                         AND MainQuery.MatterID = defCounselJurisdictionInfo.defCounselJurisdictionInfoMatterId
  GROUP  BY MainQuery.ClaimLawsuitID
            , MainQuery.ClaimantPersonalInfoID
            , MainQuery.ClaimID
            , MainQuery.IsMDL
            , MainQuery.ActiveTrialDate
            , MainQuery.ClaimLawsuitStatus
            , MainQuery.Matter
            , MainQuery.ClaimSequence
            , MainQuery.FirstName
            , MainQuery.LastName
            , MainQuery.BirthDate
            , MainQuery.DeceasedDate
            , MainQuery.SSN4Digit
            , MainQuery.Occupation
            , MainQuery.HighLevelDisease
            , MainQuery.ActiveTrialDate
            , MainQuery.DocketNumber
            , MainQuery.ServiceDate
            , MainQuery.FileDate
            , MainQuery.ReceivedDate
            , MainQuery.State
            , MainQuery.Jurisdiction
            , MainQuery.ProcessedDate
            , MainQuery.StatusDate
            , MainQuery.ProductNotes
            , MainQuery.Startdate
            , MainQuery.SettlementDate
			, MainQuery.SettlementPaidDate
            , MainQuery.SettlementAmount
            , MainQuery.sourcedate
            , MainQuery.PlaintiffCounsel
            , MainQuery.ClaimLawsuitStatusID
            , MainQuery.StateID
            , MainQuery.JurisdictionID
            , MainQuery.HighLevelDiseaseID
            , MainQuery.PlaintiffCounselID
            , MainQuery.MatterID
            , MainQuery.allegationtypeid
            , MainQuery.DefenseCounselAddressID
            , defCounselJurisdictionInfo.defCounselJurisdictionInfoDefenseCounselAddressID
			, MainQuery.ClaimLawsuitNotes
			, MainQuery.TrialNotes
			,IsSecondaryExposure



GO

GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_ClaimsReport] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_ClaimsReport] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_ClaimsReport] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_ClaimsReport] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_ClaimsReport] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_ClaimsReport] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_ClaimsReport] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_ClaimsReport] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MainQuery"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 262
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "defCounselJurisdictionInfo"
            Begin Extent = 
               Top = 6
               Left = 300
               Bottom = 99
               Right = 651
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimsReport', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_ClaimsReport', NULL, NULL
GO
