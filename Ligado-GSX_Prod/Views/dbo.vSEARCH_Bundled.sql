SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vSEARCH_Bundled]
AS
SELECT DISTINCT 
                      ip.Injured_Party_Id AS InjuredPartyID, c.Claim_Sequence_Nbr AS ClaimSequence, ip.Last_Name + ', ' + CASE WHEN ip.First_Name IS NULL 
                      THEN '' ELSE ip.First_Name + ' ' END AS ClaimantName, CONVERT(nvarchar, ip.SS_Nbr) AS SSN, c.[Diagnosis Desc] AS Diagnosis, 
                      c.Claim_Status_Cd AS ClaimStatus, c.[Release_Type Desc] AS ReleaseType, c.Release_Received_Date AS ReleaseRcvdDate, c.Settlement_Date AS SettlementDate, 
                      l.StateFiled AS State, l.JurisdictionDescription AS Jurisdiction, l.DocketNum AS DocketNumber, l.FileDate, c.Settlement_Amt AS SettlementAmount, SUM(bh.DNA_Share) 
                      AS BilledAmount
FROM         dbo.tblBundledInjuredParty AS ip INNER JOIN
                      dbo.tblBundledClaim AS c ON ip.Injured_Party_Id = c.Injured_Party_Id INNER JOIN
                      dbo.tblBundledLawsuit AS l ON c.Injured_Party_Id = l.InjuredPartyID AND c.Claim_Sequence_Nbr = l.ClaimSequenceNum LEFT OUTER JOIN
                      dbo.tblBundledBilledHistory AS bh ON bh.Injured_Party_Id = c.Injured_Party_Id AND bh.[Claim Sequence_Nbr] = c.Claim_Sequence_Nbr
WHERE     (l.LeadLawsuitFlag = 'y')
GROUP BY ip.Injured_Party_Id, c.Claim_Sequence_Nbr, ip.Last_Name, ip.First_Name, ip.SS_Nbr, c.[Diagnosis Desc], c.Claim_Status_Cd, c.[Release_Type Desc], 
                      c.Release_Received_Date, c.Settlement_Date, l.StateFiled, l.JurisdictionDescription, l.DocketNum, l.FileDate, c.Settlement_Amt

GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Bundled] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_Bundled] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Bundled] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_Bundled] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_Bundled] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_Bundled] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_Bundled] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Bundled] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_Bundled] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_Bundled] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_Bundled] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_Bundled] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ip"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 365
               Right = 291
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "bh"
            Begin Extent = 
               Top = 6
               Left = 264
               Bottom = 125
               Right = 467
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 6930
         Alias = 1245
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Bundled', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Bundled', NULL, NULL
GO
