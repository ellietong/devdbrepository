SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vDETAILS_ClaimantDocument]
AS
WITH Locked(DocumentID, Locked) AS (SELECT     mcd.DocumentID, COUNT(c.ClaimID) AS Expr1
                                                                                     FROM         dbo.tblClaim AS c INNER JOIN
                                                                                                            dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                                                                                                            dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID INNER JOIN
                                                                                                            dbo.tblMultiClaimantDocument AS mcd ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID LEFT OUTER JOIN
                                                                                                            dbo.tblClaimLawsuitStatusTracking AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
                                                                                     WHERE     (COALESCE (c.IsCurrentClaim, 0) = 1) AND (COALESCE (cl.IsPrimaryForClaim, 0) = 1) AND (clst.ClaimLawsuitStatusID IN (15, 16, 
                                                                                                            17))
                                                                                     GROUP BY mcd.DocumentID
                                                                                     UNION
                                                                                     SELECT     mcd.DocumentID, COUNT(DISTINCT ps.PaymentSettlementID) AS Expr1
                                                                                     FROM         dbo.tblPaymentSettlement AS ps LEFT OUTER JOIN
                                                                                                           dbo.tblClaimSettlement AS cs ON ps.ClaimSettlementID = cs.ClaimSettlementID LEFT OUTER JOIN
                                                                                                           dbo.tblClaimLawsuit AS cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                                                                                                           dbo.tblClaim AS c ON cl.ClaimID = c.ClaimID INNER JOIN
                                                                                                           dbo.tblMultiClaimantDocument AS mcd ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
                                                                                     GROUP BY mcd.DocumentID, c.ClaimantPersonalInfoID)
    SELECT DISTINCT     mcd.ClaimantPersonalInfoID, d.DocumentID, d.ClaimantDocumentTypeID, d.DateAccepted, d.AcceptedBy, d.DocumentTitle, d.DocumentURL, d.AmazonKey, 
                            dt.ClaimantDocumentType, d.DocumentSourceID, d.MatterID, d.Comments, mcd.SPUsername, COALESCE (s.Locked, 0) AS Locked, d.CreatedBy, d.FileSizeBytes, 
                            d.DateCreated, 
							CASE WHEN mcd.ClaimLawsuitID IS NULL THEN 'All Matter Access' ELSE Matter END AS Matter, mcd.ClaimLawsuitID,
							cl.DefenseCounselAddressID, dj.DefenseCounselAddressID AS DJDefenseCounselAddressID
    FROM			dbo.tblDocuments AS d 
	INNER JOIN		dbo.tblMultiClaimantDocument AS mcd ON d.DocumentID = mcd.DocumentID 
	INNER JOIN		dbo.tblClaimantPersonalInfo AS cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID 
	INNER JOIN		dbo.tblClaim AS c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID 
	INNER JOIN		dbo.tblMatter AS m ON c.MatterID = m.MatterID 
	INNER JOIN		dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID AND cl.ClaimLawsuitID = COALESCE(mcd.ClaimLawsuitID, cl.ClaimLawsuitID) 
	INNER JOIN      dbo.tblClaimantDocumentType AS dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID 
	LEFT OUTER JOIN Locked AS s ON mcd.DocumentID = s.DocumentID
	--New tables for filtering purposes
	INNER JOIN		tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN		tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.MatterID
	WHERE COALESCE(UploadCompleted, 0) = 1 OR CreatedBy = 'Historical'  OR d.ComplaintID IS NOT NULL 



GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantDocument] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantDocument] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantDocument] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantDocument] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantDocument] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantDocument] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantDocument] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantDocument] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantDocument] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantDocument] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantDocument] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mcd"
            Begin Extent = 
               Top = 6
               Left = 306
               Bottom = 135
               Right = 538
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dt"
            Begin Extent = 
               Top = 6
               Left = 576
               Bottom = 95
               Right = 789
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 216
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 2055
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantDocument', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantDocument', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantDocument', NULL, NULL
GO
