SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vClaimLawsuitStatusesInUse] As

SELECT ClaimLawsuitStatusID, UPPER(ClaimLawsuitStatus) AS ClaimLawsuitStatus 
	FROM (select ClaimlawsuitstatusID, ClaimLawsuitStatus 
				from tblClaimLawsuitStatus 
				where ClaimLawsuitStatusID in 
					(select distinct ClaimLawsuitStatusID from tblClaimLawsuitStatusTracking)
			)statusInfo 

GO
GRANT VIEW DEFINITION ON  [dbo].[vClaimLawsuitStatusesInUse] TO [base_user]
GRANT SELECT ON  [dbo].[vClaimLawsuitStatusesInUse] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vClaimLawsuitStatusesInUse] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vClaimLawsuitStatusesInUse] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vClaimLawsuitStatusesInUse] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vClaimLawsuitStatusesInUse] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vClaimLawsuitStatusesInUse] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vClaimLawsuitStatusesInUse] TO [power_user]
GRANT SELECT ON  [dbo].[vClaimLawsuitStatusesInUse] TO [power_user]
GRANT INSERT ON  [dbo].[vClaimLawsuitStatusesInUse] TO [power_user]
GRANT DELETE ON  [dbo].[vClaimLawsuitStatusesInUse] TO [power_user]
GRANT UPDATE ON  [dbo].[vClaimLawsuitStatusesInUse] TO [power_user]
GO
