SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[AuditHistory2]
AS
  SELECT L.AuditLogID, T.Owner, T.TableID, T.TableName, C.ColumnID,
         C.ColumnName, D.RowKey, D.Status,
         OldValue = case
           when (D.Status & 7) = 0 then D.OldValue
           when (L.Event = 'D') and ((D.Status & 1) <> 0) then BT.Value
           when (L.Event = 'D') and ((D.Status & 2) <> 0) then
             case when BI.Value is null then null else '*non-null image values cannot be displayed*' end
           when (L.Event = 'D') and ((D.Status & 4) <> 0) then BN.Value
         end,
         NewValue = case
           when (D.Status & 7) = 0 then D.NewValue
           when (L.Event <> 'D') and ((D.Status & 1) <> 0) then BT.Value
           when (L.Event <> 'D') and ((D.Status & 2) <> 0) then
             case when BI.Value is null then null else '*non-null image values cannot be displayed*' end
           when (L.Event <> 'D') and ((D.Status & 4) <> 0) then BN.Value
         end,
         L.Event, L.PostedDateTime, L.UserName, L.HostName, L.ApplicationName,
         ValueIsText = case when D.Status & 1 <> 0 then 1 else 0 end,
         ValueIsImage = case when D.Status & 2 <> 0 then 1 else 0 end,
         ValueIsNText = case when D.Status & 4 <> 0 then 1 else 0 end,
         ValueIsCapture = case when D.Status & 8 <> 0 then 1 else 0 end,
         ValueIsLookup = case when D.Status & 16 <> 0 then 1 else 0 end
  FROM dbo.AuditLogDetail D WITH (nolock)
    INNER JOIN dbo.AuditLog L WITH (nolock) ON (D.AuditLogID = L.AuditLogID)
    INNER JOIN dbo.AuditTables T WITH (nolock) ON (L.TableID = T.TableID)
    INNER JOIN dbo.AuditColumns C WITH (nolock) ON (D.ColumnID = C.ColumnID)
    LEFT OUTER JOIN dbo.AuditLogText BT WITH (nolock) ON (D.AuditLogID = BT.AuditLogID)
    LEFT OUTER JOIN dbo.AuditLogNText BN WITH (nolock) ON (D.AuditLogID = BN.AuditLogID)
    LEFT OUTER JOIN dbo.AuditLogImage BI WITH (nolock) ON (D.AuditLogID = BI.AuditLogID)
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditHistory2] TO [base_user]
GRANT SELECT ON  [dbo].[AuditHistory2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditHistory2] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditHistory2] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditHistory2] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditHistory2] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditHistory2] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditHistory2] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditHistory2] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditHistory2] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditHistory2] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditHistory2] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditHistory2] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditHistory2] TO [power_user]
GRANT SELECT ON  [dbo].[AuditHistory2] TO [power_user]
GRANT INSERT ON  [dbo].[AuditHistory2] TO [power_user]
GRANT DELETE ON  [dbo].[AuditHistory2] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditHistory2] TO [power_user]
GO
