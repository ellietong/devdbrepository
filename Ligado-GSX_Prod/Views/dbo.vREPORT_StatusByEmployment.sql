SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vREPORT_StatusByEmployment]
AS

SELECT	Jobsite, 
		MatterID,
		ClaimLawsuitTypeID,
		SUM(ActiveCount) AS ActiveCount,
		SUM(InactiveCount) AS InactiveCount,
		SUM(ClosedCount) AS ClosedCount,
		SUM(Total) AS Total

FROM (

		SELECT    COALESCE (UPPER(cle.JobsiteCity), 'UNKNOWN') + ', ' + COALESCE (s.State, 'UNKNOWN') AS Jobsite, 
					CASE WHEN ClaimLawsuitStatusID IN (41, 42) THEN 1 ELSE 0 END AS ActiveCount, 
					CASE WHEN ClaimLAwsuitStatusID IN (29, 43) THEN 1 ELSE 0 END AS InactiveCount,
					CASE WHEN ClaimLAwsuitStatusID IN (15, 16, 23, 25, 27, 44) THEN 1 ELSE 0 END AS ClosedCount,
					1 AS Total,
					cl.ClaimLawsuitID,
					c.MatterID, 
					cl.ClaimLawsuitTypeID
		FROM         dbo.tblClaimLawsuit AS cl INNER JOIN
					dbo.tblClaim AS c ON cl.ClaimID = c.ClaimID INNER JOIN
					dbo.tblClaimLawsuitStatusTracking AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID INNER JOIN
					(SELECT DISTINCT ClaimLawsuitID, JobsiteCity, JobsiteStateID FROM dbo.tblClaimLawsuitEmployment)AS cle ON cl.ClaimLawsuitID = cle.ClaimLawsuitID INNER JOIN
					dbo.tblState AS s ON cle.JobsiteStateID = s.StateID
		WHERE clst.IsPrimaryStatus = 1
		--GROUP BY COALESCE (UPPER(cle.JobsiteCity), 'UNKNOWN') + ', ' + COALESCE (s.State, 'UNKNOWN'), c.MatterID, cl.ClaimLawsuitTypeID

) a
GROUP BY Jobsite, MatterID, ClaimLawsuitTypeID

GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_StatusByEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_StatusByEmployment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_StatusByEmployment] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_StatusByEmployment] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_StatusByEmployment] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_StatusByEmployment] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_StatusByEmployment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_StatusByEmployment] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_StatusByEmployment] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_StatusByEmployment] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_StatusByEmployment] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_StatusByEmployment] TO [power_user]
GO
