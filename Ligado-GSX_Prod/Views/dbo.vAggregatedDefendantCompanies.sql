SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vAggregatedDefendantCompanies]
AS
SELECT DISTINCT ClaimLawsuitID, 
(SELECT DefendantCompany + CASE WHEN (SELECT COUNT(DefendantCompanyID) FROM tblAllegation WHERE ClaimLawsuitID = a.ClaimLawsuitID) > 1 THEN 
		CASE WHEN DefendantCompany = (SELECT MAX(DefendantCompany) FROM tblAllegation a1
INNER JOIN tblDefendantCompany dc ON a1.DefendantCompanyID = dc.DefendantCompanyID
WHERE a1.ClaimLawsuitID = a.ClaimLawsuitID) THEN '' ELSE ' | ' END ELSE '' END as [text()]
FROM tblAllegation a1
INNER JOIN tblDefendantCompany dc ON a1.DefendantCompanyID = dc.DefendantCompanyID
WHERE a1.ClaimLawsuitID = a.ClaimLawsuitID
ORDER BY DefendantCompany
FOR XML PATH ('')) AS DefendantCompany
FROM tblAllegation a



GO
GRANT VIEW DEFINITION ON  [dbo].[vAggregatedDefendantCompanies] TO [base_user]
GRANT SELECT ON  [dbo].[vAggregatedDefendantCompanies] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vAggregatedDefendantCompanies] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vAggregatedDefendantCompanies] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vAggregatedDefendantCompanies] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vAggregatedDefendantCompanies] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vAggregatedDefendantCompanies] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vAggregatedDefendantCompanies] TO [power_user]
GRANT SELECT ON  [dbo].[vAggregatedDefendantCompanies] TO [power_user]
GRANT INSERT ON  [dbo].[vAggregatedDefendantCompanies] TO [power_user]
GRANT DELETE ON  [dbo].[vAggregatedDefendantCompanies] TO [power_user]
GRANT UPDATE ON  [dbo].[vAggregatedDefendantCompanies] TO [power_user]
GO
