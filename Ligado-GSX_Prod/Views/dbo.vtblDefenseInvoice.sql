SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vtblDefenseInvoice]
AS
SELECT     di.DocumentID, di.DefenseInvoiceID, dca.DefenseCounsel, v.firmid, v.firm, di.InvoiceNumber, di.Period, di.SubmitDate, di.DatePayable, dis.Status, SUM(dic.Fees) 
                      AS fees, SUM(dic.Costs) AS costs, SUM(dic.Adjustments) AS adjustments, COALESCE (SUM(dic.Fees), 0) + COALESCE (SUM(dic.Costs), 0) 
                      - COALESCE (SUM(dic.Adjustments), 0) AS InvoiceTotal, ch.SourceNumber, ch.SourceDate, pd.PaymentAmount AS sourceamount, dic.MatterID, di.StatusID, 
                      di.InvoiceNotes, di.ApprovedBy, di.DateApproved, di.QCBy, di.QCDate, di.InvoiceLink
FROM         dbo.tblDefenseInvoice AS di INNER JOIN
                      dbo.tblDefInvClaimantDetail AS dic ON dic.DefenseInvoiceID = di.DefenseInvoiceID LEFT OUTER JOIN
                      dbo.tblPaymentDefense AS pd ON pd.DefenseInvoiceID = di.DefenseInvoiceID LEFT OUTER JOIN
                      dbo.tblDefenseInvoiceStatus AS dis ON dis.StatusID = di.StatusID LEFT OUTER JOIN
                      dbo.tblCheck AS ch ON ch.CheckID = pd.CheckID LEFT OUTER JOIN
                      dbo.vtblFirmsAndVendors AS v ON v.firmid = di.VendorFirmID LEFT OUTER JOIN
                      dbo.tblDefenseCounsel AS dca ON dca.DefenseCounselID = di.FirmID
GROUP BY di.DocumentID, di.DefenseInvoiceID, dca.DefenseCounsel, v.firm, di.InvoiceNumber, di.Period, di.SubmitDate, di.DatePayable, dis.Status, ch.SourceNumber, 
                      ch.SourceDate, pd.PaymentAmount, dic.MatterID, di.StatusID, di.InvoiceNotes, di.ApprovedBy, di.DateApproved, di.QCBy, di.QCDate, di.InvoiceLink, v.firmid



GO
GRANT VIEW DEFINITION ON  [dbo].[vtblDefenseInvoice] TO [base_user]
GRANT SELECT ON  [dbo].[vtblDefenseInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vtblDefenseInvoice] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vtblDefenseInvoice] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vtblDefenseInvoice] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vtblDefenseInvoice] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vtblDefenseInvoice] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vtblDefenseInvoice] TO [power_user]
GRANT SELECT ON  [dbo].[vtblDefenseInvoice] TO [power_user]
GRANT INSERT ON  [dbo].[vtblDefenseInvoice] TO [power_user]
GRANT DELETE ON  [dbo].[vtblDefenseInvoice] TO [power_user]
GRANT UPDATE ON  [dbo].[vtblDefenseInvoice] TO [power_user]
GO
