SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vCarrierAddress]
AS
SELECT DISTINCT facs.FirmAddressID AS CarrierFirmAddressID, f.FirmDisplayName + ' - ' + fa.FirmCity AS CarrierAddress
FROM         [Ligado-ExtranetSecurity].dbo.RoleFirmAddressClientSite AS rfacs INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.Role AS r ON rfacs.RoleID = r.RoleID INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite AS facs ON rfacs.FirmAddressClientSiteID = facs.FirmAddressClientSiteID INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.FirmAddress AS fa ON facs.FirmAddressID = fa.FirmAddressID INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.Firm AS f ON fa.FirmID = f.FirmID
WHERE     (facs.ClientSiteID = dbo.fn_GetClientSiteID()) AND (r.RoleName = 'Insurer')



GO
GRANT VIEW DEFINITION ON  [dbo].[vCarrierAddress] TO [base_user]
GRANT SELECT ON  [dbo].[vCarrierAddress] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vCarrierAddress] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vCarrierAddress] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vCarrierAddress] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vCarrierAddress] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vCarrierAddress] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vCarrierAddress] TO [power_user]
GRANT SELECT ON  [dbo].[vCarrierAddress] TO [power_user]
GRANT INSERT ON  [dbo].[vCarrierAddress] TO [power_user]
GRANT DELETE ON  [dbo].[vCarrierAddress] TO [power_user]
GRANT UPDATE ON  [dbo].[vCarrierAddress] TO [power_user]
GO
