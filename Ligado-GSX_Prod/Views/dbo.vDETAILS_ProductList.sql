SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vDETAILS_ProductList]
AS
SELECT     p.ProductID, COALESCE (pt.ProductType, 'UNSPECIFIED') + ' - ' + COALESCE (pb.ProductBrand, 'UNSPECIFIED') AS Product
FROM         dbo.tblProduct AS p INNER JOIN
                      dbo.tblProductBrand AS pb ON pb.ProductBrandID = p.ProductBrandID INNER JOIN
                      dbo.tblProductType AS pt ON pt.ProductTypeID = p.ProductTypeID



GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ProductList] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_ProductList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ProductList] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_ProductList] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_ProductList] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_ProductList] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_ProductList] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ProductList] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_ProductList] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_ProductList] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_ProductList] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_ProductList] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pb"
            Begin Extent = 
               Top = 6
               Left = 269
               Bottom = 84
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pm"
            Begin Extent = 
               Top = 6
               Left = 463
               Bottom = 84
               Right = 656
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pn"
            Begin Extent = 
               Top = 84
               Left = 269
               Bottom = 162
               Right = 424
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pt"
            Begin Extent = 
               Top = 84
               Left = 462
               Bottom = 162
               Right = 614
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output ', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ProductList', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'= 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ProductList', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ProductList', NULL, NULL
GO
