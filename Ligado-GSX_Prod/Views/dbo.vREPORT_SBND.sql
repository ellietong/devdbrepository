SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vREPORT_SBND]
AS

	
    -- Insert statements for procedure here
SELECT distinct
cl.ClaimLawsuitID,
	s.FullNameState As State,
	pca.PlaintiffCounsel, 
	CASE WHEN HighLevelDisease IS NULL THEN 'Unknown' ELSE HighLevelDisease END AS HighLevelDisease,
	m.Matter,
	c.MatterID,
	cl.DefenseCounselAddressID,
	dj.DefenseCounselAddressID AS DJDefenseCounselAddressID,
	ClaimLawsuitTypeID
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s ON l.StateID=s.StateID
inner join tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
INNER JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimID
INNER JOIN tblDisease d ON cd.DiseaseID = d.DiseaseID
INNER JOIN tblHighLevelDisease hld ON d.HighLevelDiseaseID = hld.HighLevelDiseaseID
INNER JOIN tblMatter m ON c.MatterID = m.MatterID
INNER JOIN		tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.MatterID
where	ClaimLawsuitStatusID in (25,39) 
		and  IsPrimaryStatus = 1
		and IsPrimaryForClaim = 1
		and PrimaryDisease = 1
		









GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SBND] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_SBND] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SBND] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_SBND] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_SBND] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_SBND] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_SBND] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SBND] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_SBND] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_SBND] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_SBND] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_SBND] TO [power_user]
GO
