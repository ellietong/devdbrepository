SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [dbo].[vSEARCH_Document_old]
AS

WITH Claimants AS (
	SELECT DocumentID, 
		REPLACE((SELECT TOP 5  '/Claims/ClaimantDetails.aspx?ClientSiteID=1&ClaimantPersonalInfoID=' + CONVERT(nvarchar, cpi.ClaimantPersonalInfoID) AS 'a/@href',
			'_blank' AS 'a/@target', 
			COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') + COALESCE(' ' + MiddleName, '') as 'a', '', '' as br 
		
		FROM tblClaimantPersonalInfo cpi
			INNER JOIN tblMultiClaimantDocument mcd ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		WHERE DocumentID = d.DocumentID
		FOR XML PATH ('')), '</br>', '') AS Claimants
			FROM tblDocuments d
		),
Locked 
AS
(

--Add in dismissals
SELECT mcd.DocumentID, COUNT(c.ClaimID) AS Locked FROM tblClaim c
	INNER JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l							ON cl.LawsuitID	 = l.LawsuitID
	INNER JOIN tblMultiClaimantDocument mcd			ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuitStatusTracking clst	ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
WHERE COALESCE(c.IsCurrentClaim,0) = 1 
	AND COALESCE(IsPrimaryForClaim, 0) = 1 
	AND ClaimLawsuitStatusID IN (15,16,17)
GROUP BY mcd.DocumentID
UNION
--Check for settlements
SELECT mcd.DocumentID, COUNT(DISTINCT paymentSettlementID) 
FROM tblPaymentSettlement ps
	LEFT JOIN  tblClaimSettlement cs		ON ps.ClaimSettlementID = cs.ClaimSettlementID
	LEFT JOIN  tblClaimLawsuit cl			ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN  tblClaim c					ON cl.ClaimID = c.ClaimID
	INNER JOIN tblMultiClaimantDocument mcd ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
GROUP BY mcd.DocumentID, c.ClaimantPersonalInfoID
	

)
SELECT DISTINCT 
                      d.DocumentID, d.DocumentTitle, d.DocumentURL, cdt.ClaimantDocumentType, f.FirmID, f.Firm, d.CreatedBy, d.DateCreated, d.AcceptedBy, d.DateAccepted, 
                      cdt.ClaimantDocumentTypeID, Claimants, CASE WHEN HistoricalDocumentID IS NOT NULL THEN CONVERT(bit, 1) ELSE CONVERT(bit, 0) END AS IsHistorical,
                      d.MatterID, DocumentSourceID, Comments,--, PageNums, DocCategory, csd.DocCategoryID
					  COALESCE(Locked, 0) AS Locked
FROM         Claimants claimants INNER JOIN 
				dbo.tblDocuments AS d ON claimants.DocumentID = d.DocumentID INNER JOIN
                      dbo.tblClaimantDocumentType AS cdt ON d.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID LEFT OUTER JOIN
                      dbo.tblMultiClaimantDocument AS mcd ON d.DocumentID = mcd.DocumentID LEFT OUTER JOIN
                      dbo.tblClaim AS c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID LEFT OUTER JOIN
                      dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress AS dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID LEFT OUTER JOIN
                      dbo.vtblFirm AS f ON dca.DefenseCounselID = f.FirmID LEFT OUTER JOIN
                      dbo.tblDefenseJurisdiction AS dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID --LEFT OUTER JOIN
					  LEFT JOIN Locked AS Lock ON mcd.DocumentID = lock.documentID
                      --tblClaimSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID LEFT OUTER JOIN
                      --tblClaimSettlementDocuments csd ON cs.ClaimLawsuitID = csd.ClaimLawsuitID LEFT OUTER JOIN
                      --tblClaimSettlementDocumentCategory csdc ON csd.DocCategoryID = csdc.DocCategoryID







GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Document_old] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_Document_old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Document_old] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_Document_old] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_Document_old] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_Document_old] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_Document_old] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Document_old] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_Document_old] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_Document_old] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_Document_old] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_Document_old] TO [power_user]
GO
