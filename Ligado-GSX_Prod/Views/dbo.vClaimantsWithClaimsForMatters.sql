SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vClaimantsWithClaimsForMatters] 
AS 
  SELECT DISTINCT claimantpersonalinfoid 
                  , matterid 
  FROM   tblclaim 
  WHERE  claimantpersonalinfoid IS NOT NULL 
     AND matterid IS NOT NULL 

GO
GRANT VIEW DEFINITION ON  [dbo].[vClaimantsWithClaimsForMatters] TO [base_user]
GRANT SELECT ON  [dbo].[vClaimantsWithClaimsForMatters] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vClaimantsWithClaimsForMatters] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vClaimantsWithClaimsForMatters] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vClaimantsWithClaimsForMatters] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vClaimantsWithClaimsForMatters] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vClaimantsWithClaimsForMatters] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vClaimantsWithClaimsForMatters] TO [power_user]
GRANT SELECT ON  [dbo].[vClaimantsWithClaimsForMatters] TO [power_user]
GRANT INSERT ON  [dbo].[vClaimantsWithClaimsForMatters] TO [power_user]
GRANT DELETE ON  [dbo].[vClaimantsWithClaimsForMatters] TO [power_user]
GRANT UPDATE ON  [dbo].[vClaimantsWithClaimsForMatters] TO [power_user]
GO
