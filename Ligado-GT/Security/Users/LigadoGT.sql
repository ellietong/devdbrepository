IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'LigadoGT')
CREATE LOGIN [LigadoGT] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [LigadoGT] FOR LOGIN [LigadoGT]
GO
