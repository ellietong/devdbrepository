CREATE ROLE [Ligado_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Ligado_User', N'LigadoGT'
GO
EXEC sp_addrolemember N'Ligado_User', N'spuser'
GO
