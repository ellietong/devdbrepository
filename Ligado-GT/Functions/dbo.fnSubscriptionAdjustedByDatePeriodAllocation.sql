SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnSubscriptionAdjustedByDatePeriodAllocation] (@PolicyDateID int, @SubscriptionCoverageID int)
RETURNS float
AS

BEGIN
	---- Declare the fields used to test the function
	--DECLARE @PolicyDateID int
	--SET @PolicyDateID = 1
	--DECLARE @SubscriptionCoverageID int
	--SET @SubscriptionCoverageID = 1

	-- Declare the field used to return the final result
	DECLARE @DateProportionAllocation float

	-- Declare standing variables for the policy dates
	DECLARE @SubStartDate date
	DECLARE @SubEndDate date
	DECLARE @PolicyStartDate date
	DECLARE @PolicyEndDate date
	DECLARE @CompareStart date
	DECLARE @CompareEnd date

	-- Declare the temporary table for policy dates 
	DECLARE @PolicyDates TABLE (PolicyDateID int,
								PolicyStartDate date, 
								PolicyEndDate date)

	-- Gather the policy dates - NOTE: This is where the actual/allocation switch occurs
	INSERT INTO @PolicyDates
		SELECT PolicyDateID, 
			CAST(PStartDate AS date) AS PolicyStartDate, CAST(PEndDate AS date) AS PolicyEndDate
		FROM tblPolicyDates
		WHERE PolicyDateID = @PolicyDateID
		ORDER BY PolicyDateID
				
	-- Parse out the date information for the subscription and policy records
	SET @SubStartDate = (SELECT StartDate FROM vSubscription WHERE SubscriptionCoverageID = @SubscriptionCoverageID)
	SET @SubEndDate   = (SELECT EndDate   FROM vSubscription WHERE SubscriptionCoverageID = @SubscriptionCoverageID)	
	SET @PolicyStartDate = (SELECT PolicyStartDate FROM @PolicyDates)
	SET @PolicyEndDate   = (SELECT PolicyEndDate   FROM @PolicyDates)

	-- Check if the actual policy dates fall fully with the subscription dates
	IF ((COALESCE(@SubStartDate, @PolicyStartDate) <= @PolicyStartDate) AND (COALESCE(@SubEndDate, @PolicyEndDate) >= @PolicyEndDate)) BEGIN
		-- If so, set the percentage for this date record to 100%
		SET @DateProportionAllocation = 1.000
	END
	-- Check if the subscription dates are totally outside the actual policy dates
	ELSE IF ((COALESCE(@SubStartDate, @PolicyStartDate) > @PolicyEndDate) OR (COALESCE(@SubEndDate, @PolicyEndDate) < @PolicyStartDate)) BEGIN
		-- If so, set the percentage for this date record to 0% (this should be redundant to the default value)
		SET @DateProportionAllocation = 0.000
	END
	-- Otherwise, proportion the percentage based on the number of subscription days falling within actual policy dates
	ELSE BEGIN
		IF COALESCE(@SubStartDate, @PolicyStartDate) > @PolicyStartDate BEGIN SET @CompareStart = @SubStartDate	   END
			ELSE														BEGIN SET @CompareStart = @PolicyStartDate END
		IF COALESCE(@SubEndDate,   @PolicyEndDate)   < @PolicyEndDate   BEGIN SET @CompareEnd   = @SubEndDate      END
			ELSE													    BEGIN SET @CompareEnd   = @PolicyEndDate   END
		SET @DateProportionAllocation = ((CAST(DATEDIFF(day, @CompareStart,    @CompareEnd)    AS float)) / 
								         (CAST(DATEDIFF(day, @PolicyStartDate, @PolicyEndDate) AS float)))
	END

	RETURN @DateProportionAllocation
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnSubscriptionAdjustedByDatePeriodAllocation] TO [base_user]
GRANT EXECUTE ON  [dbo].[fnSubscriptionAdjustedByDatePeriodAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fnSubscriptionAdjustedByDatePeriodAllocation] TO [power_user]
GRANT EXECUTE ON  [dbo].[fnSubscriptionAdjustedByDatePeriodAllocation] TO [power_user]
GRANT ALTER ON  [dbo].[fnSubscriptionAdjustedByDatePeriodAllocation] TO [Subscription_User]
GRANT VIEW DEFINITION ON  [dbo].[fnSubscriptionAdjustedByDatePeriodAllocation] TO [Subscription_User]
GRANT EXECUTE ON  [dbo].[fnSubscriptionAdjustedByDatePeriodAllocation] TO [Subscription_User]
GO
