CREATE TABLE [dbo].[tblTenderBasicEventType]
(
[TenderBasicEventTypeID] [int] NOT NULL IDENTITY(1, 1),
[TenderBasicEventType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderBasicEventType] ADD CONSTRAINT [PK_tblTenderBasicEventType] PRIMARY KEY CLUSTERED  ([TenderBasicEventTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicEventType] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderBasicEventType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicEventType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblTenderBasicEventType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblTenderBasicEventType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblTenderBasicEventType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblTenderBasicEventType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicEventType] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderBasicEventType] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderBasicEventType] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderBasicEventType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderBasicEventType] TO [power_user]
GO
