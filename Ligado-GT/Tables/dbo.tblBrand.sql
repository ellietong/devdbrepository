CREATE TABLE [dbo].[tblBrand]
(
[BrandID] [int] NOT NULL IDENTITY(1, 1),
[Brand] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBrand] ADD CONSTRAINT [PK_tblBrand] PRIMARY KEY CLUSTERED  ([BrandID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBrand] TO [base_user]
GRANT SELECT ON  [dbo].[tblBrand] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBrand] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBrand] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBrand] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBrand] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBrand] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBrand] TO [power_user]
GRANT SELECT ON  [dbo].[tblBrand] TO [power_user]
GRANT INSERT ON  [dbo].[tblBrand] TO [power_user]
GRANT DELETE ON  [dbo].[tblBrand] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBrand] TO [power_user]
GO
