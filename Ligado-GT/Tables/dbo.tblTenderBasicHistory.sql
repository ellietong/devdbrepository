CREATE TABLE [dbo].[tblTenderBasicHistory]
(
[TenderBasicHistoryID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [int] NOT NULL,
[TenderBasicEventTypeID] [int] NULL,
[TenderBasicContactID_From] [int] NULL,
[TenderBasicContactID_To] [int] NULL,
[Username] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenderDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderBasicHistory] ADD CONSTRAINT [PK_tblTenderBasicHistory] PRIMARY KEY CLUSTERED  ([TenderBasicHistoryID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderBasicHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicHistory] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblTenderBasicHistory] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblTenderBasicHistory] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblTenderBasicHistory] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblTenderBasicHistory] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicHistory] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderBasicHistory] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderBasicHistory] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderBasicHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderBasicHistory] TO [power_user]
GO
