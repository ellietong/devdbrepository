CREATE TABLE [dbo].[tblDiscountState]
(
[DiscountStateID] [int] NOT NULL IDENTITY(1, 1),
[StateID] [int] NULL,
[StateDiscount] [money] NULL,
[DiscountID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDiscountState] ADD CONSTRAINT [PK_tblDiscountState] PRIMARY KEY CLUSTERED  ([DiscountStateID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDiscountState] TO [base_user]
GRANT SELECT ON  [dbo].[tblDiscountState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDiscountState] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDiscountState] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblDiscountState] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblDiscountState] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblDiscountState] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDiscountState] TO [power_user]
GRANT SELECT ON  [dbo].[tblDiscountState] TO [power_user]
GRANT INSERT ON  [dbo].[tblDiscountState] TO [power_user]
GRANT DELETE ON  [dbo].[tblDiscountState] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDiscountState] TO [power_user]
GO
