CREATE TABLE [dbo].[m_tblPACE2011Payments]
(
[PACEPaymentID] [int] NULL,
[ExpertFirmID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[PlaintiffCounselAddressID] [int] NULL,
[InvoiceTotal] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACE2011Payments] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPACE2011Payments] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPACE2011Payments] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblPACE2011Payments] TO [Claims_User]
GRANT DELETE ON  [dbo].[m_tblPACE2011Payments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblPACE2011Payments] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACE2011Payments] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblPACE2011Payments] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblPACE2011Payments] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblPACE2011Payments] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblPACE2011Payments] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACE2011Payments] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblPACE2011Payments] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblPACE2011Payments] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblPACE2011Payments] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblPACE2011Payments] TO [power_user]
GO
