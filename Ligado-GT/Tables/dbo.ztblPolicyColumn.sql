CREATE TABLE [dbo].[ztblPolicyColumn]
(
[PolicyColumnID] [int] NOT NULL IDENTITY(1, 1),
[ColumnID] [int] NULL,
[PolicyDateID] [int] NULL,
[PolicyDaysInColumn] [float] NULL,
[PercentPolicyDaysInColumn] [float] NULL,
[PolicyPerOccPerColumn] [float] NULL,
[AttachmentPoint] [float] NULL,
[ProvisionValueID] [int] NULL,
[ColumnDays] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblPolicyColumn] ADD CONSTRAINT [PK_ztblPolicyColumn] PRIMARY KEY NONCLUSTERED  ([PolicyColumnID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyColumn] TO [base_user]
GRANT SELECT ON  [dbo].[ztblPolicyColumn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyColumn] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ztblPolicyColumn] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ztblPolicyColumn] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ztblPolicyColumn] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ztblPolicyColumn] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyColumn] TO [power_user]
GRANT SELECT ON  [dbo].[ztblPolicyColumn] TO [power_user]
GRANT INSERT ON  [dbo].[ztblPolicyColumn] TO [power_user]
GRANT DELETE ON  [dbo].[ztblPolicyColumn] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblPolicyColumn] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Orientation', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'AttachmentPoint'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'AttachmentPoint'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'AttachmentPoint'
GO
DECLARE @xp int
SELECT @xp=1905
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'AttachmentPoint'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'AttachmentPoint'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=1905
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ColumnID'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PercentPolicyDaysInColumn'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PercentPolicyDaysInColumn'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PercentPolicyDaysInColumn'
GO
DECLARE @xp int
SELECT @xp=1905
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PercentPolicyDaysInColumn'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PercentPolicyDaysInColumn'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyColumnID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyColumnID'
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyColumnID'
GO
DECLARE @xp int
SELECT @xp=1905
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyColumnID'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDateID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDateID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDateID'
GO
DECLARE @xp int
SELECT @xp=1905
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDateID'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDateID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDaysInColumn'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDaysInColumn'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDaysInColumn'
GO
DECLARE @xp int
SELECT @xp=2415
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDaysInColumn'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDaysInColumn'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDaysInColumn'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyDaysInColumn'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyPerOccPerColumn'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyPerOccPerColumn'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyPerOccPerColumn'
GO
DECLARE @xp int
SELECT @xp=1905
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyPerOccPerColumn'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'PolicyPerOccPerColumn'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ProvisionValueID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ProvisionValueID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ProvisionValueID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ProvisionValueID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ProvisionValueID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ProvisionValueID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ProvisionValueID'
GO
EXEC sp_addextendedproperty N'MS_TextAlign', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblPolicyColumn', 'COLUMN', N'ProvisionValueID'
GO
