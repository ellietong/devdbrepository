CREATE TABLE [dbo].[rdc_LawsuitStatusLookup]
(
[VerusStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitStatusID] [float] NULL,
[KCICLawsuitStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
