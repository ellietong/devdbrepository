CREATE TABLE [dbo].[tblCheckFundingType]
(
[FundingTypeID] [int] NOT NULL IDENTITY(1, 1),
[FundingType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCheckFundingType] ADD CONSTRAINT [PK_tblCheckFundingType] PRIMARY KEY CLUSTERED  ([FundingTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCheckFundingType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingType] TO [CheckApproval_User]
GRANT SELECT ON  [dbo].[tblCheckFundingType] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingType] TO [CheckQC_User]
GRANT SELECT ON  [dbo].[tblCheckFundingType] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingType] TO [CheckWriter_User]
GRANT SELECT ON  [dbo].[tblCheckFundingType] TO [CheckWriter_User]
GRANT INSERT ON  [dbo].[tblCheckFundingType] TO [CheckWriter_User]
GRANT UPDATE ON  [dbo].[tblCheckFundingType] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCheckFundingType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblCheckFundingType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblCheckFundingType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblCheckFundingType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCheckFundingType] TO [power_user]
GRANT SELECT ON  [dbo].[tblCheckFundingType] TO [power_user]
GRANT INSERT ON  [dbo].[tblCheckFundingType] TO [power_user]
GRANT DELETE ON  [dbo].[tblCheckFundingType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCheckFundingType] TO [power_user]
GO
