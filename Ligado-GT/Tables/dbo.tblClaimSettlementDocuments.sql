CREATE TABLE [dbo].[tblClaimSettlementDocuments]
(
[SettleDocID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[DocumentID] [bigint] NULL,
[DocCategoryID] [int] NULL,
[PageNums] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimSettlementDocuments] ADD CONSTRAINT [PK_tblSettleDocs] PRIMARY KEY CLUSTERED  ([SettleDocID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimSettlementDocuments_tblClaimLawsuit] ON [dbo].[tblClaimSettlementDocuments] ([ClaimLawsuitID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimSettlementDocuments] WITH NOCHECK ADD CONSTRAINT [FK_tblClaimSettlementDocuments_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocuments] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocuments] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimSettlementDocuments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimSettlementDocuments] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementDocuments] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocuments] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimSettlementDocuments] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimSettlementDocuments] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimSettlementDocuments] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementDocuments] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocuments] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimSettlementDocuments] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimSettlementDocuments] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimSettlementDocuments] TO [power_user]
GO
