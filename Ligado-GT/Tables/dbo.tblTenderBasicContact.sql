CREATE TABLE [dbo].[tblTenderBasicContact]
(
[TenderBasicContactID] [bigint] NOT NULL IDENTITY(1, 1),
[DefenseCounselAddressID] [bigint] NULL,
[PlaintiffCounselAddressID] [bigint] NULL,
[VendorFirmID] [int] NULL,
[DefendantCompanyID] [int] NULL,
[BrandID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTenderBasicContact] ADD CONSTRAINT [OneIDLookupPerRow_OthersNull] CHECK (([DefenseCounselAddressID] IS NOT NULL AND [PlaintiffCounselAddressID] IS NULL AND [VendorFirmID] IS NULL AND [DefendantCompanyID] IS NULL AND [BrandID] IS NULL OR [DefenseCounselAddressID] IS NULL AND [PlaintiffCounselAddressID] IS NOT NULL AND [VendorFirmID] IS NULL AND [DefendantCompanyID] IS NULL AND [BrandID] IS NULL OR [DefenseCounselAddressID] IS NULL AND [PlaintiffCounselAddressID] IS NULL AND [VendorFirmID] IS NOT NULL AND [DefendantCompanyID] IS NULL AND [BrandID] IS NULL OR [DefenseCounselAddressID] IS NULL AND [PlaintiffCounselAddressID] IS NULL AND [VendorFirmID] IS NULL AND [DefendantCompanyID] IS NOT NULL AND [BrandID] IS NULL OR [DefenseCounselAddressID] IS NULL AND [PlaintiffCounselAddressID] IS NULL AND [VendorFirmID] IS NULL AND [DefendantCompanyID] IS NULL AND [BrandID] IS NOT NULL))
GO
ALTER TABLE [dbo].[tblTenderBasicContact] ADD CONSTRAINT [PK_tblTenderBasicContact] PRIMARY KEY CLUSTERED  ([TenderBasicContactID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblTenderBasicContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicContact] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblTenderBasicContact] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblTenderBasicContact] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblTenderBasicContact] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblTenderBasicContact] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTenderBasicContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblTenderBasicContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblTenderBasicContact] TO [power_user]
GRANT DELETE ON  [dbo].[tblTenderBasicContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTenderBasicContact] TO [power_user]
GO
