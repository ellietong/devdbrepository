CREATE TABLE [dbo].[tblSavedCriteria]
(
[SavedCriteriaID] [int] NOT NULL IDENTITY(1, 1),
[SavedCriteriaGroupID] [int] NOT NULL,
[ParameterName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParameterValue] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedCriteria] ADD CONSTRAINT [PK_tblSavedCriteria] PRIMARY KEY CLUSTERED  ([SavedCriteriaID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblSavedCriteria_tblSavedCriteriaGroup] ON [dbo].[tblSavedCriteria] ([SavedCriteriaGroupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedCriteria] ADD CONSTRAINT [FK_tblSavedCriteria_tblSavedCriteriaGroup] FOREIGN KEY ([SavedCriteriaGroupID]) REFERENCES [dbo].[tblSavedCriteriaGroup] ([SavedCriteriaGroupID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedCriteria] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedCriteria] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedCriteria] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSavedCriteria] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSavedCriteria] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSavedCriteria] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSavedCriteria] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedCriteria] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedCriteria] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedCriteria] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedCriteria] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedCriteria] TO [power_user]
GO
