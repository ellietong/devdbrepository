CREATE TABLE [dbo].[ztblByMonthColumns]
(
[MonthColumnID] [int] NOT NULL,
[ColumnID] [int] NULL,
[ColumnStartDate] [datetime] NULL,
[ColumnEndDate] [datetime] NULL,
[ColumnRemaining] [money] NULL,
[TriggeredDays] [int] NULL,
[TotalMonths] [int] NULL,
[MinRemaining] [money] NULL,
[InsuranceProgramID] [int] NULL,
[OccurrenceID] [bigint] NULL,
[AllocationID] [int] NULL,
[m_PolicyMonths] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblByMonthColumns] TO [base_user]
GRANT SELECT ON  [dbo].[ztblByMonthColumns] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblByMonthColumns] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ztblByMonthColumns] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ztblByMonthColumns] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ztblByMonthColumns] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ztblByMonthColumns] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ztblByMonthColumns] TO [power_user]
GRANT SELECT ON  [dbo].[ztblByMonthColumns] TO [power_user]
GRANT INSERT ON  [dbo].[ztblByMonthColumns] TO [power_user]
GRANT DELETE ON  [dbo].[ztblByMonthColumns] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblByMonthColumns] TO [power_user]
GO
