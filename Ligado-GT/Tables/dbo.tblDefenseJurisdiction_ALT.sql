CREATE TABLE [dbo].[tblDefenseJurisdiction_ALT]
(
[DefenseJurisdictionID] [int] NOT NULL IDENTITY(1, 1),
[JurisdictionID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[CourtTypeID] [int] NULL,
[IsActive] [bit] NULL,
[StateID] [int] NULL,
[MatterID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseJurisdiction_ALT] ADD CONSTRAINT [PK_tblDefenseJurisdiction_ALT] PRIMARY KEY CLUSTERED  ([DefenseJurisdictionID]) ON [PRIMARY]
GO
