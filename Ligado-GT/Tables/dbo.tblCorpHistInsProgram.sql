CREATE TABLE [dbo].[tblCorpHistInsProgram]
(
[CorpHistInsProgramID] [int] NOT NULL IDENTITY(1, 1),
[CorporateHistoryID] [int] NULL,
[InsuranceProgramID] [int] NULL,
[From] [smalldatetime] NULL,
[Until] [smalldatetime] NULL,
[ProgramDescription] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorpHistInsProgram] ADD CONSTRAINT [PK_tblCorpHistInsProgram] PRIMARY KEY CLUSTERED  ([CorpHistInsProgramID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCorpHistInsProgram] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorpHistInsProgram] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorpHistInsProgram] TO [CorpHist_User]
GRANT INSERT ON  [dbo].[tblCorpHistInsProgram] TO [CorpHist_User]
GRANT DELETE ON  [dbo].[tblCorpHistInsProgram] TO [CorpHist_User]
GRANT UPDATE ON  [dbo].[tblCorpHistInsProgram] TO [CorpHist_User]
GRANT SELECT ON  [dbo].[tblCorpHistInsProgram] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblCorpHistInsProgram] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblCorpHistInsProgram] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblCorpHistInsProgram] TO [Enviro_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCorpHistInsProgram] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCorpHistInsProgram] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblCorpHistInsProgram] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblCorpHistInsProgram] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblCorpHistInsProgram] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCorpHistInsProgram] TO [power_user]
GRANT SELECT ON  [dbo].[tblCorpHistInsProgram] TO [power_user]
GRANT INSERT ON  [dbo].[tblCorpHistInsProgram] TO [power_user]
GRANT DELETE ON  [dbo].[tblCorpHistInsProgram] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCorpHistInsProgram] TO [power_user]
GO
