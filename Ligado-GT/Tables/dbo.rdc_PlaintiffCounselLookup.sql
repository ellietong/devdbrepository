CREATE TABLE [dbo].[rdc_PlaintiffCounselLookup]
(
[Verus Plaintiff Counsel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KCIC Plaintiff Counsel] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KCICID] [int] NULL
) ON [PRIMARY]
GO
