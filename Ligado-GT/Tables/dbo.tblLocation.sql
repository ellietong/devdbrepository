CREATE TABLE [dbo].[tblLocation]
(
[LocationID] [int] NOT NULL IDENTITY(1, 1),
[Location] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLocation] ADD CONSTRAINT [PK_tblLocation] PRIMARY KEY CLUSTERED  ([LocationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblLocation] TO [base_user]
GRANT SELECT ON  [dbo].[tblLocation] TO [base_user]
GRANT SELECT ON  [dbo].[tblLocation] TO [Language_User]
GRANT INSERT ON  [dbo].[tblLocation] TO [Language_User]
GRANT DELETE ON  [dbo].[tblLocation] TO [Language_User]
GRANT UPDATE ON  [dbo].[tblLocation] TO [Language_User]
GRANT VIEW DEFINITION ON  [dbo].[tblLocation] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblLocation] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblLocation] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblLocation] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblLocation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblLocation] TO [power_user]
GRANT SELECT ON  [dbo].[tblLocation] TO [power_user]
GRANT INSERT ON  [dbo].[tblLocation] TO [power_user]
GRANT DELETE ON  [dbo].[tblLocation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLocation] TO [power_user]
GO
