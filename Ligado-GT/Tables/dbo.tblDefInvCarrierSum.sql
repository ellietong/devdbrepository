CREATE TABLE [dbo].[tblDefInvCarrierSum]
(
[CarrierName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BilledAmount] [money] NULL,
[AmountReceived] [money] NULL,
[DatePaid] [datetime] NULL
) ON [PRIMARY]
GO
