SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vTenderBasicContact]
AS
SELECT	TenderBasicContactID,
		COALESCE(dca.DefenseCounsel, pca.PlaintiffCounsel, b.Brand, dc.DefendantCompany, vf.VendorFirm) AS TenderBasicContact
FROM tblTenderBasicContact      tbc
	LEFT JOIN tblDefenseCounselAddress   dca ON tbc.DefenseCounselAddressID   = dca.DefenseCounselAddressID
	LEFT JOIN tblPlaintiffCounselAddress pca ON tbc.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	LEFT JOIN tblBrand                   b   ON tbc.BrandID                   = b.BrandID
	LEFT JOIN tblDefendantCompany        dc  ON tbc.DefendantCompanyID        = dc.DefendantCompanyID
	LEFT JOIN tblVendorFirm              vf  ON tbc.VendorFirmID              = vf.VendorFirmID

GO
GRANT VIEW DEFINITION ON  [dbo].[vTenderBasicContact] TO [base_user]
GRANT SELECT ON  [dbo].[vTenderBasicContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vTenderBasicContact] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vTenderBasicContact] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vTenderBasicContact] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vTenderBasicContact] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vTenderBasicContact] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vTenderBasicContact] TO [power_user]
GRANT SELECT ON  [dbo].[vTenderBasicContact] TO [power_user]
GRANT INSERT ON  [dbo].[vTenderBasicContact] TO [power_user]
GRANT DELETE ON  [dbo].[vTenderBasicContact] TO [power_user]
GRANT UPDATE ON  [dbo].[vTenderBasicContact] TO [power_user]
GO
