SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vDETAILS_ClaimLawsuitSmokingHistory]
AS
  SELECT ClaimLawsuitSmokingHistoryID
         , ClaimLawsuitID
         , SmokingStart
         , SmokingEnd
         , COALESCE (PACKSPERDAY, 0)    AS PacksPerDay
         , Username
         , NULL                         AS Edit
         , NULL                         AS [Delete]
         , CASE
             WHEN SMOKINGSTART IS NOT NULL
                  AND SMOKINGEND IS NOT NULL THEN DATEDIFF(dd, SMOKINGSTART, SMOKINGEND) / 365.0 * COALESCE (PACKSPERDAY, 0)
             ELSE 0
           END                          AS PackYears
         , CONVERT(NVARCHAR(250), NULL) AS SmokingHistoryNotes
         , NULL                         AS SmokingHistory
  FROM   dbo.tblClaimLawsuitSmokingHistory

GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimLawsuitSmokingHistory] TO [power_user]
GO
