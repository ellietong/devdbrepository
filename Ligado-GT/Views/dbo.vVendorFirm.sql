SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vVendorFirm]
AS

/****** 
KCIC NOTE: NOTE: for Ligado purposes, new inserts from a client site will be done via tblVendorFirm. 
vVendor firm should be used for all vendor reads / queries, as it utilizes the security database and tblVendorFirm together.
*******/

SELECT     distinct Upper(F.FirmDisplayName) as FirmDisplayName, FA.FirmID, 
                      FA.FirmAddress1, FA.FirmAddress2, FA.FirmCity, 
                      FA.FirmZip, FA.FirmMailZip, FA.FirmPhone, S.FullNameState, S.[State], S.StateID
FROM         [Ligado-ExtranetSecurity].dbo.Firm AS F INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.FirmAddress AS FA ON F.FirmID = FA.FirmID INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite AS FACS ON FA.FirmAddressID = FACS.FirmAddressID INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.ClientSite AS CS ON FACS.ClientSiteID = CS.ClientSiteID LEFT JOIN
                      [Ligado-ExtranetSecurity].dbo.tblState AS S ON FA.FirmStateID = S.StateID INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.RoleFirmAddressClientSite AS rfacs ON FACS.FirmAddressClientSiteID = rfacs.FirmAddressClientSiteID
WHERE     (CS.ClientID = 4) AND (rfacs.RoleID = 37) and PayeeAddress = 1
UNION
SELECT Upper(VendorFirm), VendorFirmID , Address1 , Address2 , City , ZipCode , NULL , NULL , S.FullNameState, S.[State], S.StateID
FROM tblVendorFirm VF INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.tblState AS S ON VF.StateID = S.StateID

		



GO
GRANT VIEW DEFINITION ON  [dbo].[vVendorFirm] TO [base_user]
GRANT SELECT ON  [dbo].[vVendorFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vVendorFirm] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vVendorFirm] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vVendorFirm] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vVendorFirm] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vVendorFirm] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vVendorFirm] TO [power_user]
GRANT SELECT ON  [dbo].[vVendorFirm] TO [power_user]
GRANT INSERT ON  [dbo].[vVendorFirm] TO [power_user]
GRANT DELETE ON  [dbo].[vVendorFirm] TO [power_user]
GRANT UPDATE ON  [dbo].[vVendorFirm] TO [power_user]
GO
