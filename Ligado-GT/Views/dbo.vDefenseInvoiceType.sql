SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vDefenseInvoiceType] AS
	select * from tblClaimantDocumentType where ClaimantDocumentTypeID in (40, 41)

GO
GRANT VIEW DEFINITION ON  [dbo].[vDefenseInvoiceType] TO [base_user]
GRANT SELECT ON  [dbo].[vDefenseInvoiceType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDefenseInvoiceType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDefenseInvoiceType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDefenseInvoiceType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDefenseInvoiceType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDefenseInvoiceType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDefenseInvoiceType] TO [power_user]
GRANT SELECT ON  [dbo].[vDefenseInvoiceType] TO [power_user]
GRANT INSERT ON  [dbo].[vDefenseInvoiceType] TO [power_user]
GRANT DELETE ON  [dbo].[vDefenseInvoiceType] TO [power_user]
GRANT UPDATE ON  [dbo].[vDefenseInvoiceType] TO [power_user]
GO
