SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions]
As

select distinct LawsuitID, DefenseCounselAddressID from
(
select LawsuitID, DefenseCounselAddressID from tblLawsuit
union
SELECT LawsuitID, dj.DefenseCounselAddressID FROM tblLawsuit l INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND l.CourtTypeID = dj.CourtTypeID
)Query
GO
GRANT VIEW DEFINITION ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [base_user]
GRANT SELECT ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [power_user]
GRANT SELECT ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [power_user]
GRANT INSERT ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [power_user]
GRANT DELETE ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [power_user]
GRANT UPDATE ON  [dbo].[vDefenseCounselAddressIDsForLawsuitsAndTheirJurisdictions] TO [power_user]
GO
