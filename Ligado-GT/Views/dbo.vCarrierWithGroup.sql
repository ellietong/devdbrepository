SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vCarrierWithGroup]
AS
SELECT CarrierID, CarrierName, c.CarrierGroupID, CarrierGroupName
FROM [ClientSQL].[KGA-Carrier].dbo.tblCarrier c
	LEFT JOIN [ClientSQL].[KGA-Carrier].dbo.tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID




GO
