SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vREPORT_DefenseByClaimant]
AS
SELECT     CAST(d.DocumentID AS varchar(50)) AS DocumentId, d.Comments, d.DocumentURL, d.DocumentTitle, d.CreatedBy, d.DateCreated, d.ModifiedBy, d.DateModified, 
                      d.Accepted, d.AcceptedBy, d.DateAccepted, di.ApprovedBy, di.DateApproved, dca.DefenseCounsel AS Firm, di.InvoiceNumber, di.Period, di.DatePayable, 
                      di.InvoiceNotes AS AdjustmentNotes, dis.Status, '' AS DefenseInvoiceFirm, c.SourceDate AS PaidDate, c.SourceNumber, 
                      dca.DefenseCounselID AS FirmID, COALESCE (ex.VendorFirm, '') AS VendorFirm, c.SourceDate, SUM(CASE WHEN COALESCE (CheckFundingSourceID, 1) = 3 AND 
                      COALESCE (sourceamount, 0) < 0 THEN (dicd.fees * - 1) ELSE COALESCE (dicd.fees, 0) END) + SUM(CASE WHEN COALESCE (CheckFundingSourceID, 1) = 3 AND 
                      COALESCE (sourceamount, 0) < 0 THEN (dicd.costs * - 1) ELSE COALESCE (dicd.costs, 0) END) - SUM(CASE WHEN COALESCE (CheckFundingSourceID, 1) = 3 AND 
                      COALESCE (sourceamount, 0) < 0 THEN (dicd.adjustments * - 1) ELSE COALESCE (dicd.adjustments, 0) END) AS InvoiceTotal, dicd.ClaimantPersonalInfoID, 
                      cpi.FullName AS ClaimantName, ex.VendorFirmID, dis.StatusID, di.DefenseCounselAddressID, di.DefenseInvoiceID
FROM         dbo.tblDocuments AS d LEFT OUTER JOIN
                      dbo.tblDefenseInvoice AS di ON d.DocumentID = di.DocumentID LEFT OUTER JOIN
                      dbo.tblDefenseInvoiceStatus AS dis ON di.StatusID = dis.StatusID LEFT OUTER JOIN
                      dbo.tblVendorFirm AS ex ON di.VendorFirmID = ex.VendorFirmID LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress AS dca ON di.DefenseCounselAddressID = dca.DefenseCounselAddressID LEFT OUTER JOIN
                      dbo.tblPaymentDefense AS pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID LEFT OUTER JOIN
                      dbo.tblDefInvClaimantDetail AS dicd ON dicd.DefenseInvoiceID = di.DefenseInvoiceID LEFT OUTER JOIN
                          (SELECT     CheckID, SourceNumber, SourceDate, SourceAmount, MatterID, CheckFundingTypeID, CheckFundingSourceID, NavClaimResolutionKey, CheckPDF
                            FROM          dbo.tblCheck
                            WHERE      (CheckFundingTypeID IN (1, 3))) AS c ON pd.CheckID = c.CheckID AND dicd.MatterID = c.MatterID LEFT OUTER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = dicd.ClaimantPersonalInfoID
WHERE     (d.ClaimantDocumentTypeID IN (40, 41))
GROUP BY CAST(d.DocumentID AS varchar(50)), d.Comments, d.DocumentURL, d.DocumentTitle, d.CreatedBy, d.DateCreated, d.ModifiedBy, d.DateModified, d.Accepted, 
                      d.AcceptedBy, d.DateAccepted, di.ApprovedBy, di.DateApproved, dca.DefenseCounsel, di.InvoiceNumber, di.Period, di.DatePayable, di.InvoiceNotes, dis.Status, 
                      c.SourceDate, c.SourceNumber, dca.DefenseCounselID, COALESCE (ex.VendorFirm, ''), c.SourceDate, dicd.ClaimantPersonalInfoID, FullName,
                       ex.VendorFirmID, dis.StatusID, di.DefenseCounselAddressID, di.DefenseInvoiceID
HAVING      (SUM(COALESCE (dicd.Fees, 0) + COALESCE (dicd.Costs, 0) - COALESCE (dicd.Adjustments, 0)) <> 0)




GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseByClaimant] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_DefenseByClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseByClaimant] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_DefenseByClaimant] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_DefenseByClaimant] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_DefenseByClaimant] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_DefenseByClaimant] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DefenseByClaimant] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_DefenseByClaimant] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_DefenseByClaimant] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_DefenseByClaimant] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_DefenseByClaimant] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "di"
            Begin Extent = 
               Top = 6
               Left = 296
               Bottom = 114
               Right = 539
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dis"
            Begin Extent = 
               Top = 6
               Left = 577
               Bottom = 84
               Right = 744
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ex"
            Begin Extent = 
               Top = 6
               Left = 782
               Bottom = 114
               Right = 949
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dca"
            Begin Extent = 
               Top = 6
               Left = 987
               Bottom = 114
               Right = 1211
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pd"
            Begin Extent = 
               Top = 6
               Left = 1249
               Bottom = 114
               Right = 1450
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dicd"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         ', 'SCHEMA', N'dbo', 'VIEW', N'vREPORT_DefenseByClaimant', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 114
               Left = 531
               Bottom = 222
               Right = 768
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 114
               Left = 285
               Bottom = 222
               Right = 493
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vREPORT_DefenseByClaimant', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vREPORT_DefenseByClaimant', NULL, NULL
GO
