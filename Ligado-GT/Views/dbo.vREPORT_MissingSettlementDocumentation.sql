SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vREPORT_MissingSettlementDocumentation]
AS
SELECT DISTINCT 
                      TOP (100) PERCENT cpi.ClaimantPersonalInfoID,
					  cpi.FirstName,
					  cpi.LastName,
					  cpi.MiddleName,
					  CONVERT(DATE, cpi.BirthDate) AS DOB, 
					  CONVERT(DATE, 
                      cpi.DeceasedDate) AS DOD, 
					  cs.SettlementDate, 
					  cs.SettlementAmount, 
					  sg.SettlementGroup, 
					  cs.SettlementGroupID, 
					  cl.ClaimLawsuitID, 
					  cl.LawsuitID, 
                      COALESCE (sd.ClaimantIDed, 0) AS ClaimantIDed, 
					  CASE WHEN socialsecuritynumber IS NULL THEN 0 ELSE 1 END AS SSN, 
					  CASE WHEN birthdate IS NULL 
                      THEN 0 ELSE 1 END AS BirthDate, 
					  COALESCE (sd.DxMedicals, 0) AS DxMedicals, 
					  CASE WHEN cd1.DiseaseID IS NULL THEN 0 ELSE 1 END AS DiseaseID, 
                      CASE WHEN cd1.DiseaseDiagnosisDate IS NULL THEN 0 ELSE 1 END AS DiagnosisDate, 
					  CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE (sd.DODCert, 0) END AS DODCert, 
					  CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE (sd.EstatePapers, 0) END AS Estatepaper, COALESCE (sd.ProductID, 0) 
                      AS ProductID, COALESCE (p.productentered, 0) AS ProductEntered, 
					  COALESCE (sd.EmployExpoHist, 0) AS EmployExpoHist, 
					  COALESCE (sd.SettlementConfirmation, 0) 
                      AS SettlementConfirmation, 
					  CASE WHEN DeceasedDate IS NOT NULL AND LegalCapacityid IS NULL THEN 0 ELSE 1 END AS LegalCapacity, 
                      CASE WHEN (d_1.ClaimantPersonalInfoID IS NOT NULL OR
                      ExecutedReleaseDate IS NOT NULL) THEN 1 ELSE 0 END AS ReleaseReceived
FROM         dbo.tblClaim AS c INNER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                      dbo.tblClaimSettlement AS cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID INNER JOIN
                      dbo.tblClaimSettlementGroup AS sg ON cs.SettlementGroupID = sg.SettlementGroupID LEFT OUTER JOIN
                      dbo.tblClaimSettlementDocumentation AS sd ON cl.ClaimLawsuitID = sd.ClaimLawsuitID LEFT OUTER JOIN
					  (SELECT cd.* FROM tblClaimDisease cd
					  INNER JOIN tblDisease d1 ON cd.DiseaseID = d1.DiseaseID
					  WHERE  (d1.HighLevelDiseaseID <> 5)) cd1 ON c.ClaimID = cd1.ClaimID LEFT JOIN
                      --dbo.tblSettlementDocumentation AS sd ON sd.SetPymtID = spd.SetPymtID LEFT OUTER JOIN
                      --    (SELECT     cd1.ClaimDiseaseID, cd1.ClaimID, cd1.DiseaseID, cd1.PrimaryDisease, cd1.DiseaseDiagnosisDate, cd1.DoctorID, cd1.SPUsername, 
                      --                             cd1.DiseaseID_OLD, cd1.IsDefenseDisease
                      --      FROM          dbo.tblClaimDisease AS cd1 INNER JOIN
                      --                             dbo.tblDisease AS d1 ON d1.DiseaseID = cd1.DiseaseID
                      --      WHERE      (cd1.PrimaryDisease = 1) AND (d1.HighLevelDiseaseID <> 5)) AS cdi ON cdi.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatusTracking AS cls ON cls.ClaimLawsuitID = cl.ClaimLawsuitID LEFT OUTER JOIN
                          (SELECT     ClaimantRelatedPartyID, ClaimantPersonalInfoID, FirstName, MiddleName, LastName, RelationshipID, LegalCapacityID, RPAddress1, 
                                                   RPAddress2, RPCity, StateID, RPZip, RPMailZip, RPPhone, RPExtension, RPSocialSecurityNumber, RPTIN, SPUserName
                            FROM          dbo.tblClaimantRelatedParty
                            WHERE      (LegalCapacityID IN (1, 2, 3, 4, 5, 6, 7, 8, 15, 17, 30, 31))) AS crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                          (SELECT     mcd.ClaimantPersonalInfoID
                            FROM          dbo.tblMultiClaimantDocument AS mcd INNER JOIN
                                                   dbo.tblDocuments AS d ON d.DocumentID = mcd.DocumentID
                            WHERE      (d.ClaimantDocumentTypeID = 31)
                            GROUP BY mcd.ClaimantPersonalInfoID) AS d_1 ON d_1.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                          (SELECT     ce.ClaimantPersonalInfoID, CASE WHEN cp.ProductID IS NULL OR
                                                   cp.StartDate IS NULL OR
                                                   cp.EndDate IS NULL THEN 0 ELSE 1 END AS productentered
                            FROM          dbo.tblClaimantEmployment AS ce LEFT OUTER JOIN
                                                   dbo.tblClaimantProduct AS cp ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
                            WHERE      (ce.IsPrimary = 1)-- AND (cp.PrimaryProduct = 1)
                            GROUP BY ce.ClaimantPersonalInfoID, CASE WHEN cp.ProductID IS NULL OR
                                                   cp.StartDate IS NULL OR
                                                   cp.EndDate IS NULL THEN 0 ELSE 1 END) AS p ON cpi.ClaimantPersonalInfoID = p.ClaimantPersonalInfoID
WHERE     (cls.IsPrimaryStatus = 1) AND (cls.ClaimLawsuitStatusID IN (25)) AND (YEAR(cs.SettlementDate) >= 2010) --AND (cdi.PrimaryDisease = 1)


GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_MissingSettlementDocumentation] TO [power_user]
GO
