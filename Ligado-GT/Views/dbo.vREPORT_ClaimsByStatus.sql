SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vREPORT_ClaimsByStatus]
AS
select * 
, CASE WHEN LitigationCount = 1 OR InventoryCount = 1 THEN 1 ELSE 0 END AS Subtotal_Active
, CASE WHEN InactiveCount = 1 OR NoServeCount = 1 THEN 1 ELSE 0 END AS Subtotal_Inactive
, CASE WHEN DismissedCount = 1 OR SettlementCount = 1 OR WithdrawnCount = 1 THEN 1 ELSE 0 END AS Subtotal_Closed
, 1 As TOTAL

from(

select distinct
tcl.ClaimLawsuitID
, tcl.ClaimLawsuitTypeID
, tclst.ClaimLawsuitStatusID
, tc.MatterID
, tl.StateID
, ts.FullNameState
, pca.PlaintiffCounsel
, CASE WHEN tclst.ClaimLawsuitStatusID = 41 THEN 1 ELSE 0 END AS LitigationCount
, CASE WHEN tclst.ClaimLawsuitStatusID = 42 THEN 1 ELSE 0 END AS InventoryCount
, CASE WHEN tclst.ClaimLawsuitStatusID = 29 THEN 1 ELSE 0 END AS InactiveCount
, CASE WHEN tclst.ClaimLawsuitStatusID = 43 THEN 1 ELSE 0 END AS NoServeCount
, CASE WHEN tclst.ClaimLawsuitStatusID IN (15, 16, 27) THEN 1 ELSE 0 END AS DismissedCount
, CASE WHEN tclst.ClaimLawsuitStatusID IN (23, 25) THEN 1 ELSE 0 END AS SettlementCount
, CASE WHEN tclst.ClaimLawsuitStatusID = 44 THEN 1 ELSE 0 END AS WithdrawnCount
 from tblClaimLawsuit tcl 
 inner join (select lawsuitid, stateid, NationalPlaintiffCounselID from tbllawsuit) tl on tcl.LawsuitID = tl.LawsuitID
 inner join (select claimid, matterid from tblClaim) tc on tcl.ClaimID = tc.ClaimID
 inner join (select claimlawsuitid, claimlawsuitstatusid from tblClaimLawsuitStatusTracking where isprimarystatus = 1) tclst on tcl.ClaimLawsuitID = tclst.ClaimLawsuitID
 inner join (select stateid, fullnamestate from tblstate) ts on tl.StateID = ts.StateID
 inner join (select plaintiffcounseladdressid, PlaintiffCounsel from tblPlaintiffCounselAddress) pca ON tl.NationalPlaintiffCounselID = pca.PlaintiffCounselAddressID
)AllStatuses

GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_ClaimsByStatus] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_ClaimsByStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_ClaimsByStatus] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_ClaimsByStatus] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_ClaimsByStatus] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_ClaimsByStatus] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_ClaimsByStatus] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_ClaimsByStatus] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_ClaimsByStatus] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_ClaimsByStatus] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_ClaimsByStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_ClaimsByStatus] TO [power_user]
GO
