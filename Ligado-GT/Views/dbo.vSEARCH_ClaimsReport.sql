USE [Ligado-gt_prod]
GO


--DROP VIEW [dbo].[vSEARCH_ClaimsReport]
--GO
--24 s,21s
--83191
--[dbo].[vSEARCH_ClaimsReport_20150227]
--83191
--28 s,32
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[vSEARCH_ClaimsReport]
AS 
WITH dedup_defjur AS (
	SELECT DISTINCT dejur.DefenseCounselAddressID,dejur.JurisdictionID,dejur.StateID,LawsuitID
	 FROM dbo.TBLLAWSUIT(NOLOCK) laws
    LEFT JOIN dbo.TBLDEFENSEJURISDICTION(NOLOCK)  dejur  
	ON dejur.StateID = laws.StateID
	AND dejur.JurisdictionID = laws.JurisdictionID
	WHERE dejur.DefenseCounselAddressID IS NOT NULL
)	
, lawsuitinfo AS (
   SELECT   claws.CLAIMLAWSUITID
           , ACTIVETRIALDATE
           , CLAIMID
           , claws.LAWSUITID
           , ISMDL
           , CLAIMLAWSUITSTATUS
           , cllawsttrak.CLAIMLAWSUITSTATUSID
           , DOCKETNUMBER
           , laws.SERVICEDATE
           , FILEDATE
           , laws.RECEIVEDDATE
           , st.[STATE]
           , st.STATEID
           , JURISDICTION
           , jurs.JURISDICTIONID
           , PROCESSEDDATE
           , STATUSDATE  
           , plntcnsladdr.PLAINTIFFCOUNSEL  AS PLAINTIFFCOUNSEL
           , plntcnsladdr.PLAINTIFFCOUNSELID AS PLAINTIFFCOUNSELID         
           , ALLEGATIONTYPEID
			, CLIENTFILEDATE
			, CLAIMLAWSUITNOTES
			, DEFENSECOUNSEL          AS LawsuitDefenseCounsel
            , laws.DEFENSECOUNSELADDRESSID AS LawsuitDefenseCounselAddressID
            , DEFENSECOUNSELID        AS LawsuitDefenseCounselID	
			,nationalplaintiffCounselInfo.PlaintiffCounselAddressID	as nationalPlaintiffCounselAddressID
			,nationalplaintiffCounselInfo.PlaintiffCounsel 			as nationalPlaintiffCounsel
			,nationalplaintiffCounselInfo.PlaintiffCounselID		as nationalPlaintiffCounselID
    FROM 
	  dbo.TBLCLAIMLAWSUIT(NOLOCK) claws
	  JOIN TBLCLAIMLAWSUITSTATUSTRACKING(NOLOCK) cllawsttrak
	  ON cllawsttrak.ClaimLawsuitID = claws.ClaimLawsuitID
	  AND ISNULL(ISPRIMARYFORCLAIM,0) = 1
	  AND ISNULL(ISPRIMARYSTATUS,0) = 1	
	LEFT JOIN dbo.TBLCLAIMLAWSUITSTATUS(NOLOCK) cllawstat
	ON cllawstat.CLAIMLAWSUITSTATUSID = cllawsttrak.ClaimLawsuitStatusID -- lawsuitstatusinfo
	LEFT JOIN dbo.TBLALLEGATION(NOLOCK) alleg  -- lawsuitstatusinfo
	ON alleg.CLAIMLAWSUITID = cllawsttrak.CLAIMLAWSUITID		
	LEFT JOIN dbo.TBLLAWSUIT(NOLOCK) laws -- LawsuitLevelInfo
	ON laws.LawsuitID = claws.LawsuitID
	LEFT JOIN dbo.TBLSTATE(NOLOCK) st
	ON st.StateID = laws.StateID
	LEFT JOIN dbo.TBLJURISDICTION(NOLOCK) jurs
	ON jurs.JurisdictionID = laws.JurisdictionID
	LEFT JOIN dbo.TBLPLAINTIFFCOUNSELADDRESS(NOLOCK) plntcnsladdr
	ON plntcnsladdr.PlaintiffCounselAddressID = laws.PlaintiffCounselAddressID
	LEFT JOIN dbo.tblPlaintiffCounselAddress(NOLOCK) nationalplaintiffCounselInfo 
	ON nationalplaintiffCounselInfo.PlaintiffCounselAddressID =  laws.NationalPlaintiffCounselID
	LEFT JOIN dbo.TBLDEFENSECOUNSELADDRESS(NOLOCK) decnsladdr
	ON decnsladdr.DefenseCounselAddressID = laws.DefenseCounselAddressID -- LawsuitLevelInfo	
	WHERE 
	 ISNULL(ISPRIMARYFORCLAIM,0) = 1
	 AND ISNULL(ISPRIMARYSTATUS,0) = 1	
)
, prodInfo AS  ( 
 SELECT CLAIMANTEMPLOYMENTID AS prodInfoClaimentEmploymentId
       , MINSTARTDATE       AS EarliestStartDate
       , MINIMUMPRODUCTID
       , PRODUCTNOTES
	FROM   (SELECT CLAIMANTEMPLOYMENTID
               , MINSTARTDATE
               , MIN(PRODUCTID) AS MinimumProductID
        FROM   (SELECT CLAIMANTEMPLOYMENTID,PRODUCTID,MINSTARTDATE
                FROM   (SELECT CLAIMANTEMPLOYMENTID
                               , MIN(STARTDATE) AS MinStartDate
                        FROM   TBLCLAIMANTPRODUCT(nolock)
                        GROUP  BY CLAIMANTEMPLOYMENTID)MinimumStartDates
                       LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS sdEmploymentID
                                               , STARTDATE          AS sdStartDate
                                               , PRODUCTID
                                        FROM   TBLCLAIMANTPRODUCT(nolock))StartDatesAndProductIDs
                                    ON ( MinimumStartDates.CLAIMANTEMPLOYMENTID = StartDatesAndProductIDs.SDEMPLOYMENTID
                                         AND MinimumStartDates.MINSTARTDATE = StartDatesAndProductIDs.SDSTARTDATE ))MinStartDatesAndProductIDs
        GROUP  BY CLAIMANTEMPLOYMENTID, MINSTARTDATE)EarliestProductForEmployment
       LEFT OUTER JOIN (SELECT CLAIMANTEMPLOYMENTID AS nEmploymentID
                               , STARTDATE          AS nStartDate
            , PRODUCTID          AS nProductID
            , PRODUCTNOTES
     FROM   TBLCLAIMANTPRODUCT(nolock))NotesQuery
 ON ( EarliestProductForEmployment.CLAIMANTEMPLOYMENTID = NotesQuery.NEMPLOYMENTID
      AND EarliestProductForEmployment.MINSTARTDATE = NotesQuery.NSTARTDATE
      AND EarliestProductForEmployment.MINIMUMPRODUCTID = NotesQuery.NPRODUCTID )	  
  ) 	
,ClaimLevelInfo as (
	SELECT  
                 claim.CLAIMID
               , MATTER
               , CLAIMSEQUENCE
               , claim.CLAIMANTPERSONALINFOID
               , FIRSTNAME
               , LASTNAME
               , BIRTHDATE
               , DECEASEDDATE
               , SocialSecurityNumber
               , OCCUPATION
               , HIGHLEVELDISEASE 
               , PRODUCTNOTES
               , STATEID
               , hldis.HIGHLEVELDISEASEID
               , claim.MATTERID
			   ,epemp.EarliestStartDate
			   ,clemp.IsSecondaryExposure
from dbo.TBLCLAIM(nolock) claim -- ClaimLevelInfo
	--on claim.ClaimID = claws.ClaimID	
	left join dbo.TBLMATTER(nolock) mtr
	on mtr.MatterID = claim.MatterID
	left join dbo.TBLCLAIMSEQUENCE(nolock) cseq
	on cseq.ClaimSequenceID = claim.ClaimSequenceID
	left join dbo.TBLCLAIMDISEASE(nolock) cdis
	on cdis.ClaimID = claim.ClaimID
	and isnull(cdis.PRIMARYDISEASE,0) = 1
	left join dbo.TBLDISEASE dis
	on cdis.DiseaseID = dis.DiseaseID
	left join dbo.TBLHIGHLEVELDISEASE(nolock) hldis
	on hldis.HighLevelDiseaseID = dis.Highleveldiseaseid -- ClaimLevelInfo	
	left join dbo.TBLCLAIMANTPERSONALINFO(nolock) Clmnt --ClaimantLevelInfo
	on clmnt.ClaimantPersonalInfoID = claim.ClaimantPersonalInfoID
	left join dbo.TBLCLAIMANTEMPLOYMENT(nolock) clemp
	on clemp.ClaimantPersonalInfoID = clmnt.ClaimantPersonalInfoID
    and claim.CLAIMANTPERSONALINFOID = clemp.ClaimantPersonalInfoID 
	and ISPRIMARY = 1
	left join dbo.TBLOCCUPATION(nolock) occ
	on occ.OccupationID = clemp.OccupationID
	left join prodInfo epemp
	on clemp.CLAIMANTEMPLOYMENTID = epemp.PRODINFOCLAIMENTEMPLOYMENTID --ClaimantLevelInfo
)
, settlementPaymentInfo as (
SELECT CLAIMSETTLEMENTID AS SettlementPaymentInfoClaimSettlementID
	    , MIN(CHECKID)    AS EarliestCheckID
	FROM   dbo.TBLPAYMENTSETTLEMENT(nolock)
	    LEFT OUTER JOIN (SELECT CHECKID AS checkInfoCheckID
	                        FROM   dbo.TBLCHECK) checkInfo
	                    ON ( dbo.TBLPAYMENTSETTLEMENT.CHECKID = checkInfo.CHECKINFOCHECKID )
	GROUP  BY CLAIMSETTLEMENTID) 
	
, CliInfo as (
	SELECT claws.claimlawsuitid
	, activetrialdate
	, claimid
	, lawsuitid
	, ismdl
	, settlementdate
	, PAIDDATE
	, settlementamount
	, sourcedate
	, ClaimLawsuitNotes
	, ClaimLawsuitNotes AS TrialNotes	
	from dbo.TBLCLAIMLAWSUIT(nolock) claws -- CLInfo		
		left join dbo.TBLCLAIMSETTLEMENT(nolock) clsett
		on claws.CLAIMLAWSUITID = clsett.CLAIMLAWSUITID 	
		and isnull(ISPRIMARYFORCLAIM,0) = 1
		left join settlementPaymentInfo settinfo		
		on clsett.ClaimSettlementID = settinfo.SettlementPaymentInfoClaimSettlementID
		left join dbo.TBLCHECK(nolock) chk
		on settinfo.EarliestCheckID = chk.CheckID  -- CLInfo
  where  isnull(ISPRIMARYFORCLAIM,0) = 1
)


SELECT distinct
 cliInfo.CLAIMLAWSUITID                                      AS ClaimLawsuitID
       , CLAIMANTPERSONALINFOID                            AS ClaimantPersonalInfoID
       , cliInfo.ACTIVETRIALDATE                                   AS ActiveTrialDate
       , cliInfo.CLAIMID                                           AS ClaimID
       , CLAIMLAWSUITSTATUS                                AS ClaimLawsuitStatus
       , MATTER                                            AS Matter
       , CLAIMSEQUENCE                                     AS ClaimSequence
       , LASTNAME + ', ' + FIRSTNAME                       AS FullName
       , BIRTHDATE                                         AS BirthDate
       , DECEASEDDATE                                      AS DeceasedDate
       , ( CASE
             WHEN SocialSecurityNumber IS NOT NULL THEN 'XXX-XX-' + right(SocialSecurityNumber,4)
             ELSE NULL
           END )                                           SocialSecurityNumber
       , OCCUPATION                                        AS Occupation
       , HIGHLEVELDISEASE                                  AS HighLevelDisease
       , DOCKETNUMBER                                      AS DocketNumber
       , SERVICEDATE                                       AS ServiceDate
       , COALESCE(CLIENTFILEDATE, FILEDATE, RECEIVEDDATE)  AS FileDate
       , [STATE]                                           AS State
       , JURISDICTION                                      AS Jurisdiction
       , PROCESSEDDATE                                     AS ProcessedDate
       , STATUSDATE                                        AS StatusDate
       , PRODUCTNOTES                                      AS ProductNotes
       , EarliestStartDate                                 AS ProductExposureStart
       , SETTLEMENTDATE                                    AS SettlementDate
	   , PAIDDATE										   As SettlementPaidDate
       , SETTLEMENTAMOUNT                                  AS SettlementAmount
       , SOURCEDATE --AS SettlementPaidDate -- Date paid; source date is the source date of the first check that is cut for a settlement
       , PLAINTIFFCOUNSEL                                  AS PlaintiffCounsel
       , CLAIMLAWSUITSTATUSID                              AS ClaimLawsuitStatusID
       , lawsuit.STATEID                                   AS StateID
       , lawsuit.JURISDICTIONID                            AS JurisdictionID
       , cliinfo.ISMDL                                     AS IsMDL
       , HIGHLEVELDISEASEID                                AS HighLevelDiseaseID
       , ALLEGATIONTYPEID                                  AS AllegationTypeID
       , PLAINTIFFCOUNSELID                                AS PlaintiffCounselID
       , MATTERID                                          AS MatterID
       , LAWSUITDEFENSECOUNSELADDRESSID                    AS DefenseCounselAddressID
       , defjur.DefenseCounselAddressID                     AS DefenseCounselAddressID2
       , 0                                                 AS ClaimLawsuitStatusGroupID --NOTE: this is a dummy field because Marley doesn't require it but we still need the field. SK, 1/27/2014 (launch day)
       , CONVERT(INT, NULL)                                AS PersonID
       , CONVERT(INT, NULL)                                AS BrandID
       , CONVERT(NVARCHAR(50), NULL)                       AS Brand
       , CONVERT(INT, NULL)                                AS BankruptcyStayID
       , CONVERT(NVARCHAR(50), NULL)                       AS BankruptcyStay
       , CONVERT(INT, NULL)                                AS ClaimLawsuitTypeID
       , CONVERT(NVARCHAR(50), NULL)                       AS ClaimLawsuitType
       , CONVERT(NVARCHAR(100), NULL)                      AS JobsiteCity
       , CONVERT(INT, NULL)                                AS JobsiteStateID
       , CONVERT(NVARCHAR(1000), NULL)                     AS Jobsite
       , CONVERT(INT, NULL)                                AS BusinessCaseID
       , CONVERT(NVARCHAR(50), NULL)                       AS BusinessCase
	   , lawsuit.nationalPlaintiffCounselAddressID
	   , lawsuit.nationalPlaintiffCounselID
	   , lawsuit.nationalPlaintiffCounsel
	   , cliinfo.ClaimLawsuitNotes
	   , TrialNotes -- FYI TrialNotes is literally the same as ClaimLawsuitNotes; it is here so that GSX can refer to it this way in their CSV exports. 
	   ,ISNULL(claim.IsSecondaryExposure,'')				AS IsSecondaryExposure

	from cliInfo
	join lawsuitinfo lawsuit
	on cliInfo.LAWSUITID = lawsuit.LAWSUITID
	and cliInfo.claimlawsuitid = lawsuit.CLAIMLAWSUITID
	and cliInfo.claimid = lawsuit.CLAIMID
	join ClaimLevelInfo claim
	on  cliInfo.claimid = claim.CLAIMID 
	left join dedup_defjur defjur
	on defjur.JurisdictionID = lawsuit.JURISDICTIONID
	and defjur.StateID = lawsuit.STATEID
	and defjur.LawsuitID = lawsuit.LawsuitID
--	where firstname is not null and lastname is not null

--GROUP  BY cliInfo.CLAIMLAWSUITID
--          , CLAIMANTPERSONALINFOID
--          , cliInfo.CLAIMID
--          , cliInfo.ISMDL
--          , cliInfo.ACTIVETRIALDATE
--          , CLAIMLAWSUITSTATUS
--          , MATTER
--          , CLAIMSEQUENCE
--          , FIRSTNAME
--          , LASTNAME
--          , BIRTHDATE
--          , DECEASEDDATE
--          , SSN4DIGIT
--          , OCCUPATION
--          , HIGHLEVELDISEASE
--          , lawsuit.ACTIVETRIALDATE
--          , DOCKETNUMBER
--		  , CLIENTFILEDATE
--          , SERVICEDATE
--          , FILEDATE
--          , RECEIVEDDATE
--          , [STATE]
--          , JURISDICTION
--          , PROCESSEDDATE
--          , STATUSDATE
--          , PRODUCTNOTES
--          , claim.EarliestStartDate 
--          , SETTLEMENTDATE
--		  , cliinfo.PAIDDATE
--          , SETTLEMENTAMOUNT
--          , SOURCEDATE
--          , PLAINTIFFCOUNSEL
--          , CLAIMLAWSUITSTATUSID
--          , lawsuit.STATEID
--          , lawsuit.JURISDICTIONID
--          , HIGHLEVELDISEASEID
--          , ALLEGATIONTYPEID
--          , PLAINTIFFCOUNSELID
--          , MATTERID
--          , lawsuit.LAWSUITDEFENSECOUNSEL
--          , lawsuit.LAWSUITDEFENSECOUNSELID
--          , lawsuit.LAWSUITDEFENSECOUNSELADDRESSID
--          , defjur.DefenseCounselAddressID    
--		  , cliinfo.ClaimLawsuitNotes
--		  , TrialNotes





GO


