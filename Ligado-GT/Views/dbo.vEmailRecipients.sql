SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vEmailRecipients]
AS
SELECT DISTINCT 
                      [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite.FirmAddressID, [Ligado-ExtranetSecurity].dbo.Person.Email, [Ligado-ExtranetSecurity].dbo.Role.RoleName, 
                      [Ligado-ExtranetSecurity].dbo.RolePersonFirmAddressClientSite.RoleID, [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite.ClientSiteID
FROM         [Ligado-ExtranetSecurity].dbo.Person INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite ON 
                      [Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite.PersonID = [Ligado-ExtranetSecurity].dbo.Person.PersonID INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite ON 
                      [Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite.FirmAddressClientSiteID = [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite.FirmAddressClientSiteID INNER
                       JOIN
                      [Ligado-ExtranetSecurity].dbo.RolePersonFirmAddressClientSite ON 
                      [Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite.PersonFirmAddressClientSiteID = [Ligado-ExtranetSecurity].dbo.RolePersonFirmAddressClientSite.PersonFirmAddressClientSiteID
                       INNER JOIN
                      [Ligado-ExtranetSecurity].dbo.Role ON [Ligado-ExtranetSecurity].dbo.RolePersonFirmAddressClientSite.RoleID = [Ligado-ExtranetSecurity].dbo.Role.RoleID
WHERE     ([Ligado-ExtranetSecurity].dbo.Role.IsNotification = 1) AND ([Ligado-ExtranetSecurity].dbo.FirmAddressClientSite.ClientSiteID = dbo.fn_GetClientSiteID())


GO
GRANT VIEW DEFINITION ON  [dbo].[vEmailRecipients] TO [base_user]
GRANT SELECT ON  [dbo].[vEmailRecipients] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vEmailRecipients] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vEmailRecipients] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vEmailRecipients] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vEmailRecipients] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vEmailRecipients] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vEmailRecipients] TO [power_user]
GRANT SELECT ON  [dbo].[vEmailRecipients] TO [power_user]
GRANT INSERT ON  [dbo].[vEmailRecipients] TO [power_user]
GRANT DELETE ON  [dbo].[vEmailRecipients] TO [power_user]
GRANT UPDATE ON  [dbo].[vEmailRecipients] TO [power_user]
GO
