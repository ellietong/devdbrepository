SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Test_View]
AS
SELECT        TOP (10) dbo.tblDocuments.DocumentID, dbo.tblDocuments.AmazonKey, dbo.tblDocuments.ClaimantDocumentTypeID, 
                         dbo.tblClaimantPersonalInfo.FirstName, dbo.tblClaimantPersonalInfo.MiddleName, dbo.tblClaimantPersonalInfo.LastName, dbo.tblClaim.IsCurrentClaim, 
                         dbo.tblClaimLawsuitStatusTracking.ClaimLawsuitStatusID, dbo.vDOCUMENT_Locked.Locked, dbo.tblClaimantDocumentType.ClaimantDocumentType
FROM            dbo.tblDocuments INNER JOIN
                         dbo.tblMultiClaimantDocument ON dbo.tblDocuments.DocumentID = dbo.tblMultiClaimantDocument.DocumentID INNER JOIN
                         dbo.tblClaim ON dbo.tblMultiClaimantDocument.ClaimantPersonalInfoID = dbo.tblClaim.ClaimantPersonalInfoID INNER JOIN
                         dbo.tblClaimLawsuit ON dbo.tblClaim.ClaimID = dbo.tblClaimLawsuit.ClaimID INNER JOIN
                         dbo.tblLawsuit ON dbo.tblClaimLawsuit.LawsuitID = dbo.tblLawsuit.LawsuitID INNER JOIN
                         dbo.tblDefenseJurisdiction ON dbo.tblLawsuit.StateID = dbo.tblDefenseJurisdiction.StateID INNER JOIN
                         dbo.tblClaimantPersonalInfo ON dbo.tblMultiClaimantDocument.ClaimantPersonalInfoID = dbo.tblClaimantPersonalInfo.ClaimantPersonalInfoID AND 
                         dbo.tblClaim.ClaimantPersonalInfoID = dbo.tblClaimantPersonalInfo.ClaimantPersonalInfoID INNER JOIN
                         dbo.tblClaimLawsuitStatusTracking ON dbo.tblClaimLawsuit.ClaimLawsuitID = dbo.tblClaimLawsuitStatusTracking.ClaimLawsuitID INNER JOIN
                         dbo.vDOCUMENT_Locked ON dbo.tblDocuments.DocumentID = dbo.vDOCUMENT_Locked.DocumentID INNER JOIN
                         dbo.tblClaimantDocumentType ON dbo.tblDocuments.ClaimantDocumentTypeID = dbo.tblClaimantDocumentType.ClaimantDocumentTypeID
GO
GRANT VIEW DEFINITION ON  [dbo].[Test_View] TO [base_user]
GRANT SELECT ON  [dbo].[Test_View] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[Test_View] TO [Ligado_User]
GRANT SELECT ON  [dbo].[Test_View] TO [Ligado_User]
GRANT INSERT ON  [dbo].[Test_View] TO [Ligado_User]
GRANT DELETE ON  [dbo].[Test_View] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[Test_View] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[Test_View] TO [power_user]
GRANT SELECT ON  [dbo].[Test_View] TO [power_user]
GRANT INSERT ON  [dbo].[Test_View] TO [power_user]
GRANT DELETE ON  [dbo].[Test_View] TO [power_user]
GRANT UPDATE ON  [dbo].[Test_View] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[44] 4[7] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tblDocuments"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 383
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblClaim"
            Begin Extent = 
               Top = 252
               Left = 450
               Bottom = 558
               Right = 682
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblMultiClaimantDocument"
            Begin Extent = 
               Top = 16
               Left = 320
               Bottom = 246
               Right = 552
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblDefenseJurisdiction"
            Begin Extent = 
               Top = 52
               Left = 1123
               Bottom = 388
               Right = 1350
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "tblLawsuit"
            Begin Extent = 
               Top = 214
               Left = 774
               Bottom = 478
               Right = 1013
            End
            DisplayFlags = 280
            TopColumn = 13
         End
         Begin Table = "tblClaimLawsuit"
            Begin Extent = 
               Top = 28
               Left = 634
               Bottom = 158
               Right = 856
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblClaimantPersonalInfo"
            Begin Extent = 
               Top = 384
               Left = 38
               Bottom = 560
        ', 'SCHEMA', N'dbo', 'VIEW', N'Test_View', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'       Right = 288
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblClaimLawsuitStatusTracking"
            Begin Extent = 
               Top = 298
               Left = 1051
               Bottom = 520
               Right = 1300
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vDOCUMENT_Locked"
            Begin Extent = 
               Top = 6
               Left = 894
               Bottom = 130
               Right = 1064
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblClaimantDocumentType"
            Begin Extent = 
               Top = 296
               Left = 360
               Bottom = 498
               Right = 590
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 3630
         Width = 1410
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2400
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2730
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'Test_View', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'Test_View', NULL, NULL
GO
