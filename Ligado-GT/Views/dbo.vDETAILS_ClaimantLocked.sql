SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vDETAILS_ClaimantLocked]
AS
SELECT claimantpersonalinfoid
       , Sum(locked) AS Locked
FROM   (SELECT c.claimantpersonalinfoid
               , COALESCE(Count(clst.claimlawsuitstatustrackingid), 0) AS Locked
        FROM   tblclaim c-- ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
               INNER JOIN tblclaimlawsuit cl
                       ON c.claimid = cl.claimid
               INNER JOIN tbllawsuit l
                       ON cl.lawsuitid = l.lawsuitid
               --INNER JOIN tblMultiClaimantDocument mcd			ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
               LEFT JOIN (SELECT *
                          FROM   tblclaimlawsuitstatustracking
                          WHERE  claimlawsuitstatusid IN ( 15, 16, 17 )
                             AND isprimarystatus = 1)clst
                      ON cl.claimlawsuitid = clst.claimlawsuitid
        WHERE  COALESCE(c.iscurrentclaim, 0) = 1
           AND COALESCE(isprimaryforclaim, 0) = 1
        GROUP  BY c.claimantpersonalinfoid
        --HAVING COUNT(c.ClaimID)> 0
        UNION
        --Check for settlements
        SELECT c.claimantpersonalinfoid
               , COALESCE(Count(DISTINCT paymentsettlementid), 0)
        FROM   tblclaim c
               LEFT JOIN tblclaimlawsuit cl
                      ON c.claimid = cl.claimid
               LEFT JOIN tblclaimsettlement cs
                      ON cl.claimlawsuitid = cs.claimlawsuitid
               LEFT JOIN tblpaymentsettlement ps
                      ON cs.claimsettlementid = ps.claimsettlementid
        GROUP  BY c.claimantpersonalinfoid) AS lock
GROUP  BY claimantpersonalinfoid 

GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantLocked] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantLocked] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantLocked] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantLocked] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantLocked] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantLocked] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantLocked] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantLocked] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantLocked] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantLocked] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantLocked] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantLocked] TO [power_user]
GO
