SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vDefenseInvoiceSource] AS
select * from tblDocumentSource
GO
GRANT VIEW DEFINITION ON  [dbo].[vDefenseInvoiceSource] TO [base_user]
GRANT SELECT ON  [dbo].[vDefenseInvoiceSource] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDefenseInvoiceSource] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDefenseInvoiceSource] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDefenseInvoiceSource] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDefenseInvoiceSource] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDefenseInvoiceSource] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDefenseInvoiceSource] TO [power_user]
GRANT SELECT ON  [dbo].[vDefenseInvoiceSource] TO [power_user]
GRANT INSERT ON  [dbo].[vDefenseInvoiceSource] TO [power_user]
GRANT DELETE ON  [dbo].[vDefenseInvoiceSource] TO [power_user]
GRANT UPDATE ON  [dbo].[vDefenseInvoiceSource] TO [power_user]
GO
