SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vSEARCH_LegalBill]
AS
WITH Unpaid AS (SELECT     i.InvoiceID, CASE WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 0 THEN 0 WHEN IsFullyPaid IS NULL AND 
                                                             NavPaidInvoice = 1 THEN 1 ELSE IsFullyPaid END AS IsFullyPaid
                                      FROM         dbo.m_tblLegalBillInvoices AS i LEFT OUTER JOIN
                                                             dbo.tblPaymentLegalBill AS plb ON i.InvoiceID = plb.InvoiceID
                                      GROUP BY i.InvoiceID, CASE WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 0 THEN 0 WHEN IsFullyPaid IS NULL AND 
                                                             NavPaidInvoice = 1 THEN 1 ELSE IsFullyPaid END)
    SELECT     TOP (100) PERCENT i.InvoiceID, i.DefenseCounselAddressID, d.DefenseCounselID, d.DefenseCounsel, m.Matter, COALESCE (i.OriginalInvoiceNumber, 'Unknown') 
                            AS InvoiceNum, MAX(i.Period) AS Period, i.SubmittedDate AS DateSubmitted, MAX(i.DanaApprovalDate) AS DateApproved, CASE WHEN s.LegalBillStatus IS NULL 
                            THEN 'Unknown' WHEN u.IsFullyPaid = 1 THEN 'Paid' WHEN NavPaidInvoice = 1 THEN 'Paid By Navigant' ELSE s.LegalBillStatus END AS Status, 
                            c.SourceDate AS PaidDate, SUM(i.OriginalTotal) AS OriginalInvoiceTotal, CASE WHEN AppealFlag = 1 THEN SUM(AppealTotal) ELSE SUM(AdjustedTotal) 
                            END AS FinalizedInvoiceTotal
     FROM         dbo.m_tblLegalBillInvoices AS i LEFT OUTER JOIN
                            dbo.tblDefenseCounselAddress AS d ON d.DefenseCounselAddressID = i.DefenseCounselAddressID LEFT OUTER JOIN
                            dbo.tblMatter AS m ON i.MatterID = m.MatterID LEFT OUTER JOIN
                            dbo.m_tblLegalBillStatus AS s ON i.StatusID = s.LegalBillStatusID LEFT OUTER JOIN
                            Unpaid AS u ON i.InvoiceID = u.InvoiceID LEFT OUTER JOIN
                            dbo.tblPaymentLegalBill AS plb ON plb.InvoiceID = i.InvoiceID LEFT OUTER JOIN
                            dbo.tblCheck AS c ON c.CheckID = plb.CheckID
     WHERE     (COALESCE (i.SubmittedDate, i.DanaApprovalDate, i.Period) >= GETDATE() - 180) OR
                            (u.IsFullyPaid = 0) OR
                            (i.NavPaidInvoice = 0)
     GROUP BY i.InvoiceID, i.DefenseCounselAddressID, d.DefenseCounselID, d.DefenseCounsel, m.Matter, i.SubmittedDate, COALESCE (i.OriginalInvoiceNumber, 'Unknown'), 
                            CASE WHEN s.LegalBillStatus IS NULL 
                            THEN 'Unknown' WHEN u.IsFullyPaid = 1 THEN 'Paid' WHEN NavPaidInvoice = 1 THEN 'Paid By Navigant' ELSE s.LegalBillStatus END, c.SourceDate, 
                            i.AppealFlag
     ORDER BY DateSubmitted DESC, d.DefenseCounsel, m.Matter
GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_LegalBill] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_LegalBill] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_LegalBill] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_LegalBill] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_LegalBill] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_LegalBill] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_LegalBill] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_LegalBill] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_LegalBill] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_LegalBill] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_LegalBill] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_LegalBill] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "i"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 271
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 309
               Bottom = 125
               Right = 542
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 6
               Left = 580
               Bottom = 95
               Right = 756
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 794
               Bottom = 95
               Right = 978
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "u"
            Begin Extent = 
               Top = 96
               Left = 580
               Bottom = 185
               Right = 756
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "plb"
            Begin Extent = 
               Top = 96
               Left = 794
               Bottom = 215
               Right = 1004
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 0
         End
    ', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_LegalBill', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'  End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_LegalBill', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_LegalBill', NULL, NULL
GO
