SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[tblClaimLawsuitStatus]
AS
SELECT     cls.ClaimLawsuitStatusID, 
COALESCE (clsc.ClaimLawsuitStatusAlias, cls.ClaimLawsuitStatus) AS ClaimLawsuitStatus, 
clsc.ClaimLawsuitStatusGroupID,
'' AS SPUsername
FROM         [KGA-ClaimsAdmin].dbo.tblClaimLawsuitStatus AS cls INNER JOIN
                      [KGA-ClaimsAdmin].dbo.tblClaimLawsuitStatusClient AS clsc ON cls.ClaimLawsuitStatusID = clsc.ClaimLawsuitStatusID
WHERE     (clsc.CaseID = 47)




GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatus] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatus] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatus] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatus] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatus] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatus] TO [power_user]
GO
