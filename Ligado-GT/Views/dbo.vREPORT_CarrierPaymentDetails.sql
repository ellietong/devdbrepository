SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vREPORT_CarrierPaymentDetails]
AS

SELECT di.DefenseInvoiceID, 
	dic.InvoiceNumber AS 'InvoiceNumber',
	cpi.FullName AS 'FullName',
	cls.ClaimLawsuitID AS 'ClaimLawsuitID',
	NULL AS 'CarrierName',--CASE WHEN m.MatterID = 12 THEN COALESCE(Carriergroupname, 'Marsh Pool') ELSE COALESCE(Carriergroupname,'GSX') END AS 'CarrierName',
	NULL AS 'BilledAmount',--SUM(DefenseAllocation + DefenseOutsideLimitsAllocation) AS 'BilledAmount', 
	NULL AS 'AmountReceived',--Coalesce(SUM(Amount),0) AS 'AmountReceived',
	NULL AS 'DatePaid'--SourceDate as 'DatePaid'
FROM tblDefenseInvoice di
	INNER JOIN tblDefInvClaimantDetail      dic ON dic.DefenseInvoiceID	= di.DefenseInvoiceID
	--INNER JOIN tbloccurrence                o   ON o.InvoiceDetailID = dic.InvoiceDetailID
	--INNER JOIN tblexhaustion                e   ON e.occurrenceid = o.occurrenceid
	--LEFT  JOIN (SELECT MAX(AccountsReceivablePaymentID) as [AccountsReceivablePaymentID], ExhaustionID as [ExhaustionID], SUM(Amount) as [Amount], PaymentDefenseID as [PaymentDefenseID]
				--FROM tblAccountsReceivablePayment
				--WHERE COALESCE(IsHistoric, 0) <> 1 
				--GROUP BY ExhaustionID, IsHistoric,PaymentDefenseID)
				--							arp ON arp.ExhaustionID = e.ExhaustionID
	--INNER JOIN tblpolicydates               d   ON d.policydateid = e.policydateid
	--INNER JOIN tblpolicy                    p   ON p.policyid = d.policyid
	--INNER JOIN vCarrierWithGroup			c   ON c.carrierid = p.carrierid
	INNER JOIN tblMatter                    m   ON m.MatterID = dic.MatterID
	--LEFT  JOIN tblPaymentDefense			pd	ON pd.PaymentDefenseID = arp.PaymentDefenseID
	--LEFT  JOIN tblCheck						ch  ON ch.CheckID = pd.CheckID
	INNER JOIN tblclaimlawsuit				cls ON cls.ClaimLawsuitID = dic.ClaimLawsuitID
	INNER JOIN tblclaim						cl  ON cl.ClaimID = cls.ClaimID
	INNER JOIN tblClaimantPersonalInfo      cpi ON cpi.ClaimantPersonalInfoID = cl.ClaimantPersonalInfoID
WHERE DateApproved IS NOT NULL
GROUP BY di.DefenseInvoiceID, 
		--CarrierGroupName, 
		dic.InvoiceNumber, 
		cpi.FullName, 
		cls.ClaimLawsuitID, 
		m.MatterID--, SourceDate
GO
