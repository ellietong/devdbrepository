SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[TEST_vResponsibleDefenseCounsel]
AS
SELECT     dcs.DefenseCounselAddressID, d.DefenseCounsel, dcs.ResponsibleDefenseCounselAddressID, dcs.StateID, r.DefenseCounsel AS ResponsibleDefenseCounsel, 
                      dcs.IsActiveCounsel
FROM         dbo.tblDefenseCounselState AS dcs LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress AS dca ON dca.DefenseCounselAddressID = dcs.DefenseCounselAddressID LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress AS dcar ON dcar.DefenseCounselAddressID = dcs.ResponsibleDefenseCounselAddressID LEFT OUTER JOIN
                      dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID LEFT OUTER JOIN
                      dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
GROUP BY d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounselAddressID, dcs.ResponsibleDefenseCounselAddressID, dcs.IsActiveCounsel, dcs.StateID
GO
GRANT VIEW DEFINITION ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [Ligado_User]
GRANT SELECT ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [Ligado_User]
GRANT INSERT ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [Ligado_User]
GRANT DELETE ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [power_user]
GRANT SELECT ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [power_user]
GRANT INSERT ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [power_user]
GRANT DELETE ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [power_user]
GRANT UPDATE ON  [dbo].[TEST_vResponsibleDefenseCounsel] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[50] 4[25] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "dcs"
            Begin Extent = 
               Top = 56
               Left = 547
               Bottom = 193
               Right = 821
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dca"
            Begin Extent = 
               Top = 29
               Left = 297
               Bottom = 255
               Right = 514
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dcar"
            Begin Extent = 
               Top = 40
               Left = 851
               Bottom = 264
               Right = 1068
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 94
               Left = 89
               Bottom = 192
               Right = 267
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "r"
            Begin Extent = 
               Top = 105
               Left = 1098
               Bottom = 194
               Right = 1276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
     ', 'SCHEMA', N'dbo', 'VIEW', N'TEST_vResponsibleDefenseCounsel', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'    Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'TEST_vResponsibleDefenseCounsel', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'TEST_vResponsibleDefenseCounsel', NULL, NULL
GO
