SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vDETAILS_ClaimantRelatedParty]
AS
SELECT     c.ClaimantRelatedPartyID, c.ClaimantPersonalInfoID, c.FirstName, c.MiddleName, c.LastName, r.Relationship, l.LegalCapacity, c.RPCity, c.StateID, c.RPAddress2, 
                      c.RPAddress1, c.RPPhone, c.RPExtension, c.RPMailZip, c.RPZip, c.RPSocialSecurityNumber, COALESCE (NULLIF (COALESCE (c.RPAddress1 + CHAR(13) + CHAR(10), 
                      '') + COALESCE (c.RPAddress2 + CHAR(13) + CHAR(10), '') + COALESCE (c.RPCity + ', ', '') + COALESCE (s.State + ' ', '') + COALESCE (CONVERT(varchar, c.RPZip), '') 
                      + COALESCE ('-' + CONVERT(varchar, c.RPMailZip), ''), ''), '') AS CompleteAddress, c.RelationshipID, c.LegalCapacityID, s.FullNameState, s.State, 
                     FullName, CASE WHEN RPSocialSecurityNumber IS NULL OR LEN(RPSocialSecurityNumber) = 0
                      THEN '' ELSE 'XXX-XX-' + RIGHT(RPSocialSecurityNumber, 4) END AS MaskedSSN, CONVERT(DATETIME, NULL) AS RPBirthDate, CONVERT(int, NULL) AS LossOfConsortiumStatusID, CONVERT(nvarchar(50), NULL) AS LossOfConsortiumStatus
FROM         dbo.tblClaimantRelatedParty AS c LEFT OUTER JOIN
                      dbo.tblRelationship AS r ON c.RelationshipID = r.RelationshipID LEFT OUTER JOIN
                      dbo.tblLegalCapacity AS l ON c.LegalCapacityID = l.LegalCapacityID LEFT OUTER JOIN
                      dbo.tblState AS s ON c.StateID = s.StateID


GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantRelatedParty] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 289
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "r"
            Begin Extent = 
               Top = 6
               Left = 296
               Bottom = 102
               Right = 466
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 6
               Left = 504
               Bottom = 119
               Right = 718
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 756
               Bottom = 99
               Right = 907
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantRelatedParty', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantRelatedParty', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantRelatedParty', NULL, NULL
GO
