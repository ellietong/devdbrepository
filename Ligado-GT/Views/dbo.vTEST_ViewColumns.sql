SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vTEST_ViewColumns]
AS
SELECT col.name AS ColumnName, v.name AS ViewName FROM sys.objects as [v] 
INNER JOIN sys.columns col ON v.object_id = col.object_id
WHERE type_desc = 'view'


GO
