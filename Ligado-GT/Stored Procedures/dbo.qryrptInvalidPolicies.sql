SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  Procedure [dbo].[qryrptInvalidPolicies]
(@lowLayer int = NULL, @highLayer int = NULL, @lowAttach nvarchar(50) = NULL, @highAttach nvarchar(50) = NULL, @LimitTypeID INT = NULL)
As

SET NOCOUNT ON

--DECLARE @lowlayer int
--DECLARE @highlayer int
--DECLARE @lowattach nvarchar(50)
--DECLARE @highAttach nvarchar(50)
--DECLARE @limittypeid int
--SET @lowlayer = 0
--SET @highlayer = 0
--SET @lowattach = null
--SET @highAttach = null
--SET @limittypeid = 2

DECLARE @maxLimitTypeID int
DECLARE @maxLayer int
DECLARE @maxAttach money

-- Find max ID values
SELECT @maxLayer = MAX(Layer) FROM vPolicy
SELECT @maxAttach = MAX(AttachmentPoint) FROM vPolicy
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy

SELECT PolicyID, PStartDate, PEndDate, AnnualPeriod, InsuranceProgramID, AttachmentPoint, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, CarrierName, Layer, PolicyNotes, 
	NamedInsured, PolicyNum, CarrierGroupID, CarrierID, InsuranceProgram, LimitTypeID
FROM vPolicy pinfo
WHERE (PStartDate IS NULL OR PEndDate IS NULL OR AnnualPeriod IS NULL OR InsuranceProgramID IS NULL OR AttachmentPoint IS NULL OR PerOccLimit IS NULL OR PerOccLimit = 0 OR 
	LayerPerOccLimit IS NULL OR LayerPerOccLimit < PerOccLimit OR LayerPerOccLimit = 0 OR SIR IS NULL OR AggregateLimit IS NULL) AND 
	(Layer BETWEEN COALESCE(@lowLayer, 0) AND COALESCE(@highLayer, @maxLayer) AND 
	AttachmentPoint BETWEEN COALESCE(CONVERT(money, @lowAttach), 0) AND COALESCE(CONVERT(money, @highAttach), @maxAttach)) AND LimitTypeID >= COALESCE(@limitTypeID, 0) AND 
	LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)

RETURN

/* Single limit
SELECT tblPolicy.PolicyID, tblPolicyDates.PStartDate, tblPolicyDates.PEndDate, tblPolicyDates.AnnualPeriod, tblPolicy.InsuranceProgramID, tblPolicy.AttachmentPoint, 
tblPolicy.PerOccLimit, tblPolicy.LayerPerOccLimit, tblPolicy.SIR, tblPolicy.AggregateLimit, tblCarrier.CarrierName, tblPolicy.Layer, tblPolicy.PolicyNotes, 
tblNamedInsured.NamedInsured, tblPolicy.PolicyNum, tblCarrierGroup.CarrierGroupID, tblCarrier.CarrierID, tblInsuranceProgram.InsuranceProgramID, 
tblInsuranceProgram.InsuranceProgram
FROM (tblNamedInsured RIGHT JOIN (tblCarrierGroup RIGHT JOIN (tblInsuranceProgram RIGHT JOIN (tblCarrier RIGHT JOIN tblPolicy ON tblCarrier.CarrierID = tblPolicy.CarrierID)
ON tblInsuranceProgram.InsuranceProgramID = tblPolicy.NamedInsuredID) ON tblCarrierGroup.CarrierGroupID = tblCarrier.CarrierGroupID) 
ON tblNamedInsured.NamedInsuredID = tblPolicy.NamedInsuredID) LEFT JOIN tblPolicyDates ON tblPolicy.PolicyID = tblPolicyDates.PolicyID
WHERE (((tblPolicyDates.PStartDate) Is Null)) OR (((tblPolicyDates.PEndDate) Is Null)) OR (((tblPolicyDates.AnnualPeriod) Is Null)) OR (((tblPolicy.InsuranceProgramID) Is Null)) 
OR (((tblPolicy.AttachmentPoint) Is Null)) OR (((tblPolicy.PerOccLimit) Is Null Or (tblPolicy.PerOccLimit)=0)) OR (((tblPolicy.LayerPerOccLimit) Is Null Or 
(tblPolicy.LayerPerOccLimit)<[perocclimit] Or (tblPolicy.LayerPerOccLimit)=0)) OR (((tblPolicy.SIR) Is Null)) OR (((tblPolicy.AggregateLimit) Is Null));
*/


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptInvalidPolicies] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptInvalidPolicies] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptInvalidPolicies] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptInvalidPolicies] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptInvalidPolicies] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptInvalidPolicies] TO [power_user]
GO
