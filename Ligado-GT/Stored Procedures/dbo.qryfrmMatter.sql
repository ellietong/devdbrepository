SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmMatter]
as

Select MatterID, Matter
From tblMatter
Order By Matter

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMatter] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmMatter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMatter] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmMatter] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMatter] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmMatter] TO [power_user]
GO
