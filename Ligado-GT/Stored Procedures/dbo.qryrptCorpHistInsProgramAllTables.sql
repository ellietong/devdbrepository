SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[qryrptCorpHistInsProgramAllTables]
As

SET NOCOUNT ON

DECLARE @Policy TABLE (PolicyID int, InsuranceProgramID int, InsuranceProgram nvarchar(100))
INSERT INTO @Policy

SELECT PolicyID, InsuranceProgramID, InsuranceProgram
FROM vPolicy p
GROUP BY PolicyID, InsuranceProgramID, InsuranceProgram

SELECT cip.CorpHistInsProgramID, cip.CorporateHistoryID, cip.InsuranceProgramID, cip.[From], cip.[Until], 
cip.ProgramDescription, o.CorporateHistoryProfile, pinfo.InsuranceProgram
FROM tblCorpHistInsProgram cip 
LEFT JOIN tblCorporateHistoryProfile o ON cip.CorporateHistoryID = o.CorporateHistoryID
LEFT JOIN @Policy pinfo ON cip.InsuranceProgramID = pinfo.InsuranceProgramID
GROUP BY cip.CorpHistInsProgramID, cip.CorporateHistoryID, cip.InsuranceProgramID, cip.[From], cip.[Until], 
cip.ProgramDescription, o.CorporateHistoryProfile, pinfo.InsuranceProgram

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorpHistInsProgramAllTables] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCorpHistInsProgramAllTables] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorpHistInsProgramAllTables] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCorpHistInsProgramAllTables] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorpHistInsProgramAllTables] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCorpHistInsProgramAllTables] TO [power_user]
GO
