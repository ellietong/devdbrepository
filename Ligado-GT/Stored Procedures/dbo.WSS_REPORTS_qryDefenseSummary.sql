SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--USE [KGC-MarleyWylain]
--GO
--/****** Object:  StoredProcedure [dbo].[WSS_INVOICES_InvoiceSearchByClaimant]    Script Date: 06/29/2012 15:09:47 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE PROC [dbo].[WSS_REPORTS_qryDefenseSummary]
(@PaidDateStart date, @PaidDateEnd date, @emailaddress nvarchar(250), @FirmID int= null)
AS

------VARIABLES FOR TESTING----
--DECLARE @FirmID int = 174
----DECLARE @VendorFirmID int
----DECLARE @StatusID bigint
--DECLARE @PaidDateStart nvarchar(50)
--DECLARE @PaidDateEnd nvarchar(50)
----DECLARE @Period nvarchar(50)
--DECLARE @emailaddress nvarchar(250)
--SET @FirmID		   = NULL
----SET @VendorFirmID  = NULL
----SET @StatusID	   = NULL
--SET @PaidDateStart = '10/1/2010'
--SET @PaidDateEnd   = '10/31/2013'
----SET @Period		   = 'DEC 2011'
--SET @emailaddress  = 'tswindler@huddlestonbolen.com '



--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE d.ClaimantDocumentTypeID in (40, 41)'
--IF @FirmID			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND d.FirmID = ' + CONVERT(nvarchar(20),@FirmID) END
--IF @VendorFirmID	IS NOT NULL BEGIN IF @VendorFirmID = 0 SET @WhereString = @WhereString + ' AND d.VendorFirmID <= 10000' ELSE SET @WhereString = @WhereString + ' AND d.VendorFirmID = ' + CONVERT(nvarchar(20),@VendorFirmID) END
--IF @StatusID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND dis.StatusID = ' + CONVERT(nvarchar(20),@StatusID) END
IF @PaidDateStart	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(c.Sourcedate, CONVERT(date, ''1/1/1900'')) >= ''' + CONVERT(nvarchar(20),@PaidDateStart) + '''' END
IF @PaidDateEnd		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(c.Sourcedate, CONVERT(date, ''1/1/1900'')) <= ''' + CONVERT(nvarchar(20),@PaidDateEnd) + '''' END
--IF @Period			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.Period LIKE ''%'' + @PeriodParam + ''%''' END

--These IF statements determine additional WHERE statements by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.

IF @FirmID IS NOT NULL
	SET @WhereString = @WhereString + ' AND di.firmid= '+ convert(varchar,@FirmID)


DECLARE @QueryString nvarchar(max)
SET @QueryString = '
SELECT 		dicd.claimantpersonalinfoID,
			lastname+'', ''+coalesce(firstname, '' '') as Claimantname, --lastname+'', ''+firstname as ClaimantName,
sum(case when checkfundingsourceid = 3 and sourceamount<0 then (dicd.fees*-1) else dicd.fees end)+ 
sum(case when checkfundingsourceid = 3 and sourceamount<0 then (dicd.costs*-1) else (dicd.costs) end)+
sum(case when checkfundingsourceid = 3 and sourceamount<0 then (dicd.adjustments*-1) else (dicd.adjustments) end) AS DefenseTotal
			
FROM tblDocuments d
		--LEFT JOIN tblClaimantDocumentType dt		ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
		--LEFT JOIN tblDocumentGroup dg				ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT JOIN tblDefenseInvoice di				ON d.DocumentID = di.DocumentID 
		LEFT JOIN tblDefenseInvoiceStatus dis		ON di.StatusID = dis.StatusID 
		LEFT JOIN tblVendorFirm ex			ON di.VendorFirmID = ex.VendorFirmID
		LEFT JOIN tblPaymentDefense pd				ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN tblDefInvClaimantDetail dicd		ON dicd.DefenseInvoiceID = di.DefenseInvoiceID 
		LEFT JOIN (select * 
				   from tblCheck) c	ON pd.CheckID = c.CheckID and dicd.matterID=c.matterID
		LEFT JOIN tblClaimantPersonalInfo cpi		ON cpi.ClaimantPersonalInfoID = dicd.ClaimantPersonalInfoID'
 +	@WhereString + '
	GROUP BY dicd.claimantpersonalinfoid,
			lastname+'', ''+coalesce(firstname, '' '') --lastname+'', ''+firstname
	--ORDER BY DateCreated DESC, Accepted ASC' 
	--OPTION (RECOMPILE)'
PRINT @Querystring
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString--, N'@PeriodParam nvarchar(max)', @PeriodParam = @Period



----this query is including refunds, by claimant is not
--select * from tblCheck where FundingSourceID in (4,5)

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_qryDefenseSummary] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_qryDefenseSummary] TO [Ligado_User]
GO
