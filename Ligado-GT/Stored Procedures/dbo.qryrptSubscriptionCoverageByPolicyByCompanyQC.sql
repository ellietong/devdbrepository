SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyQC]
(@PolicyID int = null)
AS
SET NOCOUNT ON

--DECLARE @PolicyID int 
--SET @PolicyID = 1

SELECT MIN(PStartDate) AS MinOfPStartDate, MAX(PEndDate) AS MaxOfPEndDate, MIN(AnnualPeriod) AS MinOfAnnualPeriod, 
	InsuranceProgram, SubscriptionCompany, SubscriptionCompanySolvency, SubscriptionSection,
	SubscriptionActualPercentOfPolicy,
	SubscriptionAllocationPercentOfPolicy,
	(SubscriptionActualPercentOfPolicy * PerOccLimit) AS SubscriberPerOccActual, 
	(SubscriptionAllocationPercentOfPolicy * PerOccLimit) AS SubscriberPerOccAllocation,
	CarrierID, CarrierName, InsuranceProgramID, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
	SIR, AggregateLimit, Layer, PolicyNum, p.PolicyID, SubscriberID, SubscriptionCoverageID, 
	AgencyName, UWReference, AcctNum, StartDate, EndDate, SubscriptionSort,
	LimitTypeID, LimitType
FROM vPolicy p
	INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID
WHERE p.PolicyID = @PolicyID
GROUP BY InsuranceProgram, SubscriptionCompany, SubscriptionCompanySolvency, SubscriptionSection,
	SubscriptionActualPercentOfPolicy,
	SubscriptionAllocationPercentOfPolicy,
	(SubscriptionActualPercentOfPolicy * PerOccLimit), 
	(SubscriptionAllocationPercentOfPolicy * PerOccLimit),
	CarrierID, CarrierName, InsuranceProgramID, AttachmentPoint, PerOccLimit, LayerPerOccLimit, 
	SIR, AggregateLimit, Layer, PolicyNum, p.PolicyID, SubscriberID, SubscriptionCoverageID, 
	AgencyName, UWReference, AcctNum, StartDate, EndDate, SubscriptionSort,
	LimitTypeID, LimitType
ORDER BY SubscriptionSort

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyQC] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyQC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByPolicyByCompanyQC] TO [power_user]
GO
