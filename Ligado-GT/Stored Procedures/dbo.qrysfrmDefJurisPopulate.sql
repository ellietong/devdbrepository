SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysfrmDefJurisPopulate]
As
Set NoCount On

select DefenseJurisdictionID, Jurisdiction, s.State,DefenseCounsel, DefenseCounselCity, j.JurisdictionID
from tblDefenseJurisdiction dj 
inner join tblJurisdiction j on j.JurisdictionID = dj.JurisdictionID 
inner join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = dj.DefenseCounselAddressID 
inner join tblState s on dj.StateID = s.StateID 
inner join tblCourtType c on c.CourtTypeID = dj.CourtTypeID
Where IsActive = 1
order by Jurisdiction, State
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDefJurisPopulate] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmDefJurisPopulate] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDefJurisPopulate] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysfrmDefJurisPopulate] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDefJurisPopulate] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmDefJurisPopulate] TO [power_user]
GO
