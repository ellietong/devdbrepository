SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] (@MatterID int = NULL, @emailaddress nvarchar(250))
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MatterID int
--SET @MatterID = NULL

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

--Union table to combine 3 main components of Pending Count
DECLARE @CombinedTable TABLE (HighLevelDisease nvarchar(100), 
							  State            nvarchar(100), 
							  DiseaseCount     int)
INSERT INTO @CombinedTable
	--Counts All Claims that have their status date before the pending date and are CURRENTLY still open (1)
	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
												  ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		FullNameState AS 'State',
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS DiseaseCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl				  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l						  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimDisease cd				  ON c.ClaimID = cd.ClaimID
		LEFT  JOIN tblDisease d						  ON d.DiseaseID = cd.DiseaseID
		LEFT  JOIN tblDisease_ALT da				  ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT  JOIN tblHighLevelDisease hld			  ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
		LEFT  JOIN tblState s						  ON s.StateID = l.StateID
		WHERE ClaimLawsuitStatusID = 22 
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
		AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @maxMatterID)
		AND cd.PrimaryDisease = 1
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID, FullNameState
	
--Groups all results into one table
SELECT [State] AS 'State',
	SUM(CASE WHEN HighLevelDisease = 'Mesothelioma'  THEN DiseaseCount ELSE 0 END) AS MesoCount,
	SUM(CASE WHEN HighLevelDisease = 'Lung Cancer'	 THEN DiseaseCount ELSE 0 END) AS LungCancerCount,
	SUM(CASE WHEN HighLevelDisease = 'Other Cancer'  THEN DiseaseCount ELSE 0 END) AS OtherCancerCount,
	SUM(CASE WHEN HighLevelDisease = 'Non-Malignant' THEN DiseaseCount ELSE 0 END) AS NonMaligCount,
	SUM(CASE WHEN HighLevelDisease = 'Unknown'		 THEN DiseaseCount ELSE 0 END) AS UnknownCount,
	SUM(CASE WHEN HighLevelDisease = 'Silicosis'	 THEN DiseaseCount ELSE 0 END) AS SilicosisCount,
	SUM(DiseaseCount) AS Total
FROM @CombinedTable ct
GROUP BY [State]

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] TO [power_user]
GO
