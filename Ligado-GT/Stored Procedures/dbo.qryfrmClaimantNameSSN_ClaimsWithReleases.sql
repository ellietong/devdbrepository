SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmClaimantNameSSN_ClaimsWithReleases] (@LastNameInitial nvarchar(5))
as

Set NoCount On

--Declare @LastNameInitial nvarchar(5)
--Set @LastNameInitial = 'F'



Declare @ClaimantName table (ClaimantPersonalInfoID int,
				LastName nvarchar(40),
				FirstName nvarchar(40),
				MiddleName nvarchar(40),
				SocialSecurityNumber nvarchar(40))
Insert Into @ClaimantName
Select cpi.ClaimantPersonalInfoID,
	Case when LastName is null then '[LastName]' else LastName end as LastName,
	Case when FirstName is null then '[FirstName]' else FirstName end as FirstName,
	Case when MiddleName is null then '' else ' ' + MiddleName end as MiddleName,
	Case when SocialSecurityNumber is null then (Case when DocketNumber is null then '' else '  -  ' + DocketNumber end) 
	else '  -  ' + Left(SocialSecurityNumber,3) + '-'+ Right(Left(SocialSecurityNumber,5),2) + '-'+ Right(SocialSecurityNumber,4) end as SocialSecurityNumber
From tblClaimantPersonalInfo cpi
Inner Join tblClaim c on cpi.ClaimantPersonalInfoID=c.ClaimantPersonalInfoID
Inner Join tblClaimDisease cd on c.ClaimID=cd.ClaimID
Left Join tblDisease d on cd.DiseaseID=d.DiseaseID
Left Join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
Inner Join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
Inner Join tblClaimLawsuitStatusTracking clt on cl.ClaimLawsuitID=clt.ClaimLawsuitID
Inner Join tblClaimLawsuitStatus cls on clt.ClaimLawsuitStatusID=cls.ClaimLawsuitStatusID
Inner Join tblLawsuit l on cl.LawsuitID=l.LawsuitID
Inner Join tblJurisdiction j on l.JurisdictionID=j.JurisdictionID
Inner Join tblState s on l.StateID=s.StateID
Inner Join tblClaimantSettlement cs on cl.ClaimLawsuitID=cs.ClaimLawsuitID
Inner Join tblSettlementGroup sg on cs.SettlementGroupID=sg.SettlementGroupID
Inner Join tblClaimantRelease cr on cs.ClaimantSettlementID=cr.ClaimantSettlementID
Where clt.IsPrimaryStatus = 1
and LastName like coalesce(@LastNameInitial + '%', '%')
Group By cpi.ClaimantPersonalInfoID,
	Case when LastName is null then '[LastName]' else LastName end,
	Case when FirstName is null then '[FirstName]' else FirstName end,
	Case when MiddleName is null then '' else ' ' + MiddleName end,
	Case when SocialSecurityNumber is null then (Case when DocketNumber is null then '' else '  -  ' + DocketNumber end) else '  -  ' + Left(SocialSecurityNumber,3) + '-'+ Right(Left(SocialSecurityNumber,5),2) + '-'+ Right(SocialSecurityNumber,4) end

Select ClaimantPersonalInfoID, 
	LastName + ', ' + FirstName + MiddleName + SocialSecurityNumber
From @ClaimantName
group by ClaimantPersonalInfoID, 
	LastName + ', ' + FirstName + MiddleName + SocialSecurityNumber
Order By LastName + ', ' + FirstName + MiddleName + SocialSecurityNumber

Return


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantNameSSN_ClaimsWithReleases] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantNameSSN_ClaimsWithReleases] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantNameSSN_ClaimsWithReleases] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantNameSSN_ClaimsWithReleases] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantNameSSN_ClaimsWithReleases] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantNameSSN_ClaimsWithReleases] TO [power_user]
GO
