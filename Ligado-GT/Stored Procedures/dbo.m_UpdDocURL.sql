SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_UpdDocURL]
	-- Add the parameters for the stored procedure here
(@DocumentID int, @DocumentURL nvarchar(MAX))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblDocuments
	SET DocumentURL = @DocumentURL
	WHERE DocumentID = @DocumentID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_UpdDocURL] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_UpdDocURL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_UpdDocURL] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[m_UpdDocURL] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_UpdDocURL] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_UpdDocURL] TO [power_user]
GO
