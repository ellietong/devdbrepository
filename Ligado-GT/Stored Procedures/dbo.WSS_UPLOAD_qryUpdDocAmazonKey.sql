SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryUpdDocAmazonKey] (@DocumentID	bigint, @NewPathWithID nvarchar(250) )
AS
SET NOCOUNT ON



UPDATE tblDocuments
	SET AmazonKey = @NewPathWithID
	WHERE DocumentID = @DocumentID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocAmazonKey] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocAmazonKey] TO [Ligado_User]
GO
