SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROC [dbo].[WSS_REPORTS_DISEASE_DismissedClaimsByDisease]
(@MatterID int,@BeginDate datetime, @EndDate datetime, @emailaddress nvarchar(200) = null)

As
Set NoCount On

--DECLARE @BeginDate datetime = '1/1/2011'
--DECLARE @EndDate datetime = GETDATE()

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int, claimlawsuitstatus nvarchar (500), claimlawsuitid int,
	MoYearDismissed datetime, YearDismissed datetime,MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int, unknowncount1 int)
Insert into @DiseaseCount

Select Matter, C.MatterID,  css.claimlawsuitstatus, cl.claimlawsuitid,
 convert(datetime,(convert(nvarchar,Month(cst.statusDate))+'/1/'+ convert(nvarchar,Year(cst.StatusDate)))) as MoYearDismissed,
  convert(nvarchar,Year(cst.StatusDate)) as YearDismissed,
--Meso
Case	when DMeso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when DLC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when DOC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig, Silicosis, Pneumoconiosis
Case	when DNM.HighLevelDiseaseID in (4,7,8) Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when DUnk.HighLevelDiseaseID in (5,6) Then 1
Else 0 End AS UnknownCount,
Case	when DUnk.HighLevelDiseaseID is NULL Then 1
Else 0 End AS UnknownCount1


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (Select CD.ClaimDiseaseID,CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
LEFT JOIN tblClaimSequence csq ON c.ClaimSequenceID = csq.ClaimSequenceID
--Meso
Left Join tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
Left Join tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
Left Join tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
Left Join tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Unknown
Left Join tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID


Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
left join tblClaimSettlement cs on cs.ClaimLawsuitID = CL.ClaimLawsuitID
left join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblClaimLawsuitStatus css on css.ClaimLawsuitStatusID = cst.ClaimLawsuitStatusID
--left join tblclaimlawsuitstatusgroup csg on csg.claimlawsuitstatusid = cst.ClaimLawsuitStatusID

Where  --COALESCE(isprimaryforclaim, 0) = 1   AND 
COALESCE(IsPrimaryStatus, 0) = 1
--and csg.statusgroupid = 3
--AND cst.ClaimLawsuitStatusID not in (15,16,27,18,19,20,21,24,30,17,26)
and cst.ClaimLawsuitStatusID in (15, 16, 17, 27, 28, 19, 34, 35, 37, 41)



--AND C.MatterID >= COALESCE(@MatterID,0) AND C.MatterID <= COALESCE(NULLIF(@MatterID,0),@maxMatterID) 
and cst.StatusDate between COALESCE(@BeginDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) and COALESCE(@EndDate, GETDATE())
--AND COALESCE(cpi.jobsiteinfo, 0) = 0
 
	 Order By  Year(cst.StatusDate)


Select Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, 
	Sum(OtherCancerCount) as OtherCancerCount,
	Sum(NonMaligCount) as NonMaligCount, 
	Sum(UnknownCount+unknowncount1) as UnknownCount, 
	Sum(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + unknowncount1) AS TotalCount,
	@BeginDate as StartDate, 
	@EndDate as EndDate,
MoYearDismissed, 		YearDismissed, DATENAME(month, MoYearDismissed) AS Month, DATENAME(year, MoYearDismissed) As Year,
SUM(MesoCount + LungCancerCount + OtherCancerCount + NonMaligCount + UnknownCount + unknowncount1) AS MonthTotal , 
DATENAME(month, MoYearDismissed) + ' ' + DATENAME(year, MoYearDismissed) AS MonthYear
from @DiseaseCount
Group By MoYearDismissed, YearDismissed

Return





GO
