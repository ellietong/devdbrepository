SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_NEWFILINGS_qryNewFilingsByState] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @StartDate datetime=null, @EndDate datetime=null, @emailaddress nvarchar(200) = null) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
----TEST VARS
--DECLARE @StartDate datetime = '1/1/2014'
--DECLARE @EndDate datetime = GETDATE()

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter



select year(FileDate) as Year,FullNameState AS State, HighLevelDisease as Disease,COUNT(distinct(cl.ClaimLawsuitID)) As ClaimLawsuitCount
from tblClaim c
left join (select * from tblClaimDisease where PrimaryDisease = 1) cd on c.ClaimID = cd.ClaimID
left join tblDisease d on cd.DiseaseID = d.DiseaseID
left join tblHighLevelDisease hld on d.HighLevelDiseaseID = hld.HighLevelDiseaseID
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
--inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s ON l.StateID=s.StateID
where	coalesce(FileDate,'1/1/1900')between coalesce(@StartDate,'1/1/1900')
		and coalesce(@EndDate, '1/1/2100')
group by year(FileDate),FullNameState, HighLevelDisease
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END




GO
