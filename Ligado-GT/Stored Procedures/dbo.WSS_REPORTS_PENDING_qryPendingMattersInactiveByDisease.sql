SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_REPORTS_PENDING_qryPendingMattersInactiveByDisease] (@MatterID int=null,@EndDate datetime=null, @emailaddress nvarchar(200) = null)
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
     
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter
    
select top 10 Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end as HighLevelDisease, 
		COUNT(distinct(cl.ClaimLawsuitID)) As Count
from tblClaim c
inner join tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
inner join tblLawsuit l ON cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join (select * from tblClaimDisease where primarydisease=1) cd ON c.ClaimID=cd.ClaimID
left join tblDisease d ON d.DiseaseID=cd.DiseaseID
left join tblHighLevelDisease hld ON hld.HighLevelDiseaseID=d.HighLevelDiseaseID
where	ClaimLawsuitStatusID in (29) 
		and  IsPrimaryStatus=1
		--and ProcessedDate <= COALESCE(@EndDate,'12/31/2100')
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersInactiveByDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersInactiveByDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersInactiveByDisease] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersInactiveByDisease] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersInactiveByDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersInactiveByDisease] TO [power_user]
GO
