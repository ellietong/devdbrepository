SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_SETTLEMENT_qryPlaintiffSettleDocs] (@ClaimLawsuitID int, 
												 @emailaddress nvarchar(250))
AS
SET NOCOUNT ON

--The following set of variables can be used for testing.
--DECLARE @ClaimLawsuitID int
--DECLARE @emailaddress nvarchar(250)
--SET @ClaimLawsuitID = 267280
--SET @emailaddress    = 'ganttc@nsi.com'



DECLARE @PlaintiffCounselAddressID bigint
DECLARE @QueryString nvarchar(max)

SET @PlaintiffCounselAddressID = (SELECT distinct PlaintiffCOunselAddressID FROM vtblPerson WHERE Email = @emailaddress)


IF (@PlaintiffCounselAddressID IS NOT NULL)
BEGIN


SELECT --AmazonKey, 
		DocumentTitle, 
		DocCategory, 
		PageNums,
		SettleDocID, 
		--sd.SetPymtID, 
		csd.DocumentID,
		csd.DocCategoryID
	FROM tblClaimSettlementDocuments csd 
		INNER JOIN tblClaimSettlement cs ON cs.ClaimLawsuitID = csd.ClaimLawsuitID
		INNER JOIN tblDocuments d         ON csd.DocumentID = d.DocumentID
	--	INNER JOIN vtblPerson p ON d.CreatedBy = p.Email
		INNER JOIN tblClaimSettlementDocumentCategory dc ON csd.DocCategoryID = dc.DocCategoryID 
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimLawsuit cl ON csd.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		LEFT JOIN  tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
		WHERE -- d.CreatedBy =  @emailaddress 
		  --AND 
		  cl.ClaimLawsuitID = @ClaimLawsuitID
		  GROUP BY DocumentURL + CASE WHEN ISNUMERIC(PageNums)=1 
		  AND CHARINDEX(',', PageNums)=0 THEN '#page='+PageNums WHEN CHARINDEX(',', PageNums)-1 > 0 THEN '#page='+LEFT(PageNums, CHARINDEX(',', PageNums)-1)
			WHEN CHARINDEX('-', PageNums)-1 > 0 THEN '#page='+LEFT(PageNums, CHARINDEX('-', PageNums)-1)
			ELSE '' END, 
		DocumentTitle, 
		PageNums, 
		DocCategory, 
		SettleDocID, 
		--sd.SetPymtID, 
		csd.DocumentID, 
		csd.DocCategoryID,
		AmazonKey
END
ELSE
BEGIN
	SELECT --AmazonKey, 
		COALESCE(DocumentTitle, 'Unknown') AS DocumentTitle, 
		DocCategory, 
		PageNums, 
		SettleDocID, 
		--sd.SetPymtID, 
		csd.DocumentID,
		csd.DocCategoryID
	FROM tblClaimSettlementDocuments csd 
		LEFT JOIN tblClaimSettlement cs ON cs.ClaimLawsuitID = csd.ClaimLawsuitID
		LEFT JOIN tblDocuments d         ON csd.DocumentID = d.DocumentID
		LEFT JOIN tblClaimSettlementDocumentCategory dc ON csd.DocCategoryID = dc.DocCategoryID 
		LEFT JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		LEFT JOIN tblClaimLawsuit cl ON csd.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		LEFT JOIN  tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
		WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
		 GROUP BY DocumentURL + CASE WHEN ISNUMERIC(PageNums)=1 AND CHARINDEX(',', PageNums)=0 THEN '#page='+PageNums WHEN CHARINDEX(',', PageNums)-1 > 0 THEN '#page='+LEFT(PageNums, CHARINDEX(',', PageNums)-1)
			WHEN CHARINDEX('-', PageNums)-1 > 0 THEN '#page='+LEFT(PageNums, CHARINDEX('-', PageNums)-1)
			ELSE '' END, 
		DocumentTitle, 
		PageNums, 
		DocCategory, 
		SettleDocID, 
	--	sd.SetPymtID, 
		csd.DocumentID, 
		csd.DocCategoryID,
		AmazonKey

END

	
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffSettleDocs] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffSettleDocs] TO [Ligado_User]
GO
