SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysfrmUpdatedReleaseInfo] (@ClaimantReleaseID int)
as

Select *
From m_tblClaimantReleaseReview_20110228 r
Where ClaimantReleaseID = @ClaimantReleaseID 

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmUpdatedReleaseInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmUpdatedReleaseInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmUpdatedReleaseInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysfrmUpdatedReleaseInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmUpdatedReleaseInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmUpdatedReleaseInfo] TO [power_user]
GO
