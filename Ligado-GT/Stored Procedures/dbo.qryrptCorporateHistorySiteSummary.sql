SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*-------------------------------------------------------------*/
/* MODIFIED CODE FOR qryrptCorporateHistorySiteSummary         */
/*                                                             */
/*-------------------------------------------------------------*/

CREATE PROC [dbo].[qryrptCorporateHistorySiteSummary] (@CorporateHistoryID int, @SiteID int, @SiteActivityID int, @SiteTypeID int)
AS
SET NOCOUNT ON


--DECLARE @CorporateHistoryID int 
--DECLARE @SiteID int
--DECLARE @SiteActivityID int
--DECLARE @SiteTypeID int

--SET @CorporateHistoryID =0 
--SET @SiteID =0
--SET @SiteActivityID =0
--SET @SiteTypeID =0

DECLARE @MaxCorporateHistoryID int 
DECLARE @MaxSiteID int
DECLARE @MaxSiteActivityID int
DECLARE @MaxSiteTypeID int

SET @MaxCorporateHistoryID = (Select MAX(corporatehistoryid) from tblCorporateHistoryProfile)
SET @MaxSiteID = (Select MAX(siteid) from tblEnviroSitesDetail)
set @MaxSiteActivityID = (select MAX(siteactivityid) from tblSiteActivity)
set @MaxSiteTypeID= (select MAX(sitetypeid) from tblSiteType)

SELECT e.SiteID, s.[Status], SiteName, SiteAddress, SiteCity, [State], Division,  
OwnBegDate, OwnEndDate, OpBegDate, OpEndDate, ContaminationDate, SiteNotes, CorporateHistoryProfile, SiteActivity,SiteType, ExcludedSite,
ExcludedSiteReason
FROM tblEnviroSitesDetail e
LEFT JOIN tblCorporateHistoryProfile chp ON e.CorporateHistoryID = chp.CorporateHistoryID
LEFT JOIN tblState st ON e.StateID = st.StateID
LEFT JOIN tblStatus s ON e.StatusID = s.StatusID
left join tblSiteActivity sa on sa.SiteActivityID=e.SiteActivityID
left join tblSiteType sty on sty.SiteTypeID=e.SiteTypeID

WHERE
coalesce(e.CorporateHistoryID,0) >= COALESCE(@CorporateHistoryID, 0) 
AND coalesce(e.CorporateHistoryID,0) <= COALESCE(NULLIF(@CorporateHistoryID, 0), @maxCorporateHistoryID)
AND e.SiteID >= coalesce(@SiteID,0) 
AND e.SiteID<= coalesce(nullif(@SiteID, 0), @maxsiteid)
AND coalesce(sa.SiteActivityID,0) >= coalesce(@SiteActivityID,0)
AND coalesce(sa.SiteActivityID,0)<= coalesce(nullif(@SiteActivityID,0), @maxsiteactivityid)
AND coalesce(sty.sitetypeid,0) >= coalesce(@SiteTypeID,0)
AND coalesce(sty.SiteTypeID,0) <= coalesce(nullif(@SiteTypeID,0), @maxSitetypeid)

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorporateHistorySiteSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCorporateHistorySiteSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorporateHistorySiteSummary] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCorporateHistorySiteSummary] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorporateHistorySiteSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCorporateHistorySiteSummary] TO [power_user]
GO
