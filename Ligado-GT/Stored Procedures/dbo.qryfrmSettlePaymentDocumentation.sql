SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmSettlePaymentDocumentation] (@ClaimLawsuitID int)
as

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 4552

Select spd.ClaimSettlementID
           ,spd.[ClaimLawsuitID]
      ,sd.[ClaimantIDed]
      ,sd.[DxMedicals]
      ,sd.[DODCert]
      ,sd.[EstatePapers]
      ,sd.[ProductID]
      ,sd.[EmployExpoHist]
      ,sd.[MMSEA]
      ,sd.[MedicarePlaintiff]
      ,sd.[MedicarePlaintiffCounsel]
      ,sd.[SettlementConfirmation]
      ,spd.[ReleaseRqstDate]
      ,[ReleaseToPC]
      ,[ExecutedReleaseDate]
      ,spd.[SettlePaymentComments]
      ,[ReleaseRcvd]
      ,[MedicareFullyDocumented]
      ,[MedicareReported]
      ,[ReleaseSignedDate]
      ,[ReleaseTypeID]
      ,spd.[SPUsername]
from tblClaimSettlement spd
	left join tblClaimSettlementDocumentation sd on sd.ClaimLawsuitID = spd.ClaimLawsuitID
Where spd.ClaimLawsuitID = @ClaimLawsuitID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [power_user]
GO
