SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2]
(@AllocationID int = NULL)
As

SET NOCOUNT ON

--DECLARE @allocationid int
--SET @allocationid = 1

DECLARE @qryARBySubscriberCompanyByInsProg TABLE (AllocationID int, 
												  AllocationAmtActual money, 
												  AllocationAmtAllocation money, 
												  SubscriptionCompanySolvency varchar(256))
DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM tblAllocation

DECLARE @Policy TABLE (PolicyID int)
INSERT INTO @Policy
	SELECT PolicyID
	FROM vPolicy
	GROUP BY PolicyID

INSERT INTO @qryARBySubscriberCompanyByInsProg
	SELECT a.AllocationID, 
		SUM(s.SubscriptionActualPercentOfPolicy     * ar.Allocation) AS AllocationAmtActual, 
		SUM(s.SubscriptionAllocationPercentOfPolicy * ar.Allocation) AS AllocationAmtAllocation,
		s.SubscriptionCompanySolvency	
	FROM ((@Policy p 
		INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID) 
		INNER JOIN tblAllocationResults ar ON p.PolicyID = ar.PolicyID) 
		INNER JOIN tblAllocation a ON a.AllocationID = ar.AllocationID 
	GROUP BY a.AllocationID, s.SubscriptionCompanySolvency

SELECT AllocationID, SUM(AllocationAmtActual) as SumOfAllocationActual, SUM(AllocationAmtAllocation) as SumOfAllocationAllocation, 
	CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END AS 'SubscriberSolvencyGroup'
FROM @qryARBySubscriberCompanyByInsProg
WHERE AllocationID >= COALESCE(@allocationID, 0) AND AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID)
GROUP BY AllocationID, CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END	

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARBySubscriberCompanyByInsProgBySolvency2] TO [power_user]
GO
