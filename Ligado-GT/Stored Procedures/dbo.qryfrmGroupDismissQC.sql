SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Proc [dbo].[qryfrmGroupDismissQC]

As
Set NoCount On

--DECLARE @DocumentID bigint
--set @DocumentID = 370

SELECT 
LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4)as SSN,
cpi.FirstName , Coalesce(cpi.MiddleName + ' ','')as MiddleName , cpi.LastName, Coalesce(Suffix,'') as Suffix , laws.State,cl.ClaimLawsuitID,DocketNumber
, ClaimantDocumentType, DocumentTitle, mcd.ClaimantPersonalInfoID, ClaimLawsuitStatus, StatusDate, clst.ClaimLawsuitStatusID, cl.ClaimID, CreatedBy,
D.DocumentID, d.QCBy, DocumentURL, DismissalDate
--pc.PlaintiffCounsel,
--PrimaryLocalPlaintiffCounsel,pc.State as PCState, 
--laws.StateID,
--CourtType, 
--Jurisdiction,
--CaseCaption, 


FROM tblDocuments d
left join tblMultiClaimantDocument mcd on mcd.DocumentID = d.DocumentID
left join tblClaim c on c.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
left join tblState laws on laws.StateID = l.StateID
left join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
left join tblClaimantSuffix csuf on csuf.SuffixID = cpi.SuffixID
left join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
left join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID


Where
DetailDocumentTypeID in (16,25,36) and QCBy is not Null and IsPrimaryStatus =1 and IsCurrentClaim = 1

Group By
cpi.SocialSecurityNumber,FirstName , MiddleName , cpi.LastName, Suffix , laws.State,cl.ClaimLawsuitID,DocketNumber
, ClaimantDocumentType, DocumentTitle, mcd.ClaimantPersonalInfoID, ClaimLawsuitStatus, StatusDate, clst.ClaimLawsuitStatusID, cl.ClaimID, CreatedBy,
D.DocumentID, d.QCBy, DocumentURL, DismissalDate

order by d.DocumentID,mcd.ClaimantPersonalInfoID, LastName, FirstName, DocketNumber
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmGroupDismissQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmGroupDismissQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmGroupDismissQC] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmGroupDismissQC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmGroupDismissQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmGroupDismissQC] TO [power_user]
GO
