SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmAllegationInfo] (@ClaimLawsuitID int)

as
 
Select AllegationID, 
	ClaimLawsuitID, 
	DefendantCompanyID, 
	AllegationTypeID, 
	DamageID, 
	MaterialID
From tblAllegation
Where ClaimLawsuitID = @ClaimLawsuitID
Order By DefendantCompanyID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllegationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllegationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllegationInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmAllegationInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllegationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllegationInfo] TO [power_user]
GO
