SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] (@DocumentID bigint, 
															@DocCategoryID int, 
															@PageNums nvarchar(20), 
															@SetPymtID bigint = NULL,
															@SPUsername nvarchar(250))
AS
SET NOCOUNT ON

--DECLARE @ClaimantPersonalInfoID int
--DECLARE @DocumentID int
--DECLARE @DocCategoryID int
--DECLARE @PageNums varchar
--SET @ClaimantPersonalInfoID = 91864
--SET @DocumentID             = 973
--SET @DocCategoryID          = 4 
--SET @PageNums               = '1'


DECLARE @ClaimantPersonalInfoID int

/**Declare cursor to attach documentation to each claimant the document is attached to**/
DECLARE Cur_Claimants CURSOR Local Fast_Forward FOR
	SELECT ClaimantpersonalinfoID 
	FROM tblmulticlaimantdocument 
	WHERE documentid = @DocumentID

OPEN Cur_Claimants
FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @ClaimLawsuitID int
	SET @ClaimLawsuitID = (SELECT ClaimlawsuitID 
						   FROM tblClaimantPersonalInfo cpi 
						       INNER JOIN tblClaim c		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID 
						       INNER JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID 
						   WHERE c.IsCurrentClaim = 1 
						       AND cl.IsPrimaryForClaim = 1 
						       AND cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID)


	--if there is no record for this claim in tblClaimSettlementDocumentation then create one
	IF (SELECT COUNT(ClaimLawsuitID) FROM tblClaimSettlementDocumentation WHERE ClaimLawsuitID = @ClaimLawsuitID) = 0 
	BEGIN
		INSERT INTO tblClaimSettlementDocumentation (ClaimLawsuitID)
			SELECT @ClaimLawsuitID
	END
	

	--insert settlementdocumentation recordusing settlement payment information ID
	INSERT INTO tblClaimSettlementDocuments (DocumentID, DocCategoryID, PageNums, ClaimLawsuitID, SPUsername)
		SELECT @DocumentID, @DocCategoryID, @PageNums, @ClaimLawsuitID, @SPUsername
	

	FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID
END

CLOSE Cur_Claimants
DEALLOCATE Cur_Claimants
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryInsSettlementInfoDocument] TO [power_user]
GO
