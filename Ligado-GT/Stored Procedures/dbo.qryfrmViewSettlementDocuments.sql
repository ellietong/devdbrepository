SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmViewSettlementDocuments] (@ClaimLawsuitID int)
as

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 17075

Select ClaimantDocumentID, 
UPPER(DocCategory) + ': PACE Document - ' + ClaimantDocumentType + coalesce(' (' + pagenums + ')', '') AS DocumentTypeWithDate, 
	'https://admin.kcicextranet.com' + DocumentURL AS DocumentLink
From tblSettleDocs sd
INNER JOIN tblSettlePaymentDocumentation pd ON sd.SetPymtID = pd.SetPymtID
LEFT JOIN tblDocumentCategory cd ON sd.DocCategoryID = cd.DocCategoryID
LEFT JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblClaimantDocuments d ON sd.DocumentID = d.ClaimantDocumentID 
LEFT JOIN tblClaimantDocumentType dt ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
Where pd.ClaimLawsuitID = @ClaimLawsuitID

UNION

SELECT sd.DocumentID,
case when DateCreated is NULL then DocCategory else UPPER(DocCategory) + ': ' + DocumentTitle + ' - ' + convert(nvarchar,month(DateCreated))+'/'+convert(nvarchar,day(DateCreated))+
	'/'+convert(nvarchar,Year(DateCreated)) end  + coalesce(' (' + pagenums + ')', '')  AS DocumentTypeWithDate,
	'https://admin.kcicextranet.com' + DocumentURL AS DocumentLink
FROM tblSettleDocs sd
INNER JOIN tblSettlePaymentDocumentation pd ON sd.SetPymtID = pd.SetPymtID
LEFT JOIN tblDocumentCategory cd ON sd.DocCategoryID = cd.DocCategoryID
LEFT JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
INNER JOIN tblDocuments d ON sd.DocumentID = d.DocumentID 
WHERE pd.ClaimLawsuitID = @ClaimLawsuitID
ORDER BY DocumentTypeWithDate

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewSettlementDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewSettlementDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewSettlementDocuments] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmViewSettlementDocuments] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmViewSettlementDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmViewSettlementDocuments] TO [power_user]
GO
