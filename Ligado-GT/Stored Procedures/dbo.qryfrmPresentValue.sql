SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmPresentValue]
(@YieldCurveID int, @rate decimal (18,15), @MethodValue int, @Interest bit, @todaysdate datetime, @AllocationID int)
AS
SET NOCOUNT ON


---------------------------------------------------------------------------------------
--	Test Variables -- Leave commented unless testing
---------------------------------------------------------------------------------------

--DECLARE @YieldCurveID int
--DECLARE @rate decimal (18,15)
--DECLARE @MethodValue int
--DECLARE @Interest bit
--DECLARE @todaysdate datetime
--DECLARE @AllocationID int

--SET @YieldCurveID = 140
--SET @rate = NULL
--SET @MethodValue = 1
--SET @Interest = 0
--SET @todaysdate = GETDATE()
--SET @AllocationID = 2


---------------------------------------------------------------------------------------
--	DECLARE AND SET Variables 
---------------------------------------------------------------------------------------
DECLARE @maxAllocationID int
DECLARE @maxYieldCurveID int
DECLARE @minYieldDate date
DECLARE @maxYieldDate date
DECLARE @maxYieldRate decimal(18,15)

SELECT @maxYieldCurveID = MAX(YieldCurveID) FROM tblYieldRate
SELECT @maxAllocationID = MAX(AllocationID) FROM tblAllocation
SELECT @minYieldDate = MIN(expectedpaydate) FROM tblYieldRate WHERE YieldCurveID >= COALESCE(@YieldCurveID, 0) 
						AND YieldCurveID <= COALESCE(NULLIF(@YieldCurveID, 0), @maxYieldCurveID) 
SELECT @maxYieldDate = MAX(expectedpaydate) FROM tblYieldRate WHERE YieldCurveID >= COALESCE(@YieldCurveID, 0) 
						AND YieldCurveID <= COALESCE(NULLIF(@YieldCurveID, 0), @maxYieldCurveID) 
SELECT @maxYieldRate =  TBondRate FROM tblYieldRate 
						WHERE YieldCurveID >= COALESCE(@YieldCurveID, 0) 
						AND YieldCurveID <= COALESCE(NULLIF(@YieldCurveID, 0), @maxYieldCurveID) 
						AND ExpectedPayDate = @maxYieldDate 						


---------------------------------------------------------------------------------------
--	Update Present Value
---------------------------------------------------------------------------------------
--The PV Function will take all different equation needs into consideration
--by setting the value of "n" to 0 if ExpectedPayDate < DatetoPV.

--Only need to test for which Method to use and then feed the correct values to the equation.
---------------------------------------------------------------------------------------
-- 	Run on Yield Curve
---------------------------------------------------------------------------------------
IF @MethodValue = 1
BEGIN
	UPDATE ar
	SET ar.PV = dbo.fnPresentValue(ar.Allocation, yr.TBondRate, o.ExpectedPayDate, @todaysdate, @Interest), 
	ar.DateToPresentValue = @todaysdate, ar.PVRate = yr.TBondRate, ar.YieldCurveID = @YieldCurveID 
	FROM (tblAllocationResults ar INNER JOIN tblOccurrence o ON o.OccurrenceID = ar.OccurrenceID) INNER JOIN tblYieldRate yr ON YEAR(o.ExpectedPayDate) = YEAR(yr.ExpectedPayDate)
	WHERE ar.AllocationID >= COALESCE(@allocationID, 0) AND ar.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) AND 
		yr.YieldCurveID >= COALESCE(@YieldCurveID, 0) AND yr.YieldCurveID <= COALESCE(NULLIF(@YieldCurveID, 0), @maxYieldCurveID) 
		AND o.ExpectedPayDate <= @maxYieldDate
	UPDATE ar
	SET ar.PV = dbo.fnPresentValue(ar.Allocation, @maxYieldRate, o.ExpectedPayDate, @todaysdate, @Interest), 
	ar.DateToPresentValue = @todaysdate, ar.PVRate = @maxYieldRate, ar.YieldCurveID = @YieldCurveID 
	FROM (tblAllocationResults ar INNER JOIN tblOccurrence o ON o.OccurrenceID = ar.OccurrenceID) 
	WHERE ar.AllocationID >= COALESCE(@allocationID, 0) AND ar.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) AND 
		 o.ExpectedPayDate > @maxYieldDate
END
---------------------------------------------------------------------------------------
-- 	Run on Fixed Rate
---------------------------------------------------------------------------------------
IF @MethodValue = 2
BEGIN
	UPDATE ar
	SET ar.PV =  dbo.fnPresentValue(ar.Allocation, @rate, o.ExpectedPayDate, @todaysdate, @Interest), ar.DateToPresentValue = @todaysdate, 
	ar.PVRate = @rate
	FROM tblAllocationResults ar INNER JOIN tblOccurrence o ON ar.OccurrenceID = o.OccurrenceID
	WHERE ar.AllocationID >= COALESCE(@allocationID, 0) AND ar.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID)
END

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPresentValue] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPresentValue] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPresentValue] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPresentValue] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPresentValue] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPresentValue] TO [power_user]
GO
