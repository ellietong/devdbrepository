SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[qryrptCompLangConsistencyChecksByCarrier]
(@limitTypeID int = NULL, @carrierid int = NULL, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL, 
@lowAttach nvarchar(50) = NULL, @highAttach nvarchar(50) = NULL, @selectedProvisions nvarchar(1000), @selectedProvisionValues nvarchar(1000))
AS

SET NOCOUNT ON

--DECLARE @limitTypeID int
--DECLARE @carrierID int
--DECLARE @provisionlabelID int
--DECLARE @provisionvalueID int
--DECLARE @lowAttach nvarchar(50) 
--DECLARE @highAttach nvarchar(50) 
--DECLARE @CarrierSolvency nvarchar(50)
--DECLARE @CarrierGroupID int
--SET @limitTypeID = 1
--SET @carrierid = 0
--SET @provisionlabelid = 0
--SET @provisionvalueid = 0
--SET @lowAttach = null
--SET @highAttach = null
--SET @CarrierSolvency = '<<ALL>>'
--SET @CarrierGroupID = 0

----Declare the lists of selected IDs; note: these will be passed in, and thus should be commented out in actual stored proc
--DECLARE @selectedProvisions nvarchar(1000)
--SET @selectedProvisions = '0'
--DECLARE @selectedProvisionValues nvarchar(1000)
--SET @selectedProvisionValues = '0'



DECLARE @maxLimitTypeID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)
DECLARE @maxAttach money

SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy
SELECT @maxAttach = MAX(AttachmentPoint) FROM vPolicy


--Declare backup strings of the passed in lists to be used in the ultimate where clauses below
DECLARE @selectedProvisionsList nvarchar(1000)
SET @selectedProvisionsList = @selectedProvisions
DECLARE @selectedProvisionValuesList nvarchar(1000)
SET @selectedProvisionValuesList = @selectedProvisionValues

--Declare the tables to be used for the separated provision and provision value IDs
DECLARE @selectedProvisionsTable TABLE(provisionLabelID int)
DECLARE @selectedProvisionValuesTable TABLE(provisionValueID int)

--Prepare flags if there is only a zero in the table (and thus there should be no filter)
DECLARE @flagProvisionFilter int
DECLARE @flagProvisionValueFilter int
SET @flagProvisionFilter = 0
SET @flagProvisionValueFilter = 0

--Parse out provision and provision value ID strings	
WHILE LEN(@selectedProvisions)>0 BEGIN
	IF CHARINDEX(',',@selectedProvisions) = 0 BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, @selectedProvisions)
		IF CONVERT(int,@selectedProvisions) <> 0
			SET @flagProvisionFilter = 1
		SET @selectedProvisions = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, LEFT(@selectedProvisions, CHARINDEX(',',@selectedProvisions)-1))
		SET @selectedProvisions = RIGHT(@selectedProvisions,LEN(@selectedProvisions)-CHARINDEX(',',@selectedProvisions))
		SET @flagProvisionFilter = 1
	END
END
WHILE LEN(@selectedProvisionValues)>0 BEGIN
	IF CHARINDEX(',',@selectedProvisionValues) = 0 BEGIN
		INSERT INTO @selectedProvisionValuesTable
			SELECT CONVERT(int, @selectedProvisionValues)
		IF CONVERT(int,@selectedProvisionValues) <> 0
			SET @flagProvisionValueFilter = 1
		SET @selectedProvisionValues = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedProvisionValuesTable
			SELECT CONVERT(int, LEFT(@selectedProvisionValues, CHARINDEX(',',@selectedProvisionValues)-1))
		SET @selectedProvisionValues = RIGHT(@selectedProvisionValues,LEN(@selectedProvisionValues)-CHARINDEX(',',@selectedProvisionValues))
		SET @flagProvisionValueFilter = 1
	END
END



IF @flagProvisionFilter = 1 AND @flagProvisionValueFilter = 1
		SELECT CarrierName, p.PolicyID, PolicyNum, MIN(ActualPStartDate) AS 'StartDate', MAX(ActualPEndDate) AS 'EndDate',
						AttachmentPoint, PerOccLimit, LayerPerOccLimit, [Broker],
						pr.ProvisionLabelID, Provision, pr.ProvisionValueID, ProvisionValue
		FROM tblPolicy p 
						INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID 
						INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID
						INNER JOIN tblProvisionLabel plab ON pr.ProvisionLabelID = plab.ProvisionLabelID
						INNER JOIN tblPolicyLimits pl ON p.PolicyID = pl.PolicyID 
						INNER JOIN tblPolicyDates d ON p.PolicyID = d .PolicyID 
						INNER JOIN tblProvisionValue pv ON pr.ProvisionValueID = pv.ProvisionValueID
						LEFT JOIN tblBrokerPolicy bp ON p.PolicyID = bp.PolicyID
						LEFT JOIN tblBroker b ON bp.BrokerID = b.BrokerID 
		WHERE LimitTypeID >= COALESCE(@limitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)
						AND p.CarrierID >= COALESCE(@carrierID, 0) AND p.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) 
						AND (coalesce(CarrierGroupID,0) >= COALESCE(@carrierGroupID, 0) AND coalesce(CarrierGroupID,0) <= COALESCE(NULLIF(@carrierGroupID,0), @maxCarrierGroupID)) 
						AND CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') 
						AND	AttachmentPoint BETWEEN COALESCE(CONVERT(money, @lowAttach), 0) AND COALESCE(CONVERT(money, @highAttach), @maxAttach)
					AND pr.ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) AND 
						(pr.ProvisionValueID IN(SELECT * FROM @selectedProvisionValuesTable) OR 
						(pr.ProvisionValueID IN(SELECT ProvisionValueID FROM tblProvisionValue WHERE ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) AND ProvisionLabelID NOT IN(SELECT ProvisionLabelID FROM tblProvisionValue WHERE ProvisionValueID IN(@selectedProvisionValuesList)))))
		GROUP BY CarrierName, p.PolicyID, PolicyNum,
						AttachmentPoint, PerOccLimit, LayerPerOccLimit, [Broker],
						pr.ProvisionLabelID, Provision, pr.ProvisionValueID, ProvisionValue
		ORDER BY CarrierName, Provision, PolicyID
		
ELSE IF @flagProvisionFilter = 1
		SELECT CarrierName, p.PolicyID, PolicyNum, MIN(ActualPStartDate) AS 'StartDate', MAX(ActualPEndDate) AS 'EndDate',
						AttachmentPoint, PerOccLimit, LayerPerOccLimit, [Broker],
						pr.ProvisionLabelID, Provision, pr.ProvisionValueID, ProvisionValue
		FROM tblPolicy p 
						INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID 
						INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID
						INNER JOIN tblProvisionLabel plab ON pr.ProvisionLabelID = plab.ProvisionLabelID
						INNER JOIN tblPolicyLimits pl ON p.PolicyID = pl.PolicyID 
						INNER JOIN tblPolicyDates d ON p.PolicyID = d .PolicyID 
						INNER JOIN tblProvisionValue pv ON pr.ProvisionValueID = pv.ProvisionValueID
						LEFT JOIN tblBrokerPolicy bp ON p.PolicyID = bp.PolicyID
						LEFT JOIN tblBroker b ON bp.BrokerID = b.BrokerID 
		WHERE LimitTypeID >= COALESCE(@limitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)
						AND p.CarrierID >= COALESCE(@carrierID, 0) AND p.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) 
						AND (coalesce(CarrierGroupID,0) >= COALESCE(@carrierGroupID, 0) AND coalesce(CarrierGroupID,0) <= COALESCE(NULLIF(@carrierGroupID,0), @maxCarrierGroupID)) 
						AND CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') 
						AND	AttachmentPoint BETWEEN COALESCE(CONVERT(money, @lowAttach), 0) AND COALESCE(CONVERT(money, @highAttach), @maxAttach)
					AND pr.ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) 
		GROUP BY CarrierName, p.PolicyID, PolicyNum,
						AttachmentPoint, PerOccLimit, LayerPerOccLimit, [Broker],
						pr.ProvisionLabelID, Provision, pr.ProvisionValueID, ProvisionValue
		ORDER BY CarrierName, Provision, PolicyID
ELSE
SELECT CarrierName, p.PolicyID, PolicyNum, MIN(ActualPStartDate) AS 'StartDate', MAX(ActualPEndDate) AS 'EndDate',
						AttachmentPoint, PerOccLimit, LayerPerOccLimit, [Broker],
						pr.ProvisionLabelID, Provision, pr.ProvisionValueID, ProvisionValue
		FROM tblPolicy p 
						INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID 
						INNER JOIN tblProvision pr ON p.PolicyID = pr.PolicyID
						INNER JOIN tblProvisionLabel plab ON pr.ProvisionLabelID = plab.ProvisionLabelID
						INNER JOIN tblPolicyLimits pl ON p.PolicyID = pl.PolicyID 
						INNER JOIN tblPolicyDates d ON p.PolicyID = d .PolicyID 
						INNER JOIN tblProvisionValue pv ON pr.ProvisionValueID = pv.ProvisionValueID
						LEFT JOIN tblBrokerPolicy bp ON p.PolicyID = bp.PolicyID
						LEFT JOIN tblBroker b ON bp.BrokerID = b.BrokerID 
		WHERE LimitTypeID >= COALESCE(@limitTypeID, 0) AND LimitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)
						AND p.CarrierID >= COALESCE(@carrierID, 0) AND p.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) 
						AND (coalesce(CarrierGroupID,0) >= COALESCE(@carrierGroupID, 0) AND coalesce(CarrierGroupID,0) <= COALESCE(NULLIF(@carrierGroupID,0), @maxCarrierGroupID)) 
						AND CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') 
						AND	AttachmentPoint BETWEEN COALESCE(CONVERT(money, @lowAttach), 0) AND COALESCE(CONVERT(money, @highAttach), @maxAttach)
		GROUP BY CarrierName, p.PolicyID, PolicyNum,
						AttachmentPoint, PerOccLimit, LayerPerOccLimit, [Broker],
						pr.ProvisionLabelID, Provision, pr.ProvisionValueID, ProvisionValue
		ORDER BY CarrierName, Provision, PolicyID



RETURN





GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCompLangConsistencyChecksByCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCompLangConsistencyChecksByCarrier] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCompLangConsistencyChecksByCarrier] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCompLangConsistencyChecksByCarrier] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCompLangConsistencyChecksByCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCompLangConsistencyChecksByCarrier] TO [power_user]
GO
