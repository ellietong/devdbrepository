SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmSubscriberCompany]
AS
SET NOCOUNT ON
SELECT SubscriberID, SubscriptionCompany, SubscriptionCompanySolvency, convert(nvarchar(1000), SubscriptionCompanyNote) as SubscriptionCompanyNote, CarrierGroupName
FROM tblSubscriberCompany s
LEFT JOIN tblCarrierGroup cg ON s.CarrierGroupID = cg.CarrierGroupID
GROUP BY SubscriberID, SubscriptionCompany, SubscriptionCompanySolvency, convert(nvarchar(1000), SubscriptionCompanyNote), CarrierGroupName
ORDER BY SubscriptionCompany

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriberCompany] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriberCompany] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriberCompany] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriberCompany] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriberCompany] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriberCompany] TO [power_user]
GO
