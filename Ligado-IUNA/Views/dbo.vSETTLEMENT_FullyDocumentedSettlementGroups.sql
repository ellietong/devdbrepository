SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vSETTLEMENT_FullyDocumentedSettlementGroups]
AS

SELECT DISTINCT SettlementGroupID, 1 AS SettlementGroupFullDocumented
FROM tblClaimantPersonalInfo cpi
	LEFT JOIN tblClaim c						  ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimDisease cd                  ON c.ClaimID = cd.ClaimID
	LEFT JOIN tblClaimLawsuit cl				  ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblClaimSettlement cs			      ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaimSettlementDocumentation csd ON csd.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblLawsuit l						  ON l.LawsuitID = cl.LawsuitID
	LEFT JOIN tblClaimantEmployment ce			  ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantProduct cp				  ON ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
WHERE cs.SettlementAmount > 0 
	AND PrimaryDisease = 1
	AND (COALESCE(csd.SettlementConfirmation,0) <> 0)
	AND (COALESCE(csd.ClaimantIDed,          0) <> 0)
	AND (COALESCE(csd.DxMedicals,            0) <> 0)
	AND (COALESCE(csd.ProductID,             0) <> 0)
	AND (COALESCE(EmployExpoHist,            0) <> 0)
	AND (CASE WHEN DeceasedDate IS NULL THEN 1 ELSE COALESCE(csd.DODCert,     0) END <> 0)
	AND (CASE WHEN DeceasedDate IS NULL THEN 1 ELSE COALESCE(csd.EstatePapers,0) END <> 0)
	AND (csd.ReleaseRqstDate    IS NOT NULL)
	AND (cs.ReleaseToPC         IS NOT NULL)
	AND (ExecutedReleaseDate    IS NOT NULL)
	AND (DiseaseDiagnosisDate   IS NOT NULL)
	AND (FileDate               IS NOT NULL)
	AND (cp.ProductID           IS NOT NULL)
GO
GRANT VIEW DEFINITION ON  [dbo].[vSETTLEMENT_FullyDocumentedSettlementGroups] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSETTLEMENT_FullyDocumentedSettlementGroups] TO [Ligado_User]
GO
