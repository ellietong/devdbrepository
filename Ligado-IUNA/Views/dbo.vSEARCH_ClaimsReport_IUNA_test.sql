SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vSEARCH_ClaimsReport_IUNA_test]
AS 
/*=====================================================================================================================
Purpose:  Rewrite from the original vSEARCH_ClaimsReport to make the code easier to follow
Author:   Ellie Tong
Date:     02/02/2015
Example: SELECT
       CLAIMLAWSUITID			   
       ,ClaimantPersonalInfoID	    
       ,ActiveTrialDate			    
       ,ClaimID					    
       ,ClaimLawsuitStatus		    
       ,Matter					    
       ,ClaimSequence			         
       ,FullName				    
       ,BirthDate				    
       ,DeceasedDate			    
       ,SocialSecurityNumber                                       
       ,Occupation                                    
       ,HighLevelDisease      
	   from vSEARCH_ClaimsReport_IUNA_test
	   where claimid = 1
=====================================================================================================================*/
with settlementPaymentInfo as (
SELECT CLAIMSETTLEMENTID AS SettlementPaymentInfoClaimSettlementID
        , MIN(CHECKID)    AS EarliestCheckID
 FROM   dbo.TBLPAYMENTSETTLEMENT
        LEFT OUTER JOIN (SELECT CHECKID AS checkInfoCheckID
                         FROM   dbo.TBLCHECK) checkInfo
                     ON ( dbo.TBLPAYMENTSETTLEMENT.CHECKID = checkInfo.CHECKINFOCHECKID )
 GROUP  BY CLAIMSETTLEMENTID) 
 , prodInfo as  ( 
 SELECT claimantemploymentid AS prodInfoClaimentEmploymentId
        , productid
        , productnotes
        , startdate
 FROM   dbo.tblclaimantproduct   
  ) 
 ,dedupfnltbl as (
    SELECT  claws.CLAIMLAWSUITID
	      , ACTIVETRIALDATE
          , claws.CLAIMID
		  , laws.lawsuitid
          , ISMDL          
          , CLAIMLAWSUITSTATUS		  
          , cllawsttrak.CLAIMLAWSUITSTATUSID
          , MATTER
          , CLAIMSEQUENCE
		  , claim.CLAIMANTPERSONALINFOID
          , FIRSTNAME
          , LASTNAME
          , BIRTHDATE
          , DECEASEDDATE
          , ssn4digit = RIGHT(SOCIALSECURITYNUMBER, 4)
          , OCCUPATION
          , HIGHLEVELDISEASE
		  , DOCKETNUMBER
          , laws.SERVICEDATE
          , FILEDATE
          , laws.RECEIVEDDATE
          , st.[STATE]
          , JURISDICTION
          , PROCESSEDDATE
          , STATUSDATE
          , PRODUCTNOTES
          , startdate
          , SETTLEMENTDATE
		  , SETTLEMENTPAIDDATE = PaidDate
          , SETTLEMENTAMOUNT
          , SOURCEDATE
          , plntcnsladdr.PLAINTIFFCOUNSEL
          , laws.STATEID
          , jurs.JURISDICTIONID
          , hldis.HIGHLEVELDISEASEID
          , plntcnsladdr.PLAINTIFFCOUNSELID
          , DEFENSECOUNSEL
          , DEFENSECOUNSELID
          , laws.DEFENSECOUNSELADDRESSID
		  , DefenseCounselAddressID2 = dejur.DefenseCounselAddressID
          , claim.MATTERID
          , ALLEGATIONTYPEID
		  , NationalPlaintiffCounselAddressID = plntcnsladdr.PlaintiffCounselAddressID
		  , NationalPlaintiffCounselID = plntcnsladdr.PlaintiffCounselID
		   ,NationalPlaintiffCounsel = plntcnsladdr.PlaintiffCounsel
		  , ClaimLawsuitNotes
		  , TrialNotes = ClaimLawsuitNotes
		--  IUNA NOT USING THESE
		  --, CLIENTFILEDATE 
		   ,ID = row_number() over (partition by claws.CLAIMLAWSUITID, claws.CLAIMID,laws.lawsuitid,claim.claimantpersonalinfoid,claimantemploymentid,cllawsttrak.CLAIMLAWSUITSTATUSID, hldis.HIGHLEVELDISEASEID
		  ,allegationtypeid, plntcnsladdr.PLAINTIFFCOUNSELID,DEFENSECOUNSELID,laws.DEFENSECOUNSELADDRESSID, dejur.DEFENSECOUNSELADDRESSID
		  order by claws.CLAIMLAWSUITID)	
		  --,ID = row_number() over (partition by claws.CLAIMLAWSUITID, claws.CLAIMID,cllawsttrak.CLAIMLAWSUITSTATUSID, hldis.HIGHLEVELDISEASEID
		  --,allegationtypeid, plntcnsladdr.PLAINTIFFCOUNSELID,DEFENSECOUNSELID,laws.DEFENSECOUNSELADDRESSID, dejur.DEFENSECOUNSELADDRESSID
		  --order by claws.CLAIMLAWSUITID)
	  		
		 --select count(1)
	from dbo.TBLCLAIMLAWSUIT claws -- CLInfo	
	left join dbo.TBLCLAIMSETTLEMENT clsett
	on claws.CLAIMLAWSUITID = clsett.CLAIMLAWSUITID 
		
	left join settlementPaymentInfo settinfo
	on clsett.ClaimSettlementID = settinfo.SettlementPaymentInfoClaimSettlementID
	left join dbo.TBLCHECK chk
	on settinfo.EarliestCheckID = chk.CheckID  -- CLInfo

	left join dbo.TBLALLEGATION alleg  -- lawsuitstatusinfo
	on alleg.CLAIMLAWSUITID = claws.CLAIMLAWSUITID
	left join dbo.TBLCLAIMLAWSUITSTATUSTRACKING cllawsttrak
	on cllawsttrak.ClaimLawsuitID = claws.ClaimLawsuitID
	and isnull(ISPRIMARYSTATUS,0) = 1		 
	left join dbo.TBLCLAIMLAWSUITSTATUS cllawstat
	on cllawstat.ClaimLawsuitStatusID = cllawsttrak.ClaimLawsuitStatusID -- lawsuitstatusinfo

	left join dbo.TBLLAWSUIT laws -- LawsuitLevelInfo
	on laws.LawsuitID = claws.LawsuitID
	left join dbo.TBLSTATE st
	on st.StateID = laws.StateID
	left join dbo.TBLJURISDICTION jurs
	on jurs.JurisdictionID = laws.JurisdictionID
	left join dbo.TBLPLAINTIFFCOUNSELADDRESS plntcnsladdr
	on plntcnsladdr.PlaintiffCounselAddressID = laws.PlaintiffCounselAddressID
	left join dbo.TBLPLAINTIFFCOUNSELADDRESS natplntcnsladdr
	on natplntcnsladdr.PlaintiffCounselID = laws.NationalPlaintiffCounselID  
	-- this is the original join natplntcnsladdr.PlaintiffCounselAddressID = laws.NationalPlaintiffCounselID 
	left join dbo.TBLDEFENSECOUNSELADDRESS decnsladdr
	on decnsladdr.DefenseCounselAddressID = laws.DefenseCounselAddressID -- LawsuitLevelInfo

	left join dbo.TBLCLAIM claim -- ClaimLevelInfo
	on claim.ClaimID = claws.ClaimID	
	left join dbo.TBLMATTER mtr
	on mtr.MatterID = claim.MatterID
	left join dbo.TBLCLAIMSEQUENCE cseq
	on cseq.ClaimSequenceID = claim.ClaimSequenceID
	left join dbo.TBLCLAIMDISEASE cdis
	on cdis.ClaimID = claim.ClaimID
	and isnull(cdis.PRIMARYDISEASE,0) = 1
	left join dbo.TBLDISEASE dis
	on cdis.DiseaseID = dis.DiseaseID
	left join dbo.TBLHIGHLEVELDISEASE hldis
	on hldis.HighLevelDiseaseID = dis.Highleveldiseaseid -- ClaimLevelInfo
	
	left join dbo.TBLCLAIMANTPERSONALINFO Clmnt --ClaimantLevelInfo
	on clmnt.ClaimantPersonalInfoID = claim.ClaimantPersonalInfoID
	left join dbo.TBLCLAIMANTEMPLOYMENT clemp
	on clemp.ClaimantPersonalInfoID = clmnt.ClaimantPersonalInfoID
    and claim.CLAIMANTPERSONALINFOID = clemp.ClaimantPersonalInfoID 
	and ISPRIMARY = 1
	left join dbo.TBLOCCUPATION occ
	on occ.OccupationID = clemp.OccupationID
	left join prodInfo epemp
	on clemp.CLAIMANTEMPLOYMENTID = epemp.PRODINFOCLAIMENTEMPLOYMENTID --ClaimantLevelInfo
	
	left join dbo.TBLDEFENSEJURISDICTION dejur
	on dejur.StateID = laws.StateID
	and dejur.JurisdictionID = laws.JurisdictionID
	where 
	 isnull(ISPRIMARYFORCLAIM,0) = 1
	-- and isnull(ISPRIMARYSTATUS,0) = 1	
	)
	 SELECT
       ClaimLawsuitID AS ClaimLawsuitID
       , ClaimantPersonalInfoID AS ClaimantPersonalInfoID
       , ActiveTrialDate AS ActiveTrialDate
       , ClaimID AS ClaimID
       , ClaimLawsuitStatus AS ClaimLawsuitStatus
       , Matter AS Matter
       , ClaimSequence AS ClaimSequence
       --, firstname
       --, lastname
       , lastname + ', ' + firstname                       AS FullName
       , BirthDate AS BirthDate
       , DeceasedDate AS DeceasedDate
       , ( CASE
             WHEN ssn4digit IS NOT NULL THEN 'XXX-XX-' + ssn4digit
             ELSE NULL
           END )                                           SocialSecurityNumber
       , Occupation AS Occupation
       , HighLevelDisease AS HighLevelDisease
       , DocketNumber AS DocketNumber
       , ServiceDate AS ServiceDate
       , COALESCE(filedate, receiveddate)                  AS FileDate
       , [State] AS State
       , Jurisdiction AS Jurisdiction
       , ProcessedDate AS ProcessedDate
       , StatusDate AS StatusDate
       , ProductNotes AS ProductNotes
       , StartDate                                         AS ProductExposureStart
       , SettlementDate AS SettlementDate
	   , SETTLEMENTPAIDDATE AS SettlementPaidDate -- SettlementPaidDate is used by PPG
       , SettlementAmount AS SettlementAmount
       , SourceDate                                        --AS SettlementPaidDate -- Date paid; source date is the source date of the first check that is cut for a settlement
       , PlaintiffCounsel AS PlaintiffCounsel
       , ClaimLawsuitStatusID AS ClaimLawsuitStatusID
       , StateID AS StateID
       , JurisdictionID AS JurisdictionID
       , IsMDL AS IsMDL
       , HighLevelDiseaseID AS HighLevelDiseaseID
       , AllegationTypeID AS AllegationTypeID
       , PlaintiffCounselID AS PlaintiffCounselID
       , MatterID AS MatterID
       , DefenseCounselAddressID
       , DefenseCounselAddressID2
	   , 0 AS ClaimLawsuitStatusGroupID --NOTE: this is a dummy field because Marley doesn't require it but we still need the field. SK, 1/27/2014 (launch day)
	   , CONVERT(INT, NULL) AS PersonID, CONVERT(INT, NULL) AS BrandID, CONVERT(NVARCHAR(50), NULL) AS Brand, CONVERT(INT, NULL) AS BankruptcyStayID, CONVERT(NVARCHAR(50), NULL) AS BankruptcyStay
	   , CONVERT(INT, NULL) AS ClaimLawsuitTypeID, CONVERT(NVARCHAR(50), NULL) AS ClaimLawsuitType, CONVERT(NVARCHAR(100), NULL) AS JobsiteCity, CONVERT(INT, NULL) AS JobsiteStateID, CONVERT(NVARCHAR(1000), NULL) AS Jobsite, CONVERT(INT, NULL) AS BusinessCaseID, CONVERT(NVARCHAR(50), NULL) AS BusinessCase
	   , NationalPlaintiffCounselAddressID
	   , NationalPlaintiffCounselID
	   , NationalPlaintiffCounsel
	   , ClaimLawsuitNotes
	   , TrialNotes -- FYI TrialNotes is literally the same as ClaimLawsuitNotes; it is here so that GSX can refer to it this way in their CSV exports.
	   from dedupfnltbl
	   where ID = 1
	
GO
