SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation]
AS

SELECT main.ClaimLawsuitID,
	dc.DocCategory AS MissingDocumentation,
	main.DocCategoryID
FROM (SELECT ClaimLawsuitID, 
		  MissingDocumentation, 
		  Satisfied,
		  CASE WHEN MissingDocumentation = 'ClaimantIDed'           THEN  1
			   WHEN MissingDocumentation = 'DxMedicals'             THEN  2
			   WHEN MissingDocumentation = 'DODCert'                THEN  3
			   WHEN MissingDocumentation = 'EstatePapers'           THEN  4
			   WHEN MissingDocumentation = 'ProductID'              THEN  5
			   WHEN MissingDocumentation = 'EmployExpoHist'         THEN  6
			   WHEN MissingDocumentation = 'CompServ'               THEN  7
			   WHEN MissingDocumentation = 'SettlementConfirmation' THEN  8
			   WHEN MissingDocumentation = 'MMSEA'                  THEN  9
			   WHEN MissingDocumentation = 'PreTrialReport'         THEN 21
			   WHEN MissingDocumentation = 'ExecutedRelease'        THEN 22
			   WHEN MissingDocumentation = 'AllstateMMSEAForm'      THEN 23 END AS DocCategoryID
	  FROM (SELECT DISTINCT cl.ClaimLawsuitID, 
				COALESCE (sd.ClaimantIDed,           0) AS ClaimantIDed, 
				COALESCE (sd.DxMedicals,             0) AS DxMedicals, 
				CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE (sd.DODCert,      0) END AS DODCert, 
				CASE WHEN DeceasedDate IS NULL THEN 2 ELSE COALESCE (sd.EstatePapers, 0) END AS EstatePapers,
				COALESCE (sd.ProductID,              0) AS ProductID,
				COALESCE (sd.EmployExpoHist,         0) AS EmployExpoHist, 
				COALESCE (sd.CompServ,               0) AS CompServ,
				COALESCE (sd.SettlementConfirmation, 0) AS SettlementConfirmation, 
				COALESCE (sd.MMSEA,                  0) AS MMSEA, 
				COALESCE (sd.PreTrialReport,         0) AS PreTrialReport,
				COALESCE (sd.ExecutedRelease,        0) AS ExecutedRelease,  
				COALESCE (sd.AllstateMMSEAForm,      0) AS AllstateMMSEAForm
			FROM tblClaim c 
				INNER      JOIN tblClaimantPersonalInfo         cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
				INNER      JOIN tblClaimLawsuit                 cl  ON c.ClaimID                = cl.ClaimID 
				INNER      JOIN tblClaimSettlement              cs  ON cs.ClaimLawsuitID        = cl.ClaimLawsuitID 
				LEFT       JOIN tblClaimSettlementDocumentation sd  ON cl.ClaimLawsuitID        = sd.ClaimLawsuitID 
				LEFT OUTER JOIN tblClaimLawsuitStatusTracking   cls ON cls.ClaimLawsuitID       = cl.ClaimLawsuitID
			WHERE cls.IsPrimaryStatus = 1
				AND cls.ClaimLawsuitStatusID IN (25)) missingdoc
			UNPIVOT (Satisfied FOR MissingDocumentation IN (ClaimantIDed
			  ,DxMedicals
			  ,DODCert
			  ,EstatePapers
			  ,ProductID
			  ,EmployExpoHist
			  ,CompServ
			  ,SettlementConfirmation
			  ,MMSEA
			  ,PreTrialReport
			  ,ExecutedRelease
			  ,AllstateMMSEAForm)) AS unpiv
	  WHERE Satisfied = 0) main
	INNER JOIN tblClaimSettlementDocumentCategory dc ON main.DocCategoryID = dc.DocCategoryID

GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimDocumentation] TO [power_user]
GO
