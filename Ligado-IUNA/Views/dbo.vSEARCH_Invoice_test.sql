SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
select * from [dbo].[vSEARCH_Invoice_test]
*/

CREATE VIEW [dbo].[vSEARCH_Invoice_test]
AS
with Document as  (
			SELECT Document.*, 
				   DocumentType.ClaimantDocumentType 
			FROM   DBO.tblDocuments AS Document 
				   INNER JOIN DBO.tblClaimantdocumentType AS DocumentType
						   ON Document.ClaimantDocumentTypeID = DocumentType.ClaimantDocumentTypeID 
				   INNER JOIN DBO.tblClaimantDocumentTypeGroup AS DocumentTypeGroup 
						   ON DocumentType.ClaimantDocumentTypeID = DocumentTypeGroup.ClaimantDocumentTypeID 
			WHERE  DocumentTypeGroup.ClaimantDocumentGroupID = 2
			)
, InvoiceDetail as (		   
		   SELECT DefenseInvoiceId, 
				  SUM(COALESCE(Fees, 0) 
				  + COALESCE(Costs, 0) 
				  - COALESCE(Adjustments, 0))		AS InvoiceTotal, 
				  SUM(COALESCE(Adjustments, 0))     AS Adjustment, 
				  SUM(COALESCE(fees, 0))            AS FeeAmount, 
				  SUM(COALESCE(costs, 0))           AS DisbursementAmount, 
				  ''                                AS AccountsReceiveablePaymentTotal, 
				  MAX(MatterId)                     AS MATTERID 
		   FROM   DBO.tblDefInvClaimantDetail 
		   GROUP  BY DefenseInvoiceID ) 
SELECT Document.DocumentID
       ,ClaimantDocumentType = COALESCE(ClaimantDocumentType, 'LOCAL COUNSEL INVOICE')
       ,Document.ClaimantDocumentTypeId 
       ,Document.Comments
       ,DocumentTitle = COALESCE(Document.DocumentTitle, DefenseInvoice.InvoiceNumber)
       ,Document.CreatedBy
       ,Document.DateCreated
       ,Document.ModifiedBy 
       ,Document.DateModified
       ,Document.Accepted
       ,Document.AcceptedBy
       ,Document.DateAccepted
       ,DefenseInvoice.ApprovedBy
       ,DefenseInvoice.DateApproved
	   -- Trello card: http://bit.ly/1z6unWS
       ,Firm = UPPER(COALESCE(DefenseCounselAddress.DefenseCounsel,DefenseCounsel.DefenseCounsel, '')) 
       ,DefenseInvoice.InvoiceNumber 
       ,DefenseInvoice.Period
       ,DefenseInvoice.DatePayable
       ,FeeAmount
       ,DisbursementAmount
       ,Adjustment
       ,DefenseInvoice.InvoiceNotes AS AdjustmentNotes
       ,DefenseInvoiceStatus.Status
       ,DefenseInvoiceFirm = '' 
       ,PaidDate = PaymentCheck.SourceDate
       ,CheckNumber = PaymentCheck.SourceNumber 
       ,FirmID = DefenseInvoice.FirmID
       ,VendorFirm = CASE 
			 WHEN Document.ClaimantDocumentTypeID = 41 THEN 
			 COALESCE(VendorFirm.FirmDisplayName, '') 
			 ELSE ''
		   END
       ,DefenseInvoice.VendorFirmID
       ,PaymentCheck.SourceDate
       ,InvoiceTotal
       ,AccountsReceiveablePaymentTotal
       ,MatterID = COALESCE(Document.MatterID, 0)  
       ,DefenseInvoice.DefenseInvoiceID
       ,DefenseInvoiceStatus.StatusID
       ,FileSizeBytes
       ,Matter =  UPPER(COALESCE(Matter.Matter,Matter2.Matter, 'Unknown')) 
       ,DefenseJurisdiction.DefenseCounselAddressID
       ,DefenseJurisdiction.PersonID
FROM   DBO.tblDefenseInvoice AS DefenseInvoice 
	   INNER JOIN  Document			
                    ON DefenseInvoice.DocumentID = Document.DocumentId
	   INNER JOIN DBO.tblDefenseInvoiceStatus AS DefenseInvoiceStatus
					ON DefenseInvoice.StatusID = DefenseInvoiceStatus.StatusID 
	   INNER JOIN  InvoiceDetail		   
					ON DefenseInvoice.DefenseInvoiceID = InvoiceDetail.DefenseInvoiceID  
       LEFT OUTER JOIN DBO.tblDefenseCounselAddress AS DefenseCounselAddress 
                    ON DefenseInvoice.DefenseCounselAddressID = DefenseCounselAddress.DefenseCounselAddressID 
       LEFT OUTER JOIN DBO.tblDefenseCounsel AS DefenseCounsel
                    ON DefenseCounsel.DefenseCounselID = DefenseInvoice.FirmID 
       LEFT OUTER JOIN DBO.vVendorFirm AS VendorFirm 
                    ON DefenseInvoice.VendorFirmID = VendorFirm.FirmID 
       LEFT OUTER JOIN DBO.tblPaymentDefense AS PaymentDefense 
                    ON DefenseInvoice.DefenseInvoiceID = PaymentDefense.DefenseInvoiceID 
       LEFT OUTER JOIN DBO.tblCheck AS PaymentCheck 
                    ON PaymentDefense.CheckID = PaymentCheck.CheckID 
       LEFT JOIN DBO.tblMatter AS Matter 
              ON Document.MatterID = Matter.MatterID 
       LEFT JOIN DBO.tblMatter AS Matter2
              ON Matter2.MatterID = InvoiceDetail.MatterID 
       LEFT JOIN DBO.tblDefenseJurisdiction DefenseJurisdiction 
              ON DefenseInvoice.DefenseCounselAddressID = DefenseJurisdiction.DefenseCounselAddressID
-- WHERE DefenseInvoice.DefenseInvoiceID = 42273
GROUP BY	Document.DocumentID, 
		    COALESCE(ClaimantDocumentType, 
			     'LOCAL COUNSEL INVOICE'), 
		    Document.ClaimantDocumentTypeId, 
		    Document.Comments, 
		    COALESCE(Document.DocumentTitle,
				 DefenseInvoice.InvoiceNumber), 
		    Document.CreatedBy, 
		    Document.DateCreated, 
		    Document.ModifiedBy, 
		    Document.DateModified, 
		    Document.Accepted, 
		    Document.AcceptedBy, 
		    Document.DateAccepted, 
		    DefenseInvoice.ApprovedBy, 
		    DefenseInvoice.DateApproved, 
		    UPPER(COALESCE(DefenseCounselAddress.DefenseCounsel,
				 DefenseCounsel.DefenseCounsel, '')), 
		    DefenseInvoice.InvoiceNumber, 
		    DefenseInvoice.Period, 
		    DefenseInvoice.DatePayable, 
		    FeeAmount, 
		    DisbursementAmount, 
		    Adjustment, 
		    DefenseInvoice.InvoiceNotes, 
		    DefenseInvoiceStatus.Status, 
		    PaymentCheck.SourceDate, 
		    PaymentCheck.SourceNumber, 
		    DefenseInvoice.FirmID, 
		    CASE 
		      WHEN Document.ClaimantDocumentTypeID = 41 THEN 
		      COALESCE(VendorFirm.FirmDisplayName, '') 
		      ELSE '' 
		    END, 
		    DefenseInvoice.VendorFirmID, 
		    PaymentCheck.SourceDate, 
		    InvoiceTotal, 
		    AccountsReceiveablePaymentTotal, 
		    COALESCE(Document.MatterID, 0), 
		    DefenseInvoice.DefenseInvoiceID, 
		    DefenseInvoiceStatus.StatusID, 
		    FileSizeBytes, 
		    UPPER(COALESCE(Matter.Matter, 
				 Matter2.Matter, 
				 'Unknown')), 
		    DefenseJurisdiction.DefenseCounselAddressID, 
		    DefenseJurisdiction.PersonID

GO
