SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Updated on 1/29/2015 by ET

Instead of writing sub-queries I moved them into CTEs so that the joins in the final select statement is clean and the logic is easy to follow.  
Field names also moved to the left so they line up.
*/

CREATE VIEW [dbo].[vSEARCH_Invoice]
AS
with Document as  (
			SELECT Document.*, 
				   DocumentType.ClaimantDocumentType 
			FROM   DBO.tblDocuments AS Document 
				   INNER JOIN DBO.tblClaimantdocumentType AS DocumentType
						   ON Document.ClaimantDocumentTypeID = DocumentType.ClaimantDocumentTypeID 
				   INNER JOIN DBO.tblClaimantDocumentTypeGroup AS DocumentTypeGroup 
						   ON DocumentType.ClaimantDocumentTypeID = DocumentTypeGroup.ClaimantDocumentTypeID 
			WHERE  DocumentTypeGroup.ClaimantDocumentGroupID = 2
			)
, InvoiceDetail as (		   
		   SELECT DefenseInvoiceId, 
				  SUM(COALESCE(Fees, 0) 
				  + COALESCE(Costs, 0) 
				  - COALESCE(Adjustments, 0))		AS InvoiceTotal, 
				  SUM(COALESCE(Adjustments, 0))     AS Adjustment, 
				  SUM(COALESCE(fees, 0))            AS FeeAmount, 
				  SUM(COALESCE(costs, 0))           AS DisbursementAmount, 
				  ''                                AS AccountsReceiveablePaymentTotal, 
				  MAX(MatterId)                     AS MATTERID 
		   FROM   DBO.tblDefInvClaimantDetail 
		   GROUP  BY DefenseInvoiceID ) 
SELECT Document.DocumentID
       ,ClaimantDocumentType = COALESCE(ClaimantDocumentType, 'LOCAL COUNSEL INVOICE')
       ,Document.ClaimantDocumentTypeID 
       ,Document.Comments
       ,DocumentTitle = COALESCE(Document.DocumentTitle, DefenseInvoice.InvoiceNumber)
       ,Document.CreatedBy
       ,Document.DateCreated
       ,Document.ModifiedBy 
       ,Document.DateModified
       ,Document.Accepted
       ,Document.AcceptedBy
       ,Document.DateAccepted
       ,DefenseInvoice.ApprovedBy
       ,DefenseInvoice.DateApproved
	   -- Trello card: http://bit.ly/1z6unWS
       ,Firm = UPPER(COALESCE(DefenseCounselAddress.DefenseCounsel,DefenseCounsel.DefenseCounsel, '')) 
       ,DefenseInvoice.InvoiceNumber 
       ,DefenseInvoice.Period
       ,DefenseInvoice.DatePayable
       ,FeeAmount
       ,DisbursementAmount
       ,Adjustment
       ,DefenseInvoice.InvoiceNotes AS AdjustmentNotes
       ,DefenseInvoiceStatus.Status
       ,DefenseInvoiceFirm = '' 
       ,PaidDate = PaymentCheck.SourceDate
       ,CheckNumber = PaymentCheck.SourceNumber 
       ,FirmID = DefenseInvoice.FirmID
       ,VendorFirm = CASE 
			 WHEN Document.ClaimantDocumentTypeID = 41 THEN 
			 COALESCE(VendorFirm.FirmDisplayName, '') 
			 ELSE ''
		   END
       ,DefenseInvoice.VendorFirmID
       ,PaymentCheck.SourceDate
       ,InvoiceTotal
       ,AccountsReceiveablePaymentTotal
       ,MatterID = COALESCE(Document.MatterID, 0)  
       ,DefenseInvoice.DefenseInvoiceID
       ,DefenseInvoiceStatus.StatusID
       ,FileSizeBytes
       ,Matter =  UPPER(COALESCE(Matter.Matter,Matter2.Matter, 'Unknown')) 
       ,DefenseJurisdiction.DefenseCounselAddressID
       ,DefenseJurisdiction.PersonID
FROM   DBO.tblDefenseInvoice AS DefenseInvoice 
	   INNER JOIN  Document			
                    ON DefenseInvoice.DocumentID = Document.DocumentId
	   INNER JOIN DBO.tblDefenseInvoiceStatus AS DefenseInvoiceStatus
					ON DefenseInvoice.StatusID = DefenseInvoiceStatus.StatusID 
	   INNER JOIN  InvoiceDetail		   
					ON DefenseInvoice.DefenseInvoiceID = InvoiceDetail.DefenseInvoiceID  
       LEFT OUTER JOIN DBO.tblDefenseCounselAddress AS DefenseCounselAddress 
                    ON DefenseInvoice.DefenseCounselAddressID = DefenseCounselAddress.DefenseCounselAddressID 
       LEFT OUTER JOIN DBO.tblDefenseCounsel AS DefenseCounsel
                    ON DefenseCounsel.DefenseCounselID = DefenseInvoice.FirmID 
       LEFT OUTER JOIN DBO.vVendorFirm AS VendorFirm 
                    ON DefenseInvoice.VendorFirmID = VendorFirm.FirmID 
       LEFT OUTER JOIN DBO.tblPaymentDefense AS PaymentDefense 
                    ON DefenseInvoice.DefenseInvoiceID = PaymentDefense.DefenseInvoiceID 
       LEFT OUTER JOIN DBO.tblCheck AS PaymentCheck 
                    ON PaymentDefense.CheckID = PaymentCheck.CheckID 
       LEFT JOIN DBO.tblMatter AS Matter 
              ON Document.MatterID = Matter.MatterID 
       LEFT JOIN DBO.tblMatter AS Matter2
              ON Matter2.MatterID = InvoiceDetail.MatterID 
       LEFT JOIN DBO.tblDefenseJurisdiction DefenseJurisdiction 
              ON DefenseInvoice.DefenseCounselAddressID = DefenseJurisdiction.DefenseCounselAddressID
GROUP BY	Document.DocumentID, 
		    COALESCE(ClaimantDocumentType, 
			     'LOCAL COUNSEL INVOICE'), 
		    Document.ClaimantDocumentTypeId, 
		    Document.Comments, 
		    COALESCE(Document.DocumentTitle,
				 DefenseInvoice.InvoiceNumber), 
		    Document.CreatedBy, 
		    Document.DateCreated, 
		    Document.ModifiedBy, 
		    Document.DateModified, 
		    Document.Accepted, 
		    Document.AcceptedBy, 
		    Document.DateAccepted, 
		    DefenseInvoice.ApprovedBy, 
		    DefenseInvoice.DateApproved, 
		    UPPER(COALESCE(DefenseCounselAddress.DefenseCounsel,
				 DefenseCounsel.DefenseCounsel, '')), 
		    DefenseInvoice.InvoiceNumber, 
		    DefenseInvoice.Period, 
		    DefenseInvoice.DatePayable, 
		    FeeAmount, 
		    DisbursementAmount, 
		    Adjustment, 
		    DefenseInvoice.InvoiceNotes, 
		    DefenseInvoiceStatus.Status, 
		    PaymentCheck.SourceDate, 
		    PaymentCheck.SourceNumber, 
		    DefenseInvoice.FirmID, 
		    CASE 
		      WHEN Document.ClaimantDocumentTypeID = 41 THEN 
		      COALESCE(VendorFirm.FirmDisplayName, '') 
		      ELSE '' 
		    END, 
		    DefenseInvoice.VendorFirmID, 
		    PaymentCheck.SourceDate, 
		    InvoiceTotal, 
		    AccountsReceiveablePaymentTotal, 
		    COALESCE(Document.MatterID, 0), 
		    DefenseInvoice.DefenseInvoiceID, 
		    DefenseInvoiceStatus.StatusID, 
		    FileSizeBytes, 
		    UPPER(COALESCE(Matter.Matter, 
				 Matter2.Matter, 
				 'Unknown')), 
		    DefenseJurisdiction.DefenseCounselAddressID, 
		    DefenseJurisdiction.PersonID

GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Invoice] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_Invoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Invoice] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_Invoice] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_Invoice] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_Invoice] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_Invoice] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Invoice] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_Invoice] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_Invoice] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_Invoice] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_Invoice] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 2
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dt"
            Begin Extent = 
               Top = 6
               Left = 306
               Bottom = 101
               Right = 536
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cdtg"
            Begin Extent = 
               Top = 6
               Left = 574
               Bottom = 135
               Right = 837
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cdg"
            Begin Extent = 
               Top = 6
               Left = 875
               Bottom = 102
               Right = 1112
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "di"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 289
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dis"
            Begin Extent = 
               Top = 138
               Left = 581
               Bottom = 233
               Right = 751
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dicd"
            Begin Extent = 
               Top = 318
               Left = 336
               Bottom = 447
               Right = 550
            End
            DisplayFlags = 280
            TopColumn = 0
      ', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Invoice', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'   End
         Begin Table = "mcd"
            Begin Extent = 
               Top = 234
               Left = 579
               Bottom = 363
               Right = 811
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dca"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 531
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dc"
            Begin Extent = 
               Top = 102
               Left = 306
               Bottom = 180
               Right = 475
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ex"
            Begin Extent = 
               Top = 498
               Left = 555
               Bottom = 627
               Right = 725
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pd"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 663
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 594
               Left = 281
               Bottom = 723
               Right = 497
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 30
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Invoice', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Invoice', NULL, NULL
GO
