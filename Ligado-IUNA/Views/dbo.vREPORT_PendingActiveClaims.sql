SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE VIEW [dbo].[vREPORT_PendingActiveClaims]
AS
	SELECT DISTINCT
	cl.ClaimLawsuitID,
	pca.PlaintiffCounsel, 
	s.FullNameState AS State,
	HighLevelDisease,
	m.Matter,
	c.MatterID,
	l.DefenseCounselAddressID,
	dj.DefenseCounselAddressID AS DJDefenseCounselAddressID,
	CONVERT(INT, NULL) AS ClaimLawsuitTypeID
	FROM tblClaim c
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID=cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID=l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID=clst.ClaimLawsuitID
	LEFT JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
	LEFT JOIN tblState s ON l.StateID = s.StateID
	LEFT JOIN (SELECT * FROM 
				tblClaimDisease
				WHERE COALESCE(PrimaryDisease, 0) = 1) cd ON c.ClaimID = cd.ClaimID
	LEFT JOIN tblDisease d ON cd.DiseaseID = d.DiseaseID
	LEFT JOIN tblHighLevelDisease hld ON d.HighLevelDiseaseID = hld.HighLevelDiseaseID
	LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
	LEFT JOIN		tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID --AND c.MatterID = dj.MatterID
	WHERE	ClaimLawsuitStatusID IN (22) 
			AND  IsPrimaryStatus = 1 
			AND IsPrimaryForClaim = 1







GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_PendingActiveClaims] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_PendingActiveClaims] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_PendingActiveClaims] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_PendingActiveClaims] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_PendingActiveClaims] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_PendingActiveClaims] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_PendingActiveClaims] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_PendingActiveClaims] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_PendingActiveClaims] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_PendingActiveClaims] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_PendingActiveClaims] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_PendingActiveClaims] TO [power_user]
GO
