SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE VIEW [dbo].[vSEARCH_ClaimsReport] AS 

SELECT 
	ClaimLawsuitID AS ClaimLawsuitID
       , ClaimantPersonalInfoID AS ClaimantPersonalInfoID
       , ActiveTrialDate AS ActiveTrialDate
       , ClaimID AS ClaimID
       , ClaimLawsuitStatus AS ClaimLawsuitStatus
       , Matter AS Matter
       , ClaimSequence AS ClaimSequence
       --, firstname
       --, lastname
       , lastname + ', ' + firstname                       AS FullName
       , BirthDate AS BirthDate
       , DeceasedDate AS DeceasedDate
       , ( CASE
             WHEN ssn4digit IS NOT NULL THEN 'XXX-XX-' + ssn4digit
             ELSE NULL
           END )                                           SocialSecurityNumber
       , Occupation AS Occupation
       , HighLevelDisease AS HighLevelDisease
       , DocketNumber AS DocketNumber
       , ServiceDate AS ServiceDate
       , COALESCE(filedate, receiveddate)                  AS FileDate
       , [State] AS State
       , Jurisdiction AS Jurisdiction
       , ProcessedDate AS ProcessedDate
       , StatusDate AS StatusDate
       , ProductNotes AS ProductNotes
       , StartDate                                         AS ProductExposureStart
       , SettlementDate AS SettlementDate
	   , SETTLEMENTPAIDDATE AS SettlementPaidDate -- SettlementPaidDate is used by PPG
       , SettlementAmount AS SettlementAmount
       , SourceDate                                        --AS SettlementPaidDate -- Date paid; source date is the source date of the first check that is cut for a settlement
       , PlaintiffCounsel AS PlaintiffCounsel
       , ClaimLawsuitStatusID AS ClaimLawsuitStatusID
       , StateID AS StateID
       , JurisdictionID AS JurisdictionID
       , IsMDL AS IsMDL
       , HighLevelDiseaseID AS HighLevelDiseaseID
       , AllegationTypeID AS AllegationTypeID
       , PlaintiffCounselID AS PlaintiffCounselID
       , MatterID AS MatterID
       , lawsuitdefensecounseladdressid                    AS DefenseCounselAddressID
       , defcounseljurisdictioninfodefensecounseladdressid AS DefenseCounselAddressID2
	   , 0 AS ClaimLawsuitStatusGroupID --NOTE: this is a dummy field because Marley doesn't require it but we still need the field. SK, 1/27/2014 (launch day)
	   , CONVERT(INT, NULL) AS PersonID, CONVERT(INT, NULL) AS BrandID, CONVERT(NVARCHAR(50), NULL) AS Brand, CONVERT(INT, NULL) AS BankruptcyStayID, CONVERT(NVARCHAR(50), NULL) AS BankruptcyStay
	   , CONVERT(INT, NULL) AS ClaimLawsuitTypeID, CONVERT(NVARCHAR(50), NULL) AS ClaimLawsuitType, CONVERT(NVARCHAR(100), NULL) AS JobsiteCity, CONVERT(INT, NULL) AS JobsiteStateID, CONVERT(NVARCHAR(1000), NULL) AS Jobsite, CONVERT(INT, NULL) AS BusinessCaseID, CONVERT(NVARCHAR(50), NULL) AS BusinessCase
	   , NationalPlaintiffCounselAddressID
	   , NationalPlaintiffCounselID
	   , NationalPlaintiffCounsel
	   , ClaimLawsuitNotes
	   , TrialNotes -- FYI TrialNotes is literally the same as ClaimLawsuitNotes; it is here so that GSX can refer to it this way in their CSV exports.
FROM   (SELECT claimlawsuitid
               , activetrialdate
               , claimid
               , lawsuitid
               , ismdl
               , claimlawsuitstatus
               , claimlawsuitstatusid
               , matter
               , claimsequence
               , claimantpersonalinfoid
               , firstname
               , lastname
               , birthdate
               , deceaseddate
               , ssn4digit
               , occupation
               , highleveldisease
               , docketnumber
               , servicedate
               , filedate
               , receiveddate
               , [state]
               , jurisdiction
               , processeddate
               , statusdate
               , productnotes
               , startdate
               , settlementdate
			   , SETTLEMENTPAIDDATE
               , settlementamount
               , sourcedate
               , plaintiffcounsel
               , stateid
               , jurisdictionid
               , highleveldiseaseid
               , plaintiffcounselid
               , lawsuitdefensecounsel
               , lawsuitdefensecounselid
               , lawsuitdefensecounseladdressid
               , matterid
               , allegationtypeid
			   , NationalPlaintiffCounselAddressID
			   , NationalPlaintiffCounselID
			   , NationalPlaintiffCounsel
			   , ClaimLawsuitNotes
			   , TrialNotes
        FROM   (SELECT claimlawsuitid
                       , activetrialdate
                       , claimid
                       , lawsuitid
                       , ismdl
                       , claimlawsuitstatus
                       , claimlawsuitstatusid
                       , docketnumber
                       , servicedate
                       , filedate
                       , receiveddate
                       , [state]
                       , stateid
                       , jurisdiction
                       , jurisdictionid
                       , processeddate
                       , statusdate
                       , settlementdate
					   , SETTLEMENTPAIDDATE
                       , settlementamount
                       , sourcedate
                       , plaintiffcounsel
                       , plaintiffcounselid
                       , lawsuitdefensecounsel
                       , lawsuitdefensecounselid
                       , lawsuitdefensecounseladdressid
                       , allegationtypeid
					   , NationalPlaintiffCounselAddressID
					   , NationalPlaintiffCounselID
					   , NationalPlaintiffCounsel
					   , ClaimLawsuitNotes
					   , TrialNotes
                FROM   (SELECT claimlawsuitid
                               , activetrialdate
                               , claimid
                               , lawsuitid
                               , ismdl
                               , settlementdate
							   , SETTLEMENTPAIDDATE
                               , settlementamount
                               , sourcedate
							   , ClaimLawsuitNotes
							   , ClaimLawsuitNotes AS TrialNotes
                        FROM   tblclaimlawsuit
                               LEFT OUTER JOIN (SELECT CLAIMLAWSUITID      AS settlmentInfoClaimLawsuitid
                                                       , SETTLEMENTDATE
													   , SETTLEMENTPAIDDATE
                                                       , SETTLEMENTAMOUNT
                                                       , MATCHEDSOURCEDATE AS sourcedate
                                                FROM   (SELECT CLAIMSETTLEMENTID
                                                               , EARLIESTCHECKID
                                                               , CLAIMLAWSUITID
                                                               , SETTLEMENTDATE
                                                               , SETTLEMENTAMOUNT
															   , PAIDDATE as SETTLEMENTPAIDDATE
                                                        FROM   TBLCLAIMSETTLEMENT
                                                               LEFT OUTER JOIN (SELECT CLAIMSETTLEMENTID AS SettlementPaymentInfoClaimSettlementID
                                                                                       , MIN(CHECKID)    AS EarliestCheckID
                                                                                FROM   TBLPAYMENTSETTLEMENT
                                                                                       LEFT OUTER JOIN (SELECT CHECKID AS checkInfoCheckID
                                                                                                        FROM   TBLCHECK) checkInfo
                                                                                                    ON ( TBLPAYMENTSETTLEMENT.CHECKID = checkInfo.CHECKINFOCHECKID )
                                                                                GROUP  BY CLAIMSETTLEMENTID) settlementPaymentInfo
                                                                            ON ( TBLCLAIMSETTLEMENT.CLAIMSETTLEMENTID = settlementPaymentInfo.SETTLEMENTPAYMENTINFOCLAIMSETTLEMENTID ))settlementAndCheckInfo
                                                  LEFT OUTER JOIN (SELECT CHECKID      AS matchedCheckID
                                                                          , SOURCEDATE AS MatchedSourceDate
                                                                   FROM   TBLCHECK)additionalCheckInfo
                                                               ON ( settlementAndCheckInfo.EARLIESTCHECKID = additionalCheckInfo.MATCHEDCHECKID ))settlementInfo
                                            ON ( tblclaimlawsuit.claimlawsuitid = settlementinfo.settlmentinfoclaimlawsuitid )
                        WHERE  isprimaryforclaim = 1) CLInfo
                       LEFT OUTER JOIN(SELECT claimlawsuitid AS allegationTypeClaimLawsuitID
                                              , allegationtypeid
                                       FROM   tblallegation) AllegationTypeInfo
                                    ON ( CLInfo.claimlawsuitid = AllegationTypeInfo.allegationtypeclaimlawsuitid )
                       LEFT OUTER JOIN (SELECT trackingInfo.claimlawsuitstatusid
                                               , trackingInfo.claimlawsuitid AS ClaimLawsuitID_fromTrackingInfo
                                               , statusLabelnfo.claimlawsuitstatus
                                               , processeddate
                                               , statusdate
                                        FROM   (SELECT claimlawsuitstatusid
                                                       , claimlawsuitid
                                                       , processeddate
                                                       , statusdate
                                                FROM   tblclaimlawsuitstatustracking
                                                WHERE  isprimarystatus = 1) trackingInfo
                                               LEFT OUTER JOIN (SELECT claimlawsuitstatusid
                                                                       , claimlawsuitstatus
                                                                FROM   tblclaimlawsuitstatus)statusLabelnfo
                                                            ON ( trackingInfo.claimlawsuitstatusid = statusLabelnfo.claimlawsuitstatusid )) lawsuitstatusinfo
                                    ON ( CLInfo.claimlawsuitid = lawsuitstatusinfo.claimlawsuitid_fromtrackinginfo )
                       LEFT OUTER JOIN (SELECT lawsuitid                 AS lawsuitinfolawsuitid
                                               , docketnumber
                                               , servicedate
                                               , filedate
                                               , receiveddate
                                               , [state]
                                               , stateid
                                               , jurisdiction
                                               , jurisdictionid
                                               , plaintiffcounsel
                                               , plaintiffcounselid
                                               , defensecounsel          AS LawsuitDefenseCounsel
                                               , defensecounseladdressid AS LawsuitDefenseCounselAddressID
                                               , defensecounselid        AS LawsuitDefenseCounselID,
											   NationalPlaintiffCounselAddressID,
											   nationalplaintiffCounselInfo.NationalPlaintiffCounselID,
											   NationalPlaintiffCounsel
                                        FROM   tbllawsuit
                                               LEFT OUTER JOIN (SELECT stateid AS stateInfoStateID
                                                                       , [state]
                                                                FROM   tblstate)stateInfo
                                                            ON( tbllawsuit.stateid = stateInfo.stateinfostateid )
                                               LEFT OUTER JOIN (SELECT jurisdictionid AS jurisdictioninfoInfojurisdictionID
                                                                       , jurisdiction
                                                                FROM   tbljurisdiction)jurisdictionInfo
                                                            ON( tbllawsuit.jurisdictionid = jurisdictionInfo.jurisdictioninfoinfojurisdictionid )
                                               LEFT OUTER JOIN (SELECT plaintiffcounseladdressid AS plaintiffCounselInfoAddressId
                                                                       , plaintiffcounsel
                                                                       , plaintiffcounselid
                                                                FROM   tblplaintiffcounseladdress) plaintiffCounselInfo
                                                            ON ( tbllawsuit.plaintiffcounseladdressid = plaintiffCounselInfo.plaintiffcounselinfoaddressid )
												LEFT OUTER JOIN (SELECT     PlaintiffCounselAddressID AS NationalPlaintiffCounselAddressId, PlaintiffCounsel AS NationalPlaintiffCounsel, PlaintiffCounselID AS NationalPlaintiffCounselID
                                                                        FROM          dbo.tblPlaintiffCounselAddress) AS nationalplaintiffCounselInfo ON 
                                                                dbo.tblLawsuit.NationalPlaintiffCounselID = nationalplaintiffCounselInfo.NationalPlaintiffCounselAddressId 
                                               LEFT OUTER JOIN (SELECT defensecounseladdressid AS defenseCounselInfoAddressID
                                                                       , defensecounsel
                                                                       , defensecounselid
                                                                FROM   tbldefensecounseladdress)defensecounselinfo
                                                            ON ( tbllawsuit.defensecounseladdressid = defensecounselinfo.defensecounselinfoaddressid ))lawsuitInfo
                                    ON ( CLInfo.lawsuitid = lawsuitInfo.lawsuitinfolawsuitid ))LawsuitLevelInfo
               LEFT OUTER JOIN (SELECT claimid_fromtblclaim     AS claimID_fromtblClaim
                                       , claimantpersonalinfoid AS claimantpersonalinfoid
                                       , matter
                                       , matterid
                                       , claimsequence
                                       , highleveldisease
                                       , highleveldiseaseid
                                FROM   (SELECT claimid AS claimID_fromtblClaim
                                               , claimantpersonalinfoid
                                               , matterid
                                               , claimsequenceid
                                        FROM   tblclaim)ClaimInfo
                                       LEFT OUTER JOIN (SELECT matterid AS matterID_fromtblMatter
                                                               , matter
                                                        FROM   tblmatter) matterInfo
                                                    ON ( ClaimInfo.matterid = matterInfo.matterid_fromtblmatter )
                                       LEFT OUTER JOIN (SELECT claimsequenceid
                                                               , claimsequence
                                                        FROM   tblclaimsequence) ClaimSequenceInfo
                                                    ON ( ClaimInfo.claimsequenceid = ClaimSequenceInfo.claimsequenceid )
                                       LEFT OUTER JOIN (SELECT claimid
                                                               , highleveldisease
                                                               , highleveldiseaseid
                                                        FROM   (SELECT claimid
                                                                       , diseaseid
                                                                       , primarydisease
                                                                FROM   tblclaimdisease) theClaimDiseaseInfo
                                                               LEFT OUTER JOIN (SELECT diseaseid AS diseaseInfodiseaseid
                                                                                       , highleveldiseaseid
                                                                                FROM   tbldisease) diseaseInfo
                                                                            ON( theClaimDiseaseInfo.diseaseid = diseaseInfo.diseaseinfodiseaseid )
                                                               LEFT OUTER JOIN (SELECT highleveldiseaseid AS hldiseaseinfohldiseaseid
                                                                                       , highleveldisease
                                                                                FROM   tblhighleveldisease) hlDiseaseInfo
                                                                            ON( diseaseInfo.highleveldiseaseid = hlDiseaseInfo.hldiseaseinfohldiseaseid )
                                                        WHERE  theClaimDiseaseInfo.primarydisease = 1) ClaimDiseaseInfo
                                                    ON( ClaimInfo.claimid_fromtblclaim = ClaimDiseaseInfo.claimid ))ClaimLevelInfo
                            ON ( LawsuitLevelInfo.claimid = ClaimLevelInfo.claimid_fromtblclaim )
               LEFT OUTER JOIN(SELECT claimid                            AS claimID_forclaimantinfo
                                      , ClaimInfo.claimantpersonalinfoid AS personalinfoid
                                      , firstname
                                      , lastname
                                      , birthdate
                                      , deceaseddate
                                      , ssn4digit
                                      , occupation
                                      , productnotes
                                      , startdate
                               FROM   (SELECT claimid
                                              , claimantpersonalinfoid
                                       FROM   tblclaim) ClaimInfo
                                      LEFT OUTER JOIN (SELECT claimantpersonalinfoid
                                                              , firstname
                                                              , lastname
                                                              , birthdate
                                                              , deceaseddate
                                                              , RIGHT(socialsecuritynumber, 4) AS SSN4Digit
                                                       FROM   tblclaimantpersonalinfo) ClaimantInfo
                                                   ON ( ClaimInfo.claimantpersonalinfoid = ClaimantInfo.claimantpersonalinfoid )
                                      LEFT OUTER JOIN (SELECT employmentpersonalinfoid
                                                              , occupation
                                                              , productnotes
                                                              , startdate
                                                       FROM   (SELECT claimantpersonalinfoid AS employmentPersonalInfoId
                                                                      , occupationid
                                                                      , claimantemploymentid
                                                               FROM   tblclaimantemployment
                                                               WHERE  isprimary = 1) empInfo
                                                              LEFT OUTER JOIN (SELECT occupationid
                                                                                      , occupation
                                                                               FROM   tbloccupation) occInfo
                                                                           ON ( empInfo.occupationid = occInfo.occupationid )
                                                              LEFT OUTER JOIN (SELECT claimantemploymentid AS prodInfoClaimentEmploymentId
                                                                                      , productid
                                                                                      , productnotes
                                                                                      , startdate
                                                                               FROM   tblclaimantproduct) prodInfo
                                                                           ON ( empInfo.claimantemploymentid = prodInfo.prodinfoclaimentemploymentid )) claimantEmployment
                                                   ON ( ClaimantInfo.claimantpersonalinfoid = claimantEmployment.employmentpersonalinfoid )) ClaimantLevelInfo
                            ON( LawsuitLevelInfo.claimid = ClaimantLevelInfo.claimid_forclaimantinfo ))MainQuery
       LEFT OUTER JOIN (SELECT defensecounseladdressid AS defCounselJurisdictionInfoDefenseCounselAddressID
                               , stateid               AS defCounselJurisdictionInfoStateID
                               , jurisdictionid        AS defCounselJurisdictionInfoJurisdictionId
                        FROM   tbldefensejurisdiction)defCounselJurisdictionInfo
                    ON ( MainQuery.stateid = defCounselJurisdictionInfo.defcounseljurisdictioninfostateid
                         AND MainQuery.jurisdictionid = defCounselJurisdictionInfo.defcounseljurisdictioninfojurisdictionid )
GROUP  BY claimlawsuitid
          , claimantpersonalinfoid
          , claimid
          , ismdl
          , activetrialdate
          , claimlawsuitstatus
          , matter
          , claimsequence
          , firstname
          , lastname
          , birthdate
          , deceaseddate
          , ssn4digit
          , occupation
          , highleveldisease
          , activetrialdate
          , docketnumber
          , servicedate
          , filedate
          , receiveddate
          , [state]
          , jurisdiction
          , processeddate
          , statusdate
          , productnotes
          , startdate
          , settlementdate
		  , SETTLEMENTPAIDDATE
          , settlementamount
          , sourcedate
          , plaintiffcounsel
          , claimlawsuitstatusid
          , stateid
          , jurisdictionid
          , highleveldiseaseid
          , allegationtypeid
          , plaintiffcounselid
          , matterid
          , lawsuitdefensecounsel
          , lawsuitdefensecounselid
          , lawsuitdefensecounseladdressid
          , defcounseljurisdictioninfodefensecounseladdressid 
		  , NationalPlaintiffCounselAddressID
		  , NationalPlaintiffCounselID
		  , NationalPlaintiffCounsel
		  , ClaimLawsuitNotes
		  , TrialNotes






GO
