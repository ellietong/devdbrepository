
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [dbo].[tblDefenseInvoiceStatus] as
select * from [KGA-ClaimsAdmin].dbo.tblDefenseInvoiceStatus
where statusID in (1,2,3,4, 6)

GO

GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoiceStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoiceStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoiceStatus] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDefenseInvoiceStatus] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblDefenseInvoiceStatus] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblDefenseInvoiceStatus] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblDefenseInvoiceStatus] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefenseInvoiceStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefenseInvoiceStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseInvoiceStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefenseInvoiceStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseInvoiceStatus] TO [power_user]
GO
