SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vCRITERIA_DefenseInvoiceType] AS

/** 

NOTE: This was creteated because vDefenseInvoiceType is used from an upload perspective
This view was created for searches, which are less filtered than vDefenseInvoiceType
--Sean Killeen

*/

select 
ClaimantDocumentTypeID
, ClaimantDocumentType 
from tblClaimantDocumentType 
where claimantdocumentgroupid = 2 -- 2 corresponds to 'INVOICE'
and ClaimantDocumentTypeID = 40 -- CW: Removing everything BUT INVOICE (40) as IUNA does not use any other type 


GO
GRANT VIEW DEFINITION ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [base_user]
GRANT SELECT ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [power_user]
GRANT SELECT ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [power_user]
GRANT INSERT ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [power_user]
GRANT DELETE ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [power_user]
GRANT UPDATE ON  [dbo].[vCRITERIA_DefenseInvoiceType] TO [power_user]
GO
