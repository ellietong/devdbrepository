SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vREPORT_EventTracking]
AS

SELECT cpi.FullName
       , cpi.ClaimantPersonalInfoID
       , CASE
           WHEN SocialSecurityNumber IS NULL THEN ''
           ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4)
         END                          AS SocialSecurityNumber
       , cpi.BirthDate
       , cpi.DeceasedDate
       , tpca.PlaintiffCounselAddressID AS NationalPlaintiffCounselAddressID
       , tpca.PlaintiffCounsel        AS NationalPlaintiffCounsel
       , clt.ClaimLawsuitTypeID
       , clt.ClaimLawsuitType
       , cl.ClaimLawsuitID
       , cl.DefenseCounselAddressID
       , tdca.DefenseCounsel
       , evtype.EventType
       , et.EventTypeID
       , EventDate
       , EventDescription
       , hld.HighLevelDiseaseID
       , hld.HighLevelDisease
       , tr.TrialRiskID
       , tr.TrialRisk
       , MaxPrimaryEmploymentRecords.Jobsite
	   , cl.DemandAmount
	   , tclsai.AuthorityAmountRequested
	   , tclsai.AuthorityGrantedStatusID
	   , ags.AuthorityGrantedStatus
	   , l.JurisdictionID
	   , j.Jurisdiction + ', ' + lawsuitState.State as Jurisdiction
FROM   TBLCLAIMANTPERSONALINFO cpi
       INNER JOIN TBLCLAIM c
               ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
       INNER JOIN TBLCLAIMLAWSUIT cl
               ON c.ClaimID = cl.ClaimID
       INNER JOIN TBLCLAIMLAWSUITSTATUSTRACKING clst
               ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
       INNER JOIN TBLLAWSUIT l
               ON cl.lawsuitid = l.lawsuitid
       INNER JOIN TBLEVENTTRACKING et
               ON et.ClaimLawsuitID = cl.ClaimLawsuitID
       INNER JOIN TBLEVENTTYPE evtype
               ON et.EventTypeID = evtype.EventTypeID
       INNER JOIN TBLPLAINTIFFCOUNSELADDRESS tpca
               ON l.NationalPlaintiffCounselID = tpca.PlaintiffCounselAddressID
       INNER JOIN TBLDEFENSECOUNSELADDRESS tdca
               ON cl.DefenseCounselAddressID = tdca.DefenseCounselAddressID
       INNER JOIN TBLCLAIMLAWSUITTYPE clt
               ON cl.ClaimLawsuitTypeID = clt.ClaimLawsuitTypeID
       INNER JOIN TBLCLAIMDISEASE tcd
               ON c.ClaimID = tcd.ClaimID
       INNER JOIN TBLDISEASE td
               ON tcd.DiseaseID = td.DiseaseID
       INNER JOIN TBLHIGHLEVELDISEASE hld
               ON td.HighLevelDiseaseID = hld.HighLevelDiseaseID
       LEFT OUTER JOIN TBLTRIALRISK tr
                    ON cl.TrialRiskID = tr.TrialRiskID
		LEFT OUTER JOIN tblClaimLawsuitSettlementAuthorityInfo  tclsai on cl.ClaimLawsuitID = tclsai.ClaimLawsuitID
		LEFT OUTER JOIN tblAuthorityGrantedStatus ags on tclsai.AuthorityGrantedStatusID = ags.AuthorityGrantedStatusID
		LEFT OUTER JOIN tblJurisdiction j on l.JurisdictionID = j.JurisdictionID
       LEFT OUTER JOIN (SELECT MaxIds.claimlawsuitid
                          , ClaimLawsuitEmploymentID
                          , JobSiteCity + ', ' + State as JobSite
                   FROM   (SELECT claimlawsuitID
                                  , MAX(claimlawsuitemploymentid) AS MaxClaimLawsuitEmploymentID
                           FROM   TBLCLAIMLAWSUITEMPLOYMENT
                           GROUP  BY ClaimLawsuitID)MaxIDs
                          INNER JOIN (SELECT claimlawsuitID
                                             , claimlawsuitemploymentID
                                             , JobsiteCity
											 , State
                                      FROM   TBLCLAIMLAWSUITEMPLOYMENT
									  LEFT OUTER JOIN tblState on tblclaimlawsuitemployment.jobsitestateid = tblstate.stateid
                                      )AdditionalInfo
                                  ON MaxIds.ClaimLawsuitID = AdditionalInfo.ClaimLawsuitID
                                     AND MaxIds.MaxClaimLawsuitEmploymentID = AdditionalInfo.claimlawsuitemploymentID
									 ) MaxPrimaryEmploymentRecords
               ON cl.ClaimLawsuitID = MaxPrimaryEmploymentRecords.ClaimLawsuitID
		LEFT OUTER JOIN (select stateid, state from tblState) lawsuitState
		on l.stateid = lawsuitState.StateID
WHERE  ClaimLawsuitStatusID IN ( 41, 42 )
   AND clst.IsPrimaryStatus = 1 

GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_EventTracking] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_EventTracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_EventTracking] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_EventTracking] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_EventTracking] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_EventTracking] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_EventTracking] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_EventTracking] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_EventTracking] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_EventTracking] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_EventTracking] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_EventTracking] TO [power_user]
GO
