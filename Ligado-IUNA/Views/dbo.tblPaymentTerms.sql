SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[tblPaymentTerms]
AS
SELECT     pt.PaymentTermsID, pt.PaymentTerms
FROM         [KGA-ClaimsAdmin].dbo.tblPaymentTerms AS pt INNER JOIN
                      [KGA-ClaimsAdmin].dbo.tblPaymentTermsClient AS ptc ON pt.PaymentTermsID = ptc.PaymentTermsID
WHERE     (ptc.CaseID = dbo.fn_GetCaseID())

GO
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentTerms] TO [base_user]
GRANT SELECT ON  [dbo].[tblPaymentTerms] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentTerms] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPaymentTerms] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPaymentTerms] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPaymentTerms] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPaymentTerms] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPaymentTerms] TO [power_user]
GRANT SELECT ON  [dbo].[tblPaymentTerms] TO [power_user]
GRANT INSERT ON  [dbo].[tblPaymentTerms] TO [power_user]
GRANT DELETE ON  [dbo].[tblPaymentTerms] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPaymentTerms] TO [power_user]
GO
