SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vDETAILS_ClaimantEmployment]
AS
SELECT     ce.ClaimantEmploymentID ClaimantEmploymentID, 
		   ce.ClaimantPersonalInfoID AS ClaimantPersonalInfoID, 
		   CONVERT(bigint, 0) AS ClaimLawsuitEmploymentID, 
		   CONVERT(bigint, 0) AS ClaimLawsuitID, 
		   ce.OccupationID, 
           ce.Jobsite, 
		   ce.Comments, 
		   CONVERT(nvarchar(50), NULL) AS NavEmpStartDate, 
		   CONVERT(nvarchar(50), NULL) AS NavEmpEndDate, 
		   CONVERT(nvarchar(50), NULL) AS NavEarliestExposureDate, 
		   CONVERT(nvarchar(50), NULL) AS NavLatestExposureDate, 
           ce.IsPrimary, 
		   ce.IsSecondaryExposure, 
		   CONVERT(nvarchar(250), NULL) AS NavInjPartyKey, 
		   CONVERT(nvarchar(250), NULL) AS NavOccupation, 
		   ce.EmpStartDate, 
		   ce.EmpEndDate, 
		   ce.EarliestExposureDate, 
           ce.LatestExposureDate, 
		   ce.SPUsername, 
		   ce.JobsiteCity, 
		   ce.JobsiteStateID, 
		   CONVERT(int, 0) AS Old_OccupationID, 
		   o.Occupation, 
		   s.State, 
		   s.State AS JobsiteState, 
		   ce.JobsiteStateID AS StateID,
		   CONVERT(nvarchar(250), NULL) AS JobsiteOwner, 
		   CONVERT(nvarchar(250), NULL) AS Employer, 
		   CONVERT(int, NULL) AS BusinessCaseID, 
		   CONVERT(nvarchar(50), NULL) AS BusinessCase,
		   CONVERT(int, NULL) AS BillingCodeID,
		   CONVERT(nvarchar(50), NULL) AS BillingCode
FROM         dbo.tblClaimantEmployment AS ce LEFT JOIN
                      dbo.tblOccupation AS o ON ce.OccupationID = o.OccupationID LEFT JOIN
                      dbo.tblState AS s ON ce.JobsiteStateID = s.StateID



GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantEmployment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantEmployment] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantEmployment] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantEmployment] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantEmployment] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantEmployment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantEmployment] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantEmployment] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantEmployment] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantEmployment] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantEmployment] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ce"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 256
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "o"
            Begin Extent = 
               Top = 6
               Left = 294
               Bottom = 101
               Right = 464
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 502
               Bottom = 118
               Right = 672
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 25
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder =', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantEmployment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantEmployment', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_ClaimantEmployment', NULL, NULL
GO
