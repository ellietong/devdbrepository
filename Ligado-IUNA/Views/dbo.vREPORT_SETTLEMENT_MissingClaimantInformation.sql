SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation]
AS


	WITH ProductEnt (ClaimantPersonalInfoID, ProductEntered)
	AS
	(
		SELECT ce.ClaimantPersonalInfoID, CASE WHEN cp.ProductID IS NULL OR cp.StartDate IS NULL OR cp.EndDate IS NULL  THEN 0  ELSE 1  END AS productentered
		FROM tblClaimantEmployment ce 
			LEFT JOIN tblClaimantProduct cp on cp.ClaimantEmploymentID =ce.ClaimantEmploymentID 
		WHERE ce.IsPrimary = 1 --AND cp.PrimaryProduct = 1
		GROUP BY ce.ClaimantPersonalInfoID , 
			CASE WHEN cp.ProductID IS NULL OR cp.StartDate IS NULL OR cp.EndDate IS NULL  THEN 0  ELSE 1  end 
	)
	,
	CurrentClaimantInfo (ClaimantPersonalInfoID , [1001] , [1002] , [1003] , [1004] ,  [1005] , [1006] )
	AS
	(
		SELECT DISTINCT cpi.ClaimantPersonalInfoID,
						CASE WHEN SocialSecurityNumber IS NULL THEN 0 ELSE 1 END AS HasSSN, 
						CASE WHEN BirthDate IS NULL THEN 0 ELSE 1 END AS HasBirthDate,
						CASE WHEN DiseaseID IS NULL THEN 0 ELSE 1 END AS HasDiseaseID,
						CASE WHEN CONVERT(date, DiseaseDiagnosisDate) IS NULL THEN 0 ELSE 1 END AS HasDxDate, 
						COALESCE(p.ProductEntered, 0) AS HasProductEntered,  
						CASE WHEN DeceasedDate IS NOT NULL AND LegalCapacityid IS NULL THEN 0  ELSE 1 END AS HasLegalCapacity
		FROM tblClaim c
			INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
			INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
			INNER JOIN tblClaimSettlement cs ON cs.ClaimLawsuitID=cl.ClaimLawsuitID
			LEFT JOIN ProductEnt p ON p.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID 
			LEFT JOIN (SELECT cd1.* 
					   FROM tblClaimDisease cd1 
							INNER JOIN tbldisease d1 on d1.diseaseid = cd1.diseaseid 
					   WHERE PrimaryDisease = 1 
							AND highleveldiseaseid <> 5) cdi ON cdi.ClaimID = c.ClaimID
			INNER JOIN tblClaimLawsuitStatusTracking cls ON cls.ClaimLawsuitID = cl.ClaimLawsuitID 
			LEFT JOIN (SELECT * 
						FROM tblClaimantRelatedParty 
						WHERE LegalCapacityID IN (1,2,3,4,5,6,7,8,15,17,30,31))crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID

		WHERE cls.ClaimLawsuitStatusID IN (25)
			AND YEAR(cs.SettlementDate) >= 2010
	)
	, ClaimantInfoCategory (ClaimantInfoCategoryID, ClaimantInfoCategory) AS
	(SELECT ClaimantInfoCategoryID, ClaimantInfoCategory FROM tblClaimantInformationCategory)

	(SELECT ClaimantPersonalInfoID, unpiv.ClaimantInfoCategoryID, Satisfied, cic.ClaimantInfoCategory AS MissingClaimantInfo

	FROM

	(SELECT [ClaimantPersonalInfoID]
		  ,[1001]
		  ,[1002]
		  ,[1003]
		  ,[1004]
		  ,[1005]
		  ,[1006]
	  FROM CurrentClaimantInfo) mc
		UNPIVOT (Satisfied FOR ClaimantInfoCategoryID IN ([1001]
		  ,[1002]
		  ,[1003]
		  ,[1004]
		  ,[1005]
		  ,[1006])) as unpiv
		  INNER JOIN tblClaimantInformationCategory cic ON unpiv.ClaimantInfoCategoryID = cic.ClaimantInfoCategoryID
		 WHERE Satisfied = 0)
		
		
 
  
  
 





GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_SETTLEMENT_MissingClaimantInformation] TO [power_user]
GO
