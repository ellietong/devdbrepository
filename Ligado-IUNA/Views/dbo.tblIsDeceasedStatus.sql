SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblIsDeceasedStatus]
AS
SELECT     StatusID AS IsDeceasedStatusID, Status AS IsDeceasedStatus
FROM         [KGA-ClaimsAdmin].dbo.tblYesNoUnknown

GO
GRANT VIEW DEFINITION ON  [dbo].[tblIsDeceasedStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblIsDeceasedStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblIsDeceasedStatus] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblIsDeceasedStatus] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblIsDeceasedStatus] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblIsDeceasedStatus] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblIsDeceasedStatus] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblIsDeceasedStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblIsDeceasedStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblIsDeceasedStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblIsDeceasedStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblIsDeceasedStatus] TO [power_user]
GO
