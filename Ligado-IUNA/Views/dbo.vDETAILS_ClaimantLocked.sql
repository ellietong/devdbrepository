SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vDETAILS_ClaimantLocked]
AS

SELECT ClaimantPersonalInfoID, 
	SUM(Locked) AS Locked
FROM (SELECT c.ClaimantPersonalInfoID, 
		  COALESCE(COUNT(clst.ClaimLawsuitStatusTrackingID), 0) AS Locked
      FROM tblClaim c
          INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
          INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
          LEFT  JOIN (SELECT ClaimLawsuitStatusTrackingID, ClaimLawsuitID
					  FROM dbo.tblClaimLawsuitStatusTracking
					  WHERE ClaimLawsuitStatusID NOT IN (17, 19, 20, 22, 25, 29, 45)
						  AND IsPrimaryStatus = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
      WHERE COALESCE(c.IsCurrentClaim, 0) = 1
          AND COALESCE(IsPrimaryForClaim, 0) = 1
      GROUP BY c.ClaimantPersonalInfoID
      UNION
      --Check for settlements
      SELECT c.ClaimantPersonalInfoID, 
	      COALESCE(COUNT(DISTINCT PaymentSettlementID), 0)
      FROM tblClaim c 
	      LEFT JOIN tblClaimLawsuit      cl ON c.ClaimID            = cl.ClaimID
          LEFT JOIN tblClaimSettlement   cs ON cl.ClaimLawsuitID    = cs.ClaimLawsuitID
          LEFT JOIN tblPaymentSettlement ps ON cs.ClaimSettlementID = ps.ClaimSettlementID
      GROUP BY c.ClaimantPersonalInfoID) AS lock
GROUP BY ClaimantPersonalInfoID

GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantLocked] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantLocked] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantLocked] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantLocked] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantLocked] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantLocked] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantLocked] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_ClaimantLocked] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_ClaimantLocked] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_ClaimantLocked] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_ClaimantLocked] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_ClaimantLocked] TO [power_user]
GO
