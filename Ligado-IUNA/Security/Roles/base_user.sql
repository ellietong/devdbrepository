CREATE ROLE [base_user]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'base_user', N'chantosao'
GO
EXEC sp_addrolemember N'base_user', N'crookc'
GO
EXEC sp_addrolemember N'base_user', N'hagep'
GO
EXEC sp_addrolemember N'base_user', N'LigadoMonitoring'
GO
EXEC sp_addrolemember N'base_user', N'mollardj'
GO
EXEC sp_addrolemember N'base_user', N'owenss'
GO
EXEC sp_addrolemember N'base_user', N'tonge'
GO
EXEC sp_addrolemember N'base_user', N'wongc'
GO
EXEC sp_addrolemember N'base_user', N'youngc'
GO
GRANT CREATE PROCEDURE TO [base_user]
GRANT CREATE VIEW TO [base_user]
