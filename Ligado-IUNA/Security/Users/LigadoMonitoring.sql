IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'LigadoMonitoring')
CREATE LOGIN [LigadoMonitoring] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [LigadoMonitoring] FOR LOGIN [LigadoMonitoring]
GO
