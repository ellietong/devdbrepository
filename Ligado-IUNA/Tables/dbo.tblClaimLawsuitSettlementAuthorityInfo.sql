CREATE TABLE [dbo].[tblClaimLawsuitSettlementAuthorityInfo]
(
[ClaimLawsuitID] [bigint] NOT NULL,
[AuthorityAmountRequested] [money] NULL,
[SettlementLowAmount] [money] NULL,
[SettlementHighAmount] [money] NULL,
[EstimatedClientAllocation] [int] NULL,
[AuthorityGrantedStatusID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitSettlementAuthorityInfo] ADD CONSTRAINT [PK_tblClaimLawsuitSettlementAuthorityInfo] PRIMARY KEY CLUSTERED  ([ClaimLawsuitID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitSettlementAuthorityInfo] TO [power_user]
GO
