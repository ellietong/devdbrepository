CREATE TABLE [dbo].[tblClaimantDocumentTypeGroup]
(
[ClaimantDocumentTypeGroupID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantDocumentTypeID] [int] NOT NULL,
[ClaimantDocumentGroupID] [int] NOT NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantDocumentTypeGroup] ADD CONSTRAINT [PK_tblClaimantDocumentTypeGroup] PRIMARY KEY CLUSTERED  ([ClaimantDocumentTypeGroupID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantDocumentTypeGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantDocumentTypeGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantDocumentTypeGroup] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimantDocumentTypeGroup] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimantDocumentTypeGroup] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimantDocumentTypeGroup] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimantDocumentTypeGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantDocumentTypeGroup] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantDocumentTypeGroup] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantDocumentTypeGroup] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantDocumentTypeGroup] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantDocumentTypeGroup] TO [power_user]
GO
