CREATE TABLE [dbo].[tblReleaseTemplateType]
(
[ReleaseTemplateTypeID] [int] NOT NULL IDENTITY(1, 1),
[ReleaseTemplateType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblReleaseTemplateType] ADD CONSTRAINT [PK_tblTemplateType] PRIMARY KEY CLUSTERED  ([ReleaseTemplateTypeID]) ON [PRIMARY]
GO
