CREATE TABLE [dbo].[m_tblPACEDefenseVendor]
(
[VendorID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxIDNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEDefenseVendor] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPACEDefenseVendor] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEDefenseVendor] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblPACEDefenseVendor] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblPACEDefenseVendor] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblPACEDefenseVendor] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblPACEDefenseVendor] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEDefenseVendor] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblPACEDefenseVendor] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblPACEDefenseVendor] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblPACEDefenseVendor] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblPACEDefenseVendor] TO [power_user]
GO
