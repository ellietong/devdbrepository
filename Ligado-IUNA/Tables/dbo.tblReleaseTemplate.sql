CREATE TABLE [dbo].[tblReleaseTemplate]
(
[TemplateID] [int] NOT NULL IDENTITY(1, 1),
[StateID] [int] NULL,
[RlsTypeID] [int] NULL,
[Deceased] [bit] NULL,
[RlsRelationStatusID] [int] NULL,
[MatterID] [int] NULL,
[FilePath] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseTemplateTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblReleaseTemplate] ADD CONSTRAINT [PK_tblReleaseTemplate] PRIMARY KEY CLUSTERED  ([TemplateID]) ON [PRIMARY]
GO
