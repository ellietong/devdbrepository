CREATE TABLE [dbo].[tblClaimLawsuitType]
(
[ClaimLawsuitTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitType] ADD CONSTRAINT [PK_tblClaimLawsuitType] PRIMARY KEY CLUSTERED  ([ClaimLawsuitTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitType] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitType] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitType] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitType] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitType] TO [power_user]
GO
