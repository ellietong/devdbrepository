CREATE TABLE [dbo].[tblReportFooters]
(
[ReportFooterID] [int] NOT NULL IDENTITY(1, 1),
[ReportFooterType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportFooter] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblReportFooters] ADD CONSTRAINT [PK_tblReportFooters] PRIMARY KEY CLUSTERED  ([ReportFooterID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblReportFooters] TO [base_user]
GRANT SELECT ON  [dbo].[tblReportFooters] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblReportFooters] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblReportFooters] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblReportFooters] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblReportFooters] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblReportFooters] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblReportFooters] TO [power_user]
GRANT SELECT ON  [dbo].[tblReportFooters] TO [power_user]
GRANT INSERT ON  [dbo].[tblReportFooters] TO [power_user]
GRANT DELETE ON  [dbo].[tblReportFooters] TO [power_user]
GRANT UPDATE ON  [dbo].[tblReportFooters] TO [power_user]
GRANT SELECT ON  [dbo].[tblReportFooters] TO [Reports_User]
GRANT INSERT ON  [dbo].[tblReportFooters] TO [Reports_User]
GRANT DELETE ON  [dbo].[tblReportFooters] TO [Reports_User]
GRANT UPDATE ON  [dbo].[tblReportFooters] TO [Reports_User]
GO
