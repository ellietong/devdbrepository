CREATE TABLE [dbo].[m_LondonJanuaryAllocationAfterHoliday]
(
[PolicyID] [int] NULL,
[IndemnityAllocated] [money] NULL,
[ExpenseAllocated] [money] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [base_user]
GRANT SELECT ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [base_user]
GRANT INSERT ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [power_user]
GRANT SELECT ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [power_user]
GRANT INSERT ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [power_user]
GRANT DELETE ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [power_user]
GRANT UPDATE ON  [dbo].[m_LondonJanuaryAllocationAfterHoliday] TO [power_user]
GO
