CREATE TABLE [dbo].[tblBundledBilledHistory]
(
[Injured_Party_Id] [float] NULL,
[Claim Sequence_Nbr] [float] NULL,
[Latest_Bill_Date] [datetime] NULL,
[AMC_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APG_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AWI_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CTD_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DNA_Share] [float] NULL,
[FLE_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GAF_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IUN_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KEN_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MAR_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAT_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NBR_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOS_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NUT_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PFZ_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QUI_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHO_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[THU_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TNN_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UNC_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USG_Share] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Original_Bill_Date] [datetime] NULL,
[Total_Amount] [float] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_m_tblBundledBilledHistory_m_tblBundledInjuredParty] ON [dbo].[tblBundledBilledHistory] ([Injured_Party_Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBundledBilledHistory] ADD CONSTRAINT [FK_m_tblBundledBilledHistory_m_tblBundledInjuredParty] FOREIGN KEY ([Injured_Party_Id]) REFERENCES [dbo].[tblBundledInjuredParty] ([Injured_Party_Id])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBundledBilledHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblBundledBilledHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblBundledBilledHistory] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblBundledBilledHistory] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblBundledBilledHistory] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBundledBilledHistory] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBundledBilledHistory] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBundledBilledHistory] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBundledBilledHistory] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBundledBilledHistory] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBundledBilledHistory] TO [power_user]
GRANT SELECT ON  [dbo].[tblBundledBilledHistory] TO [power_user]
GRANT INSERT ON  [dbo].[tblBundledBilledHistory] TO [power_user]
GRANT DELETE ON  [dbo].[tblBundledBilledHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBundledBilledHistory] TO [power_user]
GO
