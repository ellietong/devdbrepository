CREATE TABLE [dbo].[tblBankruptcyStay]
(
[BankruptcyStayID] [int] NOT NULL IDENTITY(1, 1),
[BankruptcyStay] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBankruptcyStay] ADD CONSTRAINT [PK_tblBankruptcyStay] PRIMARY KEY CLUSTERED  ([BankruptcyStayID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBankruptcyStay] TO [base_user]
GRANT SELECT ON  [dbo].[tblBankruptcyStay] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBankruptcyStay] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBankruptcyStay] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBankruptcyStay] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBankruptcyStay] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBankruptcyStay] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBankruptcyStay] TO [power_user]
GRANT SELECT ON  [dbo].[tblBankruptcyStay] TO [power_user]
GRANT INSERT ON  [dbo].[tblBankruptcyStay] TO [power_user]
GRANT DELETE ON  [dbo].[tblBankruptcyStay] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBankruptcyStay] TO [power_user]
GO
