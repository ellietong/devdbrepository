CREATE TABLE [dbo].[tblSettleDocs_old2]
(
[SettleDocID] [bigint] NOT NULL,
[SetPymtID] [bigint] NULL,
[DocumentID] [bigint] NULL,
[DocCategoryID] [int] NULL,
[PageNums] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettleDocs_old2] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettleDocs_old2] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettleDocs_old2] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSettleDocs_old2] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSettleDocs_old2] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSettleDocs_old2] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSettleDocs_old2] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettleDocs_old2] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettleDocs_old2] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettleDocs_old2] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettleDocs_old2] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettleDocs_old2] TO [power_user]
GO
