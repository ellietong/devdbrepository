CREATE TABLE [dbo].[tblClaimSettlement]
(
[ClaimSettlementID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[NavSettlementDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementAmount] [money] NULL,
[PlaintiffSettlementAmount] [money] NULL,
[NavSettlementDueDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementNotificationDate] [datetime] NULL,
[SettlementFundsRequestDate] [datetime] NULL,
[PayToCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPaidDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementGroupID] [bigint] NULL,
[SettlementNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimResolutionKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavSettlementGroup] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavReleaseType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavResolutionType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementDate] [datetime] NULL,
[SettlementDueDate] [datetime] NULL,
[PaidDate] [datetime] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseSignedDate] [datetime] NULL,
[ReleaseSentDate] [datetime] NULL,
[ReleaseReceivedDate] [datetime] NULL,
[ReleaseTypeID] [int] NULL,
[ReleaseRqstDate] [datetime] NULL,
[ReleaseToPC] [datetime] NULL,
[SettlePaymentComments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseRcvd] [bit] NULL,
[MedicareFullyDocumented] [bit] NULL,
[MedicareReported] [bit] NULL,
[ReleaseTemplateTypeID] [int] NULL,
[MedicareAddendumSent] [datetime] NULL,
[MedicareAddendumRcvd] [datetime] NULL,
[MedicareFormSent] [datetime] NULL,
[MedicareFormRcvd] [datetime] NULL,
[ExecutedReleaseDate] [datetime] NULL,
[PaymentApprovedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentApprovedDate] [datetime] NULL,
[PaymentRecommendedBy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentRecommendedDate] [datetime] NULL,
[UpdateReleaseTemplate] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimSettlement] ADD CONSTRAINT [PK_mBtblClaimantSettlement] PRIMARY KEY CLUSTERED  ([ClaimSettlementID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_ClaimLawsuitID] ON [dbo].[tblClaimSettlement] ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimantSettlement_tblClaimLawsuit] ON [dbo].[tblClaimSettlement] ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimSettlement_SettlementGroupID] ON [dbo].[tblClaimSettlement] ([SettlementGroupID]) INCLUDE ([ClaimLawsuitID], [ClaimSettlementID], [SettlementDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimSettlement] ADD CONSTRAINT [FK_tblClaimantSettlement_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSettlement] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimSettlement] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimSettlement] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlement] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimSettlement] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimSettlement] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimSettlement] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimSettlement] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlement] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimSettlement] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimSettlement] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimSettlement] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimSettlement] TO [power_user]
GO
