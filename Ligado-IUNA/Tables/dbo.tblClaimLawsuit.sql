CREATE TABLE [dbo].[tblClaimLawsuit]
(
[ClaimLawsuitID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID] [bigint] NULL,
[LawsuitID] [bigint] NULL,
[IndividualDocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavIsPrimaryForClaim] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsMDL] [bit] NULL CONSTRAINT [DF_tblClaimLawsuit_IsMDL] DEFAULT ((0)),
[NavClaimLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavStatus] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavStatusDate] [datetime] NULL,
[IsPrimaryForClaim] [bit] NULL,
[AdminSettlement] [bit] NULL,
[ActiveTrialDate] [datetime] NULL,
[TrialGroupID] [bigint] NULL,
[ComplaintID] [bigint] NULL,
[SPUsername] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [bigint] NULL,
[ClaimLawsuitTypeID] [int] NULL,
[BankruptcyStayID] [int] NULL,
[ClaimLawsuitNotes] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DemandAmount] [money] NULL,
[DemandDate] [datetime] NULL,
[PresentedYear] [int] NULL,
[TrialRiskID] [int] NULL,
[PlaintiffNotes] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseExpertNotes] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE TRIGGER [dbo].[del_CLDocketNumbers]
   ON  [dbo].[tblClaimLawsuit]
   AFTER DELETE
AS 
BEGIN
      SET NOCOUNT ON;
      
       
           
DECLARE @CPI bigint
DECLARE @DocketNumStr nvarchar(4000)
DECLARE @addDocket nvarchar(4000)
BEGIN

      
      --Declare Cursor
      DECLARE Cur_CPI CURSOR Local Fast_Forward for
      SELECT DISTINCT ClaimantPersonalInfoID FROM deleted INNER JOIN tblClaim c ON c.ClaimID = deleted.ClaimID
      --Open CPI Cursor
      OPEN Cur_CPI

      --Fetch Data
      FETCH NEXT FROM Cur_CPI INTO @CPI

      WHILE @@Fetch_Status = 0
      BEGIN
                        SET @DocketNumStr = ''
                        DECLARE Cur_Docket CURSOR Local Fast_Forward for
                        SELECT L.DocketNumber FROM tblLawsuit l 
                        INNER JOIN tblClaimLawsuit cl ON cl.LawsuitID = l.LawsuitID
                        INNER JOIN tblClaim c ON C.ClaimID = cl.ClaimID WHERE ClaimantPersonalInfoID = @CPI ORDER BY L.DocketNumber ASC

                        --Open Docket Cursor
                        OPEN Cur_Docket

                        --Fetch Data
                        FETCH NEXT FROM Cur_Docket INTO @addDocket

                        WHILE @@Fetch_Status = 0
                        BEGIN
                              --assemble the string
                              if LEN(@DocketNumStr) = 0 SET @DocketNumStr = @addDocket
                              else SET @DocketNumStr =  @DocketNumStr  + ', ' +  @addDocket
                              
                        
            
                              FETCH NEXT FROM Cur_Docket INTO @addDocket
                              
                        END
                        
                        CLOSE Cur_Docket 
                        DEALLOCATE Cur_Docket
                        --insert the string into a temp table with CPI
                        UPDATE tblClaimantDocketNumbers SET DocketNumbers = @DocketNumStr WHERE ClaimantPersonalInfoID = @CPI 
                        
      FETCH NEXT FROM Cur_CPI INTO @CPI
      
      END

CLOSE Cur_CPI 
DEALLOCATE Cur_CPI 
            
END

    -- Insert statements for trigger here

END
GO
DISABLE TRIGGER [dbo].[del_CLDocketNumbers] ON [dbo].[tblClaimLawsuit]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE TRIGGER [dbo].[ins_CLDocketNumbers]
   ON  [dbo].[tblClaimLawsuit]
   AFTER INSERT
AS 
BEGIN
      SET NOCOUNT ON;
      
       
           
DECLARE @CPI bigint
DECLARE @DocketNumStr nvarchar(4000)
DECLARE @addDocket nvarchar(4000)
BEGIN

      
      --Declare Cursor
      DECLARE Cur_CPI CURSOR Local Fast_Forward for
      SELECT DISTINCT ClaimantPersonalInfoID FROM inserted INNER JOIN tblClaim c ON c.ClaimID = inserted.ClaimID
      --Open CPI Cursor
      OPEN Cur_CPI

      --Fetch Data
      FETCH NEXT FROM Cur_CPI INTO @CPI

      WHILE @@Fetch_Status = 0
      BEGIN
                        SET @DocketNumStr = ''
                        DECLARE Cur_Docket CURSOR Local Fast_Forward for
                        SELECT L.DocketNumber FROM tblLawsuit l 
                        INNER JOIN tblClaimLawsuit cl ON cl.LawsuitID = l.LawsuitID
                        INNER JOIN tblClaim c ON C.ClaimID = cl.ClaimID WHERE ClaimantPersonalInfoID = @CPI ORDER BY L.DocketNumber ASC

                        --Open Docket Cursor
                        OPEN Cur_Docket

                        --Fetch Data
                        FETCH NEXT FROM Cur_Docket INTO @addDocket

                        WHILE @@Fetch_Status = 0
                        BEGIN
                              --assemble the string
                              if LEN(@DocketNumStr) = 0 SET @DocketNumStr = @addDocket
                              else SET @DocketNumStr =  @DocketNumStr  + ', ' +  @addDocket
                              
                        
            
                              FETCH NEXT FROM Cur_Docket INTO @addDocket
                              
                        END
                        
                        CLOSE Cur_Docket 
                        DEALLOCATE Cur_Docket
                        --insert the string into a temp table with CPI
                        UPDATE tblClaimantDocketNumbers SET DocketNumbers = @DocketNumStr WHERE ClaimantPersonalInfoID = @CPI 
                        
      FETCH NEXT FROM Cur_CPI INTO @CPI
      
      END

CLOSE Cur_CPI 
DEALLOCATE Cur_CPI 
            
END

END
GO
DISABLE TRIGGER [dbo].[ins_CLDocketNumbers] ON [dbo].[tblClaimLawsuit]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE TRIGGER [dbo].[upd_CLDocketNumbers]
   ON  [dbo].[tblClaimLawsuit]
   AFTER UPDATE
AS 
BEGIN
      SET NOCOUNT ON;
      
       
           
DECLARE @CPI bigint
DECLARE @DocketNumStr nvarchar(4000)
DECLARE @addDocket nvarchar(4000)
BEGIN

      
      --Declare Cursor
      DECLARE Cur_CPI CURSOR Local Fast_Forward for
      SELECT DISTINCT ClaimantPersonalInfoID FROM inserted INNER JOIN deleted ON inserted.ClaimLawsuitID = deleted.ClaimLawsuitID INNER JOIN tblClaim c ON c.ClaimID = inserted.ClaimID or c.claimID = deleted.claimid
      --Open CPI Cursor
      OPEN Cur_CPI

      --Fetch Data
      FETCH NEXT FROM Cur_CPI INTO @CPI

      WHILE @@Fetch_Status = 0
      BEGIN
                        SET @DocketNumStr = ''
                        DECLARE Cur_Docket CURSOR Local Fast_Forward for
                        SELECT L.DocketNumber FROM tblLawsuit l 
                        INNER JOIN tblClaimLawsuit cl ON cl.LawsuitID = l.LawsuitID
                        INNER JOIN tblClaim c ON C.ClaimID = cl.ClaimID WHERE ClaimantPersonalInfoID = @CPI ORDER BY L.DocketNumber ASC

                        --Open Docket Cursor
                        OPEN Cur_Docket

                        --Fetch Data
                        FETCH NEXT FROM Cur_Docket INTO @addDocket

                        WHILE @@Fetch_Status = 0
                        BEGIN
                              --assemble the string
                              if LEN(@DocketNumStr) = 0 SET @DocketNumStr = @addDocket
                              else SET @DocketNumStr =  @DocketNumStr  + ', ' +  @addDocket
                              
                        
            
                              FETCH NEXT FROM Cur_Docket INTO @addDocket
                              
                        END
                        
                        CLOSE Cur_Docket 
                        DEALLOCATE Cur_Docket
                        --insert the string into a temp table with CPI
                        UPDATE tblClaimantDocketNumbers SET DocketNumbers = @DocketNumStr WHERE ClaimantPersonalInfoID = @CPI 
                        
      FETCH NEXT FROM Cur_CPI INTO @CPI
      
      END

CLOSE Cur_CPI 
DEALLOCATE Cur_CPI 
            
END

    -- Insert statements for trigger here

END
GO
DISABLE TRIGGER [dbo].[upd_CLDocketNumbers] ON [dbo].[tblClaimLawsuit]
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [PK_mBtblClaimLawsuitNEW] PRIMARY KEY CLUSTERED  ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimLawsuit_tblClaim] ON [dbo].[tblClaimLawsuit] ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuit_ClaimID_IsMDL] ON [dbo].[tblClaimLawsuit] ([ClaimID], [IsMDL]) INCLUDE ([ClaimLawsuitID], [IsPrimaryForClaim], [LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuit_IsMDL] ON [dbo].[tblClaimLawsuit] ([IsMDL]) INCLUDE ([ClaimID], [ClaimLawsuitID], [IsPrimaryForClaim], [LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_13865_13864_tblClaimLawsuit] ON [dbo].[tblClaimLawsuit] ([IsPrimaryForClaim]) INCLUDE ([ClaimID], [ClaimLawsuitID], [LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimLawsuit_tblLawsuit] ON [dbo].[tblClaimLawsuit] ([LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_LawsuitID_ClaimLawsuitID_ClaimID_IsPrimaryForClaim] ON [dbo].[tblClaimLawsuit] ([LawsuitID]) INCLUDE ([ClaimID], [ClaimLawsuitID], [NavIsPrimaryForClaim]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [FK_tblClaimLawsuit_tblClaim] FOREIGN KEY ([ClaimID]) REFERENCES [dbo].[tblClaim] ([ClaimID])
GO
ALTER TABLE [dbo].[tblClaimLawsuit] ADD CONSTRAINT [FK_tblClaimLawsuit_tblLawsuit] FOREIGN KEY ([LawsuitID]) REFERENCES [dbo].[tblLawsuit] ([LawsuitID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuit] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuit] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuit] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuit] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuit] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuit] TO [power_user]
GO
