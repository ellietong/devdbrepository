CREATE TABLE [dbo].[tblPrimaryNationalPlaintiffCounsel]
(
[PrimaryNationalPlaintiffCounselID] [int] NOT NULL IDENTITY(1, 1),
[PrimaryNationalPlaintiffCounsel] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPrimaryNationalPlaintiffCounsel] ADD CONSTRAINT [PK_tblPrimaryNationalCounsel] PRIMARY KEY CLUSTERED  ([PrimaryNationalPlaintiffCounselID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [power_user]
GRANT SELECT ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [power_user]
GRANT INSERT ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [power_user]
GRANT DELETE ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPrimaryNationalPlaintiffCounsel] TO [power_user]
GO
