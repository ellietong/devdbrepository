CREATE TABLE [dbo].[tblCorpHistoryPurchaseType]
(
[PurchaseTypeID] [int] NOT NULL IDENTITY(1, 1),
[PurchaseType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorpHistoryPurchaseType] ADD CONSTRAINT [PK_mEtblCorpHistoryPurchaseType] PRIMARY KEY CLUSTERED  ([PurchaseTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCorpHistoryPurchaseType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorpHistoryPurchaseType] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorpHistoryPurchaseType] TO [CorpHist_User]
GRANT INSERT ON  [dbo].[tblCorpHistoryPurchaseType] TO [CorpHist_User]
GRANT DELETE ON  [dbo].[tblCorpHistoryPurchaseType] TO [CorpHist_User]
GRANT UPDATE ON  [dbo].[tblCorpHistoryPurchaseType] TO [CorpHist_User]
GRANT SELECT ON  [dbo].[tblCorpHistoryPurchaseType] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblCorpHistoryPurchaseType] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblCorpHistoryPurchaseType] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblCorpHistoryPurchaseType] TO [Enviro_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCorpHistoryPurchaseType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblCorpHistoryPurchaseType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblCorpHistoryPurchaseType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblCorpHistoryPurchaseType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblCorpHistoryPurchaseType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblCorpHistoryPurchaseType] TO [power_user]
GRANT SELECT ON  [dbo].[tblCorpHistoryPurchaseType] TO [power_user]
GRANT INSERT ON  [dbo].[tblCorpHistoryPurchaseType] TO [power_user]
GRANT DELETE ON  [dbo].[tblCorpHistoryPurchaseType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCorpHistoryPurchaseType] TO [power_user]
GO
