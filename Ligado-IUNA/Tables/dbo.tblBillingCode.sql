CREATE TABLE [dbo].[tblBillingCode]
(
[BillingCodeID] [int] NOT NULL IDENTITY(1, 1),
[BillingCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillingCode] ADD CONSTRAINT [PK_tblBillingCode] PRIMARY KEY CLUSTERED  ([BillingCodeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBillingCode] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillingCode] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBillingCode] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBillingCode] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBillingCode] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBillingCode] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBillingCode] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBillingCode] TO [power_user]
GRANT SELECT ON  [dbo].[tblBillingCode] TO [power_user]
GRANT INSERT ON  [dbo].[tblBillingCode] TO [power_user]
GRANT DELETE ON  [dbo].[tblBillingCode] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBillingCode] TO [power_user]
GO
