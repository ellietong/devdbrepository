CREATE TABLE [dbo].[tblSettlementAllocation]
(
[SettlementAllocationID] [int] NOT NULL IDENTITY(1, 1),
[ClaimSettlementID] [int] NULL,
[CarrierAddressID] [int] NULL,
[AllocationAmount] [money] NULL,
[PaymentAmount] [money] NULL,
[PaymentDate] [datetime] NULL,
[Username] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementAllocation] ADD CONSTRAINT [PK_tblSettlementAllocation] PRIMARY KEY CLUSTERED  ([SettlementAllocationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAllocation] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAllocation] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSettlementAllocation] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSettlementAllocation] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSettlementAllocation] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSettlementAllocation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementAllocation] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementAllocation] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementAllocation] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementAllocation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementAllocation] TO [power_user]
GO
