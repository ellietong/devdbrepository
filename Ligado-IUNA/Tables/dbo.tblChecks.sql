CREATE TABLE [dbo].[tblChecks]
(
[AutoID] [int] NOT NULL,
[FundRequestStartDate] [date] NULL,
[FundRequestEndDate] [date] NULL,
[FirmID] [int] NOT NULL,
[CheckNo] [int] NOT NULL,
[CheckDate] [date] NOT NULL,
[CheckAmount] [decimal] (18, 2) NOT NULL,
[AmountDesc] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentLine] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCounselAddress] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FundTypeID] [int] NULL,
[GeneralComment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblChecks] TO [base_user]
GRANT SELECT ON  [dbo].[tblChecks] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblChecks] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblChecks] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblChecks] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblChecks] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblChecks] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblChecks] TO [power_user]
GRANT SELECT ON  [dbo].[tblChecks] TO [power_user]
GRANT INSERT ON  [dbo].[tblChecks] TO [power_user]
GRANT DELETE ON  [dbo].[tblChecks] TO [power_user]
GRANT UPDATE ON  [dbo].[tblChecks] TO [power_user]
GO
