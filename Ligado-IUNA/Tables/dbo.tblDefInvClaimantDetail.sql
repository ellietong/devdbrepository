CREATE TABLE [dbo].[tblDefInvClaimantDetail]
(
[InvoiceDetailID] [int] NOT NULL IDENTITY(1, 1),
[InvoiceNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceDate] [date] NULL,
[ClaimantPersonalInfoID] [bigint] NULL,
[Fees] [money] NULL,
[Costs] [money] NULL,
[Adjustments] [money] NULL,
[AdjustmentNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalTotal] [money] NULL,
[OriginalFee] [money] NULL,
[OriginalDisbursement] [money] NULL,
[AdjustedTotal] AS ((coalesce([Fees],(0))+coalesce([Costs],(0)))-coalesce([Adjustments],(0))),
[AdjustedFee] [money] NULL,
[AdjustedDisbursement] [money] NULL,
[AppealTotal] [money] NULL,
[AppealFee] [money] NULL,
[AppealCost] [money] NULL,
[AppealFlag] [bit] NULL,
[NavPaidInvoice] [bit] NULL,
[Notes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PACEClaimID] [bigint] NULL,
[MatterID] [int] NULL,
[InvoiceIdentifier] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseInvoiceID] [bigint] NULL,
[ClaimLawsuitID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefInvClaimantDetail] ADD CONSTRAINT [PK_tblDefInvClaimantDetail] PRIMARY KEY CLUSTERED  ([InvoiceDetailID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [missing_index_8321_8320_tblDefInvClaimantDetail] ON [dbo].[tblDefInvClaimantDetail] ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblDefInvClaimantDetail] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblDefInvClaimantDetail] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblDefInvClaimantDetail] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefInvClaimantDetail] TO [power_user]
GO
