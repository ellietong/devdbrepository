CREATE TABLE [dbo].[tblExhaustion]
(
[ExhaustionID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionDateID] [int] NOT NULL,
[OccurrenceID] [bigint] NULL,
[PolicyDateID] [int] NOT NULL,
[IndemnityAllocation] [money] NOT NULL CONSTRAINT [DF_tblExhaustion_IndemnityAllocation] DEFAULT ((0)),
[DefenseAllocation] [money] NOT NULL CONSTRAINT [DF_tblExhaustion_DefenseAllocation] DEFAULT ((0)),
[DefenseOutsideLimitsAllocation] [money] NOT NULL CONSTRAINT [DF_tblExhaustion_DefenseOutsideLimitsAllocation] DEFAULT ((0)),
[SIRIndemnityAllocation] [money] NOT NULL CONSTRAINT [DF_tblExhaustion_SIRIndemnityAllocation] DEFAULT ((0)),
[SIRDefenseAllocation] [money] NOT NULL CONSTRAINT [DF_tblExhaustion_SIRDefenseAllocation] DEFAULT ((0)),
[LedgerID] [int] NULL,
[ConsumptionTypeID] [int] NULL,
[OutsideConsumption] [money] NULL,
[BillNumber] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustion] ADD CONSTRAINT [PK_tblExhaustion] PRIMARY KEY CLUSTERED  ([ExhaustionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblExhaustion_tblExhaustionDate] ON [dbo].[tblExhaustion] ([ExhaustionDateID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblExhaustion_tblPolicyDates] ON [dbo].[tblExhaustion] ([PolicyDateID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustion] ADD CONSTRAINT [FK_tblExhaustion_tblExhaustionDate] FOREIGN KEY ([ExhaustionDateID]) REFERENCES [dbo].[tblExhaustionDate] ([ExhaustionDateID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustion] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustion] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustion] TO [Exhaustion_User]
GRANT INSERT ON  [dbo].[tblExhaustion] TO [Exhaustion_User]
GRANT DELETE ON  [dbo].[tblExhaustion] TO [Exhaustion_User]
GRANT UPDATE ON  [dbo].[tblExhaustion] TO [Exhaustion_User]
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustion] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblExhaustion] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblExhaustion] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblExhaustion] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblExhaustion] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblExhaustion] TO [power_user]
GRANT SELECT ON  [dbo].[tblExhaustion] TO [power_user]
GRANT INSERT ON  [dbo].[tblExhaustion] TO [power_user]
GRANT DELETE ON  [dbo].[tblExhaustion] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExhaustion] TO [power_user]
GO
