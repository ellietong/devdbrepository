CREATE TABLE [dbo].[tblClaimLawsuitEmployment]
(
[ClaimLawsuitEmploymentID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NOT NULL,
[OccupationID] [int] NULL,
[Jobsite] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimary] [bit] NULL,
[IsSecondaryExposure] [bit] NULL,
[EmpStartDate] [date] NULL,
[EmpEndDate] [date] NULL,
[EarliestExposureDate] [date] NULL,
[LatestExposureDate] [date] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteStateID] [int] NULL,
[JobsiteOwner] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employer] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BusinessCaseID] [int] NULL,
[BillingCodeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitEmployment] ADD CONSTRAINT [PK_tblClaimLawsuitEmployment] PRIMARY KEY CLUSTERED  ([ClaimLawsuitEmploymentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitEmployment] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitEmployment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitEmployment] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitEmployment] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitEmployment] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitEmployment] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitEmployment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitEmployment] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitEmployment] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitEmployment] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimLawsuitEmployment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitEmployment] TO [power_user]
GO
