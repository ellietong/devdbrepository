SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmAdditionalNamedInsType]
As

SET NOCOUNT ON

SELECT AdditionalNamedInsTypeID, AdditonalNamedInsType
FROM tblAdditionalNamedInsuredType

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAdditionalNamedInsType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAdditionalNamedInsType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAdditionalNamedInsType] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmAdditionalNamedInsType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAdditionalNamedInsType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAdditionalNamedInsType] TO [power_user]
GO
