SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qryrptNoticedPoliciesBySite]
(@CorrespondenceDate datetime = NULL, @Site nvarchar(100) = null, @carrierid varchar(200) = NULL, @carriergroupid varchar(200))
as
set nocount on

--DECLARE @CorrespondenceDate datetime 
--DECLARE @Site nvarchar(100) 
--DECLARE @carrierid varchar(200) 
--DECLARE @carriergroupid varchar(200)

--SET @CorrespondenceDate = '1/1/1900' 
--SET @Site =0
--SET @carrierid =0 
--SET @carriergroupid =0


DECLARE @MaxSiteID Int
Declare @MAXcarrierid int
Declare @MAXcarriergroupid int

SELECT @MaxSiteID = MAX(siteid) from tblEnviroSitesDetail
SELECT @MAXcarrierid = MAX(carrierid) from tblCarrier
SELECT @MAXcarriergroupid = MAX(carriergroupid) from tblCarrier


select ip.insuranceprogram, p.insuranceprogramid, max(pl.attachmentpoint) as maxofattachmentpoint, 
	pl.policyid, cg.carriergroupname, c.carriername, p.policynum, min(pd.pstartdate) as minofpstartdate, 
	max(pd.penddate) as maxofpenddate, pl.attachmentpoint, pl.perocclimit, cor.correspondencetypeid, 
	cor.correspondencedate, esd.sitename
from tblpolicy p inner join tblpolicydates pd on p.policyid = pd.policyid
	inner join tblpolicylimits pl on p.policyid = pl.policyid
	inner join tblinsuranceprogram ip on p.insuranceprogramid = ip.insuranceprogramid
	inner join tblcarrier c on p.carrierid = c.carrierid
	left join tblcarriergroup cg on c.carriergroupid = cg.carriergroupid
	inner join tblpolicynoticed pn on p.policyid = pn.policyid
	inner join tblcorrespondence cor on pn.correspondenceid = cor.correspondenceid
	inner join tblsitenoticed sn on cor.correspondenceid = sn.correspondenceid
	inner join tblEnviroSitesDetail esd on sn.siteid = esd.siteid

where cor.correspondencetypeid = 1
AND sn.SiteId >= coalesce(@site, 0) and sn.SiteId <= coalesce(nullif(@site, 0), @maxsiteid)
AND cor.CorrespondenceDate >= @correspondencedate and cor.correspondencedate <= COALESCE(NULLIF(@CorrespondenceDate, '1/1/1900'), getdate())
and c.CarrierID >= COALESCE(@carrierID, 0) AND c.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID)
and c.CarrierGroupID >= COALESCE(@carriergroupID, 0) AND c.CarrierGroupID <= COALESCE(NULLIF(@carriergroupID, 0), @maxCarriergroupID)


group by ip.insuranceprogram, p.insuranceprogramid, pl.policyid, cg.carriergroupname, c.carriername, p.policynum, 
	pl.attachmentpoint, pl.perocclimit, cor.correspondencetypeid, cor.correspondencedate, esd.sitename
order by ip.insuranceprogram, max(pl.attachmentpoint)
return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNoticedPoliciesBySite] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNoticedPoliciesBySite] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNoticedPoliciesBySite] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptNoticedPoliciesBySite] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNoticedPoliciesBySite] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNoticedPoliciesBySite] TO [power_user]
GO
