SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptSummaryReportByCarrierByPolicy_blairo]
(@AllocationID int, @LimitTypeID int)
As
Set Nocount on

--Declare @LimitTypeID int
--Declare @AllocationID int
--Set @AllocationID = 2
--Set @LimitTypeID = 4
--
Declare @LondonAllocation money
Declare @Header nvarchar(max)
Declare @ExhaustionTypeID int
Declare @Sig int
Set @ExhaustionTypeID = (Select ExhaustionTypeID from tblAllocation where AllocationID = @AllocationID)
Set @Header = (Select Case When SignatoryRun = 1 then 'Signatory Only' when SignatoryRun = 0 then 'Signatory & Non-Signatory' END From tblAllocation where AllocationID = @AllocationID)
Set @Sig = (Select SignatoryRun From tblAllocation where AllocationID = @AllocationID)



Declare @PreviousExhaustionTotals table (PolicyDateID int, PreviousAllocationAmt money)
Insert Into @PreviousExhaustionTotals
Select PolicyDateID,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAllocationAmt
From tblExhaustion E
INner Join tblExhaustionDate ed on ed.ExhaustionDateID=e.ExhaustionDateID
Where ExhaustionTypeID = @ExhaustionTypeID
Group By PolicyDateID

--Current Allocation
Declare @CurrentAllocation Table (PolicyDateID bigint,
	Indemnity money, Defense money, DefenseOutsideLimits money, totaldefense money,  SIRIndemnity money,
	SIRDefense money)
Insert Into @CurrentAllocation
Select PolicyDateID, 
	coalesce(Sum(AllocationIndemnity), 0) as Indemnity,
	coalesce(Sum(AllocationDefense), 0) as Defense,
	coalesce(Sum(AllocationDefenseOutsideLimits),0) as DefenseOutsideLimits,
	coalesce(Sum(AllocationDefense + AllocationDefenseOutsideLimits),0) as TotalDefense,
	coalesce(Sum(SIRAllocatedIndemnity), 0) as SIRIndemnity,
	coalesce(Sum(SIRAllocatedDefense), 0) as SIRDefense
From tblAllocationResults AR
Where AllocationID = @AllocationID 
Group BY PolicyDateID

If @Sig = 1 Begin
Declare @NonSigAllocation int
Set @NonSigAllocation = (Select NonSigAllocationID From tblAllocation where AllocationID = @AllocationID)
Set @LondonAllocation = (Select (SUM(Allocation)) from tblAllocationResults ar Inner Join tblPolicyDates pd on pd.PolicyDateID = AR.PolicyDateID
Inner Join tblPolicy p on p.PolicyID = pd.PolicyID where AllocationID = @AllocationID and carrierid=20) 

Insert Into @CurrentAllocation
Select ar.PolicyDateID,
	coalesce(Sum(AllocationIndemnity), 0) as Indemnity,
	coalesce(Sum(AllocationDefense), 0) as Defense,
	coalesce(Sum(AllocationDefenseOutsideLimits),0) as DefenseOutsideLimits,
	coalesce(Sum(AllocationDefense + AllocationDefenseOutsideLimits),0) as TotalDefense,
	coalesce(Sum(SIRAllocatedIndemnity), 0) as SIRIndemnity,
	coalesce(Sum(SIRAllocatedDefense), 0) as SIRDefense
From tblAllocationResults AR
Inner Join tblPolicyDates pd on pd.PolicyDateID = AR.PolicyDateID
Inner Join tblPolicy p on p.PolicyID = pd.PolicyID
Where AllocationID = @NonSigAllocation and p.CarrierID in (121,336,107) 
Group BY ar.PolicyDateID

End

--Query Results
Declare @Results Table(OrderID int, policyid bigint, policydateid bigint, carriername nvarchar(100), layer int, policynum nvarchar(100), pstartdate datetime, penddate datetime, attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), Solvency nvarchar(100), TotalPercentage float, Indemnity money, Defense money, DefenseOutsideLimits money, TotalDefense money, PreviousAllocationAmt money,
	Balance money, Adjustments money)
Insert Into @Results

--London
select 1 as OrderID, p.policyid, pd.policydateid, c.carriername, pl.layer, 
PolicyNum,
pd.pstartdate, pd.penddate,
	pl.attachmentpoint, pl.perocclimit as perocclimit, pl.aggregatelimit as aggregatelimit,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	'',
	'' as TotalPercentage, 
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0)as PreviousAllocationAmt,
 	(coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0) - coalesce(PreviousAllocationAmt,0)) as Balance,
 	Case When @Sig =1 then [Indemnity] * -1 + [Defense]* -1 + [DefenseOutsideLimits] * -1 else 0 end as Adjustments
	
from tblpolicy p 
	Left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	Inner Join @CurrentAllocation Ca on Ca.PolicyDateID = pd.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	Left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where limittypeid = @LimitTypeID and provisionlabelID=1 and C.CarrierID=20
group by p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, Indemnity, Defense,
DefenseOutsideLimits, pd.pstartdate, pd.penddate, pl.attachmentpoint,
	pl.perocclimit, pl.aggregatelimit, totaldefense, pro.ProvisionValueID,
	PreviousAllocationAmt
Union
--Most Carriers
select 1 as OrderID, p.policyid, pd.policydateid, c.carriername, pl.layer, p.policynum, pd.pstartdate, pd.penddate,
	pl.attachmentpoint, pl.perocclimit,
	Case When p.PolicyID= 95 then 750000 else pl.aggregatelimit end as aggregatelimit,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	'' As Solvency,
	'' as TotalPercentage,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
 	Case When p.PolicyID= 95 then(750000 - coalesce(Indemnity,0) - coalesce(Defense,0)  - coalesce(PreviousAllocationAmt,0))else
 	(coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)  - coalesce(PreviousAllocationAmt,0)) end as Balance,
 	0 as Adjustments
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on PD.PolicyDateID=PET.PolicyDateID
	inner Join @CurrentAllocation CA on PD.PolicyDateID=CA.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where limittypeid = @LimitTypeID and provisionlabelID=1 and p.CarrierID <> 20
Union
--Dana Producer (change sig amount every month)
select 2 as OrderID, '', '', 'DANA', null as layer, 'Producer' as policynum, null as pstartdate, null as penddate,
	null as attachmentpoint, null as perocclimit, null as aggregatelimit, '' as DefenseForm,
	'' As Solvency,
	'' as TotalPercentage,
	Sum(Coalesce(Case when ot.DefenseType = 0 Then UncollapsedPrior+UncollapsedPost End,0)) as Indemnity,
    0,
    Sum(Coalesce(Case when ot.DefenseType = 1 Then UncollapsedPrior+UncollapsedPost End,0)) as DefenseOutsideLimits,
	Sum(Coalesce(Case when ot.DefenseType = 1 Then UncollapsedPrior+UncollapsedPost End,0)) as TotalDefense, 
	Case when @Sig=0 then 2122813.77 when @Sig=1 then 24871784.44 end as PreviousAllocationAmt,
 	null as Balance,
 	case when @Sig =1 then @LondonAllocation else  0 end as Adjustments
from tblAllocationResultsHorizontal arh 
Inner Join tblOccurrence o on o.occurrenceID = arh.occurrenceID
Inner Join tblOccurrenceType ot on ot.occurrenceTypeID = o.occurrenceTypeID
Where AllocationID = @AllocationID 

Select R.*, @AllocationID as AllocationID, @LimitTypeID as LimitTypeID, @Header as Header2
From @Results R
order by OrderID, carriername, pstartdate


Return



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_blairo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_blairo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_blairo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_blairo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_blairo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_blairo] TO [power_user]
GO
