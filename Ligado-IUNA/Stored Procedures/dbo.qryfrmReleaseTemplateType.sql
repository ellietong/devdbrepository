SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmReleaseTemplateType] 
AS

SET NOCOUNT ON

SELECT ReleaseTemplateTypeID, ReleaseTemplateType
FROM tblReleaseTemplateType
ORDER BY ReleaseTemplateType

RETURN
GO
