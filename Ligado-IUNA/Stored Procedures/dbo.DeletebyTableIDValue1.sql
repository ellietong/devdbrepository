SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DeletebyTableIDValue]
(
	@TableName varchar(25)
	,@FieldName varchar(25)
	,@FieldValue varchar(25)	
)
AS
SET NOCOUNT on
/*
Author: Ellie Tong
Date: 02/23/2015
Purpose:  Issue simple delete statements by passing in table name, field name and values (can be separated by comma)

*/
DECLARE @SQL NVARCHAR(500) = null
	,@Message VARCHAR(250) = NULL
    ,@rowcount SMALLINT = NULL
BEGIN TRY
--	SELECT * FROM tonge.Split (@FieldValue,',')

	SET @sql = 'select count(1) from ' + @TableName  + ' where '+ @FieldName + ' in (' + @FieldValue + ')'
	PRINT @SQL
	EXECUTE sp_executesql @SQL
	
	BEGIN TRAN

	SET @SQL = 'delete from ' + @TableName  + ' where '+ @FieldName + ' in (' + @FieldValue + ')'
	PRINT @SQL
	EXECUTE sp_executesql @SQL
	SELECT @rowcount = @@ROWCOUNT

	IF (@@error <> 0 ) BEGIN
	SET @Message =  'unexpected error' + CONVERT(VARCHAR(2),@@ERROR	)
    PRINT @Message
	ROLLBACK TRAN
	RETURN 1
	END

	COMMIT TRAN
	set @Message = CONVERT(VARCHAR(2),@rowcount) + ' rows deleted.'
	print @Message

	
END TRY
BEGIN CATCH
	SELECT
		errorNumber = ERROR_NUMBER()
	   ,errorseverity=ERROR_SEVERITY()
	   ,errorstate = ERROR_STATE()
	   ,errorprocedure = ERROR_PROCEDURE()
	   ,errorline = ERROR_LINE()
	   ,errorMessage = ERROR_MESSAGE()
END CATCH
GO
