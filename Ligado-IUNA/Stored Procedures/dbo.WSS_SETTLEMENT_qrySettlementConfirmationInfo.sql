SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qrySettlementConfirmationInfo]
	-- Add the parameters for the stored procedure here
@ClaimLawsuitID int
AS
BEGIN

	SET NOCOUNT ON;
	--DECLARE @ClaimLawsuitID bigint = 244326


SELECT DISTINCT CL.ClaimLawsuitID, CPI.ClaimantPersonalInfoID, CASE WHEN Suffix IS NULL THEN CPI.LastName + ', ' ELSE CPI.LastName + ' ' + Suffix + ', ' END + CASE WHEN FirstName IS NULL 
						  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName,
						  CASE WHEN SettlementAmount IS NOT NULL THEN CONVERT( nvarchar(250), SettlementAmount, 1) ELSE '' END AS SettlementAmount,
						  CONVERT(nvarchar(100), SettlementDate, 101) AS SettlementDate, 
						  PayToCompany, CONVERT(nvarchar(100), SettlementDueDate, 101) AS SettlementDueDate,
						  SettlementNotes, 
						  CS.SPUsername, 
						  PCA.PlaintiffCounselTaxIDNum AS NationalTaxID, 
						  pca.PlaintiffCounsel AS PlaintiffCounsel,  
						  CONVERT(nvarchar(100), GETDATE(), 101) AS NotificationDate, 
						  CASE WHEN LeadPlaintiffFirstName IS NOT NULL THEN LeadPlaintiffFirstName + ' ' + LeadPlaintiffLastName ELSE LeadPlaintiffLastName END AS LeadPlaintiff, 
						  LeadPlaintiffFirstName, 
						  LeadPlaintiffLastName, 
						  l.PlaintiffCounselAddressID,
						  cs.SPUsername as CreatedBy,
						  SettlementGroup,
						  csg.PaymentTermsID,
						  PaymentTerms,
						  clst.ClaimLawsuitStatusID,
						  clstatus.ClaimLawsuitStatus,
						  LastName,
						  FirstName,
						  MiddleName,
						  Jurisdiction,
						  DocketNumber, 
						  ReleaseType,
						  CONVERT(nvarchar, COALESCE(SettlementDate, '1/1/1900'), 101) AS ResolutionDate,
						  SettlementGroup
						  --, CASE WHEN LeadPlaintiffFirstName IS NOT NULL THEN LeadPlaintiffLastName + ', ' + LeadPlaintiffFirstName ELSE LeadPlaintiffLastName END AS LeadPlaintiff			 

FROM tblClaimLawsuit CL
	LEFT JOIN tblClaimSettlement cs on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblClaim C ON CL.ClaimID = C.ClaimID
	INNER JOIN tblClaimantPersONalInfo CPI ON c.ClaimantPersONalInfoID = CPI.ClaimantPersONalInfoID
	LEFT JOIN tblClaimantSuffix CLS ON  CPI.SuffixID =CLS.SuffixID 
	INNER JOIN tblLawsuit L ON CL.LawsuitID = L.LawsuitID
	LEFT JOIN tblPlaintiffCounselAddress PCA ON PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
	LEFT JOIN tblClaimSettlementGroup csg ON cs.SettlementGroupID = csg.SettlementGroupID
	LEFT JOIN tblPaymentTerms pt ON csg.PaymentTermsID = pt.PaymentTermsID
	LEFT JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatus clstatus ON clst.ClaimLawsuitStatusID = clstatus.ClaimLawsuitStatusID
	LEFT JOIN tblJurisdiction j ON l.JurisdictionID = j.JurisdictionID
	LEFT JOIN tblReleaseType rt ON cs.ReleaseTypeID = rt.ReleaseTypeID
	WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
			AND COALESCE(clst.IsPrimaryStatus, 0) = 1
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementConfirmationInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementConfirmationInfo] TO [Ligado_User]
GO
