SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryrptGenericLanguageGroup]
(@limittypeid int, @selectedProvisions nvarchar(1000))
AS 
SET NOCOUNT ON



--DECLARE @limittypeid int
--SET @limittypeid=1
----Declare the lists of selected IDs; note: these will be passed in, and thus should be commented out in actual stored proc
--DECLARE @selectedProvisions nvarchar(1000)
--SET @selectedProvisions = '1'

--Declare backup strings of the passed in lists to be used in the ultimate where clauses below
DECLARE @selectedProvisionsList nvarchar(1000)
SET @selectedProvisionsList = @selectedProvisions

--Declare the tables to be used for the separated provision and provision value IDs
DECLARE @selectedProvisionsTable TABLE(provisionLabelID int)

--Prepare flags if there is only a zero in the table (and thus there should be no filter)
DECLARE @flagProvisionFilter int
SET @flagProvisionFilter = 0

--Parse out provision and provision value ID strings	
WHILE LEN(@selectedProvisions)>0 BEGIN
	IF CHARINDEX(',',@selectedProvisions) = 0 BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, @selectedProvisions)
		IF CONVERT(int,@selectedProvisions) <> 0
			SET @flagProvisionFilter = 1
		SET @selectedProvisions = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, LEFT(@selectedProvisions, CHARINDEX(',',@selectedProvisions)-1))
		SET @selectedProvisions = RIGHT(@selectedProvisions,LEN(@selectedProvisions)-CHARINDEX(',',@selectedProvisions))
		SET @flagProvisionFilter = 1
	END
END


IF @flagProvisionFilter = 1
SELECT CarrierName, PolicyNum, PStartDate, PEndDate, PerOccLimit, coalesce(GenericLanguage, exactLanguage) as GenericLanguage, ExactLanguage,
	Provision

FROM tblPolicy p
INNER JOIN tblExactLanguage el on el.policyid = p.PolicyID
left join tblGenericLanguage gl on el.genericLanguageID = gl.genericLanguageID
inner join tblCarrier c on c.CarrierID = p.CarrierID
inner join tblPolicyDates pd on pd.PolicyID = p.PolicyID
inner join tblPolicyLimits pl on pl.PolicyID = p.PolicyID
inner join tblProvisionLanguage plang on plang.ExactLanguageID = el.exactlanguageid
inner join tblProvisionLabel plab on plab.ProvisionLabelID = plang.ProvisionLabelID

where pl.LimitTypeID = @limittypeid
AND plab.ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable)
ELSE
SELECT CarrierName, PolicyNum, PStartDate, PEndDate, PerOccLimit, coalesce(GenericLanguage, exactLanguage) as GenericLanguage, ExactLanguage,
	Provision

FROM tblPolicy p
INNER JOIN tblExactLanguage el on el.policyid = p.PolicyID
left join tblGenericLanguage gl on el.genericLanguageID = gl.genericLanguageID
inner join tblCarrier c on c.CarrierID = p.CarrierID
inner join tblPolicyDates pd on pd.PolicyID = p.PolicyID
inner join tblPolicyLimits pl on pl.PolicyID = p.PolicyID
inner join tblProvisionLanguage plang on plang.ExactLanguageID = el.exactlanguageid
inner join tblProvisionLabel plab on plab.ProvisionLabelID = plang.ProvisionLabelID

where pl.LimitTypeID = @limittypeid
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptGenericLanguageGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptGenericLanguageGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptGenericLanguageGroup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptGenericLanguageGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptGenericLanguageGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptGenericLanguageGroup] TO [power_user]
GO
