SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmIssueType]
AS
SET NOCOUNT ON
SELECT *
FROM tblIssueType
ORDER BY IssueType
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmIssueType] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmIssueType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmIssueType] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmIssueType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmIssueType] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmIssueType] TO [power_user]
GO
