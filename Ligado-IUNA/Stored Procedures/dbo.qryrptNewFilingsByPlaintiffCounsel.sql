SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[qryrptNewFilingsByPlaintiffCounsel]
(@BeginDate datetime, @EndDate datetime, @matterid int)
As 
Set NoCount On


-- Declare @BeginDate datetime
-- Declare @EndDate datetime

-- Set @BeginDate = '7/1/2014'
-- Set @EndDate = '9/30/2014'
--Declare @matterID int
--set @matterID = 1

DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState


Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int,  PrimaryLocalPlaintiffCounsel nvarchar(1000),FilingCount int)
Insert into @DiseaseCount

Select Matter, C.MatterID, PCA.PlaintiffCounsel, 1
From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblPlaintiffCounselAddress PCA on PCA.PlaintiffCounselAddressID=L.PlaintiffCounselAddressID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
left join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=CL.ClaimLawsuitID

Left Join tblState S on L.StateID=S.StateID
LEFT JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID

Where ProcessedDate between @BeginDate and @EndDate 
	AND s.StateID <= (@maxStateID)
	AND IsPrimaryForClaim = 1 AND ClaimLawsuitStatusID = 22
	AND m.MatterID = @matterid
Order By PCA.PlaintiffCounsel

 


Select Matter, MatterID, PrimaryLocalPlaintiffCounsel, Sum(FilingCount) as NewFilingCount, @BeginDate as StartDate, @EndDate as EndDate
from @DiseaseCount
Group By Matter, MatterID, PrimaryLocalPlaintiffCounsel
ORDER BY Matter, PrimaryLocalPlaintiffCounsel

Return



GO
