SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailInvoiceSummary_IUNA]

AS
BEGIN
/*External Email to Marie Shipley which runs once a week.  The email includes two tables.
The first table is for invoices which have been uploaded within the past week.
The second table is for invoices which are over a week old but she has not yet approved or rejected. - BEH*/

--Declare and set sp_send_dbmail input variables
DECLARE @Subject nvarchar (100) = 'IUNA: Weekly Invoice Summary ' + CONVERT(varchar, GETDATE() - 7, 107) + ' - ' + CONVERT(varchar, GETDATE(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top and the header for table 1
DECLARE @BodyMessage nvarchar (max)
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--Create temp table for invoices which have been uploaded in the past week
DECLARE @NewWeekInvoicesTable TABLE (DocumentID   BIGINT, 
									 [Status]     NVARCHAR(30), 
									 InvoiceNum   NVARCHAR(100),
									 Period       NVARCHAR(100), 
									 Fees         MONEY, 
									 Disbursement MONEY, 
									 Adjustment   MONEY, 
									 SubmitDate   DATETIME, 
									 DatePayable  DATETIME, 
									 Firm         NVARCHAR(100))
INSERT INTO @NewWeekInvoicesTable
	SELECT d.DocumentID,
		[Status],
		di.InvoiceNumber,
		Period,
		Fees,
		Costs,
		Adjustments,
		DateCreated,
		DatePayable,
		DefenseCounsel
	FROM tblDocuments d
		INNER JOIN tblDefenseInvoice       di   ON di.DocumentID = d.DocumentID
		INNER JOIN tblDefenseCounsel       dc   ON di.FirmID = dc.DefenseCounselID
		INNER JOIN tblDefInvClaimantDetail dicd ON dicd.DefenseInvoiceID = di.DefenseInvoiceID
		LEFT  JOIN tblDefenseInvoiceStatus s    ON di.StatusID = s.StatusID
		INNER JOIN tblClaimantDocumentType cdt  ON cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
	WHERE Datecreated BETWEEN GETDATE() - 7 AND GETDATE()
		AND cdt.ClaimantDocumentGroupID = 2
		AND dicd.MatterID = 1
		AND FirmID <> 38
	GROUP BY d.DocumentID,
		[Status],
		di.InvoiceNumber,
		Period,
		Fees, 
		Costs, 
		Adjustments,
		DateCreated,
		DatePayable,
		DefenseCounsel

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

SET @BodyMessage = @BodyMessage + COALESCE('<H4>The following IUNA invoices have been uploaded since ' + CONVERT(varchar, GETDATE() - 7, 107) + ':</H4>' + N'<table style=" padding: .3em; border-collapse: collapse; border: 1px solid #0A0A0A;" >' +
N'<tr><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Firm</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Number</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Period</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Fees</th>' +
'<th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Expenses</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Total Billed</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Submit Date</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Payable</th>' +
CAST((SELECT @TDStyle AS 'td/@style', (SELECT Firm)       AS td, '', 
			 @TDStyle AS 'td/@style', (SELECT InvoiceNum) AS td, '', 
			 @TDStyle AS 'td/@style', (SELECT Period)     AS td, '', 
			 @TDStyle AS 'td/@style', (SELECT '$' + CONVERT(nvarchar(30), COALESCE(fees, 0), 1))         AS td, '',
			 @TDStyle AS 'td/@style', (SELECT '$' + CONVERT(nvarchar(30), COALESCE(disbursement, 0), 1)) AS td,	'',
			 @TDStyle AS 'td/@style', (SELECT '$' + CONVERT(nvarchar(30), (COALESCE(fees, 0) + COALESCE(disbursement, 0)), 1)) AS td, '',
			 @TDStyle AS 'td/@style', (SELECT CONVERT(nvarchar(100), SubmitDate, 101))  AS td, '',
			 @TDStyle AS 'td/@style', (SELECT CONVERT(nvarchar(100), DatePayable, 101)) AS td, ''
	  FROM @NewWeekInvoicesTable i
	  ORDER BY Firm, SubmitDate
	  FOR xml PATH ('tr'), TYPE) AS NVARCHAR(MAX)) +
	  N'</table>' + N'<br/>', '<H4>No IUNA invoices have been uploaded since ' + CONVERT(varchar, GETDATE() - 7, 107) + '.</H4>')

--Create temp table for invoices over a week old which have not yet been approved or rejected
DECLARE @OlderThanWeekInvoicesTable TABLE (DocumentID   NVARCHAR(100), 
										   [Status]     NVARCHAR(30), 
										   InvoiceNum   NVARCHAR(100),
										   Period       NVARCHAR(100), 
										   Fees         MONEY, 
										   Disbursement MONEY, 
										   Adjustment   MONEY, 
										   SubmitDate   DATETIME, 
										   DatePayable  DATETIME, 
										   Firm         NVARCHAR(100))
INSERT INTO @OlderThanWeekInvoicesTable
	SELECT d.DocumentID,
		[Status],
		di.InvoiceNumber,
		Period,
		Fees,
		Costs,
		Adjustments,
		DateCreated,
		DatePayable,
		DefenseCounsel
	FROM tblDocuments d
		INNER JOIN tblDefenseInvoice       di   ON di.DocumentID = d.DocumentID
		INNER JOIN tblDefenseCounsel       dc   ON di.FirmID = dc.DefenseCounselID
		INNER JOIN tblDefInvClaimantDetail dicd ON dicd.DefenseInvoiceID = di.DefenseInvoiceID
		LEFT  JOIN tblDefenseInvoiceStatus s    ON di.StatusID = s.StatusID
		INNER JOIN tblClaimantDocumentType cdt  ON cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
	WHERE DateCreated < GETDATE() - 7
		AND cdt.ClaimantDocumentGroupID = 2
		AND di.StatusID IS NULL
		AND d.MatterID = 1
		AND FirmID <> 38
	GROUP BY d.DocumentID,
		[Status],
		di.InvoiceNumber,
		Period,
		Fees, 
		Costs, 
		Adjustments,
		DateCreated,
		DatePayable,
		DefenseCounsel

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @bodymessage + COALESCE('<H4>The following IUNA invoices have been uploaded since ' + CONVERT(varchar, GETDATE() - 7, 107) + ':</H4>' + N'<table style=" padding: .3em; border-collapse: collapse; border: 1px solid #0A0A0A;" >' +
N'<tr><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Firm</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Number</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Period</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Fees</th>' +
'<th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Expenses</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Total Billed</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Submit Date</th><th style="font-family: Goudy Old Style; font-size: 10pt; border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Payable</th>' +
CAST((SELECT @TDStyle AS 'td/@style', (SELECT Firm) AS td, '',
			 @TDStyle AS 'td/@style', (SELECT InvoiceNum) AS td, '',
			 @TDStyle AS 'td/@style', (SELECT Period) AS td, '',
			 @TDStyle AS 'td/@style', (SELECT '$' + CONVERT(nvarchar(30), COALESCE(Fees, 0), 1)) AS td, '',
			 @TDStyle AS 'td/@style', (SELECT '$' + CONVERT(nvarchar(30), COALESCE(Disbursement, 0), 1)) AS td, '',
			 @TDStyle AS 'td/@style', (SELECT '$' + CONVERT(nvarchar(30), (COALESCE(Fees, 0) + COALESCE(Disbursement, 0)), 1)) AS td, '',
			 @TDStyle AS 'td/@style', (SELECT CONVERT(nvarchar(100), SubmitDate, 101))  AS td, '',
			 @TDStyle AS 'td/@style', (SELECT CONVERT(nvarchar(100), DatePayable, 101)) AS td, ''
	  FROM @OlderThanWeekInvoicesTable i
	  ORDER BY Firm, SubmitDate
	  FOR XML PATH ('tr'), TYPE) AS NVARCHAR(MAX)) +
	  N'</table>' + N'<br/>', '<H4>No unapproved IUNA invoices before ' + CONVERT(varchar, GETDATE() - 7, 107) + '.</H4>')

--Include link to invoice roll up site at the bottom
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Invoices.aspx">here</A> to access the invoice(s).</b><br><br>'

--Include KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

DECLARE @RoleID int = 216 --qryExternalEmailInvoiceSummary_IUNA (TO) RoleID 216
DECLARE @RecipientList nvarchar (500)
SET @RecipientList = (SELECT Email + '; ' AS [text()]
					  FROM [Ligado-ExtranetSecurity].dbo.vPersonRoles pr
						  INNER JOIN [Ligado-ExtranetSecurity].dbo.Person p ON pr.PersonID = p.PersonID
					  WHERE RoleID = @RoleID
						  AND ClientSiteID = dbo.fn_GetClientSiteID() FOR XML PATH(''))

--Execute email-sending stored procedure
EXEC msdb.dbo.sp_send_dbmail	
	@profile_name          = 'Claims',
	@recipients            = @RecipientList,
	@blind_copy_recipients = 'iuna_nosroc@kcic.com',
	@subject               = @subject,
	@body                  = @BodyMessage,
	@body_format           = 'html'

END

GO
