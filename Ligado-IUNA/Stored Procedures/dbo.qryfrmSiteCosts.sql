SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROC [dbo].[qryfrmSiteCosts]
AS
Set Nocount On

Select *
from dbo.tblSiteCosts

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteCosts] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteCosts] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteCosts] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmSiteCosts] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSiteCosts] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSiteCosts] TO [power_user]
GO
