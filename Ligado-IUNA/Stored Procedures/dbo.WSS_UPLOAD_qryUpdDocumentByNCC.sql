SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryUpdDocumentByNCC] (@DetailDocumentTypeID	int,					--aka ClaimantDocumentTypeID
												   @ClaimantPersonalInfoID	bigint = NULL,	
												   @Comments				nvarchar(4000) = NULL,	--comments are optional		
												   @DocumentURL				nvarchar(4000),
												   @DocumentTitle			nvarchar(4000),
												   @SPDocId					nvarchar(50),	
												   @MatterID				int = NULL,				--this is the guid assigned by SharePoint
												   @FirmID					int,
												   --These are null unless the document is an Invoice document
												   @InvoiceNumber			nvarchar(50) = NULL,
												   @Period					nvarchar(50) = NULL,
												   @DatePayable				datetime = NULL,
												   @FeeAmount				money = NULL,
												   @DisbursementAmount		money = NULL,
    											   @Adjustment				money = NULL,
    											   @AdjustmentNotes			nvarchar(4000) = NULL,
    											   @StatusID				int = NULL,
												   @ModifiedBy				nvarchar(250),
												   @DateModified			datetime,
												   @ApprovedBy				nvarchar(50) = NULL,
												   @DateApproved			datetime = NULL,
												   @DocID					int = NULL,
												   @DocumentSourceID		int,
												   @PrivateComments			nvarchar(4000) = NULL,
												   @ExpertFirmID			int = NULL)
AS

UPDATE tblDocuments 
	SET	DetailDocumentTypeID	= @DetailDocumentTypeID,	
		ClaimantPersonalInfoID	= @ClaimantPersonalInfoID,
		Comments				= @Comments,	
		DocumentURL				= @DocumentURL,	
		DocumentTitle			= @DocumentTitle,	
		InvoiceNumber			= @InvoiceNumber,
		Period					= @Period,
		DatePayable				= @DatePayable,
		FeeAmount				= @FeeAmount,
		DisbursementAmount		= @DisbursementAmount,
		Adjustment				= @Adjustment,
		AdjustmentNotes			= @AdjustmentNotes,
		StatusID				= @StatusID,
		ModifiedBy				= @ModifiedBy,
		DateModified			= @DateModified,
		ApprovedBy				= @ApprovedBy,
		DateApproved			= @DateApproved,
		DocumentSourceID		= @DocumentSourceID,
		PrivateComments			= @PrivateComments,
		MatterID				= @MatterID,
		FirmID					= @FirmID,
		ExpertFirmID			= @ExpertFirmID	
	WHERE DocumentID = @DocID

SELECT @DocID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocumentByNCC] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocumentByNCC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocumentByNCC] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocumentByNCC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocumentByNCC] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocumentByNCC] TO [power_user]
GO
