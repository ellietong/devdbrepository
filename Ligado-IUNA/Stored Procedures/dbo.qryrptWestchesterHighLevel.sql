SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryrptWestchesterHighLevel] (@SettlementDateBegin datetime, @SettlementDateEnd datetime,@ExecutedReleaseBegin datetime, @ExecutedReleaseEnd datetime, @header nvarchar(100), @Matter int)

As

SET NOCOUNT ON

-- Declare @SettlementDateBegin datetime
-- Declare @SettlementDateEnd datetime
-- Set @SettlementDateBegin='1/1/2010'
--Set @SettlementDateEnd='1/31/2015'
-- Declare @ExecutedReleaseBegin datetime
-- Declare @ExecutedReleaseEnd datetime
-- Set @ExecutedReleaseBegin='1/1/2015'
--Set @ExecutedReleaseEnd='1/31/2015'
--Declare @header nvarchar(100)
--set @header = 'January 2015'
--Declare @Matter int
--Set @Matter = 1

--7/25/12 - AG - Update query to account for capped WLB fees at $325k.

DECLARE @InvoiceDateStart datetime
SET @InvoiceDateStart = @SettlementDateBegin
DECLARE @maxMatterID int
SELECT
  @maxMatterID = MAX(MatterID)
FROM tblDefInvClaimantDetail

IF OBJECT_ID('tempdb..#Wilbraham') IS NOT NULL
   DROP TABLE #Wilbraham

CREATE TABLE #Wilbraham (
  FirmID int,
  WLBAdjustment money,
  WLBMatter int
)
INSERT INTO #Wilbraham
  SELECT
    CASE
      WHEN di.DefenseCounselAddressID = 43 THEN di.DefenseCounselAddressID
    END,
    CASE
      WHEN di.DefenseCounselAddressID = 43 AND
        dicd.MatterID = 1 AND
        Fees < 325000.00 THEN (325000.00 - Fees)
    END AS Wilbraham,
    2
  FROM tblDefenseInvoice DI
    INNER JOIN tblDefInvClaimantDetail dicd ON dicd.DefenseInvoiceID = di.DefenseInvoiceID
    INNER JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselAddressID = di.DefenseCounselAddressID
    INNER JOIN tblMatter M ON DIcd.MatterID = M.MatterID
    INNER JOIN tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
    LEFT JOIN tblPaymentDefense pd ON pd.DefenseInvoiceID = di.DefenseInvoiceID
    LEFT JOIN tblCheck ck ON ck.CheckID = pd.CheckID
  WHERE (ck.SourceDate BETWEEN @ExecutedReleaseBegin AND @ExecutedReleaseEnd
    AND DIS.StatusID IN (1, 2)
    AND (DI.SubmitDate IS NOT NULL)
    AND DIcd.MatterID >= COALESCE(1, 0)
    AND DIcd.MatterID <= COALESCE(NULLIF(1, 0), @maxMatterID)
    AND CheckFundingSourceID IN (1, 2))
    OR ((di.DefenseCounselAddressID = 105
    AND di.DatePayable BETWEEN @ExecutedReleaseBegin AND @ExecutedReleaseEnd)
    AND DIcd.MatterID >= COALESCE(1, 0)
    AND DIcd.MatterID <= COALESCE(NULLIF(1, 0), @maxMatterID))

IF OBJECT_ID('tempdb..#defense') IS NOT NULL
   DROP TABLE #defense

CREATE TABLE #defense (
  firmid int,
  defensetotal money
)
INSERT INTO #defense
  SELECT
    di.defensecounseladdressid,
    CASE
      WHEN MAX(di.DefenseCounselAddressID) = 43 AND
        @Matter = 1 AND
        Fees >= 325000 THEN SUM(325000.00 + costs)
      ELSE CASE
          WHEN MAX(di.firmID) = 21 AND
            @Matter = 1 AND
            Fees < 325000 THEN SUM(Fees + costs)
          ELSE CASE
              WHEN di.defensecounseladdressid = 43 AND
                @Matter = 2 THEN SUM(COALESCE(w.WlbAdjustment, 0) + costs)
              ELSE SUM(Fees + costs - Adjustments)
            END
        END
    END AS DefenseTotal
  FROM tblDefenseInvoice di
    INNER JOIN tblDefenseCounselAddress dca ON DI.DefenseCounselAddressID = dca.defensecounseladdressid
    JOIN tblDefInvClaimantDetail dicd ON dicd.DefenseInvoiceID = di.DefenseInvoiceID
    LEFT JOIN tblPaymentDefense PD ON PD.DefenseInvoiceID = DI.DefenseInvoiceID
    LEFT JOIN tblCheck CK ON CK.CheckID = PD.CheckID
    LEFT JOIN #Wilbraham w ON w.FirmID = di.firmid
    AND w.wlbmatter = dicd.matterid
  WHERE ((CK.SourceDate BETWEEN @ExecutedReleaseBegin AND @ExecutedReleaseEnd
    AND ck.checkfundingsourceid <> 3)
    OR (DI.DefenseCounselAddressID = 105
    AND di.datepayable BETWEEN @ExecutedReleaseBegin AND @ExecutedReleaseEnd)
    AND (DI.StatusID IN (1, 2))
    AND (di.SubmitDate IS NOT NULL))
    AND DIcd.MatterID = @Matter
  GROUP BY di.DefenseCounselAddressID,
           Defensecounsel,
           fees


SELECT
  COALESCE(SUM(SettlementAmount), 0) AS IndemnityTotal,
  (SELECT
    Matter
  FROM tblMatter
  WHERE MatterID = @Matter)
  AS Matter,
  COALESCE((SELECT
    SUM(defensetotal)
  FROM #defense), 0) AS DefenseTotal

FROM tblClaimSettlement CS
  LEFT JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = CS.ClaimLawsuitID
  LEFT JOIN tblLawsuit L ON CL.LawsuitID = L.LawsuitID
  LEFT JOIN tblClaim c ON c.ClaimID = cl.ClaimID
  LEFT JOIN tblMatter m ON m.MatterID = c.MatterID
WHERE (SettlementDate >= @SettlementDateBegin
  AND SettlementDate <= COALESCE(@SettlementDateEnd, CONVERT(varchar(10), GETDATE(), 101)))
  AND ExecutedReleaseDate BETWEEN @ExecutedReleaseBegin AND @ExecutedReleaseEnd
  AND c.MatterID >= COALESCE(@Matter, 0)
  AND c.MatterID <= COALESCE(NULLIF(@Matter, 0), @maxMatterID)
  AND SettlementAmount <> 0


RETURN
GO
