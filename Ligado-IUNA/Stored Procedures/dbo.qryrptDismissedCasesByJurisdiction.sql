SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptDismissedCasesByJurisdiction]
(@Matter int,@BeginDate datetime, @EndDate datetime)

As
Set NoCount On


--Declare @beginDate datetime
--Declare @EndDate datetime
--Declare @Matter int
--Set @beginDate='1/1/1900'
--Set @EndDate='9/30/2014'
--Set @Matter = 1



--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter


Declare @DiseaseCount Table (Matter nvarchar(100), MatterID int, claimlawsuitstatus nvarchar (500),
	MoYearDismissed datetime, YearDismissed datetime,MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int, unknowncount1 int)
Insert into @DiseaseCount

Select Matter, C.MatterID,  claimlawsuitstatus,
 convert(datetime,(convert(nvarchar,Month(cst.statusDate))+'/1/'+ convert(nvarchar,Year(cst.StatusDate)))) as MoYearDismissed,
  convert(nvarchar,Year(cst.StatusDate)) as YearDismissed,
--Meso
Case	when DMeso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when DLC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when DOC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when DNM.HighLevelDiseaseID in (4,7) Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when DUnk.HighLevelDiseaseID in (5,6) Then 1
Else 0 End AS UnknownCount,
Case	when DUnk.HighLevelDiseaseID is NULL Then 1
Else 0 End AS UnknownCount1


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (Select CD.ClaimDiseaseID,CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
LEFT JOIN tblClaimSequence csq ON c.ClaimSequenceID = csq.ClaimSequenceID
--Meso
Left Join tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
Left Join tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
Left Join tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
Left Join tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Unknown
Left Join tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID


Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
left join tblClaimSettlement cs on cs.ClaimLawsuitID = CL.ClaimLawsuitID
left join tblClaimLawsuitStatusTracking cst on cst.ClaimLawsuitID=CL.claimlawsuitid
left join tblClaimLawsuitStatus css on css.ClaimLawsuitStatusID = cst.ClaimLawsuitStatusID


Where  COALESCE(isprimaryforclaim, 0) = 1   
AND COALESCE(IsPrimaryStatus, 0) = 1
and ClaimLawsuitStatusGroupID = 3
AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) and 
ProcessedDate between COALESCE(@BeginDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) and COALESCE(@EndDate, GETDATE())

 
	 Order By  Year(cst.StatusDate)

 


Select Matter, MatterID,  Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, Sum(OtherCancerCount) as OtherCancerCount,
	Sum(NonMaligCount) as NonMaligCount, Sum(UnknownCount+unknowncount1) as UnknownCount, @BeginDate as StartDate, @EndDate as EndDate,
MoYearDismissed, 		YearDismissed 
from @DiseaseCount
Group By Matter, MatterID,  YearDismissed,MoYearDismissed



Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissedCasesByJurisdiction] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDismissedCasesByJurisdiction] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissedCasesByJurisdiction] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptDismissedCasesByJurisdiction] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissedCasesByJurisdiction] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDismissedCasesByJurisdiction] TO [power_user]
GO
