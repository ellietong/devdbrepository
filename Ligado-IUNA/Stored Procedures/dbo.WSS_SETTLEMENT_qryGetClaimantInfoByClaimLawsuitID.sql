SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryGetClaimantInfoByClaimLawsuitID]
(@ClaimLawsuitID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--DECLARE @ClaimLawsuitID bigint = 267047

    DECLARE @RRFPath nvarchar(MAX)= (SELECT TOP 1 AmazonKey--d.DocumentURL
	FROM tblClaimLawsuit cl
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMultiClaimantDocument mcd ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	INNER JOIN tblDocuments d ON mcd.DocumentID = d.DocumentID
	WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	AND ClaimantDocumentTypeID = 32 --RRF
	AND mcd.ClaimLawsuitID = @ClaimLawsuitID
	ORDER BY DateCreated DESC)

	DECLARE @MMSEAPath nvarchar(MAX)= (SELECT TOP 1 AmazonKey-- d.DocumentURL
	FROM tblClaimLawsuit cl
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMultiClaimantDocument mcd ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	INNER JOIN tblDocuments d ON mcd.DocumentID = d.DocumentID
	WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	AND ClaimantDocumentTypeID = 52 --MMSEA (Medicare Exposure Documnet)
	AND mcd.ClaimLawsuitID = @ClaimLawsuitID
	ORDER BY DateCreated DESC)

	DECLARE @UnexecutedReleasePath nvarchar(MAX)= (SELECT TOP 1 AmazonKey--d.DocumentURL
	FROM tblClaimLawsuit cl
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblMultiClaimantDocument mcd ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	INNER JOIN tblDocuments d ON mcd.DocumentID = d.DocumentID
	WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	AND ClaimantDocumentTypeID = 271 --Unexecuted release
	AND CreatedBy LIKE '%kcicllc.com'
	ORDER BY DateCreated DESC)

	--DECLARE @RepresentationPath nvarchar(MAX)= (SELECT TOP 1 d.DocumentURL
	--FROM tblClaimLawsuit cl
	--INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	--INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	--INNER JOIN tblMultiClaimantDocument mcd ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	--INNER JOIN tblDocuments d ON mcd.DocumentID = d.DocumentID
	--WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	--AND DetailDocumentTypeID = 39 --Medicare Required Document (Rep of Ptiff & PtiffCounsel)
	--ORDER BY DateCreated DESC)
	
	SELECT	c.ClaimantPersonalInfoID, 
			@RRFPath AS RRFPath, 
			@MMSEAPath AS MMSEAPath, 
			@UnexecutedReleasePath AS UnexecutedReleasePath,
		--	@RepresentationPath AS RepresentationPath,
			cpi.LastName + ', ' + CASE WHEN cpi.FirstName IS NULL THEN '' ELSE cpi.FirstName + ' ' END + 
			CASE WHEN cpi.MiddleName IS NULL THEN '' ELSE cpi.MiddleName + ' ' END AS ClaimantName,
			CASE WHEN RP.LegalCapacityID IN (1,3,4,5,6,7,8,15,17,22) OR DeceasedDate IS NOT NULL THEN
			RP.FirstName + ' ' + COALESCE(RP.MiddleName,'') + ' ' + RP.LastName + ', for ' + CPI.FirstName + ' ' + COALESCE(CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' ' + Suffix,'') + ', deceased'
			ELSE CPI.FirstName + ' ' + COALESCE(CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' ' + Suffix,'') END AS MMSEAName,
			l.PlaintiffCounselAddressID,
			pca.PlaintiffCounselID
	FROM tblClaimLawsuit cl
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantRelatedParty rp ON c.ClaimantPersonalInfoID = rp.ClaimantPersonalInfoID
	LEFT JOIN tblSuffix s ON cpi.SuffixID = s.SuffixID
	LEFT JOIN tblClaimSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	
	WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryGetClaimantInfoByClaimLawsuitID] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryGetClaimantInfoByClaimLawsuitID] TO [Ligado_User]
GO
