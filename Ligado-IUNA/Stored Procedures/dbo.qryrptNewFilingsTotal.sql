SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptNewFilingsTotal] (@StartDate datetime, @EndDate datetime,@Matter int)

AS
SET NOCOUNT ON

--Declare @matter int
--set @matter = 1
--declare @startdate datetime
--set @startdate = '1/1/1900'
--declare @enddate datetime
--set @enddate = '9/30/2014'


----Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @DiseaseCount Table (ClaimID int, Matter nvarchar(100), MatterID int, YearFiled nvarchar(50), MoYearFiled nvarchar(50), MonthID int, MesoCount int, LungCancerCount int, 
OtherCancerCount int, NonMaligCount int, SilicosisCount int, UnknownCount int, unknowncount1 int)
Insert into @DiseaseCount

Select C.ClaimID, Matter, C.MatterID, Coalesce(year(servicedate), year(fileDate), year(statusdate)) as YearFiled, 
coalesce(DateName(month,servicedate), DateName(month,filedate), DateName(month,statusdate)) as MonthFiled,
coalesce(month(servicedate), month(filedate), month(statusdate)) as MonthID,
--Meso
Case	when DMeso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when DLC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when DOC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when DNM.HighLevelDiseaseID = 4 Then 1
Else 0 End as NonMaligCount,
--Silicosis
Case	when DNM.HighLevelDiseaseID = 7 Then 1
Else 0 End as Silicosis,
--Unknown
Case	when DUnk.HighLevelDiseaseID in (5,6) Then 1
Else 0 End AS UnknownCount,
Case	when DUnk.HighLevelDiseaseID is NULL Then 1
Else 0 End AS UnknownCount1


From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (Select CD.ClaimDiseaseID,CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
--Meso
Left Join tblDisease DMeso on CD.DiseaseID=DMeso.DiseaseID

--Lung Cancer
Left Join tblDisease DLC on CD.DiseaseID=DLC.DiseaseID

--Other Cancer
Left Join tblDisease DOC on CD.DiseaseID=DOC.DiseaseID

--Non-Malig
Left Join tblDisease DNM on CD.DiseaseID=DNM.DiseaseID

--Unknown
Left Join tblDisease DUnk on CD.DiseaseID=DUnk.DiseaseID

Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
LEFT JOIN tblClaimSequence csq on c.ClaimSequenceID = csq.ClaimSequenceID
left join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=CL.ClaimLawsuitID


Where   IsPrimaryForClaim = 1 and 
C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) 
	AND ProcessedDate between @StartDate and @EndDate 
	AND ClaimLawsuitStatusID = 22
Order By COALESCE(YEAR(serviceDate), YEAR(fileDate), YEAR(StatusDate))

 


Select Matter, MatterID, YearFiled AS 'Year', MoYearFiled as 'Month', MonthID, COUNT(ClaimID) AS NewFilings, @StartDate as StartDate, @EndDate as EndDate,
Sum(MesoCount) as MesoFilings, Sum(LungCancerCount) as LCFilings, Sum(OtherCancerCount) as OCFilings,
Sum(NonMaligCount) as NMFILINGS, SUM(SilicosisCount) AS SilicosisCount, Sum(UnknownCount+unknowncount1) as UnknownFilings
from @DiseaseCount DC
Where YearFiled>=1901
Group By Matter, MatterID, YearFiled, MoYearFiled, MonthID
ORDER BY Matter, YearFiled, MonthID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsTotal] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsTotal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsTotal] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsTotal] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsTotal] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsTotal] TO [power_user]
GO
