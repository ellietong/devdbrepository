SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryUpdateSettlementDueDates]
AS

IF OBJECT_ID('tempdb..#ClaimSettlementIDsToUpdate') IS NOT NULL DROP TABLE #ClaimSettlementIDsToUpdate
CREATE TABLE #ClaimSettlementIDsToUpdate (ClaimSettlementID BIGINT)
INSERT INTO #ClaimSettlementIDsToUpdate
	SELECT ClaimSettlementID
	FROM vSETTLEMENT_FullyDocumentedSettlementGroups fdsg
		INNER JOIN tblClaimSettlement cs ON cs.SettlementGroupID = fdsg.SettlementGroupID
	WHERE SettlementDueDate IS NULL --note: this will prevent previously fully documented settlements fron being pulled in again

UPDATE dbo.tblClaimSettlement
	SET SettlementDueDate = GETDATE()+60 --note: this is currently just being set to 60 days after the job is run - is there a date I should reference rather than GETDATE()?
	WHERE ClaimSettlementID IN(SELECT ClaimSettlementID FROM #ClaimSettlementIDsToUpdate)

GO
