SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qryfrmPaymentLegalBill] (@DefenseCounselAddressID int, @MatterID int)
AS
SET NOCOUNT ON

select lbi.InvoiceID, DefenseCounselAddressID, MatterID, lbi.LBInvoiceNumber, Period, 
	Case When AppealFlag = 1 Then AppealTotal Else AdjustedTotal End as FinalizedTotal, 
	(Case When AppealFlag = 1 Then AppealTotal Else AdjustedTotal End) - SUM(Coalesce(PaymentAmount,0)) as RemainingAmount
from m_tblLegalBillInvoices lbi
left join (select InvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentLegalBill lb on ca.CheckID = lb.CheckID)
			 a on a.InvoiceID = lbi.InvoiceID
where DefenseCounselAddressID = @DefenseCounselAddressID and MatterID = @MatterID and FundsRequestDate is not null
Group By lbi.InvoiceID, DefenseCounselAddressID, lbi.LBInvoiceNumber, Period,Case When AppealFlag = 1 Then AppealTotal Else AdjustedTotal End,
	 Case When AppealFlag = 1 Then AppealTotal Else AdjustedTotal End, MatterID
Having (Case When AppealFlag = 1 Then AppealTotal Else AdjustedTotal End) - SUM(Coalesce(PaymentAmount,0)) <> 0

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentLegalBill] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentLegalBill] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentLegalBill] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentLegalBill] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentLegalBill] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentLegalBill] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentLegalBill] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentLegalBill] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentLegalBill] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentLegalBill] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentLegalBill] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentLegalBill] TO [power_user]
GO
