SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptResolvedByState] (@StartDate datetime, @EndDate datetime, @Matter int)


AS
SET NOCOUNT ON


--Declare @StartDate date
--Declare @EndDate date
--Declare @Matter int
--Set @StartDate='7/1/2014'
--Set @EndDate='9/30/2014'
--Set @Matter = 1

--Find Max Matter ID
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState

Declare @DiseaseCount Table (ClaimID int, Matter nvarchar(100), MatterID int, FullNameState nvarchar(250), YearFiled datetime, 
Disease int, lastname nvarchar (100), firstname nvarchar(100), socialsecuritynumber nvarchar(250), filedate datetime, servicedate datetime,
localplaintiffcounsel nvarchar (500), jurisdiction nvarchar (200), docketnumber nvarchar (100), ClaimSequence nvarchar (200), LawsuitStatus nvarchar(200),settlementamount money, ResolutionDate datetime)
Insert into @DiseaseCount

Select C.ClaimID, Matter, C.MatterID, s.FullNameState, year(COALESCE(FileDate,ServiceDate,'1/1/1900')) ,
--Meso
Coalesce(hld.HighLevelDiseaseID,5) as Disease,
 CPI.LastName, CPI.FirstName, COALESCE(RIGHT(CPI.SocialSecurityNumber,4),'') as SSN, coalesce(L.FileDate,'1/1/1900') as filedate, coalesce(L.ServiceDate,'1/1/1900') as servicedate, pca.PlaintiffCounsel, j.Jurisdiction, DocketNumber,
ClaimSequence,case when cls.ClaimLawsuitStatusID in (15,27) then 'Dis W/O P'  when cls.ClaimLawsuitStatusID = 16 then 'Dis W/ P' when cls.ClaimLawsuitStatusID=17 then 'DBND'
when cls.ClaimLawsuitStatusID = 23 then 'Sett' when cls.ClaimLawsuitStatusID = 28 then 'Zero'  else cls.ClaimLawsuitStatus end, 
coalesce(SettlementAmount,0) as settlementamount, clst.statusdate

From tblClaimantPersonalInfo CPI
LEFT JOIN tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT JOIN (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
LEFT JOIN tblDisease D on CD.DiseaseID = D.DiseaseID
LEFT JOIN tblHighLevelDisease HLD on HLD.HighLevelDiseaseID=D.HighLevelDiseaseID
LEFT JOIN tblMatter M on c.MatterID = M.MatterID
LEFT JOIN tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
LEFT JOIN tblLawsuit L on CL.LawsuitID=L.LawsuitID
left join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID =l.PlaintiffCounselAddressID
LEFT JOIN tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
LEFT JOIN tblDefenseCounselState DCS on L.DefenseCounselAddressID=DCS.DefenseCounselAddressID AND L.StateID=DCS.StateID
LEFT JOIN tblState S on L.StateID=S.StateID
LEFT JOIN tblClaimSequence csq on csq.ClaimSequenceID = c.ClaimSequenceID
LEFT JOIN tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID = CL.ClaimLawsuitID
LEFT JOIN tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
LEFT JOIN tblClaimSettlement cst on cst.ClaimLawsuitID = cl.ClaimLawsuitID

Where s.StateID <= (@maxStateID) --and 
	AND IsPrimaryForClaim = 1 and IsPrimaryStatus=1
	AND C.MatterID >= COALESCE(@Matter,0) AND C.MatterID <= COALESCE(NULLIF(@Matter,0),@maxMatterID) 
	AND ClaimLawsuitStatusGroupID  in (2,3)
	AND ProcessedDate between COALESCE(@StartDate, '1/1/' + CONVERT(nvarchar(4), YEAR(GETDATE()))) and COALESCE(@EndDate, GETDATE())
Order By s.State, ProcessedDate, Jurisdiction, pca.PlaintiffCounsel



Select Matter, MatterID, FullNameState, YearFiled AS 'Year', hd.HighLevelDisease, LastName, FirstName, SocialSecurityNumber, FileDate, ServiceDate, localplaintiffcounsel, Jurisdiction, DocketNumber
,ClaimSequence,@startDate as StartDate, @EndDate as EndDate, LawsuitStatus, coalesce(SettlementAmount,0) as settlementamount, ResolutionDate
from @DiseaseCount DC
LEFT JOIN tblHighLevelDisease hd on hd.HighLevelDiseaseID = DC.Disease
--where resolutionDate between @StartDate and @EndDate 
Group By Matter, MatterID, FullNameState, hd.HighLevelDisease, YearFiled, LastName, FirstName, SocialSecurityNumber, FileDate, ServiceDate, localplaintiffcounsel, Jurisdiction, DocketNumber,ClaimSequence,LawsuitStatus,
ResolutionDate,coalesce(SettlementAmount,0)

ORDER BY  Matter, FullNameState, ResolutionDate, lastname, firstname, YearFiled

RETURN




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedByState] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptResolvedByState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedByState] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptResolvedByState] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptResolvedByState] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptResolvedByState] TO [power_user]
GO
