SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantSettlementInfoEntry]
as
 
Select *
From tblClaimSettlement cs
Order By SettlementDate desc
 
Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementInfoEntry] TO [power_user]
GO
