SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  Procedure [dbo].[qryrptARQCCostsComparedAllocation]
(@allocationid int = NULL)
As

SET NOCOUNT ON

--DECLARE @AllocationID INT
--SET @AllocationID = 150

/********** Not updated for In-Claim or Discounting features*************/

/* Select applicable occurrence IDs*/
DECLARE @tmpOccConsideredOccurrencesConsideredinAllocation TABLE (occurrenceid bigint, defenseconsidered money, costsconsidered money, allocationid int)

/* Insert costs cosidered into table*/
INSERT INTO @tmpOccConsideredOccurrencesConsideredinAllocation (occurrenceid, defenseconsidered, costsconsidered, allocationid)
(SELECT OccurrenceID, SUM(Cost1 + Cost2) AS 'DefenseConsidered', SUM(Cost3+Cost4+Cost5+Cost6+Cost7+Cost8+Cost9+Cost10+Cost11+Cost12) AS 'CostsConsidered', @allocationid as 'allocationid'
FROM vOccurrence 
WHERE OccurrenceID NOT IN
	(SELECT aoe.OccurrenceID 
	FROM vAllocation a INNER JOIN tblAllocationOccurrenceExclude aoe ON a.AllocationID = aoe.AllocationID 
	WHERE a.AllocationID = @allocationid) AND 
OccurrenceTypeID  IN 
	(SELECT aote.OccurrenceTypeID 
	FROM vAllocation a INNER JOIN tblAllocationOccurrenceTypeInclude aote ON a.AllocationID = aote.AllocationID 
	WHERE a.AllocationID = @allocationid) AND
OccurrenceGroupID IN
	(SELECT aoge.OccurrenceGroupID
	FROM vAllocation a INNER JOIN tblAllocationOccurrenceGroupInclude aoge ON a.AllocationID = aoge.AllocationID
	WHERE a.AllocationID = @allocationID)
GROUP BY OccurrenceID)

--create temporary table (qryARSumofAllocationandSIRByOccurrenceID)
DECLARE @qryARSumofAllocationandSIRByOccurrenceID TABLE
	(allocationid int,
	sumofallocation money,
	sumofallocationindemnity money,
	sumofallocationdefense money,
	sumofallocationdefenseoutsidelimits money,
	sumofsirallocated money,	
	occurrenceid bigint,
	runtime datetime
)

DECLARE @maxAllocationID int
SELECT @maxAllocationID = MAX(AllocationID) FROM vAllocation

-- append results of qryARSumofAllocationandSIRByOccurrenceID into temporary table
INSERT INTO @qryARSumofAllocationandSIRByOccurrenceID (allocationid, sumofallocation, sumofallocationindemnity, sumofallocationdefense, sumofallocationdefenseoutsidelimits, sumofsirallocated, 
	occurrenceid, runtime)
(SELECT AllocationID AS 'allocationid', SUM(Allocation) AS 'sumofallocation', SUM(AllocationIndemnity) AS 'sumofallocationindemnity', SUM(AllocationDefense) AS 'sumofallocationdefense',
	SUM(AllocationDefenseOutsideLimits) AS 'sumofallocationdefenseoutsidelimits', SUM(SIRAllocated) AS 'sumofsirallocated', OccurrenceID AS 'occurrenceid', MAX(DateTime) AS 'runtime'
FROM vAllocationResults
GROUP BY AllocationID, OccurrenceID)
	
SELECT a.AllocationID, a.AllocationDescription, o.OccurrenceName, 
	SUM(oc.costsconsidered + oc.defenseconsidered - (COALESCE(ar.sumofallocation, 0) + COALESCE(ar.sumofsirallocated, 0))) AS DiffConsideredAllocated, 
	SUM(oc.defenseconsidered) AS SumOfDefenseConsidered, SUM(oc.costsconsidered) AS SumOfCostsConsidered, COALESCE(ar.sumofallocation, 0) + 
	COALESCE(ar.sumofsirallocated, 0) AS TotalAllocated, SUM(COALESCE(ar.sumofallocation, 0)) AS Allocated, RunTime, SUM(COALESCE(ar.sumofsirallocated, 0)) AS SIRAllocated, 
	SUM(COALESCE(ar.sumofallocationindemnity, 0)) AS AllocatedIndemnity, SUM(COALESCE(ar.sumofallocationdefense, 0)) AS AllocatedDefense, 
	Sum(COALESCE(ar.sumofallocationdefenseoutsidelimits, 0)) AS AllocatedDefenseOutsideLimits
FROM ((vAllocation a INNER JOIN @tmpOccConsideredOccurrencesConsideredinAllocation oc ON a.AllocationID = oc.AllocationID) 
	LEFT JOIN @qryARSumofAllocationandSIRByOccurrenceID ar ON (oc.OccurrenceID = ar.OccurrenceID) AND (oc.AllocationID = ar.AllocationID)) 
	LEFT JOIN vOccurrence o ON oc.OccurrenceID = o.OccurrenceID
WHERE a.AllocationID >= COALESCE(@allocationID, 0) AND a.AllocationID <= COALESCE(NULLIF(@allocationID, 0), @maxAllocationID) 
GROUP BY a.AllocationID, a.AllocationDescription, o.OccurrenceName, COALESCE(ar.sumofallocation, 0) + COALESCE(ar.sumofsirallocated, 0), RunTime

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARQCCostsComparedAllocation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARQCCostsComparedAllocation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARQCCostsComparedAllocation] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptARQCCostsComparedAllocation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARQCCostsComparedAllocation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARQCCostsComparedAllocation] TO [power_user]
GO
