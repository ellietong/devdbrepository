SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimantDocumentsQC] (@ClaimantPersonalInfoID int)

as

--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 251312

Select cd.ClaimantDocumentID, 
ClaimantPersonalInfoID, 
ClaimantDocumentType,
coalesce(DocumentURL, documentlink) as DocumentLink, 'Internal/Historical' as DefenseCounsel,
'4/1/2010' AS DateAccepted,
'' as Comments, '' as DocumentSource
From tblClaimantDocuments cd Left Join tblCLaimantDocumentType dt on dt.claimantdocumenttypeID = cd.claimantdocumenttypeID
Inner join tblHistoricalMultiClaimantDocuments hmcd on hmcd.ClaimantDocumentID = cd.ClaimantDocumentID
Where hmcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID 

Union 

Select d.DocumentID as ClaimantDocumentID,
mcd.ClaimantPersonalInfoID, 
ClaimantDocumentType,
DocumentURL as DocumentLink, 
DefenseCounsel as DefenseCounsel,
DateAccepted, Comments, DocumentSource
from tblDocuments d
left join tblClaimantDocumentType dt on dt.ClaimantDocumentTypeID = d.DetailDocumentTypeID 
left join tblDefenseCounsel dc on dc.DefenseCounselID = d.FirmID 
left join tblMultiClaimantDocument mcd on mcd.DocumentID = d.DocumentID
left join tblDocumentSource ds on ds.DocumentSourceID=d.DocumentSourceID
where mcd.ClaimantPersonalInfoID = @claimantpersonalInfoID


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [power_user]
GO
