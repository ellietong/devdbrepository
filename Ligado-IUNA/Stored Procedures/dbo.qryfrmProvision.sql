SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmProvision]
(@policyID int = NULL)
AS
SET NOCOUNT ON

SELECT *,dbo.fnProvisionOrder(ProvisionLabelID) as ORDERBY
	FROM  tblProvision
	WHERE PolicyID = @policyID
	ORDER BY ORDERBY

RETURN




GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvision] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvision] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvision] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmProvision] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvision] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvision] TO [power_user]
GO
