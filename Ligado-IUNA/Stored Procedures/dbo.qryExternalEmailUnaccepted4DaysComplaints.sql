SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailUnaccepted4DaysComplaints]
AS

BEGIN
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
DECLARE @Subject    NVARCHAR(100) = 'IUNA/Nosroc: Unaccepted Complaints/Documents Over 4 Days Old'
DECLARE @LogoHeader NVARCHAR(2000)

DECLARE @4DaysOld DATETIME = CONVERT(DATETIME, CONVERT(NVARCHAR, CONVERT(DATE, GETDATE() - 4)) + ' 16:00:00.000')

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (DocTitle    NVARCHAR(500), 
								   FirmID      INT, 
								   Name        NVARCHAR(50), 
								   DockNum     NVARCHAR(50),
								   ServiceDate DATE, 
								   Matter      NVARCHAR(5),
								   [State]     NVARCHAR(5))
INSERT INTO @NewComplaintsTable
	SELECT d.DocumentTitle,
		l.DefenseCounselAddressID AS FirmID,
		cpi.LastName + ', ' + cpi.FirstName + ' (' + CONVERT(varchar(50), cpi.ClaimantPersonalInfoID) + ')' AS Name,
		DocketNumber,
		MAX(l.ServiceDate),
		Matter,
		s.[State]
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo  cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		INNER JOIN tblClaim                 c   ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit          cl  ON cl.ClaimID = c.ClaimID
		INNER JOIN tblLawsuit               l   ON l.LawsuitID = cl.LawsuitID
		INNER JOIN tblState                 s   ON s.StateID = l.StateID
		INNER JOIN tblAllegation            a   ON a.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblMatter                m   ON c.MatterID = m.MatterID
	WHERE d.DateCreated < @4DaysOld
		AND ClaimantDocumentTypeID IN (8, 4, 33)
		AND RIGHT(CreatedBy, 8) = 'kcic.com'
		AND l.DefenseCounselAddressID IS NOT NULL
		AND COALESCE(Accepted, 0) <> 1
		AND IsPrimaryForClaim = 1
		AND IsCurrentClaim = 1
	GROUP BY d.DocumentTitle,
		l.DefenseCounselAddressID,
		cpi.LastName + ', ' + cpi.FirstName + ' (' + CONVERT(varchar(50), cpi.ClaimantPersonalInfoID) + ')',
		DocketNumber,
		Matter,
		[State]

DECLARE @NewDocuments TABLE (docTitle NVARCHAR(100), 
							 FirmID   INT, 
							 Name     NVARCHAR(250),
							 Matter   NVARCHAR(100), 
							 DocType  NVARCHAR(150))
INSERT INTO @NewDocuments
	SELECT d.DocumentTitle,
		l.DefenseCounselAddressID AS FirmID,
		cpi.LastName + ', ' + cpi.FirstName + ' (' + CONVERT(varchar(50), cpi.ClaimantPersonalInfoID) + ')',
		Matter,
		ClaimantDocumentType
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo  cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaim                 c   ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit          cl  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit               l   ON l.LawsuitID = cl.LawsuitID
		INNER JOIN tblMatter                m   ON c.MatterID = m.MatterID
		LEFT  JOIN tblClaimantDocumentType  ct  ON ct.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
	WHERE datecreated < GETDATE() - 4
		AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
		AND d.ClaimantDocumentTypeID NOT IN (40, 41, 8, 4)
		AND l.DefenseCounselAddressID IS NOT NULL
		AND COALESCE(Accepted, 0) <> 1
		AND cl.IsPrimaryForClaim = 1
		AND IsCurrentClaim = 1
	GROUP BY d.DocumentTitle,
		l.DefenseCounselAddressID,
		cpi.LastName + ', ' + cpi.FirstName + ' (' + CONVERT(varchar(50), cpi.ClaimantPersonalInfoID) + ')',
		Matter,
		ClaimantDocumentType

--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.
DECLARE @FirmID  NVARCHAR(10), 
		@FirmURL NVARCHAR(100)

DECLARE firm_Curs CURSOR FOR 
	SELECT nct.FirmID,
		'https://ligado.kcic.com/Claims/Dashboard?ClientSiteID=2'
	FROM @NewComplaintsTable nct
	GROUP BY nct.FirmID 
	UNION 
	SELECT doc.firmid,
		'https://ligado.kcic.com/Claims/Dashboard?ClientSiteID=2'
	FROM @NewDocuments doc
	GROUP BY doc.firmid

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @firmid, @firmURL

WHILE @@FETCH_STATUS = 0
BEGIN
	--Declare and populate the body of the email for individual firms
	DECLARE @BodyMessage nvarchar (MAX)
	
	--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
	--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
	SET @BodyMessage = @LogoHeader +
		'<STYLE TYPE="text/css">
		<!--
		TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
		TD    {font-family: Goudy Old Style; 
			  font-size: 10pt; 
			  text-align: center;
			  border: 1px solid rgb(0,121,193);
			  padding: .3em;
			  color: #000000}
		TH    {border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
		--->
		</STYLE>'

	DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

	--Declare a cursor and variables to be used in the cursor to create a separate table of complaints for each matter
	DECLARE @NumberPerMatter INT,
			@Matter          NVARCHAR(20),
			@Type            NVARCHAR(20)

	DECLARE matterDocs_Curs CURSOR FOR 
		SELECT COUNT(DocTitle),
			Matter,
			'Complaint(s): '
		FROM @NewComplaintsTable
		WHERE FirmID = @FirmID
		GROUP BY Matter 
		UNION 
		SELECT COUNT(DocTitle),
			Matter,
			'Document(s): '
		FROM @NewDocuments
		WHERE FirmID = @FirmID
		GROUP BY Matter
		ORDER BY Matter

	OPEN matterDocs_curs
	FETCH NEXT FROM matterDocs_curs INTO @NumberPerMatter, @Matter, @Type

	WHILE @@fetch_status = 0
	BEGIN
		IF (SELECT COUNT(docTitle) FROM @NewComplaintsTable WHERE FirmID = @FirmID) = 0
		BEGIN
			SET @BodyMessage = @BodyMessage + '<H4>There are currently no outstanding complaints. </H4>'
		END

		--Header line for each matter's table
		SET @BodyMessage = @BodyMessage + '<br>' + CONVERT(nvarchar(5), (SELECT @NumberPerMatter)) + ' ' + (SELECT @Matter) + ' ' + @Type

		--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
		IF @Type = 'Complaint(s): '
		BEGIN
			SET @BodyMessage = @BodyMessage + '<H4>The following complaints were uploaded more than 4 days ago and have not yet been accepted:  </H4>'

			SET @BodyMessage = @BodyMessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0">' +
				N'<tr><th style = "border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style = "border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Name</th><th style = "border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Civil Action Number</th>' +
				'<th style = "border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Service Date</th>' + '<th style = "border: 1px solid #ffffff; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">State</th>' +
				CAST((SELECT @TDStyle AS 'td/@style', (SELECT DocTitle)	AS td, '',
							 @TDStyle AS 'td/@style', (SELECT Name)     AS td, '',
							 @TDStyle AS 'td/@style', (SELECT DockNum)  AS td, '',
							 @TDStyle AS 'td/@style', (SELECT CONVERT(VARCHAR, ServiceDate, 101)) AS td, '',
							 @TDStyle AS 'td/@style', (SELECT [State])  AS td, ''
					  FROM @NewComplaintsTable c
					  WHERE c.Matter = @Matter
						  AND c.FirmID = @FirmID
					  ORDER BY serviceDate, [state], Name
					  FOR xml PATH ('tr'), TYPE) AS nvarchar(MAX)) +
				N'</table>' + N'<br/>'

			IF (SELECT COUNT(docTitle) FROM @NewDocuments WHERE FirmID = @FirmID) = 0
			BEGIN
				SET @BodyMessage = @BodyMessage + '<H4>There are currently no other documents awaiting acceptance. </H4>'
			END
		END
		ELSE
		BEGIN
			SET @BodyMessage = @BodyMessage + '<H4>The following documents were uploaded more than 4 days ago and have not yet been accepted:  </H4>'

			SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
				N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Name</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th>' +
				CAST((SELECT @TDStyle AS 'td/@style', (SELECT DocTitle) AS td, '',
							 @TDStyle AS 'td/@style', (SELECT Name)     AS td, '',
							 @TDStyle AS 'td/@style', (SELECT DocType)  AS td, ''
					  FROM @NewDocuments C
					  WHERE c.Matter = @Matter
						  AND c.FirmID = @FirmID
					  ORDER BY DocType, Name
					  FOR xml PATH ('tr'), TYPE) AS nvarchar(MAX)) +
				N'</table>' + N'<br/>'
		END

		--Repeat for next matter
		FETCH NEXT FROM matterDocs_curs INTO @NumberPerMatter, @Matter, @Type
	END

	CLOSE matterDocs_curs
	DEALLOCATE matterDocs_curs

	--Link for document library
	SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

	--KCIC address and disclaimer
	SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC <br>
		733 10th Street, NW, Suite 3001<br>
		Washington, DC 20001<br>
		(t) 202-772-2300<br>
		(f) 202-772-2333'

	DECLARE @RoleID int = 57 --Complaint 4 day reminder (TO)
	DECLARE @RecipientList nvarchar (500)
	SET @RecipientList = (SELECT Email + '; ' AS [text()]
						  FROM [Ligado-ExtranetSecurity].dbo.vPersonRoles pr
						      INNER JOIN [Ligado-ExtranetSecurity].dbo.Person p ON pr.PersonID = p.PersonID
						  WHERE RoleID = @RoleID 
						      AND pr.PersonID IN (SELECT PersonID
											      FROM [Ligado-ExtranetSecurity].dbo.PersonFirmAddressClientSite     pfacs
												      INNER JOIN [Ligado-ExtranetSecurity].dbo.FirmAddressClientSite facs ON pfacs.FirmAddressClientSiteID = facs.FirmAddressClientSiteID
												      INNER JOIN [Ligado-ExtranetSecurity].dbo.FirmAddress           fa ON facs.FirmAddressID = fa.FirmAddressID
											      WHERE ClientSiteID = 2
												      AND pfacs.Active = 1
												      AND facs.Active = 1
											          AND fa.FirmAddressID = @FirmID)
							  AND ClientSiteID = dbo.fn_GetClientSiteID() FOR XML PATH(''))

	--Execute stored procedure to send the email to that firm
	EXEC msdb.dbo.sp_send_dbmail
		@profile_name = 'Claims',
		@recipients = @RecipientList,
		@blind_copy_recipients = 'iuna_nosroc@kcic.com',
		@subject = @subject,
		@body = @BodyMessage,
		@body_format = 'html'

	--Move to the next firm to create a new email
	FETCH NEXT FROM firm_curs INTO @FirmID, @firmurl
END

CLOSE firm_curs
DEALLOCATE firm_curs

END

GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailUnaccepted4DaysComplaints] TO [power_user]
GO
