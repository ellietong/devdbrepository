SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmClaimantName-SelectLetter]
as 

Set NoCount ON

select LEFT(LastName, 1) as Letter
from tblClaimantPersonalInfo cpi
group by LEFT(LastName, 1)
order by LEFT(LastName, 1)

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantName-SelectLetter] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantName-SelectLetter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantName-SelectLetter] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantName-SelectLetter] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantName-SelectLetter] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantName-SelectLetter] TO [power_user]
GO
