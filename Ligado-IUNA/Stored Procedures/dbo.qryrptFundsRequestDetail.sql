SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptFundsRequestDetail]

As

SET NOCOUNT ON

SELECT	Firm, PayTo, InvoiceNumber, Period, InvoiceTotal,SubmitDate,DatePayable, FeeAmount,
	DisbursementAmount, Adjustment, DI.StatusID, Status,
CASE	When (FeeAmount is null AND DisbursementAmount is null) or (FeeAmount+DisbursementAmount=0) Then InvoiceTotal
	Else FeeAmount+DisbursementAmount
	End as Subtotal,
CASE	When (FeeAmount is null AND DisbursementAmount is null) or (FeeAmount+DisbursementAmount=0) Then InvoiceTotal
	Else FeeAmount+DisbursementAmount-Adjustment
	End as AmountPayable
FROM	tblDefenseInvoice DI
INNER JOIN	tblDefenseInvoiceFirm DIF ON DI.FirmID = DIF.FirmID
INNER JOIN	tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
WHERE	((DI.CheckNumber IS NULL) AND DI.DatePayable <= (SELECT CONVERT(datetime, CONVERT(varchar, GETDATE(), 101), 101) + 30 AS OneMonthOut)
	AND (DIS.StatusID IN (1, 2)) AND (DI.SubmitDate IS NOT NULL) and FundsRequestDate is Null)
ORDER BY Firm, Status, DatePayable

/* 
-- RUN THIS AFTER SENDING DEFENSE INVOICE
BEGIN TRAN

Update mBtblDefenseInvoice
Set FundsRequestDate=CONVERT(datetime, CONVERT(varchar, GETDATE(), 101))
WHERE	(CheckNumber IS NULL) AND DatePayable <= (SELECT CONVERT(datetime, CONVERT(varchar, GETDATE(), 101), 101) + 30 AS OneMonthOut)
	AND (StatusID IN (1, 2)) AND (SubmitDate IS NOT NULL) and FundsRequestDate is null
ROLLBACK TRAN
COMMIT TRAN
*/

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequestDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequestDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequestDetail] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequestDetail] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequestDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequestDetail] TO [power_user]
GO
