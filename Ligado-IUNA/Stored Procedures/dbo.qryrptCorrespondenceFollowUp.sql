SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  procedure [dbo].[qryrptCorrespondenceFollowUp]
(@siteid int = null)
as
set nocount on

--DECLARE @SiteID int
--SET @SiteID=0


DECLARE @MAXsiteid int

SELECT @MAXsiteid = MAX(siteid) FROM tblEnviroSitesDetail

select cor.correspondenceid, correspondencedate, cor.correspondencetypeid, ct.correspondencetype, 
	cor.correspondencemodeid, cm.correspondencemode, notes, attachments, confirmreceipt, f.followup,
	t.taskcategory, c.firstname + ' ' + c.lastname AS followupname, esd.sitename
from tblcorrespondence cor 
	left join tblcorrespondencemode cm on cor.correspondencemodeid = cm.correspondencemodeid
	left join tblcorrespondencetype ct on cor.correspondencetypeid = ct.correspondencetypeid
	left join tblfollowup f on cor.correspondenceid = f.correspondenceid
	left join tbltaskcategorylookup t on f.taskcategoryid = t.taskcategoryid
	left join tblinternalcontact i on f.internalcontactid = i.internalcontactid
	left join tblContact c on i.ContactID=c.ContactID
	inner join tblsitenoticed sn on cor.correspondenceid = sn.correspondenceid
	inner join tblEnviroSitesDetail esd on sn.siteid = esd.siteid
where completed = 0 
	and esd.siteID >= COALESCE(@siteID, 0) AND esd.SiteID <= COALESCE(NULLIF(@siteID, 0), @maxsiteID)
order by correspondencedate
return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorrespondenceFollowUp] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCorrespondenceFollowUp] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorrespondenceFollowUp] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCorrespondenceFollowUp] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCorrespondenceFollowUp] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCorrespondenceFollowUp] TO [power_user]
GO
