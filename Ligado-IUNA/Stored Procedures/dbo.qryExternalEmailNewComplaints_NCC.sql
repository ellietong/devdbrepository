SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailNewComplaints_NCC]
AS

BEGIN
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
DECLARE @Subject    NVARCHAR (100) = 'IUNA/Nosroc: New Complaints Uploaded ' + CONVERT(varchar, GETDATE(), 107)
DECLARE @LogoHeader NVARCHAR (2000)

DECLARE @Today4PM     DATETIME = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')
DECLARE @Yesterday4PM DATETIME = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (DocID                  INT, 
								   DocTitle               NVARCHAR(500), 
								   FirmID                 INT, 
								   LName                  NVARCHAR(50), 
								   FName                  NVARCHAR(50),
								   ClaimantPersonalInfoID BIGINT, 
								   ServiceDate            DATE, 
								   [State]                NVARCHAR(5))
INSERT INTO @NewComplaintsTable
	SELECT d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		l.DefenseCounselAddressID AS FirmID,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		MAX(l.ServiceDate),
		s.[State]
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo  cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		LEFT  JOIN tblClaim                 c   ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT  JOIN tblClaimLawsuit          cl  ON cl.ClaimID = c.ClaimID
		LEFT  JOIN tblLawsuit               l   ON l.LawsuitID = cl.LawsuitID
		LEFT  JOIN tblState                 s   ON s.StateID = l.StateID
		LEFT  JOIN tblAllegation            a   ON a.ClaimLawsuitID = cl.ClaimLawsuitID
	WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
		AND ClaimantDocumentTypeID IN (8, 4, 33)
		AND RIGHT(CreatedBy, 8) = 'kcic.com'
		AND l.DefenseCounselAddressID IS NOT NULL
		AND (c.MatterID <> 4 OR a.AllegationTypeID IN (2, 3))
		AND IsPrimaryForClaim = 1
		AND IsCurrentClaim = 1
	GROUP BY d.documentID,
		d.DocumentTitle,
		l.DefenseCounselAddressID,
		LastName,
		FirstName,
		cpi.ClaimantPersonalInfoID,
		s.[State]

DECLARE @ClaimantPerDoc TABLE (DocID INT, ClaimantList NVARCHAR(MAX))
DECLARE @DocID        INT, 
		@LastDocID    INT, 
		@Claimant     NVARCHAR(MAX), 
		@ClaimantID   BIGINT, 
		@ClaimantList NVARCHAR(MAX)

DECLARE Cur_Complaints CURSOR LOCAL FAST_FORWARD FOR 
	SELECT DocID,
		COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
		ClaimantPersonalInfoID
	FROM @NewComplaintsTable

OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID

WHILE @@FETCH_STATUS = 0
BEGIN
	IF (LEN (@ClaimantList) = 0 OR @ClaimantList IS NULL)
	BEGIN
		SET @ClaimantList = '<ul> <a href="https://www.kcicextranet.com/clients/iuna_nosroc/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + @Claimant + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
	END
	ELSE
	BEGIN
		SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="https://www.kcicextranet.com/clients/iuna_nosroc/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + COALESCE(@Claimant, '') + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
	END

	SET @LastDocID = @DocID
	FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID

	IF @DocID <> @LastDocID OR @@FETCH_STATUS <> 0
	BEGIN

	INSERT INTO @ClaimantPerDoc
		SELECT @LastDocID,
			@ClaimantList

	SET @ClaimantList = NULL
	END
END

CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar (MAX)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	
 
 
--->
</STYLE>'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
IF (SELECT COUNT(docTitle) FROM @NewComplaintsTable) > 0
BEGIN
	SET @BodyMessage = @BodyMessage + '<H4>The following complaints have been uploaded in the last 24 hours: </H4>'
	SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
		N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants (ClaimantID)</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Served*</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">State</th>' +
		CAST((SELECT
				'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;">' + DocTitle                              + '</td>', '',
				'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;">' + cl.ClaimantList                       + '</td>', '',
				'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;">' + CAST(MAX(ServiceDate) AS varchar(50)) + '</td>', '',
				'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;">' + [State]                               + '</td>', ''
			  FROM @NewComplaintsTable C
				  INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
			  GROUP BY DocTitle,
				  cl.ClaimantList,
				  [State]
			  FOR XML PATH ('tr'), TYPE) AS NVARCHAR(MAX)) +
		N'</table>' + N'<br/>'
END
ELSE
BEGIN
	SET @BodyMessage = @BodyMessage + '<H4>No Complaints have been uploaded in the last 24 hours. </H4>'
END

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Documents.aspx">here</A> to access the document(s).</b><br><br>'

--Service Date Assumption 
IF (SELECT COUNT(docTitle) FROM @NewComplaintsTable) > 0
BEGIN
	SET @BodyMessage = @BodyMessage + '<br>*Note: Date served is the latest service date for claimants'' lawsuits.<br><br>'
END

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC <br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

DECLARE @RoleID int = 56 --Complaint Summary (TO)
DECLARE @RecipientList nvarchar (500)
SET @RecipientList = (SELECT Email + '; ' AS [text()]
					  FROM [Ligado-ExtranetSecurity].dbo.vPersonRoles pr
						  INNER JOIN [Ligado-ExtranetSecurity].dbo.Person p ON pr.PersonID = p.PersonID
					  WHERE RoleID = @RoleID
						  AND ClientSiteID = dbo.fn_GetClientSiteID() FOR XML PATH(''))

--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail --The IUNA Profile sends from iuna_nosroc@kcic.com
	@profile_name          = 'Claims',
	@recipients            = @RecipientList,
	@blind_copy_recipients = 'iuna_nosroc@kcic.com',
	@subject               = @subject,
	@body                  = @BodyMessage,
	@body_format           = 'html'

END

GO
