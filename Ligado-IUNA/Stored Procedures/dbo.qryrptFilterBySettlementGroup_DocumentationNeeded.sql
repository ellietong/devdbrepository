SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptFilterBySettlementGroup_DocumentationNeeded] (@SettlementGroupID BIGINT)
AS

SET NOCOUNT ON

--declare @SettlementGroupID bigint
--set @SettlementGroupID =6749

SELECT LastName,
	FirstName,
	cpi.ClaimantPersonalInfoID, 
	DocketNumber, 
	LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4)AS SSN, 
	CASE WHEN DeceasedDate IS NOT NULL AND (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
		 THEN 'Complete'
		 WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END AS SupportingDocumentation,
	csd.ReleaseTemplateTypeID, 
	ReleaseTemplateType, 
	cl.ClaimLawsuitID, 
	HighLevelDisease  
FROM tblClaimantPersonalInfo cpi
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
	LEFT JOIN (SELECT * FROM tblClaimDisease WHERE PrimaryDisease = 1) cd ON cd.ClaimID = c.ClaimID
	LEFT JOIN tblDisease d ON d.DiseaseID = cd.DiseaseID
	LEFT JOIN tblHighLevelDisease hd ON hd.HighLevelDiseaseID = d.HighLevelDiseaseID
	LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
	LEFT JOIN tblClaimSettlement cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaimSettlementGroup csg ON csg.SettlementGroupID = cs.SettlementGroupID
	LEFT JOIN tblClaimSettlementDocumentation csd ON csd.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblReleaseTemplateType rtt ON rtt.ReleaseTemplateTypeID = csd.ReleaseTemplateTypeID
WHERE cs.SettlementGroupID=@SettlementGroupID 
	AND (CASE WHEN DeceasedDate IS NOT NULL AND (COALESCE(SettlementConfirmation,0) +COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(DODCert,0) + COALESCE(EstatePapers,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 7
			  THEN 'Complete'
			  WHEN DeceasedDate IS NULL AND (COALESCE(SettlementConfirmation,0) + COALESCE(ClaimantIDed,0) + COALESCE(DxMedicals,0) + COALESCE(ProductID,0) + COALESCE(EmployExpoHist,0)) = 5
			  THEN 'Complete'
			  ELSE 'Documentation Needed' END) = 'Documentation Needed'
GROUP BY LastName,cpi.ClaimantPersonalInfoID, FirstName, HighLevelDisease, csd.ReleaseTemplateTypeID, ReleaseTemplateType, cl.ClaimLawsuitID, DocketNumber, LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4),SettlementConfirmation, ClaimantIDed,DxMedicals,DODCert,EstatePapers,ProductID,EmployExpoHist,DeceasedDate

RETURN

GO
