SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryInternalNewComplaintsPast5DayAcceptanceEmail]
AS
BEGIN

/*Query run daily to send an internal email listing any Complaints which are over 5
days old and have not yet been accepted. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (max)

SET @Subject = 'IUNA/NOSROC: Unaccepted Complaints or Documents Over 5 Days Old'
--Set body font, insert KCIC logo/banner at the top of the email, and set table styles. 
--These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
--KCIC logo is being pulled from the KCIC site.  Contact D. Snyder with questions or requests.   
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>The following firms have unaccepted complaints or documents over 5 days old: </H4>
<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--Create temp table to hold all complaints over 5 days old which have not been accepted
--DocTitle and firmAbb are found by parsing the documentURL
DECLARE @NewComplaintsTable TABLE (docTitle nvarchar (MAX), state nvarchar (50), FirmID int, lName nvarchar (150), fname nvarchar (150), dockNum nvarchar (150), servicedate datetime, Matter nvarchar (50))
INSERT INTO @NewComplaintsTable
	SELECT DocumentTitle,
		s.[State],
		DefenseCounselID AS FirmID,
		LastName,
		FirstName,
		DocketNumber,
		ServiceDate,
		Matter
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.lawsuitid
		INNER JOIN tblState s ON s.StateID = l.stateid
		INNER JOIN tblMatter m ON c.MatterID = m.MatterID
		INNER JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	WHERE DateCreated < GETDATE() - 5
		AND ClaimantDocumentTypeID IN (8, 4)
		AND COALESCE(Accepted, 0) <> 1
		AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	GROUP BY DocumentTitle,
		s.[State],
		DefenseCounselID,
		LastName,
		FirstName,
		DocketNumber,
		ServiceDate,
		Matter

--Create temp table to hold all complaints over 5 days old which have not been accepted
--DocTitle and firmAbb are found by parsing the documentURL
DECLARE @NewDocuments TABLE (docTitle nvarchar (MAX), state nvarchar (50), FirmID int, lName nvarchar (150), fname nvarchar (150), dockNum nvarchar (150), servicedate datetime, Matter nvarchar (50))
INSERT INTO @NewDocuments
	SELECT DocumentTitle,
		s.[State],
		DefenseCounselID AS FirmID,
		LastName,
		FirstName,
		DocketNumber,
		ServiceDate,
		Matter
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l ON cl.LawsuitID = l.lawsuitid
		INNER JOIN tblState s ON s.StateID = l.stateid
		INNER JOIN tblMatter m ON c.MatterID = m.MatterID
		INNER JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
	WHERE datecreated < GETDATE() - 5
		AND ClaimantDocumentTypeID NOT IN (40, 41, 8, 4)
		AND COALESCE(Accepted, 0) <> 1
		AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	GROUP BY DocumentTitle,
		s.[State],
		DefenseCounselID,
		LastName,
		FirstName,
		DocketNumber,
		ServiceDate,
		Matter

--Declare variables to be used in the firm cursor to list documents by firm
DECLARE @firmID int, @firmURL nvarchar (100), @firmname nvarchar (100), @docCount int

DECLARE firm_Curs CURSOR FOR SELECT
	nct.firmID,
	--Update document library url if documents in Heyl Royster's Peoria library.
	'https://admin.kcicextranet.com/cp/iuna_nosroccp/Defense%20Counsel%20Documents/Forms/AllItems.aspx',
	DefenseCounsel,

	COUNT(doctitle)
FROM tblDefenseCounsel dc
RIGHT JOIN @NewComplaintsTable nct ON nct.firmID = dc.DefenseCounselID
INNER JOIN vtblFirmCase fc ON fc.FirmID = nct.FirmID
GROUP BY	nct.firmID,
			DefenseCounsel

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @firmID, @firmurl, @firmname, @docCount

WHILE @@FETCH_STATUS = 0

BEGIN
--Header for each firm with link to the document library
SET @BodyMessage = @BodyMessage + '<br>' + CONVERT(nvarchar(5), (SELECT
	@docCount)
) + ' Unaccepted Complaint(s) in the <A HREF = "' + (SELECT
	@firmURL)
+ '"> ' + (SELECT
	@firmName)
+ '</A> Document Library:'
--List documents in a table               
SET @BodyMessage = @bodymessage +
N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
--Set column headings and populate cells from the cursor
N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Last Name</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant First Name</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Civil Action Number</th>' +
'<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Service Date</th>' + '<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		docTitle)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		lname)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		fname)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		dockNum)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(varchar, servicedate, 101))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		Matter)
	AS td,
	''
FROM @NewComplaintsTable C
WHERE c.firmID = (SELECT
	@firmID)
ORDER BY serviceDate, lname
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'

--Move to next firm                 
FETCH NEXT FROM firm_curs INTO @firmID, @firmurl, @firmname, @doccount
END
CLOSE firm_curs
DEALLOCATE firm_curs




--Declare variables to be used in the firm cursor to list documents by firm
DECLARE @firmID2 int, @firmURL2 nvarchar (100), @firmname2 nvarchar (100), @docCount2 int

DECLARE firm_Curs2 CURSOR FOR SELECT
	nct.firmID,
	--Update document library url if documents in Heyl Royster's Peoria library.
	'https://admin.kcicextranet.com/cp/iuna_nosroccp/Defense%20Counsel%20Documents/Forms/AllItems.aspx',
	DefenseCounsel,
	COUNT(doctitle)
FROM tblDefenseCounsel dc
RIGHT JOIN @NewDocuments nct ON nct.firmID = dc.DefenseCounselID
INNER JOIN vtblFirmCase fc ON fc.FirmID = nct.FirmID
GROUP BY	nct.firmID,
			DefenseCounsel

OPEN firm_curs2
FETCH NEXT FROM firm_curs2 INTO @firmID2, @firmurl2, @firmname2, @docCount2

WHILE @@FETCH_STATUS = 0

BEGIN
--Header for each firm with link to the document library
SET @BodyMessage = @BodyMessage + '<br>' + CONVERT(nvarchar(5), (SELECT
	@docCount2)
) + ' Unaccepted Document(s) in the <A HREF = "' + (SELECT
	@firmURL2)
+ '"> ' + (SELECT
	@firmName2)
+ '</A> Document Library:'
--List documents in a table               
SET @BodyMessage = @bodymessage +
N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
--Set column headings and populate cells from the cursor
N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant Last Name</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant First Name</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Civil Action Number</th>' +
'<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Service Date</th>' + '<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		docTitle)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		lname)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		fname)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		dockNum)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(varchar, servicedate, 101))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		Matter)
	AS td,
	''
FROM @NewDocuments C
WHERE c.firmID = (SELECT
	@firmID2)
ORDER BY serviceDate, lname
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'

--Move to next firm                 
FETCH NEXT FROM firm_curs2 INTO @firmID2, @firmurl2, @firmname2, @doccount2
END
CLOSE firm_curs2
DEALLOCATE firm_curs2


SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--Check if the email needs to be sent
IF ((SELECT
	COUNT(DISTINCT docTitle)
FROM @NewComplaintsTable) < > 0 OR (SELECT
	COUNT(DISTINCT docTitle)
FROM @NewDocuments)
<> 0) BEGIN
--Send email
EXEC msdb.dbo.sp_send_dbmail	@profile_name = 'Claims',
								@recipients = 'iuna_nosroc@kcic.com',
								--@recipients = 'amanp@kcic.com',
								@subject = @subject,
								@body = @BodyMessage,
								@body_format = 'html'
END

END



GO
