SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptFilterBySettlementGroup]
(@SettlementGroupID bigint)
As
Set NoCount On

--declare @SettlementGroupID bigint
--set @SettlementGroupID =6749

Update tblClaimSettlement 
	SET ReleaseTemplateTypeID = Case When csd.ReleaseTemplateTypeID is null then dbo.fnDetermineReleaseTemplateType(cpi.ClaimantPersonalInfoID) else csd.ReleaseTemplateTypeID end
From tblClaimantPersonalInfo cpi 
	left join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID 
	left join tblClaimSettlementDocumentation csd on csd.ClaimLawsuitID = cl.ClaimLawsuitID
Where csd.ClaimLawsuitID in (Select ClaimLawsuitID From tblClaimSettlement Where SettlementGroupID = @SettlementGroupID)

Select LastName,
	FirstName,
	cpi.ClaimantPersonalInfoID, 
	DocketNumber, 
	LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4)as SSN, 
	CASE WHEN DeceasedDate IS NOT NULL and (COALESCE(csd.SettlementConfirmation,0) +COALESCE(csd.ClaimantIDed,0) + COALESCE(csd.DxMedicals,0) + COALESCE(csd.DODCert,0) + COALESCE(csd.EstatePapers,0) + COALESCE(csd.ProductID,0) + COALESCE(csd.EmployExpoHist,0)) = 7
		 THEN 'Complete'
		 WHEN DeceasedDate IS NULL AND (COALESCE(csd.SettlementConfirmation,0) + COALESCE(csd.ClaimantIDed,0) + COALESCE(csd.DxMedicals,0) + COALESCE(csd.ProductID,0) + COALESCE(csd.EmployExpoHist,0)) = 5
		 THEN 'Complete'
		 ELSE 'Documentation Needed' END AS SupportingDocumentation,
	csd.ReleaseTemplateTypeID, 
	ReleaseTemplateType, 
	cl.ClaimLawsuitID, 
	HighLevelDisease, 
	SettlementGroup   
from tblClaimantPersonalInfo cpi
	left join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
	left join (Select * From tblClaimDisease Where PrimaryDisease = 1) cd on cd.ClaimID = c.ClaimID
	left join tblDisease d on d.DiseaseID = cd.DiseaseID
	left join tblHighLevelDisease hd on hd.HighLevelDiseaseID = d.HighLevelDiseaseID
	left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	left join tblClaimSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblClaimSettlementGroup csg on csg.SettlementGroupID = cs.SettlementGroupID
	left join tblClaimSettlementDocumentation csd on csd.ClaimLawsuitID = cl.ClaimLawsuitID
	left join tblReleaseTemplateType rtt on rtt.ReleaseTemplateTypeID = csd.ReleaseTemplateTypeID
WHERE cs.SettlementGroupID=@SettlementGroupID
GROUP BY LastName,cpi.ClaimantPersonalInfoID, FirstName, HighLevelDisease, SettlementGroup, csd.ReleaseTemplateTypeID, ReleaseTemplateType, cl.ClaimLawsuitID, 
	DocketNumber, LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4), 
	csd.SettlementConfirmation, csd.ClaimantIDed,csd.DxMedicals,csd.DODCert, csd.EstatePapers,csd.ProductID,csd.EmployExpoHist,DeceasedDate

RETURN
GO
