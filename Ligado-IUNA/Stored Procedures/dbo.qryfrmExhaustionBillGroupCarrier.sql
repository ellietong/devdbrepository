SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmExhaustionBillGroupCarrier]
(@strBillGroup nvarchar(2000))
AS

SET NOCOUNT ON

DECLARE @strSQL nvarchar(4000)

SET @strSQL = '
	SELECT CarrierName 
	FROM tblBillGroupCarriers LEFT JOIN tblCarrier ON tblBillGroupCarriers.CarrierID = tblCarrier.CarrierID
	WHERE BillGroupID IN ( ' + @strBillGroup + ' )'

EXEC sp_executeSQL @strSQL

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustionBillGroupCarrier] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustionBillGroupCarrier] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustionBillGroupCarrier] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustionBillGroupCarrier] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExhaustionBillGroupCarrier] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmExhaustionBillGroupCarrier] TO [power_user]
GO
