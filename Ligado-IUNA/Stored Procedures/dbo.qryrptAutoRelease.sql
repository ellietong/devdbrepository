SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptAutoRelease] (@ClaimantPersonalInfoID bigint, @ClaimLawsuitID bigint)
AS

Set NoCount On

--DECLARE @Claimantpersonalinfoid bigint
--DECLARE @ClaimLawsuitID bigint
--set @Claimantpersonalinfoid = 48611
----set @Claimantpersonalinfoid = 21417 --two claims for one claimant
--Set @ClaimLawsuitID = 49660

-- Find Spouse for Claimant
Declare @Spouse nvarchar(100)
Declare @SpouseCapacity nvarchar(100)
Declare @SpouseInfo table (Spouse nvarchar(100), SpouseCapacity nvarchar(100))
Insert Into @SpouseInfo 
	SELECT Distinct crp.FirstName + ' ' + Coalesce(crp.MiddleName + ' ','') + crp.LastName as Spouse, 
		COALESCE(lc.LegalCapacity, '[CAPACITY]') as SpouseCapacity
	FROM tblClaimantPersonalInfo cpi
		inner join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
		left join tblClaimDisease cd on cd.ClaimID = c.ClaimID
		left join tblDisease d on d.DiseaseID = cd.DiseaseID
		left join tblHighLevelDisease dhld on dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
		left join tblClaimSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		left join tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		left join tblDefendantCompany dc on dc.DefendantCompanyID = a.DefendantCompanyID
		left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
		left join tblState laws on laws.StateID = l.StateID
		left join tblClaimantRelatedParty crp on crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		left join tblLegalCapacity lc on lc.LegalCapacityID = crp.LegalCapacityID
		left join tblRelationship r on r.RelationshipID = crp.RelationshipID
		left join tblClaimantEmployment ce on ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
		left join tblPlaintiffCounselAddress pc on pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		left join tblCourtType ct on ct.CourtTypeID = l.CourtTypeID
		left join tblJurisdiction j on j.JurisdictionID = l.jurisdictionID 
		left join tblClaimSettlementDocumentation csd on cl.ClaimLawsuitID = csd.ClaimLawsuitID
	Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		and cl.ClaimLawsuitID = @ClaimLawsuitID 
		and lc.LegalCapacityID in (19, 24)

Set @Spouse = (Select Spouse From @SpouseInfo)
Set @SpouseCapacity = (Select SpouseCapacity From @SpouseInfo) 

-- Find Executors for Claimant
DECLARE Executor_Curs CURSOR FOR 
	SELECT Distinct crp.FirstName + ' ' + Coalesce(crp.MiddleName + ' ','') + crp.LastName as RPName, 
		COALESCE(lc.LegalCapacity, '[CAPACITY]') as ExecutorCapacity	 	
	FROM tblClaimantPersonalInfo cpi
		inner join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
		LEFT JOIN tblClaimDisease cd ON cd.ClaimID = c.ClaimID
		LEFT JOIN tblDisease d ON d.DiseaseID = cd.DiseaseID
		LEFT JOIN tblHighLevelDisease dhld ON dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
		LEFT JOIN tblClaimSettlement cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblAllegation a ON a.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT JOIN tblDefendantCompany dc ON dc.DefendantCompanyID = a.DefendantCompanyID
		LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
		LEFT JOIN tblState laws ON laws.StateID = l.StateID
		LEFT JOIN tblClaimantRelatedParty crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblLegalCapacity lc ON lc.LegalCapacityID = crp.LegalCapacityID
		LEFT JOIN tblRelationship r ON r.RelationshipID = crp.RelationshipID
		LEFT JOIN tblClaimantEmployment ce ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblClaimantProduct cp ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
		LEFT JOIN tblPlaintiffCounselAddress pc ON pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		LEFT JOIN tblCourtType ct ON ct.CourtTypeID = l.CourtTypeID
		LEFT JOIN tblJurisdiction j ON j.JurisdictionID = l.jurisdictionID 
		LEFT JOIN tblClaimSettlementDocumentation csd ON cl.ClaimLawsuitID = csd.ClaimLawsuitID
	WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND cl.ClaimLawsuitID = @ClaimLawsuitID 
		AND lc.LegalCapacityID NOT IN (19, 24) 

DECLARE @ExecutorName NVARCHAR(100)
DECLARE @ExecutorCapacity NVARCHAR(100)
DECLARE @AllExecutorNames NVARCHAR(200)
DECLARE @AllExecutorCapacities NVARCHAR(100)

----Open Cursor to loop through executors
OPEN Executor_Curs
FETCH NEXT FROM Executor_Curs INTO @ExecutorName, @ExecutorCapacity

WHILE @@fetch_status = 0
BEGIN
	IF @AllExecutorNames IS NULL 
	BEGIN 
		SET @AllExecutorNames = @ExecutorName 
	END
	ELSE
	BEGIN
		SET @AllExecutorNames = @AllExecutorNames + ', ' + @ExecutorName 
	END
	
	IF @AllExecutorCapacities IS NULL
	BEGIN
		SET @AllExecutorCapacities = @ExecutorCapacity 
	END
	ELSE
		BEGIN
		IF @AllExecutorCapacities = @ExecutorCapacity 
		BEGIN
			SET @AllExecutorCapacities = @AllExecutorCapacities 
		END
		ELSE
		BEGIN
			SET @AllExecutorCapacities = '[MULTIPLE CAPACITIES FOR EXECUTORS]'
		END
		END
	
FETCH NEXT FROM Executor_Curs INTO @ExecutorName, @ExecutorCapacity
END

CLOSE Executor_Curs
DEALLOCATE Executor_Curs

--Select Information about Claimant for Release
SELECT  DISTINCT LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4) AS SSN,
	cpi.FirstName, 
	COALESCE(cpi.MiddleName + ' ','')AS MiddleName, 
	cpi.LastName, 
	COALESCE(Suffix,'') AS Suffix , 
	laws.State,
	pc.PlaintiffCounsel,
	NULL AS PrimaryLocalPlaintiffCounsel,
	pc.State AS PCState, 
	laws.StateID,
	COALESCE(@AllExecutorNames, '[EXECUTOR]') AS Executors,
	COALESCE(@AllExecutorCapacities, '[CAPACITY]') AS ExecutorCapacity,
	COALESCE(@Spouse,'[SPOUSE]') AS Spouse, 
	COALESCE(@SpouseCapacity, '[CAPACITY]') AS SpouseCapacity,
	CourtType, 
	Jurisdiction,
	CaseCaption, 
	DocketNumber, 
	cl.ClaimLawsuitID, 
	cs.ReleaseTemplateTypeID,
	c.MatterID,
	SettlementGroup   
FROM tblClaimantPersonalInfo cpi
	INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblClaimDisease cd ON cd.ClaimID = c.ClaimID
	LEFT JOIN tblDisease d ON d.DiseaseID = cd.DiseaseID
	LEFT JOIN tblHighLevelDisease dhld ON dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
	LEFT JOIN tblClaimSettlement cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblAllegation a ON a.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblDefendantCompany dc ON dc.DefendantCompanyID = a.DefendantCompanyID
	LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
	LEFT JOIN tblState laws ON laws.StateID = l.StateID
	LEFT JOIN tblClaimantRelatedParty crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblLegalCapacity lc ON lc.LegalCapacityID = crp.LegalCapacityID
	LEFT JOIN tblRelationship r ON r.RelationshipID = crp.RelationshipID
	LEFT JOIN tblClaimantEmployment ce ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantProduct cp ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
	LEFT JOIN tblPlaintiffCounselAddress pc ON pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	LEFT JOIN tblCourtType ct ON ct.CourtTypeID = l.CourtTypeID
	LEFT JOIN tblJurisdiction j ON j.JurisdictionID = l.jurisdictionID 
	LEFT JOIN tblClaimSettlementDocumentation csd ON cl.ClaimLawsuitID = csd.ClaimLawsuitID
	LEFT JOIN tblClaimSettlementGroup csg ON csg.SettlementGroupID = cs.SettlementGroupID
	LEFT JOIN tblClaimantSuffix csuf ON csuf.SuffixID = cpi.SuffixID
WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND cl.ClaimLawsuitID = @ClaimLawsuitID 

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoRelease] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAutoRelease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoRelease] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptAutoRelease] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAutoRelease] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAutoRelease] TO [power_user]
GO
