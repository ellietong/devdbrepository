SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryrptAutoMedicareForm] (@ClaimantPersonalInfoID bigint, @ClaimLawsuitID bigint)
AS

Set NoCount On

--DECLARE @Claimantpersonalinfoid bigint
--DECLARE @ClaimLawsuitID bigint
--set @Claimantpersonalinfoid = 50168
----set @Claimantpersonalinfoid = 21417 --two claims for one claimant
--Set @ClaimLawsuitID = 51987

--Find Single Related Party for Deceased Claimant
Declare @RelatedParty table (ClaimantPersonalInfoID int, RPFirstName nvarchar(255), RPMiddleName nvarchar(255), RPLastName nvarchar(255),
RPAddress1 nvarchar(255), RPAddress2 nvarchar(255), RPTaxIDNum nvarchar(255), 
RPCity nvarchar(255), RPPhone nvarchar(255), RPState nvarchar(255), RPZip nvarchar(255),
RPExtension nvarchar(255), RPLegalCapacity int, RPSSN nvarchar(255))
Insert Into @RelatedParty 
	SELECT Distinct cpi.ClaimantPersonalInfoID, crp.FirstName, crp.MiddleName, crp.LastName, crp.RPAddress1, crp.RPAddress2,
	crp.RPTIN, crp.RPCity, crp.RPPhone, rpstate.FullNameState, crp.RPZip, crp.RPExtension, lc.LegalCapacityID, crp.RPSocialSecurityNumber
	FROM tblClaimantPersonalInfo cpi
		inner join tblClaim c on c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		left join tblClaimLawsuit cl on cl.ClaimID = c.ClaimID
		left join tblClaimDisease cd on cd.ClaimID = c.ClaimID
		left join tblDisease d on d.DiseaseID = cd.DiseaseID
		left join tblHighLevelDisease dhld on dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
		left join tblClaimSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
		left join tblAllegation a on a.ClaimLawsuitID = cl.ClaimLawsuitID
		left join tblDefendantCompany dc on dc.DefendantCompanyID = a.DefendantCompanyID
		left join tblLawsuit l on l.LawsuitID = cl.LawsuitID
		left join tblState laws on laws.StateID = l.StateID
		left join tblClaimantRelatedParty crp on crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT join tblState rpstate ON rpstate.StateID = crp.StateID
		left join tblLegalCapacity lc on lc.LegalCapacityID = crp.LegalCapacityID
		left join tblRelationship r on r.RelationshipID = crp.RelationshipID
		left join tblClaimantEmployment ce on ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
		left join tblPlaintiffCounselAddress pc on pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		left join tblCourtType ct on ct.CourtTypeID = l.CourtTypeID
		left join tblJurisdiction j on j.JurisdictionID = l.jurisdictionID 
		left join tblClaimSettlementDocumentation csd on cl.ClaimLawsuitID = csd.ClaimLawsuitID
	Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		and cl.ClaimLawsuitID = @ClaimLawsuitID 
		and lc.LegalCapacityID in (1,8,3,5,6,7,9,15,16,17,22)

--Select Information about Claimant for Medicare Form
SELECT Distinct 
	cpi.MedicareBeneficiaryStatusID,
	cpi.FirstName, 
	Coalesce(cpi.MiddleName,'')as MiddleName, 
	cpi.LastName, 
	Coalesce(Suffix,'') as Suffix,
	laws.State,
	LEFT(SocialSecurityNumber, 3) + '-' + SUBSTRING(SocialSecurityNumber, 4, 2) + '-' + RIGHT(SocialSecurityNumber, 4)as SSN, 
	Coalesce(cpi.HealthInsuranceClaimNumber,'') as HealthInsuranceClaimNumber,
	CONVERT(nvarchar,MONTH(cpi.BirthDate))+'/'+CONVERT(NVARCHAR,DAY(cpi.BirthDate))+'/'+CONVERT(NVARCHAR,YEAR(cpi.BirthDate)) AS BirthDate,
	cpi.GenderID,
	CASE WHEN cpi.DeceasedDate IS NULL THEN 0 ELSE 1 END AS DeceasedID,
	CONVERT(NVARCHAR,MONTH(dbo.fnEarliestExposure(@ClaimantPersonalInfoID)))+'/'+CONVERT(NVARCHAR,DAY(dbo.fnEarliestExposure(@ClaimantPersonalInfoID)))+'/'+CONVERT(NVARCHAR,YEAR(dbo.fnEarliestExposure(@ClaimantPersonalInfoID))) AS FirstExposure,
	CONVERT(NVARCHAR,MONTH(dbo.fnLatestExposure(@ClaimantPersonalInfoID)))+'/'+CONVERT(NVARCHAR,DAY(dbo.fnLatestExposure(@ClaimantPersonalInfoID)))+'/'+CONVERT(NVARCHAR,YEAR(dbo.fnLatestExposure(@ClaimantPersonalInfoID))) AS LastExposure,
	CONVERT(NVARCHAR,MONTH(cs.SettlementDate))+'/'+CONVERT(NVARCHAR,DAY(cs.SettlementDate))+'/'+CONVERT(NVARCHAR,YEAR(cs.SettlementDate)) AS SettlementDate,
	CONVERT(NVARCHAR,MONTH(cs.SettlementDueDate))+'/'+CONVERT(NVARCHAR,DAY(cs.SettlementDueDate))+'/'+CONVERT(NVARCHAR,YEAR(cs.SettlementDueDate)) AS SettlementDueDate,
	'$' + LTRIM(CONVERT(CHAR(17),CAST(cs.SettlementAmount AS MONEY),1)) AS SettlementAmount,
	dcomp.DefendantCompany AS Matter,
	m.Matter AS MatterFolder,
	d.Disease+' - '+d.ICD9 AS Disease,
	COALESCE(mat.Material,'') AS Material,
	pc.PlaintiffCounsel,
	pc.FullNameState AS PlaintiffCounselState,
	pc.PlaintiffCounselAddress1,
	COALESCE(pc.PlaintiffCounselAddress2,'') AS PlaintiffCounselAddress2,
	pc.PlaintiffCounselCity,
	CASE WHEN claimsaddress.PlaintiffCounselMailZip IS NULL THEN CONVERT(NVARCHAR,pc.PlaintiffCounselZip) ELSE
	CONVERT(NVARCHAR,pc.PlaintiffCounselZip) + '-' + CONVERT(NVARCHAR,claimsaddress.PlaintiffCounselMailZip) END AS PlaintiffCounselZip,
	COALESCE(CONVERT(NVARCHAR,pc.PlaintiffCounselExtension), '') AS PlaintiffCounselExtension,
	'(' + LEFT(CONVERT(NVARCHAR,pc.PlaintiffCounselPhone), 3) + ') ' + SUBSTRING(CONVERT(NVARCHAR,pc.PlaintiffCounselPhone), 4, 3) + '-' + RIGHT(CONVERT(NVARCHAR,pc.PlaintiffCounselPhone), 4)AS PlaintiffCounselPhone,
	COALESCE(pc.PlaintiffCounselTaxIDNum,'') AS PlaintiffCounselTaxIDNum,
	rp.RPFirstName, 
	COALESCE(rp.RPMiddleName,'') AS RPMiddleName, 
	rp.RPLastName,
	COALESCE(rp.RPAddress1,'') AS RPAddress1,
	COALESCE(rp.RPAddress2,'') AS RPAddress2, 
	COALESCE(rp.RPTaxIDNum,'') AS RPTaxIDNum, 
	COALESCE(rp.RPCity,'') AS RPCity,
	COALESCE('(' + LEFT(CONVERT(NVARCHAR,rp.RPPhone), 3) + ') ' + SUBSTRING(CONVERT(NVARCHAR,rp.RPPhone), 4, 3) + '-' + RIGHT(CONVERT(NVARCHAR,rp.RPPhone), 4),'') AS RPPhone, 
	COALESCE(rp.RPState,'') AS RPState,
	COALESCE(rp.RPZip,'') AS RPZip,
	COALESCE(rp.RPExtension,'') AS RPExtension,
	DocketNumber,
	rp.RPLegalCapacity,
	COALESCE(LEFT(rp.RPSSN, 3) + '-' + SUBSTRING(rp.RPSSN, 4, 2) + '-' + RIGHT(rp.RPSSN, 4),'') AS RPSSN,
	CONVERT(NVARCHAR,csg.SettlementGroupID) AS SettlementGroupID,
	SettlementGroup
FROM tblClaimantPersonalInfo cpi
	INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblClaimDisease cd ON cd.ClaimID = c.ClaimID
	LEFT JOIN tblDisease d ON d.DiseaseID = cd.DiseaseID
	LEFT JOIN tblHighLevelDisease dhld ON dhld.HighLevelDiseaseID = d.HighLevelDiseaseID
	LEFT JOIN tblClaimSettlement cs ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblAllegation a ON a.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblMaterial mat ON mat.MaterialID = a.MaterialID
	LEFT JOIN tblDefendantCompany dc ON dc.DefendantCompanyID = a.DefendantCompanyID
	LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
	LEFT JOIN tblMatter m ON m.MatterID = c.MatterID
	LEFT JOIN tblState laws ON laws.StateID = l.StateID
	LEFT JOIN tblClaimantRelatedParty crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblLegalCapacity lc ON lc.LegalCapacityID = crp.LegalCapacityID
	LEFT JOIN tblRelationship r ON r.RelationshipID = crp.RelationshipID
	LEFT JOIN tblClaimantEmployment ce ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantProduct cp ON cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
	LEFT JOIN tblPlaintiffCounselAddress pc ON pc.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	LEFT JOIN tblCourtType ct ON ct.CourtTypeID = l.CourtTypeID
	LEFT JOIN tblJurisdiction j ON j.JurisdictionID = l.jurisdictionID 
	LEFT JOIN tblClaimSettlementDocumentation csd ON cl.ClaimLawsuitID = csd.ClaimLawsuitID
	LEFT JOIN tblClaimSettlementGroup csg ON csg.SettlementGroupID = cs.SettlementGroupID
	LEFT JOIN tblClaimantSuffix csuf ON csuf.SuffixID = cpi.SuffixID
	LEFT JOIN @RelatedParty rp ON rp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN [KGA-ClaimsAdmin].[dbo].[tblPlaintiffCounselAddress] claimsaddress ON pc.PlaintiffCounselAddressID = claimsaddress.PlaintiffCounselAddressID
	LEFT JOIN tblDefendantCompany dcomp ON dcomp.DefendantCompanyID = a.DefendantCompanyID
WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	AND cl.ClaimLawsuitID = @ClaimLawsuitID
	AND PrimaryDisease = 1

RETURN

GO
