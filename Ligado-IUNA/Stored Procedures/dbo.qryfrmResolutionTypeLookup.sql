SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmResolutionTypeLookup]
as

Select 0 as ClaimLawsuitStatusID, '<<All Claim Lawsuit Statuses>>' as ClaimLawsuitStatus, '<<All Resolution Types>>' as Status
Union All
Select ClaimLawsuitStatusID, ClaimLawsuitStatus as ClaimLawsuitStatus, ClaimLawsuitStatus as Status
From tblClaimLawsuitStatus
Where ClaimLawsuitStatus is not Null
Order By ClaimLawsuitStatus

Return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmResolutionTypeLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmResolutionTypeLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmResolutionTypeLookup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmResolutionTypeLookup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmResolutionTypeLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmResolutionTypeLookup] TO [power_user]
GO
