SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qryrptWestchesterDefenseCosts] (@Matter int, @MonthYear nvarchar(max), @paiddatestart datetime, @paiddateend datetime)

AS

SET NOCOUNT ON

--DECLARE @Matter int
--SET @Matter = 1
--declare @paiddatestart datetime
--declare @paiddateend datetime
--set @paiddatestart = '1/1/2015'
--set @paiddateend = '1/31/2015'
--Declare @MonthYear nvarchar(max)
--Set @MonthYear='January 2015'



DECLARE @maxMatterID int
SELECT
  @maxMatterID = MAX(MatterID)
FROM tblDefInvClaimantDetail

IF OBJECT_ID('tempdb..#Wilbraham') IS NOT NULL
  DROP TABLE #Wilbraham

CREATE TABLE #Wilbraham (
  FirmID int,
  WLBAdjustment money,
  WLBMatter int
)
INSERT INTO #Wilbraham
  SELECT
    CASE
      WHEN di.DefenseCounselAddressID = 43 THEN di.DefenseCounselAddressID
    END,
    CASE
      WHEN di.DefenseCounselAddressID = 43 AND
      dicd.MatterID = 1 AND
      Fees < 325000.00 THEN (325000.00 - Fees)
    END AS Wilbraham,
    2
  FROM tblDefenseInvoice DI
    INNER JOIN tblDefInvClaimantDetail dicd ON dicd.DefenseInvoiceID = di.DefenseInvoiceID
    INNER JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselAddressID = di.DefenseCounselAddressID
    INNER JOIN tblMatter M ON DIcd.MatterID = M.MatterID
    INNER JOIN tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
    LEFT JOIN tblPaymentDefense pd ON pd.DefenseInvoiceID = di.DefenseInvoiceID
    LEFT JOIN tblCheck ck ON ck.CheckID = pd.CheckID
  WHERE (ck.SourceDate BETWEEN @paiddatestart AND @paiddateend
    AND DIS.StatusID IN (1, 2)
    AND (DI.SubmitDate IS NOT NULL)
    AND DIcd.MatterID >= COALESCE(1, 0)
    AND DIcd.MatterID <= COALESCE(NULLIF(1, 0), @maxMatterID)
    AND CheckFundingSourceID IN (1, 2))
    OR ((di.DefenseCounselAddressID = 105
    AND di.DatePayable BETWEEN @paiddatestart AND @paiddateend)
    AND DIcd.MatterID >= COALESCE(1, 0)
    AND DIcd.MatterID <= COALESCE(NULLIF(1, 0), @maxMatterID))


SELECT
  di.DefenseCounselAddressID,
  Matter,
  dicd.MatterID,
  DefenseCounsel,
  di.InvoiceNumber,
  Period,
  AdjustedTotal,
  SubmitDate,
  DatePayable,
  CASE
    WHEN di.DefenseCounselAddressID = 43 AND
    @Matter = 1 AND
    (Fees) >= 325000.00 THEN 325000.00
    ELSE CASE
      WHEN di.DefenseCounselAddressID = 43 AND
      @Matter = 2 THEN COALESCE(w.WlbAdjustment, 0)
      ELSE Fees
    END
  END AS Fees,
  Costs,
  CASE
    WHEN di.DefenseCounselAddressID = 43 THEN 0
    ELSE Adjustments
  END AS 'Adjustments',
  DI.StatusID,
  Status,
  CASE
    WHEN (Fees IS NULL AND
    Costs IS NULL) OR
    (Fees + costs = 0) THEN AdjustedTotal
    WHEN di.DefenseCounselAddressID = 43 AND
    @Matter = 1 THEN (325000.00 + costs)
    WHEN di.DefenseCounselAddressID = 43 AND
    @Matter = 2 THEN (0 + costs)
    ELSE Fees + Costs
  END AS Subtotal,
  CASE
    WHEN (Fees IS NULL AND
    Costs IS NULL) OR
    (Fees + costs = 0) THEN AdjustedTotal
    WHEN di.DefenseCounselAddressID = 43 AND
    @Matter = 1 THEN (325000.00 + costs)
    WHEN di.DefenseCounselAddressID = 43 AND
    @Matter = 2 THEN (0 + costs)
    ELSE Fees + Costs - Adjustments
  END AS AmountPayable,
  @MonthYear AS 'MonthYear'
FROM tblDefenseInvoice DI
  INNER JOIN tblDefInvClaimantDetail dicd ON dicd.DefenseInvoiceID = di.DefenseInvoiceID
  INNER JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselAddressID = di.DefenseCounselAddressID
  INNER JOIN tblMatter M ON DIcd.MatterID = M.MatterID
  INNER JOIN tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
  LEFT JOIN tblPaymentDefense pd ON pd.DefenseInvoiceID = di.DefenseInvoiceID
  LEFT JOIN tblCheck ck ON ck.CheckID = pd.CheckID
  LEFT JOIN #Wilbraham w ON w.FirmID = di.FirmID
    AND w.WLBMatter = dicd.MatterID
WHERE (ck.SourceDate BETWEEN @paiddatestart AND @paiddateend
  AND DIS.StatusID IN (1, 2)
  AND (DI.SubmitDate IS NOT NULL)
  AND DIcd.MatterID >= COALESCE(@Matter, 0)
  AND DIcd.MatterID <= COALESCE(NULLIF(@Matter, 0), @maxMatterID)
  AND CheckFundingSourceID IN (1, 2))
  OR ((di.DefenseCounselAddressID = 105
  AND di.DatePayable BETWEEN @paiddatestart AND @paiddateend)
  AND Dicd.MatterID >= COALESCE(@Matter, 0)
  AND DIcd.MatterID <= COALESCE(NULLIF(@Matter, 0), @maxMatterID))
GROUP BY Matter,
         DIcd.MatterID,
         DefenseCounsel,
         DefenseCounsel,
         di.InvoiceNumber,
         Period,
         AdjustedTotal,
         SubmitDate,
         DatePayable,
         Fees,
         costs,
         Adjustments,
         DI.StatusID,
         Status,
         DI.DefenseCounselAddressID,
         w.WLBAdjustment
ORDER BY dca.DefenseCounsel, DatePayable, invoicenumber, Status

RETURN







GO
