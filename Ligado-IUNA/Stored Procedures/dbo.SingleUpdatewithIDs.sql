SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [dbo].[SingleUpdatewithIDs]
(
	@TableName varchar(50)
	,@UpdateFieldName varchar(50)
	,@UpdateValue varchar(50)
	,@ConditionFieldName varchar(50)
	,@ConditionValue varchar(1000)
)
as
/*

Author: Ellie Tong
Date: Feb.,17th, 2015
Purpose:  Use stored proc to perform a simple update to a database table
Example:
	execute dbo.SimpleUpdatewithIDs
	@TableName = 'dbo.tblPaymentDefense'
	,@UpdateFieldName = 'IsFullyPaid'
	,@UpdateValue = '1'
	,@ConditionFieldName = 'PaymentDefenseID'
	,@ConditionValue = '450,451'

*/
set nocount on
declare @sql nvarchar(max)
,@rowcount tinyint
,@message varchar(200)
begin try
	begin tran
	set @sql = 'update ' + @TableName
	+ ' set ' + @UpdateFieldName + ' = ' + @UpdateValue
	+ ' where ' + @ConditionFieldName + ' in (' + @ConditionValue + ')'
	print @sql
	EXECUTE sp_executesql @sql
	set @rowcount = @@ROWCOUNT

	if @@error <> 0 
	begin
		set @message = 'Error while updating ' + @TableName + '.  ' + CONVERT(VARCHAR(2),@@ERROR	)
		print @message
		rollback tran
		return 1
	end
	else
		commit tran
		
		set @message =  CONVERT(VARCHAR(2),@rowcount) + ' rows updated.'
		print @message 

end try
begin catch
	select ERROR_MESSAGE()
	select ERROR_LINE()
	select ERROR_NUMBER()
	select ERROR_PROCEDURE()
	select ERROR_SEVERITY()
	select ERROR_STATE()
end catch
GO
