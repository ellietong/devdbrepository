SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailNewComplaints]
AS
BEGIN
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
DECLARE @Subject    NVARCHAR (100) = 'IUNA/Nosroc: New Complaints Uploaded ' + CONVERT(varchar, GETDATE(), 107)
DECLARE @LogoHeader NVARCHAR (2000)

DECLARE @Today4PM     DATETIME = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 16:00:00.000')
DECLARE @Yesterday4PM DATETIME = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 16:00:00.000')

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.	
SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (DocID                  INT, 
								   DocTitle               NVARCHAR(500), 
								   FirmID                 INT, 
								   LName                  NVARCHAR(50), 
								   FName                  NVARCHAR(50),
								   ClaimantPersonalInfoID BIGINT, 
								   ServiceDate            DATE, 
								   [State]                NVARCHAR(5), 
								   ComplaintID            BIGINT, 
								   AcceptedBy             NVARCHAR(250))
INSERT INTO @NewComplaintsTable
	SELECT d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		l.DefenseCounselAddressID AS FirmID,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		MAX(ServiceDate),
		s.State,
		d.ComplaintID,
		COALESCE(AcceptedBy, '')
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo  cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		LEFT  JOIN tblClaim                 c   ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT  JOIN tblClaimLawsuit          cl  ON cl.ClaimID = c.ClaimID
		LEFT  JOIN tblLawsuit               l   ON l.LawsuitID = cl.LawsuitID
		LEFT  JOIN tblState                 s   ON s.StateID = l.StateID
	WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
		AND ClaimantDocumentTypeID IN (8, 4, 33)
		AND RIGHT(CreatedBy, 8) = 'kcic.com'
		AND l.DefenseCounselAddressID IS NOT NULL
		AND IsPrimaryForClaim = 1
		AND IsCurrentClaim = 1
	GROUP BY d.documentID,
		d.DocumentTitle,
		l.DefenseCounselAddressID,
		LastName,
		FirstName,
		cpi.ClaimantPersonalInfoID,
		s.State,
		d.ComplaintID,
		AcceptedBy

DECLARE @NewDocumentsTable TABLE (DocID int, 
								  DocTitle nvarchar (500), 
								  FirmID int, 
								  LName nvarchar (50),
								  FName nvarchar (50), 
								  DocType nvarchar (50), 
								  ClaimantPersonalInfoID bigint, 
								  [State] nvarchar (5), 
								  ComplaintID bigint)
INSERT INTO @NewDocumentsTable
	SELECT d.DocumentID,
		'<a href="https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/DocumentProfile.aspx?ItemId=' + COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle,
		l.DefenseCounselAddressID AS FirmID,
		cpi.LastName,
		cpi.FirstName,
		ClaimantDocumentType,
		cpi.ClaimantPersonalInfoID,
		s.State,
		d.ComplaintID
	FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo  cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		LEFT  JOIN tblClaimantDocumentType  ct  ON ct.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
		LEFT  JOIN tblClaim                 c   ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT  JOIN tblClaimLawsuit          cl  ON cl.claimid = c.claimid
		LEFT  JOIN tblLawsuit               l   ON l.lawsuitid = cl.lawsuitid
		LEFT  JOIN tblState                 s   ON s.StateID = l.StateID
	WHERE DateCreated BETWEEN GETDATE() - 1 AND GETDATE()
		AND d.ClaimantDocumentTypeID NOT IN (8, 4, 33, 40, 41, 42)
		AND RIGHT(CreatedBy, 8) = 'kcic.com'
		AND COALESCE(Accepted, 0) <> 1
		AND IsPrimaryForClaim = 1
		AND IsCurrentClaim = 1
		AND l.DefenseCounselAddressID IS NOT NULL
	GROUP BY d.DocumentID,
		d.DocumentTitle,
		s.State,
		l.DefenseCounselAddressID,
		LastName,
		FirstName,
		ClaimantDocumentType,
		cpi.claimantpersonalinfoid,
		d.ComplaintID

DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar (max))
DECLARE @DocID        INT, 
		@LastDocID    INT, 
		@Claimant     NVARCHAR(MAX), 
		@ClaimantID   BIGINT, 
		@ClaimantList NVARCHAR(MAX)

DECLARE Cur_Complaints CURSOR Local FAST_FORWARD FOR 
	SELECT DocID,
		COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
		ClaimantPersonalInfoID
	FROM @NewComplaintsTable 
	UNION 
	SELECT DocID,
		COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
		ClaimantPersonalInfoID
	FROM @NewDocumentsTable

OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID

WHILE @@FETCH_STATUS = 0
BEGIN
	IF (LEN (@ClaimantList) = 0 OR @ClaimantList IS NULL)
	BEGIN
		SET @ClaimantList = '<ul> <a href="https://www.kcicextranet.com/clients/iuna_nosroc/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + @Claimant + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
	END
	ELSE
	BEGIN
		SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="https://www.kcicextranet.com/clients/iuna_nosroc/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + COALESCE(@Claimant, '') + COALESCE(' (' + CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
	END

	SET @LastDocID = @DocID
	FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID

	IF @DocID < > @LastDocID OR @@FETCH_STATUS < > 0
	BEGIN
		INSERT INTO @ClaimantPerDoc
			SELECT @LastDocID,
				@ClaimantList

		SET @ClaimantList = NULL
	END
END

CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints

--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.
DECLARE @FirmID nvarchar (10), @FirmURL nvarchar (100)

DECLARE firm_Curs CURSOR FOR 
	SELECT nct.FirmID,
		'https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Documents.aspx'
	FROM @NewComplaintsTable nct
	GROUP BY nct.FirmID 
	UNION 
	SELECT doc.firmid,
		'https://www.kcicextranet.com/clients/iuna_nosroc/cp/Pages/Documents.aspx'
	FROM @NewDocumentsTable doc
	GROUP BY doc.firmid

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @FirmID, @FirmURL

WHILE @@FETCH_STATUS = 0
BEGIN
	--Declare and populate the body of the email for individual firms
	DECLARE @BodyMessage nvarchar (MAX)
	--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
	--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
	SET @BodyMessage = @LogoHeader

	--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
	IF (SELECT COUNT(docTitle) FROM @NewComplaintsTable WHERE FirmID = @FirmID) > 0
	BEGIN
		SET @BodyMessage = @BodyMessage + '<H4>The following complaints have been uploaded in the last 24 hours: </H4>'
		SET @BodyMessage = @BodyMessage + N'<table style="border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
			N'<tr><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);
			">Document Title</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);
			">Linked Claimants (ClaimantID) </th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);
			">Date Served*</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);
			">State</th><th style="border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193); 
			">Accepted By</th>' +
			CAST((SELECT
					'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + DocTitle                               + '</td>', '',
					'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + cl.ClaimantList                        + '</td>', '',
					'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + CAST(MAX(ServiceDate) AS nvarchar(50)) + '</td>', '',
					'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + [State]                                + '</td>', '',
					'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + AcceptedBy                             + '</td>', ''
				  FROM @NewComplaintsTable C
					  INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
				  WHERE c.FirmID = @firmid
				  GROUP BY docTitle,
					  cl.ClaimantList,
					  [State],
					  AcceptedBy 
				  FOR XML PATH ('tr'), TYPE) AS NVARCHAR(MAX)) +
			N'</table>' + N'<br/>'
	END
	ELSE
	BEGIN
		SET @BodyMessage = @BodyMessage + '<H4>No Complaints have been uploaded in the last 24 hours. </H4>'
	END

	IF (SELECT COUNT(DocTitle) FROM @NewDocumentsTable WHERE FirmID = @firmid) > 0
	BEGIN
		SET @BodyMessage = @BodyMessage + '<H4>The following documents have been uploaded in the last 24 hours: </H4>'
		SET @BodyMessage = @BodyMessage + N'<table>' +
			N'<tr><th>Document Title</th><th>Linked Claimants (ClaimantID)</th><th>Document Type</th><th>State</th>' +
			CAST((SELECT
					'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + DocTitle        + '</td>', '',
					'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + cl.ClaimantList + '</td>', '',
					'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + DocType         + '</td>', '',
					'<td style="font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: middle;">' + [State]         + '</td>', ''
				  FROM @NewDocumentsTable d
					  INNER JOIN @ClaimantPerDoc cl ON d.DocID = cl.DocID
				  WHERE FirmID = @FirmID
				  GROUP BY DocTitle,
					  cl.ClaimantList,
					  DocType,
					  [State]
				  FOR XML PATH ('tr'), TYPE) AS NVARCHAR(MAX)) +
			N'</table>' + N'<br/>'
	END
	ELSE
	BEGIN
		SET @BodyMessage = @BodyMessage + '<H4>No other new documents have been uploaded in the last 24 hours. </H4>'
	END

	--Undo the HTML stripping from the xml path function above so embedded tags work
	SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

	--Link for document library
	SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

	--Service Date Assumption 
	IF (SELECT COUNT(docTitle) FROM @NewComplaintsTable WHERE FirmID = @firmid) > 0
	BEGIN
		SET @BodyMessage = @BodyMessage + '<br>*Note: Date served is the latest service date for claimants'' lawsuits.<br><br>'
	END

	--KCIC address and disclaimer
	SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
		733 10th Street, NW, Suite 3001<br>
		Washington, DC 20001<br>
		(t) 202-772-2300<br>
		(f) 202-772-2333<br><br>'

	--This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>
	DECLARE @RoleID int = 55 --New Complaints
	DECLARE @RecipientList nvarchar (500)
	SET @RecipientList = (SELECT Email + '; ' AS [text()]
						  FROM [Ligado-ExtranetSecurity].dbo.vPersonRoles pr
						      INNER JOIN [Ligado-ExtranetSecurity].dbo.Person p ON pr.PersonID = p.PersonID
						  WHERE RoleID = @RoleID 
						      AND EXISTS (SELECT PersonFirmAddressClientSiteID
										  FROM PersonFirmAddressClientSite     pfacs
											  INNER JOIN FirmAddressClientSite facs ON pfacs.FirmAddressClientSiteID = facs.FirmAddressClientSiteID
											  INNER JOIN FirmAddress           fa ON facs.FirmAddressID = fa.FirmAddressID
										  WHERE fa.FirmID = @FirmID)
							  AND ClientSiteID = dbo.fn_GetClientSiteID() FOR XML PATH(''))

	--Execute stored procedure to send the email to that firm
	EXEC msdb.dbo.sp_send_dbmail
		@profile_name          = 'Claims',
		@recipients            = @RecipientList,
		@blind_copy_recipients = 'iuna_nosroc@kcic.com',
		@subject               = @subject,
		@body                  = @BodyMessage,
		@body_format           = 'html'

	UPDATE [KGA-ClaimsAdmin].dbo.tblComplaintTracking
		SET	ExternalEmailDate = GETDATE(),
			ExternalRecipient = LEFT(@RecipientList, 30)
		WHERE ((ComplaintID IN (SELECT ComplaintID
								FROM @NewComplaintsTable
								WHERE FirmID = @FirmID 
									AND ComplaintID IS NOT NULL)
			OR  ComplaintID IN (SELECT ComplaintID
							    FROM @NewDocumentsTable
							    WHERE FirmID = @FirmID 
									AND ComplaintID IS NOT NULL))) 
			AND ExternalEmailDate is NULL

	--Move to the next firm to create a new email
	FETCH NEXT FROM firm_curs INTO @FirmID, @firmurl
END

CLOSE firm_curs
DEALLOCATE firm_curs

END

GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailNewComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailNewComplaints] TO [power_user]
GO
