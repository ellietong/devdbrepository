CREATE TABLE [dbo].[tblBillGroupPolicyExclude]
(
[BillGroupPolicyExcludeID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupID] [int] NOT NULL,
[PolicyID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillGroupPolicyExclude] ADD CONSTRAINT [PK_tblBillGroupPolicyExclude] PRIMARY KEY CLUSTERED  ([BillGroupPolicyExcludeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupPolicyExclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillGroupPolicyExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupPolicyExclude] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblBillGroupPolicyExclude] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblBillGroupPolicyExclude] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblBillGroupPolicyExclude] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblBillGroupPolicyExclude] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupPolicyExclude] TO [power_user]
GRANT SELECT ON  [dbo].[tblBillGroupPolicyExclude] TO [power_user]
GRANT INSERT ON  [dbo].[tblBillGroupPolicyExclude] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBillGroupPolicyExclude] TO [power_user]
GO
