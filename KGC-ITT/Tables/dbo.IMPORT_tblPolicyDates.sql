CREATE TABLE [dbo].[IMPORT_tblPolicyDates]
(
[PolicyDateID] [float] NULL,
[PolicyID] [float] NULL,
[PStartDate] [datetime] NULL,
[PEndDate] [datetime] NULL,
[ActualPStartDate] [datetime] NULL,
[ActualPEndDate] [datetime] NULL,
[AnnualPeriod] [float] NULL
) ON [PRIMARY]
GO
