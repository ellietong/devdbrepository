CREATE TABLE [dbo].[IMPORT_tblPolicyLimits]
(
[PolicyID] [float] NULL,
[LimitTypeID] [float] NULL,
[AttachmentPoint] [money] NULL,
[PerOccLimit] [money] NULL,
[LayerPerOccLimit] [money] NULL,
[SIR] [money] NULL,
[SIRAggregateLimit] [money] NULL,
[AggregateLimit] [money] NULL,
[PreviouslyExhausted] [money] NULL,
[Layer] [float] NULL,
[LayerLimitNote] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
