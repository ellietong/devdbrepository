CREATE TABLE [dbo].[ztblPolicyAllocation]
(
[UniqueID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[PolicyID] [bigint] NULL,
[PolicyDateID] [int] NULL,
[Provision1] [int] NULL,
[PStartDate] [datetime] NULL,
[PEndDate] [datetime] NULL,
[NumOccurrences] [bigint] NULL,
[AnnualPeriod] [int] NULL,
[InsuranceProgramID] [int] NULL,
[AttachmentPoint] [money] NULL,
[PerOccLimit] [money] NULL,
[LayerPerOccLimit] [float] NULL,
[SIR] [money] NULL,
[AggregateLimit] [money] NULL,
[OccurrenceAllocation] [float] NULL,
[OccurrenceRemaining] [float] NULL,
[CumAllocation] [float] NULL,
[CumAllocationIndemnity] [float] NULL,
[CumAllocationDefense] [float] NULL,
[CumAllocationDefenseOutsideLimits] [float] NULL,
[CumSIR] [float] NULL,
[CumSIRIndemnity] [float] NULL,
[CumSIRDefense] [float] NULL,
[AggregateAllocated] [money] NULL,
[AggregateRemaining] [money] NULL,
[SIRAllocated] [float] NULL,
[SIRProRation] [real] NULL,
[SIRRemaining] [float] NULL,
[Exhausted] [bit] NULL,
[ColumnAllocation] [float] NULL,
[ColumnSIR] [float] NULL,
[ColumnDefense] [float] NULL,
[UpperLimit] [float] NULL,
[SIRAggregate] [money] NULL,
[LayerPercentUneven1] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblPolicyAllocation] ADD CONSTRAINT [PK_ztblPolicyAllocation] PRIMARY KEY NONCLUSTERED  ([UniqueID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ztblPolicyAllocation] TO [Allocation_User]
GRANT INSERT ON  [dbo].[ztblPolicyAllocation] TO [Allocation_User]
GRANT DELETE ON  [dbo].[ztblPolicyAllocation] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[ztblPolicyAllocation] TO [Allocation_User]
GRANT SELECT ON  [dbo].[ztblPolicyAllocation] TO [base_user]
GRANT SELECT ON  [dbo].[ztblPolicyAllocation] TO [power_user]
GRANT INSERT ON  [dbo].[ztblPolicyAllocation] TO [power_user]
GRANT DELETE ON  [dbo].[ztblPolicyAllocation] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblPolicyAllocation] TO [power_user]
GO
