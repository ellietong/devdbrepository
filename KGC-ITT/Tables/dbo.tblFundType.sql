CREATE TABLE [dbo].[tblFundType]
(
[FundTypeID] [int] NOT NULL IDENTITY(1, 1),
[FundType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFundType] ADD CONSTRAINT [PK_tblFundType] PRIMARY KEY CLUSTERED  ([FundTypeID]) ON [PRIMARY]
GO
