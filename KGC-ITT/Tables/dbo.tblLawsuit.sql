CREATE TABLE [dbo].[tblLawsuit]
(
[LawsuitID] [bigint] NOT NULL IDENTITY(1, 1),
[CaseCaption] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[JurisdictionID] [int] NOT NULL,
[StateID] [int] NULL,
[CourtTypeID] [int] NOT NULL,
[ServiceDate] [datetime] NULL,
[ReceivedDate] [datetime] NULL,
[LeadPlaintiffFirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffLastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveTrialDate] [datetime] NULL,
[DefenseCounselAddressID] [int] NULL,
[PlaintiffCounselAddressID] [int] NULL,
[PrimaryNationalPlaintiffCounselID] [int] NULL,
[IsThirdPartyComplaint] [bit] NULL,
[DefenseJurisdictionID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLawsuit] ADD CONSTRAINT [PK__tblLawsuit1__5A1A5A11] PRIMARY KEY CLUSTERED  ([LawsuitID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [power_user]
GRANT INSERT ON  [dbo].[tblLawsuit] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLawsuit] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'JurisdictionID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'JurisdictionID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'StateID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'StateID'
GO
