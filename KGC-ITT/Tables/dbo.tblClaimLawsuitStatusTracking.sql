CREATE TABLE [dbo].[tblClaimLawsuitStatusTracking]
(
[ClaimLawsuitStatusTrackingID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[ClaimLawsuitStatusID] [int] NULL,
[StatusDate] [datetime] NULL,
[StatusNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimaryStatus] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [PK_tblClaimLawsuitStatusTracking] PRIMARY KEY CLUSTERED  ([ClaimLawsuitStatusTrackingID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GO
