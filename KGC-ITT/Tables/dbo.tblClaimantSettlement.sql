CREATE TABLE [dbo].[tblClaimantSettlement]
(
[ClaimantSettlementID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NOT NULL,
[SettlementDate] [datetime] NOT NULL,
[SettlementAmount] [money] NULL,
[SettlementDueDate] [datetime] NULL,
[SettlementNotificationDate] [datetime] NULL,
[SettlementsFundsRequestDate] [datetime] NULL,
[PayToCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaidDate] [datetime] NULL,
[SettlementGroupID] [bigint] NULL,
[SettlementNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSettlement] ADD CONSTRAINT [PK_mBtblClaimantSettlement] PRIMARY KEY CLUSTERED  ([ClaimantSettlementID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSettlement] ADD CONSTRAINT [FK_tblClaimantSettlement_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantSettlement] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimantSettlement] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantSettlement] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantSettlement] TO [power_user]
GO
