CREATE TABLE [dbo].[tblClaimantRelease]
(
[ClaimantReleaseID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantSettlementID] [bigint] NULL,
[ReleaseSignedDate] [datetime] NULL,
[ReleaseSentDate] [datetime] NULL,
[ReleaseReceivedDate] [datetime] NULL,
[ReleaseTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantRelease] ADD CONSTRAINT [PK__tblClaimantRelea__4AD81681] PRIMARY KEY CLUSTERED  ([ClaimantReleaseID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantRelease] ADD CONSTRAINT [FK_tblClaimantRelease_tblClaimantSettlement] FOREIGN KEY ([ClaimantSettlementID]) REFERENCES [dbo].[tblClaimantSettlement] ([ClaimantSettlementID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblClaimantRelease] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantRelease] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantRelease] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantRelease] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimantRelease] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantRelease] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantRelease] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1000000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantReleaseID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantReleaseID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantReleaseID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantReleaseID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantReleaseID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantSettlementID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantSettlementID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantSettlementID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantSettlementID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ClaimantSettlementID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseReceivedDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseReceivedDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseReceivedDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseReceivedDate'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseReceivedDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseSignedDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseSignedDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseSignedDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseSignedDate'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseSignedDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseTypeID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseTypeID'
GO
DECLARE @xp int
SELECT @xp=2535
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseTypeID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantRelease', 'COLUMN', N'ReleaseTypeID'
GO
