CREATE TABLE [dbo].[tblClaimDisease]
(
[ClaimDiseaseID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID] [bigint] NULL,
[DiseaseID] [int] NULL,
[PrimaryDisease] [bit] NULL,
[DiseaseDiagnosisDate] [datetime] NULL,
[DoctorID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimDisease] ADD CONSTRAINT [PK_tblClaimDisease] PRIMARY KEY CLUSTERED  ([ClaimDiseaseID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimDisease] TO [power_user]
GO
