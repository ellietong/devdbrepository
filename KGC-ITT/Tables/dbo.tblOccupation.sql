CREATE TABLE [dbo].[tblOccupation]
(
[OccupationID] [int] NOT NULL IDENTITY(1, 1),
[Occupation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccupation] ADD CONSTRAINT [PK_tblOccupation] PRIMARY KEY CLUSTERED  ([OccupationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccupation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccupation] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccupation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccupation] TO [power_user]
GO
