CREATE TABLE [dbo].[tblClaimSequence]
(
[ClaimSequenceID] [int] NOT NULL IDENTITY(1, 1),
[ClaimSequence] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimSequence] ADD CONSTRAINT [PK_mBtblClaimSequence] PRIMARY KEY CLUSTERED  ([ClaimSequenceID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimSequence] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSequence] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimSequence] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimSequence] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimSequence] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimSequence] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimSequence] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence', 'COLUMN', N'ClaimSequence'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence', 'COLUMN', N'ClaimSequence'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence', 'COLUMN', N'ClaimSequence'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence', 'COLUMN', N'ClaimSequenceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence', 'COLUMN', N'ClaimSequenceID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblClaimSequence', 'COLUMN', N'ClaimSequenceID'
GO
