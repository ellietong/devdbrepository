SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPOLICY_Carrier]
AS
	SELECT DISTINCT c.CarrierID, CarrierName
	FROM tblCarrier c 
	INNER JOIN tblPolicy p ON p.CarrierID = c.CarrierID


GO
