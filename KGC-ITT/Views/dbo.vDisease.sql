SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vDisease]
AS
SELECT     diseaseid, ICD9, Disease, ICD9Desc, HighlevelDisease 
FROM        [SPSQL].[KGA-ClaimsAdmin].dbo.Tbldisease d
inner join [SPSQL].[KGA-ClaimsAdmin].dbo.tblhighleveldisease h on h.highleveldiseaseID = d.HighLeveldiseaseID
GO
GRANT VIEW DEFINITION ON  [dbo].[vDisease] TO [base_user]
GRANT SELECT ON  [dbo].[vDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDisease] TO [power_user]
GRANT SELECT ON  [dbo].[vDisease] TO [power_user]
GO
