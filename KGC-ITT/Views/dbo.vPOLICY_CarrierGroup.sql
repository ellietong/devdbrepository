SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vPOLICY_CarrierGroup]
AS
	SELECT DISTINCT cg.CarrierGroupID, CarrierGroupName
	FROM tblCarrierGroup cg
	INNER JOIN tblCarrier c ON cg.CarrierGroupID = c.CarrierGroupID
	INNER JOIN tblPolicy p ON p.CarrierID = c.CarrierID


GO
