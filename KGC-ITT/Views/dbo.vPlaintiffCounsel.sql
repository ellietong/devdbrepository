SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vPlaintiffCounsel]
AS
SELECT     p.PlaintiffCounselID, p.PlaintiffCounsel, p.PlaintiffCounselTaxIDNum, a.PlaintiffCounselAddress1, a.PlaintiffCounselAddress2, a.PlaintiffCounselCity, state, a.PlaintiffCounselZip, a.PlaintiffCounselPhone, a.PlaintiffCounselExtension
FROM        [SPSQL].[KGA-ClaimsAdmin].dbo.TblPLaintiffcounsel p
left join [SPSQL].[KGA-ClaimsAdmin].dbo.tblplaintiffcounseladdress a on a.PlaintiffCounselID = p.PlaintiffCounselID
left join [SPSQL].[KGA-ClaimsAdmin].dbo.tblstate s on s.stateid = a.PlaintiffCounselStateID
GO
GRANT VIEW DEFINITION ON  [dbo].[vPlaintiffCounsel] TO [base_user]
GRANT SELECT ON  [dbo].[vPlaintiffCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vPlaintiffCounsel] TO [power_user]
GRANT SELECT ON  [dbo].[vPlaintiffCounsel] TO [power_user]
GO
