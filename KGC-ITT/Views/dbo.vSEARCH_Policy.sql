
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE VIEW [dbo].[vSEARCH_Policy]
AS
SELECT  p.PolicyID,
		PolicyNum,
		CarrierGroupName,
		c.CarrierGroupID,
		CarrierName,
		c.CarrierID,
		p.NamedInsuredID,
		NamedInsured,
		InsuranceProgram,
		p.InsuranceProgramID,
		pd.AnnualPeriod,
		MinPStartDate AS PolicyStartDate,
		MAX(PEndDate) AS PolicyEndDate,
		AttachmentPoint,
		PerOccLimit,
		LayerPerOccLimit,
		AggregateLimit,
		pl.LimitTypeID,
		LimitType
FROM tblPolicy p
INNER JOIN tblInsuranceProgram ip ON p.InsuranceProgramID = ip.InsuranceProgramID
INNER JOIN tblCarrier c ON p.CarrierID = c.CarrierID
INNER JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID
INNER JOIN tblNamedInsured ni ON p.NamedInsuredID = ni.NamedInsuredID
INNER JOIN tblPolicyDates pd ON p.PolicyID = pd.PolicyID
INNER JOIN tblPolicyLimits pl ON p.PolicyID = pl.PolicyID
INNER JOIN tblPolicyLimitType plt ON pl.LimitTypeID = plt.LimitTypeID
GROUP BY
		p.PolicyID,
		PolicyNum,
		CarrierGroupName,
		c.CarrierGroupID,
		CarrierName,
		c.CarrierID,
		p.NamedInsuredID,
		NamedInsured,
		InsuranceProgram,
		p.InsuranceProgramID,
		pd.AnnualPeriod,
		MinPStartDate,
		AttachmentPoint,
		PerOccLimit,
		LayerPerOccLimit,
		AggregateLimit,
		pl.LimitTypeID,
		LimitType
--ORDER BY MinPStartDate ASC



GO
