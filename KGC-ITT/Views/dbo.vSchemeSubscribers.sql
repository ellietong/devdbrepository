SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vSchemeSubscribers]
AS
SELECT s.SchemeID, SchemeName, SubscriberID
FROM [SPSQL].[KGA-InsolvencyTracking].dbo.tblScheme s
LEFT JOIN [SPSQL].[KGA-InsolvencyTracking].dbo.tblLondonSchemeCompany l ON s.SchemeID = l.SchemeID
GO
GRANT VIEW DEFINITION ON  [dbo].[vSchemeSubscribers] TO [base_user]
GRANT SELECT ON  [dbo].[vSchemeSubscribers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSchemeSubscribers] TO [power_user]
GRANT SELECT ON  [dbo].[vSchemeSubscribers] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[vSchemeSubscribers] TO [Subscription_User]
GRANT SELECT ON  [dbo].[vSchemeSubscribers] TO [Subscription_User]
GO
