SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByPolicyByOccurrenceGroupQC]
(@allocationID int = null)
As

SET NOCOUNT ON

--DECLARE @allocationID int 
--DECLARE @occurrencegroupID int
 
--SET @allocationID = 67
--SET @occurrencegroupID = NULL

 Declare @limitTypeID int 
 set @limittypeid = (SELECT LimitTypeID FROM tblAllocation WHERE AllocationID = @allocationid)

 Declare @MaxOccurrenceGroupID int 
 set @MaxOccurrenceGroupID = (SELECT MAX(OccurrenceGroupID) FROM vOccurrence)

DECLARE @exhaustionTypeID int 
SET @exhaustionTypeID = (Select exhaustiontypeid from tblAllocation where AllocationID = @allocationID)

DECLARE @ExhaustionDateID int
SET @ExhaustionDateID = (Select Exhaustiondateid from tblAllocation where AllocationID = @allocationID)


DECLARE @exh table (PolicydateID int, PreviouslyExhaustedAggregate money)
INSERT INTO @exh (PolicydateID, PreviouslyExhaustedAggregate)
SELECT pd.PolicydateID, SUM(IndemnityAllocation + DefenseAllocation)
FROM tblExhaustion e INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID
	INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
WHERE ExhaustionTypeID = @exhaustionTypeID
AND ExhaustionDate <= (Select ExhaustionDate from tblExhaustionDate where ExhaustionDateID = @ExhaustionDateID)
GROUP BY pd.PolicyDateID

DECLARE @Allocation TABLE (AllocationID int, TotalSIR money, TotalAllocation money, PVTotalAllocation money)
INSERT INTO @Allocation
SELECT AllocationID, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
WHERE AllocationID = @AllocationID
GROUP BY AllocationID


DECLARE @CarrierAllocation TABLE (AllocationID int, CarrierID int, CarrierTotalSIR money, CarrierTotalAllocation money, PVCarrierTotalAllocation money)
INSERT INTO @CarrierAllocation
SELECT AllocationID, CarrierID, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVCarrierTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, CarrierID


DECLARE @CarrierGroupAllocation TABLE (AllocationID int, CarrierGroupID int, CarrierGroupTotalSIR money, CarrierGroupTotalAllocation money, PVCarrierGroupTotalAllocation money)
INSERT INTO @CarrierGroupAllocation
SELECT AllocationID, CarrierGroupID, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVCarrierGroupTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, CarrierGroupID


SELECT ar.DateToPresentValue, MIN(ar.DateTime) AS [DateTime], 
	SUM(ar.Allocation) AS Allocation, Sum(ar.AllocationIndemnity) AS AllocationIndemnity, 
	SUM(ar.AllocationDefense) AS AllocationDefense, SUM(ar.AllocationDefenseOutsideLimits) AS AllocationDefenseOutsideLimits, 
	SUM(ar.PV) AS PV, SUM(ar.SIRAllocated) AS SIRAllocated, SUM(ar.SIRAllocatedIndemnity) AS SIRIndemnity, SUM(ar.SIRAllocatedDefense) AS SIRDefense,
	p.PolicyDateID, p.PStartDate, p.PEndDate, p.ActualPStartDate, p.ActualPEndDate, p.AnnualPeriod,
	p.CarrierID, CarrierSolvency, CarrierName, 
	coalesce(CarrierGroupname, carriername)as  'CarrierSort', 
	p.CarrierGroupID, CarrierGroupName, 
	p.PolicyID, p.PolicyNum, p.AttachmentPoint, 
	Coalesce(e.PreviouslyExhaustedAggregate,0) AS PreviouslyExhaustedAggregate,
	p.PerOccLimit, p.LayerPerOccLimit, p.SIR, p.AggregateLimit, p.Layer, 
	a.AllocationID, a.AllocationDescription, a.AllocationSchemeID, a.TriggerStartID, a.TriggerStartDate, a.TriggerEndID, a.TriggerEndDate, a.OccurrenceDefId, a.UnCoveredYearstoPH, 
	a.Cost1, a.Cost2, a.Cost3, a.Cost4, a.Cost5, a.Cost6, a.Cost7, a.Cost8, a.Cost9, a.SIRID, a.SIRToPay, a.LessThanAttachmentPoint, a.LessThanLayer, a.OccurrencesInClaim, 
	InsuranceProgram, p.InsuranceProgramID, og.OccurrenceGroup, Treatment, CarrierTotalAllocation, TotalSIR, TotalAllocation,
    PVTotalAllocation, PVCarrierTotalAllocation, CarrierGroupTotalSIR, CarrierGroupTotalAllocation, PVCarrierGroupTotalAllocation
FROM vPolicy p
	INNER JOIN tblAllocationResults ar ON p.PolicyDateID = ar.PolicyDateID 
	INNER JOIN tblAllocation a ON a.AllocationID = ar.AllocationID
	INNER JOIN tblOccurrence o ON ar.occurrenceID = o.occurrenceID
	INNER JOIN tblOccurrenceGroup og ON o.occurrencegroupID = og.occurrencegroupID
	LEFT JOIN @exh e ON e.PolicydateID = p.PolicydateID
	Inner Join (Select * From ztblPolicyAllocation Where AllocationID = @AllocationID) z on z.PolicyDateID = p.PolicyDateID
	Inner Join tblTreatment t ON z.Provision1 = t.TreatmentID 
	LEFT JOIN @Allocation al ON ar.AllocationID = al.AllocationID
	LEFT JOIN @CarrierAllocation ca ON ar.AllocationID = ca.AllocationID AND ca.CarrierID = p.CarrierID
			LEFT JOIN @CarrierGroupAllocation cat ON ar.AllocationID = ca.AllocationID AND cat.CarrierGroupID = p.CarrierGroupID
WHERE a.AllocationID = @allocationID AND p.LimitTypeID = @limitTypeID 
--AND o.OccurrenceGroupID >= COALESCE(@OccurrenceGroupID, 0) AND o.OccurrenceGroupID <= COALESCE(NULLIF(@OccurrenceGroupID, 0), @maxOccurrenceGroupID)
GROUP BY ar.DateToPresentValue, p.PolicyDateID, p.PStartDate, p.PEndDate, p.ActualPStartDate, p.ActualPEndDate, p.AnnualPeriod, 
	p.CarrierID, CarrierSolvency, CarrierName, 
	coalesce(CarrierGroupname, CarrierName), 
	p.CarrierGroupID, CarrierGroupName, 
	p.PolicyID, p.PolicyNum, p.AttachmentPoint, 
	Coalesce(e.PreviouslyExhaustedAggregate,0),
	p.PerOccLimit, p.LayerPerOccLimit, p.SIR, p.AggregateLimit, p.Layer, 
	a.AllocationID, a.AllocationDescription, a.AllocationSchemeID, a.TriggerStartID, a.TriggerStartDate, a.TriggerEndID, a.TriggerEndDate, a.OccurrenceDefId, a.UnCoveredYearstoPH, 
	a.Cost1, a.Cost2, a.Cost3, a.Cost4, a.Cost5, a.Cost6, a.Cost7, a.Cost8, a.Cost9, a.SIRID, a.SIRToPay, a.LessThanAttachmentPoint, a.LessThanLayer, a.OccurrencesInClaim, 
	InsuranceProgram, p.InsuranceProgramID, og.OccurrenceGroup, Treatment, CarrierTotalAllocation, TotalSIR, TotalAllocation,
    PVTotalAllocation, PVCarrierTotalAllocation, CarrierGroupTotalSIR, CarrierGroupTotalAllocation, PVCarrierGroupTotalAllocation
order by PStartDate, p.AttachmentPoint
RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByPolicyByOccurrenceGroupQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByPolicyByOccurrenceGroupQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByPolicyByOccurrenceGroupQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByPolicyByOccurrenceGroupQC] TO [power_user]
GO
