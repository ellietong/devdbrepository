SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimantDocumentsQC] (@ClaimantPersonalInfoID int)

as

Select ClaimantDocumentID, 
	ClaimantPersonalInfoID, 
	ClaimantDocumentType,
	coalesce(DocumentURL, documentlink) as DocumentLink, 'Internal/Historical' as DefenseCounsel,
	'4/1/2010' AS DateAccepted
From  tblClaimantDocuments cd Left Join tblCLaimantDocumentType dt on dt.claimantdocumenttypeID = cd.claimantdocumenttypeID
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID 

Union 
Select DocumentID as ClaimantDocumentID,
ClaimantPersonalInfoID, 
ClaimantDocumentType,
DocumentURL as DocumentLink, 
DefenseCounsel as DefenseCounsel,
DateAccepted
from tblDocuments d
left join tblClaimantDocumentType dt on dt.ClaimantDocumentTypeID = d.DetailDocumentTypeID 
left join tblDefenseCounsel dc on dc.DefenseCounselID = d.FirmID 
where ClaimantPersonalInfoID = @claimantpersonalInfoID


Order By ClaimantDocumentType

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [power_user]
GO
