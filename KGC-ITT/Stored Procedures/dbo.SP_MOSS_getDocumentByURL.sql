SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MOSS_getDocumentByURL]
	@URL nvarchar(4000)
AS
BEGIN
	SELECT COUNT(*) FROM tblDocuments WHERE DocumentURL = @URL
END
GO
