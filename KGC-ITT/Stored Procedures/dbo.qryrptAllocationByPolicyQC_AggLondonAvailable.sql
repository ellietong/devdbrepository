SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryrptAllocationByPolicyQC_AggLondonAvailable]
(@AllocationID INT = NULL)
AS

SET NOCOUNT ON

/**** Access Input Parameters: @AllocationID =
Forms!frmMasterReports!ListAllocation ****/

--DECLARE @AllocationID INT
--SET @AllocationID = 4

DECLARE @Exhaustion TABLE (PolicyDateID INT, Exhaustion MONEY)
INSERT INTO @Exhaustion (PolicyDateID, Exhaustion)
SELECT PolicyDateID, SUM(IndemnityAllocation + DefenseAllocation) AS
Exhaustion
FROM tblExhaustion E
INNER JOIN tblExhaustionDate ED ON ED.ExhaustionDateID = E.ExhaustionDateID
WHERE ExhaustionTypeID IN (SELECT ExhaustionTypeID FROM tblAllocation WHERE
AllocationID = @AllocationID)
GROUP BY PolicyDateID

DECLARE @AvailablePolicyLimits TABLE (PolicyDateID INT,
AvailablePolicyLimits MONEY)
INSERT INTO @AvailablePolicyLimits (PolicyDateID, AvailablePolicyLimits)
SELECT PD.PolicyDateID, COALESCE(AvailableLimits, AggregateLimit) AS
AvailablePolicyLimits
FROM tblPolicy P
INNER JOIN tblPolicyDates PD ON PD.PolicyID = P.PolicyID
INNER JOIN tblPolicyLimits PL ON PL.PolicyID = P.PolicyID
LEFT JOIN dbo.m_AvailableLondonLimitsImport ALLI ON ALLI.PolicyDateID =
PD.PolicyDateID
WHERE LimitTypeID IN (SELECT LimitTypeID FROM tblAllocation WHERE
AllocationID = @AllocationID)
AND InsuranceProgramID IN (SELECT InsuranceProgramID FROM tblAllocation
WHERE AllocationID = @AllocationID)
GROUP BY PD.PolicyDateID, AggregateLimit, AvailableLimits
ORDER BY PD.PolicyDateID

--SELECT * FROM m_AvailableLondonLimitsImport

DECLARE @AllocationDefenseTreatment TABLE (PolicyID INT,
AllocationDefenseTreatment NVARCHAR(100))
INSERT INTO @AllocationDefenseTreatment (PolicyID,
AllocationDefenseTreatment)
SELECT PolicyID, Treatment AS AllocationDefenseTreatment
FROM tblProvision Pr
INNER JOIN tblCombination Co ON Co.ProvisionValueID = Pr.ProvisionValueID
INNER JOIN tblCombinationTreatment CT ON CT.Combination = Co.Combination
INNER JOIN tblTreatment T ON T.TreatmentID = CT.TreatmentID
WHERE CT.ScenarioID IN (SELECT DefenseScenarioID FROM tblAllocation WHERE
AllocationID = @AllocationID)
AND PolicyID IN (SELECT P.PolicyID FROM tblPolicy P INNER JOIN
tblPolicyLimits PL ON PL.PolicyID = P.PolicyID
WHERE InsuranceProgramID IN (SELECT InsuranceProgramID FROM tblAllocation
WHERE AllocationID = @AllocationID) AND LimitTypeID IN (SELECT LimitTypeID
FROM tblAllocation WHERE AllocationID = @AllocationID))
GROUP BY PolicyID, Treatment

--SELECT * FROM tblAllocation
--SELECT * FROM tblScenarioProvisions
--SELECT * FROM tblCombination
--SELECT * FROM tblCombinationTreatment

SELECT P.PolicyID, PD.PolicyDateID, CarrierName, PolicyNum, PStartDate,
PEndDate,
AttachmentPoint, PerOccLimit, LayerPerOccLimit, AggregateLimit,
COALESCE(Exhaustion, 0) AS Exhaustion, AvailablePolicyLimits,
AR.AllocationID, AllocationDescription, AllocationDefenseTreatment,
SUM(AllocationIndemnity) AS AllocationIndemnity, SUM(AllocationDefense) AS
AllocationDefense, SUM(AllocationDefenseOutsideLimits) AS AllocationDOL,
SUM(Allocation) AS TotalAllocation
FROM tblPolicy P
INNER JOIN tblPolicyDates PD ON PD.PolicyID = P.PolicyID
INNER JOIN tblPolicyLimits PL ON PL.PolicyID = P.PolicyID
INNER JOIN tblCarrier C ON C.CarrierID = P.CarrierID
LEFT JOIN @Exhaustion E ON E.PolicyDateID = PD.PolicyDateID
LEFT JOIN @AvailablePolicyLimits APL ON APL.PolicyDateID = PD.PolicyDateID
LEFT JOIN @AllocationDefenseTreatment ADT ON ADT.PolicyID = P.PolicyID
INNER JOIN tblAllocationResults AR ON AR.PolicyDateID = PD.PolicyDateID
INNER JOIN tblAllocation A ON A.AllocationID = AR.AllocationID
WHERE AR.AllocationID = @AllocationID
AND PL.LimitTypeID IN (SELECT LimitTypeID FROM tblAllocation WHERE
AllocationID = @AllocationID)
AND InsuranceProgramID IN (SELECT InsuranceProgramID FROM tblAllocation
WHERE AllocationID = @AllocationID)
GROUP BY P.PolicyID, PD.PolicyDateID, CarrierName, PolicyNum, PStartDate,
PEndDate,
AttachmentPoint, PerOccLimit, LayerPerOccLimit, AggregateLimit, Exhaustion,
AvailablePolicyLimits,
AR.AllocationID, AllocationDescription, AllocationDefenseTreatment
ORDER BY PStartDate, AttachmentPoint, CarrierName
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByPolicyQC_AggLondonAvailable] TO [Allocation_User]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByPolicyQC_AggLondonAvailable] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByPolicyQC_AggLondonAvailable] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByPolicyQC_AggLondonAvailable] TO [base_user]
GO
