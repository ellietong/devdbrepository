SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptPolicyIssues]
(@InsuranceProgramID INT = NULL, @LimitTypeID INT = NULL, @CarrierGroupID INT = NULL, @CarrierID INT = NULL)

AS
SET NOCOUNT ON

--DECLARE @InsuranceProgramID INT
--DECLARE @LimitTypeID INT
--DECLARE @CarrierGroupID INT
--DECLARE @CarrierID INT
--SET @InsuranceProgramID = 1
--SET @LimitTypeID = 1
--SET @CarrierGroupID = 1
--SET @CarrierID = 20

DECLARE @MAXInsuranceProgramID INT
DECLARE @MAXLimitTypeID INT
DECLARE @MAXCarrierID INT
DECLARE @MAXCarrierGroupID INT

-- Find MAX ID Values
SELECT @MAXInsuranceProgramID = MAX(InsuranceProgramID) FROM vPolicy
SELECT @MAXLimitTypeID = MAX(LimitTypeID) FROM vPolicy
SELECT @MAXCarrierID = MAX(CarrierID) FROM vPolicy
SELECT @MAXCarrierGroupID = MAX(CarrierGroupID) FROM vPolicy

IF @CarrierGroupID = 0 SET @CarrierGroupID = NULL


SELECT P.PolicyID,
		CarrierName,
		PolicyNum,
		MIN(PStartDate) AS StartDate,
		MAX(PEndDate) AS EndDate,
		AttachmentPoint,
		PerOccLimit,
		LayerPerOccLimit
FROM tblPolicy P
INNER JOIN tblCarrier C ON C.CarrierID = P.CarrierID
INNER JOIN tblPolicyDates PD ON P.PolicyID = PD.PolicyID
INNER JOIN tblPolicyLimits PL ON PL.PolicyID = P.PolicyID
LEFT JOIN tblIssues I ON I.PolicyID = P.PolicyID
WHERE LimitTypeID >= COALESCE(@LimitTypeID, 0) 
			AND LimitTypeID <= COALESCE(NULLIF(@LimitTypeID, 0), @MAXLimitTypeID)
		AND InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) 
			AND InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @MAXInsuranceProgramID)
		AND CarrierGroupID >= COALESCE(@CarrierGroupID, 0) 
			AND CarrierGroupID <= COALESCE(NULLIF(@CarrierGroupID, 0), @MAXCarrierGroupID)
		AND P.CarrierID >= COALESCE(@CarrierID, 0) 
			AND InsuranceProgramID <= COALESCE(NULLIF(@CarrierID, 0), @MAXCarrierID)
		AND DoNotReport <> 1
		
		AND P.PolicyID IN (SELECT PolicyID
							FROM tblIssues 
							WHERE MngrReviewNeeded = 1)
		GROUP BY P.PolicyID,
		CarrierName,
		PolicyNum,
		AttachmentPoint,
		PerOccLimit,
		LayerPerOccLimit

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyIssues] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyIssues] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyIssues] TO [Policy_User]
GRANT EXECUTE ON  [dbo].[qryrptPolicyIssues] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyIssues] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyIssues] TO [power_user]
GO
