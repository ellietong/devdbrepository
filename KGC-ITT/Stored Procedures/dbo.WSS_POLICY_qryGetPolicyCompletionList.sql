SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryGetPolicyCompletionList]
(@prefixText nvarchar(MAX), @count int = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SET ROWCOUNT @count
	SELECT P.PolicyID, PolicyNum + ' ' + CarrierName +  ' ' +'('+ +CONVERT(nvarchar(100),MIN(PStartDate), 103) + ' - ' + CONVERT(nvarchar(100),MAX(PEndDate), 103) +')' AS PolicyNumber
	FROM tblPolicy P 
	INNER JOIN tblPolicyDates PD ON PD.PolicyID = P.PolicyID
	INNER JOIN tblCarrier WITH (READCOMMITTED)  
	ON P.CarrierID = tblCarrier.CarrierID  
	WHERE PolicyNum LIKE (@prefixText + '%') AND InsuranceProgramID <> 3
	GROUP BY P.PolicyID, PolicyNum, CarrierName
	ORDER BY PolicyNum
	SET ROWCOUNT 0
END	

--PolicyNum, P.PolicyID, CarrierName, PStartDate, PEndDate
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryGetPolicyCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryGetPolicyCompletionList] TO [base_user]
GO
