SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimLawsuitStatusTrackingQC] (@ClaimLawsuitID int)
as

Select 
cl.ClaimLawsuitID,
clst.StatusDate,
clst.StatusNotes,
cls.ClaimLawsuitStatus,
IsPrimaryStatus

From tblClaimLawsuit cl
	Left Join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=cl.ClaimLawsuitID
	Left Join tblClaimLawsuitStatus cls on cls.claimlawsuitstatusid = clst.claimlawsuitstatusID
Where cl.ClaimLawsuitID = @ClaimLawsuitID
Order By IsPrimaryStatus desc, StatusDate desc

Return
GO
