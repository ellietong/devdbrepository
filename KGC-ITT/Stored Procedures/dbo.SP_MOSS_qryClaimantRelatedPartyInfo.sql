SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MOSS_qryClaimantRelatedPartyInfo]
@ClaimantPersonalInfoID bigint=null
AS
Select ClaimantRelatedPartyID, 
	ClaimantPersonalInfoID,  
	LastName + ', ' + CASE WHEN FirstName IS NULL 
    THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
	Relationship, 
	LC.LegalCapacity
From tblClaimantRelatedParty crp 
LEFT JOIN tblRelationship r on r.RelationshipID=crp.RelationshipID
LEFT JOIN tblLegalCapacity LC ON LC.LegalCapacityID = crp.LegalCapacityID
WHERE ClaimantPersonalInfoID=@ClaimantPersonalInfoID
GO
