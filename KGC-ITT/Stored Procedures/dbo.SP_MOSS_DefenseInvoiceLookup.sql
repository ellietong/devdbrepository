SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[SP_MOSS_DefenseInvoiceLookup]
AS
SET NOCOUNT ON

SELECT statusid, status
FROM tblDefenseInvoiceStatus

RETURN
GO
