SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[qryrptCheckLog] 
As
Set NoCount On

Select c.SourceNumber, SourceDate, SourceAmount, Matter, ApprovedBy, ApprovedDate, IsCheckCleared, IsApproved, FundingSource, FundingType
From tblCheck C
Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where ApprovedDate >= (GETDATE()-7)
Order by Matter, ApprovedDate, SourceAmount desc

Return
GO
