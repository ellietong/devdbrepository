SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryProvisionsByPolicyID] (@PolicyID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT Provision, ProvisionValue 
	FROM tblProvision P
	INNER JOIN tblProvisionLabel PL ON P.ProvisionLabelID = PL.ProvisionLabelID
	INNER JOIN tblProvisionValue PV ON P.ProvisionValueID = PV.ProvisionValueID
	WHERE PolicyID = @PolicyID AND PL.ProvisionLabelID NOT IN (14, 17, 18, 20)--AND  p.ProvisionLabelID IN (1, 4, 5, 6, 16, 27)
	ORDER BY Provision
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryProvisionsByPolicyID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryProvisionsByPolicyID] TO [base_user]
GO
