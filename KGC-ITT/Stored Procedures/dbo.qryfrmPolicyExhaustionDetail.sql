SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE  PROCEDURE [dbo].[qryfrmPolicyExhaustionDetail]
(@policyID int = NULL)
AS

SET NOCOUNT ON

SELECT ExhaustionTypeID, [Description], AllocationID, p.PolicyID, pd.PolicyDateID, 
	PStartDate, PEndDate, pl.LimitTypeID, LimitType, PerOccLimit, AggregateLimit,
	SUM(IndemnityAllocation) AS totIndemnity, SUM(DefenseAllocation) AS totDefense,
	SUM(DefenseOutsideLimitsAllocation) AS totDefenseOutsideLimits,
	SUM(SIRIndemnityAllocation) AS totIndemnitySIR, SUM(SIRDefenseAllocation) AS totIndemnitySIRDefense,
	MAX(ExhaustionDate) AS ExhaustionDateMax
FROM tblExhaustion e INNER JOIN tblExhaustionDate ed on e.ExhaustionDateID = ed.ExhaustionDateID
	INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
	INNER JOIN tblPolicy p ON p.PolicyID = pd.PolicyID
	INNER JOIN tblPolicyLimits pl ON pl.PolicyID = p.PolicyID
	INNER JOIN tblPolicyLimitType plt on pl.LimitTypeID = plt.LimitTypeID
WHERE p.PolicyID = COALESCE(@policyID, 0)
GROUP BY ExhaustionTypeID, [Description], AllocationID, p.PolicyID, pd.PolicyDateID, 
	PStartDate, PEndDate, pl.LimitTypeID, LimitType, PerOccLimit, AggregateLimit
ORDER BY ExhaustionTypeID, AllocationID, pl.LimitTypeID, PStartDate

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyExhaustionDetail] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyExhaustionDetail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPolicyExhaustionDetail] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPolicyExhaustionDetail] TO [power_user]
GO
