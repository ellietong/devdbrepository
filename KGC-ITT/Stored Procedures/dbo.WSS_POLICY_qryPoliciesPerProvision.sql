SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryPoliciesPerProvision]
	-- Add the parameters for the stored procedure here
	(@ProvisionLabelID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT DISTINCT el.PolicyID, PolicyNum + '  ' + CarrierName + ' (' + CAST(YEAR(MIN(pd.PStartDate)) AS varchar(max)) + ' - ' + CAST(YEAR(MIN(pd.PEndDate)) AS varchar(max)) + ')' AS PolicyNumber, ProvisionLabelID, AmazonKey, MIN(AnnualPeriod), AttachmentPoint--, ExactLanguage 
FROM tblProvisionLanguage pl
INNER JOIN tblExactLanguage EL on PL.ExactLanguageID = EL.ExactLanguageID
INNER JOIN tblPolicy P ON P.PolicyID = el.PolicyID
INNER JOIN tblCarrier C ON P.CarrierID = C.CarrierID
INNER JOIN tblPolicyDates PD ON pd.PolicyID = p.PolicyID
INNER JOIN tblPolicyLimits pli	ON pli.PolicyID = p.PolicyID
WHERE InsuranceProgramID <> 3 AND ProvisionLabelID = @ProvisionLabelID AND ExactLanguage NOT LIKE '%No Policy Language Found' AND ExactLanguage NOT LIKE '%Policy Language Not Analyzed'
GROUP BY el.PolicyID, CarrierName, ProvisionLabelID, AmazonKey, PolicyNum, AttachmentPoint
ORDER BY MIN(AnnualPeriod), AttachmentPoint, PolicyNumber
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryPoliciesPerProvision] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryPoliciesPerProvision] TO [base_user]
GO
