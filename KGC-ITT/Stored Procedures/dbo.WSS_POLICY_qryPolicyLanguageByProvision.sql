SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryPolicyLanguageByProvision]
(@ProvisionLabelID int, @PolicyID int)
AS
BEGIN

	--DECLARE @PolicyID int = 162
	SET NOCOUNT ON

	
--DECLARE @ProvisionLabelID int 
--SET @ProvisionLabelID = 12

	DECLARE @ColumnListComma nvarchar(MAX)
	DECLARE @ColumnListPlus nvarchar(MAX)
	DECLARE @Spacing nvarchar(10) = '<br/>' -- could also be ', '


	DECLARE @WhereString nvarchar(max)

SET @WhereString =  ' WHERE 1=1'

IF @ProvisionLabelID	  IS NOT NULL   BEGIN SET @WhereString = @WhereString +  ' AND Pl.ProvisionLabelID =' + CONVERT(nvarchar, @ProvisionLabelID) END
 


	
	SET @ColumnListComma = (
	SELECT LEFT(ProvisionLabelList, LEN(ProvisionLabelList)-1) FROM (
	SELECT  DISTINCT
				'[' + Provision + ']' + ', ' AS [text()] 
			FROM 
				tblProvisionLabel
			FOR XML PATH('')  ) pll (ProvisionLabelList))
	        
	        
	        
	SET @ColumnListPlus = (
	SELECT LEFT(ProvisionLabelList, LEN(ProvisionLabelList)-1) FROM (
	SELECT DISTINCT
				'CASE WHEN COALESCE([' + Provision + '], 0) = 1 THEN ''' + REPLACE(Provision, '''', '"') + @Spacing + ''' ELSE '''' END + ' AS [text()] 
			FROM 
				tblProvisionLabel
			FOR XML PATH('')  ) pll (ProvisionLabelList))
	        
	        
	DECLARE @Command nvarchar(MAX)

	SET @Command = 'SELECT ExactLanguageID, ' + @ColumnListPlus  +

	' AS Provisions FROM

	(SELECT  pl.ExactLanguageID, plabel.Provision, COUNT(pl.ExactLanguageID) AS ProvisionCount
	FROM tblExactLanguage el 
	INNER JOIN tblPolicy P ON el.PolicyID = P.PolicyID
	INNER JOIN tblCarrier C ON P.CarrierID = C.CarrierID
	--LEFT JOIN tblProvision Pr ON P.PolicyID = Pr.PolicyID
	--LEFT JOIN tblProvisionValue PV ON Pr.ProvisionValueID = PV.ProvisionValueID
	LEFT JOIN tblProvisionLanguage pl ON el.ExactLanguageID = pl.ExactLanguageID --LEFT
	INNER JOIN tblProvisionLanguage plang ON pl.ProvisionLanguageID = plang.ProvisionLanguageID AND pl.ProvisionLabelID = plang.ProvisionLabelID
	INNER JOIN tblProvisionLabel plabel ON plang.ProvisionLabelID = plabel.ProvisionLabelID
	INNER JOIN tblLocation l ON el.LocationID = l.LocationID' +
	@WhereString + '
	GROUP BY pl.ExactLanguageID, plabel.Provision
	
	)
		AS LanguageProvisionList
	PIVOT

	(

		Sum(ProvisionCount)

	FOR

	Provision

		IN ( ' + @ColumnListComma + ')

	) AS piv
	'
	PRINT @Command
	DECLARE @tblLanguageProvisions TABLE (ExactLanguageID int, Provisions nvarchar(MAX))
	INSERT INTO @tblLanguageProvisions
	EXEC(@Command)


	SELECT  el.ExactLanguageID, el.PolicyID, PolicyNum, PolicyNum + '  ' + CarrierName AS PolicyNumber, LEFT(ExactLanguage, 100) AS LessLanguage, REPLACE(ExactLanguage,CHAR(13),'<br>') AS ExactLanguage, BatesStart, BatesEnd, l.Location, LEFT(REPLACE(REPLACE(Provisions, '&lt;', '<'), '&gt;', '>'), LEN(REPLACE(REPLACE(Provisions, '&lt;', '<'), '&gt;', '>'))-5) AS Provisions, SPLink
	FROM tblExactLanguage el 
	INNER JOIN tblPolicy P ON el.PolicyID = P.PolicyID
	INNER JOIN tblCarrier C ON P.CarrierID = C.CarrierID
	--LEFT JOIN tblProvision Pr ON P.PolicyID = Pr.PolicyID
	--LEFT JOIN tblProvisionValue PV ON Pr.ProvisionValueID = PV.ProvisionValueID	
	--LEFT JOIN tblProvisionLanguage pl ON el.ExactLanguageID = pl.ExactLanguageID
	--INNER JOIN tblProvisionLanguage plang ON pl.ProvisionLanguageID = plang.ProvisionLanguageID AND pl.ProvisionLabelID = plang.ProvisionLabelID
	--INNER JOIN tblProvisionLabel plabel ON plang.ProvisionLabelID = plabel.ProvisionLabelID
	INNER JOIN tblLocation l ON el.LocationID = l.LocationID
	INNER JOIN @tblLanguageProvisions lp ON el.ExactLanguageID = lp.ExactLanguageID
	WHERE EXISTS (SELECT DISTINCT Pr.PolicyID, PolicyNum + '  ' + CarrierName AS PolicyNumber 
			FROM tblProvision Pr
			INNER JOIN tblPolicy P ON P.PolicyID = Pr.PolicyID
			INNER JOIN tblCarrier C ON P.CarrierID = C.CarrierID
			WHERE ProvisionLabelID = @ProvisionLabelID)
			AND el.PolicyID = @PolicyID

	RETURN
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryPolicyLanguageByProvision] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryPolicyLanguageByProvision] TO [base_user]
GO
