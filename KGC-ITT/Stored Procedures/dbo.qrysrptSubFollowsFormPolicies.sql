SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qrysrptSubFollowsFormPolicies]

(@PolicyDateID INT = NULL)
AS
SET NOCOUNT ON

--DECLARE @PolicyDateID INT
--SET @PolicyDateID = 1520

DECLARE @FollowsForm TABLE (FollowsFormID INT)
INSERT INTO @FollowsForm

SELECT FFPolicyDateID
FROM tblFollowsForm FF
WHERE PolicyDateID = @PolicyDateID

DECLARE @FollowsFormInfo TABLE (FFPolicyID INT, FFDateID INT, FFCarrierName
NVARCHAR(1000), FFPolicyNum NVARCHAR(255),
FFStartDate DATETIME, FFEndDate DATETIME, FFAttachmentPoint MONEY,
FFPerOccLimit MONEY, FFLayerPerOccLimit MONEY)
INSERT INTO @FollowsFormInfo

SELECT P.PolicyID, FollowsFormID, CarrierName, PolicyNum, ActualPStartDate,
ActualPEndDate, AttachmentPoint, PerOccLimit, LayerPerOccLimit
FROM tblPolicy P
INNER JOIN tblPolicyDates PD ON PD.PolicyID = P.PolicyID
INNER JOIN tblPolicyLimits PL ON PL.PolicyID = P.PolicyID
INNER JOIN tblCarrier C ON C.CarrierID = P.CarrierID
INNER JOIN @FollowsForm FF ON FF.FollowsFormID = PD.PolicyDateID
WHERE LimitTypeID = 3
GROUP BY P.PolicyID, FollowsFormID, PolicyDateID, CarrierName, PolicyNum,
ActualPStartDate, ActualPEndDate, AttachmentPoint, PerOccLimit,
LayerPerOccLimit

SELECT FFPolicyID, FFDateID, FFCarrierName, FFPolicyNum, MIN(FFStartDate) AS
FFStartDate, MAX(FFEndDate) AS FFEndDate,
FFAttachmentPoint, FFPerOccLimit, FFLayerPerOccLimit
FROM @FollowsFormInfo
GROUP BY FFPolicyID, FFDateID, FFCarrierName, FFPolicyNum,
FFAttachmentPoint, FFPerOccLimit, FFLayerPerOccLimit
ORDER BY FFStartDate, FFAttachmentPoint, FFCarrierName
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSubFollowsFormPolicies] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptSubFollowsFormPolicies] TO [base_user]
GRANT ALTER ON  [dbo].[qrysrptSubFollowsFormPolicies] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSubFollowsFormPolicies] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptSubFollowsFormPolicies] TO [power_user]
GO
