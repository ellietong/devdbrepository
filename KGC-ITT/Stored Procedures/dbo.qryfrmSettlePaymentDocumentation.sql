SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmSettlePaymentDocumentation] (@ClaimLawsuitID int)
as

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 4552

Select *
From tblSettlePaymentDocumentation pd
Where ClaimLawsuitID = @ClaimLawsuitID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [power_user]
GO
