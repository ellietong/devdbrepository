SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryPolicyReport] 
	-- Add the parameters for the stored procedure here
	(@InsuranceProgramID int, @LimitTypeID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @MaxInsuranceProgramID int
SET @MaxInsuranceProgramID = (SELECT MAX (InsuranceProgramID) FROM tblInsuranceProgram)
DECLARE @MaxLimitTypeID int
SET @MaxLimitTypeID = (SELECT MAX (LimitTypeID) FROM tblPolicyLimitType)
    -- Insert statements for procedure here
SELECT DISTINCT P.PolicyID, CarrierName, PolicyNum, AnnualPeriod, PStartDate, PEndDate, Layer, SIR, AttachmentPoint, PerOccLimit, LayerPerOccLimit, AggregateLimit, SPLink
FROM			tblPolicy P
		INNER JOIN tblCarrier C ON P.CarrierID = C.CarrierID
		INNER JOIN tblCarrierGroup CG ON C.CarrierGroupID = CG.CarrierGroupID
		INNER JOIN tblNamedInsured NI ON P.NamedInsuredID = NI.NamedInsuredID
		INNER JOIN tblInsuranceProgram IP ON P.InsuranceProgramID = IP.InsuranceProgramID
		INNER JOIN tblPolicyDates PD ON P.PolicyID = PD.PolicyID
		INNER JOIN tblPolicyLimits PL ON P.PolicyID = PL.PolicyID
		INNER JOIN tblPolicyLimitType PLT ON PL.LimitTypeID = PLT.LimitTypeID
WHERE P.InsuranceProgramID >= COALESCE(@InsuranceProgramID, '') AND P.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, ''), @MaxInsuranceProgramID) 
	AND PL.LimitTypeID >= COALESCE(@LimitTypeID, '') AND PL.LimitTypeID <= COALESCE(NULLIF(@LimitTypeID, ''), @MaxLimitTypeID)
ORDER BY PStartDate
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryPolicyReport] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryPolicyReport] TO [base_user]
GO
