SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryALLSplitPolicies]
(@allocationID int = null, @strWhere varchar(4000) = null, @considerPrevExhaustion bit, @exhaustionTypeID int = NULL, @exhaustionDateID int = NULL, @limitTypeID int = null)
AS
SET NOCOUNT ON

 --2008-05-01 - DTO - modified to correctly track aggregate limits (previously aggregate limits would not be calculated correctly for the very first occurrence
 --2007-07-16 - DTO - modified to correctly track prior exhaustion. Prior exhaustion now does not change 'AggregateLimit' and updates 'Occurrence
 --   Remaining' (as before) and 'AggregateAllocated' and 'AggregateRemaining' (modified). AggregateAllocated is updated by prior exhaustion
 --   and AggregateRemaining is now updated properly, -1 if no aggregate and no prior exhaustion, 0 if an exhausted aggregate, and >0 if
 --   a partially exhausted aggregate.

/*
DECLARE @allocationid int
DECLARE @strWhere varchar(4000)
DECLARE @considerPrevExhaustion bit
DECLARE @ExhaustionTypeID int
DECLARE @ExhaustionDateID int
DECLARE @limitTypeID int

SET @allocationid = 2
SET @strWhere = 'a.PolicyID NOT IN (SELECT PolicyID FROM tblProvision WHERE ProvisionValueID = 250 OR ProvisionValueID = 267 GROUP BY PolicyID) '
SET @considerPrevExhaustion = 0
SET @ExhaustionTypeID = null
SET @ExhaustionDateID = NULL  
SET @limitTypeID =1
--*/

DECLARE @ExhaustionMaxDate Date
SET @ExhaustionMaxDate = (SELECT ExhaustionDate FROM tblExhaustionDate WHERE ExhaustionDateID=@exhaustionDateID)


DECLARE @strSQL1 nvarchar(max)
DECLARE @strSQL2 nvarchar(max)

DELETE 
FROM ztblPolicyAllocation
WHERE AllocationID = @allocationID


SET @strSQL1 = 
    'DECLARE @previousExhaustion table (PolicyDateID int, AllocationTotal money, SIRAllocationTotal money)
            INSERT INTO @previousExhaustion 
            SELECT PolicyDateID, 
            SUM(IndemnityAllocation) + SUM(DefenseAllocation) AS AllocationTotal ,
            SUM(SIRIndemnityAllocation) + SUM(SIRDefenseAllocation) AS SIRAllocationTotal 
            FROM tblExhaustion a INNER JOIN tblExhaustionDate b ON a.ExhaustionDateID = b.ExhaustionDateID
            WHERE ExhaustionTypeID = ' + COALESCE(CONVERT(varchar(10), @exhaustionTypeID), 'NULL') + ' 
            AND ExhaustionDate < = ''' + COALESCE(CONVERT(varchar(20),@ExhaustionMaxDate), 'NULL') + '''
            GROUP BY PolicyDateID  
      
    INSERT INTO ztblPolicyAllocation (AllocationID, PolicyID, PolicyDateID, Provision1, PStartDate, PEndDate,
         AnnualPeriod, InsuranceProgramID, AttachmentPoint, SIR, PerOccLimit, LayerPerOccLimit, AggregateLimit,
         OccurrenceAllocation, OccurrenceRemaining, CumAllocation, CumAllocationIndemnity, CumAllocationDefense,
         CumAllocationDefenseOutsideLimits, CumSIR, CumSIRIndemnity, CumSIRDefense, AggregateAllocated, 
         AggregateRemaining, SIRAllocated, SIRProRation, SIRremaining, Exhausted, ColumnAllocation, ColumnSIR, UpperLimit, ColumnDefense, SIRAggregate)
      
      SELECT ' + convert(varchar(10), @allocationID) + ', 
				a.PolicyID, 
				c.PolicyDateID, 
				Provision1, 
				c.PStartDate, 
				c.PEndDate, 
				c.AnnualPeriod, 
				a.InsuranceProgramID, 
				d.AttachmentPoint, 
				d.SIR, 
				d.PerOccLimit, 
				d.LayerPerOccLimit, 
				d.AggregateLimit, 
				0 AS OccurrenceAllocation, 
				CASE WHEN AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) <= PerOccLimit THEN 
						CASE WHEN ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) > 0 THEN
						CASE WHEN AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) < 0 THEN 0	
							ELSE AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) END
							ELSE CASE WHEN AggregateLimit > 0 THEN AggregateLimit 
							ELSE PerOccLimit END END ELSE PerOccLimit END AS OccurrenceRemaining, 
				0 AS CumAllocation, 
				0 AS CumAllocationIndemnity, 
				0 AS CumAllocationDefense, 
				0 AS CumAllocationDefenseOutsideLimits, 
				0 AS CumSIR, 
				0 AS CumSIRIndemnity, 
				0 AS CumSIRDefense, 
				' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) AS AggregateAllocated, 
				CASE WHEN ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) > 0 THEN 
						CASE WHEN AggregateLimit - COALESCE(AllocationTotal, 0) <= 0 THEN 0	
							ELSE AggregateLimit - COALESCE(AllocationTotal, 0) END
							ELSE CASE WHEN AggregateLimit > 0 THEN AggregateLimit 
							ELSE -1 END END AS AggregateRemaining, 
				0 AS SIRAllocated, 
				0 AS SIRProration, 
				SIR AS SIRRemaining, 
				CASE WHEN CASE WHEN AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) <= PerOccLimit THEN 
						CASE WHEN ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) > 0 THEN
						CASE WHEN AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) < 0 THEN 0	
							ELSE AggregateLimit - ' + convert(varchar(1), @considerPrevExhaustion) + ' * COALESCE(AllocationTotal, 0) END
							ELSE CASE WHEN AggregateLimit > 0 THEN AggregateLimit 
							ELSE PerOccLimit END END ELSE PerOccLimit END < .01 THEN 1 ELSE 0 END AS Exhausted, 
				0 AS ColumnAllocation, 
				0 AS ColumnSIR, 
				LayerPerOccLimit + AttachmentPoint + SIR as UpperLimit, 
				0 as ColumnDefense, 
				CASE WHEN SIRAggregateLimit >0 THEN SIRAggregateLimit - COALESCE(SIRAllocationTotal,0) ELSE -1 END as SIRAggregate 
	FROM tblPolicy a INNER JOIN ztblPolicyDefense b ON a.PolicyID = b.PolicyID 
	INNER JOIN tblPolicyDates c ON a.PolicyID = c.PolicyID
	INNER JOIN tblPolicyLimits d ON a.PolicyID = d.PolicyID
	LEFT JOIN @previousExhaustion e ON e.PolicyDateID = c.PolicyDateID'
 set @strSQL2 =
    '
    WHERE b.Allocationid = ' + convert(varchar(10), @allocationID) + ' AND (d.AttachmentPoint IS NOT NULL) AND (d.PerOccLimit IS NOT NULL) AND 
         (d.PerOccLimit <> 0) AND (d.LayerPerOccLimit IS NOT NULL) AND (d.LayerPerOccLimit >= PerOccLimit) AND 
         (d.LayerPerOccLimit > 0) AND (d.SIR IS NOT NULL) AND (d.AggregateLimit IS NOT NULL) AND 
         (c.PStartDate IS NOT NULL) AND LimitTypeID = ' + convert(varchar(10), @limitTypeID)

IF @strWhere IS NOT NULL OR @strWhere <> ''
      SET @strSQL2 = @strSQL2 + ' AND ' + @strWhere
--print (@strSQL1 + @strSQL2 )
EXEC  (@strSQL1 + @strSQL2)

RETURN




GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLSplitPolicies] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLSplitPolicies] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLSplitPolicies] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLSplitPolicies] TO [power_user]
GO
