SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[qryrptProvisionLanguageByYearByLayer]
(@insuranceProgramID int = NULL, @limitTypeID int = NULL, @carrierID int = NULL, @carrierGroupID int = NULL, @carrierSolvency varchar(50) = NULL, @namedInsuredID int = NULL, 
@lowLayer int = NULL, @highLayer int = NULL, @lowAttach nvarchar(50) = NULL, @highAttach nvarchar(50) = NULL, @selectedProvisions nvarchar(1000) = NULL, @selectedProvisionValues nvarchar(1000) = NULL, @startDate int = NULL, @endDate int = NULL)

AS
SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

--DECLARE @insuranceProgramID int
--SET @insuranceProgramID = 0  
--DECLARE @limitTypeID int
--SET @limitTypeID = 6
--DECLARE @carrierID int
--SET @carrierID = 0 
--DECLARE @carrierGroupID int
--SET @carrierGroupID = 0 
--DECLARE @carrierSolvency varchar(50)
--SET @carrierSolvency = '<<ALL>>' 
--DECLARE @namedInsuredID int
--SET @namedInsuredID = 0 
--DECLARE @lowLayer int
--SET @lowLayer = NULL 
--DECLARE @highLayer int
--SET @highLayer = NULL 
--DECLARE @lowAttach nvarchar(50)
--SET @lowAttach = NULL 
--DECLARE @highAttach nvarchar(50)
--SET @highAttach = NULL 
--DECLARE @startdate int
--SET @startdate = 1960
--DECLARE @enddate int
--SET @enddate = 1970

-- Variables hold maximum values
DECLARE @lowAttachNum money
DECLARE @highAttachNum money
DECLARE @maxInsuranceProgram int
DECLARE @maxLimitTypeID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @solvency varchar(50)
DECLARE @maxLayer int
DECLARE @maxAttach int
DECLARE @maxNamedInsuredID int
DECLARE @maxProvisionLabelID int
DECLARE @maxProvisionValueID int
DECLARE @minProvisionValueID int
DECLARE @maxEndDate int

-- Find max ID values
SELECT @maxInsuranceProgram = MAX(InsuranceProgramID) FROM tblInsuranceProgram
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM tblPolicyLimitType
SELECT @maxCarrierID = MAX(CarrierID) FROM tblCarrier
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) FROM tblCarrierGroup
SELECT @maxLayer = MAX(Layer) FROM tblPolicyLimits
SELECT @maxAttach = MAX(AttachmentPoint) FROM tblPolicyLimits
SELECT @maxNamedInsuredID = MAX(NamedInsuredID) FROM tblNamedInsured
SELECT @maxEndDate = MAX(annualperiod) FROM vPolicy
IF @carrierGroupID = 0 SET @carrierGroupID = NULL

-- Eliminate Null Values
IF @highAttach IS NULL
	SET @highAttachNum = @maxAttach
ELSE 
	SET @highAttachNum = CONVERT(money, @highAttach)
IF @lowAttach IS NULL
	SET @lowAttachNum = 0
ELSE 
	SET @lowAttachNum =  CONVERT(money, @lowAttach)

----Declare the lists of selected IDs; note: these will be passed in, and thus should be commented out in actual stored proc
--DECLARE @selectedProvisions nvarchar(1000)
--SET @selectedProvisions = '20,21'
--DECLARE @selectedProvisionValues nvarchar(1000)
--SET @selectedProvisionValues = '96'

--Declare backup strings of the passed in lists to be used in the ultimate where clauses below
DECLARE @selectedProvisionsList nvarchar(1000)
SET @selectedProvisionsList = @selectedProvisions
DECLARE @selectedProvisionValuesList nvarchar(1000)
SET @selectedProvisionValuesList = @selectedProvisionValues

--Declare the tables to be used for the separated provision and provision value IDs
DECLARE @selectedProvisionsTable TABLE(provisionLabelID int)
DECLARE @selectedProvisionValuesTable TABLE(provisionValueID int)

--Prepare flags if there is only a zero in the table (and thus there should be no filter)
DECLARE @flagProvisionFilter int
DECLARE @flagProvisionValueFilter int
SET @flagProvisionFilter = 0
SET @flagProvisionValueFilter = 0

--Parse out provision and provision value ID strings	
WHILE LEN(@selectedProvisions)>0 BEGIN
	IF CHARINDEX(',',@selectedProvisions) = 0 BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, @selectedProvisions)
		IF CONVERT(int,@selectedProvisions) <> 0
			SET @flagProvisionFilter = 1
		SET @selectedProvisions = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedProvisionsTable
			SELECT CONVERT(int, LEFT(@selectedProvisions, CHARINDEX(',',@selectedProvisions)-1))
		SET @selectedProvisions = RIGHT(@selectedProvisions,LEN(@selectedProvisions)-CHARINDEX(',',@selectedProvisions))
		SET @flagProvisionFilter = 1
	END
END
WHILE LEN(@selectedProvisionValues)>0 BEGIN
	IF CHARINDEX(',',@selectedProvisionValues) = 0 BEGIN
		INSERT INTO @selectedProvisionValuesTable
			SELECT CONVERT(int, @selectedProvisionValues)
		IF CONVERT(int,@selectedProvisionValues) <> 0
			SET @flagProvisionValueFilter = 1
		SET @selectedProvisionValues = ''
	END
	ELSE BEGIN
		INSERT INTO @selectedProvisionValuesTable
			SELECT CONVERT(int, LEFT(@selectedProvisionValues, CHARINDEX(',',@selectedProvisionValues)-1))
		SET @selectedProvisionValues = RIGHT(@selectedProvisionValues,LEN(@selectedProvisionValues)-CHARINDEX(',',@selectedProvisionValues))
		SET @flagProvisionValueFilter = 1
	END
END

---------------------------------------------------------------------------------------
--QUERY DATA
IF @flagProvisionFilter = 1 AND @flagProvisionValueFilter = 1
	SELECT c.CarrierName, I.InsuranceProgram,P.CarrierID, plab.ProvisionLabelID, PVAL.ProvisionValue, PLAB.Provision ,p.policynum as PolicyNumber,pl.attachmentpoint,
		pl.perOccLimit,P.PolicyID, MIN(PD.ActualPStartDate) as PStartDate, MAX(pd.ActualPEndDate) as PEndDate
	FROM  tblPolicy P
		INNER JOIN tblCarrier c ON P.CarrierID = c.CarrierID
		LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID 
		INNER JOIN tblInsuranceProgram I on I.insuranceprogramID = p.insuranceprogramID
		INNER JOIN tblNamedInsured ni on p.NamedInsuredID = ni.NamedInsuredID
		INNER JOIN tblPolicyDates PD ON P.PolicyID = PD.PolicyID
		INNER JOIN tblPolicyLimits PL ON P.PolicyID = PL.PolicyID
		INNER JOIN tblProvision pr ON P.PolicyID = pr.PolicyID
		INNER JOIN tblProvisionLabel PLAB ON pr.ProvisionLabelID = PLAB.ProvisionLabelID
		INNER JOIN tblProvisionValue PVAL ON PVAL.ProvisionValueID = pr.ProvisionValueID
	WHERE  p.InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND 
		pl.LimitTypeID  >= COALESCE(@limitTypeID, 0) AND pl.LimitTypeID  <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
		c.CarrierID >= COALESCE(@carrierID, 0) AND c.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
		(coalesce(cg.CarrierGroupID,0) >= COALESCE(@carrierGroupID, 0) AND coalesce(cg.CarrierGroupID,0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(cg.CarrierGroupID,0) = @carrierGroupID) AND
		ni.NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND ni.NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsuredID) AND	c.CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND pl.Layer BETWEEN COALESCE(@lowLayer,0) AND COALESCE(NullIF(@highLayer,0),@maxLayer) AND
		pl.AttachmentPoint BETWEEN @lowAttachNum AND @highAttachNum AND
		pr.ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) AND 
		(pr.ProvisionValueID IN(SELECT * FROM @selectedProvisionValuesTable) OR (pr.ProvisionValueID IN(SELECT ProvisionValueID FROM tblProvisionValue WHERE ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) AND ProvisionLabelID NOT IN(SELECT ProvisionLabelID FROM tblProvisionValue WHERE ProvisionValueID IN(@selectedProvisionValuesList))))) AND
		DoNotReport <> 1 AND 
		AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
	GROUP BY  c.CarrierName, I.InsuranceProgram, P.Policyid, P.CarrierID, plab.ProvisionLabelID, PVAL.ProvisionValue, PLAB.Provision, p.policynum,pl.attachmentpoint,pl.perOccLimit

ELSE IF @flagProvisionFilter = 1
	SELECT c.CarrierName, I.InsuranceProgram,P.CarrierID, plab.ProvisionLabelID, PVAL.ProvisionValue, PLAB.Provision ,p.policynum as PolicyNumber,pl.attachmentpoint,
		pl.perOccLimit,P.PolicyID, MIN(PD.ActualPStartDate) as PStartDate, MAX(pd.ActualPEndDate) as PEndDate
	FROM  tblPolicy P
		INNER JOIN tblCarrier c ON P.CarrierID = c.CarrierID
		LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID 
		INNER JOIN tblInsuranceProgram I on I.insuranceprogramID = p.insuranceprogramID
		INNER JOIN tblNamedInsured ni on p.NamedInsuredID = ni.NamedInsuredID
		INNER JOIN tblPolicyDates PD ON P.PolicyID = PD.PolicyID
		INNER JOIN tblPolicyLimits PL ON P.PolicyID = PL.PolicyID
		INNER JOIN tblProvision pr ON P.PolicyID = pr.PolicyID
		INNER JOIN tblProvisionLabel PLAB ON pr.ProvisionLabelID = PLAB.ProvisionLabelID
		INNER JOIN tblProvisionValue PVAL ON PVAL.ProvisionValueID = pr.ProvisionValueID
	WHERE  p.InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND 
		pl.LimitTypeID  >= COALESCE(@limitTypeID, 0) AND pl.LimitTypeID  <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
		c.CarrierID >= COALESCE(@carrierID, 0) AND c.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
		(coalesce(cg.CarrierGroupID,0) >= COALESCE(@carrierGroupID, 0) AND coalesce(cg.CarrierGroupID,0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(cg.CarrierGroupID,0) = @carrierGroupID) AND
		ni.NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND ni.NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsuredID) AND	c.CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND pl.Layer BETWEEN COALESCE(@lowLayer,0) AND COALESCE(NullIF(@highLayer,0),@maxLayer) AND
		pl.AttachmentPoint BETWEEN @lowAttachNum AND @highAttachNum AND
		pr.ProvisionLabelID IN(SELECT * FROM @selectedProvisionsTable) AND 
		DoNotReport <> 1 AND 
		AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
	GROUP BY  c.CarrierName, I.InsuranceProgram, P.Policyid, P.CarrierID, plab.ProvisionLabelID, PVAL.ProvisionValue, PLAB.Provision, p.policynum,pl.attachmentpoint,pl.perOccLimit

ELSE
	SELECT c.CarrierName, I.InsuranceProgram,P.CarrierID, plab.ProvisionLabelID, PVAL.ProvisionValue, PLAB.Provision ,p.policynum as PolicyNumber,pl.attachmentpoint,
		pl.perOccLimit,P.PolicyID, MIN(PD.ActualPStartDate) as PStartDate, MAX(pd.ActualPEndDate) as PEndDate
	FROM  tblPolicy P
		INNER JOIN tblCarrier c ON P.CarrierID = c.CarrierID
		LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID 
		INNER JOIN tblInsuranceProgram I on I.insuranceprogramID = p.insuranceprogramID
		INNER JOIN tblNamedInsured ni on p.NamedInsuredID = ni.NamedInsuredID
		INNER JOIN tblPolicyDates PD ON P.PolicyID = PD.PolicyID
		INNER JOIN tblPolicyLimits PL ON P.PolicyID = PL.PolicyID
		INNER JOIN tblProvision pr ON P.PolicyID = pr.PolicyID
		INNER JOIN tblProvisionLabel PLAB ON pr.ProvisionLabelID = PLAB.ProvisionLabelID
		INNER JOIN tblProvisionValue PVAL ON PVAL.ProvisionValueID = pr.ProvisionValueID
	WHERE  p.InsuranceProgramID >= COALESCE(@insuranceProgramID, 0) AND p.InsuranceProgramID <= COALESCE(NULLIF(@insuranceProgramID, 0), @maxInsuranceProgram) AND 
		pl.LimitTypeID  >= COALESCE(@limitTypeID, 0) AND pl.LimitTypeID  <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID) AND
		c.CarrierID >= COALESCE(@carrierID, 0) AND c.CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID) AND
		(coalesce(cg.CarrierGroupID,0) >= COALESCE(@carrierGroupID, 0) AND coalesce(cg.CarrierGroupID,0) <= COALESCE(@carrierGroupID, @maxCarrierGroupID) OR coalesce(cg.CarrierGroupID,0) = @carrierGroupID) AND
		ni.NamedInsuredID >= COALESCE(@NamedInsuredID, 0) AND ni.NamedInsuredID <= COALESCE(NULLIF(@NamedInsuredID, 0), @maxNamedInsuredID) AND	c.CarrierSolvency LIKE COALESCE(NULLIF(@carrierSolvency, '<<ALL>>'), '%') AND pl.Layer BETWEEN COALESCE(@lowLayer,0) AND COALESCE(NullIF(@highLayer,0),@maxLayer) AND
		pl.AttachmentPoint BETWEEN @lowAttachNum AND @highAttachNum AND
		DoNotReport <> 1 AND 
		AnnualPeriod >= COALESCE(@startDate, 0) AND AnnualPeriod <= COALESCE(NULLIF(@enddate, 0), @maxenddate)
	GROUP BY  c.CarrierName, I.InsuranceProgram, P.Policyid, P.CarrierID, plab.ProvisionLabelID, PVAL.ProvisionValue, PLAB.Provision, p.policynum,pl.attachmentpoint,pl.perOccLimit

SET CONCAT_NULL_YIELDS_NULL ON

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptProvisionLanguageByYearByLayer] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptProvisionLanguageByYearByLayer] TO [base_user]
GRANT CONTROL ON  [dbo].[qryrptProvisionLanguageByYearByLayer] TO [power_user]
GRANT TAKE OWNERSHIP ON  [dbo].[qryrptProvisionLanguageByYearByLayer] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptProvisionLanguageByYearByLayer] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptProvisionLanguageByYearByLayer] TO [power_user]
GO
