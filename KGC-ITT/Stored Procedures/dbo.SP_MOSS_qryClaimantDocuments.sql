SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_MOSS_qryClaimantDocuments]
(@ClaimantPersonalInfoID bigint = null)
AS
BEGIN
SELECT     d.DocumentID, d.SPDocId, d.DocumentTitle, d.DocumentURL, dg.DocumentGroup, cdt.ClaimantDocumentType, 
                      cpi.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, f.Firm, d.AcceptedBy, d.DateAccepted, d.DateCreated, d.ClaimantPersonalInfoID 
FROM         dbo.tblDocuments AS d INNER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID INNER JOIN
                      dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID INNER JOIN
                      dbo.tblClaim AS c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                      dbo.vtblFirm AS f ON f.FirmID = d.FirmID
WHERE d.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
END
GO
