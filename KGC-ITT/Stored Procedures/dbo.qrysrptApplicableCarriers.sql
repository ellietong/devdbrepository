SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[qrysrptApplicableCarriers]
(@correspondenceid int)
as
set nocount on
select correspondenceid, carriername
from tblcarrier c 
inner join tblCarrierContact cc on cc.CarrierID = c.CarrierID
inner join tblCorrCarrierContact ccc on ccc.CarrierContactID=cc.CarrierContactID
where correspondenceid = @correspondenceid
return
grant execute on dbo.qrysrptApplicableCarriers to power_user, base_user
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptApplicableCarriers] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptApplicableCarriers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptApplicableCarriers] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptApplicableCarriers] TO [power_user]
GO
