SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryAnnualPeriodList]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @AnnualPeriod TABLE (AnnPer varchar(5))
INSERT INTO @AnnualPeriod
SELECT DISTINCT AnnualPeriod FROM tblPolicyDates

SELECT DISTINCT AnnPer
FROM @AnnualPeriod
UNION
SELECT ''
ORDER BY AnnPer

END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryAnnualPeriodList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryAnnualPeriodList] TO [base_user]
GO
