SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MOSS_qryClaimantEmploymentInfo]
@ClaimantPersonalInfoID bigint=null
AS
Select ce.ClaimantEmploymentID, 
	ce.ClaimantPersonalInfoID, 
	Occupation, 
	Jobsite, 
	Comments, 
	EmpStartDate, 
	EmpEndDate, 
	EarliestExposureDate, 
	LatestExposureDate, 
	IsPrimary, 
	ProductManufacturer, 
	pt.ProductType,
	ProductBrand,
	pn.ProductName
From tblClaimantEmployment ce
left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
left join tblProduct p on p.ProductID = cp.ProductID
left join tblProductName pn on pn.ProductNameID=p.ProductNameID
left join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
left join tblProductManufacturer pm on pm.ProductManufacturerID=p.ProductManufacturerID
left join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
WHERE ce.ClaimantPersonalInfoID=@ClaimantPersonalInfoID
GO
