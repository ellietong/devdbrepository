SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryrptFollowsFormPolicies]

(@PolicyID INT = NULL)
AS
SET NOCOUNT ON

--DECLARE @PolicyID INT
--SET @PolicyID = 169

DECLARE @Provisions TABLE (PolicyID INT, Provision NVARCHAR(100),
ProvisionValue NVARCHAR(255))
INSERT INTO @Provisions

SELECT PolicyID, Provision, ProvisionValue
FROM tblProvision Pr
INNER JOIN tblProvisionLabel PrL ON PrL.ProvisionLabelID =
Pr.ProvisionLabelID
INNER JOIN tblProvisionValue PrV ON PrV.ProvisionValueID =
Pr.ProvisionValueID
WHERE Pr.ProvisionLabelID = 15
GROUP BY PolicyID, Provision, ProvisionValue

--SELECT * FROM tblProvisionLabel ORDER BY Provision

DECLARE @Policy TABLE (PolicyID INT, PolicyDateID INT, CarrierName
NVARCHAR(1000), PolicyNum NVARCHAR(255), StartDate DATETIME, EndDate
DATETIME,
AttachmentPoint MONEY, PerOccLimit MONEY, LayerPerOccLimit MONEY)
INSERT INTO @Policy

SELECT P.PolicyID, PolicyDateID, CarrierName, PolicyNum, ActualPStartDate,
ActualPEndDate, AttachmentPoint, PerOccLimit, LayerPerOccLimit
FROM tblPolicy P
INNER JOIN tblPolicyDates PD ON PD.PolicyID = P.PolicyID
INNER JOIN tblPolicyLimits PL ON PL.PolicyID = P.PolicyID
INNER JOIN tblCarrier C ON C.CarrierID = P.CarrierID
WHERE LimitTypeID = 3
GROUP BY P.PolicyID, PolicyDateID, CarrierName, PolicyNum,
ActualPStartDate, ActualPEndDate, AttachmentPoint, PerOccLimit,
LayerPerOccLimit

SELECT A.PolicyID, PolicyDateID, CarrierName, PolicyNum, MIN(StartDate) AS
StartDate, MAX(EndDate) AS EndDate,
AttachmentPoint, PerOccLimit, LayerPerOccLimit,
Provision, ProvisionValue
FROM @Policy A
INNER JOIN @Provisions B ON B.PolicyID = A.PolicyID
WHERE A.PolicyID = @PolicyID
GROUP BY A.PolicyID, PolicyDateID, CarrierName, PolicyNum,
AttachmentPoint, PerOccLimit, LayerPerOccLimit,
Provision, ProvisionValue
ORDER BY StartDate, AttachmentPoint, CarrierName

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptFollowsFormPolicies] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptFollowsFormPolicies] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptFollowsFormPolicies] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFollowsFormPolicies] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptFollowsFormPolicies] TO [power_user]
GO
