SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryInternalInvoiceEmailUpdate]
AS
Begin
/*Query run daily to send an internal email listing any invoices which have been approved or rejected
and are ready to be QC'd. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(2000)

SET @Subject =    'Demo Client: New Defense Invoices Approved/Rejected as of ' + CONVERT(varchar, GETDATE(), 107) 
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>Below are any newly approved/rejected invoices for review.</H4>'

--Declare firm counter
DECLARE @FirmCount int
SET @FirmCount = 0

--Create temp table for invoices with new status changes
DECLARE @NewInvoicesTable TABLE (spdocid nvarchar(100), firmID int)
INSERT INTO @NewInvoicesTable
SELECT SPDocID, d.FirmID      
FROM tblDocuments d
WHERE DateApproved BETWEEN GETDATE()-1 AND GETDATE() 
      AND detailDocumentTypeID = 0
      and PATINDEX('%/dcp/%', DocumentURL) = 0
      and d.FirmID NOT IN (37)
GROUP BY d.firmID, SPDocId

--Gather the new invoice information into a cursor object
DECLARE @invoicecount int
DECLARE @firmID int
DECLARE @firmName nvarchar(100)
DECLARE @firmInvoiceURL nvarchar(100)
set @firmInvoiceURL =  'https://admin.kcicllc.com/cp/dcp/Defense%20Counsel%20Invoices/Forms/AllItems.aspx'
DECLARE Invoice_Curs CURSOR FOR
SELECT count(spdocid), f.firmID, firm
FROM @NewInvoicesTable i
      INNER JOIN vtblfirm f ON i.firmID = f.firmID
GROUP BY f.firmID, firm

--Open the invoice cursor
OPEN Invoice_Curs

--Fetch the first record from the cursor
FETCH NEXT FROM Invoice_Curs INTO @Invoicecount, @firmID, @firmName

WHILE @@FETCH_STATUS = 0 BEGIN
      SET @FirmCount = @FirmCount + 1
      SET @BodyMessage = @BodyMessage  + CONVERT(nvarchar(10), (SELECT @invoicecount)) + ' New Invoices Were Approved or Rejected in the <A HREF=' +
            (select @firmInvoiceURL) + '>' + @firmName + '</a> Invoice Library.<br><br>'
      FETCH NEXT FROM Invoice_Curs INTO @Invoicecount, @firmID, @firmName
END

CLOSE Invoice_Curs
DEALLOCATE Invoice_Curs

SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person respoDemo Clientble for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'


--Only send email if there are new status updates.
IF (@FirmCount > 0)     
      EXEC msdb.dbo.sp_send_dbmail
      @profile_name='Admin',
      @recipients = 'selfl@kcicllc.com; gearyj@kcicllc.com; shockleym@kcicllc.com; johnn@kcicllc.com; montgomeryi@kcicllc.com; dayc@kcicllc.com; Demo Client@kcicllc.com',
      @subject= @subject,
      @body= @BodyMessage,
      @body_format = 'html'

End
GO
