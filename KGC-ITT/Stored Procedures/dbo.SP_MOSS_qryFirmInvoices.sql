SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[SP_MOSS_qryFirmInvoices](@emailaddress nvarchar (1000)=null)
AS

DECLARE @firmid int
SET @firmid = (SELECT DISTINCT d.FirmID FROM tblDocuments d	
INNER JOIN vtblPerson p ON d.FirmID = p.FirmID WHERE p.Email = @emailaddress)

IF ((@FirmID = 24) OR (LEFT(@emailaddress,5) = 'kcic\')) 
BEGIN
	SELECT CAST(d.DocumentID as varchar(50)) as DocumentId,
		'Invoices' as ClaimantDocumentType, 
		'Invoices' as DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm
	FROM tblDocuments d
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	WHERE d.ClaimantPersonalInfoID IS NULL
END 
ELSE IF (@firmid = 42)
BEGIN
	SELECT CAST(d.DocumentID as varchar(50)) as DocumentId,
		'Invoices' as ClaimantDocumentType, 
		'Invoices' as DocumentGroup, 	
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm
	FROM tblDocuments d
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	WHERE d.ClaimantPersonalInfoID IS NULL AND d.FirmID = 24

END 
ELSE BEGIN
	SELECT 
		cast(d.DocumentID as varchar(50)) as DocumentId,
		'Invoices' as ClaimantDocumentType, 
		'Invoices' as DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm
	FROM tblDocuments d
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	WHERE d.ClaimantPersonalInfoID IS NULL 
		AND f.FirmID = @firmid
END
GO
