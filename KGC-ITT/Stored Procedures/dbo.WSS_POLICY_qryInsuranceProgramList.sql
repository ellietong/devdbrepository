SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryInsuranceProgramList]
	-- Add the parameters for the stored procedure here
AS	
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT InsuranceProgramID, InsuranceProgram
FROM tblInsuranceProgram
WHERE InsuranceProgramID <> 3
UNION
SELECT NULL, ''
ORDER BY InsuranceProgram
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryInsuranceProgramList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryInsuranceProgramList] TO [base_user]
GO
