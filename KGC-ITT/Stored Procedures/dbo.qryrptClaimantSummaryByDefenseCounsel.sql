SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptClaimantSummaryByDefenseCounsel] ( @ResponsibleDefenseCounselAddressID int,
							@ClaimLawsuitStatusID int,
							@BeginDate datetime,
							@EndDate datetime,
							@MatterID int)

AS
Set NoCount ON

--DECLARE @ResponsibleDefenseCounselAddressID int
--SET @ResponsibleDefenseCounselAddressID = 8
--DECLARE @CLAIMLAWSUITSTATUSID INT
--SET @CLAIMLAWSUITSTATUSID = 0
--DECLARE @BEGINDATE DATETIME
--SET @BEGINDATE = '1/1/1900'
--DECLARE @ENDDATE DATETIME
--SET @ENDDATE = '1/1/2012'
--declare @matterid int
--set @matterid = null

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

Declare @AllResponsibleDefenseCounsel table (DefenseCounselAddressID int, 
				DefenseCounsel nvarchar(100), 
				ResponsibleDefenseCounselAddressID int,
				StateID int, 
				ResponsibleDefenseCounsel nvarchar(100),
				IsActiveCounsel bit)
Insert Into @AllResponsibleDefenseCounsel
Select dcs.DefenseCounselAddressID,
	d.DefenseCounsel+' - '+d.DefenseCounselCity as DefenseCounsel,
	dcs.ResponsibleDefenseCounselAddressID,
	StateID as StateID,
	r.DefenseCounsel+' - '+r.DefenseCounselCity  as ResponsibleDefenseCOunsel,
	IsActiveCounsel
From tblDefenseCounselState dcs 
	Inner Join tblDefenseCounselAddress d on d.DefenseCounselAddressID = dcs.DefenseCounselAddressID
	Inner JOin tblDefenseCounselAddress r on r.DefenseCounselAddressID = dcs.ResponsibleDefenseCounselAddressID
Group By d.DefenseCounsel+' - '+d.DefenseCounselCity, r.DefenseCounsel+' - '+r.DefenseCounselCity, dcs.DefenseCounselAddressID, dcs.ResponsibleDefenseCounselAddressID, IsActiveCounsel, StateID


-- Find Max ID Values
Declare @MaxResponsibleDefenseCounselAddressID int
Declare @MaxClaimLawsuitStatusID int

--Define Max State ID
Select @MaxResponsibleDefenseCounselAddressID = MAX(ResponsibleDefenseCounselAddressID) From @AllResponsibleDefenseCounsel
If @ResponsibleDefenseCounselAddressID = 0 Set @ResponsibleDefenseCounselAddressID = Null



--Define Max Resolution Type ID
Select @MaxClaimLawsuitStatusID = MAX(ClaimLawsuitStatusID) From tblClaimLawsuitStatus
If @ClaimLawsuitStatusID = 0 Set @ClaimLawsuitStatusID = Null


Select Matter,
'BLANK MODEL CLIENT' as ClientName, 
	s.StateID, s.State, 
	Jurisdiction, 
	ClaimSequence, 
	LastName, 
	FirstName,
	MiddleName, 
	'XXXX-XX-'+right(convert(nvarchar(9), SocialSecurityNumber), 4) as SocialSecurityNumber, 
	hld.highleveldisease, 
	ClaimLawsuitStatus, 
	ServiceDate, 
	FileDate, 
	DocketNumber,
	CaseCaption, 
	LeadPlaintiffFirstName, 
	LeadPlaintiffLastName,
	PlaintiffCounsel as PrimaryLocalPlaintiffCounsel,
	DefenseCounsel as PrimaryDefenseCounsel 


From tblClaimantPersonalInfo CPI
	Left Join tblClaim C on CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
	LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
	Left Join tblClaimLawsuit CL on C.ClaimID = CL.ClaimID
	Left Join tblLawsuit L on CL.LawsuitID = L.LawsuitID
	Left Join tblState S on S.StateID = L.StateID
	Left Join tblJurisdiction J on J.JurisdictionID = L.JurisdictionID
	Left Join tblClaimSequence cs on cs.ClaimSequenceID = C.ClaimSequenceID
	Left Join tblDisease D on cd.DiseaseID=D.DiseaseID
	Left Join tblHighLevelDisease HLD on D.HighLevelDiseaseID=HLD.HighLevelDiseaseID
	Left Join tblClaimLawsuitStatusTracking clst on CL.ClaimLawsuitID= clst.ClaimLawsuitID
	Left Join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
	INNER Join @AllResponsibleDefenseCounsel r on r.DefenseCounselAddressID = l.DefenseCounselAddressID and r.StateID = l.StateID
	Left Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
	Left JOIN tblMatter m on m.MatterID=c.MatterID
Where	
	IsPrimaryForClaim = 1 
	and IsActiveCounsel = 1
	AND clst.IsPrimaryStatus=1
    AND ((clst.ClaimLawsuitStatusID >= COALESCE(@ClaimLawsuitStatusID, 0) AND clst.ClaimLawsuitStatusID <= COALESCE(@ClaimLawsuitStatusID, @MaxClaimLawsuitStatusID)))
	AND ((ResponsibleDefenseCounselAddressID >= COALESCE(@ResponsibleDefenseCounselAddressID, 0) AND ResponsibleDefenseCounselAddressID <= COALESCE(@ResponsibleDefenseCounselAddressID, @MaxResponsibleDefenseCounselAddressID)))
	AND (FileDate >= coalesce(nullif(@BeginDate,''),'1/1/1800') OR FileDate IS NULL)
	AND (FileDate <= COALESCE(NULLIF(@EndDate,''),'1/1/2050') OR FileDate IS NULL)
	and m.MatterID >= COALESCE(@MatterID, 0) AND m.MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
Order By J.Jurisdiction, LastName

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantSummaryByDefenseCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantSummaryByDefenseCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantSummaryByDefenseCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantSummaryByDefenseCounsel] TO [power_user]
GO
