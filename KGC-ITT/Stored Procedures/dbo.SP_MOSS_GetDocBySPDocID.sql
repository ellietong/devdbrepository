SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MOSS_GetDocBySPDocID]
	@SPDocId Varchar(50)
AS
--DECLARE @SPDocId nvarchar(50)
--SET @SPDocId = 'c43ef74a-898e-40bb-a64f-a09ae1b9ed25'
BEGIN
	SELECT
	d.CreatedBy,
	d.Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	d.FirmID,
	cdt.DocumentGroupID
FROM tblDocuments d
INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
WHERE SPDocId = @SPDocId
END
GO
