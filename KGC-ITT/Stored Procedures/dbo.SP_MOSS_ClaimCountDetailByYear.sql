SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[SP_MOSS_ClaimCountDetailByYear]
AS
SELECT     CASE WHEN COALESCE (YEAR(ServiceDate), YEAR(FileDate)) IS NULL THEN '2001' ELSE COALESCE (YEAR(ServiceDate), YEAR(FileDate)) 
                      END AS Year, SUM(dc.MesoCount) AS MesoFilings, SUM(dc.LungCancerCount) AS LCFilings, SUM(dc.OtherCancerCount) AS OCFilings, 
                      SUM(dc.NonMaligCount) AS NMFILINGS, SUM(dc.SilicosisCount) AS SilicosisCount, SUM(dc.UnknownCount) AS UnknownFilings, COUNT(c.ClaimID) 
                      AS NewFilings
FROM         (SELECT     c.ClaimID, CASE WHEN d.HighLevelDiseaseID = 1 THEN 1 ELSE 0 END AS MesoCount, 
									CASE WHEN d.HighLevelDiseaseID  = 2 THEN 1 ELSE 0 END AS LungCancerCount, 
									CASE WHEN d.HighLevelDiseaseID  = 3 THEN 1 ELSE 0 END AS OtherCancerCount, 
                                    CASE WHEN d.HighLevelDiseaseID = 4 THEN 1 ELSE 0 END AS NonMaligCount,
                                    CASE WHEN d.HighLevelDiseaseID = 5 THEN 1 WHEN d.HighLevelDiseaseID IS NULL THEN 1 ELSE 0 END AS UnknownCount, 
                                    CASE WHEN d.HighLevelDiseaseID = 7 THEN 1 ELSE 0 END AS SilicosisCount
                       FROM          dbo.tblClaim AS c LEFT OUTER JOIN
									(Select * from tblClaimDisease where PrimaryDisease = 1) as cd on cd.ClaimID = c.ClaimID left join
                                              dbo.tblDisease AS d ON cd.DiseaseID = d.DiseaseID) AS dc LEFT OUTER JOIN
                      dbo.tblClaim AS c ON dc.ClaimID = c.ClaimID LEFT OUTER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID LEFT OUTER JOIN
                      dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                      dbo.tblState AS s ON l.StateID = s.StateID
WHERE     (cl.IsPrimaryForClaim = 1)
GROUP BY CASE WHEN COALESCE (YEAR(ServiceDate), YEAR(FileDate)) IS NULL THEN '2001' ELSE COALESCE (YEAR(ServiceDate), YEAR(FileDate)) END
GO
