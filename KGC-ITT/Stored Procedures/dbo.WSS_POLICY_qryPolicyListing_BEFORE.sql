SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryPolicyListing_BEFORE]
(@minYear int = null, @maxYear int = null)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --DECLARE @MinYear int = 1954
    --DECLARE @MaxYear int = 1985

    DECLARE @Wherestring nvarchar(max)
    SET @Wherestring = ''
    IF(@minYear IS NOT NULL)
		SET @Wherestring = @Wherestring + ' AND YEAR(PstartDate) >= ' + CONVERT(nvarchar, @minYear)
	IF(@maxYear IS NOT NULL)
		SET  @Wherestring = @Wherestring + ' AND YEAR(PEndDate) <= ' + CONVERT(nvarchar, @maxYear)
    
--Figure out how to label policies by which # they are in a quota share -- order by carrier name then number?
	DECLARE @cmd nvarchar(MAX) = '
    
    
			DECLARE @TotalShare TABLE (PolicyNumber nvarchar(500), policyID int, StartDate datetime, EndDate datetime, totalShare float, layer int)--, policiesInShare int)
	    INSERT INTO @TotalShare
	SELECT DISTINCT PolicyNum, policyid, Min(PStartDate) AS start, MAX(PEndDate) AS [End], (COALESCE(PerOccLimit, 0)/COALESCE(LayerPerOccLimit, 0)) AS Share, Layer--, COUNT(LayerPerOccLimit) AS policiesinshare
	FROM vPolicy 
	WHERE LimitTypeID = 1 
		AND PerOccLimit <> LayerPerOccLimit 
		AND PolicyID NOT IN (30, 77) 
	GROUP BY PolicyNum, PolicyID, (COALESCE(PerOccLimit, 0)/COALESCE(LayerPerOccLimit, 0)), Layer
	ORDER BY Min(PStartDate)
	
		DECLARE @PStartDate TABLE (disStartdate date, cod int, layer int)
	INSERT into @PStartDate
	SELECT DISTINCT StartDate, COUNT(StartDate), layer FROM @TotalShare
	GROUP BY StartDate, layer
	ORDER BY StartDate

		DECLARE @TotalShareALL TABLE (PolicyNumber nvarchar(500), policyID int, StartDate datetime, EndDate datetime, totalShare float, layer int, policiesInShare int)
	    INSERT INTO @TotalShareALL
	SElect ts.*, cod FROM @TotalShare ts
	inner join @PStartDate pd on ts.StartDate = pd.disStartdate AND ts.layer = pd.layer
	where StartDate in (SELECT disStartdate  FROM @PStartDate)
	GROUP BY StartDate, PolicyNumber, policyID, EndDate, totalShare, ts.layer, cod
	
	SELECT policies.*, trueTotal AS totalShare, COALESCE(ts.policiesInShare, 1) AS PoliciesInShare FROM @TotalShareALL ts
	RIGHT JOIN (
	SELECT CarrierNickname, PolicyID, PolicyNum, PolicyPDF, Min(PStartDate) AS StartDate, MAX(PEndDate) AS EndDate, Layer, PerOccLimit, AttachmentPoint, p.SIR, PreviouslyExhaustedAggregate AS Exhausted, LayerPerOccLimit, PerOccLimit/LayerPerOccLimit AS Share,
	ROW_NUMBER() OVER(PARTITION BY MIN(PstartDate), Layer ORDER BY CarrierNickName DESC) AS ''ShareNumber''
	FROM vPolicy p
	WHERE InsuranceProgramID <> 3 AND LimitTypeID = 1 AND PolicyID NOT IN (30, 77) '
	+ @Wherestring +
	'GROUP BY CarrierNickname, PolicyID, PolicyNum, PolicyPDF, Layer, PerOccLimit, AttachmentPoint, p.SIR, PreviouslyExhaustedAggregate, LayerPerOccLimit
	) policies ON ts.PolicyNumber = policies.PolicyNum AND ts.StartDate = policies.StartDate AND ts.EndDate = policies.EndDate
	LEFT JOIN (SELECT StartDate, EndDate, Layer, SUM(COALESCE(TotalShare, 0)) AS TrueTotal FROM @TotalShareALL GROUP BY StartDate, EndDate, Layer) total  ON policies.StartDate = total.StartDate AND policies.EndDate = total.EndDate AND policies.Layer = total.layer
	ORDER BY policies.StartDate, policies.EndDate, AttachmentPoint ASC'
	PRINT @cmd
	EXEC sp_executesql @Cmd
END

GO
