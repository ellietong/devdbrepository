SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryPolicySearch] 
	-- Add the parameters for the stored procedure here
	(@PolicyID nvarchar(50), @CarrierGroupID int, @CarrierID int, @InsuranceProgramID int, @AnnualPeriodID int, @LimitTypeID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @MaxCarrierGroupID int
SET @MaxCarrierGroupID = (SELECT MAX(CarrierGroupID) FROM tblCarrierGroup)
DECLARE @MaxCarrierID int
SET @MaxCarrierID = (SELECT MAX (CarrierID) FROM tblCarrier)
DECLARE @MaxInsuranceProgramID int
SET @MaxInsuranceProgramID = (SELECT MAX (InsuranceProgramID) FROM tblInsuranceProgram)
DECLARE @MaxAnnualPeriodID int
SET @MaxAnnualPeriodID = (SELECT MAX (AnnualPeriod) FROM tblPolicyDates)
DECLARE @MaxLimitTypeID int
SET @MaxLimitTypeID = (SELECT MAX (LimitTypeID) FROM tblPolicyLimitType)



--SET @WhereString = ' AND 1=1'
DECLARE @WhereString nvarchar(max)
SET @WhereString = '1=1 AND P.InsuranceProgramID <> 3 '
IF @PolicyID IS NOT NULL
BEGIN
SET @WhereString = @WhereString + ' AND P.PolicyID = ' + @PolicyID
END
--ELSE
--BEGIN
--SET @WhereString = ''
--END
IF @CarrierGroupID IS NOT NULL BEGIN SET @WhereString = @WhereString + 'AND C.CarrierGroupID >='+ CONVERT(nvarchar(20),COALESCE(@CarrierGroupID, 0)) +' AND C.CarrierGroupID <='+ CONVERT(nvarchar(20),COALESCE(@CarrierGroupID, @MaxCarrierGroupID))+'' END 
IF @CarrierID IS NOT NULL BEGIN SET @WhereString = @WhereString + 'AND P.CarrierID >='+ CONVERT(nvarchar(20),COALESCE(@CarrierID, 0)) +' AND P.CarrierID <='+ CONVERT(nvarchar(20),COALESCE(@CarrierID, @MaxCarrierID))+'' END 
IF @InsuranceProgramID IS NOT NULL BEGIN SET @WhereString = @WhereString + 'AND P.InsuranceProgramID >='+ CONVERT(nvarchar(20),COALESCE(@InsuranceProgramID, 0)) +' AND P.InsuranceProgramID <='+ CONVERT(nvarchar(20),COALESCE(@InsuranceProgramID, @MaxInsuranceProgramID))+'' END 
IF @AnnualPeriodID IS NOT NULL BEGIN SET @WhereString = @WhereString + 'AND AnnualPeriod >='+ CONVERT(nvarchar(20),COALESCE(@AnnualPeriodID, 0))+ ' AND AnnualPeriod <=' + CONVERT(nvarchar(20),COALESCE(@AnnualPeriodID, @MaxAnnualPeriodID))+'' END 
IF @LimitTypeID IS NOT NULL BEGIN SET @WhereString = @WhereString + 'AND PL.LimitTypeID >='+ CONVERT(nvarchar(20),COALESCE(@LimitTypeID, '')) +'AND PL.LimitTypeID <='+ CONVERT(nvarchar(20),COALESCE(NULLIF(@LimitTypeID, ''), @MaxLimitTypeID))+'' END 
    -- Insert statements for procedure here
DECLARE @QueryString nvarchar(max)
SET @QueryString = '    
SELECT P.PolicyID, MIN(PStartDate) AS PStartDate, MAX(PEndDate) AS PEndDate, CarrierName, CarrierGroupName, PolicyNum, NamedInsured, InsuranceProgram, AttachmentPoint, PerOccLimit, LayerPerOccLimit, AggregateLimit, AmazonKey  
FROM tblPolicy P
		INNER JOIN tblCarrier C ON P.CarrierID = C.CarrierID
		LEFT JOIN tblCarrierGroup CG ON C.CarrierGroupID = CG.CarrierGroupID
		INNER JOIN tblNamedInsured NI ON P.NamedInsuredID = NI.NamedInsuredID
		INNER JOIN tblInsuranceProgram IP ON P.InsuranceProgramID = IP.InsuranceProgramID
		INNER JOIN tblPolicyDates PD ON P.PolicyID = PD.PolicyID
		INNER JOIN tblPolicyLimits PL ON P.PolicyID = PL.PolicyID
		INNER JOIN tblPolicyLimitType PLT ON PL.LimitTypeID = PLT.LimitTypeID
WHERE   
		
		 '+ @WhereString +'
GROUP BY P.PolicyID, CarrierName, CarrierGroupName, PolicyNum, NamedInsured, InsuranceProgram, AttachmentPoint, PerOccLimit, LayerPerOccLimit, AggregateLimit, AmazonKey
ORDER BY P.PolicyID, PolicyNum, Min(PStartDate), MAX(PEndDate)'
--Print @Querystring
EXEC sp_executesql @QueryString
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryPolicySearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryPolicySearch] TO [base_user]
GO
