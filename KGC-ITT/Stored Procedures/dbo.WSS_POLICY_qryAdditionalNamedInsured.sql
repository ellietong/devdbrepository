SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryAdditionalNamedInsured]
	-- Add the parameters for the stored procedure here
	(@PolicyID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT AdditionalNamedInsID, NamedInsured, ANI.AdditionalNamedInsTypeID, AdditonalNamedInsType, Bates, EffectiveDate
FROM tblAdditionalNamedInsured ANI
INNER JOIN tblNamedInsured NI ON ANI.NamedInsuredID = NI.NamedInsuredID
LEFT JOIN tblAdditionalNamedInsuredType ANIT ON ANI.AdditionalNamedInsTypeID = ANIT.AdditionalNamedInsTypeID
WHERE PolicyID = @PolicyID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryAdditionalNamedInsured] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryAdditionalNamedInsured] TO [base_user]
GO
