SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[SP_MOSS_qryDocumentType]

AS
SET NOCOUNT ON
--DECLARE @invoiceFlag bit
--SET @invoiceFlag = 0

BEGIN
	Select 
		ClaimantDocumentTypeID, 
		ClaimantDocumentType, DocumentGroupID
	from tblClaimantDocumentType
	WHERE DocumentGroupID IN (1, 3, 4)
	ORDER BY ClaimantDocumentType
END
GO
