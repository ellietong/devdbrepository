SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptQuotaShareByYearByLayer]


(@LimitTypeID INT)
AS
SET NOCOUNT ON

--DECLARE @LimitTypeID INT
--SET @LimitTypeID = 1

SELECT P.PolicyID, PolicyDateID, CarrierName, PolicyNum, 
MIN(ActualPStartDate) AS StartDate, MAX(ActualPEndDate) AS EndDate, 
AttachmentPoint, PerOccLimit, LayerPerOccLimit, Layer, (PerOccLimit)/(LayerPerOccLimit) AS PercentOfLayer
FROM tblPolicy P
INNER JOIN tblPolicyDates PD ON PD.PolicyID = P.PolicyID
INNER JOIN tblPolicyLimits PL ON PL.PolicyID = P.PolicyID
INNER JOIN tblCarrier C ON C.CarrierID = P.CarrierID
WHERE PerOccLimit < LayerPerOccLimit and LimitTypeID = @LimitTypeID 
GROUP BY P.PolicyID, PolicyDateID, CarrierName, PolicyNum,
AttachmentPoint, PerOccLimit, LayerPerOccLimit, Layer, (PerOccLimit)/(LayerPerOccLimit)
ORDER BY StartDate, AttachmentPoint, Layer


RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptQuotaShareByYearByLayer] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptQuotaShareByYearByLayer] TO [base_user]
GO
