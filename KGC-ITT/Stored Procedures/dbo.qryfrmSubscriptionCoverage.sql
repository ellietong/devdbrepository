SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmSubscriptionCoverage]
(@policyID int = NULL)
AS

IF @policyID IS NOT NULL
	SELECT * 
	FROM tblSubscriptionCoverage
	WHERE PolicyID = @policyID
ELSE
	SELECT *
	FROM tblSubscriptionCoverage

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriptionCoverage] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriptionCoverage] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSubscriptionCoverage] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSubscriptionCoverage] TO [power_user]
GO
