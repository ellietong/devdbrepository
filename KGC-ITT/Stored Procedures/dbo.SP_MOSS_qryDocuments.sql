SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[SP_MOSS_qryDocuments]
( @DocumentID varchar(50) = null  )
AS
--DECLARE @DocumentID nvarchar(50)
--SET @DocumentID = '2ae2e080-ecee-43f3-af9c-bd3aea3ad8b1'
SET NOCOUNT ON
DECLARE @ConvertedDocID varchar(50)

SELECT 
	cast(d.DocumentID as varchar(50)) as DocumentId,
	dt.ClaimantDocumentType, 
	dg.DocumentGroup, 		
	d.Comments,
	d.DocumentURL,
	d.DocumentTitle,
	d.SPDocId,
	d.CreatedBy,
	d.DateCreated,
	d.ModifiedBy,
	d.DateModified,
	d.Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	d.ApprovedBy,
	d.DateApproved,
	f.Firm,

	--Settlement Info Only
	NULL as SettlementGroupID,
	NULL as SettlementDate,
	NULL as SettlementAmount,

	--Settlements and Claimants
	NULL as Claimant,
	NULL as SocialSecurityNumber,
	NULL as BirthDate,
	NULL as disease, 	
	NULL as claimlawsuitstatus, 
	NULL as IsCurrentClaim, 
	
	--not used for invoices
	NULL as docketnumber, 
	NULL as CaseCaption,
	NULL as stateabb, 
	
	--lawsuits only
	NULL as jurisdiction,
	NULL as LeadPlaintiff,	
	NULL as filedate, 
	NULL as servicedate, 
	NULL as primarylocalplaintiffcounsel,

	--Invoices Only
	d.InvoiceNumber,
	d.Period,
	d.DatePayable,
	d.FeeAmount,
	d.DisbursementAmount,
	d.Adjustment,
	d.AdjustmentNotes,
	dis.Status,	
	dif.Firm DefenseInvoiceFirm, 	
	di.SubmitDate, 			

	
	--Court Documents Only
	NULL as DateOfFiling,
	NULL as DateOfDeath,
	DocketNumbers	

	
FROM tblDocuments d
	JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
	JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
	--invoices only
	LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
	LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.FirmID = dif.FirmID 
	LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
WHERE (d.DetailDocumentTypeID = 0 OR dg.DocumentGroupID = 2)
	AND (
	@DocumentID is null 
	or convert(varchar, d.DocumentID) = @DocumentID
	or d.SPDocId = @DocumentId --on the doc profile page we pass the sp guid for security purposes
	)

--STEP TWO: Get Settlements and Claimants
UNION

SELECT 
	cast(d.DocumentID as varchar(50)) as DocumentId,
	dt.ClaimantDocumentType, 
	dg.DocumentGroup, 		
	d.Comments,
	d.DocumentURL,
	d.DocumentTitle,
	d.SPDocId,
	d.CreatedBy,
	d.DateCreated,
	d.ModifiedBy,
	d.DateModified,
	d.Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	d.ApprovedBy,
	d.DateApproved,
	f.Firm,
	
	--Settlement Info Only
	d.SettlementGroupID,
	d.SettlementDate,
	d.SettlementAmount,

	--Settlements and Claimants
	COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, '') AS 'Claimant',
	CASE WHEN cpi.SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(cpi.SocialSecurityNumber,4) END AS SocialSecurityNumber,
	cpi.BirthDate,
	di2.disease, 	
	cls.claimlawsuitstatus, 
	c.IsCurrentClaim, 	
	
	--these values used for settlements and claimants, different joins used for lawsuits
	l.docketnumber, 
	l.CaseCaption,
	st.[State], 
	j.jurisdiction,
	
	--lawsuits only	
	NULL as LeadPlaintiff,	
	NULL as filedate, 
	NULL as servicedate, 
	NULL as primarylocalplaintiffcounsel,

	--Invoices Only
	NULL as InvoiceNumber,
	NULL as Period,
	NULL as DatePayable,
	NULL as FeeAmount,
	NULL as DisbursementAmount,
	NULL as Adjustment,
	NULL as AdjustmentNotes,
	NULL as Status,	
	NULL as DefenseInvoiceFirm, 	
	NULL as SubmitDate, 		

	--Court Documents Only
	NULL as DateOfFiling,
	NULL as DateOfDeath,
	DocketNumbers	
	
FROM tblDocuments d
	JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
	JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
	--settlements and claimants only
	LEFT OUTER JOIN tblClaimantPersonalInfo cpi ON d.claimantpersonalinfoid = cpi.claimantpersonalinfoid 
	LEFT OUTER JOIN tblClaim c ON cpi.claimantpersonalinfoid = c.claimantpersonalinfoid
	LEFT OUTER JOIN (SELECT * FROM tblClaimDisease CD WHERE PrimaryDisease = 1) CD ON CD.ClaimID = C.ClaimID
	LEFT OUTER JOIN tblDisease di2 ON CD.DiseaseID = di2.diseaseid
	LEFT OUTER JOIN tblClaimLawsuit cl ON c.claimid = cl.claimid and cl.IsPrimaryForClaim = 1
	LEFT OUTER JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
	LEFT OUTER JOIN tblState st ON l.stateid = st.stateid
	LEFT OUTER JOIN tblJurisdiction j ON l.jurisdictionid = j.jurisdictionid
	LEFT OUTER JOIN tblClaimLawsuitStatus cls ON cl.claimlawsuitstatusid = cls.claimlawsuitstatusid
	LEFT OUTER JOIN tblClaimantSettlement cs ON cl.claimlawsuitid = cs.claimlawsuitid
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
WHERE dg.DocumentGroupId in(1,4,5)
	AND (
	@DocumentID is null 
	--or cast(d.DocumentID as varchar(50)) = @DocumentID
	or convert(varchar, d.DocumentID) = @DocumentID
	or d.SPDocId = @DocumentId --on the doc profile page we pass the sp guid for security purposes
	)
	AND IsCurrentClaim = 1

-- STEP THREE: Get Lawsuits
UNION

SELECT 
	cast(d.DocumentID as varchar(50)) as DocumentId,
	dt.ClaimantDocumentType, 
	dg.DocumentGroup, 		
	d.Comments,
	d.DocumentURL,
	d.DocumentTitle,
	d.SPDocId,
	d.CreatedBy,
	d.DateCreated,
	d.ModifiedBy,
	d.DateModified,
	d.Accepted,
	d.AcceptedBy,
	d.DateAccepted,
	d.ApprovedBy,
	d.DateApproved,
	f.Firm,
    
	--Settlement Info Only
	NULL as SettlementGroupID,
	NULL as SettlementDate,
	NULL as SettlementAmount,

	--Settlements and Claimants
	COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, '') AS 'Claimant',
	NULL as SocialSecurityNumber,
	NULL as BirthDate,
	NULL as disease, 	
	NULL as claimlawsuitstatus, 
	NULL as IsCurrentClaim, 	
	
	--settlements and claimants, values through different joins for lawsuits
	l.docketnumber, 
	l.CaseCaption,
	s.[State], 
	
	--lawsuits only
	j.jurisdiction,
	l.leadplaintifflastname+', '+l.leadplaintifffirstname AS LeadPlaintiff,	
	l.filedate, 
	l.servicedate, 
	l.primarylocalplaintiffcounsel,

	--Invoices Only
	NULL as InvoiceNumber,
	NULL as Period,
	NULL as DatePayable,
	NULL as FeeAmount,
	NULL as DisbursementAmount,
	NULL as Adjustment,
	NULL as AdjustmentNotes,
	NULL as Status,	
	NULL as DefenseInvoiceFirm, 	
	NULL as SubmitDate, 	
	
	--Court Documents Only
	NULL as DateOfFiling,
	NULL as DateOfDeath,
	DocketNumbers
	

FROM tblDocuments d
	LEFT OUTER JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
	LEFT OUTER JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
	--lawsuits only
	LEFT OUTER JOIN tblLawsuit l ON d.lawsuitid = l.lawsuitid
	LEFT OUTER JOIN tblJurisdiction j ON l.jurisdictionid = j.jurisdictionid
	LEFT OUTER JOIN tblState s ON l.stateid = s.stateid
	LEFT OUTER JOIN tblDefenseCounselState sid ON l.DefenseCounselAddressID = sid.defensecounseladdressid AND l.stateid = sid.stateid
	LEFT OUTER JOIN tblDefenseCounselAddress dca ON sid.responsibledefensecounseladdressid = dca.defensecounseladdressid 
	LEFT OUTER JOIN tblDefenseCounsel dc on dc.DefenseCounselID=dca.defensecounselID
	LEFT OUTER JOIN tblClaimLawsuit cl ON l.lawsuitid = cl.lawsuitid
	LEFT OUTER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	LEFT OUTER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
	LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
	LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
WHERE dg.documentgroupid = 3
	AND (
	@DocumentID is null 
	--or cast(d.DocumentID as varchar(50)) = @DocumentID
	or convert(varchar, d.DocumentID) = @DocumentID
	or d.SPDocId = @DocumentId --on the doc profile page we pass the sp guid for security purposes
	)
GROUP BY cast(d.DocumentID as varchar(50)), dt.ClaimantDocumentType, dg.DocumentGroup, d.Comments, d.DocumentURL, d.DocumentTitle, d.SPDocId,
	d.CreatedBy, d.DateCreated, d.ModifiedBy, d.DateModified, d.Accepted, d.AcceptedBy, d.DateAccepted, d.ApprovedBy, d.DateApproved,
	--settlements and claimants, values through different joins for lawsuits
	l.docketnumber, l.CaseCaption, s.[State], 
	--lawsuits only
	j.jurisdiction, l.leadplaintifflastname+', '+l.leadplaintifffirstname,	l.filedate, l.servicedate, l.primarylocalplaintiffcounsel, f.Firm, COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, ''), DocketNumbers

RETURN
GO
