SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_POLICY_qryProvisionValueList]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT ProvisionValueID, ProvisionValue
	FROM tblProvisionValue PV
	WHERE ProvisionValueID IN (SELECT DISTINCT ProvisionValueID FROM tblProvision)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_POLICY_qryProvisionValueList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_POLICY_qryProvisionValueList] TO [base_user]
GO
