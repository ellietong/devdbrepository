SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryALLDefenseTreatment]
(@AllocationID int, @Scenario int)
AS
SET NOCOUNT ON

DELETE 
FROM ztblPolicyDefense
WHERE AllocationID = @allocationID


----Will be input scenario
--DECLARE @Scenario int
--SET @Scenario = 1
--DECLARE @AllocationID int
--SET @AllocationID = 1

--Create temp table
CREATE TABLE #tempPolicyCombos(PolicyID int)

--Create temp table
CREATE TABLE #tempCurrentCombos(ComboID int)

--Create string to know what the columns of your pivot table should be
DECLARE @pivotColumns nvarchar(2000)
SET @pivotColumns = ''
SELECT @pivotColumns =   @pivotColumns + '[' + cast(ProvisionLabelID AS nvarchar(10)) + '],'
FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @pivotColumns = LEFT(@pivotColumns, (CASE WHEN LEN(@pivotColumns) < 1 then 1 else LEN(@pivotColumns) end) - 1)

--Create string to know what the columns of your  temp table should be 
--(same columns as @pivotColums just different format for how it will be used)
DECLARE @alterColumns nvarchar(2000)
SET @alterColumns = ''
 SELECT @alterColumns = @alterColumns + '[' + cast(ProvisionLabelID AS varchar) + '] int,'
 FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @alterColumns = LEFT(@alterColumns, (CASE WHEN LEN(@alterColumns) < 1 then 1 else LEN(@alterColumns) end) - 1)

--Create a string to see if any of the provisions are not coded for a particular policy
--(same columns as @pivotColums just different format for how it will be used)
DECLARE @SumofColumns nvarchar(2000)
SET @SumofColumns = ''
 SELECT @SumofColumns = @SumofColumns + '[' + cast(ProvisionLabelID as varchar) + '] +'
 FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @SumofColumns = LEFT(@SumofColumns, (CASE WHEN LEN(@SumofColumns) < 1 then 1 else LEN(@SumofColumns) end) - 1)


--Create a string to see if any of the provisions are not coded for a particular policy
--(same columns as @pivotColums just different format for how it will be used)
DECLARE @ColumnsJoin nvarchar(2000)
SET @ColumnsJoin = ''
 SELECT @ColumnsJoin = @ColumnsJoin + 'a.[' + cast(ProvisionLabelID as varchar) + '] = b.['+ cast(ProvisionLabelID as varchar) + '] AND '
 FROM tblScenarioProvisions WHERE ScenarioID = @Scenario
SET @ColumnsJoin = LEFT(@ColumnsJoin, (CASE WHEN LEN(@ColumnsJoin) < 1 then 4 else LEN(@ColumnsJoin) end) - 4)

--select @ColumnsJoin


DECLARE @sql nvarchar(4000)
--Update temp table for policy data to have appropriate columns
SET @sql = 'ALTER TABLE #tempPolicyCombos
ADD ' + @alterColumns
EXEC(@sql)
--perform transform on Policy Provision data and insert it into temp table
SET @sql = 'INSERT #tempPolicyCombos
SELECT  * 
FROM
(SELECT policyid, provisionlabelid, provisionvalueid
FROM tblProvision) datatable
pivot
(MIN(provisionvalueid) for provisionlabelid in (' + @pivotColumns + ')) AS pvt'
EXEC (@sql)


--Update temp table for current combination data to have appropriate columns
SET @sql = 'ALTER TABLE #tempCurrentCombos
ADD ' + @alterColumns
EXEC(@sql)
--perform transform on current combination data and insert it into temp table  
SET @sql = 'INSERT #tempCurrentCombos
select * 
from
(select combination, provisionlabelid, provisionvalueid
from tblcombination where combination in (Select Combination from
tblCombinationTreatment where scenarioid = ' +CONVERT(nvarchar(50), @scenario) + ' )) datatable
pivot
(min(provisionvalueid) for provisionlabelid in ('+@pivotColumns+ ')) pivottable'
EXEC (@sql)



SET @sql= '
select PolicyID, ' + CONVERT(nvarchar(50),@allocationID) + ' as allocationid, ScenarioID, treatmentID
from #tempCurrentCombos a
inner join #tempPolicyCombos b on ' + @ColumnsJoin + ' 
inner join tblCombinationTreatment c on c.combination = a.comboID
where ScenarioID = ' + CONVERT(nvarchar(50), @scenario)
INSERT INTO ztblPolicyDefense
EXEC (@sql)

--/*Add Not Analyzed for GAP Records*/
--INSERT INTO ztblPolicyDefense
--SELECT PolicyID, @AllocationID, @Scenario, 1
--FROM tblPolicy
--WHERE CarrierID = 10


DROP TABLE #tempPolicyCombos
DROP TABLE #tempCurrentCombos



RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLDefenseTreatment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLDefenseTreatment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLDefenseTreatment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLDefenseTreatment] TO [power_user]
GO
