SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmAllocationOccurrenceSortOrder]
(@allocationID int)
As

SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @allocationID = 23

SELECT SortOrderAllocationID, AllocationID, SortOrderID
FROM tblAllocationSortOrder
WHERE AllocationID = @allocationID
ORDER BY SortOrderAllocationID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationOccurrenceSortOrder] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationOccurrenceSortOrder] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationOccurrenceSortOrder] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationOccurrenceSortOrder] TO [power_user]
GO
