SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[m_qryrptPolicyReport_FollowsForm_Deliverable_NoPrimaries]
As
set nocount on

DECLARE @provisionLabelID int = NULL
DECLARE @limitTypeID int = NULL

SET @provisionLabelID = 15
SET @limitTypeID = 1



DECLARE @Provisions TABLE (PolicyID INT, Provision nvarchar(100), ProvisionValue nvarchar(600))
INSERT INTO @Provisions

SELECT PolicyID, Provision, ProvisionValue
FROM vProvisions pv
WHERE ProvisionLabelID = @provisionLabelID
GROUP BY PolicyID, Provision, ProvisionValue

SELECT p.PolicyID,
pd.PolicyDateID, 
CarrierName,
PolicyNum, 
ActualPStartDate, 
ActualPEndDate, 
AnnualPeriod, 
PerOccLimit, 
LayerPerOccLimit, 
AttachmentPoint, 
prov.Provision, 
prov.ProvisionValue
FROM tblPolicy p Inner Join tblPolicyDates pd on pd.PolicyID = p.PolicyID 
      Inner Join tblPolicyLimits pl on pl.PolicyID = p.PolicyID 
      Inner Join tblCarrier c on c.CarrierID = p.CarrierID
      Left Join @Provisions prov on prov.PolicyID = p.PolicyID 
Where LimitTypeID = @limitTypeID 
and PStartDate between '12/31/1979' and '12/31/1980'
 and p.CarrierID = 20
GROUP BY p.PolicyID,
pd.PolicyDateID, 
CarrierName,
PolicyNum, 
ActualPStartDate, 
ActualPEndDate, 
AnnualPeriod, 
PerOccLimit, 
LayerPerOccLimit, 
AttachmentPoint, 
prov.Provision, 
prov.ProvisionValue
Order By CarrierName, ActualPStartDate, AttachmentPoint 


RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[m_qryrptPolicyReport_FollowsForm_Deliverable_NoPrimaries] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_qryrptPolicyReport_FollowsForm_Deliverable_NoPrimaries] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_qryrptPolicyReport_FollowsForm_Deliverable_NoPrimaries] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_qryrptPolicyReport_FollowsForm_Deliverable_NoPrimaries] TO [power_user]
GO
