CREATE ROLE [power_user]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'power_user', N'brownp'
GO
EXEC sp_addrolemember N'power_user', N'donzer'
GO
EXEC sp_addrolemember N'power_user', N'fowlerh'
GO
EXEC sp_addrolemember N'power_user', N'hagep'
GO
EXEC sp_addrolemember N'power_user', N'heymanm'
GO
EXEC sp_addrolemember N'power_user', N'sochurekn'
GO
