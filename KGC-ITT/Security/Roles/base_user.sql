CREATE ROLE [base_user]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'base_user', N'blairo'
GO
EXEC sp_addrolemember N'base_user', N'brownp'
GO
EXEC sp_addrolemember N'base_user', N'chaudoinr'
GO
EXEC sp_addrolemember N'base_user', N'crookc'
GO
EXEC sp_addrolemember N'base_user', N'donzer'
GO
EXEC sp_addrolemember N'base_user', N'fowlerh'
GO
EXEC sp_addrolemember N'base_user', N'gemouless'
GO
EXEC sp_addrolemember N'base_user', N'gutzlern'
GO
EXEC sp_addrolemember N'base_user', N'hagep'
GO
EXEC sp_addrolemember N'base_user', N'hankee'
GO
EXEC sp_addrolemember N'base_user', N'heymanm'
GO
EXEC sp_addrolemember N'base_user', N'hubbardb'
GO
EXEC sp_addrolemember N'base_user', N'kanga'
GO
EXEC sp_addrolemember N'base_user', N'KCIC\malikm'
GO
EXEC sp_addrolemember N'base_user', N'KCICLLC\lislev'
GO
EXEC sp_addrolemember N'base_user', N'labellj'
GO
EXEC sp_addrolemember N'base_user', N'maloneyd'
GO
EXEC sp_addrolemember N'base_user', N'monahanc'
GO
EXEC sp_addrolemember N'base_user', N'montgomeryc'
GO
EXEC sp_addrolemember N'base_user', N'owenss'
GO
EXEC sp_addrolemember N'base_user', N'prossc'
GO
EXEC sp_addrolemember N'base_user', N'shockleym'
GO
EXEC sp_addrolemember N'base_user', N'sochurekn'
GO
EXEC sp_addrolemember N'base_user', N'youngc'
GO
GRANT CREATE PROCEDURE TO [base_user]
GRANT CREATE VIEW TO [base_user]
