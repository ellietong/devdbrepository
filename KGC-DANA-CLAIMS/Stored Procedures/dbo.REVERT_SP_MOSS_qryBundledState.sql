SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryBundledState]

AS
BEGIN
	
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Distinct StateFiled AS [State]
	From m_tblBundledLawsuit
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryBundledState] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryBundledState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryBundledState] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryBundledState] TO [power_user]
GO
