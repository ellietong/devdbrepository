SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysrptSummaryReportByCarrierByPolicy_NonSig]
(@AllocationID int, @LimitTypeID int, @PolicyID int)
As
Set Nocount on



--Declare @LimitTypeID int
--Declare @AllocationID int
--Set @AllocationID = 52
--Set @LimitTypeID = 4

Declare @ExhaustionTypeID int
Set @ExhaustionTypeID = (Select ExhaustionTypeID from tblAllocation where AllocationID = @AllocationID)
Declare @Sig int
Set @Sig= (Select SignatoryRun From tblAllocation where AllocationID = @AllocationID)

Declare @PreviousExhaustionTotals table (PolicyID int, PreviousAllocationAmt money)
Insert Into @PreviousExhaustionTotals
Select p.policyid, --pd.PolicyDateID,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAllocationAmt
From tblExhaustion E
INner Join tblExhaustionDate ed on ed.ExhaustionDateID=e.ExhaustionDateID
inner join tblPolicyDates pd on pd.PolicyDateID=e.PolicyDateID
inner join tblPolicy p on p.PolicyID=pd.policyid
Where ExhaustionTypeID = @ExhaustionTypeID
Group By p.policyid, pd.PolicyDateID

--Current Allocation
Declare @CurrentAllocation1 Table (PolicyDateID bigint,
	PolicyID int,
	Indemnity money, Defense money, DefenseOutsideLimits money, totaldefense money,  SIRIndemnity money,
	SIRDefense money)
Insert Into @CurrentAllocation1
Select pd.PolicyDateID,
	p.policyid,
	coalesce(Sum(AllocationIndemnity), 0) as Indemnity,
	coalesce(Sum(AllocationDefense), 0) as Defense,
	coalesce(Sum(AllocationDefenseOutsideLimits),0) as DefenseOutsideLimits,
	coalesce(Sum(AllocationDefense + AllocationDefenseOutsideLimits),0) as TotalDefense,
	coalesce(Sum(SIRAllocatedIndemnity), 0) as SIRIndemnity,
	coalesce(Sum(SIRAllocatedDefense), 0) as SIRDefense
From tblAllocationResults AR
inner join tblOccurrence o on o.OccurrenceID = AR.OccurrenceID
inner join tblPolicyDates pd on pd.PolicyDateID=AR.PolicyDateID
inner join tblPolicy p on p.PolicyID=pd.PolicyID
Where AllocationID = @AllocationID
Group BY pd.PolicyDateID, p.policyid

Declare @Final table (OrderID int, CarrierName nvarchar(max), Layer int, PolicyID int, PstartDate datetime, PEndDate datetime, AttachmentPoint money, DefenseForm nvarchar(1), 
						Indemnity money, Defense Money, DefenseOutsideLimits money, TotalDefense money, PreviousAllocationAmt money, Balace money, Adjustments money, DanaReportingStatus nvarchar(max))
Insert Into @Final
--London
select 1 as OrderID, c.carriername, pl.layer, Coalesce(llp1.ReportingPolicyID,llp2.ReportingPolicyID, p.policyID) as ReportingPolicyID, 
pd.pstartdate, pd.penddate,	pl.attachmentpoint, 
	CASE	When provisionvalueID=2 then 'H'
			when provisionvalueID=3 then 'G'
			when provisionvalueID=4 then 'I' end as DefenseForm,
	coalesce(Indemnity,0)*Sum(Percentage) as Indemnity, 
	coalesce(Defense,0)*Sum(Percentage) as Defense, 
	coalesce(DefenseOutsideLimits,0)*Sum(Percentage) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0)*Sum(Percentage) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0)*Sum(Percentage) as PreviousAllocationAmt,
 	(coalesce(AggregateLimit,0)*Sum(Percentage) - coalesce(Indemnity,0)*Sum(Percentage) - coalesce(Defense,0)*Sum(Percentage) - coalesce(PreviousAllocationAmt,0)*Sum(Percentage)) as Balance,
 		0 as Adjustments, DanaReportingStatus
	
from tblpolicy p 
	Left Join tblpolicydates pd on p.policyid = pd.policyid
	Left Join @PreviousExhaustionTotals PET on P.PolicyID=PET.PolicyID
	inner Join @CurrentAllocation1 Ca on Ca.PolicyDateID = pd.PolicyDateID
	inner Join tblcarrier c on c.carrierid = p.carrierid
	Left Join tblSubscriptionCoverage sc on p.PolicyID=sc.PolicyID
	Left Join tblSubscriberCompany subc on sc.SubscriberID=Subc.subscriberID
	Left Join mtblDanaSubscriberReportingStatus dsrs on dsrs.subscriberid = subc.SubscriberID
	Left Join mtblDanaReportingStatus drs on dsrs.Danareportingstatusid = drs.Danareportingstatusid
	Left Join tblpolicylimits pl on p.policyid = pl.policyid
	Left Join tblprovision pro on p.policyID=pro.policyID
	Left Join mtblLinkingLondonPolicies llp1 on llp1.InsolventCommutedPolicyID=p.PolicyID
	Left Join mtblLinkingLondonPolicies llp2 on llp2.SolventPolicyID=p.PolicyID
Where limittypeid = @LimitTypeID and provisionlabelID=1 and C.CarrierID=20 and ca.PolicyID=@PolicyID
group by c.carriername, pl.layer, p.policynum, Indemnity, Defense, DefenseOutsideLimits, 
pd.pstartdate, pd.penddate, pl.attachmentpoint,
	pl.perocclimit, pl.aggregatelimit, 
	totaldefense, 
	pro.ProvisionValueID,
	subc.SubscriptionCompanySolvency, PreviousAllocationAmt, DanaReportingStatus, Coalesce(llp1.ReportingPolicyID,llp2.ReportingPolicyID,p.policyid)
order by PStartDate, AttachmentPoint, DanaReportingStatus


Declare @base table (PolicyID int, DanaReportingStatus nvarchar(max), Percentage float)
Insert Into @base
Select p.PolicyID, DanaReportingStatus, round(SUM(Percentage),4)
from tblpolicy p 
	Left Join tblSubscriptionCoverage sc on p.PolicyID=sc.PolicyID
	Left Join tblSubscriberCompany subc on sc.SubscriberID=Subc.subscriberID
	Left Join mtblDanaSubscriberReportingStatus dsrs on dsrs.subscriberid = subc.SubscriberID
	Left Join mtblDanaReportingStatus drs on dsrs.Danareportingstatusid = drs.Danareportingstatusid
Group by p.PolicyID, DanaReportingStatus

	
Select OrderID int, CarrierName, Layer, f.policyid as PolicyID, PstartDate, PEndDate, AttachmentPoint, DefenseForm, 
						Indemnity, Defense, DefenseOutsideLimits, TotalDefense, PreviousAllocationAmt, Balace, Adjustments, coalesce(b.DanaReportingStatus, 
						f.DanaReportingStatus) as DanaReportingStatus, b.percentage
From @Final f
Left Join @base b on b.PolicyID=f.PolicyID and b.DanaReportingStatus=f.DanaReportingStatus	
Order by PstartDate, AttachmentPoint, b.DanaReportingStatus

GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSummaryReportByCarrierByPolicy_NonSig] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptSummaryReportByCarrierByPolicy_NonSig] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSummaryReportByCarrierByPolicy_NonSig] TO [Billing_User]
GRANT EXECUTE ON  [dbo].[qrysrptSummaryReportByCarrierByPolicy_NonSig] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSummaryReportByCarrierByPolicy_NonSig] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qrysrptSummaryReportByCarrierByPolicy_NonSig] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptSummaryReportByCarrierByPolicy_NonSig] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptSummaryReportByCarrierByPolicy_NonSig] TO [power_user]
GO
