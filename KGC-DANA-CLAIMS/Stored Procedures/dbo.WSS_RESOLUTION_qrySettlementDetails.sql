SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_RESOLUTION_qrySettlementDetails] (@ClaimID bigint = NULL)
AS

--DECLARE @ClaimID int
--SET @ClaimID = 65720

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

SELECT distinct C.ClaimID, 
hddis.HighLevelDisease AS Disease, 
S.State, 
J.Jurisdiction, 
cs.ClaimSequence, 
CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, 
CONVERT(date, CPI.BirthDate) AS BirthDate, 
CPI.DeceasedDate, 
CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) 
WHEN Month(BirthDate) > Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 
WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 
  ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
cls.ClaimLawsuitStatus, 
CONVERT(date, L.ServiceDate) AS ServiceDate, 
CONVERT(date, L.FileDate) AS FileDate, 
L.DocketNumber, 
L.CaseCaption, 
L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,
pca.PlaintiffCounsel AS PrimaryLocalPlaintiffCounsel, 
DefenseCounsel AS PrimaryDefenseCounsel,
CPI.ClaimantPersonalInfoID,
ReleaseSignedDate,
rt.ReleaseType, 
CLst.StatusNotes,
ExecutedReleaseDate as ReleaseReceivedDate,
SettlementDueDate,
PaidDate,
CheckNumber,
JOBSITE,
ClaimDiseaseID,
cl.LawsuitID,
ActiveTrialDate,
ProcessedDate AS StatusDate,
SettlementDate,
COALESCE(cd.DiseaseID, disa.diseaseID_alt) AS DiseaseID,
doc.DoctorLastName + ', ' + COALESCE(doc.DoctorFirstName, '') + COALESCE(' ' + doc.DoctorMiddleName, '') AS Doctor,
SettlementAmount,
SettlementGroupID,
FullyDocumented,
CauseOfInjuryID,
IsThirdPartyComplaint,
IsMDL,
ClaimantSettlementID
FROM tblClaimantPersonalInfo CPI 
INNER JOIN tblClaim C	 ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID 
INNER  JOIN tblMatter m					ON c.MatterID = m.MatterID
INNER JOIN tblClaimLawsuit CL	 ON C.ClaimID = CL.ClaimID 
INNER JOIN tblLawsuit L	 ON CL.LawsuitID = L.LawsuitID 
INNER JOIN tblState S	 ON S.StateID = L.StateID 
INNER JOIN tblJurisdiction J	 ON J.JurisdictionID = L.JurisdictionID 
INNER JOIN tblClaimSequence cs	 ON cs.ClaimSequenceID = C.ClaimSequenceID
LEFT  JOIN tblDefenseJurisdiction dj	 ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND dj.MatterEntityID = m.MatterEntityID
LEFT  JOIN tblDefenseCounselAddress dca	 ON l.DefenseCOunselAddressID = dca.DefenseCounselAddressID
LEFT  JOIN (SELECT * 
from tblClaimDisease 
where PrimaryDisease = 1) CD	 ON CD.ClaimID = c.ClaimID
LEFT  JOIN tblDisease ddis	 ON ddis.DiseaseID = cd.DiseaseID
LEFT  JOIN tblDisease_ALT disa	 ON disa.DiseaseID_Alt = cd.DiseaseID
LEFT  JOIN tblHighLevelDisease hddis	 ON hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
LEFT  JOIN (SELECT * 
FROM tblClaimantEmployment 
WHERE IsPrimary = 1) CE	 ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID   
LEFT  JOIN (Select * 
From dbo.tblClaimLawsuitStatusTracking 
where IsPrimaryStatus = 1) clst	 ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT  JOIN tblClaimLawsuitStatus cls	 ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID 
LEFT  JOIN tblClaimantSettlement ClaimSett	 ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID 
LEFT  JOIN tblSettlePaymentDocumentation spd	 ON spd.ClaimLawsuitID = cl.ClaimLawsuitID 
LEFT  JOIN tblReleaseType rt	 ON rt.ReleaseTypeID = spd.ReleaseTypeID
LEFT  JOIN tblplaintiffcounseladdress pca	 ON pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
LEFT  JOIN tblDoctor doc	 ON cd.DoctorID = doc.DoctorID 
LEFT  JOIN tblDoctor ILODoc	 ON ILODoctorID = ILODoc.DoctorID
LEFT  JOIN tblDoctor PFTDoc	 ON PFTDoctorID = PFTDoc.DoctorID
WHERE C.ClaimID = @ClaimID
AND cl.IsPrimaryForClaim = 1


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qrySettlementDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qrySettlementDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qrySettlementDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qrySettlementDetails] TO [power_user]
GO
