SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryInsertSavedSearch]
(@Title nvarchar(max), @EmailAddress nvarchar(max))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO tblSavedSearch
	SELECT @Title, @EmailAddress
	
	SELECT MAX(SavedSearchID) FROM tblSavedSearch
END


GO
