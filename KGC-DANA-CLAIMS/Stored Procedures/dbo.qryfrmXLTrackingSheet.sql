SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Proc [dbo].[qryfrmXLTrackingSheet](@CPI bigint)

As
Set NoCount On

--Declare @CPI int
--Set @CPI = 69353

Select CPI.ClaimantPersonalInfoID, FirstName, MiddleName, LastName, SocialSecurityNumber, BirthDate, DeceasedDate,
C.ClaimID, IsCurrentClaim, Matter, CaseCaption, DocketNumber, Jurisdiction, S.State, DCA.DefenseCounsel, PCA.PlaintiffCounsel,
CLS.ClaimLawsuitStatus, SettlementGroup, SettlementAmount, SettlementDate,
coalesce(SPD.SettlementConfirmation,0) as SettlementConfirmation, 
coalesce(ClaimantIDed,0) as ClaimantIDed, 
coalesce(DxMedicals,0) as DxMedicals, 
coalesce(DODCert,0) as DODCert, 
coalesce(EstatePapers,0) as EstatePapers, 
coalesce(ProductID,0) as ProductID, 
coalesce(MMSEA,0) as MMSEA, 
coalesce(MedicarePlaintiff,0) as MedicarePlaintiff, 
coalesce(MedicarePlaintiffCounsel,0) as MedicarePlaintiffCounsel, 
coalesce(EmployExpoHist,0) as EmployExpoHist, 
ReleaseRqstDate, ReleaseToPC, ExecutedReleaseDate, SettlePaymentComments, ReleaseRcvd, ReleaseSignedDate, ReleaseType, PaymentAmount, CheckID

From tblClaimantPersonalInfo CPI
Inner Join tblClaim C		On CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
Inner Join tblMatter M		On M.MatterID = C.MatterID
Inner Join tblClaimLawsuit CL		On CL.ClaimID = C.ClaimID
LEFT Join tblClaimantSettlement CS	On CS.ClaimLawsuitID = CL.ClaimLawsuitID
Inner Join tblLawsuit L		On L.LawsuitID = CL.LawsuitID
Left Join tblPlaintiffCounselAddress PCA	On PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
Inner Join tblClaimLawsuitStatusTracking CLST	On CLST.ClaimLawsuitID = CL.ClaimLawsuitID
Inner Join tblClaimLawsuitStatus CLS	On CLS.ClaimLawsuitStatusID = CLST.ClaimLawsuitStatusID
Left Join tblSettlementGroup SG		On SG.SettlementGroupID = CS.SettlementGroupID
Left Join tblSettlePaymentDocumentation SPD	On SPD.ClaimLawsuitID = CL.ClaimLawsuitID
Left Join tblReleaseType RT		On RT.ReleaseTypeID = SPD.ReleaseTypeID
Inner Join tblJurisdiction J		On J.JurisdictionID = L.JurisdictionID
Inner Join tblState S		On S.StateID = L.StateID
Inner Join tblDefenseCounselAddress DCA	On DCA.DefenseCounselAddressID = L.DefenseCounselAddressID
Left Join tblPaymentSettlement PS	On PS.ClaimantSettlementID = CS.ClaimantSettlementID

Where IsCurrentClaim = 1 and IsPrimaryStatus = 1 and PS.PaymentSettlementID is null and CPI.ClaimantPersonalInfoID = @CPI

Group by CPI.ClaimantPersonalInfoID, FirstName, MiddleName, LastName, SocialSecurityNumber, BirthDate, DeceasedDate,
C.ClaimID, IsCurrentClaim, Matter, CaseCaption, DocketNumber, Jurisdiction, S.State, DCA.DefenseCounsel, PCA.PlaintiffCounsel,
CLS.ClaimLawsuitStatus, SettlementGroup, SettlementAmount, SettlementDate,
SettlementConfirmation, 
ClaimantIDed, 
DxMedicals, 
DODCert, 
EstatePapers, 
ProductID, 
MMSEA, 
MedicarePlaintiff, 
MedicarePlaintiffCounsel, 
EmployExpoHist, 
ReleaseRqstDate, ReleaseToPC, ExecutedReleaseDate, SettlePaymentComments, ReleaseRcvd, ReleaseSignedDate, ReleaseType, PaymentAmount, CheckID

Order by LastName, FirstName


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmXLTrackingSheet] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmXLTrackingSheet] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmXLTrackingSheet] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmXLTrackingSheet] TO [power_user]
GO
