SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] 
 (
	 @MatterEntityID int,
	 @MatterID int = NULL, 
	 @MonthID  int, 
	 @YearID   int
 )

 AS
SET NOCOUNT ON

-- --The following variables can be used for testing.
--DECLARE @MatterEntityID int
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MatterEntityID = 1
--SET @MonthID  = 12
--SET @YearID   = 2013
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with ProcessedDate, 
--      and all original ProcessedDate references have been replaced with ProcessedDate.

DECLARE @MaxMatterID int
SET @MaxMatterID = 6

DECLARE @Month int
DECLARE @Year int
DECLARE @PendingDate date
SET @Month = @MonthID
SET @Year = @YearID
SET @PendingDate = DATEADD(MONTH,1,convert(date,convert(varchar,@Month)+'/1/'+convert(varchar,@Year)))

DECLARE @WhereString nvarchar(max)
SET @WhereString = '
	WHERE ClaimLawsuitStatusID NOT IN (22) 
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
		AND IsCurrentClaim = 1
'

IF @PendingDate		IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND ProcessedDate >= ' + '''' + CONVERT(nvarchar(20),  @PendingDate) + '''' END
IF @MatterEntityID	IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND MatterEntityID = ' + CONVERT(nvarchar(20),@MatterEntityID) END
IF @MatterID		IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar(20),@MatterID) END
IF @MatterID		IS NULL		  BEGIN SET @WhereString = @WhereString + ' AND c.MatterID <> 13 ' END


----Find all claims with a status change post pending date (2a,2b)

IF object_id('tempdb..#FindClaimantsWithPostPendingDateStatusChange') IS NOT NULL
BEGIN
   DROP TABLE #FindClaimantsWithPostPendingDateStatusChange
END
CREATE TABLE #FindClaimantsWithPostPendingDateStatusChange (Claimant       int, 
															ClaimLawsuitID int)
--Find all claims with a status change post pending date (2a,2b)
DECLARE @QueryStringPostPendingDateStatusDate nvarchar(max)
SET @QueryStringPostPendingDateStatusDate = '
	SELECT c.ClaimantPersonalInfoID, 
		cl.ClaimLawsuitID
	FROM tblClaim c 
		INNER JOIN tblMatter m						  ON c.MatterID = m.MatterID
		INNER JOIN tblClaimLawsuit cl				  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l						  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		'+ @WhereString +'
	GROUP BY c.ClaimantPersonalInfoID, cl.ClaimLawsuitID
'
INSERT INTO #FindClaimantsWithPostPendingDateStatusChange
EXEC sp_executesql @QueryStringPostPendingDateStatusDate

--For all claims with a status change post pending AND a stausdate before pending date: find the max status date before pending date (2a)
DECLARE @FindMaxProcessedDateBeforePendingDate TABLE (ClaimLawsuitID2 int, 
												   MaxDate         datetime)
INSERT INTO @FindMaxProcessedDateBeforePendingDate
	SELECT cl.ClaimLawsuitID AS ClaimLawsuitID2,  
		MAX(ProcessedDate) AS MaxDate
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl								ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimLawsuitStatusTracking clst				ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN #FindClaimantsWithPostPendingDateStatusChange ft ON ft.ClaimLawsuitID = cl.ClaimLawsuitID
	WHERE ProcessedDate < @PendingDate
	GROUP BY cl.ClaimLawsuitID

--For claims with a status change post pending AND a ProcessedDate before pending date: find only those claims with a max open status before pending date (2a)
DECLARE @CheckForOnlyOpenStatusBeforePendingDate TABLE (ClaimLawsuitID3 int)
INSERT INTO @CheckForOnlyOpenStatusBeforePendingDate
	SELECT cl.ClaimLawsuitID AS ClaimLawsuitID3
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl						 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l								 ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst		 ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimLawsuitStatus cls				 ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		INNER JOIN @FindMaxProcessedDateBeforePendingDate fm ON fm.ClaimLawsuitID2 = Clst.ClaimLawsuitID AND clst.ProcessedDate = fm.maxdate
	WHERE clst.ClaimLawsuitStatusID = 22

--Check to see the status count of all claims with a status change post pending date (2b)
DECLARE @CheckForStatuscount TABLE(ClaimLawsuitID4 int, 
								   StatusCount     int)
INSERT INTO @CheckForStatuscount
	SELECT cl.ClaimLawsuitID AS ClaimLawsuitID4, 
		COUNT(ProcessedDate) AS StatusCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl								 ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimLawsuitStatusTracking clst				 ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN #FindClaimantsWithPostPendingDateStatusChange fcw ON fcw.ClaimLawsuitID = cl.ClaimLawsuitID
	GROUP BY cl.ClaimLawsuitID

--Find all claims that only have one status date and check to see if file/service/received date was before pending date (2b)
DECLARE @CheckIfFileDateBeforePendingDateForOnlyOneStatus table (ClaimLawsuitID5 int)
INSERT INTO @CheckIfFileDateBeforePendingDateForOnlyOneStatus
	SELECT cl.ClaimLawsuitID AS ClaimLawsuitID5
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl				  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l						  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimLawsuitStatus cls		  ON cls.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		INNER JOIN @CheckForStatuscount cfs			  ON cfs.ClaimLawsuitID4 = Clst.ClaimLawsuitID
	WHERE cfs.StatusCount = 1 
		AND COALESCE(ServiceDate,FileDate,ReceivedDate) < @PendingDate
	GROUP BY cl.ClaimLawsuitID

DECLARE @WhereStringOpen nvarchar(max)

SET @WhereStringOpen = '
	WHERE ClaimLawsuitStatusID = 22 
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
		AND cd.PrimaryDisease = 1
'

IF @PendingDate		IS NOT NULL   BEGIN SET @WhereStringOpen = @WhereStringOpen + ' AND (ProcessedDate < ' + '''' + CONVERT(nvarchar(20),  @PendingDate) + '''' + ' OR ProcessedDate IS NULL)' END
IF @MatterEntityID	IS NOT NULL   BEGIN SET @WhereStringOpen = @WhereStringOpen + ' AND MatterEntityID = ' + CONVERT(nvarchar(20),@MatterEntityID) END
IF @MatterID		IS NOT NULL   BEGIN SET @WhereStringOpen = @WhereStringOpen + ' AND c.MatterID = ' + CONVERT(nvarchar(20),@MatterID) END
IF @MatterID		IS NULL		  BEGIN SET @WhereStringOpen = @WhereStringOpen + ' AND c.MatterID <> 13 ' END

IF object_id('tempdb..#OpenStatusDateBeforePendingDate') IS NOT NULL
BEGIN
   DROP TABLE #OpenStatusDateBeforePendingDate
END
CREATE TABLE #OpenStatusDateBeforePendingDate (HighLevelDisease nvarchar(100), 
															State			 nvarchar (100), 
															DiseaseCount	 int)
--Counts All Claims that have their status date before the pending date and are CURRENTLY still open (1)
DECLARE @QueryStringOpenStatusDateBeforePendingDate nvarchar(max)
SET @QueryStringOpenStatusDateBeforePendingDate = '

	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN ''Unknown'' 
												  ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		FullNameState AS ''State'',
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS DiseaseCount
	FROM tblClaim c 
		INNER JOIN tblMatter m						  ON c.MatterID = m.MatterID
		INNER JOIN tblClaimLawsuit cl				  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l						  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		INNER JOIN tblClaimDisease cd				  ON c.ClaimID = cd.ClaimID
		LEFT  JOIN tblDisease d						  ON d.DiseaseID = cd.DiseaseID
		LEFT  JOIN tblDisease_ALT da				  ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT  JOIN tblHighLevelDisease hld			  ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
		LEFT  JOIN tblState s						  ON s.StateID = l.StateID
				'+ @WhereStringOpen +'
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN ''Unknown'' 
													ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID, 
			FullNameState
'
INSERT INTO #OpenStatusDateBeforePendingDate
EXEC sp_executesql @QueryStringOpenStatusDateBeforePendingDate

--Union table to combine 3 main components of Pending Count
DECLARE @CombinedTable TABLE (HighLevelDisease nvarchar(100), 
							  State            nvarchar(100), 
							  DiseaseCount     int)
INSERT INTO @CombinedTable
	--Counts All Claims that have their status date before the pending date and are CURRENTLY still open (1)
	SELECT *
	FROM #OpenStatusDateBeforePendingDate
	UNION ALL
	--Counts all Claims with a status change post pending date and an open status as of pending date (2a)
	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
												  ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		FullNameState AS 'State',
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS DiseaseCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl						   ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l								   ON cl.LawsuitID = l.LawsuitID
		INNER JOIN @CheckForOnlyOpenStatusBeforePendingDate cf ON cf.ClaimLawsuitID3 = cl.ClaimLawsuitID
		INNER JOIN tblClaimDisease cd						   ON c.ClaimID = cd.ClaimID
		LEFT  JOIN tblDisease d								   ON d.DiseaseID = cd.DiseaseID
		LEFT  JOIN tblDisease_ALT da						   ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT  JOIN tblHighLevelDisease hld					   ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
		LEFT  JOIN tblState s								   ON s.StateID = l.StateID
	WHERE cd.PrimaryDisease = 1
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID, FullNameState
	UNION ALL
	--Counts all claims with a status change post pending date and only has one status and has a file/service/received date before pending date (2b)
	SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' 
												  ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		FullNameState AS 'State',
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS DiseaseCount
	FROM tblClaim c 
		INNER JOIN tblClaimLawsuit cl									  ON c.ClaimID = cl.ClaimID
		INNER JOIN tblLawsuit l											  ON cl.LawsuitID = l.LawsuitID
		INNER JOIN @CheckIfFileDateBeforePendingDateForOnlyOneStatus cifd ON cifd.ClaimLawsuitID5 = cl.ClaimLawsuitID
		INNER JOIN tblClaimDisease cd									  ON c.ClaimID = cd.ClaimID
		LEFT  JOIN tblDisease d											  ON d.DiseaseID = cd.DiseaseID
		LEFT  JOIN tblDisease_ALT da									  ON cd.DiseaseID = da.DiseaseID_Alt
		LEFT  JOIN tblHighLevelDisease hld								  ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
		LEFT  JOIN tblState s											  ON s.StateID = l.StateID
	WHERE cd.PrimaryDisease = 1
	GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID, FullNameState

--Groups all results into one table
SELECT [State] AS 'State',
	SUM(CASE WHEN HighLevelDisease = 'Mesothelioma'  THEN DiseaseCount ELSE 0 END) AS 'Mesothelioma',
	SUM(CASE WHEN HighLevelDisease = 'Lung Cancer'	 THEN DiseaseCount ELSE 0 END) AS 'Lung Cancer',
	SUM(CASE WHEN HighLevelDisease = 'Other Cancer'  THEN DiseaseCount ELSE 0 END) AS 'Other Cancer',
	SUM(CASE WHEN HighLevelDisease = 'Non-Malignant' THEN DiseaseCount ELSE 0 END) AS 'Non-Malignant',
	SUM(CASE WHEN HighLevelDisease = 'Unknown'		 THEN DiseaseCount ELSE 0 END) AS 'Unknown',
	SUM(CASE WHEN HighLevelDisease = 'Silicosis'	 THEN DiseaseCount ELSE 0 END) AS 'Silicosis',
	SUM(DiseaseCount) AS Total
FROM @CombinedTable ct
GROUP BY [State]





GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryPendingMattersActiveByStateByDisease] TO [power_user]
GO
