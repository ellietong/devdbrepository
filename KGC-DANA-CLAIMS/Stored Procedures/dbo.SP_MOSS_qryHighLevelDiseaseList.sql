SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_MOSS_qryHighLevelDiseaseList]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT HighLevelDiseaseID, HighLevelDisease FROM tblHighLevelDisease
	--WHERE HighLevelDiseaseID IN (1, 2, 3, 4, 5)
	UNION
	SELECT NULL, ''
	ORDER BY HighLevelDisease
END



GO
