SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_MATTER_qryMatterEntityList_Filter]
AS
SET NOCOUNT ON

SELECT MatterEntityID, 
	MatterEntity 
FROM tblMatterEntity
ORDER BY MatterEntityID



GO
