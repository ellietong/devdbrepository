SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryClaimantRelatedPartyInfo]
@ClaimantPersonalInfoID bigint=null, @emailaddress nvarchar(250)
AS
BEGIN


Declare @UserFirmState table (DefenseCounselID bigint, StateID int)
Insert into @UserFirmState

Select dca.DefenseCounselID, StateID
From dbo.tblDefenseCounselState AS dcs 
left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid --and dcar.stateID = dcs.StateID 
LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
Where (ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) OR
dca.DefenseCounselID in (SELECT f.FirmID FROM vtblPerson p inner join vtblFirm f on p.FirmID=f.FirmID WHERE Email = @emailaddress))

IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress) IN (132, 133, 114) OR ((Select PersonID FROM vtblPerson WHERE Email = @emailaddress) IN (1376,1377,1378,1383,1384,1389,1392,1404))  OR (SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1
BEGIN
Select ClaimantRelatedPartyID, 
	ClaimantPersonalInfoID,  
	LastName + ', ' + CASE WHEN FirstName IS NULL 
    THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
    LastName, FirstName, MiddleName,
	Relationship, 
	LC.LegalCapacity,
	Case When crp.RPSocialSecurityNumber IS NULL Then '' ELSE 'XXX-XX-' + RIGHT(crp.RPSocialSecurityNumber, 4) END AS SSN,
	COALESCE(CONVERT(nvarchar(15), crp.RPSocialSecurityNumber), '') AS FullSSN, crp.RelationshipID, crp.LegalCapacityID
From tblClaimantRelatedParty crp 
LEFT JOIN tblRelationship r on r.RelationshipID=crp.RelationshipID
LEFT JOIN tblLegalCapacity LC ON LC.LegalCapacityID = crp.LegalCapacityID
WHERE ClaimantPersonalInfoID=@ClaimantPersonalInfoID
END
ELSE
BEGIN
Select DISTINCT ClaimantRelatedPartyID, 
	crp.ClaimantPersonalInfoID,  
	LastName + ', ' + CASE WHEN FirstName IS NULL 
    THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS RelatedPartyName,
    LastName, FirstName, MiddleName,
	Relationship, 
	LC.LegalCapacity,
	Case When crp.RPSocialSecurityNumber IS NULL Then '' ELSE 'XXX-XX-' + RIGHT(crp.RPSocialSecurityNumber, 4) END AS SSN,
	COALESCE(CONVERT(nvarchar(15), crp.RPSocialSecurityNumber), '') AS FullSSN, crp.RelationshipID, crp.LegalCapacityID
From tblClaimantRelatedParty crp 
LEFT JOIN tblRelationship r on r.RelationshipID=crp.RelationshipID
LEFT JOIN tblLegalCapacity LC ON LC.LegalCapacityID = crp.LegalCapacityID
LEFT JOIN tblClaim c ON crp.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
                      LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
                      LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
INNER JOIN tblDefenseCounselState dcs on l.DefenseCounselAddressID=dcs.DefenseCounselAddressID and l.StateID=dcs.StateID
INNER JOIN tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCounselAddressID=dca.DefenseCounselAddressID
INNER JOIN @UserFirmState u on dca.DefenseCounselID = u.DefenseCounselID and l.StateID = u.StateID
WHERE crp.ClaimantPersonalInfoID=@ClaimantPersonalInfoID
--AND DefenseCounselAddressID in (SELECT   DISTINCT  dcs.DefenseCounseladdressID
--                            FROM          dbo.tblDefenseCounselState AS dcs INNER JOIN
--											dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
--											LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
--											LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID INNER JOIN
--                                                   dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
--                            WHERE ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress))
END



END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantRelatedPartyInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantRelatedPartyInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantRelatedPartyInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantRelatedPartyInfo] TO [power_user]
GO
