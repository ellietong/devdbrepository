SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryInsSettlementInfoDocument]
(@DocumentID bigint, @DocCategoryID int, @PageNums nvarchar(20), @SetPymtID bigint = null)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--DECLARE @ClaimantPersonalInfoID int
--declare @DocumentID int
--declare @DocCategoryID int
--declare @PageNums varchar

--set @ClaimantPersonalInfoID = 91864
--set @DocumentID = 973
--set @DocCategoryID = 4 
--set @PageNums = '1'


DECLARE @ClaimantPersonalInfoID int

/**Declare cursor to attach documentation to each claimant the document is attached to**/
DECLARE Cur_Claimants CURSOR Local Fast_Forward for
select ClaimantpersonalinfoID from tblmulticlaimantdocument where documentid = @DocumentID

OPEN Cur_Claimants
FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID
WHILE @@FETCH_STATUS = 0
BEGIN

DECLARE @ClaimLawsuitID int
SET @ClaimLawsuitID = 
(select ClaimlawsuitID from tblClaimantPersonalInfo cpi inner join tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID inner join tblClaimLawsuit cl on cl.ClaimID=c.ClaimID where c.IsCurrentClaim = 1 and cl.IsPrimaryForClaim = 1 and cpi.ClaimantPersonalInfoID =@ClaimantPersonalInfoID)

DECLARE @NewSettlePaymentDocumentationID INT


--if there is no record for this claim in tblSettlePaymentDocumentation then create one
if (select COUNT(setpymtid) from tblSettlePaymentDocumentation where ClaimLawsuitID = @ClaimLawsuitID) = 0 
begin
Insert into tblSettlePaymentDocumentation (ClaimLawsuitID)
select @ClaimLawsuitID
set @NewSettlePaymentDocumentationID = (select MAX(SetPymtID) from tblSettlePaymentDocumentation)
end
ELSE
begin
set @NewSettlePaymentDocumentationID = (Select setpymtID from tblSettlePaymentDocumentation where ClaimLawsuitID = @ClaimLawsuitID)
end

--insert settlementdocumentation recordusing settlement payment information ID
insert into tblSettleDocs (DocumentID, DocCategoryID, PageNums, SetPymtID)
select @DocumentID, @DocCategoryID, @PageNums, @NewSettlePaymentDocumentationID


--update the settlementdocumentation record to include this document
--1	Claimant Identified
--2	Diagnosing Medicals
--3	Death Certificate
--4	Estate Paperwork
--5	Product Identification
--6	Emloyment/Exposure History
--7	MMSEA Settlement Form (Not in IUNA)
--8	Settlement Confirmation Letter
if @DocCategoryID = 1 
begin
update tblSettlePaymentDocumentation set ClaimantIDed = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 2 
begin
update tblSettlePaymentDocumentation set DxMedicals = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 3
begin
update tblSettlePaymentDocumentation set DODCert = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 4
begin
update tblSettlePaymentDocumentation set EstatePapers = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 5
begin
update tblSettlePaymentDocumentation set ProductID = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 6
begin
update tblSettlePaymentDocumentation set EmployExpoHist = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 7
begin
update tblSettlePaymentDocumentation set MMSEA = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 8
begin
update tblSettlePaymentDocumentation set MedicarePlaintiff = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 9
begin
update tblSettlePaymentDocumentation set MedicarePlaintiffCounsel = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end
if @DocCategoryID = 10
begin
update tblSettlePaymentDocumentation set SettlementConfirmation = 1 
where SetPymtID = @NewSettlePaymentDocumentationID
end


FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID

END
CLOSE Cur_Claimants
DEALLOCATE Cur_Claimants

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryInsSettlementInfoDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryInsSettlementInfoDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryInsSettlementInfoDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryInsSettlementInfoDocument] TO [power_user]
GO
