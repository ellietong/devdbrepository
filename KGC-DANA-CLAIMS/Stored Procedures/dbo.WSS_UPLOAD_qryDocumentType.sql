SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryDocumentType] (@invoiceFlag    int = 0, 
											   @settlementFlag bit = 0,
											   @emailaddress nvarchar(250))
AS
SET NOCOUNT ON

--DECLARE @invoiceFlag bit
--DECLARE @settlementFlag bit
--SET @invoiceFlag    = 0
--SET @settlementFlag = 0

DECLARE @PlaintiffCounselAddressID bigint = (SELECT PlaintiffCounselAddressID FROM vtblPerson WHERE Email = @emailaddress)

IF(@emailaddress like '%kcicllc.com') OR (@emailaddress like '%kcic.com') --KCIC
BEGIN
	IF @invoiceFlag = 0
	BEGIN
		IF @settlementFlag = 1
		BEGIN
			SELECT ClaimantDocumentTypeID, 
				ClaimantDocumentType, 
				DocumentGroupID
			FROM tblClaimantDocumentType
			WHERE (DocumentGroupID IN (1, 3, 4)
				AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <>16) OR ClaimantDocumentTypeID = 55
			UNION
			SELECT NULL, '-- Select Document Type --', NULL
			ORDER BY ClaimantDocumentType
		END
		ELSE
		BEGIN
			SELECT ClaimantDocumentTypeID, 
				ClaimantDocumentType, 
				DocumentGroupID
			FROM tblClaimantDocumentType
			WHERE (DocumentGroupID IN (1, 3)
				AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <> 16)  OR ClaimantDocumentTypeID = 55
			UNION
			SELECT NULL, '-- Select Document Type --', NULL
			ORDER BY ClaimantDocumentType
		END
	END
	ELSE IF @invoiceFlag = 1
	BEGIN
			SELECT ClaimantDocumentTypeID, 
				ClaimantDocumentType, 
				DocumentGroupID
			FROM tblClaimantDocumentType
			WHERE DocumentGroupID = 2 
				AND ClaimantDocumentTypeID NOT IN (41, 42)
			UNION
			SELECT NULL, '-- Select Document Type --', NULL
			ORDER BY ClaimantDocumentType
	END
	ELSE
	BEGIN
			SELECT ClaimantDocumentTypeID, 
				ClaimantDocumentType, 
				DocumentGroupID
			FROM tblClaimantDocumentType
			WHERE DocumentGroupID = 2
			UNION
			SELECT NULL, '-- Select Document Type --', NULL
			ORDER BY ClaimantDocumentType
	END

END
ELSE IF(@PlaintiffCounselAddressID IS NULL) --Non-PlaintiffCounsel
BEGIN
	
	IF @invoiceFlag = 0
	BEGIN
		IF @settlementFlag = 1
		BEGIN
			SELECT ClaimantDocumentTypeID, 
				ClaimantDocumentType, 
				DocumentGroupID
			FROM tblClaimantDocumentType
			WHERE DocumentGroupID IN (1, 3, 4)
				AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <>16 AND ClaimantDocumentTypeID <> 55
			UNION
			SELECT NULL, '-- Select Document Type --', NULL
			ORDER BY ClaimantDocumentType
		END
		ELSE
		BEGIN
			SELECT ClaimantDocumentTypeID, 
				ClaimantDocumentType, 
				DocumentGroupID
			FROM tblClaimantDocumentType
			WHERE DocumentGroupID IN (1, 3)
				AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <> 16 AND ClaimantDocumentTypeID <> 55
			UNION
			SELECT NULL, '-- Select Document Type --', NULL
			ORDER BY ClaimantDocumentType
		END
	END
	ELSE IF @invoiceFlag = 1
	BEGIN
			SELECT ClaimantDocumentTypeID, 
				ClaimantDocumentType, 
				DocumentGroupID
			FROM tblClaimantDocumentType
			WHERE DocumentGroupID = 2 
				AND ClaimantDocumentTypeID NOT IN (41, 42)
			UNION
			SELECT NULL, '-- Select Document Type --', NULL
			ORDER BY ClaimantDocumentType
	END
	ELSE
	BEGIN
			SELECT ClaimantDocumentTypeID, 
				ClaimantDocumentType, 
				DocumentGroupID
			FROM tblClaimantDocumentType
			WHERE DocumentGroupID = 2
			UNION
			SELECT NULL, '-- Select Document Type --', NULL
			ORDER BY ClaimantDocumentType
	END
END
ELSE
BEGIN
	IF @invoiceFlag = 0
	BEGIN
		IF @settlementFlag = 1
		BEGIN
			SELECT ClaimantDocumentTypeID, 
				ClaimantDocumentType, 
				DocumentGroupID
			FROM tblClaimantDocumentType
			WHERE ClaimantDocumentTypeID IN (2,4,10,12,18,24,31,39,52)
			UNION
			SELECT NULL, '-- Select Document Type --', NULL
			--WHERE DocumentGroupID IN (1, 3, 4)
			--	AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <>16
			ORDER BY ClaimantDocumentType
		END
		ELSE
		BEGIN
			SELECT ClaimantDocumentTypeID, 
				ClaimantDocumentType, 
				DocumentGroupID
			FROM tblClaimantDocumentType
			WHERE ClaimantDocumentTypeID IN (2,4,10,12,18,24,31,39,52)
			UNION
			SELECT NULL, '-- Select Document Type --', NULL
			--WHERE DocumentGroupID IN (1, 3)
			--	AND ClaimantDocumentTypeID <> 1 AND ClaimantDocumentTypeID <> 16
			ORDER BY ClaimantDocumentType
		END
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDocumentType] TO [power_user]
GO
