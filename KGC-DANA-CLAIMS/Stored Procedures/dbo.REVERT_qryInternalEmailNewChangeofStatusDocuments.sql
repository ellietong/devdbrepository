SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[REVERT_qryInternalEmailNewChangeofStatusDocuments] 
AS
BEGIN
/*Query run daily to send a separate alert email to the designated Complaint Contacts for each firm when
a new complaint is uploaded that day. - BEH*/

--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)

SET @Subject = 	
'Dana Companies, LLC: New Change of Status Documents Uploaded ' + CONVERT(varchar, getdate(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.
DECLARE @NewComplaintsTable TABLE (DocID int, docTitle nvarchar(500), FirmID int, lName nvarchar(50), 
	
fName nvarchar(50), ClaimantPersonalInfoID bigint)
INSERT INTO @NewComplaintsTable
SELECT DISTINCT d.DocumentID, '<a href="https://admin.kcicextranet.com/clients/dana/cp/Pages/DocumentProfile.aspx?ItemId='+COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle, 
	
FirmID, cpi.LastName, cpi.FirstName, cpi.ClaimantPersonalInfoID

FROM tblDocuments d
	
INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	
INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID

WHERE datecreated BETWEEN GETDATE()-1 AND GETDATE() 
	
AND detailDocumentTypeID IN (53)

	
AND FirmID IS NOT NULL
	

GROUP BY d.documentID, d.DocumentTitle,FirmID, LastName, FirstName, cpi.ClaimantPersonalInfoID
	


DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar(max), ClaimantIDList nvarchar(max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar(max), @ClaimantID bigint, @ClaimantList nvarchar(max), @ClaimantIDList nvarchar(max)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for
SELECT DocID, COALESCE(Lname, '') + ', ' +  COALESCE(Fname, ''), claimantpersonalInfoID FROM @NewComplaintsTable
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
WHILE @@FETCH_STATUS = 0
BEGIN

IF(LEN(@ClaimantList)=0 OR @ClaimantList IS NULL)
BEGIN
	
SET @ClaimantList = '<ul> <a href="https://admin.kcicextranet.com/clients/dana/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+'">'  + @Claimant + coalesce(' ('+ CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
SET @ClaimantIDList = CONVERT(nvarchar,@ClaimantID)
END
ELSE
BEGIN
	
SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="https://admin.kcicextranet.com/clients/dana/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+'">' + COALESCE(@Claimant, '') + coalesce(' ('+ CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
SET @ClaimantIDList = COALESCE(@ClaimantIDList, '') + ', ' + CONVERT(nvarchar,@ClaimantID)

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
PRINT @@FETCH_STATUS
IF @DocID <> @LastDocID OR @@FETCH_STATUS <> 0
BEGIN
	
INSERT INTO @ClaimantPerDoc
	
SELECT @LastDocID, @ClaimantList, @ClaimantIDList
	
SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints
	


--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
	
}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	


--->
</STYLE>'



--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

IF (Select COUNT(docTitle) from @NewComplaintsTable) >0 
begin
set @BodyMessage = @BodyMessage + '<H4>The following change of status documents have been uploaded in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table>' + 
	
	
N'<tr><th>Document Title</th><th>Document ID</th><th>Linked Claimants (ClaimantID)</th><th>ClaimantID</th>' +
	
	
CAST ( (
	
	
	
SELECT td = docTitle, '', td = cl.DocID, '', td = cl.ClaimantList, '', td = cl.ClaimantIDList, ''

	
	
	
FROM @NewComplaintsTable C
	
	
	
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
	
	
	
GROUP BY docTitle, cl.DocID, cl.ClaimantList, cl.ClaimantIDList
	
	
	

	
	
for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	
N'</table>' +  N'<br/>'	
	

end
ELSE
begin
Set @BodyMessage = @BodyMessage + '<H4>No change of status documents have been uploaded in the last 24 hours. </H4>'
end



--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/dana/cp/Pages/Documents.aspx">here</A> to access the document(s).</b><br><br>'


--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'


IF (Select COUNT(docTitle) from @NewComplaintsTable) >0 
begin
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The DanaProfile sends from Dana@kcicllc.com
@profile_name='Admin',
@recipients = 'dana@kcicllc.com',
@copy_recipients = 'potterm@kcicllc.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
end

END


GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalEmailNewChangeofStatusDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalEmailNewChangeofStatusDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalEmailNewChangeofStatusDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalEmailNewChangeofStatusDocuments] TO [power_user]
GO
