SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROCedure [dbo].[qryXLcomplainttracking]
 
AS 
SET NOCOUNT ON
 
--Select COUNT(complaintid) as ComplaintsInTrackingTable 
----Select * 
--from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] 
--where processdeadline >= convert(varchar, getdate(), 101) and DocumentTypeID=1 
--and CaseID=22
 
select 'Marley - Processed' as Client, ct.claimantlastname, mcd.DocumentID, CreatedBy, cpi.ClaimantPersonalInfoID, cpi.LastName, mcd.QCBy, mcd.QCDate, ct.datereceived, processdeadline
from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
left join [kgc-marleywylain].[dbo].tblClaimantPersonalInfo cpi on cpi.LastName=ct.claimantlastname
left join [kgc-marleywylain].[dbo].tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
left join [kgc-marleywylain].[dbo].tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
left join [kgc-marleywylain].[dbo].tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
left join [kgc-marleywylain].[dbo].tblLawsuit l on l.LawsuitID=cl.LawsuitID 
left join [KGC-MarleyWylain].[dbo].tblDocumentS d on d.DocumentID=mcd.DocumentID
where YEAR(ct.datereceived)= YEAR(l.receiveddate) and MONTH(ct.datereceived)=MONTH(l.receiveddate) --and DAY(ct.datereceived)>=DAY(l.receiveddate)
and caseID=26 and (CreatedBy like '%@kcic.com' or CreatedBy is null)
and processdeadline >= convert(varchar, getdate(), 101) and (mcd.QCDate>= convert(datetime,convert(nvarchar,convert(date, GETDATE()-4))) or mcd.QCDate is null)
and DocumentTypeID=1
UNION
Select 'Marley - Not Processed' as Client, ct.claimantlastname, NULL, NULL, NULL, NULL, NULL,NULL, ct.datereceived, processdeadline
from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
where complaintid not in (select complaintid from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
left join [KGC-MarleyWylain].[dbo].tblClaimantPersonalInfo cpi on cpi.LastName=ct.claimantlastname
left join [KGC-MarleyWylain].[dbo].tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
left join [KGC-MarleyWylain].[dbo].tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID  
left join [KGC-MarleyWylain].[dbo].tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
left join [KGC-MarleyWylain].[dbo].tblLawsuit l on l.LawsuitID=cl.LawsuitID 
where YEAR(ct.datereceived)= YEAR(l.receiveddate) and MONTH(ct.datereceived)=MONTH(l.receiveddate) --and DAY(ct.datereceived)>=DAY(l.receiveddate)
and ProcessDeadline >= convert(varchar, getdate(), 101) and (mcd.QCDate>= convert(datetime,convert(nvarchar,convert(date, GETDATE()-4))) or mcd.QCDate is null))
and ct.CaseID = 26 and ProcessDeadline >= convert(varchar, getdate(), 101) and DocumentTypeID=1
 
 
UNION
select 'Dana - Processed' as Client, ct.claimantlastname, mcd.DocumentID,CreatedBy, cpi.ClaimantPersonalInfoID, cpi.LastName, mcd.QCBy, mcd.QCDate, ct.datereceived, processdeadline
from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
left join [KGC-Dana-Claims].[dbo].tblClaimantPersonalInfo cpi on cpi.LastName=ct.claimantlastname
left join [KGC-Dana-Claims].[dbo].tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
left join [KGC-Dana-Claims].[dbo].tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
left join [KGC-Dana-Claims].[dbo].tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
left join [KGC-Dana-Claims].[dbo].tblLawsuit l on l.LawsuitID=cl.LawsuitID 
left join [KGC-Dana-Claims].[dbo].tblDocumentS d on d.DocumentID=mcd.DocumentID
where yEAR(ct.datereceived)= YEAR(l.receiveddate) and MONTH(ct.datereceived)=MONTH(l.receiveddate) --and DAY(ct.datereceived)>=DAY(l.receiveddate)
and caseID=3 and (CreatedBy like '%@kcic.com' or CreatedBy is null)
and processdeadline >= convert(varchar, getdate(), 101) and (mcd.QCDate>= convert(datetime,convert(nvarchar,convert(date, GETDATE()-4))) or mcd.QCDate is null)
and DocumentTypeID=1
union
Select 'Dana - Not Processed' as Client, ct.claimantlastname, NULL, NULL, NULL, NULL, NULL, NULL, ct.datereceived, processdeadline
from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
where ct.complaintid not in (select ct.complaintid from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
	left join [KGC-Dana-Claims].[dbo].tblClaimantPersonalInfo cpi on cpi.LastName=ct.claimantlastname
	left join [KGC-Dana-Claims].[dbo].tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	left join [KGC-Dana-Claims].[dbo].tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	left join [KGC-Dana-Claims].[dbo].tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
	left join [KGC-Dana-Claims].[dbo].tblLawsuit l on l.LawsuitID=cl.LawsuitID 
	left join [KGC-Dana-Claims].[dbo].tblDocumentS d on d.DocumentID=mcd.DocumentID
	where yEAR(ct.datereceived)= YEAR(l.receiveddate) and MONTH(ct.datereceived)=MONTH(l.receiveddate) --and DAY(ct.datereceived)>=DAY(l.receiveddate)and 
	and caseID=3  
	and processdeadline >= convert(varchar, getdate(), 101) and (mcd.QCDate>= convert(datetime,convert(nvarchar,convert(date, GETDATE()-4))) or mcd.QCDate is null))
and ct.CaseID = 3 and ProcessDeadline >= convert(varchar, getdate(), 101) and DocumentTypeID=1
 
union
 
select 'MSA - Processed' as Client, ct.claimantlastname, mcd.DocumentID, CreatedBy, cpi.ClaimantPersonalInfoID, cpi.LastName, mcd.QCBy, mcd.QCDate, ct.datereceived, processdeadline
from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
left join [KGC-MSAClaims].[dbo].tblClaimantPersonalInfo cpi on cpi.LastName=ct.claimantlastname
left join [KGC-MSAClaims].[dbo].tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
left join [KGC-MSAClaims].[dbo].tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
left join [KGC-MSAClaims].[dbo].tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
left join [KGC-MSAClaims].[dbo].tblLawsuit l on l.LawsuitID=cl.LawsuitID 
left join [KGC-MSAClaims].[dbo].tblDocumentS d on d.DocumentID=mcd.DocumentID
where YEAR(ct.datereceived)= YEAR(l.receiveddate) and MONTH(ct.datereceived)=MONTH(l.receiveddate) 
and caseID=29 and (CreatedBy like '%@kcic.com' or CreatedBy is null)
and processdeadline >= convert(varchar, getdate(), 101) and (mcd.QCDate>= convert(datetime,convert(nvarchar,convert(date, GETDATE()-4))) or mcd.QCDate is null)
and DocumentTypeID=1
Union
Select 'MSA - Not Processed' as Client, ct.claimantlastname, NULL, NULL, NULL, NULL, NULL, NULL, ct.datereceived, processdeadline
from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
where ct.complaintid not in (select ct.complaintid from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
	left join [KGC-MSAClaims].[dbo].tblClaimantPersonalInfo cpi on cpi.LastName=ct.claimantlastname
	left join [KGC-MSAClaims].[dbo].tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	left join [KGC-MSAClaims].[dbo].tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	left join [KGC-MSAClaims].[dbo].tblClaimLawsuit cl on cl.ClaimID=c.ClaimID	
	left join [KGC-MSAClaims].[dbo].tblLawsuit l on l.LawsuitID=cl.LawsuitID 
	left join [KGC-MSAClaims].[dbo].tblDocumentS d on d.DocumentID=mcd.DocumentID
	where YEAR(ct.datereceived)= YEAR(l.receiveddate) and MONTH(ct.datereceived)=MONTH(l.receiveddate) 
	and caseID=29 
	and processdeadline >= convert(varchar, getdate(), 101) and (mcd.QCDate>= convert(datetime,convert(nvarchar,convert(date, GETDATE()-4))) or mcd.QCDate is null))
and ct.CaseID = 29 and ProcessDeadline >= convert(varchar, getdate(), 101) and ct.DocumentTypeID=1
 
 
union
 
select 'IUNA-Nosroc - Processed' as Client, ct.claimantlastname, mcd.DocumentID, createdby, cpi.ClaimantPersonalInfoID, cpi.LastName, mcd.QCBy, mcd.QCDate, ct.datereceived, processdeadline
from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
left join [KGC-IUNA-Nosroc].[dbo].tblClaimantPersonalInfo cpi on cpi.LastName=ct.claimantlastname
left join [KGC-IUNA-Nosroc].[dbo].tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
left join [KGC-IUNA-Nosroc].[dbo].tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
left join [KGC-IUNA-Nosroc].[dbo].tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
left join [KGC-IUNA-Nosroc].[dbo].tblLawsuit l on l.LawsuitID=cl.LawsuitID
left join [KGC-IUNA-Nosroc].[dbo].tblDocumentS d on d.DocumentID=mcd.DocumentID 
where YEAR(ct.datereceived)= YEAR(l.receiveddate) and MONTH(ct.datereceived)=MONTH(l.receiveddate) --and DAY(ct.datereceived)>=DAY(l.receiveddate)
and caseID=22 and (CreatedBy like '%@kcic.com' or CreatedBy is null)
and processdeadline >= convert(varchar, getdate(), 101) and (mcd.QCDate>= convert(datetime,convert(nvarchar,convert(date, GETDATE()-4))) or mcd.QCDate is null)
and DocumentTypeID=1
Union
Select 'IUNA-Nosroc - Not Processed' as Client, ct.claimantlastname,  NULL, NULL, NULL, NULL, NULL,NULL, ct.datereceived, processdeadline
	from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
	where ct.complaintid not in (select ct.complaintid from [kga-ClaimsAdmin].[dbo].[tblComplaintTracking] ct
	left join [KGC-IUNA-Nosroc].[dbo].tblClaimantPersonalInfo cpi on cpi.LastName=ct.claimantlastname
	left join [KGC-IUNA-Nosroc].[dbo].tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	left join [KGC-IUNA-Nosroc].[dbo].tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	left join [KGC-IUNA-Nosroc].[dbo].tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
	left join [KGC-IUNA-Nosroc].[dbo].tblLawsuit l on l.LawsuitID=cl.LawsuitID
	left join [KGC-IUNA-Nosroc].[dbo].tblDocumentS d on d.DocumentID=mcd.DocumentID 
	where YEAR(ct.datereceived)= YEAR(l.receiveddate) and MONTH(ct.datereceived)=MONTH(l.receiveddate) --and DAY(ct.datereceived)>=DAY(l.receiveddate)
	and caseID=22
	and processdeadline >= convert(varchar, getdate(), 101) and (mcd.QCDate>= convert(datetime,convert(nvarchar,convert(date, GETDATE()-4))) or mcd.QCDate is null))
and ct.CaseID = 22 and ProcessDeadline >= convert(varchar, getdate(), 101) 
and DocumentTypeID=1
order by client, processdeadline, claimantlastname
 

GO
GRANT VIEW DEFINITION ON  [dbo].[qryXLcomplainttracking] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryXLcomplainttracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryXLcomplainttracking] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryXLcomplainttracking] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryXLcomplainttracking] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryXLcomplainttracking] TO [power_user]
GO
