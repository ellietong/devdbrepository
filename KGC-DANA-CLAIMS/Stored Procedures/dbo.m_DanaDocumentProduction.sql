SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_DanaDocumentProduction]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select d.DocumentID, d.DocumentTitle, d.DetailDocumentTypeID, REPLACE(cdt.ClaimantDocumentType, '/', '_') AS ClaimantDocumentType, d.DocumentURL, DateCreated			
	from tblMultiClaimantDocument mcd			
	inner join tblDocuments d on mcd.DocumentID = d.DocumentID	
	inner join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	WHERE DateCreated < '1/4/2013'
--WHERE d.DocumentID IN(
--	416453,
--415629,
--415627,
--415603,
--415606,
--415620,
--415622,
--415624,
--415625,
--408495,
--408496,
--416454,
--967,
--1263,
--1264,
--415626,
--407593,
--1985,
--388175,
--269659,
--398748,
--415636,
--416446,
--416450,
--413834,
--413835,
--416452,
--407594,
--415638
----SELECT d.DocumentID FROM tblDocuments d
----INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
----EXCEPT (SELECT DocumentID FROM mtblDownloadedDocs)

--)	
--AND LEFT(DocumentURL, 2) <> '\\'
AND d.DocumentID NOT IN (967,
1263,
1264,
1985,
269659,
388175
)
	group by d.DocumentID, d.DocumentTitle, d.DetailDocumentTypeID, cdt.ClaimantDocumentType, d.DocumentURL, DateCreated				
	order by d.documentID, d.DocumentTitle, d.DetailDocumentTypeID,cdt.ClaimantDocumentType, d.DocumentURL		


END
GO
GRANT VIEW DEFINITION ON  [dbo].[m_DanaDocumentProduction] TO [base_user]
GRANT EXECUTE ON  [dbo].[m_DanaDocumentProduction] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_DanaDocumentProduction] TO [power_user]
GRANT EXECUTE ON  [dbo].[m_DanaDocumentProduction] TO [power_user]
GO
