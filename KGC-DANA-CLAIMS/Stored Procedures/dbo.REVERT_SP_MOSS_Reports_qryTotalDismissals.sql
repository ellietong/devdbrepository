SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_Reports_qryTotalDismissals] 
	-- Add the parameters for the stored procedure here
	(@MonthID int, @YearID int, @MatterID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @MonthID int
--Set @MonthID=11
--Declare @YearID int
--Set @YearId=2010

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)
    -- Insert statements for procedure here
select COUNT(cl.ClaimLawsuitID) as Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s on l.StateID=s.StateID
where month(statusDate)=@month and YEAR(statusdate)=@year and ClaimLawsuitStatusID in (15,16,17,36,27,19) and IsPrimaryForClaim = 1 and  IsPrimaryStatus=1 --and IsCurrentClaim =1
AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
order by COUNT(cl.ClaimLawsuitID) desc

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalDismissals] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalDismissals] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalDismissals] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalDismissals] TO [power_user]
GO
