SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_EMAIL_qryExternalUnexecutedReleaseUploaded] 
AS
BEGIN

/*Query run daily to send a separate alert email to plaintiff counsel contacts for each firm when
a Dana unexecuted release is uploaded that day. - CP*/

--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @LogoHeader nvarchar(2000)
SET @Subject = 	'Dana Companies, LLC: New Release Uploaded ' + CONVERT(varchar, getdate(), 107)
SET @LogoHeader = '<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

DECLARE @Today4PM datetime
DECLARE @Yesterday4PM datetime
SET @Yesterday4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-1))+' 16:00:00.000')
SET @Today4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 16:00:00.000')

--Create a temp table for releases which have been uploaded in the past day.
DECLARE @NewReleases TABLE (DocID int, DocType nvarchar(500), docTitle nvarchar(500), FirmID int, lName nvarchar(50), 
		fName nvarchar(50), ClaimlawsuitID bigint, PlaintiffCounselAddressID int)
INSERT INTO @NewReleases
	SELECT d.DocumentID, 
		ClaimantDocumentType, 
		'<a href="https://www.kcicextranet.com/pp/dana/Pages/PlaintiffSettlementDetails.aspx?ItemId='+COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle, 
		d.FirmID, 
		cpi.LastName, 
		cpi.FirstName, 
		cl.ClaimlawsuitID,
		l.PlaintiffCounselAddressID
	FROM tblDocuments d
		INNER JOIN tblClaimantDocumentType cdt  ON cdt.ClaimantDocumentTypeID=d.DetailDocumentTypeID
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo cpi  ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		LEFT  JOIN tblclaim c					ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT  JOIN tblclaimlawsuit cl			ON cl.claimid = c.claimid and mcd.ClaimLawsuitID = cl.ClaimLawsuitID
		LEFT  JOIN tbllawsuit l					ON l.lawsuitid =cl.lawsuitid
	WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
		--convert(date,d.DateCreated) = '11/5/2012'
		AND 
		detailDocumentTypeID = 55
		--AND RIGHT(CreatedBy, 11) != 'kcicllc.com'
		--AND d.FirmID IS NOT NULL
		AND IsPrimaryForClaim = 1
		AND IsCurrentClaim = 1	
	GROUP BY d.documentID, ClaimantDocumentType, d.DocumentTitle,
		d.FirmID, LastName, FirstName, cl.ClaimlawsuitID, l.PlaintiffCounselAddressID
	ORDER BY d.FirmID, cl.ClaimLawsuitID


DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar(max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar(max), @ClaimantID bigint, @ClaimantList nvarchar(max)
DECLARE Cur_Complaints CURSOR Local Fast_Forward for
	SELECT DocID, COALESCE(Lname, '') + ', ' +  COALESCE(Fname, ''), ClaimlawsuitID 
	FROM @NewReleases

OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
WHILE @@FETCH_STATUS = 0
	BEGIN
	IF(LEN(@ClaimantList)=0 OR @ClaimantList IS NULL)
		BEGIN
			SET @ClaimantList = '<a href="https://www.kcicextranet.com/pp/dana/Pages/PlaintiffSettlementDetails.aspx?ClaimlawsuitID=' +CONVERT(nvarchar(50), @claimantID)+'">'  + @Claimant + coalesce(' ('+ CONVERT(nvarchar(50), @claimantID) + ')', '') +  '</a>'
		END
	ELSE
		BEGIN
			SET @ClaimantList = COALESCE(@ClaimantList, '') + '<a href="https://www.kcicextranet.com/pp/dana/Pages/PlaintiffSettlementDetails.aspx?ClaimlawsuitID=' +CONVERT(nvarchar(50), @claimantID)+'">' + COALESCE(@Claimant, '') + coalesce(' ('+ CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a>'
		END
	SET @LastDocID = @DocID
	FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
	PRINT @@FETCH_STATUS
	IF @DocID <> @LastDocID OR @@FETCH_STATUS <> 0
		BEGIN
			INSERT INTO @ClaimantPerDoc
			SELECT @LastDocID, @ClaimantList
			SET @ClaimantList = NULL
		END
	END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints
	
	
--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.
DECLARE @PlaintiffCounselAddressID nvarchar(10), @firmURL nvarchar(100)
DECLARE firm_Curs CURSOR FOR
	SELECT nct.plaintiffcounseladdressid, 
		'https://www.kcicextranet.com/pp/dana/Pages/PlaintiffSettlementDetails.aspx'
	FROM @NewReleases nct
	GROUP BY nct.plaintiffcounseladdressid 
OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @PlaintiffCounselAddressID, @firmURL
WHILE @@FETCH_STATUS = 0
BEGIN
	--DECLARE and populate the body of the email for individual firms
	DECLARE @BodyMessage nvarchar(MAX)
	--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
	--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
	SET @BodyMessage = @LogoHeader + 
		'<STYLE TYPE="text/css">
		<!--
		TABLE {border-collapse: collapse;
		border: 1px solid #38160C;
		color: #F6ECF0;}
		TD	
		{font-size: 10pt; 
		text-align: center;
		border: 1px solid rgb(0,121,193);
		padding: .3em;
		color: #000000;
		vertical-align: top;}
		TH	
		{border: 1px solid #ffffff;
		font-size: 10pt; 
		padding: .3em;
		color: #F6ECF0;
		background: rgb(0,121,193);}
		UL 
		{margin:0; 
		padding:0;}	
		--->
		</STYLE>'

	DECLARE @TDStyle nvarchar(max) = 'font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

	--Set table column headings and populate the cells with the Invoices from @NewReleases

	IF (SELECT COUNT(docTitle) FROM @NewReleases WHERE PlaintiffCounselAddressID = @PlaintiffCounselAddressID
	) >0 
	BEGIN
	SET @BodyMessage = @BodyMessage + '<H4>The following releases are available for download:   </H4>'
	SET @BodyMessage = @BodyMessage + N'<table style = "margin-left: 3em; border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr>
	<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimant (ClaimantID)</th>
	<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th>' +
	CAST ( (
	SELECT  @TDStyle AS 'td/@style', (SELECT ClaimantList) AS td, '',
			@TDStyle AS 'td/@style', (SELECT DocType) AS td, ''
	FROM @NewReleases C
	INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
	WHERE c.PlaintiffCounselAddressID = @PlaintiffCounselAddressID
	GROUP BY cl.ClaimantList, DocType
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	N'</table>' +  N'<br/>'	
	END

	--Undo the HTML stripping from the xml path function above so embedded tags work
	SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')
	----Link for document library
	--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

	--KCIC address and disclaimer
	SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
	733 10th Street, NW, Suite 3001<br>
	Washington, DC 20001<br>
	(t) 202-772-2300<br>
	(f) 202-772-2333'
	  
	DECLARE @RecipientList nvarchar(500)
	SET @RecipientList=''

	--Use the complaintContact field to determine the contact list.
	--DECLARE a cursor to create a recipient list for the email.
	DECLARE @Contact nvarchar(500)
	DECLARE contact_curs CURSOR 
	LOCAL FORWARD_ONLY STATIC READ_ONLY
	FOR

	SELECT email 
	FROM vtblPerson p
	WHERE plaintiffcounseladdressID = @PlaintiffCounselAddressID

	OPEN contact_curs
	FETCH NEXT FROM contact_curs INTO @contact
	WHILE @@FETCH_STATUS=0
	BEGIN 
		SET @RecipientList = @RecipientList + @Contact + '; '
		FETCH NEXT FROM contact_curs INTO @contact
	END
	CLOSE contact_curs
	DEALLOCATE contact_curs

	--QC Recipient List
	--Set @Bodymessage = @BodyMessage + '<br><br>Recipients ' + @RecipientList

	--Execute stored procedure to send the email to that firm
	BEGIN
		--Execute stored procedure to send the email to that firm
		EXEC msdb.dbo.sp_send_dbmail
			--The DanaProfile sends from Dana@kcicllc.com
			@profile_name='Claims', --DanaProfile
			@recipients = @RecipientList,
			@blind_copy_recipients = 'dana@kcic.com; prossc@kcic.com; khanm@kcic.com',
			--@recipients = 'hagep@kcic.com;jacksonc@kcic.com',
			@subject = @subject,
			@body = @BodyMessage,
			@body_format = 'html'
	END
FETCH NEXT FROM firm_curs INTO @PlaintiffCounselAddressID, @firmURL
END
CLOSE firm_curs
DEALLOCATE firm_curs
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_EMAIL_qryExternalUnexecutedReleaseUploaded] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_EMAIL_qryExternalUnexecutedReleaseUploaded] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_EMAIL_qryExternalUnexecutedReleaseUploaded] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_EMAIL_qryExternalUnexecutedReleaseUploaded] TO [power_user]
GO
