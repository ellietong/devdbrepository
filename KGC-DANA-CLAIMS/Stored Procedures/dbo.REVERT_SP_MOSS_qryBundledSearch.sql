SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryBundledSearch] 
(@ClaimantName nvarchar(max), @State nvarchar(250), @InjuredPartyID bigint, @DocketNumber nvarchar(250))
AS
SET NOCOUNT ON


--Declare @InjuredPartyID bigint
--Set @InjuredPartyID = null
--Declare @ClaimantName nvarchar(max)
--Set @ClaimantName = 'Blake'
--Declare @DocketNumber nvarchar(250)
--Set @DocketNumber = null
--Declare @State nvarchar(250)
--Set @State = 'MS'


DECLARE @ClaimantNameLocal nvarchar(max)
SET @ClaimantNameLocal = @ClaimantName
DECLARE @InjuredPartyIDLocal bigint
SET @InjuredPartyIDLocal = @InjuredPartyID



DECLARE @MaxInjuredPartyID bigint
SET @MaxInjuredPartyID = (SELECT MAX(Injured_Party_Id) FROM m_tblBundledInjuredParty)


Begin

select ip.Injured_Party_Id,c.Claim_Sequence_Nbr,ip.Last_Name + ', ' + CASE WHEN ip.First_Name IS NULL 
	THEN '' ELSE ip.First_Name + ' ' END AS ClaimantName, ip.SS_Nbr, c.[Diagnosis Desc], c.Claim_Status_Cd,c.[Release_Type Desc], 
c.Release_Received_Date, c.Settlement_Date, l.StateFiled AS [State], l.JurisdictionDescription, l.DocketNum AS DocketNumber, l.FileDate,
c.Settlement_Amt,
sum(bh.DNA_Share) AS 'Billed Amount'
from m_tblBundledInjuredParty ip
inner join m_tblBundledClaim c on ip.Injured_Party_Id = c.Injured_Party_Id
inner join m_tblBundledLawsuit l on c.Injured_Party_Id = l.InjuredPartyID and c.Claim_Sequence_Nbr  = l.ClaimSequenceNum
left join m_tblBundledBilledHistory bh on bh.Injured_Party_Id = c.Injured_Party_Id and bh.[Claim Sequence_Nbr] = c.Claim_Sequence_Nbr
Where Coalesce(l.statefiled, '') like '%'+coalesce(@State, '')+'%'
	AND((ip.Last_Name + ', ' + CASE WHEN ip.First_Name IS NULL 
					THEN '' ELSE ip.First_Name + ' ' END) like '%'+coalesce(@ClaimantNameLocal, '')+'%')
	AND DocketNum LIKE '%' + COALESCE(@DocketNumber, '') + '%'
	AND coalesce(ip.Injured_Party_Id,'0') <= COALESCE(@InjuredPartyIDLocal, @MaxInjuredPartyID) AND coalesce(ip.Injured_Party_Id,'0') >= COALESCE(@InjuredPartyIDLocal, 0)
and l.LeadLawsuitFlag = 'y'
group by ip.Injured_Party_Id, c.Claim_Sequence_Nbr,ip.Last_Name, ip.First_Name, ip.SS_Nbr, c.[Diagnosis Desc], c.Claim_Status_Cd,c.[Release_Type Desc], 
c.Release_Received_Date, c.Settlement_Date, l.StateFiled, l.CourtType, l.JurisdictionDescription, l.DocketNum, l.FileDate,
c.Settlement_Amt 
order by ClaimantName ASC, c.Claim_Sequence_Nbr ASC



END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryBundledSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryBundledSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryBundledSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryBundledSearch] TO [power_user]
GO
