SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_Reports_qryYearlyNewFilings] 
	-- Add the parameters for the stored procedure here
	(@YearID int, @MatterID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @yearID int
--Set @YearId=2010
DECLARE @PreviousMonthYTD int
set @PreviousMonthYTD = case when @yearID = 2011 then (MONTH(GETDATE())-1) else 12 end

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)
select COUNT(distinct(cl.ClaimLawsuitID))
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s on l.StateID=s.StateID
where year(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate))=@YearID 
and MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate)) <=@PreviousMonthYTD
--and IsPrimaryStatus=1
AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
AND ClaimLawsuitStatusID=22
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_Reports_qryYearlyNewFilings] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_Reports_qryYearlyNewFilings] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_Reports_qryYearlyNewFilings] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_Reports_qryYearlyNewFilings] TO [power_user]
GO
