SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Procedure [dbo].[SP_MOSS_qryState]
As

SET NOCOUNT ON

SELECT StateID, State, FullNameState
FROM tblState
UNION 
SELECT null, '', ''
ORDER BY State

RETURN



GO
