SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_MATTER_qryMatterEntityList]
AS
SET NOCOUNT ON


SELECT MatterEntityID, 
	MatterEntity 
FROM tblMatterEntity
UNION
SELECT NULL, ''
ORDER BY MatterEntityID



GO
