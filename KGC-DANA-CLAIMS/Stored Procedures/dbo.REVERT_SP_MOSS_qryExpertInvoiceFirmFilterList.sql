SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryExpertInvoiceFirmFilterList]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ExpertFirmID, ExpertFirm FROM m_tblExpertFirm
	UNION
	SELECT null, '<< All Expert Firms >>'
	ORDER BY ExpertFirm
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryExpertInvoiceFirmFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryExpertInvoiceFirmFilterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryExpertInvoiceFirmFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryExpertInvoiceFirmFilterList] TO [power_user]
GO
