SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_RELATED_PARTY_qryClaimantRelatedPartyInfo] (@ClaimantPersonalInfoID bigint = NULL, 
																	   @emailaddress nvarchar(250))
AS

SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @emailaddress nvarchar(250)
--DECLARE @ClaimantPersonalInfoID bigint
--SET @emailaddress = 'cgong@cetcap.com'
--SET @ClaimantPersonalInfoID = 251129

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE crp.ClaimantPersonalInfoID = ' + CONVERT(nvarchar(20),@ClaimantPersonalInfoID)


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' '
ELSE
BEGIN
	SET @JoinString = ' LEFT  JOIN tblClaim c					ON c.ClaimantPersonalInfoID = crp.ClaimantPersonalInfoID
						LEFT  JOIN tblClaimLawsuit cl			ON c.ClaimID = cl.ClaimID
                        LEFT  JOIN tblLawsuit l					ON cl.LawsuitID = l.LawsuitID	
						LEFT JOIN tblMatter m					 ON c.MatterID = m.MatterID					
                        LEFT  JOIN tblDefenseCounselAddress dca ON l.DefenseCounselAddressID=dca.DefenseCounselAddressID
						LEFT  JOIN tblDefenseJurisdiction dj	ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND dj.MatterEntityID = m.MatterEntityID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT ClaimantRelatedPartyID, 
		crp.ClaimantPersonalInfoID,  
		LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' END AS RelatedPartyName,
		LastName, 
		FirstName, 
		MiddleName, 
		Relationship, 
		lc.LegalCapacity,
		CASE WHEN crp.RPSocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-'' + RIGHT(crp.RPSocialSecurityNumber, 4) END AS SSN,
		COALESCE(CONVERT(nvarchar(15), crp.RPSocialSecurityNumber), '''') AS FullSSN, 
		crp.RelationshipID, 
		crp.LegalCapacityID
	FROM tblClaimantRelatedParty crp 
		LEFT JOIN tblRelationship r		ON r.RelationshipID=crp.RelationshipID
		LEFT JOIN tblLegalCapacity lc	ON lc.LegalCapacityID = crp.LegalCapacityID
		 ' +
		@JoinString +
    @WhereString + '
    OPTION (RECOMPILE)'

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryClaimantRelatedPartyInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryClaimantRelatedPartyInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryClaimantRelatedPartyInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryClaimantRelatedPartyInfo] TO [power_user]
GO
