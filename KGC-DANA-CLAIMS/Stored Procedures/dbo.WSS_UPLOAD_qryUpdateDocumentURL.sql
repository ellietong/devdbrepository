SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryUpdateDocumentURL] 
	(@DocumentID bigint, @DocumentURL nvarchar(MAX))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE tblDocuments
	SET DocumentURL = @DocumentURL
	WHERE DocumentID = @DocumentID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdateDocumentURL] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdateDocumentURL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdateDocumentURL] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdateDocumentURL] TO [power_user]
GO
