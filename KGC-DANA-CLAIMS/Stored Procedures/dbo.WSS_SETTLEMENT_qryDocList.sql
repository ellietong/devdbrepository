SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryDocList] (@ClaimLawsuitID int, 
											  @emailaddress nvarchar(250))
AS

SET NOCOUNT ON;

----The following set of variables can be used for testing.
--DECLARE @emailaddress nvarchar(250)
--DECLARE @ClaimLawsuitID bigint
--SET @emailaddress = 'ccrook@gmail.com'
--SET @ClaimLawsuitID = 23466


DECLARE @PlaintiffCounselAddressID bigint
DECLARE @QueryString nvarchar(max)

SET @PlaintiffCounselAddressID = (SELECT PlaintiffCOunselAddressID FROM vtblPerson WHERE Email = @emailaddress)

IF (@PlaintiffCounselAddressID IS NOT NULL)
BEGIN
	SET @QueryString = '
	SELECT  d.DocumentID AS DocumentID, COALESCE(ClaimantDocumentType +'': '','''') + DOCUMENTTITLE AS DocumentTitle
	FROM tblDocuments d
	INNER JOIN vtblPerson p ON d.CreatedBy = p.Email
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaim c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN  tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	WHERE p.PlaintiffCounselAddressID = ' + CONVERT(nvarchar, @PlaintiffCounselAddressID)
	+ ' AND cl.ClaimLawsuitID = ' + CONVERT(nvarchar, @ClaimLawsuitID)
	+ ' UNION
		 SELECT NULL AS DocumentID, ''-- Select a Document --''  AS DocumentTitle
		 ORDER BY 2'
		 

END
ELSE
BEGIN
		SET @QueryString = '
		SELECT  d.DocumentID AS DocumentID, COALESCE(ClaimantDocumentType +'': '','''') + DOCUMENTTITLE AS DocumentTitle
		FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaim c ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
		LEFT JOIN  tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
		WHERE cl.ClaimLawsuitID = ' + CONVERT(nvarchar, @ClaimLawsuitID)
		+ ' UNION
		 SELECT NULL AS DocumentID, ''-- Select a Document --''  AS DocumentTitle
		 ORDER BY 2'
		 
END


--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryDocList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryDocList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryDocList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryDocList] TO [power_user]
GO
