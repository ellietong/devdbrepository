SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryGetFirmByEmail_Filter]
	-- Add the parameters for the stored procedure here
	@emailaddress nvarchar(1000)
AS

DECLARE @firmID int
SET @firmID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)
BEGIN
	DECLARE @emaildomain nvarchar(1000)
	SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress), LEN(@emailaddress)-CHARINDEX('@', @emailaddress)+1)
	IF @emaildomain = '@kcicllc.com' OR @firmID IN (132, 114, 10, 100) --Dana, Cooper Walinski, DeHay & Celba
	BEGIN
		SELECT DISTINCT DefenseCounselID AS FirmID, DefenseCounsel as Firm 
		FROM tblDefenseCounsel
		WHERE DefenseCounselID NOT IN ( 135, 136)
		UNION 
		SELECT NULL, '<< All Firms >>'
		ORDER BY DefenseCounsel
	END
	ELSE
	BEGIN
		Select dca.DefenseCounselID As FirmID, dca.DefenseCounsel as Firm
		From dbo.tblDefenseCounselState AS dcs 
		left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
		LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid 
		LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
		left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
		Where (ResponsibleDefenseCounselAddressID IN (SELECT DefenseCounselAddressID FROM vtblPerson p inner join tblDefenseCounselAddress da on p.FirmID=da.DefenseCounselID WHERE Email = @emailaddress))
		UNION 
		SELECT NULL, '<< All Firms >>'
		ORDER BY dca.DefenseCounsel
	END
		
	 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryGetFirmByEmail_Filter] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryGetFirmByEmail_Filter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryGetFirmByEmail_Filter] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryGetFirmByEmail_Filter] TO [power_user]
GO
