SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryClaimantSearch] 
(@ClaimantName nvarchar(max), @SSN nvarchar(20), @StateID int, @PlaintiffCounselAddressID int, @ClaimantID int, @NavLegacyID bigint,
  @MatterID int, @IsMDL int, @emailaddress nvarchar(250), @DocketNumber nvarchar(250), @ClaimLawsuitStatusID int)
AS
SET NOCOUNT ON
DECLARE @ClaimantNameLocal nvarchar(max)
SET @ClaimantNameLocal = @ClaimantName
DECLARE @MatterIDLocal int
SET @MatterIDLocal = @MatterID
DECLARE @SSNLocal nvarchar(20)
SET @SSNLocal = @SSN
DECLARE @StateIDLocal int
SET @StateIDLocal = @StateID
DECLARE @PlaintiffCounselAddressIDLocal int
SET @PlaintiffCounselAddressIDLocal = @PlaintiffCounselAddressID
DECLARE @ClaimantIDLocal bigint
SET @ClaimantIDLocal = @ClaimantID
DECLARE @NavLegacyIDLocal bigint
SET @NavLegacyIDLocal = @NavLegacyID
DECLARE @IsMDLLocal int
SET @IsMDLLocal = @IsMDL
DECLARE @ClaimLawsuitStatusIDLocal int
SET @ClaimLawsuitStatusIDLocal = @ClaimLawsuitStatusID
DECLARE @emailaddressLocal nvarchar(250)
SET @emailaddressLocal = @emailaddress


DECLARE @MaxState int
SET @MaxState = (SELECT MAX(StateID) FROM tblState)
DECLARE @MaxPC int
SET @MaxPC = (SELECT MAX(PlaintiffCounselAddressID) FROM tblPlaintiffCounselAddress)
DECLARE @MaxClaimantID bigint
SET @MaxClaimantID = (SELECT MAX(ClaimantPersonalInfoID) FROM tblClaimantPersonalInfo)
DECLARE @MaxMatter int
SET @MaxMatter = (SELECT MAX(MatterID) FROM tblMatter)
DECLARE @MaxNavLegacyID bigint
SET @MaxNavLegacyID = (SELECT MAX(NavLegacyID) FROM tblClaimantPersonalInfo)
DECLARE @MaxMDL int
SET @MaxMDL = 1
DECLARE @MaxClaimLawsuitStatusID bigint
SET @MaxClaimLawsuitStatusID = (SELECT MAX(ClaimLawsuitStatusID) FROM tblClaimLawsuitStatus)

Declare @UserFirmState table (DefenseCounselID bigint, StateID int)
Insert into @UserFirmState

Select dca.DefenseCounselID, StateID
From dbo.tblDefenseCounselState AS dcs 
left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid --and dcar.stateID = dcs.StateID 
LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
Where (ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddressLocal) OR
dca.DefenseCounselID in (SELECT f.FirmID FROM vtblPerson p inner join vtblFirm f on p.FirmID=f.FirmID WHERE Email = @emailaddressLocal))

----If user is Dana or KCIC or DeHay specific users
IF((SELECT FirmID FROM vtblPerson WHERE Email = @emailaddressLocal) IN (114, 131, 132, 133)) OR RIGHT(@emailaddressLocal, 11) = 'kcicllc.com' OR ((Select PersonID FROM vtblPerson WHERE Email = @emailaddressLocal) IN (1376,1377,1378,1383,1384,1389,1392))  OR (SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1
BEGIN
	SELECT DISTINCT
						  CPI.ClaimantPersonalInfoID AS ClaimantID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
						  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, 
	                      
						  CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
						  BirthDate, Matter, Coalesce(PlaintiffCounsel, 'Unknown') + ' (' + Coalesce(pca.PlaintiffCounselCity, 'Unknown') + ')' As PlaintiffCounsel,
						  cpi.NavLegacyID, s.[State], IsMDL, ClaimLawsuitStatus, DocketNumber
	                      
						  --ROW_NUMBER() OVER (ORDER BY cpi.LastName, cpi.Firstname) AS ResultSetRowNumber
	                        
	FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
						  dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
						  --LEFT JOIN tblClaimantDocketNumbers cdn ON CPI.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
						  LEFT JOIN tblClaimLawsuit CL ON c.ClaimID = cl.ClaimID
						  LEFT JOIN tblLawsuit L ON cl.LawsuitID = L.LawsuitID
						  LEFT JOIN tblMatter M ON c.MatterID = M.MatterID
						  LEFT JOIN tblPlaintiffCounselAddress PCA ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
						  LEFT JOIN tblState s ON l.StateID = s.StateID
						  LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
						  LEFT JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	WHERE l.StateID BETWEEN COALESCE(@StateIDLocal, 0) AND COALESCE(@StateIDLocal, @MaxState)
			AND  coalesce(l.PlaintiffCounselAddressID,0) >= COALESCE(@PlaintiffCounselAddressIDLocal, 0) AND  coalesce(l.PlaintiffCounselAddressID,0) <= COALESCE(@PlaintiffCounselAddressIDLocal, @MaxPC)
			AND cpi.ClaimantPersonalInfoID >= COALESCE(@ClaimantIDLocal, 0) AND cpi.ClaimantPersonalInfoID <= COALESCE(@ClaimantIDLocal, @MaxClaimantID)
			AND c.MatterID >= COALESCE(@MatterIDLocal, 0) AND c.MatterID <= COALESCE(@MatterIDLocal, @MaxMatter)
			AND ((CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
				THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' end) like '%'+coalesce(@ClaimantNameLocal, '')+'%')
			AND Coalesce(socialsecurityNumber, '') like '%'+coalesce(@SSNLocal, '')+'%'
			AND coalesce(cpi.NavLegacyID,'0') <= COALESCE(@NavLegacyIDLocal, @MaxNavLegacyID) AND coalesce(cpi.NavLegacyID,'0') >= COALESCE(@NavLegacyIDLocal, 0)
			AND COALESCE(IsMDL, 0) >= COALESCE(@IsMDLLocal, 0) AND COALESCE(IsMDL, 0) <= COALESCE(@IsMDLLocal, @MaxMDL)
			AND DocketNumber LIKE '%' + COALESCE(@DocketNumber, '') + '%'
			AND clst.ClaimLawsuitStatusID >= COALESCE(@ClaimLawsuitStatusIDLocal, 0) AND clst.ClaimLawsuitStatusID <= COALESCE(@ClaimLawsuitStatusIDLocal, @MaxClaimLawsuitStatusID)
	ORDER BY ClaimantName
END
ELSE
BEGIN
	SELECT DISTINCT
						  CPI.ClaimantPersonalInfoID AS ClaimantID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
						  THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END AS ClaimantName, 
	                      
						  CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
						  BirthDate, Matter, Coalesce(PlaintiffCounsel, 'Unknown') + ' (' + Coalesce(pca.PlaintiffCounselCity, 'Unknown') + ')' As PlaintiffCounsel,
						  cpi.NavLegacyID, s.[State], IsMDL, ClaimLawsuitStatus, DocketNumber
	                      
						  --ROW_NUMBER() OVER (ORDER BY cpi.LastName, cpi.Firstname) AS ResultSetRowNumber
	                        
	FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
						  dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
						  --LEFT JOIN tblClaimantDocketNumbers cdn ON CPI.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
						  LEFT JOIN tblClaimLawsuit CL ON c.ClaimID = cl.ClaimID
						  LEFT JOIN tblLawsuit L ON cl.LawsuitID = L.LawsuitID
									INNER JOIN tblDefenseCounselState dcs on l.DefenseCounselAddressID=dcs.DefenseCounselAddressID and l.StateID=dcs.StateID
									INNER JOIN tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCounselAddressID=dca.DefenseCounselAddressID
									INNER JOIN @UserFirmState u on dca.DefenseCounselID = u.DefenseCounselID and l.StateID = u.StateID
						  LEFT JOIN tblMatter M ON c.MatterID = M.MatterID
						  LEFT JOIN tblPlaintiffCounselAddress PCA ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
						  LEFT JOIN tblState s ON l.StateID = s.StateID
						  LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
						  LEFT JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
	WHERE l.StateID BETWEEN COALESCE(@StateIDLocal, 0) AND COALESCE(@StateIDLocal, @MaxState)
			AND   coalesce(l.PlaintiffCounselAddressID,0) >= COALESCE(@PlaintiffCounselAddressIDLocal, 0) AND  coalesce(l.PlaintiffCounselAddressID,0) <= COALESCE(@PlaintiffCounselAddressIDLocal, @MaxPC)
			AND cpi.ClaimantPersonalInfoID >= COALESCE(@ClaimantIDLocal, 0) AND cpi.ClaimantPersonalInfoID <= COALESCE(@ClaimantIDLocal, @MaxClaimantID)
			AND c.MatterID >= COALESCE(@MatterIDLocal, 0) AND c.MatterID <= COALESCE(@MatterIDLocal, @MaxMatter)
			AND ((CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
				THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' end) like '%'+coalesce(@ClaimantNameLocal, '')+'%')
			AND Coalesce(socialsecurityNumber, '') like '%'+coalesce(@SSNLocal, '')+'%'
			AND coalesce(cpi.NavLegacyID,'0') <= COALESCE(@NavLegacyIDLocal, @MaxNavLegacyID) AND coalesce(cpi.NavLegacyID,'0') >= COALESCE(@NavLegacyIDLocal, 0)
			AND COALESCE(IsMDL, 0) >= COALESCE(@IsMDLLocal, 0) AND COALESCE(IsMDL, 0) <= COALESCE(@IsMDLLocal, @MaxMDL)
			AND DocketNumber LIKE '%' + COALESCE(@DocketNumber, '') + '%'
			AND clst.ClaimLawsuitStatusID >= COALESCE(@ClaimLawsuitStatusIDLocal, 0) AND clst.ClaimLawsuitStatusID <= COALESCE(@ClaimLawsuitStatusIDLocal, @MaxClaimLawsuitStatusID)
	ORDER BY ClaimantName
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantSearch] TO [power_user]
GO
