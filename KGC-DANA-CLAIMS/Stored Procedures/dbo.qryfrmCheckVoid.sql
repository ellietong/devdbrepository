SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Procedure [dbo].[qryfrmCheckVoid] (@CheckID int)
as
Set NoCount On

Update tblCheck Set SourceAmount = 0, FundingSourceID = 3
Where CheckID = @CheckID

Update tblPaymentDefense Set PaymentAmount = 0
Where CheckID = @CheckID

Update tblPaymentDefense Set IsFullyPaid = 0
Where DefenseInvoiceID in (Select DefenseInvoiceID From tblPaymentDefense where CheckID = @CheckID)

Update tblPaymentSettlement Set PaymentAmount = 0
Where CheckID = @CheckID

Update tblPaymentSettlement Set IsFullyPaid = 0
Where ClaimantSettlementID in (Select ClaimantSettlementID From tblPaymentSettlement where CheckID = @CheckID)

Update tblPaymentLegalBill Set PaymentAmount = 0
Where CheckID = @CheckID

Update tblPaymentLegalBill Set IsFullyPaid = 0
Where InvoiceID in (Select InvoiceID From tblPaymentLegalBill where CheckID = @CheckID)


Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckVoid] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckVoid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckVoid] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheckVoid] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckVoid] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckVoid] TO [power_user]
GO
