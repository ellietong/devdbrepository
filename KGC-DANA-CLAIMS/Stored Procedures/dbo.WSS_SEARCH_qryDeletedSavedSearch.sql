SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryDeletedSavedSearch]
	-- Add the parameters for the stored procedure here
	@SavedSearchID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--Delete ClaimantIDs    
DELETE FROM tblSavedClaimantIds
WHERE SavedSearchID = @SavedSearchID

--Delete Claimant Name Record(s)
DELETE FROM tblSavedClaimantName
WHERE SavedSearchID = @SavedSearchID

--Delete Docket Number Record(s)
DELETE FROM tblSavedDocketNumber
WHERE SavedSearchID = @SavedSearchID

--Delete Lawsuit Status ID Record(s)
DELETE FROM tblSavedLawsuitStatus
WHERE SavedSearchID = @SavedSearchID

--Delete MatterEntityID Record(s)
DELETE FROM tblSavedMatterEntity
WHERE SavedSearchID = @SavedSearchID

--Delete MatterID Record(s)
DELETE FROM tblSavedMatter
WHERE SavedSearchID = @SavedSearchID

--Delete PlaintiffCounselAddressID Record(s)
DELETE FROM tblSavedPlaintiffCounsel
WHERE SavedSearchID = @SavedSearchID

--Delete SSN Record(s)
DELETE FROM tblSavedSSN
WHERE SavedSearchID = @SavedSearchID

--Delete StateID Record(s)
DELETE FROM tblSavedState
WHERE SavedSearchID = @SavedSearchID

--Delete IsMDL Record(s)
DELETE FROM tblSavedIsMDL
WHERE SavedSearchID = @SavedSearchID

--Deleted SavedSearch Record
DELETE FROM tblSavedSearch
WHERE SavedSearchID = @SavedSearchID
END


GO
