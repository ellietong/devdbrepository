SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] (@emailaddress nvarchar(1000) = NULL)
AS

--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'vicki.stringham@danacollc.com'

DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

IF ((@DefenseCounselID = 132) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'))
BEGIN
	SELECT CAST(d.DocumentID as varchar(50)) as DocumentId,
		d.SPDocId,
		d.DocumentTitle,
		d.DocumentURL,				
		d.FirmID,
		f.Firm,		
		dt.ClaimantDocumentType, 
		COALESCE(ExpertFirm, '') AS ExpertFirm,
		d.Period,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal,
		d.DatePayable,
		dis.[Status],
		d.AcceptedBy,
		d.DateAccepted,
		d.DateCreated
	FROM tblDocuments d
		INNER JOIN tblClaimantDocumentType dt	ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		INNER JOIN tblDocumentGroup dg			ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
		LEFT JOIN  tblDefenseInvoiceFirm dif	ON di.ExpertFirmID = dif.FirmID 
		LEFT JOIN  tblDefenseInvoiceStatus dis	ON d.StatusID = dis.StatusID 
		LEFT JOIN  tblMatter m					ON d.MatterID = m.MatterID
		LEFT JOIN  vtblFirm f					ON d.FirmID = f.FirmID
		LEFT JOIN  m_tblExpertFirm ex			ON d.ExpertFirmID = ex.ExpertFirmID
		LEFT JOIN  tblPaymentDefense pd			ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN  tblCheck c					ON pd.CheckID = c.CheckID
	WHERE dt.DocumentGroupID = 2 
		AND d.StatusID IS NULL 
		AND ((coalesce(d.statusid,0)!=5) or (coalesce(di.StatusID,0)!=5))
	ORDER BY Accepted ASC, DateCreated DESC
END 
--ELSE BEGIN

--SELECT 
--cast(d.DocumentID as varchar(50)) as DocumentId,
--dt.ClaimantDocumentType, 
--dg.DocumentGroup, 	
--d.Comments,
--d.DocumentURL,
--d.DocumentTitle,
--d.SPDocId,
--d.CreatedBy,
--d.DateCreated,
--d.ModifiedBy,
--d.DateModified,
--d.Accepted,
--d.AcceptedBy,
--d.DateAccepted,
--d.ApprovedBy,
--d.DateApproved,
--f.Firm,
--d.InvoiceNumber,
--d.Period,
--d.DatePayable,
--d.FeeAmount,
--d.DisbursementAmount,
--d.Adjustment,
--d.AdjustmentNotes,
--dis.Status,	
--dif.Firm DefenseInvoiceFirm, 	
--m.Matter,
--di.DatePaid, 
--di.CheckNumber,
--d.FirmID,
--SourceDate,
--(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal,
--COALESCE(ExpertFirm, '') AS ExpertFirm
--FROM tblDocuments d
--JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
--JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
--LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
--LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.ExpertFirmID = dif.FirmID 
--LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
--LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
--LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
--LEFT OUTER JOIN m_tblExpertFirm ex ON d.ExpertFirmID = ex.ExpertFirmID
--LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
--LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
--WHERE dg.DocumentGroupID = 2 AND d.StatusID IS NULL AND ((coalesce(d.statusid,0)!=5) or (coalesce(di.StatusID,0)!=5))
--AND f.FirmID = @DefenseCounselID
--ORDER BY Accepted ASC, DateCreated DESC
--END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICE_qryFirmInvoices_Outstanding] TO [power_user]
GO
