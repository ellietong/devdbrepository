SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryUpdClaim]
	(@ClaimDiseaseID bigint, @DiseaseID int, @ActiveTrialDate datetime, @LawsuitID bigint,
	@ILODoctorID int, @PFTDoctorID int, 
	@ILOProfusion nvarchar(100), @ILODate datetime,
    @PFTDate datetime, @PFTTypeID int, 
    @PFTFVC_Actual float, @PFTFVC_Predicted float,
    @PFTFEV1_Actual float, @PFTFEV1_Predicted float,
    @PFTFEV1FVC_Actual float, @PFTFEV1FVC_Predicted float,
    @PFTTLC_Actual float, @PFTTLC_Predicted float,
    @PFTDLCO_Actual float, @PFTDLCO_Predicted float,
    @PFTTV float, @Username nvarchar(250),
    @ClaimID bigint, @DiseaseDiagnosisDate datetime
	
	)
AS
BEGIN
	UPDATE tblClaimDisease
	SET DiseaseID = @DiseaseID,
	    SPUsername = @Username,
	    DiseaseDiagnosisDate = @DiseaseDiagnosisDate
	WHERE ClaimDiseaseID = @ClaimDiseaseID
	
	UPDATE tblLawsuit
	SET ActiveTrialDate = @ActiveTrialDate,
	    SPUsername = @Username
	WHERE LawsuitID = @LawsuitID
	
	UPDATE tblClaim
	SET ILODoctorID = COALESCE(@ILODoctorID, ILODoctorID),
      ILOProfusion = COALESCE(@ILOProfusion, ILOProfusion),
      ILODate = COALESCE(@ILODate,ILODate),
      PFTDoctorID = COALESCE(@PFTDoctorID,PFTDoctorID),
      PFTDate = COALESCE(@PFTDate,PFTDate),
      PFTTypeID = COALESCE(@PFTTypeID,PFTTypeID),
      PFTFVC_Actual = COALESCE(@PFTFVC_Actual,PFTFVC_Actual),
      PFTFVC_Predicted = COALESCE(@PFTFVC_Predicted,PFTFVC_Predicted),
      PFTFEV1_Actual = COALESCE(@PFTFEV1_Actual,PFTFEV1_Actual),
      PFTFEV1_Predicted = COALESCE(@PFTFEV1_Predicted,PFTFEV1_Predicted),
      [PFTFEV1andFVC_Actual] = COALESCE(@PFTFEV1FVC_Actual,[PFTFEV1andFVC_Actual]),
      [PFTFEV1andFVC_Predicted] = COALESCE(@PFTFEV1FVC_Predicted,[PFTFEV1andFVC_Predicted]),
      PFTTLC_Actual = COALESCE(@PFTTLC_Actual,PFTTLC_Actual),
      PFTTLC_Predicted = COALESCE(@PFTTLC_Predicted,PFTTLC_Predicted),
      PFTDLCO_Actual = COALESCE(@PFTDLCO_Actual,PFTDLCO_Actual),
      PFTDLCO_Predicted = COALESCE(@PFTDLCO_Predicted,PFTDLCO_Predicted),
      PFTTV = COALESCE(@PFTTV,PFTTV),
      SPUsername = @Username
      WHERE ClaimID = @ClaimID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdClaim] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdClaim] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdClaim] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdClaim] TO [power_user]
GO
