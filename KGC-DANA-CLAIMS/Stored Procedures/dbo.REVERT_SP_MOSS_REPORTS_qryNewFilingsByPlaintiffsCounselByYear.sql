SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilingsByPlaintiffsCounselByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @YearID int, @MonthID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @year int
--Set @Year=2010

--select pc.PlaintiffCounsel, COUNT(cl.ClaimLawsuitID) AS Count
--from tblClaim c 
--inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
--inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
--inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
--inner join tblPlaintiffCounselAddress_ALT pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
--inner join tblPlaintiffCounsel_ALT pc on pc.PlaintiffCounselID=pca.PlaintiffCounselID
--where year(coalesce(ServiceDate,FileDate,ReceivedDate))=@YearID and IsPrimaryStatus=1
--group by pc.PlaintiffCounsel
--order by COUNT(cl.ClaimLawsuitID) desc

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @year int
SET @year = @YearID

DECLARE @Month int
set @Month= @MonthID


select pca.PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
where	year(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate))=@Year
		and MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate)) <=@month
		--and IsPrimaryForClaim = 1 
		--and IsPrimaryStatus=1
		and MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
		AND ClaimLawsuitStatusID=22
group by pca.PlaintiffCounsel
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, pca.PlaintiffCounsel


END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilingsByPlaintiffsCounselByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilingsByPlaintiffsCounselByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilingsByPlaintiffsCounselByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilingsByPlaintiffsCounselByYear] TO [power_user]
GO
