SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryMultiClaimantSearch] 
(@ClaimantName nvarchar(max), @SSN nvarchar(20), @StateID int, @PlaintiffCounselAddressID int, @NavLegacyID bigint,
  @MatterID int, @IsMDL int, @emailaddress nvarchar(250), @DocketNumber nvarchar(250), @ClaimLawsuitStatusID int, @ClaimantIDs integer_list_tbltype READONLY)
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantName nvarchar(max)
--DECLARE @SSN nvarchar(20)
--DECLARE @StateID int
--DECLARE @PlaintiffCounselAddressID int
--DECLARE @NavLegacyID bigint
--DECLARE @MatterID int
--DECLARE @IsMDL int
--DECLARE @emailaddress nvarchar(250)
--DECLARE @DocketNumber nvarchar(250)
--DECLARE @ClaimLawsuitStatusID int
--DECLARE @ClaimantIDs TABLE (n int) 
--SET @ClaimantName = 'smith'
--SET @SSN = null
--SET @StateID = NULL
--SET @PlaintiffCounselAddressID = NULL
--SET @NavLegacyID = NULL
--SET @MatterID = NULL
--SET @IsMDL = NULL
--SET @emailaddress = 'youngc@kcicllc.com'
--SET @DocketNumber = NULL
--SET @ClaimLawsuitStatusID = NULL

--This temp table tracks defense counsels by state, used below to determine permissions for the resulting data sets.
IF object_id('tempdb..#UserFirmState') IS NOT NULL
BEGIN
   DROP TABLE #UserFirmState
END
CREATE TABLE #UserFirmState (DefenseCounselID bigint, StateID int)
INSERT INTO #UserFirmState
	SELECT dca.DefenseCounselID, StateID
	FROM tblDefenseCounselState AS dcs 
		LEFT JOIN tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = dcs.DefenseCounselAddressID
		LEFT JOIN tblDefenseCounselAddress dcar on dcar.DefenseCounselAddressID = dcs.ResponsibleDefenseCounselAddressID
		LEFT JOIN tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
		LEFT JOIN tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
	WHERE (ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) 
		OR dca.DefenseCounselID in (SELECT f.FirmID FROM vtblPerson p INNER JOIN vtblFirm f ON p.FirmID = f.FirmID WHERE Email = @emailaddress))

--This temp table pulls the claimant ids from the table passed in above, putting them in a form usable by the queries below.
IF object_id('tempdb..#ClaimantIDs') IS NOT NULL
BEGIN
   DROP TABLE #ClaimantIDs
END
CREATE TABLE #ClaimantIDs (n int)
INSERT INTO #ClaimantIDs
	SELECT n FROM @ClaimantIDs

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11) = 'kcicllc.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ''
ELSE
	SET @JoinString = ' INNER JOIN tblDefenseCounselState dcs on l.DefenseCounselAddressID=dcs.DefenseCounselAddressID and l.StateID=dcs.StateID
						INNER JOIN tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCounselAddressID=dca.DefenseCounselAddressID
						INNER JOIN #UserFirmState u on dca.DefenseCounselID = u.DefenseCounselID and l.StateID = u.StateID '

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE 1=1'
IF @ClaimantName			  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND ((CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' ELSE MiddleName + '' '' end) like ''%'' + @ClaimantNameParam + ''%'')' END
IF @SSN						  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(socialsecurityNumber, '''') like ''%'' + @SSNParam + ''%''' END
IF @StateID					  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND l.StateID = ' + CONVERT(nvarchar(20), @StateID) END
IF @PlaintiffCounselAddressID IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(l.PlaintiffCounselAddressID,0) = ' + CONVERT(nvarchar(20),@PlaintiffCounselAddressID) END
IF @NavLegacyID				  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(cpi.NavLegacyID,''0'') = ' + CONVERT(nvarchar(20),@NavLegacyID) END
IF @MatterID				  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar(20),@MatterID) END
IF @IsMDL					  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND COALESCE(IsMDL, 0) = ' + CONVERT(nvarchar(20),@IsMDL) END
IF @DocketNumber			  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND DocketNumber LIKE ''%'' + @DocketNumberParam + ''%''' END
IF @ClaimLawsuitStatusID	  IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND clst.ClaimLawsuitStatusID = ' + CONVERT(nvarchar(20),@ClaimLawsuitStatusID) END
IF (SELECT COUNT(n) FROM #ClaimantIDs) <> 0 BEGIN SET @WhereString = @WhereString + ' AND cpi.ClaimantPersonalInfoID IN(SELECT n FROM #ClaimantIDs)' END


--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT CPI.ClaimantPersonalInfoID AS ClaimantID, 
		CPI.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '' '' ELSE MiddleName + '' '' END AS ClaimantName, 
		CASE WHEN SocialSecurityNumber IS NULL THEN '''' ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
		BirthDate, Matter, Coalesce(PlaintiffCounsel, ''Unknown'') + '' ('' + Coalesce(pca.PlaintiffCounselCity, ''Unknown'') + '')'' As PlaintiffCounsel,
		cpi.NavLegacyID, s.[State], IsMDL, ClaimLawsuitStatus, DocketNumber 
	 FROM dbo.tblClaimantPersonalInfo AS CPI
		INNER JOIN dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit CL ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit L ON cl.LawsuitID = L.LawsuitID ' + 
		@JoinString + '
		LEFT JOIN tblMatter M ON c.MatterID = M.MatterID
		LEFT JOIN tblPlaintiffCounselAddress PCA ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		LEFT JOIN tblState s ON l.StateID = s.StateID
		LEFT JOIN (SELECT * FROM tblClaimLawsuitStatusTracking WHERE COALESCE(IsPrimaryStatus, 0) = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
		LEFT JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID' + 
	 @WhereString + '
	 ORDER BY ClaimantName 
	 OPTION (RECOMPILE)';


--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@ClaimantNameParam nvarchar(max), @SSNParam nvarchar(max), @DocketNumberParam nvarchar(max)', @ClaimantNameParam = @ClaimantName, @SSNParam = @SSN, @DocketNumberParam = @DocketNumber
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryMultiClaimantSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryMultiClaimantSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryMultiClaimantSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryMultiClaimantSearch] TO [power_user]
GO
