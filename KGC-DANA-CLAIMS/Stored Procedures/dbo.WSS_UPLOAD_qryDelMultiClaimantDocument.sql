SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryDelMultiClaimantDocument] (@MultiClaimantDocumentID bigint)
AS
SET NOCOUNT ON

DELETE FROM tblMultiClaimantDocument
WHERE MultiClaimantDocumentID = @MultiClaimantDocumentID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDelMultiClaimantDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDelMultiClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDelMultiClaimantDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDelMultiClaimantDocument] TO [power_user]
GO
