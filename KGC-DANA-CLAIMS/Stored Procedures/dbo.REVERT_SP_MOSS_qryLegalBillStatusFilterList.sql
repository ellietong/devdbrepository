SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryLegalBillStatusFilterList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT LegalBillStatusID, CASE WHEN LegalBillStatus = 'Finalized' THEN 'Finalized / Paid' ELSE LegalBillStatus END As LegalBillStatus
  FROM m_tblLegalBillStatus
  UNION
  SELECT NULL, '<< All Statuses >>'
  ORDER BY LegalBillStatus
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryLegalBillStatusFilterList] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryLegalBillStatusFilterList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryLegalBillStatusFilterList] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryLegalBillStatusFilterList] TO [power_user]
GO
