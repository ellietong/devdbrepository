
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptLondonExpense] (@SourceDateStart date,  @SourceDateEnd date)
AS
/*
	Author: GraceAnn Pike
	Date:02/24/21015
	Purpose: Refactored qryrptLondonExpense.
	Example:
	execute  [dbo].[qryrptLondonExpense] 
	@SourceDateStart ='1/1/2015'
	,@SourceDateEnd = '1/31/2015'

*/
begin
Set Nocount on
BEGIN TRY

	--Variables for testing
	--Declare @SourceDateStart DATE = '1/1/2015'	
	--Declare @SourceDateEnd DATE = '1/31/2015'
	
	create TABLE #OToole (Period varchar(50), SourceDate date)
	create TABLE #DefenseInvoiceVoid  (Defenseinvoiceid int)
	create TABLE #LegalBillVoid  (InvoiceID int)
	create TABLE #LondonExpense (DefenseCounsel Varchar(100), Period varchar(50), PeriodMonth int, PeriodYear int, SourceDate date, Amount money)


	Insert into #OToole
	Select Period, SourceDate from tblDefenseInvoice di
	join tblpaymentdefense pd on pd.DefenseInvoiceID=di.DefenseInvoiceID
	join tblcheck ch on ch.CheckID=pd.CheckID
	Where DefenseCounselAddressID=69 and SourceDate>=@SourceDateStart and sourcedate<=@SourceDateEnd
	

	Insert into #DefenseInvoiceVoid
	SElect DefenseInvoiceID from tblpaymentdefense pd
	join tblCheck ch on ch.checkid=pd.checkid
	where FundingSourceID=3 and SourceDate<=@SourceDateStart
	

	Insert into #LegalBillVoid
	SElect plb.InvoiceID from tblPaymentLegalBill plb
	join tblCheck ch on ch.checkid=plb.checkid
	where FundingSourceID=3 and SourceDate<=@SourceDateStart
	
	
	INsert into #LondonExpense	
	select DefenseCounsel, Cast(UPPER(Concat(CONVERT(VARCHAR(3), DATENAME(MM, Period)),' ',Year(Period))) as varchar(50)), Month(period), Year(Period), SourceDate, case when AppealFlag=1 then Sum(AppealTotal) else sum(AdjustedTotal) end as Amount
	from tblPaymentLegalBill plb
	inner join tblCheck c on c.CheckID=plb.CheckID
	inner join m_tblLegalBillInvoices lbi on lbi.InvoiceID=plb.InvoiceID
	inner join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID=lbi.DefenseCounselAddressID
	left join #LegalBillVoid LBV on LBV.InvoiceID=lbi.InvoiceID
	where SourceDate>=@SourceDateStart and sourcedate<=@SourceDateEnd and lbi.MatterID=4 and lbv.InvoiceID is null and SourceAmount<>0
	group by DefenseCounsel,  Concat(CONVERT(VARCHAR(3), DATENAME(MM, Period)),' ',Year(Period)), SourceDate, AppealFlag, MOnth(Period), Year(Period)
	
	Union
	
	select Case when Coalesce(ExpertFirm, DefenseCounsel)='MAS, LLC' then 'Shumaker Loop & Kendrick LLP' else Coalesce(ExpertFirm, DefenseCounsel) end as Firm, Period, Month(period), Year(Period), SourceDate,  Sum(InvoiceTotal)
	from tblPaymentDefense pd
	inner join tblCheck c on c.CheckID=pd.CheckID
	inner join tblDefenseInvoice di on di.DefenseInvoiceID=pd.DefenseInvoiceID
	left Join m_tblExpertFirm ef on ef.ExpertFirmID = di.ExpertFirmID
	left join (Select * from tblDefenseCounselAddress dca where DefenseCounselAddressID not in (69, 105)) dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
	left join #DefenseInvoiceVoid div on div.DefenseInvoiceID=di.DefenseInvoiceID
	where SourceDate>=@SourceDateStart and sourcedate<=@SourceDateEnd and di.MatterID=4 and Coalesce(Expertfirm, Defensecounsel) is not NULL and div.Defenseinvoiceid is null and SourceAmount<>0
	group by Period, SourceDate, Coalesce(ExpertFirm, DefenseCounsel), Month(period), Year(Period)
	
	Union 
	
	---KCIC Adjustment
	select Coalesce(ExpertFirm, DefenseCounsel) as Firm, Period,  Month(period), Year(Period), SourceDate,  31091.58
	from tblPaymentDefense pd
	inner join tblCheck c on c.CheckID=pd.CheckID
	inner join tblDefenseInvoice di on di.DefenseInvoiceID=pd.DefenseInvoiceID
	left Join m_tblExpertFirm ef on ef.ExpertFirmID = di.ExpertFirmID
	left join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
	Where DefenseCounselID=62 and SourceDate>=@SourceDateStart and sourcedate<=@SourceDateEnd and PaymentAmount=45000
	group by Period, SourceDate, Coalesce(ExpertFirm, DefenseCounsel), Month(period), Year(Period)
	
	UNION
	
	---O'Toole Adjustment
	
	select Coalesce(ExpertFirm, DefenseCounsel) as Firm, di.Period,  Month(di.period), Year(di.Period), SourceDate, sum(invoicetotal) 
	from tblDefenseInvoice di 
	left Join m_tblExpertFirm ef on ef.ExpertFirmID = di.ExpertFirmID
	left join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
	join #OToole O on o.Period=di.Period
	where StatusID = 5
	and di.MatterID <> 5 and di.DefenseCounselAddressID=69
	group by di.Period, expertfirm, DefenseCounsel, SourceDate, Month(di.period), Year(di.Period)
	
	Union
	
	----Adjustment
	select DefenseCounsel, Cast(UPPER(Concat(CONVERT(VARCHAR(3), DATENAME(MM, Period)),' ',Year(Period))) as varchar(50)), Month(Period), Year(Period), SourceDate, case when AppealFlag=1 then AppealTotal*-1 else AdjustedTotal*-1 end as Amount
	from tblPaymentLegalBill plb
	inner join tblCheck c on c.CheckID=plb.CheckID
	inner join m_tblLegalBillInvoices lbi on lbi.InvoiceID=plb.InvoiceID
	inner join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID=lbi.DefenseCounselAddressID
	left join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID=lbi.ClaimantPersonalInfoID
	left join tblClaim claim on claim.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	left join tblClaimLawsuit cl on cl.ClaimID=claim.ClaimID
	left join tblAllegation al on al.ClaimLawsuitID=cl.ClaimLawsuitID
	left join tblAllegationType at on at.AllegationTypeID=al.AllegationTypeID
	where SourceDate>=@SourceDateStart and sourcedate<=@SourceDateEnd and (AllegationType='Premises' or AllegationType='Premises and Product')
	group by DefenseCounsel,  Concat(CONVERT(VARCHAR(3), DATENAME(MM, Period)),' ',Year(Period)), SourceDate, AppealFlag, MOnth(Period), Year(Period), AppealTotal, AdjustedTotal
	
	Union
	
	select Coalesce(ExpertFirm, DefenseCounsel) as Firm, Period, Month(period), Year(Period), SourceDate,  InvoiceTotal*-1
	from tblPaymentDefense pd
	inner join tblCheck c on c.CheckID=pd.CheckID
	inner join tblDefenseInvoice di on di.DefenseInvoiceID=pd.DefenseInvoiceID
	left Join m_tblExpertFirm ef on ef.ExpertFirmID = di.ExpertFirmID
	left join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
	left join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID=di.ClaimantPersonalInfoID
	left join tblClaim claim on claim.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	left join tblClaimLawsuit cl on cl.ClaimID=claim.ClaimID
	left join tblAllegation al on al.ClaimLawsuitID=cl.ClaimLawsuitID
	left join tblAllegationType at on at.AllegationTypeID=al.AllegationTypeID
	where SourceDate>=@SourceDateStart and sourcedate<=@SourceDateEnd and (AllegationType='Premises' or AllegationType='Premises and Product')
	group by Period, SourceDate, Coalesce(ExpertFirm, DefenseCounsel), Month(period), Year(Period), InvoiceTotal	
	ORder By DefenseCounsel, SourceDate, MOnth(Period), Year(Period)
	
	
	Select DefenseCounsel, Period, SourceDate, PeriodMonth, PeriodYear,  Sum(Amount) as Amount, Year(GetDate()) as 'Year',Month(DateADD(d,-1,Dateadd(mm, DateDiff(m,0,GetDate()),0))) as 'Month' 
	FROM #LondonExpense
	Group by DefenseCounsel, Period, SourceDate, PeriodMonth, PeriodYear
	Order by Defensecounsel, Sourcedate, PeriodYear, PeriodMonth





	Return
END TRY
BEGIN CATCH
		DECLARE @ErrorNumber INT = ERROR_NUMBER();
		DECLARE @ErrorLine INT = ERROR_LINE();
		DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
		DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
		DECLARE @ErrorState INT = ERROR_STATE();

		PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
		PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

END CATCH
end
GO
