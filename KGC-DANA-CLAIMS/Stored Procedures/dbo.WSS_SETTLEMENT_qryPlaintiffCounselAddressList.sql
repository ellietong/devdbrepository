SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList]
AS
SET NOCOUNT ON

SELECT PlaintiffCounselAddressID, 
	PlaintiffCounsel + ' (' + CASE WHEN PlaintiffCounselCity IS NULL THEN 'UNKNOWN' ELSE PlaintiffCounselCity END + ')' AS PlaintiffCounsel 
FROM tblPlaintiffCounselAddress pca 
WHERE PlaintiffCounsel IS NOT NULL 
	AND PlaintiffCounsel <> ''
	AND EXISTS (SELECT PlaintiffCounselAddressID FROM vtblPerson p WHERE pca.PlaintiffCounselAddressID=p.PlaintiffCounselAddressID)
UNION
SELECT NULL, NULL
ORDER BY PlaintiffCounsel + ' (' + CASE WHEN PlaintiffCounselCity IS Null THEN 'UNKNOWN' ELSE PlaintiffCounselCity END + ')', PlaintiffCounselAddressID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffCounselAddressList] TO [power_user]
GO
