SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_Reports_qryTotalActivePending] 
	-- Add the parameters for the stored procedure here
(@MatterID int=null, @MonthID int, @YearID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

--declare @PendingDate date
--set @PendingDate ='9/30/2011'

--Declare @Month int
--Declare @Year int
--set @Month=9
--set @Year=2011

Declare @Month int
Declare @Year int
Declare @PendingDate date
set @Month=@MonthID
set @Year=@YearID
set @PendingDate= DATEADD(MONTH,1,convert(date,convert(varchar,@Month)+'/1/'+convert(varchar,@Year)))


--Find all claims with a status change post pending date(2a,2b)
declare @FindClaimantsWithPostPendingDateStatusChange table (claimant int, claimlawsuitID int)
insert into @FindClaimantsWithPostPendingDateStatusChange
select c.ClaimantPersonalInfoID, cl.ClaimLawsuitID
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
where	StatusDate>=@PendingDate
		and ClaimLawsuitStatusID not in (22) 
		and IsPrimaryForClaim = 1 
		and  IsPrimaryStatus=1
		and IsCurrentClaim=1
		and MatterID >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(@matterid, @maxMatterID)
group by c.ClaimantPersonalInfoID, cl.ClaimLawsuitID

--For all claims with a status change post pending AND a stausdate before pending date: find the max status date before pending date (2a)
declare @FindMaxStatusDateBeforePendingDate table(claimlawsuitID2 int, maxdate datetime)
insert into @FindMaxStatusDateBeforePendingDate
select cl.ClaimLawsuitID,  max(StatusDate)
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join @FindClaimantsWithPostPendingDateStatusChange ft on ft.claimlawsuitID=cl.ClaimLawsuitID
where StatusDate <@PendingDate
group by cl.ClaimLawsuitID

--For claims with a status change post pending AND a statusdate before pending date: find only those claims with a max open status before pending date (2a)
declare @CheckForOnlyOpenStatusBeforePendingDate table (claimlawsuitID3 int)
insert into @CheckForOnlyOpenStatusBeforePendingDate
select cl.ClaimLawsuitID
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
inner join @FindMaxStatusDateBeforePendingDate fm on fm.claimlawsuitID2=Clst.ClaimLawsuitID and clst.StatusDate=fm.maxdate
where clst.ClaimLawsuitStatusID=22

--Check to see the status count of all claims with a status change post pending date (2b)
declare @CheckForStatuscount table(claimlawsuitID4 int, statuscount int)
insert into @CheckForStatuscount
select cl.ClaimLawsuitID, count(StatusDate)
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join @FindClaimantsWithPostPendingDateStatusChange fcw on fcw.claimlawsuitID=cl.ClaimLawsuitID
group by cl.ClaimLawsuitID

--Find all claims that only have one status date and check to see if file/service/received date was before pending date (2b)
declare @CheckIfFileDateBeforePendingDateForOnlyOneStatus table (claimlawsuitID5 int)
insert into @CheckIfFileDateBeforePendingDateForOnlyOneStatus
select cl.ClaimLawsuitID
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
inner join @CheckForStatuscount cfs on cfs.claimlawsuitID4=Clst.ClaimLawsuitID
where cfs.statuscount=1 and coalesce(ServiceDate,FileDate,ReceivedDate) <@PendingDate
group by cl.ClaimLawsuitID

--Union table to combine 3 main components of Pending Count
declare @CombinedTable table (HighLevelDisease nvarchar(100), State nvarchar (100), DiseaseCount int)
insert into @CombinedTable

--Counts All Claims that have their status date before the pending date and are CURRENTLY still open (1)
select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, FullNamestate,
COUNT(distinct(cl.ClaimLawsuitID))
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimDisease cd on c.ClaimID=cd.ClaimID
left join tblDisease d on d.DiseaseID=cd.DiseaseID
left join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
left join tblHighLevelDisease hld on hld.HighLevelDiseaseID=d.HighLevelDiseaseID or hld.HighLevelDiseaseID=da.HighLevelDiseaseID
left join tblState s on s.StateID=l.StateID
where	(StatusDate<@PendingDate or StatusDate is null)
		and ClaimLawsuitStatusID =22 
		and IsPrimaryForClaim = 1 
		and  IsPrimaryStatus=1
		--and IsCurrentClaim=1
		and MatterID >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(@matterid, @maxMatterID)
		and cd.PrimaryDisease=1
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, hld.HighLevelDiseaseID, FullNameState

union all

--Counts all Claims with a status change post pending date and an open status as of pending date (2a)
Select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, FullNamestate,
COUNT(distinct(cl.ClaimLawsuitID))
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join @CheckForOnlyOpenStatusBeforePendingDate cf on cf.claimlawsuitID3=cl.ClaimLawsuitID
inner join tblClaimDisease cd on c.ClaimID=cd.ClaimID
left join tblDisease d on d.DiseaseID=cd.DiseaseID
left join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
left join tblHighLevelDisease hld on hld.HighLevelDiseaseID=d.HighLevelDiseaseID or hld.HighLevelDiseaseID=da.HighLevelDiseaseID
left join tblState s on s.StateID=l.StateID
where cd.PrimaryDisease=1
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, hld.HighLevelDiseaseID, FullNameState

union all

--Counts all claims with a status change post pending date and only has one status and has a file/service/received date before pending date (2b)
Select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, FullNamestate,
COUNT(distinct(cl.ClaimLawsuitID))
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join @CheckIfFileDateBeforePendingDateForOnlyOneStatus cifd on cifd.claimlawsuitID5=cl.ClaimLawsuitID
inner join tblClaimDisease cd on c.ClaimID=cd.ClaimID
left join tblDisease d on d.DiseaseID=cd.DiseaseID
left join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
left join tblHighLevelDisease hld on hld.HighLevelDiseaseID=d.HighLevelDiseaseID or hld.HighLevelDiseaseID=da.HighLevelDiseaseID
left join tblState s on s.StateID=l.StateID
where cd.PrimaryDisease=1
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, hld.HighLevelDiseaseID, FullNameState


--Groups all results into one table
select SUM(ct.DiseaseCount)
from @CombinedTable ct



END


GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalActivePending] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalActivePending] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalActivePending] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalActivePending] TO [power_user]
GO
