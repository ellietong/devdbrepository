SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata] (@DocumentID int)
AS

--DECLARE @DocumentID int
--SET @DocumentID = 154

SELECT DetailDocumentTypeID,
	d.ClaimantPersonalInfoID,
	Comments,
	DocumentURL,
	DocumentTitle,
	SPDocId,
	CreatedBy,
	d.DateCreated,
	d.InvoiceNumber,
	d.Period,
	d.DatePayable,
	ModifiedBy,
	DateModified,
	d.FeeAmount,
	d.DisbursementAmount,
    d.Adjustment,
    d.AdjustmentNotes,
    d.StatusID,
    d.FirmID,
    d.MatterID, 
    PrivateComments,
    DocumentSourceID,
    d.ExpertFirmID,
    cdt.DocumentGroupID,
    IsFullyPaid,
    d.DocumentID,
    DismissalDate,
    QCDate AS DismissalProcessDate,
    d.ClaimLawsuitStatusID,
    NULL as Temphash,
    Accepted AS AutoAccept,
    DetailDocumentTypeID AS ClaimantDocumentTypeID,
    AmazonKey
FROM tblDocuments d
	INNER JOIN tblClaimantDocumentType cdt	ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
	LEFT JOIN  tblDefenseInvoice di			ON d.DocumentID = di.DocumentID 
	LEFT JOIN  tblPaymentDefense pd			ON di.DefenseInvoiceID=pd.DefenseInvoiceID
	LEFT JOIN  m_tblExpertFirm ef			ON d.ExpertFirmID = ef.ExpertFirmID 
	LEFT JOIN  tblDefenseInvoiceStatus dis	ON d.StatusID = dis.StatusID 
	LEFT JOIN  vtblFirm f					ON d.FirmID = f.FirmID
	LEFT JOIN  tblClaimantDocketNumbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
WHERE d.DocumentID = @DocumentID 
	AND COALESCE(IsFullyPaid, 0) <> 1

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_PROFILE_qryGetDocMetadata] TO [power_user]
GO
