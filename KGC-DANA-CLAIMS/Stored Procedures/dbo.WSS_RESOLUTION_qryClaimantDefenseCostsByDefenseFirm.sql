SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] (@ClaimantPersonalInfoID int) 
AS
SET NOCOUNT ON
	
--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 9494

DECLARE @InvoiceTotal TABLE (ClaimantPersonalInfoID int, ExpertFirmID int, DefenseCounselAddressID int, DefenseTotal money, MatterID int)
INSERT INTO @InvoiceTotal
	--Invoices linked on NavLegacyIDs - Pre-KCIC transfer
	SELECT cpi.ClaimantPersonalInfoID, 
		NULL AS ExpertFirmID, 
		a.DefenseCounselAddressID AS DefenseCounselID, 
		CASE WHEN AppealFlag = 1 THEN Sum(a.AppealTotal) ELSE SUM(a.AdjustedTotal) END AS DefenseTotal,
		MatterID 
	FROM tblClaimantPersonalInfo cpi
		INNER JOIN m_tblLegalBillInvoices a ON a.Navlegacyid=cpi.NavLegacyID
	WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and cpi.ClaimantPersonalInfoID is null
	GROUP BY cpi.ClaimantPersonalInfoID, MatterID, a.DefenseCounselAddressID, AppealFlag
	UNION
	--Invoices linked on KCIC IDs
	SELECT cpi.ClaimantPersonalInfoID, 
		NULL AS ExpertFirmID, 
		a.DefenseCounselAddressID AS DefenseCounselID, 
		CASE WHEN AppealFlag = 1 THEN Sum(a.AppealTotal) ELSE SUM(a.AdjustedTotal) END AS DefenseTotal,
		MatterID 
	FROM tblClaimantPersonalInfo cpi
		INNER JOIN m_tblLegalBillInvoices a ON a.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	GROUP BY cpi.ClaimantPersonalInfoID, MatterID, a.DefenseCounselAddressID, AppealFlag
	UNION
	--Approved Expert and General Invoices with a ClaimantID link
	SELECT CPI.ClaimantPersonalInfoID, 
		di.ExpertFirmID, 
		Di.DefenseCounselAddressID, 
		SUM(DI.InvoiceTotal) as DefenseTotal,
		MatterID
	FROM tblDefenseInvoice DI
		INNER JOIN tblClaimantPersonalInfo CPI ON CPI.ClaimantPersonalInfoID=DI.ClaimantPersonalInfoID
	WHERE DI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and DI.StatusID in (1,2,5)
	GROUP BY CPI.ClaimantPersonalInfoID, MatterID, di.ExpertFirmID, Di.DefenseCounselAddressID

SELECT ClaimantPersonalInfoID, 
	Matter,
	i.MatterID,
	COALESCE(DefenseCounsel,ExpertFirm) AS DefenseCounsel, 
	COALESCE(i.DefenseCounselAddressID, 0) AS DefenseCounselID, 
	i.ExpertFirmID, 
	SUM(DefenseTotal) AS DefenseTotal 
FROM @InvoiceTotal i 
	LEFT JOIN tblDefenseCounselAddress d ON i.DefenseCounselAddressID = d.DefenseCounselAddressID
	LEFT JOIN m_tblExpertFirm e			 ON i.ExpertFirmID = e.ExpertFirmID
	LEFT JOIN tblMatter m ON i.MatterID = m.MatterID
GROUP BY ClaimantPersonalInfoID, Matter, i.MatterID, DefenseCounsel, i.DefenseCounselAddressID, ExpertFirm, i.ExpertFirmID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GO
