SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryfrmAllocationExhaustionTypeID]
(@allocationID int)

AS

SET NOCOUNT ON

SELECT et.ExhaustionTypeID, ExhaustionType
FROM tblAllocationExhaustionType ae INNER JOIN tblExhaustionType et ON ae.ExhaustionTypeID = et.ExhaustionTypeID
WHERE AllocationID = @allocationID
ORDER BY ExhaustionType

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationExhaustionTypeID] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationExhaustionTypeID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationExhaustionTypeID] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationExhaustionTypeID] TO [power_user]
GO
