SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryClaimantSearch_Paged_old] 
(@ClaimantName nvarchar(max), @SSN nvarchar(20), @StateID int, @PlaintiffCounselAddressID int, @ClaimantID int, @NavInjPartyKey nvarchar(100), @ClaimID bigint, @startRow int, @pageSize int)
AS
SET NOCOUNT ON
--DECLARE @ClaimantName nvarchar(max)
--SET @ClaimantName = 'smith'
--DECLARE @Birthdate nvarchar(20)
--SET @Birthdate = null
--DECLARE @SSN nvarchar(20)
--SET @SSN = null
--DECLARE @StateID int
--SET @StateID = null
--DECLARE @PlaintiffCounselAddressID int
--SET @PlaintiffCounselAddressID = null
--DECLARE @ClaimantID bigint
--SET @ClaimantID = null
--DECLARE @ClaimID bigint
--SET @ClaimID = null
--DECLARE @navInjpartyKey nvarchar(100)
--SET @navInjpartyKey = null
--DECLARE @startRow int
--SET @startRow = 0
--DECLARE @pageSize int
--SET @pageSize = 50

DECLARE @MaxState int
SET @MaxState = (SELECT MAX(StateID) FROM tblState)
DECLARE @MaxPC int
SET @MaxPC = (SELECT MAX(PlaintiffCounselAddressID) FROM tblPlaintiffCounselAddress)
DECLARE @MaxClaimantID bigint
SET @MaxClaimantID = (SELECT MAX(ClaimantPersonalInfoID) FROM tblClaimantPersonalInfo)
DECLARE @MaxClaimID bigint
SET @MaxClaimID = (SELECT MAX(ClaimID) FROM tblClaim)





SELECT * FROM (
SELECT TOP (SELECT @pageSize)
                      CPI.ClaimantPersonalInfoID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN ' ' ELSE MiddleName + ' ' END + '- (' + COALESCE(DocketNumbers, 'N/A') + ')' AS ClaimantName, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE SocialSecurityNumber END AS SSNSearch,
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
                      COALESCE(CONVERT(VARCHAR(10), CPI.BirthDate, 101), '') AS BirthDate,
                      ROW_NUMBER() OVER (ORDER BY cpi.LastName, cpi.Firstname) AS ResultSetRowNumber
                        
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID
                      LEFT JOIN tblClaimantDocketNumbers cdn ON CPI.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
                      LEFT JOIN tblClaimLawsuit CL ON c.ClaimID = cl.ClaimID
                      LEFT JOIN tblLawsuit L ON cl.LawsuitID = L.LawsuitID
WHERE  (
		(CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
       THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' end) like '%'+coalesce(@ClaimantName, '')+'%'
       AND Coalesce(socialsecurityNumber, '') like '%'+coalesce(@SSN, '')+'%'
		AND l.StateID BETWEEN COALESCE(@StateID, 0) AND COALESCE(@StateID, @MaxState)
		AND  PlaintiffCounselAddressID >= COALESCE(@PlaintiffCounselAddressID, 0) AND PlaintiffCounselAddressID <= COALESCE(@PlaintiffCounselAddressID, @MaxPC)
		AND cpi.ClaimantPersonalInfoID >= COALESCE(@ClaimantID, 0) AND cpi.ClaimantPersonalInfoID <= COALESCE(@ClaimantID, @MaxClaimantID))
		AND c.ClaimID >= COALESCE(@ClaimID, 0) AND c.ClaimID <= COALESCE(@ClaimID,@maxclaimID)
		AND cpi.NavInjPartyKey LIKE '%' + COALESCE(@NavInjPartyKey, '') + '%'
) AS PagedResults
WHERE ResultSetRowNumber > @startRow AND ResultSetRowNumber <= (@startRow+@pageSize)
ORDER BY ClaimantName
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantSearch_Paged_old] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantSearch_Paged_old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantSearch_Paged_old] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantSearch_Paged_old] TO [power_user]
GO
