SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_REPORTS_qryTopTenDismissalsByStateByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @YearID int, @MonthID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @YearID int
--Set @YearId=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @year int
SET @year = @YearID

DECLARE @month int
set @month = @MonthID

    -- Insert statements for procedure here
select Top 10 FullNameState, COUNT(distinct(cl.ClaimLawsuitID)) as Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s on l.StateID=s.StateID
where YEAR(statusdate)=@year 
		and MONTH(StatusDate) <=@month
		and ClaimLawsuitStatusID in (15,16,17,27,36,19) 
		and IsPrimaryForClaim = 1 
		and  IsPrimaryStatus=1
		and MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
group by FullNameState
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryTopTenDismissalsByStateByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryTopTenDismissalsByStateByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryTopTenDismissalsByStateByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryTopTenDismissalsByStateByYear] TO [power_user]
GO
