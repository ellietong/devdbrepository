SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] (@ClaimantPersonalInfoID int, 
																		 @DefenseCounselID		 nvarchar(250), 
																		 @ExpertFirmID			 int,
																		 @MatterID int) 
AS
SET NOCOUNT ON
	
--DECLARE @ClaimantPersonalInfoID int
--DECLARE @DefenseCounselID int
--DECLARE @ExpertFirmID int
--SET @ClaimantPersonalInfoID = 68789
--SET @DefenseCounselID	      = 63
--SET @ExpertFirmID           = NULL

DECLARE @UnpaidInvoice TABLE (InvoiceID int, IsFullyPaid bit)
INSERT INTO @UnpaidInvoice
	SELECT i.InvoiceID, 
		CASE WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 0 THEN 0 
			 WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 1 THEN 1
			 WHEN IsFullyPaid = 0 and NavPaidInvoice = 1 THEN 1
			 WHEN IsFullyPaid = 0 and NavPaidInvoice = 0 THEN 0
			 ELSE IsFullyPaid END AS IsFullyPaid 
	FROM m_tblLegalBillInvoices i
		LEFT JOIN tblPaymentLegalBill pd on i.InvoiceID = pd.InvoiceID
	GROUP BY i.InvoiceID, CASE WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 0 THEN 0 WHEN IsFullyPaid IS NULL AND NavPaidInvoice = 1 THEN 1 
		WHEN IsFullyPaid = 0 and NavPaidInvoice = 1 THEN 1 WHEN IsFullyPaid = 0 and NavPaidInvoice = 0 THEN 0 ELSE IsFullyPaid END

--Invoices linked on NavLegacyIDs - Pre-KCIC transfer
SELECT cpi.ClaimantPersonalInfoID, 
	a.DefenseCounselAddressID AS DefenseCounselID, 
	CASE WHEN a.OriginalInvoiceNumber IS Null THEN 'Unknown' else a.OriginalInvoiceNumber END AS InvoiceNumber, 
	CASE WHEN AppealFlag = 1 THEN Sum(a.AppealTotal) else SUM(a.AdjustedTotal) END AS DefenseTotal,
	CASE WHEN NavPaidInvoice = 1 THEN 'Paid by Navigant' WHEN NavPaidInvoice = 0 and u.IsFullyPaid = 1 THEN 'Paid' ELSE lbs.LegalBillStatus END AS 'Status', 
	SourceDate AS DatePaid
FROM tblClaimantPersonalInfo cpi
	INNER JOIN m_tblLegalBillInvoices a ON a.navlegacyid=cpi.NavLegacyID
	LEFT  JOIN @UnpaidInvoice u			ON a.InvoiceID = u.InvoiceID
	LEFT  JOIN m_tblLegalBillStatus lbs ON a.StatusID = lbs.LegalBillStatusID
	LEFT  JOIN tblPaymentLegalBill plb	ON a.InvoiceID = plb.InvoiceID
	LEFT  JOIN tblCheck c				ON plb.CheckID = c.CheckID
WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
	AND a.DefenseCounselAddressID = @DefenseCounselID 
	AND a.MatterID = @MatterID
	AND cpi.ClaimantPersonalInfoID IS NULL
GROUP BY cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID, a.OriginalInvoiceNumber, AppealFlag, 
	CASE WHEN NavPaidInvoice = 1 THEN 'Paid by Navigant' WHEN NavPaidInvoice = 0 and u.IsFullyPaid = 1 THEN 'Paid' ELSE lbs.LegalBillStatus END, SourceDate
UNION
--Invoices linked on KCIC IDs
SELECT cpi.ClaimantPersonalInfoID, 
	a.DefenseCounselAddressID AS DefenseCounselID, 
	CASE WHEN a.OriginalInvoiceNumber IS Null THEN 'Unknown' else a.OriginalInvoiceNumber END AS InvoiceNumber, 
	CASE WHEN AppealFlag = 1 THEN Sum(a.AppealTotal) else SUM(a.AdjustedTotal) END AS DefenseTotal,
	CASE WHEN NavPaidInvoice = 1 THEN 'Paid by Navigant' WHEN NavPaidInvoice = 0 and u.IsFullyPaid = 1 THEN 'Paid' ELSE lbs.LegalBillStatus END AS 'Status', 
	SourceDate AS DatePaid
FROM tblClaimantPersonalInfo cpi
	INNER JOIN m_tblLegalBillInvoices a ON a.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
	LEFT  JOIN @UnpaidInvoice u			ON a.InvoiceID = u.InvoiceID
	LEFT  JOIN m_tblLegalBillStatus lbs ON a.StatusID = lbs.LegalBillStatusID
	LEFT  JOIN tblPaymentLegalBill plb	ON a.InvoiceID = plb.InvoiceID
	LEFT  JOIN tblCheck c				ON plb.CheckID = c.CheckID
WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
	AND a.DefenseCounselAddressID = @DefenseCounselID
	AND a.MatterID = @MatterID
GROUP BY cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID, a.OriginalInvoiceNumber, AppealFlag, 
	CASE WHEN NavPaidInvoice = 1 THEN 'Paid by Navigant' WHEN NavPaidInvoice = 0 AND u.IsFullyPaid = 1 THEN 'Paid' ELSE lbs.LegalBillStatus END, SourceDate
UNION 
--Approved Expert and General Invoices with a ClaimantID link
SELECT CPI.ClaimantPersonalInfoID, 
	COALESCE(di.ExpertFirmID, 
	di.DefenseCounselAddressID) AS DefenseCounselID, 
	di.InvoiceNumber AS InvoiceNumber, 
	SUM(di.InvoiceTotal) AS DefenseTotal,
	CASE WHEN IsFullyPaid = 1 THEN 'Paid' ELSE [Status] END AS 'Status', 
	c.SourceDate AS DatePaid
FROM tblClaimantPersonalInfo cpi
	INNER JOIN tblDefenseInvoice di			ON CPI.ClaimantPersonalInfoID=DI.ClaimantPersonalInfoID
	INNER JOIN tblDefenseInvoiceStatus ds	ON di.StatusID=ds.StatusID
	LEFT  JOIN tblPaymentDefense pd			ON di.DefenseInvoiceID=pd.DefenseInvoiceID
	LEFT  JOIN tblCheck c					ON pd.CheckID = c.CheckID
WHERE cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
	AND DI.StatusID IN(1, 2, 5) 
	AND di.ExpertFirmID = @ExpertFirmID 
	AND di.DefenseCounselAddressID = 0
	AND di.MatterID = @MatterID
GROUP BY cpi.ClaimantPersonalInfoID, di.ExpertFirmID, di.DefenseCounselAddressID, di.InvoiceNumber, 
	CASE WHEN IsFullyPaid = 1 THEN 'Paid' ELSE [Status] END, SourceDate
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qryClaimantDefenseCostsByInvoice] TO [power_user]
GO
