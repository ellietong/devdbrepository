SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[REVERT_SP_MOSS_qryUpdSettlePaymentDocumentation] (@SetPymtID int, @SettlePaymentComments nvarchar(max), @ReleaseTypeID int,
 @ExecutedReleaseDate Date, @MedicareReported bit, @ReleaseToPC datetime,@ReleaseRqstDate Date)
as

--DECLARE @ClaimID int
--SET @ClaimID = 1

update tblSettlePaymentDocumentation
set SettlePaymentComments = @SettlePaymentComments, 
ReleaseTypeID = CASE WHEN @ReleaseTypeID = 0 THEN NULL ELSE @ReleaseTypeID END, 
ExecutedReleaseDate = nullif(@ExecutedReleaseDate,''),
ReleaseRqstDate = nullif(@ReleaseRqstDate,''), 
	MedicareReported = @MedicareReported,
	ReleaseToPC = nullif(@ReleaseToPC, '')
where SetPymtID = @SetPymtID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdSettlePaymentDocumentation] TO [power_user]
GO
