SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Proc [dbo].[qryfrmExportedBankStatement] 
as

SET NOCOUNT ON

SELECT c.CheckID, 
	SourceNumber, 
	SourceDate, 
	SourceAmount, 
	IsCheckCleared,
    CheckDate, 
    CheckAmount * -1 AS CheckAmount, 
    CheckNumber, 
    CheckNote
FROM tblCheck c
    INNER JOIN tblCheckApproval ca     ON c.CheckID = ca.CheckID
    INNER JOIN ztblBankStatementInfo bs ON c.SourceNumber = bs.CheckNumber
WHERE  IsCheckCleared IS NULL
    OR IsCheckCleared = 0
ORDER BY SourceNumber
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExportedBankStatement] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmExportedBankStatement] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmExportedBankStatement] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmExportedBankStatement] TO [power_user]
GO
