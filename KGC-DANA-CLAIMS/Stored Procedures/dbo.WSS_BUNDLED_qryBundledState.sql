SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_BUNDLED_qryBundledState]
AS
	
SET NOCOUNT ON

SELECT DISTINCT StateFiled AS [State]
FROM m_tblBundledLawsuit
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_BUNDLED_qryBundledState] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_BUNDLED_qryBundledState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_BUNDLED_qryBundledState] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_BUNDLED_qryBundledState] TO [power_user]
GO
