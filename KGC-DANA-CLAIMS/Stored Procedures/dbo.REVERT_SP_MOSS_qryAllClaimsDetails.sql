SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryAllClaimsDetails]
@ClaimID bigint=null, @emailaddress nvarchar(250)
AS
BEGIN

--DECLARE @ClaimID int
--SET @ClaimID = 9494
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'snyderd@kcicllc.com'

Declare @UserFirmState table (DefenseCounselID bigint, StateID int)
Insert into @UserFirmState

Select dca.DefenseCounselID, StateID
From dbo.tblDefenseCounselState AS dcs 
left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid --and dcar.stateID = dcs.StateID 
LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
Where (ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) OR
dca.DefenseCounselID in (SELECT f.FirmID FROM vtblPerson p inner join vtblFirm f on p.FirmID=f.FirmID WHERE Email = @emailaddress))

IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress) IN (132, 133,114) OR ((Select PersonID FROM vtblPerson WHERE Email = @emailaddress) IN (1376,1377,1378,1383,1384,1389,1392,1404)) OR (SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1
BEGIN
SELECT  DISTINCT   l.stateid, l.DefenseCounselAddressID, c.ClaimID, hddis.HighLevelDisease AS Disease, S.State, J.Jurisdiction, cs.ClaimSequence, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, L.DocketNumber, L.CaseCaption, 
                      L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  pca.PlaintiffCounsel AS PrimaryLocalPlaintiffCounsel, 
                      r_1.ResponsibleDefenseCOunsel AS PrimaryDefenseCounsel, CPI.ClaimantPersonalInfoID, ReleaseSignedDate, rt.ReleaseType, 
                      CLst.StatusNotes, ExecutedReleaseDate as ReleaseReceivedDate, SettlementDueDate, PaidDate, CheckNumber, JOBSITE, ClaimDiseaseID, cl.LawsuitID, ActiveTrialDate,
                      StatusDate, SettlementDate, COALESCE(cd.DiseaseID, disa.diseaseID_alt) AS DiseaseID,
                       doc.DoctorLastName + ', ' + COALESCE(doc.DoctorFirstName, '') + COALESCE(' ' + doc.DoctorMiddleName, '') AS Doctor, SettlementAmount, SettlementGroupID,
                       FullyDocumented, CauseOfInjuryID,CASE WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + ', ' + COALESCE(ilodoc.DoctorFirstName, '') + COALESCE(' ' + ilodoc.DoctorMiddleName, '') END AS ILODoctor,
                       CASE WHEN PFTDoctorID IS NULL THEN NULL ELSE pftdoc.DoctorLastName + ', ' + COALESCE(pftdoc.DoctorFirstName, '') + COALESCE(' ' + pftdoc.DoctorMiddleName, '')END AS PFTDoctor, ILODoctorID, ILOProfusion,
                       ILODate, PFTDoctorID, PFTDate, PFTTypeID, PFTFVC_Actual, PFTFVC_Predicted, 
                       CASE WHEN PFTFVC_Predicted <> 0 THEN (PFTFVC_Actual/PFTFVC_Predicted) ELSE 0 END AS PFTFVC_Pct,
                       
                         PFTFEV1_Actual, PFTFEV1_Predicted, CASE WHEN PFTFEV1_Predicted <> 0 THEN (PFTFEV1_Actual/PFTFEV1_Predicted) ELSE 0 END AS PFTFEV1_Pct,
                        [PFTFEV1andFVC_Actual] AS PFTFEV1FVC_Actual, [PFTFEV1andFVC_Predicted] AS PFTFEV1FVC_Predicted,
                         CASE WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN ([PFTFEV1andFVC_Actual]/[PFTFEV1andFVC_Predicted]) ELSE 0 END AS PFTFEV1FVC_Pct,
                       
                       PFTTLC_Actual, PFTTLC_Predicted, CASE WHEN PFTTLC_Predicted <> 0 THEN (PFTTLC_Actual/PFTTLC_Predicted) ELSE 0 END AS PFTTLC_Pct, 
                        PFTDLCO_Actual, PFTDLCO_Predicted, CASE WHEN PFTDLCO_Predicted <> 0 THEN (PFTDLCO_Actual/PFTDLCO_Predicted) ELSE 0 END AS PFTDLCO_Pct, 
                        PFTTV, IsThirdPartyComplaint, IsMDL, AllegationType + ' - ' + Material AS Allegation, CASE WHEN IsPrimaryForClaim = 1 THEN 'YES' ELSE 'NO' END AS PrimaryLawsuit, DiseaseDiagnosisDate
                        
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID INNER JOIN
					  dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                          (SELECT     dcs.DefenseCounseladdressID, dca.DefenseCounsel, dcs.ResponsibleDefenseCounseladdressID, dcs.StateID, 
                                                   r.DefenseCounsel AS ResponsibleDefenseCOunsel, dcs.IsActiveCounsel
                            FROM          dbo.tblDefenseCounselState AS dcs LEFT JOIN
											dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
											LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
											LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID LEFT JOIN
                                                   dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
                            GROUP BY dca.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounseladdressID, dcs.ResponsibleDefenseCounseladdressID, dcs.IsActiveCounsel, dcs.StateID) 
                      AS r_1 ON r_1.DefenseCounseladdressID = L.DefenseCounselAddressID AND r_1.StateID = L.StateID
                      left join (SELECT * from tblClaimDisease where PrimaryDisease = 1) as CD on CD.ClaimID = c.ClaimID
					left join tblDisease ddis on ddis.DiseaseID = cd.DiseaseID
					left join tblDisease_ALT disa ON disa.DiseaseID_Alt = cd.DiseaseID
					left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
					 LEFT JOIN (SELECT * FROM tblClaimantEmployment WHERE IsPrimary = 1) CE ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID   
                      Left Join (Select * From dbo.tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                      LEFT JOIN dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
					  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
                      left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
                      LEFT JOIN tblDoctor doc ON cd.DoctorID = doc.DoctorID 
                      LEFT JOIN tblDoctor ILODoc ON ILODoctorID=ILODoc.DoctorID
                      LEFT JOIN tblDoctor PFTDoc ON PFTDoctorID=PFTDoc.DoctorID
                      LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
                      INNER JOIN tblAllegationType at ON a.AllegationTypeID = at.AllegationTypeID
                      INNER JOIN tblMaterial mat ON a.MaterialID = mat.MaterialID
                      
            
WHERE C.ClaimID = @ClaimID --AND cl.IsPrimaryForClaim = 1
ORDER BY PrimaryLawsuit DESC, State

END

ELSE
BEGIN

SELECT  DISTINCT   l.stateid, l.DefenseCounselAddressID, c.ClaimID, hddis.HighLevelDisease AS Disease, S.State, J.Jurisdiction, cs.ClaimSequence, 
                      CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
                      THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL 
                      THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(date, CPI.BirthDate) AS BirthDate, CPI.DeceasedDate, 
                      CASE WHEN Month(BirthDate) < Month(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) WHEN Month(BirthDate) > Month(DeceasedDate) 
                      THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 WHEN Month(BirthDate) = Month(DeceasedDate) THEN CASE WHEN Day(BirthDate) 
                      > Day(DeceasedDate) THEN DateDiff(yy, BirthDate, DeceasedDate) - 1 ELSE DateDiff(yy, BirthDate, DeceasedDate) END END AS DeathAge, 
                      cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, L.DocketNumber, L.CaseCaption, 
                      L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  pca.PlaintiffCounsel AS PrimaryLocalPlaintiffCounsel, 
                      r_1.ResponsibleDefenseCOunsel AS PrimaryDefenseCounsel, CPI.ClaimantPersonalInfoID, ReleaseSignedDate, rt.ReleaseType, 
                      CLst.StatusNotes, ExecutedReleaseDate as ReleaseReceivedDate, SettlementDueDate, PaidDate, CheckNumber, JOBSITE, ClaimDiseaseID, cl.LawsuitID, ActiveTrialDate,
                      StatusDate, SettlementDate, COALESCE(cd.DiseaseID, disa.diseaseID_alt) AS DiseaseID,
                       doc.DoctorLastName + ', ' + COALESCE(doc.DoctorFirstName, '') + COALESCE(' ' + doc.DoctorMiddleName, '') AS Doctor, SettlementAmount, SettlementGroupID,
                       FullyDocumented, CauseOfInjuryID,CASE WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + ', ' + COALESCE(ilodoc.DoctorFirstName, '') + COALESCE(' ' + ilodoc.DoctorMiddleName, '') END AS ILODoctor,
                       CASE WHEN PFTDoctorID IS NULL THEN NULL ELSE pftdoc.DoctorLastName + ', ' + COALESCE(pftdoc.DoctorFirstName, '') + COALESCE(' ' + pftdoc.DoctorMiddleName, '')END AS PFTDoctor, ILODoctorID, ILOProfusion,
                       ILODate, PFTDoctorID, PFTDate, PFTTypeID, PFTFVC_Actual, PFTFVC_Predicted, 
                       CASE WHEN PFTFVC_Predicted <> 0 THEN (PFTFVC_Actual/PFTFVC_Predicted) ELSE 0 END AS PFTFVC_Pct,
                       
                         PFTFEV1_Actual, PFTFEV1_Predicted, CASE WHEN PFTFEV1_Predicted <> 0 THEN (PFTFEV1_Actual/PFTFEV1_Predicted) ELSE 0 END AS PFTFEV1_Pct,
                        [PFTFEV1andFVC_Actual] AS PFTFEV1FVC_Actual, [PFTFEV1andFVC_Predicted] AS PFTFEV1FVC_Predicted,
                         CASE WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN ([PFTFEV1andFVC_Actual]/[PFTFEV1andFVC_Predicted]) ELSE 0 END AS PFTFEV1FVC_Pct,
                       
                       PFTTLC_Actual, PFTTLC_Predicted, CASE WHEN PFTTLC_Predicted <> 0 THEN (PFTTLC_Actual/PFTTLC_Predicted) ELSE 0 END AS PFTTLC_Pct, 
                        PFTDLCO_Actual, PFTDLCO_Predicted, CASE WHEN PFTDLCO_Predicted <> 0 THEN (PFTDLCO_Actual/PFTDLCO_Predicted) ELSE 0 END AS PFTDLCO_Pct, 
                        PFTTV, IsThirdPartyComplaint, IsMDL, AllegationType + ' - ' + Material AS Allegation, CASE WHEN IsPrimaryForClaim = 1 THEN 'YES' ELSE 'NO' END AS PrimaryLawsuit, DiseaseDiagnosisDate
FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
                      dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
                      dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID 
                      
						INNER JOIN tblDefenseCounselState dcs on l.DefenseCounselAddressID=dcs.DefenseCounselAddressID and l.StateID=dcs.StateID
						INNER JOIN tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCounselAddressID=dca.DefenseCounselAddressID
						INNER JOIN @UserFirmState u on dca.DefenseCounselID = u.DefenseCounselID and l.StateID = u.StateID INNER JOIN
                      
                      dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
                      dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
                      dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
                          (SELECT     dcs.DefenseCounseladdressID, dca.DefenseCounsel, dcs.ResponsibleDefenseCounseladdressID, dcs.StateID, 
                                                   r.DefenseCounsel AS ResponsibleDefenseCOunsel, dcs.IsActiveCounsel
                            FROM          dbo.tblDefenseCounselState AS dcs LEFT JOIN
											dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
											LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
											LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID LEFT JOIN
                                                   dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
                            GROUP BY dca.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounseladdressID, dcs.ResponsibleDefenseCounseladdressID, dcs.IsActiveCounsel, dcs.StateID) 
                      AS r_1 ON r_1.DefenseCounseladdressID = L.DefenseCounselAddressID AND r_1.StateID = L.StateID
                      left join (SELECT * from tblClaimDisease where PrimaryDisease = 1) as CD on CD.ClaimID = c.ClaimID
					left join tblDisease ddis on ddis.DiseaseID = cd.DiseaseID
					left join tblDisease_ALT disa ON disa.DiseaseID_Alt = cd.DiseaseID
					left join tblHighLevelDisease hddis on hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
					 LEFT JOIN (SELECT * FROM tblClaimantEmployment WHERE IsPrimary = 1) CE ON CE.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID   
                      Left Join (Select * From dbo.tblClaimLawsuitStatusTracking where IsPrimaryStatus = 1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
                      LEFT JOIN dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID LEFT OUTER JOIN
                      dbo.tblClaimantSettlement AS ClaimSett ON ClaimSett.ClaimLawsuitID = CL.ClaimLawsuitID LEFT OUTER JOIN
					  dbo.tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID left outer join
                      dbo.tblReleaseType AS rt ON rt.ReleaseTypeID = spd.ReleaseTypeID
                      left join tblplaintiffcounseladdress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid
                      LEFT JOIN tblDoctor doc ON cd.DoctorID = doc.DoctorID 
                      LEFT JOIN tblDoctor ILODoc ON ILODoctorID=ILODoc.DoctorID
                      LEFT JOIN tblDoctor PFTDoc ON PFTDoctorID=PFTDoc.DoctorID
                      LEFT JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
                      INNER JOIN tblAllegationType at ON a.AllegationTypeID = at.AllegationTypeID
                      INNER JOIN tblMaterial mat ON a.MaterialID = mat.MaterialID
                      
            
WHERE C.ClaimID = @ClaimID --AND cl.IsPrimaryForClaim = 1
ORDER BY PrimaryLawsuit DESC, State
END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryAllClaimsDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryAllClaimsDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryAllClaimsDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryAllClaimsDetails] TO [power_user]
GO
