SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryDocList] (@ClaimantPersonalInfoID int, @emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress) IN (132, 133) OR (SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1
BEGIN

	SELECT d.DocumentID as DocumentID, COALESCE(ClaimantDocumentType +': ','') + DOCUMENTTITLE as DocumentTitle
    FROM tblDocuments D
    LEFT JOIN tblClaimantDocumentType CDT ON CDT.ClaimantDocumentTypeID = D.DetailDocumentTypeID
    inner join tblMultiClaimantDocument mcd on mcd.DocumentID=d.DocumentID
    where mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
    UNION
    SELECT NULL as DocumentID, '-- Select a Document --'  as DocumentTitle
    ORDER BY 2
END
ELSE
BEGIN
    SELECT d.DocumentID as DocumentID, COALESCE(ClaimantDocumentType +': ','') + DOCUMENTTITLE as DocumentTitle
    FROM tblDocuments D
    LEFT JOIN tblClaimantDocumentType CDT ON CDT.ClaimantDocumentTypeID = D.DetailDocumentTypeID
    inner join tblMultiClaimantDocument mcd on mcd.DocumentID=d.DocumentID
    inner join dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID
    where mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
    AND dg.DocumentGroupID NOT IN (2, 4) AND cdt.ClaimantDocumentTypeID NOT IN (1, 40)
    UNION
    SELECT NULL as DocumentID, '-- Select a Document --'  as DocumentTitle
    ORDER BY 2
END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDocList] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDocList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDocList] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDocList] TO [power_user]
GO
