SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryfrmUpdateComplaintQCStatus] (@QCName nvarchar(250), @ClaimantPersonalInfoID int, @documentID int)
AS

--DECLARE @QCName nvarchar(250)
--DECLARE @ClaimantPersonalInfoID int
--DECLARE @documentID int
--SET @QCName = 'youngc'
--SET @ClaimantPersonalInfoID = 1
--SET @documentID = 31643

UPDATE tblMultiClaimantDocument
	SET QCBy = @QCName, QCDate = GETDATE()
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
		AND DocumentID = @documentID
		
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateComplaintQCStatus] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateComplaintQCStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmUpdateComplaintQCStatus] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmUpdateComplaintQCStatus] TO [power_user]
GO
