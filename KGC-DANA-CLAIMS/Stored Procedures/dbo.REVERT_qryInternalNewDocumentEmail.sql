SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[REVERT_qryInternalNewDocumentEmail]
As
Begin

/*Query run daily to send an internal email listing any new documents. -BEH*/

--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(MAX)

SET @Subject =    'Dana Companies, LLC: New Documents Uploaded as of ' + convert(varchar, getdate(), 107) 
--Set body font, insert KCIC logo/banner at the top of the email  
--KCIC logo is being pulled from the KCIC site.  Contact D. Snyder with questions or requests.   
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>
<H4>Below are the new uploaded documents for review.</H4>'

SET @BodyMessage =  @BodyMessage + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	border: 1px solid #38160C;
	color: #F6ECF0;}
TD	{font-family: Goudy Old Style; 
	font-size: 10pt; 
	text-align: center;
	border: 1px solid rgb(0,121,193);
	padding: .3em;
	color: #000000}
TH	{border: 1px solid #ffffff;
	font-size: 10pt; 
	padding: .3em;
	color: #F6ECF0;
	background: rgb(0,121,193);}
--->
</STYLE>'


--Create temp table to hold all new documents
DECLARE @NewDocuments TABLE (docID nvarchar(200), FirmID int, cpiID bigint)
INSERT INTO @NewDocuments
SELECT spdocid, FirmID, mcd.ClaimantPersonalInfoID
FROM tblDocuments d
	inner join tblMultiClaimantDocument mcd on mcd.DocumentID=d.DocumentID
WHERE datecreated BETWEEN GETDATE()-1 AND GETDATE() 
      --Do not include any documents uploaded by us.
      AND RIGHT(CreatedBy, 11) != 'kcicllc.com'
      AND detailDocumentTypeID NOT IN (0,40,41)
GROUP BY SPDocId, FirmID, mcd.ClaimantPersonalInfoID

--Create cursor for each firm
DECLARE @DocCount int, @firm nvarchar(200), @firmURL nvarchar(200)

DECLARE Document_Curs CURSOR FOR

SELECT COUNT(DISTINCT docid), dc.DefenseCounsel, 'https://admin.kcicextranet.com/clients/dana/cp/Pages/Documents.aspx'
FROM @newdocuments d
INNER JOIN tblDefenseCounsel dc ON d.FirmID = dc.DefenseCounselID
INNER JOIN vtblFirmCase fc ON dc.DefenseCounselID = fc.FirmID
WHERE CaseID = 3
GROUP BY dc.DefenseCounsel
ORDER BY dc.DefenseCounsel
--Open
OPEN Document_Curs
--Fetch
FETCH NEXT FROM Document_Curs INTO @DocCount, @firm, @firmURL
WHILE @@fetch_status = 0
BEGIN
      --List which firm, how many documents, and the firm url
      SET @BodyMessage = @BodyMessage + convert(nvarchar(10), (Select @DocCount)) + ' New Document(s) in the <A HREF =' 
            + @firmURL+'>' + @firm + '</A> Document Library<br><br>'    
FETCH NEXT FROM Document_Curs INTO @DocCount, @firm, @firmURL
      END
CLOSE Document_Curs
DEALLOCATE Document_Curs

IF((SELECT COUNT(distinct ClaimantPersonalInfoID) FROM tblClaimLawsuit cl INNER JOIN tblClaim c ON c.ClaimID = cl.ClaimID INNER JOIN @NewDocuments nd ON nd.cpiID = c.ClaimantPersonalInfoID
										inner join tblClaimLawsuitStatusTracking clst on clst.ClaimLawsuitID=cl.ClaimLawsuitID
WHERE clst.ClaimLawsuitStatusID IN (23, 28)  and IsPrimaryStatus = 1) > 0)
BEGIN
SET @BodyMessage = @bodymessage + '<H4>The following settled claimants have new documents:</H4>'

SET @BodyMessage = @BodyMessage + N'<table>' + N'<tr><th>Last Name</th><th>First Name</th><th>Document Count</th></tr>'



DECLARE @firstName nvarchar(500), @lastName nvarchar(500)
DECLARE Claimant_Curs CURSOR FOR
SELECT DISTINCT FirstName, LastName, COUNT(DISTINCT docId) AS DocumentCount FROM tblClaimantPersonalInfo cpi
INNER JOIN tblClaim c ON c.ClaimantPersonalInfoID  = cpi.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
INNER JOIN @NewDocuments nd ON nd.cpiID = cpi.ClaimantPersonalInfoID
INNER JOIN (SELECT ClaimantPersonalInfoID FROM tblClaimLawsuit cl INNER JOIN tblClaim c ON c.ClaimID = cl.ClaimID inner join tblclaimlawsuitstatustracking clst on clst.claimlawsuitid = cl.claimlawsuitid
				 WHERE clst.ClaimLawsuitStatusID IN (23, 28) and isprimarystatus = 1) s ON cpi.ClaimantPersonalInfoID = s.ClaimantPersonalInfoID
GROUP BY FirstName, LastName
ORDER BY LastName, FirstName
   
--Open
OPEN Claimant_Curs
--Fetch
FETCH NEXT FROM Claimant_Curs INTO @firstname, @lastname, @DocCount
WHILE @@fetch_status = 0
BEGIN


SET @BodyMessage = @BodyMessage + N'<tr><td>' + (SELECT @lastName) + '</td><td>' + (SELECT @firstName) + '</td><td>' + (SELECT CONVERT(nvarchar(10), @DocCount)) + '</td></tr>' 


FETCH NEXT FROM Claimant_Curs INTO  @firstname, @lastname, @DocCount
END
CLOSE Claimant_Curs
DEALLOCATE Claimant_Curs

SET @BodyMessage = @BodyMessage + N'</table>'
END       
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person responsible for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

--Check for new documents
IF(SELECT COUNT(DISTINCT docid)
FROM @newdocuments) <> 0      
BEGIN
      --Send email
      EXEC msdb.dbo.sp_send_dbmail
      @profile_name='Admin',
      @recipients = 'dana@kcicllc.com',
	  @copy_recipients = 'wegmanc@kcicllc.com; monahanc@kcicllc.com; prossc@kcicllc.com; hagep@kcicllc.com;', 
      @subject= @subject,
      @body= @BodyMessage,
      @body_Format = 'html'
      END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalNewDocumentEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalNewDocumentEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalNewDocumentEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalNewDocumentEmail] TO [power_user]
GO
