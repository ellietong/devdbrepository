SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryUpdDocExtendedInfo]
(@DocumentID int, @DocumentTypeID int, @DocumentSourceID int, @FirmID int, @Accept bit, @DismissalTypeID int,  @DismissalDate datetime, @EmailAddress nvarchar(250),
@ReleaseTypeID int, @ReleaseRequestDate datetime, @ReleaseSentDate datetime, @ExecutedReleaseRcvdDate datetime)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblDocuments
	SET DetailDocumentTypeID = @DocumentTypeID,
		DocumentSourceID = @DocumentSourceID,
		FirmID = @FirmID,
		DateModified = GETDATE(),
		ModifiedBy = @EmailAddress,
		DismissalProcessDate = CASE WHEN @DocumentTypeID = 16 AND DismissalProcessDate IS NULL THEN GETDATE() WHEN @DocumentTypeID <> 16 AND DismissalProcessDate IS NOT NULL THEN NULL ELSE DismissalProcessDate END,
		ClaimLawsuitStatusID = CASE WHEN @DocumentTypeID = 16 THEN @DismissalTypeID ELSE NULL END,
		DismissalDate = CASE WHEN @DocumentTypeID = 16 THEN @DismissalDate ELSE DismissalDate END,
		Accepted = CASE WHEN COALESCE(Accepted, 0) = 1 THEN Accepted WHEN @Accept = 0 AND COALESCE(Accepted, 0) = 0 THEN NULL ELSE 1 END,
		AcceptedBy = Case WHEN AcceptedBy IS NOT NULL THEN AcceptedBy WHEN @Accept = 1 THEN @EmailAddress ELSE NULL END,
		DateAccepted = CASE WHEN DateAccepted IS NOT NULL THEN DateAccepted WHEN @Accept = 1 THEN GETDATE() ELSE NULL END
		
	WHERE DocumentID = @DocumentID
	
	
DECLARE @ClaimantPersonalInfoID int

/**Declare cursor to attach documentation to each claimant the document is attached to**/
DECLARE Cur_Claimants CURSOR Local Fast_Forward for
select ClaimantpersonalinfoID from tblmulticlaimantdocument where documentid = @DocumentID

OPEN Cur_Claimants
FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID
WHILE @@FETCH_STATUS = 0
BEGIN

DECLARE @ClaimLawsuitID int
SET @ClaimLawsuitID = 
(select ClaimlawsuitID from tblClaimantPersonalInfo cpi inner join tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID inner join tblClaimLawsuit cl on cl.ClaimID=c.ClaimID where c.IsCurrentClaim = 1 and cl.IsPrimaryForClaim = 1 and cpi.ClaimantPersonalInfoID =@ClaimantPersonalInfoID)

DECLARE @NewSettlePaymentDocumentationID INT


--if there is no record for this claim in tblSettlePaymentDocumentation then create one
if (select COUNT(setpymtid) from tblSettlePaymentDocumentation where ClaimLawsuitID = @ClaimLawsuitID) = 0 
begin
Insert into tblSettlePaymentDocumentation (ClaimLawsuitID)
select @ClaimLawsuitID
set @NewSettlePaymentDocumentationID = (select MAX(SetPymtID) from tblSettlePaymentDocumentation)
end
ELSE
begin
set @NewSettlePaymentDocumentationID = (Select setpymtID from tblSettlePaymentDocumentation where ClaimLawsuitID = @ClaimLawsuitID)
end

--Update release information
UPDATE tblSettlePaymentDocumentation 
SET	ReleaseRqstDate = COALESCE(@ReleaseRequestDate, ReleaseRqstDate),
	ReleaseToPC = COALESCE(@ReleaseSentDate, ReleaseToPC),
	ExecutedReleaseDate = COALESCE(@ExecutedReleaseRcvdDate, ExecutedReleaseDate),
	ReleaseTypeID = COALESCE(@ReleaseTypeID, ReleaseTypeID),
	SPUsername = @EmailAddress
WHERE SetPymtID = @NewSettlePaymentDocumentationID


FETCH NEXT FROM Cur_Claimants INTO @ClaimantPersonalInfoID

END
CLOSE Cur_Claimants
DEALLOCATE Cur_Claimants

END
GO
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdDocExtendedInfo] TO [power_user]
GO
