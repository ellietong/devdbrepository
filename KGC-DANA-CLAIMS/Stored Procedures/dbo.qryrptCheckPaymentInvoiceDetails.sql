SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCheckPaymentInvoiceDetails] (@CheckID int)

As

SET NOCOUNT ON

--Declare @CheckID int
--Set @CheckID = 1481

Declare @FundingInvoices table(DefenseCounselID bigint, Firm nvarchar(250), InvoiceNumber nvarchar(250), InvoiceTotal money, Period date, SubmittedDate date)
Insert into @FundingInvoices


select  DefenseCounselID as DefenseCounselID,
		DefenseCounsel as Firm,
		i.OriginalInvoiceNumber as InvoiceNumber,
		case when AppealFlag=1 then
		SUM(AppealTotal) else
		sum(AdjustedTotal) end as InvoiceTotal,
		max(DATENAME("mm", i.Period) + ' ' + DATENAME("yy", i.Period)) as Period,
		MAX(SubmittedDate) as SubmittedDate
from tblPaymentLegalBill plb
inner join m_tblLegalBillInvoices i on plb.InvoiceID=i.InvoiceID
inner join tblCheck c on plb.CheckID=c.CheckID
Left Join tblDefenseCounselAddress da on i.DefenseCounselAddressID=da.DefenseCounselAddressID
Where c.CheckID = @CheckID
Group by DefenseCounselID, DefenseCounsel, i.OriginalInvoiceNumber, AppealFlag

Union

select  DefenseCounselID as DefenseCounselID,
		Coalesce(ExpertFirm, DefenseCounsel) as Firm,
		di.InvoiceNumber as InvoiceNumber,
		SUM(InvoiceTotal) as InvoiceTotal,
		MAX(Period),
		MAX(SubmitDate) as SubmittedDate
from tblPaymentDefense pd
inner join tbldefenseinvoice di on pd.DefenseInvoiceID=di.defenseInvoiceID
inner join tblCheck c on pd.CheckID=c.CheckID
left Join m_tblExpertFirm ef on ef.ExpertFirmID = di.ExpertFirmID
Left Join tblDefenseCounselAddress da on di.DefenseCounselAddressID=da.DefenseCounselAddressID
Where c.CheckID = @CheckID
Group by DefenseCounselID, Coalesce(ExpertFirm, DefenseCounsel), di.InvoiceNumber

Select f.Firm, max(f.Period) as Period, max(f.SubmittedDate) as SubmittedDate, f.InvoiceNumber, sum(f.InvoiceTotal) as InvoiceTotal
From @FundingInvoices f
Group BY f.DefenseCounselID, f.Firm, f.InvoiceNumber
Order By f.Firm, max(f.Period), f.InvoiceNumber

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentInvoiceDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentInvoiceDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentInvoiceDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentInvoiceDetails] TO [power_user]
GO
