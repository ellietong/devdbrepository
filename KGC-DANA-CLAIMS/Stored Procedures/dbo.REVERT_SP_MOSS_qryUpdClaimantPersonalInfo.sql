SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryUpdClaimantPersonalInfo]
(@ClaimantPersonalInfoID bigint, @Address1 nvarchar(250), @Address2 nvarchar(250), @StateID int, @City nvarchar(250), @Zip nvarchar(5), @Mailzip nvarchar(4), 
@BirthDate datetime, @DeceasedDate datetime, @GenderID int, @MaritalStatusID int, @IsSmoker bit, @SmokerStart datetime, @SmokerEnd datetime, @PacksPerDay float,
@Username nvarchar(250), @FullSSN nvarchar(150))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE tblClaimantPersonalInfo
    SET Address1 = UPPER(@Address1),
		Address2 = UPPER(@Address2),
		StateID = @StateID,
		City = UPPER(@City),
		Zip = @Zip,
		MailZip = @Mailzip,
		BirthDate = @BirthDate,
		DeceasedDate = @DeceasedDate,
		GenderID = @GenderID,
		MaritalStatusID = @MaritalStatusID,
		IsSmoker = @IsSmoker,
		SmokerStart = @SmokerStart,
		SmokerEnd = @SmokerEnd,
		PacksPerDay = @PacksPerDay,
		SPUsername = @Username,
		SocialSecurityNumber = REPLACE(REPLACE(@FullSSN, '-', ''), ' ', '')
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	
END


--CPI.ClaimantPersonalInfoID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
--                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
--                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, Gender, cpi.GenderID, cpi.MaritalStatusID,
--                      MaritalStatus, coalesce(nullif(coalesce(Address1 + CHAR(13) + CHAR(10), '') + coalesce(Address2 + CHAR(13) + CHAR(10),'') + 
--						coalesce(City + ', ','') + coalesce(s.State + ' ','') + coalesce(convert(varchar,Zip),'') + Coalesce('-'+convert(varchar,mailzip),''),''),'') as CompleteAddress, BirthDate, DeceasedDate, City, cpi.StateID, Zip, MailZip, Address1, Address2
                     
                     
                     
                     
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdClaimantPersonalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdClaimantPersonalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdClaimantPersonalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdClaimantPersonalInfo] TO [power_user]
GO
