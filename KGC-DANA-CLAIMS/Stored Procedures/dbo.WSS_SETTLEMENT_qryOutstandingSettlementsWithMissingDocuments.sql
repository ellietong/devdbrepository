SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithMissingDocuments] (@ClaimLawsuitID bigint)

As

SET NOCOUNT ON

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 267049


DECLARE @ClaimantDocs TABLE(DODCert int, EstatePapers int, SettlementConfirmation int, 
							MedicarePlaintiff int, MedicarePlaintiffCounsel int, 
							MedicareFormB int, MMSEA int)

DECLARE @MissingDocuments TABLE(MissingDocName nvarchar(100))

INSERT INTO @ClaimantDocs
	SELECT --sp.ClaimantIDed, sp.DxMedicals, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE sp.DODCert END AS DODCert, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE sp.EstatePapers END AS EstatePapers,
		--sp.ProductID, 
		--sp.EmployExpoHist, 
		sp.settlementconfirmation, 
		sp.MedicarePlaintiff,
		sp.MedicarePlaintiffCounsel,
		sp.MedicareFormB,
		sp.MMSEA
		--CASE WHEN MedicareFullyDocumented IS NOT NULL THEN 1 ELSE 0 END
	FROM tblClaim c
		LEFT JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID		
		LEFT JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		LEFT join tblSettlementDocumentation sp on sp.SetPymtID = spd.SetPymtID
	WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	
--GET PC OF CLAIM LAWSUIT
DECLARE @PlaintiffCounselID bigint = (	SELECT PlaintiffCounselID 
										FROM tblClaimLawsuit cl
										LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
										INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID WHERE cl.ClaimLawsuitID = @ClaimLawsuitID)
	
		
--DOCUMENTATION REQUIRED FOR ALL PLAINTIFFS COUNSEL
INSERT INTO @MissingDocuments
	SELECT 'Death Certificate' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DODCert IS NULL Or DODCert = 0
INSERT INTO @MissingDocuments
	SELECT 'Estate Paperwork' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EstatePapers IS NULL Or EstatePapers = 0
INSERT INTO @MissingDocuments
	SELECT 'Release Request Form' AS MissingDocName
	WHERE NOT EXISTS (
		SELECT d.DocumentID
		FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		WHERE ClaimLawsuitID = @ClaimLawsuitID
				AND DetailDocumentTypeID = 32)	
		AND (SELECT ReleaseRqstDate FROM tblSettlePaymentDocumentation WHERE ClaimLawsuitID = @ClaimLawsuitID) IS NULL		
INSERT INTO @MissingDocuments
	SELECT 'Unexecuted Release' AS MissingDocName
	WHERE NOT EXISTS (
		SELECT d.DocumentID
		FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		WHERE ClaimLawsuitID = @ClaimLawsuitID
				AND DetailDocumentTypeID = 55)--54 = unexecuted release on dev, 55 on prod
	AND (SELECT ReleaseToPC FROM tblSettlePaymentDocumentation WHERE ClaimLawsuitID = @ClaimLawsuitID) IS NULL
	
	
INSERT INTO @MissingDocuments
		SELECT 'Executed Release' AS MissingDocName
		WHERE NOT EXISTS (
			SELECT d.DocumentID
			FROM tblDocuments d
			INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
			WHERE ClaimLawsuitID = @ClaimLawsuitID
					AND DetailDocumentTypeID = 31)
		AND (SELECT ExecutedReleaseDate FROM tblSettlePaymentDocumentation WHERE ClaimLawsuitID = @ClaimLawsuitID) IS NULL
					
					
					
--Load firms that need MMSEA form
DECLARE @MMSEA TABLE(PlaintiffCounselID int)
INSERT INTO @MMSEA  --, Keller, Mulvihill respectively
SELECT 643 --Deaton
UNION
SELECT 519 --Keller
UNION
SELECT 952 --Mulvihill

--Load firms that need Medicare Form B
DECLARE @FormB TABLE(PlaintiffCounselID int)
INSERT INTO @FormB  
SELECT 26 --Cooney & Conway
UNION
SELECT 257 --Kazan
UNION
SELECT 294 --O'Brien

--FIRMS WITH REP of PLAINTIFF
DECLARE @RepOfPlaintiff TABLE(PlaintiffCounselID int)
INSERT INTO @RepOfPlaintiff  
SELECT 643 --Deaton
UNION
SELECT 952 --Mulvihill
UNION
SELECT 150 --Weitz
UNION
SELECT 519 --Keller



--FIRMS WITH REP of PLAINTIFF COUNSEL
DECLARE @RepOfPlaintiffCounsel TABLE(PlaintiffCounselID int)
INSERT INTO @RepOfPlaintiffCounsel  
SELECT 150 --Weitz

--MMSEA
IF(@PlaintiffCounselID IN (SELECT PlaintiffCounselID FROM @MMSEA))
BEGIN			
	INSERT INTO @MissingDocuments
	SELECT 'MMSEA Settlement Form' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MMSEA IS NULL Or MMSEA = 0
END

--FORM B
IF(@PlaintiffCounselID IN (SELECT PlaintiffCounselID FROM @FormB))
BEGIN			
	INSERT INTO @MissingDocuments
	SELECT 'Medicare Form B' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MedicareFormB IS NULL Or MedicareFormB = 0
END

--REP OF PLAINTIFF
IF(@PlaintiffCounselID IN (SELECT PlaintiffCounselID FROM @RepOfPlaintiff))
BEGIN			
	INSERT INTO @MissingDocuments
	SELECT 'Representation of Plaintiff' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MedicarePlaintiff IS NULL Or MedicarePlaintiff = 0
END

--REP OF PLAINTIFF COUNSEL
IF(@PlaintiffCounselID IN (SELECT PlaintiffCounselID FROM @RepOfPlaintiffCounsel))
BEGIN			
	INSERT INTO @MissingDocuments
	SELECT 'Representation of Plaintiff Counsel' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MedicarePlaintiffCounsel IS NULL Or MedicarePlaintiffCounsel = 0
END


--DEFAULT: ALL OTHER PLAINTIFFS COUNSEL (ie: those not in the tables constructed above)
IF(@PlaintiffCounselID NOT IN (	SELECT PlaintiffCounselID FROM @MMSEA
								UNION
								SELECT PlaintiffCounselID FROM @FormB
								UNION
								SELECT PlaintiffCounselID FROM @RepOfPlaintiff
								UNION
								SELECT PlaintiffCounselID FROM @RepOfPlaintiffCounsel
))
BEGIN
	--MMSEA
	INSERT INTO @MissingDocuments
	SELECT 'MMSEA Settlement Form' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MMSEA IS NULL Or MMSEA = 0
	
	--REP OF PLAINTIFF	
	INSERT INTO @MissingDocuments
	SELECT 'Representation of Plaintiff' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MedicarePlaintiff IS NULL Or MedicarePlaintiff = 0

	--REP OF PLAINTIFF COUNSEL	
	INSERT INTO @MissingDocuments
	SELECT 'Representation of Plaintiff Counsel' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MedicarePlaintiffCounsel IS NULL Or MedicarePlaintiffCounsel = 0

END

SELECT MissingDocName FROM @MissingDocuments
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithMissingDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithMissingDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithMissingDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithMissingDocuments] TO [power_user]
GO
