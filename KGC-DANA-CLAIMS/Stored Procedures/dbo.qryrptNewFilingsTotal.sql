SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[qryrptNewFilingsTotal]
(@EndDate date, @StartDate date)

AS
SET NOCOUNT ON

--Declare @EndDate Date
--Set @EndDate = '1/1/2011'
--Declare @StartDate Date
--Set @StartDate = '1/1/1900'

Declare @DiseaseCount Table (ClaimID int, MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int, SilicosisCount int)
Insert into @DiseaseCount

Select C.ClaimID, 
--Meso
Case	when Meso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when LC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when OC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when NM.HighLevelDiseaseID = 4 Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when Unk.HighLevelDiseaseID = 5 Then 1
		When Unk.HighLevelDiseaseID is Null Then 1
Else 0 End AS UnknownCount,
--Silicosis
Case	when Sil.HighLevelDiseaseID = 7 Then 1
Else 0 End as SilicosisCount


From tblClaim c
LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
--Meso
Left Join tblDisease Meso on CD.DiseaseID=Meso.DiseaseID

--Lung Cancer
Left Join tblDisease LC on CD.DiseaseID=LC.DiseaseID

--Other Cancer
Left Join tblDisease OC on CD.DiseaseID=OC.DiseaseID

--Non-Malig
Left Join tblDisease NM on CD.DiseaseID=NM.DiseaseID

--Unknown
Left Join tblDisease Unk on CD.DiseaseID=Unk.DiseaseID

--Silicosis
Left Join tblDisease Sil on CD.DiseaseID=Sil.DiseaseID



SELECT  COALESCE(YEAR(FileDate), YEAR(ServiceDate)) AS Year, 
	    COUNT(c.ClaimID) AS NewFilings, SUM(MesoCount) AS MesoFilings, SUM(LungCancerCount) AS LCFilings, 
	    SUM(OtherCancerCount) AS OCFilings, SUM(NonMaligCount) AS NMFILINGS, SUM(SilicosisCount) AS SilicosisCount,
	    SUM(UnknownCount) AS UnknownFilings
FROM @DiseaseCount dc
Left Join tblClaim c on dc.ClaimID=c.ClaimID
Left JOIN tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
Left JOIN tblLawsuit l on cl.LawsuitID = l.LawsuitID
Left JOIN tblState s on l.StateID = s.StateID

WHERE IsPrimaryForClaim = 1 and 
 (FileDate <= @EndDate or ServiceDate <= @EndDate) and 
(FileDate >= @StartDate or ServiceDate >= @StartDate )--and
Group By COALESCE(YEAR(FileDate), YEAR(ServiceDate))
Order By COALESCE(YEAR(FileDate), YEAR(ServiceDate))
	   

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsTotal] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsTotal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsTotal] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsTotal] TO [power_user]
GO
