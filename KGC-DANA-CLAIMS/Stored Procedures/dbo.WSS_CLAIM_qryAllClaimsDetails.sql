SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_CLAIM_qryAllClaimsDetails] (@ClaimID		 bigint = NULL, 
												  @emailaddress nvarchar(250))
AS

SET NOCOUNT ON

--The following set of variables can be used for testing.
--DECLARE @ClaimID int
--DECLARE @emailaddress nvarchar(250)
--SET @ClaimID = 109911--494044
--SET @emailaddress = 'bpassmore@powersfrost.com'

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE c.ClaimID = ' + CONVERT(nvarchar(20),@ClaimID)


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString = ' LEFT JOIN  tblDefenseCounselAddress dca	ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID '
ELSE
BEGIN
	SET @JoinString = ' LEFT JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND dj.MatterEntityID = m.MatterEntityID
						LEFT JOIN tblDefenseCounselAddress dca on l.DefenseCounselAddressID=dca.DefenseCounselAddressID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT c.ClaimID, 
		hddis.HighLevelDisease AS Disease, 
		S.State, 
		J.Jurisdiction, 
		cs.ClaimSequence, 
        cls.ClaimLawsuitStatus, 
        CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) AS FileDate, 
        L.DocketNumber, 
		L.CaseCaption, 
		L.LeadPlaintiffLastName + '', '' + L.LeadPlaintiffFirstName AS LeadPlaintiffName,  
		pca.PlaintiffCounsel AS PrimaryLocalPlaintiffCounsel, 
		DefenseCounsel AS PrimaryDefenseCounsel, 
		ClaimDiseaseID, 
		cl.LawsuitID, 
		ActiveTrialDate,
		doc.DoctorLastName + '', '' + COALESCE(doc.DoctorFirstName, '''') + COALESCE('' '' + doc.DoctorMiddleName, '''') AS Doctor,CauseOfInjuryID,
		CASE WHEN ILODoctorID IS NULL THEN NULL ELSE ilodoc.DoctorLastName + '', '' + COALESCE(ilodoc.DoctorFirstName, '''') + COALESCE('' '' + ilodoc.DoctorMiddleName, '''') END AS ILODoctor,
		CASE WHEN PFTDoctorID IS NULL THEN NULL ELSE pftdoc.DoctorLastName + '', '' + COALESCE(pftdoc.DoctorFirstName, '''') + COALESCE('' '' + pftdoc.DoctorMiddleName, '''')END AS PFTDoctor, 
		ILODoctorID, 
		ILOProfusion, 
		ILODate, 
		PFTDoctorID,
		PFTDate, 
		PFTTypeID, 
		PFTFVC_Actual, 
		PFTFVC_Predicted, 
		CASE WHEN PFTFVC_Predicted <> 0 THEN (PFTFVC_Actual/PFTFVC_Predicted) ELSE 0 END AS PFTFVC_Pct,
		PFTFEV1_Actual, 
		PFTFEV1_Predicted, 
		CASE WHEN PFTFEV1_Predicted <> 0 THEN (PFTFEV1_Actual/PFTFEV1_Predicted) ELSE 0 END AS PFTFEV1_Pct,
		[PFTFEV1andFVC_Actual] AS PFTFEV1FVC_Actual, 
		[PFTFEV1andFVC_Predicted] AS PFTFEV1FVC_Predicted,
		CASE WHEN [PFTFEV1andFVC_Predicted] <> 0 THEN ([PFTFEV1andFVC_Actual]/[PFTFEV1andFVC_Predicted]) ELSE 0 END AS PFTFEV1FVC_Pct,
		PFTTLC_Actual, 
		PFTTLC_Predicted, 
		CASE WHEN PFTTLC_Predicted <> 0 THEN (PFTTLC_Actual/PFTTLC_Predicted) ELSE 0 END AS PFTTLC_Pct, 
		PFTDLCO_Actual, 
		PFTDLCO_Predicted, 
		CASE WHEN PFTDLCO_Predicted <> 0 THEN (PFTDLCO_Actual/PFTDLCO_Predicted) ELSE 0 END AS PFTDLCO_Pct, 
		PFTTV, 
		AllegationType + '' - '' + Material AS Allegation, 
		CASE WHEN IsPrimaryForClaim = 1 THEN ''YES'' ELSE ''NO'' END AS PrimaryLawsuit, DiseaseDiagnosisDate,
		COALESCE(IsPrimaryForClaim,0),
		cd.DiseaseID, 
		cl.HistoricalNotes
     FROM tblClaimantPersonalInfo cpi 
		LEFT JOIN tblClaim c							ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblLawsuit l							ON cl.LawsuitID = l.LawsuitID
		INNER JOIN tblState s							ON s.StateID = l.StateID
		INNER JOIN tblJurisdiction j					ON j.JurisdictionID = l.JurisdictionID
		INNER JOIN tblClaimSequence cs					ON cs.ClaimSequenceID = c.ClaimSequenceID
		LEFT JOIN tblMatter m					 ON c.MatterID = m.MatterID ' +
		@JoinString + ' 
        LEFT JOIN  (SELECT * 
					FROM tblClaimDisease 
					WHERE PrimaryDisease = 1) cd		ON cd.ClaimID = c.ClaimID
		LEFT JOIN  tblDisease ddis						ON ddis.DiseaseID = cd.DiseaseID
		LEFT JOIN  tblDisease_ALT disa					ON disa.DiseaseID_Alt = cd.DiseaseID
		LEFT JOIN  tblHighLevelDisease hddis			ON hddis.HighLevelDiseaseID = ddis.HighLevelDiseaseID OR hddis.HighLevelDiseaseID = disa.HighLevelDiseaseID
		LEFT JOIN  (SELECT * 
					FROM tblClaimLawsuitStatusTracking 
					WHERE IsPrimaryStatus = 1) clst		ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
        LEFT JOIN  tblClaimLawsuitStatus cls			ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
        LEFT JOIN  tblplaintiffcounseladdress pca		ON pca.plaintiffcounseladdressid = l.plaintiffcounseladdressid
        LEFT JOIN  tblDoctor doc						ON cd.DoctorID = doc.DoctorID 
        LEFT JOIN  tblDoctor ILODoc						ON ILODoctorID=ILODoc.DoctorID
        LEFT JOIN  tblDoctor PFTDoc						ON PFTDoctorID=PFTDoc.DoctorID
        LEFT JOIN  tblAllegation a						ON cl.ClaimLawsuitID = a.ClaimLawsuitID
        INNER JOIN tblAllegationType at					ON a.AllegationTypeID = at.AllegationTypeID
        INNER JOIN tblMaterial mat						ON a.MaterialID = mat.MaterialID ' +
     @WhereString + '
     ORDER BY COALESCE(IsPrimaryForClaim,0) DESC
	 OPTION (RECOMPILE)'

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIM_qryAllClaimsDetails] TO [power_user]
GO
