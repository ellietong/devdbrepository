SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_DanaDocumentProduction_20130604]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
select DISTINCT d.DocumentID, d.DocumentTitle, d.DetailDocumentTypeID, cdt.ClaimantDocumentType, d.DocumentURL, AmazonKey			
from tblMultiClaimantDocument mcd			
inner join tblDocuments d on mcd.DocumentID = d.DocumentID	
inner join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
inner join m_AmEmpClaimants_June2013_Analysis amp on amp.ClaimantPersonalInfoID=mcd.ClaimantPersonalInfoID
where d.DocumentID <=424366 AND d.DocumentID > 3357
ORDER BY DocumentID

END
GO
