SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptCashActivity] (@BalanceStartDate datetime, @BalanceEndDate datetime)

As
Set NoCount On

--Declare @MatterID int, @BalanceStartDate datetime, @BalanceEndDate datetime
--Set @MatterID = null
--Set @BalanceStartDate = '10/2/2010'
--Set @BalanceEndDate = '4/11/2012'

Declare @MinBalanceStartDate datetime
Set @MinBalanceStartDate = (Case when @BalanceStartDate < '3/14/2011' then '3/14/2011' Else @BalanceStartDate END)


Declare @StartingBalance money
Set @StartingBalance = (
Coalesce((Select SUM(CashActivityAmount) From tblMiscCashActivity mca 
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID 
where CashActivityDate >= '3/14/2011' and CashActivityDate < @MinBalanceStartDate),0)
+
Coalesce((Select SUM(SourceAmount)*-1 From tblCheck c
Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
Where SourceDate >= '3/14/2011' and SourceDate < @MinBalanceStartDate),0)
)

Declare @Detail table (Matter nvarchar(250), FundingType nvarchar(250), FundingSource nvarchar(250), SourceDate datetime, SourceNumber nvarchar(250), Indemnity money, Defense money, MiscFees money, CashReceipts money, Comment nvarchar(Max), CheckStatus bit)
Insert Into @Detail

--Defense
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, 0, SourceAmount*-1 as SourceAmount, 0, 0, Coalesce(ExpertFirm,DefenseCounsel) as Firm, IsCheckCleared
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentDefense pd on pd.CheckID = c.CheckID
	Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
	Left Join tblDefenseCounselAddress dca on di.DefenseCounselAddressID = dca.DefenseCounselAddressID
	Left Join m_tblExpertFirm ef on di.ExpertFirmID = ef.ExpertFirmID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @MinBalanceStartDate and SourceDate <= @BalanceEndDate) AND c.FundingTypeID in (1,4)
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, Coalesce(ExpertFirm,DefenseCounsel), IsCheckCleared

Union ALL

Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, 0, SourceAmount*-1 as SourceAmount, 0, 0, DefenseCounsel as Firm, IsCheckCleared
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentLegalBill plb on plb.CheckID = c.CheckID
	Inner Join m_tblLegalBillInvoices lbi on lbi.InvoiceID = plb.InvoiceID
	Left Join tblDefenseCounselAddress dca on lbi.DefenseCounselAddressID = dca.DefenseCounselAddressID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @MinBalanceStartDate and SourceDate <= @BalanceEndDate) AND c.FundingTypeID in (3)
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, DefenseCounsel, IsCheckCleared

Union ALL

--Settlement
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1 as SourceAmount, 0, 0, 0, PlaintiffCounsel, IsCheckCleared
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentSettlement ps on ps.CheckID = c.CheckID
	Inner Join tblClaimantSettlement cs on ps.ClaimantSettlementID = cs.ClaimantSettlementID
	Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.FundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.FundingTypeID
Where (SourceDate >= @MinBalanceStartDate and SourceDate <= @BalanceEndDate) AND c.FundingTypeID = 2
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, PlaintiffCounsel, IsCheckCleared

Union ALL

Select Matter, CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, CashActivityAmount, 0, CashActivityNotes, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @MinBalanceStartDate and CashActivityDate <= @BalanceEndDate) AND CashActivityAmount < 0 AND mcat.CashActivityTypeID not in (3,4,6)

Union ALL

Select Matter, CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, 0, CashActivityAmount, CashActivityNotes, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @MinBalanceStartDate and CashActivityDate <= @BalanceEndDate) AND CashActivityAmount > 0 AND mcat.CashActivityTypeID not in (3,4,6)

Union ALL

--Hicks Settlement paid prior to 4/1/2011 transfer
Select Matter, CashActivityType, 'N/A', CashActivityDate, '6000', CashActivityAmount, 0, 0, 0, CashActivityNotes, '1'
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @MinBalanceStartDate and CashActivityDate <= @BalanceEndDate) AND mcat.CashActivityTypeID = 3

Union ALL

--Jacobi Claims defense paid prior to 4/1/2011 transfer
Select Matter, CashActivityType, 'N/A', CashActivityDate, '6001', 0, CashActivityAmount, 0, 0, CashActivityNotes, '1'
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	Inner Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @MinBalanceStartDate and CashActivityDate <= @BalanceEndDate) AND mcat.CashActivityTypeID IN (4,6)

--Select '', 'Starting Balance', 'N/A', @BalanceStartDate , 'N/A', 0, 0, 0, Coalesce(@StartingBalance,0), 'Starting Balance', Null

Select *, @MinBalanceStartDate as BalanceStartDate, @BalanceEndDate as BalanceEndDate from @Detail Order By SourceDate

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [power_user]
GO
