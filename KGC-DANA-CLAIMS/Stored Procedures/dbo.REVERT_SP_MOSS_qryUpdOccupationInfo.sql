SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryUpdOccupationInfo]
(@ClaimantEmploymentID int, @OccupationID int, @EmpStartDate datetime, @EmpEndDate datetime, @Username nvarchar(250), @Jobsite nvarchar(4000), @JobsiteCity nvarchar(250), @JobsiteStateID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE tblClaimantEmployment
    SET OccupationID = @OccupationID,
		EmpStartDate = @EmpStartDate,
		EmpEndDate = @EmpEndDate,
		SPUsername = @Username,
		Jobsite = UPPER(@Jobsite),
		JobsiteCity = UPPER(@JobsiteCity),
		JobsiteStateID = @JobsiteStateID
	WHERE ClaimantEmploymentID = @ClaimantEmploymentID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdOccupationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryUpdOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryUpdOccupationInfo] TO [power_user]
GO
