SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_addDocument]
(
	@DetailDocumentTypeID int,					--aka ClaimantDocumentTypeID
	@ClaimantPersonalInfoID bigint = null,
	@CreatedBy nvarchar(250),
	@DateCreated datetime,
	@DocumentURL nvarchar(4000),
	@DocumentTitle nvarchar(4000),
	@SPDocId nvarchar(50),	
	@Comments nvarchar(4000) = null,			--comments are optional	
	@FirmID int = null,
	@DocumentSourceID int,

	--These are null unless the document is an Invoice document
	@InvoiceNumber nvarchar(50) = null,
	@Period nvarchar(50) = null,
	@DatePayable datetime = null,
	@FeeAmount money = null,
	@DisbursementAmount money = null,
    @Adjustment money = null,
    @AdjustmentNotes nvarchar(4000) = null,
    @StatusID int = null,
    @MatterID int = null,
    @ExpertFirmID int = null,
    @PrivateComments nvarchar(4000) = null,
    @Approved bit = null,
    @ApprovedBy nvarchar(250) = null,
    @DateApproved datetime = null
)

AS
DECLARE @MaxDocID int
SET @MaxDocID = (SELECT IDENT_CURRENT('tblDocuments')+IDENT_INCR('tblDocuments'))

INSERT INTO tblDocuments (
	DetailDocumentTypeID,	
	ClaimantPersonalInfoID,
	Comments,
	DocumentURL,
	DocumentTitle,
	SPDocId,
	CreatedBy,
	DateCreated,
	InvoiceNumber,
	Period,
	DatePayable,
	FeeAmount,
	DisbursementAmount,
    Adjustment,
    AdjustmentNotes,
    StatusID,	
	ModifiedBy,
	DateModified,
	FirmID,
	DocumentSourceID,
	MatterID,
	ExpertFirmID,
	PrivateComments,
	ApprovedBy,
	DateApproved
)
VALUES (	
	@DetailDocumentTypeID,
	@ClaimantPersonalInfoID,
	@Comments,
	REPLACE(@DocumentURL, @DocumentTitle, CONVERT(nvarchar(10), @MaxDocID) + '_' + @DocumentTitle),
	@DocumentTitle,
	@SPDocId,
	@CreatedBy,
	@DateCreated,
	@InvoiceNumber,
	@Period,
	@DatePayable,
	@FeeAmount,
	@DisbursementAmount,
    @Adjustment,
    @AdjustmentNotes,
    @StatusID,	
	@CreatedBy,
	@DateCreated,	
	@FirmID,
	@DocumentSourceID,
	@MatterID,
	@ExpertFirmID,
	@PrivateComments,
	@ApprovedBy,
	@DateApproved
)

SELECT @MaxDocID


GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_addDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_addDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_addDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_addDocument] TO [power_user]
GO
