SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryInsExpertFirm]
(
	@ExpertFirm nvarchar(250),
	@Address1 nvarchar(250),
	@Address2 nvarchar(250),
	@City nvarchar(250),
	@StateID int,
	@Zip nvarchar(10),
	@TaxID nvarchar(50),
	@SPUsername nvarchar(250)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO m_tblExpertFirm
	SELECT @ExpertFirm, @Address1, @Address2, @City, @StateID, @Zip, @TaxID, @SPUsername
		
	SELECT MAX(ExpertFirmID) FROM m_tblExpertFirm
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryInsExpertFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryInsExpertFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryInsExpertFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryInsExpertFirm] TO [power_user]
GO
