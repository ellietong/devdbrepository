SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qrysfrmClaimsForSettlementDocs]
(@DocumentID bigint)

As
Set NoCount On

select LastName + coalesce(', ' + FirstName + ' ' + MiddleName,', ' + FirstName ,', ' +  MiddleName, '') as ClaimantName, 
		SocialSecurityNumber, IndividualDocketNumber, ClaimLawsuitStatus, StatusDate, CaseCaption, DocketNumber, 
		State as FullNameState, cl.ClaimLawsuitID, clst.ClaimLawsuitStatusTrackingID, dismissaldate,
		cs.SettlementDate, spd.*, cpi.ClaimantPersonalInfoID
  from tblClaimantPersonalInfo cpi
  inner join tblMultiClaimantDocument mcd on mcd.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
  inner join tblClaim c on c.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
  inner join tblClaimLawsuit cl on cl.ClaimID=c.ClaimID
  Left join (select * from tblClaimLawsuitStatusTracking where IsPrimaryStatus =1) clst on clst.ClaimLawsuitID = cl.ClaimLawsuitID
  LEFT join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
  inner join tblLawsuit l on l.LawsuitID=cl.LawsuitID
  inner join tblState s on s.StateID=l.StateID
  left join tblClaimantSettlement cs on cs.ClaimLawsuitID = cl.ClaimLawsuitID
  left join tblSettlePaymentDocumentation spd on spd.ClaimLawsuitID = cl.ClaimLawsuitID
  where DocumentID = @DocumentID

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForSettlementDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForSettlementDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmClaimsForSettlementDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmClaimsForSettlementDocs] TO [power_user]
GO
