SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_getDocumentByURL]
	@URL nvarchar(4000)
AS
BEGIN
	SELECT COUNT(*) FROM tblDocuments WHERE DocumentURL = @URL
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_getDocumentByURL] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_getDocumentByURL] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_getDocumentByURL] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_getDocumentByURL] TO [power_user]
GO
