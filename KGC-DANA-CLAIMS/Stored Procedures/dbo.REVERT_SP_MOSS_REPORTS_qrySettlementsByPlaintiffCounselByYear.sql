SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByPlaintiffCounselByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @YearID int, @MonthID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @YearID int
--Set @YearId=2011
--Declare @MonthID int
--Set @MonthID=11
--Declare @matterid int
--Set @matterid= null

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @year int
SET @year = @YearID

DECLARE @Month int
set @Month = @MonthID

select pca.PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(Coalesce(SettlementAmount,0)) as IndemnityPaid, sum(Coalesce(SettlementAmount,0))/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
inner join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
where	YEAR(coalesce(SettlementDate,statusdate))=@year 
		and MONTH(coalesce(SettlementDate,statusdate)) <=@Month
		and ClaimLawsuitStatusID in (23,24,25,28) 
		and IsPrimaryForClaim = 1 
		and  IsPrimaryStatus=1
		and MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
group by pca.PlaintiffCounsel
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, pca.PlaintiffCounsel

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByPlaintiffCounselByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByPlaintiffCounselByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByPlaintiffCounselByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByPlaintiffCounselByYear] TO [power_user]
GO
