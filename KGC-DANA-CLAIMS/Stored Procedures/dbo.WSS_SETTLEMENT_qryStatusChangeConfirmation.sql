SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_SETTLEMENT_qryStatusChangeConfirmation]
(@ClaimLawsuitID int, @Emailaddress nvarchar(250))
As
Set Nocount on

--DECLARE @ClaimLawsuitID INT
--SET @ClaimLawsuitID = 267281

SELECT 
CPI.FirstName + ' ' + COALESCE(CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(Suffix,'') AS ClaimantName,
Jurisdiction + ', ' + UPPER(S.FullNameState) AS Jurisdiction,
DocketNumber,
ClaimLawsuitStatus,
CASE WHEN ClaimLawsuitStatusGroupID=2 THEN '' ELSE SettlementAmount END AS SettlementAmount,
StatusDate AS ResolutionDate,
PaymentTerms,
PlaintiffCounsel,
ProcessedDate AS NotificationDate,
@Emailaddress AS CreatedBy

FROM tblClaimantPersonalInfo CPI
LEFT JOIN tblClaimantRelatedParty RP ON RP.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
LEFT JOIN tblClaimantSuffix CS ON CS.SuffixID=CPI.SuffixID
INNER JOIN tblClaim C ON C.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit CL ON CL.ClaimID=C.ClaimID
INNER JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID=CL.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus CLS ON CLS.ClaimLawsuitStatusID=CLST.ClaimLawsuitStatusID
INNER JOIN tblLawsuit L ON L.LawsuitID=CL.LawsuitID
INNER JOIN tblPlaintiffCounselAddress PCA on PCA.PlaintiffCounselAddressID=L.PlaintiffCounselAddressID
INNER JOIN tblState S ON S.StateID=L.StateID
INNER JOIN tblJurisdiction J ON J.JurisdictionID=L.JurisdictionID
LEFT JOIN tblClaimantSettlement CSET ON CSET.ClaimLawsuitID=CL.ClaimLawsuitID
LEFT JOIN tblSettlementGroup SG ON SG.SettlementGroupID=CSET.SettlementGroupID
LEFT JOIN tblPaymentTerms PT ON PT.PaymentTermsID=SG.PaymentTermsID
WHERE CL.ClaimLawsuitID = @ClaimLawsuitID AND IsPrimaryStatus=1
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryStatusChangeConfirmation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryStatusChangeConfirmation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryStatusChangeConfirmation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryStatusChangeConfirmation] TO [power_user]
GO
