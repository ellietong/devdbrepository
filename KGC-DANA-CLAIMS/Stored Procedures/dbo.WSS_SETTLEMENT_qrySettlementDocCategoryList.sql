SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_SETTLEMENT_qrySettlementDocCategoryList]
AS
SET NOCOUNT ON

SELECT DocCategoryID, 
	CASE WHEN DocCategoryID = 9 THEN 'MMSEA' ELSE DocCategory END AS DocCategory
FROM tblDocumentCategory
UNION
SELECT NULL, '-- Select a Category --'
ORDER BY DocCategory
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocCategoryList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocCategoryList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocCategoryList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocCategoryList] TO [power_user]
GO
