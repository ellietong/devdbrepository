SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_REPORTS_qryPendingMattersInactiveByDisease] (@MatterID int=null, @MonthID int, @YearID int)
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
     
DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)

--declare @PendingDate date
--set @PendingDate ='9/30/2011'

--Declare @Month int
--Declare @Year int
--set @Month=9
--set @Year=2011

Declare @Month int
Declare @Year int
Declare @PendingDate date
set @Month=@MonthID
set @Year=@YearID
set @PendingDate= DATEADD(MONTH,1,convert(date,convert(varchar,@Month)+'/1/'+convert(varchar,@Year)))


declare @FindClaimantsWithPostPendingDateStatusChange table (claimant int, claimlawsuitID int)
insert into @FindClaimantsWithPostPendingDateStatusChange
select c.ClaimantPersonalInfoID, cl.ClaimLawsuitID
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
where	StatusDate>=@PendingDate
		and ClaimLawsuitStatusID not in (29,21) 
		and IsPrimaryForClaim = 1 
		and  IsPrimaryStatus=1
		and IsCurrentClaim=1
		and MatterID>= COALESCE(@matterid, 0) AND MatterID <= COALESCE(@matterid, @maxMatterID)
group by c.ClaimantPersonalInfoID, cl.ClaimLawsuitID


declare @FindMaxStatusDateBeforePendingDate table(claimlawsuitID2 int, maxdate datetime)
insert into @FindMaxStatusDateBeforePendingDate
select cl.ClaimLawsuitID,  max(StatusDate)
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join @FindClaimantsWithPostPendingDateStatusChange ft on ft.claimlawsuitID=cl.ClaimLawsuitID
where StatusDate <@PendingDate
group by cl.ClaimLawsuitID

declare @CheckForOnlyOpenStatusBeforePendingDate table (claimlawsuitID3 int)
insert into @CheckForOnlyOpenStatusBeforePendingDate
select cl.ClaimLawsuitID
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimLawsuitStatus cls on cls.ClaimLawsuitStatusID=clst.ClaimLawsuitStatusID
inner join @FindMaxStatusDateBeforePendingDate fm on fm.claimlawsuitID2=Clst.ClaimLawsuitID and clst.StatusDate=fm.maxdate
where clst.ClaimLawsuitStatusID in (21,29)

declare @CheckForStatuscount table(claimlawsuitID4 int, statuscount int)
insert into @CheckForStatuscount
select cl.ClaimLawsuitID, count(StatusDate)
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join @FindClaimantsWithPostPendingDateStatusChange fcw on fcw.claimlawsuitID=cl.ClaimLawsuitID
group by cl.ClaimLawsuitID


declare @CombinedTable table (HighLevelDisease nvarchar(100), State nvarchar (100), DiseaseCount int)
insert into @CombinedTable

select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end AS HighLevelDisease, FullNamestate,
COUNT(distinct(cl.ClaimLawsuitID))
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimDisease cd on c.ClaimID=cd.ClaimID
left join tblDisease d on d.DiseaseID=cd.DiseaseID
left join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
left join tblHighLevelDisease hld on hld.HighLevelDiseaseID=d.HighLevelDiseaseID or hld.HighLevelDiseaseID=da.HighLevelDiseaseID
left join tblState s on s.StateID=l.StateID
where	(StatusDate<@PendingDate or StatusDate is null)
		and ClaimLawsuitStatusID in (21,29) 
		and IsPrimaryForClaim = 1 
		and  IsPrimaryStatus=1
		--and IsCurrentClaim=1
		and MatterID>= COALESCE(@matterid, 0) AND MatterID <= COALESCE(@matterid, @maxMatterID)
		and cd.PrimaryDisease=1
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, hld.HighLevelDiseaseID, FullNameState

union all

Select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end AS HighLevelDisease, FullNamestate,
COUNT(distinct(cl.ClaimLawsuitID))
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join @CheckForOnlyOpenStatusBeforePendingDate cf on cf.claimlawsuitID3=cl.ClaimLawsuitID
inner join tblClaimDisease cd on c.ClaimID=cd.ClaimID
left join tblDisease d on d.DiseaseID=cd.DiseaseID
left join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
left join tblHighLevelDisease hld on hld.HighLevelDiseaseID=d.HighLevelDiseaseID or hld.HighLevelDiseaseID=da.HighLevelDiseaseID
left join tblState s on s.StateID=l.StateID
where cd.PrimaryDisease=1
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, hld.HighLevelDiseaseID, FullNameState





--Groups all results into one table
select Top 10 ct.HighLevelDisease as HighLevelDisease, SUM(ct.DiseaseCount) as Count
from @CombinedTable ct
left join tblHighLevelDisease hld on hld.HighLevelDisease=ct.HighLevelDisease
group by ct.HighLevelDisease, HighLevelDiseaseID
Order by HighLevelDiseaseID



END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryPendingMattersInactiveByDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryPendingMattersInactiveByDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryPendingMattersInactiveByDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryPendingMattersInactiveByDisease] TO [power_user]
GO
