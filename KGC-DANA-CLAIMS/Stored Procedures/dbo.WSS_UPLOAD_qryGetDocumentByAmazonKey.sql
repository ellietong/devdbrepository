SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_UPLOAD_qryGetDocumentByAmazonKey]
	@Key nvarchar(4000)
AS
BEGIN
	SELECT COUNT(*) FROM tblDocuments WHERE AmazonKey LIKE '%' + (SELECT LEFT(@Key, (CHARINDEX('_', @Key)-1))) + '%'
END


GO
