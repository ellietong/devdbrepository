SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryHistoricalDocuments]
(@ClaimantPersonalInfoID bigint = null, @emailaddress nvarchar(250))
AS
BEGIN
--DECLARE @claimantpersonalinfoid bigint
--SET @ClaimantPersonalInfoID = 71363
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'mozart@bhplaw.com'

IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress) IN (132, 133)
BEGIN	

SELECT   cd.ClaimantDocumentID, ClaimantPersonalInfoID, 
                      cd.SPDocId, DocumentTitle, 
                      DocumentURL,
                      ClaimantDocumentType
FROM         tblClaimantDocuments cd 
			INNER JOIN tblHistoricalMultiClaimantDocuments hmcd ON cd.ClaimantDocumentID = hmcd.ClaimantDocumentID
            INNER JOIN dbo.tblClaimantDocumentType cdt ON cd.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
            INNER JOIN dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID
            WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND DocumentTitle IS NOT NULL
			AND dg.DocumentGroupID <> 2 AND cdt.ClaimantDocumentTypeID NOT IN (40)
END

ELSE
BEGIN

SELECT   cd.ClaimantDocumentID, ClaimantPersonalInfoID, 
                      cd.SPDocId, DocumentTitle, 
                      DocumentURL,
                      ClaimantDocumentType
FROM         tblClaimantDocuments cd 
			INNER JOIN tblHistoricalMultiClaimantDocuments hmcd ON cd.ClaimantDocumentID = hmcd.ClaimantDocumentID
            INNER JOIN dbo.tblClaimantDocumentType cdt ON cd.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
            INNER JOIN dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID
            WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND DocumentTitle IS NOT NULL
			AND dg.DocumentGroupID NOT IN (2, 4) AND cdt.ClaimantDocumentTypeID NOT IN (1, 40)
			
END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryHistoricalDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryHistoricalDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryHistoricalDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryHistoricalDocuments] TO [power_user]
GO
