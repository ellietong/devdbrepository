SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByDiseaseByMonth] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @MonthID int, @YearID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--Declare @Month int
--Set @Month=3
--Declare @Year int
--Set @Year=2003
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

DECLARE @Settlement table (HighLevelDisease nvarchar(25), Count int, Settlement money, AverageIndemnity money)
INSERT INTO @Settlement
select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end as HighLevelDisease, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(SettlementAmount) as IndemnityPaid, sum(SettlementAmount)/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimDisease cd on c.ClaimID=cd.ClaimID
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
left join tblDisease d on d.DiseaseID=cd.DiseaseID
left join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
left join tblHighLevelDisease hld on hld.HighLevelDiseaseID=d.HighLevelDiseaseID or hld.HighLevelDiseaseID=da.HighLevelDiseaseID
where	month(coalesce(SettlementDate,statusdate))=@month and YEAR(coalesce(SettlementDate,statusdate))=@year 
		and ClaimLawsuitStatusID in (23,24,25,28) 
		and IsPrimaryForClaim = 1 
		and  IsPrimaryStatus=1
		and cd.PrimaryDisease=1
		and MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, hld.HighLevelDiseaseID


select hld.HighLevelDisease, coalesce(Count,0) AS Count, coalesce(Settlement,0) As IndemnityPaid, coalesce(AverageIndemnity,0) AS AverageIndemnity
from tblHighLevelDisease hld
left join @Settlement s on hld.HighLevelDisease=s.HighLevelDisease
where HighLevelDiseaseID !=6
order by hld.HighLevelDiseaseID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByDiseaseByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByDiseaseByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByDiseaseByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByDiseaseByMonth] TO [power_user]
GO
