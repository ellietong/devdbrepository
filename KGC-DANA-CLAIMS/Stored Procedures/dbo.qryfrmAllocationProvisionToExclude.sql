SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmAllocationProvisionToExclude]
(@allocationID int = NULL)
AS
SET NOCOUNT ON

	SELECT ProvisionValueID, AllocationID, ProvisionLabelID
	FROM tblAllocationProvisionToExclude
	WHERE AllocationID = @allocationID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationProvisionToExclude] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationProvisionToExclude] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmAllocationProvisionToExclude] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmAllocationProvisionToExclude] TO [power_user]
GO
