SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Procedure [dbo].[qryrptPolicyReportsSplitGroupedIssues_QC]
(@policyid int = null)

As
set nocount on

--DECLARE @PolicyID int
--SET @policyid = 35

DECLARE @MaxPolicyId INT
SELECT @MaxPolicyId = MAX(policyID) from tblpolicy

SELECT
InsuranceProgram,  Min(PStartDate) AS Splitstart,
max(PEndDate) AS splitend, tblNamedInsured.NamedInsured, CarrierName, CarrierGroupName,
CarrierGroupID, CarrierSolvency, PolicyNum, p.Policyid, InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate,
PolicyNotes
FROM         vPolicy p LEFT JOIN
                      dbo.vProvisions A ON p.PolicyID = A.PolicyID LEFT JOIN
                      dbo.tblIssues ON dbo.tblIssues.PolicyID = p.PolicyID LEFT JOIN
                      dbo.tblNamedInsured ON dbo.tblNamedInsured.NamedInsuredID = p.NamedInsuredID
WHERE 
p.policyid >= COALESCE(@policyid, 0) AND p.policyid <= COALESCE(NULLIF(@policyid, 0), @MaxPolicyId)

GROUP BY  InsuranceProgram, 
tblNamedInsured.NamedInsured, CarrierName, CarrierGroupName,
CarrierGroupID, CarrierSolvency, PolicyNum, p.Policyid, InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate,
PolicyNotes
ORDER BY InsuranceProgram, SplitStart
return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplitGroupedIssues_QC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplitGroupedIssues_QC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplitGroupedIssues_QC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplitGroupedIssues_QC] TO [power_user]
GO
