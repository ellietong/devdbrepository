SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryFirmDocuments](@emailaddress nvarchar (1000)=null)
AS
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'sreich@dehay.com'
--Declare and set the temporary field used to check the user's firm based on their login email address
Declare @UserFirmState table (DefenseCounselID bigint, StateID int, DefenseCounsel nvarchar (250))
Insert into @UserFirmState

Select dca.DefenseCounselID, StateID, dca.DefenseCounsel
From dbo.tblDefenseCounselState AS dcs 
left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid 
LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
Where (ResponsibleDefenseCounselAddressID IN (SELECT DefenseCounselAddressID FROM vtblPerson p inner join tblDefenseCounselAddress da on p.FirmID=da.DefenseCounselID WHERE Email = @emailaddress))


DECLARE @firmid int
SET @firmid = (SELECT DISTINCT FirmID FROM  vtblPerson WHERE Email = @emailaddress)

IF (@firmid IN (132, 133) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11) = 'kcicllc.com'))
BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL AS DirectDocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, f.Firm, Accepted, '/cp/Dana/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/cp/Dana/Pages/Documents.aspx' AS DocumentURL
	FROM dbo.tblDocuments AS d
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		INNER JOIN vtblFirm AS f ON d.FirmID = f.FirmID
	WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40, 41, 42)
	ORDER BY Accepted ASC, DateCreated DESC
END 
--Allows Dehay, Cooper and Celba to see all documents for all firms except invoices and settlement related documents.
 IF (@firmid IN (10, 114, 100)) BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL AS DirectDocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, f.Firm, Accepted, '/cp/Dana/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/cp/Dana/Pages/Documents.aspx' AS DocumentURL
	FROM dbo.tblDocuments AS d
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		INNER JOIN vtblFirm AS f ON d.FirmID = f.FirmID
	WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40, 41, 42) AND cdt.DocumentGroupID <> 4
	ORDER BY Accepted ASC, DateCreated DESC
END ELSE BEGIN
	SELECT DISTINCT d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType, d.DocumentURL AS DirectDocumentURL, d.SPDocId, d.DateCreated, 
		d.AcceptedBy, d.DateAccepted, f.DefenseCounsel AS Firm, Accepted, '/cp/Dana/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/cp/Dana/Pages/Documents.aspx' AS DocumentURL
	FROM dbo.tblDocuments AS d
		INNER JOIN dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		INNER JOIN @UserFirmState AS f ON d.FirmID = f.DefenseCounselID
	WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40, 41, 42) AND cdt.DocumentGroupID <> 4
		AND f.DefenseCounselID = d.FirmID
	ORDER BY Accepted ASC, DateCreated DESC
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryFirmDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryFirmDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryFirmDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryFirmDocuments] TO [power_user]
GO
