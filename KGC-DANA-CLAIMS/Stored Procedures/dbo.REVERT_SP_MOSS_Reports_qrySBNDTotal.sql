SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_Reports_qrySBNDTotal]  (@matterID int=null, @MonthID int, @YearID int)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
--Declare @yearID int
--Set @YearId=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--declare @PendingDate date
--set @PendingDate ='10/1/2011'

--Declare @Month int
--Declare @Year int
--set @Month=9
--set @Year=2011

Declare @Month int
Declare @Year int
Declare @PendingDate date
set @Month=@MonthID
set @Year=@YearID
set @PendingDate= DATEADD(MONTH,1,convert(date,convert(varchar,@Month)+'/1/'+convert(varchar,@Year)))



select COUNT(cl.ClaimLawsuitID) as Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
left join tblState s on l.StateID=s.StateID
where statusDate<@PendingDate and ClaimLawsuitStatusID in (25) and IsPrimaryForClaim = 1 and  IsPrimaryStatus=1-- and IsCurrentClaim =1
		and MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(@matterid, @maxMatterID)
order by COUNT(cl.ClaimLawsuitID) desc


END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_Reports_qrySBNDTotal] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_Reports_qrySBNDTotal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_Reports_qrySBNDTotal] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_Reports_qrySBNDTotal] TO [power_user]
GO
