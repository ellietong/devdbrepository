SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryUpdDocument] (@DetailDocumentTypeID		int,					--aka ClaimantDocumentTypeID
											  @ClaimantPersonalInfoID	bigint = NULL,	
											  @Comments					nvarchar(4000) = NULL,	--comments are optional		
											  @DocumentURL				nvarchar(4000),
											  @DocumentTitle			nvarchar(4000),
											  @MatterID					int = NULL,				--this is the guid assigned by SharePoint
											  @DocID					int,
											  @FirmID					int,
											  --These are null unless the document is an Invoice document
											  @InvoiceNumber			nvarchar(50) = NULL,
											  @Period					nvarchar(50) = NULL,
											  @DatePayable				datetime = NULL,
											  @FeeAmount				money = NULL,
											  @DisbursementAmount		money = NULL,
											  @ModifiedBy				nvarchar(250),
											  @DateModified				datetime,
											  @Approved					bit = NULL,
											  @ApprovedBy				nvarchar(50) = NULL,
											  @DateApproved				datetime = NULL,
											  @DocumentSourceID			int,
											  @PrivateComments			nvarchar(4000),
											  @ExpertFirmID				int = NULL)
AS

UPDATE tblDocuments 
	SET DetailDocumentTypeID	= @DetailDocumentTypeID,	
		ClaimantPersonalInfoID	= @ClaimantPersonalInfoID,
		Comments				= @Comments,	
		DocumentTitle			= @DocumentTitle,
		InvoiceNumber			= @InvoiceNumber,
		Period					= @Period,
		DatePayable				= @DatePayable,
		FeeAmount				= @FeeAmount,
		DisbursementAmount		= @DisbursementAmount,
		ModifiedBy				= @ModifiedBy,
		DateModified			= @DateModified,
		DocumentSourceID		= @DocumentSourceID,
		PrivateComments         = @PrivateComments,
		MatterID				= @MatterID,
		FirmID					= @FirmID,
		ExpertFirmID			= @ExpertFirmID
	WHERE DocumentID = @DocID

SELECT DocumentID 
FROM tblDocuments 
WHERE DocumentID = @DocID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocument] TO [power_user]
GO
