SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryGetMedicareRequirements] 
@ClaimLawsuitID bigint = NULL,
@ClaimID bigint = NULL,
@LawsuitID bigint = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



IF @ClaimID IS NOT NULL AND @LawsuitID IS NOT NULL
BEGIN
SET @ClaimLawsuitID = (SELECT TOP 1 ClaimLawsuitID FROM tblClaimLawsuit WHERE ClaimID = @ClaimID AND LawsuitID = @LawsuitID)
END

IF object_id('tempdb..#DoesNeedAffidavit') IS NOT NULL
BEGIN
   DROP TABLE #DoesNeedAffidavit
END
CREATE TABLE #DoesNeedAffidavit (Req NVARCHAR(20), 
															PlaintiffCounselID INT,
															ClaimLawsuitID BIGINT,
															DocumentCategoryID INT)
INSERT INTO #DoesNeedAffidavit
SELECT DISTINCT 'AFFIDAVITCLAIMANT' AS Req, PlaintiffCounselID, cl.ClaimLawsuitID, 27 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	INNER JOIN (Select * FROM tblClaimantRelatedParty crp WHERE LegalCapacityID <> 19) crp ON c.ClaimantPersonalInfoID = crp.ClaimantPersonalInfoID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	AND PlaintiffCounselID IN (26)
	AND COALESCE(MedicareBeneficiaryStatusID, 0) <> 2
	AND DeceasedDate IS NULL	
UNION
SELECT DISTINCT 'AFFIDAVITRELATED' AS Req, PlaintiffCounselID, cl.ClaimLawsuitID, 27 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	AND PlaintiffCounselID IN (26)
	AND COALESCE(MedicareBeneficiaryStatusID, 0) <> 2
	AND DeceasedDate IS NOT NULL
UNION
SELECT DISTINCT 'AFFIDAVITSPOUSE' AS Req, PlaintiffCounselID, cl.ClaimLawsuitID, 27 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	INNER JOIN (Select * FROM tblClaimantRelatedParty crp WHERE LegalCapacityID = 19) crp ON c.ClaimantPersonalInfoID = crp.ClaimantPersonalInfoID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	AND PlaintiffCounselID IN (26)
	AND COALESCE(MedicareBeneficiaryStatusID, 0) <> 2
	AND DeceasedDate IS NULL


SELECT DISTINCT 'MMSEA' AS Req, PlaintiffCounselID, 9 AS DocumentCategoryID
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	--AND (PlaintiffCounselID IN (271, 519, 643, 952)
	--OR l.StateID NOT IN (45, 56))
	AND PlaintiffCounselID NOT IN (26, 150, 257, 294, 331, 586, 715)
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'FORMA1' AS Req, PlaintiffCounselID, 24 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	AND (PlaintiffCounselID IN (331, 586)
	OR (l.StateID IN (45, 56)
	AND PlaintiffCounselID NOT IN (26, 150, 257, 271, 294, 519, 643, 715, 952)))
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'FORMA2' AS Req, PlaintiffCounselID, 25 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	AND (PlaintiffCounselID IN (331, 586)
	OR (l.StateID IN (45, 56)
	AND PlaintiffCounselID NOT IN (26, 150, 257, 271, 294, 519, 643, 715, 952)))
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'FORMB' AS Req, PlaintiffCounselID, 13 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	AND (PlaintiffCounselID IN (26, 257, 294, 331, 586)
	OR (l.StateID IN (45, 56)
		AND PlaintiffCounselID NOT IN (150, 271, 519, 643, 715, 952)))
	AND PlaintiffCounselID IS NOT NULL
	AND cl.ClaimLawsuitID NOT IN (SELECT ClaimLawsuitID FROM #DoesNeedAffidavit)
UNION
SELECT DISTINCT 'FORMC1' AS Req, PlaintiffCounselID, 26 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	AND PlaintiffCounselID IN (331, 586) 
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'ROP' AS Req, PlaintiffCounselID, 10 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	--AND (PlaintiffCounselID IN (150, 271, 519, 643, 952)
	--OR l.StateID NOT IN (45, 56))
	AND PlaintiffCounselID NOT IN (26, 257, 294, 331, 586, 715)
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'ROPC' AS Req, PlaintiffCounselID, 11 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	--AND (PlaintiffCounselID IN (150)
	--OR l.StateID NOT IN (45, 56))
	AND PlaintiffCounselID NOT IN (26, 257, 271, 294, 331, 519, 586, 643, 715, 952)
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT Req, PlaintiffCounselID, DocumentCategoryID
FROM #DoesNeedAffidavit
ORDER BY Req, PlaintiffCounselID
END


GO
GRANT ALTER ON  [dbo].[WSS_SETTLEMENT_qryGetMedicareRequirements] TO [khanm]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryGetMedicareRequirements] TO [khanm]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryGetMedicareRequirements] TO [khanm]
GO
