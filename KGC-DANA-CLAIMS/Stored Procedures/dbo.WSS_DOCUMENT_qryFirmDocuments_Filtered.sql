SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_DOCUMENT_qryFirmDocuments_Filtered](@emailaddress nvarchar (1000)=null, @FilterDocumentTypeID int, @FilterFirmID int)
AS

SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @Emailaddress nvarchar(250)
--DECLARE @FilterDocumentTypeID int
--DECLARE @FilterFirmID int
--SET @emailaddress = 'scelba@celba.com'
--SET @FilterDocumentTypeID = null
--SET @FilterFirmID = null

DECLARE @DefenseCounselID int
SET @DefenseCounselID = (SELECT DISTINCT FirmID FROM  vtblPerson WHERE Email = @emailaddress)

DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE email = @emailaddress)

DECLARE @maxDocType int
SET @maxDocType = (SELECT MAX(ClaimantDocumentTypeID) FROM tblClaimantDocumentType)

DECLARE @MaxFirmID int
SET @MaxFirmID = (SELECT MAX(DefensecounselID) FROM tblDefenseCounsel)



--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE cdt.ClaimantDocumentTypeID NOT IN (1, 40)
					 AND ClaimantDocumentTypeID >= ' + CONVERT(nvarchar(20), COALESCE(@FilterDocumentTypeID, 0)) + ' AND ClaimantDocumentTypeID <= ' + CONVERT(nvarchar(20),COALESCE(@FilterDocumentTypeID, @maxDocType)) + '
					 AND d.FirmID >= ' + CONVERT(nvarchar(20),COALESCE(@FilterFirmID, 0)) + ' AND d.FirmID <= ' + CONVERT(nvarchar(20),COALESCE(@FilterFirmID, @maxFirmID)) + ' 
					 AND HistoricalDocumentID IS NULL 
					 AND COALESCE(IsPrimaryForClaim, 0) = 1 
					 AND COALESCE(IsCurrentClaim, 0) = 1'


--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF (@DefenseCounselID IN (132, 133)) OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com')
	SET @JoinString = ' '
ELSE IF (@DefenseCounselID IN (10, 114) OR @emailaddress = 'scelba@celba.com')
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND cdt.DocumentGroupID <> 4'
END
ELSE
BEGIN
	SET @JoinString = ' '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),COALESCE(@DefenseCounselAddressID,0)) + ')
										AND cdt.DocumentGroupID <> 4'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT d.DocumentID, 
		d.DocumentTitle, 
		cdt.ClaimantDocumentType, 
		d.DocumentURL AS DirectDocumentURL, 
		d.DateCreated, 
		d.AcceptedBy, 
		d.DateAccepted, 
		dca.DefenseCounsel + '' ('' + DefenseCounselCity + '')'' AS Firm, 
		Accepted,
		''/cp/Dana/Pages/DocumentProfile.aspx?ItemId='' + CONVERT(nvarchar(100), d.DocumentID) + ''&source='' + ''/cp/Dana/Pages/Documents.aspx'' AS DocumentURL		
	 FROM dbo.tblDocuments d 
		INNER JOIN tblMultiClaimantDocument mcd		ON d.DocumentID = mcd.DocumentID
		INNER JOIN dbo.tblClaimantDocumentType cdt	ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
		LEFT  JOIN tblClaim c						ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
		LEFT  JOIN tblClaimLawsuit cl				ON c.ClaimID = cl.ClaimID
		LEFT  JOIN tblLawsuit l						ON cl.LawsuitID = l.LawsuitID
		LEFT JOIN tblMatter m					 ON c.MatterID = m.MatterID
		LEFT  JOIN tblDefenseJurisdiction dj		ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND dj.MatterEntityID = m.MatterEntityID
		LEFT  JOIN tblDefenseCounselAddress dca		ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID ' +
		@JoinString +
     @WhereString + '
	 ORDER BY Accepted ASC, DateCreated DESC
	 OPTION (RECOMPILE)'
	 
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryFirmDocuments_Filtered] TO [power_user]
GO
