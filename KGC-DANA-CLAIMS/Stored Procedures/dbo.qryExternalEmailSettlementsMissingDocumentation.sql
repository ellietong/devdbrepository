SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[qryExternalEmailSettlementsMissingDocumentation]
AS
BEGIN


/*Query run daily to send a separate alert email to the primary contacts for each plaintiff firm notifying them of any settled claimants missing documentation. - SDO*/

--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)

DECLARE @SettlementDate datetime
SET @SettlementDate = '4/1/2011'


SET @Subject =
'Dana Companies, LLC: Settlements Missing Documentation ' + CONVERT(varchar, GETDATE(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for recently settled claimants.
DECLARE @OutstandingSettlements TABLE (ClaimantName nvarchar (250),
ClaimantPersonalInfoID int,
ClaimLawsuitID int,
PlaintiffCounsel nvarchar (500),
PlaintiffCounselAddressID int,
SettlementDate date)
INSERT INTO @OutstandingSettlements
	SELECT
		'<a href="https://www.kcicextranet.com/pp/dana/Pages/PlaintiffSettlementDetails.aspx?ClaimLawsuitID=' + COALESCE(CONVERT(nvarchar(50), c.ClaimantPersonalInfoID), '') + '">' + cpi.LastName + ', ' + FirstName + COALESCE(' ' + MiddleName, '') + '</a>' AS ClaimantName,
		c.ClaimantPersonalInfoID,
		cl.ClaimLawsuitID,
		PlaintiffCounsel,
		l.PlaintiffCounselAddressID,
		SettlementDate
	FROM tblClaim c
	LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	LEFT JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
	LEFT JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatus cst ON cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
	LEFT JOIN tblSettlementGroup s ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT JOIN tblClaimantDocketNumbers cd ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
	LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID
	LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	WHERE COALESCE(IsCurrentClaim, 0) = 1 AND COALESCE(clst.isprimarystatus, 0) = 1
	AND clst.ClaimLawsuitStatusID = 23
	AND StatusDate >= @SettlementDate
	AND NOT EXISTS (SELECT
		PaymentSettlementID
	FROM tblPaymentSettlement ps
	WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
	AND cs.SettlementDate + 30 = GETDATE()
	GROUP BY	cpi.LastName,
				FirstName,
				MiddleName,
				cl.ClaimLawsuitID,
				c.ClaimantPersonalInfoID,
				cl.LawsuitID,
				PlaintiffCounsel,
				l.PlaintiffCounselAddressID,
				SettlementDate,
				SettlementGroup,
				cs.SettlementGroupID
	ORDER BY SettlementDate DESC,
	SettlementGroup,
	cpi.LastName + ', ' + FirstName + COALESCE(' ' + MiddleName, '')

DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar (max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar (max), @ClaimantID bigint, @ClaimantList nvarchar (max), @DateSettled date
DECLARE @LastClaimLawsuitID int = NULL

--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.
DECLARE @PlaintiffCounselAddressID nvarchar (10), @PlaintiffPortal nvarchar (100)

DECLARE firm_Curs CURSOR FOR SELECT
	PlaintiffCounselAddressID,
	'https://www.kcicextranet.com/pp/dana/Pages/Plaintiff%27sPortal.aspx'
FROM @OutstandingSettlements nct
GROUP BY PlaintiffCounselAddressID

OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @PlaintiffCounselAddressID, @PlaintiffPortal
WHILE @@FETCH_STATUS = 0
BEGIN

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
	
border: 1px solid #38160C;
	
color: #F6ECF0;}
TD	
{font-family: Goudy Old Style; 
	
font-size: 10pt; 
	
text-align: center;
	
border: 1px solid rgb(0,121,193);
	
padding: .3em;
	
color: #000000;
	
vertical-align: top;
 
width: 190px;}
TH	
{border: 1px solid #ffffff;
	
font-size: 10pt; 
	
padding: .3em;
	
color: #F6ECF0;
	
background: rgb(0,121,193);}
UL {margin:0; 
	
padding:0;}	
 
 
--->
</STYLE>'




DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--Set table column headings and populate the cells with the Claimants from @OutstandingSettlements

IF (SELECT
	COUNT(ClaimantName)
FROM @OutstandingSettlements
WHERE PlaintiffCounselAddressID = @PlaintiffCounselAddressID) > 0
begin
DECLARE @ClaimLawsuitID int

SET @BodyMessage = @BodyMessage + '<H4>The following claimants were settled 30 days ago and are missing settlement documentation that is required for payment: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Claimant</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Settlement Date </th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193); width: 195px;">Missing Documents</th>'


DECLARE Claimant_Curs CURSOR FOR SELECT DISTINCT
	ClaimantName,
	SettlementDate,
	ClaimLawsuitID
FROM @OutstandingSettlements
WHERE PlaintiffCounselAddressID = @PlaintiffCounselAddressID


OPEN Claimant_Curs
FETCH NEXT FROM Claimant_Curs INTO @Claimant, @DateSettled, @ClaimLawsuitID
WHILE @@FETCH_STATUS = 0
BEGIN

DECLARE @MissingDocsTable nvarchar (max) = ' '
EXEC [dbo].[qryExternalMissingDocsByClaimLawsuit]	@MissingDocsTable OUTPUT,
													@ClaimLawsuitID,
													@PlaintiffCounselAddressID

IF (@MissingDocsTable IS NULL)
BEGIN
FETCH NEXT FROM Claimant_Curs INTO @Claimant, @DateSettled, @ClaimLawsuitID
END
ELSE
BEGIN

SET @BodyMessage = @BodyMessage +

CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		@Claimant)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		@DateSettled)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		@MissingDocsTable)
	AS td,
	''
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX))

FETCH NEXT FROM Claimant_Curs INTO @Claimant, @DateSettled, @ClaimLawsuitID
END
END
CLOSE Claimant_Curs
DEALLOCATE Claimant_Curs


SET @bodyMessage = @BodyMessage + N'</table>' + N'<br/>'
end


--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT
	@PlaintiffPortal)
+ '>here</A> to access the KCIC Extranet to resolve the deficiencies.</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

DECLARE @RecipientList nvarchar (500)
SET @RecipientList = ''

--Declare a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar (500)
DECLARE contact_curs CURSOR
LOCAL FORWARD_ONLY STATIC READ_ONLY
FOR SELECT
	email
FROM vtblPerson p
WHERE PlaintiffCounselAddressID = @PlaintiffCounselAddressID


OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS = 0
BEGIN

SET @RecipientList = @RecipientList + @Contact + '; '

FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs


--QC Recipient List
--Set @Bodymessage = @BodyMessage + '<br>Recipients: ' + @RecipientList

IF (SELECT
	COUNT(PlaintiffCounselAddressID)
FROM @OutstandingSettlements
WHERE PlaintiffCounselAddressID = @PlaintiffCounselAddressID) > 0
BEGIN
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail --The Claims sends from Dana@kcic.com
@profile_name = 'Claims',
@recipients = @RecipientList,
@blind_copy_recipients = 'dana@kcic.com; prossc@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
END

--Move to the next firm to create a new email
FETCH NEXT FROM firm_curs INTO @PlaintiffCounselAddressID, @PlaintiffPortal
END
CLOSE firm_curs
DEALLOCATE firm_curs

END

GO
