SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptExpenseClaimListing] (@AllocationID int)

AS
Set NoCount On


--Declare @AllocationID int
--Set @AllocationID=50


DECLARE @AllocationDetails nvarchar(200)
SET @AllocationDetails = (Select Convert(nvarchar(50),[Month]) + ' ' + Convert(nvarchar(50),[Year]) + ' - ' + Convert(nvarchar(50),BillNumber)
FROM tblallocation
Where AllocationID=@AllocationID)

DECLARE @TotalAllocatedExpense money
SET @TotalAllocatedExpense = (SELECT SUM(Cost1)
FROM CLIENTSQL.[KGC-DanaWellington].[dbo].[tbloccurrence] o
INNER JOIN CLIENTSQL.[KGC-DanaWellington].[dbo].[tblAllocationOccurrenceGroupInclude] aog on aog.OccurrenceGroupID=o.OccurrenceGroupID
where AllocationID=@AllocationID)

SELECT de.[Start] as StartDate, de.[End] as EndDate, de.Percentage, @TotalAllocatedExpense AS TotalAllocatedExpense, (@TotalAllocatedExpense* de.Percentage) AS AllocatedExpense, @AllocationDetails as AllocationDetails
FROM CLIENTSQL.[KGC-DanaWellington].[dbo].[m_tblExpenseModelJanuary2012] de
ORDER BY de.Start DESC
--Return

--GO
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptExpenseClaimListing] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptExpenseClaimListing] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptExpenseClaimListing] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptExpenseClaimListing] TO [power_user]
GO
