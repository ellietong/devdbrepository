SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByStateByMonth] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @MonthID int, @YearID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--Declare @MonthID int
--Set @MonthID=11
--Declare @YearID int
--Set @YearId=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

select FullNameState, COUNT(distinct(cl.ClaimLawsuitID)) as Count, sum(SettlementAmount) as IndemnityPaid, sum(SettlementAmount)/COUNT(distinct(cl.ClaimLawsuitID)) as AverageIndemnity
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimantSettlement cs on cs.ClaimLawsuitID=cl.ClaimLawsuitID
left join tblState s on l.StateID=s.StateID
where	month(coalesce(SettlementDate,statusdate))=@month and YEAR(coalesce(SettlementDate,statusdate))=@year 
		and ClaimLawsuitStatusID in (23,24,25,28)  
		and IsPrimaryForClaim = 1 
		and  IsPrimaryStatus=1
		and MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
group by FullNameState
order by COUNT(distinct(cl.ClaimLawsuitID)) desc  


END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByStateByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByStateByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByStateByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySettlementsByStateByMonth] TO [power_user]
GO
