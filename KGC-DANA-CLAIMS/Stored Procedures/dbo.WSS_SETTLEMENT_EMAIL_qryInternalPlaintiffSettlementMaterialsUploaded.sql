SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_EMAIL_qryInternalPlaintiffSettlementMaterialsUploaded] 
AS
BEGIN


/*Query run daily to send an internal email for new documents uploaded by plaintiff counsel contacts that day. - CP*/

--DECLARE and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @LogoHeader nvarchar(2000)

DECLARE @Today4PM datetime
DECLARE @Yesterday4PM datetime
SET @Yesterday4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()-1))+' 16:00:00.000')
SET @Today4PM = convert(datetime,convert(nvarchar,convert(date, GETDATE()))+' 16:00:00.000')


SET @Subject = 	'Dana Companies, LLC: New Settlement Documents Uploaded ' + CONVERT(varchar, getdate(), 107)
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'

--Create a temp table for settlement documents which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.

DECLARE @NewSettlementDocs TABLE (DocID int, DocType nvarchar(500), docTitle nvarchar(500), FirmID int, lName nvarchar(50), 
fName nvarchar(50), ClaimantPersonalInfoID bigint)
INSERT INTO @NewSettlementDocs
	SELECT d.DocumentID, 
			ClaimantDocumentType, 
			'<a href="admin.kcicextranet.com/clients/dana/cp/pages/claimantdetails.aspx?ItemId='+COALESCE(CONVERT(nvarchar(50), d.DocumentID), '') + '">' + COALESCE(d.DocumentTitle, '') + '</a>' AS DocumentTitle, 
			COALESCE(d.FirmID, 0),
			cpi.LastName, 
			cpi.FirstName, 
			cpi.ClaimantPersonalInfoID	

	FROM tblDocuments d
		INNER JOIN tblClaimantDocumentType cdt  ON cdt.ClaimantDocumentTypeID=d.DetailDocumentTypeID
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimantPersonalInfo cpi  ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
		LEFT  JOIN tblclaim c					ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT  JOIN tblclaimlawsuit cl			ON cl.claimid = c.claimid
		LEFT  JOIN tbllawsuit l					ON l.lawsuitid =cl.lawsuitid
		INNER JOIN vtblPerson p					ON p.Email=d.CreatedBy
	WHERE d.DateCreated BETWEEN @Yesterday4PM AND @Today4PM
		--AND detailDocumentTypeID IN (2,4,10,12,18,24,31,32,39,52)
		--Specify that the document was created by a plaintiff counsel contact
		--AND d.FirmID IN (SELECT FirmID FROM vtblPerson WHERE PlaintiffCounselAddressID is not null)	
		AND p.PlaintiffCounselAddressID is not null
		--AND d.FirmID IS NOT NULL
		AND IsPrimaryForClaim = 1
		AND IsCurrentClaim = 1	
	GROUP BY d.documentID, ClaimantDocumentType, d.DocumentTitle,
	d.FirmID, LastName, FirstName, cpi.ClaimantPersonalInfoID
	ORDER BY d.FirmID, cpi.ClaimantPersonalInfoID

DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar(max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar(max), @ClaimantID bigint, @ClaimantList nvarchar(max)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for
SELECT DocID, COALESCE(Lname, '') + ', ' +  COALESCE(Fname, ''), ClaimantpersonalInfoID FROM @NewSettlementDocs
--
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
WHILE @@FETCH_STATUS = 0
BEGIN
IF(LEN(@ClaimantList)=0 OR @ClaimantList IS NULL)
BEGIN
SET @ClaimantList = '<ul> <a href="https://admin.kcicextranet.com/clients/dana/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+'">'  + @Claimant + coalesce(' ('+ CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
END
ELSE
BEGIN
SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul> <a href="https://admin.kcicextranet.com/clients/dana/cp/pages/claimantdetails.aspx?ClaimantPersonalInfoID=' +CONVERT(nvarchar(50), @claimantID)+'">'  + @Claimant + coalesce(' ('+ CONVERT(nvarchar(50), @claimantID) + ')', '') + '</a></ul>'
END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
PRINT @@FETCH_STATUS
IF @DocID <> @LastDocID OR @@FETCH_STATUS <> 0
BEGIN
INSERT INTO @ClaimantPerDoc
SELECT @LastDocID, @ClaimantList
SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints

--DECLARE and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader + 
	'<STYLE TYPE="text/css">
	<!--
	TABLE {border-collapse: collapse;
	border: 1px solid #38160C;
	color: #F6ECF0;}
	TD	
	{font-size: 10pt; 
	text-align: center;
	border: 1px solid rgb(0,121,193);
	padding: .3em;
	color: #000000;
	vertical-align: top;}
	TH	
	{border: 1px solid #ffffff;
	font-size: 10pt; 
	padding: .3em;
	color: #F6ECF0;
	background: rgb(0,121,193);}
	UL {margin:0; 
	padding:0;}	
	--->
	</STYLE>'

DECLARE @TDStyle nvarchar(max) = 'font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable

IF (SELECT COUNT(docTitle) FROM @NewSettlementDocs --where FirmID = @firmid
) >0 
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>The following settlement documents have been uploaded by plaintiff counsel in the last 24 hours: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style = "margin-left: 3em; border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimant (ClaimantID)</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th>' +
CAST ( (
SELECT  @TDStyle AS 'td/@style', (SELECT ClaimantList) AS td, '',
		@TDStyle AS 'td/@style', (SELECT DocType) AS td, ''
FROM @NewSettlementDocs C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
--WHERE SettlementGroupID = (select @SettlementGroupID)
GROUP BY cl.ClaimantList, DocType
for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
N'</table>' +  N'<br/>'	

END
ELSE
BEGIN
SET @BodyMessage = @BodyMessage + '<H4>No new settlement documents have been uploaded in the last 24 hours. </H4>'
END

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

----Link for document library
--SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT @firmURL) + '>here</A> to access the document(s).</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'
  
--Execute stored procedure to send the email to that firm
BEGIN
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The DanaProfile sends from Dana@kcicllc.com
@profile_name='Claims',
@recipients = 'dana@kcic.com',
@blind_copy_recipients = 'prossc@kcic.com; khanm@kcic.com; mollardj@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_EMAIL_qryInternalPlaintiffSettlementMaterialsUploaded] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_EMAIL_qryInternalPlaintiffSettlementMaterialsUploaded] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_EMAIL_qryInternalPlaintiffSettlementMaterialsUploaded] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_EMAIL_qryInternalPlaintiffSettlementMaterialsUploaded] TO [power_user]
GO
