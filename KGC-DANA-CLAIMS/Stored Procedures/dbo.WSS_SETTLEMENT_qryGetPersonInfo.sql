SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryGetPersonInfo]
@PersonID int
	-- Add the parameters for the stored procedure here

AS	
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT PrimaryContact AS PrimaryContact
	FROM vtblPerson
	WHERE PersonID = @PersonID 

END

GO
