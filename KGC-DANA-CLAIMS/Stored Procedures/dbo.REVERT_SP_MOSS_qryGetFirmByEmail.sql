SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryGetFirmByEmail]


	
-- Add the parameters for the stored procedure here
	
@emailaddress nvarchar(1000)
AS
DECLARE @firmID int
SET @firmID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)
BEGIN
	
DECLARE @emaildomain nvarchar(1000)
	
SET @emaildomain = SUBSTRING(@emailaddress, CHARINDEX('@', @emailaddress), LEN(@emailaddress)-CHARINDEX('@', @emailaddress)+1)
	
IF @emaildomain = '@kcicllc.com' OR @firmID = 132
	
BEGIN
	
	
SELECT DISTINCT DefenseCounselID as FirmID, DefenseCounsel as Firm 
	
	
FROM tblDefenseCounsel 
	
	
WHERE DefenseCounselID NOT IN (134, 135, 136,268)
	
	
ORDER BY DefenseCounsel
	
END
	
ELSE
	
BEGIN
	
	
SELECT DISTINCT FirmID, DefenseCounsel as Firm FROM vtblPerson 
	
	
INNER JOIN tblDefenseCounsel ON vtblPerson.FirmID = tblDefenseCounsel.DefenseCounselID
	
	
WHERE @emailaddress = vtblPerson.Email
	
	
ORDER BY DefenseCounsel
	
END
	
	

	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryGetFirmByEmail] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryGetFirmByEmail] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryGetFirmByEmail] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryGetFirmByEmail] TO [power_user]
GO
