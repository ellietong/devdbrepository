SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptClaimantDocumentsQC] (@ClaimantPersonalInfoID int)

as
--DECLARE @ClaimantPersonalInfOID bigint = 88230

Select d.DocumentID as ClaimantDocumentID,
mcd.ClaimantPersonalInfoID, 
ClaimantDocumentType,
COALESCE(AmazonKey,DocumentURL) as DocumentLink, 
COALESCE(DefenseCounsel, 'Internal/Historical') as DefenseCounsel,
DateAccepted,
ds.DocumentSource,
Comments
from tblDocuments d
INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
left join tblClaimantDocumentType dt on dt.ClaimantDocumentTypeID = d.DetailDocumentTypeID 
left join tblDefenseCounsel dc on dc.DefenseCounselID = d.FirmID
left join tblDocumentSource ds on ds.DocumentSourceID = d.DocumentSourceID 
where mcd.ClaimantPersonalInfoID = @claimantpersonalInfoID


Order By ClaimantDocumentType

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimantDocumentsQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimantDocumentsQC] TO [power_user]
GO
