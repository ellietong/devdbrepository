SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qrySettlementDocumentation] (@ClaimLawsuitID int)
AS

--DECLARE @ClaimID int
--SET @ClaimID = 1

SELECT pd.SetPymtID, 
	COALESCE(sd.ClaimantIDed, 0) AS ClaimantIDed, 
	COALESCE(sd.DODCert, 0) AS DODCert, 
	COALESCE(sd.DxMedicals, 0) AS DxMedicals,  
	COALESCE(sd.EmployExpoHist,0) AS EmployExpoHist,  
	COALESCE(sd.EstatePapers,0) AS EstatePapers, 
	COALESCE(sd.productid,0) AS ProductID, 
	COALESCE(sd.SettlementConfirmation, 0) AS SettlementConfirmation, 
	pd.ExecutedReleaseDate, 
	pd.ReleaseRqstDate,
	pd.ReleaseToPC, 
	COALESCE(rt.ReleaseType, '') as ReleaseType, 
	cpi.MedicareBeneficiaryStatusID, 
	MedicareBeneficiaryStatus,
	COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS MMSEA, 
	COALESCE(sd.MedicarePlaintiff, 0) AS MedicarePlaintiff, 
	COALESCE(sd.MedicarePlaintiffCounsel, 0) AS MedicarePlaintiffCounsel, 
	DeceasedDate, 
	SettlePaymentComments,
	COALESCE(MedicareReported, 0) AS MedicareReported, 
	pd.ReleaseTypeID,
	COALESCE(NULLIF(MedicareFormB, 0), sd.MMSEA, 0) AS MedicareFormB,
	COALESCE(MedicareFormA1, 0) AS MedicareFormA1,
	COALESCE(MedicareFormA2, 0) AS MedicareFormA2,
	COALESCE(MedicareFormC1, 0) AS MedicareFormC1,
	COALESCE(MedicareAffidavit, 0) AS MedicareAffidavit
FROM tblClaimLawsuit cl
	LEFT JOIN tblSettlePaymentDocumentation pd 	ON cl.ClaimLawsuitID = pd.ClaimLawsuitID
	LEFT JOIN tblSettlementDocumentation sd		ON sd.setpymtID=PD.SetPymtID
	LEFT JOIN tblReleaseType rt					ON pd.ReleaseTypeID = rt.ReleaseTypeID
	LEFT JOIN tblClaim c						ON cl.ClaimID = c.ClaimID
	LEFT JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblMedicareBeneficiaryStatus mbs	ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettlementDocumentation] TO [power_user]
GO
