SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_qryPlaintiffSettlementDocCategoryList]
AS
SET NOCOUNT ON

SELECT DocCategoryID, 
	CASE WHEN DocCategoryID = 9 THEN 'MMSEA' ELSE DocCategory END AS DocCategory 
FROM tblDocumentCategory
WHERE DocCategoryID IN (3, 4, 9, 10, 11, 12, 13, 24, 25, 26)
UNION
SELECT NULL, '-- Select a Category --'
ORDER BY DocCategory
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffSettlementDocCategoryList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffSettlementDocCategoryList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffSettlementDocCategoryList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffSettlementDocCategoryList] TO [power_user]
GO
