SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup]
AS
SET NOCOUNT ON

SELECT StatusID, [Status]
FROM tblDefenseInvoiceStatus
WHERE StatusID <> 3
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryDefenseInvoiceLookup] TO [power_user]
GO
