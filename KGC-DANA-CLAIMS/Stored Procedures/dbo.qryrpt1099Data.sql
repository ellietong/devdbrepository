SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE Proc [dbo].[qryrpt1099Data]
as
--SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
SET NOCOUNT ON
--This query is used to populate the 1099 forms for each tax year.
Declare @TaxYear int
Set @TaxYear = 2011

Set nocount on


Declare @CombinedTable table (PayeeID nvarchar(50), Payee nvarchar(200), TaxID nvarchar(50), Address1 nvarchar(200), Address2 nvarchar(200), City nvarchar(200), ZipCode nvarchar(10), State nvarchar(10), Box14 money, Box7 money)
insert @CombinedTable

SELECT 'V'+cast(coalesce(EF.ExpertFirmID,dca.defensecounselID) as NCHAR) as PayeeID, Coalesce(ExpertFirm, DefenseCounsel) as Payee, coalesce(EF.taxID,F.taxID) as TaxID, coalesce(Address1,DefenseCounselAddress1) as Address1, coalesce(Address2,DefenseCounselAddress2) as Address2,
coalesce(City,DefenseCounselCity) as City, coalesce(ZipCode, DefenseCounselZip) as ZipCode, coalesce(s.State,dca.State) as State, 0 as Box14, sum(InvoiceTotal) as Box7
FROM tblCheck C
INNER JOIN tblPaymentDefense PD ON PD.CheckID = C.CheckID
INNER JOIN tblDefenseInvoice DI ON DI.DefenseInvoiceID = PD.DefenseInvoiceID
LEFT JOIN m_tblExpertFirm EF ON EF.ExpertFirmID = DI.ExpertFirmID
left join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
left join tblState s on s.StateID = EF.StateID
left join sharesql.fbsecurity.dbo.tblFirmAddress f on di.DefenseCounselAddressID = f.FirmAddressID
WHERE YEAR(SourceDate) = @taxyear and di.DefenseCounselAddressID not in (84,90,69,63,61,4,68,35,188,72,107) and DI.ExpertFirmID not in (9)-- AND FundingTypeID = 3
GROUP BY coalesce(EF.ExpertFirmID,dca.defensecounselID),Coalesce(ExpertFirm, DefenseCounsel),coalesce(EF.taxID,F.taxID), coalesce(Address1,DefenseCounselAddress1), coalesce(Address2,DefenseCounselAddress2), coalesce(City,DefenseCounselCity),coalesce(ZipCode, DefenseCounselZip), coalesce(s.State,dca.State)
--ORDER BY Payee

Union 

--select * from m_tblLegalBillInvoices
--select * from sharesql.fbsecurity.dbo.tblFirmAddress

SELECT 'D'+ cast(f.firmid AS NCHAR), F.Firm, FA.TaxID as taxID, FirmAddress1, FirmAddress2, FirmCity, firmZip + Case when firmmailzip is null then '' else '-'+FirmMailZip End as Zip, State, 0 as Box14, sum(case when AppealFlag=1 then AppealTotal else AdjustedTotal end) as Box7
FROM tblCheck C
INNER JOIN tblPaymentLegalBill PLB ON PLB.CheckID = C.CheckID
INNER JOIN m_tblLegalBillInvoices LBI ON PLB.InvoiceID = LBI.InvoiceID
left join sharesql.fbsecurity.dbo.tblFirmAddress FA on LBI.DefenseCounselAddressID = FA.FirmAddressID
LEFT JOIN vtblFirm F ON F.FirmID = FA.FirmID
left join tblState s on s.StateID = FA.firmstateID
WHERE YEAR(SourceDate) = @TaxYear and LBI.DefenseCounselAddressID not in (63,4,68,35,188,72,107) --and payeeaddress=1--AND FundingTypeID = 1 and payeeaddress = 1
GROUP BY firmaddressid,f.firmid, F.Firm, FA.TaxID, FirmAddress1, FirmAddress2, FirmCity, firmZip + Case when firmmailzip is null then '' else '-'+FirmMailZip End , State, payeeaddress
--ORDER BY Firm

Union

SELECT 'P'+cast(pca.PlaintiffCounselID as NCHAR), PC.PlaintiffCounsel,PC.PlaintiffCounselTaxID, pca.PlaintiffCounselAddress1, pca.PlaintiffCounselAddress2, 
pca.PlaintiffCounselCity,pca.PlaintiffCounselZip, S.State, sum(PaymentAmount) as Box14, 0 as Box7
FROM tblCheck C
INNER JOIN tblPaymentSettlement PS ON PS.CheckID = C.CheckID
INNER JOIN tblClaimantSettlement CS ON CS.ClaimantSettlementID = PS.ClaimantSettlementID
INNER JOIN tblClaimLawsuit CL ON CL.ClaimLawsuitID = CS.ClaimLawsuitID
INNER JOIN tblLawsuit L ON L.LawsuitID = CL.LawsuitID
LEFT JOIN tblPlaintiffCounselAddress_ALT PCA ON PCA.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
LEFT JOIN tblPlaintiffCounsel_ALT PC on PCA.PlaintiffCounselID=PC.PlaintiffCounselID
LEFT JOIN tblState S on S.StateID=PCA.PlaintiffCounselStateID
WHERE YEAR(SourceDate) = @TaxYear AND payeeAddress = 1
GROUP BY pca.PlaintiffCounselID, PC.PlaintiffCounsel,PC.PlaintiffCounselTaxID, pca.PlaintiffCounselAddress1, pca.PlaintiffCounselAddress2, 
pca.PlaintiffCounselCity,pca.PlaintiffCounselZip, State
--Order by PlaintiffCounsel

Union

SELECT 
case when PACE.ExpertFirmID != 0 then 'V'+cast(EF.ExpertFirmID as NCHAR)
when PACE.DefenseCounselAddressID !=0 then 'D'+CAST(F.FirmID as NCHAR)
when PACE.PlaintiffCounselAddressID !=0 then 'P'+CAST(PCA.PlaintiffCounselID as NCHAR) END as PayeeID,

case when PACE.ExpertFirmID != 0 then ExpertFirm
when PACE.DefenseCounselAddressID !=0 then F.Firm
when PACE.PlaintiffCounselAddressID !=0 then PC.PlaintiffCounsel END as PAYEE,

case when PACE.ExpertFirmID != 0 then EF.TaxID
when PACE.DefenseCounselAddressID !=0 then FA.TaxID
when PACE.PlaintiffCounselAddressID !=0 then PC.PlaintiffCounselTaxID END as TaxID,

case when PACE.ExpertFirmID != 0 then Address1
when PACE.DefenseCounselAddressID !=0 then FirmAddress1
when PACE.PlaintiffCounselAddressID !=0 then PCA.PlaintiffCounselAddress1 END as Address1,

case when PACE.ExpertFirmID != 0 then Address2
when PACE.DefenseCounselAddressID !=0 then FirmAddress2
when PACE.PlaintiffCounselAddressID !=0 then PCA.PlaintiffCounselAddress2 END as Address2,

case when PACE.ExpertFirmID != 0 then City
when PACE.DefenseCounselAddressID !=0 then FirmCity
when PACE.PlaintiffCounselAddressID !=0 then PCA.PlaintiffCounselCity END as City,

case when PACE.ExpertFirmID != 0 then ZipCode
when PACE.DefenseCounselAddressID !=0 then firmZip + Case when firmmailzip is null then '' else '-'+FirmMailZip End
when PACE.PlaintiffCounselAddressID !=0 then PCA.PlaintiffCounselZip END as ZipCode,

case when PACE.ExpertFirmID != 0 then s.State
when PACE.DefenseCounselAddressID !=0 then Std.State
when PACE.PlaintiffCounselAddressID !=0 then st.State END as State,

case when PACE.ExpertFirmID != 0 then 0
when PACE.DefenseCounselAddressID !=0 then 0
when PACE.PlaintiffCounselAddressID !=0 then SUM(PACE.InvoiceTotal) END as Box14,

case when PACE.ExpertFirmID != 0 then SUM(PACE.InvoiceTotal)
when PACE.DefenseCounselAddressID !=0 then SUM(PACE.InvoiceTotal)
when PACE.PlaintiffCounselAddressID !=0 then 0 END as Box7

FROM m_tblPACE2011Payments PACE
LEFT JOIN m_tblExpertFirm EF ON EF.ExpertFirmID = PACE.ExpertFirmID
left join sharesql.fbsecurity.dbo.tblFirmAddress FA on PACE.DefenseCounselAddressID = FA.FirmAddressID
LEFT JOIN vtblFirm F ON F.FirmID = FA.FirmID
LEFT JOIN tblPlaintiffCounselAddress_ALT PCA ON PCA.PlaintiffCounselAddressID = PACE.PlaintiffCounselAddressID
LEFT JOIN tblPlaintiffCounsel_ALT PC on PCA.PlaintiffCounselID=PC.PlaintiffCounselID
left join tblState s on s.StateID = EF.StateID
left join tblState st on st.StateID = pca.PlaintiffCounselStateID
left join tblState std on std.StateID = FA.FirmStateID
where PACE.ExpertFirmID not in (9)
GROUP BY PACE.ExpertFirmID, PACE.DefenseCounselAddressID, PACE.PlaintiffCounselAddressID,EF.ExpertFirmID, F.FirmID, PCA.PlaintiffCounselID, ExpertFirm, F.Firm, PC.PlaintiffCounsel, EF.TaxID, FA.TaxID, PC.PlaintiffCounselTaxID, Address1, FirmAddress1, PCA.PlaintiffCounselAddress1, Address2, FirmAddress2, PCA.PlaintiffCounselAddress2, City, FirmCity, PCA.PlaintiffCounselCity, ZipCode, firmZip + Case when firmmailzip is null then '' else '-'+FirmMailZip End, PCA.PlaintiffCounselZip, s.State, std.State, st.State


Select PayeeID, Payee, TaxID, Address1, Address2, City, ZipCode, State, case when SUM(Box14)=0 then '' else convert(nvarchar, cast (SUM(Box14) as money),1) end as Box14, case when SUM(Box7)=0 then '' else CONVERT(nvarchar, cast (SUM(Box7) as money),1) end as Box7
from @CombinedTable
Group by PayeeID, Payee, TaxID, Address1, Address2, City, ZipCode, State
Order by Payee


return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrpt1099Data] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrpt1099Data] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrpt1099Data] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrpt1099Data] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrpt1099Data] TO [power_user]
GO
