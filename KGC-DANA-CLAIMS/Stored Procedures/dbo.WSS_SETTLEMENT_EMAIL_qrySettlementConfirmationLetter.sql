SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_EMAIL_qrySettlementConfirmationLetter] (@SettlementGroupID BIGINT)
AS
BEGIN

--DECLARE @SettlementGroupID BIGINT
--SET @SettlementGroupID=5911

-----------------------------------------------------------------------------------
--SETTLEMENT DETAILS: PULL BACK CLAIMANT DETAILS FOR THE SELECTED SETTLEMENT GROUP
-----------------------------------------------------------------------------------

DECLARE @SettlementDetail TABLE (SettlementGroupID INT, PlaintiffCounselID INT, PlaintiffCounselAddressID INT, SettlementAmount MONEY, SettlementDate DATETIME, 
PlaintiffCounselName NVARCHAR(MAX), PlaintiffCounsel NVARCHAR(MAX), PaymentTerms NVARCHAR(Max), 
ClaimantName NVARCHAR(MAX), Matter NVARCHAR(MAX), StateID INT)

INSERT INTO @SettlementDetail
SELECT SG.SettlementGroupID, PCA.PlaintiffCounselID, pca.PlaintiffCounselAddressID,
COALESCE(SettlementAmount,0), 
SettlementDate, 
COALESCE(FName, '') AS PlaintiffCounselName,
UPPER(PlaintiffCounsel),
PaymentTerms,
dbo.CamelCase(CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,'')),
--CASE WHEN RP.LegalCapacityID IN (1,3,4,5,6,7,8,15,17,22) OR DeceasedDate IS NOT NULL 
--	THEN RP.FirstName + ' ' + COALESCE(RP.MiddleName,'') + ' ' + RP.LastName + ', Individually and as ' + LegalCapacity + ' of the Estate of ' + CPI.FirstName + ' ' + 
--	 COALESCE(CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(Suffix,'')
--	ELSE CPI.FirstName + ' ' + COALESCE(CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(Suffix,'') END AS ClaimantName,
Matter, l.StateID
FROM tblClaimantPersonalInfo CPI
LEFT JOIN tblClaimantRelatedParty RP	  ON RP.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
LEFT JOIN tblClaimantSuffix CS	  ON CS.SuffixID=CPI.SuffixID
LEFT JOIN tblLegalCapacity LC	  ON LC.LegalCapacityID=RP.LegalCapacityID
LEFT JOIN tblClaim C	  ON C.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
LEFT JOIN tblMatter M	  ON M.MatterID=C.MatterID
LEFT JOIN tblClaimLawsuit CL	  ON CL.ClaimID=C.ClaimID
LEFT JOIN tblClaimLawsuitStatusTracking CLST	  ON CLST.ClaimLawsuitID=CL.ClaimLawsuitID
LEFT JOIN tblClaimLawsuitStatus CLS	  ON CLS.ClaimLawsuitStatusID=CLST.ClaimLawsuitStatusID
LEFT JOIN tblLawsuit L	  ON L.LawsuitID=CL.LawsuitID
INNER JOIN tblPlaintiffCounselAddress PCA	  ON PCA.PlaintiffCounselAddressID=L.PlaintiffCounselAddressID
INNER JOIN tblState S	  ON S.StateID=L.StateID
INNER JOIN tblJurisdiction J	  ON J.JurisdictionID=L.JurisdictionID
INNER JOIN tblClaimantSettlement CSET	  ON CSET.ClaimLawsuitID=CL.ClaimLawsuitID
INNER JOIN tblSettlementGroup SG	  ON SG.SettlementGroupID=CSET.SettlementGroupID
INNER JOIN tblPaymentTerms PT	  ON PT.PaymentTermsID=SG.PaymentTermsID
LEFT JOIN (SELECT person.* FROM 
vtblPerson person
INNER JOIN tblSettlementConfirmationContact scc ON person.PersonID = scc.PersonID
WHERE PrimaryContact=1 and SettlementGroupID=@SettlementGroupID) P ON P.PlaintiffCounselAddressID=PCA.PlaintiffCounselAddressID
WHERE SG.SettlementGroupID=@SettlementGroupID
GROUP BY SG.SettlementGroupID,
PCA.PlaintiffCounselID,
pca.PlaintiffCounselAddressID,
SettlementDate,
SettlementAmount, 
FName, 
PlaintiffCounsel,
PaymentTerms,
dbo.CamelCase(CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,'')),
--CASE WHEN RP.LegalCapacityID IN (1,3,4,5,6,7,8,15,17,22) OR DeceasedDate IS NOT NULL THEN
--RP.FirstName + ' ' + COALESCE(RP.MiddleName,'') + ' ' + RP.LastName + ', Individually and as ' + LegalCapacity + ' of the Estate of ' + CPI.FirstName + ' ' + COALESCE(CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(Suffix,'')
--ELSE CPI.FirstName + ' ' + COALESCE(CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(Suffix,'') END,
Matter, l.StateID

DECLARE @SettlementAmount MONEY = (SELECT SUM(SettlementAmount) FROM @SettlementDetail)
DECLARE @SettlementDate DATE = (SELECT SettlementDate FROM @SettlementDetail WHERE SettlementDate > '4/1/2011' GROUP BY SettlementDate)
DECLARE @PlaintiffCounselName nvarchar(MAX)
SET @PlaintiffCounselName = (SELECT CAST((SELECT PlaintiffCounselName + ' & ' FROM @SettlementDetail GROUP BY PlaintiffCounselName  FOR XML PATH('')) AS text))
SET @PlaintiffCounselName = LEFT(@PlaintiffCounselName, LEN(@PlaintiffCounselName)-6)--accounts for &amp; encoded ampersand
PRINT @PlaintiffCounselName
DECLARE @PlaintiffCounsel NVARCHAR(MAX) = (SELECT PlaintiffCounsel FROM @SettlementDetail GROUP BY PlaintiffCounsel)
DECLARE @PaymentTerms NVARCHAR(MAX) = (SELECT PaymentTerms FROM @SettlementDetail GROUP BY PaymentTerms)
DECLARE @PlaintiffCounselID INT = (SELECT PlaintiffCounselID FROM @SettlementDetail GROUP BY PlaintiffCounselID)
DECLARE @Matter NVARCHAR(MAX)
DECLARE @StateID INT = (SELECT StateID FROM @SettlementDetail GROUP BY StateID)
IF (SELECT COUNT(DISTINCT Matter) FROM @SettlementDetail)>1
SET @Matter = 'Dana Companies, LLC'
ELSE 
SET @Matter =(SELECT Matter FROM @SettlementDetail GROUP BY Matter)

--DECLARE @ClaimantName NVARCHAR(MAX) = (SELECT ClaimantName FROM @SettlementDetail)

--SETTLED CLAIMANT DETAILS: CREATE TABLE OF CLAIMANTS WITH A SETTLED STATUS (SETTLEMENT STATUS GROUP 1)
DECLARE @SettledClaims TABLE (SettlementGroupID INT, ClaimantName NVARCHAR(max), Jurisdiction NVARCHAR(max), Disease NVARCHAR(max), DocketNumber NVARCHAR(max), Matter NVARCHAR(max), Allocation MONEY, ReleaseType NVARCHAR(max))
INSERT INTO @SettledClaims

SELECT SettlementGroupID, 
dbo.CamelCase(CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,'')),
dbo.CamelCase(Jurisdiction), 
dbo.CamelCase(CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END) AS HighLevelDisease,
DocketNumber,
Matter,
SettlementAmount,
dbo.CamelCase(ReleaseType)
FROM tblClaimantPersonalInfo CPI
LEFT JOIN tblClaimantRelatedParty RP	  ON RP.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
LEFT JOIN tblClaimantSuffix CS	  ON CS.SuffixID=CPI.SuffixID
INNER JOIN tblClaim C	  ON C.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
INNER JOIN tblMatter M	  ON M.MatterID=C.MatterID
INNER JOIN tblClaimDisease CD	  ON CD.ClaimID=C.ClaimID
INNER JOIN tblClaimLawsuit CL	  ON CL.ClaimID=C.ClaimID
INNER JOIN tblLawsuit L	  ON L.LawsuitID=CL.LawsuitID
INNER JOIN tblJurisdiction J	  ON J.JurisdictionID=L.JurisdictionID
INNER JOIN tblClaimantSettlement CSET	  ON CSET.ClaimLawsuitID=CL.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID=CL.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus CLS	  ON CLS.ClaimLawsuitStatusID=CLST.ClaimLawsuitStatusID
INNER JOIN tblSettlePaymentDocumentation SPD  ON SPD.ClaimLawsuitID=CL.ClaimLawsuitID
INNER JOIN tblReleaseType RT	  ON RT.ReleaseTypeID=SPD.ReleaseTypeID
LEFT JOIN tblDisease d	  ON d.DiseaseID=cd.DiseaseID
LEFT JOIN tblDisease_ALT da	  ON cd.DiseaseID=da.DiseaseID_Alt
LEFT JOIN tblHighLevelDisease hld	  ON hld.HighLevelDiseaseID=d.HighLevelDiseaseID OR hld.HighLevelDiseaseID=da.HighLevelDiseaseID
WHERE SettlementGroupID = @SettlementGroupID
AND ClaimLawsuitStatusGroupID = 1
AND IsPrimaryStatus = 1
AND PrimaryDisease = 1
GROUP BY SettlementGroupID, 
CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,''),
Jurisdiction, 
CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END,
DocketNumber,
Matter,
SettlementAmount,
ReleaseType

--DISMISSED CLAIMANT DETAILS: CREATE TABLE OF CLAIMANTS WITH A DISMISSED STATUS (SETTLEMENT STATUS GROUP 2)
DECLARE @DismissedClaims TABLE (SettlementGroupID INT, ClaimantName NVARCHAR(max), Jurisdiction NVARCHAR(max), Disease NVARCHAR(max), DocketNumber NVARCHAR(max), Matter NVARCHAR(max), DismissalType NVARCHAR(max))
INSERT INTO @DismissedClaims

SELECT SettlementGroupID, 
dbo.CamelCase(CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,'')),
dbo.CamelCase(Jurisdiction), 
dbo.CamelCase(CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END) AS HighLevelDisease,
DocketNumber,
Matter,
dbo.CamelCase(ClaimLawsuitStatus)

FROM tblClaimantPersonalInfo CPI
LEFT JOIN tblClaimantRelatedParty RP	  ON RP.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
LEFT JOIN tblClaimantSuffix CS	  ON CS.SuffixID=CPI.SuffixID
INNER JOIN tblClaim C	  ON C.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
INNER JOIN tblMatter M	  ON M.MatterID=C.MatterID
INNER JOIN tblClaimDisease CD	  ON CD.ClaimID=C.ClaimID
INNER JOIN tblClaimLawsuit CL	  ON CL.ClaimID=C.ClaimID
INNER JOIN tblLawsuit L	  ON L.LawsuitID=CL.LawsuitID
INNER JOIN tblJurisdiction J	  ON J.JurisdictionID=L.JurisdictionID
INNER JOIN tblClaimantSettlement CSET	  ON CSET.ClaimLawsuitID=CL.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID=CL.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus CLS	  ON CLS.ClaimLawsuitStatusID=CLST.ClaimLawsuitStatusID
LEFT JOIN tblDisease d	  ON d.DiseaseID=cd.DiseaseID
LEFT JOIN tblDisease_ALT da	  ON cd.DiseaseID=da.DiseaseID_Alt
LEFT JOIN tblHighLevelDisease hld	  ON hld.HighLevelDiseaseID=d.HighLevelDiseaseID OR hld.HighLevelDiseaseID=da.HighLevelDiseaseID
WHERE SettlementGroupID = @SettlementGroupID
AND ClaimLawsuitStatusGroupID = 2
AND IsPrimaryStatus = 1
AND PrimaryDisease = 1
GROUP BY SettlementGroupID, CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,''),
Jurisdiction, 
CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END,
DocketNumber,
Matter,
ClaimLawsuitStatus


DECLARE @MaxProcessedDate date = (
SELECT MAX(CONVERT(DATE, ProcessedDate)) 
FROM tblClaimantSettlement cs
INNER JOIN tblClaimLawsuit CL	  ON CL.ClaimLawsuitID=CS.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID=CL.ClaimLawsuitID
INNER JOIN tblClaimDisease CD	  ON CD.ClaimID=CL.ClaimID
WHERE SettlementGroupID =@SettlementGroupID
AND clst.ClaimLawsuitStatusID = 41
AND IsPrimaryStatus = 0
)

--CONTINUED CLAIMANT DETAILS: CREATE TABLE OF CLAIMANTS WITH A CONTINUED STATUS (CLAIM LAWSUIT STATUS OF "CONTINUED" IS THEIR NON-PRIMARY STATUS)
DECLARE @ContinuedClaims TABLE (SettlementGroupID INT, ClaimantName NVARCHAR(max), Jurisdiction NVARCHAR(max), Disease NVARCHAR(max), DocketNumber NVARCHAR(max), Matter NVARCHAR(max), Continuance NVARCHAR(max))
INSERT INTO @ContinuedClaims
SELECT SettlementGroupID, 
dbo.CamelCase(CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,'')),
dbo.CamelCase(Jurisdiction), 
dbo.CamelCase(CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END) AS HighLevelDisease,
DocketNumber,
Matter,
dbo.CamelCase(ClaimLawsuitStatus)

FROM tblClaimantPersonalInfo CPI
LEFT JOIN tblClaimantRelatedParty RP	  ON RP.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
LEFT JOIN tblClaimantSuffix CS	  ON CS.SuffixID=CPI.SuffixID
INNER JOIN tblClaim C	  ON C.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
INNER JOIN tblMatter M	  ON M.MatterID=C.MatterID
INNER JOIN tblClaimDisease CD	  ON CD.ClaimID=C.ClaimID
INNER JOIN tblClaimLawsuit CL	  ON CL.ClaimID=C.ClaimID
INNER JOIN tblLawsuit L	  ON L.LawsuitID=CL.LawsuitID
INNER JOIN tblJurisdiction J	  ON J.JurisdictionID=L.JurisdictionID
INNER JOIN tblClaimantSettlement CSET	  ON CSET.ClaimLawsuitID=CL.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID=CL.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus CLS	  ON CLS.ClaimLawsuitStatusID=CLST.ClaimLawsuitStatusID
LEFT JOIN tblDisease d	  ON d.DiseaseID=cd.DiseaseID
LEFT JOIN tblDisease_ALT da	  ON cd.DiseaseID=da.DiseaseID_Alt
LEFT JOIN tblHighLevelDisease hld	  ON hld.HighLevelDiseaseID=d.HighLevelDiseaseID OR hld.HighLevelDiseaseID=da.HighLevelDiseaseID
WHERE SettlementGroupID = @SettlementGroupID
AND clst.ClaimLawsuitStatusID = 41 
AND IsPrimaryStatus = 0
AND PrimaryDisease = 1
AND CONVERT(DATE, ProcessedDate) = @MaxProcessedDate
GROUP BY SettlementGroupID, 
CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,''),
Jurisdiction, 
CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END,
DocketNumber,
Matter,
ClaimLawsuitStatus

-----------------------------------------------
--EMAIL DETAILS: BUILD THE CONTENT OF THE EMAIL
-----------------------------------------------
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(MAX)

--BUILDING CLAIMANT TABLES WITH CLAIMANT DETAILS FOR THE SELECTED SETTLEMENT GROUP
DECLARE @TDStyle nvarchar(max) = 'font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

-------------------------------------------------
----**DISMISSAL ONLY** EMAIL DETAILS: BUILD THE CONTENT OF THE EMAIL
-------------------------------------------------

IF (SELECT COUNT(*) FROM @SettledClaims) < 1
BEGIN
--EMAIL SUBJECT LINE
SET @Subject = 'Dana Companies, LLC: New Dismissal Confirmation ' + CONVERT(varchar, getdate(), 107) --UPDATE THIS!!!!!!!!

--DECLARE AND POPULATE THE BODY OF THE EMAIL

SET @BodyMessage = 'Dear ' + @PlaintiffCounselName + ':'

SET @BodyMessage = @BodyMessage + '<br><br>This letter will serve to confirm the agreement between your firm and ' + @Matter + ' its subsidiaries and affiliates which was entered into on ' + CONVERT (NVARCHAR,@SettlementDate,101) + '.  The individual status of the case(s) is listed below. 

<br><br>'


--DISMISSED CLAIMANT DETAIL TABLE
IF (Select COUNT(SettlementGroupID) from @DismissedClaims where SettlementGroupID = @SettlementGroupID) >0 
BEGIN
SET @BodyMessage = @BodyMessage + '<b>Dismissed Claimants: </b>'
SET @BodyMessage = @BodyMessage + N'<table style = "margin-left: 3em; border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Plaintiff</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jurisdiction</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Docket Number</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disease</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Dismissal Type</th>' +

COALESCE(CAST ( (
SELECT  @TDStyle AS 'td/@style', (SELECT ClaimantName) as td, '',
@TDStyle AS 'td/@style', (SELECT Jurisdiction) as td, '',
@TDStyle AS 'td/@style', (SELECT DocketNumber) as td, '',
@TDStyle AS 'td/@style', (SELECT Matter) as td, '',
@TDStyle AS 'td/@style', (SELECT Disease) as td, '',
@TDStyle AS 'td/@style', (SELECT DismissalType) as td, ''
FROM @DismissedClaims
WHERE SettlementGroupID = (select @SettlementGroupID)
GROUP BY ClaimantName, Jurisdiction, DocketNumber, Matter, Disease, DismissalType
for xml path( 'tr' ), type) AS NVARCHAR(MAX) ), '') + 
N'</table>' +  N'<br/>'	

END

--CONTINUED CLAIMANT DETAIL TABLE
IF (Select COUNT(SettlementGroupID) from @ContinuedClaims where SettlementGroupID = @SettlementGroupID) >0 
BEGIN
SET @BodyMessage = @BodyMessage + '<b>Continued Claimants: </b>'
SET @BodyMessage = @BodyMessage + N'<table style = "margin-left: 3em; border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Plaintiff</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jurisdiction</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Docket Number</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disease</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Continuance</th>' +

COALESCE(CAST ( (
SELECT  @TDStyle AS 'td/@style', (SELECT ClaimantName) as td, '',
@TDStyle AS 'td/@style', (SELECT Jurisdiction) as td, '',
@TDStyle AS 'td/@style', (SELECT DocketNumber) as td, '',
@TDStyle AS 'td/@style', (SELECT Matter) as td, '',
@TDStyle AS 'td/@style', (SELECT Disease) as td, '',
@TDStyle AS 'td/@style', (SELECT Continuance) as td, ''
FROM @ContinuedClaims
WHERE SettlementGroupID = (select @SettlementGroupID)
GROUP BY ClaimantName, Jurisdiction, DocketNumber, Matter, Disease, Continuance
for xml path( 'tr' ), type) AS NVARCHAR(MAX) ), '') + 
N'</table>' +  N'<br/>'	

END

BEGIN
SET @BodyMessage = @BodyMessage + '<br>It is agreed and understood that this dismissal is for any and all companion actions in this or any jurisdiction for the above referenced plaintiff.' 

SET @BodyMessage = @BodyMessage + '<br><br>If you have any questions concerning the contents of this letter, please contact me directly at (848) 260-0637. '

SET @BodyMessage = @BodyMessage + '<br><br>Thank you for your anticipated cooperation.'

SET @BodyMessage = @BodyMessage + '<br><br>Sincerely,'

SET @BodyMessage = @BodyMessage + '<br><br>George J. Jacobi'

SET @BodyMessage = @BodyMessage + '<br>President'

--KCIC ADDRESS
SET @BodyMessage = @BodyMessage + '<br><br>
KCIC, LLC on behalf of Jacobi Claims Management, LLC and Dana Companies, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'	


END
END
--***********************************************************
--***********************************************************
ELSE
BEGIN


--EMAIL SUBJECT LINE
SET @Subject = 'Dana Companies, LLC: New Settlement Confirmation ' + CONVERT(varchar, @SettlementDate, 107)

--DECLARE AND POPULATE THE BODY OF THE EMAIL

SET @BodyMessage = 'Dear ' + @PlaintiffCounselName + ':'

SET @BodyMessage = @BodyMessage + '<br><br>This letter will serve to confirm the agreement between your firm and ' + @Matter + ' its subsidiaries and affiliates which was entered into on ' + CONVERT (NVARCHAR,@SettlementDate,101) + '. The individual status of the case(s) is listed below.  The total settlement amount is $' + CONVERT (NVARCHAR,@SettlementAmount,101) + '. 
<br><br>Please <a href="http://www.kcicextranet.com/pp/dana/Pages/Plaintiff''sPortal.aspx">click here</a> to access the KCIC Plaintiff’s Portal where you can track the processing status of the settled claimant(s) and complete the required documents.
<br><br><b>Settled Claimant(s): </b>'

--BUILDING CLAIMANT TABLES WITH CLAIMANT DETAILS FOR THE SELECTED SETTLEMENT GROUP
--DECLARE @TDStyle nvarchar(max) = 'font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--SETTLED CLAIMANT DETAIL TABLE
SET @BodyMessage = @BodyMessage + N'<table style = "margin-left: 3em; border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Plaintiff</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jurisdiction</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Docket Number</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disease</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Allocation</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Release Type</th>' +

COALESCE(CAST ( (
SELECT  @TDStyle AS 'td/@style', (SELECT ClaimantName) as td, '',
@TDStyle AS 'td/@style', (SELECT Jurisdiction) as td, '',
@TDStyle AS 'td/@style', (SELECT DocketNumber) as td, '',
@TDStyle AS 'td/@style', (SELECT Matter) as td, '',
@TDStyle AS 'td/@style', (SELECT Disease) as td, '',
@TDStyle AS 'td/@style', (SELECT '$'+convert(nvarchar(30), coalesce(Allocation, 0), 1)) as td, '',
@TDStyle AS 'td/@style', (SELECT ReleaseType) as td, ''
FROM @SettledClaims
WHERE SettlementGroupID = (select @SettlementGroupID)
GROUP BY ClaimantName, Jurisdiction, DocketNumber, Matter, Disease, Allocation, ReleaseType
for xml path( 'tr' ), type) AS NVARCHAR(MAX) ), '') + 
N'</table>' +  N'<br/>'	

--To Test HTML Uncomment the line below, save messages results as .html file
--print @bodymessage

--DISMISSED CLAIMANT DETAIL TABLE
IF (Select COUNT(SettlementGroupID) from @DismissedClaims where SettlementGroupID = @SettlementGroupID) >0 
BEGIN
SET @BodyMessage = @BodyMessage + '<b>Dismissed Claimants: </b>'
SET @BodyMessage = @BodyMessage + N'<table style = "margin-left: 3em; border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Plaintiff</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jurisdiction</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Docket Number</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disease</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Dismissal Type</th>' +

COALESCE(CAST ( (
SELECT  @TDStyle AS 'td/@style', (SELECT ClaimantName) as td, '',
@TDStyle AS 'td/@style', (SELECT Jurisdiction) as td, '',
@TDStyle AS 'td/@style', (SELECT DocketNumber) as td, '',
@TDStyle AS 'td/@style', (SELECT Matter) as td, '',
@TDStyle AS 'td/@style', (SELECT Disease) as td, '',
@TDStyle AS 'td/@style', (SELECT DismissalType) as td, ''
FROM @DismissedClaims
WHERE SettlementGroupID = (select @SettlementGroupID)
GROUP BY ClaimantName, Jurisdiction, DocketNumber, Matter, Disease, DismissalType
for xml path( 'tr' ), type) AS NVARCHAR(MAX) ), '') + 
N'</table>' +  N'<br/>'	

END

--CONTINUED CLAIMANT DETAIL TABLE
IF (Select COUNT(SettlementGroupID) from @ContinuedClaims where SettlementGroupID = @SettlementGroupID) >0 
BEGIN
SET @BodyMessage = @BodyMessage + '<b>Continued Claimants: </b>'
SET @BodyMessage = @BodyMessage + N'<table style = "margin-left: 3em; border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Plaintiff</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jurisdiction</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Docket Number</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disease</th>
<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Continuance</th>' +

COALESCE(CAST ( (
SELECT  @TDStyle AS 'td/@style', (SELECT ClaimantName) as td, '',
@TDStyle AS 'td/@style', (SELECT Jurisdiction) as td, '',
@TDStyle AS 'td/@style', (SELECT DocketNumber) as td, '',
@TDStyle AS 'td/@style', (SELECT Matter) as td, '',
@TDStyle AS 'td/@style', (SELECT Disease) as td, '',
@TDStyle AS 'td/@style', (SELECT Continuance) as td, ''
FROM @ContinuedClaims
WHERE SettlementGroupID = (select @SettlementGroupID)
GROUP BY ClaimantName, Jurisdiction, DocketNumber, Matter, Disease, Continuance
for xml path( 'tr' ), type) AS NVARCHAR(MAX) ), '') + 
N'</table>' +  N'<br/>'	

END

--INSERT FIRM-SPECIFIC LANGUAGE FOR WEITZ & LUXENBERG
IF @PlaintiffCounselID = 150 
BEGIN 
SET @BodyMessage = @BodyMessage + 'We have agreed that as a condition of this settlement and in compliance with Section 111 of the Medicare, Medicaid and SCHIP Extension Act of 2007 (“MMSEA Section 111”) that Plaintiff(s) and your firm will agree to the following if a lien exists:'
END
--OTHERWISE USE THE GENERIC LANGUAGE
ELSE
BEGIN
SET @BodyMessage = @BodyMessage + 'We have agreed that as a condition of this settlement and in compliance with Section 111 of the Medicare, Medicaid and SCHIP Extension Act of 2007 (“MMSEA Section 111”) that Plaintiff(s) and your firm acknowledge that the settlement funds described herein will be deposited into the trust account of ' + @PlaintiffCounsel + ' and the settlement funds will be released to Plaintiff(s) when all of the following conditions have occurred, if applicable:'
END

--INSERT FIRM-SPECIFIC LANGUAGE FOR WEITZ & LUXENBERG, THE LANIER FIRM BAILEY PERRIN BAILEY AND NAPOLI BERN RIPKA
IF @PlaintiffCounselID IN (150,75,783,586)
BEGIN
SET @BodyMessage = @BodyMessage + 
'<br><p style="margin-left: 3em;"> (1)  Your firm will reimburse or otherwise satisfy the Medicare Program for conditional payments made on claims for items and services relating to the injuries that are the subjects of this action being resolved by this Settlement in a timely manner as set forth in the MSP Provisions;
<br>(2)  Your firm will hold or arrange to hold sufficient net settlement funds (defined as gross settlement funds less procurement costs following 42 C.F.R. §411.37) in trust, escrow, or other similar client trust account (should needs-based government benefits such as Medicaid require preserving), until such time as any obligation to reimburse the Medicare Program for conditional payments on claims for items and services relating to the injuries that are the subject of this action being resolved by this settlement have been fully resolved or satisfied;
<br>(3)  Your firm will take all reasonable and necessary actions to ensure that any such reimbursement obligation is in fact resolved or satisfied. </p>'
END
--OTHERWISE USE THE GENERIC LANGUAGE
ELSE
BEGIN
SET @BodyMessage = @BodyMessage + 
'<br><p style="margin-left: 3em;"> (1) Receipt of a Final Demand for Reimbursement of Conditional Payments from Medicare or its designated agent;
<br>(2)  Payment made to Medicare in Satisfaction of Medicare’s Final Demand for Reimbursement of Conditional Payments;
<br>(3)  Establishment of a Medicare Set Aside, approved by Medicare or a third party reviewer, for future treatment of the alleged injury;
<br>(4)  Receipt of a Medical Report indicating no further treatment is required for the alleged injury. </p>'
END

SET @BodyMessage = @BodyMessage + 'In consideration of the above described settlement money, Plaintiff(s) agree to defend, hold harmless, and indemnify Releasees and their attorneys from and against all Medicare liens, claims, or penalties arising out of or relating in any way to this settlement, the payment to the settled Plaintiff(s) described hereinabove, or Medicare reporting requirements. '

--INSERT ADDITIONAL FIRM-SPECIFIC LANGUAGE FOR SIMMONS BROWDER GIANARIS ANGELIDES & BARNERD LLC.  NO ALTERNATE LANGUAGE FOR OTHER FIRMS.
IF @PlaintiffCounselID = 331 
BEGIN 
SET @BodyMessage = @BodyMessage + 'The extent of this indemnity obligation shall be limited to the total amount of the settlement payment to each of the Plaintiffs described hereinabove. '
END

SET @BodyMessage = @BodyMessage + 'Plaintiff(s) and your firm also expressly agree to provide all information required for MMSEA Section 111 reporting by Releasees. Accordingly, you must complete the forms below related to MMSEA reporting:'

--FIRM-SPECIFIC MEDICARE DOCUMENT REQUIREMENTS

--Firms that need Form A-1, B, C-1: Simmons Browder Gianaris Angelides & Barner and Napoli Bern Ripka
IF @PlaintiffCounselID in (331,586)
BEGIN
SET @BodyMessage = @BodyMessage + '<br><p style="margin-left: 3em;"> (1) Medicare Form A-1; and 
<br>(2) Medicare Form A-2; and 
<br>(3) Medicare Form B; and
<br>(4) Medicare Form C-1 </p>
<p style="margin-left: 3em;">OR
<p style="margin-left: 3em;">(1) MMSEA Settlement Information Form; and 
<br>(2) MMSEA Representation of Plaintiff; and 
<br>(3) MMSEA Representation of Plaintiff’s Counsel.</p>
The MMSEA Settlement Information Form can be completed online using the link above.  If Plaintiff has neither sought nor received any financial assistance or payment from Medicare, Medicaid or Social Security Disability Insurance (SSDI) arising out of, related to, or because of any medical treatment or disabilities asserted in this case, there is no need to complete the MMSEA Settlement Information Form.  The additional two forms will be provided with the Release. Please fully complete these forms and upload them to the site. Changes to the forms are not permitted. Plaintiff(s) and your firm expressly agree and acknowledge that the forms must be properly completed and uploaded to the site as a condition precedent to payment of any settlement amount due under this agreement. KCIC is unable to pay any settlement amount due under this agreement until these MMSEA forms are uploaded to the site and can be processed and verified. Therefore, please be sure to complete the forms as soon as possible. '
END

--Firms that need Form B only: Cooney & Conway, Kazan McClain and O'Brien
ELSE IF @PlaintiffCounselID in (26,257,294)
BEGIN
SET @BodyMessage = @BodyMessage + '<br><p style="margin-left: 3em;"> (1) Form B - Medicare Confidential Reporting Information Form </p> 
Please fully complete this form and upload it to the site. Plaintiff(s) and your firm expressly agree and acknowledge that the form must be properly completed and uploaded to the site as a condition precedent to payment of any settlement amount due under this agreement. KCIC is unable to pay any settlement amount due under our agreement until this form is uploaded to the site and can be processed and verified. Therefore, please be sure to complete this form as soon as possible. '
END

--Firms that need MMSEA Info Form and Rep of Plaintiff: Deaton Law Firm, Keller Fishback and Mulvihill
ELSE IF @PlaintiffCounselID in (643,519,952)
BEGIN
SET @BodyMessage = @BodyMessage + '<br><p style="margin-left: 3em;"> (1) MMSEA Settlement Information Form; and 
<br>(2) MMSEA Representation of Plaintiff </p>
The MMSEA Settlement Information Form can be completed online using the link above.  If Plaintiff has neither sought nor received any financial assistance or payment from Medicare, Medicaid or Social Security Disability Insurance (SSDI) arising out of, related to, or because of any medical treatment or disabilities asserted in this case, there is no need to complete the MMSEA Settlement Information Form.  The MMSEA Representation of Plaintiff form will be provided with the Release. Please fully complete this form and upload it to the site. Changes to the forms are not permitted. Plaintiff(s) and your firm expressly agree and acknowledge that the forms must be properly completed and uploaded to the site as a condition precedent to payment of any settlement amount due under this agreement. KCIC is unable to pay any settlement amount due under this agreement until these MMSEA forms are uploaded to the site and can be processed and verified. Therefore, please be sure to complete the forms as soon as possible. '
END

--Firms that need Rep of Plaintiff and Plaintif Counsel only: Weitz & Luxenberg
ELSE IF @PlaintiffCounselID = 150
BEGIN
SET @BodyMessage = @BodyMessage + '<br><p style="margin-left: 3em;"> (1) MMSEA Representation of Plaintiff Counsel; and 
<br>(2) MMSEA Representation of Plaintiff. </p>
The MMSEA Representation of Plaintiff and MMSEA Representation of Plaintiff Counsel forms will be provided with the Release. Please fully complete these forms and upload them to the site. Changes to the forms are not permitted. Plaintiff(s) and your firm expressly agree and acknowledge that the forms must be properly completed and uploaded to the site as a condition precedent to payment of any settlement amount due under this agreement. KCIC is unable to pay any settlement amount due under our agreement until these MMSEA forms are uploaded to the site and can be processed and verified. Therefore, please be sure to complete the forms as soon as possible. '
END

--States that can use Medicare Forms A & B: Michigan and Illinois
ELSE IF @StateID in (45,56)
BEGIN
SET @BodyMessage = @BodyMessage + '<br><p style="margin-left: 3em;"> (1) Medicare Form A-1; and 
<br>(2) Medicare Form A-2; and
<br>(3) Medicare Form B </p>
<p style="margin-left: 3em;">OR
<p style="margin-left: 3em;">(1) MMSEA Settlement Information Form; and 
<br>(2) MMSEA Representation of Plaintiff; and 
<br>(3) MMSEA Representation of Plaintiff’s Counsel.</p>
The MMSEA Settlement Information Form can be completed online using the link above.  If Plaintiff has neither sought nor received any financial assistance or payment from Medicare, Medicaid or Social Security Disability Insurance (SSDI) arising out of, related to, or because of any medical treatment or disabilities asserted in this case, there is no need to complete the MMSEA Settlement Information Form.  The additional two forms will be provided with the Release. Please fully complete these forms and upload them to the site. Changes to the forms are not permitted. Plaintiff(s) and your firm expressly agree and acknowledge that the forms must be properly completed and uploaded to the site as a condition precedent to payment of any settlement amount due under this agreement. KCIC is unable to pay any settlement amount due under this agreement until these MMSEA forms are uploaded to the site and can be processed and verified. Therefore, please be sure to complete the forms as soon as possible. '
END

--Standard Medicare Forms that all other firms are required to complete
ELSE
BEGIN
SET @BodyMessage = @BodyMessage + '<br><p style="margin-left: 3em;"> (1) MMSEA Settlement Information Form; and 
<br>(2) MMSEA Representation of Plaintiff; and 
<br>(3) MMSEA Representation of Plaintiff’s Counsel.</p>
The MMSEA Settlement Information Form can be completed online using the link above.  If Plaintiff has neither sought nor received any financial assistance or payment from Medicare, Medicaid or Social Security Disability Insurance (SSDI) arising out of, related to, or because of any medical treatment or disabilities asserted in this case, there is no need to complete the MMSEA Settlement Information Form.  The additional two forms will be provided with the Release. Please fully complete these forms and upload them to the site. Changes to the forms are not permitted. Plaintiff(s) and your firm expressly agree and acknowledge that the forms must be properly completed and uploaded to the site as a condition precedent to payment of any settlement amount due under this agreement. KCIC is unable to pay any settlement amount due under this agreement until these MMSEA forms are uploaded to the site and can be processed and verified. Therefore, please be sure to complete the forms as soon as possible. '
END


--INSERT FIRM-SPECIFIC LANGUAGE FOR WEITZ & LUXENBERG
IF @PlaintiffCounselID = 150 
BEGIN 
SET @BodyMessage = @BodyMessage + '<br><br>It was also agreed that your office does not need to complete the MMSEA forms for each of the settled cases, however, you agreed that your office will provide any missing information if it cannot be obtained from local counsel in order to complete the forms. '
END

SET @BodyMessage = @BodyMessage + '<br><br>Furthermore, it is agreed and understood that this settlement fully releases ' + @Matter + ' from any and all companion actions in this or any jurisdiction for the herein referenced Plaintiff(s). '

SET @BodyMessage = @BodyMessage + '<br><br>It is also agreed that the terms of this Agreement shall remain confidential between the parties, their principals, and their counsel. If any of the terms of this Agreement are revealed or publicly disclosed in any manner by any party to any other unauthorized person or party, it shall be considered a breach of the Agreement as to that party thereby entitling ' + @Matter + ' to a rescission of this Agreement as to that party and/or an action against such party for breach of contract for liquidated damages in the amount of the settlement as to that party. '

SET @BodyMessage = @BodyMessage + '<br><br>In accordance with our settlement agreement, payment is ' + @PaymentTerms + '. '

SET @BodyMessage = @BodyMessage + '<br><br>Additionally, by following the link above you will find a release request form (RRF) to be completed for the settled case(s). Please complete and electronically submit the RRF via the site. Within five (5) business days of KCIC’s receipt of the completed RRF a Release will be generated and uploaded to the site for execution.  You will be notified by email when the Release is available for download. Please be advised that ' + @Matter + ' will accept only Releases prepared by KCIC and properly executed by the Plaintiff(s). Please be further advised that payment will not be issued prior to KCIC’s receipt of such properly executed Release(s). '

SET @BodyMessage = @BodyMessage + '<br><br>Should you have any questions, please do not hesitate to contact KCIC or me.'

SET @BodyMessage = @BodyMessage + '<br><br>Sincerely,'

SET @BodyMessage = @BodyMessage + '<br><br>George J. Jacobi'

SET @BodyMessage = @BodyMessage + '<br>President'

--KCIC ADDRESS
SET @BodyMessage = @BodyMessage + '<br><br>
KCIC, LLC on behalf of Jacobi Claims Management, LLC and Dana Companies, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

END

------------------------------------------------------------------------------
-- EMAIL RECIPIENT DETAILS: DETERMINE WHO SHOULD RECEIVE THE SETTLEMENT EMAIL
------------------------------------------------------------------------------

--ASSEMBLE CONTACT LIST
DECLARE @PlaintiffCounselAddressID INT
SET @PlaintiffCounselAddressID = (SELECT PlaintiffCounselAddressID FROM @SettlementDetail GROUP BY PlaintiffCounselAddressID)

DECLARE @RecipientList nvarchar(500)
SET @RecipientList=''

--Use the plaintiffcounseladdressID field to determine the contact list.
--DECLARE a cursor to create a recipient list for the email.
DECLARE @Contact nvarchar(500)
DECLARE contact_curs CURSOR 
LOCAL FORWARD_ONLY STATIC READ_ONLY
FOR
SELECT email 
FROM vtblPerson p
INNER JOIN tblSettlementConfirmationContact scc ON p.PersonID = scc.PersonID
WHERE scc.SettlementGroupID = @SettlementGroupID

OPEN contact_curs
FETCH NEXT FROM contact_curs INTO @contact
WHILE @@FETCH_STATUS=0
BEGIN 
SET @RecipientList = @RecipientList + @Contact + '; '
FETCH NEXT FROM contact_curs INTO @contact
END
CLOSE contact_curs
DEALLOCATE contact_curs

--QC Recipient List By Inserting Recipients in the body of the email
SET @Bodymessage = @BodyMessage + '<br><br>Recipients: ' + '<p style = "font-size: 10pt; font-weight: bold">' + @RecipientList + '</p>'

-----------------
--SEND THE EMAIL 
-----------------

----IF (Select COUNT(SettlementGroupID) from @SettlementDetail) >0 
--BEGIN
----Execute stored procedure to send the email to that firm
--EXEC msdb.dbo.sp_send_dbmail
----The DanaProfile sends from Dana@kcicllc.com
--@profile_name='Claims',
----@recipients = 'khanm@kcic.com',
--@recipients = 'gjacobi@jacobiclaims.com; dnichols@jacobiclaims.com', 
--@blind_copy_recipients = 'dana@kcic.com; jacksonc@kcic.com; khanm@kcic.com',
--@subject = @subject,
--@body = @BodyMessage,
--@body_format = 'html'
--END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_EMAIL_qrySettlementConfirmationLetter] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_EMAIL_qrySettlementConfirmationLetter] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_EMAIL_qrySettlementConfirmationLetter] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_EMAIL_qrySettlementConfirmationLetter] TO [power_user]
GO
