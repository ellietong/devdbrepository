SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryLegalBillDefenseInvoiceSummary_ByFirm] ( @FirmIDFilter int, @StatusID int = null)
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	 --SET NOCOUNT ON added to prevent extra result sets from
	 --interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @FirmFilter int
Set @FirmFilter = @FirmIDFilter
DECLARE @StatusFilter int
SET @StatusFilter = @StatusID

DECLARE @MaxStatusID int
SET @MaxStatusID = (SELECT MAX(LegalBillStatusID) FROM m_tblLegalBillStatus)


Declare @UnpaidInvoice table (InvoiceID int, IsFullyPaid bit)
Insert into @UnpaidInvoice

Select	InvoiceID, 
		Case When IsFullyPaid is Null and NavPaidInvoice = 0 then 0 
		When IsFullyPaid is Null and NavPaidInvoice = 1 then 1
		else IsFullyPaid end as IsFullyPaid 
From	m_tblLegalBillInvoices i
	Left Join tblPaymentDefense pd on i.InvoiceID = pd.DefenseInvoiceID
Group By InvoiceID, 
		Case When IsFullyPaid is Null and NavPaidInvoice = 0 then 0 When IsFullyPaid is Null and NavPaidInvoice = 1 then 1 else IsFullyPaid end

IF(@StatusID IS NULL)
BEGIN
	Select	i.DefenseCounselAddressID, 
			DefenseCounsel, 
			Matter, 
			Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end as OriginalInvoiceNumber, 
			MAX(Period) as MaxPeriod,
			--convert(nvarchar(250),Min(Period)) + ' - ' +convert(nvarchar(250),Max(Period)) as MaxPeriod, 
			SubmittedDate, 
			MAX(DanaApprovalDate) as MaxApprovedDate, 
			Case when s.LegalBillStatus IS null then 'Unknown' 
			When IsFullyPaid = 1 then 'Paid'
			When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end as Status, 
			Sum(OriginalTotal) as OriginalInvoiceTotal, 
			Case when AppealFlag = 1 then Sum(AppealTotal) Else Sum(AdjustedTotal) END as FinalizedInvoiceTotal
	From	m_tblLegalBillInvoices i
		Left Join tblDefenseCounselAddress d on d.DefenseCounselAddressID = i.DefenseCounselAddressID
		Left Join tblMatter m on i.MatterID = m.MatterID
		Left Join m_tblLegalBillStatus s on i.StatusID=s.LegalBillStatusID
		Left Join @UnpaidInvoice u on i.InvoiceID = u.InvoiceID
	Where	((coalesce(submitteddate, danaapprovaldate, period) >= (GETDATE()-365)) 
			OR u.IsFullyPaid = 0 OR i.NavPaidInvoice = 0) 
			AND i.DefenseCounselAddressID = @FirmFilter
	Group By	i.DefenseCounselAddressID, 
				DefenseCounsel, 
				Matter, 
				SubmittedDate,
				Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end, 
				Case when s.LegalBillStatus IS null then 'Unknown' 
				When IsFullyPaid = 1 then 'Paid'
				When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end,
				AppealFlag
	Order By	SubmittedDate desc, DefenseCounsel, Matter
END
ELSE
BEGIN
Select	i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end as OriginalInvoiceNumber, 
		MAX(Period) as MaxPeriod,
		--convert(nvarchar(250),Min(Period)) + ' - ' +convert(nvarchar(250),Max(Period)) as MaxPeriod, 
		SubmittedDate, 
		MAX(DanaApprovalDate) as MaxApprovedDate, 
		Case when s.LegalBillStatus IS null then 'Unknown' 
		When IsFullyPaid = 1 then 'Paid'
		When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end as Status, 
		Sum(OriginalTotal) as OriginalInvoiceTotal, 
		Case when AppealFlag = 1 then Sum(AppealTotal) Else Sum(AdjustedTotal) END as FinalizedInvoiceTotal
From	m_tblLegalBillInvoices i
	Left Join tblDefenseCounselAddress d on d.DefenseCounselAddressID = i.DefenseCounselAddressID
	Left Join tblMatter m on i.MatterID = m.MatterID
	Left Join m_tblLegalBillStatus s on i.StatusID=s.LegalBillStatusID
	Left Join @UnpaidInvoice u on i.InvoiceID = u.InvoiceID
Where	((coalesce(submitteddate, danaapprovaldate, period) >= (GETDATE()-365)) 
		OR u.IsFullyPaid = 0 OR i.NavPaidInvoice = 0) 
		AND i.DefenseCounselAddressID = @FirmFilter
		AND i.StatusID = @StatusFilter
Group By	i.DefenseCounselAddressID, 
			DefenseCounsel, 
			Matter, 
			SubmittedDate,
			Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end, 
			Case when s.LegalBillStatus IS null then 'Unknown' 
			When IsFullyPaid = 1 then 'Paid'
			When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end,
			AppealFlag
Order By	SubmittedDate desc, DefenseCounsel, Matter
END


END


GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryLegalBillDefenseInvoiceSummary_ByFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryLegalBillDefenseInvoiceSummary_ByFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryLegalBillDefenseInvoiceSummary_ByFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryLegalBillDefenseInvoiceSummary_ByFirm] TO [power_user]
GO
