SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_Reports_qryTotalNewFilings]
	-- Add the parameters for the stored procedure here
	(@MonthID int, @YearID int, @MatterID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--DECLARE @MonthID int
--DECLARE @YearID int
--SET @MonthID = 5
--SET @YearID = 2011
--Declare @MatterID int
--Set @MatterID = null

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)
DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID
select COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblPlaintiffCounselAddress_ALT pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
inner join tblPlaintiffCounsel_ALT pc on pc.PlaintiffCounselID=pca.PlaintiffCounselID
where month(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate))=@Month 
and year(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate))=@Year 
--and IsPrimaryStatus=1 and IsPrimaryForClaim = 1
AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(@MatterID, @MaxMatterID)
AND ClaimLawsuitStatusID=22
order by COUNT(distinct(cl.ClaimLawsuitID)) desc

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalNewFilings] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalNewFilings] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalNewFilings] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_Reports_qryTotalNewFilings] TO [power_user]
GO
