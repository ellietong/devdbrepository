SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_MOSS_qryClaimantLawsuitStatusList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT cs.ClaimLawsuitStatusID, ClaimLawsuitStatus FROM tblClaimLawsuitStatus cs
	WHERE cs.ClaimLawsuitStatusID IN (15, 16,35, 21, 22, 23, 44, 45)
	UNION
	SELECT NULL, ''
	ORDER BY ClaimLawsuitStatus
END



GO
