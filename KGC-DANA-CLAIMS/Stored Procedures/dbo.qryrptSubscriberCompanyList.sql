SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryrptSubscriberCompanyList]
(@Solvency varchar(50) = NULL)
AS
SET NOCOUNT ON

--DECLARE @Solvency varchar(50) 
--SET @Solvency = null

SELECT SubscriberID, SubscriptionCompany, SubscriptionCompanySolvency, convert(nvarchar(1000), SubscriptionCompanyNote) as SubscriptionCompanyNote, CarrierGroupName, subscriptionsort
FROM vSubscription s
WHERE s.SubscriptionCompanySolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%')
GROUP BY SubscriberID, SubscriptionCompany, SubscriptionCompanySolvency, convert(nvarchar(1000), SubscriptionCompanyNote), CarrierGroupName, subscriptionsort
ORDER BY SubscriptionSort

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriberCompanyList] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriberCompanyList] TO [base_user]
GRANT ALTER ON  [dbo].[qryrptSubscriberCompanyList] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriberCompanyList] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriberCompanyList] TO [power_user]
GRANT ALTER ON  [dbo].[qryrptSubscriberCompanyList] TO [Subscription_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriberCompanyList] TO [Subscription_User]
GRANT EXECUTE ON  [dbo].[qryrptSubscriberCompanyList] TO [Subscription_User]
GO
