SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCheckPaymentMultiple] (@SourceDate datetime)

As

SET NOCOUNT ON

--Declare @SourceDate datetime
--Set @SourceDate = '4/25/2011'

Declare @CheckAmount table (CheckID int, CheckAmount money)
Insert Into @CheckAmount
Select CheckID, Sum(PaymentAmount)
From tblPaymentDefense
Where CheckID in (select CheckID from tblCheck where SourceDate = @SourceDate)
Group By CheckID
Union
Select CheckID, Sum(PaymentAmount)
From tblPaymentLegalBill
Where CheckID in (select CheckID from tblCheck where SourceDate = @SourceDate)
Group By CheckID
Union
Select CheckID, Sum(PaymentAmount)
From tblPaymentSettlement
Where CheckID in (select CheckID from tblCheck where SourceDate = @SourceDate)
Group By CheckID

Declare @CheckFundingLink table (CheckID int, FundingTypeID int, CheckApproved bit)
Insert Into @CheckFundingLink
Select c.CheckID, FundingTypeID, Case When ca.CheckID is null then 0 When ca.CheckID is not null then 1 End
From tblCheck c
Left Join tblCheckApproval ca on c.CheckID = ca.CheckID
Where SourceDate = @SourceDate


Select c.CheckID, ExpertFirm as Firm, Address1 + Case When Address2 is null then '' Else ' 
' End + Coalesce(Address2,'
') + '
' + City + ', ' +s.State + ' ' +Convert(nvarchar(10),ZipCode) as FirmAddress, SourceNumber as CheckNo, dbo.fnCurrencyToWords(Coalesce(CheckAmount,0)) as AmountDesc,
Coalesce(CheckAmount,0) as CheckAmount, SourceDate as CheckDate, c.FundingTypeID, Matter,
Case when CheckApproved = 1 Then '' When CheckApproved = 0 Then 'VOID' End As CheckStatus, --convert(nvarchar(100),COUNT(pd.DefenseInvoiceID)) + 
'Invoices Totaling ' as InvoiceDetail
From tblCheck c
Inner Join @CheckFundingLink cfl on c.CheckID = cfl.CheckID
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join m_tblExpertFirm ef on di.ExpertFirmID = ef.ExpertFirmID
Inner Join tblState s on s.StateID = ef.StateID
Left Join @CheckAmount ca on ca.CheckID = c.CheckID
Where cfl.FundingTypeID = 1 and c.SourceAmount >= 50000
Group By c.CheckID, ExpertFirm, Address1 + Case When Address2 is null then '' Else ' 
' End + Coalesce(Address2,'
') + '
' + City + ', ' +s.State + ' ' +Convert(nvarchar(10),ZipCode), SourceNumber, dbo.fnCurrencyToWords(Coalesce(CheckAmount,0)),
Coalesce(CheckAmount,0), SourceDate, c.FundingTypeID, Matter, Case when CheckApproved = 1 Then '' When CheckApproved = 0 Then 'VOID' End

Union

Select c.CheckID, PlaintiffCounsel as Firm, PlaintiffCounselAddress1 + Case When PLaintiffCOunselAddress2 is null then '' Else ' 
' End + Coalesce(PlaintiffCounselAddress2,'
') + '
' + PlaintiffCounselCity + ', ' +pca.State + ' ' +Convert(nvarchar(10),PlaintiffCounselZip) as FirmAddress, SourceNumber as CheckNo, 
dbo.fnCurrencyToWords(Coalesce(CheckAmount,0)) as AmountDesc,
Coalesce(CheckAmount,0) as CheckAmount, SourceDate as CheckDate, c.FundingTypeID, Matter,
Case when CheckApproved = 1 Then '' When CheckApproved = 0 Then 'VOID' End As CheckStatus, '' As InvoiceDetail
From tblCheck c
Inner Join @CheckFundingLink cfl on c.CheckID = cfl.CheckID
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentSettlement ps on c.CheckID = ps.CheckID
Inner Join tblClaimantSettlement cs on cs.ClaimantSettlementID = ps.ClaimantSettlementID
Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
Left Join @CheckAmount ca on ca.CheckID = c.CheckID
Where cfl.FundingTypeID = 2 
Group By c.CheckID, SourceNumber, dbo.fnCurrencyToWords(Coalesce(CheckAmount,0)), Coalesce(CheckAmount,0),
SourceDate, PlaintiffCounsel, PlaintiffCounselAddress1 + Case When PLaintiffCOunselAddress2 is null then '' Else ' 
' End + Coalesce(PlaintiffCounselAddress2,'
') + '
' + PlaintiffCounselCity + ', ' +pca.State + ' ' +Convert(nvarchar(10),PlaintiffCounselZip), c.FundingTypeID, Matter, Case when CheckApproved = 1 Then '' When CheckApproved = 0 Then 'VOID' End

Union

Select c.CheckID, DefenseCounsel as Firm, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip) as FirmAddress, SourceNumber as CheckNo, dbo.fnCurrencyToWords(Coalesce(CheckAmount,0)) as AmountDesc,
Coalesce(CheckAmount,0) as CheckAmount, SourceDate as CheckDate, c.FundingTypeID, Matter,
Case when CheckApproved = 1 Then '' When CheckApproved = 0 Then 'VOID' End As CheckStatus, 'From ' + Convert(nvarchar(12),MIN(Period),1) + ' - ' + convert(nvarchar(10),Max(Period),1) + '    ' + --convert(nvarchar(100),COUNT(plb.InvoiceID)) + 
' Invoices Totaling ' as InvoiceDetail
From tblCheck c
Inner Join @CheckFundingLink cfl on c.CheckID = cfl.CheckID
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentLegalBill plb on c.CheckID = plb.CheckID
Inner Join m_tblLegalBillInvoices lbi on lbi.InvoiceID = plb.InvoiceID
Inner Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = lbi.DefenseCounselAddressID
Left Join @CheckAmount ca on ca.CheckID = c.CheckID
Where cfl.FundingTypeID = 3 and c.SourceAmount >= 50000
Group By c.CheckID, DefenseCounsel, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip), SourceNumber, dbo.fnCurrencyToWords(Coalesce(CheckAmount,0)),
Coalesce(CheckAmount,0), SourceDate, c.FundingTypeID, Matter, Case when CheckApproved = 1 Then '' When CheckApproved = 0 Then 'VOID' End

Union

Select c.CheckID, DefenseCounsel as Firm, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip) as FirmAddress, SourceNumber as CheckNo, dbo.fnCurrencyToWords(Coalesce(CheckAmount,0)) as AmountDesc,
Coalesce(CheckAmount,0) as CheckAmount, SourceDate as CheckDate, c.FundingTypeID, Matter,
Case when CheckApproved = 1 Then '' When CheckApproved = 0 Then 'VOID' End As CheckStatus, --convert(nvarchar(100), COUNT(pd.DefenseInvoiceID)) + 
'Invoices Totaling ' as InvoiceDetail
From tblCheck c
Inner Join @CheckFundingLink cfl on c.CheckID = cfl.CheckID
Inner Join tblMatter m on m.MatterID = c.MatterID
Inner Join tblPaymentDefense pd on c.CheckID = pd.CheckID
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Inner Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
Left Join @CheckAmount ca on ca.CheckID = c.CheckID
Where cfl.FundingTypeID = 4 and c.SourceAmount >= 50000
Group By c.CheckID, DefenseCounsel, DefenseCounselAddress1 + Case When DefenseCounselAddress2 is null then '' Else ' 
' End + Coalesce(DefenseCounselAddress2,'
') + '
' + DefenseCounselCity + ', ' +dca.State + ' ' +Convert(nvarchar(10),DefenseCounselZip), SourceNumber, dbo.fnCurrencyToWords(Coalesce(CheckAmount,0)),
Coalesce(CheckAmount,0), SourceDate, c.FundingTypeID, Matter, Case when CheckApproved = 1 Then '' When CheckApproved = 0 Then 'VOID' End


GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentMultiple] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentMultiple] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentMultiple] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentMultiple] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckPaymentMultiple] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckPaymentMultiple] TO [power_user]
GO
