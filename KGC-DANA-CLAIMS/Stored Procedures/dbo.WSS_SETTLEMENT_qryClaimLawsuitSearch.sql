SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] 
(
@ClaimantName nvarchar(500) = null, 
@StateID int = null, 
@SSN nvarchar(15) = null,
@MatterEntityID	int = null,
@MatterID int = null, 
@DocketNumber nvarchar(500) = null,
@CaseCaption nvarchar(500) = null, 
@HighLevelDiseaseID int = null, 
@PlaintiffCounselAddressID int = null
)
AS
BEGIN

--DECLARE @ClaimLawsuitID bigint = NULL
--DECLARE @ClaimantName nvarchar(500) = NULL--'smith'
--DECLARE @StateID int = NULL
--DECLARE @SSN nvarchar(15) = 463705312
--DECLARE @MatterEntityID int = null
--DECLARE @MatterID int = null
--DECLARE @DocketNumber nvarchar(500) = null
--DECLARE @CaseCaption nvarchar(500) = null
--DECLARE @HighLevelDiseaseID int = null
--DECLARE @PlaintiffCounselAddressID int = null

SET NOCOUNT ON

DECLARE @WhereString nvarchar(MAX) = ''
--Construct Where String
IF(@ClaimantName IS NOT NULL)
	SET @WhereString = @WhereString + ' AND LastName + COALESCE('' '' + sfx.Suffix, '''') + '', '' + FirstName + COALESCE('' '' + MiddleName, '''') LIKE ''%' + @ClaimantName + '%'''
IF(@StateID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND cpi.StateID = ' + CONVERT(nvarchar, @StateID)
IF(@SSN IS NOT NULL)
	SET @WhereString = @WhereString + ' AND cpi.SocialSecurityNumber LIKE ''%'+ @SSN + '%'''
IF (@MatterEntityID	IS NOT NULL)
	SET @WhereString = @WhereString + ' AND m.MatterEntityID = ' + CONVERT(nvarchar(20),@MatterEntityID)
IF(@MatterID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar, @MatterID)
IF(@DocketNumber IS NOT NULL)
	SET @WhereString = @WhereString + ' AND l.DocketNumber LIKE ''%' + @DocketNumber + '%'''
IF(@CaseCaption IS NOT NULL)
	SET @WhereString = @WhereString + ' AND l.CaseCaption LIKE ''%' + @CaseCaption + '%'''
IF(@HighLevelDiseaseID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND d.HighLevelDiseaseID = ' + CONVERT(nvarchar, @HighLevelDiseaseID)
IF(@PlaintiffCounselAddressID IS NOT NULL)
	SET @WhereString = @WhereString + ' AND l.PlaintiffCounselAddressID = ' + CONVERT(nvarchar, @PlaintiffCounselAddressID)


DECLARE @QueryString nvarchar(MAX) = 
'
SELECT 
		c.ClaimantPersonalInfoID,
		cl.ClaimLawsuitID, 
		LastName + COALESCE('' '' + sfx.Suffix, '''') + '', '' + FirstName + COALESCE('' '' + MiddleName, '''')  as ClaimantName,
		DocketNumber,
		HighLevelDisease,
		Matter,
		CaseCaption,
		[State]
		--ClaimLawsuitStatus
		--CASE WHEN EXISTS (SELECT SettlementAuthorityID FROM tblSettlementAuthority sa WHERE sa.ClaimLawsuitID = cl.ClaimLawsuitID) THEN ''Authority Requested'' ELSE '''' END AS AuthorityStatus
FROM tblClaimLawsuit cl 
LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
LEFT JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblSuffix sfx ON cpi.SuffixID = sfx.SuffixID
LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
LEFT JOIN tblClaimDisease cd ON c.ClaimID = cd.ClaimID
LEFT JOIN tblMatter m ON c.MatterID = m.MatterID
LEFT JOIN tblDisease d ON cd.DiseaseID = d.DiseaseID
LEFT JOIN tblHighLevelDisease hld ON d.HighLevelDiseaseID = hld.HighLevelDiseaseID
LEFT JOIN tblState s ON l.StateID = s.StateID
INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
INNER JOIN tblClaimLawsuitStatus cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
WHERE	PrimaryDisease = 1 
		AND IsCurrentClaim = 1
		--AND IsPrimaryForClaim = 1
		AND IsPrimaryStatus = 1
		AND cls.ClaimLawsuitStatusGroupID = 3
		' + @WhereString + '
ORDER BY ClaimantName'
		
EXEC sp_executesql @QueryString

END



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryClaimLawsuitSearch] TO [power_user]
GO
