SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_MATTER_qryMattersByEntity]
@MatterEntityID int = NULL
AS
SET NOCOUNT ON

IF @MatterEntityID IS NULL
BEGIN
	SELECT NULL AS MatterID, '' AS Matter
	RETURN
END
ELSE
BEGIN
	SELECT MatterID, 
		Matter 
	FROM tblMatter
	WHERE MatterEntityID = @MatterEntityID
	UNION
	SELECT NULL, ''
	ORDER BY Matter
END

GO
