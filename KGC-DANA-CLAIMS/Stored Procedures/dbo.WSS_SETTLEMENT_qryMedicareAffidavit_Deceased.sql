SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryMedicareAffidavit_Deceased]
(@ClaimLawsuitID int)
As
Set Nocount on

--DECLARE @ClaimLawsuitID INT
--SET @ClaimLawsuitID = 267280

SELECT 
CASE WHEN RP.FirstName IS NULL THEN '' ELSE RP.FirstName + ' ' END + 
CASE WHEN RP.MiddleName IS NOT NULL THEN RP.MiddleName + ' ' ELSE '' END + 
CASE WHEN RP.LastName IS NULL THEN '' ELSE RP.LastName + '' END AS RPName,
LegalCapacity,
COALESCE(CaseCaption, 'UNKNOWN') AS CaseCaption,
CourtType,
Jurisdiction,
UPPER(FullNameState) AS [FullNameState], 
DocketNumber,
CASE WHEN CPI.FirstName IS NULL THEN '' ELSE CPI.FirstName + ' ' END + 
CASE WHEN CPI.MiddleName IS NOT NULL THEN CPI.MiddleName + ' ' ELSE '' END + 
CASE WHEN CPI.LastName IS NULL THEN '' ELSE CPI.LastName + '' END AS ClaimantName,
YEAR(GETDATE()) AS CurrentYear
FROM tblClaimantPersonalInfo CPI
LEFT JOIN tblClaimantRelatedParty RP on RP.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
LEFT JOIN tblClaimantSuffix CS on CS.SuffixID=CPI.SuffixID
LEFT JOIN tblLegalCapacity LC on LC.LegalCapacityID=RP.LegalCapacityID
INNER JOIN tblClaim C on C.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit CL on CL.ClaimID=C.ClaimID
INNER JOIN tblLawsuit L on L.LawsuitID=CL.LawsuitID
INNER JOIN tblCourtType CT on CT.CourtTypeID=L.CourtTypeID
INNER JOIN tblState S on S.StateID=L.StateID
INNER JOIN tblJurisdiction J on J.JurisdictionID=L.JurisdictionID
WHERE ClaimLawsuitID = @ClaimLawsuitID
GO
