SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_SETTLEMENT_qryPlaintiffSettleDocs] (@ClaimLawsuitID int, 
												 @emailaddress nvarchar(250))
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimLawsuitID int
--DECLARE @emailaddress nvarchar(250)
--SET @ClaimLawsuitID = 23466
--SET @emailaddress    = 'crookc@kcicllc.com'


DECLARE @PlaintiffCounselAddressID bigint
DECLARE @QueryString nvarchar(max)

SET @PlaintiffCounselAddressID = (SELECT PlaintiffCOunselAddressID FROM vtblPerson WHERE Email = @emailaddress)


IF (@PlaintiffCounselAddressID IS NOT NULL)
BEGIN
	
	SET @QueryString = 
	'SELECT AmazonKey, 
		DocumentTitle, 
		PageNums, 
		DocCategory, 
		SettleDocID, 
		sd.SetPymtID, 
		sd.DocumentID, 
		sd.DocCategoryID
	FROM tblSettleDocs sd 
		INNER JOIN tblSettlePaymentDocumentation spd ON sd.SetPymtID = spd.SetPymtID
		INNER JOIN tblDocuments d         ON sd.DocumentID = d.DocumentID
		INNER JOIN vtblPerson p ON d.CreatedBy = p.Email
		INNER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID 
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimLawsuit cl ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		LEFT JOIN  tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
		WHERE p.PlaintiffCounselAddressID = ' + CONVERT(nvarchar, @PlaintiffCounselAddressID)
		+ ' AND cl.ClaimLawsuitID = ' + CONVERT(nvarchar, @ClaimLawsuitID) + 
		'  GROUP BY DocumentURL + CASE WHEN ISNUMERIC(PageNums)=1 AND CHARINDEX('','', PageNums)=0 THEN ''#page=''+PageNums WHEN CHARINDEX('','', PageNums)-1 > 0 THEN ''#page=''+LEFT(PageNums, CHARINDEX('','', PageNums)-1)
			WHEN CHARINDEX(''-'', PageNums)-1 > 0 THEN ''#page=''+LEFT(PageNums, CHARINDEX(''-'', PageNums)-1)
			ELSE '''' END, 
		DocumentTitle, 
		PageNums, 
		DocCategory, 
		SettleDocID, 
		sd.SetPymtID, 
		sd.DocumentID, 
		sd.DocCategoryID,
		AmazonKey'
END
ELSE
BEGIN
	SET @QueryString = 
	'SELECT AmazonKey, 
		DocumentTitle, 
		PageNums, 
		DocCategory, 
		SettleDocID, 
		sd.SetPymtID, 
		sd.DocumentID, 
		sd.DocCategoryID
	FROM tblSettleDocs sd 
		INNER JOIN tblSettlePaymentDocumentation spd ON sd.SetPymtID = spd.SetPymtID
		INNER JOIN tblDocuments d         ON sd.DocumentID = d.DocumentID
		INNER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID 
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		INNER JOIN tblClaimLawsuit cl ON spd.ClaimLawsuitID = cl.ClaimLawsuitID
		INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
		LEFT JOIN  tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
		WHERE cl.ClaimLawsuitID = ' + CONVERT(nvarchar, @ClaimLawsuitID) + 
		' GROUP BY DocumentURL + CASE WHEN ISNUMERIC(PageNums)=1 AND CHARINDEX('','', PageNums)=0 THEN ''#page=''+PageNums WHEN CHARINDEX('','', PageNums)-1 > 0 THEN ''#page=''+LEFT(PageNums, CHARINDEX('','', PageNums)-1)
			WHEN CHARINDEX(''-'', PageNums)-1 > 0 THEN ''#page=''+LEFT(PageNums, CHARINDEX(''-'', PageNums)-1)
			ELSE '''' END, 
		DocumentTitle, 
		PageNums, 
		DocCategory, 
		SettleDocID, 
		sd.SetPymtID, 
		sd.DocumentID, 
		sd.DocCategoryID,
		AmazonKey'
END
PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffSettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffSettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffSettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffSettleDocs] TO [power_user]
GO
