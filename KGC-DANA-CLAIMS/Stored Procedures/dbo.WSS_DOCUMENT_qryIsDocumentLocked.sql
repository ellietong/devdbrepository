SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] (@DocumentID bigint)
AS

--SET NOCOUNT ON

--DECLARE @documentID bigint = 406853
--Check for settlements
--SET @retVal =
DECLARE @settlementsAndDismissals TABLE (Value int)

INSERT INTO @settlementsAndDismissals
SELECT COUNT(DISTINCT paymentSettlementID)
FROM tblPaymentSettlement ps
	LEFT JOIN  tblClaimantSettlement cs		ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
	LEFT JOIN  tblClaimLawsuit cl			ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN  tblClaim c					ON cl.ClaimID = c.ClaimID
	INNER JOIN tblMultiClaimantDocument mcd ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
WHERE mcd.DocumentID = @DocumentID
GROUP BY c.ClaimantPersonalInfoID
	
--Union in dismissals
UNION
SELECT COUNT(c.ClaimID) FROM tblClaim c
	INNER JOIN tblClaimLawsuit cl					ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l							ON cl.LawsuitID	 = l.LawsuitID
	INNER JOIN tblMultiClaimantDocument mcd			ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuitStatusTracking clst	ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
WHERE COALESCE(c.IsCurrentClaim,0) = 1 
	AND COALESCE(IsPrimaryForClaim, 0) = 1 
	AND ClaimLawsuitStatusID IN (15,16,17)
	AND mcd.DocumentID = @DocumentID
		
--return the total # found
SELECT SUM(Value) FROM @settlementsAndDismissals
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_DOCUMENT_qryIsDocumentLocked] TO [power_user]
GO
