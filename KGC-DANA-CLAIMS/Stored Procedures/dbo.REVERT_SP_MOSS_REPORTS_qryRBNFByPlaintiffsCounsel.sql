SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_REPORTS_qryRBNFByPlaintiffsCounsel] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @MonthID int, @YearID int)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--declare @PendingDate date
--set @PendingDate ='10/1/2011'

--Declare @Month int
--Declare @Year int
--set @Month=9
--set @Year=2011

Declare @Month int
Declare @Year int
Declare @PendingDate date
set @Month=@MonthID
set @Year=@YearID
set @PendingDate= DATEADD(MONTH,1,convert(date,convert(varchar,@Month)+'/1/'+convert(varchar,@Year)))


    -- Insert statements for procedure here
select pca.PlaintiffCounsel, COUNT(distinct(cl.ClaimLawsuitID)) as Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID

where	StatusDate<@PendingDate
		and ClaimLawsuitStatusID in (34) 
		and IsPrimaryForClaim = 1 
		and  IsPrimaryStatus=1
		and MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
group by pca.PlaintiffCounsel
order by COUNT(distinct(cl.ClaimLawsuitID)) desc, pca.PlaintiffCounsel

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryRBNFByPlaintiffsCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryRBNFByPlaintiffsCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryRBNFByPlaintiffsCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryRBNFByPlaintiffsCounsel] TO [power_user]
GO
