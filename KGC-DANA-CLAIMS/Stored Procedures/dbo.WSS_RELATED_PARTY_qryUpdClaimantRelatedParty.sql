SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] (@ClaimantRelatedPartyID	bigint, 
																	  @FirstName				nvarchar(500), 
																	  @MiddleName				nvarchar(500), 
																	  @LastName					nvarchar(500), 
																	  @RelationshipID			int, 
																	  @LegalCapacityID			int, 
																	  @FullSSN					bigint)
AS
SET NOCOUNT ON

UPDATE tblClaimantRelatedParty
	SET FirstName				= UPPER(@FirstName),
		MiddleName				= UPPER(@MiddleName),
		LastName				= UPPER(@LastName),
		RelationshipID			= @RelationshipID, 
		LegalCapacityID			= @LegalCapacityID,
		RPSocialSecurityNumber	= CASE WHEN @FullSSN = '0' OR LEN(@FullSSN) = 0 THEN NULL ELSE @FullSSN END
	WHERE ClaimantRelatedPartyID = @ClaimantRelatedPartyID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RELATED_PARTY_qryUpdClaimantRelatedParty] TO [power_user]
GO
