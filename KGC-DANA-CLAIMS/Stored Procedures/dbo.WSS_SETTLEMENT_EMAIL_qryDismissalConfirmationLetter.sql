SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_EMAIL_qryDismissalConfirmationLetter] (@SettlementGroupID BIGINT)
AS
BEGIN

--DECLARE @SettlementGroupID BIGINT
--SET @SettlementGroupID=5758


-----------------------------------------------------------------------------------
--SETTLEMENT DETAILS: PULL BACK CLAIMANT DETAILS FOR THE SELECTED SETTLEMENT GROUP
-----------------------------------------------------------------------------------

DECLARE @SettlementDetail TABLE (SettlementGroupID INT, PlaintiffCounselID INT, PlaintiffCounselAddressID INT, SettlementAmount MONEY, SettlementDate DATETIME, 
		PlaintiffCounselName NVARCHAR(MAX), PlaintiffCounsel NVARCHAR(MAX), PaymentTerms NVARCHAR(Max), 
		ClaimantName NVARCHAR(MAX), Matter NVARCHAR(MAX), FirmAddressID int)
		
INSERT INTO @SettlementDetail
SELECT SG.SettlementGroupID, 
	PCA.PlaintiffCounselID, 
	pca.PlaintiffCounselAddressID,
	COALESCE(SettlementAmount,0), 
	SettlementDate, 
	COALESCE(FName, '') AS PlaintiffCounselName,
	UPPER(PlaintiffCounsel),
	PaymentTerms,
	dbo.CamelCase(CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,'')),
	Matter,
	DefenseCounselAddressID
FROM tblClaimantPersonalInfo CPI
	LEFT JOIN tblClaimantRelatedParty RP						  ON RP.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantSuffix CS								  ON CS.SuffixID=CPI.SuffixID
	LEFT JOIN tblLegalCapacity LC								  ON LC.LegalCapacityID=RP.LegalCapacityID
	LEFT JOIN tblClaim C										  ON C.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
	LEFT JOIN tblMatter M										  ON M.MatterID=C.MatterID
	LEFT JOIN tblClaimLawsuit CL								  ON CL.ClaimID=C.ClaimID
	LEFT JOIN tblClaimLawsuitStatusTracking CLST				  ON CLST.ClaimLawsuitID=CL.ClaimLawsuitID
	LEFT JOIN tblClaimLawsuitStatus CLS							  ON CLS.ClaimLawsuitStatusID=CLST.ClaimLawsuitStatusID
	LEFT JOIN tblLawsuit L										  ON L.LawsuitID=CL.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress PCA					  ON PCA.PlaintiffCounselAddressID=L.PlaintiffCounselAddressID
	INNER JOIN tblState S										  ON S.StateID=L.StateID
	INNER JOIN tblJurisdiction J								  ON J.JurisdictionID=L.JurisdictionID
	INNER JOIN tblClaimantSettlement CSET						  ON CSET.ClaimLawsuitID=CL.ClaimLawsuitID
	INNER JOIN tblSettlementGroup SG							  ON SG.SettlementGroupID=CSET.SettlementGroupID
	INNER JOIN tblPaymentTerms PT								  ON PT.PaymentTermsID=SG.PaymentTermsID
	LEFT JOIN (SELECT person.* FROM 
				vtblPerson person
				INNER JOIN tblSettlementConfirmationContact scc ON person.PersonID = scc.PersonID
				WHERE PrimaryContact=1 and SettlementGroupID=@SettlementGroupID) P ON P.PlaintiffCounselAddressID=PCA.PlaintiffCounselAddressID
WHERE SG.SettlementGroupID=@SettlementGroupID AND ClaimLawsuitStatusGroupID = 2
GROUP BY SG.SettlementGroupID,
	PCA.PlaintiffCounselID,
	pca.PlaintiffCounselAddressID,
	SettlementDate,
	SettlementAmount, 
	FName, 
	PlaintiffCounsel,
	PaymentTerms,
	dbo.CamelCase(CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,'')),
	Matter,
	DefenseCounselAddressID


--ASSEMBLE CONTACT LIST
DECLARE @firmaddressid NVARCHAR(10)

        DECLARE firm_Curs CURSOR
        FOR
            SELECT  sd.FirmAddressID 
                    
            FROM    @SettlementDetail sd
            GROUP BY sd.FirmAddressID
           
        OPEN firm_curs
       FETCH NEXT FROM firm_curs INTO @FirmAddressID   
        WHILE @@FETCH_STATUS = 0 
BEGIN
           PRINT @FirmaddressID
        
  
  DECLARE @RecipientList NVARCHAR(500)
                SET @RecipientList = ''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
                DECLARE @Contact NVARCHAR(500)
                DECLARE contact_curs CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY
                FOR
                    SELECT  email
                    FROM    vtblPerson p
                    WHERE   FirmAddressID = @FirmAddressID
                            AND complaintContact = 1
                            AND p.status = 1
	

                OPEN contact_curs
                FETCH NEXT FROM contact_curs INTO @contact
                WHILE @@FETCH_STATUS = 0 
                    BEGIN 

                        SET @RecipientList = @RecipientList + @Contact + '; '

                        FETCH NEXT FROM contact_curs INTO @contact
                    END
                CLOSE contact_curs
                DEALLOCATE contact_curs

  
      
DECLARE @SettlementAmount MONEY = (SELECT SUM(SettlementAmount) FROM @SettlementDetail)
DECLARE @SettlementDate DATE = (SELECT SettlementDate FROM @SettlementDetail WHERE SettlementDate > '4/1/2011' GROUP BY SettlementDate)
DECLARE @PlaintiffCounselName nvarchar(MAX)
	SET @PlaintiffCounselName = (SELECT CAST((SELECT PlaintiffCounselName + ' & ' FROM @SettlementDetail GROUP BY PlaintiffCounselName  FOR XML PATH('')) AS text))
	SET @PlaintiffCounselName = LEFT(@PlaintiffCounselName, LEN(@PlaintiffCounselName)-6)--accounts for &amp; encoded ampersand
	
DECLARE @PlaintiffCounsel NVARCHAR(MAX) = (SELECT PlaintiffCounsel FROM @SettlementDetail GROUP BY PlaintiffCounsel)
DECLARE @PaymentTerms NVARCHAR(MAX) = (SELECT PaymentTerms FROM @SettlementDetail GROUP BY PaymentTerms)
DECLARE @PlaintiffCounselID INT = (SELECT PlaintiffCounselID FROM @SettlementDetail GROUP BY PlaintiffCounselID)
DECLARE @Matter NVARCHAR(MAX)
	IF (SELECT COUNT(DISTINCT Matter) FROM @SettlementDetail)>1
	SET @Matter = 'Dana Companies, LLC'
	ELSE 
	SET @Matter =(SELECT Matter FROM @SettlementDetail GROUP BY Matter)

--DECLARE @ClaimantName NVARCHAR(MAX) = (SELECT ClaimantName FROM @SettlementDetail)

--DISMISSED CLAIMANT DETAILS: CREATE TABLE OF CLAIMANTS WITH A DISMISSED STATUS (SETTLEMENT STATUS GROUP 2)
DECLARE @DismissedClaims TABLE (SettlementGroupID INT, ClaimantName NVARCHAR(max), Jurisdiction NVARCHAR(max), Disease NVARCHAR(max), DocketNumber NVARCHAR(max), Matter NVARCHAR(max), DismissalType NVARCHAR(max), FirmAddressID int)
INSERT INTO @DismissedClaims

SELECT SettlementGroupID, 
	dbo.CamelCase(CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,'')) AS ClaimantName,
	dbo.CamelCase(Jurisdiction) as Jurisdiction, 
	dbo.CamelCase(CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END) AS HighLevelDisease,
	DocketNumber,
	Matter,
	dbo.CamelCase(ClaimLawsuitStatus),
	DefenseCounselAddressID

FROM tblClaimantPersonalInfo CPI
	LEFT JOIN tblClaimantRelatedParty RP		  ON RP.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantSuffix CS				  ON CS.SuffixID=CPI.SuffixID
	INNER JOIN tblClaim C						  ON C.ClaimantPersonalInfoID=CPI.ClaimantPersonalInfoID
	INNER JOIN tblMatter M						  ON M.MatterID=C.MatterID
	INNER JOIN tblClaimDisease CD				  ON CD.ClaimID=C.ClaimID
	INNER JOIN tblClaimLawsuit CL				  ON CL.ClaimID=C.ClaimID
	INNER JOIN tblLawsuit L						  ON L.LawsuitID=CL.LawsuitID
	INNER JOIN tblJurisdiction J				  ON J.JurisdictionID=L.JurisdictionID
	INNER JOIN tblClaimantSettlement CSET		  ON CSET.ClaimLawsuitID=CL.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking CLST ON CLST.ClaimLawsuitID=CL.ClaimLawsuitID
	INNER JOIN tblClaimLawsuitStatus CLS		  ON CLS.ClaimLawsuitStatusID=CLST.ClaimLawsuitStatusID
	LEFT JOIN tblDisease d						  ON d.DiseaseID=cd.DiseaseID
	LEFT JOIN tblDisease_ALT da					  ON cd.DiseaseID=da.DiseaseID_Alt
	LEFT JOIN tblHighLevelDisease hld			  ON hld.HighLevelDiseaseID=d.HighLevelDiseaseID OR hld.HighLevelDiseaseID=da.HighLevelDiseaseID
WHERE SettlementGroupID = @SettlementGroupID
	AND ClaimLawsuitStatusGroupID = 2
	AND IsPrimaryStatus = 1
	AND PrimaryDisease = 1
GROUP BY SettlementGroupID, CPI.FirstName + COALESCE(' ' + CPI.MiddleName,'') + ' ' + CPI.LastName + COALESCE(' '+ Suffix,''),
	Jurisdiction, 
	CASE WHEN hld.HighLevelDisease IS NULL THEN 'Unknown' ELSE hld.HighLevelDisease END,
	DocketNumber,
	Matter,
	ClaimLawsuitStatus,
	DefenseCounselAddressID


            
     
-----------------------------------------------
--EMAIL DETAILS: BUILD THE CONTENT OF THE EMAIL
-----------------------------------------------
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(MAX)

--BUILDING CLAIMANT TABLES WITH CLAIMANT DETAILS FOR THE SELECTED SETTLEMENT GROUP
DECLARE @TDStyle nvarchar(max) = 'font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

BEGIN


--EMAIL SUBJECT LINE
SET @Subject = 'Dana Companies, LLC: New Dismissals ' + CONVERT(varchar, getdate(), 107)

--DECLARE AND POPULATE THE BODY OF THE EMAIL


SET @BodyMessage = 'The following dismissals have been reached as part of a settlement, please upload the court orders at your earliest convenience.<br><br>'

--To Test HTML Uncomment the line below, save messages results as .html file
--print @bodymessage

--DISMISSED CLAIMANT DETAIL TABLE
IF (Select COUNT(SettlementGroupID) from @DismissedClaims where SettlementGroupID = @SettlementGroupID) >0 
	BEGIN
	SET @BodyMessage = @BodyMessage + '<b>Dismissed Claimants: </b></br>'
	SET @BodyMessage = @BodyMessage + N'<table style = "margin-left: 3em; border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr>'+
		 '<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Plaintiff</th>' +
		'<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Jurisdiction</th>'+
		'<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Docket Number</th>'+
		'<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Matter</th>'+
		'<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Disease</th>'+
		'<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Dismissal Type</th>' +

	COALESCE(CAST ( (
	SELECT  @TDStyle AS 'td/@style', (SELECT ClaimantName) as td, '',
			@TDStyle AS 'td/@style', (SELECT Jurisdiction) as td, '',
			@TDStyle AS 'td/@style', (SELECT DocketNumber) as td, '',
			@TDStyle AS 'td/@style', (SELECT Matter) as td, '',
			@TDStyle AS 'td/@style', (SELECT Disease) as td, '',
			@TDStyle AS 'td/@style', (SELECT DismissalType) as td, ''
	FROM @DismissedClaims
	WHERE SettlementGroupID = (select @SettlementGroupID)
	GROUP BY ClaimantName, Jurisdiction, DocketNumber, Matter, Disease, DismissalType
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ), '') + 
	N'</table>' +  N'<br/>'	


 
END


--KCIC ADDRESS NOT NEEDED ANYMORE

END

------------------------------------------------------------------------------
-- EMAIL RECIPIENT DETAILS: DETERMINE WHO SHOULD RECEIVE THE SETTLEMENT EMAIL
------------------------------------------------------------------------------
 
        


--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
--PRINT 'STARTING POPULATION of contact list'
                     
        

--QC Recipient List By Inserting Recipients in the body of the email
--SET @Bodymessage = @BodyMessage + '<br><br>Recipients: ' + '<p style = "font-size: 10pt; font-weight: bold">' + @RecipientList + '</p>'

-------------------
----SEND THE EMAIL 
-------------------
----PRINT @RecipientList
----IF (Select COUNT(SettlementGroupID) from @SettlementDetail) >0 
--BEGIN
----Execute stored procedure to send the email to that firm
--EXEC msdb.dbo.sp_send_dbmail
----The DanaProfile sends from Dana@kcicllc.com
--@profile_name='Claims',
--@recipients = @RecipientList, 
--@blind_copy_recipients = 'prossc@kcic.com; dana@kcic.com; khanm@kcic.com',
--@subject = @subject,
--@body = @BodyMessage,
--@body_format = 'html'
--END

--Move to the next firm to create a new email
      FETCH NEXT FROM firm_curs INTO @FirmAddressID   
      END          
      CLOSE firm_curs
        DEALLOCATE firm_curs        
END

GO
