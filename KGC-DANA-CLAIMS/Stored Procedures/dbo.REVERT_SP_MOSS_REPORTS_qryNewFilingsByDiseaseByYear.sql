SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilingsByDiseaseByYear] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @YearID int, @MonthID int)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     -- Insert statements for procedure here
    
--Declare @year int
--Set @Year=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @year int
SET @year = @YearID

DECLARE @Month int
set @Month = @MonthID

select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end as HighLevelDisease, 
		COUNT(distinct(cl.ClaimLawsuitID)) AS Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblLawsuit l on cl.LawsuitID=l.LawsuitID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimDisease cd on c.ClaimID=cd.ClaimID
left join tblDisease d on d.DiseaseID=cd.DiseaseID
left join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
left join tblHighLevelDisease hld on hld.HighLevelDiseaseID=d.HighLevelDiseaseID or hld.HighLevelDiseaseID=da.HighLevelDiseaseID
where	year(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate))= @Year 
		and MONTH(coalesce(ServiceDate,FileDate,ReceivedDate,Statusdate)) <=@Month
		--and IsPrimaryForClaim = 1 
		--and IsPrimaryStatus=1
		and PrimaryDisease=1
		and MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
		AND ClaimLawsuitStatusID=22
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, hld.HighLevelDiseaseID
order by hld.HighLevelDiseaseID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilingsByDiseaseByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilingsByDiseaseByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilingsByDiseaseByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryNewFilingsByDiseaseByYear] TO [power_user]
GO
