SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_REPORTS_CLAIM_qryClaimsReports] (@ClaimLawsuitStatusGroupID int, 
                                                       @HighLevelDiseaseID        int, 
                                                       @StateID                   int, 
                                                       @PlaintiffCounselID        bigint, 
                                                       @JurisdictionID            int, 
                                                       @MatterID                  int, 
                                                       @IsMDL                     int, 
                                                       @emailaddress              nvarchar(250), 
                                                       @AllegationTypeID          int)
AS
SET NOCOUNT ON

----VARIABLES FOR TESTING----
--DECLARE @ClaimLawsuitStatusGroupID int
--DECLARE @HighLevelDiseaseID        int
--DECLARE @StateID                   int
--DECLARE @PlaintiffCounselID        bigint
--DECLARE @JurisdictionID            int
--DECLARE @MatterID                  int
--DECLARE @IsMDL                     int
--DECLARE @emailaddress              nvarchar(250)
--DECLARE @AllegationTypeID         int
--SET @ClaimLawsuitStatusGroupID = NULL
--SET @HighLevelDiseaseID        = NULL
--SET @StateID                   = NULL
--SET @PlaintiffCounselID        = NULL
--SET @JurisdictionID            = NULL
--SET @MatterID                  = NULL
--SET @IsMDL                     = NULL
--SET @emailaddress              = 'crookc@kcicllc.com' 
--SET @AllegationTypeID          = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

--Prepare permissions-related fields for use in queries below.
DECLARE @DefenseCounselAddressID int
SET @DefenseCounselAddressID = (SELECT FirmAddressID 
								FROM vtblPerson 
								WHERE Email = @emailaddress)

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE COALESCE(cl.IsPrimaryForClaim, 0) = 1 AND clst.IsPrimaryStatus = 1'
IF @ClaimLawsuitStatusGroupID	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND cls.ClaimLawsuitStatusGroupID = ' + CONVERT(nvarchar(20),@ClaimLawsuitStatusGroupID) END
IF @HighLevelDiseaseID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND (ddisease.HighLevelDiseaseID = ' + CONVERT(nvarchar(20),@HighLevelDiseaseID) + ' OR da.HighLevelDiseaseID = ' + CONVERT(nvarchar(20),@HighLevelDiseaseID) + ')' END
IF @StateID					IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.StateID = ' + CONVERT(nvarchar(20),@StateID) END
IF @PlaintiffCounselID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND pca.PlaintiffCounselID = ' + CONVERT(nvarchar(20),@PlaintiffCounselID) END
IF @JurisdictionID			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.JurisdictionID  = ' + CONVERT(nvarchar(20),@JurisdictionID) END
IF @MatterID				IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar(20),@MatterID) END
IF @IsMDL					IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND IsMDL = ' + CONVERT(nvarchar(20),@IsMDL) END
IF @AllegationTypeID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND a.AllegationTypeID = ' + CONVERT(nvarchar(20),@AllegationTypeID) END

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF((RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com') OR ((SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1))
	SET @JoinString  = ''
ELSE
BEGIN
	SET @JoinString  = ' INNER JOIN tblDefenseJurisdiction dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID '
	SET @WhereString = @WhereString + ' AND (dj.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),@DefenseCounselAddressID) + ' OR l.DefenseCounselAddressID = ' + CONVERT(nvarchar(20),@DefenseCounselAddressID) + ')'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT DISTINCT c.ClaimID, 
		c.ClaimantPersonalInfoID, 
		IsMDL, 
		dhld.HighLevelDisease AS Disease, 
		s.State, 
		j.Jurisdiction, 
		cs.ClaimSequence, 
		cpi.LastName + '', '' + CASE WHEN FirstName IS NULL THEN '''' 
															ELSE FirstName + '' '' END + CASE WHEN MiddleName IS NULL THEN '''' 
																													  ELSE MiddleName + '' '' END AS ClaimantName, 
		CASE WHEN SocialSecurityNumber IS NULL THEN '''' 
											   ELSE ''XXX-XX-''+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
		BirthDate, 
		cpi.DeceasedDate, 
		cls.ClaimLawsuitStatus, 
		CONVERT(date, l.ServiceDate) AS ServiceDate, 
		CONVERT(date, l.FileDate) AS FileDate, 
		l.DocketNumber, 
		l.CaseCaption, 
		l.LeadPlaintiffLastName + '', '' + l.LeadPlaintiffFirstName AS LeadPlaintiffName, 
		pca.PlaintiffCounsel, 
		DefenseCounsel AS PrimaryDefenseCounsel, 
		m.Matter, 
		cl.ActiveTrialDate, 
		a.AllegationTypeID, 
		AllegationType,
		CASE WHEN clst.ClaimLawsuitStatusID !=22 THEN StatusDate
												 ELSE Null END AS ResolutionDate
	 FROM tblClaimantPersonalInfo cpi 
		INNER JOIN tblClaim c                          ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID 
		INNER JOIN tblClaimLawsuit cl                  ON c.ClaimID = cl.ClaimID 
		INNER JOIN tblLawsuit l                        ON cl.LawsuitID = l.LawsuitID ' +
		@JoinString + '
		LEFT  JOIN tblPlaintiffCounselAddress pca	   ON pca.plaintiffcounseladdressid = l.plaintiffcounseladdressid 
		INNER JOIN tblState s						   ON s.StateID = l.StateID 
		INNER JOIN tblJurisdiction j				   ON j.JurisdictionID = l.JurisdictionID 
		INNER JOIN tblClaimSequence cs				   ON cs.ClaimSequenceID = c.ClaimSequenceID 
		LEFT  JOIN tblDefenseCounselAddress dca		   ON l.DefenseCounselAddressID = dca.DefenseCounselAddressID
		LEFT  JOIN (SELECT * 
		            FROM tblClaimDisease 
		            WHERE PrimaryDisease = 1) cd       ON cd.ClaimID = c.ClaimID 
		LEFT  JOIN tblDisease ddisease				   ON cd.DiseaseID = ddisease.DiseaseID 
		LEFT  JOIN tblDisease_ALT da				   ON cd.DiseaseID = da.DiseaseID_Alt 
		LEFT  JOIN tblHighLevelDisease dhld			   ON dhld.HighLevelDiseaseID = ddisease.HighLevelDiseaseID OR dhld.HighLevelDiseaseID = da.HighLevelDiseaseID 
		INNER JOIN (SELECT * 
		            FROM tblClaimLawsuitStatusTracking 
		            WHERE IsPrimaryStatus = 1) clst    ON cl.ClaimLawsuitID = clst.ClaimLawsuitID 
		INNER JOIN tblClaimLawsuitStatus cls		   ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
		LEFT JOIN tblClaimLawsuitStatusGroup clsg	   ON cls.ClaimLawsuitStatusGroupID = clsg.ClaimLawsuitStatusGroupID
		LEFT  JOIN tblClaimantSettlement csett		   ON cl.ClaimLawsuitID = csett.ClaimLawsuitID
		LEFT  JOIN tblMatter m						   ON c.MatterID = m.MatterID
		LEFT  JOIN tblAllegation a					   ON cl.ClaimLawsuitID = a.ClaimLawsuitID
		LEFT  JOIN tblAllegationType at				   ON a.AllegationTypeID = at.AllegationTypeID ' + 
	 @WhereString + '
	 OPTION (RECOMPILE)'

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString



GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_CLAIM_qryClaimsReports] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_CLAIM_qryClaimsReports] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_CLAIM_qryClaimsReports] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_CLAIM_qryClaimsReports] TO [power_user]
GO
