SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[WSS_MATTER_qryMatterList_Filter]
AS
SET NOCOUNT ON

--SELECT MatterID, 
--	Matter, 
--	MatterEntityID 
--FROM tblMatter
--UNION
--SELECT NULL, '', NULL
--ORDER BY Matter
SELECT MatterID, 
	Matter + ' (' + MatterEntity + ')' AS Matter, 
	m.MatterEntityID,
	MatterEntity 
FROM tblMatter m
	INNER JOIN tblMatterEntity me on m.MatterEntityID = me.MatterEntityID
UNION
SELECT NULL, NULL, NULL, NULL--NULL, '', NULL, ''
ORDER BY Matter + ' (' + MatterEntity + ')'
GO
