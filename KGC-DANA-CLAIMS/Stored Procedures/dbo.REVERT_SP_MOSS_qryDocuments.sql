SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[REVERT_SP_MOSS_qryDocuments]
( @DocID int, @emailaddress nvarchar(250))
AS
--DECLARE @DocID int
--SET @DocID = 96
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'crookc@kcicllc.com'


--GET LIST OF CLAIMANTS FOR DOCUMENT

Declare @UserFirmState table (DefenseCounselID bigint, StateID int)
Insert into @UserFirmState

Select dca.DefenseCounselID, StateID
From dbo.tblDefenseCounselState AS dcs 
left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid --and dcar.stateID = dcs.StateID 
LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
Where (ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) OR
dca.DefenseCounselID in (SELECT f.FirmID FROM vtblPerson p inner join vtblFirm f on p.FirmID=f.FirmID WHERE Email = @emailaddress))

DECLARE @Claimants nvarchar(max)
SET @Claimants= '' 

DECLARE @SettleDocs nvarchar(max)
SET @SettleDocs= '' 

DECLARE @addClaimant nvarchar(max)
DECLARE @addClaimantID int


DECLARE @DocCat nvarchar(max)
DECLARE @PageNum nvarchar(max)

IF(RIGHT(@emailaddress, 11) = 'kcicllc.com' OR (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress) IN (132, 133))
BEGIN
DECLARE Cur_Claimants CURSOR Local Fast_Forward for
SELECT COALESCE(mcd.Claimantpersonalinfoid, ''), COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') + ' (' + COALESCE(CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID), '') + ')'
FROM tblMultiClaimantDocument mcd
LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE mcd.DocumentID = @DocID
GROUP BY mcd.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID) + ')' , LastName, FirstName
UNION 
SELECT COALESCE(d.Claimantpersonalinfoid, ''), COALESCE(LastName, '') + ', ' + COALESCE(FirstName, '') + ' (' + COALESCE(CONVERT(nvarchar(10), d.ClaimantPersonalInfoID), '') + ')' 
FROM tblDocuments d
INNER JOIN tblClaimantPersonalInfo cpi ON d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE d.DocumentID = @DocID
GROUP BY d.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), d.ClaimantPersonalInfoID) + ')' , LastName, FirstName
--ORDER BY LastName, FirstName
END
ELSE
BEGIN
DECLARE Cur_Claimants CURSOR Local Fast_Forward for
SELECT mcd.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID) + ')' 
FROM tblMultiClaimantDocument mcd
LEFT JOIN tblClaimantPersonalInfo cpi ON mcd.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.claimantpersonalInfoID
LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
INNER JOIN tblDefenseCounselState dcs on l.DefenseCounselAddressID=dcs.DefenseCounselAddressID and l.StateID=dcs.StateID
INNER JOIN tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCounselAddressID=dca.DefenseCounselAddressID
INNER JOIN @UserFirmState u on dca.DefenseCounselID = u.DefenseCounselID and l.StateID = u.StateID
WHERE mcd.DocumentID = @DocID
--AND DefenseCounselAddressID IN (SELECT DISTINCT dcs.DefenseCounseladdressID
-- FROM dbo.tblDefenseCounselState AS dcs INNER JOIN
-- dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
-- LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
-- LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID INNER JOIN
-- dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
-- WHERE ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
-- GROUP BY d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounseladdressID, dcs.ResponsibleDefenseCounseladdressID, dcs.IsActiveCounsel, dcs.StateID)
GROUP BY mcd.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), mcd.ClaimantPersonalInfoID) + ')' , LastName, FirstName
UNION
SELECT d.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), d.ClaimantPersonalInfoID) + ')' 
FROM tblDocuments d
LEFT JOIN tblClaimantPersonalInfo cpi ON d.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.claimantpersonalInfoID
LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
INNER JOIN tblDefenseCounselState dcs on l.DefenseCounselAddressID=dcs.DefenseCounselAddressID and l.StateID=dcs.StateID
INNER JOIN tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCounselAddressID=dca.DefenseCounselAddressID
--INNER JOIN @UserFirmState u on dca.DefenseCounselID = u.DefenseCounselID and l.StateID = u.StateID
WHERE d.DocumentID = @DocID
--AND DefenseCounselAddressID IN (SELECT DISTINCT dcs.DefenseCounseladdressID
-- FROM dbo.tblDefenseCounselState AS dcs INNER JOIN
-- dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
-- LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
-- LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID INNER JOIN
-- dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
-- WHERE ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
-- GROUP BY d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounseladdressID, dcs.ResponsibleDefenseCounseladdressID, dcs.IsActiveCounsel, dcs.StateID)
GROUP BY d.Claimantpersonalinfoid, LastName + ', ' + FirstName + ' (' + CONVERT(nvarchar(10), d.ClaimantPersonalInfoID) + ')' , LastName, FirstName
--ORDER BY LastName, FirstName
END

--------------------------------------------------------------------

--Open Docket Cursor
OPEN Cur_Claimants

--Fetch Data
FETCH NEXT FROM Cur_Claimants INTO @addClaimantID, @addClaimant

WHILE @@Fetch_Status = 0
BEGIN
--assemble the string
--if LEN(@Claimants) = 0 SET @Claimants = '' + @addClaimant + ''
--else SET @Claimants = @Claimants + '
--' + '' + @addClaimant + ''
  if LEN(@Claimants) = 0 SET @Claimants = '<a target="_blank" href="/clients/Dana/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + COALESCE(CONVERT(nvarchar(10), @addClaimantID), '') + '">' + COALESCE(@addClaimant, '') + '</a>'
      else SET @Claimants =  @Claimants  + '<br />' +  '<a target="_blank" href="/clients/Dana/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + COALESCE(CONVERT(nvarchar(10), @addClaimantID), '') + '">' + COALESCE(@addClaimant, '') + '</a>'
      PRINT @Claimants


FETCH NEXT FROM Cur_Claimants INTO @addClaimantID, @addClaimant

END

CLOSE Cur_Claimants 
DEALLOCATE Cur_Claimants


-----------------------------------------------------------------

DECLARE Cur_SettleDoc CURSOR Local Fast_Forward for
SELECT DocCategory, PageNums
--ClaimLawsuitStatus, DismissalDate, DismissalProcessDate
FROM tblSettleDocs sd 
LEFT JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID
WHERE DocumentID = @DocID

--Open Docket Cursor
OPEN Cur_SettleDoc

--Fetch Data
FETCH NEXT FROM Cur_SettleDoc INTO @DocCat, @PageNum

WHILE @@Fetch_Status = 0
BEGIN
      --assemble the string
      if LEN(@SettleDocs) = 0 SET @SettleDocs = COALESCE(@DocCat, '') + '&nbsp;&nbsp;pg(s): ' + COALESCE(@PageNum, '')
      else SET @SettleDocs =  @SettleDocs  + '<br />' +  COALESCE(@DocCat, '') + '&nbsp;&nbsp;pg(s): ' + COALESCE(@PageNum, '')
      PRINT @SettleDocs


      FETCH NEXT FROM Cur_SettleDoc INTO @DocCat, @PageNum
      
END


CLOSE Cur_SettleDoc 
DEALLOCATE Cur_SettleDoc

------------------------------------------------------------------------------------------------------------------


SELECT 
cast(d.DocumentID as varchar(50)) as DocumentId,
dt.ClaimantDocumentType, 
dg.DocumentGroup, 
d.Comments,
d.DocumentURL,
d.DocumentTitle,
d.SPDocId,
d.CreatedBy,
d.DateCreated,
d.ModifiedBy,
d.DateModified,
d.Accepted,
d.AcceptedBy,
d.DateAccepted,
d.ApprovedBy,
d.DateApproved,
f.Firm,
COALESCE(@Claimants, '') AS Claimants,
CASE WHEN LEN(@SettleDocs)=0 THEN 'N/A' ELSE COALESCE(@SettleDocs, 'N/A') END AS SettleDocs,
d.DocumentID,
d.DismissalDate,
QCDate AS DismissalProcessDate,
ClaimLawsuitStatus AS DismissalType,
DocumentSource,
d.DocumentSourceID,
NULL as AutoAccept,
--Settlement Info Only
NULL as SettlementGroupID,
NULL as SettlementDate,
NULL as SettlementAmount,

--Settlements and Claimants
NULL as Claimant,
NULL as SocialSecurityNumber,
NULL as BirthDate,
NULL as disease, 
NULL as claimlawsuitstatus, 
NULL as IsCurrentClaim, 

--not used for invoices
NULL as docketnumber, 
NULL as CaseCaption,
NULL as stateabb, 

--lawsuits only
NULL as jurisdiction,
NULL as LeadPlaintiff, 
NULL as filedate, 
NULL as servicedate, 
NULL as primarylocalplaintiffcounsel,

--Invoices Only
d.InvoiceNumber,
d.Period,
d.DatePayable,
d.FeeAmount,
d.DisbursementAmount,
d.Adjustment,
d.AdjustmentNotes,
dis.Status, 
ef.ExpertFirm ExpertFirm, 
di.SubmitDate,
PrivateComments,
Coalesce(IsFullyPaid,0) as IsFullyPaid,



--Court Documents Only
NULL as DateOfFiling,
NULL as DateOfDeath,
DocketNumbers



FROM tblDocuments d
JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
--invoices only
LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
LEFT OUTER JOIN tblPaymentDefense pd on di.DefenseInvoiceID=pd.DefenseInvoiceID
LEFT OUTER JOIN m_tblExpertFirm ef ON d.ExpertFirmID = ef.ExpertFirmID 
LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
LEFT OUTER JOIN tblClaimantDocketNumbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
LEFT OUTER JOIN tblDocumentSource ds ON d.DocumentSourceID = ds.DocumentSourceID
LEFT OUTER JOIN tblClaimlawsuitstatus cls ON d.claimlawsuitstatusID = cls.claimlawsuitstatusID
WHERE (d.DetailDocumentTypeID = 0 OR dg.DocumentGroupID = 2)
AND d.DocumentID = @DocID


--STEP TWO: Get Settlements and Claimants
UNION

SELECT 
cast(d.DocumentID as varchar(50)) as DocumentId,
dt.ClaimantDocumentType, 
dg.DocumentGroup, 
d.Comments,
d.DocumentURL,
d.DocumentTitle,
d.SPDocId,
d.CreatedBy,
d.DateCreated,
d.ModifiedBy,
d.DateModified,
d.Accepted,
d.AcceptedBy,
d.DateAccepted,
d.ApprovedBy,
d.DateApproved,
f.Firm,
COALESCE(@Claimants, '') AS Claimants,
CASE WHEN LEN(@SettleDocs)=0 THEN 'N/A' ELSE COALESCE(@SettleDocs, 'N/A') END AS SettleDocs,
d.DocumentID,
d.DismissalDate,
QCDate AS DismissalProcessDate,
ClaimLawsuitStatus AS DismissalType,
DocumentSource,
d.DocumentSourceID,
NULL as AutoAccept,

--Settlement Info Only
d.SettlementGroupID,
d.SettlementDate,
d.SettlementAmount,

--Settlements and Claimants
COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, '') AS 'Claimant',
CASE WHEN cpi.SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(cpi.SocialSecurityNumber,4) END AS SocialSecurityNumber,
cpi.BirthDate,
di2.disease, 
cls.claimlawsuitstatus, 
c.IsCurrentClaim, 

--these values used for settlements and claimants, different joins used for lawsuits
l.docketnumber, 
l.CaseCaption,
st.[State], 
j.jurisdiction,

--lawsuits only 
NULL as LeadPlaintiff, 
NULL as filedate, 
NULL as servicedate, 
NULL as primarylocalplaintiffcounsel,

--Invoices Only
NULL as InvoiceNumber,
NULL as Period,
NULL as DatePayable,
NULL as FeeAmount,
NULL as DisbursementAmount,
NULL as Adjustment,
NULL as AdjustmentNotes,
NULL as Status, 
NULL as ExpertFirm, 
NULL as SubmitDate, 
PrivateComments,
NULL as IsFullyPaid,

--Court Documents Only
NULL as DateOfFiling,
NULL as DateOfDeath,
DocketNumbers 

FROM tblDocuments d
JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
--settlements and claimants only
LEFT OUTER JOIN tblClaimantPersonalInfo cpi ON d.claimantpersonalinfoid = cpi.claimantpersonalinfoid 
LEFT OUTER JOIN tblClaim c ON cpi.claimantpersonalinfoid = c.claimantpersonalinfoid
LEFT OUTER JOIN (SELECT * FROM tblClaimDisease CD WHERE PrimaryDisease = 1) CD ON CD.ClaimID = C.ClaimID
LEFT OUTER JOIN tblDisease di2 ON CD.DiseaseID = di2.diseaseid
LEFT OUTER JOIN tblClaimLawsuit cl ON c.claimid = cl.claimid and cl.IsPrimaryForClaim = 1
LEFT OUTER JOIN tblLawsuit l ON cl.lawsuitid = l.lawsuitid
LEFT OUTER JOIN tblState st ON l.stateid = st.stateid
LEFT OUTER JOIN tblJurisdiction j ON l.jurisdictionid = j.jurisdictionid
LEFT OUTER JOIN tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT OUTER JOIN tblClaimLawsuitStatus cls ON clst.claimlawsuitstatusid = cls.claimlawsuitstatusid
LEFT OUTER JOIN tblClaimantSettlement cs ON cl.claimlawsuitid = cs.claimlawsuitid
LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
LEFT OUTER JOIN tblDocumentSource ds ON d.DocumentSourceID = ds.DocumentSourceID
WHERE dg.DocumentGroupId in(1,4,5)
AND d.DocumentID = @DocID
--AND IsCurrentClaim = 1

-- STEP THREE: Get Lawsuits
UNION

SELECT 
cast(d.DocumentID as varchar(50)) as DocumentId,
dt.ClaimantDocumentType, 
dg.DocumentGroup, 
d.Comments,
d.DocumentURL,
d.DocumentTitle,
d.SPDocId,
d.CreatedBy,
d.DateCreated,
d.ModifiedBy,
d.DateModified,
d.Accepted,
d.AcceptedBy,
d.DateAccepted,
d.ApprovedBy,
d.DateApproved,
f.Firm,
COALESCE(@Claimants, '') AS Claimants,
CASE WHEN LEN(@SettleDocs)=0 THEN 'N/A' ELSE COALESCE(@SettleDocs, 'N/A') END AS SettleDocs,
d.DocumentID,
d.DismissalDate,
QCDate AS DismissalProcessDate,
ClaimLawsuitStatus AS DismissalType,
DocumentSource,
d.DocumentSourceID,
NULL as AutoAccept,

--Settlement Info Only
NULL as SettlementGroupID,
NULL as SettlementDate,
NULL as SettlementAmount,

--Settlements and Claimants
COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, '') AS 'Claimant',
NULL as SocialSecurityNumber,
NULL as BirthDate,
NULL as disease, 
NULL as claimlawsuitstatus, 
NULL as IsCurrentClaim, 

--settlements and claimants, values through different joins for lawsuits
l.docketnumber, 
l.CaseCaption,
s.[State], 

--lawsuits only
j.jurisdiction,
l.leadplaintifflastname+', '+l.leadplaintifffirstname AS LeadPlaintiff, 
l.filedate, 
l.servicedate, 
pca.PlaintiffCounsel AS primarylocalplaintiffcounsel,

--Invoices Only
NULL as InvoiceNumber,
NULL as Period,
NULL as DatePayable,
NULL as FeeAmount,
NULL as DisbursementAmount,
NULL as Adjustment,
NULL as AdjustmentNotes,
NULL as Status, 
NULL as ExpertFirm, 
NULL as SubmitDate, 
PrivateComments,
NULL as IsFullyPaid,

--Court Documents Only
NULL as DateOfFiling,
NULL as DateOfDeath,
DocketNumbers


FROM tblDocuments d
LEFT OUTER JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
LEFT OUTER JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
--lawsuits only
LEFT OUTER JOIN tblLawsuit l ON d.lawsuitid = l.lawsuitid
LEFT OUTER JOIN tblJurisdiction j ON l.jurisdictionid = j.jurisdictionid
LEFT OUTER JOIN tblState s ON l.stateid = s.stateid
LEFT OUTER JOIN tblDefenseCounselState sid ON l.DefenseCounselAddressID = sid.defensecounseladdressid AND l.stateid = sid.stateid
LEFT OUTER JOIN tblDefenseCounselAddress dca ON sid.responsibledefensecounseladdressid = dca.defensecounseladdressid 
LEFT OUTER JOIN tblDefenseCounsel dc on dc.DefenseCounselID=dca.defensecounselID
LEFT OUTER JOIN tblClaimLawsuit cl ON l.lawsuitid = cl.lawsuitid
LEFT OUTER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
LEFT OUTER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = d.ClaimantPersonalInfoID
LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
LEFT OUTER JOIN tblClaimantDocketNUmbers cdn ON d.ClaimantPersonalInfoID = cdn.ClaimantPersonalInfoID
LEFT OUTER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
LEFT OUTER JOIN tblDocumentSource ds ON d.DocumentSourceID = ds.DocumentSourceID
LEFT OUTER JOIN tblClaimlawsuitstatus cls ON d.claimlawsuitstatusID = cls.claimlawsuitstatusID
WHERE dg.documentgroupid = 3
AND d.DocumentID = @DocID
GROUP BY cast(d.DocumentID as varchar(50)), dt.ClaimantDocumentType, dg.DocumentGroup, d.Comments, d.DocumentURL, d.DocumentTitle, d.SPDocId,
d.CreatedBy, d.DateCreated, d.ModifiedBy, d.DateModified, d.Accepted, d.AcceptedBy, d.DateAccepted, d.ApprovedBy, d.DateApproved,
--settlements and claimants, values through different joins for lawsuits
l.docketnumber, l.CaseCaption, s.[State], 
--lawsuits only
j.jurisdiction, l.leadplaintifflastname+', '+l.leadplaintifffirstname, l.filedate, l.servicedate, pca.PlaintiffCounsel, f.Firm, COALESCE(cpi.lastname, '') +', '+COALESCE(cpi.firstname, '')+' '+COALESCE(cpi.middlename, ''), DocketNumbers,
PrivateComments,d.DocumentID, DismissalDate, QCDate, ClaimLawsuitStatus, DocumentSource, d.DocumentSourceID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDocuments] TO [power_user]
GO
