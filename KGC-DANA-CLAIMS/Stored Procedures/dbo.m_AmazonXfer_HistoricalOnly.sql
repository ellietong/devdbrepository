SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_AmazonXfer_HistoricalOnly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT  d.DocumentID, DocumentURL, REPLACE(REPLACE(DocumentTitle, '\', ''), '/', '') AS DocumentTitle,
			d.DetailDocumentTypeID,REPLACE(REPLACE(ClaimantDocumentType, '\', '_'), '/', '_') AS ClaimantDocumentType,
			 AmazonKey
    FROM tblDocuments d
    INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
   -- INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID=mcd.DocumentID
    WHERE LEFT(DocumentURL, 9) = '/archive/' 
    AND AmazonKey IS NULL 
    AND DocumentURL IS NOT NULL
    GROUP BY d.DocumentID, DocumentURL, REPLACE(REPLACE(DocumentTitle, '\', ''), '/', '') ,
			d.DetailDocumentTypeID,REPLACE(REPLACE(ClaimantDocumentType, '\', '_'), '/', '_'),
			 AmazonKey
    ORDER BY DocumentID ASC
  END
  
GO
