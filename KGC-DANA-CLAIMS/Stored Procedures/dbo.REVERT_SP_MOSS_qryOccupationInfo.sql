SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryOccupationInfo]
(@ClaimantPersonalInfoID int, @emailaddress nvarchar(250))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare @UserFirmState table (DefenseCounselID bigint, StateID int)
Insert into @UserFirmState

Select dca.DefenseCounselID, StateID
From dbo.tblDefenseCounselState AS dcs 
left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid --and dcar.stateID = dcs.StateID 
LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
Where (ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) OR
dca.DefenseCounselID in (SELECT f.FirmID FROM vtblPerson p inner join vtblFirm f on p.FirmID=f.FirmID WHERE Email = @emailaddress))


IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR(SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress) IN (132, 133, 114) OR ((Select PersonID FROM vtblPerson WHERE Email = @emailaddress) IN (1376,1377,1378,1383,1384,1389,1392,1404))
BEGIN
    -- Insert statements for procedure here
	SELECT ce.ClaimantEmploymentID, COALESCE(o.OccupationID, 1) AS OccupationID, o.Occupation, Jobsite, EmpStartDate, EmpEndDate, cpi.FirstName + ' ' + cpi.LastName AS Claimant,
			JobsiteCity, JobsiteStateID, s.State AS JobsiteState, IsPrimary
	FROM tblClaimantEmployment ce 
	INNER JOIN tblClaimantPersonalInfo cpi ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT Join tblOccupation o on o.OccupationID = ce.OccupationID
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblState s ON ce.JobsiteStateID = s.StateID
	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	ORDER BY IsPrimary DESC
END
ELSE
BEGIN
	SELECT DISTINCT ce.ClaimantEmploymentID, COALESCE(o.OccupationID, 1) AS OccupationID, o.Occupation, Jobsite, EmpStartDate, EmpEndDate, cpi.FirstName + ' ' + cpi.LastName AS Claimant,
	JobsiteCity, JobsiteStateID, s.State AS JobsiteState, IsPrimary
	FROM tblClaimantEmployment ce 
	INNER JOIN tblClaimantPersonalInfo cpi ON ce.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT Join tblOccupation o on o.OccupationID = ce.OccupationID
	LEFT JOIN tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
	LEFT JOIN tblState s ON ce.JobsiteStateID = s.StateID
	INNER JOIN tblDefenseCounselState dcs on l.DefenseCounselAddressID=dcs.DefenseCounselAddressID and l.StateID=dcs.StateID
	INNER JOIN tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCounselAddressID=dca.DefenseCounselAddressID
	INNER JOIN @UserFirmState u on dca.DefenseCounselID = u.DefenseCounselID and l.StateID = u.StateID
	WHERE ce.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
	ORDER BY IsPrimary DESC
	--AND DefenseCounselAddressID in (SELECT   DISTINCT  dcs.DefenseCounseladdressID
 --                           FROM          dbo.tblDefenseCounselState AS dcs INNER JOIN
	--										dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
	--										LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
	--										LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID INNER JOIN
 --                                                  dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
 --                           WHERE ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress))
END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryOccupationInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryOccupationInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryOccupationInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryOccupationInfo] TO [power_user]
GO
