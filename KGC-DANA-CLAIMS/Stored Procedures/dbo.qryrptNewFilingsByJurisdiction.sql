SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptNewFilingsByJurisdiction]
(@ResponsibleDefenseCounselAddressID int, @StateID int, @BeginDate datetime, @EndDate datetime)
As 
Set NoCount On


--DECLARE @ResponsibleDefenseCounselAddressID int
--SET @ResponsibleDefenseCounselAddressID = 3

--DECLARE @StateID INT
--SET @StateID = 40
--DECLARE @BEGINDATE DATETIME
--SET @BEGINDATE = null
--DECLARE @ENDDATE DATETIME
--SET @ENDDATE = null



DECLARE @maxStateID int
SELECT @maxStateID = MAX(StateID) FROM tblState

Declare @AllResponsibleDefenseCounsel table (DefenseCounselAddressID int, 
				DefenseCounsel nvarchar(100), 
				ResponsibleDefenseCounselAddressID int,
				StateID int, 
				ResponsibleDefenseCounsel nvarchar(100),
				IsActiveCounsel bit)
Insert Into @AllResponsibleDefenseCounsel
Select dcs.DefenseCounselAddressID,
	d.DefenseCounsel+' - '+d.DefenseCounselCity as DefenseCounsel,
	dcs.ResponsibleDefenseCounselAddressID,
	StateID as StateID,
	r.DefenseCounsel+' - '+r.DefenseCounselCity  as ResponsibleDefenseCOunsel,
	IsActiveCounsel
From tblDefenseCounselState dcs 
	Inner Join tblDefenseCounselAddress d on d.DefenseCounselAddressID = dcs.DefenseCounselAddressID
	Inner JOin tblDefenseCounselAddress r on r.DefenseCounselAddressID = dcs.ResponsibleDefenseCounselAddressID
Group By d.DefenseCounsel+' - '+d.DefenseCounselCity, r.DefenseCounsel+' - '+r.DefenseCounselCity, dcs.DefenseCounselAddressID, dcs.ResponsibleDefenseCounselAddressID, IsActiveCounsel, StateID


-- Find Max ID Values
Declare @MaxResponsibleDefenseCounselAddressID int

--Define Max State ID
Select @MaxResponsibleDefenseCounselAddressID = MAX(ResponsibleDefenseCounselAddressID) From @AllResponsibleDefenseCounsel
If @ResponsibleDefenseCounselAddressID = 0 Set @ResponsibleDefenseCounselAddressID = Null


Declare @DiseaseCount Table (State nvarchar(150), Jurisdiction nvarchar(250), PrimaryLocalPlaintiffCounsel nvarchar(1000),
	YearFiled datetime, MesoCount int, LungCancerCount int, OtherCancerCount int, NonMaligCount int, UnknownCount int)
Insert into @DiseaseCount

Select S.FullNameState as state, Jurisdiction, pca.PlaintiffCounsel+' - '+pca.PlaintiffCounselCity as PrimaryLocalPlaintiffCounsel,
convert(datetime,('1/1/'+convert(nvarchar,Year(ServiceDate)))) as YearFiled,
--Meso
Case	when Meso.HighLevelDiseaseID = 1 Then 1
Else 0 End AS MesoCount,
--Lung Cancer
Case	when LC.HighLevelDiseaseID = 2 Then 1
Else 0 End as LungCancerCount,
--Other Cancer
Case	when OC.HighLevelDiseaseID = 3 Then 1
Else 0 End as OtherCancerCount,
--NonMalig
Case	when NM.HighLevelDiseaseID = 4 Then 1
Else 0 End as NonMaligCount,
--Unknown
Case	when Unk.HighLevelDiseaseID = 5 Then 1
		When Unk.HighLevelDiseaseID is Null Then 1
Else 0 End AS UnknownCount

From tblClaimantPersonalInfo CPI
Left Join tblClaim C on CPI.ClaimantPersonalINfoID=C.ClaimantPersonalInfoID
LEFT Join (Select CD.ClaimDiseaseID, CD.ClaimID, CD.DiseaseDiagnosisDate, CD.DiseaseID, CD.DoctorID, CD.PrimaryDisease From tblClaimDisease CD Where PrimaryDisease = 1) CD on CD.ClaimID=C.ClaimID
--Meso
Left Join tblDisease Meso on CD.DiseaseID=Meso.DiseaseID

--Lung Cancer
Left Join tblDisease LC on CD.DiseaseID=LC.DiseaseID

--Other Cancer
Left Join tblDisease OC on CD.DiseaseID=OC.DiseaseID

--Non-Malig
Left Join tblDisease NM on CD.DiseaseID=NM.DiseaseID

--Unknown
Left Join tblDisease Unk on CD.DiseaseID=Unk.DiseaseID

Left Join tblClaimLawsuit CL on C.ClaimID=CL.ClaimID
Left Join tblLawsuit L on CL.LawsuitID=L.LawsuitID
Left Join tblJurisdiction J on L.JurisdictionID=J.JurisdictionID
--Left Join tblDefenseCounselState DCS on L.DefenseCounselAddressID=DCS.DefenseCounselAddressID AND L.StateID=DCS.StateID
Left Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
Left Join tblState S on L.StateID=S.StateID
Left Join @AllResponsibleDefenseCounsel r on r.DefenseCounselAddressID = l.DefenseCounselAddressID and r.StateID = l.StateID


Where  s.StateID >= COALESCE(@StateID, 0) AND s.StateID <= COALESCE(NULLIF(@StateID, 0), @maxStateID)
 and 
 ServiceDate between coalesce(@BeginDate, '1/1/1800') and coalesce(@EndDate, '1/1/2050')
 --and IsPrimaryForClaim = 1
	--AND ((r.ResponsibleDefenseCounselAddressID >= COALESCE(@ResponsibleDefenseCounselAddressID, 0) AND r.ResponsibleDefenseCounselAddressID <= COALESCE(@ResponsibleDefenseCounselAddressID, @MaxResponsibleDefenseCounselAddressID)))

Order By State, Year(ServiceDate), Jurisdiction, PrimaryLocalPlaintiffCounsel


Select State, Year(YearFiled) AS 'OpenYear', year(@enddate) as 'CloseYear', Jurisdiction, PrimaryLocalPlaintiffCounsel, Sum(MesoCount) as MesoCount,
	Sum(LungCancerCount) as LungCancerCount, Sum(OtherCancerCount) as OtherCancerCount,
	Sum(NonMaligCount) as NonMaligCount, Sum(UnknownCount) as UnknownCount
from @DiseaseCount
Group By State, YearFiled, Jurisdiction, PrimaryLocalPlaintiffCounsel

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByJurisdiction] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByJurisdiction] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNewFilingsByJurisdiction] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNewFilingsByJurisdiction] TO [power_user]
GO
