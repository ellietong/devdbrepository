SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmCheckAddAll] (@FundingTypeID int, @CheckID int, @ExpertFirmID int = NULL, @PlaintiffCounselAddressID int = NULL, @DefenseCounselAddressID int = NULL)
AS
Set NoCount On

Declare @SettlementCheck int
Set @SettlementCheck = (Select COUNT(*) from tblPaymentSettlement where CheckID = @CheckID)

Declare @DefenseCheck int
Set @DefenseCheck = (Select COUNT(*) from tblPaymentDefense where CheckID = @CheckID)

Declare @LegalBillCheck int
Set @LegalBillCheck = (Select COUNT(*) from tblPaymentLegalBill where CheckID = @CheckID)

Declare @ApprovedCheck int
Set @ApprovedCheck = (Select Case when ca.CheckID is not null Then 1 Else 0 End
	From tblCheck c Left Join tblCheckApproval ca on c.CheckID = ca.CheckID where c.CheckID = @CheckID)

If @FundingTypeID = 1 Begin --Expert Invoices

Declare @CurrentFirmID1 int
Set @CurrentFirmID1 = (@ExpertFirmID)
Declare @AcceptableFirmID1 int
Set @AcceptableFirmID1 = (Select Coalesce(ExpertFirmID,@CurrentFirmID1) From tblPaymentDefense pd Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID Where CheckID = @CheckID Group By Coalesce(ExpertFirmID,@CurrentFirmID1))

	If ((@AcceptableFirmID1 = @CurrentFirmID1 AND @ApprovedCheck = 0) Or (@AcceptableFirmID1 is Null)) AND @SettlementCheck = 0 AND @LegalBillCheck = 0 Begin
		Insert Into tblPaymentDefense (DefenseInvoiceID, CheckID, PaymentAmount)
		Select di.DefenseInvoiceID, @CheckID, InvoiceTotal-SUM(Coalesce(PaymentAmount,0))
		from tblDefenseInvoice di
			left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
						 a on a.DefenseInvoiceID = di.DefenseInvoiceID
		where ExpertFirmID = @ExpertFirmID and FundsRequestDate is not null and di.DatePaymentApproved IS NOT NULL
		Group By di.DefenseInvoiceID, InvoiceTotal
		Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0
	End
	
End

If @FundingTypeID = 2 Begin -- Settlements

Declare @CurrentFirmID2 int
Set @CurrentFirmID2 = @PlaintiffCounselAddressID
Declare @AcceptableFirmID2 int
Set @AcceptableFirmID2 = (Select Coalesce(PlaintiffCounselAddressID, @CurrentFirmID2) From tblPaymentSettlement ps 
	Inner Join tblClaimantSettlement cs on cs.ClaimantSettlementID = ps.ClaimantSettlementID
	Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID 
	Where CheckID = @CheckID
	Group By Coalesce(PlaintiffCounselAddressID, @CurrentFirmID2))
	
	If (@AcceptableFirmID2 = @CurrentFirmID2 AND @ApprovedCheck = 0 AND @DefenseCheck = 0 AND @LegalBillCheck = 0) Or (@AcceptableFirmID2 is Null AND @DefenseCheck = 0 AND @LegalBillCheck = 0) Begin
		Insert Into tblPaymentSettlement (ClaimantSettlementID, CheckID, PaymentAmount)
		Select cs.ClaimantSettlementID, @CheckID, SettlementAmount-SUM(Coalesce(PaymentAmount,0))
		From tblClaim c
			INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
			INNER JOIN tblClaimantSettlement cs			 ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
			INNER JOIN tblLawsuit l						 ON l.LawsuitID = cl.LawsuitID
			LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID
			LEFT JOIN (Select ClaimantSettlementID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentSettlement ps on ca.CheckID = ps.CheckID)
				 a on a.ClaimantSettlementID = cs.ClaimantSettlementID
		Where COALESCE(IsCurrentClaim,0) = 1
			AND l.PlaintiffCounselAddressID = @PlaintiffCounselAddressID
			AND c.ClaimSequenceID not in (4,2,3,11)
			--AND ExecutedReleaseDate <= GETDATE()
			AND cs.PaymentApprovedDate is not NULL
		Group By cs.ClaimantSettlementID, SettlementAmount
		Having SettlementAmount-SUM(Coalesce(PaymentAmount,0)) <> 0
	End

End

If @FundingTypeID = 3 Begin --Legal Bills

Declare @CurrentFirmID3 int
Set @CurrentFirmID3 = (@DefenseCounselAddressID)
Declare @AcceptableFirmID3 int
Set @AcceptableFirmID3 = (Select Coalesce(DefenseCounselAddressID,@CurrentFirmID3) From tblPaymentLegalBill pd Inner Join m_tblLegalBillInvoices di on di.InvoiceID = pd.InvoiceID Where CheckID = @CheckID Group By Coalesce(DefenseCounselAddressID,@CurrentFirmID3))

	If (@AcceptableFirmID3 = @CurrentFirmID3 AND @ApprovedCheck = 0 AND @SettlementCheck = 0 AND @DefenseCheck = 0) Or (@AcceptableFirmID3 is Null AND @SettlementCheck = 0 AND @DefenseCheck = 0) Begin
		Insert Into tblPaymentLegalBill(InvoiceID, CheckID, PaymentAmount)
		Select lbi.InvoiceID, @CheckID, (Case When AppealFlag = 1 Then AppealTotal Else AdjustedTotal End) - SUM(Coalesce(PaymentAmount,0))
		from m_tblLegalBillInvoices lbi
			left join (select InvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentLegalBill lb on ca.CheckID = lb.CheckID)
					a on a.InvoiceID = lbi.InvoiceID
		Where DefenseCounselAddressID = @DefenseCounselAddressID and FundsRequestDate is not null AND lbi.DatePaymentApproved IS NOT NULL
		Group By lbi.InvoiceID, AppealFlag, AppealTotal, AdjustedTotal
		Having (Case When AppealFlag = 1 Then AppealTotal Else AdjustedTotal End)- SUM(Coalesce(PaymentAmount,0)) <> 0
	End

End

If @FundingTypeID = 4 Begin --General Invoices

Declare @CurrentFirmID4 int
Set @CurrentFirmID4 = (@DefenseCounselAddressID)
Declare @AcceptableFirmID4 int
Set @AcceptableFirmID4  = (Select Coalesce(DefenseCounselAddressID,@CurrentFirmID4) From tblPaymentDefense pd Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID Where CheckID = @CheckID Group By Coalesce(DefenseCounselAddressID,@CurrentFirmID4))

	If ((@AcceptableFirmID4 = @CurrentFirmID4 AND @ApprovedCheck = 0) Or (@AcceptableFirmID4 is Null)) AND @SettlementCheck = 0 AND @LegalBillCheck = 0 Begin
		Insert Into tblPaymentDefense (DefenseInvoiceID, CheckID, PaymentAmount)
		Select di.DefenseInvoiceID, @CheckID, InvoiceTotal-SUM(Coalesce(PaymentAmount,0))
		from tblDefenseInvoice di
			left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
				 a on a.DefenseInvoiceID = di.DefenseInvoiceID
		where DefenseCounselAddressID = @DefenseCounselAddressID and FundsRequestDate is not null AND di.DatePaymentApproved IS NOT NULL
		Group By di.DefenseInvoiceID, InvoiceTotal
		Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0
	End

End


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddAll] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddAll] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddAll] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddAll] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheckAddAll] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheckAddAll] TO [power_user]
GO
