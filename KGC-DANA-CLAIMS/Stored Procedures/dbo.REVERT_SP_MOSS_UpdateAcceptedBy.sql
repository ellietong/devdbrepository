SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_UpdateAcceptedBy]
	@DocId int,
	@AcceptedBy NVarChar(250)
AS
BEGIN
	Update tblDocuments
	Set Accepted = 1,
	AcceptedBy = @AcceptedBy,
	DateAccepted = GetDate()
WHERE DocumentID = @DocId
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_UpdateAcceptedBy] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_UpdateAcceptedBy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_UpdateAcceptedBy] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_UpdateAcceptedBy] TO [power_user]
GO
