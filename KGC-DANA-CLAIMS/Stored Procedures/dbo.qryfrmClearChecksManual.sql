SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Proc [dbo].[qryfrmClearChecksManual] 
as

SET NOCOUNT ON

SELECT     tblcheck.checkid, tblCheck.SourceNumber, tblCheck.SourceDate, 
			tblCheck.SourceAmount, tblCheckApproval.IsCheckCleared
FROM         tblCheck INNER JOIN tblCheckApproval ON tblCheck.CheckID = tblCheckApproval.CheckID 
WHERE     (tblCheckApproval.IsCheckCleared IS NULL) OR
                      (tblCheckApproval.IsCheckCleared = 0)
ORDER BY tblCheck.SourceNumber



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClearChecksManual] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClearChecksManual] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClearChecksManual] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClearChecksManual] TO [power_user]
GO
