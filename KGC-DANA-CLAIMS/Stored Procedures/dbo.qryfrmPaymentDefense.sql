SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qryfrmPaymentDefense] (@ExpertFirmID int)--, @MatterID int)  --CPJ Removed MatterID parameter
AS
SET NOCOUNT ON

select di.DefenseInvoiceID, ExpertFirmID, InvoiceNumber, MatterID, Period, InvoiceTotal, InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount
from tblDefenseInvoice di
left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
			 a on a.DefenseInvoiceID = di.DefenseInvoiceID
where ExpertFirmID = @ExpertFirmID 
--and MatterID = @MatterID  --CPJ Removed MatterID parameter
and FundsRequestDate is not null
and DatePaymentApproved is not null
Group By di.DefenseInvoiceID, ExpertFirmID, InvoiceNumber, Period, InvoiceTotal, MatterID
Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [power_user]
GO
