SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_REPORTS_qrySBNDByDisease] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @MonthID int, @YearID int)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

--declare @PendingDate date
--set @PendingDate ='10/1/2011'

--Declare @Month int
--Declare @Year int
--set @Month=9
--set @Year=2011

Declare @Month int
Declare @Year int
Declare @PendingDate date
set @Month=@MonthID
set @Year=@YearID
set @PendingDate= DATEADD(MONTH,1,convert(date,convert(varchar,@Month)+'/1/'+convert(varchar,@Year)))


    -- Insert statements for procedure here
select Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end as HighLevelDisease, 
COUNT(distinct(cl.ClaimLawsuitID)) as Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimDisease cd on c.ClaimID=cd.ClaimID
left join tblDisease d on d.DiseaseID=cd.DiseaseID
left join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
left join tblHighLevelDisease hld on hld.HighLevelDiseaseID=d.HighLevelDiseaseID or hld.HighLevelDiseaseID=da.HighLevelDiseaseID
where	StatusDate<@PendingDate
		and ClaimLawsuitStatusID in (25) 
		and IsPrimaryForClaim = 1
		and  IsPrimaryStatus=1
		and cd.PrimaryDisease = 1
		and MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, hld.HighLevelDiseaseID
order by hld.HighLevelDiseaseID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySBNDByDisease] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySBNDByDisease] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySBNDByDisease] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qrySBNDByDisease] TO [power_user]
GO
