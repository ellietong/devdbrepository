SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[qryrptNoticeWeeklySummary]
(@Site nvarchar(100) = null, @insprog int = null)
as
SET NOCOUNT ON


------TEST VARS
--DECLARE @site nvarchar
--DECLARE @carriergroupID nvarchar

--DECLARE @insprog int = 0
--Set @site = null;
--set @carrierGroupID = null;
------END TEST VARS

DECLARE @ClientName nvarchar(100)
DECLARE @LawFirmName nvarchar(100)

SET @ClientName = (select internalcompany from tblInternalCompany where InternalCompanyID = 3)
SET @LawFirmName = (select internalcompany from tblInternalCompany where InternalCompanyID=2)


DECLARE @MaxSiteID Int
DECLARE @MaxclaimID Int
DECLARE @maxinsprog int
SELECT @MaxSiteID = MAX(siteid) from tblEnviroSitesDetail
SELECT @maxinsprog = MAX(insuranceprogramID) from tblPolicy
SELECT      cor.CorrespondenceID, cor.CorrespondenceDate, cor.CorrespondenceTypeID, ct.CorrespondenceType, s.SiteName, cor.Notes, 
                        cor.CorrespondenceModeID, 0 AS dstask, 0 AS ktask, 0 AS ptask, CASE WHEN Cor.CorrespondenceDate >= DATEADD(day, - 7, CURRENT_TIMESTAMP)
                         THEN 0 WHEN (Cor.CorrespondenceDate <= DATEADD(day, - 8, CURRENT_TIMESTAMP) AND Cor.CorrespondenceDate >= DATEADD(day, - 31, 
                        CURRENT_TIMESTAMP)) THEN 8 WHEN (Cor.CorrespondenceDate <= DATEADD(day, - 32, CURRENT_TIMESTAMP) AND 
                        Cor.CorrespondenceDate >= DATEADD(day, - 61, CURRENT_TIMESTAMP)) THEN 31 ELSE 61 END AS DateGroup, com.CorrespondenceMode, @LawFirmName as LawFirm, 
                        @ClientName as client
FROM          dbo.tblCorrespondence cor LEFT OUTER JOIN
                        dbo.tblCorrespondenceType ct ON cor.CorrespondenceTypeID = ct.CorrespondenceTypeID LEFT OUTER JOIN
                        dbo.tblSiteNoticed sn ON cor.CorrespondenceID = sn.CorrespondenceID INNER JOIN
                        dbo.tblEnviroSitesDetail s ON sn.SiteID = s.SiteID INNER JOIN
                        dbo.tblFollowUp fo ON cor.CorrespondenceID = fo.CorrespondenceID INNER JOIN
                        dbo.tblCorrespondenceMode com ON cor.CorrespondenceModeID = com.CorrespondenceModeID
						INNER JOIN tblPolicyNoticed pn ON pn.CorrespondenceID = cor.CorrespondenceID
                        INNER JOIN tblPolicy p on pn.PolicyID = p.PolicyID
WHERE       (fo.Completed = 0) AND sn.SiteID >= coalesce(@site, 0) and sn.SiteID <= coalesce(nullif(@site, 0), @maxsiteid)
			
			AND coalesce(p.InsuranceProgramID, 0) >= coalesce(@insprog, 0) and coalesce(p.InsuranceProgramID, 0)  <= coalesce(nullif(@insprog, 0), @maxinsprog)

GROUP BY cor.CorrespondenceID, cor.CorrespondenceDate, cor.CorrespondenceTypeID, ct.CorrespondenceType, s.SiteName, 
                        CASE WHEN Cor.CorrespondenceDate >= DATEADD(day, - 7, CURRENT_TIMESTAMP) THEN 0 WHEN (Cor.CorrespondenceDate <= DATEADD(day, - 8, 
                        CURRENT_TIMESTAMP) AND Cor.CorrespondenceDate >= DATEADD(day, - 31, CURRENT_TIMESTAMP)) 
                        THEN 8 WHEN (Cor.CorrespondenceDate <= DATEADD(day, - 32, CURRENT_TIMESTAMP) AND Cor.CorrespondenceDate >= DATEADD(day, - 61, 
                        CURRENT_TIMESTAMP)) THEN 31 ELSE 61 END, cor.Notes, cor.CorrespondenceModeID, com.CorrespondenceMode
ORDER BY cor.CorrespondenceID

Return




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptNoticeWeeklySummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptNoticeWeeklySummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptNoticeWeeklySummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptNoticeWeeklySummary] TO [power_user]
GO
