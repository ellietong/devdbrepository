SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusGroupList]
AS
SET NOCOUNT ON

SELECT ClaimLawsuitStatusGroupID, 
	ClaimLawsuitStatusGroup
FROM tblClaimLawsuitStatusGroup
UNION
SELECT NULL, ''
ORDER BY ClaimLawsuitStatusGroup
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusGroupList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusGroupList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusGroupList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusGroupList] TO [power_user]
GO
