SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Proc [dbo].[qryrptClaimLawsuitInfoQCNew] (@ClaimantPersonalInfoID int, @claimID int, @matterID int)
as
--DECLARE @ClaimantPersonalInfoID int, @claimID int, @matterID int
--set @ClaimantPersonalInfoID = 57232
--SEt @matterID = 1
--Set @claimID = 10988


Select --Claim Info
	ClaimantPersonalInfoID,
	IsCurrentClaim,
	ClaimSequence,
	Matter,
	c.MatterID,
	c.ClaimID, 
	cl.ClaimLawsuitID,
	cl.LawsuitID,
	IndividualDocketNumber,
	IsPrimaryForClaim,
 CaseCaption, DocketNumber, FileDate, Jurisdiction, S.State, CourtType, ServiceDate,
	ReceivedDate, DefenseCounsel + ' - ' + DefenseCounselCity as DefenseCounsel, PlaintiffCounsel + ' - ' + PlaintiffCounselCity as PlaintiffCounsel, LeadPlaintiffFirstName,
	LeadPlaintiffLastName, ActiveTrialDate, ctl.ComplaintTrackingID
From tblClaim c
	Left Join tblClaimLawsuit cl on c.claimID = cl.claimID
	Left Join tblClaimSequence cs on cs.claimsequenceID = c.claimsequenceID
	Left Join tblMatter m on c.MatterID = m.MatterID
left join tblLawsuit L on l.LawsuitID = cl.LawsuitID
Left Join tblJurisdiction J on J.JurisdictionID=L.JurisdictionID
Left Join tblState S on S.StateID=L.StateID
Left Join tblCourtType CT on CT.CourtTypeID=L.CourtTypeID
Left Join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = L.DefenseCounselAddressID
Left Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = L.PlaintiffCounselAddressID
left join tblComplaintTrackingLawsuit ctl on ctl.LawsuitID = l.LawsuitID

Where c.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and c.MatterID = @matterID And c.ClaimID = @ClaimID
Order By IsPrimaryForClaim desc


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitInfoQCNew] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitInfoQCNew] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptClaimLawsuitInfoQCNew] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptClaimLawsuitInfoQCNew] TO [power_user]
GO
