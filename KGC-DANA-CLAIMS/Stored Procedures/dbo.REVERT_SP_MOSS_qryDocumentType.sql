SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[REVERT_SP_MOSS_qryDocumentType] (@invoiceFlag int=0, @settlementFlag bit=0)

AS
SET NOCOUNT ON
--DECLARE @invoiceFlag bit
--SET @invoiceFlag = 0
--DECLARE @settlementFlag bit
--SET @settlementFlag = 0
IF @invoiceFlag = 0
BEGIN
	if @settlementFlag = 1
	BEGIN
		Select 
		ClaimantDocumentTypeID, 
		ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID IN (1, 3, 4)
		AND ClaimantDocumentTypeID <> 1
		ORDER BY ClaimantDocumentType
	END
	ELSE
	BEGIN
		Select 
			ClaimantDocumentTypeID, 
			ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID IN (1, 3)
		AND ClaimantDocumentTypeID <> 1
		ORDER BY ClaimantDocumentType
	END
END
ELSE if @invoiceFlag = 1
BEGIN
		Select 
			ClaimantDocumentTypeID, 
			ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID = 2 AND ClaimantDocumentTypeID NOT IN (41, 42)
		ORDER BY ClaimantDocumentType
END
ELSE
BEGIN
		Select 
			ClaimantDocumentTypeID, 
			ClaimantDocumentType, DocumentGroupID
		from tblClaimantDocumentType
		WHERE DocumentGroupID = 2
		ORDER BY ClaimantDocumentType
END

GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDocumentType] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDocumentType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryDocumentType] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryDocumentType] TO [power_user]
GO
