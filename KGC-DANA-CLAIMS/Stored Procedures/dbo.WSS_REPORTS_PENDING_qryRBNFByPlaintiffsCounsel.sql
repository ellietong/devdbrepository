SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel]  
(
	 @MatterEntityID int,
	 @MatterID int = NULL, 
	 @MonthID  int, 
	 @YearID   int
 )

 AS
SET NOCOUNT ON

 ----The following variables can be used for testing.
--DECLARE @MatterEntityID int
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MatterEntityID = 1
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @WhereString nvarchar(max)
SET @WhereString = '
	WHERE ClaimLawsuitStatusID IN (34) 
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
'

DECLARE @maxMatterID int
SELECT @maxMatterID = 6

DECLARE @Month int
DECLARE @Year int
DECLARE @PendingDate date
SET @Month = @MonthID
SET @Year = @YearID
SET @PendingDate = DATEADD(MONTH,1,CONVERT(date,CONVERT(varchar,@Month)+'/1/'+CONVERT(varchar,@Year)))

IF @PendingDate		IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND ProcessedDate < ' + '''' + CONVERT(nvarchar(20),  @PendingDate) + '''' END
IF @MatterEntityID	IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND MatterEntityID = ' + CONVERT(nvarchar(20),@MatterEntityID) END
IF @MatterID		IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar(20),@MatterID) END
IF @MatterID		IS NULL		  BEGIN SET @WhereString = @WhereString + ' AND c.MatterID <> 13 ' END


DECLARE @QueryString nvarchar(max)
SET @QueryString = '
SELECT pca.PlaintiffCounsel, COUNT(DISTINCT(cl.ClaimLawsuitID)) AS ''Count''
FROM tblClaim c 
	INNER JOIN tblMatter m						  ON c.MatterID = m.MatterID
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca     ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		'+ @WhereString +'
GROUP BY pca.PlaintiffCounsel
ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC, pca.PlaintiffCounsel
'
EXEC sp_executesql @QueryString




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] TO [power_user]
GO
