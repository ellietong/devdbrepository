SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryClaimantDefenseCostsByDefenseFirm] 
	-- Add the parameters for the stored procedure here
	(@ClaimantPersonalInfoID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 9494

	

Declare @InvoiceTotal table (ClaimantPersonalInfoID int, ExpertFirmID int, DefenseCounselAddressID int, DefenseTotal money)
Insert into @InvoiceTotal

--Invoices linked on NavLegacyIDs - Pre-KCIC transfer
Select  cpi.ClaimantPersonalInfoID, NULL as ExpertFirmID, a.DefenseCounselAddressID as DefenseCounselID, Case When AppealFlag = 1 then Sum(a.AppealTotal) else SUM(a.AdjustedTotal) END as DefenseTotal 
From tblClaimantPersonalInfo cpi
Inner Join m_tblLegalBillInvoices a on a.Navlegacyid=cpi.NavLegacyID
Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and cpi.ClaimantPersonalInfoID is null
Group By cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID, AppealFlag

Union

--Invoices linked on KCIC IDs
Select  cpi.ClaimantPersonalInfoID, NULL as ExpertFirmID, a.DefenseCounselAddressID as DefenseCounselID, Case When AppealFlag = 1 then Sum(a.AppealTotal) else SUM(a.AdjustedTotal) END as DefenseTotal 
From tblClaimantPersonalInfo cpi
Inner Join m_tblLegalBillInvoices a on a.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID --and a.DefenseCounselAddressID = @DefenseCounselID
Group By cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID, AppealFlag

Union

--Approved Expert and General Invoices with a ClaimantID link
Select CPI.ClaimantPersonalInfoID, di.ExpertFirmID, Di.DefenseCounselAddressID, SUM(DI.InvoiceTotal) as DefenseTotal
From tblDefenseInvoice DI
Inner Join tblClaimantPersonalInfo CPI on CPI.ClaimantPersonalInfoID=DI.ClaimantPersonalInfoID
Where DI.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and DI.StatusID in (1,2,5)
Group by CPI.ClaimantPersonalInfoID, di.ExpertFirmID, Di.DefenseCounselAddressID



Select	ClaimantPersonalInfoID, Coalesce(DefenseCounsel,ExpertFirm) as DefenseCounsel, Coalesce(i.DefenseCounselAddressID, 0) as DefenseCounselID, i.ExpertFirmID, Sum(DefenseTotal) as DefenseTotal 
From @InvoiceTotal i 
Left Join tblDefenseCounselAddress d on i.DefenseCounselAddressID = d.DefenseCounselAddressID
Left Join m_tblExpertFirm e on i.ExpertFirmID = e.ExpertFirmID
Group By ClaimantPersonalInfoID, DefenseCounsel, i.DefenseCounselAddressID, ExpertFirm, i.ExpertFirmID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantDefenseCostsByDefenseFirm] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantDefenseCostsByDefenseFirm] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantDefenseCostsByDefenseFirm] TO [power_user]
GO
