SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmBillGroup]
As

SET NOCOUNT ON

SELECT BillGroupID, BillGroupName
FROM tblBillGroup

UNION 

SELECT  0, '<<All Bill Groups>>' AS BillGroupName
FROM tblBillGroup

ORDER BY BillGroupName

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmBillGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmBillGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmBillGroup] TO [power_user]
GO
