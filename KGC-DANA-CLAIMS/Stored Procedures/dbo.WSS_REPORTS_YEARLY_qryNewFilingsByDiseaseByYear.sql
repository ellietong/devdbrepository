SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByDiseaseByYear] 
 (
	 @MatterEntityID int,
	 @MatterID int = NULL, 
	 @MonthID  int, 
	 @YearID   int
 )

 AS
SET NOCOUNT ON

 ----The following variables can be used for testing.
--DECLARE @MatterEntityID int
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MatterEntityID = 1
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE PrimaryDisease = 1 '

IF @MonthID			IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND MONTH(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) <= ' + '''' + CONVERT(nvarchar(20),@MonthID) + '''' END
IF @YearID			IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND YEAR(COALESCE(ServiceDate,FileDate,ReceivedDate,ProcessedDate)) = ' + '''' + CONVERT(nvarchar(20),@YearID) + '''' END
IF @MatterEntityID	IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND MatterEntityID = ' + CONVERT(nvarchar(20),@MatterEntityID) END
IF @MatterID		IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar(20),@MatterID) END
IF @MatterID		IS NULL		  BEGIN SET @WhereString = @WhereString + ' AND c.MatterID <> 13 ' END

DECLARE @QueryString nvarchar(max)
SET @QueryString = '
SELECT CASE WHEN hld.HighLevelDisease IS NULL THEN ''Unknown'' 
                                              ELSE hld.HighLevelDisease END AS HighLevelDisease, 
		COUNT(DISTINCT(cl.ClaimLawsuitID)) AS Count
FROM tblClaim c 
	INNER JOIN tblMatter m						  ON c.MatterID = m.MatterID
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
    LEFT JOIN (select * from tblClaimLawsuitStatusTracking where ClaimLawsuitStatusID=22) clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
    INNER JOIN tblClaimDisease cd                 ON c.ClaimID = cd.ClaimID
	LEFT  JOIN tblDisease d                       ON d.DiseaseID = cd.DiseaseID
	LEFT  JOIN tblDisease_ALT da                  ON cd.DiseaseID = da.DiseaseID_Alt
	LEFT  JOIN tblHighLevelDisease hld            ON hld.HighLevelDiseaseID = d.HighLevelDiseaseID OR hld.HighLevelDiseaseID = da.HighLevelDiseaseID
		'+ @WhereString +'
GROUP BY CASE WHEN hld.HighLevelDisease IS NULL THEN ''Unknown'' ELSE hld.HighLevelDisease END, hld.HighLevelDiseaseID
ORDER BY hld.HighLevelDiseaseID
'
EXEC sp_executesql @QueryString




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByDiseaseByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByDiseaseByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByDiseaseByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryNewFilingsByDiseaseByYear] TO [power_user]
GO
