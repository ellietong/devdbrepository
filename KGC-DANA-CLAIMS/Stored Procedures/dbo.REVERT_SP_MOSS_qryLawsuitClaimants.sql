SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryLawsuitClaimants]
(@LawsuitID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CPI.ClaimantPersonalInfoID AS ClaimantID, CPI.LastName + ', ' + CASE WHEN FirstName IS NULL 
                      THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName
	FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo CPI ON c.ClaimantPersonalInfoID = CPI.ClaimantPersonalInfoID
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
	WHERE cl.LawsuitID = @LawsuitID
	ORDER BY LastName, FirstName
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryLawsuitClaimants] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryLawsuitClaimants] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryLawsuitClaimants] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryLawsuitClaimants] TO [power_user]
GO
