SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[REVERT_SP_MOSS_ClaimsReports]
(@ClaimLawsuitStatusID int, @HighLevelDiseaseID int, @StateID int, @PlaintiffCounselID bigint, @JurisdictionID int, @MatterID int, 
@IsMDL int, @emailaddress nvarchar(250), @AllegationTypeID int)
AS
BEGIN

--DECLARE @ClaimLawsuitStatusID int
--SET @ClaimLawsuitStatusID = null
--DECLARE @HighLevelDiseaseID int
--SET @HighLevelDiseaseID = null
--DECLARE @StateID int
--SET @StateID = null
--DECLARE @PlaintiffCounselID bigint
--SET @PlaintiffCounselID = 10
--DECLARE @DefenseCounselAddressID int
--SET @DefenseCounselAddressID = null
--DECLARE @JurisdictionID int
--SET @JurisdictionID = null
--DECLARE @Paid bit
--SET @Paid = null
--DECLARE @MatterID int
--SET @MatterID = null
--DECLARE @IsMDL int
--SET @IsMDL = 0
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'crookc@kcicllc.com' 

DECLARE @MaxHighLevelDiseaseID int
SET @MaxHighLevelDiseaseID = (SELECT MAX(HighLevelDiseaseID) FROM tblHighLevelDisease)
DECLARE @MaxStateID int
SET @MaxStateID = (SELECT MAX(StateID) FROM tblState)
DECLARE @MaxPlaintiffCounselID int
SET @MaxPlaintiffCounselID = (SELECT MAX(PlaintiffCounselID) FROM tblPlaintiffCounselAddress)
--DECLARE @MaxDefenseCounselAddressID int
--SET @MaxDefenseCounselAddressID = (SELECT MAX(DefenseCounselAddressID) FROM tblDefenseCounselAddress)
DECLARE @MaxJurisdictionID int
SET @MaxJurisdictionID = (SELECT MAX(JurisdictionID) FROM tblJurisdiction)
DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(MatterID) FROM tblMatter)
DECLARE @MaxClaimLawsuitStatusID int
SET @MaxClaimLawsuitStatusID = (SELECT MAX(ClaimLawsuitStatusID) FROM tblClaimLawsuitStatus)
DECLARE @MaxMDL int
SET @MaxMDL = 1
DECLARE @MaxAllegationID int
SET @MaxAllegationID = (SELECT MAX(AllegationTypeID) FROM tblAllegationType)


Declare @UserFirmState table (DefenseCounselID bigint, StateID int)
Insert into @UserFirmState

Select dca.DefenseCounselID, StateID
From dbo.tblDefenseCounselState AS dcs 
left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid --and dcar.stateID = dcs.StateID 
LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
Where (ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) OR
dca.DefenseCounselID in (SELECT f.FirmID FROM vtblPerson p inner join vtblFirm f on p.FirmID=f.FirmID WHERE Email = @emailaddress))


DECLARE @FirmID int
SET @FirmID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)

IF(@FirmID IN (132, 133, 114)) OR RIGHT(@emailaddress, 11) = 'kcicllc.com' OR ((Select PersonID FROM vtblPerson WHERE Email = @emailaddress) IN (1376,1377,1378,1383,1384,1389,1392,1404))
BEGIN
	SELECT     DISTINCT(C.ClaimID), c.ClaimantPersonalInfoID, IsMDL, DHLD.HighLevelDisease AS Disease, S.State, J.Jurisdiction, cs.ClaimSequence, 
						  CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
						  THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
						  BirthDate, CPI.DeceasedDate, cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) 
						  AS FileDate, L.DocketNumber, L.CaseCaption, L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName, 
						  pca.PlaintiffCounsel, r_1.ResponsibleDefenseCounsel AS PrimaryDefenseCounsel, m.Matter, l.ActiveTrialDate, a.AllegationTypeID, AllegationType,
						  CASE WHEN clst.ClaimLawsuitStatusID IN (15,16,17, 36, 19, 27) THEN clst.StatusDate WHEN  clst.ClaimLawsuitStatusID in (23, 25) THEN csett.SettlementDate ELSE NULL END AS ResolutionDate
	FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
						  dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
						  dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
						  dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID 
						  left join tblplaintiffcounselAddress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid INNER JOIN
						  dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
						  dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
						  dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
							  (SELECT     dcs.DefenseCounselAddressID, d.DefenseCounsel, dcs.ResponsibleDefenseCounselAddressID, dcs.StateID, 
													   r.DefenseCounsel AS ResponsibleDefenseCounsel, dcs.IsActiveCounsel
								FROM          dbo.tblDefenseCounselState AS dcs LEFT JOIN
													   dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid LEFT join
													   dbo.tblDefenseCounselAddress dcar on dcar.defensecounseladdressid = dcs.responsibledefensecounseladdressid LEFT join
													   dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID LEFT JOIN
													   dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
								GROUP BY d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounseladdressID, dcs.ResponsibleDefenseCounseladdressID, dcs.IsActiveCounsel, dcs.StateID) 
						  AS r_1 ON r_1.DefenseCounseladdressID = L.DefenseCounseladdressID AND r_1.StateID = L.StateID LEFT OUTER JOIN
						  (select * from dbo.tblClaimDisease where PrimaryDisease = 1) as cd on cd.ClaimID = c.ClaimID LEFT OUTER JOIN
						  dbo.tblDisease AS DDisease ON Cd.DiseaseID = DDisease.DiseaseID LEFT OUTER JOIN
						  dbo.tblDisease_ALT DA ON cd.DiseaseID = DA.DiseaseID_Alt LEFT OUTER JOIN
						  dbo.tblHighLevelDisease AS DHLD ON DHLD.HighLevelDiseaseID = DDisease.HighLevelDiseaseID OR DHLD.HighLevelDiseaseID = DA.HighLevelDiseaseID INNER JOIN
						  (SELECT * FROM tblClaimLawsuitStatusTracking WHERE IsPrimaryStatus = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID INNER JOIN
						  dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
						  LEFT OUTER JOIN tblClaimantSettlement csett ON cl.ClaimLawsuitID = csett.ClaimLawsuitID
						  INNER JOIN tblMatter M ON c.MatterID = m.MatterID
						  LEFT OUTER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
						  LEFT JOIN tblAllegationType at ON a.AllegationTypeID = at.AllegationTypeID
	WHERE     (COALESCE(CL.IsPrimaryForClaim, 0) = 1) 
				AND ((ddisease.HighLevelDiseaseID >= COALESCE(@HighLevelDiseaseID, 0) AND ddisease.HighLevelDiseaseID <= COALESCE(@HighLevelDiseaseID, @MaxHighLevelDiseaseID)) OR (DA.HighLevelDiseaseID >= COALESCE(@HighLevelDiseaseID, 0) AND DA.HighLevelDiseaseID <= COALESCE(@HighLevelDiseaseID, @MaxHighLevelDiseaseID)))
				AND l.StateID >= COALESCE(@StateID, 0) AND l.StateID <= COALESCE(@StateID, @MaxStateID)
				AND PCA.PlaintiffCounselID >= COALESCE(@PlaintiffCounselID, 0) AND pca.PlaintiffCounselID <= COALESCE(@PlaintiffCounselID, @MaxPlaintiffCounselID)
				--AND l.DefenseCounselAddressID >= COALESCE(@DefenseCounselAddressID, 0) AND l.DefenseCounselAddressID <= COALESCE(@DefenseCounselAddressID, @MaxDefenseCounselAddressID)
				AND l.JurisdictionID  >= COALESCE(@JurisdictionID, 0) AND l.JurisdictionID <= COALESCE(@JurisdictionID, @MaxJurisdictionID)
				AND clst.ClaimLawsuitStatusID >= COALESCE(@ClaimLawsuitStatusID, 0) AND clst.ClaimLawsuitStatusID <= COALESCE(@ClaimLawsuitStatusID, @MaxClaimLawsuitStatusID)
				--AND CASE WHEN PaidDate IS NOT NULL THEN 1 ELSE 0 END = @Paid
				AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(@MatterID, @MaxMatterID)
				AND IsMDL >= COALESCE(@IsMDL, 0) AND IsMDL <= COALESCE(@IsMDL, @MaxMDL)
				AND clst.IsPrimaryStatus = 1
				AND a.AllegationTypeID >= COALESCE(@AllegationTypeID, 0) AND a.AllegationTypeID <= COALESCE(@AllegationTypeID, @MaxAllegationID)

	END
ELSE
BEGIN
SELECT     DISTINCT(C.ClaimID), c.ClaimantPersonalInfoID, IsMDL, DHLD.HighLevelDisease AS Disease, S.State, J.Jurisdiction, cs.ClaimSequence, 
						  CPI.LastName + ', ' + CASE WHEN FirstName IS NULL THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL 
						  THEN '' ELSE MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-'+RIGHT(SocialSecurityNumber, 4) END AS SSN, 
						  BirthDate, CPI.DeceasedDate, cls.ClaimLawsuitStatus, CONVERT(date, L.ServiceDate) AS ServiceDate, CONVERT(date, L.FileDate) 
						  AS FileDate, L.DocketNumber, L.CaseCaption, L.LeadPlaintiffLastName + ', ' + L.LeadPlaintiffFirstName AS LeadPlaintiffName, 
						  pca.PlaintiffCounsel, r_1.ResponsibleDefenseCounsel AS PrimaryDefenseCounsel, m.Matter, l.ActiveTrialDate, a.AllegationTypeID, AllegationType,
						  CASE WHEN clst.ClaimLawsuitStatusID IN (15,16,17, 36, 19, 27) THEN clst.StatusDate WHEN  clst.ClaimLawsuitStatusID in (23, 25) THEN csett.SettlementDate ELSE NULL END AS ResolutionDate
	FROM         dbo.tblClaimantPersonalInfo AS CPI INNER JOIN
						  dbo.tblClaim AS C ON CPI.ClaimantPersonalInfoID = C.ClaimantPersonalInfoID INNER JOIN
						  dbo.tblClaimLawsuit AS CL ON C.ClaimID = CL.ClaimID INNER JOIN
						  dbo.tblLawsuit AS L ON CL.LawsuitID = L.LawsuitID 
						   INNER JOIN tblDefenseCounselState dcs on l.DefenseCounselAddressID=dcs.DefenseCounselAddressID and l.StateID=dcs.StateID
						  INNER JOIN tblDefenseCounselAddress dca on dcs.ResponsibleDefenseCounselAddressID=dca.DefenseCounselAddressID
						  INNER JOIN @UserFirmState u on dca.DefenseCounselID = u.DefenseCounselID and l.StateID = u.StateID
                      
						  left join tblplaintiffcounselAddress pca on pca.plaintiffcounseladdressid = L.plaintiffcounseladdressid INNER JOIN
						  dbo.tblState AS S ON S.StateID = L.StateID INNER JOIN
						  dbo.tblJurisdiction AS J ON J.JurisdictionID = L.JurisdictionID INNER JOIN
						  dbo.tblClaimSequence AS cs ON cs.ClaimSequenceID = C.ClaimSequenceID LEFT OUTER JOIN
							  (SELECT     dcs.DefenseCounselAddressID, d.DefenseCounsel, dcs.ResponsibleDefenseCounselAddressID, dcs.StateID, 
													   r.DefenseCounsel AS ResponsibleDefenseCounsel, dcs.IsActiveCounsel
								FROM          dbo.tblDefenseCounselState AS dcs LEFT JOIN
													   dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid LEFT join
													   dbo.tblDefenseCounselAddress dcar on dcar.defensecounseladdressid = dcs.responsibledefensecounseladdressid LEFT join
													   dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID LEFT JOIN
													   dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
								GROUP BY d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounseladdressID, dcs.ResponsibleDefenseCounseladdressID, dcs.IsActiveCounsel, dcs.StateID) 
						  AS r_1 ON r_1.DefenseCounseladdressID = L.DefenseCounseladdressID AND r_1.StateID = L.StateID LEFT OUTER JOIN
						  (select * from dbo.tblClaimDisease where PrimaryDisease = 1) as cd on cd.ClaimID = c.ClaimID LEFT OUTER JOIN
						  dbo.tblDisease AS DDisease ON Cd.DiseaseID = DDisease.DiseaseID LEFT OUTER JOIN
						  dbo.tblDisease_ALT DA ON cd.DiseaseID = DA.DiseaseID_Alt LEFT OUTER JOIN
						  dbo.tblHighLevelDisease AS DHLD ON DHLD.HighLevelDiseaseID = DDisease.HighLevelDiseaseID OR DHLD.HighLevelDiseaseID = DA.HighLevelDiseaseID INNER JOIN
						  (SELECT * FROM tblClaimLawsuitStatusTracking WHERE IsPrimaryStatus = 1) clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID INNER JOIN
						  dbo.tblClaimLawsuitStatus AS cls ON CLst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
						  LEFT OUTER JOIN tblClaimantSettlement csett ON cl.ClaimLawsuitID = csett.ClaimLawsuitID
						  INNER JOIN tblMatter M ON c.MatterID = m.MatterID
						  LEFT OUTER JOIN tblAllegation a ON cl.ClaimLawsuitID = a.ClaimLawsuitID
						  LEFT JOIN tblAllegationType at ON a.AllegationTypeID = at.AllegationTypeID
	WHERE     (COALESCE(CL.IsPrimaryForClaim, 0) = 1) 
				AND ((ddisease.HighLevelDiseaseID >= COALESCE(@HighLevelDiseaseID, 0) AND ddisease.HighLevelDiseaseID <= COALESCE(@HighLevelDiseaseID, @MaxHighLevelDiseaseID)) OR (DA.HighLevelDiseaseID >= COALESCE(@HighLevelDiseaseID, 0) AND DA.HighLevelDiseaseID <= COALESCE(@HighLevelDiseaseID, @MaxHighLevelDiseaseID)))
				AND l.StateID >= COALESCE(@StateID, 0) AND l.StateID <= COALESCE(@StateID, @MaxStateID)
				AND pca.PlaintiffCounselID >= COALESCE(@PlaintiffCounselID, 0) AND pca.PlaintiffCounselID <= COALESCE(@PlaintiffCounselID, @MaxPlaintiffCounselID)
				--AND l.DefenseCounselAddressID >= COALESCE(@DefenseCounselAddressID, 0) AND l.DefenseCounselAddressID <= COALESCE(@DefenseCounselAddressID, @MaxDefenseCounselAddressID)
				AND l.JurisdictionID  >= COALESCE(@JurisdictionID, 0) AND l.JurisdictionID <= COALESCE(@JurisdictionID, @MaxJurisdictionID)
				AND clst.ClaimLawsuitStatusID >= COALESCE(@ClaimLawsuitStatusID, 0) AND clst.ClaimLawsuitStatusID <= COALESCE(@ClaimLawsuitStatusID, @MaxClaimLawsuitStatusID)
				--AND CASE WHEN PaidDate IS NOT NULL THEN 1 ELSE 0 END = @Paid
				AND c.MatterID >= COALESCE(@MatterID, 0) AND c.MatterID <= COALESCE(@MatterID, @MaxMatterID)
				AND IsMDL >= COALESCE(@IsMDL, 0) AND IsMDL <= COALESCE(@IsMDL, @MaxMDL)
				AND clst.IsPrimaryStatus = 1
				AND a.AllegationTypeID >= COALESCE(@AllegationTypeID, 0) AND a.AllegationTypeID <= COALESCE(@AllegationTypeID, @MaxAllegationID)
				--AND l.DefenseCounselAddressID IN (SELECT   DISTINCT  dcs.DefenseCounseladdressID
    --                        FROM          dbo.tblDefenseCounselState AS dcs INNER JOIN
				--							dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
				--							LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid
				--							LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID INNER JOIN
    --                                               dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
    --                        WHERE ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
    --                        GROUP BY d.DefenseCounsel, r.DefenseCounsel, dcs.DefenseCounseladdressID, dcs.ResponsibleDefenseCounseladdressID, dcs.IsActiveCounsel, dcs.StateID)
END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_ClaimsReports] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_ClaimsReports] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_ClaimsReports] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_ClaimsReports] TO [power_user]
GO
