SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_WSS_qryDelDocument]
(@DocumentID int, @Emailaddress nvarchar(250))
AS
BEGIN
	SET NOCOUNT ON;
	
	--Select the DocumentURL so C# code knows the path of the file which is to be deleted
	SELECT DocumentURL FROM tblDocuments WHERE DocumentID = @DocumentID
	
	--Update the SPUsername field in tblMultiClaimantDocument prior to delete for audit log purposes
	UPDATE tblMultiClaimantDocument
		SET SPUsername = @emailaddress
	WHERE DocumentID = @DocumentID
	
	--Update the ModifiedBy field in tblDocuments prior to delete for audit log purposes
	UPDATE tblDocuments
		SET ModifiedBy = @emailaddress
	WHERE DocumentID = @DocumentID
	
	--This is used on ViewDoc to delete **ALL** tblDocument and tblMultiClaimantDocument records for a given DocumentID
	DELETE FROM tblMultiClaimantDocument WHERE DocumentID = @DocumentID
	DELETE FROM tblDocuments WHERE DocumentID = @DocumentID
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_WSS_qryDelDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_WSS_qryDelDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_WSS_qryDelDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_WSS_qryDelDocument] TO [power_user]
GO
