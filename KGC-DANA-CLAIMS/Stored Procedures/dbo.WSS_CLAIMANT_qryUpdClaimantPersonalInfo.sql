SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_CLAIMANT_qryUpdClaimantPersonalInfo] (@ClaimantPersonalInfoID bigint, 
																 @Address1				 nvarchar(250), 
																 @Address2				 nvarchar(250), 
																 @StateID				 int, 
																 @City					 nvarchar(250), 
																 @Zip					 nvarchar(5), 
																 @Mailzip				 nvarchar(4), 
																 @BirthDate				 datetime, 
																 @DeceasedDate			 datetime, 
																 @GenderID				 int, 
																 @MaritalStatusID		 int, 
																 @IsSmoker				 bit, 
																 @SmokerStart			 datetime, 
																 @SmokerEnd				 datetime, 
																 @PacksPerDay			 float,
																 @Username				 nvarchar(250), 
																 @FullSSN				 nvarchar(150))
AS

SET NOCOUNT ON

UPDATE tblClaimantPersonalInfo
	SET Address1 = UPPER(@Address1),
		Address2 = UPPER(@Address2),
		StateID = @StateID,
		City = UPPER(@City),
		Zip = @Zip,
		MailZip = @Mailzip,
		BirthDate = @BirthDate,
		DeceasedDate = @DeceasedDate,
		GenderID = @GenderID,
		MaritalStatusID = @MaritalStatusID,
		IsSmoker = @IsSmoker,
		SmokerStart = @SmokerStart,
		SmokerEnd = @SmokerEnd,
		PacksPerDay = @PacksPerDay,
		SPUsername = @Username,
		SocialSecurityNumber = REPLACE(REPLACE(@FullSSN, '-', ''), ' ', '')
	WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantPersonalInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantPersonalInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantPersonalInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_CLAIMANT_qryUpdClaimantPersonalInfo] TO [power_user]
GO
