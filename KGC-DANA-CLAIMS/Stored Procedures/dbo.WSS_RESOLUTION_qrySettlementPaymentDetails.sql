SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_RESOLUTION_qrySettlementPaymentDetails] (@ClaimantSettlementID bigint = NULL)
AS

SELECT cs.ClaimantSettlementID, 
	ps.PaymentAmount, 
	c.SourceNumber as CheckNumber, 
	c.SourceDate as PaymentDate, 
	CASE WHEN IsFullyPaid = 1							 THEN 'Settlement Fully Paid' 
		 WHEN IsFullyPaid = 0 AND SUM(PaymentAmount) > 0 THEN 'Settlement Not Fully Paid' 
														 ELSE 'No Payments Made' END AS SettlementPaymentStatus
FROM tblClaimantSettlement cs 
	LEFT JOIN tblPaymentSettlement ps	on cs.ClaimantSettlementID=ps.ClaimantSettlementID
	LEFT JOIN tblCheck c				on ps.CheckID=c.CheckID
WHERE cs.ClaimantSettlementID = @ClaimantSettlementID
GROUP BY cs.ClaimantSettlementID, ps.PaymentAmount, c.SourceNumber, c.SourceDate, IsFullyPaid
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qrySettlementPaymentDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qrySettlementPaymentDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_RESOLUTION_qrySettlementPaymentDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_RESOLUTION_qrySettlementPaymentDetails] TO [power_user]
GO
