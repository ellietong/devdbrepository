SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList]
AS
SET NOCOUNT ON

SELECT ClaimLawsuitStatusID, 
	ClaimLawsuitStatus 
FROM tblClaimLawsuitStatus
WHERE ClaimLawsuitStatusID IN (15, 16, 21, 22, 23, 25, 17, 19, 27, 34, 35, 37, 44, 45)
UNION
SELECT NULL, ''
ORDER BY ClaimLawsuitStatus
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryClaimantLawsuitStatusList] TO [power_user]
GO
