SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[REVERT_qryInternalKingAndSpaldingInvoiceLoaded] 
      
AS
BEGIN
/*Internal email to alert that King and Spalding has uploaded an invoice. - BEH*/

--Declare and set sp_send_dbmail input variables
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(max)

SET @Subject =    'Demo Client: National Coordinating Counsel Invoice Loaded ' + CONVERT(varchar, GETDATE(), 107) 
--Set the font for the entire email, and include the KCIC logo/banner on the top and the header for table 1
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>King & Spalding loaded the following invoice(s) on ' + CONVERT(varchar, GETDATE(), 107) + '.</H4>'

--Create temp table for invoices which have been uploaded by K&S in the past day.
DECLARE @NewInvoicesTable TABLE (spDocID nvarchar(100), firmID int, [status] nvarchar(30), invoiceNum nvarchar(100), 
            period nvarchar(100), fees  money, disbursement money, adjustment money, submitDate datetime, datePayable datetime)
INSERT INTO @NewInvoicesTable
SELECT SPDocID, 
      --Retrieve the firm's invoice site abbreviation by parsing the documentURL
      --There are two different cases. One for those firms who are categorized as Defense Counsels and 
      --the other for firms who are categorized only as Firms.
      FirmID, [Status], InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, DateCreated, DatePayable
FROM tblDocuments d LEFT JOIN tblDefenseInvoiceStatus s ON d.StatusID=s.StatusID
WHERE Datecreated BETWEEN GETDATE()-1 AND GETDATE() 
      AND detailDocumentTypeID = 0
      --This email only includes invoices uploaded by King and Spalding
      AND FirmID = 24
      and PATINDEX('%/dcp/%', DocumentURL) = 0
      and FirmID NOT IN (37)
GROUP BY DocumentURL, SPDocId, [Status], InvoiceNumber, Period, FeeAmount, DisbursementAmount, Adjustment, 
            DateCreated, DatePayable, FirmID

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
Set @BodyMessage = @BodyMessage + 
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {font-family: Goudy Old Style; 
      font-size: 10pt; 
      border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @bodymessage + N'<table>' + 
    N'<tr><th>Firm</th><th>Invoice Number</th><th>Period</th><th>Fees</th>' + 
      '<th>Expenses</th><th>Total Billed</th><th>Submit Date</th><th>Date Payable</th>' +
    CAST ( (
            SELECT  td = firm, '', td = invoicenum, '', td = period, '', 
                        td = '$'+convert(nvarchar(30), coalesce(fees, 0), 1), '', 
                        td = '$'+convert(nvarchar(30), coalesce(disbursement, 0), 1), '', 
                        td = '$'+convert(nvarchar(30),(coalesce(fees, 0)+coalesce(disbursement, 0)), 1), '', 
                        td = convert(nvarchar(100), submitdate, 101), '', 
                        td = convert(nvarchar(100), datepayable, 101), ''
            FROM @NewInvoicesTable i
                  INNER JOIN vtblFirm f ON f.FirmID = i.firmID
                  ORDER BY firm, submitdate
            for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
      N'</table>' +  N'<br/>'



--Include link to the King and Spalding Invoice library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = http://admin.kcicllc.com/cp/dcp/Defense%20Counsel%20Invoices/Forms/AllItems.aspx> here</A> to access these invoices.</b><br><br>'

--Include KCIC address and disclaimer
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person respoDemo Clientble for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'
IF(SELECT COUNT(DISTINCT spdocid)
FROM @NewInvoicesTable) <> 0  
BEGIN
--Execute email-sending stored procedure
EXEC msdb.dbo.sp_send_dbmail
--The Demo ClientProfile sends from Demo Client@kcicllc.com
@profile_name='DemoClientProfile',
@recipients = 'DemoClient@kcicllc.com',
@copy_recipients = 'selfL@kcicllc.com; shockleym@kcicllc.com; dayc@kcicllc.com; johnn@kcicllc.com; montgomeryi@kcicllc.com',
@subject= @subject,
@body= @BodyMessage,
@body_format = 'html'
END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalKingAndSpaldingInvoiceLoaded] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalKingAndSpaldingInvoiceLoaded] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_qryInternalKingAndSpaldingInvoiceLoaded] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_qryInternalKingAndSpaldingInvoiceLoaded] TO [power_user]
GO
