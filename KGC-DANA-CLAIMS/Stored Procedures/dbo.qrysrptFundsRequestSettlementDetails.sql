SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC  [dbo].[qrysrptFundsRequestSettlementDetails] 

As

SET NOCOUNT ON

		
Select	cs.SettlementGroupID, 
		s.settlementgroup,
		Case When MiddleName IS Null then FirstName + ' ' + LastName ELSE
		FirstName + ' ' + MiddleName + ' ' + LastName end as ClaimantName, 
		cs.SettlementAmount
From tblClaimantPersonalInfo cpi
	INNER JOIN tblClaim c						 ON cpi.ClaimantPersonalInfoID=c.ClaimantPersonalInfoID
	INNER JOIN tblClaimSequence csq				 ON c.ClaimSequenceID = csq.ClaimSequenceID
	INNER JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblClaimLawsuitStatusTracking clt ON cl.ClaimLawsuitID=clt.ClaimLawsuitID
	INNER JOIN tblClaimantSettlement cs			 ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
	INNER JOIN tblLawsuit l						 ON l.LawsuitID = cl.LawsuitID
	LEFT  JOIN tblSettlementGroup s				 ON cs.SettlementGroupID = s.SettlementGroupID
	LEFT  JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID
	LEFT  JOIN tblPaymentSettlement ps			 ON cs.ClaimantSettlementID=ps.ClaimantSettlementID
Where	(COALESCE(IsCurrentClaim,0) = 1 
		AND clt.ClaimLawsuitStatusID = 23
		AND ExecutedReleaseDate <= GETDATE() 
		AND spd.ReleaseRcvd = 1
		AND spd.SettlementConfirmation = 1
		AND spd.ClaimantIDed = 1
		AND spd.DxMedicals = 1
		AND spd.EmployExpoHist = 1
		AND spd.ProductID = 1
		AND spd.MMSEA = 1
		AND spd.MedicarePlaintiff = 1
		AND spd.MedicarePlaintiffCounsel = 1
		AND cs.SettlementFundsRequestDate is NULL
		AND (ps.IsFullyPaid is null or ps.IsFullyPaid!=1)
		AND SettlementAmount >0
		AND cpi.ClaimantPersonalInfoID !=70924)
		--OR cs.ClaimantSettlementID=141449
		
Group By cs.SettlementGroupID, 
		s.SettlementGroup,
		Case When MiddleName IS Null then FirstName + ' ' + LastName ELSE
		FirstName + ' ' + MiddleName + ' ' + LastName end, 
		cs.SettlementAmount
	

--select cs.ClaimantSettlementID, cs.SettlementGroupID, settlementgroup,SettlementAmount, paiddate,cpi.ClaimantPersonalInfoID, FirstName, lastname, cs.SettlementFundsRequestDate, ps.IsFullyPaid
--from tblClaimantSettlement cs
--inner join tblClaimLawsuit cl on cl.ClaimLawsuitID=cs.ClaimLawsuitID
--inner join tblClaim c on c.ClaimID=cl.ClaimID
--inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID=c.ClaimantPersonalInfoID
--left join tblSettlementGroup sg on sg.SettlementGroupID=cs.SettlementGroupID
--left join tblPaymentSettlement ps on cs.ClaimantSettlementID=ps.ClaimantSettlementID
----where cpi.ClaimantPersonalInfoID=249801
--where cs.SettlementGroupID = 4671

--select * from tblClaimantSettlement
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptFundsRequestSettlementDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptFundsRequestSettlementDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptFundsRequestSettlementDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptFundsRequestSettlementDetails] TO [power_user]
GO
