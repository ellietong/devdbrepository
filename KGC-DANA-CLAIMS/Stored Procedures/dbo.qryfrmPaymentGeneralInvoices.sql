SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmPaymentGeneralInvoices] (@DefenseCounselAddressID int)--, @MatterID int) --CPJ Removed MatterID parameter

As
Set NoCount On

select di.DefenseInvoiceID,DefenseCounselAddressID, MatterID, InvoiceNumber, Period, InvoiceTotal, InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount
from tblDefenseInvoice di
left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
			 a on a.DefenseInvoiceID = di.DefenseInvoiceID
where DefenseCounselAddressID = @DefenseCounselAddressID 
--and MatterID = @MatterID --CPJ Removed MatterID parameter
and FundsRequestDate is not null
and DatePaymentApproved is not null --CPJ Added PaymentApprovedDate parameter
Group By di.DefenseInvoiceID, DefenseCounselAddressID, InvoiceNumber, Period, InvoiceTotal, MatterID
Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentGeneralInvoices] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentGeneralInvoices] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentGeneralInvoices] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentGeneralInvoices] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentGeneralInvoices] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentGeneralInvoices] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentGeneralInvoices] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentGeneralInvoices] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentGeneralInvoices] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentGeneralInvoices] TO [power_user]
GO
