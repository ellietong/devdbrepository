SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[WSS_BUNDLED_qryBundledSearch] (@ClaimantName		nvarchar(max), 
													  @State			nvarchar(250), 
													  @InjuredPartyID	bigint, 
													  @DocketNumber		nvarchar(250))
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @ClaimantName   nvarchar(max)
--DECLARE @State          nvarchar(250)
--DECLARE @InjuredPartyID bigint
--DECLARE @DocketNumber   nvarchar(250)
--SET @ClaimantName   = 'Blake'
--SET @State          = 'MS'
--SET @InjuredPartyID = NULL
--SET @DocketNumber   = NULL

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE l.LeadLawsuitFlag = ''y'''
IF @ClaimantName	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND((ip.Last_Name + '', '' + CASE WHEN ip.First_Name IS NULL THEN '''' ELSE ip.First_Name + '' '' END) like ''%'' + @ClaimantNameParam + ''%'')' END
IF @State			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND l.StateFiled LIKE ''%'' + @StateParam + ''%''' END
IF @InjuredPartyID	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND ip.Injured_Party_Id = ' + CONVERT(nvarchar(20), @InjuredPartyID) END
IF @DocketNumber	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND DocketNum LIKE ''%'' + @DocketNumberParam + ''%''' END

--Here the final query is pieced together using the WHERE string created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = '
	SELECT ip.Injured_Party_Id,
		c.Claim_Sequence_Nbr,
		ip.Last_Name + '', '' + CASE WHEN ip.First_Name IS NULL THEN '''' ELSE ip.First_Name + '' '' END AS ClaimantName, 
		ip.SS_Nbr, 
		c.[Diagnosis Desc], 
		c.Claim_Status_Cd,
		c.[Release_Type Desc], 
		c.Release_Received_Date, 
		c.Settlement_Date, 
		l.StateFiled AS [State], 
		l.JurisdictionDescription, 
		l.DocketNum AS DocketNumber, 
		l.FileDate,
		c.Settlement_Amt,
		SUM(bh.DNA_Share) AS [Billed Amount]
	FROM m_tblBundledInjuredParty ip
		INNER JOIN m_tblBundledClaim c			ON ip.Injured_Party_Id = c.Injured_Party_Id
		INNER JOIN m_tblBundledLawsuit l		ON c.Injured_Party_Id = l.InjuredPartyID AND c.Claim_Sequence_Nbr  = l.ClaimSequenceNum
		LEFT JOIN  m_tblBundledBilledHistory bh	ON bh.Injured_Party_Id = c.Injured_Party_Id AND bh.[Claim Sequence_Nbr] = c.Claim_Sequence_Nbr' +
	@WhereString + '
	GROUP BY ip.Injured_Party_Id, c.Claim_Sequence_Nbr, ip.Last_Name, ip.First_Name, ip.SS_Nbr, c.[Diagnosis Desc], c.Claim_Status_Cd, c.[Release_Type Desc], 
		c.Release_Received_Date, c.Settlement_Date, l.StateFiled, l.JurisdictionDescription, l.DocketNum, l.FileDate,
		c.Settlement_Amt 
	ORDER BY ClaimantName, c.Claim_Sequence_Nbr
	OPTION (RECOMPILE)'

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@ClaimantNameParam nvarchar(max), @StateParam nvarchar(max), @DocketNumberParam nvarchar(max)', @ClaimantNameParam = @ClaimantName, @StateParam = @State, @DocketNumberParam = @DocketNumber
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_BUNDLED_qryBundledSearch] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_BUNDLED_qryBundledSearch] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_BUNDLED_qryBundledSearch] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_BUNDLED_qryBundledSearch] TO [power_user]
GO
