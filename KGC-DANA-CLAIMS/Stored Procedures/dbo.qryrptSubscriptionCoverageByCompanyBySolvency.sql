SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryrptSubscriptionCoverageByCompanyBySolvency]	
(@limitTypeID int = NULL)
AS
SET NOCOUNT ON
 
--DECLARE @limitTypeID int
--SET @limitTypeID = 0

DECLARE @maxLimitTypeID int
SELECT @maxLimitTypeID = MAX(LimitTypeID) FROM vPolicy

DECLARE @qrySubscriptionCoverageByCompany TABLE (
	[insuranceprogram] varchar (256),
	[subscriptionCompany] varchar(256),
	[subscriptionCompanySolvency] varchar(256),	
	[SubscriberPerOccActual] money,
	[SubscriberPerOccAllocation] money,
	[CarrierID] int, 
	[InsuranceProgramID] int, 
	[SubscriberID] int)	

INSERT INTO @qrySubscriptionCoverageByCompany 
	SELECT InsuranceProgram, s.SubscriptionCompany, s.SubscriptionCompanySolvency, 
		(s.SubscriptionActualPercentOfPolicy * p.PerOccLimit) AS SubscriberPerOccActual, 
		(s.SubscriptionAllocationPercentOfPolicy * p.PerOccLimit) AS SubscriberPerOccAllocation,
		CarrierID, InsuranceProgramID, s.SubscriberID
	FROM vPolicy p 
		INNER JOIN vSubscription s ON p.PolicyID = s.PolicyID
	WHERE p.limitTypeID >= COALESCE(@limitTypeID, 0) AND p.limitTypeID <= COALESCE(NULLIF(@limitTypeID, 0), @maxLimitTypeID)
		and DoNotReport <>1
	GROUP BY InsuranceProgram, s.SubscriptionCompany, s.SubscriptionCompanySolvency, CarrierID, InsuranceProgramID, s.SubscriberID, 
		(s.SubscriptionActualPercentOfPolicy * p.PerOccLimit), 
		(s.SubscriptionAllocationPercentOfPolicy * p.PerOccLimit)

SELECT CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END AS 'SubscriberSolvencyGroup', 
	SUM(SubscriberPerOccActual) AS SumOfSubscriberPerOccActual, SUM(SubscriberPerOccAllocation) AS SumOfSubscriberPerOccAllocation
FROM @qrySubscriptionCoverageByCompany
GROUP BY CASE WHEN SubscriptionCompanySolvency = 'Solvent' THEN 'Solvent' ELSE 'Insolvent' END

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByCompanyBySolvency] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByCompanyBySolvency] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSubscriptionCoverageByCompanyBySolvency] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSubscriptionCoverageByCompanyBySolvency] TO [power_user]
GO
