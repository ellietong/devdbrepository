SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_WSS_DOCUMENT_qryIsDocumentLocked]
(@DocumentID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT COUNT(DISTINCT paymentSettlementID) FROM tblPaymentSettlement ps
	LEFT JOIN tblClaimantSettlement cs ON ps.ClaimantSettlementID = cs.ClaimantSettlementID
	LEFT JOIN tblClaimLawsuit cl ON cs.ClaimLawsuitID = cl.ClaimLawsuitID
	LEFT JOIN tblClaim c ON cl.ClaimID = c.ClaimID
	INNER JOIN tblMultiClaimantDocument mcd ON mcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
	WHERE mcd.DocumentID = @DocumentID
	GROUP BY c.ClaimantPersonalInfoID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_WSS_DOCUMENT_qryIsDocumentLocked] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_WSS_DOCUMENT_qryIsDocumentLocked] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_WSS_DOCUMENT_qryIsDocumentLocked] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_WSS_DOCUMENT_qryIsDocumentLocked] TO [power_user]
GO
