SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_REPORTS_qryDismissalsByDiseaseByMonth] 
	-- Add the parameters for the stored procedure here
	(@MatterID int=null, @MonthID int, @YearID int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--Declare @MonthID int
--Set @MonthID=11
--Declare @YearID int
--Set @YearId=2010
DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @month int
DECLARE @year int
SET @month = @MonthID
SET @year = @YearID

    -- Insert statements for procedure here
select	Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end as HighLevelDisease, 
		COUNT(distinct(cl.ClaimLawsuitID)) As Count
from tblClaim c 
inner join tblClaimLawsuit cl on c.ClaimID=cl.ClaimID
inner join tblClaimLawsuitStatusTracking clst on cl.ClaimLawsuitID=clst.ClaimLawsuitID
inner join tblClaimDisease cd on c.ClaimID=cd.ClaimID
left join tblDisease d on d.DiseaseID=cd.DiseaseID
left join tblDisease_ALT da on cd.DiseaseID=da.DiseaseID_Alt
left join tblHighLevelDisease hld on hld.HighLevelDiseaseID=d.HighLevelDiseaseID or hld.HighLevelDiseaseID=da.HighLevelDiseaseID
where	month(statusDate)=@month 
		and YEAR(statusdate)=@year 
		and ClaimLawsuitStatusID in (15,16,17,19,27,36) 
		and IsPrimaryForClaim = 1 
		and  IsPrimaryStatus=1
		and PrimaryDisease=1
		and MatterID  >= COALESCE(@matterid, 0) AND MatterID <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
group by Case when hld.HighLevelDisease IS NULL Then 'Unknown' Else hld.HighLevelDisease end, hld.HighLevelDiseaseID
order by hld.HighLevelDiseaseID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryDismissalsByDiseaseByMonth] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryDismissalsByDiseaseByMonth] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryDismissalsByDiseaseByMonth] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_REPORTS_qryDismissalsByDiseaseByMonth] TO [power_user]
GO
