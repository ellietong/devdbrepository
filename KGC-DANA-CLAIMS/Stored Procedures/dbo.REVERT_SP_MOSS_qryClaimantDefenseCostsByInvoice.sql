SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryClaimantDefenseCostsByInvoice] 


	
-- Add the parameters for the stored procedure here
	
(@ClaimantPersonalInfoID int, @DefenseCounselID nvarchar(250), @ExpertFirmID int) 
AS
BEGIN
	
-- SET NOCOUNT ON added to prevent extra result sets from
	
-- interfering with SELECT statements.
	
SET NOCOUNT ON;
	

--Declare @ClaimantPersonalInfoID int
--Set @ClaimantPersonalInfoID = 68789

--Declare @DefenseCounselID int
--Set @DefenseCounselID = 63

--DECLARE @ExpertFirmID int
--SET @ExpertFirmID = null

Declare @UnpaidInvoice table (InvoiceID int, IsFullyPaid bit)
Insert into @UnpaidInvoice

Select	
i.InvoiceID, 
	
	
Case When IsFullyPaid is Null and NavPaidInvoice = 0 then 0 
	
	
When IsFullyPaid is Null and NavPaidInvoice = 1 then 1
	
	
When IsFullyPaid = 0 and NavPaidInvoice = 1 then 1
	
	
When IsFullyPaid = 0 and NavPaidInvoice = 0 then 0
	
	
else IsFullyPaid end as IsFullyPaid 
From	
m_tblLegalBillInvoices i
	
Left Join tblPaymentLegalBill pd on i.InvoiceID = pd.InvoiceID
Group By i.InvoiceID, 
	
	
Case When IsFullyPaid is Null and NavPaidInvoice = 0 then 0 
	
	
When IsFullyPaid is Null and NavPaidInvoice = 1 then 1
	
	
When IsFullyPaid = 0 and NavPaidInvoice = 1 then 1
	
	
When IsFullyPaid = 0 and NavPaidInvoice = 0 then 0
	
	
else IsFullyPaid end

--Invoices linked on NavLegacyIDs - Pre-KCIC transfer
Select  cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID as DefenseCounselID, Case When a.OriginalInvoiceNumber IS Null then 'Unknown' else a.OriginalInvoiceNumber END as InvoiceNumber , Case When AppealFlag = 1 then Sum(a.AppealTotal) else SUM(a.AdjustedTotal) END as DefenseTotal,
	
	
Case when NavPaidInvoice = 1 then 'Paid by Navigant'
	
	
when NavPaidInvoice = 0 and u.IsFullyPaid = 1 then 'Paid'
	
	
else lbs.LegalBillStatus end as Status, SourceDate as DatePaid
From tblClaimantPersonalInfo cpi
Inner Join m_tblLegalBillInvoices a on a.navlegacyid=cpi.NavLegacyID
Left Join @UnpaidInvoice u on a.InvoiceID = u.InvoiceID
LEFT JOIN m_tblLegalBillStatus lbs ON a.StatusID = lbs.LegalBillStatusID
left join tblPaymentLegalBill plb ON a.InvoiceID = plb.InvoiceID
left join tblCheck c On plb.CheckID = c.CheckID
Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and a.DefenseCounselAddressID = @DefenseCounselID and cpi.ClaimantPersonalInfoID is null
Group By cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID, a.OriginalInvoiceNumber, AppealFlag, 
	
	
Case when NavPaidInvoice = 1 then 'Paid by Navigant'
	
	
when NavPaidInvoice = 0 and u.IsFullyPaid = 1 then 'Paid'
	
	
else lbs.LegalBillStatus end, SourceDate

Union

--Invoices linked on KCIC IDs
Select  cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID as DefenseCounselID, Case When a.OriginalInvoiceNumber IS Null then 'Unknown' else a.OriginalInvoiceNumber END as InvoiceNumber , Case When AppealFlag = 1 then Sum(a.AppealTotal) else SUM(a.AdjustedTotal) END as DefenseTotal,
	
	
Case when NavPaidInvoice = 1 then 'Paid by Navigant'
	
	
when NavPaidInvoice = 0 and u.IsFullyPaid = 1 then 'Paid'
	
	
else lbs.LegalBillStatus end as Status, SourceDate as DatePaid
From tblClaimantPersonalInfo cpi
Inner Join m_tblLegalBillInvoices a on a.ClaimantPersonalInfoID=cpi.ClaimantPersonalInfoID
Left Join @UnpaidInvoice u on a.InvoiceID = u.InvoiceID
LEFT JOIN m_tblLegalBillStatus lbs ON a.StatusID = lbs.LegalBillStatusID
left join tblPaymentLegalBill plb ON a.InvoiceID = plb.InvoiceID
left join tblCheck c On plb.CheckID = c.CheckID
Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and a.DefenseCounselAddressID = @DefenseCounselID
Group By cpi.ClaimantPersonalInfoID, a.DefenseCounselAddressID, a.OriginalInvoiceNumber, AppealFlag, 
	
	
Case when NavPaidInvoice = 1 then 'Paid by Navigant'
	
	
when NavPaidInvoice = 0 and u.IsFullyPaid = 1 then 'Paid'
	
	
else lbs.LegalBillStatus end, SourceDate

Union 

--Approved Expert and General Invoices with a ClaimantID link
Select CPI.ClaimantPersonalInfoID, Coalesce(di.ExpertFirmID, Di.DefenseCounselAddressID) as DefenseCounselID, Di.InvoiceNumber as InvoiceNumber, 
	SUM(DI.InvoiceTotal) as DefenseTotal,
	Case when IsFullyPaid = 1 then 'Paid' Else Status end as Status, c.SourceDate as DatePaid
From tblClaimantPersonalInfo cpi
Inner Join tblDefenseInvoice di on CPI.ClaimantPersonalInfoID=DI.ClaimantPersonalInfoID
Inner Join tblDefenseInvoiceStatus ds on di.StatusID=ds.StatusID
Left Join tblPaymentDefense pd on di.DefenseInvoiceID=pd.DefenseInvoiceID
left join tblCheck c ON pd.CheckID = c.CheckID
Where cpi.ClaimantPersonalInfoID = @ClaimantPersonalInfoID and DI.StatusID in (1,2, 5) AND di.ExpertFirmID = @ExpertFirmID AND di.DefenseCounselAddressID = 0
Group by CPI.ClaimantPersonalInfoID, di.ExpertFirmID, Di.DefenseCounselAddressID,  Di.InvoiceNumber,
	
	
Case when IsFullyPaid = 1 then 'Paid'
	
	
Else Status end, SourceDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantDefenseCostsByInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantDefenseCostsByInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantDefenseCostsByInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantDefenseCostsByInvoice] TO [power_user]
GO
