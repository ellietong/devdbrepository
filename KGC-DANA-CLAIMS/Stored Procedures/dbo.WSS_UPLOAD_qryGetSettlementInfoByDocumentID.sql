SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryGetSettlementInfoByDocumentID] (@DocumentID bigint)
AS
SET NOCOUNT ON

SELECT CONVERT(nvarchar(MAX), sd.SettleDocID) AS SettleDocID, 
	CONVERT(nvarchar(MAX), sd.DocCategoryID) AS DocCategoryID, 
	CONVERT(nvarchar(MAX), PageNums) AS PageNums, 
	CONVERT(nvarchar(MAX), SetPymtID) AS SetPymtID
FROM tblDocuments d
	INNER JOIN tblSettleDocs sd			ON d.DocumentID = sd.DocumentID
	INNER JOIN tblDocumentCategory dc	ON sd.DocCategoryID = dc.DocCategoryID
WHERE d.DocumentID = @DocumentID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetSettlementInfoByDocumentID] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetSettlementInfoByDocumentID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryGetSettlementInfoByDocumentID] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryGetSettlementInfoByDocumentID] TO [power_user]
GO
