SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_qrySettleDocs] (@SettlePaymentID int, 
												 @emailaddress nvarchar(250))
AS
SET NOCOUNT ON

----The following set of variables can be used for testing.
--DECLARE @SettlePaymentID int
--DECLARE @emailaddress nvarchar(250)
--SET @SettlePaymentID = 146
--SET @emailaddress    = 'reichs@dehay.com'

--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE DetailDocumentTypeID <> 40
						AND SetPymtID = ' + CONVERT(nvarchar(20),@SettlePaymentID)

DECLARE @ClientFirmID bigint = 132 -- Dana
DECLARE @JacobiClaimsFirmID bigint = 133 -- Jacobi Claims
DECLARE @Jacobi_ClaimsFirmID bigint = 268 -- Jacobi Claims (Non-Invoice)
DECLARE @UserFirmID bigint = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)

--These IF statements determine the JOINS by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @JoinString nvarchar(max)
IF( (RIGHT(@emailaddress, 11) = 'kcicllc.com' 
	OR RIGHT(@emailaddress, 8) = 'kcic.com')
	OR @UserFirmID = @JacobiClaimsFirmID
	OR @UserFirmID = @Jacobi_ClaimsFirmID
	OR @UserFirmID = @ClientFirmID
	)
	SET @JoinString = ' '
ELSE
BEGIN
	SET @JoinString = ' INNER JOIN tblClaimantDocumentType cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID
						INNER JOIN tblDocumentGroup dg         ON dg.DocumentGroupID = cdt.DocumentGroupID '
	SET @WhereString = @WhereString + ' AND dg.DocumentGroupID NOT IN (2, 4)'
END

--Here the final query is pieced together using the JOIN and WHERE strings created above, including a RECOMPILE statement.
DECLARE @QueryString nvarchar(max)
SET @QueryString = 
	'SELECT AmazonKey, 
		DocumentTitle, 
		PageNums, 
		DocCategory, 
		SettleDocID, 
		SetPymtID, 
		sd.DocumentID, 
		sd.DocCategoryID
	FROM tblSettleDocs sd 
		INNER JOIN tblDocuments d         ON sd.DocumentID = d.DocumentID
		INNER JOIN tblDocumentCategory dc ON sd.DocCategoryID = dc.DocCategoryID ' +
		@JoinString + 
    @WhereString + '
    OPTION (RECOMPILE)'

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString


GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qrySettleDocs] TO [power_user]
GO
