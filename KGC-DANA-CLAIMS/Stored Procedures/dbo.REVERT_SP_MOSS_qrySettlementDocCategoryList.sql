SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qrySettlementDocCategoryList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT DocCategoryID, DocCategory FROM tblDocumentCategory
    UNION
    SELECT NULL, '-- Select a Category --'
    ORDER BY DocCategory
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qrySettlementDocCategoryList] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qrySettlementDocCategoryList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qrySettlementDocCategoryList] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qrySettlementDocCategoryList] TO [power_user]
GO
