SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SEARCH_qryInsertSavedParameter]
(@Type nvarchar(max), @Value nvarchar(MAX), @SavedSearchID bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	IF(@Type = 'SSNParam')
	BEGIN
		INSERT INTO tblSavedSSN 
		SELECT @Value, @SavedSearchID
	
	END
	ELSE IF (@Type = 'CNParam')
	BEGIN
		INSERT INTO tblSavedClaimantName 
		SELECT @Value, @SavedSearchID
	END
	ELSE IF (@Type = 'DNParam')
	BEGIN
		INSERT INTO tblSavedDocketNumber 
		SELECT @Value, @SavedSearchID
	END
	ELSE IF (@Type = 'StParam')
	BEGIN
		INSERT INTO tblSavedState
		SELECT Convert(int, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'PCParam')
	BEGIN
		INSERT INTO tblSavedPlaintiffCOunsel 
		SELECT Convert(bigint, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'LSParam')
	BEGIN
		INSERT INTO tblSavedLawsuitStatus 
		SELECT Convert(int, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'MEParam')
	BEGIN
		INSERT INTO tblSavedMatterEntity 
		SELECT Convert(int, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'MParam')
	BEGIN
		INSERT INTO tblSavedMatter 
		SELECT Convert(int, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'CIDParam')
	BEGIN
		INSERT INTO tblSavedClaimantIDs 
		SELECT Convert(bigint, @Value), @SavedSearchID
	END
	ELSE IF (@Type = 'MDLParam')
	BEGIN
		INSERT INTO tblSavedIsMDL 
		SELECT Convert(bit, @Value), @SavedSearchID
	END
	
END


GO
