SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROC [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] 
 (
	 @MatterEntityID int,
	 @MatterID int = NULL, 
	 @MonthID  int, 
	 @YearID   int
 )

 AS
SET NOCOUNT ON

 ----The following variables can be used for testing.
--DECLARE @MatterEntityID int
--DECLARE @MonthID int
--DECLARE @YearID int
--DECLARE @MatterID int
--SET @MatterEntityID = 1
--SET @MonthID  = 5
--SET @YearID   = 2011
--SET @MatterID = NULL

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with StatusDate, 
--      and all original StatusDate references have been replaced with ProcessedDate.

DECLARE @WhereString nvarchar(max)
SET @WhereString = '
	WHERE ClaimLawsuitStatusID in (15,16,17,19,27,36) 
		AND IsPrimaryForClaim = 1 
		AND IsPrimaryStatus = 1
'

IF @MonthID			IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND MONTH(ProcessedDate) <= ' + '''' + CONVERT(nvarchar(20),@MonthID) + '''' END
IF @YearID			IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND YEAR(ProcessedDate) = ' + '''' + CONVERT(nvarchar(20),@YearID) + '''' END
IF @MatterEntityID	IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND MatterEntityID = ' + CONVERT(nvarchar(20),@MatterEntityID) END
IF @MatterID		IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND c.MatterID = ' + CONVERT(nvarchar(20),@MatterID) END
IF @MatterID		IS NULL		  BEGIN SET @WhereString = @WhereString + ' AND c.MatterID <> 13 ' END

DECLARE @QueryString nvarchar(max)
SET @QueryString = '
SELECT TOP 10 pca.PlaintiffCounsel, 
	COUNT(DISTINCT(cl.ClaimLawsuitID)) AS ''Count''
FROM tblClaim c 
	INNER JOIN tblMatter m						  ON c.MatterID = m.MatterID
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca     ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
		'+ @WhereString +'
GROUP BY pca.PlaintiffCounsel
ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC, pca.PlaintiffCounsel
'
EXEC sp_executesql @QueryString




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_YEARLY_qryTopTenDismissalsByPlaintiffsCounselByYear] TO [power_user]
GO
