SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_LAWSUIT_qryInsDismissal]
    (
      @ClaimLawsuitID BIGINT ,
      @DismissalTypeID INT ,
      @DismissalDate DATE ,
      @DocumentID BIGINT ,
      @Emailaddress NVARCHAR(250)
    )
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;


	---If settled or dismissed, add dismissal but don't change the status - settlement or old dismissal is still primary status.
        IF ( SELECT claimlawsuitstatusid
             FROM   tblClaimLawsuitStatusTracking
             WHERE  ClaimLawsuitID = @ClaimLawsuitID
                    AND IsPrimaryStatus = 1
           ) IN ( 23, 24, 25,15,16,19,27,36 ) 
            BEGIN

                INSERT  INTO tblClaimLawsuitStatusTracking
                        ( ClaimLawsuitID ,
                          ClaimLawsuitStatusID ,
                          StatusDate ,
                          IsPrimaryStatus ,
                          StatusNotes ,
                          ProcessedDate
                        )
                        SELECT  @ClaimLawsuitID ,
                                @DismissalTypeID ,
                                @DismissalDate ,
                                0 ,
                                'Updated by upload of documentID '
                                + CONVERT(VARCHAR(500), @DocumentID) + ' by '
                                + @EmailAddress ,
                                CONVERT(DATE, GETDATE())

            END
        ELSE 
            BEGIN
	
		--Claim not settled or dismissed, dismissal will change status and become primary.
                UPDATE  tblClaimLawsuitStatusTracking
                SET     IsPrimaryStatus = 0
                WHERE   ClaimLawsuitID = @ClaimLawsuitID
		
		
                INSERT  INTO tblClaimLawsuitStatusTracking
                        ( ClaimLawsuitID ,
                          ClaimLawsuitStatusID ,
                          StatusDate ,
                          IsPrimaryStatus ,
                          StatusNotes ,
                          ProcessedDate
                        )
                        SELECT  @ClaimLawsuitID ,
                                @DismissalTypeID ,
                                @DismissalDate ,
                                1 ,
                                'Updated by upload of documentID '
                                + CONVERT(VARCHAR(500), @DocumentID) + ' by '
                                + @EmailAddress ,
                                CONVERT(DATE, GETDATE())
            END
	
	--Insert record into tblMultiClaimantDocument to link document to individual claimant
        INSERT  INTO tblMultiClaimantDocument
                ( DocumentID ,
                  ClaimantPersonalInfoID ,
                  ClaimLawsuitID
                )
                SELECT  @DocumentID ,
                        c.ClaimantPersonalInfoID ,
                        @ClaimLawsuitID
                FROM    tblClaimLawsuit cl
                        INNER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
                WHERE   cl.ClaimLawsuitID = @ClaimLawsuitID
	
	
    END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryInsDismissal] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryInsDismissal] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_LAWSUIT_qryInsDismissal] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_LAWSUIT_qryInsDismissal] TO [power_user]
GO
