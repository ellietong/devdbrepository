SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[qryrptDismissalPassThroughQC]
AS 
SET NOCOUNT ON

Select d.DocumentID, d.DocumentTitle, cdt.ClaimantDocumentType AS DocumentType, StatusDate, d.DismissalDate as DocDismissalDate, cls2.ClaimLawsuitStatus as DocClaimLawsuitStatus, QCBy, 
	   cpi.ClaimantPersonalInfoID, cpi.LastName + ', ' + cpi.FirstName + COALESCE(' ' + cpi.MiddleName,'') AS ClaimantName, IndividualDocketNumber,
	   l.CaseCaption, l.DocketNumber, cls.ClaimLawsuitStatus, clst.DismissalDate
from tblDocuments d
left join tblMultiClaimantDocument mcd ON mcd.DocumentID = d.DocumentID
left join tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
left join tblClaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
left join tblClaimLawsuit cl ON cl.ClaimID = c.ClaimID
left join tblLawsuit l ON l.LawsuitID = cl.LawsuitID
left join tblClaimLawsuitStatusTracking clst ON clst.ClaimLawsuitID = cl.ClaimLawsuitID
left join tblClaimLawsuitStatus CLS on clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
left join tblClaimLawsuitStatus cls2 on cls2.ClaimLawsuitStatusID=d.ClaimLawsuitStatusID
left join tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
where 
CONVERT(date, statusdate) = CONVERT(date, getdate())
and coalesce(d.dismissaldate, d.claimlawsuitstatusid) is not null
ORDER BY DocumentID, ClaimantName, ClaimantPersonalInfoID


RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissalPassThroughQC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDismissalPassThroughQC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDismissalPassThroughQC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDismissalPassThroughQC] TO [power_user]
GO
