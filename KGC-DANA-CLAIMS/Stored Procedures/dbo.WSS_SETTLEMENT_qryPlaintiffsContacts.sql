SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryPlaintiffsContacts] 
(@PlaintiffCounselAddressID bigint = null)
AS
BEGIN
	--DECLARE @PlaintiffCounselAddressID bigint = 123
	SET NOCOUNT ON;

	SELECT NULL AS PersonID, NULL AS ContactName
	UNION
	SELECT PersonID, LName + COALESCE(', ' + FName, '') + ' (' + Email + ')' as ContactName
	FROM vtblPerson p
	WHERE COALESCE(p.PlaintiffCounselAddressID, 0) = @PlaintiffCounselAddressID
		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffsContacts] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffsContacts] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffsContacts] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryPlaintiffsContacts] TO [power_user]
GO
