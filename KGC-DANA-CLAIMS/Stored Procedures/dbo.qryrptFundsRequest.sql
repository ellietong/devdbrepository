SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC  [dbo].[qryrptFundsRequest] 

As

SET NOCOUNT ON


Declare @FundingInvoices table(DefenseCounselID bigint, Firm nvarchar(250), InvoiceTotal money)
Insert into @FundingInvoices

--LB Non-Appeal Invoices
Select	D.DefenseCounselID as DefenseCounselID,
		D.DefenseCounsel as Firm,
		SUM(AdjustedTotal) as InvoiceTotal
From m_tblLegalBillInvoices i
	Left Join tblDefenseCounselAddress da on i.DefenseCounselAddressID=da.DefenseCounselAddressID
	Left Join tblDefenseCounsel d on da.DefenseCounselID=d.DefenseCounselID
Where	SubmittedForPayment is not null AND 
		FundsRequestDate is null AND
		AppealFlag = 0 AND
		AdjustedTotal != 0
Group By D.DefenseCounselID, d.DefenseCounsel


UNION

--Appeal LB Invoices
Select	D.DefenseCounselID as DefenseCounselID,
		D.DefenseCounsel as Firm,
		SUM(AppealTotal) as InvoiceTotal
From m_tblLegalBillInvoices i
	Left Join tblDefenseCounselAddress da on i.DefenseCounselAddressID=da.DefenseCounselAddressID
	Left Join tblDefenseCounsel d on da.DefenseCounselID=d.DefenseCounselID
Where	SubmittedForPayment is not null AND 
		FundsRequestDate is null AND
		AppealFlag = 1 AND
		AppealTotal != 0
Group By D.DefenseCounselID, d.DefenseCounsel


UNION

--Expert and General Invoices
Select	DefenseCounselID, 
		Coalesce(ExpertFirm, DefenseCounsel) as Firm, 
		SUM(InvoiceTotal) 
from tblDefenseInvoice di 
left Join m_tblExpertFirm ef on ef.ExpertFirmID = di.ExpertFirmID
left join tblDefenseCounselAddress dca on dca.DefenseCounselAddressID = di.DefenseCounselAddressID
Where (FundsRequestDate is null and SubmitDate is not null and StatusID in (1,2)) --Need to limit out KCIC invoices
Group By DefenseCounselID, Coalesce(ExpertFirm, DefenseCounsel)


Select f.DefenseCounselID, f.Firm, Sum(f.InvoiceTotal) as InvoiceTotal
From @FundingInvoices f
Group BY f.DefenseCounselID, f.Firm
Order By f.Firm




GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequest] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequest] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptFundsRequest] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptFundsRequest] TO [power_user]
GO
