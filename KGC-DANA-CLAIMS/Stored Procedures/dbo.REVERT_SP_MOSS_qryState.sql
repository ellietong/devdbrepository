SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[REVERT_SP_MOSS_qryState]
As

SET NOCOUNT ON

SELECT StateID, State, FullNameState
FROM tblState
UNION 
SELECT null, '', ''
ORDER BY State

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryState] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryState] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryState] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryState] TO [power_user]
GO
