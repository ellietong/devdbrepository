SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[m_DanaDocumentProduction_20130606]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent etra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
select DISTINCT d.documentID, d.documentTitle, d.DetailDocumentTypeID, cdt.ClaimantDocumentType, d.documentURL, AmazonKey			
from tblMultiClaimantDocument mcd			
inner join tblDocuments d on mcd.DocumentID = d.DocumentID
inner join tblClaimantDocumentType cdt on cdt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
inner join m_AmEmpClaimants_June2013_Analysis amp on amp.ClaimantPersonalInfoID=mcd.ClaimantPersonalInfoID
where d.documentID <=424366
AND NOT EXISTS (SELECT 1 FROM mDocProd dp WHERE dp.DocumentID = d.DocumentID)
END
GO
