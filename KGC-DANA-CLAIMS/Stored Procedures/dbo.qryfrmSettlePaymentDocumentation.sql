SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmSettlePaymentDocumentation] (@ClaimLawsuitID int)
as

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 4552

Select spd.[SetPymtID]
           ,[ClaimLawsuitID]
      ,sd.[ClaimantIDed]
      ,sd.[DxMedicals]
      ,sd.[DODCert]
      ,sd.[EstatePapers]
      ,sd.[ProductID]
      ,sd.[EmployExpoHist]
      ,sd.[MMSEA]
      ,sd.[MedicarePlaintiff]
      ,sd.[MedicarePlaintiffCounsel]
      ,sd.[SettlementConfirmation]
      ,[ReleaseRqstDate]
      ,[ReleaseToPC]
      ,[ExecutedReleaseDate]
      ,[SettlePaymentComments]
      ,[ReleaseRcvd]
      ,[MedicareFullyDocumented]
      ,[MedicareReported]
      ,[ReleaseSignedDate]
      ,[ReleaseTypeID]
      ,[SPUsername]
from dbo.tblSettlePaymentDocumentation spd
left join [dbo].[tblSettlementDocumentation] sd on sd.SetPymtID = spd.SetPymtID
Where ClaimLawsuitID = @ClaimLawsuitID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [power_user]
GO
