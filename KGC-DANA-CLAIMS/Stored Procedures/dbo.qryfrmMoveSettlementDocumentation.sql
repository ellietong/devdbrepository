SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryfrmMoveSettlementDocumentation]
(@SetpymtID int, @ClaimLawsuitID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



--DECLARE @SetpymtID int
--DECLARE @ClaimLawsuitID int

--SET @SetpymtID = 13
--SET @ClaimLawsuitID = 46723


DECLARE @NewSettPymtID int

--If a record does not exist in tblSettlePaymentDocumentation for this claim lawsuit, create one with documentation checklist from before
IF (SELECT count(SetPymtID) from tblSettlePaymentDocumentation where ClaimLawsuitID = @ClaimlawsuitID) = 0 
BEGIN

Insert into tblSettlePaymentDocumentation (ClaimLawsuitID, ClaimantIDed, DxMedicals, DODCert, EstatePapers, ProductID, EmployExpoHist, MMSEA, MedicarePlaintiff, MedicarePlaintiffCounsel)
select @ClaimLawsuitID, ClaimantIDed, DxMedicals, DODCert, EstatePapers, ProductID, EmployExpoHist, MMSEA, MedicarePlaintiff, MedicarePlaintiffCounsel
from tblSettlePaymentDocumentation where SetPymtID = @SetpymtID

set @NewSettPymtID = (select MAX(SetPymtID) from tblSettlePaymentDocumentation)

END

--otherwise update record, uniting information from two documentation checklists
ELSE
BEGIN

UPDATE tblSettlePaymentDocumentation
SET ClaimantIDed = coalesce(nullif(ClaimantIDed,0), (select ClaimantIDed from tblSettlePaymentDocumentation where SetPymtID = @SetpymtID)), 
DxMedicals = coalesce(nullif(DxMedicals,0), (select DxMedicals from tblSettlePaymentDocumentation where SetPymtID = @SetpymtID)), 
DODCert = coalesce(nullif(DODCert,0), (select DODCert from tblSettlePaymentDocumentation where SetPymtID = @SetpymtID)), 
EstatePapers = coalesce(nullif(EstatePapers,0), (select EstatePapers from tblSettlePaymentDocumentation where SetPymtID = @SetpymtID)), 
ProductID = coalesce(nullif(ProductID,0), (select ProductID from tblSettlePaymentDocumentation where SetPymtID = @SetpymtID)), 
EmployExpoHist = coalesce(nullif(EmployExpoHist,0), (select EmployExpoHist from tblSettlePaymentDocumentation where SetPymtID = @SetpymtID)), 
MMSEA = coalesce(nullif(MMSEA,0), (select MMSEA from tblSettlePaymentDocumentation where SetPymtID = @SetpymtID)), 
MedicarePlaintiff = coalesce(nullif(MedicarePlaintiff,0), (select MedicarePlaintiff from tblSettlePaymentDocumentation where SetPymtID = @SetpymtID)), 
MedicarePlaintiffCounsel = coalesce(nullif(MedicarePlaintiffCounsel,0), (select MedicarePlaintiffCounsel from tblSettlePaymentDocumentation where SetPymtID = @SetpymtID))
WHERE ClaimLawsuitID = @ClaimLawsuitID

set @NewSettPymtID = (Select setpymtID from tblSettlePaymentDocumentation where ClaimLawsuitID = @ClaimLawsuitID)

END


--delete from original documentation checklist

UPDATE tblSettlePaymentDocumentation
SET ClaimantIDed = 0, 
DxMedicals = 0, 
DODCert = 0, 
EstatePapers = 0, 
ProductID = 0, 
EmployExpoHist = 0, 
MMSEA = 0, 
MedicarePlaintiff = 0, 
MedicarePlaintiffCounsel = 0

WHERE SetPymtID = @SetpymtID

--update setpmtID in settle docs with old setpmtid to have NEW setpmtid!

UPDATE tblSettleDocs
SET SetPymtID = @NewSettPymtID 
WHERE SetPymtID = @SetpymtID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMoveSettlementDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmMoveSettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMoveSettlementDocumentation] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryfrmMoveSettlementDocumentation] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmMoveSettlementDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmMoveSettlementDocumentation] TO [power_user]
GO
