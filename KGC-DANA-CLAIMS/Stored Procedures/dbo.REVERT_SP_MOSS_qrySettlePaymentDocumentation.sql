SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[REVERT_SP_MOSS_qrySettlePaymentDocumentation] (@ClaimID int)
as

--DECLARE @ClaimID int
--SET @ClaimID = 1

Select pd.SetPymtID, COALESCE(sd.ClaimantIDed, 0) AS ClaimantIDed, COALESCE(sd.DODCert, 0) AS DODCert, COALESCE(sd.DxMedicals, 0) AS DxMedicals,  COALESCE(sd.EmployExpoHist,0) AS EmployExpoHist,  COALESCE(sd.EstatePapers,0) AS EstatePapers, COALESCE(sd.productid,0) AS ProductID, COALESCE(sd.SettlementConfirmation, 0) AS SettlementConfirmation, pd.ExecutedReleaseDate, pd.ReleaseRqstDate,
pd.ReleaseToPC, COALESCE(rt.ReleaseType, '') as ReleaseType, 
cpi.MedicareBeneficiaryStatusID, MedicareBeneficiaryStatus,
COALESCE(sd.MMSEA, 0) AS MMSEA, ReleaseToPC, COALESCE(sd.MedicarePlaintiff, 0) AS MedicarePlaintiff, COALESCE(sd.MedicarePlaintiffCounsel, 0) AS MedicarePlaintiffCounsel, DeceasedDate, SettlePaymentComments,
COALESCE(MedicareReported, 0) AS MedicareReported, pd.ReleaseTypeID
From tblSettlePaymentDocumentation pd
LEFT JOIN tblSettlementDocumentation sd ON sd.setpymtID=PD.SetPymtID
LEFT OUTER JOIN tblReleaseType rt ON pd.ReleaseTypeID = rt.ReleaseTypeID
LEFT OUTER JOIN tblClaimLawsuit cl ON pd.ClaimLawsuitID = cl.ClaimLawsuitID
LEFT OUTER JOIN tblClaim c ON cl.ClaimID = c.ClaimID
LEFT OUTER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
LEFT OUTER JOIN tblMedicareBeneficiaryStatus mbs ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID
Where pd.ClaimLawsuitID IN (SELECT DISTINCT ClaimLawsuitID FROM tblClaimLawsuit WHERE ClaimID = @ClaimID)-- AND COALESCE(IsPrimaryForClaim, 0) = 1)

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qrySettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qrySettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qrySettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qrySettlePaymentDocumentation] TO [power_user]
GO
