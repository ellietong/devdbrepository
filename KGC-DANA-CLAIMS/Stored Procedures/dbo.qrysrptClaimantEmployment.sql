SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysrptClaimantEmployment]
(@ClaimantPersonalInfoID bigint)

As
Set NoCount On

--declare @claimantpersonalinfoid int
--set @claimantpersonalinfoid = 110933

Select ClaimantEmploymentID, Occupation, Jobsite, Comments, EmpStartDate, EmpEndDate, dbo.fnEarliestExposure(claimantpersonalinfoid) as EarliestExposureDate, 
dbo.fnLatestExposure(claimantpersonalinfoid)as LatestExposureDate, IsPrimary, IsSecondaryExposure, JobsiteCity, s.State
From tblClaimantEmployment CE
Inner Join tblOccupation o on o.OccupationID = CE.OccupationID
left join tblState s on s.StateID = CE.JobsiteStateID
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By EmpStartDate

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantEmployment] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantEmployment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantEmployment] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantEmployment] TO [power_user]
GO
