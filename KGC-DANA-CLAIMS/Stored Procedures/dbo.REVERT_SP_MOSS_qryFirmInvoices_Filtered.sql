SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryFirmInvoices_Filtered](@emailaddress nvarchar (1000)=null, @FilterStatusID int, @FilterDocumentTypeID int, @FilterFirmID int, @FilterExpertFirmID int)
AS
--DECLARE @emailaddress nvarchar(max)
--SET @emailaddress = 'vicki.stringham@danacollc.com'
--DECLARE @FilterStatusID int
--DECLARE @FilterDocumentTypeID int
--DECLARE @FilterFirmID int

--SET @FilterStatusID = null
--SET @FilterDocumentTypeID = null
--SET @FilterFirmID = 38

DECLARE @firmid int
SET @firmid = (SELECT DISTINCT FirmID FROM vtblperson WHERE Email = @emailaddress)

DECLARE @MaxStatusID int
SET @MaxStatusID = (SELECT MAX(StatusID) FROM tblDefenseInvoiceStatus)

DECLARE @MaxDocType int
SET @MaxDocType = (SELECT MAX(ClaimantDocumentTypeID) FROM tblClaimantDocumentType)

DECLARE @MaxFirmID int
SET @MaxFirmID = (SELECT MAX(DefenseCounselID) FROM tblDefenseCounsel)

DECLARE @MaxExpertFirmID int
SET @MaxExpertFirmID = (SELECT MAX(ExpertFirmID) FROM m_tblExpertFirm)

IF ((@firmid = 132) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11) = 'kcicllc.com'))
BEGIN
	SELECT CAST(d.DocumentID as varchar(50)) as DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm, 				
		m.Matter,
		di.DatePaid, 
		di.CheckNumber,
		d.FirmID,
		COALESCE(ExpertFirm, '') AS ExpertFirm,
		SourceDate,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.ExpertFirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
		LEFT OUTER JOIN m_tblExpertFirm ex ON d.ExpertFirmID = ex.ExpertFirmID
		LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
	WHERE dt.DocumentGroupID = 2
	AND ClaimantDocumentTypeID >= COALESCE(@FilterDocumentTypeID, 0) AND ClaimantDocumentTypeID <= COALESCE(@FilterDocumentTypeID, @maxDocType)
		AND d.FirmID >= COALESCE(@FilterFirmID, 0) AND d.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
		AND d.StatusID >= COALESCE(@FilterStatusID, 0) AND d.StatusID <= COALESCE(@FilterStatusID, @MaxStatusID)
		AND COALESCE(d.ExpertFirmID, 0) >= COALESCE(@FilterExpertFirmID, 0) AND COALESCE(d.ExpertFirmID, 0) <= COALESCE(@FilterExpertFirmID, @maxExpertFirmID)
		and ((coalesce(d.statusid,0)!=5) or (coalesce(di.StatusID,0)!=5))
	ORDER BY Accepted ASC, DateCreated DESC
END 
ELSE IF @firmid in (100, 117)
BEGIN
	SELECT 
		cast(d.DocumentID as varchar(50)) as DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm, 				
		m.Matter,
		di.DatePaid, 
		di.CheckNumber,
		d.FirmID,
		COALESCE(ExpertFirm, '') AS ExpertFirm,
		SourceDate,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.ExpertFirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
		LEFT OUTER JOIN m_tblExpertFirm ex ON d.ExpertFirmID = ex.ExpertFirmID
		LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
	WHERE dg.DocumentGroupID = 2
		AND f.FirmID IN (100,117)
		AND ClaimantDocumentTypeID >= COALESCE(@FilterDocumentTypeID, 0) AND ClaimantDocumentTypeID <= COALESCE(@FilterDocumentTypeID, @maxDocType)
		AND d.FirmID >= COALESCE(@FilterFirmID, 0) AND d.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
		AND d.StatusID >= COALESCE(@FilterStatusID, 0) AND d.StatusID <= COALESCE(@FilterStatusID, @MaxStatusID)
		AND COALESCE(d.ExpertFirmID, 0) >= COALESCE(@FilterExpertFirmID, 0) AND COALESCE(d.ExpertFirmID, 0) <= COALESCE(@FilterExpertFirmID, @maxExpertFirmID)
		and ((coalesce(d.statusid,0)!=5) or (coalesce(di.StatusID,0)!=5))
	ORDER BY Accepted ASC, DateCreated DESC
END 
ELSE BEGIN
	SELECT 
		cast(d.DocumentID as varchar(50)) as DocumentId,
		dt.ClaimantDocumentType, 
		dg.DocumentGroup, 		
		d.Comments,
		d.DocumentURL,
		d.DocumentTitle,
		d.SPDocId,
		d.CreatedBy,
		d.DateCreated,
		d.ModifiedBy,
		d.DateModified,
		d.Accepted,
		d.AcceptedBy,
		d.DateAccepted,
		d.ApprovedBy,
		d.DateApproved,
		f.Firm,
		d.InvoiceNumber,
		d.Period,
		d.DatePayable,
		d.FeeAmount,
		d.DisbursementAmount,
		d.Adjustment,
		d.AdjustmentNotes,
		dis.Status,	
		dif.Firm DefenseInvoiceFirm, 				
		m.Matter,
		di.DatePaid, 
		di.CheckNumber,
		d.FirmID,
		COALESCE(ExpertFirm, '') AS ExpertFirm,
		SourceDate,
		(COALESCE(d.FeeAmount, 0) + COALESCE(d.DisbursementAmount, 0) - COALESCE(d.Adjustment, 0)) AS InvoiceTotal
	FROM tblDocuments d
		JOIN tblClaimantDocumentType dt ON d.DetailDocumentTypeID = dt.ClaimantDocumentTypeID
		JOIN tblDocumentGroup dg ON dt.DocumentGroupID = dg.DocumentGroupID
		LEFT OUTER JOIN tblDefenseInvoice di ON d.DocumentID = di.DocumentID 
		LEFT OUTER JOIN tblDefenseInvoiceFirm dif ON di.ExpertFirmID = dif.FirmID 
		LEFT OUTER JOIN tblDefenseInvoiceStatus dis ON d.StatusID = dis.StatusID 
		LEFT OUTER JOIN tblMatter m ON d.MatterID = m.MatterID
		LEFT OUTER JOIN vtblFirm f ON d.FirmID = f.FirmID
		LEFT OUTER JOIN m_tblExpertFirm ex ON d.ExpertFirmID = ex.ExpertFirmID
		LEFT OUTER JOIN tblPaymentDefense pd ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT OUTER JOIN tblCheck c ON pd.CheckID = c.CheckID
	WHERE dg.DocumentGroupID = 2
		AND f.FirmID = @firmid
		AND ClaimantDocumentTypeID >= COALESCE(@FilterDocumentTypeID, 0) AND ClaimantDocumentTypeID <= COALESCE(@FilterDocumentTypeID, @maxDocType)
		AND d.FirmID >= COALESCE(@FilterFirmID, 0) AND d.FirmID <= COALESCE(@FilterFirmID, @maxFirmID)
		AND d.StatusID >= COALESCE(@FilterStatusID, 0) AND d.StatusID <= COALESCE(@FilterStatusID, @MaxStatusID)
		AND COALESCE(d.ExpertFirmID, 0) >= COALESCE(@FilterExpertFirmID, 0) AND COALESCE(d.ExpertFirmID, 0) <= COALESCE(@FilterExpertFirmID, @maxExpertFirmID)
		and ((coalesce(d.statusid,0)!=5) or (coalesce(di.StatusID,0)!=5))
	ORDER BY Accepted ASC, DateCreated DESC
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryFirmInvoices_Filtered] TO [power_user]
GO
