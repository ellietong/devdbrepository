SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[qrysfrmDefJurisSelect](@DefenseJurisdictionID int)
AS
SET NOCOUNT ON

SELECT dj.DefenseCounselAddressID, dj.JurisdictionID, dj.CourtTypeID, dj.StateID 
FROM tblDefenseJurisdiction dj
WHERE dj.DefenseJurisdictionID = @DefenseJurisdictionID 


GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDefJurisSelect] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmDefJurisSelect] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDefJurisSelect] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmDefJurisSelect] TO [power_user]
GO
