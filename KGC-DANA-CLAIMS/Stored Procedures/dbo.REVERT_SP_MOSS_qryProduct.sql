SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryProduct]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SELECT ProductID, COALESCE(ProductType, 'UNSPECIFIED') + ' - ' + COALESCE(ProductManufacturer, 'UNSPECIFIED') + ' - ' + COALESCE(ProductBrand, 'UNSPECIFIED') + ' - ' + COALESCE(ProductName, 'UNSPECIFIED') AS Product
	FROM
	tblProduct p
	Inner Join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
	Inner Join tblProductManufacturer pm on pm.ProductManufacturerID = p.ProductManufacturerID
	Inner Join tblProductName pn on pn.ProductNameID = p.ProductNameID
	INner Join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
	UNION
	SELECT NULL, '' As Product
	ORDER BY COALESCE(ProductType, 'UNSPECIFIED') + ' - ' + COALESCE(ProductManufacturer, 'UNSPECIFIED') + ' - ' + COALESCE(ProductBrand, 'UNSPECIFIED') + ' - ' + COALESCE(ProductName, 'UNSPECIFIED')
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryProduct] TO [power_user]
GO
