SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryfrmCopyDocumentToDefenseInvoice]
(@DocumentID int, @ExpertFirmID int,@InvoiceNumber nvarchar(255), @Period nvarchar(255), @DefenseCounselAddressID int,@ClaimantPersonalInfoID int, @MatterID int)
AS

SET NOCOUNT ON

--Copy Document Record to mBtblDefenseInvoice
INSERT INTO tblDefenseInvoice (StatusID, ExpertFirmID, InvoiceNumber, Period, 


	
FeeAmount, DisbursementAmount, Adjustment, InvoiceTotal, SubmitDate, DatePayable, CheckNumber,
	
FundsRequestDate, DatePaid, DocumentID, AdjustmentNotes, InvoiceLink, CheckLink, DefenseCounselAddressID,ClaimantPersonalInfoID, MatterID)
SELECT StatusID, 
	
@ExpertFirmID AS ExpertFirmID,
	
@InvoiceNumber AS InvoiceNumber, 
	
@Period AS Period, 
	
FeeAmount, 
	
DisbursementAmount, 
	
Adjustment, 
	
(FeeAmount + DisbursementAmount - Adjustment) AS InvoiceTotal,
	
DateCreated AS SubmitDate, 
	
(DateCreated + 60) AS DatePayable,
	
NULL AS CheckNumber,
	
NULL AS FundsRequestDate,
	
NULL AS DatePaid,
	
DocumentID,
	
AdjustmentNotes,
	
DocumentURL AS InvoiceLink,
	
'' AS CheckLink, @DefenseCounselAddressID as DefenseCounselAddressID, @ClaimantPersonalInfoID as ClaimantPersonalInfoID, @MatterID as MatterID
FROM tblDocuments 
WHERE DocumentID = @DocumentID

--Update Document Record in tblDocuments
UPDATE tblDocuments
SET QCDate = GETDATE()
WHERE DocumentID = @DocumentID

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCopyDocumentToDefenseInvoice] TO [power_user]
GO
