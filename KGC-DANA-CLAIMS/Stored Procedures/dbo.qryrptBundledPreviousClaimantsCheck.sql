SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROC [dbo].[qryrptBundledPreviousClaimantsCheck] (@ClaimantPersonalInfoID bigint)

AS
SET NOCOUNT ON

--Declare @ClaimantPersonalInfoID bigint
--Set @ClaimantPersonalInfoID = 69353

SELECT Injured_Party_Id, First_Name, Middle_Name, Last_Name, SS_Nbr, [Date of Birth],
CASE WHEN First_Name LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1 
WHEN First_Name LIKE (SELECT MiddleName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1
ELSE 0 END AS FlagFirstName,
CASE WHEN Middle_Name LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1 
WHEN Middle_Name LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) THEN 1
ELSE 0 END AS FlagMiddleName,
CASE WHEN Last_Name LIKE (SELECT LastName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END AS FlagLastName,
CASE WHEN SS_Nbr LIKE (SELECT SocialSecurityNumber FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END AS FlagSocialSecurityNumber,
CASE WHEN [Date of Birth] LIKE (SELECT BirthDate FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END AS FlagBirthDate
FROM m_tblBundledFInjuredParty
WHERE 
(((((LEFT(First_Name,3) LIKE (SELECT LEFT(FirstName,3) FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) OR
LEFT(Middle_Name,3) LIKE (SELECT LEFT(FirstName,3) FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)) OR
(LEFT (First_Name,3) LIKE (SELECT LEFT(MiddleName,3) FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID))))
AND Last_Name LIKE (SELECT LastName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)) OR
(SS_Nbr LIKE (SELECT SocialSecurityNumber FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)) OR
([Date of Birth] = (SELECT BirthDate FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID)))

ORDER BY CASE WHEN Last_Name LIKE (SELECT LastName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END,
CASE WHEN First_Name LIKE (SELECT FirstName FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END,
CASE WHEN SS_Nbr LIKE (SELECT SocialSecurityNumber FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END,
CASE WHEN [Date of Birth] LIKE (SELECT BirthDate FROM tblClaimantPersonalInfo WHERE claimantpersonalinfoid = @claimantPersonalInfoID) 
THEN 1 ELSE 0 END

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptBundledPreviousClaimantsCheck] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptBundledPreviousClaimantsCheck] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptBundledPreviousClaimantsCheck] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptBundledPreviousClaimantsCheck] TO [power_user]
GO
