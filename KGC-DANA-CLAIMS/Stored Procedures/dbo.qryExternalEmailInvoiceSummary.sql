SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryExternalEmailInvoiceSummary]

AS
BEGIN
/*External Email to Marie Shipley which runs once a week.  The email includes two tables.
The first table is for invoices which have been uploaded within the past week.
The second table is for invoices which are over a week old but she has not yet approved or rejected. - BEH*/

--Declare and set sp_send_dbmail input variables
DECLARE @Subject nvarchar (100)
DECLARE @BodyMessage nvarchar (max)

SET @Subject = 'Dana Companies, LLC: Weekly Invoices Summary ' + CONVERT(varchar, GETDATE() - 7, 107) + ' - ' + CONVERT(varchar, GETDATE(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top and the header for table 1
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcic.com/i/KCICLogoBar_solid.PNG" alt="logo"/>
<H4>The following invoices have been uploaded since ' + CONVERT(varchar, GETDATE() - 7, 107) + '.</H4>'

--Create temp table for invoices which have been uploaded in the past week
DECLARE @NewWeekInvoicesTable TABLE (spDocID nvarchar (100), firmID int, [status] nvarchar (30), invoiceNum nvarchar (100),
period nvarchar (100), fees money, disbursement money, adjustment money, submitDate datetime, datePayable datetime, DocumentType nvarchar (500))
INSERT INTO @NewWeekInvoicesTable
	SELECT
		SPDocID,
		--Retrieve the firm's invoice site abbreviation by parsing the documentURL
		--There are two different cases. One for those firms who are categorized as Defense Counsels and 
		--the other for firms who are categorized only as Firms.
		dc.DefenseCounselID AS FirmID,
		[Status],
		InvoiceNumber,
		Period,
		FeeAmount,
		DisbursementAmount,
		Adjustment,
		DateCreated,
		DatePayable,
		ClaimantDocumentType
	FROM tblDocuments d
	LEFT JOIN tblDefenseInvoiceStatus s ON d.StatusID = s.StatusID
	INNER JOIN tblDefenseCounsel dc ON dc.DefenseCounselID = d.FirmID
	INNER JOIN tblClaimantDocumentType dt ON dt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	WHERE Datecreated BETWEEN GETDATE() - 7 AND GETDATE()
	AND dt.DocumentGroupID = 2
	--This email excludes invoices uploaded by Dana
	AND FirmID NOT IN (132)


	GROUP BY	DocumentURL,
				SPDocId,
				[Status],
				InvoiceNumber,
				Period,
				FeeAmount,
				DisbursementAmount,
				Adjustment,
				DateCreated,
				DatePayable,
				dc.DefenseCounselID,
				ClaimantDocumentType

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @BodyMessage +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH {font-family: Goudy Old Style; 
      font-size: 10pt; 
      border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
--->
</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @bodymessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;"
>' +
N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Firm</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Type</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Number</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Period</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Fees</th>' +
'<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Expenses</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Total Billed</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Submit Date</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Payable</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		firm)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		DocumentType)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		invoicenum)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		period)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), COALESCE(fees, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), COALESCE(disbursement, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), (COALESCE(fees, 0) + COALESCE(disbursement, 0)), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(100), submitdate, 101))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(100), datepayable, 101))
	AS td,
	''
FROM @NewWeekInvoicesTable i
INNER JOIN vtblFirm f ON f.FirmID = i.firmID
ORDER BY firm, submitdate
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'


--Create temp table for invoices over a week old which have not yet been approved or rejected
DECLARE @OlderThanWeekInvoicesTable TABLE (spdocid nvarchar (100), firmID int, invoicenum nvarchar (100),
period nvarchar (100), fees money, disbursement money, adjustment money, submitdate datetime, datepayable datetime, documenttype nvarchar (500))
INSERT INTO @OlderThanWeekInvoicesTable
	SELECT
		SPDocID,
		--Retrieve the firm's invoice site abbreviation by parsing the documentURL
		--There are two different cases. One for those firms who are categorized as Defense Counsels and 
		--the other for firms who are categorized only as Firms.
		dc.DefenseCounselID AS FirmID,
		InvoiceNumber,
		Period,
		FeeAmount,
		DisbursementAmount,
		Adjustment,
		DateCreated,
		DatePayable,
		claimantdocumenttype
	FROM tblDocuments d
	INNER JOIN tblDefenseCounsel dc ON dc.DefenseCounselID = d.FirmID
	INNER JOIN tblClaimantDocumentType dt ON dt.ClaimantDocumentTypeID = d.DetailDocumentTypeID
	WHERE Datecreated < GETDATE() - 7
	AND DocumentGroupID = 2
	AND d.StatusID IS NULL
	--This email excludes invoices uploaded by Dana
	AND d.FirmID NOT IN (132)
	GROUP BY	DocumentURL,
				SPDocId,
				InvoiceNumber,
				Period,
				FeeAmount,
				DisbursementAmount,
				Adjustment,
				DateCreated,
				DatePayable,
				dc.DefenseCounselID,
				ClaimantDocumentType

if (SELECT
	COUNT(spdocid)
FROM @OlderThanWeekInvoicesTable) > 0
BEGIN
--Add header for table 2
SET @BodyMessage = @BodyMessage + '<H4>The following invoices were uploaded before ' + CONVERT(varchar, GETDATE() - 7, 107) + ' and remain unapproved.</H4>'


--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
SET @BodyMessage = @bodymessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Firm</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Type</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Invoice Number</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Period</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Fees</th>' +
'<th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Expenses</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Total Billed</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Submit Date</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Payable</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		firm)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		documenttype)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		invoicenum)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		period)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), COALESCE(fees, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), COALESCE(disbursement, 0), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		'$' + CONVERT(nvarchar(30), (COALESCE(fees, 0) + COALESCE(disbursement, 0)), 1))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(100), submitdate, 101))
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		CONVERT(nvarchar(100), datepayable, 101))
	AS td,
	''
FROM @OlderThanWeekInvoicesTable i
INNER JOIN vtblFirm f ON f.FirmID = i.firmID
ORDER BY firm, submitdate
FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'

end
--Include link to invoice roll up site at the bottom
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = "https://www.kcicextranet.com/clients/dana/cp/Pages/Invoices.aspx"> here</A> to access these invoices.</b><br><br>'

--Include KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br><b>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'

--Execute email-sending stored procedure
EXEC msdb.dbo.sp_send_dbmail --The Demo ClientProfile sends from Demo Client@kcic.com
@profile_name = 'Claims',
@recipients = 'vicki.stringham@danacollc.com',
@blind_copy_recipients = 'Dana@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

END


GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailInvoiceSummary] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailInvoiceSummary] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalEmailInvoiceSummary] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalEmailInvoiceSummary] TO [power_user]
GO
