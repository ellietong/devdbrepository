SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryExternalMissingDocsByClaimLawsuit]
(@MissingDocsTable nvarchar(MAX) output, @ClaimLawsuitID int, @PlaintiffCounselAddressID int)
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @ClaimantDocs TABLE(DODCert int, EstatePapers int, SettlementConfirmation int, 
							MedicarePlaintiff int, MedicarePlaintiffCounsel int, 
							MedicareFormB int, MMSEA int)

DECLARE @MissingDocuments TABLE(MissingDocName nvarchar(100))

INSERT INTO @ClaimantDocs
	SELECT CASE WHEN DeceasedDate IS NULL THEN 2 ELSE sp.DODCert END AS DODCert, 
		CASE WHEN DeceasedDate IS NULL THEN 2 ELSE sp.EstatePapers END AS EstatePapers,
		sp.settlementconfirmation, 
		sp.MedicarePlaintiff,
		sp.MedicarePlaintiffCounsel,
		sp.MedicareFormB,
		sp.MMSEA
	FROM tblClaim c
		LEFT JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID		
		LEFT JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		LEFT join tblSettlementDocumentation sp on sp.SetPymtID = spd.SetPymtID
	WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
	
--GET PC OF CLAIM LAWSUIT
DECLARE @PlaintiffCounselID bigint = (	SELECT PlaintiffCounselID 
										FROM tblClaimLawsuit cl
										LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
										INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID WHERE cl.ClaimLawsuitID = @ClaimLawsuitID)
	
		
--DOCUMENTATION REQUIRED FOR ALL PLAINTIFFS COUNSEL
INSERT INTO @MissingDocuments
	SELECT 'Death Certificate' AS MissingDocName
	FROM @ClaimantDocs
	WHERE DODCert IS NULL Or DODCert = 0
INSERT INTO @MissingDocuments
	SELECT 'Estate Paperwork' AS MissingDocName
	FROM @ClaimantDocs
	WHERE EstatePapers IS NULL Or EstatePapers = 0
INSERT INTO @MissingDocuments
	SELECT 'Release Request Form' AS MissingDocName
	WHERE NOT EXISTS (
		SELECT d.DocumentID
		FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		WHERE ClaimLawsuitID = @ClaimLawsuitID
				AND DetailDocumentTypeID = 32)	
		AND (SELECT ReleaseRqstDate FROM tblSettlePaymentDocumentation WHERE ClaimLawsuitID = @ClaimLawsuitID) IS NULL		
INSERT INTO @MissingDocuments
	SELECT 'Unexecuted Release' AS MissingDocName
	WHERE NOT EXISTS (
		SELECT d.DocumentID
		FROM tblDocuments d
		INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
		WHERE ClaimLawsuitID = @ClaimLawsuitID
				AND DetailDocumentTypeID = 55)--54 = unexecuted release on dev, 55 on prod
	AND (SELECT ReleaseToPC FROM tblSettlePaymentDocumentation WHERE ClaimLawsuitID = @ClaimLawsuitID) IS NULL
	
	
INSERT INTO @MissingDocuments
		SELECT 'Executed Release' AS MissingDocName
		WHERE NOT EXISTS (
			SELECT d.DocumentID
			FROM tblDocuments d
			INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
			WHERE ClaimLawsuitID = @ClaimLawsuitID
					AND DetailDocumentTypeID = 31)
		AND (SELECT ExecutedReleaseDate FROM tblSettlePaymentDocumentation WHERE ClaimLawsuitID = @ClaimLawsuitID) IS NULL
					
					
					
--Load firms that need MMSEA form
DECLARE @MMSEA TABLE(PlaintiffCounselID int)
INSERT INTO @MMSEA  --, Keller, Mulvihill respectively
SELECT 643 --Deaton
UNION
SELECT 519 --Keller
UNION
SELECT 952 --Mulvihill

--Load firms that need Medicare Form B
DECLARE @FormB TABLE(PlaintiffCounselID int)
INSERT INTO @FormB  
SELECT 26 --Cooney & Conway
UNION
SELECT 257 --Kazan
UNION
SELECT 294 --O'Brien

--FIRMS WITH REP of PLAINTIFF
DECLARE @RepOfPlaintiff TABLE(PlaintiffCounselID int)
INSERT INTO @RepOfPlaintiff  
SELECT 643 --Deaton
UNION
SELECT 952 --Mulvihill
UNION
SELECT 150 --Weitz
UNION
SELECT 519 --Keller



--FIRMS WITH REP of PLAINTIFF COUNSEL
DECLARE @RepOfPlaintiffCounsel TABLE(PlaintiffCounselID int)
INSERT INTO @RepOfPlaintiffCounsel  
SELECT 150 --Weitz

--MMSEA
IF(@PlaintiffCounselID IN (SELECT PlaintiffCounselID FROM @MMSEA))
BEGIN			
	INSERT INTO @MissingDocuments
	SELECT 'MMSEA Settlement Form' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MMSEA IS NULL Or MMSEA = 0
END

--FORM B
IF(@PlaintiffCounselID IN (SELECT PlaintiffCounselID FROM @FormB))
BEGIN			
	INSERT INTO @MissingDocuments
	SELECT 'Medicare Form B' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MedicareFormB IS NULL Or MedicareFormB = 0
END

--REP OF PLAINTIFF
IF(@PlaintiffCounselID IN (SELECT PlaintiffCounselID FROM @RepOfPlaintiff))
BEGIN			
	INSERT INTO @MissingDocuments
	SELECT 'Representation of Plaintiff' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MedicarePlaintiff IS NULL Or MedicarePlaintiff = 0
END

--REP OF PLAINTIFF COUNSEL
IF(@PlaintiffCounselID IN (SELECT PlaintiffCounselID FROM @RepOfPlaintiffCounsel))
BEGIN			
	INSERT INTO @MissingDocuments
	SELECT 'Representation of Plaintiff Counsel' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MedicarePlaintiffCounsel IS NULL Or MedicarePlaintiffCounsel = 0
END


--DEFAULT: ALL OTHER PLAINTIFFS COUNSEL (ie: those not in the tables constructed above)
IF(@PlaintiffCounselID NOT IN (	SELECT PlaintiffCounselID FROM @MMSEA
								UNION
								SELECT PlaintiffCounselID FROM @FormB
								UNION
								SELECT PlaintiffCounselID FROM @RepOfPlaintiff
								UNION
								SELECT PlaintiffCounselID FROM @RepOfPlaintiffCounsel
))
BEGIN
	--MMSEA
	INSERT INTO @MissingDocuments
	SELECT 'MMSEA Settlement Form' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MMSEA IS NULL Or MMSEA = 0
	
	--REP OF PLAINTIFF	
	INSERT INTO @MissingDocuments
	SELECT 'Representation of Plaintiff' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MedicarePlaintiff IS NULL Or MedicarePlaintiff = 0

	--REP OF PLAINTIFF COUNSEL	
	INSERT INTO @MissingDocuments
	SELECT 'Representation of Plaintiff Counsel' AS MissingDocName
	FROM @ClaimantDocs
	WHERE MedicarePlaintiffCounsel IS NULL Or MedicarePlaintiffCounsel = 0

END

--SELECT MissingDocName FROM @MissingDocuments

DECLARE @TDStyle nvarchar(max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'


SET @MissingDocsTable = @MissingDocsTable + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' + N'<tr>' +
	
	
CAST ( (
SELECT	@TDStyle AS 'td/@style', (SELECT MissingDocName) as td, ''
FROM @MissingDocuments md
for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
N'</table>' +  N'<br/>'									

END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryExternalMissingDocsByClaimLawsuit] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryExternalMissingDocsByClaimLawsuit] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalMissingDocsByClaimLawsuit] TO [Claims_User]
GRANT EXECUTE ON  [dbo].[qryExternalMissingDocsByClaimLawsuit] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[qryExternalMissingDocsByClaimLawsuit] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryExternalMissingDocsByClaimLawsuit] TO [power_user]
GO
