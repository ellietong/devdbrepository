SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryALLColumnOn] (@AllocationID int, @OriginalOccurrenceID int)

As

Set NoCount On
	
Update ztblPolicyDateIDTriggeredSig
set BlockTriggered = 1
Where OriginalOccurrenceID = @OriginalOccurrenceID and AllocationID = @AllocationID
	
UPDATE ztblSigLevel
SET ColumnOn = 1
FROM ztblSigLevel z 
Inner Join ztblPolicyDateIDTriggeredSig zt on z.ColumnID=zt.ColumnID and zt.PolicyID =z.PolicyID
Where AllocationID = @AllocationID and BlockTriggered = 1
	
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryALLColumnOn] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryALLColumnOn] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryALLColumnOn] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryALLColumnOn] TO [power_user]
GO
