SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument]
(@ClaimantPersonalInfoID bigint, @DocumentID bigint, @ClaimLawsuitID bigint=null, @Emailaddress nvarchar(250))
AS
SET NOCOUNT ON

DECLARE @PlaintiffCounselAddressID bigint = (SELECT PlaintiffCounselAddressID FROM vtblPerson WHERE Email = @Emailaddress)

IF(@ClaimLawsuitID IS NULL AND (SELECT DetailDocumentTypeID FROM tblDocuments WHERE DocumentID = @DocumentID) IN (31, 32, 55))
BEGIN
	SET @ClaimLawsuitID = (SELECT cl.ClaimLawsuitID
							FROM tblClaim c
							INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
							INNER JOIN tblClaimantSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
							WHERE IsCurrentClaim = 1 and IsPrimaryForClaim = 1
							AND ClaimantPersonalInfoID = @ClaimantPersonalInfoID
							AND MatterID = (SELECT TOP 1 MatterID FROM tblDocuments WHERE DocumentID = @DocumentID)
							AND EXISTS (SELECT SetPymtID FROM tblSettlePaymentDocumentation spd WHERE cl.ClaimLawsuitID = spd.ClaimLawsuitID))
							
END

INSERT INTO tblMultiClaimantDocument (ClaimantPersonalInfoID, DocumentID, ClaimLawsuitID, SPUsername)
	VALUES(@ClaimantPersonalInfoID, @DocumentID, @ClaimLawsuitID, @Emailaddress)

----If no ClaimLawsuitID passed from 
--IF(@ClaimLawsuitID IS NULL)
--BEGIN
--	SET @ClaimLawsuitID = (SELECT ClaimLawsuitID 
--							FROM tblClaim c INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID 
--							WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID 
--							AND IsCurrentClaim = 1
--							AND IsPrimaryForClaim = 1
--							AND EXISTS (SELECT SetPymtID FROM tblSettlePaymentDocumentation spd
--										WHERE spd.ClaimLawsuitID = cl.ClaimLawsuitID))	
--END

--Update ReleaseRqstDate when RRF uploadded
IF(SELECT DetailDocumentTypeID FROM tblDocuments WHERE DocumentID = @DocumentID) = 32
BEGIN
	UPDATE tblSettlePaymentDocumentation
	SET ReleaseRqstDate = GETDATE(),
		SPUsername = @Emailaddress
	WHERE ClaimLawsuitID = @ClaimLawsuitID
END

--Update ReleaseToPC when Unexecuted Release Uploaded
IF(SELECT DetailDocumentTypeID FROM tblDocuments WHERE DocumentID = @DocumentID) = 55 --55 on prod, 54 on dev
BEGIN
	UPDATE tblSettlePaymentDocumentation
	SET ReleaseToPC = GETDATE(),
		SPUsername = @Emailaddress
	WHERE ClaimLawsuitID = @ClaimLawsuitID
END

--Update ReleaseRcvd when Release uploaded
IF(SELECT DetailDocumentTypeID FROM tblDocuments WHERE DocumentID = @DocumentID) = 31
BEGIN
	UPDATE tblSettlePaymentDocumentation
	SET ExecutedReleaseDate = GETDATE(),
		SPUsername = @Emailaddress
	WHERE ClaimLawsuitID = @ClaimLawsuitID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryAddMultiClaimantDocument] TO [power_user]
GO
