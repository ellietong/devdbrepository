SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryGetJobsiteCityCompletionList]
(@prefix nvarchar(MAX), @count int = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SET ROWCOUNT @count
	SELECT DISTINCT JobsiteCity FROM tblClaimantEmployment WHERE JobsiteCity LIKE @prefix + '%'
	SET ROWCOUNT 0
END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryGetJobsiteCityCompletionList] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryGetJobsiteCityCompletionList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryGetJobsiteCityCompletionList] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryGetJobsiteCityCompletionList] TO [power_user]
GO
