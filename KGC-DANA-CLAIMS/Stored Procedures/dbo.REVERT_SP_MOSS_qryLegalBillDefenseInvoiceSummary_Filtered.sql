SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryLegalBillDefenseInvoiceSummary_Filtered] (@emailaddress nvarchar(250), @FilterFirmID int, @MatterID int, @StatusID int)
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	 --SET NOCOUNT ON added to prevent extra result sets from
	 --interfering with SELECT statements.
	SET NOCOUNT ON;

--Declare @DefenseCounselAddressID int
--Set @DefenseCounselAddressID = null

--Declare @emailaddress nvarchar(250)
--Set @emailaddress = 'crookc@kcicllc.com'

--DECLARE @FilterFirmID int
--SET @FilterFirmID = NULL
--DECLARE @MatterID int
--SET @MatterID = NULL
--DECLARE @StatusID int
--SET @StatusID = NULL

DECLARE @MaxMatterID int
SET @MaxMatterID = (SELECT MAX(matterID) FROM tblMatter)
DECLARE @MaxFirmID int
SET @MaxFirmID = (SELECT MAX(DefenseCounselID) FROM tblDefenseCounsel)
DECLARE @MaxStatusID int
SET @MaxStatusID = (SELECT MAX([LegalBillStatusID]) FROM m_tblLegalBillStatus)


Declare @UnpaidInvoice table (InvoiceID int, IsFullyPaid bit)
Insert into @UnpaidInvoice

Select	InvoiceID, 
		Case When IsFullyPaid is Null and NavPaidInvoice = 0 then 0 
		When IsFullyPaid is Null and NavPaidInvoice = 1 then 1
		else IsFullyPaid end as IsFullyPaid 
From	m_tblLegalBillInvoices i
	Left Join tblPaymentDefense pd on i.InvoiceID = pd.DefenseInvoiceID
Group By InvoiceID, 
		Case When IsFullyPaid is Null and NavPaidInvoice = 0 then 0 When IsFullyPaid is Null and NavPaidInvoice = 1 then 1 else IsFullyPaid end
		
		
DECLARE @firmid int
SET @firmid = (SELECT Case when RIGHT(@emailaddress, 15)='smithmurphy.com' then '115' else FirmID end FROM vtblperson WHERE Email = @emailaddress)

IF(RIGHT(@emailaddress, 11)='kcicllc.com' OR @firmid = 132) --Update this so it shows all for Vicki, too.
BEGIN
Select	i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end as OriginalInvoiceNumber, 
		MAX(Period) as MaxPeriod,
		--convert(nvarchar(250),Min(Period)) + ' - ' +convert(nvarchar(250),Max(Period)) as MaxPeriod, 
		SubmittedDate, 
		Max(DanaApprovalDate) as MaxApprovedDate, 
		Case when s.LegalBillStatus IS null then 'Unknown' 
		When IsFullyPaid = 1 then 'Paid'
		When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end as Status, 
		Sum(OriginalTotal) as OriginalInvoiceTotal, 
		Case when AppealFlag = 1 then Sum(AppealTotal) Else Sum(AdjustedTotal) END as FinalizedInvoiceTotal
From	m_tblLegalBillInvoices i
	Left Join tblDefenseCounselAddress d on d.DefenseCounselAddressID = i.DefenseCounselAddressID
	Left Join tblMatter m on i.MatterID = m.MatterID
	Left Join m_tblLegalBillStatus s on i.StatusID=s.LegalBillStatusID
	Left Join @UnpaidInvoice u on i.InvoiceID = u.InvoiceID
Where	((coalesce(submitteddate, danaapprovaldate, period) >= (GETDATE()-180)) 
		OR u.IsFullyPaid = 0 OR i.NavPaidInvoice = 0)  
		AND i.MatterID >= COALESCE(@MatterID, 0) AND i.MatterID <= COALESCE(@MatterID, @maxMatterID)
		AND i.StatusID >= COALESCE(@StatusID, 0) AND i.StatusID <= COALESCE(@StatusID, @MaxStatusID)
		--AND i.DefenseCounselAddressID >= COALESCE(@firmID, 0) AND i.StatusID <= COALESCE(@firmID, @MaxFirmID)
		--AND i.DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
Group By	i.DefenseCounselAddressID, 
			DefenseCounsel, 
			Matter, 
			SubmittedDate,
			Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end, 
			Case when s.LegalBillStatus IS null then 'Unknown' 
			When IsFullyPaid = 1 then 'Paid'
			When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end,
			AppealFlag
Order By	SubmittedDate desc, DefenseCounsel, Matter
END
else if (Select personID FROM vtblPerson WHERE Email = @emailaddress) IN (1405, 1406)
BEGIN
	Select	i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end as OriginalInvoiceNumber, 
		MAX(Period) as MaxPeriod,
		--convert(nvarchar(250),Min(Period)) + ' - ' +convert(nvarchar(250),Max(Period)) as MaxPeriod, 
		SubmittedDate, 
		Max(DanaApprovalDate) as MaxApprovedDate, 
		Case when s.LegalBillStatus IS null then 'Unknown' 
		When IsFullyPaid = 1 then 'Paid' 
		When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end as Status, 
		Sum(OriginalTotal) as OriginalInvoiceTotal, 
		Case when AppealFlag = 1 then Sum(AppealTotal) Else Sum(AdjustedTotal) END as FinalizedInvoiceTotal
From	m_tblLegalBillInvoices i
	Left Join tblDefenseCounselAddress d on d.DefenseCounselAddressID = i.DefenseCounselAddressID
	Left Join tblMatter m on i.MatterID = m.MatterID
	Left Join m_tblLegalBillStatus s on i.StatusID=s.LegalBillStatusID
	Left Join @UnpaidInvoice u on i.InvoiceID = u.InvoiceID
Where	
	i.DefenseCounselAddressID = 0
Group By	i.DefenseCounselAddressID, 
			DefenseCounsel, 
			Matter, 
			SubmittedDate,
			Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end, 
			Case when s.LegalBillStatus IS null then 'Unknown' 
			When IsFullyPaid = 1 then 'Paid'
			When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end,
			AppealFlag
Order By	SubmittedDate desc, DefenseCounsel, Matter

END
ELSE IF @firmid IN (100,117)
--Celba LLC and Steven Celba LLC invoices
BEGIN
Select	i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end as OriginalInvoiceNumber, 
		MAX(Period) as MaxPeriod,
		--convert(nvarchar(250),Min(Period)) + ' - ' +convert(nvarchar(250),Max(Period)) as MaxPeriod, 
		SubmittedDate, 
		Max(DanaApprovalDate) as MaxApprovedDate, 
		Case when s.LegalBillStatus IS null then 'Unknown' 
		When IsFullyPaid = 1 then 'Paid'
		When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end as Status, 
		Sum(OriginalTotal) as OriginalInvoiceTotal, 
		Case when AppealFlag = 1 then Sum(AppealTotal) Else Sum(AdjustedTotal) END as FinalizedInvoiceTotal
From	m_tblLegalBillInvoices i
	Left Join tblDefenseCounselAddress d on d.DefenseCounselAddressID = i.DefenseCounselAddressID
	Left Join tblMatter m on i.MatterID = m.MatterID
	Left Join m_tblLegalBillStatus s on i.StatusID=s.LegalBillStatusID
	Left Join @UnpaidInvoice u on i.InvoiceID = u.InvoiceID
Where	((coalesce(submitteddate, danaapprovaldate, period) >= (GETDATE()-180)) 
		OR u.IsFullyPaid = 0 OR i.NavPaidInvoice = 0)  
		AND i.DefenseCounselAddressID IN (SELECT DefenseCounselAddressID FROM tblDefenseCounselAddress WHERE DefenseCounselID IN (100,117))
		AND i.MatterID >= COALESCE(@MatterID, 0) AND i.MatterID <= COALESCE(@MatterID, @maxMatterID)
		AND i.StatusID >= COALESCE(@StatusID, 0) AND i.StatusID <= COALESCE(@StatusID, @MaxStatusID)
Group By	i.DefenseCounselAddressID, 
			DefenseCounsel, 
			Matter, 
			SubmittedDate,
			Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end, 
			Case when s.LegalBillStatus IS null then 'Unknown' 
			When IsFullyPaid = 1 then 'Paid'
			When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end,
			AppealFlag
Order By	SubmittedDate desc, DefenseCounsel, Matter

END
ELSE IF @firmid IN (115)
--Smith Murphy invoices --Smith Murphy users coded as part of Reed Smith Firm in order to view Reed Smith claimants, however should not see Reed Smith invoices
BEGIN
Select	i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end as OriginalInvoiceNumber, 
		MAX(Period) as MaxPeriod,
		--convert(nvarchar(250),Min(Period)) + ' - ' +convert(nvarchar(250),Max(Period)) as MaxPeriod, 
		SubmittedDate, 
		Max(DanaApprovalDate) as MaxApprovedDate, 
		Case when s.LegalBillStatus IS null then 'Unknown' 
		When IsFullyPaid = 1 then 'Paid'
		When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end as Status, 
		Sum(OriginalTotal) as OriginalInvoiceTotal, 
		Case when AppealFlag = 1 then Sum(AppealTotal) Else Sum(AdjustedTotal) END as FinalizedInvoiceTotal
From	m_tblLegalBillInvoices i
	Left Join tblDefenseCounselAddress d on d.DefenseCounselAddressID = i.DefenseCounselAddressID
	Left Join tblMatter m on i.MatterID = m.MatterID
	Left Join m_tblLegalBillStatus s on i.StatusID=s.LegalBillStatusID
	Left Join @UnpaidInvoice u on i.InvoiceID = u.InvoiceID
Where	((coalesce(submitteddate, danaapprovaldate, period) >= (GETDATE()-180)) 
		OR u.IsFullyPaid = 0 OR i.NavPaidInvoice = 0)  
		AND i.DefenseCounselAddressID IN (SELECT DefenseCounselAddressID FROM tblDefenseCounselAddress WHERE DefenseCounselID IN (115))
		AND i.MatterID >= COALESCE(@MatterID, 0) AND i.MatterID <= COALESCE(@MatterID, @maxMatterID)
		AND i.StatusID >= COALESCE(@StatusID, 0) AND i.StatusID <= COALESCE(@StatusID, @MaxStatusID)
Group By	i.DefenseCounselAddressID, 
			DefenseCounsel, 
			Matter, 
			SubmittedDate,
			Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end, 
			Case when s.LegalBillStatus IS null then 'Unknown' 
			When IsFullyPaid = 1 then 'Paid'
			When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end,
			AppealFlag
Order By	SubmittedDate desc, DefenseCounsel, Matter

END
ELSE
BEGIN
Select	i.DefenseCounselAddressID, 
		DefenseCounsel, 
		Matter, 
		Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end as OriginalInvoiceNumber, 
		MAX(Period) as MaxPeriod,
		--convert(nvarchar(250),Min(Period)) + ' - ' +convert(nvarchar(250),Max(Period)) as MaxPeriod, 
		SubmittedDate, 
		Max(DanaApprovalDate) as MaxApprovedDate, 
		Case when s.LegalBillStatus IS null then 'Unknown' 
		When IsFullyPaid = 1 then 'Paid'
		When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end as Status, 
		Sum(OriginalTotal) as OriginalInvoiceTotal, 
		Sum(AdjustedTotal + AppealTotal) as FinalizedInvoiceTotal,
		Case when AppealFlag = 1 then Sum(AppealTotal) Else Sum(AdjustedTotal) END as FinalizedInvoiceTotal
From	m_tblLegalBillInvoices i
	Left Join tblDefenseCounselAddress d on d.DefenseCounselAddressID = i.DefenseCounselAddressID
	Left Join tblMatter m on i.MatterID = m.MatterID
	Left Join m_tblLegalBillStatus s on i.StatusID=s.LegalBillStatusID
	Left Join @UnpaidInvoice u on i.InvoiceID = u.InvoiceID
Where	((coalesce(submitteddate, danaapprovaldate, period) >= (GETDATE()-180)) 
		OR u.IsFullyPaid = 0 OR i.NavPaidInvoice = 0)  
		AND i.DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
		AND i.MatterID >= COALESCE(@MatterID, 0) AND i.MatterID <= COALESCE(@MatterID, @maxMatterID)
		AND i.StatusID >= COALESCE(@StatusID, 0) AND i.StatusID <= COALESCE(@StatusID, @MaxStatusID)
Group By	i.DefenseCounselAddressID, 
			DefenseCounsel, 
			Matter, 
			SubmittedDate,
			Case when OriginalInvoiceNumber IS null then 'Unknown' else OriginalInvoiceNumber end, 
			Case when s.LegalBillStatus IS null then 'Unknown' 
			When IsFullyPaid = 1 then 'Paid'
			When NavPaidInvoice = 1 then 'Paid By Navigant' else s.LegalBillStatus end,
			AppealFlag
Order By	SubmittedDate desc, DefenseCounsel, Matter
END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryLegalBillDefenseInvoiceSummary_Filtered] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryLegalBillDefenseInvoiceSummary_Filtered] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryLegalBillDefenseInvoiceSummary_Filtered] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryLegalBillDefenseInvoiceSummary_Filtered] TO [power_user]
GO
