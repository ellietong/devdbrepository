SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryClaimantEmploymentInfo]
@ClaimantPersonalInfoID bigint=null
AS
--DECLARE @ClaimantPersonalInfoID bigint
--SET @ClaimantPersonalInfoID = 88805
--IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR @emailaddress = 'vicki.stringham@danacollc.com'
--BEGIN
Select ce.ClaimantEmploymentID, 
	ce.ClaimantPersonalInfoID, 
	Occupation, 
	Jobsite, 
	Comments, 
	EmpStartDate, 
	EmpEndDate, 
	EarliestExposureDate, 
	LatestExposureDate, 
	IsPrimary, 
	ProductManufacturer, 
	pt.ProductType,
	ProductBrand,
	pn.ProductName
From tblClaimantEmployment ce
INNER join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
left join tblProduct p on p.ProductID = cp.ProductID
left join tblProductName pn on pn.ProductNameID=p.ProductNameID
left join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
left join tblProductManufacturer pm on pm.ProductManufacturerID=p.ProductManufacturerID
left join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
left join tblOccupation o ON ce.OccupationID = o.OccupationID
WHERE ce.ClaimantPersonalInfoID=@ClaimantPersonalInfoID
--END
--ELSE
--BEGIN
--Select ce.ClaimantEmploymentID, 
--	ce.ClaimantPersonalInfoID, 
--	Occupation, 
--	Jobsite, 
--	Comments, 
--	EmpStartDate, 
--	EmpEndDate, 
--	EarliestExposureDate, 
--	LatestExposureDate, 
--	IsPrimary, 
--	ProductManufacturer, 
--	pt.ProductType,
--	ProductBrand,
--	pn.ProductName
--From tblClaimantEmployment ce
--left join tblClaimantProduct cp on cp.ClaimantEmploymentID = ce.ClaimantEmploymentID
--left join tblProduct p on p.ProductID = cp.ProductID
--left join tblProductName pn on pn.ProductNameID=p.ProductNameID
--left join tblProductBrand pb on pb.ProductBrandID = p.ProductBrandID
--left join tblProductManufacturer pm on pm.ProductManufacturerID=p.ProductManufacturerID
--left join tblProductType pt on pt.ProductTypeID = p.ProductTypeID
--left join tblOccupation o ON ce.OccupationID = o.OccupationID
--LEFT JOIN tblClaim c ON ce.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
--left join tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
--left join tblLawsuit l ON cl.LawsuitID = l.LawsuitID
--WHERE ce.ClaimantPersonalInfoID=@ClaimantPersonalInfoID
--AND DefenseCounselAddressID = (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress)
--END
GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantEmploymentInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantEmploymentInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantEmploymentInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantEmploymentInfo] TO [power_user]
GO
