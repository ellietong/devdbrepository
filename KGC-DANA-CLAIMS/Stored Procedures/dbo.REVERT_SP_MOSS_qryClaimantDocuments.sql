SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REVERT_SP_MOSS_qryClaimantDocuments]
(@ClaimantPersonalInfoID bigint = null, @emailaddress nvarchar(250))
AS
--DECLARE @emailaddress nvarchar(250)
--SET @emailaddress = 'snyderd@kcicllc.com'
--DECLARE @ClaimantPersonalInfoID bigint
--SET @ClaimantPersonalInfoID = 9494 
BEGIN

--Declare @UserFirmState table (DefenseCounselID bigint, StateID int)
--Insert into @UserFirmState

--Select dca.DefenseCounselID, StateID
--From dbo.tblDefenseCounselState AS dcs 
--left JOIN dbo.tblDefenseCounselAddress dca on dca.defensecounseladdressid = dcs.defensecounseladdressid
--LEFT join dbo.tbldefensecounseladdress dcar on dcar.defensecounseladdressid = dcs.ResponsibleDefenseCounseladdressid --and dcar.stateID = dcs.StateID 
--LEFT join dbo.tblDefenseCounsel AS d ON d.DefenseCounselID = dca.DefenseCounselID
--left JOIN dbo.tblDefenseCounsel AS r ON r.DefenseCounselID = dcar.DefenseCounselID
--Where (ResponsibleDefenseCounselAddressID IN (SELECT FirmAddressID FROM vtblPerson WHERE Email = @emailaddress) OR
--dca.DefenseCounselID in (SELECT f.FirmID FROM vtblPerson p inner join vtblFirm f on p.FirmID=f.FirmID WHERE Email = @emailaddress))


IF RIGHT(@emailaddress, 11) = 'kcicllc.com' OR (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress) IN (132, 133) OR (SELECT NoFilters FROM vtblPerson WHERE Email = @emailaddress) = 1
BEGIN


SELECT  DISTINCT   d.DocumentID, d.SPDocId, d.DocumentTitle, 
CASE WHEN d.FirmID IS NULL THEN DocumentURL ELSE '/clients/Dana/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/clients/Dana/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), @ClaimantPersonalInfoID) END AS DocumentURL, dg.DocumentGroup, cdt.ClaimantDocumentType, 
                      COALESCE(f.Firm, 'Historical') AS Firm, d.AcceptedBy, d.DateAccepted, d.Datecreated, mcd.ClaimantPersonalInfoID, DocumentURL as DirectDocumentURL, CreatedBy
FROM tblMultiClaimantDocument mcd INNER JOIN         
	dbo.tblDocuments AS d ON mcd.DocumentID = d.documentID INNER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID INNER JOIN
                      dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID 
                      LEFT JOIN dbo.vtblFirm AS f ON f.FirmID = d.FirmID          
WHERE mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
AND dg.DocumentGroupID <> 2 AND cdt.ClaimantDocumentTypeID NOT IN (40)
--UNION


--SELECT cd.ClaimantDocumentID, NULL as SpDocID, DocumentTitle, DocumentURL, dg.DocumentGroup, ClaimantDocumentType, 'Historical' AS Firm, 'Historical' as AcceptedBy,
--NULL as DateAccepted, NULL as DateALTERd, hmcd.ClaimantPersonalInfoID, DocumentURL as DirectDocumentURL

--FROM         tblClaimantDocuments cd 
--			INNER JOIN tblHistoricalMultiClaimantDocuments hmcd ON cd.ClaimantDocumentID = hmcd.ClaimantDocumentID
--            INNER JOIN dbo.tblClaimantDocumentType cdt ON cd.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
--            INNER JOIN dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID
--            WHERE ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND DocumentTitle IS NOT NULL
--			AND dg.DocumentGroupID <> 2 AND cdt.ClaimantDocumentTypeID NOT IN (1, 40)


ORDER BY Datecreated DESC
END

ELSE

BEGIN

SELECT  DISTINCT   d.DocumentID, d.SPDocId, d.DocumentTitle, 
CASE WHEN d.FirmID IS NULL THEN DocumentURL ELSE '/clients/Dana/cp/Pages/DocumentProfile.aspx?ItemId=' + CONVERT(nvarchar(100), d.DocumentID) + '&source=' + '/clients/Dana/cp/Pages/ClaimantDetails.aspx?ClaimantPersonalInfoID=' + CONVERT(nvarchar(100), @ClaimantPersonalInfoID) END AS DocumentURL, 
dg.DocumentGroup, cdt.ClaimantDocumentType, 
                       COALESCE(f.Firm, 'Historical') AS Firm, d.AcceptedBy, d.DateAccepted, d.Datecreated, mcd.ClaimantPersonalInfoID, Matter, DocumentURL as DirectDocumentURL, CreatedBy
FROM tblMultiClaimantDocument mcd INNER JOIN         
	dbo.tblDocuments AS d ON mcd.DocumentID = d.documentID INNER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimantDocumentType AS cdt ON d.DetailDocumentTypeID = cdt.ClaimantDocumentTypeID INNER JOIN
                      dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID INNER JOIN
                      dbo.tblClaim AS c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT JOIN
                      dbo.vtblFirm AS f ON f.FirmID = d.FirmID LEFT JOIN
                      dbo.tblMatter m ON d.MatterID = m.MatterID
                      LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
                      LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
WHERE mcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID
AND dg.DocumentGroupID NOT IN (2, 4) AND cdt.ClaimantDocumentTypeID NOT IN (1, 40)
--UNION 


--SELECT cd.ClaimantDocumentID, NULL as SpDocID, DocumentTitle, DocumentURL, dg.DocumentGroup, ClaimantDocumentType, 'Historical' AS Firm, 'Historical' as AcceptedBy,
--NULL as DateAccepted, NULL as DateALTERd, hmcd.ClaimantPersonalInfoID, NULL as Matter, DocumentURL as DirectDocumentURL

--FROM         tblClaimantDocuments cd 
--			INNER JOIN tblHistoricalMultiClaimantDocuments hmcd ON cd.ClaimantDocumentID = hmcd.ClaimantDocumentID
--            INNER JOIN dbo.tblClaimantDocumentType cdt ON cd.ClaimantDocumentTypeID = cdt.ClaimantDocumentTypeID 
--            INNER JOIN dbo.tblDocumentGroup AS dg ON dg.DocumentGroupID = cdt.DocumentGroupID
--            INNER JOIN dbo.tblClaim AS c ON hmcd.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
--            LEFT JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
--            LEFT JOIN tblLawsuit L ON cl.LawsuitID = l.LawsuitID
--            WHERE hmcd.ClaimantPersonalInfoID = @ClaimantPersonalInfoID AND DocumentTitle IS NOT NULL
--			AND dg.DocumentGroupID NOT IN (2, 4) AND cdt.ClaimantDocumentTypeID NOT IN (1, 40)
ORDER BY Datecreated DESC


END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantDocuments] TO [base_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantDocuments] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[REVERT_SP_MOSS_qryClaimantDocuments] TO [power_user]
GRANT EXECUTE ON  [dbo].[REVERT_SP_MOSS_qryClaimantDocuments] TO [power_user]
GO
