SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[WSS_SETTLEMENT_qryOutstandingSettlementsWithIDs]  (@emailaddress nvarchar(250), @SettlementGroupID int = NULL)

As

SET NOCOUNT ON

--DECLARE @EmailAddress nvarchar(250) = 'owenss@kcic.com'
--DECLARE @SettlementGroupID int = NULL

DECLARE @MaxSettlementGroupID int = (SELECT MAX(SettlementGroupID) FROM tblSettlementGroup)

DECLARE @QueryString NVARCHAR(MAX) = ''
SET @QueryString = '
SELECT  msd.ClaimantPersonalInfoID AS ClaimantID, 
		msd.ClaimantPersonalInfoID, 
		ClaimantName,
		SSN,
		Disease,
		DiagnosisDate,
		BirthDate, 
		DeceasedDate,
		ReleaseRqstDate,
		ReleaseToPC,
		ExecutedReleaseDate,
		SettlementDate, 
		SettlementAmount, 
		SettlementGroup,
		SettlementGroupID,
		PlaintiffCounselID,
		PlaintiffCounsel,
		StateID,
		COALESCE(Missing, ''All required claimant information submitted.'') AS MissingClaimantInfo,
		COALESCE(MissingDocs, ''All required documents submitted.'') AS MissingDocumentation
FROM vRequiredDocsByClaimant msd
	INNER JOIN dbo.mtvMissingClaimantInfo(0) mtvc ON msd.ClaimantPersonalInfoID = mtvc.ClaimantPersonalInfoID
	INNER JOIN dbo.mtvMissingSettlementDocumentationList(0) mtvd ON spdSetPymtID = mtvd.SetPymtID
ORDER BY SettlementDate desc
  OPTION (RECOMPILE)'

PRINT @QueryString

--Finally, the query created above is executed.
EXEC sp_executesql @QueryString

GO
