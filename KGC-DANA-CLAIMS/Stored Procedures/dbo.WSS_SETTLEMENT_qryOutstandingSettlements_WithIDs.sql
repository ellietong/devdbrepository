SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs]  (@emailaddress nvarchar(250), @SettlementGroupID int = NULL)--, @SettlementDate datetime)

As

SET NOCOUNT ON

--DECLARE @EmailAddress nvarchar(250) = 'randy@gorijulianlaw.com'
--DECLARE @SettlementGroupID int = NULL

DECLARE @PlaintiffCounselAddressID bigint = (SELECT PlaintiffCOunselAddressID FROM vtblPerson WHERE Email = @EmailAddress)

DECLARE @WhereString nvarchar(max) = 'WHERE COALESCE(IsCurrentClaim,0) = 1 
										AND coalesce(clst.isprimarystatus,0) = 1
										AND clst.ClaimLawsuitStatusID in (23, 44, 45) 
										AND StatusDate >= ''4/1/2011''
										AND cs.SettlementGroupID IS NOT NULL
										AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimantSettlementID)
									 '
IF @PlaintiffCounselAddressID IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND l.PlaintiffCounselAddressID = ' + '''' + CONVERT(nvarchar(20),@PlaintiffCOunselAddressID) + '''' END

IF @SettlementGroupID IS NOT NULL   BEGIN SET @WhereString = @WhereString + ' AND cs.SettlementGroupID = ' + '''' + CONVERT(nvarchar(20),@SettlementGroupID) + '''' END

DECLARE @QueryString nvarchar(max) = ''

SET @QueryString = '
	SELECT --cpi.LastName + '', '' + FirstName + COALESCE('' '' + MIddleName, '''') As ClaimantName, 
	--cpi.LastName + '', '' + FirstName + COALESCE('' '' + MIddleName, '''') + '' '' +  st.State + '' '' + convert(nvarchar, l.StateID) + '' '' AS ClaimantName,
	cpi.LastName + '', '' + FirstName + COALESCE('' '' + MIddleName, '''') AS ClaimantName,
	cl.ClaimLawsuitID,
	c.ClaimantPersonalInfoID,
	cl.LawsuitID,
	--PlaintiffCounsel + '' (''+ convert(nvarchar, pca.PlaintiffCounselID) +'')'' AS PlaintiffCounsel,
	PlaintiffCounsel AS PlaintiffCounsel,
	SettlementDate,
	UPPER(SettlementGroup) AS SettlementGroup,
	cs.SettlementGroupID,
	Matter,
	MissingDocs as missing,
	l.stateid
	FROM tblClaim c
		INNER JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		INNER JOIN tblMatter m		ON c.MatterID = m.MatterID	
		INNER JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
		INNER JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		INNER JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
		INNER JOIN tblState st ON l.StateID = st.StateID
		INNER JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID	
		LEFT JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
		LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		INNER JOIN mtvMissingSettlementDocumentationListPP(0) mtv ON spd.SetPymtID = mtv.SetPymtID
'
+ @WhereString +
'
	GROUP BY	cpi.LastName + '', '' + FirstName + COALESCE('' '' + MIddleName, ''''), 
	--cpi.LastName + '', '' + FirstName + COALESCE('' '' + MIddleName, '''') + '' '' + st.State + '' '' + convert(nvarchar, l.StateID),
				cl.ClaimLawsuitID,
				c.ClaimantPersonalInfoID,
				cl.LawsuitID,
				PlaintiffCounsel,
				pca.PlaintiffCounselID,
				SettlementDate,
				SettlementGroup,
				cs.SettlementGroupID,
				Matter,
				MissingDocs,
				l.stateid
	ORDER BY	SettlementDate desc, 
				SettlementGroup,
				cpi.LastName + '', '' + FirstName + COALESCE('' '' + MIddleName, '''')
				--cpi.LastName + '', '' + FirstName + COALESCE('' '' + MIddleName, '''') + '' '' + st.State + '' '' + convert(nvarchar, l.StateID)

'

PRINT @QueryString
EXEC sp_executesql @QueryString
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs] TO [power_user]
GO
