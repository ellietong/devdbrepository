SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Function [dbo].[fnNextLowestAttachmentPoint]
(@AllocationID int, @InsuranceProgramID int, 
@TriggerStart  datetime, @TriggerEnd datetime,  
@LimitTypeID int, @UpperLimit money) 

Returns money

AS 

----For Testing
--DECLARE @AllocationID int
--DECLARE @InsuranceProgramID nvarchar(250)
--DECLARE @triggerstart datetime
--DECLARE @triggerend datetime
--DECLARE @limittypeID int
--Declare @UpperLimit money
-- SET @allocationId = 232
-- SET @InsuranceProgramID = '1'
-- SET @triggerstart = '1/1/1971'
-- SET @triggerend = '5/31/1985'
-- SET @limittypeid = 4
-- Set @UpperLimit = 52785000


BEGIN
--RAM 3/9/2010 calculates the next lowest attachment point based on where the "Upper Limit" of a policy is currently in the policy

Declare @temp table (columnid int,columnstartdate datetime, columnenddate datetime, minattach money)
Insert into @temp
Select zc.columnid,
	columnstartdate, 
	columnenddate, 
	min(zc.attachmentpoint)
From (Select * from ztblPolicyAllocation where InsuranceProgramID in (@InsuranceProgramID) AND (OccurrenceRemaining-ColumnAllocation) > .1 AND allocationID = @allocationID ) za
	inner join ztblpolicycolumn zc on zc.policydateid=za.policydateid
	inner join (SELECT * FROM tblAllocationresultscolumn WHERE InsuranceProgramID in (@InsuranceProgramID) and allocationid = @allocationID ) arc on zc.columnid = arc.columnid
	inner join (select policyid from ztblpolicycolumn z INNER JOIN ztblPolicyAllocation a ON z.PolicyDateID = a.PolicyDateID where provisionvalueid=7)q on q.policyid=za.policyid where (OccurrenceRemaining-ColumnAllocation) > .1
	and za.allocationID = @allocationid 

group by zc.columnid, columnstartdate,columnenddate
order by columnstartdate


Return(
Select MIN(a.AttachmentPoint)
From ztblpolicycolumn c
	inner join @temp t on t.columnid=c.columnid
	inner join ztblpolicyallocation a on a.policydateid=c.policydateid AND t.minattach=a.attachmentpoint
	INNER JOIN tblPolicyLimits l ON a.PolicyID = l.PolicyID
Where (occurrenceremaining- CumAllocationIndemnity - CumAllocationDefense) > 0.01
			and allocationid= @allocationid 
			AND  InsuranceProgramID in (@InsuranceProgramID)
			AND ColumnStartDate < @TriggerEnd
			AND ColumnEndDate > @TriggerStart
			AND l.limittypeID = @limitTypeID
			and a.AttachmentPoint > @UpperLimit)
END
GO
