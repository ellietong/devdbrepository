SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[mtvMissingClaimantInfo]
(@ClaimantPersonalInfoID int)
RETURNS 
@MissingClaimantInformation TABLE
(
	ClaimantPersonalInfoID int primary key NOT NULL,
	Missing NVARCHAR(MAX)
)
AS
BEGIN

DECLARE @CurrentClaimantInfo TABLE  (ClaimantPersonalInfoID bigint,
									 SSN bit, 
									 BirthDate bit, 
									 DiagnosisDate bit) 

INSERT INTO @CurrentClaimantInfo
SELECT DISTINCT ClaimantPersonalInfoID,
				CASE WHEN SSN is NULL THEN 0 ELSE 1 END AS HasSSN, 
				CASE WHEN birthdate is NULL THEN 0 ELSE 1 END AS HasBirthDate,
				CASE WHEN CONVERT(date, DiagnosisDate) IS NULL THEN 0 ELSE 1 END AS HasDxDate
FROM vRequiredDocsByClaimant

DECLARE @MissingClaimantInfo  TABLE  (ID int NOT NULL IDENTITY(1,1), ClaimantPersonalInfoID bigint, MissingClaimantInformation nvarchar(500), Remainder int)
INSERT INTO @MissingClaimantInfo
SELECT ClaimantPersonalInfoID, 'Social Security Number', NULL
FROM @CurrentClaimantInfo
WHERE SSN = 0
UNION
SELECT ClaimantPersonalInfoID, 'Birth Date', NULL
FROM @CurrentClaimantInfo
WHERE BirthDate = 0
UNION
SELECT ClaimantPersonalInfoID, 'Diagnosis Date', NULL
FROM @CurrentClaimantInfo
WHERE DiagnosisDate = 0

--Calculate the remainder fOR every-other row highlighting
UPDATE @MissingClaimantInfo
SET Remainder = fdp.Remainder 
FROM (SELECT ID, (ID % 2) AS Remainder FROM @MissingClaimantInfo) fdp
INNER JOIN @MissingClaimantInfo ofdp ON fdp.ID = ofdp.ID
WHERE ofdp.ID = fdp.ID

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: verdana,arial,helvetica,sans-serif; font-size: 8pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: verdana,arial,helvetica,sans-serif; font-size: 8pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'

INSERT INTO @MissingClaimantInformation
SELECT DISTINCT cci.ClaimantPersonalInfoID,
		COALESCE((SELECT N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0; width:200px;">' + 	
		CAST((SELECT CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleB END AS 'td/@style', (SELECT UPPER(COALESCE(MissingClaimantInformation, ''))) as td, ''
		FROM @MissingClaimantInfo mci
		WHERE cci.ClaimantPersonalInfoID = mci.ClaimantPersonalInfoID 
		for xml path( 'tr' ), type) AS NVARCHAR(MAX) )) +  N'</table>' +  N'<br/>'	, 'All required claimant information submitted.') AS Missing
FROM @CurrentClaimantInfo cci
	
	RETURN 
END



GO
