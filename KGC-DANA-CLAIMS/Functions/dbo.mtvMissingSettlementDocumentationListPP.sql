SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[mtvMissingSettlementDocumentationListPP]
(
	@SetPymtID int
)
RETURNS 
@MissingDocuments TABLE
(
	SetPymtID int,
	MissingDocs NVARCHAR(MAX)
)
AS
BEGIN

DECLARE @CurrentDocumentation TABLE  
(
SetPymtID int, 
ClaimantIDed bit, 
DxMedicals bit, 
DODCert int, 
EstatePapers int, 
ProductID bit,
ExpoHist bit, 
SettleConfirmation bit, 
Release bit,
UnexecutedRelease bit,
ReleaseRequested bit,
--MMSEA bit,
--RepOfPlaintiff bit,
--RepOfPlaintiffCounsel bit,
--MedicareFormB bit
MMSEA bit,
FORMA1 bit,
FORMA2 bit,
FORMB bit,
FORMC1 bit,
ROP bit,
ROPC bit,
AFFIDAVIT bit
)
INSERT INTO @CurrentDocumentation
SELECT DISTINCT SetPymtID,
ClaimantIDed, 
DxMedicals, 
DODCert , 
EstatePapers , 
ProductID ,
EmployExpoHist , 
SettlementConfirmation , 
ReleaseReceived ,
UnexecutedReleaseReceived ,
ReleaseRequested ,
MMSEA ,
FORMA1 ,
FORMA2 ,
FORMB ,
FORMC1 ,
RepOfPlaintiff ,
RepOfPlaintiffCounsel,
Affidavit
FROM vDocumentationByClaimantID dbc

DECLARE @MissingDocumentation TABLE  (ID int, SetPymtID bigint, MissingDocumentation nvarchar(500), Remainder int)

INSERT INTO @MissingDocumentation
SELECT ROW_NUMBER() OVER (ORDER BY (SELECT NULL)), *, 
ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) % 2
FROM 
(
SELECT cd.SetPymtID,'Death Certificate' AS MissingDocs
FROM @CurrentDocumentation cd
WHERE cd.DODCert = 0
UNION
SELECT cd.SetPymtID, 'Estate Paperwork' AS MissingDocs
FROM @CurrentDocumentation cd
WHERE cd.EstatePapers = 0
UNION
SELECT cd.SetPymtID,'Executed Release' AS MissingDocs
FROM @CurrentDocumentation cd
WHERE Release = 0
UNION
SELECT cd.SetPymtID,'Unexecuted Release'
FROM @CurrentDocumentation cd
WHERE UnexecutedRelease = 0
UNION
SELECT cd.SetPymtID,'Release Request Form'
FROM @CurrentDocumentation cd
WHERE ReleaseRequested = 0
UNION
SELECT SetPymtID, 'Medicare Forms' AS MissingDocs
FROM @CurrentDocumentation
WHERE MMSEA = 0 OR ROP = 0 OR FORMB = 0 OR FORMA1 = 0 OR FORMA2 = 0 OR FORMC1 = 0 OR AFFIDAVIT = 0 
--UNION
--SELECT SetPymtID, 'MMSEA Settlement Form' AS MissingDocs
--FROM @CurrentDocumentation
--WHERE MMSEA = 0
--UNION
--SELECT SetPymtID, 'Medicare Representation of Plaintiff' AS MissingDocs
--FROM @CurrentDocumentation
--WHERE ROP = 0
--UNION
--SELECT SetPymtID, 'Medicare Representation of Plaintiff Counsel' AS MissingDocs
--FROM @CurrentDocumentation
--WHERE ROPC = 0
--UNION
--SELECT SetPymtID, 'Medicare Form B' AS MissingDocs
--FROM @CurrentDocumentation
--WHERE FORMB = 0
--UNION
--SELECT SetPymtID, 'Medicare Form A-1' AS MissingDocs
--FROM @CurrentDocumentation
--WHERE FORMA1 = 0
--UNION
--SELECT SetPymtID, 'Medicare Form A-2' AS MissingDocs
--FROM @CurrentDocumentation
--WHERE FORMA2 = 0
--UNION
--SELECT SetPymtID, 'Medicare Form C-1' AS MissingDocs
--FROM @CurrentDocumentation
--WHERE FORMC1 = 0
--UNION
--SELECT SetPymtID, 'Medicare Affidavit' AS MissingDocs
--FROM @CurrentDocumentation
--WHERE AFFIDAVIT = 0
) d

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: verdana,arial,helvetica,sans-serif; font-size: 8pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: verdana,arial,helvetica,sans-serif; font-size: 8pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'

INSERT INTO @MissingDocuments
SELECT DISTINCT sd2.SetPymtID,
		COALESCE((SELECT N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0; width:200px;">' + 	
  CAST ( (
SELECT	CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleB END AS 'td/@style', (SELECT UPPER(COALESCE(MissingDocumentation, ''))) as td, ''
	FROM @MissingDocumentation sd1
	WHERE sd1.SetPymtID = sd2.SetPymtID 
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) )) +  N'</table>' +  N'<br/>'	, 'All required documents submitted.') AS Missing
FROM @CurrentDocumentation sd2

--SELECT *
--FROM @MissingDocuments

	RETURN 

END





GO
