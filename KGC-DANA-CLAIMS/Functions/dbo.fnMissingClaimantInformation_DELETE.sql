SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO




CREATE FUNCTION [dbo].[fnMissingClaimantInformation_DELETE]
(@ClaimLawsuitID bigint, @ClaimantPersonalInfoID bigint)

RETURNS NVARCHAR(MAX)
AS
BEGIN

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 42360

--IF object_id('tempdb..@CurrentClaimantInfo') IS NOT NULL
--BEGIN
--   DROP TABLE @CurrentClaimantInfo
--END
DECLARE @CurrentClaimantInfo TABLE  (SSN bit, 
									 BirthDate bit, 
									 --Disease bit, 
									 DiagnosisDate bit,  
									 HasProductEntered bit, 
									 LegalCapacity bit)
INSERT INTO @CurrentClaimantInfo
SELECT distinct CASE WHEN socialsecuritynumber is NULL THEN 0 ELSE 1 END AS HasSSN, 
				CASE WHEN birthdate is NULL THEN 0 ELSE 1 END AS HasBirthDate,
				--CASE WHEN DiseaseID IS NULL THEN 0 ELSE 1 END AS HasDiseaseID,
				CASE WHEN CONVERT(date, DiseaseDiagnosisDate) IS NULL THEN 0 ELSE 1 END AS HasDxDate, 
				CASE WHEN COALESCE(p.ProductEntered, 0) = 0 THEN 0  ELSE 1 END AS HasProductEntered,  
				CASE WHEN DeceasedDate IS NULL THEN 1 WHEN DeceasedDate IS NOT NULL AND LegalCapacityID IS NULL THEN 0 END AS HasLegalCapacity
FROM tblClaim c
	INNER JOIN tblClaimantPersonalInfo cpi	 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID	
	INNER JOIN tblClaimLawsuit cl	 ON c.ClaimID = cl.ClaimID
	LEFT JOIN (
			SELECT DISTINCT ce.ClaimantPersonalInfoID, 1 AS ProductEntered
			FROM tblClaimantEmployment ce 
			INNER JOIN tblClaimantProduct cp on ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
			WHERE ce.IsPrimary = 1
				AND ClaimantPersonalInfoID = @ClaimantPersonalInfoID
				AND cp.ProductID IS NOT NULL 
				AND cp.StartDate IS NOT NULL 
				AND cp.EndDate IS NOT NULL 	
			 ) p	 ON c.claimantpersonalinfoid =p.ClaimantPersonalInfoID
	LEFT JOIN (SELECT cd1.* 
			   FROM tblClaimDisease cd1 
				INNER JOIN tbldisease d1 on d1.diseaseid = cd1.diseaseid 
			   WHERE PrimaryDisease = 1 
				AND highleveldiseaseid <> 5) cdi	 ON c.ClaimID = cdi.ClaimID
	LEFT JOIN (SELECT * 
			   FROM tblClaimantRelatedParty 
			   WHERE LegalCapacityID in (1,2,3,4,5,6,7,8,15,17,30,31))crp	 ON crp.ClaimantPersonalInfoID =cpi.ClaimantPersonalInfoID 
WHERE cl.ClaimLawsuitID = @ClaimLawsuitID


--SELECT * FROM @CurrentClaimantInfo

--IF object_id('tempdb..#MissingClaimantInfo') IS NOT NULL
--BEGIN
--   DROP TABLE #MissingClaimantInfo
--END
DECLARE @MissingClaimantInfo  TABLE  (ID int NOT NULL IDENTITY(1,1), MissingClaimantInformation nvarchar(500), Remainder int)
INSERT INTO @MissingClaimantInfo
SELECT 'Social Security Number', NULL
FROM @CurrentClaimantInfo
WHERE SSN = 0
UNION
SELECT 'Birth Date', NULL
FROM @CurrentClaimantInfo
WHERE BirthDate = 0
--UNION
--SELECT 'Disease Diagnosis', NULL
--FROM @CurrentClaimantInfo
--WHERE Disease = 0
UNION
SELECT 'Diagnosis Date', NULL
FROM @CurrentClaimantInfo
WHERE DiagnosisDate = 0
UNION
SELECT 'Product Information', NULL
FROM @CurrentClaimantInfo
WHERE HasProductEntered = 0
UNION
SELECT 'Legal Capacity', NULL
FROM @CurrentClaimantInfo
WHERE LegalCapacity = 0



	--Calculate the remainder for every-other row highlighting
	UPDATE @MissingClaimantInfo
	SET Remainder = fdp.Remainder 
	FROM (SELECT ID, (ID % 2) AS Remainder FROM @MissingClaimantInfo) fdp
	INNER JOIN @MissingClaimantInfo ofdp ON fdp.ID = ofdp.ID
	WHERE ofdp.ID = fdp.ID

--SELECT *
--FROM @MissingClaimantInfo

	DECLARE @Remainder TABLE (arID int, RowCounter int, Remainder int)
	INSERT INTO @Remainder
	SELECT ID,
		ROW_NUMBER() OVER (ORDER BY ID) AS Row_Counter,  ROW_NUMBER() OVER (ORDER BY ID)% 2 AS Remainder    
	FROM @MissingClaimantInfo

DECLARE @tdStyleW nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #FFFFFF;'
DECLARE @tdStyleB nvarchar(MAX) = 'font-family: Calibri; font-size: 11pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; vertical-align: left; background-color: #D6E8FF;'


DECLARE @MissingClaimantInfoTable nvarchar(max) = ''

SET @MissingClaimantInfoTable = @MissingClaimantInfoTable + N'<table style="border-collapse: collapse; border: 1px solid @38160C; color: @F6ECF0;">' + 
	

N'<tr>
' +
	
	
CAST ( (
SELECT CASE WHEN Remainder = 1 THEN @TDStyleW ELSE @tdStyleB END AS 'td/@style', (SELECT COALESCE(MissingClaimantInformation, '')) as td, ''
	FROM @MissingClaimantInfo
	for xml path( 'tr' ), type) AS NVARCHAR(MAX) ) + 
	
	N'</table>' +  N'<br/>'	
	--SELECT @MissingClaimantInfoTable
	RETURN @MissingClaimantInfoTable --AS MissingClaimantInfo



END




GO
