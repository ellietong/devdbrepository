SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vDocumentationByClaimantID]
AS
SELECT DISTINCT 
                         cpi.ClaimantPersonalInfoID, spd.SetPymtID AS spdSetPymtID, c.ClaimID, cpi.FirstName, cpi.LastName, cpi.ClaimantPersonalInfoID AS ClaimantID, 
                         cpi.LastName + ', ' + CASE WHEN cpi.FirstName IS NULL THEN '' ELSE cpi.FirstName + ' ' END + CASE WHEN cpi.MiddleName IS NULL 
                         THEN '' ELSE cpi.MiddleName + ' ' END AS ClaimantName, CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) 
                         END AS SSN, CONVERT(datetime, cpi.BirthDate) AS Birthdate, CONVERT(datetime, cpi.DeceasedDate) AS DeceasedDate, 
                         cdi.DiseaseDiagnosisDate AS DiagnosisDate, cdi.Disease, cs.SettlementDate, cs.SettlementAmount, sg.SettlementGroup, cs.SettlementGroupID, cl.ClaimLawsuitID, 
                         spd.ReleaseRqstDate, spd.ReleaseToPC, spd.ExecutedReleaseDate, spd.SetPymtID, CASE WHEN (doc.ClaimantPersonalInfoID IS NOT NULL OR
                         ExecutedReleaseDate IS NOT NULL) THEN 1 ELSE 0 END AS ReleaseReceived, CASE WHEN (unex.ClaimantPersonalInfoID IS NOT NULL OR
                         ReleaseToPC IS NOT NULL) THEN 1 ELSE 0 END AS UnexecutedReleaseReceived, CASE WHEN (rreq.ClaimantPersonalInfoID IS NOT NULL OR
                         spd.ReleaseRqstDate IS NOT NULL) THEN 1 ELSE 0 END AS ReleaseRequested, COALESCE (docs.EstatePapers, 2) AS EstatePapers, COALESCE (docs.DODCert, 2) 
                         AS DODCert, COALESCE (sd.ClaimantIDed, 0) AS ClaimantIDed, COALESCE (sd.DxMedicals, 0) AS DxMedicals, COALESCE (sd.ProductID, 0) AS ProductID, 
                         COALESCE (sd.EmployExpoHist, 0) AS EmployExpoHist, COALESCE (sd.SettlementConfirmation, 0) AS SettlementConfirmation, 
						 MedicareBeneficiaryStatusID,    
						 CASE WHEN COALESCE(MedicareBeneficiaryStatusID, 0) <> 2 AND mrpcs.AFFIDAVIT = 1 THEN COALESCE(sd.MedicareAffidavit, 0) ELSE 2 END AS Affidavit,						                      
						 CASE WHEN mrpcs.FORMC1 = 1 THEN COALESCE (sd.MedicareFormC1, 0) 
                         ELSE 2 END AS FORMC1, 
						 CASE WHEN socialsecuritynumber IS NULL 
                         THEN 0 ELSE 1 END AS HasSSN, CASE WHEN birthdate IS NULL THEN 0 ELSE 1 END AS HasBirthDate, CASE WHEN CONVERT(date, DiseaseDiagnosisDate) 
                         IS NULL THEN 0 ELSE 1 END AS HasDxDate, CASE WHEN COALESCE (p.ProductEntered, 0) = 0 THEN 0 ELSE 1 END AS HasProductEntered, 
                         CASE WHEN DeceasedDate IS NULL THEN 1 WHEN DeceasedDate IS NOT NULL AND LegalCapacityID IS NULL THEN 0 ELSE 2 END AS HasLegalCapacity,					 
						 CASE WHEN mrpcs.MMSEA = 1 AND mrpcs.FORMB = 1 AND mrpcs.FORMA1 = 1 AND mrpcs.FORMA2 = 1 AND mrpcs.ROP = 1 and mrpcs.ROPC = 1 THEN CASE WHEN (CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiff,0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiffCounsel,0) AS INT) = 3 OR CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicareFormA1,0) AS INT)+CAST(COALESCE(sd.MedicareFormA2,0) AS INT) = 3) THEN 1 ELSE COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) END ELSE CASE WHEN mrpcs.MMSEA = 1 THEN COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) ELSE 2 END END AS MMSEA,						 
						 CASE WHEN mrpcs.MMSEA = 1 AND mrpcs.FORMB = 1 AND mrpcs.FORMA1 = 1 AND mrpcs.FORMA2 = 1 AND mrpcs.ROP = 1 and mrpcs.ROPC = 1 THEN CASE WHEN (CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiff,0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiffCounsel,0) AS INT) = 3 OR CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicareFormA1,0) AS INT)+CAST(COALESCE(sd.MedicareFormA2,0) AS INT) = 3)  THEN 2 ELSE COALESCE (sd.MedicareFormA1, 0) END ELSE CASE WHEN mrpcs.FORMA1 = 1 THEN COALESCE (sd.MedicareFormA1, 0) ELSE 2 END END AS FORMA1,						 
						 CASE WHEN mrpcs.MMSEA = 1 AND mrpcs.FORMB = 1 AND mrpcs.FORMA1 = 1 AND mrpcs.FORMA2 = 1 AND mrpcs.ROP = 1 and mrpcs.ROPC = 1 THEN CASE WHEN (CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiff,0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiffCounsel,0) AS INT) = 3 OR CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicareFormA1,0) AS INT)+CAST(COALESCE(sd.MedicareFormA2,0) AS INT) = 3)  THEN 2 ELSE COALESCE (sd.MedicareFormA2, 0) END ELSE CASE WHEN mrpcs.FORMA2 = 1 THEN COALESCE (sd.MedicareFormA2, 0) ELSE 2 END END AS FORMA2,						 
						 CASE WHEN mrpcs.MMSEA = 1 AND mrpcs.FORMB = 1 AND mrpcs.FORMA1 = 1 AND mrpcs.FORMA2 = 1 AND mrpcs.ROP = 1 and mrpcs.ROPC = 1 THEN CASE WHEN (CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiff,0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiffCounsel,0) AS INT) = 3 OR CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicareFormA1,0) AS INT)+CAST(COALESCE(sd.MedicareFormA2,0) AS INT) = 3)  THEN 2 ELSE COALESCE(NULLIF(sd.MedicareFormB, 0), sd.MMSEA, 0) END ELSE CASE WHEN COALESCE(AFFIDAVIT, 0) = 0 THEN CASE WHEN mrpcs.FORMB = 1 THEN COALESCE(NULLIF(sd.MedicareFormB, 0), sd.MMSEA, 0) ELSE 2 END WHEN COALESCE(AFFIDAVIT, 0) = 1 AND MedicareBeneficiaryStatusID = 2 THEN COALESCE(NULLIF(MedicareFormB, 0), sd.MMSEA, 0) ELSE 2 END END AS FORMB,						 
						 CASE WHEN mrpcs.MMSEA = 1 AND mrpcs.FORMB = 1 AND mrpcs.FORMA1 = 1 AND mrpcs.FORMA2 = 1 AND mrpcs.ROP = 1 and mrpcs.ROPC = 1 THEN CASE WHEN (CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiff,0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiffCounsel,0) AS INT) = 3 OR CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicareFormA1,0) AS INT)+CAST(COALESCE(sd.MedicareFormA2,0) AS INT) = 3)  THEN 2 ELSE COALESCE (sd.MedicarePlaintiff, 0) END ELSE CASE WHEN mrpcs.ROP = 1 THEN COALESCE (sd.MedicarePlaintiff, 0) ELSE 2 END END AS RepOfPlaintiff,						 
						 CASE WHEN mrpcs.MMSEA = 1 AND mrpcs.FORMB = 1 AND mrpcs.FORMA1 = 1 AND mrpcs.FORMA2 = 1 AND mrpcs.ROP = 1 and mrpcs.ROPC = 1 THEN CASE WHEN (CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiff,0) AS INT)+CAST(COALESCE(sd.MedicarePlaintiffCounsel,0) AS INT) = 3 OR CAST(COALESCE(NULLIF(sd.MMSEA,0), MedicareFormB, 0) AS INT)+CAST(COALESCE(sd.MedicareFormA1,0) AS INT)+CAST(COALESCE(sd.MedicareFormA2,0) AS INT) = 3)  THEN 2 ELSE COALESCE (sd.MedicarePlaintiffCounsel, 0) END ELSE CASE WHEN mrpcs.ROPC = 1 THEN COALESCE (sd.MedicarePlaintiffCounsel, 0) ELSE 2 END END AS RepOfPlaintiffCounsel												 
FROM            dbo.tblClaim AS c INNER JOIN
                         dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                         dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                         dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                         dbo.tblClaimantSettlement AS cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID INNER JOIN
                         dbo.tblClaimLawsuitStatusTracking AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblSettlementGroup AS sg ON cs.SettlementGroupID = sg.SettlementGroupID LEFT OUTER JOIN
                             (SELECT        cd1.ClaimDiseaseID, cd1.ClaimID, cd1.DiseaseID, cd1.PrimaryDisease, cd1.DiseaseDiagnosisDate, cd1.DoctorID, cd1.NavDisease, cd1.SPUsername,
                                                          cd1.DiseaseID_OLD, d1.Disease
                               FROM            dbo.tblClaimDisease AS cd1 INNER JOIN
                                                         dbo.tblDisease AS d1 ON d1.DiseaseID = cd1.DiseaseID
                               WHERE        (cd1.PrimaryDisease = 1) AND (d1.HighLevelDiseaseID <> 5)) AS cdi ON cdi.ClaimID = c.ClaimID LEFT OUTER JOIN
                         dbo.tblSettlePaymentDocumentation AS spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblSettlementDocumentation AS sd ON spd.SetPymtID = sd.SetPymtID LEFT OUTER JOIN
                         dbo.tblPaymentSettlement AS ps ON cs.ClaimantSettlementID = ps.ClaimantSettlementID LEFT OUTER JOIN
                             (SELECT        mcd.ClaimantPersonalInfoID
                               FROM            dbo.tblMultiClaimantDocument AS mcd INNER JOIN
                                                         dbo.tblDocuments AS d ON d.DocumentID = mcd.DocumentID
                               WHERE        (d.DetailDocumentTypeID = 31)
                               GROUP BY mcd.ClaimantPersonalInfoID) AS doc ON doc.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                             (SELECT        mcd.ClaimantPersonalInfoID
                               FROM            dbo.tblMultiClaimantDocument AS mcd INNER JOIN
                                                         dbo.tblDocuments AS d ON d.DocumentID = mcd.DocumentID
                               WHERE        (d.DetailDocumentTypeID = 55)
                               GROUP BY mcd.ClaimantPersonalInfoID) AS unex ON unex.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                             (SELECT        mcd.ClaimantPersonalInfoID
                               FROM            dbo.tblMultiClaimantDocument AS mcd INNER JOIN
                                                         dbo.tblDocuments AS d ON d.DocumentID = mcd.DocumentID
                               WHERE        (d.DetailDocumentTypeID = 32)
                               GROUP BY mcd.ClaimantPersonalInfoID) AS rreq ON rreq.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID LEFT OUTER JOIN
                             (SELECT        spd.SetPymtID, COALESCE (sd.EstatePapers, 0) AS EstatePapers, COALESCE (sd.DODCert, 0) AS DODCert
                               FROM            dbo.tblClaimantPersonalInfo AS cpi INNER JOIN
                                                         dbo.tblClaim AS c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID INNER JOIN
                                                         dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                                                         dbo.tblSettlePaymentDocumentation AS spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID INNER JOIN
                                                         dbo.tblSettlementDocumentation AS sd ON spd.SetPymtID = sd.SetPymtID
                               WHERE        (cpi.DeceasedDate IS NOT NULL)) AS docs ON spd.SetPymtID = docs.SetPymtID LEFT OUTER JOIN
                         dbo.tblPlaintiffCounselAddress AS pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID LEFT OUTER JOIN
                         dbo.vMedicareRequirementsByPlaintiffCounselByStatePivot AS mrpcs ON pca.PlaintiffCounselID = mrpcs.PlaintiffCounselID AND 
                         l.StateID = mrpcs.StateID LEFT OUTER JOIN
                             (SELECT DISTINCT ce.ClaimantPersonalInfoID, 1 AS ProductEntered
                               FROM            dbo.tblClaimantEmployment AS ce INNER JOIN
                                                         dbo.tblClaimantProduct AS cp ON ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
                               WHERE        (ce.IsPrimary = 1) AND (cp.ProductID IS NOT NULL) AND (cp.StartDate IS NOT NULL) AND (cp.EndDate IS NOT NULL)) AS p ON 
                         c.ClaimantPersonalInfoID = p.ClaimantPersonalInfoID LEFT OUTER JOIN
                             (SELECT        ClaimantRelatedPartyID, ClaimantPersonalInfoID, FirstName, MiddleName, LastName, RelationshipID, LegalCapacityID, RPAddress1, RPAddress2, 
                                                         RPCity, StateID, RPZip, RPMailZip, RPPhone, RPExtension, RPSocialSecurityNumber, RPTIN, NavInjPartyKey, NavRelationship, 
                                                         NavLegalCapacity
                               FROM            dbo.tblClaimantRelatedParty
                               WHERE        (LegalCapacityID IN (1, 2, 3, 4, 5, 6, 7, 8, 15, 17, 30, 31))) AS crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE        (clst.IsPrimaryStatus = 1) AND (clst.ClaimLawsuitStatusID IN (23)) AND (YEAR(cs.SettlementDate) >= 2011) AND (ps.ClaimantSettlementID IS NULL) AND 
                         (cs.SettlementGroupID IS NOT NULL)



GO
GRANT VIEW DEFINITION ON  [dbo].[vDocumentationByClaimantID] TO [base_user]
GRANT SELECT ON  [dbo].[vDocumentationByClaimantID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDocumentationByClaimantID] TO [power_user]
GRANT SELECT ON  [dbo].[vDocumentationByClaimantID] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[51] 4[10] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 26
               Left = 651
               Bottom = 155
               Right = 883
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 14
               Left = 342
               Bottom = 143
               Right = 592
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 174
               Left = 389
               Bottom = 303
               Right = 611
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 446
               Left = 772
               Bottom = 575
               Right = 1042
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 316
               Left = 707
               Bottom = 445
               Right = 951
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clst"
            Begin Extent = 
               Top = 595
               Left = 456
               Bottom = 724
               Right = 705
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sg"
            Begin Extent = 
               Top = 19
               Left = 1250
               Bottom = 131
               Right = 1440
            End
            DisplayFlags = 280
            TopColumn = 0
', 'SCHEMA', N'dbo', 'VIEW', N'vDocumentationByClaimantID', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'
         End
         Begin Table = "cdi"
            Begin Extent = 
               Top = 366
               Left = 906
               Bottom = 495
               Right = 1109
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "spd"
            Begin Extent = 
               Top = 160
               Left = 879
               Bottom = 289
               Right = 1111
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sd"
            Begin Extent = 
               Top = 134
               Left = 1100
               Bottom = 263
               Right = 1322
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ps"
            Begin Extent = 
               Top = 476
               Left = 1055
               Bottom = 605
               Right = 1279
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "doc"
            Begin Extent = 
               Top = 277
               Left = 0
               Bottom = 355
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "unex"
            Begin Extent = 
               Top = 584
               Left = 216
               Bottom = 662
               Right = 430
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rreq"
            Begin Extent = 
               Top = 17
               Left = 0
               Bottom = 95
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "docs"
            Begin Extent = 
               Top = 138
               Left = 1324
               Bottom = 250
               Right = 1494
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pca"
            Begin Extent = 
               Top = 270
               Left = 1091
               Bottom = 399
               Right = 1320
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mrpcs"
            Begin Extent = 
               Top = 446
               Left = 1328
               Bottom = 575
               Right = 1512
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 603
               Left = 852
               Bottom = 698
               Right = 1066
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crp"
            Begin Extent = 
               Top = 420
               Left = 65
               Bottom = 549
               Right = 285
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDocumentationByClaimantID', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDocumentationByClaimantID', NULL, NULL
GO
