SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblSuffix]
AS
SELECT     SuffixID, Suffix
FROM         [KGA-ClaimsAdmin].dbo.tblSuffix

GO
GRANT VIEW DEFINITION ON  [dbo].[tblSuffix] TO [base_user]
GRANT SELECT ON  [dbo].[tblSuffix] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSuffix] TO [power_user]
GRANT SELECT ON  [dbo].[tblSuffix] TO [power_user]
GRANT INSERT ON  [dbo].[tblSuffix] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSuffix] TO [power_user]
GO
