SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblDefendantCompany]
AS
SELECT     DefendantCompanyID, DefendantCompany, ClientID, IsActive
FROM         [KGA-ClaimsAdmin].dbo.tblDefendantCompany AS tblDefendantCompany_1

GO
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompany] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefendantCompany] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefendantCompany] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblDefendantCompany] TO [power_user]
GRANT SELECT ON  [dbo].[tblDefendantCompany] TO [power_user]
GO
