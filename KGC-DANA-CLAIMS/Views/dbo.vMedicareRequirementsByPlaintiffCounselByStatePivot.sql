SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vMedicareRequirementsByPlaintiffCounselByStatePivot]
AS
WITH ReqDocs (Req, PlaintiffCounselID, StateID)
AS
(
SELECT DISTINCT 'MMSEA' AS Req, PlaintiffCounselID, StateID 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE --(PlaintiffCounselID IN (271, 519, 643, 952)
	--OR l.StateID NOT IN (45, 56))
	--AND 
	PlaintiffCounselID NOT IN (26, 150, 257, 294, 331, 586, 715)
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'FORMA1' AS Req, PlaintiffCounselID, StateID  
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE (PlaintiffCounselID IN (331, 586)
	OR (l.StateID IN (45, 56)
	AND PlaintiffCounselID NOT IN (26, 150, 257, 271, 294, 519, 643, 715, 952)))
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'FORMA2' AS Req, PlaintiffCounselID, StateID  
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE (PlaintiffCounselID IN (331, 586)
	OR (l.StateID IN (45, 56)
	AND PlaintiffCounselID NOT IN (26, 150, 257, 271, 294, 519, 643, 715, 952)))
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'FORMB' AS Req, PlaintiffCounselID, StateID  
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE (PlaintiffCounselID IN (26, 257, 294, 331, 586)
	OR (l.StateID IN (45, 56)
		AND PlaintiffCounselID NOT IN (150, 271, 519, 643, 715, 952)))
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'FORMC1' AS Req, PlaintiffCounselID, StateID  
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE PlaintiffCounselID IN (331, 586) 
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'ROP' AS Req, PlaintiffCounselID, StateID  
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE 
--(PlaintiffCounselID IN (150, 271, 519, 643, 952)
	--OR l.StateID NOT IN (45, 56))
	--AND 
	PlaintiffCounselID NOT IN (26, 257, 294, 331, 586, 715)
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'ROPC' AS Req, PlaintiffCounselID, StateID  
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE 
--(PlaintiffCounselID IN (150)
	--OR l.StateID NOT IN (45, 56))
	--AND
	 PlaintiffCounselID NOT IN (26, 257, 271, 294, 331, 519, 586, 643, 715, 952)
	AND PlaintiffCounselID IS NOT NULL
UNION
SELECT DISTINCT 'AFFIDAVIT' AS Req, PlaintiffCounselID, StateID 
FROM tblLawsuit l
	INNER JOIN tblClaimLawsuit cl ON l.LawsuitID = cl.LawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
WHERE PlaintiffCounselID IN (26)
)

select PlaintiffCounselID, StateID, 
[MMSEA],
[FORMA1],
[FORMA2],
[FORMB],
[FORMC1],
[ROP],
[ROPC],
[AFFIDAVIT]
from 
(
select plaintiffCounselID, Stateid, req
from reqdocs
) as missdocs
pivot
(
count(req)
for req in ([MMSEA], [FORMA1], [FORMA2], [FORMB], [FORMC1], [ROP], [ROPC], [AFFIDAVIT])
) as pivottable






GO
