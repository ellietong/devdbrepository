SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblOccupation]
AS
SELECT     OccupationID, Occupation
FROM         [KGA-ClaimsAdmin].dbo.tblOccupation AS tblOccupation_1

GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccupation] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccupation] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccupation] TO [power_user]
GO
