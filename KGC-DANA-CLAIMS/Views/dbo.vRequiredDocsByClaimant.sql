SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vRequiredDocsByClaimant]
AS
SELECT DISTINCT 
                         cpi.ClaimantPersonalInfoID, spd.SetPymtID AS spdSetPymtID, cpi.ClaimantPersonalInfoID AS ClaimantID, cpi.LastName + ', ' + CASE WHEN FirstName IS NULL 
                         THEN '' ELSE FirstName + ' ' END + CASE WHEN MiddleName IS NULL THEN '' ELSE MiddleName + ' ' END AS ClaimantName, 
                         CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SSN, CONVERT(datetime, cpi.BirthDate) AS Birthdate,
                          CONVERT(datetime, cpi.DeceasedDate) AS DeceasedDate, cdi.DiseaseDiagnosisDate AS DiagnosisDate, cdi.Disease, cs.SettlementDate, cs.SettlementAmount, 
                         sg.SettlementGroup, cs.SettlementGroupID, spd.ReleaseRqstDate, spd.ReleaseToPC, spd.ExecutedReleaseDate, spd.SetPymtID, p.ProductEntered, 
                         crp.LegalCapacityID, pca.PlaintiffCounselID, pca.PlaintiffCounsel, l.StateID
FROM            dbo.tblClaim AS c INNER JOIN
                         dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                         dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                         dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                         dbo.tblClaimantSettlement AS cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID INNER JOIN
                         dbo.tblClaimLawsuitStatusTracking AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblSettlementGroup AS sg ON cs.SettlementGroupID = sg.SettlementGroupID LEFT OUTER JOIN
                             (SELECT        cd1.ClaimDiseaseID, cd1.ClaimID, cd1.DiseaseID, cd1.PrimaryDisease, cd1.DiseaseDiagnosisDate, cd1.DoctorID, cd1.NavDisease, cd1.SPUsername,
                                                          cd1.DiseaseID_OLD, d1.Disease
                               FROM            dbo.tblClaimDisease AS cd1 INNER JOIN
                                                         dbo.tblDisease AS d1 ON d1.DiseaseID = cd1.DiseaseID
                               WHERE        (cd1.PrimaryDisease = 1) AND (d1.HighLevelDiseaseID <> 5)) AS cdi ON cdi.ClaimID = c.ClaimID LEFT OUTER JOIN
                         dbo.tblSettlePaymentDocumentation AS spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblSettlementDocumentation AS sd ON spd.SetPymtID = sd.SetPymtID LEFT OUTER JOIN
                         dbo.tblPaymentSettlement AS ps ON cs.ClaimantSettlementID = ps.ClaimantSettlementID LEFT OUTER JOIN
                         dbo.tblPlaintiffCounselAddress AS pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID LEFT OUTER JOIN
                             (SELECT DISTINCT ce.ClaimantPersonalInfoID, 1 AS ProductEntered
                               FROM            dbo.tblClaimantEmployment AS ce INNER JOIN
                                                         dbo.tblClaimantProduct AS cp ON ce.ClaimantEmploymentID = cp.ClaimantEmploymentID
                               WHERE        (ce.IsPrimary = 1) AND (cp.ProductID IS NOT NULL) AND (cp.StartDate IS NOT NULL) AND (cp.EndDate IS NOT NULL)) AS p ON 
                         c.ClaimantPersonalInfoID = p.ClaimantPersonalInfoID LEFT OUTER JOIN
                             (SELECT        MAX(LegalCapacityID) AS LegalCapacityID, ClaimantPersonalInfoID
                               FROM            dbo.tblClaimantRelatedParty
                               WHERE        (LegalCapacityID IN (1, 2, 3, 4, 5, 6, 7, 8, 15, 17, 30, 31))
                               GROUP BY ClaimantPersonalInfoID) AS crp ON crp.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
WHERE        (clst.IsPrimaryStatus = 1) AND (clst.ClaimLawsuitStatusID IN (23)) AND (YEAR(cs.SettlementDate) >= 2011) AND (ps.ClaimantSettlementID IS NULL) AND 
                         (cs.SettlementGroupID IS NOT NULL)


GO
GRANT VIEW DEFINITION ON  [dbo].[vRequiredDocsByClaimant] TO [base_user]
GRANT SELECT ON  [dbo].[vRequiredDocsByClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vRequiredDocsByClaimant] TO [power_user]
GRANT SELECT ON  [dbo].[vRequiredDocsByClaimant] TO [power_user]
GRANT INSERT ON  [dbo].[vRequiredDocsByClaimant] TO [power_user]
GRANT DELETE ON  [dbo].[vRequiredDocsByClaimant] TO [power_user]
GRANT UPDATE ON  [dbo].[vRequiredDocsByClaimant] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[12] 2[29] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -288
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 297
               Bottom = 114
               Right = 508
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 546
               Bottom = 114
               Right = 742
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 294
               Left = 38
               Bottom = 402
               Right = 278
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 267
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "clst"
            Begin Extent = 
               Top = 114
               Left = 547
               Bottom = 222
               Right = 770
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sg"
            Begin Extent = 
               Top = 114
               Left = 305
               Bottom = 222
               Right = 509
            End
            DisplayFlags = 280
            TopColumn = 0
     ', 'SCHEMA', N'dbo', 'VIEW', N'vRequiredDocsByClaimant', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'    End
         Begin Table = "cdi"
            Begin Extent = 
               Top = 294
               Left = 316
               Bottom = 402
               Right = 501
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "spd"
            Begin Extent = 
               Top = 6
               Left = 780
               Bottom = 114
               Right = 985
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sd"
            Begin Extent = 
               Top = 6
               Left = 1023
               Bottom = 114
               Right = 1216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ps"
            Begin Extent = 
               Top = 294
               Left = 539
               Bottom = 402
               Right = 741
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "doc"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 465
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "unex"
            Begin Extent = 
               Top = 294
               Left = 968
               Bottom = 357
               Right = 1161
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rreq"
            Begin Extent = 
               Top = 294
               Left = 1199
               Bottom = 357
               Right = 1392
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "docs"
            Begin Extent = 
               Top = 294
               Left = 779
               Bottom = 387
               Right = 930
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pca"
            Begin Extent = 
               Top = 402
               Left = 269
               Bottom = 510
               Right = 473
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 31
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vRequiredDocsByClaimant', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vRequiredDocsByClaimant', NULL, NULL
GO
