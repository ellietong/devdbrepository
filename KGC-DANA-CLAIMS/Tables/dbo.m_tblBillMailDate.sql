CREATE TABLE [dbo].[m_tblBillMailDate]
(
[BillMailingID] [bigint] NOT NULL IDENTITY(1, 1),
[BillMatterID] [int] NOT NULL,
[BillTypeID] [int] NOT NULL,
[BillDate] [date] NULL,
[MailingDate] [date] NULL,
[NavBillNumber] [bigint] NULL,
[NavMonth] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavYear] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDescription] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavComment] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblBillMailDate] ADD CONSTRAINT [PK_m_tblBillMailDate] PRIMARY KEY CLUSTERED  ([BillMailingID]) ON [PRIMARY]
GO
