CREATE TABLE [dbo].[tblSavedClaimantIds]
(
[SavedClaimantID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantID] [bigint] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedClaimantIds] ADD CONSTRAINT [PK_tblSavedClaimantIds] PRIMARY KEY CLUSTERED  ([SavedClaimantID]) ON [PRIMARY]
GO
