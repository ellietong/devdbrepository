CREATE TABLE [dbo].[tblProvisionValue_Archive]
(
[ProvisionValueID] [int] NOT NULL IDENTITY(1, 1),
[ProvisionLabelID] [int] NOT NULL,
[ProvisionValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionValue_Archive] ADD CONSTRAINT [PK__tblProvisionValu__24927208] PRIMARY KEY CLUSTERED  ([ProvisionValueID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblProvisionValue_Archive] ADD CONSTRAINT [FK_tblProvisionValue_tblProvisionLabel] FOREIGN KEY ([ProvisionLabelID]) REFERENCES [dbo].[tblProvisionLabel_Archive] ([ProvisionLabelID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblProvisionValue_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblProvisionValue_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblProvisionValue_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblProvisionValue_Archive] TO [power_user]
GRANT SELECT ON  [dbo].[tblProvisionValue_Archive] TO [Provision_User]
GRANT INSERT ON  [dbo].[tblProvisionValue_Archive] TO [Provision_User]
GRANT DELETE ON  [dbo].[tblProvisionValue_Archive] TO [Provision_User]
GRANT UPDATE ON  [dbo].[tblProvisionValue_Archive] TO [Provision_User]
GO
