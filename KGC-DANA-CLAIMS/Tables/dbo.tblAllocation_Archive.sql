CREATE TABLE [dbo].[tblAllocation_Archive]
(
[AllocationID] [int] NOT NULL IDENTITY(1, 1),
[AllocationDescription] [nvarchar] (260) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaveResults] [int] NULL,
[LimitTypeID] [int] NULL,
[AllocationSchemeID] [int] NULL,
[TriggerStartID] [int] NULL,
[TriggerStartDate] [datetime] NULL,
[TriggerEndID] [int] NULL,
[TriggerEndDate] [datetime] NULL,
[OccurrenceDefId] [int] NULL,
[UnCoveredYearstoPH] [bit] NULL CONSTRAINT [DF_tblAllocation_UnCoveredYearstoPH] DEFAULT ((0)),
[VerticallyUnCollapse] [bit] NULL CONSTRAINT [DF_tblAllocation_VerticallyUnCollapse] DEFAULT ((0)),
[Cost1] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost1] DEFAULT ((0)),
[Cost2] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost2] DEFAULT ((0)),
[Cost3] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost3] DEFAULT ((0)),
[Cost4] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost4] DEFAULT ((0)),
[Cost5] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost5] DEFAULT ((0)),
[Cost6] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost6] DEFAULT ((0)),
[Cost7] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost7] DEFAULT ((0)),
[Cost8] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost8] DEFAULT ((0)),
[Cost9] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost9] DEFAULT ((0)),
[Cost10] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost10] DEFAULT ((0)),
[Cost11] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost11] DEFAULT ((0)),
[Cost12] [bit] NULL CONSTRAINT [DF_tblAllocation_Cost12] DEFAULT ((0)),
[SIRID] [smallint] NULL,
[SIRToPay] [money] NULL,
[ConsiderPreviousExhaustion] [bit] NULL CONSTRAINT [DF_tblAllocation_ConsiderPreviousExhaustion] DEFAULT ((0)),
[ConsiderExhaustionCarterWallace] [bit] NULL CONSTRAINT [DF_tblAllocation_ConsiderExhaustionCarterWallace] DEFAULT ((0)),
[ExhaustionTypeID] [int] NULL,
[ExhaustionDateID] [int] NULL,
[LessThanAttachmentPoint] [money] NULL,
[LessThanLayer] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccurrencesInClaim] [bit] NULL CONSTRAINT [DF_tblAllocation_OccurrencesInClaim] DEFAULT ((0)),
[Cost1Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost1Discount] DEFAULT ((1)),
[Cost2Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost2Discount] DEFAULT ((1)),
[Cost3Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost3Discount] DEFAULT ((1)),
[Cost4Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost4Discount] DEFAULT ((1)),
[Cost5Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost5Discount] DEFAULT ((1)),
[Cost6Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost6Discount] DEFAULT ((1)),
[Cost7Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost7Discount] DEFAULT ((1)),
[Cost8Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost8Discount] DEFAULT ((1)),
[Cost9Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost9Discount] DEFAULT ((1)),
[Cost10Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost10Discount] DEFAULT ((1)),
[Cost11Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost11Discount] DEFAULT ((1)),
[Cost12Discount] [real] NOT NULL CONSTRAINT [DF_tblAllocation_Cost12Discount] DEFAULT ((1)),
[AllocationNotes] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConsiderPreviousAllocation] [bit] NULL CONSTRAINT [DF_tblAllocation_ConsiderPreviousAllocation] DEFAULT ((0)),
[PreviousAllocation] [int] NULL,
[UnevenQSID] [int] NULL,
[ProratedSIRAgg] [bit] NULL CONSTRAINT [DF_tblAllocation_ProratedSIRAgg] DEFAULT ((0)),
[DefenseScenarioID] [int] NULL,
[SignatoryRun] [bit] NULL,
[NonSigAllocationID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocation_Archive] ADD CONSTRAINT [PK_tblAllocation] PRIMARY KEY CLUSTERED  ([AllocationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocation_Archive] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocation_Archive] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocation_Archive] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocation_Archive] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocation_Archive] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocation_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocation_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocation_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocation_Archive] TO [power_user]
GO
