CREATE TABLE [dbo].[m_tblExpenseModelJuly2011]
(
[Start] [datetime] NULL,
[End] [datetime] NULL,
[Percentage] [float] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblExpenseModelJuly2011] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblExpenseModelJuly2011] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblExpenseModelJuly2011] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblExpenseModelJuly2011] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblExpenseModelJuly2011] TO [power_user]
GO
