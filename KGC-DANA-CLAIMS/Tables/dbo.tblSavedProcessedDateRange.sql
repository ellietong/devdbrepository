CREATE TABLE [dbo].[tblSavedProcessedDateRange]
(
[SavedProcessedDateID] [int] NOT NULL IDENTITY(1, 1),
[ProcessedDateRange] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedProcessedDateRange] ADD CONSTRAINT [PK_tblSavedProcessedDateRange] PRIMARY KEY CLUSTERED  ([SavedProcessedDateID]) ON [PRIMARY]
GO
