CREATE TABLE [dbo].[tblClaim_TEMP]
(
[ClaimID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavIsCurrentClaim] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimSequence] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPlaintiffDisease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPlaintiffDiseaseDate] [datetime] NULL,
[NavDefenseDisease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDefenseDiseaseDate] [datetime] NULL,
[ClaimSequenceID] [int] NULL,
[IsCurrentClaim] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim_TEMP] ADD CONSTRAINT [PK_tblClaim_TEMP] PRIMARY KEY CLUSTERED  ([ClaimID_ALT]) ON [PRIMARY]
GO
