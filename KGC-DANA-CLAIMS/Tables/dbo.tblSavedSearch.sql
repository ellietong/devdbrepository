CREATE TABLE [dbo].[tblSavedSearch]
(
[SavedSearchID] [bigint] NOT NULL IDENTITY(1, 1),
[SavedSearchTitle] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SPUserName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedSearch] ADD CONSTRAINT [PK_tblSavedSearch] PRIMARY KEY CLUSTERED  ([SavedSearchID]) ON [PRIMARY]
GO
