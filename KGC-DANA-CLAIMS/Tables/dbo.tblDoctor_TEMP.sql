CREATE TABLE [dbo].[tblDoctor_TEMP]
(
[DoctorID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoctorLastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoctorFirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoctorMiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDoctor_TEMP] ADD CONSTRAINT [PK_tblDoctor_TEMP] PRIMARY KEY CLUSTERED  ([DoctorID_ALT]) ON [PRIMARY]
GO
