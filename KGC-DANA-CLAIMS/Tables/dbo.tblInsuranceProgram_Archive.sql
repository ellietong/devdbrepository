CREATE TABLE [dbo].[tblInsuranceProgram_Archive]
(
[InsuranceProgramID] [int] NOT NULL IDENTITY(1, 1),
[InsuranceProgram] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblInsuranceProgram_Archive] ADD CONSTRAINT [PK_tblInsuranceProgram] PRIMARY KEY CLUSTERED  ([InsuranceProgramID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblInsuranceProgram_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblInsuranceProgram_Archive] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblInsuranceProgram_Archive] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblInsuranceProgram_Archive] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblInsuranceProgram_Archive] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblInsuranceProgram_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblInsuranceProgram_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblInsuranceProgram_Archive] TO [power_user]
GO
