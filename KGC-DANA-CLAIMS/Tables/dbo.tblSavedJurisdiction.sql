CREATE TABLE [dbo].[tblSavedJurisdiction]
(
[SavedJurisdictionID] [bigint] NOT NULL IDENTITY(1, 1),
[JurisdictionID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedJurisdiction] ADD CONSTRAINT [PK_tblSavedJurisdiction] PRIMARY KEY CLUSTERED  ([SavedJurisdictionID]) ON [PRIMARY]
GO
