CREATE TABLE [dbo].[tblSavedAllegation]
(
[SavedAllegationID] [bigint] NOT NULL IDENTITY(1, 1),
[AllegationTypeID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedAllegation] ADD CONSTRAINT [PK_tblSavedAllegation] PRIMARY KEY CLUSTERED  ([SavedAllegationID]) ON [PRIMARY]
GO
