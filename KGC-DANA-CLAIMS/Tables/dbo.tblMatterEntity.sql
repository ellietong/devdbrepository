CREATE TABLE [dbo].[tblMatterEntity]
(
[MatterEntityID] [int] NOT NULL IDENTITY(1, 1),
[MatterEntity] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMatterEntity] ADD CONSTRAINT [PK_tblMatterEntity] PRIMARY KEY CLUSTERED  ([MatterEntityID]) ON [PRIMARY]
GO
