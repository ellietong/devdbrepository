CREATE TABLE [dbo].[ztblAllSumsTest1]
(
[testID] [int] NOT NULL IDENTITY(1, 1),
[allocid] [int] NULL,
[occloop] [int] NULL,
[maxocc] [int] NULL,
[occid] [int] NULL,
[corphistid] [int] NULL,
[trigstart] [datetime] NULL,
[trigend] [datetime] NULL,
[defcost] [money] NULL,
[indcost] [money] NULL,
[numpolicies] [int] NULL,
[rank_indem] [int] NULL,
[rank_def] [int] NULL,
[defbypol] [money] NULL,
[indbypol] [money] NULL,
[polid] [int] NULL,
[poldateid] [int] NULL,
[alloc] [money] NULL,
[sir_alloc] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblAllSumsTest1] ADD CONSTRAINT [PK_ztblAllSumsTest1] PRIMARY KEY CLUSTERED  ([testID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ztblAllSumsTest1] TO [Allocation_User]
GRANT INSERT ON  [dbo].[ztblAllSumsTest1] TO [Allocation_User]
GRANT DELETE ON  [dbo].[ztblAllSumsTest1] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[ztblAllSumsTest1] TO [Allocation_User]
GRANT SELECT ON  [dbo].[ztblAllSumsTest1] TO [base_user]
GRANT SELECT ON  [dbo].[ztblAllSumsTest1] TO [power_user]
GRANT INSERT ON  [dbo].[ztblAllSumsTest1] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblAllSumsTest1] TO [power_user]
GO
