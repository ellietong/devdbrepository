CREATE TABLE [dbo].[tblPFT_TEMP]
(
[PFTID] [bigint] NOT NULL IDENTITY(1, 1),
[NavClaimKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PFTDate] [datetime] NULL,
[NavPFTType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPFTDoctor] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PFTFVC_Actual] [float] NULL,
[PFTFVC_Predicted] [float] NULL,
[PFTFEV1_Actual] [float] NULL,
[PFTFEV1_Predicted] [float] NULL,
[PFTFEV1AndFVC_Actual] [float] NULL,
[PFTFEV1AndFVC_Predicted] [float] NULL,
[PFTTLC_Actual] [float] NULL,
[PFTTLC_Predicted] [float] NULL,
[PFTDLCO_Actual] [float] NULL,
[PFTDLCO_Predicted] [float] NULL,
[PFTTV] [float] NULL,
[ArchivePFTDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPFT_TEMP] ADD CONSTRAINT [PK_tblPFT_TEMP] PRIMARY KEY CLUSTERED  ([PFTID]) ON [PRIMARY]
GO
