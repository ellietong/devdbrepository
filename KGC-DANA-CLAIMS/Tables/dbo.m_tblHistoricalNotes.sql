CREATE TABLE [dbo].[m_tblHistoricalNotes]
(
[NavClaimLawsuitKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [int] NULL,
[ClaimID] [int] NULL,
[LawsuitID] [int] NULL,
[IsMDL] [int] NULL,
[IsPrimaryForClaim] [int] NULL,
[HistoricalComments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
