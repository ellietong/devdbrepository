CREATE TABLE [dbo].[m_tblClaimantIDLink_TEMP]
(
[OldClaimantPersonalInfoID] [bigint] NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLegacyID] [bigint] NULL,
[NavRefID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
