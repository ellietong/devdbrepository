CREATE TABLE [dbo].[tblTriggerLabel]
(
[TriggerID] [int] NOT NULL IDENTITY(1, 1),
[TriggerDescription] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTriggerLabel] ADD CONSTRAINT [PK_tblTriggerLabel] PRIMARY KEY NONCLUSTERED  ([TriggerID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblTriggerLabel] TO [base_user]
GRANT SELECT ON  [dbo].[tblTriggerLabel] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[tblTriggerLabel] TO [Occurrence_User]
GRANT DELETE ON  [dbo].[tblTriggerLabel] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[tblTriggerLabel] TO [Occurrence_User]
GRANT SELECT ON  [dbo].[tblTriggerLabel] TO [power_user]
GRANT INSERT ON  [dbo].[tblTriggerLabel] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTriggerLabel] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderByOn', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_ColumnHidden', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', 'COLUMN', N'TriggerDescription'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', 'COLUMN', N'TriggerDescription'
GO
DECLARE @xp int
SELECT @xp=1890
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', 'COLUMN', N'TriggerDescription'
GO
EXEC sp_addextendedproperty N'MS_ColumnHidden', N'False', 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', 'COLUMN', N'TriggerID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', 'COLUMN', N'TriggerID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblTriggerLabel', 'COLUMN', N'TriggerID'
GO
