CREATE TABLE [dbo].[Temp_DefenseExpense]
(
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Percentage] [float] NULL
) ON [PRIMARY]
GO
