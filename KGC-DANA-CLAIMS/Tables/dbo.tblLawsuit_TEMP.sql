CREATE TABLE [dbo].[tblLawsuit_TEMP]
(
[LawsuitID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLawsuitID] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CaseCaption] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[ServiceDate] [datetime] NULL,
[ReceivedDate] [datetime] NULL,
[ActiveTrialDate] [datetime] NULL,
[NavJurisdiction] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavState] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavCourtType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDefenseCounsel] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPlaintiffCounsel] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavNationalPlaintiffCounsel] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffFirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffLastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsThirdPartyComplaint] [bit] NULL,
[IsMDL] [bit] NULL,
[NavIsThirdPartyComplaint] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavIsMDL] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavFileDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavServiceDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JurisdictionID] [int] NULL,
[StateID] [int] NULL,
[CourtTypeID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[PlaintiffCounselAddressID] [int] NULL,
[PrimaryNationalPlaintiffCounselID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLawsuit_TEMP] ADD CONSTRAINT [PK_tblLawsuit_TEMP] PRIMARY KEY CLUSTERED  ([LawsuitID_ALT]) ON [PRIMARY]
GO
