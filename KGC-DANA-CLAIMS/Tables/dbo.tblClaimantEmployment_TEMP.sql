CREATE TABLE [dbo].[tblClaimantEmployment_TEMP]
(
[ClaimantEmploymentID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavOccupation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jobsite] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimary] [bit] NULL,
[Comments] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmpStartDate] [datetime] NULL,
[EmpEndDate] [datetime] NULL,
[EarliestExposureDate] [datetime] NULL,
[LatestExposureDate] [datetime] NULL,
[JobsiteCity] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavJobsiteState] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSecondaryExposure] [bit] NULL,
[IsApprovedExposure] [bit] NULL,
[NavIsPrimary] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavIsSecondaryExposure] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavIsApprovedExposure] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArchiveEmpStartDate] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArchiveEmpEndDate] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArchiveEarliestExposureDate] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArchiveLatestExposureDate] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobStateID] [int] NULL,
[OccupationID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantEmployment_TEMP] ADD CONSTRAINT [PK_tblClaimantEmployment_TEMP] PRIMARY KEY CLUSTERED  ([ClaimantEmploymentID_ALT]) ON [PRIMARY]
GO
