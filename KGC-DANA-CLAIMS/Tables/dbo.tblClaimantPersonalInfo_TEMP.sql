CREATE TABLE [dbo].[tblClaimantPersonalInfo_TEMP]
(
[ClaimantPersonalInfoID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[FirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialSecurityNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [datetime] NULL,
[DeceasedDate] [datetime] NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavRefID] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLegacyID] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [bigint] NULL,
[SuffixID] [int] NULL,
[NavSuffix] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaritalStatusID] [int] NULL,
[NavMaritalStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GenderID] [int] NULL,
[NavGender] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[NavState] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSmoker] [bit] NULL,
[NavSmoker] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArchiveBirthDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArchiveDeceasedDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicareBeneficiaryStatusID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantPersonalInfo_TEMP] ADD CONSTRAINT [PK_tblClaimantPersonalInfo_TEMP] PRIMARY KEY CLUSTERED  ([ClaimantPersonalInfoID_ALT]) ON [PRIMARY]
GO
