CREATE TABLE [dbo].[m_tblPacePremiseExhaustion]
(
[PolicyNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PACEInsurerName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PACELimitAmount] [money] NULL,
[Indemnity] [money] NULL,
[Expense] [money] NULL,
[Total] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblPacePremiseExhaustion] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPacePremiseExhaustion] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblPacePremiseExhaustion] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblPacePremiseExhaustion] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblPacePremiseExhaustion] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblPacePremiseExhaustion] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblPacePremiseExhaustion] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblPacePremiseExhaustion] TO [power_user]
GO
