CREATE TABLE [dbo].[m_tblHistoricalLiability]
(
[HistoricalLiabilityID] [bigint] NOT NULL IDENTITY(1, 1),
[BillMatterID] [int] NOT NULL,
[BillDate] [date] NOT NULL,
[BillTypeID] [int] NOT NULL,
[Indemnity] [money] NULL,
[Defense] [money] NULL,
[NavBillNumber] [bigint] NULL,
[NavMonth] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavYear] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblHistoricalLiability] ADD CONSTRAINT [PK_m_tblHistoricalLiability] PRIMARY KEY CLUSTERED  ([HistoricalLiabilityID]) ON [PRIMARY]
GO
