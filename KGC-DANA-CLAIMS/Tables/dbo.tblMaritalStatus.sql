CREATE TABLE [dbo].[tblMaritalStatus]
(
[MaritalStatusID] [int] NOT NULL IDENTITY(1, 1),
[MaritalStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMaritalStatus] ADD CONSTRAINT [PK_mBtblMaritalStatus] PRIMARY KEY CLUSTERED  ([MaritalStatusID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblMaritalStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblMaritalStatus] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblMaritalStatus] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblMaritalStatus] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblMaritalStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblMaritalStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblMaritalStatus] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblMaritalStatus', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblMaritalStatus', 'COLUMN', N'MaritalStatus'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblMaritalStatus', 'COLUMN', N'MaritalStatus'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblMaritalStatus', 'COLUMN', N'MaritalStatus'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblMaritalStatus', 'COLUMN', N'MaritalStatusID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblMaritalStatus', 'COLUMN', N'MaritalStatusID'
GO
EXEC sp_addextendedproperty N'MS_IMEMode', N'0', 'SCHEMA', N'dbo', 'TABLE', N'tblMaritalStatus', 'COLUMN', N'MaritalStatusID'
GO
