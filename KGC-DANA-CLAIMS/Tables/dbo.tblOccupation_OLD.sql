CREATE TABLE [dbo].[tblOccupation_OLD]
(
[OccupationID] [int] NOT NULL IDENTITY(1, 1),
[Occupation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccupation_OLD] ADD CONSTRAINT [PK_tblOccupation] PRIMARY KEY CLUSTERED  ([OccupationID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccupation_OLD] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblOccupation_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblOccupation_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccupation_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccupation_OLD] TO [power_user]
GO
