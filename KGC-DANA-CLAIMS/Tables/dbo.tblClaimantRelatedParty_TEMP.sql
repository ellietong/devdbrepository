CREATE TABLE [dbo].[tblClaimantRelatedParty_TEMP]
(
[ClaimantRelatedPartyID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavRelationship] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLegalCapacity] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPAddress1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPAddress2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPCity] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[RPZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPPhone] [bigint] NULL,
[RPSocialSecurityNumber] [bigint] NULL,
[RPTIN] [int] NULL,
[RelationshipID] [int] NULL,
[LegalCapacityID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantRelatedParty_TEMP] ADD CONSTRAINT [PK_tblClaimantRelatedParty_TEMP] PRIMARY KEY CLUSTERED  ([ClaimantRelatedPartyID_ALT]) ON [PRIMARY]
GO
