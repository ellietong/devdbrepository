CREATE TABLE [dbo].[tblSavedDocketNumber]
(
[SavedDocketNumberID] [bigint] NOT NULL IDENTITY(1, 1),
[DocketNumber] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SavedSearchID] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedDocketNumber] ADD CONSTRAINT [PK_tblSavedDocketNumber] PRIMARY KEY CLUSTERED  ([SavedDocketNumberID]) ON [PRIMARY]
GO
