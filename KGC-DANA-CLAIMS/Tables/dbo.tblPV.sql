CREATE TABLE [dbo].[tblPV]
(
[PVID] [int] NOT NULL IDENTITY(1, 1),
[Method] [int] NULL,
[PVRate] [real] NULL,
[DateToPresentValue] [datetime] NULL,
[YieldCurveID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPV] ADD CONSTRAINT [PK_tblPV] PRIMARY KEY CLUSTERED  ([PVID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPV] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblPV] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblPV] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblPV] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblPV] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblPV] TO [base_user]
GRANT ALTER ON  [dbo].[tblPV] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPV] TO [power_user]
GRANT SELECT ON  [dbo].[tblPV] TO [power_user]
GRANT INSERT ON  [dbo].[tblPV] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPV] TO [power_user]
GO
