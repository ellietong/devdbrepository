CREATE TABLE [dbo].[tblClaimLawsuitStatusTrackingID_Issues]
(
[ClaimLawsuitStatusTrackignID] [int] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTrackingID_Issues] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTrackingID_Issues] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTrackingID_Issues] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatusTrackingID_Issues] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTrackingID_Issues] TO [Claims_User]
GO
