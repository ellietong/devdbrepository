CREATE TABLE [dbo].[tblPolicy_Archive]
(
[PolicyID] [int] NOT NULL IDENTITY(1, 1),
[MinPStartDate] [datetime] NULL,
[CarrierID] [int] NULL,
[NamedInsuredID] [int] NULL,
[InsuranceProgramID] [int] NULL,
[PolicyNum] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PolicyNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyReviewed] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyPDF] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitialDataEntry] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitialDataEntryDate] [datetime] NULL,
[PolicyReview] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PolicyReviewDate] [datetime] NULL,
[PolPicPDFLink] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoNotReport] [bit] NULL CONSTRAINT [DF_tblPolicy_DoNotReport] DEFAULT ((0)),
[NavAIGPolicyID] [bigint] NULL,
[NavSigPolicyID] [bigint] NULL,
[NavNonSigPolicyID] [bigint] NULL,
[NavCarrier] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldPolicyID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicy_Archive] ADD CONSTRAINT [PK_tblPolicy] PRIMARY KEY CLUSTERED  ([PolicyID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPolicy_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicy_Archive] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblPolicy_Archive] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblPolicy_Archive] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblPolicy_Archive] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblPolicy_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicy_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicy_Archive] TO [power_user]
GO
