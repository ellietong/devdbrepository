CREATE TABLE [dbo].[ztblAllSumsTest2]
(
[poltestid] [int] NOT NULL IDENTITY(1, 1),
[allocid] [int] NULL,
[occid] [int] NULL,
[trigstart] [datetime] NULL,
[trigend] [datetime] NULL,
[polid] [int] NULL,
[poldateid] [int] NULL,
[pstart] [datetime] NULL,
[pend] [datetime] NULL,
[polpct] [decimal] (18, 3) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblAllSumsTest2] ADD CONSTRAINT [PK_ztblAllSumsTest2] PRIMARY KEY CLUSTERED  ([poltestid]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ztblAllSumsTest2] TO [Allocation_User]
GRANT INSERT ON  [dbo].[ztblAllSumsTest2] TO [Allocation_User]
GRANT DELETE ON  [dbo].[ztblAllSumsTest2] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[ztblAllSumsTest2] TO [Allocation_User]
GRANT SELECT ON  [dbo].[ztblAllSumsTest2] TO [base_user]
GRANT SELECT ON  [dbo].[ztblAllSumsTest2] TO [power_user]
GRANT INSERT ON  [dbo].[ztblAllSumsTest2] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblAllSumsTest2] TO [power_user]
GO
