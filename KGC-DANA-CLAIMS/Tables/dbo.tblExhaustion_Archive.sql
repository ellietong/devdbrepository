CREATE TABLE [dbo].[tblExhaustion_Archive]
(
[ExhaustionID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionDateID] [int] NOT NULL,
[OccurrenceID] [bigint] NULL,
[PolicyDateID] [int] NOT NULL,
[IndemnityAllocation] [money] NOT NULL CONSTRAINT [DF_tblExhaustion_IndemnityAllocation] DEFAULT ((0)),
[DefenseAllocation] [money] NOT NULL CONSTRAINT [DF_tblExhaustion_DefenseAllocation] DEFAULT ((0)),
[DefenseOutsideLimitsAllocation] [money] NOT NULL CONSTRAINT [DF_tblExhaustion_DefenseOutsideLimitsAllocation] DEFAULT ((0)),
[SIRIndemnityAllocation] [money] NOT NULL CONSTRAINT [DF_tblExhaustion_SIRIndemnityAllocation] DEFAULT ((0)),
[SIRDefenseAllocation] [money] NOT NULL CONSTRAINT [DF_tblExhaustion_SIRDefenseAllocation] DEFAULT ((0)),
[LedgerID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustion_Archive] ADD CONSTRAINT [PK_tblExhaustion] PRIMARY KEY CLUSTERED  ([ExhaustionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblExhaustion_Archive] ADD CONSTRAINT [FK_tblExhaustion_tblExhaustionDate] FOREIGN KEY ([ExhaustionDateID]) REFERENCES [dbo].[tblExhaustionDate_Archive] ([ExhaustionDateID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblExhaustion_Archive] ADD CONSTRAINT [FK_tblExhaustion_tblPolicyDates] FOREIGN KEY ([PolicyDateID]) REFERENCES [dbo].[tblPolicyDates_Archive] ([PolicyDateID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblExhaustion_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblExhaustion_Archive] TO [Exhaustion_User]
GRANT INSERT ON  [dbo].[tblExhaustion_Archive] TO [Exhaustion_User]
GRANT DELETE ON  [dbo].[tblExhaustion_Archive] TO [Exhaustion_User]
GRANT UPDATE ON  [dbo].[tblExhaustion_Archive] TO [Exhaustion_User]
GRANT SELECT ON  [dbo].[tblExhaustion_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblExhaustion_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblExhaustion_Archive] TO [power_user]
GO
