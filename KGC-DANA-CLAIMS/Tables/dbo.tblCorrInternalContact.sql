CREATE TABLE [dbo].[tblCorrInternalContact]
(
[CorrInternalContactID] [int] NOT NULL IDENTITY(1, 1),
[CorrespondenceID] [int] NOT NULL,
[InternalContactID] [int] NOT NULL,
[RecipientTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorrInternalContact] ADD CONSTRAINT [PK_tblCorrInternalContact] PRIMARY KEY CLUSTERED  ([CorrInternalContactID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCorrInternalContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorrInternalContact] TO [Notice_User]
GRANT INSERT ON  [dbo].[tblCorrInternalContact] TO [Notice_User]
GRANT DELETE ON  [dbo].[tblCorrInternalContact] TO [Notice_User]
GRANT UPDATE ON  [dbo].[tblCorrInternalContact] TO [Notice_User]
GRANT SELECT ON  [dbo].[tblCorrInternalContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblCorrInternalContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCorrInternalContact] TO [power_user]
GO
