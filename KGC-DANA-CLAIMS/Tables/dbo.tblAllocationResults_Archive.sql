CREATE TABLE [dbo].[tblAllocationResults_Archive]
(
[AllocationResultID] [bigint] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[PolicyID] [int] NULL,
[PolicyDateID] [bigint] NULL,
[PStartSplit] [datetime] NULL,
[PEndSplit] [datetime] NULL,
[AnnualPeriodSplit] [int] NULL,
[Allocation] [money] NULL,
[AllocationIndemnity] [money] NULL,
[AllocationDefense] [money] NULL,
[AllocationDefenseOutsideLimits] [money] NULL,
[OccurrenceID] [bigint] NULL,
[TriggerStartDateUsed] [datetime] NULL,
[TriggerEndDateUsed] [datetime] NULL,
[SIRAllocated] [money] NULL,
[SIRAllocatedIndemnity] [money] NULL,
[SIRAllocatedDefense] [money] NULL,
[SIRDiscount] [float] NULL,
[DateTime] [datetime] NULL,
[PV] [decimal] (38, 15) NULL,
[DateToPresentValue] [datetime] NULL,
[PVRate] [decimal] (18, 15) NULL,
[YieldCurveID] [int] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblAllocationResults] ON [dbo].[tblAllocationResults_Archive] ([AllocationResultID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_allocationid] ON [dbo].[tblAllocationResults_Archive] ([AllocationResultID], [AllocationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_psplitstart] ON [dbo].[tblAllocationResults_Archive] ([PStartSplit], [PEndSplit]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResults_Archive] ADD CONSTRAINT [FK_tblAllocationResults_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation_Archive] ([AllocationID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResults_Archive] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationResults_Archive] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationResults_Archive] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationResults_Archive] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationResults_Archive] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationResults_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResults_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationResults_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationResults_Archive] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResults_Archive', 'COLUMN', N'AllocationResultID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResults_Archive', 'COLUMN', N'OccurrenceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResults_Archive', 'COLUMN', N'OccurrenceID'
GO
