CREATE TABLE [dbo].[tblClaimantRelease_TEMP]
(
[ClaimantReleaseID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavClaimResolutionKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavReleaseType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseSignedDate] [datetime] NULL,
[ReleaseSentDate] [datetime] NULL,
[ReleaseReceivedDate] [datetime] NULL,
[ReleaseTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantRelease_TEMP] ADD CONSTRAINT [PK_tblClaimantRelease_TEMP] PRIMARY KEY CLUSTERED  ([ClaimantReleaseID_ALT]) ON [PRIMARY]
GO
