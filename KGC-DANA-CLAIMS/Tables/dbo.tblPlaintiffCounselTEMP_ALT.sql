CREATE TABLE [dbo].[tblPlaintiffCounselTEMP_ALT]
(
[PlaintiffCounselID_TEMP] [bigint] NOT NULL IDENTITY(1, 1),
[PlaintiffCounselAddress_Nav] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselAddressID] [int] NULL,
[PlaintiffCounselID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPlaintiffCounselTEMP_ALT] ADD CONSTRAINT [PK_tblPlaintiffCounselTEMP_ALT] PRIMARY KEY CLUSTERED  ([PlaintiffCounselID_TEMP]) ON [PRIMARY]
GO
