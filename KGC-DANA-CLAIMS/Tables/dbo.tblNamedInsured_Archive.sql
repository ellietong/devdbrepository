CREATE TABLE [dbo].[tblNamedInsured_Archive]
(
[NamedInsuredID] [int] NOT NULL IDENTITY(1, 1),
[NamedInsured] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblNamedInsured_Archive] ADD CONSTRAINT [PK_tblNamedInsured] PRIMARY KEY NONCLUSTERED  ([NamedInsuredID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblNamedInsured_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblNamedInsured_Archive] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblNamedInsured_Archive] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblNamedInsured_Archive] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblNamedInsured_Archive] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblNamedInsured_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblNamedInsured_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblNamedInsured_Archive] TO [power_user]
GO
