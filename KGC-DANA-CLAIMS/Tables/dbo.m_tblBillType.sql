CREATE TABLE [dbo].[m_tblBillType]
(
[BillTypeID] [int] NOT NULL IDENTITY(1, 1),
[BillType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblBillType] ADD CONSTRAINT [PK_m_tblBillType] PRIMARY KEY CLUSTERED  ([BillTypeID]) ON [PRIMARY]
GO
