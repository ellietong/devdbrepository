CREATE TABLE [dbo].[tblAllocationResultsColumn_Archive]
(
[ColumnID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[InsuranceProgramID] [int] NULL,
[ColumnStartDate] [datetime] NULL,
[ColumnEndDate] [datetime] NULL,
[ColumnDays] [int] NULL,
[ColumnPerOcc] [money] NULL,
[ColumnCumAllocation] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsColumn_Archive] ADD CONSTRAINT [PK_tblAllocationResultsColumn] PRIMARY KEY NONCLUSTERED  ([ColumnID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsColumn_Archive] ADD CONSTRAINT [FK_tblAllocationResultsColumn_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation_Archive] ([AllocationID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsColumn_Archive] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationResultsColumn_Archive] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationResultsColumn_Archive] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationResultsColumn_Archive] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationResultsColumn_Archive] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationResultsColumn_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsColumn_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationResultsColumn_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationResultsColumn_Archive] TO [power_user]
GO
