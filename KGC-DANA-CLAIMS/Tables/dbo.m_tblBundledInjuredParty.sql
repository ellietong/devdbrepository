CREATE TABLE [dbo].[m_tblBundledInjuredParty]
(
[Injured_Party_Id] [float] NOT NULL,
[First_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Middle_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SS_Nbr] [float] NULL,
[Date of Birth] [datetime] NULL,
[Date of Death] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblBundledInjuredParty] ADD CONSTRAINT [PK_m_tblBundledInjuredParty] PRIMARY KEY CLUSTERED  ([Injured_Party_Id]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblBundledInjuredParty] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblBundledInjuredParty] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblBundledInjuredParty] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblBundledInjuredParty] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblBundledInjuredParty] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblBundledInjuredParty] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblBundledInjuredParty] TO [power_user]
GO
