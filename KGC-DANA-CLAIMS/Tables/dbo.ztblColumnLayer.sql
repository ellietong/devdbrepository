CREATE TABLE [dbo].[ztblColumnLayer]
(
[ColumnLayerID] [int] NOT NULL IDENTITY(1, 1),
[SigLevelColumnID] [int] NOT NULL,
[Layer] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblColumnLayer] ADD CONSTRAINT [PK_ztblColumnLayer] PRIMARY KEY CLUSTERED  ([ColumnLayerID]) ON [PRIMARY]
GO
