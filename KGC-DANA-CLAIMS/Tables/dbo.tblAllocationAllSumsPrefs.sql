CREATE TABLE [dbo].[tblAllocationAllSumsPrefs]
(
[AllSumsPrefsID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NOT NULL,
[PolicyID] [int] NOT NULL,
[Rank_Indem] [int] NOT NULL CONSTRAINT [DF_tblAllocationAllSumsPrefs_Indemnity] DEFAULT ((0)),
[Rank_Def] [int] NOT NULL CONSTRAINT [DF_tblAllocationAllSumsPrefs_Rank_Def] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationAllSumsPrefs] ADD CONSTRAINT [PK_tblAllocationAllSumsPrefs] PRIMARY KEY CLUSTERED  ([AllSumsPrefsID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationAllSumsPrefs] ADD CONSTRAINT [FK_tblAllocationAllSumsPrefs_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation_Archive] ([AllocationID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationAllSumsPrefs] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationAllSumsPrefs] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationAllSumsPrefs] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationAllSumsPrefs] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationAllSumsPrefs] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationAllSumsPrefs] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationAllSumsPrefs] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationAllSumsPrefs] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationAllSumsPrefs] TO [power_user]
GO
