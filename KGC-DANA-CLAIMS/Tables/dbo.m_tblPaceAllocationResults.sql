CREATE TABLE [dbo].[m_tblPaceAllocationResults]
(
[AllocationID] [int] NULL,
[ClaimID] [int] NULL,
[PolicyID] [int] NULL,
[SegmentID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EffectiveDays] [decimal] (18, 0) NULL,
[PercentOfDays] [decimal] (18, 0) NULL,
[TotalIndemnity] [money] NULL,
[TotalExpense] [money] NULL,
[DeductIncurred] [money] NULL,
[SIRIncurred] [money] NULL,
[InsolvIncurred] [money] NULL,
[AmountType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblPaceAllocationResults] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPaceAllocationResults] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblPaceAllocationResults] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblPaceAllocationResults] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblPaceAllocationResults] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblPaceAllocationResults] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblPaceAllocationResults] TO [power_user]
GO
