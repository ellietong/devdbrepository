CREATE TABLE [dbo].[tblSettlePaymentDocumentation]
(
[SetPymtID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[SettlementConfirmation] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_SettlementConfirmation] DEFAULT ((0)),
[ClaimantIDed] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_ClaimantIDed] DEFAULT ((0)),
[DxMedicals] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_DxMedicals] DEFAULT ((0)),
[DODCert] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_DODCert] DEFAULT ((0)),
[EstatePapers] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_EstatePapers] DEFAULT ((0)),
[ProductID] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_ProductID] DEFAULT ((0)),
[MMSEA] [bit] NULL,
[MedicarePlaintiff] [bit] NULL,
[MedicarePlaintiffCounsel] [bit] NULL,
[EmployExpoHist] [bit] NULL CONSTRAINT [DF_tblSettlePaymentDocumentation_EmployExpoHist] DEFAULT ((0)),
[ReleaseRqstDate] [datetime] NULL,
[ReleaseToPC] [datetime] NULL,
[ExecutedReleaseDate] [datetime] NULL,
[SettlePaymentComments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseRcvd] [bit] NULL,
[MedicareFullyDocumented] [bit] NULL,
[MedicareReported] [bit] NULL,
[ReleaseSignedDate] [datetime] NULL,
[ReleaseTypeID] [int] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlePaymentDocumentation] ADD CONSTRAINT [PK_tblSettlePaymentDocumentation] PRIMARY KEY CLUSTERED  ([SetPymtID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblSettlePaymentDocumentation] TO [base_user]
GRANT ALTER ON  [dbo].[tblSettlePaymentDocumentation] TO [chaudoinr]
GRANT SELECT ON  [dbo].[tblSettlePaymentDocumentation] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblSettlePaymentDocumentation] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblSettlePaymentDocumentation] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlePaymentDocumentation] TO [power_user]
GO
