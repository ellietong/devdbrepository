CREATE TABLE [dbo].[tblPlaintiffCounselAddress_ALT]
(
[PlaintiffCounselAddressID] [int] NOT NULL IDENTITY(1, 1),
[PlaintiffCounselID] [int] NULL,
[PlaintiffCounselAddress1] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselAddress2] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselStateID] [int] NULL,
[PlaintiffCounselZip] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselMailZip] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselExtension] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayeeAddress] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPlaintiffCounselAddress_ALT] ADD CONSTRAINT [PK_tblPlaintiffCounselAddress_ALT] PRIMARY KEY CLUSTERED  ([PlaintiffCounselAddressID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounselAddress_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounselAddress_ALT] TO [base_user]
GRANT ALTER ON  [dbo].[tblPlaintiffCounselAddress_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounselAddress_ALT] TO [power_user]
GRANT INSERT ON  [dbo].[tblPlaintiffCounselAddress_ALT] TO [power_user]
GRANT DELETE ON  [dbo].[tblPlaintiffCounselAddress_ALT] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPlaintiffCounselAddress_ALT] TO [power_user]
GO
