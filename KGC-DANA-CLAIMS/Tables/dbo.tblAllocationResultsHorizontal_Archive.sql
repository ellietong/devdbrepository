CREATE TABLE [dbo].[tblAllocationResultsHorizontal_Archive]
(
[ARhorizontalID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[OccurrenceID] [bigint] NULL,
[uncollapsedPrior] [money] NULL,
[uncollapsedPost] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsHorizontal_Archive] ADD CONSTRAINT [PK_tblAllocationResultsHorizontal] PRIMARY KEY NONCLUSTERED  ([ARhorizontalID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationResultsHorizontal_Archive] ADD CONSTRAINT [FK_tblAllocationResultsHorizontal_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation_Archive] ([AllocationID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationResultsHorizontal_Archive] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal_Archive] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationResultsHorizontal_Archive] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationResultsHorizontal_Archive] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationResultsHorizontal_Archive] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationResultsHorizontal_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationResultsHorizontal_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationResultsHorizontal_Archive] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResultsHorizontal_Archive', 'COLUMN', N'OccurrenceID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationResultsHorizontal_Archive', 'COLUMN', N'OccurrenceID'
GO
