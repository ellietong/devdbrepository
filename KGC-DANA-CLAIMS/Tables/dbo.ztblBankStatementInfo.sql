CREATE TABLE [dbo].[ztblBankStatementInfo]
(
[CheckDate] [datetime] NULL,
[CheckAmount] [money] NULL,
[Star] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNote] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[ztblBankStatementInfo] TO [base_user]
GRANT ALTER ON  [dbo].[ztblBankStatementInfo] TO [power_user]
GRANT SELECT ON  [dbo].[ztblBankStatementInfo] TO [power_user]
GRANT INSERT ON  [dbo].[ztblBankStatementInfo] TO [power_user]
GRANT DELETE ON  [dbo].[ztblBankStatementInfo] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblBankStatementInfo] TO [power_user]
GO
