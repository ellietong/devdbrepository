CREATE TABLE [dbo].[tblCorpHistory]
(
[CorpHistoryID] [int] NOT NULL IDENTITY(1, 1),
[AcqSaleDate] [smalldatetime] NULL,
[AcqSoldID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PurchaseTypeID] [int] NULL,
[InsuranceProgramID] [int] NULL,
[AcquiringCompany] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcquiredCompany] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreviousOwner] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewOwner] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CorpSourceID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCorpHistory] ADD CONSTRAINT [PK_mEtblCorpHistory] PRIMARY KEY CLUSTERED  ([CorpHistoryID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblCorpHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblCorpHistory] TO [CorpHist_User]
GRANT INSERT ON  [dbo].[tblCorpHistory] TO [CorpHist_User]
GRANT DELETE ON  [dbo].[tblCorpHistory] TO [CorpHist_User]
GRANT UPDATE ON  [dbo].[tblCorpHistory] TO [CorpHist_User]
GRANT SELECT ON  [dbo].[tblCorpHistory] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblCorpHistory] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblCorpHistory] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblCorpHistory] TO [Enviro_User]
GRANT SELECT ON  [dbo].[tblCorpHistory] TO [power_user]
GRANT INSERT ON  [dbo].[tblCorpHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCorpHistory] TO [power_user]
GO
