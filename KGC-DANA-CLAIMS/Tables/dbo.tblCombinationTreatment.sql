CREATE TABLE [dbo].[tblCombinationTreatment]
(
[CombinationTreatmentID] [int] NOT NULL IDENTITY(1, 1),
[ScenarioID] [int] NOT NULL,
[Combination] [int] NOT NULL,
[TreatmentID] [int] NULL,
[TreatmentNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCombinationTreatment] ADD CONSTRAINT [PK_tblCombinationTreatment] PRIMARY KEY CLUSTERED  ([CombinationTreatmentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblCombinationTreatment] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblCombinationTreatment] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblCombinationTreatment] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblCombinationTreatment] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblCombinationTreatment] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblCombinationTreatment] TO [base_user]
GRANT SELECT ON  [dbo].[tblCombinationTreatment] TO [power_user]
GRANT INSERT ON  [dbo].[tblCombinationTreatment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblCombinationTreatment] TO [power_user]
GO
