CREATE TABLE [dbo].[tblSavedMatterEntity]
(
[SavedMatterEntityID] [int] NOT NULL IDENTITY(1, 1),
[MatterEntityID] [int] NOT NULL,
[SavedSearchID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedMatterEntity] ADD CONSTRAINT [PK_tblSavedMatterEntity] PRIMARY KEY CLUSTERED  ([SavedMatterEntityID]) ON [PRIMARY]
GO
