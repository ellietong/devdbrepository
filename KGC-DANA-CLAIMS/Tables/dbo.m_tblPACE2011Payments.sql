CREATE TABLE [dbo].[m_tblPACE2011Payments]
(
[PACEPaymentID] [int] NULL,
[ExpertFirmID] [int] NULL,
[DefenseCounselAddressID] [int] NULL,
[PlaintiffCounselAddressID] [int] NULL,
[InvoiceTotal] [money] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblPACE2011Payments] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblPACE2011Payments] TO [Claims_User]
GRANT DELETE ON  [dbo].[m_tblPACE2011Payments] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblPACE2011Payments] TO [Claims_User]
GO
