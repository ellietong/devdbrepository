CREATE TABLE [dbo].[tblClaimLawsuitStatusTracking]
(
[ClaimLawsuitStatusTrackingID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [bigint] NULL,
[ClaimLawsuitStatusID] [int] NULL,
[StatusDate_old] [datetime] NULL,
[StatusNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DismissalDate_old] [datetime] NULL,
[IsPrimaryStatus] [bit] NULL,
[NavStatus] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessedDate] [datetime] NULL,
[StatusDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [PK_tblClaimLawsuitStatusTracking] PRIMARY KEY CLUSTERED  ([ClaimLawsuitStatusTrackingID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaimLawsuitStatusTracking_ClaimLawsuitID_ClaimLawsuitStatusID] ON [dbo].[tblClaimLawsuitStatusTracking] ([ClaimLawsuitID]) INCLUDE ([ClaimLawsuitStatusID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_IsPrimaryStatus_ClaimLawsuitStatusID_ClaimLawsuitID] ON [dbo].[tblClaimLawsuitStatusTracking] ([IsPrimaryStatus], [ClaimLawsuitStatusID]) INCLUDE ([ClaimLawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_IsPrimaryStatus_ClaimLawsuitStatusID_ClaimLawsuitID_StatusDate] ON [dbo].[tblClaimLawsuitStatusTracking] ([IsPrimaryStatus], [ClaimLawsuitStatusID]) INCLUDE ([ClaimLawsuitID], [StatusDate_old]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [FK_tblClaimLawsuitStatusTracking_tblClaimLawsuit] FOREIGN KEY ([ClaimLawsuitID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID])
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking] ADD CONSTRAINT [FK_tblClaimLawsuitStatusTracking_tblClaimLawsuitStatus] FOREIGN KEY ([ClaimLawsuitStatusID]) REFERENCES [dbo].[tblClaimLawsuitStatus] ([ClaimLawsuitStatusID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [base_user]
GRANT ALTER ON  [dbo].[tblClaimLawsuitStatusTracking] TO [chaudoinr]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblClaimLawsuitStatusTracking] TO [monahanc]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimLawsuitStatusTracking] TO [power_user]
GRANT ALTER ON  [dbo].[tblClaimLawsuitStatusTracking] TO [prossc]
GO
