CREATE TABLE [dbo].[m_tblPACEHistDefInvoice]
(
[INVOICEID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCNID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PERIOD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FEES] [money] NULL,
[EXPENSES] [money] NULL,
[ADJUSTMENTS] [money] NULL,
[TOTAL] [money] NULL,
[STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHECKNUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHECKDATE] [datetime] NULL,
[VendorName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
