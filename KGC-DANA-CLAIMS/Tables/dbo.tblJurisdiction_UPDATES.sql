CREATE TABLE [dbo].[tblJurisdiction_UPDATES]
(
[JurisdictionID_OLD] [int] NULL,
[JurisdictionID_NEW] [int] NULL,
[Jurisdiction] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
