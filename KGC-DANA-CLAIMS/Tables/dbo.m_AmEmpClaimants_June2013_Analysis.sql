CREATE TABLE [dbo].[m_AmEmpClaimants_June2013_Analysis]
(
[ClaimantPersonalInfoID] [int] NULL,
[DOFE] [datetime] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_AmEmpClaimants_June2013_Analysis] TO [base_user]
GRANT SELECT ON  [dbo].[m_AmEmpClaimants_June2013_Analysis] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_AmEmpClaimants_June2013_Analysis] TO [Billing_User]
GRANT SELECT ON  [dbo].[m_AmEmpClaimants_June2013_Analysis] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[m_AmEmpClaimants_June2013_Analysis] TO [power_user]
GRANT SELECT ON  [dbo].[m_AmEmpClaimants_June2013_Analysis] TO [power_user]
GRANT INSERT ON  [dbo].[m_AmEmpClaimants_June2013_Analysis] TO [power_user]
GRANT DELETE ON  [dbo].[m_AmEmpClaimants_June2013_Analysis] TO [power_user]
GRANT UPDATE ON  [dbo].[m_AmEmpClaimants_June2013_Analysis] TO [power_user]
GO
