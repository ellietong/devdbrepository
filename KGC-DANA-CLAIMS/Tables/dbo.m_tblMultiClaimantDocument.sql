CREATE TABLE [dbo].[m_tblMultiClaimantDocument]
(
[MultiClaimantDocumentID] [int] NULL,
[DocumentID] [int] NULL,
[CPI_Old] [int] NULL,
[CPI_Keep] [int] NULL
) ON [PRIMARY]
GO
