CREATE TABLE [dbo].[tblAllocationScheme]
(
[AllocationSchemeID] [int] NOT NULL IDENTITY(1, 1),
[AllocationScheme] [nvarchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationScheme] ADD CONSTRAINT [PK_tblAllocationScheme] PRIMARY KEY NONCLUSTERED  ([AllocationSchemeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationScheme] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationScheme] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationScheme] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationScheme] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationScheme] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationScheme] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationScheme] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationScheme] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationScheme] TO [power_user]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1000000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', 'COLUMN', N'AllocationScheme'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', 'COLUMN', N'AllocationScheme'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', 'COLUMN', N'AllocationScheme'
GO
DECLARE @xp smallint
SELECT @xp=3225
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', 'COLUMN', N'AllocationScheme'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', 'COLUMN', N'AllocationScheme'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', 'COLUMN', N'AllocationSchemeID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', 'COLUMN', N'AllocationSchemeID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', 'COLUMN', N'AllocationSchemeID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', 'COLUMN', N'AllocationSchemeID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationScheme', 'COLUMN', N'AllocationSchemeID'
GO
