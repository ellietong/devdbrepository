CREATE TABLE [dbo].[tblILO_TEMP]
(
[ILOID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavILODoctor] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILOProfusion] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblILO_TEMP] ADD CONSTRAINT [PK_tblILO_TEMP] PRIMARY KEY CLUSTERED  ([ILOID_ALT]) ON [PRIMARY]
GO
