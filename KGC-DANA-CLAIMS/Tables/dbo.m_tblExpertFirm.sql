CREATE TABLE [dbo].[m_tblExpertFirm]
(
[ExpertFirmID] [int] NOT NULL IDENTITY(1, 1),
[ExpertFirm] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NULL,
[ZipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[INS_EmailAlert]
   ON  [dbo].[m_tblExpertFirm]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Declare and set email inputs.
DECLARE @Subject nvarchar(100)
DECLARE @BodyMessage nvarchar(2000)

SET @Subject =    'Dana Claims: Expert Firm Added'
--Set body font, insert KCIC logo/banner at the top of the email  
--KCIC logo is pulled from the KCIC website.  If you need assistance please contact D. Snyder.     
SET @BodyMessage = '<FONT FACE = "Goudy Old Style">
<img src="http://www.kcicllc.com/i/KCICLogoBar_solid.PNG" alt="logo"/>'
--Create temp table to hold all new documents



      --List which firm, how many documents, and the firm url
      SET @BodyMessage = @BodyMessage + ' <b><h3>New Expert Invoice Firm Added:</h3></b> <br><br>' 
      SET @BodyMessage = @BodyMessage + ' <b>Expert Firm:</b> ' + (SELECT COALESCE(inserted.ExpertFirm, '') FROM inserted)  + ' <br>'
      SET @BodyMessage = @BodyMessage + ' <b>Address1:</b> '   + (SELECT COALESCE(inserted.Address1, '') FROM inserted) + ' <br>'
      SET @BodyMessage = @BodyMessage + ' <b>Address2:</b> '   + (SELECT COALESCE(inserted.Address2, '') FROM inserted) + ' <br>'
      SET @BodyMessage = @BodyMessage + ' <b>City:</b> '   + (SELECT COALESCE(inserted.City, '') FROM inserted) + ' <br>'
      SET @BodyMessage = @BodyMessage + ' <b>State:</b> '   + (SELECT COALESCE([State], '') FROM inserted INNER JOIN tblState ON inserted.StateID = tblstate.StateID) + ' <br>'
      SET @BodyMessage = @BodyMessage + ' <b>Zip:</b> '   + (SELECT COALESCE(inserted.ZipCode, '') FROM inserted) + ' <br>'
      SET @BodyMessage = @BodyMessage + ' <b>TaxID:</b> '   + (SELECT COALESCE(inserted.TaxID, '') FROM inserted) + ' <br>'
          






            
SET @BodyMessage = @BodyMessage+ '<br><br>KCIC, LLC (a Kenesis Holdings company)<br>
1100 New York Ave., NW, Suite 650 East<br>
Washington, DC 20005<br>
(t) 202-772-2300<br>
(f) 202-772-2333<br><br>

This email message (and any attached file) from KCIC, LLC is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, printing, copying, or distribution is prohibited. If you are not the intended recipient, or a person respoDemo Clientble for delivering this confidential communication to the intended recipient, please contact the sender by reply email and destroy all copies of the original message.</font>'

--Check for new documents

BEGIN
      --Send email
      EXEC msdb.dbo.sp_send_dbmail
      @profile_name='Admin',
      @recipients = 'dana@kcicllc.com',
      @copy_recipients = '',
      @subject= @subject,
      @body= @BodyMessage,
      @body_Format = 'html'
      END

END

    -- Insert statements for trigger here

GO
ALTER TABLE [dbo].[m_tblExpertFirm] ADD CONSTRAINT [PK_m_tblExpertFirm] PRIMARY KEY CLUSTERED  ([ExpertFirmID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblExpertFirm] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblExpertFirm] TO [base_user]
GRANT ALTER ON  [dbo].[m_tblExpertFirm] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblExpertFirm] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblExpertFirm] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblExpertFirm] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblExpertFirm] TO [Claims_User]
GRANT ALTER ON  [dbo].[m_tblExpertFirm] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblExpertFirm] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblExpertFirm] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblExpertFirm] TO [power_user]
GO
