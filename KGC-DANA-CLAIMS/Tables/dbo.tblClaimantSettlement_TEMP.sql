CREATE TABLE [dbo].[tblClaimantSettlement_TEMP]
(
[ClaimantSettlementID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavClaimResolutionKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavResolutionType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavSettlementGroup] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementGroupID] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementDate] [datetime] NULL,
[SettlementAmount] [money] NULL,
[SettlementDueDate] [datetime] NULL,
[PayToCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaidDate] [datetime] NULL,
[NavCheckAmount] [money] NULL,
[NavAllocationAmount] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantSettlement_TEMP] ADD CONSTRAINT [PK_tblClaimantSettlement_TEMP] PRIMARY KEY CLUSTERED  ([ClaimantSettlementID_ALT]) ON [PRIMARY]
GO
