CREATE TABLE [dbo].[m_tblMDL2010Cases]
(
[MasterID] [int] NULL,
[Number] [int] NULL,
[WorkerLastName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkerFirstName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkerMiddleInitial] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CaseSytle] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferorCourt] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferorCourtCivilActionNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MDLCivilActionNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateCourtCivilActionNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateAbbreviation] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffFirm] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DismissalType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DismissalDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT ALTER ON  [dbo].[m_tblMDL2010Cases] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblMDL2010Cases] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblMDL2010Cases] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblMDL2010Cases] TO [Claims_User]
GO
