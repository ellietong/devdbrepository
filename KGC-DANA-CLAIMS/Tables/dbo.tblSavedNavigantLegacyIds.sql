CREATE TABLE [dbo].[tblSavedNavigantLegacyIds]
(
[SavedNavigantLegacyID] [int] NOT NULL IDENTITY(1, 1),
[NavigantLegacyID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
