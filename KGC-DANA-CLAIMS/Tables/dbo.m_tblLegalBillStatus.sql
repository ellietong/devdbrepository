CREATE TABLE [dbo].[m_tblLegalBillStatus]
(
[LegalBillStatusID] [int] NOT NULL IDENTITY(1, 1),
[LegalBillStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblLegalBillStatus] ADD CONSTRAINT [PK_m_tblLegalBillStatus] PRIMARY KEY CLUSTERED  ([LegalBillStatusID]) ON [PRIMARY]
GO
