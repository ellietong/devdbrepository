CREATE TABLE [dbo].[tblSmoking_TEMP]
(
[SmokingID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmokerStart] [datetime] NULL,
[SmokerEnd] [datetime] NULL,
[PacksPerDay] [float] NULL,
[ArchiveSmokerStart] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArchiveSmokerEnd] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSmoking_TEMP] ADD CONSTRAINT [PK_tblSmoking_TEMP] PRIMARY KEY CLUSTERED  ([SmokingID_ALT]) ON [PRIMARY]
GO
