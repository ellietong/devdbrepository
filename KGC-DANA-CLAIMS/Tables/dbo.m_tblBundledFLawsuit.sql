CREATE TABLE [dbo].[m_tblBundledFLawsuit]
(
[Injured_Party_Id] [float] NULL,
[Claim Sequence_Nbr] [float] NULL,
[Law_Suit_Nbr] [float] NULL,
[Version_Nbr] [float] NULL,
[Docket_Nbr] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff First_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff Last_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plainitff_Middle_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Local_Plaintiff_Nbr] [float] NULL,
[Local_Plaintiff_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[National_Plaintiff_Nbr] [float] NULL,
[National_Plaintiff_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Liaison_Counsel Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Liaison_Counsel_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Trial_Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[File_Date] [datetime] NULL,
[State_Filed] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisdiction Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Jurisidiction Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Court_Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Court Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Court Level Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Lead_Law_Suit_Flag] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff_Type_Cd] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Plaintiff Type Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Law_Suit_Version_Type_Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Law_Suit_Version_Type_Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Law_Suit_QC_Flag] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date Input] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date Received] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QC Input Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Surrogate_Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblBundledFLawsuit] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblBundledFLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblBundledFLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblBundledFLawsuit] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblBundledFLawsuit] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblBundledFLawsuit] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblBundledFLawsuit] TO [power_user]
GO
