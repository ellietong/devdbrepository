CREATE TABLE [dbo].[tblSavedClaimantName]
(
[SavedClaimantNameID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SavedSearchID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedClaimantName] ADD CONSTRAINT [PK_tblSavedClaimantName] PRIMARY KEY CLUSTERED  ([SavedClaimantNameID]) ON [PRIMARY]
GO
