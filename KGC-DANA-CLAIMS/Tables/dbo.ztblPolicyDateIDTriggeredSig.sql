CREATE TABLE [dbo].[ztblPolicyDateIDTriggeredSig]
(
[PolicyID] [int] NULL,
[AllocationID] [int] NULL,
[OriginalOccurrenceID] [int] NULL,
[ColumnID] [int] NULL,
[BlockTriggered] [bit] NULL
) ON [PRIMARY]
GO
