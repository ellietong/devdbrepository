CREATE TABLE [dbo].[m_tblBundledLawsuit]
(
[InjuredPartyID] [float] NULL,
[ClaimSequenceNum] [float] NULL,
[LawsuitNum] [float] NULL,
[VersionNum] [float] NULL,
[DocketNum] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffFirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffLastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffMiddleName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalPlaintiffNum] [float] NULL,
[LocalPlaintiffName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NationalPlaintiffNum] [float] NULL,
[NationalPlaintiffName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LiasonCounselNum] [float] NULL,
[LiasonCounselName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrialDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[StateFiled] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JurisdictionCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JurisdictionDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourtType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourtDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourtLevelCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadLawsuitFlag] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffTypeCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffTypeDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawsuitVersionTypeCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawsuitVersionTypeDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LawsuitQCFlag] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateInput] [datetime] NULL,
[DateReceived] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QCInputDate] [datetime] NULL,
[SurrogateDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblBundledLawsuit] ADD CONSTRAINT [FK_m_tblBundledLawsuit_m_tblBundledInjuredParty] FOREIGN KEY ([InjuredPartyID]) REFERENCES [dbo].[m_tblBundledInjuredParty] ([Injured_Party_Id])
GO
