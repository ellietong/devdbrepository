CREATE TABLE [dbo].[m_tblAfterMarketStatusUpdates]
(
[NavStatus] [int] NULL,
[ClaimLawsuitID] [int] NULL,
[ClaimLawsuitStatusID] [int] NULL,
[Statusdate] [datetime] NULL
) ON [PRIMARY]
GO
