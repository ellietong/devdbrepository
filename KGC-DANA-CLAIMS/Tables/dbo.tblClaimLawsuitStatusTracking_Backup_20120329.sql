CREATE TABLE [dbo].[tblClaimLawsuitStatusTracking_Backup_20120329]
(
[ClaimLawsuitStatusTrackingID] [bigint] NOT NULL,
[ClaimLawsuitID] [bigint] NULL,
[ClaimLawsuitStatusID] [int] NULL,
[StatusDate] [datetime] NULL,
[StatusNotes] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DismissalDate] [datetime] NULL,
[IsPrimaryStatus] [bit] NULL,
[NavStatus] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuitStatusTracking_Backup_20120329] ADD CONSTRAINT [PK_tblClaimLawsuitStatusTracking_Backup_20120329] PRIMARY KEY CLUSTERED  ([ClaimLawsuitStatusTrackingID]) ON [PRIMARY]
GO
