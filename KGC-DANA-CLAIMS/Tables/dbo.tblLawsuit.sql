CREATE TABLE [dbo].[tblLawsuit]
(
[LawsuitID] [bigint] NOT NULL IDENTITY(1, 1),
[CaseCaption] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavFileDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JurisdictionID_OLD] [int] NULL,
[StateID] [int] NULL,
[CourtTypeID] [int] NULL,
[NavServiceDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavReceivedDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffFirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffLastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavActiveTrialDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefenseCounselAddressID] [int] NULL,
[PlaintiffCounselAddressID] [int] NULL,
[PrimaryNationalPlaintiffCounselID] [int] NULL,
[IsThirdPartyComplaint] [bit] NULL,
[NavLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLawsuitID] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavJurisdiction] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavState] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavCourtType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDefenseCounsel] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPlaintiffCounsel] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavNationalPlaintiffCounsel] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [datetime] NULL,
[ServiceDate] [datetime] NULL,
[ReceivedDate] [datetime] NULL,
[ActiveTrialDate_old] [datetime] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavIsMDL] [bit] NULL,
[JurisdictionID] [int] NULL,
[OldPlaintiffCounselAddressID] [int] NULL,
[OldDefenseCounselAddressID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblLawsuit] ADD CONSTRAINT [PK__tblLawsuit1__5A1A5A11] PRIMARY KEY CLUSTERED  ([LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IND_DocketNumber_LawsuitID_CaseCaption] ON [dbo].[tblLawsuit] ([DocketNumber]) INCLUDE ([CaseCaption], [LawsuitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblLawsuit_DocketNumber_StateID] ON [dbo].[tblLawsuit] ([DocketNumber], [StateID]) INCLUDE ([LawsuitID], [PlaintiffCounselAddressID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_PlaintiffCounselAddressID_LawsuitID_FileDate_ServiceDate_ReceivedDate] ON [dbo].[tblLawsuit] ([PlaintiffCounselAddressID]) INCLUDE ([FileDate], [LawsuitID], [ReceivedDate], [ServiceDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tbllawsuit_StateID_DefenseCounselAddressID_DocketNumber] ON [dbo].[tblLawsuit] ([StateID], [DefenseCounselAddressID], [DocketNumber]) INCLUDE ([LawsuitID], [PlaintiffCounselAddressID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblLawsuit_StateID_PlaintiffCounselAddressID_LawsuitID] ON [dbo].[tblLawsuit] ([StateID], [PlaintiffCounselAddressID]) INCLUDE ([LawsuitID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblLawsuit] TO [base_user]
GRANT ALTER ON  [dbo].[tblLawsuit] TO [chaudoinr]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblLawsuit] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblLawsuit] TO [monahanc]
GRANT SELECT ON  [dbo].[tblLawsuit] TO [power_user]
GRANT INSERT ON  [dbo].[tblLawsuit] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLawsuit] TO [power_user]
GRANT ALTER ON  [dbo].[tblLawsuit] TO [prossc]
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'CourtTypeID'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'JurisdictionID_OLD'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'JurisdictionID_OLD'
GO
EXEC sp_addextendedproperty N'MS_DisplayControl', N'109', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'StateID'
GO
EXEC sp_addextendedproperty N'MS_Format', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblLawsuit', 'COLUMN', N'StateID'
GO
