CREATE TABLE [dbo].[tblPaymentSettlement_TEMP]
(
[PaymentSettlementID_ALT] [int] NOT NULL IDENTITY(1, 1),
[NavClaimResolutionKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CheckNumber] [int] NULL,
[PaidDate] [datetime] NULL,
[CheckAmount] [money] NULL,
[AllocationAmount] [money] NULL,
[ClaimantSettlementID] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPaymentSettlement_TEMP] ADD CONSTRAINT [PK_tblPaymentSettlement_TEMP] PRIMARY KEY CLUSTERED  ([PaymentSettlementID_ALT]) ON [PRIMARY]
GO
