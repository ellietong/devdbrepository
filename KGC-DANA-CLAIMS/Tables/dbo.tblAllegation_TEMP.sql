CREATE TABLE [dbo].[tblAllegation_TEMP]
(
[AllegationID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavClaimLawsuitKey] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDefendantCompany] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavAllegation] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDamage] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavMaterial] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimLawsuitID] [int] NULL,
[DefendantCompanyID] [int] NULL,
[AllegationTypeID] [int] NULL,
[DamageID] [int] NULL,
[MaterialID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllegation_TEMP] ADD CONSTRAINT [PK_tblAllegation_TEMP] PRIMARY KEY CLUSTERED  ([AllegationID_ALT]) ON [PRIMARY]
GO
