CREATE TABLE [dbo].[tblFollowsForm]
(
[FFID] [int] NOT NULL IDENTITY(1, 1),
[PolicyDateID] [int] NOT NULL,
[FFPolicyDateID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFollowsForm] ADD CONSTRAINT [PK_tblFollowsForm] PRIMARY KEY NONCLUSTERED  ([FFID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFollowsForm] ADD CONSTRAINT [FK_tblFollowsForm_tblPolicyDates] FOREIGN KEY ([PolicyDateID]) REFERENCES [dbo].[tblPolicyDates_Archive] ([PolicyDateID]) ON DELETE CASCADE
GO
GRANT SELECT ON  [dbo].[tblFollowsForm] TO [base_user]
GRANT SELECT ON  [dbo].[tblFollowsForm] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblFollowsForm] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblFollowsForm] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblFollowsForm] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblFollowsForm] TO [power_user]
GRANT INSERT ON  [dbo].[tblFollowsForm] TO [power_user]
GRANT UPDATE ON  [dbo].[tblFollowsForm] TO [power_user]
GO
