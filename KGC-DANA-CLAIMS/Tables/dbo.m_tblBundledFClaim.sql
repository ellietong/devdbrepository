CREATE TABLE [dbo].[m_tblBundledFClaim]
(
[Injured_Party_Id] [float] NULL,
[Claim_Sequence_Nbr] [float] NULL,
[Claim_Status_Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claim_Status_Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First_Diagnosis_Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First_Exposure_Date] [datetime] NULL,
[Last_Exposure_Date] [datetime] NULL,
[Diagnosis Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diagnosis Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Settlement_Amt] [float] NULL,
[Settlement_Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Release_Received_Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Release_Type] [float] NULL,
[Release_Type Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Input_Date] [datetime] NULL,
[Settlement_Group] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Claim_QC_Flag] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Dynamic Category Code] [float] NULL,
[Dynamic Category Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image Status Number] [float] NULL,
[Image Status Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Box Number] [float] NULL,
[Box Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Roll Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Frame Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISR Input Date] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblBundledFClaim] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblBundledFClaim] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblBundledFClaim] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblBundledFClaim] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblBundledFClaim] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblBundledFClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblBundledFClaim] TO [power_user]
GO
