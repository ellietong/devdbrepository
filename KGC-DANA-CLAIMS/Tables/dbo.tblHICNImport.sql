CREATE TABLE [dbo].[tblHICNImport]
(
[HICNImportID] [int] NOT NULL IDENTITY(1, 1),
[CMSQueryDate] [datetime] NULL,
[HICN] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedicareBeneficiaryStatusID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblHICNImport] ADD CONSTRAINT [PK_tblHICNImport] PRIMARY KEY CLUSTERED  ([HICNImportID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblHICNImport] TO [base_user]
GRANT SELECT ON  [dbo].[tblHICNImport] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblHICNImport] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblHICNImport] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblHICNImport] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblHICNImport] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblHICNImport] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblHICNImport] TO [power_user]
GRANT SELECT ON  [dbo].[tblHICNImport] TO [power_user]
GRANT INSERT ON  [dbo].[tblHICNImport] TO [power_user]
GRANT UPDATE ON  [dbo].[tblHICNImport] TO [power_user]
GO
