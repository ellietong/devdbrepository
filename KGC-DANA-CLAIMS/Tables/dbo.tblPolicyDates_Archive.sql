CREATE TABLE [dbo].[tblPolicyDates_Archive]
(
[PolicyDateID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NOT NULL,
[PStartDate] [datetime] NOT NULL,
[PEndDate] [datetime] NOT NULL,
[ActualPStartDate] [datetime] NOT NULL,
[ActualPEndDate] [datetime] NOT NULL,
[AnnualPeriod] [int] NULL,
[OldPolicyID] [int] NULL,
[OldPolicyDateID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyDates_Archive] ADD CONSTRAINT [PK_tblPolicyDates] PRIMARY KEY CLUSTERED  ([PolicyDateID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblPolicyDates_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyDates_Archive] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblPolicyDates_Archive] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblPolicyDates_Archive] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblPolicyDates_Archive] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblPolicyDates_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicyDates_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicyDates_Archive] TO [power_user]
GO
