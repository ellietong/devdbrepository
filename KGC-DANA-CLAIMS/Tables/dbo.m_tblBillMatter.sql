CREATE TABLE [dbo].[m_tblBillMatter]
(
[BillMatterID] [int] NOT NULL IDENTITY(1, 1),
[BillMatter] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblBillMatter] ADD CONSTRAINT [PK_m_tblBillMatter] PRIMARY KEY CLUSTERED  ([BillMatterID]) ON [PRIMARY]
GO
