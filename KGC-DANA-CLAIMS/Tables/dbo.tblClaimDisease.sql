CREATE TABLE [dbo].[tblClaimDisease]
(
[ClaimDiseaseID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimID] [bigint] NULL,
[DiseaseID] [int] NULL,
[PrimaryDisease] [bit] NULL,
[DiseaseDiagnosisDate] [datetime] NULL,
[DoctorID] [int] NULL,
[NavDisease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiseaseID_OLD] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimDisease] ADD CONSTRAINT [PK_tblClaimDisease] PRIMARY KEY CLUSTERED  ([ClaimDiseaseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_ClaimID_PrimaryDisease] ON [dbo].[tblClaimDisease] ([ClaimID], [PrimaryDisease]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [base_user]
GRANT ALTER ON  [dbo].[tblClaimDisease] TO [chaudoinr]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimDisease] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblClaimDisease] TO [monahanc]
GRANT SELECT ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimDisease] TO [power_user]
GRANT ALTER ON  [dbo].[tblClaimDisease] TO [prossc]
GO
