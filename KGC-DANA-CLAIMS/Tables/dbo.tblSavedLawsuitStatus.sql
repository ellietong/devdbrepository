CREATE TABLE [dbo].[tblSavedLawsuitStatus]
(
[SavedLawsuitStatusID] [bigint] NOT NULL IDENTITY(1, 1),
[LawsuitStatusID] [int] NULL,
[SavedSearchID] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedLawsuitStatus] ADD CONSTRAINT [PK_tblSavedLawsuitStatus] PRIMARY KEY CLUSTERED  ([SavedLawsuitStatusID]) ON [PRIMARY]
GO
