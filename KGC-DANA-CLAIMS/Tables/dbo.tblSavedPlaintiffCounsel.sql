CREATE TABLE [dbo].[tblSavedPlaintiffCounsel]
(
[SavedPlaintiffCounselID] [bigint] NOT NULL IDENTITY(1, 1),
[PlaintiffCounselAddressID] [int] NULL,
[SavedSearchID] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedPlaintiffCounsel] ADD CONSTRAINT [PK_tblSavedPlaintiffCounsel] PRIMARY KEY CLUSTERED  ([SavedPlaintiffCounselID]) ON [PRIMARY]
GO
