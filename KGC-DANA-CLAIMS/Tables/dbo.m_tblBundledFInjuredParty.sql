CREATE TABLE [dbo].[m_tblBundledFInjuredParty]
(
[Injured_Party_Id] [float] NULL,
[First_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Middle_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SS_Nbr] [float] NULL,
[Date of Birth] [datetime] NULL,
[Date of Death] [datetime] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[m_tblBundledFInjuredParty] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblBundledFInjuredParty] TO [Claims_User]
GRANT INSERT ON  [dbo].[m_tblBundledFInjuredParty] TO [Claims_User]
GRANT UPDATE ON  [dbo].[m_tblBundledFInjuredParty] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_tblBundledFInjuredParty] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblBundledFInjuredParty] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblBundledFInjuredParty] TO [power_user]
GO
