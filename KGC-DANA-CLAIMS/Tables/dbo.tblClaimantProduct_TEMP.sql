CREATE TABLE [dbo].[tblClaimantProduct_TEMP]
(
[ClaimantProductID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductID] [int] NULL,
[NavProductName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductNameID] [int] NULL,
[NavBrand] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductBrandID] [int] NULL,
[NavProductType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductTypeID] [int] NULL,
[NavManufacturer] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductManufacturerID] [int] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[ArchiveStartDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantProduct_TEMP] ADD CONSTRAINT [PK_tblClaimantProduct_TEMP] PRIMARY KEY CLUSTERED  ([ClaimantProductID_ALT]) ON [PRIMARY]
GO
