CREATE TABLE [dbo].[tbl_ClaimToInjuredParty_ThroughMarch2011]
(
[ProducerID] [int] NULL,
[MonthlyBillID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimID] [int] NULL,
[InjuredPartyID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaimCategory] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IP_KEY] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tbl_ClaimToInjuredParty_ThroughMarch2011] TO [base_user]
GRANT SELECT ON  [dbo].[tbl_ClaimToInjuredParty_ThroughMarch2011] TO [Claims_User]
GRANT INSERT ON  [dbo].[tbl_ClaimToInjuredParty_ThroughMarch2011] TO [Claims_User]
GRANT DELETE ON  [dbo].[tbl_ClaimToInjuredParty_ThroughMarch2011] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tbl_ClaimToInjuredParty_ThroughMarch2011] TO [Claims_User]
GRANT SELECT ON  [dbo].[tbl_ClaimToInjuredParty_ThroughMarch2011] TO [power_user]
GRANT INSERT ON  [dbo].[tbl_ClaimToInjuredParty_ThroughMarch2011] TO [power_user]
GRANT UPDATE ON  [dbo].[tbl_ClaimToInjuredParty_ThroughMarch2011] TO [power_user]
GO
