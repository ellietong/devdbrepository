CREATE TABLE [dbo].[tblDefenseJurisdiction]
(
[DefenseJurisdictionID] [int] NOT NULL IDENTITY(1, 1),
[JurisdictionID] [int] NOT NULL,
[DefenseCounselAddressID] [int] NOT NULL,
[CourtTypeID] [int] NOT NULL,
[MatterID] [int] NULL,
[StateID] [int] NOT NULL,
[IsActive] [bit] NOT NULL,
[MatterEntityID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDefenseJurisdiction] ADD CONSTRAINT [PK_tblDefenseJurisdiction] PRIMARY KEY CLUSTERED  ([DefenseJurisdictionID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [base_user]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblDefenseJurisdiction] TO [Claims_User]
GRANT SELECT ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GRANT INSERT ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GRANT DELETE ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GRANT UPDATE ON  [dbo].[tblDefenseJurisdiction] TO [power_user]
GO
