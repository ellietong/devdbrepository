CREATE TABLE [dbo].[tblClaim]
(
[ClaimID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[IsCurrentClaim] [bit] NULL,
[ClaimSequenceID] [int] NULL,
[FullyDocumented] [bit] NULL,
[CauseOfInjuryID] [int] NULL,
[MatterID] [int] NULL,
[ILODoctorID] [int] NULL,
[ILOProfusion] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILODate] [datetime] NULL,
[PFTDoctorID] [int] NULL,
[PFTDate] [datetime] NULL,
[PFTTypeID] [int] NULL,
[PFTFVC_Actual] [float] NULL,
[PFTFVC_Predicted] [float] NULL,
[PFTFEV1_Actual] [float] NULL,
[PFTFEV1_Predicted] [float] NULL,
[PFTFEV1AndFVC_Actual] [float] NULL,
[PFTFEV1AndFVC_Predicted] [float] NULL,
[PFTTLC_Actual] [float] NULL,
[PFTTLC_Predicted] [float] NULL,
[PFTDLCO_Actual] [float] NULL,
[PFTDLCO_Predicted] [float] NULL,
[PFTTV] [float] NULL,
[NavClaimKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimSequence] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPlaintiffDisease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavDefenseDisease] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavMatterID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPlaintiffDiseaseDate] [datetime] NULL,
[NavDefenseDiseaseDate] [datetime] NULL,
[NavPFTType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavILODoctor] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavPFTDoctor] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [PK_mBtblClaim] PRIMARY KEY CLUSTERED  ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaim_ClaimantPersonalInfoID_MatterID] ON [dbo].[tblClaim] ([ClaimantPersonalInfoID], [MatterID]) INCLUDE ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [INDX_tblClaim_MatterID] ON [dbo].[tblClaim] ([MatterID]) INCLUDE ([ClaimantPersonalInfoID], [ClaimID], [ClaimSequenceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaim] ADD CONSTRAINT [FK_tblClaim_tblClaimantPersonalInfo] FOREIGN KEY ([ClaimantPersonalInfoID]) REFERENCES [dbo].[tblClaimantPersonalInfo] ([ClaimantPersonalInfoID])
GO
GRANT SELECT ON  [dbo].[tblClaim] TO [base_user]
GRANT ALTER ON  [dbo].[tblClaim] TO [chaudoinr]
GRANT SELECT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaim] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaim] TO [Claims_User]
GRANT ALTER ON  [dbo].[tblClaim] TO [monahanc]
GRANT SELECT ON  [dbo].[tblClaim] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaim] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaim] TO [power_user]
GRANT ALTER ON  [dbo].[tblClaim] TO [prossc]
GO
