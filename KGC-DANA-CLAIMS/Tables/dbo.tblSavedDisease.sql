CREATE TABLE [dbo].[tblSavedDisease]
(
[SavedDiseaseID] [bigint] NOT NULL IDENTITY(1, 1),
[DiseaseID] [int] NOT NULL,
[SavedSearchID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedDisease] ADD CONSTRAINT [PK_tblSavedDiseaseSearch_Claims] PRIMARY KEY CLUSTERED  ([SavedDiseaseID]) ON [PRIMARY]
GO
