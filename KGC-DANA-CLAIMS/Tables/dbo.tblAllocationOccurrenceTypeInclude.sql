CREATE TABLE [dbo].[tblAllocationOccurrenceTypeInclude]
(
[OccurrenceTypeIncludeID] [int] NOT NULL IDENTITY(1, 1),
[AllocationID] [int] NULL,
[OccurrenceTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationOccurrenceTypeInclude] ADD CONSTRAINT [PK_tblAllocationOccurrenceTypeExclude] PRIMARY KEY NONCLUSTERED  ([OccurrenceTypeIncludeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAllocationOccurrenceTypeInclude] ADD CONSTRAINT [FK_tblAllocationOccurrenceTypeInclude_tblAllocation] FOREIGN KEY ([AllocationID]) REFERENCES [dbo].[tblAllocation_Archive] ([AllocationID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [base_user]
GRANT SELECT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [power_user]
GRANT INSERT ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAllocationOccurrenceTypeInclude] TO [power_user]
GO
DECLARE @xp tinyint
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', N'[tblAllocationOccurrenceTypeInclude].[OccurrenceTypeID], [tblAllocationOccurrenceTypeInclude].[OccurrenceTypeIncludeID]', 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', NULL, NULL
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', NULL, NULL
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'AllocationID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'AllocationID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'AllocationID'
GO
DECLARE @xp smallint
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'AllocationID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'AllocationID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp smallint
SELECT @xp=2115
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'OccurrenceTypeID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'OccurrenceTypeIncludeID'
GO
DECLARE @xp bit
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'OccurrenceTypeIncludeID'
GO
DECLARE @xp smallint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'OccurrenceTypeIncludeID'
GO
DECLARE @xp smallint
SELECT @xp=2805
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'OccurrenceTypeIncludeID'
GO
DECLARE @xp tinyint
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblAllocationOccurrenceTypeInclude', 'COLUMN', N'OccurrenceTypeIncludeID'
GO
