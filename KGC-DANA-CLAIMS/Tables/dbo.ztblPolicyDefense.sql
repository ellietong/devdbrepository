CREATE TABLE [dbo].[ztblPolicyDefense]
(
[PolicyDefenseID] [int] NOT NULL IDENTITY(1, 1),
[PolicyID] [int] NOT NULL,
[AllocationID] [int] NOT NULL,
[ScenarioID] [int] NOT NULL,
[Provision1] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblPolicyDefense] ADD CONSTRAINT [PK_ztblPolicyDefense] PRIMARY KEY CLUSTERED  ([PolicyDefenseID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyDefense] TO [Allocation_User]
GRANT SELECT ON  [dbo].[ztblPolicyDefense] TO [Allocation_User]
GRANT INSERT ON  [dbo].[ztblPolicyDefense] TO [Allocation_User]
GRANT DELETE ON  [dbo].[ztblPolicyDefense] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[ztblPolicyDefense] TO [Allocation_User]
GRANT SELECT ON  [dbo].[ztblPolicyDefense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblPolicyDefense] TO [power_user]
GRANT SELECT ON  [dbo].[ztblPolicyDefense] TO [power_user]
GO
