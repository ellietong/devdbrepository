CREATE TABLE [dbo].[tblOccurrenceGroup_Archive]
(
[OccurrenceGroupID] [int] NOT NULL IDENTITY(1, 1),
[OccurrenceGroup] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OccGrpTriggerStart] [datetime] NULL,
[OccGrpDiscount] [real] NULL,
[SiteID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblOccurrenceGroup_Archive] ADD CONSTRAINT [PK_tblOccurrenceGroup] PRIMARY KEY CLUSTERED  ([OccurrenceGroupID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[tblOccurrenceGroup_Archive] TO [base_user]
GRANT SELECT ON  [dbo].[tblOccurrenceGroup_Archive] TO [Occurrence_User]
GRANT INSERT ON  [dbo].[tblOccurrenceGroup_Archive] TO [Occurrence_User]
GRANT DELETE ON  [dbo].[tblOccurrenceGroup_Archive] TO [Occurrence_User]
GRANT UPDATE ON  [dbo].[tblOccurrenceGroup_Archive] TO [Occurrence_User]
GRANT SELECT ON  [dbo].[tblOccurrenceGroup_Archive] TO [power_user]
GRANT INSERT ON  [dbo].[tblOccurrenceGroup_Archive] TO [power_user]
GRANT UPDATE ON  [dbo].[tblOccurrenceGroup_Archive] TO [power_user]
GO
