CREATE TABLE [dbo].[tblClaimLawsuit_TEMP]
(
[ClaimLawsuitID_ALT] [bigint] NOT NULL IDENTITY(1, 1),
[NavClaimKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavClaimLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavLawsuitKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavIsPrimary] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavStatusDate] [datetime] NULL,
[IndividualDocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPrimary] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimLawsuit_TEMP] ADD CONSTRAINT [PK_tblClaimLawsuit_TEMP] PRIMARY KEY CLUSTERED  ([ClaimLawsuitID_ALT]) ON [PRIMARY]
GO
