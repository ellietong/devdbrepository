CREATE TABLE [dbo].[zzzAnonymize_tblClaimLawsuitProduct]
(
[ClaimLawsuitProductID] [bigint] NOT NULL IDENTITY(1, 1),
[ProductID] [int] NULL,
[StartDate] [date] NULL,
[EndDate] [date] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[zzzAnonymize_tblClaimLawsuitProduct] ADD CONSTRAINT [PK_zzzAnonymize_tblClaimLawsuitProduct] PRIMARY KEY CLUSTERED  ([ClaimLawsuitProductID]) ON [PRIMARY]
GO
