CREATE TABLE [dbo].[tblClaimantSmokingHistory]
(
[ClaimantSmokingHistoryID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantPersonalInfoID] [bigint] NULL,
[SmokingStart] [date] NULL,
[SmokingEnd] [date] NULL,
[PacksPerDay] [float] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmokingHistoryNotes] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblClaimantSmokingHistory_Del] ON [dbo].[tblClaimantSmokingHistory] FOR DELETE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\shockleym on March 16, 2014 11:45 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int
  SELECT @RowsAffected = COUNT(*) FROM DELETED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    -- Populate audit detail records...
    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      133,
      0,
      CONVERT(NVARCHAR, [ClaimantPersonalInfoID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      134,
      0,
      CONVERT(NVARCHAR, [ClaimantSmokingHistoryID])
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      135,
      0,
      CONVERT( NVARCHAR( 100 ), [PacksPerDay], 2 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      136,
      0,
      CONVERT( NVARCHAR( 34 ), [SmokingEnd], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      137,
      0,
      CONVERT( NVARCHAR( 34 ), [SmokingStart], 121 )
    FROM
      DELETED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      138,
      8,
      CONVERT( NVARCHAR(max), [SPUsername] )
    FROM
      DELETED

    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 9, @RowsAffected, 'D', @AuditedUser
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblClaimantSmokingHistory_Del]', 'last', 'delete', null
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblClaimantSmokingHistory_Ins] ON [dbo].[tblClaimantSmokingHistory] FOR INSERT AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\shockleym on March 16, 2014 11:45 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int
  SELECT @RowsAffected = COUNT(*) FROM INSERTED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    -- Populate audit detail records...
    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, NewValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      133,
      0,
      CONVERT(NVARCHAR, [ClaimantPersonalInfoID])
    FROM
      INSERTED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, NewValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      134,
      0,
      CONVERT(NVARCHAR, [ClaimantSmokingHistoryID])
    FROM
      INSERTED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, NewValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      135,
      0,
      CONVERT( NVARCHAR( 100 ), [PacksPerDay], 2 )
    FROM
      INSERTED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, NewValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      136,
      0,
      CONVERT( NVARCHAR( 34 ), [SmokingEnd], 121 )
    FROM
      INSERTED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, NewValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      137,
      0,
      CONVERT( NVARCHAR( 34 ), [SmokingStart], 121 )
    FROM
      INSERTED

    INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, NewValue )
    SELECT
      @AuditID,
      'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, [ClaimantSmokingHistoryID]),
      138,
      8,
      CONVERT( NVARCHAR(max), [SPUsername] )
    FROM
      INSERTED

    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 9, @RowsAffected, 'I', @AuditedUser
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblClaimantSmokingHistory_Ins]', 'last', 'insert', null
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_kr_audit_tblClaimantSmokingHistory_Upd] ON [dbo].[tblClaimantSmokingHistory] FOR UPDATE AS
BEGIN

  -- OmniAudit 1.14.0.495
  -- Copyright © 2001-2011 Krell Software, Inc.
  -- www.krell-software.com
  -- 
  -- Generated by KCICLLC\shockleym on March 16, 2014 11:45 PM

  SET NOCOUNT ON

  DECLARE @ValidRun INT, @RowsAffected INT, @PreExistingID INT, @MaxID INT, @AuditedUser sysname, @AuditID int, @ChangeCount INT, @CaptureCount INT
  SELECT @RowsAffected = COUNT(*) FROM INSERTED


  IF( @RowsAffected > 0 )
  BEGIN
    SELECT @AuditedUser = NULL

    EXEC @ValidRun = dbo.pr_kr_AuditLogHeader 'C6181EEE-5254-4DF1-89B5-1FBAE386970E', @AuditID OUTPUT
    IF( @ValidRun <> 0 )
      RETURN

    SELECT @CaptureCount = 0

    -- Test for a key update...
    SELECT @ChangeCount = COUNT(*)
    FROM
      INSERTED i
      FULL OUTER JOIN DELETED d ON ( d.[ClaimantSmokingHistoryID] = i.[ClaimantSmokingHistoryID] )

    -- Populate audit detail records...
    IF( UPDATE( [ClaimantPersonalInfoID] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimantSmokingHistoryID], d.[ClaimantSmokingHistoryID] )),
        133,
        0,
        CONVERT(NVARCHAR, d.[ClaimantPersonalInfoID]),
        CONVERT(NVARCHAR, i.[ClaimantPersonalInfoID])
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimantSmokingHistoryID] = i.[ClaimantSmokingHistoryID] )
      WHERE
          ( d.[ClaimantPersonalInfoID] <> i.[ClaimantPersonalInfoID] ) OR
        ( d.[ClaimantPersonalInfoID] IS NULL AND i.[ClaimantPersonalInfoID] IS NOT NULL ) OR
        ( i.[ClaimantPersonalInfoID] IS NULL AND d.[ClaimantPersonalInfoID] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [ClaimantSmokingHistoryID] ) )
    BEGIN
      -- Audit key column updates...
      IF( @ChangeCount > @RowsAffected )
      BEGIN
        INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
        SELECT
          @AuditID,
          'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimantSmokingHistoryID], d.[ClaimantSmokingHistoryID] )),
          134,
          0,
          CONVERT(NVARCHAR, d.[ClaimantSmokingHistoryID]),
          CONVERT(NVARCHAR, i.[ClaimantSmokingHistoryID])
        FROM
          INSERTED i
          FULL OUTER JOIN DELETED d ON ( d.[ClaimantSmokingHistoryID] = i.[ClaimantSmokingHistoryID] )
        WHERE
          ( d.[ClaimantSmokingHistoryID] <> i.[ClaimantSmokingHistoryID] ) OR
          ( d.[ClaimantSmokingHistoryID] IS NULL AND i.[ClaimantSmokingHistoryID] IS NOT NULL ) OR
          ( i.[ClaimantSmokingHistoryID] IS NULL AND d.[ClaimantSmokingHistoryID] IS NOT NULL )
        SELECT @CaptureCount = @CaptureCount + @@RowCount
      END
    END

    IF( UPDATE( [PacksPerDay] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimantSmokingHistoryID], d.[ClaimantSmokingHistoryID] )),
        135,
        0,
        CONVERT( NVARCHAR( 100 ), d.[PacksPerDay], 2 ),
        CONVERT( NVARCHAR( 100 ), i.[PacksPerDay], 2 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimantSmokingHistoryID] = i.[ClaimantSmokingHistoryID] )
      WHERE
          ( d.[PacksPerDay] <> i.[PacksPerDay] ) OR
        ( d.[PacksPerDay] IS NULL AND i.[PacksPerDay] IS NOT NULL ) OR
        ( i.[PacksPerDay] IS NULL AND d.[PacksPerDay] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [SmokingEnd] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimantSmokingHistoryID], d.[ClaimantSmokingHistoryID] )),
        136,
        0,
        CONVERT( NVARCHAR( 34 ), d.[SmokingEnd], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[SmokingEnd], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimantSmokingHistoryID] = i.[ClaimantSmokingHistoryID] )
      WHERE
          ( d.[SmokingEnd] <> i.[SmokingEnd] ) OR
        ( d.[SmokingEnd] IS NULL AND i.[SmokingEnd] IS NOT NULL ) OR
        ( i.[SmokingEnd] IS NULL AND d.[SmokingEnd] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [SmokingStart] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimantSmokingHistoryID], d.[ClaimantSmokingHistoryID] )),
        137,
        0,
        CONVERT( NVARCHAR( 34 ), d.[SmokingStart], 121 ),
        CONVERT( NVARCHAR( 34 ), i.[SmokingStart], 121 )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimantSmokingHistoryID] = i.[ClaimantSmokingHistoryID] )
      WHERE
          ( d.[SmokingStart] <> i.[SmokingStart] ) OR
        ( d.[SmokingStart] IS NULL AND i.[SmokingStart] IS NOT NULL ) OR
        ( i.[SmokingStart] IS NULL AND d.[SmokingStart] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    IF( UPDATE( [SPUsername] ) )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, OldValue, NewValue )
      SELECT
        @AuditID,
        'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimantSmokingHistoryID], d.[ClaimantSmokingHistoryID] )),
        138,
        0,
        CONVERT( NVARCHAR(max), d.[SPUsername] ),
        CONVERT( NVARCHAR(max), i.[SPUsername] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimantSmokingHistoryID] = i.[ClaimantSmokingHistoryID] )
      WHERE
          ( d.[SPUsername] <> i.[SPUsername] ) OR
        ( d.[SPUsername] IS NULL AND i.[SPUsername] IS NOT NULL ) OR
        ( i.[SPUsername] IS NULL AND d.[SPUsername] IS NOT NULL )

      SELECT @CaptureCount = @CaptureCount + @@RowCount
    END

    -- Handle capture values that were not already caught by auditing
    IF( @CaptureCount > 0 )
    BEGIN
      INSERT dbo.AuditLogDetail( AuditLogID, RowKey, ColumnID, Status, NewValue )
      SELECT
        @AuditID,
        'ClaimantSmokingHistoryID=' + CONVERT(VARCHAR, ISNULL( i.[ClaimantSmokingHistoryID], d.[ClaimantSmokingHistoryID] )),
        138,
        8,
        CONVERT( NVARCHAR(max), i.[SPUsername] )
      FROM
        INSERTED i
        FULL OUTER JOIN DELETED d ON ( d.[ClaimantSmokingHistoryID] = i.[ClaimantSmokingHistoryID] )
      WHERE
        ( d.[SPUsername] = i.[SPUsername] ) OR
        ( ( d.[SPUsername] IS NULL ) AND ( i.[SPUsername] IS NULL ) )
    END

    IF( @CaptureCount > 0 )
    begin
    EXEC dbo.pr_kr_AuditLogTrailer @AuditID, 9, @RowsAffected, 'U', @AuditedUser
    end
  END  -- IF( @RowsAffected > 0 )
END
GO
EXEC sp_settriggerorder N'[dbo].[tr_kr_audit_tblClaimantSmokingHistory_Upd]', 'last', 'update', null
GO
ALTER TABLE [dbo].[tblClaimantSmokingHistory] ADD CONSTRAINT [PK_tblClaimantSmokingHistory] PRIMARY KEY CLUSTERED  ([ClaimantSmokingHistoryID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSmokingHistory] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantSmokingHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSmokingHistory] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimantSmokingHistory] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimantSmokingHistory] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimantSmokingHistory] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimantSmokingHistory] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantSmokingHistory] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantSmokingHistory] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantSmokingHistory] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantSmokingHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantSmokingHistory] TO [power_user]
GO
