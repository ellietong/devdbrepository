CREATE TABLE [dbo].[ztblColumnLayer]
(
[ColumnLayerID] [int] NOT NULL IDENTITY(1, 1),
[SigLevelColumnID] [int] NOT NULL,
[Layer] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblColumnLayer] ADD CONSTRAINT [PK_ztblColumnLayer] PRIMARY KEY CLUSTERED  ([ColumnLayerID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblColumnLayer] TO [base_user]
GRANT SELECT ON  [dbo].[ztblColumnLayer] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblColumnLayer] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ztblColumnLayer] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ztblColumnLayer] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ztblColumnLayer] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ztblColumnLayer] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ztblColumnLayer] TO [power_user]
GRANT SELECT ON  [dbo].[ztblColumnLayer] TO [power_user]
GRANT INSERT ON  [dbo].[ztblColumnLayer] TO [power_user]
GRANT DELETE ON  [dbo].[ztblColumnLayer] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblColumnLayer] TO [power_user]
GO
