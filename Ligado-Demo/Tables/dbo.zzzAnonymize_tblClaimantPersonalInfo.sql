CREATE TABLE [dbo].[zzzAnonymize_tblClaimantPersonalInfo]
(
[ClaimantPersonalInfoID] [bigint] NOT NULL IDENTITY(1, 1),
[FirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SocialSecurityNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zzzAnonymize_tblClaimantPersonalInfo] ADD CONSTRAINT [PK_zzzAnonymize_tblClaimantPersonalInfo] PRIMARY KEY CLUSTERED  ([ClaimantPersonalInfoID]) ON [PRIMARY]
GO
