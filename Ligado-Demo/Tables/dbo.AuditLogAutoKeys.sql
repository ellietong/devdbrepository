CREATE TABLE [dbo].[AuditLogAutoKeys]
(
[TableName] [char] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[KeyValue] [int] NOT NULL CONSTRAINT [DF_AuditLogAutoKeys_KeyValue] DEFAULT ((0)),
[Filler] [char] (6000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_AuditLogAutoKeys_Filler] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditLogAutoKeys] ADD CONSTRAINT [PK_AuditLogAutoKeys] PRIMARY KEY CLUSTERED  ([TableName]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditLogAutoKeys] TO [base_user]
GRANT SELECT ON  [dbo].[AuditLogAutoKeys] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditLogAutoKeys] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditLogAutoKeys] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditLogAutoKeys] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditLogAutoKeys] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditLogAutoKeys] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditLogAutoKeys] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLogAutoKeys] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditLogAutoKeys] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditLogAutoKeys] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditLogAutoKeys] TO [omniaudit_admin]
GRANT VIEW DEFINITION ON  [dbo].[AuditLogAutoKeys] TO [power_user]
GRANT SELECT ON  [dbo].[AuditLogAutoKeys] TO [power_user]
GRANT INSERT ON  [dbo].[AuditLogAutoKeys] TO [power_user]
GRANT DELETE ON  [dbo].[AuditLogAutoKeys] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditLogAutoKeys] TO [power_user]
GO
