CREATE TABLE [dbo].[tblPayer]
(
[PayerID] [int] NOT NULL IDENTITY(1, 1),
[Payer] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPayer] ADD CONSTRAINT [PK_mEtblPayer] PRIMARY KEY CLUSTERED  ([PayerID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPayer] TO [base_user]
GRANT SELECT ON  [dbo].[tblPayer] TO [base_user]
GRANT SELECT ON  [dbo].[tblPayer] TO [Enviro_User]
GRANT INSERT ON  [dbo].[tblPayer] TO [Enviro_User]
GRANT DELETE ON  [dbo].[tblPayer] TO [Enviro_User]
GRANT UPDATE ON  [dbo].[tblPayer] TO [Enviro_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPayer] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPayer] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPayer] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPayer] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPayer] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPayer] TO [power_user]
GRANT SELECT ON  [dbo].[tblPayer] TO [power_user]
GRANT INSERT ON  [dbo].[tblPayer] TO [power_user]
GRANT DELETE ON  [dbo].[tblPayer] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPayer] TO [power_user]
GO
