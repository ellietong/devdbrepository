CREATE TABLE [dbo].[tblGeneralDocumentGroup]
(
[GeneralDocumentGroupID] [int] NOT NULL IDENTITY(1, 1),
[GroupName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblGeneralDocumentGroup] ADD CONSTRAINT [PK_tblGeneralDocumentGroup] PRIMARY KEY CLUSTERED  ([GeneralDocumentGroupID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblGeneralDocumentGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblGeneralDocumentGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblGeneralDocumentGroup] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblGeneralDocumentGroup] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblGeneralDocumentGroup] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblGeneralDocumentGroup] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblGeneralDocumentGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblGeneralDocumentGroup] TO [power_user]
GRANT SELECT ON  [dbo].[tblGeneralDocumentGroup] TO [power_user]
GRANT INSERT ON  [dbo].[tblGeneralDocumentGroup] TO [power_user]
GRANT DELETE ON  [dbo].[tblGeneralDocumentGroup] TO [power_user]
GRANT UPDATE ON  [dbo].[tblGeneralDocumentGroup] TO [power_user]
GO
