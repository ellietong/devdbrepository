CREATE TABLE [dbo].[m_tblOccupationLinking]
(
[OldOccupationID] [int] NULL,
[UpdatedOccupationID] [int] NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblOccupationLinking] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblOccupationLinking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblOccupationLinking] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblOccupationLinking] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblOccupationLinking] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblOccupationLinking] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblOccupationLinking] TO [Ligado_User]
GRANT ALTER ON  [dbo].[m_tblOccupationLinking] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblOccupationLinking] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblOccupationLinking] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblOccupationLinking] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblOccupationLinking] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblOccupationLinking] TO [power_user]
GO
