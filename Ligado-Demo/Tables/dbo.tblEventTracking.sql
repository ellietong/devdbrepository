CREATE TABLE [dbo].[tblEventTracking]
(
[EventTrackingID] [int] NOT NULL IDENTITY(1, 1),
[ClaimLawsuitID] [int] NULL,
[EventTypeID] [int] NULL,
[EventDate] [datetime] NULL,
[EventDescription] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Username] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEventTracking] ADD CONSTRAINT [PK_tblEventTracking] PRIMARY KEY CLUSTERED  ([EventTrackingID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblEventTracking] TO [base_user]
GRANT SELECT ON  [dbo].[tblEventTracking] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblEventTracking] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblEventTracking] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblEventTracking] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblEventTracking] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblEventTracking] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblEventTracking] TO [power_user]
GRANT SELECT ON  [dbo].[tblEventTracking] TO [power_user]
GRANT INSERT ON  [dbo].[tblEventTracking] TO [power_user]
GRANT DELETE ON  [dbo].[tblEventTracking] TO [power_user]
GRANT UPDATE ON  [dbo].[tblEventTracking] TO [power_user]
GO
