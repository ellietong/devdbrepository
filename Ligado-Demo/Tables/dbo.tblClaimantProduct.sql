CREATE TABLE [dbo].[tblClaimantProduct]
(
[ClaimantProductID] [bigint] NOT NULL IDENTITY(1, 1),
[ClaimantEmploymentID] [bigint] NULL,
[ProductID] [int] NULL,
[NavStartDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavEndDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavInjPartyKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavProductName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavBrand] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavProductType] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NavManufacturer] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductNotes] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantProduct] ADD CONSTRAINT [PK__mBtblCla__3B9049C42E06CDA9] PRIMARY KEY CLUSTERED  ([ClaimantProductID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblClaimantProduct_tblClaimantProduct] ON [dbo].[tblClaimantProduct] ([ProductID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimantProduct] ADD CONSTRAINT [FK_tblClaimantProduct_tblClaimantProduct] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[tblProduct] ([ProductID]) ON DELETE CASCADE
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantProduct] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantProduct] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT DELETE ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimantProduct] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantProduct] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimantProduct] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimantProduct] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimantProduct] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimantProduct] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimantProduct] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimantProduct] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', NULL, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'ClaimantProductID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'ClaimantProductID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'ClaimantProductID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'NavEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'NavEndDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'NavEndDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'NavStartDate'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'NavStartDate'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'tblClaimantProduct', 'COLUMN', N'NavStartDate'
GO
