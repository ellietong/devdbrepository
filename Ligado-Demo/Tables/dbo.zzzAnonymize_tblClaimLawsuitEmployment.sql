CREATE TABLE [dbo].[zzzAnonymize_tblClaimLawsuitEmployment]
(
[ClaimLawsuitEmploymentID] [bigint] NOT NULL IDENTITY(1, 1),
[OccupationID] [int] NULL,
[Jobsite] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobsiteCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zzzAnonymize_tblClaimLawsuitEmployment] ADD CONSTRAINT [PK_zzzAnonymize_tblClaimLawsuitEmployment] PRIMARY KEY CLUSTERED  ([ClaimLawsuitEmploymentID]) ON [PRIMARY]
GO
