CREATE TABLE [dbo].[tblPolicyDateType]
(
[PolicyDateTypeID] [int] NOT NULL IDENTITY(1, 1),
[PolicyDateType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPolicyDateType] ADD CONSTRAINT [PK_tblPolicyDateType] PRIMARY KEY CLUSTERED  ([PolicyDateTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDateType] TO [Allocation_User]
GRANT SELECT ON  [dbo].[tblPolicyDateType] TO [Allocation_User]
GRANT INSERT ON  [dbo].[tblPolicyDateType] TO [Allocation_User]
GRANT DELETE ON  [dbo].[tblPolicyDateType] TO [Allocation_User]
GRANT UPDATE ON  [dbo].[tblPolicyDateType] TO [Allocation_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDateType] TO [base_user]
GRANT SELECT ON  [dbo].[tblPolicyDateType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDateType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPolicyDateType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPolicyDateType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPolicyDateType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPolicyDateType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDateType] TO [Policy_User]
GRANT SELECT ON  [dbo].[tblPolicyDateType] TO [Policy_User]
GRANT INSERT ON  [dbo].[tblPolicyDateType] TO [Policy_User]
GRANT DELETE ON  [dbo].[tblPolicyDateType] TO [Policy_User]
GRANT UPDATE ON  [dbo].[tblPolicyDateType] TO [Policy_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPolicyDateType] TO [power_user]
GRANT SELECT ON  [dbo].[tblPolicyDateType] TO [power_user]
GRANT INSERT ON  [dbo].[tblPolicyDateType] TO [power_user]
GRANT DELETE ON  [dbo].[tblPolicyDateType] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPolicyDateType] TO [power_user]
GO
