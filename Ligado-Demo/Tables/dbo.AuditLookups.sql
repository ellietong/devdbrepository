CREATE TABLE [dbo].[AuditLookups]
(
[LookupID] [int] NOT NULL,
[RelationshipID] [int] NOT NULL,
[ExpressionIsColumn] [bit] NOT NULL,
[LookupExpression] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditLookups] ADD CONSTRAINT [PK_AuditLookups] PRIMARY KEY CLUSTERED  ([LookupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditLookups] ADD CONSTRAINT [FK_AuditRelationships] FOREIGN KEY ([RelationshipID]) REFERENCES [dbo].[AuditRelationships] ([RelationshipID])
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditLookups] TO [base_user]
GRANT SELECT ON  [dbo].[AuditLookups] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditLookups] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditLookups] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditLookups] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditLookups] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditLookups] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditLookups] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLookups] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditLookups] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditLookups] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditLookups] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLookups] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditLookups] TO [power_user]
GRANT SELECT ON  [dbo].[AuditLookups] TO [power_user]
GRANT INSERT ON  [dbo].[AuditLookups] TO [power_user]
GRANT DELETE ON  [dbo].[AuditLookups] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditLookups] TO [power_user]
GO
