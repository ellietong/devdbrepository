CREATE TABLE [dbo].[tblPayment]
(
[PaymentID] [bigint] NOT NULL IDENTITY(1, 1),
[LedgerID] [int] NULL,
[Payment] [money] NULL,
[PaymentDate] [datetime] NULL,
[PaymentNote] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPayment] ADD CONSTRAINT [PK_tblPayment] PRIMARY KEY CLUSTERED  ([PaymentID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPayment] TO [base_user]
GRANT SELECT ON  [dbo].[tblPayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPayment] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPayment] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPayment] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPayment] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPayment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblPayment] TO [power_user]
GRANT SELECT ON  [dbo].[tblPayment] TO [power_user]
GRANT INSERT ON  [dbo].[tblPayment] TO [power_user]
GRANT DELETE ON  [dbo].[tblPayment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPayment] TO [power_user]
GO
