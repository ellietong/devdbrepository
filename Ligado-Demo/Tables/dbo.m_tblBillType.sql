CREATE TABLE [dbo].[m_tblBillType]
(
[BillTypeID] [int] NOT NULL IDENTITY(1, 1),
[BillType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_tblBillType] ADD CONSTRAINT [PK_m_tblBillType] PRIMARY KEY CLUSTERED  ([BillTypeID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblBillType] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblBillType] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblBillType] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblBillType] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblBillType] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblBillType] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblBillType] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblBillType] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblBillType] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblBillType] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblBillType] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblBillType] TO [power_user]
GO
