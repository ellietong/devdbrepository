CREATE TABLE [dbo].[AuditColumns]
(
[TableID] [int] NOT NULL,
[ColumnID] [int] NOT NULL IDENTITY(1, 1),
[ColumnName] [sys].[sysname] NOT NULL,
[AuditColumn] [bit] NOT NULL CONSTRAINT [DF_AuditColumns_Audited] DEFAULT ((0)),
[CaptureColumn] [bit] NOT NULL CONSTRAINT [DF_AuditColumns_Captured] DEFAULT ((0)),
[IsLookup] [bit] NOT NULL CONSTRAINT [DF_AuditColumns_IsLookup] DEFAULT ((0)),
[KeyColumn] [tinyint] NOT NULL CONSTRAINT [DF_AuditColumns_KeyColumn] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditColumns] ADD CONSTRAINT [PK_AuditColumns] PRIMARY KEY NONCLUSTERED  ([TableID], [ColumnID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AuditColumns2] ON [dbo].[AuditColumns] ([ColumnID]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_AuditColumns1] ON [dbo].[AuditColumns] ([TableID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditColumns] ADD CONSTRAINT [FK_AuditColumns1] FOREIGN KEY ([TableID]) REFERENCES [dbo].[AuditTables] ([TableID])
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditColumns] TO [base_user]
GRANT SELECT ON  [dbo].[AuditColumns] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditColumns] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditColumns] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditColumns] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditColumns] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditColumns] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditColumns] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditColumns] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditColumns] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditColumns] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditColumns] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditColumns] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditColumns] TO [power_user]
GRANT SELECT ON  [dbo].[AuditColumns] TO [power_user]
GRANT INSERT ON  [dbo].[AuditColumns] TO [power_user]
GRANT DELETE ON  [dbo].[AuditColumns] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditColumns] TO [power_user]
GO
