CREATE TABLE [dbo].[zzzAnonymize_tblLawsuit]
(
[LawsuitID] [bigint] NOT NULL IDENTITY(1, 1),
[CaseCaption] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocketNumber] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffFirstName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadPlaintiffLastName] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zzzAnonymize_tblLawsuit] ADD CONSTRAINT [PK_zzzAnonymize_tblLawsuit] PRIMARY KEY CLUSTERED  ([LawsuitID]) ON [PRIMARY]
GO
