CREATE TABLE [dbo].[tblComplaintTrackingLawsuit]
(
[ComplaintTrackingLawsuitID] [int] NOT NULL IDENTITY(1, 1),
[ComplaintTrackingID] [int] NULL,
[LawsuitID] [int] NULL
) ON [PRIMARY]
GO
