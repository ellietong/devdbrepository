CREATE TABLE [dbo].[m_tblPACEHistDefInvoice]
(
[INVOICEID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCNID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PERIOD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FEES] [money] NULL,
[EXPENSES] [money] NULL,
[ADJUSTMENTS] [money] NULL,
[TOTAL] [money] NULL,
[STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHECKNUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHECKDATE] [datetime] NULL,
[VendorName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEHistDefInvoice] TO [base_user]
GRANT SELECT ON  [dbo].[m_tblPACEHistDefInvoice] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEHistDefInvoice] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_tblPACEHistDefInvoice] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_tblPACEHistDefInvoice] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_tblPACEHistDefInvoice] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_tblPACEHistDefInvoice] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_tblPACEHistDefInvoice] TO [power_user]
GRANT SELECT ON  [dbo].[m_tblPACEHistDefInvoice] TO [power_user]
GRANT INSERT ON  [dbo].[m_tblPACEHistDefInvoice] TO [power_user]
GRANT DELETE ON  [dbo].[m_tblPACEHistDefInvoice] TO [power_user]
GRANT UPDATE ON  [dbo].[m_tblPACEHistDefInvoice] TO [power_user]
GO
