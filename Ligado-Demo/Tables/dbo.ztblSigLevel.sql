CREATE TABLE [dbo].[ztblSigLevel]
(
[SigLevelID] [int] NOT NULL IDENTITY(1, 1),
[ColumnID] [int] NULL,
[PolicyID] [int] NULL,
[UpperLimit] [float] NULL,
[SmallestIndemnity] [float] NULL,
[SmallestDefense] [float] NULL,
[AttachmentPoint] [float] NULL,
[ColumnOn] [bit] NULL,
[ColumnDays] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ztblSigLevel] ADD CONSTRAINT [PK_ztblSigLevel] PRIMARY KEY CLUSTERED  ([SigLevelID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[ztblSigLevel] TO [base_user]
GRANT SELECT ON  [dbo].[ztblSigLevel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[ztblSigLevel] TO [Ligado_User]
GRANT SELECT ON  [dbo].[ztblSigLevel] TO [Ligado_User]
GRANT INSERT ON  [dbo].[ztblSigLevel] TO [Ligado_User]
GRANT DELETE ON  [dbo].[ztblSigLevel] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[ztblSigLevel] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[ztblSigLevel] TO [power_user]
GRANT SELECT ON  [dbo].[ztblSigLevel] TO [power_user]
GRANT INSERT ON  [dbo].[ztblSigLevel] TO [power_user]
GRANT DELETE ON  [dbo].[ztblSigLevel] TO [power_user]
GRANT UPDATE ON  [dbo].[ztblSigLevel] TO [power_user]
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x02
EXEC sp_addextendedproperty N'MS_DefaultView', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Filter', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_FilterOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_HideNewField', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_OrderBy', N'[ztblSigLevel].[PolicyID]', 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOn', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_OrderByOnLoad', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', NULL, NULL
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_Orientation', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=10000
EXEC sp_addextendedproperty N'MS_TableMaxRecords', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_TotalsRow', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'AttachmentPoint'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'AttachmentPoint'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'AttachmentPoint'
GO
DECLARE @xp int
SELECT @xp=1710
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'AttachmentPoint'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'AttachmentPoint'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'ColumnID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'ColumnID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'PolicyID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'PolicyID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'PolicyID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'PolicyID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'PolicyID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SigLevelID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SigLevelID'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SigLevelID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SigLevelID'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SigLevelID'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SmallestDefense'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SmallestDefense'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SmallestDefense'
GO
DECLARE @xp int
SELECT @xp=1770
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SmallestDefense'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SmallestDefense'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SmallestIndemnity'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SmallestIndemnity'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SmallestIndemnity'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SmallestIndemnity'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'SmallestIndemnity'
GO
DECLARE @xp int
SELECT @xp=-1
EXEC sp_addextendedproperty N'MS_AggregateType', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'UpperLimit'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnHidden', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'UpperLimit'
GO
DECLARE @xp int
SELECT @xp=0
EXEC sp_addextendedproperty N'MS_ColumnOrder', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'UpperLimit'
GO
DECLARE @xp int
SELECT @xp=2235
EXEC sp_addextendedproperty N'MS_ColumnWidth', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'UpperLimit'
GO
DECLARE @xp varbinary (1)
SELECT @xp=0x00
EXEC sp_addextendedproperty N'MS_TextAlign', @xp, 'SCHEMA', N'dbo', 'TABLE', N'ztblSigLevel', 'COLUMN', N'UpperLimit'
GO
