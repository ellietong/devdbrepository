CREATE TABLE [dbo].[tblBillGroupSubscribers]
(
[BillGroupSubscriberID] [int] NOT NULL IDENTITY(1, 1),
[BillGroupID] [int] NOT NULL,
[SubscriberID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBillGroupSubscribers] ADD CONSTRAINT [PK_tblBillGroupSubscribers] PRIMARY KEY CLUSTERED  ([BillGroupSubscriberID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupSubscribers] TO [base_user]
GRANT SELECT ON  [dbo].[tblBillGroupSubscribers] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupSubscribers] TO [Billing_User]
GRANT SELECT ON  [dbo].[tblBillGroupSubscribers] TO [Billing_User]
GRANT INSERT ON  [dbo].[tblBillGroupSubscribers] TO [Billing_User]
GRANT DELETE ON  [dbo].[tblBillGroupSubscribers] TO [Billing_User]
GRANT UPDATE ON  [dbo].[tblBillGroupSubscribers] TO [Billing_User]
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupSubscribers] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblBillGroupSubscribers] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblBillGroupSubscribers] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblBillGroupSubscribers] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblBillGroupSubscribers] TO [Ligado_User]
GRANT ALTER ON  [dbo].[tblBillGroupSubscribers] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblBillGroupSubscribers] TO [power_user]
GRANT SELECT ON  [dbo].[tblBillGroupSubscribers] TO [power_user]
GRANT INSERT ON  [dbo].[tblBillGroupSubscribers] TO [power_user]
GRANT DELETE ON  [dbo].[tblBillGroupSubscribers] TO [power_user]
GRANT UPDATE ON  [dbo].[tblBillGroupSubscribers] TO [power_user]
GO
