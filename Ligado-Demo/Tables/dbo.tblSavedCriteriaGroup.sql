CREATE TABLE [dbo].[tblSavedCriteriaGroup]
(
[SavedCriteriaGroupID] [int] NOT NULL IDENTITY(1, 1),
[SavedCriteriaGroupName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedCriteriaGroup] ADD CONSTRAINT [PK_tblSavedCriteriaGroup] PRIMARY KEY CLUSTERED  ([SavedCriteriaGroupID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblSavedCriteriaGroup_tblSavedCriteriaGroup] ON [dbo].[tblSavedCriteriaGroup] ([SavedCriteriaGroupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSavedCriteriaGroup] ADD CONSTRAINT [FK_tblSavedCriteriaGroup_tblSavedCriteriaGroup] FOREIGN KEY ([SavedCriteriaGroupID]) REFERENCES [dbo].[tblSavedCriteriaGroup] ([SavedCriteriaGroupID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSavedCriteriaGroup] TO [base_user]
GRANT SELECT ON  [dbo].[tblSavedCriteriaGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedCriteriaGroup] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSavedCriteriaGroup] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSavedCriteriaGroup] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSavedCriteriaGroup] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSavedCriteriaGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSavedCriteriaGroup] TO [power_user]
GRANT SELECT ON  [dbo].[tblSavedCriteriaGroup] TO [power_user]
GRANT INSERT ON  [dbo].[tblSavedCriteriaGroup] TO [power_user]
GRANT DELETE ON  [dbo].[tblSavedCriteriaGroup] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSavedCriteriaGroup] TO [power_user]
GO
