CREATE TABLE [dbo].[AuditConfig]
(
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditConfig] ADD CONSTRAINT [PK_AuditConfig] PRIMARY KEY CLUSTERED  ([Name]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditConfig] TO [base_user]
GRANT SELECT ON  [dbo].[AuditConfig] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditConfig] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditConfig] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditConfig] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditConfig] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditConfig] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditConfig] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditConfig] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditConfig] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditConfig] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditConfig] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditConfig] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditConfig] TO [power_user]
GRANT SELECT ON  [dbo].[AuditConfig] TO [power_user]
GRANT INSERT ON  [dbo].[AuditConfig] TO [power_user]
GRANT DELETE ON  [dbo].[AuditConfig] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditConfig] TO [power_user]
GO
