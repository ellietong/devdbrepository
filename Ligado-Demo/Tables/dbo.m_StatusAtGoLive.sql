CREATE TABLE [dbo].[m_StatusAtGoLive]
(
[ClaimLawsuitID] [int] NULL,
[ClaimLawsuitStatusID] [int] NULL,
[ID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[m_StatusAtGoLive] ADD CONSTRAINT [PK_m_StatusAtGoLive] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[m_StatusAtGoLive] TO [base_user]
GRANT SELECT ON  [dbo].[m_StatusAtGoLive] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[m_StatusAtGoLive] TO [Claims_User]
GRANT SELECT ON  [dbo].[m_StatusAtGoLive] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[m_StatusAtGoLive] TO [Ligado_User]
GRANT SELECT ON  [dbo].[m_StatusAtGoLive] TO [Ligado_User]
GRANT INSERT ON  [dbo].[m_StatusAtGoLive] TO [Ligado_User]
GRANT DELETE ON  [dbo].[m_StatusAtGoLive] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[m_StatusAtGoLive] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[m_StatusAtGoLive] TO [power_user]
GRANT SELECT ON  [dbo].[m_StatusAtGoLive] TO [power_user]
GRANT INSERT ON  [dbo].[m_StatusAtGoLive] TO [power_user]
GRANT DELETE ON  [dbo].[m_StatusAtGoLive] TO [power_user]
GRANT UPDATE ON  [dbo].[m_StatusAtGoLive] TO [power_user]
GO
