CREATE TABLE [dbo].[tblAccountsReceivablePayment]
(
[AccountsReceivablePaymentID] [int] NOT NULL IDENTITY(1, 1),
[ExhaustionID] [int] NULL,
[PaymentID] [int] NULL,
[Amount] [money] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAccountsReceivablePayment] ADD CONSTRAINT [PK_tblAccountsReceivablePayment] PRIMARY KEY CLUSTERED  ([AccountsReceivablePaymentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAccountsReceivablePayment] ADD CONSTRAINT [FK_tblAccountsReceivablePayment_tblExhaustion] FOREIGN KEY ([ExhaustionID]) REFERENCES [dbo].[tblExhaustion] ([ExhaustionID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblAccountsReceivablePayment] TO [base_user]
GRANT SELECT ON  [dbo].[tblAccountsReceivablePayment] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblAccountsReceivablePayment] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblAccountsReceivablePayment] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblAccountsReceivablePayment] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblAccountsReceivablePayment] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblAccountsReceivablePayment] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblAccountsReceivablePayment] TO [power_user]
GRANT SELECT ON  [dbo].[tblAccountsReceivablePayment] TO [power_user]
GRANT INSERT ON  [dbo].[tblAccountsReceivablePayment] TO [power_user]
GRANT DELETE ON  [dbo].[tblAccountsReceivablePayment] TO [power_user]
GRANT UPDATE ON  [dbo].[tblAccountsReceivablePayment] TO [power_user]
GO
