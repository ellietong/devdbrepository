CREATE TABLE [dbo].[tblClaimSettlementDocumentCategory_OLD]
(
[DocCategoryID] [int] NOT NULL IDENTITY(1, 1),
[DocCategory] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblClaimSettlementDocumentCategory_OLD] ADD CONSTRAINT [PK_tblDocumentCategory] PRIMARY KEY CLUSTERED  ([DocCategoryID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [base_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [Claims_User]
GRANT INSERT ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [Claims_User]
GRANT UPDATE ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [Claims_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [power_user]
GRANT SELECT ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [power_user]
GRANT INSERT ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [power_user]
GRANT DELETE ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [power_user]
GRANT UPDATE ON  [dbo].[tblClaimSettlementDocumentCategory_OLD] TO [power_user]
GO
