CREATE TABLE [dbo].[tblPlaintiffCounsel_ALT]
(
[PlaintiffCounselID] [bigint] NOT NULL IDENTITY(1, 1),
[PlaintiffCounsel] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlaintiffCounselTaxID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPlaintiffCounsel_ALT] ADD CONSTRAINT [PK_tblPlaintiffCounsel_ALT] PRIMARY KEY CLUSTERED  ([PlaintiffCounselID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounsel_ALT] TO [base_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel_ALT] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounsel_ALT] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel_ALT] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblPlaintiffCounsel_ALT] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblPlaintiffCounsel_ALT] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblPlaintiffCounsel_ALT] TO [Ligado_User]
GRANT ALTER ON  [dbo].[tblPlaintiffCounsel_ALT] TO [power_user]
GRANT VIEW DEFINITION ON  [dbo].[tblPlaintiffCounsel_ALT] TO [power_user]
GRANT SELECT ON  [dbo].[tblPlaintiffCounsel_ALT] TO [power_user]
GRANT INSERT ON  [dbo].[tblPlaintiffCounsel_ALT] TO [power_user]
GRANT DELETE ON  [dbo].[tblPlaintiffCounsel_ALT] TO [power_user]
GRANT UPDATE ON  [dbo].[tblPlaintiffCounsel_ALT] TO [power_user]
GO
