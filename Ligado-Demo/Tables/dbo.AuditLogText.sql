CREATE TABLE [dbo].[AuditLogText]
(
[AuditLogID] [int] NOT NULL,
[RowKey] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnID] [int] NOT NULL,
[Value] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditLogText] ADD CONSTRAINT [PK_AuditLogText] PRIMARY KEY NONCLUSTERED  ([AuditLogID], [RowKey], [ColumnID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditLogText] TO [base_user]
GRANT SELECT ON  [dbo].[AuditLogText] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditLogText] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditLogText] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditLogText] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditLogText] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditLogText] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditLogText] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLogText] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditLogText] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditLogText] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditLogText] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditLogText] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditLogText] TO [power_user]
GRANT SELECT ON  [dbo].[AuditLogText] TO [power_user]
GRANT INSERT ON  [dbo].[AuditLogText] TO [power_user]
GRANT DELETE ON  [dbo].[AuditLogText] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditLogText] TO [power_user]
GO
