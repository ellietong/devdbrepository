CREATE TABLE [dbo].[AuditTables]
(
[TableID] [int] NOT NULL IDENTITY(1, 1),
[Owner] [sys].[sysname] NOT NULL,
[TableName] [sys].[sysname] NOT NULL,
[AuditTable] [bit] NOT NULL,
[AuditInserts] [bit] NOT NULL,
[AuditUpdates] [bit] NOT NULL,
[AuditDeletes] [bit] NOT NULL,
[AuditDeletesAll] [bit] NOT NULL,
[UniqueKeyType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserColumn] [sys].[sysname] NOT NULL CONSTRAINT [DF__AuditTabl__UserC__3A2E441A] DEFAULT (' ')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditTables] ADD CONSTRAINT [PK_AuditTables] PRIMARY KEY NONCLUSTERED  ([TableID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AuditTables_Name] ON [dbo].[AuditTables] ([Owner], [TableName]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditTables] TO [base_user]
GRANT SELECT ON  [dbo].[AuditTables] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditTables] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditTables] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditTables] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditTables] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditTables] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditTables] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditTables] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditTables] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditTables] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditTables] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditTables] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditTables] TO [power_user]
GRANT SELECT ON  [dbo].[AuditTables] TO [power_user]
GRANT INSERT ON  [dbo].[AuditTables] TO [power_user]
GRANT DELETE ON  [dbo].[AuditTables] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditTables] TO [power_user]
GO
