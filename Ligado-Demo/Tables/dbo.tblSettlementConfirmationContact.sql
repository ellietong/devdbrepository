CREATE TABLE [dbo].[tblSettlementConfirmationContact]
(
[SettlementContactID] [bigint] NOT NULL IDENTITY(1, 1),
[SettlementGroupID] [bigint] NOT NULL,
[PersonID] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementConfirmationContact] ADD CONSTRAINT [PK_tblSettlementContactID] PRIMARY KEY CLUSTERED  ([SettlementContactID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_tblSettlementContactID_tblClaimLawsuit] ON [dbo].[tblSettlementConfirmationContact] ([SettlementGroupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSettlementConfirmationContact] ADD CONSTRAINT [FK_tblSettlementContactID_tblClaimLawsuit] FOREIGN KEY ([SettlementGroupID]) REFERENCES [dbo].[tblClaimLawsuit] ([ClaimLawsuitID])
GO
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementConfirmationContact] TO [base_user]
GRANT SELECT ON  [dbo].[tblSettlementConfirmationContact] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementConfirmationContact] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblSettlementConfirmationContact] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblSettlementConfirmationContact] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblSettlementConfirmationContact] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblSettlementConfirmationContact] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblSettlementConfirmationContact] TO [power_user]
GRANT SELECT ON  [dbo].[tblSettlementConfirmationContact] TO [power_user]
GRANT INSERT ON  [dbo].[tblSettlementConfirmationContact] TO [power_user]
GRANT DELETE ON  [dbo].[tblSettlementConfirmationContact] TO [power_user]
GRANT UPDATE ON  [dbo].[tblSettlementConfirmationContact] TO [power_user]
GO
