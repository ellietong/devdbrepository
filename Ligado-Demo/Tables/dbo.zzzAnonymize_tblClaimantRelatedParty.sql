CREATE TABLE [dbo].[zzzAnonymize_tblClaimantRelatedParty]
(
[ClaimantRelatedPartyID] [bigint] NOT NULL IDENTITY(1, 1),
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPAddress1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPAddress2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPZip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPPhone] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPSocialSecurityNumber] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPUsername] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[zzzAnonymize_tblClaimantRelatedParty] ADD CONSTRAINT [PK_zzzAnonymize_tblClaimantRelatedParty] PRIMARY KEY CLUSTERED  ([ClaimantRelatedPartyID]) ON [PRIMARY]
GO
