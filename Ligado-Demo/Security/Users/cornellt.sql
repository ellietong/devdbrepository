IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'KCICLLC\cornellt')
CREATE LOGIN [KCICLLC\cornellt] FROM WINDOWS
GO
CREATE USER [cornellt] FOR LOGIN [KCICLLC\cornellt]
GO
GRANT CREATE FUNCTION TO [cornellt]
GRANT CREATE PROCEDURE TO [cornellt]
GRANT CREATE TABLE TO [cornellt]
GRANT CREATE VIEW TO [cornellt]
