IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'LigadoGSX')
CREATE LOGIN [LigadoGSX] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [LigadoGSX] FOR LOGIN [LigadoGSX]
GO
