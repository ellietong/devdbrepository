IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'KCICLLC\shockleym')
CREATE LOGIN [KCICLLC\shockleym] FROM WINDOWS
GO
CREATE USER [shockleym] FOR LOGIN [KCICLLC\shockleym]
GO
GRANT CREATE FUNCTION TO [shockleym]
GRANT CREATE PROCEDURE TO [shockleym]
GRANT CREATE TABLE TO [shockleym]
GRANT CREATE VIEW TO [shockleym]
