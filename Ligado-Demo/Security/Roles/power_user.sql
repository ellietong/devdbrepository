CREATE ROLE [power_user]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'power_user', N'crookc'
GO
EXEC sp_addrolemember N'power_user', N'marshalla'
GO
EXEC sp_addrolemember N'power_user', N'owenss'
GO
EXEC sp_addrolemember N'power_user', N'potterm'
GO
EXEC sp_addrolemember N'power_user', N'scottc'
GO
EXEC sp_addrolemember N'power_user', N'shockleym'
GO
