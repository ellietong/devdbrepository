CREATE ROLE [PolPic_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'PolPic_User', N'base_user'
GO
EXEC sp_addrolemember N'PolPic_User', N'casillaw'
GO
EXEC sp_addrolemember N'PolPic_User', N'eastlacka'
GO
EXEC sp_addrolemember N'PolPic_User', N'hughesr'
GO
EXEC sp_addrolemember N'PolPic_User', N'husainm'
GO
EXEC sp_addrolemember N'PolPic_User', N'khanm'
GO
EXEC sp_addrolemember N'PolPic_User', N'zongc'
GO
