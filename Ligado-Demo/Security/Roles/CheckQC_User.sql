CREATE ROLE [CheckQC_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'CheckQC_User', N'chaudoinr'
GO
EXEC sp_addrolemember N'CheckQC_User', N'cornellt'
GO
EXEC sp_addrolemember N'CheckQC_User', N'jacksonc'
GO
EXEC sp_addrolemember N'CheckQC_User', N'kanga'
GO
EXEC sp_addrolemember N'CheckQC_User', N'khanm'
GO
EXEC sp_addrolemember N'CheckQC_User', N'onyiukeo'
GO
