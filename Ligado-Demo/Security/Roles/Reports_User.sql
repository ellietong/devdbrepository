CREATE ROLE [Reports_User]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'Reports_User', N'base_user'
GO
EXEC sp_addrolemember N'Reports_User', N'casillaw'
GO
EXEC sp_addrolemember N'Reports_User', N'eastlacka'
GO
EXEC sp_addrolemember N'Reports_User', N'hughesr'
GO
EXEC sp_addrolemember N'Reports_User', N'husainm'
GO
EXEC sp_addrolemember N'Reports_User', N'khanm'
GO
EXEC sp_addrolemember N'Reports_User', N'zongc'
GO
