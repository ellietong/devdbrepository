SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fn_GetClientSiteID]()
RETURNS int
AS
BEGIN
    RETURN 29
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetClientSiteID] TO [base_user]
GRANT EXECUTE ON  [dbo].[fn_GetClientSiteID] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[fn_GetClientSiteID] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[fn_GetClientSiteID] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[fn_GetClientSiteID] TO [power_user]
GRANT EXECUTE ON  [dbo].[fn_GetClientSiteID] TO [power_user]
GO
