CREATE TYPE [dbo].[integer_list_tbltype] AS TABLE
(
[n] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([n])
)
GO
