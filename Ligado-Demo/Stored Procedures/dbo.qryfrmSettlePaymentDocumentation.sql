SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmSettlePaymentDocumentation] (@ClaimLawsuitID int)
as

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 4552

Select distinct cs.[ClaimLawsuitID]
      ,[ClaimantIDed]
      ,[DxMedicals]
      ,[DODCert]
      ,[EstatePapers]
      ,[ProductID]
      ,[EmployExpoHist]
      ,[CompServ]
      ,[SettlementConfirmation]
      ,cs.[ReleaseRqstDate]
      ,[ReleaseToPC]
      ,[ExecutedReleaseDate]
      ,cs.[SettlePaymentComments]
      ,[ReleaseRcvd]
      ,[MedicareFullyDocumented]
      ,[MedicareReported]
      ,[ReleaseSignedDate]
      ,[ReleaseTypeID]
      ,[ReleaseTemplateTypeID]
      ,[MedicareFormSent]
      ,[MedicareFormRcvd]
      ,cs.[SPUsername]
from tblClaimSettlement cs
	left join dbo.tblClaimSettlementDocuments csd ON cs.ClaimLawsuitID = csd.ClaimLawsuitID
	left join tblClaimSettlementDocumentation doc ON cs.ClaimLawsuitID = doc.ClaimLawsuitID
Where cs.ClaimLawsuitID = @ClaimLawsuitID

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmSettlePaymentDocumentation] TO [power_user]
GO
