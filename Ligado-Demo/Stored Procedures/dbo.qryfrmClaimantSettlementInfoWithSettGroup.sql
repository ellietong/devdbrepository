SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryfrmClaimantSettlementInfoWithSettGroup] (@ClaimLawsuitID int)
as

--DECLARE @ClaimLawsuitID int
--SET @ClaimLawsuitID = 4552

Select cs.*, SettlementGroup, PaymentTerms
From tblClaimSettlement cs
LEFT JOIN tblClaimSettlementGroup csg ON cs.SettlementGroupID = csg.SettlementGroupID
LEFT JOIN tblPaymentTerms pt ON csg.PaymentTermsID = pt.PaymentTermsID
Where ClaimLawsuitID = @ClaimLawsuitID
Order By SettlementDate desc

Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementInfoWithSettGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementInfoWithSettGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementInfoWithSettGroup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementInfoWithSettGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantSettlementInfoWithSettGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantSettlementInfoWithSettGroup] TO [power_user]
GO
