SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryInternalEmailNewSettlementApprovals] 
AS
SET NOCOUNT ON
 
BEGIN

 
--Declare and set email inputs.
Declare @Subject nvarchar(100)
Declare @LogoHeader nvarchar(2000)
 
Declare @Today10AM datetime
Declare @Yesterday10AM datetime
SET @Yesterday10AM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 1)) + ' 10:00:00.000')
SET @Today10AM = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE())) + ' 10:00:00.000')
DECLARE @SendEmail bit = 0

SET @Subject = 'GSX Payment Approval Summary ' + CONVERT(varchar, GETDATE(), 107)

--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site	


SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'




--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar(max)

--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
	<!--
	TABLE {border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;}
	TD	
	{font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: top;
		
	}
	TH	
	{border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);}
	UL {margin:0; 
		
	padding:0;}	
 
 
	--->
	</STYLE>'



SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
	<img src="http://www.kcicllc.com/i/KCICLogoBar_Solid.png" alt="logo"/>'
--CSS
DECLARE @MainHeaderStyle nvarchar(max) = 'background-color: #0079C1; font-weight: bold; color: white; padding: 4px;'
DECLARE @tdStyle nvarchar(max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000; vertical-align: left;'

IF (SELECT
	COUNT(*)
FROM tblClaim c
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblMatter m ON c.MatterID = m.MatterID
INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
INNER JOIN tblClaimSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
WHERE cs.PaymentApprovedDate BETWEEN @Yesterday10AM AND @Today10AM
AND NOT EXISTS (SELECT
	PaymentSettlementID
FROM tblPaymentSettlement ps
WHERE cs.ClaimSettlementID = ps.ClaimSettlementID))
> 0 BEGIN
SET @SendEmail = 1


--PRINT TABLE SHOWING SETTLEMENT AUTHORITY FIELDS FIRST
SET @BodyMessage = @BodyMessage + '<H4>The following settlement(s) have been approved for payment.<br/><br/>'-- +


--PRINT TABLE HEADERS
SET @BodyMessage = @BodyMessage + '
	<table>
		<thead>
			<tr>
				<th style="' + @MainHeaderStyle + '">Claimant</th>
				<th style="' + @MainHeaderStyle + '">Settlement Group</th>
				<th style="' + @MainHeaderStyle + '">Plaintiff Counsel</th>
				<th style="' + @MainHeaderStyle + '">Matter</th>
				<th style="' + @MainHeaderStyle + '">Settlement Date</th>
				<th style="' + @MainHeaderStyle + '">Settlement Amount</th>
				
			</tr>
		</thead>
	'
--PRINT TABLE OF CLAIMS
SET @BodyMessage = @BodyMessage +

COALESCE((CAST((SELECT
	[td/@style] = @tdStyle,
	[td/a/@href] = ' <a href="https://ligado.kcic.com/Claims/ClaimantDetails?ClientSiteID=19&ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), cpi.ClaimantPersonalInfoID),
	[td/a] = cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''),
	[td] = '',
	'',
	[td/@style] = @tdStyle,
	td = COALESCE(UPPER(SettlementGroup), 'Unknown'),
	'',
	[td/@style] = @tdStyle,
	td = COALESCE(PlaintiffCounsel, 'Unknown'),
	'',
	[td/@style] = @tdStyle,
	td = CONVERT(nvarchar, m.Matter, 101),
	'',
	[td/@style] = @tdStyle,
	td = CONVERT(nvarchar, SettlementDate, 101),
	'',
	[td/@style] = @tdStyle,
	td = '$' + CONVERT(nvarchar, CONVERT(money, settlementamount)),
	''

FROM tblClaim c
INNER JOIN tblClaimantPersonalInfo cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
INNER JOIN tblMatter m ON c.MatterID = m.MatterID
INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
INNER JOIN tblClaimSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
LEFT JOIN tblClaimSettlementGroup s ON cs.SettlementGroupID = s.SettlementGroupID
LEFT JOIN tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID=l.PlaintiffCounselAddressID
WHERE cs.PaymentApprovedDate BETWEEN @Yesterday10AM AND @Today10AM
AND NOT EXISTS (SELECT
	PaymentSettlementID
FROM tblPaymentSettlement ps
WHERE cs.ClaimSettlementID = ps.ClaimsettlementID)

GROUP BY	cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''),
			cpi.ClaimantPersonalInfoID,
			PlaintiffCounsel,
			SettlementDate,
			SettlementGroup,
			cs.PaymentApprovedBy,
			cs.PaymentApprovedDate,
			ClaimSettlementID,
			m.Matter,
			SettlementAmount
FOR xml PATH ('tr'), TYPE)
AS nvarchar(max))), '')

--CLOSE TABLE
SET @BodyMessage = @BodyMessage + '</table><br>'
END



--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
	733 10th Street, NW, Suite 3001<br>
	Washington, DC 20001<br>
	(t) 202-772-2300<br>
	(f) 202-772-2333'


DECLARE @RecipientList nvarchar(500)


----QC Recipient List
--Set @Bodymessage = @BodyMessage + 'Recipients' + @RecipientList
--PRINT @RecipientList

--PRINT @SendEmail

--IF (@SendEmail = 1) BEGIN
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail
--The Profile_Name sends from [client]@kcicllc.com email address
@profile_name = 'Claims',
@recipients = 'shockleym@kcic.com;gsx@kcic.com',
--@recipients = 'hagep@kcic.com;shockleym@kcic.com',
--@copy_recipients = '',
--@blind_copy_recipients = 'gsx@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'
--END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailNewSettlementApprovals] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailNewSettlementApprovals] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailNewSettlementApprovals] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryInternalEmailNewSettlementApprovals] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailNewSettlementApprovals] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailNewSettlementApprovals] TO [power_user]
GO
