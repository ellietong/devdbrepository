SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pr_kr_AuditLogHeader] (@ConfigID uniqueidentifier,
                                           @AuditLogID int output )
AS
BEGIN
  set nocount on
  if @ConfigID <> 'C6181EEE-5254-4DF1-89B5-1FBAE386970E'
  begin
    raiserror ('OmniAudit: The audit configuration has been changed.  Audit triggers must be regenerated to match the new configuration.', 18, 1) WITH nowait
    return 1
  end
  exec dbo.pr_kr_AuditAutoKey 'AuditLog', @AuditLogID output
END
GO
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditLogHeader] TO [base_user]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogHeader] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditLogHeader] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogHeader] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogHeader] TO [omniaudit_admin]
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditLogHeader] TO [power_user]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogHeader] TO [power_user]
GO
