SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo]
	-- Add the parameters for the stored procedure here
(@ClaimantPersonalInfoID int, @ClaimLawsuitID int, @SettlementAmount money, @SettlementDate date,
@NotificationDate date, @EmailAddress nvarchar(250), @PlaintiffCounselAddressID int,
@ClaimLawsuitStatusID int, @SettlementGroupID int, @ReleaseTypeID int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @LawsuitID int
SET @LawsuitID = (SELECT TOP 1 LawsuitID FROM tblClaimLawsuit WHERE ClaimLawsuitID = @ClaimLawsuitID)


IF @ClaimLawsuitID IN (Select ClaimLawsuitID FROM tblClaimSettlement)
BEGIN
	UPDATE tblClaimSettlement
	SET SettlementAmount = @SettlementAmount,
		SettlementDate = @SettlementDate,
		SettlementNotificationDate = @NotificationDate,
		SettlementGroupID = @SettlementGroupID,
		ReleaseTypeID = @ReleaseTypeID,
		SPUsername = @EmailAddress
	WHERE ClaimLawsuitID = @ClaimLawsuitID
END
ELSE
BEGIN
	INSERT INTO tblClaimSettlement (ClaimLawsuitID, SettlementAmount, SettlementDate, SettlementGroupID, ReleaseTypeID,SPUsername)
	VALUES (@ClaimLawsuitID, @SettlementAmount, @SettlementDate, @SettlementGroupID, @ReleaseTypeID, @EmailAddress)
END

UPDATE tblLawsuit	
SET 
	PlaintiffCounselAddressID = @PlaintiffCounselAddressID,
	SPUsername = @EmailAddress
WHERE LawsuitID = @LawsuitID	
	--If status is "continued" just insert the Continued status as non-primary status
	IF @ClaimLawsuitStatusID = 41 --continued
	BEGIN
		INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, ProcessedDate, IsPrimaryStatus, SPUsername)
		VALUES(@ClaimLawsuitID, @ClaimLawsuitStatusID, GETDATE(), 0, @EmailAddress)
	END
	ELSE --insert a new primary status
	BEGIN 
		UPDATE tblClaimLawsuitStatusTracking
		SET IsPrimaryStatus = 0
		WHERE ClaimLawsuitID = @ClaimLawsuitID

		INSERT INTO tblClaimLawsuitStatusTracking (ClaimLawsuitID, ClaimLawsuitStatusID, StatusDate, ProcessedDate, IsPrimaryStatus, SPUsername)
		VALUES(@ClaimLawsuitID, @ClaimLawsuitStatusID, @SettlementDate, GETDATE(), 1, @EmailAddress)
	END
END

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdClaimantSettlementInfo] TO [power_user]
GO
