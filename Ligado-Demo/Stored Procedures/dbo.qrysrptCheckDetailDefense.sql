SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[qrysrptCheckDetailDefense] (@CheckID int)

AS
Set NoCount On

SELECT DI.InvoiceNumber, Period, SubmittedTotal AS InvoiceTotal, PaymentAmount
From tblPaymentDefense pd
Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
Where CheckID = @CheckID

Union

Select lbi.LBInvoiceNumber, Period, (AdjustedTotal+AppealTotal) as InvoiceTotal, PaymentAmount
From tblPaymentLegalBill plb
Inner Join m_tblLegalBillInvoices lbi on lbi.InvoiceID = plb.InvoiceID
Where CheckID = @CheckID


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailDefense] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailDefense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailDefense] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailDefense] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailDefense] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailDefense] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptCheckDetailDefense] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptCheckDetailDefense] TO [power_user]
GO
