SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[qryfrmInternalContactAddressComboBox]
as
set nocount on
select icon.internalcontactid, coalesce(firstname+ ' ', '')  + coalesce(lastname+ '   ', '')  + coalesce([address line 1]+ '   ','')  + COALESCE([address line 2]+ '  ','')  + coalesce(city+ ', ', '')  + coalesce(state, '') as contactmerge, lastname
from tblinternalcompany icom
	inner join tblinternalcontact icon on icom.internalcompanyid = icon.internalcompanyid
group by icon.internalcontactid, coalesce(firstname+ ' ', '')  + coalesce(lastname+ '   ', '')  + coalesce([address line 1]+ '   ','')  + COALESCE([address line 2]+ '  ','')  + coalesce(city+ ', ', '')  + coalesce(state, ''), lastname
order by lastname
return


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInternalContactAddressComboBox] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmInternalContactAddressComboBox] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInternalContactAddressComboBox] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmInternalContactAddressComboBox] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmInternalContactAddressComboBox] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmInternalContactAddressComboBox] TO [power_user]
GO
