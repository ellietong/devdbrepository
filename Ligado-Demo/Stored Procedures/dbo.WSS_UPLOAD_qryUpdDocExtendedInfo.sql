SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] (@DocumentID			  bigint, 
													 @DocumentTypeID		  int, 
													 @DocumentSourceID		  int,  
													 @Accept				  bit, 
													 @EmailAddress			  nvarchar(250), 
													 @ReleaseTypeID			  int = null, 
													 @ReleaseRequestDate	  datetime = null, 
													 @ReleaseSentDate		  datetime = null, 
													 @ExecutedReleaseRcvdDate datetime = null)
AS
SET NOCOUNT ON

UPDATE tblDocuments
	SET ClaimantDocumentTypeID = @DocumentTypeID,
		DocumentSourceID = @DocumentSourceID,
		DateModified = GETDATE(),
		ModifiedBy = @EmailAddress,
		--DismissalProcessDate = CASE WHEN @DocumentTypeID = 16 AND DismissalProcessDate IS NULL THEN GETDATE() WHEN @DocumentTypeID <> 16 AND DismissalProcessDate IS NOT NULL THEN NULL ELSE DismissalProcessDate END,
		--ClaimLawsuitStatusID = CASE WHEN @DocumentTypeID = 16 THEN @DismissalTypeID ELSE NULL END,
		--DismissalDate = CASE WHEN @DocumentTypeID = 16 THEN @DismissalDate ELSE DismissalDate END,
		Accepted = CASE WHEN COALESCE(Accepted, 0) = 1 THEN Accepted WHEN @Accept = 0 AND COALESCE(Accepted, 0) = 0 THEN NULL ELSE 1 END,
		AcceptedBy = Case WHEN AcceptedBy IS NOT NULL THEN AcceptedBy WHEN @Accept = 1 THEN @EmailAddress ELSE NULL END,
		DateAccepted = CASE WHEN DateAccepted IS NOT NULL THEN DateAccepted WHEN @Accept = 1 THEN GETDATE() ELSE NULL END
	WHERE DocumentID = @DocumentID

IF @Accept = 1 AND RIGHT(@EmailAddress, 8) = 'kcic.com'
BEGIN
	UPDATE tblMultiClaimantDocument
		SET QCBy = @EmailAddress,
			QCDate = GETDATE()
			WHERE DocumentID = @DocumentID 	
END		
	
--Create ClaimSettlement records if necessary
INSERT INTO tblClaimSettlement (ClaimLawsuitID)
			SELECT ClaimLawsuitID FROM tblMultiClaimantDocument mcd WHERE DocumentID = @DocumentID 
			AND NOT EXISTS (SELECT ClaimLawsuitID FROM tblClaimSettlement cs WHERE mcd.ClaimLawsuitID = cs.ClaimLawsuitID)
--Update CLaimSettlement records with the provided info
UPDATE tblClaimSettlement 
	SET	ReleaseRqstDate = COALESCE(@ReleaseRequestDate, ReleaseRqstDate),
		ReleaseToPC = COALESCE(@ReleaseSentDate, ReleaseToPC),
		ExecutedReleaseDate = COALESCE(@ExecutedReleaseRcvdDate, ExecutedReleaseDate),
		ReleaseTypeID = COALESCE(@ReleaseTypeID, ReleaseTypeID),
		SPUsername = @EmailAddress
	WHERE ClaimLawsuitID IN (SELECT ClaimLawsuitID FROM tblMultiClaimantDocument WHERE DocumentID = @DocumentID)
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_UPLOAD_qryUpdDocExtendedInfo] TO [power_user]
GO
