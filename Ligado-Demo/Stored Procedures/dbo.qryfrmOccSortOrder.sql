SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmOccSortOrder]
As

SET NOCOUNT ON

SELECT *
FROM tblAllocationSortOrderOptions
ORDER BY SortOrderID

RETURN



GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccSortOrder] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccSortOrder] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccSortOrder] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmOccSortOrder] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmOccSortOrder] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmOccSortOrder] TO [power_user]
GO
