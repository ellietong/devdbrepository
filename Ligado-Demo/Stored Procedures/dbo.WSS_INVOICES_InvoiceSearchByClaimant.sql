SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_INVOICES_InvoiceSearchByClaimant]
(@FirmID int = null, @VendorFirmID int = null, @StatusID bigint = null, @PaidDateStart date = null, @PaidDateEnd date = null, @Period nvarchar(50) = null ,@emailaddress nvarchar(250) = null)
AS

------VARIABLES FOR TESTING----
--DECLARE @FirmID int
--DECLARE @VendorFirmID int
--DECLARE @StatusID bigint
--DECLARE @PaidDateStart nvarchar(50)
--DECLARE @PaidDateEnd nvarchar(50)
--DECLARE @Period nvarchar(50)
--DECLARE @emailaddress nvarchar(250)
--SET @FirmID		   = 5
--SET @VendorFirmID  = NULL
--SET @StatusID	   = NULL
--SET @PaidDateStart = '1/1/2010'
--SET @PaidDateEnd   = '12/31/2010'
--SET @Period		   = NULL
--SET @emailaddress  = 'shockleym@kcic.com'


--These IF statements determine the WHERE by which filters are actually in use, in order to run a modified query that does not include unused filters to increase speed.
DECLARE @WhereString nvarchar(max)
SET @WhereString = ' WHERE d.ClaimantDocumentTypeID in (40, 41)'
IF @FirmID			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.FirmID = ' + CONVERT(nvarchar(20),@FirmID) END
IF @VendorFirmID	IS NOT NULL BEGIN IF @VendorFirmID = 0 SET @WhereString = @WhereString + ' AND d.VendorFirmID <= 10000' ELSE SET @WhereString = @WhereString + ' AND di.VendorFirmID = ' + CONVERT(nvarchar(20),@VendorFirmID) END
IF @StatusID		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND dis.StatusID = ' + CONVERT(nvarchar(20),@StatusID) END
IF @PaidDateStart	IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(c.Sourcedate, CONVERT(date, ''1/1/1900'')) >= ''' + CONVERT(nvarchar(20),@PaidDateStart, 101) + '''' END
IF @PaidDateEnd		IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND COALESCE(c.Sourcedate, CONVERT(date, ''1/1/1900'')) <= ''' + CONVERT(nvarchar(20),@PaidDateEnd, 101) + '''' END
IF @Period			IS NOT NULL BEGIN SET @WhereString = @WhereString + ' AND di.Period LIKE ''%'' + @PeriodParam + ''%''' END

--These IF statements determine additional WHERE statements by the current user, in order to run a modified query that adjusts the results based on what they should be able to see.
DECLARE @FirmByEmailID int
SET @FirmByEmailID = (SELECT FirmID FROM vtblPerson WHERE Email = @emailaddress)
IF ((@firmbyemailid in(188)) OR (LEFT(@emailaddress,5) = 'kcic\') OR (RIGHT(@emailaddress, 11)='kcicllc.com' OR RIGHT(@emailaddress, 8)='kcic.com'))
	SET @WhereString = @WhereString
ELSE
	SET @WhereString = @WhereString + ' AND di.firmid= @FirmByEmailID'

DECLARE @QueryString nvarchar(max)
SET @QueryString = ' 
SELECT CAST(d.DocumentID AS varchar(50)) AS DocumentId, 
 		   d.Comments,
			d.DocumentURL, 
			d.DocumentTitle, 
			d.CreatedBy, 
			d.DateCreated, 
			d.ModifiedBy, 
			d.DateModified,
			d.Accepted, 
			d.AcceptedBy, 
			d.DateAccepted, 
			di.ApprovedBy, 
			di.DateApproved, 
			DefenseCounsel AS Firm, 
			di.InvoiceNumber, 
			di.Period,
			di.DatePayable, 
			di.InvoiceNotes as AdjustmentNotes, 
			dis.Status, 
			'''' AS DefenseInvoiceFirm, 
			c.SourceDate as PaidDate, 
			c.SourceNumber as CheckNumber, 
			dca.DefenseCounselID AS FirmID, 
			
			COALESCE(ex.VendorFirm, '''') AS VendorFirm, 
			c.SourceDate, 
			--sum(coalesce(fees,0)+coalesce(costs,0)-coalesce(adjustments,0)) as InvoiceTotal,
			sum(case when coalesce(CheckFundingSourceID,1) = 3 and coalesce(sourceamount,0)<0 then (dicd.fees*-1) else coalesce(dicd.fees,0) end)+ 
			sum(case when coalesce(CheckFundingSourceID,1) = 3 and coalesce(sourceamount,0)<0 then (dicd.costs*-1) else coalesce(dicd.costs,0) end)-
			sum(case when coalesce(CheckFundingSourceID,1) = 3 and coalesce(sourceamount,0)<0 then (dicd.adjustments*-1) else coalesce(dicd.adjustments,0) end) as InvoiceTotal,
			dicd.claimantpersonalinfoid,
			lastname+'', ''+firstname as Claimant
	FROM tblDocuments d
		LEFT JOIN tblDefenseInvoice di				ON d.DocumentID = di.DocumentID 
		LEFT JOIN tblDefenseInvoiceStatus dis		ON di.StatusID = dis.StatusID 
		LEFT JOIN tblVendorFirm ex					ON di.VendorFirmID = ex.VendorFirmID
		LEFT JOIN tblDefenseCounselAddress dca		ON di.DefenseCounselAddressID = dca.DefenseCounselAddressID
		LEFT JOIN tblPaymentDefense pd				ON di.DefenseInvoiceID = pd.DefenseInvoiceID	
		LEFT JOIN tblDefInvClaimantDetail dicd		ON dicd.DefenseInvoiceID = di.DefenseInvoiceID
		LEFT JOIN (select *
				   from tblCheck
				   WHERE checkfundingtypeid in (1,3)) c	ON pd.CheckID = c.CheckID and dicd.matterID=c.matterID
		LEFT JOIN tblClaimantPersonalInfo cpi		ON cpi.ClaimantPersonalInfoID = dicd.ClaimantPersonalInfoID'

 +	@WhereString + ' 
	GROUP BY CAST(d.DocumentID AS varchar(50)), 
 		   d.Comments,
			d.DocumentURL, 
			d.DocumentTitle, 
			--d.SPDocId, 
			d.CreatedBy, 
			d.DateCreated, 
			d.ModifiedBy, 
			d.DateModified,
			d.Accepted, 
			d.AcceptedBy, 
			d.DateAccepted, 
			di.ApprovedBy, 
			di.DateApproved, 
			dca.DefenseCounsel, 
			di.InvoiceNumber, 
			di.Period,
			di.DatePayable, 
			di.InvoiceNotes, 
			dis.Status, 
			c.SourceDate, 
			c.SourceNumber, 
			dca.DefenseCounselID,
			COALESCE(ex.VendorFirm, ''''), 
			c.SourceDate, 
			dicd.claimantpersonalinfoid,
			lastname+'', ''+firstname 
	Having sum(coalesce(fees,0)+coalesce(costs,0)-coalesce(adjustments,0))<>0
	ORDER BY DateCreated DESC, Accepted ASC' 
	--OPTION (RECOMPILE)'
PRINT @QueryString
--Finally, the query created above is executed.
EXEC sp_executesql @QueryString, N'@PeriodParam nvarchar(max), @FirmByEmailID int', @PeriodParam = @Period, @FirmByEmailID = @FirmByEmailID




GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_InvoiceSearchByClaimant] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_InvoiceSearchByClaimant] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_InvoiceSearchByClaimant] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_InvoiceSearchByClaimant] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_INVOICES_InvoiceSearchByClaimant] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_INVOICES_InvoiceSearchByClaimant] TO [power_user]
GO
