SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCedure [dbo].[qryfrmCheck] (@CheckID int)

As
Set NoCount On

Select * from tblCheckApproval Where CheckID = @CheckID

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheck] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheck] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheck] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheck] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheck] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheck] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheck] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheck] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheck] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCheck] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCheck] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCheck] TO [power_user]
GO
