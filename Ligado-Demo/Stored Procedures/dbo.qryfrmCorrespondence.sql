SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[qryfrmCorrespondence]
AS
SET NOCOUNT ON

SELECT *
FROM tblCorrespondence
Order by CorrespondenceDate
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorrespondence] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorrespondence] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorrespondence] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCorrespondence] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCorrespondence] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCorrespondence] TO [power_user]
GO
