SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmrptAllocationProvisiontoExcludeUnionCombo]
(@provisionLabelID int = Null)
AS
SET NOCOUNT ON

DECLARE @maxProvisionLabelID int
SELECT @maxProvisionLabelID = MAX(ProvisionLabelID) FROM tblProvisionValue

SELECT ProvisionValueID, ProvisionLabelID, ProvisionValue
FROM tblProvisionValue
WHERE ProvisionLabelID >= COALESCE(@provisionLabelID, 0) AND ProvisionLabelID <= COALESCE(NULLIF(@provisionLabelID, 0), @maxProvisionLabelID)

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmrptAllocationProvisiontoExcludeUnionCombo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmrptAllocationProvisiontoExcludeUnionCombo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmrptAllocationProvisiontoExcludeUnionCombo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmrptAllocationProvisiontoExcludeUnionCombo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmrptAllocationProvisiontoExcludeUnionCombo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmrptAllocationProvisiontoExcludeUnionCombo] TO [power_user]
GO
