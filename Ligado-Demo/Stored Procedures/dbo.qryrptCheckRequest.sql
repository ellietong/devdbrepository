SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[qryrptCheckRequest]

(@FundsRequestStartDate datetime, @FundsRequestEndDate datetime)

As

SET NOCOUNT ON

SELECT Firm, PayTo, Period, SubmitDate, InvoiceTotal, FirmAddress, InvoiceNumber, FundsRequestDate
FROM	tblDefenseInvoice DI
INNER JOIN	tblDefenseInvoiceFirm DIF ON DI.FirmID = DIF.FirmID
INNER JOIN	tblDefenseInvoiceStatus DIS ON DI.StatusID = DIS.StatusID
Where FundsRequestDate Between @FundsRequestStartDate and @FundsRequestEndDate

RETURN

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckRequest] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckRequest] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckRequest] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCheckRequest] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCheckRequest] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCheckRequest] TO [power_user]
GO
