SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryfrmClaimLawsuitInfoForEmploymentAndProducts] (@ClaimantPersonalInfoID int)
as

Select c.ClaimID, cl.ClaimLawsuitID, cl.LawsuitID	
From tblClaim c
	INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID
Order By IsPrimaryForClaim

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitInfoForEmploymentAndProducts] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitInfoForEmploymentAndProducts] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitInfoForEmploymentAndProducts] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitInfoForEmploymentAndProducts] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimLawsuitInfoForEmploymentAndProducts] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimLawsuitInfoForEmploymentAndProducts] TO [power_user]
GO
