SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE Procedure [dbo].[qryrptAllocationByClaimDetails] (@ExhaustionDateID int, @LimitTypeID int, @carrierID int = NULL, @matterid int = NULL)
As
Set NoCount on



----------------------------------------------------------------------------------------------------------
--Declare @ExhaustionDateID int
--Set @ExhaustionDateID = 9
 
--Declare @LimitTypeID int
--Set @LimitTypeID=1

--DECLARE @carrierID int 
--SET @carrierID = null

--DECLARE @matterid int 
--SET @matterid = null
------------------------------------------------------------------------------------------------------------
DECLARE @maxCarrierID int
SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy

Declare @maxMatterID int
Select @maxmatterid = max(matterid) from tblmatter

Declare @FirstPolicyStartDate table (occurrenceid int, FirstPolicyStartDate date)
insert into @FirstPolicyStartDate
Select occurrenceid, min(pstartdate) 
from tblpolicydates d inner join tblexhaustion e on e.policydateid = d.policydateid
where exhaustiondateid = @ExhaustionDateID and d.policydateid not in (3098, 3099) -- exclude producer policies
group by occurrenceid



Select '' as AllocationDescription, 
	e.PolicyDateID,
	o.ClaimID, 
	triggerstart1 as EarliestExposureDate, 
	triggerend1 as DiagnosisDate, 
	o.OccurrenceID, 
	OccurrenceGroup, 
	OccurrenceName, 
	ot.OccurrenceTypeID, 
	ot.occurrencetype,
	convert(money,(O.Cost1 + O.Cost3)) as CostAllocated, 
	PolicyNum,
	Case when p.carrierid = 10 then 'General Signal (SPX)' else CarrierName end as carriername,
	Case when p.policydateid = 3098 then triggerstart1 
		 when p.policydateid = 3099 then NULL
		 else Min(PStartDate) end as MinStartDate, 
	Case when p.policydateid = 3098 then min(FirstPolicyStartDate)
		 when p.policydateid = 3099 then NULL
		 else Max(PEndDate) end as MaxEndDate, 
	coalesce(Layer,0)+1 as Layer, 
	SUM(IndemnityAllocation) as AllocationIndemnity, 
	SUM(DefenseAllocation) as AllocationDefense, 
	SUM(SIRIndemnityAllocation+SIRDefenseAllocation) as SIRAllocated, 
	0 as Adjustment, 
	SUM(DefenseOutsideLimitsAllocation) as AllocationDefenseOutsideLimits,
	Matter, 
	cl.ClaimLawsuitID, 
	SettlementDate,
	ed.Description
From tblExhaustion e
	INNER JOIN vPolicy p ON e.PolicyDateID = p.PolicyDateID
	Inner Join tblOccurrence O on e.OccurrenceID=O.OccurrenceID
	Inner Join tblOccurrenceGroup OG on OG.OccurrenceGroupID=O.OccurrenceGroupID
	Left Join tblOccurrenceType ot on o.OccurrenceTypeID = ot.OccurrenceTypeID
	left join tblclaimsettlement cs on cs.ClaimLawsuitID = o.ClaimID
	left join tblclaimlawsuit cl on cl.claimlawsuitid = o.claimid
	left join tblclaim c on c.claimid = cl.claimid
	left join tblmatter m on m.matterid = c.matterid
	Left join @FirstPolicyStartDate f on f.occurrenceid = e.OccurrenceID
	Left Join tblExhaustionDate ed on ed.ExhaustionDateID = e.ExhaustionDateID
Where coalesce(p.LimitTypeID,1) = @LimitTypeID
	 AND e.ExhaustionDateID = @ExhaustionDateID
	 AND CarrierID >= COALESCE(@carrierID, 0) AND CarrierID <= COALESCE(NULLIF(@carrierID, 0), @maxCarrierID)
	 AND c.MatterID >= COALESCE(@matterid, 0) AND c.Matterid <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
GROUP BY o.ClaimID, triggerstart1, triggerend1, o.OccurrenceID, OccurrenceGroup, 
	OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
	O.Cost1 + O.Cost3, PolicyNum,
	Case when p.carrierid = 10 then 'General Signal (SPX)' else CarrierName end, 
	Layer, matter, e.PolicyDateID, cl.ClaimLawsuitID,
	p.PolicyDateID, settlementdate,ed.Description
	
Order by matter
	
RETURN









GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetails] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetails] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetails] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetails] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetails] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetails] TO [power_user]
GO
