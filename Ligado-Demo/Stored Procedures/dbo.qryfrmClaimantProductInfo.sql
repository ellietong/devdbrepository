SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmClaimantProductInfo] (@ClaimLawsuitID int)
as

Select ClaimLawsuitProductID, 
	ClaimLawsuitID, 
	ProductName, 
	ProductBrand, 
	ProductType, 
	ProductManufacturer, 
	StartDate, 
	EndDate
From tblClaimLawsuitProduct clp
	LEFT JOIN tblProduct p ON clp.ProductID = p.ProductID
	LEFT JOIN tblProductBrand pb ON p.ProductBrandID = pb.ProductBrandID
	LEFT JOIN tblProductManufacturer pm ON p.ProductManufacturerID = pm.ProductManufacturerID
	LEFT JOIN tblProductName pn ON p.ProductNameID = pn.ProductNameID
	LEFT JOIN tblProductType pt ON p.ProductTypeID = pt.ProductTypeID
Where ClaimLawsuitID = @ClaimLawsuitID 
Order By ProductType, ProductName

Return

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantProductInfo] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantProductInfo] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantProductInfo] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantProductInfo] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmClaimantProductInfo] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmClaimantProductInfo] TO [power_user]
GO
