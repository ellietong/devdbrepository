SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmCarrierList]
(@carrierGroupID int = NULL)
AS
SET NOCOUNT ON
IF @carrierGroupID IS NOT NULL
	SELECT CarrierID, CarrierName + ' - ' + CASE WHEN CarrierGroupName IS NULL THEN CarrierName ELSE CarrierGroupName END as carriername, CarrierSolvency, PolicyHolder, c.CarrierGroupID, CarrierNote, SolvencySource, DateInsolvent
	FROM tblCarrier c LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID
	WHERE c.CarrierGroupID = @carrierGroupID
	ORDER BY CarrierName
ELSE
	SELECT CarrierID, CarrierName + ' - ' + CASE WHEN CarrierGroupName IS NULL THEN CarrierName ELSE CarrierGroupName END as carriername, CarrierSolvency, PolicyHolder, c.CarrierGroupID, CarrierNote, SolvencySource, DateInsolvent
		FROM tblCarrier c LEFT JOIN tblCarrierGroup cg ON c.CarrierGroupID = cg.CarrierGroupID
	ORDER BY CarrierName
RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierList] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierList] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierList] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierList] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierList] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierList] TO [power_user]
GO
