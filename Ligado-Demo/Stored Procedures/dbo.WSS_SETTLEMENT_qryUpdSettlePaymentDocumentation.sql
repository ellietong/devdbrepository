SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] (@ClaimSettlementID int, 
																	@SettlePaymentComments nvarchar(max), 
																	@ReleaseTypeID int,
																	@ExecutedReleaseDate date, 
																	@MedicareReported bit, 
																	@ReleaseToPC datetime,
																	@ReleaseRqstDate date)
AS

--DECLARE @ClaimID int
--SET @ClaimID = 1

UPDATE tblClaimSettlement
	SET SettlePaymentComments = @SettlePaymentComments, 
		ReleaseTypeID = CASE WHEN @ReleaseTypeID = 0 THEN NULL ELSE @ReleaseTypeID END, 
		ReleaseSignedDate = NULLIF(@ExecutedReleaseDate,''),
		ReleaseRqstDate = NULLIF(@ReleaseRqstDate,''), 
		MedicareReported = @MedicareReported,
		ReleaseSentDate = NULLIF(@ReleaseToPC, '')
	WHERE ClaimSettlementID = @ClaimSettlementID
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryUpdSettlePaymentDocumentation] TO [power_user]
GO
