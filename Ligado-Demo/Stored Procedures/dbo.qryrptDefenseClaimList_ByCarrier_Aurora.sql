SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] (@ExhaustionDateID INT)
AS
SET NOCOUNT ON

--Declare @ExhaustionDateID int
--Set @ExhaustionDateID = 24

--select * from tblExhaustionDate


--Pull allocation amounts for requested Carrier
DECLARE @CarrierAllocation TABLE (OccurrenceID INT, AllocatedAmount FLOAT,CarrierGroupID INT ,CarrierGroupName NVARCHAR (100))
INSERT INTO @CarrierAllocation
SELECT OC.OccurrenceID, SUM(e.DefenseAllocation) + SUM(e.DefenseOutsideLimitsAllocation) AS AllocatedAmount, cg.CarrierGroupID ,CarrierGroupName
FROM tblExhaustion E
JOIN tblOccurrence OC ON OC.OccurrenceID = E.OccurrenceID
INNER JOIN tblPolicyDates pd ON pd.PolicyDateID = e.PolicyDateID
JOIN tblPolicy P ON p.PolicyID = pd.PolicyID
JOIN tblCarrier C ON c.CarrierID = p.CarrierID
INNER JOIN tblcarriergroup cg ON cg.CarrierGroupID = c.CarrierGroupID
WHERE e.ExhaustionDateID = @ExhaustionDateID 
GROUP BY OC.OccurrenceID, cg.CarrierGroupID,CarrierGroupName

--Select distinct carrierGroupID,carrierGroupName from @CarrierAllocation

--select Sum(AllocatedAmount) from @CarrierAllocation
--select * from tbloccurrence
--select * from tblexhaustion
--Select * from tbldefinvclaimantdetail
--select * from vtblFirm
--select * from tblDefenseInvoice


SELECT di.defenseinvoiceid, COALESCE(vf.VendorFirm,dc.defensecounsel) AS Firm, 
--firm as Vendor, 
dic.ClaimLawsuitID AS ClaimantLawsuitID, 
lastname+', '+firstname AS ClaimantName, 
'XXX-XX-'+RIGHT(COALESCE(socialsecuritynumber,''),4) AS socialsecuritynumber, 
CASE WHEN s.state = 'ZZ' THEN '' ELSE s.state END AS State, 
jurisdiction, 
LTRIM(RTRIM(dic.invoicenumber)) AS invoicenumber, 
SUM(CASE WHEN CarrierGroupID = 13 THEN AllocatedAmount ELSE 0 END) AS Hartford, 
SUM(CASE WHEN CarrierGroupID = 30 THEN AllocatedAmount ELSE 0 END) AS AllState,
SUM(CASE WHEN CarrierGroupID = 31 THEN AllocatedAmount ELSE 0 END) AS ACE,
SUM(CASE WHEN CarrierGroupID = 47 THEN AllocatedAmount ELSE 0 END) AS Liberty,
AdjustedTotal AS AdjustedTotal, 
o.TriggerStart1 AS DOFE,
--Coalesce(convert(money,allocatedamount), 0) as AllocAmount, 
cost1,
--coalesce(CarrierGroupName,'') as CarrierGroupName, 
OccurrenceTypeID,
cdt.ClaimantDocumentType,
(dic.Fees + dic.costs) AS InvoiceTotal,
Matter
FROM tbldefenseinvoice di
INNER JOIN tbldefinvclaimantdetail dic ON dic.DefenseInvoiceID = di.DefenseInvoiceID
--inner join tblpaymentdefense pd on pd.defenseinvoiceid = di.defenseinvoiceid
--inner join tblcheck ch on ch.checkid = pd.checkid
LEFT JOIN tblclaimlawsuit cl ON cl.ClaimLawsuitID = dic.ClaimLawsuitID
LEFT JOIN tblclaim c ON c.claimid = cl.ClaimID
LEFT JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
LEFT JOIN tbljurisdiction j ON j.jurisdictionid = l.jurisdictionid
LEFT JOIN tblstate s ON s.stateid = l.stateid
LEFT JOIN tblVendorFirm vf ON vf.VendorFirmID=di.VendorFirmID
LEFT JOIN tblDefenseCounsel dc ON dc.DefenseCounselID=di.VendorFirmID
--left join tbldefensecounseladdress dca on dca.defensecounseladdressid = di.defensecounseladdressid
--left join vtblFirm v on v.FirmID = di.VendorFirmID
LEFT JOIN tblOccurrence o ON dic.InvoiceDetailID = o.OccurrenceNote
--left join (select * from tbloccurrence where occurrencegroupid = @OccurrenceGroupID and occurrencetypeid = 2) o on o.claimid = dic.claimantpersonalinfoid and 
--		  --abs(round(o.cost1,2)) = abs(round((fees+costs-adjustments),2)) and di.defenseinvoiceid = convert(int,o.occurrencenote)
--		  abs(o.cost1) = abs(fees+costs-adjustments) and di.defenseinvoiceid = convert(int,o.occurrencenote)
--and o.occurrencename <> dic.invoicenumber
INNER JOIN @carrierallocation ca ON ca.occurrenceid = o.occurrenceid
LEFT JOIN tblDocuments d ON d.DocumentID = di.DocumentID
LEFT JOIN tblClaimantDocumentType cdt ON cdt.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
LEFT JOIN tblMatter m ON m.MatterID = c.MatterID
--where --sourcedate between '6/1/2013' and '6/30/2013'
GROUP BY di.defenseinvoiceid, COALESCE(vf.VendorFirm,dc.defensecounsel), 
--firm, 
dic.ClaimLawsuitID , lastname+', '+firstname , 
'XXX-XX-'+RIGHT(COALESCE(socialsecuritynumber,''),4) , s.state, jurisdiction, LTRIM(RTRIM(dic.invoicenumber)) , 
adjustedTotal,
o.TriggerStart1,
--Coalesce(convert(money,allocatedamount), 0) , 
cost1,
--coalesce(CarrierGroupName,''), 
OccurrenceTypeID,
cdt.ClaimantDocumentType,
(dic.Fees + dic.costs),
Matter
 HAVING SUM(fees+costs-adjustments)<>0
--compute sum(case when cost1<0 then (fees+costs-adjustments)*1 else (fees+costs-adjustments) end), sum(Coalesce(convert(money,allocatedamount), 0))








GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptDefenseClaimList_ByCarrier_Aurora] TO [power_user]
GO
