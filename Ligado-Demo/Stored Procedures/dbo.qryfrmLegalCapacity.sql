SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[qryfrmLegalCapacity]
as
Select LegalCapacityID, LegalCapacity
From tblLegalCapacity
Order By LegalCapacity

Return

GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLegalCapacity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmLegalCapacity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLegalCapacity] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmLegalCapacity] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmLegalCapacity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmLegalCapacity] TO [power_user]
GO
