SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs]  (@emailaddress nvarchar(250), @SettlementGroupID int = NULL, @PlaintiffCounselID int = NULL)--, @SettlementDate datetime)

As

SET NOCOUNT ON

--DECLARE @MatterID int
--SET @MatterID = 4
--DECLARE @EmailAddress nvarchar(250) = 'crookc@kcicllc.com
--'
--DECLARE @SettlementGroupID int = 1

DECLARE @MaxSettlementGroupID int = (SELECT MAX(SettlementGroupID) FROM tblClaimSettlementGroup)
DECLARE @MaxPlaintiffCounselID int = (SELECT MAX(PlaintiffCounselID) FROM tblPlaintiffCounsel)

DECLARE @PCAs TABLE (PlaintiffCounselAddressID bigint)
INSERT INTO @PCAs
SELECT DISTINCT PlaintiffCounselAddressID FROM tblPlaintiffCounselAddress 
WHERE COALESCE(@PlaintiffCounselID, 0) <= PlaintiffCounselID AND COALESCE(@PlaintiffCounselID, @MaxPlaintiffCounselID) >= PlaintiffCounselID

DECLARE @SettlementDate datetime
SET @SettlementDate = '4/1/2011'--DATEADD(mm, -18, GETDATE())

--DECLARE @PlaintiffCOunselAddressID bigint = (SELECT PlaintiffCOunselAddressID FROM vtblPerson WHERE Email = @EmailAddress)
--PRINT @PlaintiffCounseladdressid
--IF(@PlaintiffCOunselAddressID IS NOT NULL)
--BEGIN

--PRINT 'test'
--SELECT cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, '') As ClaimantName, 
--cl.ClaimLawsuitID,
--c.ClaimantPersonalInfoID,
--cl.LawsuitID,
--PlaintiffCounsel,
--SettlementDate,
--UPPER(SettlementGroup) AS SettlementGroup,
--cs.SettlementGroupID
--FROM tblClaim c
--	LEFT JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
--	LEFT JOIN tblMatter m		ON c.MatterID = m.MatterID	
--	LEFT JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
--	LEFT JOIN tblClaimantSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
--	LEFT JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
--	LEFT JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID	
--	LEFT JOIN tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
--	LEFT JOIN tblSettlementGroup s	ON cs.SettlementGroupID = s.SettlementGroupID
--	--LEFT JOIN tblClaimantDocketNumbers cd		ON cpi.ClaimantPersonalInfoID = cd.ClaimantPersonalInfoID
--	LEFT JOIN tblSettlePaymentDocumentation spd ON cl.ClaimLawsuitID = spd.ClaimLawsuitID	
--	LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
	
----WHERE --COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
--		--AND clst.ClaimLawsuitStatusID = 23
--	--	AND StatusDate >= @SettlementDate
--	--	AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimantSettlementID = ps.ClaimSettlementID)
--	--	AND l.PlaintiffCounselAddressID = @PlaintiffCOunselAddressID
--	--	AND cs.SettlementGroupID >= COALESCE(@SettlementGroupID, 0) AND cs.SettlementGroupID <= COALESCE(@SettlementGroupID, @MaxSettlementGroupID)
--GROUP BY	cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''), 
--			cl.ClaimLawsuitID,
--			c.ClaimantPersonalInfoID,
--			cl.LawsuitID,
--			PlaintiffCounsel,
--			SettlementDate,
--			SettlementGroup,
--			cs.SettlementGroupID
--ORDER BY	SettlementDate desc, 
--			SettlementGroup,
--			cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, '')
--END
--ELSE
--BEGIN

	DECLARE @MissingDocuments TABLE (ClaimLawsuitID bigint, MissingDocumentation nvarchar(MAX))
	--INSERT INTO @MissingDocuments
	--SELECT ClaimLawsuitID, 
	--	CAST((SELECT '', 'td' = 'Death Certificate',
	--			'', 'td' = 'Estate Papers'
	--		FOR XML PATH('tr'), ROOT('table'))AS Text) 
	--FROM tblClaimLawsuit 
		
		
		

	DECLARE @ClaimantDocs TABLE(ClaimLawsuitID bigint, DODCert int, EstatePapers int, SettlementConfirmation int, 
								MedicarePlaintiff int, MedicarePlaintiffCounsel int, 
								MedicareFormB int, MMSEA int)

	--DECLARE @MissingDocuments TABLE(ClaimLawsuitID bigint, MissingDocumentation nvarchar(100))
	
	INSERT INTO @ClaimantDocs
		SELECT --sp.ClaimantIDed, sp.DxMedicals, 
			cl.ClaimLawsuitID,
			CASE WHEN DeceasedDate IS NULL THEN 2 ELSE csd.DODCert END AS DODCert, 
			CASE WHEN DeceasedDate IS NULL THEN 2 ELSE csd.EstatePapers END AS EstatePapers,
			--sp.ProductID, 
			--sp.EmployExpoHist, 
			csd.settlementconfirmation, 
			csd.MedicarePlaintiff,
			csd.MedicarePlaintiffCounsel,
			1 AS MedicareFormB,
			csd.MMSEA
			--CASE WHEN MedicareFullyDocumented IS NOT NULL THEN 1 ELSE 0 END
		FROM tblClaim c
			LEFT JOIN tblClaimantPersonalInfo cpi		 ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID		
			LEFT JOIN tblClaimLawsuit cl				 ON c.ClaimID = cl.ClaimID
			LEFT JOIN tblClaimSettlementDocumentation csd ON cl.ClaimLawsuitID = csd.ClaimLawsuitID	
			LEFT JOIN tblClaimSettlement cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
			INNER JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
			INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
			WHERE SettlementGroupID >= COALESCE(@SettlementGroupID, 0) AND SettlementGroupID <= COALESCE(@SettlementGroupID, @MaxSettlementGroupID)
			AND l.PlaintiffCounselAddressID IN (SELECT PlaintiffCounselAddressID FROM @PCAs)
			AND COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
			AND clst.ClaimLawsuitStatusID = 23
			AND StatusDate >= @SettlementDate
			AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimSettlementID = ps.ClaimSettlementID)
		--WHERE cl.ClaimLawsuitID = @ClaimLawsuitID
		
	--GET PC OF CLAIM LAWSUIT
	DECLARE @UserPlaintiffCounselID bigint = (	SELECT TOP 1 PlaintiffCounselID 
											FROM tblClaimLawsuit cl
											LEFT JOIN tblLawsuit l ON cl.LawsuitID = l.LawsuitID
											INNER JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID)-- WHERE cl.ClaimLawsuitID = @ClaimLawsuitID)
		
			
	--DOCUMENTATION REQUIRED FOR ALL PLAINTIFFS COUNSEL
	INSERT INTO @MissingDocuments
		SELECT ClaimLawsuitID, 'Death Certificate' AS MissingDocName
		FROM @ClaimantDocs cd
		WHERE DODCert IS NULL Or DODCert = 0
	INSERT INTO @MissingDocuments
		SELECT ClaimLawsuitID, 'Estate Paperwork' AS MissingDocName
		FROM @ClaimantDocs
		WHERE EstatePapers IS NULL Or EstatePapers = 0
	INSERT INTO @MissingDocuments
		SELECT mcd.ClaimLawsuitID, 'Release Request Form' AS MissingDocName
		--WHERE NOT EXISTS (
		--	SELECT d.DocumentID
			FROM tblDocuments d
			INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
			LEFT JOIN (SELECT ClaimLawsuitID, ReleaseRqstDate FROM tblClaimSettlement) spd ON mcd.ClaimLawsuitID = spd.ClaimLawsuitID
			WHERE --ClaimLawsuitID = @ClaimLawsuitID
					 ClaimantDocumentTypeID = 32
			AND ReleaseRqstDate IS NULL		
	INSERT INTO @MissingDocuments
		SELECT mcd.ClaimLawsuitID, 'Unexecuted Release' AS MissingDocName
		--WHERE NOT EXISTS (
		--	SELECT d.DocumentID
			FROM tblDocuments d
			INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
			WHERE --ClaimLawsuitID = @ClaimLawsuitID
					 ClaimantDocumentTypeID = 55--54 = unexecuted release on dev, 55 on prod
		AND (SELECT ReleaseToPC FROM tblClaimSettlement WHERE ClaimLawsuitID = mcd.ClaimLawsuitID) IS NULL

	INSERT INTO @MissingDocuments
			SELECT mcd.ClaimLawsuitID, 'Executed Release' AS MissingDocName
			--WHERE NOT EXISTS (
				--SELECT d.DocumentID
				FROM tblDocuments d
				INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
				WHERE --ClaimLawsuitID = @ClaimLawsuitID
						 ClaimantDocumentTypeID = 31
			AND (SELECT ExecutedReleaseDate FROM tblClaimSettlement WHERE ClaimLawsuitID = mcd.ClaimLawsuitID) IS NULL
						
						
						
	--Load firms that need MMSEA form
	DECLARE @MMSEA TABLE(PlaintiffCounselID int)
	INSERT INTO @MMSEA  --, Keller, Mulvihill respectively
	SELECT 643 --Deaton
	UNION
	SELECT 519 --Keller
	UNION
	SELECT 952 --Mulvihill

	--Load firms that need Medicare Form B
	DECLARE @FormB TABLE(PlaintiffCounselID int)
	INSERT INTO @FormB  
	SELECT 26 --Cooney & Conway
	UNION
	SELECT 257 --Kazan
	UNION
	SELECT 294 --O'Brien

	--FIRMS WITH REP of PLAINTIFF
	DECLARE @RepOfPlaintiff TABLE(PlaintiffCounselID int)
	INSERT INTO @RepOfPlaintiff  
	SELECT 643 --Deaton
	UNION
	SELECT 952 --Mulvihill
	UNION
	SELECT 150 --Weitz
	UNION
	SELECT 519 --Keller



	--FIRMS WITH REP of PLAINTIFF COUNSEL
	DECLARE @RepOfPlaintiffCounsel TABLE(PlaintiffCounselID int)
	INSERT INTO @RepOfPlaintiffCounsel  
	SELECT 150 --Weitz

	--MMSEA
	IF(@userplaintiffcounselid IN (SELECT PlaintiffCounselID FROM @MMSEA))
	BEGIN			
		INSERT INTO @MissingDocuments
		SELECT ClaimLawsuitID, 'MMSEA Settlement Form' AS MissingDocName
		FROM @ClaimantDocs
		WHERE MMSEA IS NULL Or MMSEA = 0
	END

	--FORM B
	IF(@userplaintiffcounselid IN (SELECT PlaintiffCounselID FROM @FormB))
	BEGIN			
		INSERT INTO @MissingDocuments
		SELECT ClaimLawsuitID, 'Medicare Form B' AS MissingDocName
		FROM @ClaimantDocs
		WHERE MedicareFormB IS NULL Or MedicareFormB = 0
	END

	--REP OF PLAINTIFF
	IF(@userplaintiffcounselid IN (SELECT PlaintiffCounselID FROM @RepOfPlaintiff))
	BEGIN			
		INSERT INTO @MissingDocuments
		SELECT ClaimLawsuitID, 'Representation of Plaintiff' AS MissingDocName
		FROM @ClaimantDocs
		WHERE MedicarePlaintiff IS NULL Or MedicarePlaintiff = 0
	END

	--REP OF PLAINTIFF COUNSEL
	IF(@userplaintiffcounselid IN (SELECT PlaintiffCounselID FROM @RepOfPlaintiffCounsel))
	BEGIN			
		INSERT INTO @MissingDocuments
		SELECT ClaimLawsuitID, 'Representation of Plaintiff Counsel' AS MissingDocName
		FROM @ClaimantDocs
		WHERE MedicarePlaintiffCounsel IS NULL Or MedicarePlaintiffCounsel = 0
	END


	--DEFAULT: ALL OTHER PLAINTIFFS COUNSEL (ie: those not in the tables constructed above)
	IF(@userplaintiffcounselid NOT IN (	SELECT PlaintiffCounselID FROM @MMSEA
									UNION
									SELECT PlaintiffCounselID FROM @FormB
									UNION
									SELECT PlaintiffCounselID FROM @RepOfPlaintiff
									UNION
									SELECT PlaintiffCounselID FROM @RepOfPlaintiffCounsel
	))
	BEGIN
		--MMSEA
		INSERT INTO @MissingDocuments
		SELECT ClaimLawsuitID, 'MMSEA Settlement Form' AS MissingDocName
		FROM @ClaimantDocs
		WHERE MMSEA IS NULL Or MMSEA = 0
		
		--REP OF PLAINTIFF	
		INSERT INTO @MissingDocuments
		SELECT ClaimLawsuitID, 'Representation of Plaintiff' AS MissingDocName
		FROM @ClaimantDocs
		WHERE MedicarePlaintiff IS NULL Or MedicarePlaintiff = 0

		--REP OF PLAINTIFF COUNSEL	
		INSERT INTO @MissingDocuments
		SELECT ClaimLawsuitID, 'Representation of Plaintiff Counsel' AS MissingDocName
		FROM @ClaimantDocs
		WHERE MedicarePlaintiffCounsel IS NULL Or MedicarePlaintiffCounsel = 0

	END


	DECLARE @MissingDocumentation TABLE (ClaimLawsuitID bigint, MissingDocumentation nvarchar(MAX))
	INSERT INTO @MissingDocumentation
	SELECT ClaimLawsuitID, 
		CAST((SELECT '', 'td' = MissingDocumentation
		FROM @MissingDocuments md 
		WHERE md.ClaimLawsuitID = mdouter.ClaimLawsuitID
			FOR XML PATH('tr'), ROOT('table'))AS Text)
	FROM @MissingDocuments mdouter
	GROUP BY ClaimLawsuitID
		
	--SELECT * FROM @MissingDocumentation
	SELECT 
	cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, '') As ClaimantName, 
	cs.SettlementDate,
    UPPER(COALESCE(SettlementGroup, 'Unknown')) AS SettlementGroup,
	COALESCE(PlaintiffCounsel, 'Unknown') AS PlaintiffCounsel,
	MissingDocumentation,
	cl.ClaimLawsuitID,
	c.ClaimantPersonalInfoID,
	cl.LawsuitID,
	cs.SettlementGroupID
	FROM tblClaim c
		LEFT JOIN tblClaimantPersonalInfo cpi		ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
		LEFT JOIN tblMatter m		ON c.MatterID = m.MatterID	
		LEFT JOIN tblClaimLawsuit cl		ON c.ClaimID = cl.ClaimID
		LEFT JOIN tblClaimSettlement cs		ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
		LEFT JOIN tblLawsuit l		oN l.LawsuitID = cl.LawsuitID
		LEFT JOIN tblClaimLawsuitStatusTracking clst		ON cl.ClaimLawsuitID = clst.ClaimLawsuitID	
		LEFT JOIN tblClaimLawsuitStatus cst on cst.ClaimLawsuitStatusID = clst.ClaimLawsuitStatusID
		LEFT JOIN tblClaimSettlementGroup csg	ON cs.SettlementGroupID = csg.SettlementGroupID
		LEFT JOIN tblPlaintiffCounselAddress pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID
		--INNER JOIN @MissingDocuments md ON cl.ClaimLawsuitID = md.ClaimLawsuitID
		LEFT JOIN @MissingDocumentation md ON cl.ClaimLawsuitID = md.ClaimLawsuitID
	WHERE COALESCE(IsCurrentClaim,0) = 1 and coalesce(clst.isprimarystatus,0) = 1
			AND clst.ClaimLawsuitStatusID = 23
			AND StatusDate >= @SettlementDate
			AND NOT EXISTS (SELECT PaymentSettlementID FROM tblPaymentSettlement ps WHERE cs.ClaimSettlementID = ps.ClaimSettlementID)
			AND cs.SettlementGroupID >= COALESCE(@SettlementGroupID, 0) AND cs.SettlementGroupID <= COALESCE(@SettlementGroupID, 10000)--@MaxSettlementGroupID)
			AND l.PlaintiffCounselAddressID IN (SELECT PlaintiffCounselAddressID FROM @PCAs)
	GROUP BY	cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, ''), 
				cl.ClaimLawsuitID,
				c.ClaimantPersonalInfoID,
				cl.LawsuitID,
				PlaintiffCounsel,
				SettlementDate,
				SettlementGroup,
				cs.SettlementGroupID,
				MissingDocumentation
	ORDER BY	SettlementDate desc, 
				SettlementGroup,
				cpi.LastName + ', ' + FirstName + COALESCE(' ' + MIddleName, '')
--END
GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_SETTLEMENT_qryOutstandingSettlements_WithIDs] TO [power_user]
GO
