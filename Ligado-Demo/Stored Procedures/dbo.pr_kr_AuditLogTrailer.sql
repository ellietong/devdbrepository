SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pr_kr_AuditLogTrailer] (@AuditLogID int,
                                            @TableID int,
                                            @RowsAffected int,
                                            @Event char(1),
                                            @UserValue sysname )
AS
BEGIN
  set nocount on

  if @UserValue is null
    select @UserValue = suser_sname()

  insert AuditLog(AuditLogID, TableID, RowsAffected, Event, UserName)
    values(@AuditLogID, @TableID, @RowsAffected, @Event, @UserValue)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditLogTrailer] TO [base_user]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogTrailer] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditLogTrailer] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogTrailer] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogTrailer] TO [omniaudit_admin]
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditLogTrailer] TO [power_user]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogTrailer] TO [power_user]
GO
