SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptCashActivity] (@BalanceStartDate datetime, @BalanceEndDate datetime)

As
Set NoCount On

--Declare  @BalanceStartDate datetime, @BalanceEndDate datetime
--Set @BalanceStartDate = '3/15/14'
--Set @BalanceEndDate = '8/1/14'

Declare @MinBalanceStartDate datetime
Set @MinBalanceStartDate = (Case when @BalanceStartDate < '3/14/2011' then '3/14/2011' Else @BalanceStartDate END)


Declare @StartingBalance money
Set @StartingBalance = (
Coalesce((Select SUM(CashActivityAmount) From tblMiscCashActivity mca 
Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID = mcaa.MiscCashActivityID 
where CashActivityDate >= '3/14/2011' and CashActivityDate < @MinBalanceStartDate),0)
+
Coalesce((Select SUM(SourceAmount)*-1 From tblCheck c
Inner Join tblCheckApproval ca on c.CheckID = ca.CheckID
Where SourceDate >= '3/14/2011' and SourceDate < @MinBalanceStartDate),0)
)

Declare @Detail table (Matter nvarchar(250), FundingType nvarchar(250), FundingSource nvarchar(250), SourceDate datetime, SourceNumber nvarchar(250), 
Indemnity money, Defense money, MiscFees money, CashReceipts money, Comment nvarchar(Max), CheckStatus bit)
Insert Into @Detail

--Defense
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, 0 as Indemnity, SourceAmount*-1 as Defense, 0 as MiscFees, 0, 'ACH wire payment of '+Coalesce(vendorfirm, DefenseCounsel)+' Expert Invoice(s)' as Comment, IsCheckCleared
From tblCheck c
	Inner Join tblCheckApproval ca on ca.CheckID = c.CheckID
	Inner Join tblPaymentDefense pd on pd.CheckID = c.CheckID
	Inner Join tblDefenseInvoice di on di.DefenseInvoiceID = pd.DefenseInvoiceID
	Left Join tblDefenseCounselAddress dca on di.DefenseCounselAddressID = dca.DefenseCounselAddressID
	left join tblVendorFirm V on V.VendorFirmID = di.VendorFirmID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = c.checkFundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = c.checkFundingTypeID
Where (SourceDate >= @MinBalanceStartDate and SourceDate <= @BalanceEndDate) AND c.CheckFundingTypeID in (1,3,4)
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, SourceAmount*-1, Coalesce(vendorfirm,DefenseCounsel), IsCheckCleared


Union ALL

--Settlement
Select Matter, FundingType, FundingSource, SourceDate, SourceNumber, sum(paymentamount)*-1 as Indemnity, 0, 0, 0, 'ACH wire payment of '+PlaintiffCounsel+' settlement(s) - '+FullName, IsCheckCleared
From tblCheck ch
	Inner Join tblCheckApproval ca on ca.CheckID = ch.CheckID
	Inner Join tblPaymentSettlement ps on ps.CheckID = ch.CheckID
	Inner Join tblClaimSettlement cs on ps.ClaimSettlementID = cs.ClaimSettlementID
	Inner Join tblClaimLawsuit cl on cl.ClaimLawsuitID = cs.ClaimLawsuitID
	inner join tblclaim c on c.ClaimID=cl.ClaimID
    inner join tblClaimantPersonalInfo cpi on cpi.ClaimantPersonalInfoID=c.ClaimantPersonalInfoID
	Inner Join tblLawsuit l on l.LawsuitID = cl.LawsuitID
	Inner Join tblPlaintiffCounselAddress pca on pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
	Inner Join tblMatter m on c.MatterID = m.MatterID
	Inner Join tblCheckFundingSource cfs on cfs.FundingSourceID = ch.CheckFundingSourceID
	Inner Join tblCheckFundingType cft on cft.FundingTypeID = ch.checkFundingTypeID
Where (SourceDate >= @MinBalanceStartDate and SourceDate <= @BalanceEndDate) AND ch.checkFundingTypeID in (1, 2)
Group by Matter, FundingType, FundingSource, SourceDate, SourceNumber, PlaintiffCounsel, IsCheckCleared,FullName

Union ALL

Select coalesce(Matter, 'All'), CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, CashActivityAmount as MiscFees, 0 as CashReceipts, CashActivityNotes, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	left Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @MinBalanceStartDate and CashActivityDate <= @BalanceEndDate) AND CashActivityAmount < 0 --AND mcat.CashActivityTypeID not in (3,4,6)

Union ALL

Select coalesce(Matter, 'All'), CashActivityType, 'N/A', CashActivityDate, 'N/A', 0, 0, 0, CashActivityAmount, CashActivityNotes, Null
From tblMiscCashActivity mca
	Inner Join tblMiscCashActivityApproval mcaa on mca.MiscCashActivityID=mcaa.MiscCashActivityID
	Inner Join tblMiscCashActivityType mcat on mcat.CashActivityTypeID = mca.CashActivityTypeID
	left Join tblMatter m on m.MatterID = mca.MatterID
Where (CashActivityDate >= @MinBalanceStartDate and CashActivityDate <= @BalanceEndDate) AND CashActivityAmount > 0 --AND mcat.CashActivityTypeID not in (3,4,6)


Select *, @MinBalanceStartDate as BalanceStartDate, @BalanceEndDate as BalanceEndDate from @Detail Order By SourceDate

GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptCashActivity] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptCashActivity] TO [power_user]
GO
