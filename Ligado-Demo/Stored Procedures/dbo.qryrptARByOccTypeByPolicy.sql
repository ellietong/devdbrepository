SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptARByOccTypeByPolicy]
(@allocationID int = null, @CarrierID INT = NULL, @Solvency varchar (50)= NULL, 
@CarrierGroupID int = NULL, @InsuranceProgramID INT = NULL)
As
SET NOCOUNT ON

--DECLARE @AllocationID int
--SET @AllocationID = 56
--DECLARE @CarrierID int
--SET @CarrierID = 0
--DECLARE @CarrierGroupID int
--SET @CarrierGroupID = 0
--DECLARE @Solvency varchar (50)
--SET @Solvency = null
--DECLARE @InsuranceProgramID INT
--SET @InsuranceProgramID = 0


DECLARE @limittypeid int
SET @limittypeid = (SELECT LimitTypeID FROM tblAllocation WHERE AllocationID = @allocationID)

--DECLARE Variables
DECLARE @maxAllocationID int
DECLARE @maxCarrierID int
DECLARE @maxCarrierGroupID int
DECLARE @maxInsuranceProgramID int

--SET Variables
SELECT @maxAllocationID = MAX(AllocationID) from vAllocation
SELECT @maxCarrierID = MAX(CarrierID) from vPolicy
SELECT @maxCarrierGroupID = MAX(CarrierGroupID) from vPolicy
SELECT @maxInsuranceProgramID = MAX (InsuranceProgramID) from vPolicy


DECLARE @Allocation TABLE (AllocationID int, TotalAggregateLimit money, TotalSIR money, TotalAllocation money, PVTotalAllocation money)
INSERT INTO @Allocation
SELECT AllocationID, SUM(AggregateLimit) AS AggregateLimit, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID


DECLARE @CarrierAllocation TABLE (AllocationID int, CarrierID int, CarrierAggregateLimit money, CarrierTotalSIR money, CarrierTotalAllocation money, PVCarrierTotalAllocation money)
INSERT INTO @CarrierAllocation
SELECT AllocationID, CarrierID, SUM(AggregateLimit) AS AggregateLimit, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVCarrierTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, CarrierID


DECLARE @CarrierGroupAllocation TABLE (AllocationID int, CarrierGroupID int, CarrierGroupAggregateLimit money, CarrierGroupTotalSIR money, CarrierGroupTotalAllocation money, PVCarrierGroupTotalAllocation money)
INSERT INTO @CarrierGroupAllocation
SELECT AllocationID, CarrierGroupID, SUM(AggregateLimit) AS AggregateLimit, SUM(SIRAllocated) AS TotalSIR, SUM(Allocation) AS TotalAllocation, SUM(PV) AS PVCarrierGroupTotalAllocation
FROM tblAllocationResults ar
INNER JOIN vPolicy p ON ar.PolicyDateID = p.PolicyDateID
WHERE AllocationID = @AllocationID AND LimitTypeID = @LimitTypeID
GROUP BY AllocationID, CarrierGroupID

Declare @ExhaustionTypeID int
Set @exhaustiontypeid = (Select ExhaustionTypeID From tblAllocation Where AllocationID = @allocationID)

Declare @ExhaustionDateID int
set @ExhaustionDateID = (Select ExhaustionDateID from tblAllocation where AllocationID = @allocationid)

DECLARE @exh table (ExhaustionTypeID int, PolicydateID int, PreviouslyExhaustedAggregate money)
INSERT INTO @exh (ExhaustionTypeID, PolicydateID, PreviouslyExhaustedAggregate)
SELECT ExhaustionTypeID, pd.PolicydateID, SUM(IndemnityAllocation + DefenseAllocation)
FROM tblExhaustion e 
	INNER JOIN tblExhaustionDate ed ON e.ExhaustionDateID = ed.ExhaustionDateID
	INNER JOIN tblPolicyDates pd ON e.PolicyDateID = pd.PolicyDateID
WHERE ExhaustionTypeID = @exhaustionTypeID
	AND ExhaustionDate < = (select ExhaustionDate from tblExhaustionDate where ExhaustionDateID = @ExhaustionDateID)
GROUP BY ExhaustionTypeID, pd.PolicydateID


DECLARE @Policy TABLE (PolicyID int, PolicyDateID int, CarrierGroupID int, CarrierGroupName nvarchar(250), 
CarrierID int, CarrierName nvarchar(250), CarrierSolvency nvarchar(25), PolicyNum nvarchar(100), 
AttachmentPoint money, PreviouslyExhaustedAggregate money, PerOccLimit money, LayerPerOccLimit money, 
SIR money, AggregateLimit money, Layer tinyint, InsuranceProgramID int, InsuranceProgram nvarchar(50),
LimitTypeID int, PStartDate datetime, PEndDate datetime, ActualPStartDate datetime, ActualPEndDate datetime)
INSERT INTO @Policy

SELECT PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, CarrierSolvency,
PolicyNum, AttachmentPoint, PreviouslyExhaustedAggregate, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, 
Layer, InsuranceProgramID, InsuranceProgram, LimitTypeID, PStartDate, PEndDate, ActualPStartDate, ActualPEndDate
FROM vPolicy p
WHERE LimitTypeID = @LimitTypeID
GROUP BY PolicyID, PolicyDateID, CarrierGroupID, CarrierGroupName, CarrierID, CarrierName, CarrierSolvency,
PolicyNum, AttachmentPoint, PreviouslyExhaustedAggregate, PerOccLimit, LayerPerOccLimit, SIR, AggregateLimit, 
Layer, InsuranceProgramID, InsuranceProgram, LimitTypeID, PStartDate, PEndDate, ActualPStartDate, ActualPEndDate


SELECT pinfo.InsuranceProgram, o.OccurrenceType, o.OccurrenceTypeID, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' ('+ pinfo.CarrierGroupName + ')' END AS 'CarrierInfo', 
	pinfo.PolicyNum, pinfo.PolicyID, pinfo.CarrierID, pinfo.CarrierGroupID, pinfo.CarrierName, pinfo.CarrierGroupName, 
	CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END AS 'CarrierGroupInfo', a.AllocationID, 
	a.AllocationDescription, MAX(ar.DateTime) AS MaxOfDateTime, SUM(ar.Allocation) AS SumOfAllocation, CONVERT(money, SUM(ar.PV)) AS SumOfPV, 
	SUM(ar.SIRAllocated) AS SumOfSIRAllocated, ar.DateToPresentValue, pinfo.LayerPerOccLimit, pinfo.SIR, pinfo.AggregateLimit, 
	pinfo.AttachmentPoint, pinfo.PerOccLimit, pinfo.pstartdate, pinfo.penddate, pinfo.actualpstartdate, pinfo.actualpenddate, pinfo.policydateid,
	Treatment, YieldCurveDate as PVRateDate, coalesce(e.PreviouslyExhaustedAggregate, 0) as previouslyExhaustedAggregate,
	CarrierTotalAllocation, COALESCE(CarrierGroupTotalAllocation,CarrierTotalAllocation) AS CarrierGroupTotalAllocation, TotalAllocation,
	COALESCE(CarrierTotalSIR,0) AS CarrierTotalSIR, PVTotalAllocation, PVCarrierTotalAllocation, CarrierGroupTotalSIR, PVCarrierGroupTotalAllocation,
	CarrierAggregateLimit, CarrierGroupAggregateLimit, TotalSIR, TotalAggregateLimit
FROM @Policy pinfo 
	INNER JOIN vAllocationResults ar ON pinfo.PolicydateID = ar.PolicydateID
	INNER JOIN vAllocation a ON a.AllocationID = ar.AllocationID 
	INNER JOIN vOccurrence o ON ar.OccurrenceID = o.OccurrenceID
	LEFT JOIN tblYieldCurve yc on yc.YieldCurveID = ar.YieldCurveID 
	Left Join @exh e on e.PolicydateID = pinfo.PolicydateID 
		Inner Join (Select * From ztblPolicyAllocation Where AllocationID = @AllocationID) z on z.PolicyDateID = pinfo.PolicyDateID
	Inner Join tblTreatment t ON z.Provision1 = t.TreatmentID 
	LEFT JOIN @Allocation al ON ar.AllocationID = al.AllocationID
	LEFT JOIN @CarrierAllocation ca ON ar.AllocationID = ca.AllocationID AND ca.CarrierID = pinfo.CarrierID
	LEFT JOIN @CarrierGroupAllocation cga ON ar.AllocationID = ca.AllocationID AND cga.CarrierGroupID = pinfo.CarrierGroupID
WHERE a.AllocationID = @allocationID AND
	pinfo.CarrierID >= COALESCE(@CarrierID, 0) AND pinfo.CarrierID <= COALESCE(NULLIF(@CarrierID, 0), @maxCarrierID) AND 
	pinfo.CarrierSolvency LIKE COALESCE(NULLIF(@Solvency, '<<ALL>>'), '%') AND
	(coalesce(pinfo.CarrierGroupID,0) >= COALESCE(@CarrierGroupID, 0) AND coalesce(pinfo.CarrierGroupID,0) <= COALESCE(nullif(@CarrierGroupID,0), @maxCarrierGroupID)) AND 
	pinfo.InsuranceProgramID >= COALESCE(@InsuranceProgramID, 0) AND pinfo.InsuranceProgramID <= COALESCE(NULLIF(@InsuranceProgramID, 0), @maxInsuranceProgramID) 
GROUP BY pinfo.InsuranceProgram, o.OccurrenceType, o.OccurrenceTypeID, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierName + ' ('+ pinfo.CarrierGroupName + ')' END, 
	pinfo.PolicyNum, pinfo.PolicyID, pinfo.CarrierID, pinfo.CarrierGroupID, pinfo.CarrierName, pinfo.CarrierGroupName, CASE WHEN pinfo.CarrierGroupName IS NULL THEN pinfo.CarrierName ELSE pinfo.CarrierGroupName END,
	a.AllocationID, a.AllocationDescription, ar.DateToPresentValue, pinfo.LayerPerOccLimit, pinfo.SIR,  pinfo.AggregateLimit, pinfo.AttachmentPoint, pinfo.PerOccLimit, 
	Treatment, YieldCurveDate, e.PreviouslyExhaustedAggregate,  pinfo.pstartdate, pinfo.penddate, pinfo.actualpstartdate, pinfo.actualpenddate, pinfo.policydateid,
	CarrierTotalAllocation, COALESCE(CarrierGroupTotalAllocation,CarrierTotalAllocation), TotalAllocation,
	CarrierTotalSIR, PVTotalAllocation, PVCarrierTotalAllocation, CarrierGroupTotalSIR, PVCarrierGroupTotalAllocation,
	CarrierAggregateLimit, CarrierGroupAggregateLimit, TotalSIR, TotalAggregateLimit

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccTypeByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccTypeByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccTypeByPolicy] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptARByOccTypeByPolicy] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptARByOccTypeByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptARByOccTypeByPolicy] TO [power_user]
GO
