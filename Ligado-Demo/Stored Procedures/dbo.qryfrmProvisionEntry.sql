SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[qryfrmProvisionEntry]
AS
SET NOCOUNT ON

SELECT  *
	FROM  tblProvision



RETURN






GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionEntry] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionEntry] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionEntry] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionEntry] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmProvisionEntry] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmProvisionEntry] TO [power_user]
GO
