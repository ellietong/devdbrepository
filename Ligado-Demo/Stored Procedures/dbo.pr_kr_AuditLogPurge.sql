SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[pr_kr_AuditLogPurge] (@CutoffDateTime datetime, @BatchSize int = null)
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @NumRows int
  DECLARE @SQL varchar(1000)

  IF @BatchSize IS NULL
    SELECT @BatchSize = 1000

  SELECT @NumRows = Count(*)
    FROM dbo.AuditLog
    WHERE PostedDateTime < @CutoffDateTime

  IF @NumRows <= @BatchSize
    DELETE FROM dbo.AuditLog WHERE PostedDateTime < @CutoffDateTime
  ELSE
  BEGIN
    -- Purge data in batches for efficiency

    -- Build SQL to delete a single batch
    SELECT @SQL = 'DELETE FROM dbo.AuditLog WHERE AuditLogID IN ' + CHAR(13) +
                  '  (SELECT TOP ' + convert(varchar, @BatchSize) + ' AuditLogID' + CHAR(13) +
                  '    FROM dbo.AuditLog WHERE PostedDateTime < ' + CHAR(39) + convert(varchar, @CutoffDateTime, 121) + CHAR(39) + ')'

TopOfBatchLoop:

    -- Delete rows for that batch
    EXEC (@SQL)

    CHECKPOINT

    -- Do we have more to delete?
    SELECT @NumRows = Count(*)
      FROM dbo.AuditLog
      WHERE PostedDateTime < @CutoffDateTime

    -- We have more than a batch left to delete
    IF @NumRows > @BatchSize GOTO TopOfBatchLoop

    -- We have less than a batch to delete
    IF @NumRows > 0 DELETE FROM dbo.AuditLog WHERE PostedDateTime < @CutoffDateTime

    CHECKPOINT
  END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditLogPurge] TO [base_user]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogPurge] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditLogPurge] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogPurge] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogPurge] TO [omniaudit_admin]
GRANT VIEW DEFINITION ON  [dbo].[pr_kr_AuditLogPurge] TO [power_user]
GRANT EXECUTE ON  [dbo].[pr_kr_AuditLogPurge] TO [power_user]
GO
