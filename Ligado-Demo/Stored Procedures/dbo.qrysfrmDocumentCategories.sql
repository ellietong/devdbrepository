SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysfrmDocumentCategories]
(@DocumentID bigint)

As
Set NoCount On

select DocumentID, doccategoryID, PageNums
  from tblSettleDocs
  where DocumentID = @DocumentID
  --group by DocumentID, doccategoryID, PageNums

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDocumentCategories] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysfrmDocumentCategories] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDocumentCategories] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysfrmDocumentCategories] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysfrmDocumentCategories] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysfrmDocumentCategories] TO [power_user]
GO
