SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCedure [dbo].[qryfrmPaymentDefense] (@DefenseCounselAddressID INT = NULL, @MatterID int)
AS
SET NOCOUNT ON

select di.DefenseInvoiceID, DefenseCounselAddressID, di.InvoiceNumber, di.Period, InvoiceTotal, InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) as RemainingAmount
from vtblDefenseInvoice di
left join tblDefenseInvoice div on div.DefenseInvoiceID = di.DefenseInvoiceID
left join (select DefenseInvoiceID, PaymentAmount from tblcheckapproval ca Inner Join tblPaymentDefense pd on ca.CheckID = pd.CheckID)
			 a on a.DefenseInvoiceID = di.DefenseInvoiceID
WHERE DefenseCounselAddressID = @DefenseCounselAddressID 
Group By di.DefenseInvoiceID, DefenseCounselAddressID, di.InvoiceNumber, di.Period, InvoiceTotal
Having InvoiceTotal-SUM(Coalesce(PaymentAmount,0)) <> 0

RETURN


GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [CheckApproval_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [CheckApproval_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [CheckQC_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [CheckQC_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [CheckWriter_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [CheckWriter_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmPaymentDefense] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmPaymentDefense] TO [power_user]
GO
