SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryrptPolicyReportsSplitGroupedQCAll_QC]
(@policyid int = null)
As
set nocount on

DECLARE @MAXpolicyid INT
-- Find max ID values
SELECT @MAXpolicyid = MAX(policyid) FROM tblPolicy

--declare @policyid int
--set @policyid=25

DECLARE @Policy TABLE (PolicyID int, PolicyNum nvarchar(100), CarrierID int, PolicyNotes nvarchar (4000),
InitialDataEntry nvarchar(50), InitialDataEntryDate datetime, PolicyReview nvarchar(50), 
PolicyReviewDate datetime, NamedInsuredID int, InsuranceProgramID int)
INSERT INTO @Policy

SELECT PolicyID, PolicyNum, CarrierID, PolicyNotes, InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate,
NamedInsuredID, InsuranceProgramID
FROM vPolicy
GROUP BY PolicyID, PolicyNum, CarrierID, PolicyNotes, InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate,
NamedInsuredID, InsuranceProgramID

SELECT
tblInsuranceProgram.InsuranceProgram,
A.Policyid, tblCarrier.CarrierName,
PolicyNum, b.ProvisionLabelID, c.provisionvalue, d.provision,
min(tblPolicyDates.ActualPStartDate) AS Splitstart,
max(tblPolicyDates.ActualPEndDate) AS splitend,
tblNamedInsured.NamedInsured,
PolicyNotes,
InitialDataEntry, InitialDataEntryDate, PolicyReview, PolicyReviewDate

FROM @Policy A
left JOIN tblProvision B on A.policyID = b.policyID
left JOIN tblprovisionvalue C on B.provisionvalueid = C.provisionvalueid
INNER JOIN tblProvisionlabel D ON b.ProvisionLabelID = d.ProvisionLabelID
INNER JOIN tblCarrier ON A.CarrierID = tblCarrier.CarrierID
left JOIN tblNamedInsured ON tblNamedInsured.NamedInsuredID = A.NamedInsuredID
INNER JOIN tblInsuranceProgram ON tblInsuranceProgram.InsuranceProgramID = A.InsuranceProgramID
INNER JOIN tblPolicyDates ON A.PolicyID = tblPolicyDates.PolicyID

WHERE a.policyid >= COALESCE(@policyid, 0) AND
a.policyid <= COALESCE(NULLIF(@policyid, 0), @MAXpolicyid)

GROUP BY  tblInsuranceProgram.InsuranceProgram, A.Policyid, tblCarrier.CarrierName,
PolicyNum, b.ProvisionLabelID, c.provisionvalue,d.provision,
tblNamedInsured.NamedInsured,
InitialDataEntry, InitialDataEntryDate,
PolicyReview, PolicyReviewDate, PolicyNotes

ORDER BY tblInsuranceProgram.InsuranceProgram
return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplitGroupedQCAll_QC] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplitGroupedQCAll_QC] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplitGroupedQCAll_QC] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplitGroupedQCAll_QC] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptPolicyReportsSplitGroupedQCAll_QC] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptPolicyReportsSplitGroupedQCAll_QC] TO [power_user]
GO
