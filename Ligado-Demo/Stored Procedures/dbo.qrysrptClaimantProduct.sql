SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qrysrptClaimantProduct]
(@ClaimantPersonalInfoID bigint)

As
Set NoCount On

--DECLARE @ClaimantPersonalInfoID int
--SET @ClaimantPersonalInfoID = 1

Select ClaimLawsuitProductID, cpn.ProductName, cpb.ProductBrand, cpt.ProductType, cpm.ProductManufacturer, StartDate, EndDate
From tblClaimLawsuitProduct clp
inner join tblClaimLawsuit cl ON clp.ClaimLawsuitID = cl.ClaimLawsuitID
inner join tblClaim c ON cl.ClaimID = c.ClaimID
LEFT JOIN tblProduct cp ON clp.ProductID = cp.ProductID
LEFT JOIN tblProductType cpt ON cp.ProductTypeID = cpt.ProductTypeID
LEFT Join tblProductName cpn ON cp.ProductNameID = cpn.ProductNameID
LEFT JOIN tblProductManufacturer cpm ON cp.ProductManufacturerID = cpm.ProductManufacturerID
LEFT JOIN tblProductBrand cpb ON cp.ProductBrandID = cpb.ProductBrandID
Where ClaimantPersonalInfoID = @ClaimantPersonalInfoID

RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantProduct] TO [base_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantProduct] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantProduct] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantProduct] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qrysrptClaimantProduct] TO [power_user]
GRANT EXECUTE ON  [dbo].[qrysrptClaimantProduct] TO [power_user]
GO
