SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[qryrptSummaryReportByCarrierByPolicy]
(@ExhaustionDateID int) 
As
Set Nocount on


 --Declare @ExhaustionDateID int

 --Set @ExhaustionDateID = 35


-----------------------------------------------------------------------------------------------------------------------------------------------


--Current Allocation
Declare @CurrentAllocation Table (
			ExhaustionTypeID int, 
			ExhaustionID bigint, 
			ExhaustionDateID int, 
			OccurrenceID bigint,
			OccurrenceTypeID bigint, 
			PolicyDateID bigint,
			IndemnityAllocation money, 
			DefenseAllocation money, 
			DefenseOutsideLimitsAllocation money, 
			SIRIndemnityAllocation money,
			SIRDefenseAllocation money,
			 LedgerID bigint, 
			 OutsideConsumption money)
	
Insert Into @CurrentAllocation
Select 
			ExhaustionTypeID, 
			ExhaustionID, 
			E.ExhaustionDateID, 
			e.OccurrenceID,
			OccurrenceTypeID, 
			PolicyDateID, 
			IndemnityAllocation,
			DefenseAllocation, 
			DefenseOUtsideLimitsAllocation, 
			SIRIndemnityAllocation, 
			SIRDefenseAllocation,
			LedgerID, 
			OutsideConsumption 
From tblExhaustion E
	Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
	Inner join tblOccurrence o on o.OccurrenceID=e.OccurrenceID
Where 
	ExhaustionTypeID=3
	and ED.ExhaustionDateID=@ExhaustionDateID 
	and ConsumptionTypeID in (1)  --1 is Billed, 2 is Credit, 3 is Outside

Declare @CurrentAllocationTotals table (
				OccurrenceTypeID bigint, 
				ExhaustionTypeID int, 
				PolicyDateID int,  
				DefenseFees money,
				DefenseOutsideLimitsFees money,
				DefenseCosts money,
				DefenseOutsideLimitsCosts money,  
				TotalDefense money, 
				SIRDefenseAllocation money)
Insert Into @CurrentAllocationTotals
Select			OccurrenceTypeID, 
				ExhaustionTypeID, 
				PolicyDateID,
				case when occurrencetypeid=3 then 0 else sum(DefenseAllocation) end as DefenseFees,
				case when occurrencetypeid=3 then 0 else sum(DefenseOutsideLimitsAllocation) end as DefenseOutsideLimitsFees,
				case when occurrencetypeid=4 then 0 else sum(DefenseAllocation) end as DefenseCosts,
				case when occurrencetypeid=4 then 0 else sum(DefenseOutsideLimitsAllocation) end as DefenseOutsideLimitsCosts,
				coalesce(Sum(DefenseAllocation + DefenseOutsideLimitsAllocation),0) as TotalDefense,
				coalesce(Sum(SIRDefenseAllocation), 0) as SIRDefense
From @CurrentAllocation
Group By OccurrenceTypeID,ExhaustionTypeID, PolicyDateID



--Current Adjustment
Declare @CurrentAdjustment Table (
				ExhaustionTypeID int, 
				ExhaustionID bigint, 
				ExhaustionDateID int, 
				OccurrenceID bigint,
				OccurrenceTypeID bigint,  
				PolicyDateID bigint,
				TotalAdjustment money)
Insert Into @CurrentAdjustment
Select 
				ExhaustionTypeID, 
				ExhaustionID, 
				E.ExhaustionDateID, 
				e.OccurrenceID,
				OccurrenceTypeID,  
				PolicyDateID, 
				Coalesce(SUM(Indemnityallocation+DefenseAllocation),0) as TotalAdjustment
From tblExhaustion E
		Inner Join tblExhaustionDate ED on ED.ExhaustionDateID=E.ExhaustionDateID
		Inner join tblOccurrence o on o.OccurrenceID=e.OccurrenceID
Where 
	ExhaustionTypeID=3
	and ED.ExhaustionDateID = @ExhaustionDateID --Changed to Bill Exhaustion Date ID from adjustment (Now use ConsumptionTypeID)
	and ConsumptionTypeID in (2,3) --1 is Billed, 2 is Credit, 3 is Outside
Group by 
	OccurrenceTypeID, ExhaustionTypeID, ExhaustionID, E.ExhaustionDateID, e.OccurrenceID, PolicyDateID

Declare @CurrentAdjustmentTotals table (
				OccurrenceTypeID int,
				ExhaustionTypeID int, 
				PolicyDateID int, 
				TotalAdjustment money) 
Insert Into @CurrentAdjustmentTotals
Select			OccurrenceTypeID,
				ExhaustionTypeID, 
				PolicyDateID,
				coalesce(Sum(TotalAdjustment), 0) as TotalAdjustment
From @CurrentAdjustment
Group By 
		OccurrenceTypeID,ExhaustionTypeID, PolicyDateID




-- Header Summary
Declare @HeaderSummary Table (
				ExhaustionType nvarchar(100), 
				Description nvarchar(100), 
				DefenseForm nvarchar(30), 
				footerdate nvarchar(50))
Insert into @HeaderSummary
Select 
				ExhaustionType, 
				Matter as Description, 
				'Within', 
				Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
		Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
		inner join tblExhaustion e on e.ExhaustionDateID=ed.ExhaustionDateID
		inner join tblOccurrence o on o.OccurrenceID=e.OccurrenceID
		inner join tblClaimLawsuit cl on cl.ClaimLawsuitID=o.ClaimID
		inner join tblclaim c on c.ClaimID=cl.ClaimID
		inner join tblMatter m on m.MatterID=c.MatterID
Where 
		eD.ExhaustionTypeID=3
		and ed.ExhaustionDateID=@ExhaustionDateID

union
Select 
				ExhaustionType, 
				Matter as Description, 
				'Excess', 
				Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
		Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
		inner join tblExhaustion e on e.ExhaustionDateID=ed.ExhaustionDateID
		inner join tblOccurrence o on o.OccurrenceID=e.OccurrenceID
		inner join tblClaimLawsuit cl on cl.ClaimLawsuitID=o.ClaimID
		inner join tblclaim c on c.ClaimID=cl.ClaimID
		inner join tblMatter m on m.MatterID=c.MatterID
Where 
		ed.ExhaustionTypeID=3 
		and ed.ExhaustionDateID=@ExhaustionDateID
union
Select 
				ExhaustionType, 
				Matter as Description, 
				'', 
				Datename(MONTH,exhaustiondate-45) +' ' + Datename(Year,exhaustiondate-45) as FooterDate
From tblExhaustionDate ED
		Inner Join tblExhaustionType ET on ET.ExhaustionTypeID=ED.ExhaustionTypeID
		inner join tblExhaustion e on e.ExhaustionDateID=ed.ExhaustionDateID
		inner join tblOccurrence o on o.OccurrenceID=e.OccurrenceID
		inner join tblClaimLawsuit cl on cl.ClaimLawsuitID=o.ClaimID
		inner join tblclaim c on c.ClaimID=cl.ClaimID
		inner join tblMatter m on m.MatterID=c.MatterID
Where 
		ed.ExhaustionTypeID=3 
		and ed.ExhaustionDateID=@ExhaustionDateID






--Query Results
Declare @Results Table(policyid bigint, policydateid bigint, carriername nvarchar(100),carriergroupname nvarchar(100), layer bigint, policynum nvarchar(100), pstartdate nvarchar(100), penddate nvarchar(100),
 attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), DefenseFees money, DefenseOutsideLimitsFees money, DefenseCosts money, DefenseOutsideLimitsCosts money)
	
Insert Into @Results


--Carriers
select  
	--Case when OccurrenceTypeID = 3 then 'Costs'
		--else 'Fees' end as 'Type', 
	p.policyid, 
	pd.policydateid, 
	c.carriername,
	cg.CarrierGroupName, 
	coalesce(pl.layer,0)+1 as Layer, 
	p.policynum,  
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
		when (pd.policydateid = 1833) then '09/01/1953'
		--*****************************ADD IN MATTERID TOO FOR POLICYDATEID 1833
	else convert(nvarchar(100), PStartDate , 101) end as pstartdate, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end  as penddate,
	pl.attachmentpoint, 
	pl.perocclimit, 
	pl.aggregatelimit,
	CASE	When provisionvalueID=2 then 'Within'
			when provisionvalueID=3 then 'Excess'
		end as DefenseForm, 
	sum(DefenseFees) as DefenseFees, 
	sum(DefenseOutsideLimitsFees) as DefenseOutsideLimitsFees,
	sum(DefenseCosts) as DefenseCosts, 
	Sum(DefenseOutsideLimitsCosts) as DefenseOutsideLimitsCosts
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	join tblCarrierGroup cg on cg.CarrierGroupID=c.CarrierGroupID
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where 
	CAT.ExhaustionTypeID = 3 
	and provisionlabelID=1 
	and c.CarrierID not in (10)
Group by
	pl.aggregatelimit,pl.perocclimit, pl.attachmentpoint, pl.layer,cg.CarrierGroupName,p.policyid, pd.policydateid, c.carriername,  p.policynum, Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
		when (pd.policydateid = 1833) then '09/01/1953'
	else convert(nvarchar(100), PStartDate , 101) end, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end,
	CASE	When provisionvalueID=2 then 'Within'
			when provisionvalueID=3 then 'Excess'
		end


Union

--GSX Producer
select  
	--Case when OccurrenceTypeID = 3 then 'Costs'
	--	else 'Fees' end as 'Type',
	p.policyid, 
	pd.policydateid, 
	'GSX'  as carriername,
	'GSX'  as carriergroupname,
	'100' as layer, 
	p.policynum as PolicyNum, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
	else convert(nvarchar(100), PStartDate , 101) end as pstartdate, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end  as penddate,
	'' as attachmentpoint, 
	'-1000000000'  as perocclimit, 
	'-1000000000'  as aggregatelimit, 
	'' as defenseform,
	sum(DefenseFees) as DefenseFees, 
	sum(DefenseOutsideLimitsFees) as DefenseOutsideLimitsFees,
	sum(DefenseCosts) as DefenseCosts, 
	Sum(DefenseOutsideLimitsCosts) as DefenseOutsideLimitsCosts
from tblPolicyDates pd
	inner join tblPolicy p on p.PolicyID=pd.PolicyID
	inner join tblcarrier c on c.carrierid=p.carrierid
	join tblCarrierGroup cg on cg.CarrierGroupID=c.CarrierGroupID
	inner join tblexhaustion e on e.policydateid=pd.policydateid
	inner Join @CurrentAllocationTotals CAT on PD.PolicyDateID=CAT.PolicyDateID
where c.CarrierID in (10)
group by 
	OccurrenceTypeID,p.policyid, pd.policydateid, c.carriername,  p.policynum, 		Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
	else convert(nvarchar(100), PStartDate , 101) end, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end,	coalesce(OutsideConsumption,0)




	--select * from @Results


--Adjustment Results
Declare @Adjustment Table(OrderID int, OrderName nvarchar(100), policyid bigint, policydateid bigint, carriername nvarchar(100), carriergroupname nvarchar(100), layer int, policynum nvarchar(100), pstartdate datetime, penddate datetime, attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), TotalAdjustment money)
Insert Into @Adjustment



--Carriers

select  
	'1' as OrderID, 
	'All Insurers', 
	p.policyid, 
	pd.policydateid, 
	c.carriername,
	cg.carriergroupname, 
	coalesce(pl.layer,0)+1 as Layer, 
	p.policynum, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
	else convert(nvarchar(100), PStartDate , 101) end as pstartdate, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end  as penddate,
	attachmentpoint, 
	perocclimit, 
	aggregatelimit, 
	CASE	When provisionvalueID=2 then 'Within'
			when provisionvalueID=3 then 'Excess'
		end as DefenseForm, 
	coalesce(TotalAdjustment,0) as TotalAdjustment 
from 
	tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAdjustmentTotals CAAT on PD.PolicyDateID=CAAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	join tblCarrierGroup cg on cg.CarrierGroupID=c.CarrierGroupID
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where 
	CAAT.ExhaustionTypeID = 3 
	and c.CarrierID not in (10)

UNION


--GSX Producer
select  
	'2' as OrderID, 
	'Producer', 
	p.policyid, 
	pd.policydateid, 
	''  as carriername,
	'GSX'  as carriergroupname,
	'' as layer, 
	p.PolicyNum as policynum, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then ''
	else convert(nvarchar(100), PStartDate , 101) end as pstartdate, 
	Case when pd.policydateid = 3098 then '' 
		when pd.policydateid = 3099 then '' 
	else convert(nvarchar(100), PEndDate , 101) end  as penddate,
	'' as attachmentpoint, 
	'-1000000000'  as perocclimit, 
	'-1000000000' as aggregatelimit, 
	'' as defenseform,
	coalesce(TotalAdjustment,0) as TotalAdjustment 
from 
	tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	inner Join @CurrentAdjustmentTotals CAAT on PD.PolicyDateID=CAAT.PolicyDateID
	inner join tblcarrier c on c.carrierid = p.carrierid
	join tblCarrierGroup cg on cg.CarrierGroupID=c.CarrierGroupID
Where  
	CAAT.ExhaustionTypeID = 3 
	and c.CarrierID in (10)


Select 
	R.*, ExhaustionType, 
	[Description],coalesce(sum(a.TotalAdjustment),0) as TotalAdjustment, 
	hs.footerdate
From 
	@Results R
	Inner Join @HeaderSummary HS on R.DefenseForm=HS.DefenseForm
	left join @Adjustment a on r.policydateid=a.policydateid
group by  r.policyid , r.policydateid , r.carriername ,r.carriergroupname, r.layer , r.policynum , r.pstartdate , r.penddate , r.attachmentpoint , r.perocclimit , r.aggregatelimit ,
	r.DefenseForm,DefenseCosts,DefenseOutsideLimitsCosts,DefenseFees,DefenseOutsideLimitsFees,
	ExhaustionType, [Description], hs.footerdate
order by carriername, Cast(r.penddate as DATE), cast(r.pstartdate as date),  policynum



Return







GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy] TO [power_user]
GO
