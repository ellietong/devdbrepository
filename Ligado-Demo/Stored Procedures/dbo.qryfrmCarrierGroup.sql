SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[qryfrmCarrierGroup]
AS
SET NOCOUNT ON
SELECT CarrierGroupID, CarrierGroupName, CarrierGroupNote
FROM tblCarrierGroup
ORDER BY CarrierGroupName
RETURN
GO
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierGroup] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierGroup] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierGroup] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierGroup] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryfrmCarrierGroup] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryfrmCarrierGroup] TO [power_user]
GO
