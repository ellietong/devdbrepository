SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] (@MatterID int = NULL, 
                                                                   @MonthID  int, 
                                                                   @YearID   int)
AS
SET NOCOUNT ON

----The following variables can be used for testing.
--DECLARE @MatterID int
--DECLARE @MonthID int
--DECLARE @YearID int
--SET @MatterID = NULL
--SET @MonthID  = 5
--SET @YearID   = 2011

--NOTE: All references to DismissalDate/ResolutionDate have been replaced with ProcessedDate, 
--      and all original ProcessedDate references have been replaced with ProcessedDate.

DECLARE @maxMatterID int
SELECT @maxMatterID = MAX(MatterID) FROM tblMatter

DECLARE @Month int
DECLARE @Year int
DECLARE @PendingDate date
SET @Month = @MonthID
SET @Year = @YearID
SET @PendingDate = DATEADD(MONTH,1,CONVERT(date,CONVERT(varchar,@Month)+'/1/'+CONVERT(varchar,@Year)))

SELECT pca.PlaintiffCounsel, COUNT(DISTINCT(cl.ClaimLawsuitID)) AS 'Count'
FROM tblClaim c 
	INNER JOIN tblClaimLawsuit cl                 ON c.ClaimID = cl.ClaimID
	INNER JOIN tblLawsuit l                       ON cl.LawsuitID = l.LawsuitID
	INNER JOIN tblClaimLawsuitStatusTracking clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID
	INNER JOIN tblPlaintiffCounselAddress pca     ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
WHERE ProcessedDate < @PendingDate
	AND ClaimLawsuitStatusID IN (34) 
	AND IsPrimaryForClaim = 1 
	AND IsPrimaryStatus = 1
	AND MatterID >= COALESCE(@MatterID, 0) AND MatterID <= COALESCE(NULLIF(@MatterID, 0), @maxMatterID)
GROUP BY pca.PlaintiffCounsel
ORDER BY COUNT(DISTINCT(cl.ClaimLawsuitID)) DESC, pca.PlaintiffCounsel

GO
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] TO [base_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] TO [power_user]
GRANT EXECUTE ON  [dbo].[WSS_REPORTS_PENDING_qryRBNFByPlaintiffsCounsel] TO [power_user]
GO
