SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE Procedure [dbo].[qryrptAllocationByClaimDetailsMulti] (@ExhaustionDateID int, @LimitTypeID int, @carrierID1 int = NULL,@carrierID2 int = NULL,@carrierID3 int = NULL,@carrierID4 int = NULL,@carrierID5 int = NULL, @matterid int = NULL)
As
Set NoCount on



----------------------------------------------------------------------------------------------------------
--Declare @ExhaustionDateID int
--Set @ExhaustionDateID = 9
 
--Declare @LimitTypeID int
--Set @LimitTypeID=1

--DECLARE @carrierID1 int
--SET @carrierID1 = 88

--DECLARE @carrierID2 int
--SET @carrierID2 = 459

--DECLARE @carrierID3 int
--SET @carrierID3 = NULL

--DECLARE @carrierID4 int
--SET @carrierID4 = NULL

--DECLARE @carrierID5 int
--SET @carrierID5 = NULL

--DECLARE @matterid int 
--SET @matterid = null
------------------------------------------------------------------------------------------------------------
--DECLARE @maxCarrierID int
--SELECT @maxCarrierID = MAX(CarrierID) FROM vPolicy

Declare @maxMatterID int
Select @maxmatterid = max(matterid) from tblmatter

Declare @FirstPolicyStartDate table (occurrenceid int, FirstPolicyStartDate date)
insert into @FirstPolicyStartDate
Select occurrenceid, min(pstartdate) 
from tblpolicydates d inner join tblexhaustion e on e.policydateid = d.policydateid
where exhaustiondateid = @ExhaustionDateID and d.policydateid not in (3098, 3099) -- exclude producer policies
group by occurrenceid




Select '' as AllocationDescription, 
	e.PolicyDateID,
	o.ClaimID, 
	triggerstart1 as EarliestExposureDate, 
	triggerend1 as DiagnosisDate, 
	o.OccurrenceID, 
	OccurrenceGroup, 
	OccurrenceName, 
	ot.OccurrenceTypeID, 
	ot.occurrencetype,
	convert(money,(O.Cost1 + O.Cost3)) as CostAllocated, 
	PolicyNum,
	Case when p.carrierid = 10 then 'Producer' else CarrierName end as carriername,
	Case when p.policydateid = 3098 then triggerstart1 
		 when p.policydateid = 3099 then Null
		 else Min(PStartDate) end as MinStartDate, 
	Case when p.policydateid = 3098 then min(FirstPolicyStartDate)
		 when p.policydateid = 3099 then Null
		 else Max(PEndDate) end as MaxEndDate, 
	coalesce(Layer,0)+1 as Layer, 
	SUM(IndemnityAllocation) as AllocationIndemnity, 
	SUM(DefenseAllocation) as AllocationDefense, 
	SUM(SIRIndemnityAllocation+SIRDefenseAllocation) as SIRAllocated, 
	0 as Adjustment, 
	SUM(DefenseOutsideLimitsAllocation) as AllocationDefenseOutsideLimits,
	Matter, 
	cl.ClaimLawsuitID, 
	SettlementDate,
	ed.Description,
	p.CarrierGroupName
From tblExhaustion e
	INNER JOIN vPolicy p ON e.PolicyDateID = p.PolicyDateID
	Inner Join tblOccurrence O on e.OccurrenceID=O.OccurrenceID
	Inner Join tblOccurrenceGroup OG on OG.OccurrenceGroupID=O.OccurrenceGroupID
	Left Join tblOccurrenceType ot on o.OccurrenceTypeID = ot.OccurrenceTypeID
	left join tblclaimsettlement cs on cs.ClaimLawsuitID = o.ClaimID
	left join tblclaimlawsuit cl on cl.claimlawsuitid = o.claimid
	left join tblclaim c on c.claimid = cl.claimid
	left join tblmatter m on m.matterid = c.matterid
	Left join @FirstPolicyStartDate f on f.occurrenceid = e.OccurrenceID
	Left Join tblExhaustionDate ed on ed.ExhaustionDateID = e.ExhaustionDateID
Where coalesce(p.LimitTypeID,1) = @LimitTypeID
	 AND e.ExhaustionDateID = @ExhaustionDateID
	 AND p.CarrierID in (Coalesce(@carrierID1,238238),Coalesce(@carrierID2,238238),Coalesce(@carrierID3,238238),Coalesce(@carrierID4,238238),Coalesce(@carrierID5,238238))
	 AND c.MatterID >= COALESCE(@matterid, 0) AND c.Matterid <= COALESCE(NULLIF(@matterid, 0), @maxMatterID)
GROUP BY o.ClaimID, triggerstart1, triggerend1, o.OccurrenceID, OccurrenceGroup, 
	OccurrenceName, ot.OccurrenceTypeID, ot.occurrencetype,
	O.Cost1 + O.Cost3, PolicyNum,
	Case when p.carrierid = 10 then 'Producer' else CarrierName end, 
	Layer, matter, e.PolicyDateID, cl.ClaimLawsuitID,
	p.PolicyDateID, settlementdate,ed.Description,p.CarrierGroupName
	
Order by matter
	
RETURN








GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetailsMulti] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetailsMulti] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetailsMulti] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetailsMulti] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptAllocationByClaimDetailsMulti] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptAllocationByClaimDetailsMulti] TO [power_user]
GO
