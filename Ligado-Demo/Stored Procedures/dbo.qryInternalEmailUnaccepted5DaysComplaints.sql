SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[qryInternalEmailUnaccepted5DaysComplaints]
AS
BEGIN

SET NOCOUNT ON


--Declare and set email inputs.
Declare @Subject nvarchar (100)
Declare @LogoHeader nvarchar (2000)


Declare @FourDaysAgo datetime
SET @FourDaysAgo = CONVERT(datetime, CONVERT(nvarchar, CONVERT(date, GETDATE() - 4)) + ' 16:00:00.000')


SET @Subject = 'GSX: Unaccepted Complaints/Documents Over 4 Days Old'
--Set the font for the entire email, and include the KCIC logo/banner on the top of the email.
--KCIC logo is temporarily being pulled from D. Snyder's Google site    

SET @LogoHeader = '<FONT FACE = "Goudy Old Style"> 
<img src="http://www.kcic.com/i/KCICLogoBar_Solid.png" alt="logo"/>'


--Create a temp table for Complaints which have been uploaded in the past day.
--DocTitle and FirmAbb are retreived by parsing the documentURL.

DECLARE @NewComplaintsTable TABLE (DocID int, docTitle nvarchar (500), FirmAddressID int, lName nvarchar (50),
fName nvarchar (50), ClaimantPersonalInfoID bigint, ServiceDate date)
INSERT INTO @NewComplaintsTable
	SELECT
		d.DocumentID,
		COALESCE(d.DocumentTitle, ''),
		cl.DefenseCounselAddressID,
		cpi.LastName,
		cpi.FirstName,
		cpi.ClaimantPersonalInfoID,
		MAX(l.ServiceDate)

	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
	WHERE d.DateCreated < @FourDaysAgo
	AND d.ClaimantDocumentTypeID IN (8, 4, 33)
	AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	AND COALESCE(Accepted, 0) <> 1
	AND COALESCE(IsPrimaryForClaim, 0) = 1
	AND COALESCE(IsCurrentClaim, 0) = 1
	GROUP BY	d.documentID,
				d.DocumentTitle,
				cl.DefenseCounselAddressID,
				LastName,
				FirstName,
				cpi.ClaimantPersonalInfoID


DECLARE @NewDocumentsTable TABLE (DocID int, docTitle nvarchar (500), FirmAddressID int, lName nvarchar (50),
fName nvarchar (50), DocType nvarchar (50), ClaimantPersonalInfoID bigint)
INSERT INTO @NewDocumentsTable
	SELECT
		d.DocumentID,
		COALESCE(d.DocumentTitle, ''),
		cl.DefenseCounselAddressID,
		cpi.LastName,
		cpi.FirstName,
		ClaimantDocumentType,
		cpi.claimantpersonalinfoid
	FROM tblDocuments d
	INNER JOIN tblMultiClaimantDocument mcd ON d.DocumentID = mcd.DocumentID
	INNER JOIN tblClaimantPersonalInfo cpi ON cpi.ClaimantPersonalInfoID = mcd.ClaimantPersonalInfoID
	LEFT JOIN tblClaimantDocumentType ct ON ct.ClaimantDocumentTypeID = d.ClaimantDocumentTypeID
	LEFT JOIN tblclaim c ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
	LEFT JOIN tblclaimlawsuit cl ON cl.claimid = c.claimid
	LEFT JOIN tbllawsuit l ON l.lawsuitid = cl.lawsuitid
	WHERE datecreated < GETDATE() - 4
	AND d.ClaimantDocumentTypeID IN (7, 11, 23, 25, 6)
	AND (RIGHT(CreatedBy, 11) = 'kcicllc.com' OR RIGHT(CreatedBy, 8) = 'kcic.com')
	AND COALESCE(Accepted, 0) <> 1
	AND COALESCE(IsPrimaryForClaim, 0) = 1
	AND COALESCE(IsCurrentClaim, 0) = 1
	GROUP BY	d.DocumentID,
				d.DocumentTitle,
				cl.DefenseCounselAddressID,
				LastName,
				FirstName,
				ClaimantDocumentType,
				cpi.claimantpersonalinfoid

DECLARE @ClaimantPerDoc TABLE (DocID int, ClaimantList nvarchar (max))
DECLARE @DocID int, @LastDocID int, @Claimant nvarchar (max), @ClaimantID bigint, @ClaimantList nvarchar (max)

DECLARE Cur_Complaints CURSOR Local Fast_Forward for SELECT
	DocID,
	COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
	claimantpersonalInfoID
FROM @NewComplaintsTable UNION SELECT
	DocID,
	COALESCE(Lname, '') + ', ' + COALESCE(Fname, ''),
	claimantpersonalInfoID
FROM @NewDocumentsTable
OPEN Cur_Complaints
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
WHILE @@FETCH_STATUS = 0
BEGIN

IF (LEN (@ClaimantList) = 0 OR @ClaimantList IS NULL)
BEGIN
SET @ClaimantList = '<ul> <a href="https://ligado.kcic.com/Claims/ClaimantDetails?ClientSiteID=19&ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + @Claimant + '</a></ul>'
END
ELSE
BEGIN
SET @ClaimantList = COALESCE(@ClaimantList, '') + '<ul>  <a href="https://ligado.kcic.com/Claims/ClaimantDetails?ClientSiteID=19&ClaimantPersonalInfoID=' + CONVERT(nvarchar(50), @claimantID) + '">' + COALESCE(@Claimant, '') + '</a></ul>'

END
SET @LastDocID = @DocID
FETCH NEXT FROM Cur_Complaints INTO @DocID, @Claimant, @ClaimantID
PRINT @@FETCH_STATUS
IF @DocID < > @LastDocID OR @@FETCH_STATUS < > 0
BEGIN
INSERT INTO @ClaimantPerDoc
	SELECT
		@LastDocID,
		@ClaimantList
SET @ClaimantList = NULL
END
END
CLOSE Cur_Complaints
DEALLOCATE Cur_Complaints
 

--Declare a cursor and variables to be used in the cursor to create a separate email for each firm.

DECLARE @FirmAddressID nvarchar (10), @firmURL nvarchar (100)

DECLARE firm_Curs CURSOR FOR SELECT
	nct.FirmAddressID,
	'https://ligado.kcic.com/Claims/Dashboard?ClientSiteID=19'
FROM @NewComplaintsTable nct
GROUP BY nct.FirmAddressID UNION SELECT
	doc.FirmAddressID,
	'https://ligado.kcic.com/Claims/Dashboard?ClientSiteID=19'
FROM @NewDocumentsTable doc
GROUP BY doc.FirmAddressID


OPEN firm_curs
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmURL
WHILE @@FETCH_STATUS = 0
BEGIN

--Declare and populate the body of the email for individual firms
DECLARE @BodyMessage nvarchar (MAX)
--Set the font, color, and border style for the tables. These style settings will remain for any table in this query.
--TABLE is for table wide style. TD is for cell specific style. TH is for the table header style.
SET @BodyMessage = @LogoHeader +
'<STYLE TYPE="text/css">
<!--
TABLE {border-collapse: collapse;
      border: 1px solid #38160C;
      color: #F6ECF0;}
TD    {font-family: Goudy Old Style; 
      font-size: 10pt; 
      text-align: center;
      border: 1px solid rgb(0,121,193);
      padding: .3em;
      color: #000000}
TH    {border: 1px solid #ffffff;
      padding: .3em;
      color: #F6ECF0;
      background: rgb(0,121,193);}
UL {margin:0; 
	padding:0;}	      
--->
</STYLE>'

DECLARE @TDStyle nvarchar (max) = 'font-family: Goudy Old Style; font-size: 10pt; text-align: center; border: 1px solid rgb(0,121,193); padding: .3em; color: #000000'

--Set table column headings and populate the cells with the Invoices from @NewInvoicesTable
IF (SELECT
	COUNT(docTitle)
FROM @NewComplaintsTable
WHERE FirmAddressID = @FirmAddressID) > 0
begin
SET @BodyMessage = @BodyMessage + '<H4>The following complaints were uploaded more than 4 days ago and have not yet been accepted: </H4>'
SET @BodyMessage = @BodyMessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Date Served*</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		docTitle)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		ClaimantList)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		MAX(ServiceDate))
	AS td,
	''
FROM @NewComplaintsTable C
INNER JOIN @ClaimantPerDoc cl ON c.DocID = cl.DocID
WHERE c.FirmAddressID = (SELECT
	@FirmAddressID)
GROUP BY	docTitle,
			cl.ClaimantList

FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'
end
ELSE
begin
SET @BodyMessage = @BodyMessage + '<H4>There are currently no outstanding complaints. </H4>'
end


IF (SELECT
	COUNT(docTitle)
FROM @NewDocumentsTable
WHERE FirmAddressID = @FirmAddressID) > 0
begin
SET @BodyMessage = @BodyMessage + '<H4>The following documents were uploaded more than 4 days ago and have not yet been accepted: </H4>'

SET @BodyMessage = @BodyMessage + N'<table style = "border-collapse: collapse; border: 1px solid #38160C; color: #F6ECF0;">' +
N'<tr><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Title</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Linked Claimants</th><th style = "border: 1px solid #ffffff; font-size: 10pt; padding: .3em; color: #F6ECF0; background: rgb(0,121,193);">Document Type</th>' +
CAST((SELECT
	@TDStyle AS 'td/@style',
	(SELECT
		docTitle)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		cl.ClaimantList)
	AS td,
	'',
	@TDStyle AS 'td/@style',
	(SELECT
		DocType)
	AS td,
	''
FROM @NewDocumentsTable d
INNER JOIN @ClaimantPerDoc cl ON d.DocID = cl.DocID
WHERE FirmAddressID = (SELECT
	@FirmAddressID)
GROUP BY	docTitle,
			cl.ClaimantList,
			DocType

FOR xml PATH ('tr'), TYPE)
AS nvarchar(MAX)) +
N'</table>' + N'<br/>'
end
ELSE
begin
SET @BodyMessage = @BodyMessage + '<H4>There are currently no other documents awaiting acceptance.</H4>'
end

--Undo the HTML stripping from the xml path function above so embedded tags work
SET @BodyMessage = REPLACE(REPLACE(@BodyMessage, '&lt;', '<'), '&gt;', '>')

--Link for document library
SET @BodyMessage = @BodyMessage + '<br><b>Click <A Href = ' + (SELECT
	@firmURL)
+ '>here</A> to access the document(s).</b><br><br>'

--KCIC address and disclaimer
SET @BodyMessage = @BodyMessage + '<br><br>KCIC, LLC<br>
733 10th Street, NW, Suite 3001<br>
Washington, DC 20001<br>
(t) 202-772-2300<br>
(f) 202-772-2333'


DECLARE @RecipientList nvarchar (500)
SET @RecipientList = ''
DECLARE @CCRecipientList nvarchar (500)
SET @CCRecipientList = ''

--Use the complaintContact field to determine the contact list.
--Declare a cursor to create a recipient list for the email.
--DECLARE @Contact nvarchar (500)
--DECLARE contact_curs CURSOR
--LOCAL FORWARD_ONLY STATIC READ_ONLY
--FOR SELECT
--DISTINCT
--	email
--FROM vEmailRecipients e
--WHERE FirmAddressID = @FirmAddressID and e.roleid=57

--OPEN contact_curs
--FETCH NEXT FROM contact_curs INTO @contact
--WHILE @@FETCH_STATUS = 0
--BEGIN

--SET @RecipientList = @RecipientList + @Contact + '; '

--FETCH NEXT FROM contact_curs INTO @contact
--END
--CLOSE contact_curs
--DEALLOCATE contact_curs

---- Populate CC Recipients
--DECLARE @ContactCC nvarchar (500)
--DECLARE contact_cursCC CURSOR LOCAL FORWARD_ONLY STATIC READ_ONLY
--FOR SELECT
--DISTINCT
--	email
--FROM vEmailRecipients e
--WHERE e.RoleID=61


--OPEN contact_cursCC
--FETCH NEXT FROM contact_cursCC INTO @contactCC
--WHILE @@FETCH_STATUS = 0
--BEGIN
--SET @CCRecipientList = @CCRecipientList + @ContactCC + '; '
--FETCH NEXT FROM contact_cursCC INTO @contactCC
--END
--CLOSE contact_cursCC
--DEALLOCATE contact_cursCC

--PRINT @RecipientList
--PRINT @CCRecipientList
--Execute stored procedure to send the email to that firm
EXEC msdb.dbo.sp_send_dbmail 
@profile_name = 'Claims',
--@recipients = 'hagep@kcic.com',
@recipients = 'gsx@kcic.com',
@blind_copy_recipients = 'cornellt@kcic.com',
@subject = @subject,
@body = @BodyMessage,
@body_format = 'html'

--Move to the next firm to create a new email
FETCH NEXT FROM firm_curs INTO @FirmAddressID, @firmurl
END
CLOSE firm_curs
DEALLOCATE firm_curs

END






GO
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailUnaccepted5DaysComplaints] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailUnaccepted5DaysComplaints] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailUnaccepted5DaysComplaints] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryInternalEmailUnaccepted5DaysComplaints] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryInternalEmailUnaccepted5DaysComplaints] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryInternalEmailUnaccepted5DaysComplaints] TO [power_user]
GO
