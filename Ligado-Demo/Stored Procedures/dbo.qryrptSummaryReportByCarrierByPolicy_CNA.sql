SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[qryrptSummaryReportByCarrierByPolicy_CNA]
(@AllocationID int)
As
Set Nocount on

--Declare @AllocationID int
--Set @AllocationID = 51

--
Declare @LondonAllocation money
Declare @Header2 nvarchar(max)
Declare @ExhaustionTypeID int
Declare @Sig int
Declare @LimitTypeID int
Set @ExhaustionTypeID = (Select ExhaustionTypeID from tblAllocation where AllocationID = @AllocationID)
Set @Header2 = (Select Convert(nvarchar(50),[Month])+' '+Convert(nvarchar(50),[Year])+' - '+Convert(nvarchar(50),BillNumber) from tblAllocation where AllocationID=@AllocationID)
Set @Sig = (Select SignatoryRun From tblAllocation where AllocationID = @AllocationID)
Set @LimitTypeID = (Select LimitTypeID from tblAllocation where AllocationID=@AllocationID)


Declare @PreviousExhaustionTotals table (PolicyID int, --PolicyDateID int, 
PreviousAllocationAmt money)
Insert Into @PreviousExhaustionTotals

Select p.policyid, --pd.PolicyDateID,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAllocationAmt
From tblExhaustion E
INner Join tblExhaustionDate ed on ed.ExhaustionDateID=e.ExhaustionDateID
inner join tblPolicyDates pd on pd.PolicyDateID=e.PolicyDateID
inner join tblPolicy p on p.PolicyID=pd.policyid
Where ExhaustionTypeID = @ExhaustionTypeID
and CarrierID <> 20
Group By p.policyid, pd.PolicyDateID
union
Select p.policyid, --pd.PolicyDateID,
	coalesce(Sum(DefenseAllocation + IndemnityAllocation),0) as PreviousAllocationAmt
From tblExhaustion E
INner Join tblExhaustionDate ed on ed.ExhaustionDateID=e.ExhaustionDateID
inner join tblPolicyDates pd on pd.PolicyDateID=e.PolicyDateID
inner join tblPolicy p on p.PolicyID=pd.policyid
inner join mtblLinkingLondonPolicies llp on llp.ReportingPolicyID=p.PolicyID
Where ExhaustionTypeID = @ExhaustionTypeID
Group By p.policyid, pd.PolicyDateID



--OriginalLondonNumbers
Declare @LondonOriginal table (PolicyID int, PolicyNum nvarchar(500), perocclimit money, aggregatelimit money, pstartdate datetime, penddate datetime)
Insert into @LondonOriginal
select ReportingPolicyID, PolicyNum, PerOccLimit, AggregateLimit, PStartDate, PEndDate 
from mtblLinkingLondonPolicies llp0
inner join tblPolicy p on p.PolicyID=llp0.ReportingPolicyID
inner join tblPolicyLimits pl on pl.PolicyID=p.PolicyID
inner join tblPolicyDates pd on pd.PolicyID=p.PolicyID
where LimitTypeID=4
order by PolicyNum

--London Solvent Percentages
Declare @SolventAllocation table (PolicyID int, DanaReportingStatus nvarchar(max), Percentage float)
Insert Into @SolventAllocation
Select p.PolicyID, DanaReportingStatus, SUM(Percentage)
from tblpolicy p 
	Left Join tblSubscriptionCoverage sc on p.PolicyID=sc.PolicyID
	Left Join tblSubscriberCompany subc on sc.SubscriberID=Subc.subscriberID
	Left Join mtblDanaSubscriberReportingStatus dsrs on dsrs.subscriberid = subc.SubscriberID
	Left Join mtblDanaReportingStatus drs on dsrs.Danareportingstatusid = drs.Danareportingstatusid
Where p.policyid in (select ReportingPolicyID from mtblLinkingLondonPolicies) and DanaReportingStatus='Solvent'
Group by p.PolicyID, DanaReportingStatus

--London Insolvent Allocation Eroding Limits
Declare @InsolventAllocation table (PolicyID int, InsolventAllocation money)
Insert Into @InsolventAllocation

Select 
	coalesce(llp1.reportingpolicyid, p.policyid) as PolicyID,
	coalesce(Sum(AllocationIndemnity + AllocationDefense), 0) as InsolventAllocation
	From tblAllocationResults AR
inner join tblPolicyDates pd on pd.PolicyDateID=AR.PolicyDateID
inner join tblPolicy p on p.PolicyID=pd.PolicyID
	Left Join mtblLinkingLondonPolicies llp1 on llp1.InsolventCommutedPolicyID=p.PolicyID

Where AllocationID = @AllocationID 
Group BY 
--ar.PolicyDateID, 
coalesce(llp1.reportingpolicyid, p.policyid)


--Current Allocation
Declare @CurrentAllocation Table (--PolicyDateID bigint, 
PolicyID int,
	Indemnity money, Defense money, DefenseOutsideLimits money, totaldefense money,  SIRIndemnity money,
	SIRDefense money)
Insert Into @CurrentAllocation
Select --ar.PolicyDateID, 
	coalesce(llp1.reportingpolicyid, llp2.reportingpolicyid, p.policyid) as PolicyID,
	coalesce(Sum(AllocationIndemnity), 0) as Indemnity,
	coalesce(Sum(AllocationDefense), 0) as Defense,
	coalesce(Sum(AllocationDefenseOutsideLimits),0) as DefenseOutsideLimits,
	coalesce(Sum(AllocationDefense + AllocationDefenseOutsideLimits),0) as TotalDefense,
	coalesce(Sum(SIRAllocatedIndemnity), 0) as SIRIndemnity,
	coalesce(Sum(SIRAllocatedDefense), 0) as SIRDefense
From tblAllocationResults AR
inner join tblPolicyDates pd on pd.PolicyDateID=AR.PolicyDateID
inner join tblPolicy p on p.PolicyID=pd.PolicyID
	Left Join mtblLinkingLondonPolicies llp1 on llp1.InsolventCommutedPolicyID=p.PolicyID
	Left Join mtblLinkingLondonPolicies llp2 on llp2.SolventPolicyID=p.PolicyID
Where AllocationID = @AllocationID 
Group BY --ar.PolicyDateID, 
coalesce(llp1.reportingpolicyid, llp2.reportingpolicyid, p.policyid)

If @Sig = 1 Begin
Declare @NonSigAllocation int
Set @NonSigAllocation = (Select NonSigAllocationID From tblAllocation where AllocationID = @AllocationID)
Set @LondonAllocation = (Select (SUM(Allocation)) from tblAllocationResults ar Inner Join tblPolicyDates pd on pd.PolicyDateID = AR.PolicyDateID
Inner Join tblPolicy p on p.PolicyID = pd.PolicyID where AllocationID = @AllocationID and carrierid=20) 


Insert Into @CurrentAllocation
Select --ar.PolicyDateID, 
p.PolicyID,
	coalesce(Sum(AllocationIndemnity), 0) as Indemnity,
	coalesce(Sum(AllocationDefense), 0) as Defense,
	coalesce(Sum(AllocationDefenseOutsideLimits),0) as DefenseOutsideLimits,
	coalesce(Sum(AllocationDefense + AllocationDefenseOutsideLimits),0) as TotalDefense,
	coalesce(Sum(SIRAllocatedIndemnity), 0) as SIRIndemnity,
	coalesce(Sum(SIRAllocatedDefense), 0) as SIRDefense
From tblAllocationResults AR
Inner Join tblPolicyDates pd on pd.PolicyDateID = AR.PolicyDateID
Inner Join tblPolicy p on p.PolicyID = pd.PolicyID
Where AllocationID = @NonSigAllocation and p.CarrierID in (121,336,107) 
Group BY --ar.PolicyDateID, 
p.PolicyID

End

--Query Results
Declare @Results Table(OrderID int, policyid bigint, --policydateid bigint, 
carriername nvarchar(100), layer int, policynum nvarchar(100), pstartdate datetime, penddate datetime, attachmentpoint money, perocclimit money, aggregatelimit money,
	DefenseForm nvarchar(100), Solvency nvarchar(100), TotalPercentage float, Indemnity money, Defense money, DefenseOutsideLimits money, TotalDefense money, PreviousAllocationAmt money,
	Balance money, Adjustments money)
Insert Into @Results

--London

select 1 as OrderID, ca.PolicyID, 
--pd.policydateid, 
c.carriername, pl.layer, 
coalesce(lo.PolicyNum,p.policynum) as PolicyNum,
lo.pstartdate, lo.penddate,
pl.attachmentpoint, 
lo.perocclimit as perocclimit, 
lo.aggregatelimit as aggregatelimit,

CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	'',
	'' as TotalPercentage, 
	sum(coalesce(Indemnity,0)) as Indemnity, 
	sum(coalesce(Defense,0)) as Defense, 
	sum(coalesce(DefenseOutsideLimits,0)) as DefenseOutsideLimits, 
	sum(coalesce(TotalDefense,0)) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0)as PreviousAllocationAmt,
	coalesce(lo.AggregateLimit,0) - coalesce((PreviousAllocationAmt * Percentage),0) - coalesce(Indemnity,0) - coalesce(Defense,0) as Balance,
		 0 as Adjustments
	
	--Case When @Sig =1 then sum([Indemnity]) * (Percentage) * -1 + sum([Defense])* (Percentage) * -1 + sum([DefenseOutsideLimits])  * (Percentage) * -1 else 0 end as Adjustments
	
from tblpolicy p 
	--Left join tblpolicydates pd on p.policyid = pd.policyid
		Inner Join @CurrentAllocation Ca on Ca.PolicyID = p.PolicyID
	left Join @PreviousExhaustionTotals PET on PET.PolicyID=p.PolicyID
	inner join tblcarrier c on c.carrierid = p.carrierid
	Left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID

	Left Join mtblLinkingLondonPolicies llp1 on llp1.InsolventCommutedPolicyID=p.PolicyID --Added
	Left Join mtblLinkingLondonPolicies llp2 on llp2.SolventPolicyID=p.PolicyID --Added
	Left join @LondonOriginal lo on lo.PolicyID=Ca.PolicyID
	left join @SolventAllocation sa on sa.PolicyID=lo.PolicyID
	left join @InsolventAllocation ia on ia.PolicyID=lo.PolicyID
Where limittypeid = @LimitTypeID and provisionlabelID=1 and C.CarrierID=20
group by ca.PolicyID, --pd.policydateid, 
c.carriername, pl.layer, coalesce(lo.PolicyNum,p.policynum), 
Indemnity, Defense,
DefenseOutsideLimits, 
lo.pstartdate, lo.penddate, pl.attachmentpoint,
	lo.perocclimit, lo.aggregatelimit, 
	--totaldefense, 
	pro.ProvisionValueID,
	PreviousAllocationAmt,
	Percentage, InsolventAllocation
	
	--select * from @Results
	
		
Union
--Most Carriers
select 1 as OrderID, p.policyid, --pd.policydateid, 
c.carriername, pl.layer, p.policynum, pd.pstartdate, pd.penddate,
	pl.attachmentpoint, pl.perocclimit,
	Case When p.PolicyID= 95 then 750000 else pl.aggregatelimit end as aggregatelimit,
CASE	When provisionvalueID=2 then 'H'
	when provisionvalueID=3 then 'G'
	when provisionvalueID=4 then 'I'
	end as DefenseForm,
	'' As Solvency,
	'' as TotalPercentage,
	coalesce(Indemnity,0) as Indemnity, 
	coalesce(Defense,0) as Defense, 
	coalesce(DefenseOutsideLimits,0) as DefenseOutsideLimits, 
	coalesce(TotalDefense,0) as TotalDefense, 
	coalesce(PreviousAllocationAmt,0) as PreviousAllocationAmt,
 	Case When p.PolicyID= 95 then(750000 - coalesce(Indemnity,0) - coalesce(Defense,0)  - coalesce(PreviousAllocationAmt,0))else
 	(coalesce(AggregateLimit,0) - coalesce(Indemnity,0) - coalesce(Defense,0)  - coalesce(PreviousAllocationAmt,0)) end as Balance,
 	0 as Adjustments
from tblpolicy p 
	left join tblpolicydates pd on p.policyid = pd.policyid
	left Join @PreviousExhaustionTotals PET on P.PolicyID=PET.PolicyID
	inner Join @CurrentAllocation CA on P.PolicyID=CA.PolicyID
	inner join tblcarrier c on c.carrierid = p.carrierid
	left join tblpolicylimits pl on p.policyid = pl.policyid
	left join tblprovision pro on p.policyID=pro.policyID
Where limittypeid = @LimitTypeID and provisionlabelID=1 and p.CarrierID <> 20
Union
--Dana Producer (change sig amount every month)
select 2 as OrderID, '', --'', 
'DANA', null as layer, 'Producer' as policynum, null as pstartdate, null as penddate,
	null as attachmentpoint, null as perocclimit, null as aggregatelimit, '' as DefenseForm,
	'' As Solvency,
	'' as TotalPercentage,
	Sum(Coalesce(Case when ot.DefenseType = 0 Then UncollapsedPrior+UncollapsedPost End,0)) as Indemnity,
    0,
    Sum(Coalesce(Case when ot.DefenseType = 1 Then UncollapsedPrior+UncollapsedPost End,0)) as DefenseOutsideLimits,
	Sum(Coalesce(Case when ot.DefenseType = 1 Then UncollapsedPrior+UncollapsedPost End,0)) as TotalDefense, 
	Case when @Sig=0 then 2122813.77 when @Sig=1 then 22373833.00 end as PreviousAllocationAmt,
 	null as Balance,
 	0 as Adjustments
from tblAllocationResultsHorizontal arh 
Inner Join tblOccurrence o on o.occurrenceID = arh.occurrenceID
Inner Join tblOccurrenceType ot on ot.occurrenceTypeID = o.occurrenceTypeID
Where AllocationID = @AllocationID 

Set @LondonAllocation = (Select sum(Adjustments)*-1 from @Results) --where carrierid=20) 

Select R.*, @AllocationID as AllocationID, @LimitTypeID as LimitTypeID, --@Header1 as Header1,
 @Header2 as Header2
From @Results R
order by OrderID, carriername, pstartdate


Return
GO
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_CNA] TO [base_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_CNA] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_CNA] TO [Ligado_User]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_CNA] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_CNA] TO [power_user]
GRANT EXECUTE ON  [dbo].[qryrptSummaryReportByCarrierByPolicy_CNA] TO [power_user]
GO
