SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vExhaustion]
AS

SELECT ExhaustionID, ex.ExhaustionDateID, exd.ExhaustionTypeID, ExhaustionType, ExhaustionDate, AllocationID, 
[Description], OccurrenceID, PolicyDateID, IndemnityAllocation, DefenseAllocation, DefenseOutsideLimitsAllocation, 
SIRIndemnityAllocation, SIRDefenseAllocation, LedgerID
FROM tblExhaustion EX
LEFT JOIN tblExhaustionDate EXD ON EXD.ExhaustionDateID = EX.ExhaustionDateID
LEFT JOIN tblExhaustionType EXT ON EXT.ExhaustionTypeID = EXD.ExhaustionTypeID

GO
GRANT VIEW DEFINITION ON  [dbo].[vExhaustion] TO [base_user]
GRANT SELECT ON  [dbo].[vExhaustion] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vExhaustion] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vExhaustion] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vExhaustion] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vExhaustion] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vExhaustion] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vExhaustion] TO [power_user]
GRANT SELECT ON  [dbo].[vExhaustion] TO [power_user]
GRANT INSERT ON  [dbo].[vExhaustion] TO [power_user]
GRANT DELETE ON  [dbo].[vExhaustion] TO [power_user]
GRANT UPDATE ON  [dbo].[vExhaustion] TO [power_user]
GO
