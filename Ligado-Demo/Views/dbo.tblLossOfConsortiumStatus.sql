SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[tblLossOfConsortiumStatus]
AS
SELECT     StatusID AS LossOfConsortiumStatusID, Status AS LossOfConsortiumStatus
FROM         [KGA-ClaimsAdmin].dbo.tblYesNoUnknown

GO
GRANT VIEW DEFINITION ON  [dbo].[tblLossOfConsortiumStatus] TO [base_user]
GRANT SELECT ON  [dbo].[tblLossOfConsortiumStatus] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblLossOfConsortiumStatus] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblLossOfConsortiumStatus] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblLossOfConsortiumStatus] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblLossOfConsortiumStatus] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblLossOfConsortiumStatus] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblLossOfConsortiumStatus] TO [power_user]
GRANT SELECT ON  [dbo].[tblLossOfConsortiumStatus] TO [power_user]
GRANT INSERT ON  [dbo].[tblLossOfConsortiumStatus] TO [power_user]
GRANT DELETE ON  [dbo].[tblLossOfConsortiumStatus] TO [power_user]
GRANT UPDATE ON  [dbo].[tblLossOfConsortiumStatus] TO [power_user]
GO
