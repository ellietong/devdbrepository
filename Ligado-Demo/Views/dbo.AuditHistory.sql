SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[AuditHistory]
AS
  SELECT L.AuditLogID, T.Owner, T.TableID, T.TableName, C.ColumnID,
         C.ColumnName, D.RowKey, D.Status, D.OldValue, D.NewValue,
         L.Event, L.PostedDateTime, L.UserName, L.HostName, L.ApplicationName,
         ValueIsText = case when D.Status & 1 <> 0 then 1 else 0 end,
         ValueIsImage = case when D.Status & 2 <> 0 then 1 else 0 end,
         ValueIsNText = case when D.Status & 4 <> 0 then 1 else 0 end,
         ValueIsCapture = case when D.Status & 8 <> 0 then 1 else 0 end,
         ValueIsLookup = case when D.Status & 16 <> 0 then 1 else 0 end
  FROM dbo.AuditLogDetail D WITH (nolock)
    INNER JOIN dbo.AuditLog L WITH (nolock) ON (D.AuditLogID = L.AuditLogID)
    INNER JOIN dbo.AuditTables T WITH (nolock) ON (L.TableID = T.TableID)
    INNER JOIN dbo.AuditColumns C WITH (nolock) ON (D.ColumnID = C.ColumnID)
GO
GRANT VIEW DEFINITION ON  [dbo].[AuditHistory] TO [base_user]
GRANT SELECT ON  [dbo].[AuditHistory] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[AuditHistory] TO [Ligado_User]
GRANT SELECT ON  [dbo].[AuditHistory] TO [Ligado_User]
GRANT INSERT ON  [dbo].[AuditHistory] TO [Ligado_User]
GRANT DELETE ON  [dbo].[AuditHistory] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[AuditHistory] TO [Ligado_User]
GRANT REFERENCES ON  [dbo].[AuditHistory] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditHistory] TO [omniaudit_admin]
GRANT INSERT ON  [dbo].[AuditHistory] TO [omniaudit_admin]
GRANT DELETE ON  [dbo].[AuditHistory] TO [omniaudit_admin]
GRANT UPDATE ON  [dbo].[AuditHistory] TO [omniaudit_admin]
GRANT SELECT ON  [dbo].[AuditHistory] TO [omniaudit_logreader]
GRANT VIEW DEFINITION ON  [dbo].[AuditHistory] TO [power_user]
GRANT SELECT ON  [dbo].[AuditHistory] TO [power_user]
GRANT INSERT ON  [dbo].[AuditHistory] TO [power_user]
GRANT DELETE ON  [dbo].[AuditHistory] TO [power_user]
GRANT UPDATE ON  [dbo].[AuditHistory] TO [power_user]
GO
