SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vSETTLEMENT_PaymentApproval]
AS

SELECT        cpi.LastName + ', ' + cpi.FirstName + COALESCE (' ' + cpi.MiddleName, '') AS ClaimantName, cl.ClaimLawsuitID, cs.ClaimSettlementID, c.ClaimantPersonalInfoID, pca.PlaintiffCounsel, cs.SettlementDate, 
                         cs.SettlementAmount, m.Matter, CASE WHEN PaymentApprovedBy IS NULL AND PaymentApprovedDate IS NULL THEN 0 ELSE 1 END AS IsApproved, CASE WHEN PaymentRecommendedBy IS NULL AND 
                         PaymentRecommendedDate IS NULL THEN 0 ELSE 1 END AS IsRecommended, COALESCE (SUM(ar.Amount), 0) AS TotalARAmount, o.ExpectedPayDate
FROM            dbo.tblClaim AS c INNER JOIN
                         dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                         dbo.tblMatter AS m ON c.MatterID = m.MatterID INNER JOIN
                         dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                         dbo.tblClaimLawsuitStatusTracking AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID INNER JOIN
                         dbo.tblLawsuit AS l ON l.LawsuitID = cl.LawsuitID LEFT OUTER JOIN
                         dbo.tblPlaintiffCounselAddress AS pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID INNER JOIN
                         dbo.tblClaimSettlement AS cs ON cl.ClaimLawsuitID = cs.ClaimLawsuitID LEFT OUTER JOIN
                         dbo.tblPaymentSettlement AS ps ON cs.ClaimSettlementID = ps.ClaimSettlementID LEFT OUTER JOIN
                         dbo.tblOccurrence AS o ON o.ClaimID = cl.ClaimLawsuitID INNER JOIN
                         dbo.tblExhaustion AS e ON e.OccurrenceID = o.OccurrenceID LEFT JOIN
                         dbo.tblAccountsReceivablePayment AS ar ON ar.ExhaustionID = e.ExhaustionID
WHERE         (COALESCE (clst.IsPrimaryStatus, 0) = 1) AND (clst.ClaimLawsuitStatusID IN (23, 24, 25, 39)) AND (cs.SettlementAmount >= 0)
				AND PaymentApprovedBy IS NULL And occurrencetypeid = 1
GROUP BY cpi.LastName + ', ' + cpi.FirstName + COALESCE (' ' + cpi.MiddleName, ''), cl.ClaimLawsuitID, cs.ClaimSettlementID, c.ClaimantPersonalInfoID, cl.LawsuitID, pca.PlaintiffCounsel, cs.SettlementDate, 
                         cs.SettlementAmount, m.Matter, CASE WHEN PaymentApprovedBy IS NULL AND PaymentApprovedDate IS NULL THEN 0 ELSE 1 END, CASE WHEN PaymentRecommendedBy IS NULL AND 
                         PaymentRecommendedDate IS NULL THEN 0 ELSE 1 END, o.ExpectedPayDate
--HAVING        (COALESCE (SUM(ps.PaymentAmount), 0) < cs.SettlementAmount)






--GO



GO
GRANT VIEW DEFINITION ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [base_user]
GRANT SELECT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT SELECT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT INSERT ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT DELETE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GRANT UPDATE ON  [dbo].[vSETTLEMENT_PaymentApproval] TO [power_user]
GO
