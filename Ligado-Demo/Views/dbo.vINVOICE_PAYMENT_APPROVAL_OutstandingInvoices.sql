SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE view [dbo].[vINVOICE_PAYMENT_APPROVAL_OutstandingInvoices] as
SELECT  TOP 10 di.DefenseInvoiceID,
		d.DocumentID,
		d.DocumentTitle,
		d.DocumentURL,				
		di.FirmID,
		dc.DefenseCounsel AS Firm,		
		dt.ClaimantDocumentType, 
		di.Period,
		SUM(dicd.Fees + dicd.Costs - dicd.Adjustments) AS InvoiceTotal,
		di.DatePayable,
		dis.[Status],
		d.AcceptedBy,
		d.DateAccepted,
		d.DateCreated,
		v.VendorName as VendorFirm,
		'PLACEHOLDER' AS CoverageLine,--InsuranceProgram AS CoverageLine,
		di.InvoiceNumber,
		IsRecommended,
		IsApproved,
		SUM(dicd.Fees) AS Fees,
		SUM(dicd.Costs) AS Costs,
		PrivateComment,
		SUM(dicd.Adjustments) AS Adjustments 
	FROM 
		tblDefenseInvoice di	
		INNER JOIN tblDocuments d ON di.DocumentID = d.DocumentID
		INNER JOIN tblClaimantDocumentType dt	ON d.ClaimantDocumentTypeID = dt.ClaimantDocumentTypeID
		INNER JOIN tblClaimantDocumentGroup dg	ON dt.ClaimantDocumentGroupID = dg.ClaimantDocumentGroupID
		--INNER JOIN vSEARCH_Invoice si			ON di.DefenseInvoiceID = si.DefenseInvoiceID
		--LEFT JOIN vtblDefenseInvoice dinv		ON dinv.DocumentID = d.DocumentID
		LEFT JOIN  tblDefenseInvoiceStatus dis	ON di.StatusID = dis.StatusID 
		INNER JOIN  tblDefenseCounsel dc			ON di.FirmID = dc.DefenseCounselID
		LEFT JOIN  tblPaymentDefense pd			ON di.DefenseInvoiceID = pd.DefenseInvoiceID
		LEFT JOIN  tblCheck c					ON pd.CheckID = c.CheckID
		LEFT JOIN  tblVendor v  				ON v.VendorID = di.VendorFirmID
		INNER JOIN tblDefInvClaimantDetail dicd	ON di.DefenseInvoiceID = dicd.DefenseInvoiceID 
		--LEFT JOIN tblInsuranceProgram ip		ON di.InsuranceProgramID = ip.InsuranceProgramID
    WHERE dt.ClaimantDocumentGroupID = 2-- AND (di.StatusID IS NULL OR di.StatusID = 3)
	GROUP BY 
		di.DefenseInvoiceID,
		d.DocumentID,
		d.DocumentTitle,
		d.DocumentURL,				
		di.FirmID,
		dc.DefenseCounsel,		
		dt.ClaimantDocumentType, 
		di.Period,
		di.DatePayable,
		dis.[Status],
		d.AcceptedBy,
		d.DateAccepted,
		d.DateCreated,
		v.VendorName,
		di.InvoiceNumber,
		IsRecommended,
		IsApproved,
		PrivateComment


GO
GRANT SELECT ON  [dbo].[vINVOICE_PAYMENT_APPROVAL_OutstandingInvoices] TO [LigadoGSX]
GO
