SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE VIEW [dbo].[vDETAILS_SettlementDocumentation]
AS
SELECT cl.ClaimLawsuitID
       , cs.ClaimSettlementID
       , CONVERT(BIT, COALESCE (sd.ClaimantIDed, 0))             AS ClaimantIDed
       , CONVERT(BIT, COALESCE (sd.DODCert, 0))                  AS DODCert
       , CONVERT(BIT, COALESCE (sd.DxMedicals, 0))               AS DxMedicals
       , CONVERT(BIT, COALESCE (sd.EmployExpoHist, 0))           AS EmployExpoHist
       , CONVERT(BIT, COALESCE (sd.EstatePapers, 0))             AS EstatePapers
       , CONVERT(BIT, COALESCE (sd.ProductID, 0))                AS ProductID
       , CONVERT(BIT, COALESCE (sd.SettlementConfirmation, 0))   AS SettlementConfirmation
       , cs.ExecutedReleaseDate
       , cs.ReleaseRqstDate
       , cs.ReleaseToPC
       , COALESCE (rt.ReleaseType, '')                           AS ReleaseType
       , cpi.MedicareBeneficiaryStatusID
       , mbs.MedicareBeneficiaryStatus
       , CONVERT(BIT, COALESCE (sd.MMSEA, 0))                    AS MMSEA
       , CONVERT(BIT, COALESCE (sd.MedicarePlaintiff, 0))        AS MedicarePlaintiff
       , CONVERT(BIT, COALESCE (sd.MedicarePlaintiffCounsel, 0)) AS MedicarePlaintiffCounsel
       , cpi.DeceasedDate
       , cs.SettlePaymentComments
       , CONVERT(BIT, COALESCE (cs.MedicareReported, 0))         AS MedicareReported
       , CONVERT(BIT, COALESCE (sd.PreTrialReport, 0))           AS PreTrialReport
       , CONVERT(BIT, COALESCE (sd.ExecutedRelease, 0))          AS ExecutedRelease
       , CONVERT(BIT, COALESCE (sd.AllstateMMSEAForm, 0))        AS AllstateMMSEAForm
	   , CONVERT(BIT, COALESCE (sd.AllstateMMSEAForm, 0))        AS CarrierSpecificForm
       , CONVERT(BIT, COALESCE (sd.CompServ, 0))                 AS CompServ
       , cs.ReleaseTypeID
       , CONVERT(BIT, COALESCE(cs.MedicareFullyDocumented, 0))   AS MedicareFullyDocumented
       , MIN(clp.StartDate)                                      AS EarliestProductStartDate
FROM   DBO.TBLCLAIMLAWSUIT AS cl
       LEFT OUTER JOIN DBO.TBLCLAIMSETTLEMENT AS cs
                    ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
       LEFT OUTER JOIN DBO.TBLCLAIMSETTLEMENTDOCUMENTATION AS sd
                    ON sd.ClaimLawsuitID = cl.ClaimLawsuitID
       LEFT OUTER JOIN DBO.TBLRELEASETYPE AS rt
                    ON cs.ReleaseTypeID = rt.ReleaseTypeID
       LEFT OUTER JOIN DBO.TBLCLAIM AS c
                    ON cl.ClaimID = c.ClaimID
       LEFT OUTER JOIN DBO.TBLCLAIMANTPERSONALINFO AS cpi
                    ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID
       LEFT OUTER JOIN DBO.TBLMEDICAREBENEFICIARYSTATUS AS mbs
                    ON cpi.MedicareBeneficiaryStatusID = mbs.MedicareBeneficiaryStatusID
       LEFT OUTER JOIN DBO.TBLCLAIMLAWSUITPRODUCT clp
                    ON cl.ClaimLawsuitID = clp.ClaimLawsuitID
GROUP  BY cl.ClaimLawsuitID
          , cs.ClaimSettlementID
          , sd.ClaimantIDed
          , sd.DODCert
          , sd.DxMedicals
          , sd.EmployExpoHist
          , sd.EstatePapers
          , sd.ProductID
          , sd.SettlementConfirmation
          , cs.ExecutedReleaseDate
          , cs.ReleaseRqstDate
          , cs.ReleaseToPC
          , rt.ReleaseType
          , cpi.MedicareBeneficiaryStatusID
          , mbs.MedicareBeneficiaryStatus
          , sd.MMSEA
          , sd.MedicarePlaintiff
          , sd.MedicarePlaintiffCounsel
          , cpi.DeceasedDate
          , cs.SettlePaymentComments
          , cs.MedicareReported
          , sd.PreTrialReport
          , sd.ExecutedRelease
          , sd.AllstateMMSEAForm
          , sd.CompServ
          , cs.ReleaseTypeID
          , cs.MedicareFullyDocumented 

GO
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementDocumentation] TO [base_user]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementDocumentation] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementDocumentation] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementDocumentation] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vDETAILS_SettlementDocumentation] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vDETAILS_SettlementDocumentation] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vDETAILS_SettlementDocumentation] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vDETAILS_SettlementDocumentation] TO [power_user]
GRANT SELECT ON  [dbo].[vDETAILS_SettlementDocumentation] TO [power_user]
GRANT INSERT ON  [dbo].[vDETAILS_SettlementDocumentation] TO [power_user]
GRANT DELETE ON  [dbo].[vDETAILS_SettlementDocumentation] TO [power_user]
GRANT UPDATE ON  [dbo].[vDETAILS_SettlementDocumentation] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cs"
            Begin Extent = 
               Top = 6
               Left = 298
               Bottom = 114
               Right = 522
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sd"
            Begin Extent = 
               Top = 6
               Left = 568
               Bottom = 136
               Right = 790
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rt"
            Begin Extent = 
               Top = 6
               Left = 828
               Bottom = 102
               Right = 998
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 102
               Left = 828
               Bottom = 232
               Right = 1060
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 288
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mbs"
            Begin Extent = 
               Top = 138
               Left = 326
               Bottom = 234
               Right = 565
            End
            DisplayFlags = 280
            TopColumn = 0
        ', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SettlementDocumentation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SettlementDocumentation', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vDETAILS_SettlementDocumentation', NULL, NULL
GO
