SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vDETAILS_DefendantCompanyNamings]
AS
SELECT dc.DefendantCompany, COALESCE(a.DefendantCompanyID, 0) AS DefendantCompanyID, COALESCE(cl.LawsuitID, 0) AS LawsuitID
FROM   dbo.tblAllegation AS a INNER JOIN
             dbo.tblClaimLawsuit AS cl ON a.ClaimLawsuitID = cl.ClaimLawsuitID INNER JOIN
             dbo.tblDefendantCompany AS dc ON a.DefendantCompanyID = dc.DefendantCompanyID
WHERE DefendantCompanyParentID IS not NULL 
GROUP BY dc.DefendantCompany, a.DefendantCompanyID, cl.LawsuitID



GO
GRANT SELECT ON  [dbo].[vDETAILS_DefendantCompanyNamings] TO [LigadoGSX]
GO
