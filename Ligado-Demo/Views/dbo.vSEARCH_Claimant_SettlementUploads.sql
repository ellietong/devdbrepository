SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE VIEW [dbo].[vSEARCH_Claimant_SettlementUploads]
AS
SELECT     cl.ClaimLawsuitID, cpi.ClaimantPersonalInfoID, cpi.FullName, l.DocketNumber, 
                      CASE WHEN SocialSecurityNumber IS NULL THEN '' ELSE 'XXX-XX-' + RIGHT(SocialSecurityNumber, 4) END AS SocialSecurityNumber, m.Matter, c.MatterID, 
                      COALESCE (pca.PlaintiffCounsel, 'Unknown') + ' (' + COALESCE (pca.PlaintiffCounselCity, 'Unknown') + ')' AS PlaintiffCounsel, l.PlaintiffCounselAddressID, s.State, 
                      s.StateID, cl.IsMDL, cls.ClaimLawsuitStatus, cls.ClaimLawsuitStatusGroupID, pca.PlaintiffCounselID, cpi.NavInjuredPartyID, 
                      l.DefenseCounselAddressID, l.FileDate, clst.ClaimLawsuitStatusID, dj.DefenseCounselAddressID AS DefenseCounselAddressID2
FROM         dbo.tblClaim AS c INNER JOIN
                      dbo.tblClaimantPersonalInfo AS cpi ON c.ClaimantPersonalInfoID = cpi.ClaimantPersonalInfoID INNER JOIN
                      dbo.tblClaimLawsuit AS cl ON c.ClaimID = cl.ClaimID INNER JOIN
                      dbo.tblLawsuit AS l ON cl.LawsuitID = l.LawsuitID LEFT OUTER JOIN
                      dbo.tblMatter AS m ON c.MatterID = m.MatterID LEFT OUTER JOIN
                      tblState AS s ON l.StateID = s.StateID LEFT OUTER JOIN
                      tblPlaintiffCounselAddress AS pca ON l.PlaintiffCounselAddressID = pca.PlaintiffCounselAddressID LEFT OUTER JOIN
                          (SELECT     ClaimLawsuitStatusTrackingID, ClaimLawsuitID, ClaimLawsuitStatusID, StatusNotes, IsPrimaryStatus, NavStatus, ProcessedDate, StatusDate, 
                                                   SPUsername
                            FROM          dbo.tblClaimLawsuitStatusTracking
                            WHERE      (COALESCE (IsPrimaryStatus, 0) = 1)) AS clst ON cl.ClaimLawsuitID = clst.ClaimLawsuitID LEFT OUTER JOIN
                      dbo.tblClaimLawsuitStatus AS cls ON clst.ClaimLawsuitStatusID = cls.ClaimLawsuitStatusID
					  LEFT OUTER JOIN
					   dbo.tblDefenseJurisdiction AS dj ON l.StateID = dj.StateID AND l.JurisdictionID = dj.JurisdictionID AND c.MatterID = dj.MatterID LEFT OUTER JOIN
                      dbo.tblDefenseCounselAddress AS dca ON dj.DefenseCounselAddressID = dca.DefenseCounselAddressID

					  WHERE clst.ClaimLawsuitStatusID NOT IN (23, 25, 39) AND IsPrimaryStatus=1--Non-"Settled" statuses




GO
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [base_user]
GRANT SELECT ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [power_user]
GRANT SELECT ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [power_user]
GRANT INSERT ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [power_user]
GRANT DELETE ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [power_user]
GRANT UPDATE ON  [dbo].[vSEARCH_Claimant_SettlementUploads] TO [power_user]
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[53] 4[8] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 270
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cpi"
            Begin Extent = 
               Top = 6
               Left = 308
               Bottom = 136
               Right = 558
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 6
               Left = 596
               Bottom = 136
               Right = 818
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 277
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 138
               Left = 315
               Bottom = 234
               Right = 485
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 138
               Left = 523
               Bottom = 251
               Right = 693
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pca"
            Begin Extent = 
               Top = 252
               Left = 315
               Bottom = 382
               Right = 544
            End
            DisplayFlags = 280
            TopColumn = 0
         End', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Claimant_SettlementUploads', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'
         Begin Table = "clst"
            Begin Extent = 
               Top = 252
               Left = 582
               Bottom = 382
               Right = 831
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cls"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 274
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Claimant_SettlementUploads', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vSEARCH_Claimant_SettlementUploads', NULL, NULL
GO
