SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [dbo].[vREPORT_DISEASE_Dismissals]
AS

SELECT 
ClaimLawsuitID,
[State],
[Month] AS Period, 
PeriodSort,
CASE WHEN HighLevelDisease = 'Lung Cancer' THEN 1 ELSE 0 END AS LungCancerCount,
CASE WHEN HighLevelDisease = 'Mesothelioma' THEN 1 ELSE 0 END AS MesoCount,
CASE WHEN HighLevelDisease = 'Other Cancer' THEN 1 ELSE 0 END AS OtherCancerCount,
CASE WHEN HighLevelDisease = 'Non-Malignant' THEN 1 ELSE 0 END AS NonMaligCount,
CASE WHEN HighLevelDisease = 'Unknown' THEN 1 ELSE 0 END AS UnknownCount,
1 AS TotalCount,
StatusDate,
MatterID,
DefenseCounselAddressID,
DJDefenseCounselAddressID,
ClaimLawsuitTypeID
 FROM vREPORT_Dismissals
 




GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_DISEASE_Dismissals] TO [power_user]
GO
