SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [dbo].[vChecksPaid]  AS

SELECT c.CheckID, c.SourceNumber, c.SourceAmount, c.SourceDate, dca.DefenseCounsel AS Payee
FROM tblCheck c
INNER JOIN tblPaymentDefense pd ON c.CheckID = pd.CheckID
INNER JOIN tblDefenseInvoice di ON di.DefenseInvoiceID = pd.DefenseInvoiceID
INNER JOIN tblDefenseCounselAddress dca ON dca.DefenseCounselAddressID = di.DefenseCounselAddressID
WHERE CheckFundingTypeID=1

UNION
SELECT c.CheckID, c.SourceNumber, c.SourceAmount, c.SourceDate, pca.PlaintiffCounsel
FROM tblCheck c
INNER JOIN tblPaymentSettlement ps ON c.CheckID = ps.CheckID
INNER JOIN tblClaimSettlement cs ON cs.ClaimSettlementID = ps.ClaimSettlementID
INNER JOIN tblClaimLawsuit cl ON cl.ClaimLawsuitID = cs.ClaimLawsuitID
INNER JOIN tblLawsuit l ON l.LawsuitID = cl.LawsuitID
INNER JOIN tblPlaintiffCounselAddress pca ON pca.PlaintiffCounselAddressID = l.PlaintiffCounselAddressID
WHERE CheckFundingTypeID=2

UNION
SELECT c.CheckID, c.SourceNumber, c.SourceAmount, c.SourceDate, v.Firm
FROM tblCheck c
INNER JOIN tblPaymentDefense pd ON c.CheckID = pd.CheckID
INNER JOIN tblDefenseInvoice di ON di.DefenseInvoiceID = pd.DefenseInvoiceID
INNER JOIN vtblFirmsAndVendors v ON v.FirmID=di.VendorFirmID
WHERE CheckFundingTypeID=3



GO
GRANT VIEW DEFINITION ON  [dbo].[vChecksPaid] TO [base_user]
GRANT SELECT ON  [dbo].[vChecksPaid] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vChecksPaid] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vChecksPaid] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vChecksPaid] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vChecksPaid] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vChecksPaid] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vChecksPaid] TO [power_user]
GRANT SELECT ON  [dbo].[vChecksPaid] TO [power_user]
GRANT INSERT ON  [dbo].[vChecksPaid] TO [power_user]
GRANT DELETE ON  [dbo].[vChecksPaid] TO [power_user]
GRANT UPDATE ON  [dbo].[vChecksPaid] TO [power_user]
GO
