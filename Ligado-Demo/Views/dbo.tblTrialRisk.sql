SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[tblTrialRisk]
AS
SELECT     TrialRiskID, TrialRisk
FROM         WEBSQL.[KGA-ClaimsAdmin].dbo.tblTrialRisk AS tblTrialRisk_FromClaimsAdmin

GO
GRANT VIEW DEFINITION ON  [dbo].[tblTrialRisk] TO [base_user]
GRANT SELECT ON  [dbo].[tblTrialRisk] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[tblTrialRisk] TO [Ligado_User]
GRANT SELECT ON  [dbo].[tblTrialRisk] TO [Ligado_User]
GRANT INSERT ON  [dbo].[tblTrialRisk] TO [Ligado_User]
GRANT DELETE ON  [dbo].[tblTrialRisk] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[tblTrialRisk] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[tblTrialRisk] TO [power_user]
GRANT SELECT ON  [dbo].[tblTrialRisk] TO [power_user]
GRANT INSERT ON  [dbo].[tblTrialRisk] TO [power_user]
GRANT DELETE ON  [dbo].[tblTrialRisk] TO [power_user]
GRANT UPDATE ON  [dbo].[tblTrialRisk] TO [power_user]
GO
