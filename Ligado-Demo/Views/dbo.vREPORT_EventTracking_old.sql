SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vREPORT_EventTracking_old]
AS

SELECT	cpi.FullName,
		cpi.ClaimantPersonalInfoID,
		cl.ClaimLawsuitID,
		evtype.EventType,
		et.EventTypeID,
		EventDate,
		EventDescription
FROM tblClaimantPersonalInfo cpi
INNER JOIN tblClaim c ON cpi.ClaimantPersonalInfoID = c.ClaimantPersonalInfoID
INNER JOIN tblClaimLawsuit cl ON c.ClaimID = cl.ClaimID
INNER JOIN tblEventTracking et ON et.ClaimLawsuitID = cl.ClaimLawsuitID
INNER JOIN tblEventType evtype ON et.EventTypeID = evtype.EventTypeID


GO
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_EventTracking_old] TO [base_user]
GRANT SELECT ON  [dbo].[vREPORT_EventTracking_old] TO [base_user]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_EventTracking_old] TO [Ligado_User]
GRANT SELECT ON  [dbo].[vREPORT_EventTracking_old] TO [Ligado_User]
GRANT INSERT ON  [dbo].[vREPORT_EventTracking_old] TO [Ligado_User]
GRANT DELETE ON  [dbo].[vREPORT_EventTracking_old] TO [Ligado_User]
GRANT UPDATE ON  [dbo].[vREPORT_EventTracking_old] TO [Ligado_User]
GRANT VIEW DEFINITION ON  [dbo].[vREPORT_EventTracking_old] TO [power_user]
GRANT SELECT ON  [dbo].[vREPORT_EventTracking_old] TO [power_user]
GRANT INSERT ON  [dbo].[vREPORT_EventTracking_old] TO [power_user]
GRANT DELETE ON  [dbo].[vREPORT_EventTracking_old] TO [power_user]
GRANT UPDATE ON  [dbo].[vREPORT_EventTracking_old] TO [power_user]
GO
